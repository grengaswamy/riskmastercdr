﻿
using System;
using System.Xml ;
using Riskmaster.Common ;
namespace Riskmaster.Application.AsyncManagement
{
	/// <summary>
	/// Summary description for AsyncHelper.
	/// </summary>
	public class AsyncDbParams
	{
		private int m_iStatus ;
		private string m_sExportId ;
		private double m_fPercentageCompletion ;
		public int Status
		{
			get
			{
				return m_iStatus;

			}
			set
			{
				m_iStatus= value;
			}
		}
		public string ExportId
		{
			get
			{
				return m_sExportId;

			}
			set
			{
				m_sExportId= value;
			}
		}
		public double PercentageCompletion
		{
			get
			{
				return m_fPercentageCompletion;

			}
			set
			{
				try
				{
					m_fPercentageCompletion= Convert.ToDouble(value) ;
				}
				catch
				{
					m_fPercentageCompletion = 0;
				}
			}
		}
		/// <summary>
		///  Relying on manual serialization/de-serialization, native support for the same may have some performance overhead.
		/// </summary>
		/// <param name="p_objParamsDoc"></param>
		public void Deserialize(ref XmlDocument p_objParamsDoc)
		{
			this.ExportId =p_objParamsDoc.SelectSingleNode("//AsynsDbParams/ExportId").InnerText;
			this.Status = Conversion.ConvertStrToInteger(p_objParamsDoc.SelectSingleNode("//AsynsDbParams/Status").InnerText);
			try
			{
				this.PercentageCompletion =Conversion.ConvertStrToDouble( p_objParamsDoc.SelectSingleNode("//AsynsDbParams/PercentageCompletion").InnerText ) ;
			}
			catch
			{
				this.PercentageCompletion = 0;
			}


		}
		/// <summary>
		/// Relying on manual serialization/de-serialization, native support for the same may have some performance overhead.
		/// </summary>
		/// <param name="p_objParamsDoc"></param>
		public void Serialize(ref XmlDocument p_objParamsDoc)
		{
			p_objParamsDoc.SelectSingleNode("//AsynsDbParams/ExportId").InnerText = this.ExportId.ToString();
			p_objParamsDoc.SelectSingleNode("//AsynsDbParams/Status").InnerText = this.Status.ToString();
			p_objParamsDoc.SelectSingleNode("//AsynsDbParams/PercentageCompletion").InnerText =this.PercentageCompletion.ToString();

		}

	}
	public class AsyncJobParams
	{
		private string m_sAssemblyName ;
		private string m_sClassName ;
		private string m_sFunctionName ;
		public string AssemblyName
		{
			get
			{
				return m_sAssemblyName;

			}
			set
			{
				m_sAssemblyName= value;
			}
		}
		public string ClassName
		{
			get
			{
				return m_sClassName;

			}
			set
			{
				m_sClassName= value;
			}
		}
		public string FunctionName
		{
			get
			{
				return m_sFunctionName;

			}
			set
			{
				m_sFunctionName= value;
			}
		}
		/// <summary>
		///  Relying on manual serialization/de-serialization, native support for the same may have some performance overhead, though not tried yet.
		/// </summary>
		/// <param name="p_objParamsDoc"></param>
		public void Deserialize(ref XmlDocument p_objParamsDoc)
		{
			
			//string sAssemblyKey = p_objParamsDoc.SelectSingleNode("//AssemblyInfoKey").InnerText ;
            //TODO: Determine how this is actually supposed to work
            RMAdapterElement rmExtensibilityAdapter = RMConfigurationManager.GetRMExtensibilityAdaptors()[0];

            this.AssemblyName = rmExtensibilityAdapter.Assembly;
            this.ClassName = rmExtensibilityAdapter.Class;
            this.FunctionName = rmExtensibilityAdapter.Method;
		}
		

	}

	public class UIDisplayParams
	{
		int m_iOverAllStatusForUI ;
		string m_sMessage ;
		public int OverAllStatusForUI
		{
			set
			{
				m_iOverAllStatusForUI = value ;
			}
			get
			{
				return m_iOverAllStatusForUI ;
			}
		}
		public string MessageForUI
		{
			set
			{
				m_sMessage =value ;
			}
			get
			{
				return m_sMessage ;
			}
		}
		public void Serialize(ref XmlDocument p_objDoc)
		{
			p_objDoc.SelectSingleNode("//UIParams//ExportStatus").InnerText = m_iOverAllStatusForUI.ToString() ;
			p_objDoc.SelectSingleNode("//UIParams//Message").InnerText = m_sMessage ;

		}
		public void Deserialize(ref XmlDocument p_objDoc)
		{
            this.OverAllStatusForUI = Conversion.ConvertObjToInt(p_objDoc.SelectSingleNode("//UIParams//ExportStatus").InnerText, 0);
			this.MessageForUI = p_objDoc.SelectSingleNode("//UIParams//Message").InnerText  ;
		}
	}
	
	public class Utility
	{
        public static string GetSessionDSN(int p_iClientId)
		{
            return RMConfigurationSettings.GetSessionDSN(p_iClientId);
		}
		
			public static void RemoveAllChildNodes(ref XmlDocument p_objSrc , string p_sNode )
			{
				try
				{ 
              
					p_objSrc.SelectSingleNode("//" + p_sNode ).RemoveAll() ;

				}
				catch(Exception p_objErr)
				{
					throw p_objErr ;
				}
				finally
				{
				}
			}
		public static void ImportXmlNodes(ref XmlDocument p_objSrc , ref XmlDocument p_objTarget, string p_sNodeToBeImported, string p_sInsertAfter)
		{
			try
			{ 
              
			  p_objTarget.SelectSingleNode("//" + p_sInsertAfter ).AppendChild(p_objTarget.ImportNode(p_objSrc.SelectSingleNode("//" + p_sNodeToBeImported),true));

			}
			catch(Exception p_objErr)
			{
				throw p_objErr ;
			}
			finally
			{
			}
		}
		public static void CopyChildNodesUnderRootNode(ref XmlDocument p_objTarget , ref XmlDocument p_objSrc, string p_sNodeNameInTarget)
		{
			try
			{ 
              
				XmlNodeList objSrcNodesList =  p_objSrc.DocumentElement.ChildNodes ;
				foreach( XmlNode objSrcNode  in objSrcNodesList )
				{
					
                   p_objTarget.DocumentElement.AppendChild(p_objTarget.ImportNode(objSrcNode,true)) ;
				}
				
 
			}
			catch(Exception p_objErr)
			{
				throw p_objErr ;
			}
			finally
			{
			}
		}
        public static void LoggerForWorkerThreads(Exception p_objException, string p_sMessage, int p_iClientId)
		{
			Exception objInnerMost;
			LogItem objEr=new LogItem();
			try
			{
				 
				Utility.WorkerForException(p_objException,out objInnerMost);
				
				objEr.Message= p_sMessage ;

				objEr.RMExceptSource = p_objException.Source;
				objEr.Category = "CommonWebServiceLog";
				objEr.RMExceptStack = p_objException.StackTrace;

				objEr.RMParamList.Add("Actual Exception",objInnerMost.Message);
                Log.Write(objEr, p_iClientId);
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				objEr = null ;
			}

		}
		public static void WorkerForException( Exception p_objException,out Exception p_objRetException)
		{
			try
			{ 
				if(p_objException.InnerException!=null)
				{
					WorkerForException(p_objException.InnerException,out p_objRetException);
				}
				else
				{
					p_objRetException=p_objException;
					return;
				}
				
			}
			catch(Exception p_objExcep)
			{
				throw p_objExcep;
			}
			
		}
	}
}
