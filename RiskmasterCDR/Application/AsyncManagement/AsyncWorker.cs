using System;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Reflection ;
using Riskmaster.BusinessAdaptor.Common ;

namespace Riskmaster.Application.AsyncManagement
{
	/// <summary>
	/// Summary description for AsyncWorker.
	/// </summary>
	public class AsyncWorkerThread
	{
		XmlDocument m_objDocToExport;
		DbWriter m_objWriter;
		AsyncDbParams m_objDBParams ;
		AsyncJobParams m_objAssemblyInfo ;
		XmlDocument m_objDBAndAssemblyInfoParams ;
		int m_iProcessedUsersCount = 0;
		int m_iCountToBeExported = 0;
        int m_iClientId = 0;
		public XmlDocument InputDoc
		{
			get
			{
				return m_objDocToExport;
			}
			set
			{
				m_objDocToExport =  value;
			}
		}
		public int ProcessedUsersCount
		{
			get
			{
				return m_iProcessedUsersCount;
			}
			set
			{
				m_iProcessedUsersCount =  value;
			}
		}
		public int CountToBeExported
		{
			get
			{
				return m_iCountToBeExported;
			}
			set
			{
				m_iCountToBeExported =  value;
			}
		}
		public AsyncWorkerThread(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
        public AsyncWorkerThread(XmlDocument p_objDoc, AsyncDbParams p_objDbParams, int p_iClientId)
		{
			try
			{
                m_iClientId = p_iClientId;
				m_objDocToExport = p_objDoc;
				m_objDBParams = p_objDbParams ;

				m_objDBAndAssemblyInfoParams= new XmlDocument() ;
				m_objDBAndAssemblyInfoParams.LoadXml(this.GetOuterXml( ref m_objDocToExport,"AsyncParamsSection")) ;
			}
			catch(Exception p_objErr)
			{
                Utility.LoggerForWorkerThreads(p_objErr, Globalization.GetString("AsyncWorkerThread.Error", m_iClientId), m_iClientId);

			}
			finally
			{
			}

		}
		private string GetOuterXml(ref XmlDocument p_objSrcDoc, string p_sNodeName)
		{
			return p_objSrcDoc.SelectSingleNode("//"+p_sNodeName).OuterXml ;
		}
		private string GetInnerXml(ref XmlDocument p_objSrcDoc, string p_sNodeName)
		{
			return p_objSrcDoc.SelectSingleNode("//"+p_sNodeName).InnerXml;
		}
		public void StartWork()
		{
			string sAssemblyName = "", sClassName = "" , sFunctionName = "";

			m_objAssemblyInfo = new AsyncJobParams() ;
			XmlDocument objDocToLongFunctionCall = new XmlDocument() ;
			try
			{
				/*look for input document containing the async info...
				 * */
				if(m_objDocToExport==null)
				{
					/*As this method is invoked by a new thread, try logging the error and exit
					 *!!!Dont throw the error, will be an unhandled exception. 
					 * */
        
				}
				m_objAssemblyInfo.Deserialize ( ref m_objDBAndAssemblyInfoParams) ;

				sAssemblyName = m_objAssemblyInfo.AssemblyName ; 
				sClassName = m_objAssemblyInfo.ClassName ; 
				sFunctionName = m_objAssemblyInfo.FunctionName ; 

				object objTraget ;
				Assembly objAssembly ;
				Type objType ;
				object[] objParam ;
				try
				{
					objAssembly =Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + sAssemblyName);// Assembly.Load(AppDomain.CurrentDomain.BaseDirectory + "bin//" + sAssemblyName);
					objType = objAssembly.GetType(sClassName);
    
					// Create an instance
					objTraget = Activator.CreateInstance ( objType );
                    
					//Get info for method
					MethodInfo objMethodInfo = objType.GetMethod( sFunctionName );
                      
					objDocToLongFunctionCall.LoadXml( this.GetInnerXml( ref m_objDocToExport,"XmlForFunctionCall" ) ) ;
                    
					XmlDocument p_objDocOut = new XmlDocument() ;
					objParam = new object[4];
					objParam[0] = objDocToLongFunctionCall ;
					objParam[1] = p_objDocOut ;
                    objParam[2] = new BusinessAdaptorErrors(m_iClientId) ;
					objParam[3] = this ;

					//invoke method
					objMethodInfo.Invoke( objTraget ,objParam );
                    
                    p_objDocOut = (XmlDocument)objParam[1] ;

					Utility.CopyChildNodesUnderRootNode( ref m_objDocToExport, ref p_objDocOut, "form") ;
                    Utility.RemoveAllChildNodes( ref m_objDocToExport , "XmlForFunctionCall" ) ;

					this.UpdateStatus( 2 ); /*Complete flag.*/

				}
				catch(Exception p_objErr)
				{
					
					throw p_objErr ;
					

				}
				finally
				{
					objTraget = null ;
					objAssembly = null  ;
					objType = null ;
					objParam = null ;

				}
			}
			catch(Exception p_objErr)
			{
				try
				{
					this.UpdateStateDocForWorkerErrors() ;

					this.UpdateStatus( 3 ); /*Error flag.*/
					
				}
				finally
				{
                    Utility.LoggerForWorkerThreads(p_objErr, Globalization.GetString("AsyncWorkerThread.StartWork.Error", m_iClientId), m_iClientId);
				}
			}
			finally
			{
				try
				{
					Thread.CurrentThread.Abort();
				}
				catch
				{
				}
			}
		}
		public void UpdatePercentageCompletion()
		{   
			//AsyncDbParams objDbParams = new AsyncDbParams() ;
			try
			{
                m_objWriter = DbFactory.GetDbWriter(Utility.GetSessionDSN(m_iClientId));
                m_objWriter.Connection = DbFactory.GetDbConnection(Utility.GetSessionDSN(m_iClientId));
				m_objWriter.Connection.Open();
                
				m_objDBParams.Deserialize( ref m_objDBAndAssemblyInfoParams ) ;
				string sTempExportId= m_objDBParams.ExportId ;

				m_objWriter.Tables.Add("ASYNC_TRACKER");
				m_objWriter.Where.Add("AsyncId='"+sTempExportId+"'");
				m_objWriter.Fields.Add("AsyncStatus",0);

				double temp = (double)((double)m_iProcessedUsersCount/(double)m_iCountToBeExported);
				double dPercentageCompletion = (double)temp*100;

				m_objWriter.Fields.Add("PercentageCompletion",dPercentageCompletion);
				m_objWriter.Execute();
				m_objWriter.Connection.Close();
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				if(m_objWriter!=null)
				{
					if(m_objWriter.Connection!=null)
					{
						if(m_objWriter.Connection.State==System.Data.ConnectionState.Open)
						{
							m_objWriter.Connection.Close();
							m_objWriter = null;
						}
					}
				}
			}
		}
		public void UpdateStatus( int p_iStatus )
		{
			byte[] arrState = null;
			try
			{
				m_objDBParams.Deserialize( ref m_objDBAndAssemblyInfoParams ) ;

				m_objDBParams.Status = p_iStatus ;
				m_objDBParams.Serialize( ref m_objDBAndAssemblyInfoParams ) ;

                m_objDocToExport.SelectSingleNode("//AsyncParamsSection").InnerXml = m_objDBAndAssemblyInfoParams.InnerXml ;

                m_objWriter = DbFactory.GetDbWriter(Utility.GetSessionDSN(m_iClientId));
                m_objWriter.Connection = DbFactory.GetDbConnection(Utility.GetSessionDSN(m_iClientId));
				m_objWriter.Connection.Open();

				string sExportId = m_objDBParams.ExportId ; //m_objDocToExport.SelectSingleNode("//ExportId").InnerText;

				m_objWriter.Tables.Add( "ASYNC_TRACKER" );
				m_objWriter.Where.Add( "AsyncId='"+sExportId+"'" );
				m_objWriter.Fields.Add( "AsyncStatus",m_objDBParams.Status );

				arrState = Conversion.GetByteArr( m_objDocToExport.OuterXml );

				m_objWriter.Fields.Add( "AsyncState",arrState );
				m_objWriter.Fields.Add( "PercentageCompletion",100 );
				m_objWriter.Execute();
				m_objWriter.Connection.Close();
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				arrState= null ;

				if(m_objWriter!=null)
				{
					if(m_objWriter.Connection!=null)
					{
						if(m_objWriter.Connection.State==System.Data.ConnectionState.Open)
						{
							m_objWriter.Connection.Close();
							m_objWriter = null;
						}
					}
				}
			}
		}
		public void UpdateStateDocForWorkerErrors()
		{
			try
			{

                m_objDBAndAssemblyInfoParams.SelectSingleNode("//WorkerThreadErrorSection/ErrMesageForUI").InnerText = Globalization.GetString("AsyncWorkerThread.StartWork.Error", m_iClientId);

			}
			catch(Exception p_objErr)
			{
				throw p_objErr ;
			}
			finally
			{
			}
		}
		public void ClearAsyncDependencies(string p_sLdapExportId)
		{
			DbReader objReader = null ;
			string sDelSql = "Delete from ASYNC_TRACKER where AsyncId = '" + p_sLdapExportId +"'" ;
			try
			{
                objReader = DbFactory.GetDbReader(Utility.GetSessionDSN(m_iClientId), sDelSql);
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Close();
					objReader = null;
				}
			}
		}
	}
}
