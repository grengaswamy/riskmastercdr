
using System;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.IO;
using System.Text;
using System.Threading;
using System.Xml;
using Riskmaster.Db;
using System.Data;
using System.Collections;
using System.Reflection ;
namespace Riskmaster.Application.AsyncManagement
{
	/// <summary>
	/// Summary description for AsyncHandler.
	/// TO DO -> *******Implement thread pooling else
	/// OS may throw errors if number of requested new threads exceed the number of "Parallel threads(AsyncWorkerThread)" OS can handle.
	/// </summary>
	public class AsyncHandler
	{

		DbWriter m_objWriter;
        int m_iClientId = 0;
		public AsyncHandler(int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
		public void InitAndPollForResults(ref XmlDocument p_objUsedIdsDoc)
		{
			string sExportId = "";
			AsyncDbParams objDbParams = new AsyncDbParams() ;
			try
			{
				/*Validate the incoming document against the xml schema.*/
				this.ValidateIncomingDoc( ref p_objUsedIdsDoc ) ;

                AsyncWorkerThread objWorker = new AsyncWorkerThread(m_iClientId);

                objDbParams.Deserialize( ref p_objUsedIdsDoc ) ;

				switch(objDbParams.Status)
				{
					case 0:
					{
						try
						{
							sExportId = Guid.NewGuid().ToString();
                            m_objWriter = DbFactory.GetDbWriter(Utility.GetSessionDSN(m_iClientId));

                            m_objWriter.Connection = DbFactory.GetDbConnection(Utility.GetSessionDSN(m_iClientId));
							m_objWriter.Connection.Open();
							m_objWriter.Tables.Add("ASYNC_TRACKER");
							m_objWriter.Fields.Add("AsyncId",sExportId);
							m_objWriter.Fields.Add("AsyncStatus",0);
							m_objWriter.Execute();
							m_objWriter.Connection.Close();
                            
							objDbParams.ExportId = sExportId ;
							objDbParams.Status = 1 ;
                            objDbParams.Serialize(ref p_objUsedIdsDoc);

                            objWorker = new AsyncWorkerThread(p_objUsedIdsDoc, objDbParams, m_iClientId);
							Thread objADWorker =new Thread(new System.Threading.ThreadStart(objWorker.StartWork));
							objADWorker.Start();
						}
						catch(Exception p_objErr)
						{
							throw p_objErr;
						}
						finally
						{
							if(m_objWriter!=null)
							{
								if(m_objWriter.Connection!=null)
								{
									if(m_objWriter.Connection.State==System.Data.ConnectionState.Open)
									{
										m_objWriter.Connection.Close();
										m_objWriter = null;
									}
								}
							}
						}
						break;
					}
					case 1:
					{
						DbReader objRead = null;
						try
						{
							//string sExportTempId = p_objUsedIdsDoc.SelectSingleNode("//ExportId").InnerText;
							string sExportTempId = objDbParams.ExportId ;
                            objRead = DbFactory.GetDbReader(Utility.GetSessionDSN(m_iClientId), "select AsyncStatus,AsyncState,PercentageCompletion from ASYNC_TRACKER where AsyncId='" + sExportTempId + "'");
							if(objRead.Read())
							{
								int iTempStatus = objRead.GetInt32("AsyncStatus");

								switch ( iTempStatus )
								{
									case 0 :
									{
										objDbParams.Status = 1 ;//Being processed...
										objDbParams.PercentageCompletion = objRead.GetDouble("PercentageCompletion");
										objDbParams.Serialize(ref p_objUsedIdsDoc);
										break ;
									}
									case 2 :
									{
										objDbParams.Status = 2;//Complete...
										objDbParams.Serialize(ref p_objUsedIdsDoc);
										p_objUsedIdsDoc.LoadXml(System.Text.ASCIIEncoding.ASCII.GetString(objRead.GetAllBytes("AsyncState")));
										objWorker.ClearAsyncDependencies(objDbParams.ExportId);
										break ;
									}
									default : /*Some error has occured. */
									{
										objDbParams.Status = 3;
										objDbParams.Serialize(ref p_objUsedIdsDoc);
										p_objUsedIdsDoc.LoadXml(System.Text.ASCIIEncoding.ASCII.GetString(objRead.GetAllBytes("AsyncState")));
										objWorker.ClearAsyncDependencies(objDbParams.ExportId);
                                       break ;
									}
								}
	
							}
						}
						catch(Exception p_objErr)
						{
							throw p_objErr;
						}
						finally
						{
							if(objRead!=null)
							{
								objRead.Close();
								objRead = null;

							}
						}
						break;
					}
                    
				}
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
			}
		}
		
		/// <summary>
		/// This function will check for various required nodes.
		/// </summary>
		/// <param name="p_objDoc">Incoming doc.</param>
		private void ValidateIncomingDoc(ref XmlDocument p_objDoc)
		{
			try
			{
                /*
				 * Schema for validation to be created and incoming doc to be checked against.
				 * */
			}
			catch(Exception p_objErr)
			{
			}
			finally
			{
			}
		}
	}
	
	
}
