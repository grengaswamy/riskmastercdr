Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPiXWorkLoss
    Const sClassName As String = "CWCPiXWorkLoss"
    Const m_TableName As String = "PI_X_WORK_LOSS"
    ' Object properties
    Private m_DateLastWorked As String
    Private m_DateReturned As String
    Private m_DisabilityDate As String
    Private m_Duration As Integer
    Private m_NonConsecutivePeriod As Integer
    Private m_PhysicalRestrictions As Integer
    Private m_PiRowId As Integer
    Private m_PiWlRowId As Integer
    Private m_RTWQualifier As Integer
    Private m_RTWSameEmployer As Integer
    Private m_RTWType As Integer
    Private m_StateDuration As Integer
    Private m_WorkIntensity As Integer

    Public Property PiWlRowId() As Integer
        Get
            PiWlRowId = m_PiWlRowId
        End Get
        Set(ByVal Value As Integer)
            m_PiWlRowId = Value
        End Set
    End Property

    Public Property PiRowId() As Integer
        Get
            PiRowId = m_PiRowId
        End Get
        Set(ByVal Value As Integer)
            m_PiRowId = Value
        End Set
    End Property

    Public Property DateLastWorked() As String
        Get
            DateLastWorked = m_DateLastWorked
        End Get
        Set(ByVal Value As String)
            m_DateLastWorked = Value
        End Set
    End Property

    Public Property DateReturned() As String
        Get
            DateReturned = m_DateReturned
        End Get
        Set(ByVal Value As String)
            m_DateReturned = Value
        End Set
    End Property

    Public Property Duration() As Integer
        Get
            Duration = m_Duration
        End Get
        Set(ByVal Value As Integer)
            m_Duration = Value
        End Set
    End Property


    Public Property StateDuration() As Integer
        Get
            StateDuration = m_StateDuration
        End Get
        Set(ByVal Value As Integer)
            m_StateDuration = Value
        End Set
    End Property

    'JM 04/16/2004 Crawford EDI

    Public Property DisabilityDate() As String
        Get
            DisabilityDate = m_DisabilityDate
        End Get
        Set(ByVal Value As String)
            m_DisabilityDate = Value
        End Set
    End Property

    Public Property WorkIntensity() As Integer
        Get
            WorkIntensity = m_WorkIntensity
        End Get
        Set(ByVal Value As Integer)
            m_WorkIntensity = Value
        End Set
    End Property

    Public Property RTWQualifier() As Integer
        Get
            RTWQualifier = m_RTWQualifier
        End Get
        Set(ByVal Value As Integer)
            m_RTWQualifier = Value
        End Set
    End Property

    Public Property RTWSameEmployer() As Integer
        Get
            RTWSameEmployer = m_RTWSameEmployer
        End Get
        Set(ByVal Value As Integer)
            m_RTWSameEmployer = Value
        End Set
    End Property


    Public Property NonConsecPeriod() As Integer
        Get
            NonConsecPeriod = m_NonConsecutivePeriod
        End Get
        Set(ByVal Value As Integer)
            m_NonConsecutivePeriod = Value
        End Set
    End Property


    Public Property RTWType() As Integer
        Get
            RTWType = m_RTWType
        End Get
        Set(ByVal Value As Integer)
            m_RTWType = Value
        End Set
    End Property
    Public Property PhysicalRestriction() As Integer
        Get
            PhysicalRestriction = m_PhysicalRestrictions
        End Get
        Set(ByVal Value As Integer)
            m_PhysicalRestrictions = Value
        End Set
    End Property

    Public Sub ClearObject()
        m_PiWlRowId = 0
        'm_PiRowId = 0
        m_DateLastWorked = ""
        m_DateReturned = ""
        m_Duration = 0
        m_StateDuration = 0
        m_DisabilityDate = ""
        m_WorkIntensity = 0
        m_RTWQualifier = 0
        m_RTWSameEmployer = 0
        m_NonConsecutivePeriod = 0
        m_RTWType = 0
        m_PhysicalRestrictions = 0



    End Sub
    Private Function LoadData(ByRef PiWlRowId As Integer) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim sSQL As String
        Dim objReader As DbReader

        Try
            ' Set SQL to load object data
            sSQL = "SELECT * FROM " & m_TableName & " WHERE PI_WL_ROW_ID=" & m_PiWlRowId
            ' Open Recordset
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_PiWlRowId = objReader.GetInt32("PI_WL_ROW_ID")
                m_PiRowId = objReader.GetInt32("PI_ROW_ID")
                m_DateLastWorked = objReader.GetString("DATE_LAST_WORKED")
                m_DateReturned = objReader.GetString("DATE_RETURNED")
                m_Duration = objReader.GetInt32("DURATION")
                m_StateDuration = objReader.GetInt32("STATE_DURATION")
                'jtodd22 03/06/2008 this should be a string --m_DisabilityDate = objReader.GetInt32("DISABILITY_DATE")
                m_DisabilityDate = objReader.GetString("DISABILITY_DATE")
                m_WorkIntensity = objReader.GetInt32("WORK_INTENSITY")
                m_RTWQualifier = objReader.GetInt32("RTW_QUALIFIER")
                m_RTWSameEmployer = objReader.GetInt32("RTW_SAME_EMPLOYER")
                m_NonConsecutivePeriod = objReader.GetInt32("NONCONSEC_PERIOD")
                m_RTWType = objReader.GetInt32("RTW_TYPE")
                m_PhysicalRestrictions = objReader.GetInt32("PHYSICAL_RESTRICT")
            End If


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
End Class

