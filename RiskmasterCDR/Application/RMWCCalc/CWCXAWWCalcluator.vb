Option Strict Off
Option Explicit On
Public Class CWCXAWWCalcluator
    Const sClassName As String = "CWCXAWWCalculator"
    Private m_IsSpendableJurisdiction As Integer
    Private m_YesCodeID As Integer
    Public objUserScreen As New CWCXAWWScreenState
    Public objBenefits As Object
    Public sMessage As String
    Private m_Warning As String

    Public Function LoadData(ByRef lJurisRowID As Integer, ByRef sDateOfEventDB As String) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim sTmp As String
        Try
            LoadData = 0
            sTmp = ""

            objBenefits = New RMJuRuLib.CJRJurisBenefitDefs()
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefits.LoadData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefits.LoadData(g_objUser, lJurisRowID, 3, -1) = -1 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefits.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefits.Count = 0 Then
                    sTmp = ""
                    sTmp = sTmp & "There are no Indemnity Benefits defined for this jurisdiction." & vbCrLf & vbCrLf
                    sTmp = sTmp & "Filing Jurisdiction Is " & modFunctions.GetStateName_SQL(lJurisRowID) & "." & vbCrLf
                    sTmp = sTmp & "Date Of Event Is " & modFunctions.sDBDateFormat(sDateOfEventDB, "Short Date") & "."
                End If
            End If

            sMessage = sTmp

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If Trim(g_sErrDescription) = "" Then
                    g_sErrDescription = Err.Description
                Else
                    g_sErrDescription = g_sErrDescription & ";" & Err.Description
                End If
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function ValidateData() As Integer
        Const sFunctionName As String = "ValidateData"

        Dim sTmp As String
        Dim sTmp2 As String
        Dim sTmp3 As String

        Try

            ValidateData = 0

            sTmp = ""
            sTmp = sTmp & "The Claimant can not have Hourly Earnings and Piece Work Earnings" & vbCrLf
            sTmp = sTmp & "from the same primary employer." & vbCrLf & vbCrLf
            sTmp = sTmp & "If the Claimant is an Hourly Worker for this claim, then " & vbCrLf
            sTmp = sTmp & "zero out the data in the Piece Work area and list the " & vbCrLf
            sTmp = sTmp & "Piece Work Earnings as 'Other Income-Concurrent Employer'." & vbCrLf & vbCrLf
            sTmp = sTmp & "If the Claimant is an Piece Worker for this claim," & vbCrLf
            sTmp = sTmp & "then zero out the Hourly data and list the " & vbCrLf
            sTmp = sTmp & "Hourly Work Earnings as 'Other Income-Concurrent Employer'."

            sTmp2 = ""
            sTmp2 = sTmp2 & "The Claimant can not have Salary Earnings and Piece Work Earnings" & vbCrLf
            sTmp2 = sTmp2 & "from the same primary employer." & vbCrLf & vbCrLf
            sTmp2 = sTmp2 & "If the Claimant is a Salary Worker for this claim, then " & vbCrLf
            sTmp2 = sTmp2 & "zero out the data in the Piece Work area and list the " & vbCrLf
            sTmp2 = sTmp2 & "Piece Work Earnings as 'Other Income-Concurrent Employer'." & vbCrLf & vbCrLf
            sTmp2 = sTmp2 & "If the Claimant is an Piece Worker for this claim," & vbCrLf
            sTmp2 = sTmp2 & "then zero out the Salary data and list the " & vbCrLf
            sTmp2 = sTmp2 & "Piece Work Earnings as 'Other Income-Concurrent Employer'."

            sTmp3 = ""
            sTmp3 = sTmp3 & "The Claimant can not have Salary Earnings and Hourly Earnings" & vbCrLf
            sTmp3 = sTmp3 & "from the same primary employer." & vbCrLf & vbCrLf
            sTmp3 = sTmp3 & "If the Claimant is a Salary Worker for this claim, then " & vbCrLf
            sTmp3 = sTmp3 & "zero out the data in the Hourly area and list the " & vbCrLf
            sTmp3 = sTmp3 & "Hourly Earnings as 'Other Income-Concurrent Employer'." & vbCrLf & vbCrLf
            sTmp3 = sTmp3 & "If the Claimant is an Hourly Worker for this claim," & vbCrLf
            sTmp3 = sTmp3 & "then zero out the Salary data and list the " & vbCrLf
            sTmp3 = sTmp3 & "Salary Earnings as 'Other Income-Concurrent Employer'."

            '-----------------------------------------------------------------------------------
            With objUserScreen
                If .HourEarnings > 0 And .PieceWorkEarningsTotal > 0 Then
                    m_Warning = m_Warning & sTmp & vbCrLf
                End If

                If .HourEarnings > 0 And .SalaryEarnings > 0 Then
                    m_Warning = m_Warning & sTmp3 & vbCrLf
                End If

                If .PieceWorkEarningsTotal > 0 And .SalaryEarnings > 0 Then
                    m_Warning = m_Warning & sTmp2 & vbCrLf
                End If

                If .PieceWorkEarningsTotal > 0 And .PieceWorkPayPeriodType = -1 Then
                    m_Warning = m_Warning & "Piece Work Earnings must be qualified by a Pay Period Type." & vbCrLf
                End If

                If .SalaryEarnings > 0 And .SalaryPayPeriodType = -1 Then
                    m_Warning = m_Warning & "Earnings must be qualified by a Pay Period Type." & vbCrLf
                End If
                '---------------------------------------------------------------------------------------------
                If m_IsSpendableJurisdiction = m_YesCodeID Then
                    sTmp = ""
                    sTmp = sTmp & "You must select a number of Dependents " & vbCrLf
                    sTmp = sTmp & "for the Calculator to work."
                    If IsNumeric(.Dependents) Then
                        If .Dependents = 0 Then
                            m_Warning = m_Warning & sTmp & vbCrLf
                        End If
                    Else
                        m_Warning = m_Warning & sTmp & vbCrLf
                    End If

                    sTmp = ""
                    sTmp = sTmp & "You must select a Tax Filing Status " & vbCrLf
                    sTmp = sTmp & "for the Calculator to work."
                    If .TaxStatus < 0 Then
                        m_Warning = m_Warning & sTmp & vbCrLf
                    End If
                End If
                '-----------------------------------------------------------------------------------------------
            End With
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If Trim(g_sErrDescription) = "" Then
                    g_sErrDescription = Err.Description
                Else
                    g_sErrDescription = g_sErrDescription & ";" & Err.Description
                End If
            End With
            ValidateData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
        End Try

    End Function

    Public Property IsSpendableJurisdiction() As Integer
        Get
            Dim lErrorNumber As Integer
            IsSpendableJurisdiction = modFunctions.IsSpendableJurisdiction(lErrorNumber)
        End Get
        Set(ByVal Value As Integer)
            m_IsSpendableJurisdiction = Value
        End Set
    End Property

    Public Property YesCodeID() As Integer
        Get
            Dim lErrorNumber As Integer
            YesCodeID = modFunctions.GetYesCodeID(lErrorNumber)
        End Get
        Set(ByVal Value As Integer)
            m_YesCodeID = Value
        End Set
    End Property

    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property
End Class

