Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCXUserTransactionTypes
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CWCXUserTransactionTypes"
    'local variable to hold collection
    Private m_Warning As String
    Private mCol As Collection

    Private Function GetSQLFieldList() As String
        Dim sSQL As String

        GetSQLFieldList = ""

        sSQL = ""

        sSQL = sSQL & "SELECT DISTINCT"
        sSQL = sSQL & " WCP_TRANS_TYPES.CODE_ID"
        sSQL = sSQL & ",CODES.SHORT_CODE"
        sSQL = sSQL & ",CODES_TEXT.CODE_DESC"

        GetSQLFieldList = sSQL
    End Function

    Public Function LoadData(ByRef lBenefitLookUpID As Integer, ByRef lBenefitLookUpIDSecond As Integer, iClientId As Integer) As Integer
        Const sFunctionName As String = "LoadData"
        Dim i As Short
        Dim objReader As DbReader
        Dim lBenefitType As Integer
        Dim objRecord As CWCXUserTransactionType
        Dim sSQL As String
        Try

            LoadData = 0

            m_Warning = ""

            If Count > 0 Then
                For i = Count To 1 Step -1
                    Remove(i)
                Next
            End If

            lBenefitType = modFunctions.GetIndemnityBenefitTypeNumber

            sSQL = GetSQLFieldList()
            sSQL = sSQL & " FROM WCP_TRANS_TYPES, CODES, CODES_TEXT"
            sSQL = sSQL & " WHERE WCP_TRANS_TYPES.CODE_ID = CODES.CODE_ID"
            sSQL = sSQL & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID"
            sSQL = sSQL & " AND (WCP_TRANS_TYPES.BENEFIT_LKUP_ID = " & lBenefitLookUpID
            sSQL = sSQL & " OR WCP_TRANS_TYPES.BENEFIT_LKUP_ID = " & lBenefitLookUpIDSecond & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                objRecord = New CWCXUserTransactionType
                With objRecord
                    .CodeID = objReader.GetInt32("CODE_ID")
                    .Desc = objReader.GetString("CODE_DESC")
                    .ShortCode = objReader.GetString("SHORT_CODE")
                End With

                mCol.Add(objRecord)
            End While
            'jlt 07/06/2004 be sure to drop the Recordset


            If Count = 0 Then
                m_Warning = ""
                m_Warning = m_Warning & "There are no Transaction Mappings for this Benefit." & vbCrLf
                m_Warning = m_Warning & "Please create the mapping in Utilities." & vbCrLf & vbCrLf
                m_Warning = m_Warning & "Jurisdiction is:  " & modFunctions.GetStateName_SQL((g_objXClaim.FilingStateID)) & vbCrLf
                m_Warning = m_Warning & "Benefit is:  " & modFunctions.GetBenefitDescription(lBenefitLookUpID, iClientId)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function Add() As CWCXUserTransactionType
        Try
            'create a new object
            Dim objNewMember As CWCXUserTransactionType
            objNewMember = New CWCXUserTransactionType
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CWCXUserTransactionType
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        'GetEnumerator = mCol.GetEnumerator


    End Function


    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
End Class

