Option Strict Off
Option Explicit On
Imports Riskmaster.Db
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
Public Class CWCTempSpendable
    Implements _ICalculator
    '---------------------------------------------------------------------------------------
    ' Module    : CWCTempspendable
    ' DateTime  :
    ' Author    : jtodd22
    ' Purpose   : Beware that there are two different types of jurisdictions for Workers
    ' ..........: Compensation; jurisdictions that work from a discounted AWW and jurisdictions
    ' ..........: that work from a Spendable Income lookup table
    '---------------------------------------------------------------------------------------
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "TTD"
    Const sClassName As String = "CWCTempSpendable"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleTTDAA"
    '******************************************************************************************************************
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objSpendableIncome As Object
    Dim objSpendableIncomeRule As Object

    Public CalculatedPayment As Double
    Public CalculatedPayment_01 As Double
    Public CalculatedPayment_02 As Double
    Public ClaimantJurisAWW As Double
    Public ClaimantOriginalAWW As Double

    Public BenefitDays As Integer
    Public BenefitDays_01 As Integer
    Public BenefitDays_02 As Integer
    Public BenefitEndDate As String
    Public BenefitStartDate As String
    Public BenefitRate_Effective As Double
    Public BenefitRate_Original As Double
    Public dBasicRate_CA_TwoYear As Double

    Private m_DaysToPaymentDue As Short


    Public EventDateCalendar As Date
    Public EventDateDTG As String
    Public JurisdictionMaxAww As Double
    Private m_JurisMaxBenRateWeek As Double
    Private m_JurisMaxBenRateDay As Double
    Private m_JurisMinBenRateWeek As Double
    Private m_JurisMinBenRateDay As Double


    Public PayLateCharge As Short
    Public RealMinBenRateWeek As Double
    Public RealMinBenRateDay As Double

    Public TwoYearStartDateCalendar As String
    Public TwoYearStartDateDTG As String

    Private m_WaitingPeriodIsPaid As Short

    Public Property WaitingPeriodIsPaid() As Short
        Get
            WaitingPeriodIsPaid = m_WaitingPeriodIsPaid
        End Get
        Set(ByVal Value As Short)
            m_WaitingPeriodIsPaid = Value
        End Set
    End Property

    Public Property DaysToPaymentDue() As Short
        Get
            DaysToPaymentDue = m_DaysToPaymentDue
        End Get
        Set(ByVal Value As Short)
            m_DaysToPaymentDue = Value
        End Set
    End Property

    Public Property JurisMaxBenRateDay() As Double
        Get
            JurisMaxBenRateDay = m_JurisMaxBenRateDay
        End Get
        Set(ByVal Value As Double)
            m_JurisMaxBenRateDay = Value
        End Set
    End Property

    Public Property JurisMaxBenRateWeek() As Double
        Get
            JurisMaxBenRateWeek = m_JurisMaxBenRateWeek
        End Get
        Set(ByVal Value As Double)
            m_JurisMaxBenRateWeek = Value
        End Set
    End Property

    Public Property JurisMinBenRateDay() As Double
        Get
            JurisMinBenRateDay = m_JurisMinBenRateDay
        End Get
        Set(ByVal Value As Double)
            m_JurisMinBenRateDay = Value
        End Set
    End Property

    Public Property JurisMinBenRateWeek() As Double
        Get
            JurisMinBenRateWeek = m_JurisMinBenRateWeek
        End Get
        Set(ByVal Value As Double)
            m_JurisMinBenRateWeek = Value
        End Set
    End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 1/5/2005 13:48
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : jlt, 03/23/2001 California uses the employee AWW (m_AWW_WeeklyRate) directly in
    ' ..........: calculation where the AWW is <= a state set value (m_dttdJurisdictionMaxAww) otherwise the
    ' ..........: state set max value (m_dttdJurisdictionMaxAww) is used
    ' ..........: jlt 07/29/2003  cap aww if over the jurisdiction max aww
    '--------------------------------------------------------------------------------------
    '
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Try
            Dim objReader As DbReader
            Dim dblNewClaimantOriginalAWW As Double
            Dim sStartDate As String
            Dim dbl As Double
            Dim dDateOfEvent As Date
            Dim lReturn As Integer
            Dim sDateOfEvent_DTG As String
            Dim sDummyDate_DTG As String
            Dim sSQL As String
            Dim sTemp As String

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then
                Exit Function
            End If

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then
                Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.RuleTotalWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_RuleTotalWeeks = objBenefitRule.RuleTotalWeeks
            BenefitDays = g_objXPaymentParms.BenefitDays
            BenefitEndDate = g_objXPaymentParms.BenefitEndDate
            BenefitStartDate = g_objXPaymentParms.BenefitStartDate

            Me.ClaimantOriginalAWW = g_objXClaim.ClaimantOriginalAWW
            Me.ClaimantJurisAWW = g_objXClaim.ClaimantOriginalAWW
            'jlt 07/29/2003  cap aww if over the jurisdiction max aww
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If g_objXClaim.ClaimantOriginalAWW > objBenefitRule.MaxAWW And objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Me.ClaimantJurisAWW > objBenefitRule.MaxAWW Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.ClaimantJurisAWW = objBenefitRule.MaxAWW
                End If
            End If
            ' -------------------
            'jtodd22 09/27/2006 -getting the spendable rate here looked like a good idea
            'jtodd22 09/27/2006 -but was realy a bad idea as it is not a data load activty
            'jtodd22 09/27/2006 -it should only be in the dGetBasicRate, or lCalculate functions
            'lReturn = modFunctions.GetRateSpendable(Me.dBasicRate, m_ErrorMaskSpendData, g_objXClaim.AWWToCompensate)
            'If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then
            '    Exit Function
            'End If
            ' -----------------------
            'jtodd22 beware the two year rule applies to payments, not to rates in general
            'jtodd22 reload the rates only if the whole payment is affected
            'jtodd22 if a payment is split the CalculatePayment function must handle it
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayCurrentRateAfterTwoYears. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PayCurrentRateAfterTwoYears = g_lYesCodeID Then
                GetTwoYearStartDateCalendar()
                sTemp = GetTwoYearStartDateCalendar()
                Me.TwoYearStartDateCalendar = sTemp
                Me.TwoYearStartDateDTG = Mid(sTemp, 7, 4) & Mid(sTemp, 1, 2) & Mid(sTemp, 4, 2)

                'UPGRADE_WARNING: Couldn't resolve default property of object PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sDummyDate_DTG = PadDateWithZeros(Me.BenefitStartDate)
                sDummyDate_DTG = Mid(sDummyDate_DTG, 7, 4) & Mid(sDummyDate_DTG, 1, 2) & Mid(sDummyDate_DTG, 4, 2)
                If sDummyDate_DTG >= Me.TwoYearStartDateDTG Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    objBenefitRule.LoadDataByEventDate(g_objUser, g_objXClaim.FilingStateID, Me.TwoYearStartDateDTG)
                    'modFunctions.GetRateSpendable Me.dBasicRate, m_ErrorMaskSpendData, g_objXClaim.AWWToCompensate
                    'If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then Exit Function
                End If
            End If

            sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitTypeAbbr, "")
            m_PaidPendingWeeks = GetBenefitWeeksPaidOrPending(sTemp)


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:32
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim dRate As Double
        Dim lReturn As Integer
        Dim sTemp As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            dRate = 0

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EarningsRequiredCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EarningsRequiredCode = g_lYesCodeID Then
                If g_objXPaymentParms.EarningsPerWeek = 0 And g_objXPaymentParms.EarningsPerMonth = 0 Then
                    m_ErrorMask = g_objXErrorMask.cNoEarnings
                    Exit Function
                End If
                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                If (DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1) Mod 7 <> 0 Then
                    m_Note = "Because you have earnings for this benefit period, the benefit payment Time frame (Beginning-Through) must be in full weeks."
                    Exit Function
                End If
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EarningsPermittedCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EarningsPermittedCode = g_lNoCodeID Then
                If g_objXPaymentParms.EarningsPerWeek > 0 Then
                    m_Note = "Earnings are not permitted with this benefit type."
                    Exit Function
                End If
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = "Claimant has been paid or has pending payment " & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks of TTP or TPD." & vbCrLf & "There is no Temporary liability left to pay."
                    Exit Function
                End If
            End If

            modFunctions.BuildTopOfWorkSheet(colWorkSheet)

            colWorkSheet.Add("|Claimant's Tax Status | " & modFunctions.GetCodeDesc_SQL((g_objXClaim.JurisTaxStatusCode)))
            colWorkSheet.Add("|Claimant's Exemptions | " & g_objXClaim.JurisTaxExemptions)
            colWorkSheet.Add("||")

            g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If g_objXClaim.AWWToCompensate > objBenefitRule.MaxAWW Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|State Imposed Max AWW Limit                       |" & Format(objBenefitRule.MaxAWW, "Currency"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    g_objXClaim.AWWToCompensate = objBenefitRule.MaxAWW
                    colWorkSheet.Add("|AWW to compensate      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
                End If
            End If

            If g_objXPaymentParms.EarningsPerWeek > 0 Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|The Earnings per week are | " & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))
                g_objXClaim.AWWToCompensate = g_objXClaim.AWWToCompensate - g_objXPaymentParms.EarningsPerWeek
                colWorkSheet.Add("|AWW to compensate      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayCurrentRateAfterTwoYears. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PayCurrentRateAfterTwoYears = g_lYesCodeID Then
                colWorkSheet.Add("|Two Year Anniversary Date                     |" & Me.TwoYearStartDateCalendar)
                Select Case g_objXClaim.FilingStatePostalCode
                    Case "CA"
                        'use CA Labor Code 4661.5
                        Select Case CaliforniaTwoYearPaymentRule()
                            Case 0
                            Case g_objXErrorMask.cPreCATwoYear
                                colWorkSheet.Add("     |Two year rule does not apply to any part of payment period.  " & "The whole payment period is before the two year anniversary, use old rates.|")
                                colWorkSheet.Add("     ||")
                                m_ErrorMask = g_objXErrorMask.cNoRateChange

                            Case g_objXErrorMask.cRateChanged
                                colWorkSheet.Add("     |Two year rule does apply to whole payment period.  " & " The whole payment period is past the two year anniversary and has a rate change")
                                m_Note = "Dates of Payment are over two years from Date of Event.  There is a change in Payment Rates, the change was used in payment calculations"
                                ReLoadData_TwoYearRule()
                                m_Note = "Date of Payment is over two years from Date of Event.  Rates are advanced to Date of Payment for calculations as per California law."
                            Case g_objXErrorMask.cSplitPayment
                                sTemp = "The payment period contains the two year anniversary and a rate change."
                                colWorkSheet.Add("     |" & sTemp & "|")
                                m_ErrorMask = g_objXErrorMask.cSplitPayment
                                m_Note = sTemp & vbCrLf & "Do you want RiskMaster do the calculations?"
                                Exit Function
                            Case g_objXErrorMask.cNoRateChange
                                sTemp = "Dates of Payment are over two years from Date of Event.  There was no change in Payment Rates."
                                colWorkSheet.Add("     |" & sTemp & "|")
                                m_ErrorMask = g_objXErrorMask.cNoRateChange
                                m_Note = sTemp
                            Case g_objXErrorMask.cNoRateChange + g_objXErrorMask.cSplitPayment
                                sTemp = "The payment period contains the two year anniversary but does not have a rate change."
                                colWorkSheet.Add("     |" & sTemp & "|")
                                m_ErrorMask = g_objXErrorMask.cNoRateChange
                                m_Note = sTemp
                        End Select 'Select Case CaliforniaTwoYearPaymentRule()
                    Case Else
                End Select
            End If
            Select Case m_ErrorMask
                Case g_objXErrorMask.cSplitPayment
                    'calculate payment split across the two year date
                    Err.Raise(80000, sClassName & "." & sFunctionName, "Coding is not complete for payment split by Two Year Rule")
                    Exit Function
                Case Else
                    'calculate payment that is not across the two year date or two year rule does not apply
                    lReturn = modFunctions.GetRateSpendable(dRate, m_ErrorMaskSpendData, modFunctions.RoundStandard((g_objXClaim.AWWToCompensate), 0))
                    If dRate = 0 Then
                        m_Warning = ""
                        m_Warning = m_Warning & "The compensation rate was not found or determined." & vbCrLf & vbCrLf
                        m_Warning = m_Warning & "Jurisdiction is:  " & g_objXClaim.FilingStatePostalCode & vbCrLf
                        m_Warning = m_Warning & "Event date:  " & g_objXClaim.DateOfEventCalar
                        Exit Function
                    End If
                    If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then Exit Function
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dRate > objBenefitRule.MaxCompRate And objBenefitRule.MaxCompRate > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dRate = objBenefitRule.MaxCompRate
                        colWorkSheet.Add("     |Jurisdiction Maximum Rate is|" & Format(dRate, "Currency"))
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Select Case objBenefitRule.FloorAmount
                        Case 0
                            'There is not valid floor, note; this could go to zero
                            Me.BenefitRate_Effective = dRate
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Me.CalculatedPayment = ((dRate / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                        Case Is >= Me.ClaimantJurisAWW
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                                dRate = modFunctions.RoundStandard((Me.ClaimantJurisAWW), 2)
                                Me.BenefitRate_Effective = dRate
                                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Me.CalculatedPayment = ((dRate / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                            End If
                        Case Is < Me.ClaimantJurisAWW
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Me.CalculatedPayment = ((dRate / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If objBenefitRule.FloorAmount > dRate Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dRate = objBenefitRule.FloorAmount
                                Me.BenefitRate_Effective = dRate
                                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Me.CalculatedPayment = ((dRate / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                            End If
                    End Select
            End Select
            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function


            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Public Function CaliforniaTwoYearPaymentRule() As Short
        Const sFunctionName As String = "CaliforniaTwoYearPaymentRule"
        Try
            'jlt, 03/23/2001 California has a rule where payments made two (2) years after an event
            'date are calculated at current rates
            Dim sTemp As String

            m_Note = ""
            Select Case CDate(Me.TwoYearStartDateCalendar)
                Case Is > CDate(Me.BenefitEndDate)
                    'two year rule does not apply to whole payment period
                    'whole payment period is before the two year anniversary
                    CaliforniaTwoYearPaymentRule = g_objXErrorMask.cPreCATwoYear
                Case Is <= CDate(Me.BenefitStartDate)
                    'two year rule does apply to whole payment period
                    'whole payment period is past the two year anniversary
                    If Me.BenefitRate_Effective = Me.dBasicRate_CA_TwoYear Then
                        CaliforniaTwoYearPaymentRule = g_objXErrorMask.cNoRateChange
                        m_Note = "Dates of Payment are over two years from Date of Event.  There was no change in Payment Rates."
                    Else
                        CaliforniaTwoYearPaymentRule = g_objXErrorMask.cRateChanged
                        m_Note = "Dates of Payment are over two years from Date of Event.  There is a change in Payment Rates, the change was used in payment calculations"
                    End If
                Case Else
                    'two year rule does apply to part of payment period
                    If Me.dBasicRate_CA_TwoYear = Me.BenefitRate_Effective Then
                        m_Note = "Dates of Payment include two year anniversary.  There was no change in Payment Rates."
                        CaliforniaTwoYearPaymentRule = g_objXErrorMask.cNoRateChange + g_objXErrorMask.cSplitPayment
                    Else
                        m_Note = "Dates of Payment include two year anniversary." & vbCrLf & "There is a change in Payment Rates."
                        CaliforniaTwoYearPaymentRule = g_objXErrorMask.cSplitPayment
                    End If

            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function ReLoadData_TwoYearRule() As Integer
        Const sFunctionName As String = "ReLoadData_TwoYearRule"
        Try
            Dim objReader As DbReader
            Dim sSQL As String
            Dim g_dblMaxBenefit As Double
            Dim dblMaxAWW As Double
            Dim dblMinBenefit As Double
            Dim sStartDate As String
            Dim dblCalculatedPayment As Double
            Dim dbl As Double
            Dim dDateOfEvent As Date

            sSQL = "SELECT MAX_BENEFIT, MAX_AWW, MIN_BENEFIT FROM WCP_RULE_TTD" & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND '" & Me.TwoYearStartDateDTG & "' >= BEGIN_DATE" & " ORDER BY BEGIN_DATE DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                g_dblMaxBenefit = objReader.GetInt32("MAX_BENEFIT")
                dblMaxAWW = objReader.GetInt32("MAX_AWW")
                dblMinBenefit = objReader.GetInt32("MIN_BENEFIT")
            Else
                Err.Raise(vbObjectError + 7000, "ReLoadData_TwoYearRule", "Unable to retrieve TTD Rates")
            End If


            Me.ClaimantJurisAWW = f_dGetClaimantWeeklyWage((g_objXClaim.ClaimID), 0)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objBenefitRule.MaxAWW = dblMaxAWW

            Select Case UCase(g_objXClaim.FilingStatePostalCode)
                Case "CA"
                    'jlt, 03/23/2001 California uses the employee AWW (m_AWW_WeeklyRate) directly in
                    'calculation where the AWW is <= a state set value (m_dttdJurisdictionMaxAww) otherwise the
                    'state set max value (m_dttdJurisdictionMaxAww) is used
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If Me.ClaimantJurisAWW > objBenefitRule.MaxAWW Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Me.ClaimantJurisAWW = objBenefitRule.MaxAWW
                    Else
                    End If
                    Me.BenefitRate_Effective = modFunctions.RoundStandard(Me.ClaimantJurisAWW * 2 / 3, 2)
                    'jlt 10/09/2002 California says if the Claimant's AWW is below the min. rate use the AWW
                    ' as the rate, in the calculation of payment insure that a Claimant's payment does not go below the
                    ' min rate if it calculates below the min.
                    Select Case Me.ClaimantJurisAWW
                        Case Is <= dblMinBenefit
                            Me.BenefitRate_Effective = modFunctions.RoundStandard((Me.ClaimantJurisAWW), 2)
                        Case Is <= (dblMinBenefit * 1.5)
                            Me.BenefitRate_Effective = modFunctions.RoundStandard(Me.ClaimantJurisAWW * 2 / 3, 2)
                            If Me.BenefitRate_Effective < dblMinBenefit Then
                                Me.BenefitRate_Effective = dblMinBenefit
                            End If
                        Case Else
                    End Select
                Case Else
            End Select
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            ReLoadData_TwoYearRule = g_lErrNum

        End Try

    End Function
    Private Function ClearObject() As Integer
        EventDateCalendar = CDate("12/31/1899")
        EventDateDTG = vbNullString
        JurisdictionMaxAww = -0.01
        ClaimantJurisAWW = -0.01
        BenefitRate_Effective = -0.01
        dBasicRate_CA_TwoYear = -0.01
        JurisMaxBenRateWeek = -0.01
        JurisMaxBenRateDay = -0.01
        RealMinBenRateWeek = -0.01
        RealMinBenRateDay = -0.01
        JurisMinBenRateWeek = -0.01
        JurisMinBenRateDay = -0.01
        CalculatedPayment_01 = -0.01
        CalculatedPayment_02 = 0
        'jtodd22 do not reset BenefitDays it is set in calling executable
        m_CheatSheetTitle = "Temporary Total"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function

    Public Function lCalculatePayment_CaTwoYearSplitPayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "CalculatePayment_CaTwoYearSplitPayment"
        Try
            Dim sBenefitEndDate As String
            Dim sBenefitStartDate As String
            Dim sTemp As String
            Me.BenefitDays_01 = 0
            Me.BenefitDays_02 = 0
            Me.CalculatedPayment = 0
            Me.CalculatedPayment_01 = 0
            Me.CalculatedPayment_02 = 0


            'build top of work sheet
            colWorkSheet.Add("     |Claim Number                                      |" & g_objXClaim.ClaimNumber, CStr(1))
            colWorkSheet.Add("     |Claimant Name                                     |" & g_objXClaim.ClaimantNameLFM, CStr(2))
            colWorkSheet.Add("     |                                                  |", CStr(3))

            colWorkSheet.Add("     |Date of Injury                                    |" & g_objXClaim.DateOfEventCalar, CStr(4))
            colWorkSheet.Add("     |Claimant AWW                                      |" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            colWorkSheet.Add("     |State Imposed Limit                               |" & Format(objBenefitRule.MaxAWW, "Currency"))
            colWorkSheet.Add("     |Claimant AWW As Capped (State Imposed Limit)      |" & Format(Me.ClaimantJurisAWW, "Currency"))

            colWorkSheet.Add("     |Benefit Payment Start Date                        |" & CDate(Me.BenefitStartDate), CStr(5))
            colWorkSheet.Add("     |Benefit Payment End Date                          |" & CDate(Me.BenefitEndDate), CStr(6))
            colWorkSheet.Add("||")
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayCurrentRateAfterTwoYears. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PayCurrentRateAfterTwoYears = g_lYesCodeID Then
                colWorkSheet.Add("     |Two Year Anniversary Date                         | " & Me.TwoYearStartDateCalendar)
            End If
            '**********************************************************************************************************
            sBenefitEndDate = Me.BenefitEndDate
            sBenefitStartDate = Me.BenefitStartDate
            Me.BenefitEndDate = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, CDate(Me.TwoYearStartDateCalendar)))
            lCalculatePayment_CA_Split()
            CalculatedPayment_01 = Me.CalculatedPayment
            Me.BenefitDays_01 = Me.BenefitDays
            colWorkSheet.Add("||")
            colWorkSheet.Add("|First Part Payment Start Date|" & Me.BenefitStartDate)
            colWorkSheet.Add("|First Part Payment End Date|" & Me.BenefitEndDate)
            colWorkSheet.Add("|First Part Benefit Days|" & Me.BenefitDays)
            colWorkSheet.Add("|First Part Weekly Rate|" & Format(Me.BenefitRate_Effective, "Currency"))
            colWorkSheet.Add("|First Part Benefit Payment|" & Format(Me.CalculatedPayment, "Currency"))
            '*************************************************************************************
            BenefitStartDate = Me.TwoYearStartDateCalendar
            Me.BenefitEndDate = sBenefitEndDate
            lCalculatePayment_CA_Split()
            CalculatedPayment_02 = Me.CalculatedPayment
            Me.BenefitDays_02 = Me.BenefitDays
            colWorkSheet.Add("||")
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            colWorkSheet.Add("|Second Part State Imposed Limit|" & Format(objBenefitRule.MaxAWW, "Currency"))
            colWorkSheet.Add("|Second Part Claimant AWW As Capped (State Imposed Limit)|" & Format(Me.ClaimantJurisAWW, "Currency"))
            colWorkSheet.Add("|Second Part Payment Start Date|" & Me.BenefitStartDate)
            colWorkSheet.Add("|Second Part Payment End Date|" & Me.BenefitEndDate)
            colWorkSheet.Add("|Second Part Benefit Days|" & Me.BenefitDays)
            colWorkSheet.Add("|Second Part Weekly Rate|" & Format(Me.BenefitRate_Effective, "Currency"))
            colWorkSheet.Add("|Second Part Benefit Payment|" & Format(Me.CalculatedPayment, "Currency"))
            '*****************************************************************************
            CalculatedPayment = Me.CalculatedPayment_01 + Me.CalculatedPayment_02
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Benefit Payment Total|" & Format(Me.CalculatedPayment, "Currency"))
            Me.BenefitEndDate = sBenefitEndDate
            Me.BenefitStartDate = sBenefitStartDate
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            Me.BenefitDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(Me.BenefitStartDate), CDate(Me.BenefitEndDate)) + 1

        Catch ex As Exception

        Finally
        End Try

    End Function
    Public Function lCalculatePayment_CA_Split() As Integer
        Const sFunctionName As String = "lCalculatePayment_CA_Split"

        Dim objReader As DbReader
        Dim sSQL As String
        Dim g_dblMaxBenefit As Double
        Dim dblMaxAWW As Double
        Dim dblMinBenefit As Double
        Dim dblCalculatedPayment As Double
        Dim dbl As Double
        Dim dDateOfEvent As Date
        Try
            lCalculatePayment_CA_Split = 0
            'check for fatal errors
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(Me.BenefitStartDate), CDate(g_objXClaim.DateOfEventCalar)) > 0 Then
                Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit start date(" & Me.BenefitStartDate & ") cannot precede event date(" & g_objXClaim.DateOfEventCalar & ")")
            End If
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(Me.BenefitEndDate), CDate(Me.BenefitStartDate)) > 0 Then
                Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit end date(" & Me.BenefitEndDate & ") cannot precede the benefit start date date(" & Me.BenefitStartDate & ")")
            End If

            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            Me.BenefitDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(Me.BenefitStartDate), CDate(Me.BenefitEndDate)) + 1
            Select Case g_objXClaim.FilingStatePostalCode
                Case "CA"
                    'use CA Labor Code 4661.5
                    Select Case CaliforniaTwoYearPaymentRule()
                        Case 0
                        Case 1
                            'two year rule does not apply to whole payment period
                            'whole payment period is before the two year anniversary, use old rates
                            m_ErrorMask = g_objXErrorMask.cNoRateChange
                            Me.CalculatedPayment = dblCalculatedPayment
                        Case g_objXErrorMask.cRateChanged
                            'two year rule does apply to whole payment period
                            'whole payment period is past the two year anniversary
                            m_Note = "Dates of Payment are over two years from Date of Event.  There is a change in Payment Rates, the change was used in payment calculations"
                            ReLoadData_TwoYearRule()
                            m_Note = "Date of Payment is over two years from Date of Event.  Rates are advanced to Date of Payment for calculations as per California law."
                        Case g_objXErrorMask.cSplitPayment
                            m_ErrorMask = g_objXErrorMask.cSplitPayment
                            m_Note = "The payment period contains the two year anniversary and a rate change." & vbCrLf & "Do you want RiskMaster do the calculations?"
                            Exit Function
                        Case g_objXErrorMask.cNoRateChange
                            m_ErrorMask = g_objXErrorMask.cNoRateChange
                            m_Note = "Dates of Payment are over two years from Date of Event.  There was no change in Payment Rates."

                    End Select
                    Select Case Me.ClaimantJurisAWW
                        Case Is < Me.JurisMinBenRateWeek
                            'jlt 04/10/2001 aww is below state floor (first level)
                            'benefit is dollar for dollar
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((Me.ClaimantJurisAWW / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                        Case Is < (Me.JurisMinBenRateWeek * 1.5)
                            'jlt 04/10/2001 aww is over state floor and below second level
                            'benefit is at the Jurisdiction Min
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((Me.ClaimantJurisAWW / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                            If dblCalculatedPayment < 0.01 Then
                                dblCalculatedPayment = 0
                            End If
                        Case Else
                            dbl = modFunctions.RoundStandard(Me.ClaimantJurisAWW * 2 / 3, 2) 'same as base rate
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((dbl / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                    End Select
                    If CDate(Me.TwoYearStartDateCalendar) > CDate("12/31/2002") And g_objXClaim.FilingStatePostalCode = "CA" Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dblCalculatedPayment < ((126 / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays) Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((126 / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays)
                            'colWorkSheet.Add "|California imposed floor for Events after 12/31/2002 (or Two Year Rule) is $126 per week|"
                        End If
                    Else
                    End If
                Case Else
            End Select
            Me.CalculatedPayment = dblCalculatedPayment
            If Me.CalculatedPayment < 0.01 Then
                g_lErrNum = g_objXErrorMask.cNoPaymentDue
                Exit Function
            End If
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lCalculatePayment_CA_Split = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            lCalculatePayment_CA_Split = g_lErrNum

        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dMaxRate As Double
        Dim lReturn As Integer

        Try

            dGetBasicRate = 0

            bRuleIsLocal = False
            dBasicRate = 0
            dMaxRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            lReturn = GetSpendableIncomeRule()
            Select Case lReturn
                Case -1 'spendable income jurisdiction
                    lReturn = modFunctions.GetRateSpendable(dBasicRate, m_ErrorMaskSpendData, AverageWage)
                    If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSAWW + m_ErrorMaskSpendData) > g_objXErrorMask.cSuccess Then
                        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                        If bRuleIsLocal = True Then objBenefitRule = Nothing
                    End If
                    'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    If bRuleIsLocal = True Then objBenefitRule = Nothing
                    dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)
                    Exit Function

                Case 0 'discounted jurisdiction
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = modFunctions.RoundStandard(AverageWage * objBenefitRule.PrimeRate, 2)
                    'there are three jurisdictions (California, Ohio and Texas)that have dual rate conditions
                    'each have different rules
                    Select Case g_objXClaim.FilingStatePostalCode
                        Case "CA"
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayCurrentRateAfterTwoYears. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If objBenefitRule.PayCurrentRateAfterTwoYears = g_lYesCodeID Then
                                If Not IsDate(Me.TwoYearStartDateCalendar) Then
                                    Me.TwoYearStartDateCalendar = GetTwoYearStartDateCalendar()
                                End If
                                If Today >= CDate(Me.TwoYearStartDateCalendar) Then
                                    lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTD", objBenefitRule, sDLLClassNameJRRule, True)
                                    'error messages are generated in GetBenefitRuleTT
                                    If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dBasicRate = modFunctions.RoundStandard(AverageWage * objBenefitRule.PrimeRate, 2)
                                End If
                            End If

                        Case "OH"
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If objBenefitRule.SecondRate > 0 Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If m_PaidPendingWeeks < objBenefitRule.SecondRateMaxWeeks Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dBasicRate = modFunctions.RoundStandard(AverageWage * objBenefitRule.SecondRate, 2)
                                Else
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dBasicRate = modFunctions.RoundStandard(AverageWage * objBenefitRule.PrimeRate, 2)
                                End If
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dBasicRate = modFunctions.RoundStandard(AverageWage * objBenefitRule.PrimeRate, 2)
                            End If

                        Case "TX"
                            'Texas is handled in CWCTempIncomeTexas because of the wage factor

                    End Select
                    dRateBeforeRules = dBasicRate
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If (AverageWage < objBenefitRule.FloorAmount) Then
                                dBasicRate = AverageWage
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If (dBasicRate < objBenefitRule.FloorAmount) Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dBasicRate = objBenefitRule.FloorAmount
                                End If
                            End If
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If (dBasicRate < objBenefitRule.FloorAmount) Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dBasicRate = objBenefitRule.FloorAmount
                            End If
                        End If
                    End If
                    With objBenefitRule
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If .MaxCompRateWeekly > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If dBasicRate > .MaxCompRateWeekly Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dBasicRate = .MaxCompRateWeekly
                            End If
                        End If
                    End With
                Case g_objXErrorMask.cNoJurisdictionalRule
                    m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                    Exit Function
            End Select

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objSpendableIncome may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncome = Nothing
        'UPGRADE_NOTE: Object objSpendableIncomeRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncomeRule = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

