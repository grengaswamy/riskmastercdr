Option Strict Off
Option Explicit On
Public Class CWCEnumErrorMask
    'const sClassName as string = "CWCEnumErrorMask"
    Public cSuccess As Integer
    Public cNoWeeklyWage As Integer
    Public cNoEventDate As Integer
    Public cNoBankAccounts As Integer
    Public cNoTransMapping As Integer
    Public cNoRecord As Integer
    Public cNoPaymentDue As Integer
    Public cNoPPDRecord As Integer
    Public cSplitPayment As Integer
    Public cNoRateChange As Integer
    Public cRateChanged As Integer
    Public cPreCATwoYear As Integer
    Public cNoEarnings As Integer
    Public cNoEmployee As Integer
    Public cNoJurisdictionalRule As Integer
    Public cNoImpairment As Integer
    Public cNoImpairmentDate As Integer
    Public cInvalidReserve As Integer
    Public cNoTransTypeCode As Integer
    Public cNoSpendableIncomeData As Integer
    Public cInvalidTransDates As Integer
    Public cNoDateOfDeath As Integer
    Public cNoBodyMemberData As Integer
    Public cNoDependentsOnRecord As Integer
    Public cMissingTaxStatus As Integer
    Public cNoTempTotalRule As Integer
    Public cNoValidAWWCalSetup As Integer
    Public cNoValidBenefitCalSetup As Integer


    Public Function SetConstantValues() As Integer
        cSuccess = 0
        cNoWeeklyWage = 1
        cNoEventDate = 2
        cNoBankAccounts = 4
        cNoTransMapping = 8
        cNoRecord = 16
        cNoPaymentDue = 32
        cNoPPDRecord = 64
        cSplitPayment = 128
        cNoRateChange = 256
        cRateChanged = 512
        cPreCATwoYear = 1024
        cNoEarnings = 2048
        cNoEmployee = 4096
        cNoJurisdictionalRule = 8192
        cNoImpairment = 16384
        cInvalidReserve = 32768
        cNoTransTypeCode = 65536
        cNoSpendableIncomeData = 131072
        cInvalidTransDates = 131072
        cNoDateOfDeath = 262144
        cNoBodyMemberData = 524288
        cNoDependentsOnRecord = 1048576
        cMissingTaxStatus = 2097152
        cNoImpairmentDate = 4194304
        cNoTempTotalRule = 8388608
        cNoValidAWWCalSetup = 17777216
        cNoValidBenefitCalSetup = 35554432




    End Function
End Class

