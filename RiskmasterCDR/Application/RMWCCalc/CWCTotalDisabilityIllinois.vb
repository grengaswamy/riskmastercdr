Option Strict Off
Option Explicit On
Public Class CWCTotalDisabilityIllinois
    Implements _ICalculator
    ' Notes     : Beware that there are two different types of jurisdictions for Workers
    ' ..........: Compensation; jurisdictions that work from a discounted AWW and jurisdictions
    ' ..........: that work from a Spendable Income lookup table
    '---------------------------------------------------------------------------------------
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "TTD"
    Const sClassName As String = "CWCTotalDisabilityIllinois"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleTTDIL"
    Const m_MinimumSwitchDateDBFormat As String = "20060102"
    '******************************************************************************************************************
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objSpendableIncome As Object
    Dim objSpendableIncomeRule As Object

    Public JurisdictionMaxAww As Double 'does not have to have a value
    Public BenefitDays As Integer
    Public BenefitDays_01 As Integer
    Public BenefitDays_02 As Integer
    Public BenefitEndDate As String
    Public BenefitStartDate As String
    Public BenefitRate_Effective As Double
    Public BenefitRate_Original As Double
    Public BenefitRate_TTD As Double
    Public BenefitRate_TPD As Double

    Public PayLateCharge As Short

    Private m_WaitingPeriodIsPaid As Short

    Public Property WaitingPeriodIsPaid() As Short
        Get
            WaitingPeriodIsPaid = m_WaitingPeriodIsPaid
        End Get
        Set(ByVal Value As Short)
            m_WaitingPeriodIsPaid = Value
        End Set
    End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 1/5/2005 13:48
    ' Author    : jtodd22
    ' Purpose   :
    ' Note      : jlt, 03/23/2001 California uses the employee AWW (m_AWW_WeeklyRate) directly in
    ' ..........: calculation where the AWW is <= a state set value (m_dttdJurisdictionMaxAww) otherwise the
    ' ..........: state set max value (m_dttdJurisdictionMaxAww) is used
    ' ..........: jlt 07/29/2003  cap aww if over the jurisdiction max aww
    '--------------------------------------------------------------------------------------
    '
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Try

            Dim dblNewClaimantOriginalAWW As Double
            Dim sStartDate As String
            Dim dbl As Double
            Dim dDateOfEvent As Date
            Dim lReturn As Integer
            Dim sDateOfEvent_DTG As String
            Dim sDummyDate_DTG As String
            Dim sSQL As String
            Dim sTemp As String

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function


            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.RuleTotalWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_RuleTotalWeeks = objBenefitRule.RuleTotalWeeks

            BenefitDays = g_objXPaymentParms.BenefitDays
            BenefitEndDate = g_objXPaymentParms.BenefitEndDate
            BenefitStartDate = g_objXPaymentParms.BenefitStartDate

            sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitTypeAbbr, "")
            m_PaidPendingWeeks = GetBenefitWeeksPaidOrPending(sTemp)


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:32
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim bRateChanged As Boolean
        Dim dRate As Double
        Dim lReturn As Integer

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            dRate = 0

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            If m_EarningsRequiredCode = g_lYesCodeID Then
                If g_objXPaymentParms.EarningsPerWeek = 0 And g_objXPaymentParms.EarningsPerMonth = 0 Then
                    m_Warning = "Claimant does not have earnings and earnings are required."
                    Exit Function
                End If
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = "Claimant has been paid or has pending payment " & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks of TTP or TPD." & vbCrLf & "There is no Temporary liability left to pay."
                    Exit Function
                End If
            End If

            'build top of work sheet
            If colWorkSheet.Count() = 0 Then
                'build top of work sheet
                modFunctions.BuildTopOfWorkSheet(colWorkSheet)
                'build benefit type detail
            Else
                colWorkSheet.Add("||")
            End If

            g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW

            If g_objXPaymentParms.EarningsPerWeek > 0 Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|The Earnings per week are | " & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))
                g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW - g_objXPaymentParms.EarningsPerWeek
                colWorkSheet.Add("|AWW to compensate      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dRate = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * (objBenefitRule.PrimeRate / 100), 2)
            If dRate = 0 Then
                m_Warning = ""
                m_Warning = m_Warning & "The compensation rate was not found or determined." & vbCrLf & vbCrLf
                m_Warning = m_Warning & "Jurisdiction is:  " & g_objXClaim.FilingStatePostalCode & vbCrLf
                m_Warning = m_Warning & "Event date:  " & g_objXClaim.DateOfEventCalar
                Exit Function
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If dRate > objBenefitRule.MaxCompRateWeekly And objBenefitRule.MaxCompRateWeekly > 0 Then dRate = objBenefitRule.MaxCompRateWeekly

            'Apply Miniumn rates
            bRateChanged = False
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDateDTG. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EffectiveDateDTG < m_MinimumSwitchDateDBFormat Then
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child1
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child1 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with One Child|" & Format(objBenefitRule.Child1, "Currency"))
                            End If
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child2
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child2 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with Two Children|" & Format(objBenefitRule.Child2, "Currency"))
                            End If
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child3
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child3 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with Three Children|" & Format(objBenefitRule.Child3, "Currency"))
                            End If
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child4 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child4
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child4 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum Rate with Four Children|" & Format(objBenefitRule.Child4, "Currency"))
                            End If
                        End If
                    Case Else
                        Select Case Trim(UCase(GetCodeDesc_SQL(modFunctions.GetMartialStatusCode)))
                            Case "MARRIED"
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.MarriedValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.MarriedValue
                                    bRateChanged = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If g_objXClaim.AWWToCompensate < objBenefitRule.MarriedValue Then
                                        dRate = g_objXClaim.AWWToCompensate
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        colWorkSheet.Add("|Minimum Rate Married Status|" & Format(objBenefitRule.MarriedValue, "Currency"))
                                    End If
                                End If
                            Case Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.SingleValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.SingleValue
                                    bRateChanged = True
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If g_objXClaim.AWWToCompensate < objBenefitRule.SingleValue Then
                                        dRate = g_objXClaim.AWWToCompensate
                                    Else
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        colWorkSheet.Add("|Minimum Rate Single Status|" & Format(objBenefitRule.SingleValue, "Currency"))
                                    End If
                                End If
                        End Select
                End Select
                If bRateChanged = True Then
                    colWorkSheet.Add("|Payment Rate after Minimum is|" & Format(dRate, "Currency"))
                End If
            Else
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID) + modFunctions.GetDependentSpouseCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse0
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse0 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with zero Child/Spouse|" & Format(objBenefitRule.ChildSpouse0, "Currency"))
                            End If
                        End If
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse1
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse1 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with One Child/Spouse|" & Format(objBenefitRule.ChildSpouse1, "Currency"))
                            End If
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse2
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse2 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with Two Children/Spouse|" & Format(objBenefitRule.ChildSpouse2, "Currency"))
                            End If
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse3
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse3 Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with Three Children/Spouse|" & Format(objBenefitRule.ChildSpouse3, "Currency"))
                            End If
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse4P Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse4P
                            bRateChanged = True
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse4P Then
                                dRate = g_objXClaim.AWWToCompensate
                            Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Minimum with four or more Children/Spouse|" & Format(objBenefitRule.ChildSpouse4P, "Currency"))
                            End If
                        End If
                End Select
                If bRateChanged = True Then
                    colWorkSheet.Add("|Payment Rate after Minimum is|" & Format(dRate, "Currency"))
                End If
            End If

            If g_objXPaymentParms.EarningsPerWeek > 0 Then
                colWorkSheet.Add("|Claimant's Weekly Rate After Earnings  |" & Format(dRate, "Currency"))
            End If

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function


            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Private Function ClearObject() As Integer
        BenefitRate_Effective = -0.01
        BenefitRate_TTD = -0.01
        BenefitRate_TPD = -0.01
        'jtodd22 do not reset BenefitDays it is set in calling executable
        m_CheatSheetTitle = "Temporary Total"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dRate As Double
        Dim lReturn As Integer

        Try

            dGetBasicRate = g_objXErrorMask.cSuccess
            bRuleIsLocal = False
            dBasicRate = 0
            dRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dRate = modFunctions.RoundStandard(AverageWage * (objBenefitRule.PrimeRate / 100), 2)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If dRate > objBenefitRule.MaxCompRateWeekly And objBenefitRule.MaxCompRateWeekly > 0 Then dRate = objBenefitRule.MaxCompRateWeekly

            If g_objXClaim.ClaimantOriginalAWW > 0 Then
                g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
            Else
                g_objXClaim.AWWToCompensate = AverageWage
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDateDTG. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EffectiveDateDTG < m_MinimumSwitchDateDBFormat Then
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child1
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child1 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child2
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child2 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child3
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child3 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.Child4 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.Child4
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.Child4. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.Child4 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case Else
                        Select Case Trim(UCase(GetCodeDesc_SQL(modFunctions.GetMartialStatusCode)))
                            Case "MARRIED"
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.MarriedValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarriedValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.MarriedValue
                                    If g_objXClaim.AWWToCompensate < dRate Then dRate = g_objXClaim.AWWToCompensate
                                End If
                            Case Else
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If dRate < objBenefitRule.SingleValue Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SingleValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dRate = objBenefitRule.SingleValue
                                    If g_objXClaim.AWWToCompensate < dRate Then dRate = g_objXClaim.AWWToCompensate
                                End If
                        End Select
                End Select
            Else
                Select Case modFunctions.GetDependentChildCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID) + modFunctions.GetDependentSpouseCount(g_objXClaim.ClaimantEID, g_objXClaim.EventID)
                    Case 0
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse0
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse0 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse1 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse1
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse1. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse1 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 2
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse2 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse2
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse2 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 3
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse3 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse3
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse3. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse3 Then dRate = g_objXClaim.AWWToCompensate
                        End If
                    Case 4
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRate < objBenefitRule.ChildSpouse4P Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRate = objBenefitRule.ChildSpouse4P
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ChildSpouse4P. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If g_objXClaim.AWWToCompensate < objBenefitRule.ChildSpouse4P Then dRate = g_objXClaim.AWWToCompensate
                        End If
                End Select
            End If
            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objSpendableIncome may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncome = Nothing
        'UPGRADE_NOTE: Object objSpendableIncomeRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncomeRule = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

