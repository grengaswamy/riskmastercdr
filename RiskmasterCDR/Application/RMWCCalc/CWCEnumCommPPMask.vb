Option Strict Off
Option Explicit On
Public Class CWCEnumCommPPMask
    Public cppNoPPD As Short
    Public cppLiabilityPaid As Short
    Public cppCommutatedToZero As Short
    Public cppPastLastDatePayable As Short
    Public cpp30PlusDayWarning As Short
    Public cppClaimUnderPaid As Short
    Public cppClaimOverPaid As Short
    Public cppExcessiveLumpSum As Short

    Public Function SetConstantValues() As Integer
        cppNoPPD = 1
        cppLiabilityPaid = 2
        cppCommutatedToZero = 4
        cppPastLastDatePayable = 8
        cpp30PlusDayWarning = 16
        cppClaimUnderPaid = 32
        cppClaimOverPaid = 64
        cppExcessiveLumpSum = 128


    End Function
End Class

