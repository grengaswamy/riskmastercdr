Option Strict Off
Option Explicit On
Public Class CWCXBodyMember
    'sClassName = "CWCXBodyMember"
    Public AmputatedCode As Integer
    Public AmputationPercentRate As Double
    Public BodyMemberDesc As String
    Public Degrees As Short
    Public ImpairmentPercentage As Double
    Public LiabilityAmount As Double
    Public MaxAmount As Decimal
    Public MaxWeeks As Double
    Public MaxMonths As Double
    Public PayRate As Double
    Public TimeUnit As Integer
End Class

