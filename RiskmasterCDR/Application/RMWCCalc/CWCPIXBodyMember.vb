Option Strict Off
Option Explicit On
Public Class CWCPIXBodyMember
    Const sClassName As String = "CWCPIXBodyMember"
    Const sTableName As String = "PI_X_BODY_MEMBER"
    Private m_TableRowID As Integer
    Private m_PiRowId As Integer
    Private m_WCPBodyMemberID As Integer
    Private m_ImpairmentPercent As Double
    Private m_AmputatedCode As Integer 'Yes/No code

    Public ReadOnly Property Amputated() As Integer
        Get
            AmputatedCode = m_AmputatedCode
        End Get
    End Property
    Public WriteOnly Property AmputatedCode() As Integer
        Set(ByVal Value As Integer)
            m_AmputatedCode = Value
        End Set
    End Property

    Public Property TableRowID() As Integer
        Get
            TableRowID = m_TableRowID
        End Get
        Set(ByVal Value As Integer)
            m_TableRowID = Value
        End Set
    End Property

    Public Property PiRowId() As Integer
        Get
            PiRowId = m_PiRowId
        End Get
        Set(ByVal Value As Integer)
            m_PiRowId = Value
        End Set
    End Property

    Public Property WCPBodyMemberID() As Integer
        Get
            WCPBodyMemberID = m_WCPBodyMemberID
        End Get
        Set(ByVal Value As Integer)
            m_WCPBodyMemberID = Value
        End Set
    End Property

    Public Property ImpairmentPercent() As Double
        Get
            ImpairmentPercent = m_ImpairmentPercent
        End Get
        Set(ByVal Value As Double)
            m_ImpairmentPercent = Value
        End Set
    End Property
End Class

