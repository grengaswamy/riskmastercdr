Option Strict Off
Option Explicit On
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
Public Class CWCWorkLossDis_Occ
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "TPD"
    Const sClassName As String = "CWorkLossDis_Occ"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleTTDAA"
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    '******************************************************************************************************************
    '***The California Two year Rule, Labor Code 4661.5 does not apply to Temporary Partial Disability or Work Loss

    Public PayLateCharge As Short

    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim sTemp As String

        Dim dblNewClaimantOriginalAWW As Double
        Dim sStartDate As String
        Dim dbl As Double
        Dim dDateOfEvent As Date
        Dim lReturn As Integer
        Dim sDateOfEvent_DTG As String
        Try
            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            m_EarningsRequiredCode = g_lYesCodeID
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)


            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.RuleTotalWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_RuleTotalWeeks = objBenefitRule.RuleTotalWeeks

            sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitTypeAbbr, "")
            m_PaidPendingWeeks = modBenefitFunctions.GetBenefitWeeksPaidOrPending(sTemp)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                If Trim(g_sErrDescription) = "" Then
                    g_sErrDescription = Err.Description
                Else
                    g_sErrDescription = g_sErrDescription & ";" & Err.Description
                End If
            End With

            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            lLoadData = g_lErrNum

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:34
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim dRate As Double
        Dim lReturn As Integer
        Dim sTmp As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            If m_EarningsRequiredCode = g_lYesCodeID Then
                If g_objXPaymentParms.EarningsPerWeek = 0 And g_objXPaymentParms.EarningsPerMonth = 0 Then
                    m_Warning = "Claimant does not have earnings and earnings are required."
                    Exit Function
                End If
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = "Claimant has been paid or has pending payment " & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks of TTP or TPD." & vbCrLf & "There is no Temporary liability left to pay."
                    Exit Function
                End If
            End If

            'build top of work sheet
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail

            g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW - g_objXPaymentParms.EarningsPerWeek
            colWorkSheet.Add("|EarningsPerWeek |" & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))
            colWorkSheet.Add("|Weekly Wage to Compensate |" & Format(g_objXClaim.AWWToCompensate, "Currency"))

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                colWorkSheet.Add("     |Jurisdiction Imposed AWW Maximum Limit|" & Format(objBenefitRule.MaxAWW, "Currency"))
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If g_objXClaim.AWWToCompensate > objBenefitRule.MaxAWW Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    g_objXClaim.AWWToCompensate = objBenefitRule.MaxAWW
                    colWorkSheet.Add("|Weekly Wage To Compensate after Max AWW Limit |" & Format(g_objXClaim.AWWToCompensate, "Currency"))
                End If
            Else
                colWorkSheet.Add("     |Jurisdiction Imposed AWW Maximum Limit|(None)")
            End If

            Select Case UCase(g_objXClaim.FilingStatePostalCode)
                Case "CA"
                    If g_objXClaim.DateOfEventDTG > "20021231" Then
                        If g_objXClaim.AWWToCompensate < 189 Then
                            g_objXClaim.AWWToCompensate = 189
                            sTmp = ""
                            sTmp = sTmp & "California Imposed a minimum TTD Rate of $126 per week for events after 12/31/2002."
                            sTmp = sTmp & "For calculations this is the same as a minimum AWW of $189 per Week."
                            colWorkSheet.Add("|" & sTmp & "|")
                            colWorkSheet.Add("|Claimant AWW (After Imposing Jurisdiction Cap and Floor Limits)      |" & Format(g_objXClaim.AWWToCompensate, "Currency"))
                        End If
                    Else
                        colWorkSheet.Add("|Claimant AWW As Capped (Jurisdiction Imposed Limit)      |" & Format(g_objXClaim.AWWToCompensate, "Currency"))
                    End If
                Case Else
                    colWorkSheet.Add("|Claimant AWW As Capped (Jurisdiction Imposed Limit)      |" & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End Select

            With g_objXClaim
                dRate = dGetBasicRate(.AWWToCompensate, .ClaimantTTDRate, .JurisTaxExemptions, .JurisTaxStatusCode, 0, .ClaimantHourlyPayRate)
            End With

            colWorkSheet.Add("     ||")
            colWorkSheet.Add("     |Claimant's Effective Compensation Rate|" & Format(dRate, "Currency"))
            colWorkSheet.Add("     |")

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function

            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
            lCalculatePayment = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Temporary Partial"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function
    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dMaxRate As Double
        Dim lErrorNumber As Integer
        Dim lReturn As Integer

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0
            dMaxRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function

                'jlt 07/29/2003  cap aww if over the jurisdiction max aww
                g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW

                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If g_objXClaim.AWWToCompensate > objBenefitRule.MaxAWW And objBenefitRule.MaxAWW > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    g_objXClaim.AWWToCompensate = objBenefitRule.MaxAWW
                End If
            End If

            lReturn = modFunctions.IsSpendableJurisdiction(lErrorNumber)
            Select Case lReturn
                Case g_lYesCodeID 'spendable income jurisdiction
                    Select Case g_objXClaim.FilingStatePostalCode
                        Case "IA"
                            'uses a discount for Temporary Partial
                            '(g_objXClaim.ClaimantOriginalAWW - g_objXClaim.EarningsPerWeek) * discount

                        Case Else
                            lReturn = modFunctions.GetRateSpendable(dBasicRate, m_ErrorMaskSpendData, AverageWage)
                            If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSAWW + m_ErrorMaskSpendData) > g_objXErrorMask.cSuccess Then Exit Function

                            With objBenefitRule
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                If .MaxCompRateWeekly > 0 Then
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If dBasicRate > .MaxCompRateWeekly Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        dBasicRate = .MaxCompRateWeekly
                                        dGetBasicRate = dBasicRate
                                        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                        If bRuleIsLocal Then objBenefitRule = Nothing
                                        Exit Function
                                    End If
                                End If
                            End With
                    End Select

                Case g_lNoCodeID 'discounted jurisdiction
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.PrimeRate = 0 Then
                        m_Warning = "The Jurisdictional Rule's Prime Rate is zero (0).  The Rate can not be calculated."
                        dGetBasicRate = 0
                        Exit Function
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * objBenefitRule.PrimeRate, 2)
                    'there are three jurisdictions (California, Ohio and Texas)that have dual rate conditions
                    'each have different rules
                    Select Case g_objXClaim.FilingStatePostalCode
                        Case "CA"
                            'California does not use the two year rule for workloss
                        Case "OH"
                            'Ohio does not have dual rates for WorkLoss
                        Case "TX"
                            'Texas is handled in CWCTempIncomeTexas because of the wage factor
                    End Select

                    With objBenefitRule
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If .MaxCompRateWeekly > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If dBasicRate > .MaxCompRateWeekly Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dBasicRate = .MaxCompRateWeekly
                                dGetBasicRate = dBasicRate
                                'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                                If bRuleIsLocal Then objBenefitRule = Nothing
                                Exit Function
                            End If
                        End If
                    End With

                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.FloorAmount > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If objBenefitRule.DollarForDollar <> g_lYesCodeID Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If dBasicRate < objBenefitRule.FloorAmount Then dBasicRate = objBenefitRule.FloorAmount
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Select Case objBenefitRule.FloorAmount
                                Case Is >= g_objXClaim.AWWToCompensate
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                                        dBasicRate = modFunctions.RoundStandard((g_objXClaim.AWWToCompensate), 2)
                                    End If
                                Case Is >= (g_objXClaim.AWWToCompensate * objBenefitRule.PrimeRate)
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    dBasicRate = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * objBenefitRule.PrimeRate, 2)
                                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                    If dBasicRate < objBenefitRule.FloorAmount Then
                                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                        dBasicRate = objBenefitRule.FloorAmount
                                    End If
                            End Select
                        End If
                    End If
            End Select

            'SAWW based Maximum and Minimum is handled in the objBenefit.LoadDataByEventDate medthod
            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function



    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property

    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property

    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function

    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

