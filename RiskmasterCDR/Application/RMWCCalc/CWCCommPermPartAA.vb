Option Strict Off
Option Explicit On
Imports Riskmaster.Db
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
Public Class CWCCommPermPartAA
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "PPDCOMM"
    Const sClassName As String = "CWCCommPermPartAA"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRulePPDCOM"
    Public objBenefitRule As Object
    'Dim objBenefitRule As RMJuRuLib.CJRBenefitRuleTTD
    Public objPPD As CWCPermPartDis
    Public objCalcBenefitRule As Object
    Private objScreens As CWCEnumEnumDisBenCalcScrn
    Private oEnumPPDMask As CWCEnumPPDMask
    Private oEnumCommPPMask As CWCEnumCommPPMask

    Public AmountOwed As Double
    Public BenefitWeeksEliminated As Double
    Public BenefitWeeksRemaining As Double
    Public DateOfCommutation As String
    Public DateOfCommutation_DTG As String
    Public DaysExpectedPaid As Short
    Public LastBenefitPaidDate As String 'this is default "date of commutation"
    Public LastPermPartialDate As String 'Last PP Payable
    Public LumpSum As Double
    Public RemainingWeeks As Double
    Public WeeksEliminated As Double
    Public NewPPDRate As Double
    Public ClaimID As Integer
    Public FilingStateID As Integer
    Public BenefitRate_New As Double
    Public PermCommType As Short
    Private m_bInitialized As Boolean
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Object
        Const sFunctionName As String = "lCalculatePayment"

        'Commutation of permanent benefits
        Dim cCommutedValue As Decimal
        Dim cPaymentReduced As Decimal

        Dim dDays_1 As Double
        Dim dFractionalWeek As Double

        Dim dPDBenefitWeeksPaid As Double
        Dim dPPDBenefitWeeksPayable As Double

        Dim dPDBenefitWeeksTotal As Double

        Dim LumpSum As Double
        Dim dPVAmount As Double
        Dim dPVDelta As Double
        Dim dPVDelta_1 As Double
        Dim dPVDelta_2 As Double
        Dim dPVDelta_3c As Double
        Dim dPVDelta_3f As Double
        Dim dPVDelta_Xc As Double
        Dim dPVFractionalWeek As Double
        Dim dPVHigher As Double
        Dim dPVLower As Double
        Dim dPVHigher_1 As Double
        Dim dPVLower_1 As Double
        Dim dPVHigher_2 As Double
        Dim dPVLower_2 As Double
        Dim dPVProportionalAmount As Double
        Dim dPVWeeksRemaining As Double
        Dim dPVWeeksRemainingAfterComm As Double
        Dim dTest As Double
        Dim i As Short
        Dim iPDWeeksLower As Short

        Dim lBenefitDaysCalendar As Integer
        Dim lBenefitDaysPaid As Integer
        Dim lBenefitWeeksPaid As Integer
        Dim lDaysPDPayable As Integer
        Dim lReturn As Integer
        Dim objReader As DbReader

        Dim sSQL As String
        Dim sTemp As String

        Try

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object lCalculatePayment. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lCalculatePayment = g_objXErrorMask.cSuccess
            'build top of work sheet
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail
            Me.BenefitRate_New = 0
            lBenefitDaysCalendar = 0
            lBenefitDaysPaid = 0
            lBenefitWeeksPaid = 0

            'lBenefitDaysCalendar...DateDiff produces the difference between dates, we want the
            'the day count beginning with Date1 and ending with Date2 or (DateDiff + 1)
            'jlt 08/02/2001
            If Me.objPPD.LastCommutationDate = "" Then
                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                lBenefitDaysCalendar = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sDBDateFormat((Me.objPPD.BeginPrntDate_Original), "Short Date")), CDate(Me.DateOfCommutation)) + 1
                '        If f_bGetPaymentHistoryByBenefitSummary(Me.objPPD.BeginPrntDate_Original, Me.DateOfCommutation) Then
                '        Else
                '        End If
            Else
                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                lBenefitDaysCalendar = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(Me.objPPD.BeginPrntDate_Original), CDate(Me.DateOfCommutation)) + 1
                '        If f_bGetPaymentHistoryByBenefitSummary(Me.objPPD.LastCommutationDate, Me.objPPD.DateOfCommutation_DTG) Then
                '        Else
                '        End If
            End If

            lBenefitDaysPaid = modFunctions.RoundStandard((Me.objPPD.Standard.PaidPendingWeeks * 7) / Me.objPPD.BenefitRate_Effective, 4)

            dTest = Me.objPPD.WeeksPayable_Effective
            If dTest = -1 Then
                Exit Function
            Else
                dPPDBenefitWeeksPayable = dTest
            End If
            sTemp = Me.objPPD.BeginPrntDate_Original
            sTemp = Mid(sTemp, 5, 2) & "/" & Mid(sTemp, 7, 2) & "/" & Mid(sTemp, 1, 4)
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            lBenefitDaysPaid = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sTemp), CDate(Me.DateOfCommutation)) + 1
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dPDBenefitWeeksPaid = modFunctions.RoundStandard(lBenefitDaysPaid / objCalcBenefitRule.JurisWorkWeek, 4)

            dDays_1 = modFunctions.RoundStandard(7 * dPDBenefitWeeksPaid, 0)

            'Check that the benefit realy paid matches the calendar benefit
            lBenefitWeeksPaid = modFunctions.RoundStandard((Me.objPPD.Standard.PaidPendingWeeks) / Me.objPPD.BenefitRate_Effective, 4)

            Select Case lBenefitDaysPaid
                Case Is < lBenefitDaysCalendar
                    'Benefit is under paid, two things can cause:
                    '1)Voc Rehab, this is a legal reason for shortage
                    '2)Payment is not in funds_trans_split or funds_auto_split, this not a legal reason for shortage
                    dTest = GetBenefitAmountPaidOrPending2("4,5,6,16", Me.objPPD.LastCommutationDate_DTG)
                    If dTest = 0 Then

                        'Benefit is under paid, warn client of shortage, adjust dPDBenefitWeeksPaid
                        m_Note = "This claim appears to be under paid based as expected liability and payment history." & vbCrLf & "Please insure payments are correct before saving this commutation."
                        ''                       "This application will adjust the 'Weeks Paid' for this commutation.", , m_sFormName
                        ''                dPDBenefitWeeksPaid =modfunctions.roundstandard(m_dPermanentPartialPaid / m_dPPDBenefitRate_Old, 4)
                        ''                colWorkSheet.add (12) = "     |Weeks Paid was adjusted for apparent shortage in payments|"
                    Else
                        'Benefit presumed correct, do nothing
                    End If
                Case Is = lBenefitDaysCalendar
                    'Benefit is perfect match, do nothing
                Case Is > lBenefitDaysCalendar
                    'Benefit is over paid, warn client, exit sub

                    m_Note = "This claim appears to be over paid based as expected liability and payment history." & vbCrLf & "Please insure payments are correct."
                    ''                   "This application will will not procede for this commutation.", , m_sFormName
                    ''            Exit Sub
                Case Else
                    'error
            End Select
            If dPDBenefitWeeksPaid <> modFunctions.RoundStandard(Me.objPPD.Standard.PaidPendingWeeks / Me.objPPD.BenefitRate_Effective, 4) Then
            End If
            dDays_1 = modFunctions.RoundStandard(7 * dPDBenefitWeeksPaid, 4)
            Me.BenefitWeeksRemaining = modFunctions.RoundStandard(dPPDBenefitWeeksPayable - dPDBenefitWeeksPaid, 4)


            'build top of work sheet
            colWorkSheet.Add("     |Claim Number                                      |" & g_objXClaim.ClaimNumber, CStr(1))
            colWorkSheet.Add("     |Claimant Name                                     |" & g_objXClaim.ClaimantNameLFM, CStr(2))
            colWorkSheet.Add("     |                                                  |", CStr(3))

            colWorkSheet.Add("     |Date of Injury                                    |" & g_objXClaim.DateOfEventCalar, CStr(4))
            colWorkSheet.Add("     |Permanent Benefit Commencement Date               |" & sDBDateFormat((Me.objPPD.BeginPrntDate_Original), "Short Date"), CStr(5))
            colWorkSheet.Add("     |Date of Commutation (DOC)                         |" & Me.DateOfCommutation, CStr(6))
            colWorkSheet.Add("     |Permanent Impairment Rate                         |" & Me.objPPD.DisabilityRate_Effective, CStr(7))
            colWorkSheet.Add("     |Permanent Benefit Rate                            |" & Format(Me.objPPD.BenefitRate_Effective, "Currency"), CStr(8))
            colWorkSheet.Add("     |Number of weeks of indemnity                      |" & Me.objPPD.WeeksPayable_Effective, CStr(9))
            colWorkSheet.Add("     |                                                  |", CStr(10))





            Select Case Me.PermCommType
                Case 0
                    Exit Function
                Case 1
                    'all remaining
                    'commutation of all remaining pd
                    'begin Procedure 1

                    'me.BenefitWeeksRemaining is 1f
                    'begin Procedure 2
                    dPVHigher = modFunctions.RoundStandard(f_dPVWeeksRemaining(Me.BenefitWeeksRemaining + 1), 4)
                    dPVLower = modFunctions.RoundStandard(f_dPVWeeksRemaining(Me.BenefitWeeksRemaining), 4)
                    If (dPVHigher - dPVLower) < 0.00005 Then
                        dPVDelta_1 = 0
                    Else
                        dPVDelta_1 = modFunctions.RoundStandard(dPVHigher - dPVLower, 4)
                    End If
                    dFractionalWeek = modFunctions.RoundStandard(Me.BenefitWeeksRemaining - Int(Me.BenefitWeeksRemaining), 4)
                    dPVFractionalWeek = modFunctions.RoundStandard(dPVDelta_1 * dFractionalWeek, 4)
                    dPVWeeksRemaining = dPVLower + dPVFractionalWeek
                    'dPVWeeksRemaining is 2g
                    'begin Procedure 3
                    If dPVWeeksRemaining < 0.00005 Then
                        Me.Standard.CalculatedPaymentRegular = 0
                    Else
                        Me.Standard.CalculatedPaymentRegular = CDec(CDbl(System.Math.Round(dPVWeeksRemaining, 4)) * Me.objPPD.BenefitRate_Effective)
                    End If
                    Me.AmountOwed = 0
                    Me.BenefitRate_New = 0
                    ''            Me.lblComm_PPRemainingWeeks_Data.Caption = CStr(0)
                    'build core of work sheet
                    colWorkSheet.Add("     |                                                                  |", CStr(11))
                    colWorkSheet.Add("     |                                                                  |", CStr(12))
                    colWorkSheet.Add("     |                                                                  |", CStr(13))
                    colWorkSheet.Add("1)   |Determine weeks of PD remaining after date of commutation (DOC)   |", CStr(14))
                    colWorkSheet.Add("1.a) |Number of days from PD commencement through DOC inclusive         |" & lBenefitDaysCalendar, CStr(15))
                    colWorkSheet.Add("1.b) |Divide by 7 days/week                                             |7", CStr(16))
                    colWorkSheet.Add("1.c) |Number of weeks from PD commencement though DOC                   |" & dPDBenefitWeeksPaid, CStr(17))
                    colWorkSheet.Add("1.d) |Total weeks of PD                                                 |" & dPPDBenefitWeeksPayable, CStr(18))
                    colWorkSheet.Add("1.e) |Subtract weeks elapsed through DOC(1c)                            |" & dPDBenefitWeeksPaid, CStr(19))
                    colWorkSheet.Add("1.f) |Weeks of PD remaining after date of commutation                   |" & Me.BenefitWeeksRemaining, CStr(20))
                    colWorkSheet.Add("     |                                                                  |", CStr(21))
                    colWorkSheet.Add("2)   |Determine PV of weeks of PD remaining after DOC (1c)              |", CStr(22))
                    colWorkSheet.Add("2.a) |PV of number of weeks just above 1f                               |" & dPVHigher, CStr(23))
                    colWorkSheet.Add("2.b) |Subtract next lower PV from table                                 |" & dPVLower, CStr(24))
                    colWorkSheet.Add("2.c) |Difference of 2a and 2b                                           |" & dPVDelta_1, CStr(25))
                    colWorkSheet.Add("2.d) |Multiply by fractional portion of 1f                              |" & dFractionalWeek, CStr(26))
                    colWorkSheet.Add("2.e) |PV of fractional week                                             |" & dPVFractionalWeek, CStr(27))
                    colWorkSheet.Add("2.f) |Add 2b                                                            |" & dPVLower, CStr(28))
                    colWorkSheet.Add("2.g) |PV of weeks remaining after DOC                                   |" & dPVWeeksRemaining, CStr(29))
                    colWorkSheet.Add("     |                                                                  |", CStr(30))
                    colWorkSheet.Add("3)   |Determine commuted value of all PD due for period after DOC       |", CStr(31))
                    colWorkSheet.Add("3.a) |PV of weeks remaining after DOC (2g)                              |" & dPVWeeksRemaining, CStr(32))
                    colWorkSheet.Add("3.b) |Multiply by PD rate                                               |" & Format(Me.objPPD.BenefitRate_Effective, "Currency"), CStr(33))
                    colWorkSheet.Add("3.c) |Commuted value of all PD due for period after DOC                 |" & Format(cCommutedValue, "Currency"), CStr(34))

                Case 2
                    'commutation of pd "off the far end"
                    'begin Procedure 1
                    'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                    If Me.objPPD.BenefitRate_Effective < 0.01 Or IsDBNull(Me.objPPD.BenefitRate_Effective) Then
                        dPVAmount = 0
                    Else
                        dPVAmount = modFunctions.RoundStandard(Me.LumpSum / Me.objPPD.BenefitRate_Effective, 4)
                    End If
                    'begin Procedure X
                    dPVHigher_1 = modFunctions.RoundStandard(f_dPVWeeksRemaining(Me.BenefitWeeksRemaining + 1), 4)
                    dPVLower_1 = modFunctions.RoundStandard(f_dPVWeeksRemaining(Me.BenefitWeeksRemaining), 4)
                    If (dPVHigher_1 - dPVLower_1) < 0.00005 Then
                        dPVDelta_Xc = 0
                    Else
                        dPVDelta_Xc = modFunctions.RoundStandard(dPVHigher_1 - dPVLower_1, 4)
                    End If
                    dFractionalWeek = modFunctions.RoundStandard(Me.BenefitWeeksRemaining - Int(Me.BenefitWeeksRemaining), 4)
                    dPVFractionalWeek = modFunctions.RoundStandard(dPVDelta_Xc * dFractionalWeek, 4)
                    dPVWeeksRemaining = dPVLower_1 + dPVFractionalWeek
                    'dPVWeeksRemaining is Example B, 2c
                    'begin Procedure 2
                    dPVWeeksRemainingAfterComm = dPVWeeksRemaining - dPVAmount
                    sSQL = "SELECT PRESENT_VALUE, WEEKS FROM WCP_PRSNT_VALUE_PD" & " WHERE PRESENT_VALUE <= " & (dPVWeeksRemaining - dPVAmount) & " ORDER BY PRESENT_VALUE DESC"
                    If DbFactory.GetDatabaseType(g_ConnectionString) = 1 Then sSQL = Replace(sSQL, "AS", " ") 'Remove alias As(notneeded except for access)
                    objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                    If (objReader.Read()) Then
                        dPVLower = objReader.GetInt32(0)
                        iPDWeeksLower = objReader.GetInt32(1)
                    Else
                        'fatal error
                    End If


                    sSQL = "SELECT MIN(PRESENT_VALUE) AS MIN_PV" & " FROM WCP_PRSNT_VALUE_PD" & " WHERE PRESENT_VALUE >= " & (dPVWeeksRemaining - dPVAmount)


                    If DbFactory.GetDatabaseType(g_ConnectionString) = 1 Then sSQL = Replace(sSQL, "AS", " ") 'Remove alias As(notneeded except for access)
                    objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                    If (objReader.Read()) Then
                        dPVHigher = objReader.GetInt32(0)
                    Else
                        'fatal error
                    End If


                    dPVDelta_3c = modFunctions.RoundStandard(dPVHigher - dPVLower, 4)
                    'dPVDelta_3c is Example B 3c

                    dPVDelta_3f = modFunctions.RoundStandard(dPVWeeksRemaining - dPVAmount - dPVLower, 4)

                    dPVProportionalAmount = modFunctions.RoundStandard(dPVDelta_3f / dPVDelta_3c, 4)

                    Me.BenefitWeeksRemaining = dPVProportionalAmount + iPDWeeksLower
                    'me.BenefitWeeksRemaining is Example B, 3j

                    Me.AmountOwed = modFunctions.RoundStandard(Me.BenefitWeeksRemaining * Me.objPPD.BenefitRate_Effective, 4)
                    'me.AmountOwed is Example B, 4c
                    Me.BenefitWeeksEliminated = modFunctions.RoundStandard(dPPDBenefitWeeksPayable - dPDBenefitWeeksPaid - Me.BenefitWeeksRemaining, 4)
                    'dPDWeeksEliminated is Example B, 5c
                    ''            lblComm_PPWeeksEliminated_Data.Caption = CStr(me.BenefitWeeksEliminated)
                    ''            Me.lblComm_PPRemainingWeeks_Data.Caption = FormatNumber(me.BenefitWeeksRemaining, 2)
                    ''            Me.lblPme.AmountOwedAmount.Caption = Format(me.AmountOwed, "Currency")
                    Me.Standard.CalculatedPaymentRegular = Me.LumpSum

                    If Me.BenefitWeeksRemaining < 0 And Me.AmountOwed < 0 Then
                        MsgBox("The Lump Sum is greater than the commutation value by " & Format(Me.AmountOwed, "Currency") & "." & vbCrLf & "Please use the 'All Remaining' for maximim payout.", , "Commutation of PP")
                        Exit Function
                    Else
                    End If
                    'build core of work sheet
                    colWorkSheet.Add("     |                                                                  |", CStr(11))
                    colWorkSheet.Add("     |                                                                  |", CStr(12))
                    colWorkSheet.Add("     |                                                                  |", CStr(13))
                    colWorkSheet.Add("1)   |Determine PV (at $1/week) of amount to be commuted                |", CStr(14))
                    colWorkSheet.Add("1.a) |Amount to be commuted                                             |" & Format(Me.LumpSum, "Currency"), CStr(15))
                    colWorkSheet.Add("1.b) |Divide by weekly PD rate                                          |" & Format(Me.objPPD.BenefitRate_Effective, "Currency"), CStr(16))
                    colWorkSheet.Add("1.c) |PV of amount to be commuted                                       |" & dPVAmount, CStr(17))
                    colWorkSheet.Add("     |                                                                  |", CStr(18))
                    colWorkSheet.Add("X)   |Determine PV of weeks of PD remaining after DOC (1c)              |", CStr(19))
                    colWorkSheet.Add("X.a) |PV of number of weeks just above 1f                               |" & dPVHigher_1, CStr(20))
                    colWorkSheet.Add("X.b) |Subtract next lower PV from table                                 |" & dPVLower_1, CStr(21))
                    colWorkSheet.Add("X.c) |Difference of 2a and 2b                                           |" & dPVDelta_Xc, CStr(22))
                    colWorkSheet.Add("X.d) |Multiply by fractional portion of 1f                              |" & dFractionalWeek, CStr(23))
                    colWorkSheet.Add("X.e) |PV of fractional week                                             |" & dPVFractionalWeek, CStr(24))
                    colWorkSheet.Add("X.f) |Add 2b                                                            |" & dPVHigher_1, CStr(25))
                    colWorkSheet.Add("X.g) |PV of weeks remaining after DOC                                   |" & dPVWeeksRemaining, CStr(26))
                    colWorkSheet.Add("     |                                                                  |", CStr(27))
                    colWorkSheet.Add("2)   |Determine PV of weeks remaining after commutation off far end     |", CStr(28))
                    colWorkSheet.Add("2.a) |PV of weeks remaining after DOC                                   |" & dPVWeeksRemaining, CStr(29))
                    colWorkSheet.Add("2.b) |Subtract PV of amount to be commuted (1c)                         |" & dPVAmount, CStr(30))
                    colWorkSheet.Add("2.c) |PV of weeks remaining after commutation off far end               |" & dPVWeeksRemainingAfterComm, CStr(31))
                    colWorkSheet.Add("     |                                                                  |", CStr(32))
                    colWorkSheet.Add("3)   |Determine number of weeks of PD remaining after commutation       |", CStr(33))
                    colWorkSheet.Add("3.a) |PV just above 2c                                                  |" & dPVHigher, CStr(34))
                    colWorkSheet.Add("3.b) |Subtract PV just below 2c                                         |" & dPVLower, CStr(35))
                    colWorkSheet.Add("3.c) |Difference of 3a and 3b                                           |" & dPVDelta_3c, CStr(36))
                    colWorkSheet.Add("3.d) |PV of weeks remaining after commutation off far end               |" & dPVWeeksRemainingAfterComm, CStr(37))
                    colWorkSheet.Add("3.e) |Subtract 3b                                                       |" & dPVLower, CStr(38))
                    colWorkSheet.Add("3.f) |Difference of 3d and 3e                                           |" & dPVDelta_3f, CStr(39))
                    colWorkSheet.Add("3.g) |Divide by 3c                                                      |" & dPVDelta_3c, CStr(40))
                    colWorkSheet.Add("3.h) |Proportional amount of                                            |" & dPVProportionalAmount, CStr(41))
                    colWorkSheet.Add("3.i) |Add to                                                            |" & iPDWeeksLower, CStr(42))
                    colWorkSheet.Add("3.j) |Number of weeks PD remaining after commutation off far end        |" & Me.BenefitWeeksRemaining, CStr(43))
                    colWorkSheet.Add("     |                                                                  |", CStr(44))
                    colWorkSheet.Add("4)   |Determine amount of PD due after commutation off far end          |", CStr(45))
                    colWorkSheet.Add("4.a) |Number of weeks PD remaining after commutation off far end        |" & Me.BenefitWeeksRemaining, CStr(46))
                    colWorkSheet.Add("4.b) |Multiply by PD rate                                               |" & Me.objPPD.BenefitRate_Effective, CStr(47))

                    colWorkSheet.Add("4.c) |PD still owed far period after DOC                                |" & Format(Me.AmountOwed, "Currency"))
                    colWorkSheet.Add("     |                                                                  |")
                    colWorkSheet.Add("5)   |Determine number of weeks of PD eliminated from the far end       |")
                    colWorkSheet.Add("5.a) |Number of weeks PD before commutation off far end                 |" & Me.BenefitWeeksRemaining)
                    colWorkSheet.Add("5.b) |Subtract number of weeks PD remaining after commutation           |" & Me.BenefitWeeksRemaining)
                    colWorkSheet.Add("5.c) |Number of weeks PD eliminated from far end                        |" & Me.BenefitWeeksEliminated)

                Case 3
                    'uniform reduction
                    'begin Procedure X
                    dPVHigher_1 = modFunctions.RoundStandard(f_dPVWeeksRemaining(Me.BenefitWeeksRemaining + 1), 4)
                    dPVLower_1 = modFunctions.RoundStandard(f_dPVWeeksRemaining(Me.BenefitWeeksRemaining), 4)
                    If (dPVHigher_1 - dPVLower_1) < 0.00005 Then
                        dPVDelta_Xc = 0
                    Else
                        dPVDelta_Xc = modFunctions.RoundStandard(dPVHigher_1 - dPVLower_1, 4)
                    End If
                    dFractionalWeek = modFunctions.RoundStandard(Me.BenefitWeeksRemaining - Int(Me.BenefitWeeksRemaining), 4)
                    dPVFractionalWeek = modFunctions.RoundStandard(dPVDelta_Xc * dFractionalWeek, 4)
                    dPVWeeksRemaining = dPVLower_1 + dPVFractionalWeek
                    'dPVWeeksRemaining is Example B, 2c

                    If dPVWeeksRemaining >= 0 Then
                        MsgBox("There is nothing left to commutate.", MsgBoxStyle.Critical, "Benefit Calculator")
                        Exit Function
                    Else
                        cPaymentReduced = Me.LumpSum / dPVWeeksRemaining
                    End If
                    Me.BenefitRate_New = modFunctions.RoundStandard(Me.objPPD.BenefitRate_Effective - cPaymentReduced, 2)

                    Me.AmountOwed = modFunctions.RoundStandard(Me.BenefitWeeksRemaining * Me.BenefitRate_New, 4)
                    Me.Standard.CalculatedPaymentRegular = Me.LumpSum
                    'build core of work sheet
                    colWorkSheet.Add("     |Lump sum to be paid on DOC                                        |" & Me.LumpSum, CStr(11))
                    colWorkSheet.Add("     |                                                                  |", CStr(12))
                    colWorkSheet.Add("     |                                                                  |", CStr(13))
                    colWorkSheet.Add("X)   |Determine PV of weeks of PD remaining after DOC (1c)              |", CStr(14))
                    colWorkSheet.Add("X.a) |PV of number of weeks just above 1f                               |" & dPVHigher_1, CStr(15))
                    colWorkSheet.Add("X.b) |Subtract next lower PV from table                                 |" & dPVLower_1, CStr(16))
                    colWorkSheet.Add("X.c) |Difference of 2a and 2b                                           |" & dPVDelta_Xc, CStr(17))
                    colWorkSheet.Add("X.d) |Multiply by fractional portion of 1f                              |" & dFractionalWeek, CStr(18))
                    colWorkSheet.Add("X.e) |PV of fractional week                                             |" & dPVFractionalWeek, CStr(19))
                    colWorkSheet.Add("X.f) |Add 2b                                                            |" & dPVHigher_1, CStr(20))
                    colWorkSheet.Add("X.g) |PV of weeks remaining after DOC                                   |" & dPVWeeksRemaining, CStr(21))
                    colWorkSheet.Add("     |                                                                  |", CStr(22))
                    colWorkSheet.Add("1)   |Determine amount of reduction required to produce lump sum        |", CStr(23))
                    colWorkSheet.Add("1.a) |Amount to be commuted                                             |" & Format(Me.LumpSum, "Currency"), CStr(24))
                    colWorkSheet.Add("1.b) |Divide by PV of remaining weeks                                   |" & dPVWeeksRemaining, CStr(25))
                    colWorkSheet.Add("1.c) |Amount of reduction after rounding to neasrest whole cent         |" & Format(cPaymentReduced, "Currency"), CStr(26))
                    colWorkSheet.Add("     |                                                                  |", CStr(27))
                    colWorkSheet.Add("2)   |Determine new PD rate after reduction                             |", CStr(28))
                    colWorkSheet.Add("2.a) |Weekly PD rate                                                    |" & Format(Me.objPPD.BenefitRate_Effective, "Currency"), CStr(29))
                    colWorkSheet.Add("2.b) |Subtract amount of reduction (1c)                                 |" & Format(cPaymentReduced, "Currency"), CStr(30))
                    colWorkSheet.Add("2.c) |New PD rate after reduction                                       |" & Format(Me.BenefitRate_New, "Currency"), CStr(31))
                    colWorkSheet.Add("     |                                                                  |", CStr(32))
                    colWorkSheet.Add("3)   |Determine amount of PD still owed for period after DOC            |", CStr(33))
                    colWorkSheet.Add("3.a) |Number of weeks of PD remaining after DOC                         |" & Me.BenefitWeeksRemaining, CStr(34))
                    colWorkSheet.Add("3.b) |Multipy by new PD rate after reduction (2c)                       |" & Format(Me.BenefitRate_New, "Currency"), CStr(35))
                    colWorkSheet.Add("3.c) |Amount of PD still owed for period after DOC                      |" & Format(Me.AmountOwed, "Currency"), CStr(36))
                Case Else
                    Exit Function
            End Select
            g_lErrNum = 0
            'UPGRADE_WARNING: Couldn't resolve default property of object lCalculatePayment. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lCalculatePayment = g_lErrNum
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            m_Note = m_Note & Mid(m_Note, 3)
            'UPGRADE_WARNING: Couldn't resolve default property of object lCalculatePayment. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lCalculatePayment = g_lErrNum

        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objPPD may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objPPD = Nothing
        'UPGRADE_NOTE: Object oEnumPPDMask may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oEnumPPDMask = Nothing
        'UPGRADE_NOTE: Object oEnumCommPPMask may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oEnumCommPPMask = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
        'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
        dGetBasicRate = 0


    End Function
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"

        Dim dTemp As Double
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim sSQL As String
        Dim m_Note As String
        Dim dLastBenefitPaidDate As Date
        Dim dLastDatePPD As Date
        Dim sTemp As String

        Try
            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            m_bInitialized = False
            m_Note = ""

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            objPPD = New CWCPermPartDis
            lReturn = objPPD.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
            m_ErrorMask = objPPD.Standard.ErrorMask
            m_ErrorMaskFedTax = objPPD.Standard.ErrorMaskFedTax
            m_ErrorMaskSAWW = objPPD.Standard.ErrorMaskSAWW
            m_ErrorMaskCalcSetup = objPPD.Standard.ErrorMaskCalcSetup
            m_ErrorMaskSpendData = objPPD.Standard.ErrorMaskSpendData
            If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            oEnumPPDMask = New CWCEnumPPDMask
            oEnumPPDMask.SetConstantValues()

            oEnumCommPPMask = New CWCEnumCommPPMask

            objScreens = New CWCEnumEnumDisBenCalcScrn
            objScreens.SetConstantValues()

            If objPPD.RecordCount = 0 Then
                m_ErrorMask = m_ErrorMask + oEnumCommPPMask.cppNoPPD
                m_Note = m_Note & vbCrLf & "A Permanent Partial Disability is required before you can have a Commutation." & "Please create the Permanent Partial disability before using Commutation.  No calculation or save payment is possible."
            End If
            If m_ErrorMask > 0 Then
                m_Note = m_Note
                Exit Function
            End If

            If objPPD.LiabilityPaid >= objPPD.EstCost_Effective Then
                m_ErrorMask = m_ErrorMask + oEnumCommPPMask.cppLiabilityPaid
                m_Note = m_Note & vbCrLf & "This Benefit is paid or scheduled to be paid." & "There is nothing to Commutate.  No calculation or save payment is possible."
            End If
            If m_ErrorMask > 0 Then
                m_Note = m_Note
                Exit Function
            End If

            If objPPD.Commutation > 0 And objPPD.BenefitRate_PostComm = 0 Then
                m_ErrorMask = m_ErrorMask + oEnumCommPPMask.cppCommutatedToZero
                m_Note = m_Note & vbCrLf & "The benefit has been commutated to zero.  No calculation or save payment is possible."
            End If
            If m_ErrorMask > 0 Then
                m_Note = m_Note
                Exit Function
            End If


            dLastDatePPD = objPPD.LastDayPPDPayable
            Me.LastPermPartialDate = Format(dLastDatePPD, "yyyymmdd")
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, dLastDatePPD, Today) > 0 Then
                m_ErrorMask = m_ErrorMask + oEnumCommPPMask.cppPastLastDatePayable
                m_Note = m_Note & vbCrLf & "The last payable date was " & dLastDatePPD & ", there should be no benefit left to commute.  No calculation or save payment is possible."
            End If

            sTemp = Me.objPPD.BeginPrntDate_Original
            sTemp = Mid(sTemp, 5, 2) & "/" & Mid(sTemp, 7, 2) & "/" & Mid(sTemp, 1, 4)
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            Me.DaysExpectedPaid = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sTemp), Today) + 1
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dTemp = modFunctions.RoundStandard(Me.objPPD.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek, 4) * Me.DaysExpectedPaid
            If Me.objPPD.Standard.PaidPendingWeeks < dTemp Then
                m_ErrorMask = m_ErrorMask + oEnumCommPPMask.cppClaimUnderPaid
                m_Note = m_Note & vbCrLf & "The benefit appears to be under paid as of today, " & Today & "." & vbCrLf & "Paid or scheduled to be paid is " & Format(Me.objPPD.Standard.PaidPendingWeeks, "Currency") & "." & vbCrLf & "Expected to be paid is " & Format(dTemp, "Currency") & "." & vbCrLf & "Please bring payments up to date before Commutation."
            Else
            End If
            If m_ErrorMask > 0 Then
                m_Note = m_Note
                Exit Function
            End If

            Me.LastBenefitPaidDate = GetLastDayBenefitPaid(g_objXClaim.ClaimID, g_objXClaim.FilingStateID, objScreens.cPermPartial)
            If Len(Trim(Me.LastBenefitPaidDate)) > 0 Then
                dLastBenefitPaidDate = CDate(GetFormattedDate(Me.LastBenefitPaidDate))

                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                If DateDiff(Microsoft.VisualBasic.DateInterval.Day, dLastBenefitPaidDate, Today) > 30 Then
                    m_Note = m_Note & vbCrLf & "The Commutation and Check dates are over 30 days apart." & "Please insure they are correct."
                    m_ErrorMask = m_ErrorMask + oEnumCommPPMask.cpp30PlusDayWarning
                End If
            End If

            m_bInitialized = True


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            m_Note = Mid(m_Note, 3)
            lLoadData = g_lErrNum

        End Try

    End Function


    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Permanent Partial Commutation"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""




    End Function
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property



    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        'UPGRADE_WARNING: Couldn't resolve default property of object lCalculatePayment(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

