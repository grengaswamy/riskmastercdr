Option Strict Off
Option Explicit On
Imports Riskmaster.Db

Public Class CWCWCPClaim
    Const sClassName As String = "CWCWCPClaim"
    Private m_AttyFeeCode As Integer
    Private m_AttyFeePercent As Double
    Private m_BeginPrintDateEffective As String
    Private m_BeginPrintDateOriginal As String
    Private m_BenefitType As String
    Private m_BenefitRateEffective As Double
    Private m_BenefitRateOriginal As Double
    Private m_ClaimID As Integer
    Private m_Commutation As Double
    Private m_DisabilityRateEffective As Double
    Private m_DisabilityRateOriginal As Double
    Private m_EndRecvdDateEffective As String
    Private m_EndRecvdDateOriginal As String
    Private m_EstCostEffective As Double
    Private m_EstCostOriginal As Double
    Private m_FormID As Integer
    Private m_IncludePaidToDateCode As Integer

    Private m_Paid As Double
    Private m_RowID As Integer
    Private m_WeeksPayableEffective As Double
    Private m_WeeksPayableOriginal As Double

    Private Function AssignData(ByRef sSQL As String) As Integer
        Const sFunctionName As String = "AssignData"
        Dim dTest As Double
        Dim objReader As DbReader
        Try

            AssignData = 0
            'jtodd22 no-no-----Clearobject
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_AttyFeeCode = objReader.GetInt("ATTY_FEE_CODE")
                m_AttyFeePercent = objReader.GetInt("ATTY_FEE_PERCENT")
                m_BeginPrintDateOriginal = CStr(objReader.GetString("BEGIN_PRNT_DATE"))  'RMA-1046 Issue Fixed
                m_BeginPrintDateEffective = m_BeginPrintDateOriginal
                m_BenefitType = objReader.GetString("BENEFIT_TYPE")
                m_BenefitRateOriginal = objReader.GetDouble("BENEFIT_RATE")
                m_BenefitRateEffective = m_BenefitRateOriginal
                m_ClaimID = objReader.GetInt("CLAIM_ID")
                m_Commutation = objReader.GetInt("COMMUTATION")
                m_DisabilityRateOriginal = objReader.GetDouble("DISABILITY_RATE")
                m_DisabilityRateEffective = m_DisabilityRateOriginal
                m_EndRecvdDateOriginal = objReader.GetString("END_RECVD_DATE")
                m_EndRecvdDateEffective = m_EndRecvdDateOriginal
                m_EstCostOriginal = objReader.GetInt("EST_COST")
                m_EstCostEffective = m_EstCostOriginal
                m_FormID = objReader.GetInt("FORM_ID")
                m_IncludePaidToDateCode = objReader.GetInt("INCLUDEPAID_CODE")
                m_Paid = objReader.GetInt("PAID")
                m_RowID = objReader.GetInt("ROW_ID")
                m_WeeksPayableOriginal = objReader.GetInt("WEEKS_PAYABLE")

                m_WeeksPayableEffective = m_WeeksPayableOriginal

                'dTest = objReader.GetInt32( "")

            Else
                m_RowID = 0
            End If
            AssignData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            AssignData = Err.Number
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Property AttyFeeCode() As Integer
        Get
            AttyFeeCode = m_AttyFeeCode
        End Get
        Set(ByVal Value As Integer)
            m_AttyFeeCode = Value
        End Set
    End Property

    Public Property AttyFeePercent() As Double
        Get
            AttyFeePercent = m_AttyFeePercent
        End Get
        Set(ByVal Value As Double)
            m_AttyFeePercent = Value
        End Set
    End Property

    Public Property ClaimID() As Integer
        Get
            ClaimID = m_ClaimID
        End Get
        Set(ByVal Value As Integer)
            m_ClaimID = Value
        End Set
    End Property

    Public Property Commutation() As Double
        Get
            Commutation = m_Commutation
        End Get
        Set(ByVal Value As Double)
            m_Commutation = Value
        End Set
    End Property

    Public Property BenefitType() As String
        Get
            BenefitType = m_BenefitType
        End Get
        Set(ByVal Value As String)
            m_BenefitType = Value
        End Set
    End Property

    Public Property BenefitRateEffective() As Double
        Get
            BenefitRateEffective = m_BenefitRateEffective
        End Get
        Set(ByVal Value As Double)
            m_BenefitRateEffective = Value
        End Set
    End Property

    Public Property BenefitRateOriginal() As Double
        Get
            BenefitRateOriginal = m_BenefitRateOriginal
        End Get
        Set(ByVal Value As Double)
            m_BenefitRateOriginal = Value
        End Set
    End Property

    Public Property DisabilityRateEffective() As Double
        Get
            DisabilityRateEffective = m_DisabilityRateEffective
        End Get
        Set(ByVal Value As Double)
            m_DisabilityRateEffective = Value
        End Set
    End Property

    Public Property DisabilityRateOriginal() As Double
        Get
            DisabilityRateOriginal = m_DisabilityRateOriginal
        End Get
        Set(ByVal Value As Double)
            m_DisabilityRateOriginal = Value
        End Set
    End Property

    Public Property FormID() As Integer
        Get
            FormID = m_FormID
        End Get
        Set(ByVal Value As Integer)
            m_FormID = Value
        End Set
    End Property

    Public Property BeginPrintDateEffective() As String
        Get
            BeginPrintDateEffective = m_BeginPrintDateEffective
        End Get
        Set(ByVal Value As String)
            m_BeginPrintDateEffective = Value
        End Set
    End Property

    Public Property BeginPrintDateOriginal() As String
        Get
            BeginPrintDateOriginal = m_BeginPrintDateOriginal
        End Get
        Set(ByVal Value As String)
            m_BeginPrintDateOriginal = Value
        End Set
    End Property


    Public Property EndRecvdDateEffective() As String
        Get
            EndRecvdDateEffective = m_EndRecvdDateEffective
        End Get
        Set(ByVal Value As String)
            m_EndRecvdDateEffective = Value
        End Set
    End Property


    Public Property EndRecvdDateOriginal() As String
        Get
            EndRecvdDateOriginal = m_EndRecvdDateOriginal
        End Get
        Set(ByVal Value As String)
            m_EndRecvdDateOriginal = Value
        End Set
    End Property

    Public Property EstCostEffective() As Double
        Get
            EstCostEffective = m_EstCostEffective
        End Get
        Set(ByVal Value As Double)
            m_EstCostEffective = Value
        End Set
    End Property

    Public Property EstCostOriginal() As Double
        Get
            EstCostOriginal = m_EstCostOriginal
        End Get
        Set(ByVal Value As Double)
            m_EstCostOriginal = Value
        End Set
    End Property

    Public Property Paid() As Double
        Get
            Paid = m_Paid
        End Get
        Set(ByVal Value As Double)
            m_Paid = Value
        End Set
    End Property

    Public Property RowID() As Integer
        Get
            RowID = m_RowID
        End Get
        Set(ByVal Value As Integer)
            m_RowID = Value
        End Set
    End Property

    Public Property WeeksPayableEffective() As Double
        Get
            WeeksPayableEffective = m_WeeksPayableEffective
        End Get
        Set(ByVal Value As Double)
            m_WeeksPayableEffective = Value
        End Set
    End Property

    Public Property WeeksPayableOriginal() As Double
        Get
            WeeksPayableOriginal = m_WeeksPayableOriginal
        End Get
        Set(ByVal Value As Double)
            m_WeeksPayableOriginal = Value
        End Set
    End Property


    Public Property IncludePaidToDateCode() As Integer
        Get

            IncludePaidToDateCode = m_IncludePaidToDateCode

        End Get
        Set(ByVal Value As Integer)

            m_IncludePaidToDateCode = Value

        End Set
    End Property
    '***Permanent Partial Commutations do not have a separate record in WCP_CLAIMS
    '***Life Pension Commutations do not have a separate record in WCP_CLAIMS
    '***
    '***read one record--find the most current record, the one without a commutation
    '***update one record--update the most current record with commutation related changes
    '***append one record--update with the new start date, benefit rate....
    Public Function f_bUpdateBenefitRecordAfterCommutation(ByRef sBenefitType As String, ByRef lClaimID As Integer, ByRef dblBenefitRate_New As Double, ByRef dCommutationDate As Date, ByRef dblPayment As Double, Optional ByRef dblRemainingWeeks As Double = 0) As Boolean
        Const sFunctionName As String = "f_bUpdateBenefitRecordAfterCommutation"


        Dim dblBenefitRate As Double
        Dim dblDisabilityRate As Double
        Dim dblEstCost As Double
        Dim lRowID As Integer
        Dim sNow As String
        Dim sSQL As String
        Dim objReader As DbReader
        Dim sCommutationDate As String
        Dim d As Date
        Try
            f_bUpdateBenefitRecordAfterCommutation = False

            sNow = Format(Now, "yyyymmddhhmmss")
            sCommutationDate = Format(dCommutationDate, "yyyymmdd")

            sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & lClaimID & " AND BENEFIT_TYPE = '" & sBenefitType & "'" & " ORDER BY ROW_ID DESC"


            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                lRowID = objReader.GetInt32("ROW_ID")
                dblDisabilityRate = objReader.GetInt32("DISABILITY_RATE")
                dblBenefitRate = objReader.GetInt32("BENEFIT_RATE")
                dblEstCost = modFunctions.RoundStandard(objReader.GetInt32("EST_COST"), 2)

                objReader.Close()

                sSQL = "SELECT * FROM WCP_CLAIMS WHERE ROW_ID = " & lRowID
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

                Dim objWriter As DbWriter = DbFactory.GetDbWriter(objReader, True)
                objReader.Close()

                If StrComp("PPD", sBenefitType, CompareMethod.Text) = 0 Then
                    If dblEstCost = 0 Then
                        dblEstCost = modFunctions.RoundStandard(dblBenefitRate * objReader.GetInt32("WEEKS_PAYABLE"), 2)
                        objWriter.Fields("EST_COST").Value = dblEstCost
                    End If
                End If
                objWriter.Fields("END_RECVD_DATE").Value = sCommutationDate
                objWriter.Fields("COMMUTATION").Value = dblPayment
                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = sNow
                objWriter.Execute()

                objWriter = Nothing

                lRowID = lGetNextUID("WCP_CLAIMS")
                sSQL = "SELECT * FROM WCP_CLAIMS WHERE ROW_ID = -1"
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objReader.Close()

                d = DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, dCommutationDate)
                sCommutationDate = Format(d, "YYYYMMDD")

                objWriter.Fields.Add("CLAIM_ID", lClaimID)
                objWriter.Fields.Add("ROW_ID", lRowID)
                objWriter.Fields.Add("DISABILITY_RATE", dblDisabilityRate)
                objWriter.Fields.Add("BENEFIT_TYPE", sBenefitType)
                objWriter.Fields.Add("BENEFIT_RATE", dblBenefitRate_New)
                objWriter.Fields.Add("BEGIN_PRNT_DATE", sCommutationDate)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", sNow)
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", sNow)

                If StrComp("PPD", sBenefitType, CompareMethod.Text) = 0 Then
                    dblEstCost = modFunctions.RoundStandard(dblBenefitRate_New * dblRemainingWeeks, 2)
                    objWriter.Fields.Add("EST_COST", dblEstCost)
                    objWriter.Fields.Add("WEEKS_PAYABLE", dblRemainingWeeks)
                End If
                objWriter.Execute()
            Else
                Err.Raise(7000, "", "Error executing f_bUpdateBenefitRecordAfterCommutation, unable to find original record.")
            End If

            f_bUpdateBenefitRecordAfterCommutation = True

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function LoadDataByBeginDate(ByRef sBenefitTypeAbbr As String, ByRef sSQL As String) As Integer
        Const sFunctionName As String = "LoadDataByBeginDate"


        '''stub
        '''stub
        '''    LoadDataByBeginDate = AssignData(sSQL)


    End Function
    Public Function LoadDataByRowID(ByRef lRowID As Integer, ByRef sBenefitTypeAbbr As String) As Integer
        Const sFunctionName As String = "LoadDataByRowID"
        Dim sSQL As String
        Dim objReader As DbReader
        Try
            LoadDataByRowID = 0

            sSQL = ""
            sSQL = sSQL & "SELECT *"
            sSQL = sSQL & " FROM WCP_CLAIMS"
            sSQL = sSQL & " WHERE BENEFIT_TYPE = '" & sBenefitTypeAbbr & "'"
            sSQL = sSQL & " AND CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND ROW_ID = " & lRowID

            LoadDataByRowID = AssignData(sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LoadDataByRowID = Err.Number
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function LoadDataLastRecordByRowID(ByRef sBenefitType As String) As Integer
        Const sFunctionName As String = "LoadDataLastRecordByRowID"
        Try

            Dim sSQL As String
            Dim sSQL2 As String

            LoadDataLastRecordByRowID = 0

            sBenefitType = UCase(sBenefitType)

            'jtodd22 no-no-----Clearobject

            sSQL = ""
            sSQL = sSQL & "SELECT MAX(ROW_ID)"
            sSQL = sSQL & " FROM WCP_CLAIMS"
            sSQL = sSQL & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND BENEFIT_TYPE = '" & sBenefitType & "'"

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT *"
            sSQL2 = sSQL2 & " FROM WCP_CLAIMS"
            sSQL2 = sSQL2 & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL2 = sSQL2 & " AND BENEFIT_TYPE = '" & sBenefitType & "'"
            sSQL2 = sSQL2 & " AND ROW_ID = (" & sSQL & ")"

            LoadDataLastRecordByRowID = AssignData(sSQL2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LoadDataLastRecordByRowID = Err.Number
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function LoadDataOriginalRecord(ByRef sSQL As String) As Integer
        Const sFunctionName As String = "LoadDataOriginalRecord"
        Dim objReader As DbReader
        Try
            LoadDataOriginalRecord = 0
            'Clearobject
            LoadDataOriginalRecord = AssignData(sSQL)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LoadDataOriginalRecord = Err.Number
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function SaveDataOriginalRecord() As Integer
        Const sFunctionName As String = "SaveDataOriginalRecord"

        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim sNow As String
        Dim sSQL As String
        Dim dTest As String

        Try
            SaveDataOriginalRecord = 0
            sNow = Format(Now, "YYYYMMDDHHMMSS")
            m_BenefitType = UCase(m_BenefitType)

            sSQL = ""
            sSQL = sSQL & "SELECT * FROM WCP_CLAIMS"
            sSQL = sSQL & " WHERE ROW_ID = " & m_RowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            'Add/Update '##Mukesh


            If (objReader.Read()) Then
                objWriter = DbFactory.GetDbWriter(objReader, True)
                objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                objWriter.Fields("DTTM_RCD_LAST_UPD").Value = sNow
                objWriter.Fields("ATTY_FEE_CODE").Value = m_AttyFeeCode
                objWriter.Fields("ATTY_FEE_PERCENT").Value = m_AttyFeePercent
                objWriter.Fields("BENEFIT_RATE").Value = m_BenefitRateEffective
                objWriter.Fields("BENEFIT_TYPE").Value = m_BenefitType
                objWriter.Fields("BEGIN_PRNT_DATE").Value = m_BeginPrintDateEffective
                objWriter.Fields("COMMUTATION").Value = m_Commutation
                objWriter.Fields("DISABILITY_RATE").Value = m_DisabilityRateEffective
                objWriter.Fields("END_RECVD_DATE").Value = m_EndRecvdDateEffective

                dTest = CStr(modFunctions.RoundStandard(m_WeeksPayableEffective * m_BenefitRateEffective, 2))
                If CDbl(dTest) > 0 Then
                    objWriter.Fields("EST_COST").Value = modFunctions.RoundStandard(m_WeeksPayableEffective * m_BenefitRateEffective, 2)
                Else
                    If m_EstCostOriginal > 0 Then
                        objWriter.Fields("EST_COST").Value = m_EstCostOriginal
                    Else
                        objWriter.Fields("EST_COST").Value = m_EstCostEffective
                    End If
                End If
                objWriter.Fields("FORM_ID").Value = m_FormID
                objWriter.Fields("INCLUDEPAID_CODE").Value = m_IncludePaidToDateCode
                objWriter.Fields("PAID").Value = m_Paid
                objWriter.Fields("WEEKS_PAYABLE").Value = m_WeeksPayableEffective
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)
                objWriter.Fields.Add("CLAIM_ID", g_objXClaim.ClaimID)
                objWriter.Fields.Add("ROW_ID", m_RowID)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", sNow)

                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", sNow)
                objWriter.Fields.Add("ATTY_FEE_CODE", m_AttyFeeCode)
                objWriter.Fields.Add("ATTY_FEE_PERCENT", m_AttyFeePercent)
                objWriter.Fields.Add("BENEFIT_RATE", m_BenefitRateEffective)
                objWriter.Fields.Add("BENEFIT_TYPE", m_BenefitType)
                objWriter.Fields.Add("BEGIN_PRNT_DATE", m_BeginPrintDateEffective)
                objWriter.Fields.Add("COMMUTATION", m_Commutation)
                objWriter.Fields.Add("DISABILITY_RATE", m_DisabilityRateEffective)
                objWriter.Fields.Add("END_RECVD_DATE", m_EndRecvdDateEffective)
                dTest = CStr(modFunctions.RoundStandard(m_WeeksPayableEffective * m_BenefitRateEffective, 2))
                If CDbl(dTest) > 0 Then
                    objWriter.Fields.Add("EST_COST", modFunctions.RoundStandard(m_WeeksPayableEffective * m_BenefitRateEffective, 2))
                Else
                    If m_EstCostOriginal > 0 Then
                        objWriter.Fields.Add("EST_COST", m_EstCostOriginal)
                    Else
                        objWriter.Fields.Add("EST_COST", m_EstCostEffective)
                    End If
                End If
                objWriter.Fields.Add("FORM_ID", m_FormID)
                objWriter.Fields.Add("INCLUDEPAID_CODE", m_IncludePaidToDateCode)
                objWriter.Fields.Add("PAID", m_Paid)
                objWriter.Fields.Add("WEEKS_PAYABLE", m_WeeksPayableEffective)
            End If
            objWriter.Execute()
            SaveDataOriginalRecord = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            SaveDataOriginalRecord = Err.Number
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function GetExistingRowID(ByRef sSQL As String) As Integer
        Const sFunctionName As String = "GetExistingRowID"

        Dim objReader As DbReader
        Try
            GetExistingRowID = 0
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetExistingRowID = objReader.GetInt32("ROW_ID")
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function GetRowIDMaximum(ByVal sBenefitTypeAbbr As String) As Integer
        Const sFunctionName As String = "GetRowIDMaximum"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetRowIDMaximum = 0

            sSQL = ""
            sSQL = sSQL & "SELECT MAX(ROW_ID)"
            sSQL = sSQL & " FROM " & g_sWCPClaimsTableName
            sSQL = sSQL & " WHERE BENEFIT_TYPE = '" & sBenefitTypeAbbr & "'"
            sSQL = sSQL & " AND CLAIM_ID = " & g_objXClaim.ClaimID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetRowIDMaximum = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function GetRowIDMinimum(ByVal sBenefitTypeAbbr As String) As Integer
        Const sFunctionName As String = "GetRowIDMinimum"
        Dim objReader As DbReader
        Dim sSQL As String

        Try

            GetRowIDMinimum = 0

            sSQL = ""
            sSQL = sSQL & "SELECT MIN(ROW_ID)"
            sSQL = sSQL & " FROM " & g_sWCPClaimsTableName
            sSQL = sSQL & " WHERE BENEFIT_TYPE = '" & sBenefitTypeAbbr & "'"
            sSQL = sSQL & " AND CLAIM_ID = " & g_objXClaim.ClaimID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetRowIDMinimum = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function
End Class

