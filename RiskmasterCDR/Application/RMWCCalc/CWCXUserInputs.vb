Option Strict Off
Option Explicit On
Public Class CWCXUserInputs
    Const sClassName As String = "CWCXUserInputs"
    '               111111111122222
    '      123456789012345678901234
    Public BenefitTypeText As String
    Public Earnings As Double
    Public FirstPaymentDateCalendar As String
    Public FirstPaymentDateDBFormat As String
    Public LastPaymentDateCalendar As String
    Public LastPaymentDateDBFormat As String
    Public NumberOfTimeUnits As Integer
    Public NumberOfPayments As Integer
    Public PaymentPeriodText As String
    Public WaitFirstDateCalendar As String
    Public WaitFirstDateDBFormat As String
    Public WaitLastDateCalendar As String
    Public WaitLastDateDBFormat As String
    Public WaitNumberOfTimeUnits As Integer
    Public WaitNumberOfPayments As Integer
    Public Warning As String
End Class

