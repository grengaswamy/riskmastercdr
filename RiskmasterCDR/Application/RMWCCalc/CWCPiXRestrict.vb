Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPiXRestrict


    Const sClassName As String = "CWCPIXRestrict"
    Const m_TableName As String = "PI_X_RESTRICT"

    ' Object properties
    Private m_DateFirstRestrct As String
    Private m_DateLastRestrct As String
    Private m_Duration As Integer
    Private m_PercentDisabled As Integer
    Private m_PiRestrictRowId As Integer
    Private m_PiRowId As Integer
    Private m_PositionCode As Integer

    Public Property PiRestrictRowId() As Integer
        Get
            PiRestrictRowId = m_PiRestrictRowId
        End Get
        Set(ByVal Value As Integer)
            m_PiRestrictRowId = Value
        End Set
    End Property

    Public Property PiRowId() As Integer
        Get
            PiRowId = m_PiRowId
        End Get
        Set(ByVal Value As Integer)
            m_PiRowId = Value
        End Set
    End Property

    Public Property DateFirstRestrct() As String
        Get
            DateFirstRestrct = m_DateFirstRestrct
        End Get
        Set(ByVal Value As String)
            m_DateFirstRestrct = Value
        End Set
    End Property

    Public Property PercentDisabled() As Integer
        Get
            PercentDisabled = m_PercentDisabled
        End Get
        Set(ByVal Value As Integer)
            m_PercentDisabled = Value
        End Set
    End Property

    Public Property DateLastRestrct() As String
        Get
            DateLastRestrct = m_DateLastRestrct
        End Get
        Set(ByVal Value As String)
            m_DateLastRestrct = Value
        End Set
    End Property

    Public Property Duration() As Integer
        Get
            Duration = m_Duration
        End Get
        Set(ByVal Value As Integer)
            m_Duration = Value
        End Set
    End Property

    Public Property PositionCode() As Integer
        Get
            PositionCode = m_PositionCode
        End Get
        Set(ByVal Value As Integer)
            m_PositionCode = Value
        End Set
    End Property
    Public Sub ClearObject()
        m_PiRestrictRowId = 0
        'm_PiRowId = 0
        m_DateFirstRestrct = ""
        m_PercentDisabled = 0
        m_DateLastRestrct = ""
        m_Duration = 0
        m_PositionCode = 0


    End Sub

    'Private Function LoadData(ByRef PiRestrictRowId As Integer) As Integer
    '	Const sFunctionName As String = "lLoadData"
    '	Dim sSQL As String
    '       Dim objReader As dbReader

    '       Try

    '           m_PiRestrictRowId = objReader.GetInt32("PI_RESTRICT_ROW_ID")
    '           m_PiRowId = objReader.GetInt32("PI_ROW_ID")
    '           m_DateFirstRestrct = objReader.GetString("DATE_FIRST_RESTRCT")
    '           m_PercentDisabled = objReader.GetInt32("PERCENT_DISABLED")
    '           m_DateLastRestrct = objReader.GetString("DATE_LAST_RESTRCT")
    '           m_Duration = objReader.GetInt32("DURATION")
    '           m_PositionCode = objReader.GetInt32("POSITION_CODE")
    '           g_DBObject.DB_CloseRecordset(iRdSet, ROCKETCOMLib.DTGDBCloseOptions.DB_DROP)

    '       Catch ex As Exception
    '           With Err
    '               g_lErrNum = Err.Number
    '               g_sErrSrc = .Source
    '               g_sErrDescription = Err.Description
    '           End With
    '           LoadData = g_lErrNum
    '           g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
    '           g_lErrLine = Erl()
    '           LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '           Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

    '       Finally

    '       End Try

    'End Function
End Class

