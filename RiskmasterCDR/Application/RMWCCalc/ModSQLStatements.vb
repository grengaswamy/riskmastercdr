Option Strict Off
Option Explicit On
Module ModSQLStatements
	Const sModuleName As String = "modSQLStatements"
	'Public Type p_BenefitDetail
	'    BenefitRate_Effective As Double
	'    BenefitRate_Original As Double
	'    BenefitRate_PostComm As Double
	'    Estimated_Cost_Effective As Double
	'    Estimated_Cost_Original As Double
	'    Estimated_Cost_PostComm As Double
	'    EndDateDTGDate_Original As String
	'    EndDateDTGDate_PostComm As String
	'    ImpairmentRate As Double
	'    RecordCount As Long
	'    StartDateDTGDate_Effective As String
	'    StartDateDTGDate_Original As String
	'    StartDateDTGDate_PostComm As String
	'    TotalDaysOfPPDLiability As Long
	'    VocRehabDaysAdvance As Long
	'    VocReHabSuppDaysDelay As Long
	'    WeeksLiable_Effective As Double
	'    WeeksLiable_Original As Double
	'    WeeksLiable_PostComm As Double
	'End Type
	
	'Public Type p_BenefitCheatSheet
	'    AWWClaimant As Double
	'    AWWEffective As Double
	'    AWWJurisDictional As Double
	'    BenefitRate_Effective_LP As Double
	'    BenefitRate_Effective_PPD As Double
	'    BenefitRate_Effective_Total As Double
	'    BenefitRate_Life_Max As Double
	'    BenefitRate_Life_Min As Double
	'    BenefitRate_PPD_Max As Double
	'    BenefitRate_PPD_Min As Double
	'    BenefitRate_Total_Max As Double
	'    BenefitRate_Total_Min As Double
	'    DateOfEventDTGDate As String
	'    FilingJurisdictionID As Long
	'    ImpairmentRate As Double
	'    JurisdictionalMultipler As Double
	'    StartDateDTGDate_Effective_LP As String
	'    StartDateDTGDate_Effective_PPD As String
	'    TotalDaysOfPPDLiability As Long
	'    VocRehabDaysAdvance As Long
	'    VocReHabSuppDaysDelay As Long
	'
	'End Type
	Public Function SelectClaimDataByClaimIDByBenefitTypeWithOrderBy(ByVal sBenefitType As String, ByVal sSortOrder As String) As String
		Dim sSQL As String
		
		sSortOrder = Trim(UCase(sSortOrder))
		sBenefitType = Trim(UCase(sBenefitType))
		sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
		
		If sBenefitType > "" Then
			sSQL = sSQL & " AND BENEFIT_TYPE = '" & sBenefitType & "'"
		End If
		
		'sSQL = sSQL & " AND ROW_ID > 0"
		
		If sSortOrder > "" Then
			sSQL = sSQL & " ORDER BY ROW_ID " & sSortOrder
		End If
		
		SelectClaimDataByClaimIDByBenefitTypeWithOrderBy = sSQL
		


	End Function
	Public Function SelectClaimDataByRowID(ByVal lRowID As Integer) As String
		Dim sSQL As String
		
		sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE ROW_ID = " & lRowID
		
		SelectClaimDataByRowID = sSQL


	End Function
	Public Function SelectClaimDataByClaimIDByBenefitType(ByVal sBenefitType As String) As String
		Dim sSQL As String
		
		sBenefitType = Trim(UCase(sBenefitType))
		sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID & " AND BENEFIT_TYPE = '" & sBenefitType & "'"
		
		SelectClaimDataByClaimIDByBenefitType = sSQL
		


	End Function
	Public Function SelectClaimDatabyBeginDate(ByVal sBenefitType As String, ByVal sBeginDate_DTGDate As String) As String
		Dim sSQL As String
		Dim sSQL2 As String
		sBenefitType = Trim(UCase(sBenefitType))
		
		sSQL2 = ""
		sSQL2 = sSQL2 & "SELECT MAX(BEGIN_PRNT_DATE)"
		sSQL2 = sSQL2 & " FROM WCP_CLAIMS"
		sSQL2 = sSQL2 & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
		sSQL2 = sSQL2 & " AND BENEFIT_TYPE = 'PTD'"
		
		sSQL = ""
		sSQL = sSQL & "SELECT * FROM WCP_CLAIMS"
		sSQL = sSQL & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
		sSQL = sSQL & " AND BENEFIT_TYPE = '" & sBenefitType & "'"
		sSQL = sSQL & " AND BEGIN_PRNT_DATE = (" & sSQL2 & ")"
		
		SelectClaimDatabyBeginDate = sSQL
		


	End Function
	Public Function SelectRecordCountByBenefit(ByVal sBenefitType As String) As String
		Dim sSQL As String
		
		sBenefitType = Trim(UCase(sBenefitType))
		
		sSQL = "SELECT Count(*) AS L_RECORDS FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
		
		If sBenefitType > "" Then
			sSQL = sSQL & " AND BENEFIT_TYPE = '" & sBenefitType & "'"
		End If
		
        If modGlobals.g_dbMake <> Riskmaster.Db.eDatabaseType.DBMS_IS_ACCESS Then sSQL = Replace(sSQL, " AS ", " ") 'Remove alias As(notneeded except for access)
		
		SelectRecordCountByBenefit = sSQL
		


	End Function
	Public Function SelectPPD_BenefitRateLimits(ByVal dPercentDisability As Double) As String
		Dim sSQL As String
		Dim sSQL2 As String
		
		SelectPPD_BenefitRateLimits = ""
		
		sSQL2 = ""
		sSQL2 = sSQL2 & "SELECT"
		sSQL2 = sSQL2 & " MAX(BEGIN_DATE)"
		sSQL2 = sSQL2 & " FROM WCP_PPD_WEEKS"
		sSQL2 = sSQL2 & " WHERE BEGIN_DATE <= '" & g_objXClaim.DateOfEventDTG & "'"
		
		sSQL = ""
		sSQL = sSQL & "SELECT"
		sSQL = sSQL & " STATE_ROW_ID "
		sSQL = sSQL & " FROM WCP_PPD_LIMITS,BEGIN_DATE,BEGIN_PERCENT, BEGIN_DATE"
		sSQL = sSQL & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID
		sSQL = sSQL & " AND BEGIN_DATE = (" & sSQL2 & ")"
		sSQL = sSQL & " AND " & dPercentDisability & " >= BEGIN_PERCENT"
		sSQL = sSQL & " ORDER BY BEGIN_PERCENT DESC, BEGIN_DATE DESC"
		
		SelectPPD_BenefitRateLimits = sSQL
		'programmer beware;  Oracle does always return records as expected use both fields in ORDER BY clause
		'jlt, 02/28/01


	End Function
	Public Function SelectPPD_BenefitWeeks(ByVal dPercentDisablility As Double) As String
		Dim sSQL As String
		Dim sSQL2 As String
		
		SelectPPD_BenefitWeeks = ""
		
		sSQL2 = ""
		sSQL2 = sSQL2 & "SELECT"
		sSQL2 = sSQL2 & " MAX(BEGIN_DATE)"
		sSQL2 = sSQL2 & " FROM WCP_PPD_WEEKS"
		sSQL2 = sSQL2 & " WHERE BEGIN_DATE <= '" & g_objXClaim.DateOfEventDTG & "'"
		
		sSQL = sSQL
		sSQL = sSQL & "SELECT"
		sSQL = sSQL & " BEGIN_DATE,STATE_ROW_ID,BEGIN_PERCENT,END_PERCENT"
		sSQL = sSQL & " FROM WCP_PPD_WEEKS"
		sSQL = sSQL & " WHERE BEGIN_DATE = (" & sSQL2 & ")"
		sSQL = sSQL & " AND STATE_ROW_ID = " & g_objXClaim.FilingStateID
		sSQL = sSQL & " AND (" & dPercentDisablility & " BETWEEN BEGIN_PERCENT AND END_PERCENT)"
		sSQL = sSQL & " ORDER BY BEGIN_DATE DESC"
		
		SelectPPD_BenefitWeeks = sSQL
		


	End Function
	
	Public Function SelectTransTypeCodeFromMapping(ByVal iScreenID As Short, ByVal sCaption As String) As String
		Dim sSQL As String
		
		sSQL = "SELECT DISTINCT WCP_TRANS_TYPES.SCREEN_ID" & ", WCP_TRANS_TYPES.CODE_ID" & ", CODES.SHORT_CODE" & ", CODES_TEXT.CODE_DESC" & " FROM WCP_TRANS_TYPES, CODES, CODES_TEXT" & " WHERE WCP_TRANS_TYPES.CODE_ID = CODES.CODE_ID" & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID" & " AND WCP_TRANS_TYPES.STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND WCP_TRANS_TYPES.SCREEN_ID = " & iScreenID
		
		SelectTransTypeCodeFromMapping = sSQL


	End Function
	Public Function SelectFundsByTransID(ByVal lTransId As String) As String
		Dim sSQL As String
		sSQL = "SELECT * FROM FUNDS WHERE TRANS_ID = " & lTransId
		SelectFundsByTransID = sSQL


	End Function
	
	Public Function SelectGetLastDayBenefitPaid(ByVal sTransTypeCodes As String) As String
		Dim sSQL As String
		sSQL = "SELECT MAX(TO_DATE) AS LAST_DTGDATE" & " FROM FUNDS_TRANS_SPLIT, FUNDS" & " WHERE FUNDS_TRANS_SPLIT.TRANS_ID = FUNDS.TRANS_ID" & " AND TRANS_TYPE_CODE IN (" & sTransTypeCodes & ")" & " AND CLAIM_ID = " & g_objXClaim.ClaimID
		
		SelectGetLastDayBenefitPaid = sSQL


	End Function
	
	Public Function SelectPaymentHistoryByBenefitSummary(ByVal sDateClause As String, ByVal sTransTypeCodes As String) As String
		Dim sSQL As String
		
		sSQL = "SELECT FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE" & ",SUM(FUNDS_TRANS_SPLIT.AMOUNT) AS S_AMOUNT" & ",FUNDS_TRANS_SPLIT.FROM_DATE" & ",FUNDS_TRANS_SPLIT.TO_DATE" & " FROM FUNDS, FUNDS_TRANS_SPLIT" & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID" & " AND VOID_FLAG = 0" & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodes & ")"
		
		If sDateClause <> "" Then sSQL = sSQL & sDateClause
		
		sSQL = sSQL & " GROUP BY TRANS_TYPE_CODE,FROM_DATE,TO_DATE ORDER BY TO_DATE"
		
        If modGlobals.g_dbMake = Riskmaster.Db.eDatabaseType.DBMS_IS_ACCESS Then
            sSQL = Replace(sSQL, " AS ", " ")
        End If
		
		SelectPaymentHistoryByBenefitSummary = sSQL


	End Function
	Public Function SelectAutoChecksAffected(ByVal sTransTypeCodeIDs As String) As String
		Dim sSQL As String
		SelectAutoChecksAffected = ""
		sSQL = ""
		sSQL = sSQL & "SELECT"
		sSQL = sSQL & " DISTINCT FUNDS_AUTO.AUTO_BATCH_ID"
		sSQL = sSQL & " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT"
		sSQL = sSQL & " WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID"
		sSQL = sSQL & " AND FUNDS_AUTO.CLAIM_ID = " & g_objXClaim.ClaimID
		sSQL = sSQL & " AND FUNDS_AUTO.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
		sSQL = sSQL & " AND FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeIDs & ")"
		
		SelectAutoChecksAffected = sSQL


	End Function
	
	Public Function SelectCheckStatusCodeIds() As String
		Dim sSQL As String
		
		SelectCheckStatusCodeIds = ""
		
		sSQL = ""
		sSQL = sSQL & "SELECT CODES.CODE_ID"
		sSQL = sSQL & " FROM CODES, CODES_TEXT"
		sSQL = sSQL & " WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID"
		sSQL = sSQL & " AND CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'CHECK_STATUS')"
		
		SelectCheckStatusCodeIds = sSQL
		


	End Function
End Module

