Option Strict Off
Option Explicit On
Public Class CWCEnumLPMask
    Const sClassName As String = "CWCEnumLPMask"

    Public clpNoPPD As Short
    Public clpLowDisRate As Short
    Public clpLiabilityNotPaid As Short
    Public clpBenefitRateChanged As Short
    Public clpNoBenefitRate As Short
    Public clpPrematurePayment As Short
    Public clpBadBenefitStartDate As Short

    Public Function SetConstantValues() As Integer
        clpNoPPD = 1
        clpLowDisRate = 2
        clpLiabilityNotPaid = 4
        clpBenefitRateChanged = 8
        clpNoBenefitRate = 16
        clpPrematurePayment = 32
        clpBadBenefitStartDate = 64


    End Function
End Class

