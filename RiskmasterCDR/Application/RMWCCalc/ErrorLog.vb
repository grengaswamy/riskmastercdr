Option Strict Off
Option Explicit On
Module ErrorLog
	Const sModuleName As String = "ErrorLog"
	Public Sub LogError(ByVal sProcedure As String, ByVal ErrLine As Integer, ByRef ErrNumber As Integer, ByRef ErrSource As String, ByRef ErrDescription As String, Optional ByRef bBubbleErr As Boolean = False)
        Dim iFile As Short
        Dim sFileName As String
        Dim sItem As String
        Try
            iFile = FreeFile()

            If Trim(g_sDictPath & "") = "" Then
                'UPGRADE_WARNING: Couldn't resolve default property of object QueryValue(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sItem = QueryValue("SOFTWARE\DTG\RISKMASTER", "InstallationDirectory")
                If Trim(sItem) > "" Then
                    If Right(sItem, 1) = "\" Then
                        sFileName = sItem & "RMDLLError.log"
                    Else
                        sFileName = sItem & "\" & "RMDLLError.log"
                    End If
                Else
                    sFileName = My.Application.Info.DirectoryPath
                    If Right(sFileName, 1) <> "\" Then sFileName = sFileName & "\"
                    sFileName = sFileName & "RMDLLError.log"
                End If
            Else
                sFileName = g_sDictPath
                If Right(sFileName, 1) <> "\" Then sFileName = sFileName & "\"
                sFileName = sFileName & "RMDLLError.log"
            End If

            FileOpen(iFile, sFileName, OpenMode.Append, , OpenShare.Shared)
            PrintLine(iFile, """" & Format(Now, "yyyymmddhhnnss") & """," & """" & sProcedure & """," & """" & ErrLine & """," & """" & ErrNumber & """," & """" & ErrSource & """," & """" & ErrDescription & """,")
            FileClose(iFile)
            iFile = 0

#If BB_DEBUG Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression BB_DEBUG did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		' Note: in this project (Possibly because of the form or the third party control)
		' the setting for "non-interactive" is greyed out.
		' Additionally any MessageBoxes will NOT FIRE and ARE NOT ROUTED TO THE EVENT LOG
		'LIKE THEY NORMALLY WOULD BE.  They cause the process to lock up requiring a
		' bounce of the web-server.
		'MsgBox "Error " & Err.Number & " (" & ErrDescription & ") in procedure " & sProcedure & " of " & ErrSource & "."
#End If

        Catch ex As Exception

        Finally
            If iFile <> 0 Then FileClose(iFile)
            If bBubbleErr Then Err.Raise(ErrNumber, ErrSource & ":" & sProcedure, ErrDescription)
        End Try

    End Sub
End Module

