Option Strict Off
Option Explicit On
Imports System.Xml

Public Class CWCXBodyMemberCalc
    Const sClassName As String = "CWCXBodyMember"
    'jtodd22 09/27/2006 -there is no standard interface

    Public objJurisBenefitRule As Object
    Public objPIBodyMembers As CWCPIXBodyMembers
    Public objJurisMembers As Object
    Public objCOMRule As Object
    Public objTTCalc As Object

    Public objXClaim As CWCXClaim

    Dim m_dTotalMonths As Double
    Dim m_dTotalMoney As Double
    Dim m_dTotalWeeks As Double

    Dim objBodyMember As Object
    Dim objFetchMember As Object
    'Dim objJurisMembers As CJRBodyMembers
    Dim objJurisBenefitRule2 As Object
    Dim objJurisBenefitRule3 As Object
    'Dim m_TransSplit_1 As CFundsTransSplit

    Dim ssGridDataChanged As Boolean

    Public pAbbreviation As String
    Public pBankAcctID As Integer
    Public pTransTypeCode As Integer
    Private m_HasCommutationRule As Integer

    Private m_Warning As String

    Public Function GetTempTotalMaximum() As Double
        Const sFunctionName As String = "GetTempTotalMaximum"
        Dim lReturn As Integer
        Dim sAbbrev As String

        Try

            sAbbrev = modFunctions.GetTempTotalAbbreviation(m_Warning, (objXClaim.FilingStateID))
            lReturn = modFunctions.GetCalculator(objTTCalc, sAbbrev, (objXClaim.FilingStateID), (objXClaim.DateOfEventDTG))
            'UPGRADE_WARNING: Couldn't resolve default property of object objTTCalc.dGetBasicRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            GetTempTotalMaximum = objTTCalc.dGetBasicRate(objXClaim.ClaimantOriginalAWW, objXClaim.ClaimantTTDRate, objXClaim.JurisTaxExemptions, objXClaim.JurisTaxStatusCode, 0, objXClaim.ClaimantHourlyPayRate)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadData(ByRef lClaimID As Integer) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim lReturn As Integer

        Try

            m_Warning = ""
            m_HasCommutationRule = 0

            objXClaim = New CWCXClaim
            lReturn = objXClaim.LoadData(lClaimID)
            If lReturn <> 0 Then
                's brick
            End If

            Select Case UCase(modFunctions.GetStatePostalCode_SQL((objXClaim.FilingStateID)))
                Case "IL"
                    objJurisBenefitRule = New RMJuRuLib.CJRBenefitRuleSPPDIL()
                Case Else
                    objJurisBenefitRule = New RMJuRuLib.CJRBenefitRuleSPPDAA()
            End Select

            'jtodd22 get the objJurisBenefitRule first as we will be using the NoCodeID and YesCodeID properties later
            'UPGRADE_WARNING: Couldn't resolve default property of object objJurisBenefitRule.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lReturn = objJurisBenefitRule.LoadDataByEventDate(g_objUser, objXClaim.FilingStateID, objXClaim.DateOfEventDTG)
            Select Case lReturn
                Case 0 'NOT expected, early exit without error
                    m_Warning = m_Warning & "Failed to Load Scheduled Permanent Partial Benefit Rule, early exit."

                Case -1 'normal
                    'UPGRADE_WARNING: Couldn't resolve default property of object objJurisBenefitRule.TableRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objJurisBenefitRule.TableRowID < 1 Then
                        m_Warning = ""
                        m_Warning = m_Warning & "Failed to Load Benefit Rule for Scheduled PPD Benefits."
                        m_Warning = m_Warning & "Please have the Jurisdictional Rule created." & vbCrLf & vbCrLf
                        m_Warning = m_Warning & "Jurisdiction is:  " & modFunctions.GetStateName_SQL((objXClaim.FilingStateID)) & vbCrLf
                        m_Warning = m_Warning & "Date of Event is:  " & sDBDateFormat((objXClaim.DateOfEventDTG), "Short Date")
                    End If
                Case Else 'error
                    m_Warning = m_Warning & "Failed to Load Scheduled Permanent Partial Benefit Rule, processing error."
            End Select
            If m_Warning > "" Then
                'UPGRADE_NOTE: Object objXClaim may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objXClaim = Nothing
                'UPGRADE_NOTE: Object objJurisBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objJurisBenefitRule = Nothing
                Exit Function
            End If

            objCOMRule = New RMJuRuLib.CJRCommutation()
            'UPGRADE_WARNING: Couldn't resolve default property of object objCOMRule.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lReturn = objCOMRule.LoadDataByEventDate(g_objUser, objXClaim.FilingStateID, objXClaim.DateOfEventDTG)
            Select Case lReturn
                Case 0 'NOT expected, early exit without error
                    m_Warning = m_Warning & "Failed to Load Scheduled Permanent Partial Benefit Commutation Rule, early exit."
                Case -1 'normal
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCOMRule.TableRowID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objCOMRule.TableRowID > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objJurisBenefitRule.YesCodeID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        m_HasCommutationRule = objJurisBenefitRule.YesCodeID
                    End If
                Case Else 'error
                    m_Warning = m_Warning & "Failed to Load Scheduled Permanent Partial Benefit Commutation Rule, processing error."
            End Select

            objJurisMembers = New RMJuRuLib.CJRBodyMembers()
            'UPGRADE_WARNING: Couldn't resolve default property of object objJurisMembers.LoadDataByEventDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lReturn = objJurisMembers.LoadDataByEventDate(g_objUser, objXClaim.FilingStateID, objXClaim.DateOfEventDTG)

            objPIBodyMembers = New CWCPIXBodyMembers()
            lReturn = objPIBodyMembers.LoadData(0)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function


    Public Property Warning() As String
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property HasCommutationRule() As Integer
        Get
            HasCommutationRule = m_HasCommutationRule
        End Get
        Set(ByVal Value As Integer)
            m_HasCommutationRule = Value
        End Set
    End Property

    Public Function CalculationXML(ByRef sMessage As String, ByRef sXMLIn As String, ByRef sXMLOut As String) As Integer
        Const sFunctionName As String = "CalculationXML"

        Dim dRate As Double
        Dim i2 As Short
        Dim i3 As Short

        Dim cPIBodyMembers As CWCPIXBodyMembers
        Dim objPIBodyMember As CWCPIXBodyMember

        Dim sAmputatedCode As String
        Dim sBodyMember As String
        Dim sImpairment As String

        Dim xmlIn As New XmlDocument
        Dim xmlInroot As XmlElement
        Dim xmlInNodeBodyMembers As XmlElement
        Dim xmlInNodeMemberDetail As XmlElement
        Dim xmlInNode3 As XmlElement
        Dim xmlInNode4 As XmlElement
        Dim xmlInNode5 As XmlElement
        Dim xmlInNodes As XmlNodeList
        Dim xmlInChildNode As XmlElement

        Dim xmlOut As New XmlDocument
        Dim xmlOutroot As XmlElement
        Dim xmlOutNodeBodyMembers As XmlElement
        Dim xmlOutNodes As XmlNodeList
        Dim xmlOutnode1 As XmlElement
        Dim xmlOutNode2 As XmlElement
        Dim xmlOutNode3 As XmlElement
        Dim xmlOutNode4 As XmlElement
        Dim xmlOutNode5 As XmlElement

        Try

            'get the rate before spending time on data
            'UPGRADE_WARNING: Couldn't resolve default property of object objJurisBenefitRule.YesCodeID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objJurisBenefitRule.UseTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objJurisBenefitRule.UseTTDRateCode = objJurisBenefitRule.YesCodeID Then
                dRate = GetTempTotalMaximum()
                If dRate = 0 Then
                    sMessage = "Temporary Total Rate was not found."
                    Exit Function
                End If
            Else
                'there must be a different way for rate
            End If
            If dRate = 0 Then
                sMessage = "The Rate was not found."
                Exit Function
            End If

            xmlIn = New XmlDocument

            xmlIn.LoadXml(sXMLIn)
            xmlInroot = xmlIn.SelectSingleNode("pibodymembers")
            xmlIn.AppendChild(xmlInroot)
            xmlInNodeBodyMembers = xmlIn.SelectSingleNode("//bodymembers")
            xmlInNodes = xmlInNodeBodyMembers.ChildNodes

            cPIBodyMembers = New CWCPIXBodyMembers
            For Each xmlInChildNode In xmlInNodes
                'read each node into an object
                xmlInNodeMemberDetail = xmlInChildNode
                objPIBodyMember = New CWCPIXBodyMember
                'UPGRADE_WARNING: Couldn't resolve default property of object xmlInNodeMemberDetail.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sAmputatedCode = xmlInNodeMemberDetail.GetAttribute("amputated")
                'UPGRADE_WARNING: Couldn't resolve default property of object xmlInNodeMemberDetail.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sBodyMember = xmlInNodeMemberDetail.GetAttribute("bodymemberdesc")
                'UPGRADE_WARNING: Couldn't resolve default property of object xmlInNodeMemberDetail.getAttribute(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sImpairment = xmlInNodeMemberDetail.GetAttribute("impairment")
                With objPIBodyMember
                    If IsNumeric(sAmputatedCode) Then
                        .AmputatedCode = CInt(sAmputatedCode)
                    End If
                    '.b = sBodyMember
                    If IsNumeric(sImpairment) Then
                        '    .ImpairmentPercentage = CDbl(sImpairment)
                    End If
                    'cPIBodyMembers.Add objPIBodyMember
                End With
            Next xmlInChildNode
            '    For i2 = 1 To cPIBodyMembers.Count
            '        For i3 = 1 To objJurisMembers.Count
            '            If cPIBodyMembers.Item(i2).BodyMemberDesc = objJurisMembers.Item(i3).BodyMemberDesc Then
            '                With objJurisMembers
            '                    cPIBodyMembers.Item(i2).PayRate = dRate
            '                    If objJurisBenefitRule.UseImpairPercentCode = objJurisBenefitRule.YesCodeID Then
            '                        If .Item(i3).MaxWeeks > 0 Then
            '                            cPIBodyMembers.Item(i2).MaxWeeks = .Item(i3).MaxWeeks
            '                            cPIBodyMembers.Item(i2).LiabilityAmount = .Item(i3).MaxWeeks * (cPIBodyMembers.Item(i2).ImpairmentPercentage / 100) * dRate
            '                        Else
            '                            cPIBodyMembers.Item(i2).MaxMonths = .Item(i3).MaxMonths
            '                            cPIBodyMembers.Item(i2).LiabilityAmount = .Item(i3).MaxMonths * (cPIBodyMembers.Item(i2).ImpairmentPercentage / 100) * dRate
            '                        End If
            '                    Else
            '                        If .Item(i3).MaxWeeks > 0 Then
            '                            cPIBodyMembers.Item(i2).LiabilityAmount = .Item(i3).MaxWeeks * dRate
            '                        Else
            '                            cPIBodyMembers.Item(i2).LiabilityAmount = .Item(i3).MaxMonths * dRate
            '                        End If
            '                    End If
            '                End With
            '            End If
            '        Next            'For i3 = 1 To objJurisMembers.Count
            '    Next                'For i2 = 1 To cPIBodyMembers.Count

            'build the out bound xml
            xmlOut = New XmlDocument
            xmlOutroot = xmlOut.CreateElement("pibodymembers")
            xmlOut.AppendChild(xmlOutroot)
            'create a grouping
            xmlOutNode2 = xmlOut.CreateNode(XmlNodeType.Element, "bodymembers", "")
            'UPGRADE_WARNING: Couldn't resolve default property of object xmlOutNode2. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            xmlOutroot.AppendChild(xmlOutNode2)
            xmlOutNode3 = xmlOut.CreateNode(XmlNodeType.Element, "memberdetail", "")
            If cPIBodyMembers.Count > 0 Then
                For i2 = 1 To cPIBodyMembers.Count
                    With cPIBodyMembers
                        ''                xmlOutNode3.setAttribute "impairment", CStr(dImpairment)
                        '                xmlOutNode3.setAttribute "bodymemberdesc", sBodyMember
                        '                xmlOutNode3.setAttribute "amputated", CStr(lAmputatedCode)
                        '                xmlOutNode3.setAttribute "rate", CStr(.Item(i2).Rate)
                        '                xmlOutNode3.setAttribute "liabilityamount", CStr(.Item(i2).LiabilityAmount)
                        '                xmlOutNode2.appendChild xmlOutNode3
                    End With
                Next i2
            End If

            sXMLOut = xmlOut.InnerXml.ToString()

            CalculationXML = 0
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            CalculationXML = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
End Class

