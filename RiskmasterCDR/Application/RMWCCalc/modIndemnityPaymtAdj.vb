Option Strict Off
Option Explicit On
Module modIndemnityPaymtAdj
	'---------------------------------------------------------------------------------------
	' Module    : modIndemnityPaymtAdj
	' DateTime  : 8/16/2006 10:02
	' Author    : jtodd22
	' Purpose   : A common place for the Indemnity payment adjustments
	' Note      : The only reason for this module is the common
	' ..........: defines of funds related objects and the need to
	' ..........: control the total adjustment process on a pass/fail
	' ..........: basis.
	'---------------------------------------------------------------------------------------
	
	Const sModuleName As String = "modIndemnityPaymtAdj"
	Dim cWorkSheet As Collection
	Dim colTransSplitsReCalcs As CWCXTransSplitReCalcs
	Dim objBeneCalc As Object
	Dim oTransAbbrs As CWCXTransTypeIDAbbrs
	Dim oTransSplitsRecalcsPaid As CWCXTransSplitReCalcs
	Dim oTransSplitsRecalcsPend As CWCXTransSplitReCalcs
    'Public Function GetAutoChecksPendingBatches() As String
    '	Const subRoutineName As String = "GetAutoChecksPendingBatches"
    '	Dim objReader As dbReader
    '	Dim sOut As String
    '	Dim sSQL As String
    '	Dim sTransTypeCodeIDs As String

    '       Try

    '           GetAutoChecksPendingBatches = ""
    '           sTransTypeCodeIDs = GetTransTypeCodeIDsIndemnity
    '           sOut = ""

    '           sSQL = ""
    '           sSQL = sSQL & "SELECT"
    '           sSQL = sSQL & " SUM(FUNDS_AUTO_SPLIT.AMOUNT)"
    '           sSQL = sSQL & " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT"
    '           sSQL = sSQL & " WHERE FUNDS_AUTO.AUTO_SPLIT_ID = FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID"
    '           sSQL = sSQL & " AND FUNDS_AUTO.CLAIM_ID = " & g_objXClaim.ClaimID
    '           sSQL = sSQL & " AND FUNDS_AUTO.CLAIMANT_EID = " & g_objXClaim.ClaimantEID

    '           objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
    '           While objReader.Read()
    '               If sOut = "" Then
    '                   sOut = CStr(objReader.GetInt32(1))
    '               Else
    '                   sOut = sOut & "," & CStr(objReader.GetInt32(1))
    '               End If
    '               g_DBObject.DB_MoveNext(iRdSet)
    '           End While

    '           GetAutoChecksPendingBatches = sOut

    '       Catch ex As Exception
    '           With Err
    '               g_lErrNum = Err.Number
    '               g_sErrSrc = .Source
    '               g_sErrDescription = g_sErrDescription & Err.Description
    '           End With
    '           g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
    '           g_lErrLine = Erl()
    '           LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '           Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


    '       Finally
    '           

    '       End Try

    '   End Function

    'Public Function IndemnityPaymtsAdjustments() As Integer



    'End Function
    'Public Function IndemnityPaymtsAuto() As Integer
    '    Const subRoutineName As String = "IndemnityPaymtsAuto"
    '    Dim lReturn As Integer

    '    Try

    '        IndemnityPaymtsAuto = 0
    '        'Let the error handler reset the value


    '    Catch ex As Exception
    '        With Err
    '            g_lErrNum = Err.Number
    '            g_sErrSrc = .Source
    '            g_sErrDescription = g_sErrDescription & Err.Description
    '        End With
    '        IndemnityPaymtsAuto = g_lErrNum
    '        g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
    '        g_lErrLine = Erl()
    '        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '        Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

    '    Finally
    '    End Try

    'End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : IndemnityPaymtsReCalc
    ' DateTime  : 8/16/2006 10:30
    ' Author    : jtodd22
    ' Purpose   : This is the common recalculation routine
    '---------------------------------------------------------------------------------------
    '
    'Private Function IndemnityPaymtsReCalc(ByRef oRecalc As CWCXTransSplitReCalcs) As Integer
    '    Const subRoutineName As String = "IndemnityPaymtsReCalc"
    '    Dim lIndexAbbr As Integer
    '    Dim lIndexTransSplit As Integer
    '    Dim lLastPaymtTransCodeID As Integer
    '    Dim lReturn As Integer
    '    Dim sAbbr As String
    '    Dim sTmp As String

    '    Const sFunctionName As String = "IndemnityPaymtsReCalc"

    '    Try

    '        IndemnityPaymtsReCalc = 0
    '        'let error handler reset value

    '        oTransAbbrs = New CWCXTransTypeIDAbbrs

    '        lReturn = oTransAbbrs.LoadData
    '        If oTransAbbrs.Count = 0 Then
    '            'no benefits defined--Error--Error
    '        End If

    '        lLastPaymtTransCodeID = oRecalc.Item(1).TransTypeCode
    '        sAbbr = ""
    '        For lIndexAbbr = 1 To oTransAbbrs.Count
    '            If oTransAbbrs.Item(lIndexAbbr).TransTypeCodeID = lLastPaymtTransCodeID Then
    '                sAbbr = oTransAbbrs.Item(lIndexAbbr).Abbreviation
    '            End If
    '        Next lIndexAbbr
    '        If sAbbr = "" Then
    '            'the benefit was not found
    '        End If
    '        For lIndexTransSplit = 1 To oRecalc.Count
    '            If lLastPaymtTransCodeID <> oRecalc.Item(lIndexTransSplit).TransTypeCode Then
    '                lLastPaymtTransCodeID = oRecalc.Item(lIndexTransSplit).TransTypeCode
    '                sAbbr = ""
    '                For lIndexAbbr = 1 To oTransAbbrs.Count
    '                    If oTransAbbrs.Item(lIndexAbbr).TransTypeCodeID = lLastPaymtTransCodeID Then
    '                        sAbbr = oTransAbbrs.Item(lIndexAbbr).Abbreviation
    '                    End If
    '                Next lIndexAbbr
    '                If sAbbr = "" Then
    '                    'the benefit was not found
    '                End If
    '            End If
    '            With g_objXPaymentParms
    '                .EarningsPerWeek = oRecalc.Item(lIndexTransSplit).TPEarningsAmount
    '                sTmp = oRecalc.Item(lIndexTransSplit).ToDate
    '                .RegularEndDate = Mid(sTmp, 5, 2) & "/" & Mid(sTmp, 7, 2) & "/" & Mid(sTmp, 1, 4)
    '                sTmp = oRecalc.Item(lIndexTransSplit).FromDate
    '                .RegularStartDate = Mid(sTmp, 5, 2) & "/" & Mid(sTmp, 7, 2) & "/" & Mid(sTmp, 1, 4)
    '                .RegularNumberOfPayments = 1
    '                .TotalNumberofPayments = 1
    '            End With
    '            lReturn = modFunctions.GetCalculator(objBeneCalc, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
    '            If lReturn <> 0 Then
    '            End If
    '            'UPGRADE_WARNING: Couldn't resolve default property of object objBeneCalc.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '            oRecalc.Item(lIndexTransSplit).AmountNew = objBeneCalc.Standard.CalculatedPaymentRegular
    '            oRecalc.Item(lIndexTransSplit).AmountDifference = oRecalc.Item(lIndexTransSplit).AmountNew - oRecalc.Item(lIndexTransSplit).AmountOriginal
    '        Next lIndexTransSplit

    '    Catch ex As Exception
    '        With Err
    '            g_lErrNum = Err.Number
    '            g_sErrSrc = .Source
    '            g_sErrDescription = g_sErrDescription & Err.Description
    '        End With
    '        IndemnityPaymtsReCalc = g_lErrNum
    '        g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
    '        g_lErrLine = Erl()
    '        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '        Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

    '    Finally
    '    End Try

    'End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : IndemnityPaymtsDateDataCheck
    ' DateTime  : 8/16/2006 10:09
    ' Author    : jtodd22
    ' Purpose   : To find the null dated payments that can not be adjusted
    ' Note      : There should not be ANY null dated payments
    '---------------------------------------------------------------------------------------
    '
    'Public Function IndemnityPaymtsDateDataCheck() As String
    '    Const subRoutineName As String = "IndemnityPaymtsDateDataCheck"
    '    Dim objReader As dbReader
    '    Dim lIndex As Integer
    '    Dim lReturn As Integer
    '    Dim sOut As String

    '    Try

    '        IndemnityPaymtsDateDataCheck = ""

    '        sOut = ""

    '        colTransSplitsReCalcs = New CWCXTransSplitReCalcs
    '        lReturn = colTransSplitsReCalcs.LoadDataNullDates("")
    '        For lIndex = 1 To colTransSplitsReCalcs.Count
    '            If sOut = "" Then
    '                sOut = colTransSplitsReCalcs.Item(lIndex).ControlNumber
    '            Else
    '                sOut = sOut & ", " & colTransSplitsReCalcs.Item(lIndex).ControlNumber
    '            End If
    '        Next lIndex

    '        IndemnityPaymtsDateDataCheck = sOut

    '    Catch ex As Exception
    '        With Err
    '            g_lErrNum = Err.Number
    '            g_sErrSrc = .Source
    '            g_sErrDescription = g_sErrDescription & Err.Description
    '        End With
    '        g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
    '        g_lErrLine = Erl()
    '        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '        Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

    '    Finally
    '    End Try

    'End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : IndemnityPaymtsPaid
    ' DateTime  : 8/9/2006 08:35
    ' Author    : jtodd22
    ' Purpose   : find, recalculate and save funds paid
    '    get paid funds records that are indemnity payments,
    'order by date and transaction id
    'recalculate each record
    'the difference is the new lien/payment
    '---------------------------------------------------------------------------------------
    '
    'Public Function IndemnityPaymtsPaid() As Integer
    '    Const subRoutineName As String = "IndemnityPaymtsPaid"
    '    Dim lReturn As Integer

    '    Try

    '        IndemnityPaymtsPaid = 0
    '        'Let the error handler reset the value

    '        oTransSplitsRecalcsPaid = New CWCXTransSplitReCalcs
    '        lReturn = oTransSplitsRecalcsPaid.LoadData("=")
    '        If oTransSplitsRecalcsPaid.Count = 0 Then
    '        End If

    '        lReturn = IndemnityPaymtsReCalc(oTransSplitsRecalcsPaid)

    '    Catch ex As Exception
    '        With Err
    '            g_lErrNum = Err.Number
    '            g_sErrSrc = .Source
    '            g_sErrDescription = g_sErrDescription & Err.Description
    '        End With
    '        IndemnityPaymtsPaid = g_lErrNum
    '        g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
    '        g_lErrLine = Erl()
    '        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '        Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

    '    Finally
    '    End Try

    'End Function


    '---------------------------------------------------------------------------------------
    ' Procedure : IndemnityPaymtsPend
    ' DateTime  : 8/9/2006 08:35
    ' Author    : jtodd22
    ' Purpose   : find, recalculate and save funds Pend
    '    get Pend funds records that are indemnity payments,
    'order by date and transaction id
    'recalculate each record
    'the difference is the new lien/payment
    '---------------------------------------------------------------------------------------
    '
    'Public Function IndemnityPaymtsPend() As Integer
    '    Const subRoutineName As String = "IndemnityPaymtsPend"
    '    Dim lReturn As Integer

    '    Try

    '        IndemnityPaymtsPend = 0
    '        'Let the error handler reset the value

    '        oTransSplitsRecalcsPend = New CWCXTransSplitReCalcs
    '        lReturn = oTransSplitsRecalcsPend.LoadData("<>")
    '        If oTransSplitsRecalcsPend.Count = 0 Then
    '        End If

    '        lReturn = IndemnityPaymtsReCalc(oTransSplitsRecalcsPend)

    '    Catch ex As Exception
    '        With Err
    '            g_lErrNum = Err.Number
    '            g_sErrSrc = .Source
    '            g_sErrDescription = g_sErrDescription & Err.Description
    '        End With
    '        IndemnityPaymtsPend = g_lErrNum
    '        g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & subRoutineName & "|"
    '        g_lErrLine = Erl()
    '        LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
    '        Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

    '    Finally
    '    End Try

    'End Function
End Module

