Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCBenefitsInCalc
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CWCBenefitsInCalc"
    'local variable to hold collection
    Private mCol As Collection
    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CWCBenefitInCalc
        Get
            'used when referencing an element in the collection
            'vntIndexKey contains either the Index or Key to the collection,
            'this is why it is declared as a Variant
            'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property
    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        GetEnumerator = mCol.GetEnumerator
    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        'used when removing an element from the collection
        'vntIndexKey contains either the Index or Key, which is why
        'it is declared as a Variant
        'Syntax: x.Remove(xyz)
        mCol.Remove(vntIndexKey)


    End Sub
   
    Public Sub New()
        MyBase.New()
        mCol = New Collection
    End Sub
   
    Protected Overrides Sub Finalize()
        mCol = Nothing
        MyBase.Finalize()
    End Sub

    Public Function LoadData(ByRef FilingStateID As Integer) As Integer
        Const sFunctionName As String = "lLoadData"

        Dim dTemp As Double
        Dim objReader As DbReader
        Dim iTemp As Short
        Dim lReturn As Integer
        Dim lTmp As Integer
        Dim sSQL As String
        Try
            mCol = New Collection
            sSQL = ""
            sSQL = sSQL & "SELECT * FROM WCP_BENEFIT_LKUP"
            sSQL = sSQL & " WHERE JURIS_ROW_ID = " & FilingStateID
            sSQL = sSQL & " AND USE_IN_CALCULATOR <> 0"
            sSQL = sSQL & " AND ABBREVIATION IS NOT NULL"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While (objReader.Read())
                lTmp = 0
                lTmp = objReader.GetInt32("BENEFIT_LKUP_ID")
                AddToCollection(objReader.GetString("JURIS_BENEFIT_DESC"), objReader.GetString("ABBREVIATION"), lTmp, "k" & Right("000000" & CStr(lTmp), 5))
            End While
            LoadData = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Private Function AddToCollection(ByRef BenefitDesc As String, ByRef Abbreviation As String, ByRef DatabaseLookupID As Integer, ByRef sKey As String) As CWCBenefitInCalc
        Const sFunctionName As String = "AddToCollection"

        'create a new object
        Dim objNewMember As CWCBenefitInCalc
        Try
            objNewMember = New CWCBenefitInCalc
            'set the properties passed into the method
            objNewMember.BenefitDesc = BenefitDesc
            objNewMember.DatabaseLookupID = DatabaseLookupID
            objNewMember.Abbreviation = Abbreviation
            mCol.Add(objNewMember, sKey)
            'return the object created
            AddToCollection = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_WARNING: Couldn't resolve default property of object AddToCollection. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'AddToCollection = g_lErrNum #Mukesh
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function
End Class

