Option Strict Off
Option Explicit On
Public Class CWCVocRehabSupp
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "VRMASP"
    Const sClassName As String = "CWCVocRehabSupp"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleVRMASP"

    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Private oEnumErrorMask As CWCEnumErrorMask
    Private oEnumSuppMask As CWCEnumSuppMask

    Private m_PPDLiabilityPaid As Double
    Public BenefitRate_Effective As Double
    Public CalculatedPayment As Double
    Public BenefitDays As Integer

    Public NoCalculate As Short
    Public ClaimID As Integer
    Public RecordCount As Integer

    Public JurisMaxBenRateDay As Double
    Public JurisMaxBenRateWeek As Double
    Public MinBenRateDay As Double
    Public MinBenRateWeek As Double

    Public BenefitEndDate As String
    Public BenefitStartDate As String


    Public objPPD As Object
    Public objTTD As Object
    Public objVR As Object
    Public PayLateCharge As Short

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objTTD may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objTTD = Nothing
        'UPGRADE_NOTE: Object objPPD may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objPPD = Nothing
        'UPGRADE_NOTE: Object objVR may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objVR = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim sAbbr As String
        Dim sWarning As String
        Try
            Dim lReturn As Integer
            Dim dDateOfEvent As Date
            Dim dblOverPayment, dblPayment, dblDaysInOverPayment As Double

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            oEnumErrorMask = New CWCEnumErrorMask
            oEnumErrorMask.SetConstantValues()

            oEnumSuppMask = New CWCEnumSuppMask
            oEnumSuppMask.SetConstantValues()

            sAbbr = modFunctions.GetTempTotalAbbreviation(sWarning, (g_objXClaim.FilingStateID))
            If sAbbr > "" Then
                lReturn = modFunctions.GetCalculator(objTTD, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
                If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
                'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lReturn = objTTD.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
                If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            Else
                m_ErrorMask = oEnumErrorMask.cNoTempTotalRule
                If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            End If

            sAbbr = "VRMA"
            'UPGRADE_WARNING: Couldn't resolve default property of object objVR.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lReturn = objVR.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
            If sAbbr > "" Then
                lReturn = modFunctions.GetCalculator(objVR, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
                If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
                'UPGRADE_WARNING: Couldn't resolve default property of object objVR.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lReturn = objVR.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
                If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            Else
                m_ErrorMask = oEnumErrorMask.cNoTempTotalRule
                If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            End If

            sAbbr = "PPD"
            lReturn = modFunctions.GetCalculator(objPPD, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
            If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            'UPGRADE_WARNING: Couldn't resolve default property of object objPPD.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lReturn = objPPD.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
            If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:34
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim lReturn As Integer
        Dim m_Note As String
        Dim dDateOfEvent As Date
        Dim dblOverPayment, dblPayment, dblDaysInOverPayment As Double

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            m_ErrorMask = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function


            Me.NoCalculate = System.Math.Abs(CInt(False))
            'objBenefitRule.PermPartRequiredCode

            'jlt 01/10/2004     Me.LiabilityPaid = GetBenefitAmountPaidOrPending("PPD-%", VocRehabsuppscn, objPPD.BeginPrntDate_Effective) + GetBenefitAmountPaidOrPending("PDSVR-%", VocRehabsuppscn, objPPD.BeginPrntDate_Effective)
            'UPGRADE_WARNING: Couldn't resolve default property of object objPPD.BeginPrntDate_Effective. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PPDLiabilityPaid = GetBenefitAmountPaidOrPending2("4,5,6,16", objPPD.BeginPrntDate_Effective)

            '??????   why the > 0 condition.  it allows impossible conditions
            If m_PPDLiabilityPaid > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objPPD.Liability. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Select Case m_PPDLiabilityPaid
                    Case Is > objPPD.Liability
                        m_Note = m_Note & vbCrLf & "This Benefit, PD Supplement, is overpaid.  It is not possible to calculate or save a payment."
                        m_ErrorMask = m_ErrorMask + oEnumSuppMask.csuppLiabilityOverPaid
                        GoTo No_Calculate
                    Case Is = objPPD.Liability
                        m_Note = m_Note & vbCrLf & "This Benefit, PD Supplement, is paid.  It is not possible to calculate or save a payment."
                        m_ErrorMask = m_ErrorMask + oEnumSuppMask.csuppLiabilityPaid
                        GoTo No_Calculate
                End Select
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objVR.BenefitRate_Effective. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objTTD.BenefitRate_Effective. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.BenefitRate_Effective = objTTD.BenefitRate_Effective - objVR.BenefitRate_Effective
            If Me.BenefitRate_Effective < 0.005 Then
                m_Note = m_Note & vbCrLf & "There is no Supplemental Payable on this claim. It is not possible to calculate or save a payment."
                m_ErrorMask = m_ErrorMask + oEnumSuppMask.csuppNoPayable
                GoTo No_Calculate
            End If

No_Calculate:
            Me.NoCalculate = System.Math.Abs(CInt(True))
            'build top of work sheet
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail
            dDateOfEvent = CDate(GetFormattedDate(g_objXClaim.DateOfEventDTG))

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dblPayment = (Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.objPPD.Liability. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dblOverPayment = dblPayment + m_PPDLiabilityPaid - Me.objPPD.Liability
            If dblOverPayment > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dblDaysInOverPayment = dblOverPayment / (Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek)
                If dblDaysInOverPayment > 1 Then
                    m_Note = m_Note & vbCrLf & "The ending payment date will result in an overpayment of liability by " & Format(dblOverPayment, "#.00") & "." & " Please change the through payment date backwards by at least " & Int(dblDaysInOverPayment) & " days and recalculate the payment." & " It is not possible to save a payment."
                    m_ErrorMask = m_ErrorMask + oEnumSuppMask.csuppOverPaymentFatal
                    dblPayment = 0
                    GoTo No_Calculate
                Else
                    m_Note = m_Note & vbCrLf & "Payment was reduced to avoid overpayment."
                    m_ErrorMask = m_ErrorMask + oEnumSuppMask.csuppOverPaymentNonFatal
                    'UPGRADE_WARNING: Couldn't resolve default property of object Me.objPPD.Liability. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dblPayment = Me.objPPD.Liability - m_PPDLiabilityPaid
                End If
            End If
            Me.CalculatedPayment = dblPayment

            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            m_Note = m_Note & Mid(m_Note, 3)
            m_ErrorMask = m_ErrorMask
            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim lReturn As Integer

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If


            Select Case modFunctions.GetStatePostalCode_SQL((g_objXClaim.FilingStateID))
                Case "CA"

                Case Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.MaxCompRate > 0 And dBasicRate > objBenefitRule.MaxCompRate Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dBasicRate = objBenefitRule.MaxCompRate
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dBasicRate < objBenefitRule.MinCompRate Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dBasicRate = objBenefitRule.MinCompRate
                    End If
            End Select

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function



    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Voc Rehab Supp"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property


    Public Property PPDLiabilityPaid() As Double
        Get

            PPDLiabilityPaid = m_PPDLiabilityPaid

        End Get
        Set(ByVal Value As Double)

            m_PPDLiabilityPaid = Value

        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

