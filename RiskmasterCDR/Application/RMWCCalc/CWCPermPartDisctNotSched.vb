Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPermPartDisctNotSched
    Implements _ICalculator
    '---------------------------------------------------------------------------------------
    ' Module    : CWCPermPartDisctNotSched
    ' DateTime  : 5/2/2006 14:09
    ' Author    : jtodd22
    ' Purpose   : To handle the discount jurisdictions with a Non Scheduled permanent partial
    ' Notes     : Be aware that commutations can affect the rate before and after payments start
    '---------------------------------------------------------------------------------------

    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "U-PPD"
    Const sClassName As String = "CWCPermPartDisctNotSched"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleNSPPDAA"
    Private colClaimantBodyMembers As New CWCPIXBodyMembers
    Public colBodyMembers As Object

    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objWCPClaim As CWCWCPClaim

    'these members represent fields in WCP_CLAIMS
    Public BenefitRate_Original As Double 'pre-commutation rate, if record was commutated
    Public BenefitRate_PostComm As Double 'only exists if a commutation
    Public BenefitRate_Effective As Double
    Public Commutation As Double
    Private m_DisabilityRate_Original As Double
    Private m_DisabilityRate_PostComm As Double
    Private m_DisabilityRate_Effective As Double
    Public EstCost_Original As Double
    Public EstCost_PostComm As Double
    Public EstCost_Effective As Double
    Public Liability As Double
    Public LiabilityPaid As Double
    Public WeeksPayable_Original As Double
    Public WeeksPayable_PostComm As Double
    Public WeeksPayable_Effective As Double
    Private m_BeginPrntDate_Original As String
    Private m_BeginPrntDate_PostComm As String
    Private m_BeginPrntDate_Effective As String
    Public EndRecdDate_Original As String
    Public EndRecdDate_PostComm As String
    'end members linked to WCP_CLAIMS

    Public LastCommutationDate As String
    Public LastCommutationDate_DTG As String
    Public LastDayPPDPayable As Date
    Public LastDayPPDPaid As String
    Public WeeksPayable As Double

    Public BenefitDays As Integer
    Public RecordCount As Integer


    Public JurisMaxBenRateDay As Double
    Public JurisMaxBenRateWeek As Double
    Public MinBenRateDay As Double
    Public MinBenRateWeek As Double

    Public BenefitEndDate As String
    Public BenefitStartDate As String
    Public PayLateCharge As Short

    Public BenefitRate_PPD As Double

    Public Function PutClaimBenefitData() As Boolean
        Const sFunctionName As String = "PutClaimBenefitData"

        Dim dRate As Double
        Dim lReturn As Integer
        Dim lRowID As Integer
        Dim sSQL2 As String
        Dim sSQL3 As String

        Try

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT MAX(ROW_ID)"
            sSQL2 = sSQL2 & " FROM " & g_sWCPClaimsTableName
            sSQL2 = sSQL2 & " WHERE BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'"
            sSQL2 = sSQL2 & " AND CLAIM_ID = " & g_objXClaim.ClaimID

            lRowID = DbFactory.ExecuteScalar(g_ConnectionString, sSQL2)

            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT * "
            sSQL3 = sSQL3 & " FROM " & g_sWCPClaimsTableName
            sSQL3 = sSQL3 & " WHERE BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'"
            sSQL3 = sSQL3 & " AND CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL3 = sSQL3 & " AND ROW_ID = (" & sSQL2 & ")"

            If lRowID = 0 Then 'new record
                lReturn = modFunctions.GetMMIImpairment(m_DisabilityRate_Original, m_BeginPrntDate_Original)
                If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then Exit Function
                objWCPClaim.LoadDataOriginalRecord(sSQL3)
                With objWCPClaim
                    'not used here--.AttyFeeCode
                    'not used here--.AttyFeePercent
                    .BeginPrintDateEffective = m_BeginPrntDate_Original
                    .BeginPrintDateOriginal = m_BeginPrntDate_Original
                    .BenefitRateEffective = dRate
                    .BenefitRateOriginal = dRate
                    .BenefitType = m_sBenefitTypeAbbr
                    .ClaimID = g_objXClaim.ClaimID
                    'not used here--.Commutation
                    .DisabilityRateEffective = m_DisabilityRate_Original
                    .DisabilityRateOriginal = m_DisabilityRate_Original
                    'not used here--.EndRecvdDateEffective
                    'calculated in object--.EstCostEffective
                    'not used here--.FormID
                    'not used here--.Paid
                    .WeeksPayableEffective = Me.WeeksPayable_Effective
                End With
                objWCPClaim.SaveDataOriginalRecord()
            Else
                'have multiple records
                'jtodd22 do not change data
            End If
            PutClaimBenefitData = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim dbl As Double
        Dim dTemp As Double
        Dim objReader As DbReader
        Dim iTemp As Short
        Dim lReturn As Integer
        Dim lRowID As Integer
        Dim sSQL As String
        Dim sSQL2 As String
        Dim sSQL3 As String

        Dim dblCommutation, dblBenefitNew As Double
        Dim dblOverPayment, dblDays As Double
        Dim lRecordCount As Integer
        Dim sNote As String
        Dim sExistingCommutationRate As String
        Dim sTemp As String
        Dim dDateOfEvent As Date

        Try
            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function

            lReturn = modFunctions.GetMMIImpairment(m_DisabilityRate_Original, m_BeginPrntDate_Original)
            If m_DisabilityRate_Original = 0 Then m_ErrorMask = g_objXErrorMask.cNoImpairment
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function

            'm_MMIDateRequiredCode = objBenefitRule.
            objWCPClaim = New CWCWCPClaim

            lRowID = objWCPClaim.GetRowIDMinimum(m_sBenefitTypeAbbr)
            Select Case lRowID
                Case 0
                    'we need to create a new record
                    Me.PutClaimBenefitData()

                Case Else
                    'we have an existing record
                    sSQL2 = ""
                    sSQL2 = sSQL2 & "SELECT *"
                    sSQL2 = sSQL2 & " FROM " & g_sWCPClaimsTableName
                    sSQL2 = sSQL2 & " WHERE ROW_ID = " & lRowID
                    objWCPClaim.LoadDataOriginalRecord(sSQL2)
                    'if there is a commutation the rate changed get the last record written
                    If objWCPClaim.Commutation > 0 Then

                        lReturn = objWCPClaim.LoadDataLastRecordByRowID(m_sBenefitTypeAbbr)
                        If objWCPClaim.Commutation > 0 Then
                            Me.LastCommutationDate_DTG = objWCPClaim.EndRecvdDateEffective
                        End If
                    End If

                    lLoadData = g_objXErrorMask.cSuccess
            End Select

            'fetch the minimum and maximum benefits allowed for this state, event date, and effective dis rate
            lReturn = GetPPDLimits(m_DisabilityRate_Effective)
            Select Case lReturn
                Case 0 'expected
                    Me.JurisMaxBenRateWeek = g_dblMaxBenefit
                    Me.MinBenRateWeek = g_dblMinBenefit
                Case Else 'bad thing
                    lLoadData = lReturn
                    Exit Function
            End Select

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.MinBenRateDay = Me.MinBenRateWeek / objCalcBenefitRule.JurisWorkWeek
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.JurisMaxBenRateDay = Me.JurisMaxBenRateWeek / objCalcBenefitRule.JurisWorkWeek
            If Trim(m_BeginPrntDate_Effective & "") = "" Then
                m_PaidPendingWeeks = 0
            Else
                'jlt 01/10/2004     m_PaidPendingWeeks = GetBenefitAmountPaidOrPending("PID-%", 3, m_BeginPrntDate_Effective) + _
                ''jlt 01/10/2004                                              GetBenefitAmountPaidOrPending("PPD-%", 9, m_BeginPrntDate_Effective)
                m_PaidPendingWeeks = GetBenefitAmountPaidOrPending2("4,5,6,16", m_BeginPrntDate_Effective)
            End If
            Me.LastDayPPDPayable = f_dtGetLastDayPermPartialPayable((Me.WeeksPayable_Original), m_BeginPrntDate_Original)
            sTemp = Mid(m_BeginPrntDate_Effective, 5, 2) & "/" & Mid(m_BeginPrntDate_Effective, 7, 2) & "/" & Mid(m_BeginPrntDate_Effective, 1, 4)
            If Me.BenefitRate_Effective > 0 Then
                iTemp = ((m_PaidPendingWeeks * 7) / Me.BenefitRate_Effective)
                Me.LastDayPPDPaid = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, iTemp - 1, CDate(sTemp)))
            Else
                iTemp = 0
            End If
            'days paid must include the m_BeginPrntDate_Effective as it is the intitlement day jlt
            'UPGRADE_WARNING: Couldn't resolve default property of object PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.LastDayPPDPaid = PadDateWithZeros(Me.LastDayPPDPaid)
            If Len(Me.LastCommutationDate_DTG) = 8 Then
                sTemp = Me.LastCommutationDate_DTG
                sTemp = Mid(sTemp, 5, 2) & "/" & Mid(sTemp, 7, 2) & "/" & Mid(sTemp, 1, 4)
                Me.LastCommutationDate = sTemp
            End If
            Me.Liability = (Me.BenefitRate_Effective * Me.WeeksPayable_Effective)


            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:36
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Double
        Const sFunctionName As String = "lCalculatePayment"
        Dim i2 As Short
        Dim i3 As Short
        Dim dMonthsToPay As Double
        Dim dWeeksToPay As Double

        Dim lPIRowID As Integer
        Dim lReturn As Integer
        Dim dblOverPayment As Double
        Dim dblDays As Double
        Dim dDateOfEvent As Date
        Dim sNote As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            If m_UseBodyMembersCode = g_lYesCodeID Then
                '****jtodd22, 12/18/2006
                '****This is an ERROR, a non scheduled PPD does not use body members
                'blow up application on with m_Warning
                m_Warning = ""
                m_Warning = m_Warning & "Jurisdictional Rules is set to use a body member table." & vbCrLf
                m_Warning = m_Warning & "This is not permitted for non-scheduled Permanent Partial payments." & vbCrLf
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            dDateOfEvent = CDate(GetFormattedDate(g_objXClaim.DateOfEventDTG))

            'build top of work sheet
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail
            If m_DisabilityRate_Effective > 0 Then
                colWorkSheet.Add("|Percentage Disability                             |" & (m_DisabilityRate_Effective / 100).ToString("P1"))
            Else
            End If
            colWorkSheet.Add("||")
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Claimant's Original AWW|" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))
            colWorkSheet.Add("||")
            colWorkSheet.Add("|The Weekly Benefit Rate is |" & Format(Me.BenefitRate_Effective, "Currency"))
            colWorkSheet.Add("||")

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, (Me.BenefitRate_Effective))

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function


            '        Me.CalculatedPayment = (Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays
            '        Select Case Me.RecordCount
            '            Case 0
            '            Case Else
            '                dblOverPayment = Me.CalculatedPayment + m_PaidPendingWeeks - Me.EstCost_Effective
            '                If dblOverPayment > 0 Then
            '                    dblDays = dblOverPayment / Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek
            '                    If dblDays >= 1 Then
            '                        sNote = sNote & vbCrLf & "The ending payment date will result in an overpayment of liability by " & Format(Round(dblOverPayment, 2), "#.00") & "." & _
            ''                                   "Please change the through payment date backwards by " & Int(dblDays) & " days and recalculate the payment." & _
            ''                                   "No calculation is possible."
            '                        Me.CalculatedPayment = 0
            '                    Else
            '                        sNote = sNote & vbCrLf & "There is an overpayment of liability on the though payment date." & _
            ''                                   "The payment will be reduced to avoid an overpayment."
            '                        Me.CalculatedPayment = Me.Liability - Me.LiabilityPaid
            '                    End If
            '                End If
            '        End Select
            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
hSkip:
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function

    Private Function ClearObject() As Integer
        Const sFunctionName As String = "ClearObject"
        Try

            BenefitRate_Original = 0 'pre-commutation rate, if record was commutated
            BenefitRate_PostComm = 0 'only exists if a commutation
            BenefitRate_Effective = 0 'zero as start of LoadData
            Commutation = 0
            m_DisabilityRate_Original = 0
            m_DisabilityRate_PostComm = 0
            m_DisabilityRate_Effective = 0
            EstCost_Original = 0
            EstCost_PostComm = 0
            EstCost_Effective = 0
            Liability = 0
            LiabilityPaid = 0
            WeeksPayable_Original = 0
            WeeksPayable_PostComm = 0
            WeeksPayable_Effective = 0
            m_BeginPrntDate_Original = vbNullString
            m_BeginPrntDate_PostComm = vbNullString
            m_BeginPrntDate_Effective = vbNullString
            EndRecdDate_Original = vbNullString
            EndRecdDate_PostComm = vbNullString
            ''end members linked to WCP_CLAIMS
            '
            LastDayPPDPayable = CDate("12/31/1899")
            WeeksPayable = 0
            BenefitDays = 0
            RecordCount = 0
            m_CheatSheetTitle = "Permanent Partial"
            m_EarningsRequiredCode = g_lNoCodeID
            m_ErrorMask = g_objXErrorMask.cSuccess
            m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
            m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
            m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
            m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
            m_MMIDateRequiredCode = g_lNoCodeID
            m_Note = ""
            m_PaidPendingMonths = 0
            m_PaidPendingWeeks = 0
            m_PayPeriodName = ""
            m_RuleTotalMonths = 0
            m_RuleTotalWeeks = 0
            m_UseBodyMembersCode = g_lNoCodeID
            m_Warning = ""

            m_CalculatedPaymentAuto = 0
            m_CalculatedPaymentCatchUp = 0
            m_CalculatedPaymentRegular = 0
            m_CalculatedPaymentWaitingPeriod = 0

            JurisMaxBenRateDay = 0
            JurisMaxBenRateWeek = 0
            MinBenRateDay = 0
            MinBenRateWeek = 0


            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objBenefitRule = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            ClearObject = g_lErrNum

        End Try

    End Function
    Public Function GetExistingWeeklyRate_PPDBenefit() As Double
        Const sFunctionName As String = "GetExistingWeeklyRate_PPDBenefit"

        'Employee has a Permanent Partial Disability
        'The Weekly Rate can change during a Commutation
        'RiskMaster stores a rate in the table WCP_CLAIMS by CLAIM_ID and by ROW_ID
        Dim dTest As Double
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL_2 As String
        Dim rs As Short
        Try
            '***Two conditions to check
            '***1)existing record, with out commutation
            '******disability rate can change, most common
            '******may not have a 'P & S Date'
            '******beware that a claim can have multiple commutations
            '***2)existing record, with commutation
            '******disability rate does not change
            '******must have a 'P & S Date' for commutations
            '******beware that a claim can have multiple commutations
            '**********************************************************************************
            '***On each commutation an existing record is updated and a new record is created with
            '***the benefit base data.
            GetExistingWeeklyRate_PPDBenefit = 0
            '***
            Select Case Me.RecordCount
                Case 0
                    'never happens, me.objidscalc.ppd.loaddata forces first record
                Case 1
                    'have record, no commutation
                    'disability rate can change
                    dTest = 0
                    sSQL_2 = SQLSelectClaimDataWithOrderBy(m_sBenefitTypeAbbr, "DESC")
                    objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL_2)
                    If objReader.Read() Then
                        dTest = objReader.GetInt32("BENEFIT_RATE")
                        If dTest = 0 Then
                            dTest = GetExistingWeeklyRate_PPDBenefit
                            Call PutClaimBenefitData()
                        Else
                            GetExistingWeeklyRate_PPDBenefit = dTest
                        End If
                        GetExistingWeeklyRate_PPDBenefit = dTest
                    Else
                        'should not happen
                    End If


                Case Else
                    'have commutation, rate does not change from screen inputs
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            If Not (objReader Is Nothing) Then
                objReader.Close()
                objReader.Dispose()
            End If

        End Try

    End Function
    Public Function GetPPDWeeks() As Double
        Const sFunctionName As String = "GetPPDWeeks"

        '**************************************************************************************************
        '***note:  do not call this if commutation > 0
        '***If Me.RecordCount > 1 the percentage must not change in calling application.
        '**************************************************************************************************
        Dim sSQL As String
        Dim objReader As DbReader
        Dim dblBenefitWeeks, dblDistractor As Double
        Try
            If Me.RecordCount > 1 Then Exit Function

            sSQL = "SELECT * FROM WCP_PPD_WEEKS" & " WHERE BEGIN_DATE <= '" & g_objXClaim.DateOfEventDTG & "'" & " AND STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND (" & m_DisabilityRate_Effective & " BETWEEN BEGIN_PERCENT AND END_PERCENT)" & " ORDER BY BEGIN_DATE DESC"

            'jlt, would expect 7 records or less to be returned, we want the one with the BEGIN_DATE
            'closest to the Date_of_Event and before the Date of Event
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                dblDistractor = objReader.GetInt32("PD_DISTRACTOR")
                GetPPDWeeks = objReader.GetInt32("BASE_WEEKS") + objReader.GetInt32("PD_MULTIPLIER") * (m_DisabilityRate_Effective - dblDistractor)
            Else
                GetPPDWeeks = 0
            End If



        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)

            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function SQLSelectPPD_BenefitWeeks() As String
        Dim sSQL As String

        sSQL = "SELECT * FROM WCP_PPD_WEEKS" & " WHERE BEGIN_DATE <= '" & g_objXClaim.DateOfEventDTG & "'" & " AND STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND (" & m_DisabilityRate_Effective & " BETWEEN BEGIN_PERCENT AND END_PERCENT)" & " ORDER BY BEGIN_DATE DESC"
        'jlt, would expect 7 records or less to be returned, we want the one with the BEGIN_DATE
        'closest to the Date_of_Event and before the Date of Event
        SQLSelectPPD_BenefitWeeks = sSQL



    End Function

    Public Function SQLSelectPPDClaimDataWithOrderBy(ByVal sSortOrder As String) As String
        Dim sSQL As String

        sSortOrder = Trim(UCase(sSortOrder))
        sSQL = ""
        sSQL = sSQL & "SELECT *"
        sSQL = sSQL & " FROM " & g_sWCPClaimsTableName
        sSQL = sSQL & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID & " AND BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'" & " ORDER BY ROW_ID " & sSortOrder

        SQLSelectPPDClaimDataWithOrderBy = sSQL



    End Function

    Public Function GetWeeklyRate_PPDBenefit_Calcu() As Double
        Const sFunctionName As String = "GetWeeklyRate_PPDBenefit"

        'Employee has a Permanent Partial Disability
        'The Weekly Rate can change during a Commutation
        'RiskMaster stores a rate in the table WCP_CLAIMS by CLAIM_ID and by ROW_ID
        Dim dBenefitAmount As Double
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim sSQL As String
        Try
            '***Two conditions to check
            '***1)existing record, with out commutation
            '******disability rate can change, most common
            '******may not have a 'P & S Date'
            '******beware that a claim can have multiple commutations
            '***2)existing record, with commutation
            '******disability rate does not change
            '******must have a 'P & S Date' for commutations
            '******beware that a claim can have multiple commutations
            '**********************************************************************************
            '***On each commutation an existing record is updated and a new record is created with
            '***the benefit base data.
            GetWeeklyRate_PPDBenefit_Calcu = 0
            '***
            Select Case Me.RecordCount
                Case Is < 2
                    'have record, no commutation
                    'disability rate can change
                    If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then Exit Function
                    'fetch the minimum and maximum benefits allowed for this state, event date, and effective dis rate
                    lReturn = GetPPDLimits(m_DisabilityRate_Effective)
                    Select Case lReturn
                        Case 0 'expected
                            Me.JurisMaxBenRateWeek = g_dblMaxBenefit
                            Me.MinBenRateWeek = g_dblMinBenefit
                        Case Else 'bad thing
                    End Select

                    Select Case dBenefitAmount
                        Case Is < Me.MinBenRateWeek
                            dBenefitAmount = Me.MinBenRateWeek
                        Case Is > Me.JurisMaxBenRateWeek
                            dBenefitAmount = Me.JurisMaxBenRateWeek
                        Case Else
                            dBenefitAmount = dBenefitAmount
                    End Select
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.JurisMaxBenRateDay = Me.JurisMaxBenRateWeek / objCalcBenefitRule.JurisWorkWeek
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.MinBenRateDay = Me.MinBenRateWeek / objCalcBenefitRule.JurisWorkWeek

                    Me.BenefitRate_Effective = modFunctions.RoundStandard(dBenefitAmount, 2)
                    GetWeeklyRate_PPDBenefit_Calcu = modFunctions.RoundStandard(dBenefitAmount, 2)
                Case Else
                    'have commutation, rate does not change from screen inputs
            End Select

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePPDSLibility
    ' DateTime  : 1/29/2005 12:16
    ' Author    : jtodd22
    ' Purpose   : To return an object that lists the Body Member related libility for the event
    '---------------------------------------------------------------------------------------
    Public Function CalculatePPDSLiability(ByRef objBodyMembers As Object) As Integer
        Const sFunctionName As String = "CalculatePPDSLiability"
        Dim i As Short
        Dim lTest As Integer
        'UPGRADE_NOTE: Rate was upgraded to Rate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
        Dim Rate_Renamed As Double
        Try

            CalculatePPDSLiability = 0
            'beware of a commutation
            Rate_Renamed = objWCPClaim.BenefitRateEffective
            If Rate_Renamed = 0 Then
                'puke
                Exit Function
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LoadDataClaim. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lTest = objBenefitRule.LoadDataClaim(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            If lTest <> -1 Then
                'puke
                Exit Function
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseImpairPercentCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseImpairPercentCode = g_lYesCodeID Then
                With objBodyMembers
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Count. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    For i = 1 To objBodyMembers.Count
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If .Item(i).MaxMonths = 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Item(i).LiabilityAmount = .Item(i).ImpairmentPercentage * .Item(i).MaxWeeks * Rate_Renamed
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBodyMembers.Item. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .Item(i).LiabilityAmount = .Item(i).ImpairmentPercentage * .Item(i).MaxMonths * Rate_Renamed
                        End If
                    Next i
                End With
            End If
            CalculatePPDSLiability = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            CalculatePPDSLiability = Err.Number
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dRate As Double
        Dim lReturn As Integer
        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0
            dRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            'tables have minimum and maximum built in
            'SAWW does not apply to spendable income jurisdictions

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            Me.BenefitRate_PPD = modFunctions.RoundStandard(dRate, 2)

            dGetBasicRate = modFunctions.RoundStandard(dRate, 2)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object colBodyMembers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        colBodyMembers = Nothing
        'UPGRADE_NOTE: Object colClaimantBodyMembers may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        colClaimantBodyMembers = Nothing
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objWCPClaim may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objWCPClaim = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Public Property DisabilityRate_Original() As Double
        Get
            DisabilityRate_Original = m_DisabilityRate_Original
        End Get
        Set(ByVal Value As Double)
            m_DisabilityRate_Original = Value
        End Set
    End Property

    Public Property DisabilityRate_PostComm() As Double
        Get
            DisabilityRate_PostComm = m_DisabilityRate_PostComm
        End Get
        Set(ByVal Value As Double)
            m_DisabilityRate_PostComm = Value
        End Set
    End Property

    Public Property DisabilityRate_Effective() As Double
        Get
            DisabilityRate_Effective = m_DisabilityRate_Effective
        End Get
        Set(ByVal Value As Double)
            m_DisabilityRate_Effective = Value
        End Set
    End Property

    Public Property BeginPrntDate_Original() As String
        Get
            BeginPrntDate_Original = m_BeginPrntDate_Original
        End Get
        Set(ByVal Value As String)
            m_BeginPrntDate_Original = Value
        End Set
    End Property

    Public Property BeginPrntDate_PostComm() As String
        Get
            BeginPrntDate_PostComm = m_BeginPrntDate_PostComm
        End Get
        Set(ByVal Value As String)
            m_BeginPrntDate_PostComm = Value
        End Set
    End Property

    Public Property BeginPrntDate_Effective() As String
        Get
            BeginPrntDate_Effective = m_BeginPrntDate_Effective
        End Get
        Set(ByVal Value As String)
            m_BeginPrntDate_Effective = Value
        End Set
    End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

