Option Strict Off
Option Explicit On
Imports Riskmaster.Db

Public Class CWCCommLife
    Implements _ICalculator

    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "LPCOMM"
    Const sClassName As String = "CWCCommLife"
    Const sDLLClassNameJRRule As String = ""

    Public objBenefitRule As Object
    Public objLP As CWCLifePen
    Public objCalcBenefitRule As Object
    Private oEnumCommLifeMask As CWCEnumCommLifeMask
    Private oEnumLPMask As CWCEnumLPMask
    Private objScreens As CWCEnumEnumDisBenCalcScrn
    Public DateOfCommutation As String
    Public DateOfCommutation_DTG As String
    Public ExactAge As Double
    Public Gender As String
    Public LastBenefitPaidDate As String 'this is default "date of commutation"
    Public BenefitRate_New As Double
    Public LifeCommType As Short
    Public LumpSum As Double
    Public LPRate_New As Double

    Private m_bInitialized As Boolean

    Private Function ClearObject() As Integer
        m_bInitialized = False
        m_EarningsRequiredCode = g_lNoCodeID
        m_CheatSheetTitle = "Life Pension Commutation"
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""



    End Function
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        '****************************************************************************************
        '***General Notes
        '***1) Biggest issue is the ending date of Permanent Partial Benefit as Life Pension
        '***   starts the next day
        '****************************************************************************************
        Const sFunctionName As String = "lCalculatePayment"
        Dim dCommutedPayment As Double
        Dim dDeferredYears As Double
        Dim dDelta1 As Double
        Dim dDelta2 As Double
        Dim dFractionalAge As Double
        Dim dFractionalDeferred As Double
        Dim dHighAgePV_1 As Double
        Dim dHighAgePV_2 As Double
        Dim dInterpolation As Double
        Dim dInterpolationForAge As Double
        Dim dLowAgePV_1 As Double
        Dim dLowAgePV_2 As Double

        Dim dReductionInLPRate As Double
        Dim dPV_LifePension As Double
        Dim dTest As Double
        Dim dTotalWeeksPD As Double
        Dim dDays_1 As Double
        Dim dDays_2 As Double
        Dim iDays As Short
        Dim iDaysPDPayable As Short
        Dim iYearColumn As Short
        Dim sYearColumn As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            If Trim(Me.DateOfCommutation & "") = "" Then
                m_Warning = "Date of Commutation is invalid."
                Exit Function
            End If

            'build top of work sheet
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail

            Me.BenefitRate_New = 0
            'step one for all life pension commutation
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            iDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sDBDateFormat((g_objXClaim.ClaimantBirthDateDTG), "Short Date")), CDate(Me.DateOfCommutation))
            Me.ExactAge = modFunctions.RoundStandard(iDays / 365.24, 3) '1c
            colWorkSheet.Add("     |                                  |", CStr(3))
            colWorkSheet.Add("     |Date of Birth                     |" & sDBDateFormat((g_objXClaim.ClaimantBirthDateDTG), "Short Date"), CStr(4))
            colWorkSheet.Add("     |Life Pension Commencement Date    |" & sDBDateFormat((Me.objLP.BeginPrntDate_Effective), "Short Date"), CStr(6))
            colWorkSheet.Add("     |Date of Commutation (DOC)         |" & sDBDateFormat((Me.DateOfCommutation), "Short Date"), CStr(7))
            colWorkSheet.Add("     |Life Pension Rate                 |" & Format(Me.objLP.BenefitRate_Original, "Currency"), CStr(8))
            If Me.objLP.BenefitRate_PostComm > 0 Then
                colWorkSheet.Add("     |Secondary Life Pension Rate   |" & Format(Me.objLP.BenefitRate_PostComm, "Currency"), CStr(9))
            Else
                colWorkSheet.Add("     |   |", CStr(9))
            End If
            colWorkSheet.Add("     |Gender                            |" & Me.Gender, CStr(10))
            colWorkSheet.Add("     |                                  |", CStr(11))
            colWorkSheet.Add("     |                                  |", CStr(12))
            'build Life Pension Commencement Date
            colWorkSheet.Add("     |                                  |", CStr(13))
            colWorkSheet.Add("0)   |Determine Life Pension Commencement Date   |", CStr(14))
            colWorkSheet.Add("0.a) |Date of Permanent and Stationary Medical   |" & sDBDateFormat((Me.objLP.objPPD.BeginPrntDate_Original), "Short Date"), CStr(15))
            colWorkSheet.Add("0.b) |Days of PPD                                |+" & Me.objLP.TotalDaysOfPPDLiability, CStr(16))
            colWorkSheet.Add("0.c) |Days of Voc Rehab                          |+" & Me.objLP.VocRehabDaysAdvance, CStr(17))
            colWorkSheet.Add("0.d) |Effective Days of Voc Rehab Supplementals  |-" & Me.objLP.VocReHabSuppDaysDelay, CStr(18))
            colWorkSheet.Add("0.e) |Life Pension Commencement Date             |" & Me.DateOfCommutation, CStr(19))
            'build exact age part of work sheet
            colWorkSheet.Add("     |                                  |", CStr(20))
            colWorkSheet.Add("1)   |Determine exact age on date of commutation |", CStr(21))
            colWorkSheet.Add("1.a) |Number of days from DOB through DOC        |" & iDays, CStr(22))
            colWorkSheet.Add("1.b) |Divide by number of days per year          |365.24", CStr(23))
            colWorkSheet.Add("1.c) |Exact age on date of commutation           |" & Me.ExactAge, CStr(24))
            colWorkSheet.Add("     |                                           |", CStr(25))
            colWorkSheet.Add("     |                                           |", CStr(26))
            colWorkSheet.Add("     |                                           |", CStr(27))

            '***********************************
            'step one plus is by type of commutation
            Select Case Me.LifeCommType
                Case 0
                    'default value, does nothing
                Case 1
                    'before payments begin
                    dTotalWeeksPD = Me.objLP.objPPD.WeeksPayable_Original
                    dDays_1 = (dTotalWeeksPD * 7)
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    iDaysPDPayable = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sDBDateFormat((Me.objLP.BeginPrntDate_Effective), "Short Date")), CDate(Me.DateOfCommutation)) + 1
                    dDays_2 = dDays_1 - CDbl(iDaysPDPayable)
                    dDeferredYears = modFunctions.RoundStandard(dDays_2 / 365.24, 3)
                    '2g  calculating procedure break
                    iYearColumn = DateDiff(Microsoft.VisualBasic.DateInterval.Year, CDate(Me.DateOfCommutation), CDate(Me.objLP.BeginPrntDate_Effective))
                    sYearColumn = "YRS_" & Trim(CStr(iYearColumn))
                    dLowAgePV_1 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge))
                    '3a
                    dHighAgePV_1 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge) + 1)
                    dDelta1 = modFunctions.RoundStandard(dLowAgePV_1 - dHighAgePV_1, 2)
                    dFractionalAge = modFunctions.RoundStandard(Me.ExactAge - Int(Me.ExactAge), 3)
                    dInterpolationForAge = modFunctions.RoundStandard(dDelta1 * dFractionalAge, 2)
                    '3e
                    iYearColumn = DateDiff(Microsoft.VisualBasic.DateInterval.Year, CDate(Me.DateOfCommutation), CDate(Me.objLP.BeginPrntDate_Effective))
                    sYearColumn = "YRS_" & Trim(CStr(iYearColumn))
                    dLowAgePV_2 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge))
                    sYearColumn = "YRS_" & Trim(CStr(iYearColumn + 1))
                    dHighAgePV_2 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge))
                    dDelta2 = modFunctions.RoundStandard(dLowAgePV_1 - dHighAgePV_2, 2)
                    dFractionalDeferred = modFunctions.RoundStandard(dDeferredYears - Int(dDeferredYears), 3)
                    dInterpolation = modFunctions.RoundStandard(dDelta2 * dFractionalDeferred, 2)
                    dPV_LifePension = modFunctions.RoundStandard(dLowAgePV_1 - dInterpolationForAge - dInterpolation, 2)
                    '2m  calculating procedure break
                    dCommutedPayment = modFunctions.RoundStandard(dPV_LifePension * Me.objLP.BenefitRate_Original, 2)
                    dTest = dCommutedPayment
                    'build core of work sheet
                    colWorkSheet.Add("     |Lump Sum to be Paid on DOC                                     |", CStr(28))
                    colWorkSheet.Add("2)   |Determine number of years between date of commutation (DOC)    |", CStr(29))
                    colWorkSheet.Add("2.a) |Total weeks of PD                                              |" & dTotalWeeksPD, CStr(30))
                    colWorkSheet.Add("2.b) |Multiplay by 7 days per week                                   | 7", CStr(31))
                    colWorkSheet.Add("2.c) |Total days of PD                                               |" & dDays_1, CStr(32))
                    colWorkSheet.Add("2.d) |Subtract number of days from PD commence through DOC inclusive |" & iDaysPDPayable, CStr(33))
                    colWorkSheet.Add("2.e) |Number of days from DOC to LP commencement                     |" & dDays_2, CStr(34))
                    colWorkSheet.Add("2.f) |Divde by 365.24 days/year                                      |365.24", CStr(35))
                    colWorkSheet.Add("2.g) |Period in years from DOC to start of LP                        |" & dDeferredYears, CStr(36))
                    colWorkSheet.Add("     |                                                               |", CStr(37))
                    colWorkSheet.Add("3)   |Determin PV of Life pension for exact age at date              |", CStr(38))
                    colWorkSheet.Add("3.a) |PV for age below 1c deferred below 2g                          |" & dLowAgePV_1, CStr(39))
                    colWorkSheet.Add("3.b) |Subtract PV for age above 1c deferred below 2g                 |" & dHighAgePV_1, CStr(40))
                    colWorkSheet.Add("3.c) |Difference of 3a and 3b                                        |" & dDelta1, CStr(41))
                    colWorkSheet.Add("3.d) |Multiply by fractional potrion of age at DOC                   |" & dFractionalAge, CStr(42))
                    colWorkSheet.Add("3.e) |Interpolation adjustment for age                               |" & dInterpolationForAge, CStr(43))
                    colWorkSheet.Add("3.f) |PV for age below 1c deferred below 2g                          |" & dLowAgePV_2, CStr(44))
                    colWorkSheet.Add("3.g) |Subtract PV for age below 1c deferred above 2g                 |" & dHighAgePV_2, CStr(45))
                    colWorkSheet.Add("3.h) |Difference of 3f and 3g                                        |" & dDelta2, CStr(46))
                    colWorkSheet.Add("3.i) |Multiply by fractional portion of deferral period(from 3g)     |" & dFractionalDeferred, CStr(47))
                    colWorkSheet.Add("3.j) |Determin PV of Life pension for exact age at date              |" & dInterpolation, CStr(48))
                    colWorkSheet.Add("3.k) |PV for age below 1c deferred below 2g                          |" & dLowAgePV_1, CStr(49))
                    colWorkSheet.Add("3.l) |Subtract sum of 3e and 3j                                      |" & -dInterpolationForAge - dInterpolation, CStr(50))
                    colWorkSheet.Add("3.m) |PV of life pension (for age " & Me.ExactAge & " deferred " & dDeferredYears & " years |" & dPV_LifePension, CStr(51))
                    colWorkSheet.Add("     |                                                                  |", CStr(52))
                    colWorkSheet.Add("4)   |Determine commuted value of all LP as of DOC                      |", CStr(53))
                    colWorkSheet.Add("4.a) |PV of life pension(from 3m)                               |" & dPV_LifePension, CStr(54))
                    colWorkSheet.Add("4.b) |Multiply by LP rate                                       |" & Format(Me.objLP.BenefitRate_Original, "Currency"), CStr(55))
                    colWorkSheet.Add("4.c) |Commuted value of all life pension                        |" & Format(dCommutedPayment, "Currency"), CStr(56))
                Case 2
                    'after payments begin
                    iYearColumn = 0
                    sYearColumn = "YRS_" & Trim(CStr(iYearColumn))
                    dLowAgePV_1 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge))
                    dHighAgePV_1 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge) + 1)
                    dDelta1 = modFunctions.RoundStandard(dLowAgePV_1 - dHighAgePV_1, 2)
                    dInterpolation = modFunctions.RoundStandard(dDelta1 * modFunctions.RoundStandard(Me.ExactAge - Int(Me.ExactAge), 3), 2)
                    dPV_LifePension = modFunctions.RoundStandard(dLowAgePV_1 - dInterpolation, 2)
                    'dPV_LifePension is 2h
                    '**********************************
                    If Me.objLP.BenefitRate_PostComm > 0 Then
                        dCommutedPayment = modFunctions.RoundStandard(dPV_LifePension * Me.objLP.BenefitRate_PostComm, 2)
                    Else
                        dCommutedPayment = modFunctions.RoundStandard(dPV_LifePension * Me.objLP.BenefitRate_Original, 2)
                    End If
                    dTest = dCommutedPayment
                    'build core of work sheet
                    colWorkSheet.Add("     |                                                        |", CStr(28))
                    colWorkSheet.Add("2)   |Determine PV of life pension as of exact age of DOC     |", CStr(29))
                    colWorkSheet.Add("2.a) |PV for age in table below 1c                            |" & dLowAgePV_1, CStr(30))
                    colWorkSheet.Add("2.b) |PV for age in table above 1c                            |" & dHighAgePV_1, CStr(31))
                    colWorkSheet.Add("2.c) |Difference of 2a and 2b                                 |" & dDelta1, CStr(32))
                    colWorkSheet.Add("2.d) |Multiply by fractional portion of age from 1c           |" & modFunctions.RoundStandard(Me.ExactAge - Int(Me.ExactAge), 3), CStr(33))
                    colWorkSheet.Add("2.e) |Interpolation adjustment for 2d                         |" & dInterpolation, CStr(34))
                    colWorkSheet.Add("2.f) |PV for age in table below 1c (from 2a)                  |" & dLowAgePV_1, CStr(35))
                    colWorkSheet.Add("2.g) |Subtract 2e                                             |" & dInterpolation, CStr(36))
                    colWorkSheet.Add("2.h) |PV of life pension as of exact age on DOC               |" & dPV_LifePension, CStr(37))
                    colWorkSheet.Add("     |                                                        |", CStr(38))
                    colWorkSheet.Add("3)   |Determine commuted value of all life pension indemnity  |", CStr(39))
                    colWorkSheet.Add("3.a) |PV of life pension as of exact age on DOC (2h)          |" & dPV_LifePension, CStr(40))
                    If Me.objLP.BenefitRate_PostComm > 0 Then
                        colWorkSheet.Add("3.b) |Multiply by life pension rate                           |" & Format(Me.objLP.BenefitRate_PostComm, "Currency"), CStr(41))
                    Else
                        colWorkSheet.Add("3.b) |Multiply by life pension rate                           |" & Format(Me.objLP.BenefitRate_Original, "Currency"), CStr(41))
                    End If
                    colWorkSheet.Add("3.c) |Commuted value of all life pension due after DOC        |" & Format(dCommutedPayment, "Currency"), CStr(42))
                Case 3
                    'Uniform reduction of life pension payments
                    sYearColumn = "YRS_0"

                    dLowAgePV_1 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge))
                    dHighAgePV_1 = f_dGetLifeBenefitPV_Age(sYearColumn, Int(Me.ExactAge) + 1)
                    dDelta1 = modFunctions.RoundStandard(dLowAgePV_1 - dHighAgePV_1, 2)
                    dInterpolation = modFunctions.RoundStandard(dDelta1 * (Me.ExactAge - Int(Me.ExactAge)), 2)
                    dPV_LifePension = modFunctions.RoundStandard(dLowAgePV_1 - dInterpolation, 2) '2h
                    dReductionInLPRate = modFunctions.RoundStandard(Me.LumpSum / dPV_LifePension, 2) '3c

                    Me.LPRate_New = Me.objLP.BenefitRate_Effective - dReductionInLPRate '4c
                    If Me.LPRate_New < 0.005 Then
                        Me.LumpSum = modFunctions.RoundStandard(Me.objLP.BenefitRate_Effective * dPV_LifePension, 2)
                        m_ErrorMask = oEnumCommLifeMask.clSubZeroNewRate
                        m_Note = "The new Benefit rate is less than $0.00." & vbCrLf & "Please reduce the Lump Sum to less than " & Format(Me.LumpSum, "Currency") & "."
                        Exit Function
                    Else
                    End If

                    dCommutedPayment = Me.LumpSum
                    'build core of work sheet
                    colWorkSheet.Add("     |Lump Sum to be paid on DOC                           |" & Format(Me.LumpSum, "Currency"), CStr(11))
                    'array index is alway 25 at this point and colWorkSheet.add (25) <> ""
                    colWorkSheet.Add("2)   |Determine PV of life pension as of exact age of DOC  |", CStr(28))
                    colWorkSheet.Add("2.a) |PV for age in table below 1c                         |" & dLowAgePV_1, CStr(29))
                    colWorkSheet.Add("2.b) |PV for age in table aboue 1c                         |" & dHighAgePV_1, CStr(30))
                    colWorkSheet.Add("2.c) |Difference of 2a and 2b                              |" & dDelta1, CStr(31))
                    colWorkSheet.Add("2.d) |Multiply by fractional portion of age from 1c        |" & modFunctions.RoundStandard(Me.ExactAge - Int(Me.ExactAge), 3), CStr(32))
                    colWorkSheet.Add("2.e) |Interpolation adjustment for 2d                      |" & dInterpolation, CStr(33))
                    colWorkSheet.Add("2.f) |PV for age in table below 1c (from 2a)               |" & dLowAgePV_1, CStr(34))
                    colWorkSheet.Add("2.g) |Subtract 2e                                          |" & dInterpolation, CStr(35))
                    colWorkSheet.Add("2.h) |PV of life pension as of exact age on DOC            |" & dPV_LifePension, CStr(36))
                    colWorkSheet.Add("     |                                                        |", CStr(37))
                    colWorkSheet.Add("3)   |Calculate amount of reduction in LP rate             |", CStr(38))
                    colWorkSheet.Add("3.a) |Amount to be commuted                                |" & Format(Me.LumpSum, "Currency"), CStr(39))
                    colWorkSheet.Add("3.b) |Divide by PV of LP (2h)                              |" & dPV_LifePension, CStr(40))
                    colWorkSheet.Add("3.c) |Amount of weekly reduction in LP                     |" & Format(dReductionInLPRate, "Currency"), CStr(41))
                    colWorkSheet.Add("     |                                                        |", CStr(42))
                    colWorkSheet.Add("4)   |Calculate LP rate after commutation                  |", CStr(43))
                    If Me.objLP.BenefitRate_PostComm > 0 Then
                        colWorkSheet.Add("4.a) |LP rate before commutation                           |" & Format(Me.objLP.BenefitRate_PostComm, "Currency"), CStr(44))
                    Else
                        colWorkSheet.Add("4.a) |LP rate before commutation                           |" & Format(Me.objLP.BenefitRate_Original, "Currency"), CStr(44))
                    End If
                    colWorkSheet.Add("4.b) |Subtract weekly reduction in LP (3c)                 |" & Format(dReductionInLPRate, "Currency"), CStr(45))
                    colWorkSheet.Add("4.c) |LP rate after commutation                            |" & Format(Me.LPRate_New, "Currency"), CStr(46))

                Case Else
            End Select
            Me.Standard.CalculatedPaymentRegular = dCommutedPayment
            lCalculatePayment = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            m_Note = m_Note & Mid(m_Note, 3)
            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Private Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"

        'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
        'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week

        dGetBasicRate = 0
    End Function
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim lReturn As Integer
        Dim sSQL As String
        'Dim objReader As dbReader
        Dim objReader As DbReader
        Dim m_Note As String
        Dim dLastBenefitPaidDate As Date

        Try

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            m_Note = ""

            If Trim(sDLLClassNameJRRule) = "" Then
                lLoadData = g_objXErrorMask.cNoJurisdictionalRule
                m_ErrorMask = g_objXErrorMask.cNoJurisdictionalRule
                Exit Function
            End If

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function

            oEnumCommLifeMask = New CWCEnumCommLifeMask
            oEnumCommLifeMask.SetConstantValues()

            oEnumLPMask = New CWCEnumLPMask
            oEnumLPMask.SetConstantValues()

            objScreens = New CWCEnumEnumDisBenCalcScrn
            objScreens.SetConstantValues()

            objLP = New CWCLifePen
            objLP.objPPD = New CWCPermPartDis
            objLP.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)

            With objLP.objPPD
                If .RecordCount = 0 Then
                    m_ErrorMask = m_ErrorMask + oEnumCommLifeMask.clNoPPD
                    m_Note = m_Note & vbCrLf & "A Permanent Partial Disability is required before you can have a Life Pension." & "Please create the Permanent Partial disability before using Life Pension."
                End If
                If .DisabilityRate_Effective < 70 Then
                    m_ErrorMask = m_ErrorMask + oEnumLPMask.clpLowDisRate
                    m_Note = m_Note & vbCrLf & "This claim does not have a Life Pension Benefit (disability rate is <70%)."
                End If
            End With

            'birth date and gender are required for the computation
            If Len(Trim(g_objXClaim.ClaimantBirthDateDTG)) = 0 Or Not IsDate(GetFormattedDate(g_objXClaim.ClaimantBirthDateDTG)) Then
                m_Note = m_Note & vbCrLf & "Claimaint lacks a valid birth date in database; unable to calculate payment."
                m_ErrorMask = m_ErrorMask + oEnumCommLifeMask.clNoBirthdate
            End If

            If g_objXClaim.ClaimantSexCode < 1 Then
                m_Note = m_Note & vbCrLf & "Claimant lacks sexcode; unable to calculate payment."
                m_ErrorMask = m_ErrorMask + oEnumCommLifeMask.clNoGender
            Else
                sSQL = "SELECT CODES.SHORT_CODE FROM CODES WHERE CODE_ID = " & g_objXClaim.ClaimantSexCode
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                If (objReader.Read()) Then
                    Me.Gender = objReader.GetString(0)
                Else
                    Err.Raise(7000, "", "Unable to retrieve sex for sexcode, " & g_objXClaim.ClaimantSexCode & ".")
                End If
            End If

            If m_ErrorMask > 0 Then
                'fatal errors
                m_ErrorMask = m_ErrorMask
                m_Note = m_Note
                Exit Function
            End If

            If objLP.Standard.ErrorMask And oEnumLPMask.clpNoBenefitRate Then m_ErrorMask = m_ErrorMask + oEnumCommLifeMask.clNoBenefitRate
            If objLP.Standard.ErrorMask And oEnumLPMask.clpBadBenefitStartDate Then m_ErrorMask = m_ErrorMask + oEnumCommLifeMask.clBadBenefitStartDate
            m_Note = m_Note & vbCrLf & objLP.WarningMessage


            Me.LastBenefitPaidDate = GetLastDayBenefitPaid(g_objXClaim.ClaimID, g_objXClaim.FilingStateID, objScreens.cLifePension)
            If Len(Trim(Me.LastBenefitPaidDate)) > 0 Then
                dLastBenefitPaidDate = CDate(GetFormattedDate(Me.LastBenefitPaidDate))

                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                If DateDiff(Microsoft.VisualBasic.DateInterval.Day, dLastBenefitPaidDate, Today) > 30 Then
                    m_Note = m_Note & vbCrLf & "The Commutation and Check dates are over 30 days apart." & "Please insure they are correct."
                    m_ErrorMask = m_ErrorMask + oEnumCommLifeMask.cl30PlusDayWarning
                End If
            End If

            m_bInitialized = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            If Not (objReader Is Nothing) Then
                objReader.Close()
                objReader.Dispose()
            End If
            m_Note = m_Note & Mid(m_Note, 3)
            m_ErrorMask = m_ErrorMask
            lLoadData = g_lErrNum

        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objLP may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objLP = Nothing
        'UPGRADE_NOTE: Object oEnumCommLifeMask may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oEnumCommLifeMask = Nothing
        'UPGRADE_NOTE: Object oEnumLPMask may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oEnumLPMask = Nothing
        'UPGRADE_NOTE: Object objScreens may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objScreens = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function

    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

