Option Strict Off
Option Explicit On
Imports Riskmaster.Db
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
Public Class CWCTempTotlDisDualRate
    Implements _ICalculator
    '---------------------------------------------------------------------------------------
    ' Module    : CWCTempTotlDisDualRate
    ' DateTime  : 3/22/2005 13:18
    ' Author    : jtodd22
    ' Purpose   : Beware that there are two different types of jurisdictions for Workers
    ' ..........: Compensation; jurisdictions that work from a discounted AWW and jurisdictions
    ' ..........: that work from a Spendable Income lookup table.  We are using only a discounted
    ' ..........: jurisdiction in this module.
    ' Note......: This is aimed at Ohio.
    '---------------------------------------------------------------------------------------
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "TTD"
    Const sClassName As String = "CWCTempTotlDisDualRate"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleTTDAA"
    Const m_sBenefitAbbs As String = "TTD,TPD"
    '******************************************************************************************************************
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object

    Private m_CalculatedPayment As Double
    Private m_CalculatedPayment_01 As Double
    Private m_CalculatedPayment_02 As Double
    Private m_ClaimantJurisAWW As Double

    Private m_BenefitRate_Effective As Double
    Private m_BenefitRate_Original As Double
    Private m_BenefitRate_TTD As Double
    Private m_BenefitRate_TTD_CA_PrimeRate As Double
    Private m_BenefitRate_TPD As Double

    Private m_DaysToPaymentDue As Short

    Private m_PayLateCharge As Short

    Private m_PrimeRateStartDateCalendar As String
    Private m_PrimeRateStartDateDTG As String


    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 1/5/2005 13:48
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    '
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Try
            Dim objReader As DbReader
            Dim dblNewClaimantOriginalAWW As Double
            Dim sStartDate As String
            Dim dbl As Double
            Dim dDateOfEvent As Date
            Dim lReturn As Integer
            Dim sDateOfEvent_DTG As String
            Dim sDummyDate_DTG As String
            Dim sTemp As String

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)
            'jlt 07/29/2003  cap aww if over the jurisdiction max aww
            g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If g_objXClaim.ClaimantOriginalAWW > objBenefitRule.MaxAWW And objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If g_objXClaim.AWWToCompensate > objBenefitRule.MaxAWW Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    g_objXClaim.AWWToCompensate = objBenefitRule.MaxAWW
                End If
            End If


            lLoadData = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:32
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim dDaysToPrimeRate As Double
        Dim dMoreWeeks As Double
        Dim objReader As DbReader
        Dim dblMaxAWW As Double
        Dim dblMinBenefit As Double
        Dim dbl As Double
        Dim lReturn As Integer
        Dim sTemp As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            If m_EarningsRequiredCode = g_lYesCodeID Then
                If g_objXPaymentParms.EarningsPerWeek = 0 And g_objXPaymentParms.EarningsPerMonth = 0 Then
                    m_Warning = "Claimant does not have earnings and earnings are required."
                    Exit Function
                End If
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = "Claimant has been paid or has pending payment " & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks of TTP or TPD." & vbCrLf & "There is no Temporary liability left to pay."
                    Exit Function
                End If
            End If

            'build top of work sheet
            If colWorkSheet.Count() = 0 Then
                'build top of work sheet
                modFunctions.BuildTopOfWorkSheet(colWorkSheet)
                'build benefit type detail
            Else
                colWorkSheet.Add("||")
            End If


            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                colWorkSheet.Add("|State Imposed Max AWW Limit                       |" & Format(objBenefitRule.MaxAWW, "Currency"))
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.DollarForDollar <> g_lYesCodeID Then
                sTemp = ""
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTemp = sTemp & "Jurisdiction imposed a minimum TTD Rate of " & Format(objBenefitRule.FloorAmount, "Currency")
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDateDTG. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTemp = sTemp & " per week for events after " & sDBDateFormat(objBenefitRule.EffectiveDateDTG, "Short Date")
                sTemp = sTemp & ".  For calculations this is the same as a minimum AWW of "
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTemp = sTemp & Format(objBenefitRule.FloorAmount / objBenefitRule.PrimeRate, "Currency") & " per Week."
                colWorkSheet.Add("|" & sTemp & "|")

                colWorkSheet.Add("|Claimant AWW (After Imposing State Cap and Floor Limits)      |" & Format(g_objXClaim.AWWToCompensate, "Currency"))
            Else
                colWorkSheet.Add("|Claimant AWW As Capped (State Imposed Limit)      |" & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If

            'deduct the earnings for partial disalbility
            g_objXClaim.AWWToCompensate = g_objXClaim.AWWToCompensate - g_objXPaymentParms.EarningsPerWeek
            If g_objXClaim.AWWToCompensate < 0 Then
                g_objXClaim.AWWToCompensate = 0
                m_ErrorMask = g_objXErrorMask.cNoEarnings
                Exit Function
            End If

            'Begin sorting out the rates versus payments
            sTemp = modFunctions.GetBenefitLookUpIDs("TTD", "TPD")
            m_PaidPendingWeeks = GetBenefitWeeksPaidOrPending(sTemp)
            dMoreWeeks = g_objXPaymentParms.BenefitDays / 7
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If m_PaidPendingWeeks > objBenefitRule.SecondRateMaxWeeks Then
                'second rate is not a consideration
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If (m_PaidPendingWeeks + dMoreWeeks) < objBenefitRule.SecondRateMaxWeeks Then
                'prime rate is not a consideration
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If m_PaidPendingWeeks < objBenefitRule.SecondRateMaxWeeks And ((m_PaidPendingWeeks + dMoreWeeks) > objBenefitRule.SecondRateMaxWeeks) Then
                m_ErrorMask = g_objXErrorMask.cSplitPayment
            End If
            Select Case m_ErrorMask
                Case g_objXErrorMask.cSplitPayment
                    're-calculate split data
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dDaysToPrimeRate = (objBenefitRule.SecondRateMaxWeeks - m_PaidPendingWeeks) * 7
                    With g_objXPaymentParms
                        If .CatchUpDays > dDaysToPrimeRate Then
                            .CatchUpEndDate02 = .CatchUpEndDate
                        End If
                    End With
                    Err.Raise(80000, sClassName & "." & sFunctionName, "Coding is not complete for payment split by Two Year Rule")
                    Exit Function
                Case Else
                    'calculate payment that is not across the two year date or two year rule does not apply
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Select Case objBenefitRule.FloorAmount
                        Case 0
                            'There is not valid floor, note; this could go to zero
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dbl = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * objBenefitRule.PrimeRate, 2)
                            Me.BenefitRate_Effective = dbl
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Me.CalculatedPayment = ((dbl / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays)
                        Case Is >= g_objXClaim.AWWToCompensate
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                                dbl = modFunctions.RoundStandard((g_objXClaim.AWWToCompensate), 2)
                                Me.BenefitRate_Effective = dbl
                                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Me.CalculatedPayment = ((dbl / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays)
                            End If
                        Case Is < g_objXClaim.AWWToCompensate
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dbl = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * objBenefitRule.PrimeRate, 2)
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If objBenefitRule.FloorAmount > dbl Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dbl = objBenefitRule.FloorAmount
                                Me.BenefitRate_Effective = dbl
                                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Me.CalculatedPayment = ((dbl / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays)
                            End If
                    End Select
                    colWorkSheet.Add("     |Original Weekly Rate is|" & Format(Me.BenefitRate_Original, "Currency"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.FloorAmount > 0 And objBenefitRule.DollarForDollar <> g_lYesCodeID Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDate_dtg. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        colWorkSheet.Add("|Jurisdiction imposed floor for Events after " & sDBDateFormat(objBenefitRule.EffectiveDate_dtg, "Short Date") & " is " & Format(objBenefitRule.FloorAmount, "Currency") & " per week|")
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        colWorkSheet.Add("|The Effective weekly rate is| " & Format(objBenefitRule.FloorAmount, "Currency"))
                    Else
                        colWorkSheet.Add("     |The Effective Weekly Rate is|" & Format(Me.BenefitRate_Effective, "Currency"))
                    End If
                    colWorkSheet.Add("     |Payment is|" & Format(Me.CalculatedPayment, "Currency"))
            End Select

            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Public Function ReLoadData_PrimeRateRule() As Integer
        Const sFunctionName As String = "ReLoadData_PrimeRateRule"
        Try
            Dim objReader As DbReader
            Dim sSQL As String
            Dim g_dblMaxBenefit As Double
            Dim dblMaxAWW As Double
            Dim dblMinBenefit As Double
            Dim sStartDate As String
            Dim dblCalculatedPayment As Double
            Dim dbl As Double
            Dim dDateOfEvent As Date

            sSQL = "SELECT MAX_BENEFIT, MAX_AWW, MIN_BENEFIT FROM WCP_RULE_TTD" & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND '" & Me.PrimeRateStartDateDTG & "' >= BEGIN_DATE" & " ORDER BY BEGIN_DATE DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                g_dblMaxBenefit = objReader.GetInt32("MAX_BENEFIT")
                dblMaxAWW = objReader.GetInt32("MAX_AWW")
                dblMinBenefit = objReader.GetInt32("MIN_BENEFIT")
            Else
                Err.Raise(vbObjectError + 7000, "ReLoadData_PrimeRateRule", "Unable to retrieve TTD Rates")
            End If


            g_objXClaim.AWWToCompensate = f_dGetClaimantWeeklyWage((g_objXClaim.ClaimID), 0)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objBenefitRule.MaxAWW = dblMaxAWW

            Select Case UCase(GetStatePostalCode_SQL((g_objXClaim.FilingStateID)))
                Case "CA"
                    'jlt, 03/23/2001 California uses the employee AWW (m_AWW_WeeklyRate) directly in
                    'calculation where the AWW is <= a state set value (m_dttdJurisdictionMaxAww) otherwise the
                    'state set max value (m_dttdJurisdictionMaxAww) is used
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If g_objXClaim.AWWToCompensate > objBenefitRule.MaxAWW Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        g_objXClaim.AWWToCompensate = objBenefitRule.MaxAWW
                    Else
                    End If
                    Me.BenefitRate_Effective = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * 2 / 3, 2)
                    'jlt 10/09/2002 California says if the Claimant's AWW is below the min. rate use the AWW
                    ' as the rate, in the calculation of payment insure that a Claimant's payment does not go below the
                    ' min rate if it calculates below the min.
                    Select Case g_objXClaim.AWWToCompensate
                        Case Is <= dblMinBenefit
                            Me.BenefitRate_Effective = modFunctions.RoundStandard((g_objXClaim.AWWToCompensate), 2)
                        Case Is <= (dblMinBenefit * 1.5)
                            Me.BenefitRate_Effective = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * 2 / 3, 2)
                            If Me.BenefitRate_Effective < dblMinBenefit Then
                                Me.BenefitRate_Effective = dblMinBenefit
                            End If
                        Case Else
                    End Select
                Case Else
            End Select
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            ReLoadData_PrimeRateRule = g_lErrNum

        End Try

    End Function
    Private Function ClearObject() As Integer
        m_ClaimantJurisAWW = -0.01
        m_BenefitRate_Effective = -0.01
        m_BenefitRate_TTD = -0.01
        m_BenefitRate_TTD_CA_PrimeRate = -0.01
        m_BenefitRate_TPD = -0.01
        m_CalculatedPayment_01 = -0.01
        m_CalculatedPayment_02 = 0
        m_CalculatedPaymentLateCharge = 0
        'jtodd22 do not reset BenefitDays it is set in calling executable
        m_CheatSheetTitle = "Temporary Total"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function

    Public Function lCalculatePayment_CaPrimeRateSplitPayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "CalculatePayment_CaPrimeRateSplitPayment"
        Try
            Dim lReturn As Integer
            Dim sBenefitEndDate As String
            Dim sBenefitStartDate As String
            Dim sTemp As String

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'build top of work sheet
            colWorkSheet.Add("     |Claim Number                                      |" & g_objXClaim.ClaimNumber, CStr(1))
            colWorkSheet.Add("     |Claimant Name                                     |" & g_objXClaim.ClaimantNameLFM, CStr(2))
            colWorkSheet.Add("     |                                                  |", CStr(3))

            colWorkSheet.Add("     |Date of Injury                                    |" & g_objXClaim.DateOfEventCalar, CStr(4))
            colWorkSheet.Add("     |Claimant AWW                                      |" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            colWorkSheet.Add("     |State Imposed Limit                               |" & Format(objBenefitRule.MaxAWW, "Currency"))
            colWorkSheet.Add("     |Claimant AWW As Capped (State Imposed Limit)      |" & Format(g_objXClaim.AWWToCompensate, "Currency"))

            colWorkSheet.Add("     |Benefit Payment Start Date                        |" & CDate(g_objXPaymentParms.BenefitStartDate), CStr(5))
            colWorkSheet.Add("     |Benefit Payment End Date                          |" & CDate(g_objXPaymentParms.BenefitEndDate), CStr(6))
            colWorkSheet.Add("||")
            colWorkSheet.Add("     |Two Year Anniversary Date                         | " & Me.PrimeRateStartDateCalendar)
            '**********************************************************************************************************
            sBenefitEndDate = g_objXPaymentParms.BenefitEndDate
            sBenefitStartDate = g_objXPaymentParms.BenefitStartDate
            g_objXPaymentParms.BenefitEndDate = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, -1, CDate(Me.PrimeRateStartDateCalendar)))
            lCalculatePayment_CA_Split()
            CalculatedPayment_01 = Me.CalculatedPayment
            colWorkSheet.Add("||")
            colWorkSheet.Add("|First Part Payment Start Date|" & g_objXPaymentParms.BenefitStartDate)
            colWorkSheet.Add("|First Part Payment End Date|" & g_objXPaymentParms.BenefitEndDate)
            colWorkSheet.Add("|First Part Benefit Days|" & g_objXPaymentParms.BenefitDays)
            colWorkSheet.Add("|First Part Weekly Rate|" & Format(Me.BenefitRate_Effective, "Currency"))
            colWorkSheet.Add("|First Part Benefit Payment|" & Format(Me.CalculatedPayment, "Currency"))
            '*************************************************************************************
            g_objXPaymentParms.BenefitStartDate = Me.PrimeRateStartDateCalendar
            g_objXPaymentParms.BenefitEndDate = sBenefitEndDate
            lCalculatePayment_CA_Split()
            CalculatedPayment_02 = Me.CalculatedPayment
            colWorkSheet.Add("||")
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            colWorkSheet.Add("|Second Part State Imposed Limit|" & Format(objBenefitRule.MaxAWW, "Currency"))
            colWorkSheet.Add("|Second Part Claimant AWW As Capped (State Imposed Limit)|" & Format(g_objXClaim.AWWToCompensate, "Currency"))
            colWorkSheet.Add("|Second Part Payment Start Date|" & g_objXPaymentParms.BenefitStartDate)
            colWorkSheet.Add("|Second Part Payment End Date|" & g_objXPaymentParms.BenefitEndDate)
            colWorkSheet.Add("|Second Part Benefit Days|" & g_objXPaymentParms.BenefitDays)
            colWorkSheet.Add("|Second Part Weekly Rate|" & Format(Me.BenefitRate_Effective, "Currency"))
            colWorkSheet.Add("|Second Part Benefit Payment|" & Format(Me.CalculatedPayment, "Currency"))
            '*****************************************************************************
            CalculatedPayment = Me.CalculatedPayment_01 + Me.CalculatedPayment_02
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Benefit Payment Total|" & Format(Me.CalculatedPayment, "Currency"))
            g_objXPaymentParms.BenefitEndDate = sBenefitEndDate
            g_objXPaymentParms.BenefitStartDate = sBenefitStartDate
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            g_objXPaymentParms.BenefitDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1

        Catch ex As Exception

        Finally
        End Try

    End Function
    Public Function lCalculatePayment_CA_Split() As Integer
        Const sFunctionName As String = "lCalculatePayment_CA_Split"

        Dim objReader As DbReader
        Dim sSQL As String
        Dim g_dblMaxBenefit As Double
        Dim dblMaxAWW As Double
        Dim dblMinBenefit As Double
        Dim dblCalculatedPayment As Double
        Dim dbl As Double
        Dim dDateOfEvent As Date
        Try
            lCalculatePayment_CA_Split = 0
            'check for fatal errors
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXClaim.DateOfEventCalar)) > 0 Then
                Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit start date(" & g_objXPaymentParms.BenefitStartDate & ") cannot precede event date(" & g_objXClaim.DateOfEventCalar & ")")
            End If
            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            If DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitEndDate), CDate(g_objXPaymentParms.BenefitStartDate)) > 0 Then
                Err.Raise(vbObjectError + 7000, "CalculatePayment", "Benefit end date(" & g_objXPaymentParms.BenefitEndDate & ") cannot precede the benefit start date date(" & g_objXPaymentParms.BenefitStartDate & ")")
            End If

            'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
            g_objXPaymentParms.BenefitDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1
            Select Case GetStatePostalCode_SQL((g_objXClaim.FilingStateID))
                Case "CA"
                    'use CA Labor Code 4661.5
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Select Case g_objXClaim.AWWToCompensate
                        Case Is < objBenefitRule.FloorAmount
                            'jlt 04/10/2001 aww is below state floor (first level)
                            'benefit is dollar for dollar
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((g_objXClaim.AWWToCompensate / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays)
                        Case Is < (objBenefitRule.FloorAmount * 1.5)
                            'jlt 04/10/2001 aww is over state floor and below second level
                            'benefit is at the Jurisdiction Min
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((g_objXClaim.AWWToCompensate / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays)
                            If dblCalculatedPayment < 0.01 Then
                                dblCalculatedPayment = 0
                            End If
                        Case Else
                            dbl = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * 2 / 3, 2) 'same as base rate
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((dbl / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays)
                    End Select
                    If CDate(Me.PrimeRateStartDateCalendar) > CDate("12/31/2002") And GetStatePostalCode_SQL((g_objXClaim.FilingStateID)) = "CA" Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dblCalculatedPayment < ((126 / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays) Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dblCalculatedPayment = ((126 / objCalcBenefitRule.JurisWorkWeek) * g_objXPaymentParms.BenefitDays)
                            'colWorkSheet.Add "|California imposed floor for Events after 12/31/2002 (or Two Year Rule) is $126 per week|"
                        End If
                    Else
                    End If
                Case Else
            End Select
            Me.CalculatedPayment = dblCalculatedPayment
            If Me.CalculatedPayment < 0.01 Then
                g_lErrNum = g_objXErrorMask.cNoPaymentDue
                Exit Function
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lCalculatePayment_CA_Split = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dMaxRate As Double
        Dim lReturn As Integer

        Try

            dGetBasicRate = 0
            dMaxRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            'discounted jurisdiction
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.SecondRate > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If m_PaidPendingWeeks < objBenefitRule.SecondRateMaxWeeks Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    BenefitRate_TTD = modFunctions.RoundStandard(AverageWage * objBenefitRule.SecondRate, 2)
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    BenefitRate_TTD = modFunctions.RoundStandard(AverageWage * objBenefitRule.PrimeRate, 2)
                End If
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                BenefitRate_TTD = modFunctions.RoundStandard(AverageWage * objBenefitRule.PrimeRate, 2)
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (AverageWage < objBenefitRule.FloorAmount) Then
                        BenefitRate_TTD = AverageWage
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If (BenefitRate_TTD < objBenefitRule.FloorAmount) Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            BenefitRate_TTD = objBenefitRule.FloorAmount
                        End If
                    End If
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (BenefitRate_TTD < objBenefitRule.FloorAmount) Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        BenefitRate_TTD = objBenefitRule.FloorAmount
                    End If
                End If
            End If
            With objBenefitRule
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If .MaxCompRate > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If BenefitRate_TTD > .MaxCompRate Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        BenefitRate_TTD = .MaxCompRate
                    End If
                End If
            End With
            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(BenefitRate_TTD, 2)
        Catch ex As TypeLoadException
            'hLoadDataError:
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing



        End Try

    End Function


    Public Property DaysToPaymentDue() As Short
        Get
            DaysToPaymentDue = m_DaysToPaymentDue
        End Get
        Set(ByVal Value As Short)
            m_DaysToPaymentDue = Value
        End Set
    End Property

    Public Property CalculatedPayment() As Double
        Get
            CalculatedPayment = m_CalculatedPayment
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPayment = Value
        End Set
    End Property

    Public Property CalculatedPayment_01() As Double
        Get
            CalculatedPayment_01 = m_CalculatedPayment_01
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPayment_01 = Value
        End Set
    End Property

    Public Property CalculatedPayment_02() As Double
        Get
            CalculatedPayment_02 = m_CalculatedPayment_02
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPayment_02 = Value
        End Set
    End Property

    Public Property ClaimantJurisAWW() As Double
        Get
            ClaimantJurisAWW = m_ClaimantJurisAWW
        End Get
        Set(ByVal Value As Double)
            m_ClaimantJurisAWW = Value
        End Set
    End Property


    Public Property BenefitRate_Effective() As Double
        Get
            BenefitRate_Effective = m_BenefitRate_Effective
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_Effective = Value
        End Set
    End Property


    Public Property BenefitRate_Original() As Double
        Get
            BenefitRate_Original = m_BenefitRate_Original
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_Original = Value
        End Set
    End Property


    Public Property BenefitRate_TTD() As Double
        Get
            BenefitRate_TTD = m_BenefitRate_TTD
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_TTD = Value
        End Set
    End Property


    Public Property BenefitRate_TTD_CA_PrimeRate() As Double
        Get
            BenefitRate_TTD_CA_PrimeRate = m_BenefitRate_TTD_CA_PrimeRate
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_TTD_CA_PrimeRate = Value
        End Set
    End Property


    Public Property BenefitRate_TPD() As Double
        Get
            BenefitRate_TPD = m_BenefitRate_TPD
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_TPD = Value
        End Set
    End Property



    'Public Property CalculatedPaymentLateCharge() As Double
    '    Get
    '        CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
    '    End Get
    '    Set(ByVal Value As Double)
    '        m_CalculatedPaymentLateCharge = Value
    '    End Set
    'End Property

    Public Property PayLateCharge() As Short
        Get
            PayLateCharge = m_PayLateCharge
        End Get
        Set(ByVal Value As Short)
            m_PayLateCharge = Value
        End Set
    End Property

    Public ReadOnly Property PrimeRateStartDateCalendar() As String
        Get

            Const sFunctionName As String = "Property Get PrimeRateStartDateDTG"
            Dim dDaysToPrimeRate As Double
            Dim sTemp As String
            Try
                If objBenefitRule Is Nothing Then lLoadData(g_objXClaim.ClaimantHourlyPayRate)
                If m_PaidPendingWeeks = 0 Then
                    sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitAbbs, "")
                    m_PaidPendingWeeks = GetBenefitWeeksPaidOrPending(sTemp)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dDaysToPrimeRate = (objBenefitRule.SecondRateMaxWeeks - m_PaidPendingWeeks) * 7
                dDaysToPrimeRate = modFunctions.RoundStandard(dDaysToPrimeRate + 0.49, 0)
                m_PrimeRateStartDateCalendar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, dDaysToPrimeRate, CDate(g_objXPaymentParms.BenefitStartDate)))
                'UPGRADE_WARNING: Couldn't resolve default property of object modFunctions.PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                m_PrimeRateStartDateCalendar = modFunctions.PadDateWithZeros(m_PrimeRateStartDateCalendar)
                PrimeRateStartDateCalendar = m_PrimeRateStartDateCalendar
            Catch ex As Exception
                With Err()
                    g_lErrNum = Err.Number
                    g_sErrSrc = .Source
                    g_sErrDescription = Err.Description
                End With
                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                g_lErrLine = Erl()
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
            End Try
        End Get
    End Property
    ''Public Property Let PrimeRateStartDateCalendar(ByVal vData As String)
    ''    m_PrimeRateStartDateCalendar = vData
    ''End Property

    Public ReadOnly Property PrimeRateStartDateDTG() As String
        Get
            Const sFunctionName As String = "Property Get PrimeRateStartDateDTG"
            Dim dDaysToPrimeRate As Double
            Dim sTemp As String
            Try
                If objBenefitRule Is Nothing Then lLoadData(g_objXClaim.ClaimantHourlyPayRate)
                If m_PaidPendingWeeks = 0 Then
                    sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitAbbs, "")
                    m_PaidPendingWeeks = GetBenefitWeeksPaidOrPending(sTemp)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dDaysToPrimeRate = (objBenefitRule.SecondRateMaxWeeks - m_PaidPendingWeeks) * 7
                dDaysToPrimeRate = modFunctions.RoundStandard(dDaysToPrimeRate + 0.49, 0)
                PrimeRateStartDateDTG = modFunctions.sAddDTGDate((g_objXPaymentParms.BenefitStartDate), CInt(dDaysToPrimeRate))
            Catch ex As Exception
                With Err()
                    g_lErrNum = Err.Number
                    g_sErrSrc = .Source
                    g_sErrDescription = Err.Description
                End With
                g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                g_lErrLine = Erl()
                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
            End Try
        End Get
    End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    ''Public Property Let PrimeRateStartDateDTG(ByVal vData As String)
    ''    m_PrimeRateStartDateDTG = vData
    ''End Property

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs
    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()
    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

