Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCXClaim
    Const sClassName As String = "CWCXClaim"
    Private m_AWWToCompensate As Double
    Private m_ClaimantAgeAtEvent As Short
    Private m_ClaimantOriginalAWW As Double
    Private m_ClaimantBirthDateDTG As String
    Private m_ClaimantEID As Integer
    Private m_ClaimantImpairment As Double
    Private m_ClaimantNameLFM As String
    Private m_ClaimantSexCode As Integer
    Private m_ClaimantTTDRate As Double
    Private m_ClaimantHourlyPayRate As Double
    Private m_ClaimantWeeklyPayRate As Double
    Private m_ClaimID As Integer
    Private m_ClaimNumber As String
    Private m_DateOfDeathDTG As String
    Private m_DateOfEventCalar As String
    Private m_DateOfEventDTG As String
    Private m_DateOfEventTwoYearCalar As String
    Private m_DateofEventTwoYearDTG As String
    Private m_DependentCount As Integer
    Private m_EventID As Integer
    Private m_FilingStateID As Integer
    Private m_FilingStatePostalCode As String
    Private m_JurisTaxStatusCode As Integer
    Private m_JurisTaxExemptions As Integer
    Private m_JurisDefinedWorkWeek As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSAWW As Integer

    Private Function ClearObject() As Integer
        m_ClaimantOriginalAWW = 0
        m_ClaimantBirthDateDTG = vbNullString
        m_ClaimantEID = 0
        m_ClaimantNameLFM = vbNullString
        m_ClaimantSexCode = 0
        m_ClaimantTTDRate = 0
        m_ClaimantWeeklyPayRate = 0
        m_ClaimID = 0
        m_ClaimNumber = vbNullString
        m_DateOfDeathDTG = vbNullString
        m_DateOfEventDTG = vbNullString
        m_DateofEventTwoYearDTG = vbNullString
        m_ErrorMask = 0
        m_ErrorMaskCalcSetup = 0
        m_ErrorMaskSAWW = 0
        m_EventID = 0
        m_FilingStateID = 0
        m_JurisTaxStatusCode = 0 'jtodd22 02/13/2008 must always initialize as 0 (zero)
        m_JurisTaxExemptions = 0
        m_JurisDefinedWorkWeek = 0
    End Function

    Private Function GetAWW(ByRef lClaimID As Integer) As Double
        Const sFunctionName As String = "GetAWW"
        Dim objReader As DbReader
        Dim lPIRowID As Integer
        Dim sSQL As String
        Try

            GetAWW = 0

            m_ClaimantOriginalAWW = 0
            m_ClaimantTTDRate = 0
            m_ClaimantHourlyPayRate = 0

            lPIRowID = f_lGetPIRowID(lClaimID)

            sSQL = "SELECT HOURLY_RATE FROM PERSON_INVOLVED WHERE PI_ROW_ID = " & lPIRowID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_ClaimantHourlyPayRate = objReader.GetDouble("HOURLY_RATE")
            End If


            sSQL = ""
            sSQL = sSQL & "SELECT AWW, TTD_BASE, JR_TAX_STATUS_CODE, JR_TAX_EXEMPTIONS"
            sSQL = sSQL & " FROM CLAIM_AWW WHERE CLAIM_ID = " & lClaimID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_ClaimantOriginalAWW = objReader.GetDouble("AWW")
                m_ClaimantTTDRate = objReader.GetDouble("TTD_BASE")
                m_JurisTaxStatusCode = objReader.GetInt32("JR_TAX_STATUS_CODE")
                m_JurisTaxExemptions = objReader.GetInt32("JR_TAX_EXEMPTIONS")
            End If
            ''    'jtodd22 01/07/2008 this code was in the original.  Without documentation for the reason
            ''    'there is no logic for the codes existence.
            ''    If m_ClaimantOriginalAWW < 0.01 Or IsNull(m_ClaimantOriginalAWW) Then
            ''        sSQL = "SELECT HOURLY_RATE, WEEKLY_RATE FROM PERSON_INVOLVED WHERE PI_ROW_ID = " & lPIRowID
            ''        iRdSet = g_DBObject.DB_CreateRecordset(g_dbConn1, sSQL, DB_FORWARD_ONLY, 0)
            ''        If (objReader.Read()) Then
            ''            m_ClaimantOriginalAWW = objReader.GetInt32( "WEEKLY_RATE")
            ''            m_ClaimantWeeklyPayRate = m_ClaimantOriginalAWW
            ''        End If
            ''        SafeDropRecordset iRdSet
            ''    End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetAWW = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Private Function GetClaimantImpairment() As Double
        Const sFunctionName As String = "GetClaimantImpairment"
        Dim objReader As DbReader
        Dim sSQL As String
        Dim sSQL2 As String
        Dim sSQL3 As String

        Try
            GetClaimantImpairment = 0
            sSQL = ""
            sSQL = sSQL & " FROM PERSON_INVOLVED, PI_X_MMI_HIST"
            sSQL = sSQL & " WHERE PERSON_INVOLVED.PI_ROW_ID = PI_X_MMI_HIST.PI_ROW_ID"
            sSQL = sSQL & " AND PERSON_INVOLVED.EVENT_ID = " & m_EventID
            sSQL = sSQL & " AND PERSON_INVOLVED.PI_EID = " & m_ClaimantEID

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT"
            sSQL2 = sSQL2 & " MAX(PI_X_MMI_HIST.PI_X_MMIROW_ID)"
            sSQL2 = sSQL2 & sSQL

            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT"
            sSQL3 = sSQL3 & " PERCENT_DISABILITY"
            sSQL3 = sSQL3 & " FROM PI_X_MMI_HIST"
            sSQL3 = sSQL3 & " WHERE PI_X_MMIROW_ID = (" & sSQL2 & ")"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If (objReader.Read()) Then
                m_ClaimantImpairment = objReader.GetDouble("PERCENT_DISABILITY")
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetClaimantImpairment = -1

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Private Function GetClaimantEntity(ByRef lClaimID As Integer) As Integer
        Const sFunctionName As String = "GetClaimantEntity"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            GetClaimantEntity = 0
            'jtodd22 absolute no-no Clearobject

            sSQL = ""
            sSQL = sSQL & " SELECT"
            sSQL = sSQL & " CLAIMANT_EID"
            sSQL = sSQL & ",FIRST_NAME,MIDDLE_NAME,LAST_NAME"
            sSQL = sSQL & ",BIRTH_DATE"
            sSQL = sSQL & ",ENTITY_ID, SEX_CODE"
            sSQL = sSQL & " FROM ENTITY,CLAIMANT"
            sSQL = sSQL & " WHERE CLAIMANT.CLAIM_ID = " & lClaimID
            sSQL = sSQL & " AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_ClaimantBirthDateDTG = objReader.GetString("BIRTH_DATE")
                m_ClaimantEID = objReader.GetInt32("CLAIMANT_EID")
                m_ClaimantNameLFM = objReader.GetString("LAST_NAME") & ", "
                m_ClaimantNameLFM = m_ClaimantNameLFM & objReader.GetString("FIRST_NAME") & " "
                m_ClaimantNameLFM = m_ClaimantNameLFM & objReader.GetString("MIDDLE_NAME")
                m_ClaimantSexCode = objReader.GetInt32("SEX_CODE")
            End If
            GetClaimantEntity = -1
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetClaimantEntity = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    Private Function GetClaimantDateOfDeath() As Integer
        Const sFunctionName As String = "GetClaimantDateOfDeath"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            GetClaimantDateOfDeath = 0

            sSQL = ""
            sSQL = sSQL & " SELECT"
            sSQL = sSQL & " PERSON_INVOLVED.DATE_OF_DEATH"
            sSQL = sSQL & " FROM CLAIM, CLAIMANT, PERSON_INVOLVED"
            sSQL = sSQL & " WHERE CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID"
            sSQL = sSQL & " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
            sSQL = sSQL & " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID"
            sSQL = sSQL & " AND CLAIM.CLAIM_ID = " & g_objXClaim.ClaimID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_DateOfDeathDTG = objReader.GetString("DATE_OF_DEATH")
            End If
            GetClaimantDateOfDeath = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetClaimantDateOfDeath = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Private Function GetDependentCount() As Integer
        Const sFunctionName As String = "GetDependentCount"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            GetDependentCount = 0

            sSQL = ""
            sSQL = sSQL & " SELECT"
            sSQL = sSQL & " COUNT(PI_X_DEPENDENT.PI_ROW_ID) XDEPENDENTS"
            sSQL = sSQL & " FROM CLAIM, CLAIMANT, PERSON_INVOLVED, PI_X_DEPENDENT"
            sSQL = sSQL & " WHERE CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID"
            sSQL = sSQL & " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
            sSQL = sSQL & " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID"
            sSQL = sSQL & " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_DEPENDENT.PI_ROW_ID"
            sSQL = sSQL & " AND CLAIM.CLAIM_ID = " & g_objXClaim.ClaimID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetDependentCount = objReader.GetInt32("XDEPENDENTS")
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetDependentCount = (-1 * Err.Number)

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function
    Private Function GetJurisdefinedWorkWeek() As Integer
        Const sFunctionName As String = "GetJurisdefinedWorkWeek"
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim objRule As Object

        Try

            GetJurisdefinedWorkWeek = 0
            Select Case GetStatePostalCode_SQL(m_FilingStateID)
                Case "ID"
                    lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTD", objRule, "RMJuRuLib.CJRBenefitRuleTTDID")
                Case "NH"
                    lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTD", objRule, "RMJuRuLib.CJRBenefitRuleTTDNH")
                Case "FL"
                    lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTD", objRule, "RMJuRuLib.CJRBenefitRuleTTDAA")
                Case "WA"
                    lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTD", objRule, "RMJuRuLib.CJRBenefitRuleTTDWA")
                Case Else
                    lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTD", objRule, "RMJuRuLib.CJRBenefitRuleTTDAA")
            End Select
            'error messages are generated in GetBenefitRuleTT
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            'UPGRADE_WARNING: Couldn't resolve default property of object objRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_JurisDefinedWorkWeek = objRule.JurisWorkWeek
            If m_JurisDefinedWorkWeek = 0 Then m_JurisDefinedWorkWeek = 7

            'UPGRADE_NOTE: Object objRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objRule = Nothing

            GetJurisdefinedWorkWeek = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetJurisdefinedWorkWeek = g_lErrNum

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function LoadData(ByRef lClaimID As Integer) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = g_objXErrorMask.cSuccess
            ClearObject()

            sSQL = ""
            sSQL = sSQL & " SELECT"
            sSQL = sSQL & " EVENT.DATE_OF_EVENT"
            sSQL = sSQL & ",EVENT.EVENT_ID"
            sSQL = sSQL & ",CLAIM_ID,CLAIM_NUMBER"
            sSQL = sSQL & ",FILING_STATE_ID"
            sSQL = sSQL & " FROM EVENT,CLAIM"
            sSQL = sSQL & " WHERE CLAIM.CLAIM_ID = " & lClaimID
            sSQL = sSQL & " AND CLAIM.EVENT_ID = EVENT.EVENT_ID"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                m_ClaimID = objReader.GetInt32("CLAIM_ID")
                m_ClaimNumber = objReader.GetString("CLAIM_NUMBER")
                m_DateOfEventDTG = objReader.GetString("DATE_OF_EVENT")
                If Len(m_DateOfEventDTG) = 8 Then
                    m_DateOfEventCalar = Mid(m_DateOfEventDTG, 5, 2) & "/" & Mid(m_DateOfEventDTG, 7, 2) & "/" & Mid(m_DateOfEventDTG, 1, 4)
                End If
                m_EventID = objReader.GetInt32("EVENT_ID")
                m_FilingStateID = objReader.GetInt32("FILING_STATE_ID")
            End If

            GetAWW(lClaimID)
            GetClaimantEntity(lClaimID)
            GetClaimantDateOfDeath()
            If IsDate(m_DateOfEventCalar) Then
                m_DateOfEventTwoYearCalar = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.DayOfYear, 2, CDate(m_DateOfEventCalar)))
            End If
            If IsDate(m_DateOfEventTwoYearCalar) Then
                'UPGRADE_WARNING: Couldn't resolve default property of object PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                m_DateOfEventTwoYearCalar = PadDateWithZeros(m_DateOfEventTwoYearCalar)
                m_DateofEventTwoYearDTG = Mid(m_DateOfEventTwoYearCalar, 7, 4) & Mid(m_DateOfEventTwoYearCalar, 1, 2) & Mid(m_DateOfEventTwoYearCalar, 4, 2)
            End If
            m_ClaimantAgeAtEvent = iCalculateAge(sDBDateFormat(m_ClaimantBirthDateDTG, "Short Date"), sDBDateFormat(m_DateOfEventDTG, "Short Date"))
            m_DependentCount = GetDependentCount()
            m_FilingStatePostalCode = GetStatePostalCode_SQL(m_FilingStateID)
            GetClaimantImpairment()
            'jtodd22 03/08/2006  --GetJurisdefinedWorkWeek
            m_AWWToCompensate = m_ClaimantOriginalAWW

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Property ClaimantAgeAtEvent() As Short
        Get
            ClaimantAgeAtEvent = m_ClaimantAgeAtEvent
        End Get
        Set(ByVal Value As Short)
            m_ClaimantAgeAtEvent = Value
        End Set
    End Property

    Public Property ClaimantImpairment() As Double
        Get
            ClaimantImpairment = m_ClaimantImpairment
        End Get
        Set(ByVal Value As Double)
            m_ClaimantImpairment = Value
        End Set
    End Property

    Public Property ClaimantOriginalAWW() As Double
        Get
            ClaimantOriginalAWW = m_ClaimantOriginalAWW
        End Get
        Set(ByVal Value As Double)
            m_ClaimantOriginalAWW = Value
        End Set
    End Property

    Public Property ClaimantTTDRate() As Double
        Get
            ClaimantTTDRate = m_ClaimantTTDRate
        End Get
        Set(ByVal Value As Double)
            m_ClaimantTTDRate = Value
        End Set
    End Property

    Public Property ClaimantBirthDateDTG() As String
        Get
            ClaimantBirthDateDTG = m_ClaimantBirthDateDTG
        End Get
        Set(ByVal Value As String)
            m_ClaimantBirthDateDTG = Value
        End Set
    End Property
    Public Property ClaimantEID() As Integer
        Get
            ClaimantEID = m_ClaimantEID
        End Get
        Set(ByVal Value As Integer)
            m_ClaimantEID = Value
        End Set
    End Property
    Public Property ClaimantNameLFM() As String
        Get
            ClaimantNameLFM = m_ClaimantNameLFM
        End Get
        Set(ByVal Value As String)
            m_ClaimantNameLFM = Value
        End Set
    End Property
    Public Property ClaimantSexCode() As Integer
        Get
            ClaimantSexCode = m_ClaimantSexCode
        End Get
        Set(ByVal Value As Integer)
            m_ClaimantSexCode = Value
        End Set
    End Property

    Public Property ClaimantHourlyPayRate() As Double
        Get
            ClaimantHourlyPayRate = m_ClaimantHourlyPayRate
        End Get
        Set(ByVal Value As Double)
            m_ClaimantHourlyPayRate = Value
        End Set
    End Property

    Public Property ClaimantWeeklyPayRate() As Double
        Get
            ClaimantWeeklyPayRate = m_ClaimantWeeklyPayRate
        End Get
        Set(ByVal Value As Double)
            m_ClaimantWeeklyPayRate = Value
        End Set
    End Property

    Public Property ClaimID() As Integer
        Get
            ClaimID = m_ClaimID
        End Get
        Set(ByVal Value As Integer)
            m_ClaimID = Value
        End Set
    End Property
    Public Property ClaimNumber() As String
        Get
            ClaimNumber = m_ClaimNumber
        End Get
        Set(ByVal Value As String)
            m_ClaimNumber = Value
        End Set
    End Property

    Public Property DateOfDeathDTG() As String
        Get
            DateOfDeathDTG = m_DateOfDeathDTG
        End Get
        Set(ByVal Value As String)
            m_DateOfDeathDTG = Value
        End Set
    End Property
    '####################################################################
    Public Property DateOfEventCalar() As String
        Get
            DateOfEventCalar = m_DateOfEventCalar
        End Get
        Set(ByVal Value As String)
            m_DateOfEventCalar = Value
        End Set
    End Property
    Public Property DateOfEventDTG() As String
        Get
            DateOfEventDTG = m_DateOfEventDTG
        End Get
        Set(ByVal Value As String)
            m_DateOfEventDTG = Value
        End Set
    End Property
    '####################################################################
    Public Property DateOfEventTwoYearCalar() As String
        Get
            DateOfEventTwoYearCalar = m_DateOfEventTwoYearCalar
        End Get
        Set(ByVal Value As String)
            m_DateOfEventTwoYearCalar = Value
        End Set
    End Property
    Public Property DateOfEventTwoYearDTG() As String
        Get
            DateOfEventTwoYearDTG = m_DateofEventTwoYearDTG
        End Get
        Set(ByVal Value As String)
            m_DateofEventTwoYearDTG = Value
        End Set
    End Property
    '####################################################################

    Public Property DependentCount() As Integer
        Get
            DependentCount = m_DependentCount
        End Get
        Set(ByVal Value As Integer)
            m_DependentCount = Value
        End Set
    End Property

    Public Property EventID() As Integer
        Get
            EventID = m_EventID
        End Get
        Set(ByVal Value As Integer)
            m_EventID = Value
        End Set
    End Property

    Public Property FilingStateID() As Integer
        Get
            FilingStateID = m_FilingStateID
        End Get
        Set(ByVal Value As Integer)
            m_FilingStateID = Value
        End Set
    End Property
    Public Property FilingStatePostalCode() As String
        Get
            FilingStatePostalCode = m_FilingStatePostalCode
        End Get
        Set(ByVal Value As String)
            m_FilingStatePostalCode = Value
        End Set
    End Property

    Public Property JurisTaxStatusCode() As Integer
        Get
            JurisTaxStatusCode = m_JurisTaxStatusCode
        End Get
        Set(ByVal Value As Integer)
            m_JurisTaxStatusCode = Value
        End Set
    End Property

    Public Property JurisTaxExemptions() As Integer
        Get
            JurisTaxExemptions = m_JurisTaxExemptions
        End Get
        Set(ByVal Value As Integer)
            m_JurisTaxExemptions = Value
        End Set
    End Property

    Public Property JurisDefinedWorkWeek() As Integer
        Get
            JurisDefinedWorkWeek = m_JurisDefinedWorkWeek
        End Get
        Set(ByVal Value As Integer)
            m_JurisDefinedWorkWeek = Value
        End Set
    End Property

    Public Property AWWToCompensate() As Double
        Get
            AWWToCompensate = m_AWWToCompensate
        End Get
        Set(ByVal Value As Double)
            m_AWWToCompensate = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer
        Get
            m_ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer
        Get
            m_ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer
        Get
            m_ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property
End Class

