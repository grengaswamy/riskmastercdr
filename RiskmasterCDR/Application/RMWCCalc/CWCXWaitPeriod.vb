Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCXWaitPeriod
    '1234567890123456789012345678901234567890
    'RMWCCalc.CWCXWaitPeriod
    Const sClassName As String = "CWCXWaitPeriod"
    Public CurrentPayStatus As Integer
    Public PayStatuses As New CWCEnumWaitPeriodPayStatus
    Public WaitingPeriodFirstDateDTG As String
    Public WaitingPeriodToDateDTG As String

    Public objPIXRestricts As CWCPIXRestricts
    Public objPIXWorkLosses As CWCPIXWorkLosses

    Private m_NoCodeID As Integer
    Private m_YesCodeID As Integer
    Private m_CurrentDisabilityDateDBFormat As String
    Private m_MostRecentRetToWkDateDBFormat As String
    Private m_OriginalDisabilityDateDBFormat As String
    Private m_RetroactivePeriodDays As Integer
    Private m_RetroactivePerSatisfiedCode As Integer
    Private m_WaitingPeriodDays As Integer
    Private m_WaitingPeriodSatisfiedCode As Integer


    Public Function LoadData() As Integer
        Dim lErrNumber As Integer
        Dim lReturn As Integer

        m_NoCodeID = modFunctions.GetNoCodeID(lErrNumber)
        m_YesCodeID = modFunctions.GetYesCodeID(lErrNumber)

        lReturn = objPIXRestricts.LoadData(0)
        lReturn = objPIXWorkLosses.LoadData(0)

        PayStatuses.LoadData()

        CurrentPayStatus = WaitingPeriodStatus()


    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : WaitingPeriodStatus
    ' DateTime  : 7/12/2005 16:32
    ' Author    : jtodd22
    ' Purpose   : to find if the waiting period is paid, or needs to be paid
    '---------------------------------------------------------------------------------------
    '
    Public Function WaitingPeriodStatus() As Integer
        Const sFunctionName As String = "WaitingPeriodStatus"
        Dim i2 As Short
        Dim objReader As DbReader
        Dim lIndex As Integer
        Dim lReturn As Integer
        Dim lTotalWorkLossDays As Integer
        Dim lWorkLossDays As Integer

        Dim objBenefitCalcRule As Object
        Dim sAbbr As String
        Dim sSQL2 As String
        Dim sSQL3 As String
        Dim sSQL4 As String
        Dim sDateRTW As String
        Dim sDisabilityDateDTG As String
        Dim sWarning As String

        Try

            WaitingPeriodStatus = PayStatuses.NotApplicable

            If objPIXWorkLosses.Count + objPIXRestricts.Count = 0 Then
                Exit Function
            End If

            If objPIXWorkLosses.Count > 0 Then
                WaitingPeriodStatus = PayStatuses.IsApplicable
            End If

            'jtodd22 10/11/2006 Alabama does use a Temporary Partial Waiting Period
            'If objPIXRestricts.Count > 0 Then
            '    WaitingPeriodStatus = PayStatuses.IsApplicable
            'End If

            'jtodd22 10/11/2006 find out if the Waiting Period is Payable

            objBenefitCalcRule = New RMJuRuLib.CJRCalcBenefit()
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitCalcRule.LoadDataByJurisRowIDEffectiveDate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            lReturn = objBenefitCalcRule.LoadDataByJurisRowIDEffectiveDate(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG)
            Select Case lReturn
                Case -1 'normally expected
                Case 0
                    g_sWarning = "Call from RMWCCalc.CWCXWaitPeriod to RMJuRuLib.CJRCalcBenefit exited with out loading data correctly and with out Error."
                Case Else 'error
                    Err.Raise(vbObjectError + 90000, sClassName & "." & WaitingPeriodStatus, "Failed to load CJRCalcBenefit")
            End Select

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitCalcRule.WaitDays. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_WaitingPeriodDays = objBenefitCalcRule.WaitDays
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitCalcRule.RetroActiveDays. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_RetroactivePeriodDays = objBenefitCalcRule.RetroActiveDays

            If objPIXWorkLosses.Count > 0 Then
                lIndex = 1
                m_OriginalDisabilityDateDBFormat = objPIXWorkLosses.Item(lIndex).DisabilityDate
                lIndex = objPIXWorkLosses.Count
                If objPIXWorkLosses.Item(lIndex).PiWlRowId > 0 Then
                    With objPIXWorkLosses
                        lIndex = objPIXWorkLosses.Count
                        m_CurrentDisabilityDateDBFormat = .Item(lIndex).DisabilityDate
                        m_MostRecentRetToWkDateDBFormat = .Item(lIndex).DateReturned
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitCalcRule.CountFromCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Select Case objBenefitCalcRule.CountFromCode
                            Case 2 'count wait days from event date

                            Case 4
                                For i2 = 1 To objPIXWorkLosses.Count
                                    sDateRTW = .Item(i2).DateReturned
                                    If Trim(sDateRTW & "") = "" Then sDateRTW = Format(Now, "YYYYMMDD")
                                    sDisabilityDateDTG = .Item(i2).DisabilityDate
                                    If Trim(sDisabilityDateDTG & "") = "" Then sDisabilityDateDTG = .Item(i2).DateLastWorked
                                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                                    lWorkLossDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sDBDateFormat(sDisabilityDateDTG, "Short Date")), CDate(sDBDateFormat(sDateRTW, "Short Date")))
                                    lTotalWorkLossDays = lTotalWorkLossDays + System.Math.Abs(lWorkLossDays)
                                Next

                            Case 5
                                lIndex = objPIXWorkLosses.Count
                                sDateRTW = .Item(lIndex).DateReturned
                                If Trim(sDateRTW & "") = "" Then sDateRTW = Format(Now, "YYYYMMDD")
                                sDisabilityDateDTG = .Item(lIndex).DisabilityDate
                                If Trim(sDisabilityDateDTG & "") = "" Then sDisabilityDateDTG = .Item(lIndex).DateLastWorked
                                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                                lWorkLossDays = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(sDBDateFormat(sDisabilityDateDTG, "Short Date")), CDate(sDBDateFormat(sDateRTW, "Short Date")))
                                lTotalWorkLossDays = lTotalWorkLossDays + System.Math.Abs(lWorkLossDays)
                        End Select
                        If lTotalWorkLossDays > (m_WaitingPeriodDays - 1) Then
                            m_WaitingPeriodSatisfiedCode = m_YesCodeID
                        End If
                        If lTotalWorkLossDays > (m_RetroactivePeriodDays - 1) Then
                            m_RetroactivePerSatisfiedCode = m_YesCodeID
                        End If

                    End With
                End If
            End If

            If m_RetroactivePerSatisfiedCode = m_NoCodeID Then
                WaitingPeriodStatus = PayStatuses.NotPayableAtThisTime
            End If

            If m_RetroactivePerSatisfiedCode = m_YesCodeID Then
                WaitingPeriodStatus = PayStatuses.NotPaid
            End If

            WaitingPeriodFirstDateDTG = m_OriginalDisabilityDateDBFormat
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitCalcRule.WaitDays. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            WaitingPeriodToDateDTG = sAddDTGDate(WaitingPeriodFirstDateDTG, objBenefitCalcRule.WaitDays - 1)

            'jtodd22 --healing periods may bite us
            sAbbr = modFunctions.GetTempTotalAbbreviation(sWarning, (g_objXClaim.FilingStateID))

            sSQL2 = ""
            sSQL2 = sSQL2 & "SELECT BENEFIT_LKUP_ID FROM WCP_BENEFIT_LKUP"
            sSQL2 = sSQL2 & " WHERE ABBREVIATION = '" & sAbbr & "'"
            sSQL2 = sSQL2 & " AND WCP_BENEFIT_LKUP.JURIS_ROW_ID = " & g_objXClaim.FilingStateID

            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES"
            sSQL3 = sSQL3 & " WHERE WCP_TRANS_TYPES.STATE_ROW_ID = " & g_objXClaim.FilingStateID
            sSQL3 = sSQL3 & " AND BENEFIT_LKUP_ID IN (" & sSQL2 & ")"

            sSQL4 = ""
            sSQL4 = sSQL4 & "SELECT DISTINCT"
            sSQL4 = sSQL4 & " FUNDS_TRANS_SPLIT.FROM_DATE"
            sSQL4 = sSQL4 & " FROM FUNDS, FUNDS_TRANS_SPLIT"
            sSQL4 = sSQL4 & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
            sSQL4 = sSQL4 & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL4 = sSQL4 & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL4 = sSQL4 & " AND FUNDS.VOID_FLAG = 0"
            sSQL4 = sSQL4 & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sSQL3 & ")"
            sSQL4 = sSQL4 & " AND FUNDS_TRANS_SPLIT.FROM_DATE = '" & WaitingPeriodFirstDateDTG & "'"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL4)
            If (objReader.Read()) Then
                WaitingPeriodStatus = PayStatuses.Paid
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitCalcRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objBenefitCalcRule = Nothing
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Sub New()
        MyBase.New()
        objPIXRestricts = New CWCPIXRestricts
        objPIXWorkLosses = New CWCPIXWorkLosses
    End Sub

    Protected Overrides Sub Finalize()
        objPIXRestricts = Nothing
        objPIXWorkLosses = Nothing
        MyBase.Finalize()
    End Sub
End Class

