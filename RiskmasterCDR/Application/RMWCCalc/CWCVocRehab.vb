Option Strict Off
Option Explicit On
Imports Riskmaster.Db
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
Public Class CWCVocRehab
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "VRMA"
    Const sClassName As String = "CWCVocRehab"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleVRMA"

    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object

    Public BenefitDays As Integer
    Public BenefitEndDate As String
    Public BenefitStartDate As String

    Public BenefitRate_Effective As Double
    Public BenefitRate_JurisMax As Double
    Public CalculatedPayment As Double
    Public CalculatedPaymentAttyFee As Double
    Public JurisMaxBenRateDay As Double
    Public JurisMaxBenRateWeek As Double


    Public MinBenRateDay As Double
    Public MinBenRateWeek As Double
    Public PayLateCharge As Short

    Public WithHoldAttorneyFees As Short
    Public WithHoldAttorneyFeeRate As Double
    Public WithHoldAttorneyFeeAmount As Double
    Public objTTD As CWCTempTotlDis

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objTTD may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objTTD = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim objReader As DbReader
        Dim lReturn As Integer
        Dim dblBenefitRate As Double
        Dim sAbbr As String
        Dim sWarning As String

        Try

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = g_objWCPClaim.LoadDataLastRecordByRowID(m_sBenefitTypeAbbr)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            sAbbr = modFunctions.GetTempTotalAbbreviation(sWarning, (g_objXClaim.FilingStateID))
            lReturn = modFunctions.GetCalculator(objTTD, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = objTTD.Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
            m_ErrorMask = objTTD.Standard.ErrorMask
            m_ErrorMaskFedTax = objTTD.Standard.ErrorMaskFedTax
            m_ErrorMaskSAWW = objTTD.Standard.ErrorMaskSAWW
            m_ErrorMaskCalcSetup = objTTD.Standard.ErrorMaskCalcSetup
            m_ErrorMaskSpendData = objTTD.Standard.ErrorMaskSpendData
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            dblBenefitRate = objTTD.BenefitRate_Effective

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.JurisMaxAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If dblBenefitRate > objBenefitRule.JurisMaxAmount Then dblBenefitRate = objBenefitRule.JurisMaxAmount

            Me.BenefitRate_Effective = dblBenefitRate
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.JurisMaxAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.BenefitRate_JurisMax = objBenefitRule.JurisMaxAmount

            Me.WithHoldAttorneyFees = g_objWCPClaim.AttyFeeCode
            Me.WithHoldAttorneyFeeRate = g_objWCPClaim.AttyFeePercent
            Me.WithHoldAttorneyFeeAmount = 0

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            lLoadData = g_lErrNum

        End Try

    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:35
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim dRate As Double
        Dim lReturn As Integer

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            m_ErrorMask = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'build top of work sheet
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail
            colWorkSheet.Add("     |State Imposed Limit                               |" & Format(Me.objTTD.JurisdictionMaxAww, "Currency"))
            colWorkSheet.Add("     |Claimant AWW As Capped (State Imposed Limit)      |" & Format(Me.objTTD.ClaimantJurisAWW, "Currency"))

            colWorkSheet.Add("     |Benefit Payment Start Date                        |" & CDate(Me.BenefitStartDate))
            colWorkSheet.Add("     |Benefit Payment End Date                          |" & CDate(Me.BenefitEndDate))
            colWorkSheet.Add("||")
            colWorkSheet.Add("     |Temporyary Total Disability Weekly Rate|" & Format(Me.objTTD.BenefitRate_Effective, "Currency"))
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.JurisMaxAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            colWorkSheet.Add("     |State Imposed VRMA Max Weekly Rate|" & Format(objBenefitRule.JurisMaxAmount, "Currency"))
            colWorkSheet.Add("     |Effective VRMA Weekly Rate|" & Format(Me.BenefitRate_Effective, "Currency"))
            colWorkSheet.Add("     ||")

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.CalculatedPayment = (Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays
            colWorkSheet.Add("     |Days in Payment Period|" & Me.BenefitDays)
            colWorkSheet.Add("     |Gross Payment|" & Format(Me.CalculatedPayment, "Currency"))
            If WithHoldAttorneyFees = 1 Then
                colWorkSheet.Add("|With Held Attorney Fee at Percentage Rate|" & Me.WithHoldAttorneyFeeRate & "%")
                Me.WithHoldAttorneyFeeRate = Me.WithHoldAttorneyFeeRate / 100
                colWorkSheet.Add("|Calculated Payment|" & Format(Me.CalculatedPayment, "Currency"))
                colWorkSheet.Add("|Add Late Charge|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                colWorkSheet.Add("|Total Payments|" & Format(Me.CalculatedPayment + Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                colWorkSheet.Add("|Multiple By|" & Me.WithHoldAttorneyFeeRate)
                Me.CalculatedPaymentAttyFee = (Me.CalculatedPayment + Me.Standard.CalculatedPaymentLateCharge) * Me.WithHoldAttorneyFeeRate
                Me.CalculatedPaymentAttyFee = modFunctions.RoundStandard((Me.CalculatedPaymentAttyFee), 2)
                colWorkSheet.Add("|With Held Attorney Amount|" & Format(Me.CalculatedPaymentAttyFee, "Currency"))
            Else
                Me.CalculatedPaymentAttyFee = 0
            End If

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function


            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
            colWorkSheet.Add("|Net Payment This Period|" & Format(Me.CalculatedPayment + Me.Standard.CalculatedPaymentLateCharge - Me.CalculatedPaymentAttyFee, "Currency"))

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lCalculatePayment = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function PutClaimBenefitData(ByVal lWithHoldAttorneyFees As Integer, ByVal dWithHoldAttorneyFeeRate As Double) As Boolean
        Const sFunctionName As String = "PutClaimBenefitData"
        '*************************************************************
        '*** find the record
        '*** if the record is missing (new benefit) then add a record
        '*** end existing record
        '*************************************************************
        Dim dPermDisRate As Double
        Dim objReader As DbReader
        Dim objWriter As DbWriter
        Dim result As Short
        Dim ndx As Short
        Dim lTemp As Integer
        Dim sSQLSelect As String
        Dim sSQLUpdate As String
        Dim sTemp01 As String
        Dim s_Now As String
        Try
            lTemp = -1
            s_Now = Format(Now, "yyyymmddhhnnss")
            sSQLSelect = SelectClaimDataByClaimIDByBenefitType(m_sBenefitTypeAbbr)

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQLSelect)
            If objReader.Read() Then
                lTemp = objReader.GetInt32("ROW_ID")
                objReader.Close()
            Else
                objWriter = DbFactory.GetDbWriter(objReader, False)

                '.DB_PutData rsSelect, "DISABILITY_RATE", Me.objPPD.DisabilityRate_Effective
                objWriter.Fields.Add("BENEFIT_TYPE", m_sBenefitTypeAbbr)
                objWriter.Fields.Add("CLAIM_ID", g_objXClaim.ClaimID)
                objWriter.Fields.Add("ATTY_FEE_CODE", lGetNextUID("WCP_CLAIMS"))
                objWriter.Fields.Add("ROW_ID", lWithHoldAttorneyFees)
                objWriter.Fields.Add("ATTY_FEE_PERCENT", dWithHoldAttorneyFeeRate)
                objWriter.Fields.Add("ADDED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_ADDED", s_Now)
                objWriter.Fields.Add("UPDATED_BY_USER", g_objUser.LoginName)
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD", s_Now)
                objWriter.Execute()
                objReader.Close()
            End If
            '*************************************
            '*************************************
            If lTemp > -1 Then
                sSQLUpdate = SQLSelectClaimDataByRowID(lTemp)
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQLUpdate)
                If objReader.Read() Then
                    objWriter = DbFactory.GetDbWriter(objReader, True)
                    objWriter.Fields("ATTY_FEE_CODE").Value = lWithHoldAttorneyFees
                    objWriter.Fields("ATTY_FEE_PERCENT").Value = dWithHoldAttorneyFeeRate
                    objWriter.Fields("UPDATED_BY_USER").Value = g_objUser.LoginName
                    objWriter.Fields("DTTM_RCD_LAST_UPD").Value = s_Now

                End If
            End If
            PutClaimBenefitData = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
            PutClaimBenefitData = g_lErrNum
        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim lReturn As Integer
        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayTTDRateCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PayTTDRateCode = g_lYesCodeID Then
                dBasicRate = TempTotalRate
            End If

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function



    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Voc Rehab"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

