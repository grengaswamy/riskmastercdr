Option Strict Off
Option Explicit On
Public Class CWCTempPartNtSDiscountNY
    Implements _ICalculator
    '---------------------------------------------------------------------------------------
    ' Module    : CWCTempPartNtSDiscountNY
    ' DateTime  : 12/11/2006
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "TIB-P"
    Const m_sBenefittypeAbbr2 As String = "TP"
    Const sClassName As String = "CWCTempPartNtSDiscountNY"
    '                    RMWCCalc.
    '                    1234567890123456789012345678901234567890
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleTPDNtSAA"

    '******************************************************************************************************************
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object

    Private m_BenefitRate_Effective As Double
    Private m_BenefitRate_Original As Double
    Private m_EarningsPerWeek As Double
    Private m_PayHighRate As Integer
    Public CalculatedPayment As Double

    Public ClaimantJurisAWW As Double
    Public ClaimantOriginalAWW As Double

    Private m_DaysToPaymentDue As Short

    Private m_OffsetPerWeek As Double

    Public EventDateCalendar As Date
    Public EventDateDTG As String

    Public JurisdictionMaxAww As Double

    Public PayLateCharge As Short
    Public RealMinBenRateWeek As Double
    Public RealMinBenRateDay As Double

    Private m_MMIRate As Double
    Private m_MMIDateDTG As String

    '---------------------------------------------------------------------------------------
    ' Procedure : ClearObject
    ' DateTime  : 6/2/2005 11:42
    ' Author    : jtodd22
    ' Purpose   : Clear the object before loading.
    ' Note      : This function should be called only as part of LoadData.
    '---------------------------------------------------------------------------------------
    '
    Private Function ClearObject() As Integer
        EventDateCalendar = CDate("12/31/1899")
        EventDateDTG = vbNullString
        JurisdictionMaxAww = -0.01
        ClaimantJurisAWW = -0.01
        m_BenefitRate_Effective = -0.01
        RealMinBenRateWeek = -0.01
        RealMinBenRateDay = -0.01
        m_PayHighRate = 0 'jtodd22 presume employee is high hourly rate in Texas

        'jtodd22 do not reset BenefitDays it is set in calling executable
        m_CheatSheetTitle = "Temporary Partial"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function

    Private Function GetEffectiveRate(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "GetEffectiveRate"
        Dim dCurrentRate As Double
        Dim dWeeklyRate As Double
        Try

            GetEffectiveRate = 0

            With g_objXClaim
                dCurrentRate = dGetBasicRate(.AWWToCompensate, .ClaimantTTDRate, .JurisTaxExemptions, .JurisTaxStatusCode, 0, .ClaimantHourlyPayRate)
            End With

            colWorkSheet.Add("|Claimant's Weekly Rate Before Limits/Earnings      |" & Format(dCurrentRate, "Currency"))
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If dCurrentRate > objBenefitRule.MaxCompRateWeekly And objBenefitRule.MaxCompRateWeekly > 0 Then dCurrentRate = objBenefitRule.MaxCompRateWeekly
            colWorkSheet.Add("|Claimant's Weekly Rate After Maximum Limit      |" & Format(dCurrentRate, "Currency"))

            dWeeklyRate = dCurrentRate

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Select Case objBenefitRule.FloorAmount
                Case 0
                    'There is not a valid floor, note; this could go to zero

                Case Is = g_objXClaim.AWWToCompensate
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                        dWeeklyRate = g_objXClaim.AWWToCompensate
                    Else
                        'jtodd22 07/27/2005--do nothing
                    End If

                Case Is > g_objXClaim.AWWToCompensate
                    'objBenefitRule.FloorAmount is greater than g_objXClaim.AWWToCompensate
                    'this a hard floor amount, California is only known jurisdiction
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dWeeklyRate = objBenefitRule.FloorAmount
                    End If
                    'this a soft floor amount
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                        dWeeklyRate = modFunctions.RoundStandard((g_objXClaim.AWWToCompensate), 2)
                    End If

                Case Is < Me.ClaimantJurisAWW
                    'objBenefitRule.FloorAmount less than g_objXClaim.AWWToCompensate
                    'When the Claimant's AWW is equal to or over the floor amount and
                    'the Claimant's AWW causes the weekly rate to calculate below the floor amount
                    'the Claimant gets the floor amount
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If objBenefitRule.FloorAmount > dWeeklyRate Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dWeeklyRate = objBenefitRule.FloorAmount
                        End If
                    End If
            End Select

            m_BenefitRate_Effective = dWeeklyRate

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetEffectiveRate = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:32
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim d As Double
        Dim dMoreWeeks As Double
        Dim dNewWeeks As Object
        Dim dRate As Double
        Dim dWeeks As Double
        Dim lDays As Integer
        Dim lReturn As Integer
        Dim sTmp As String
        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            dRate = 0

            lReturn = lLoadData((g_objXClaim.ClaimantHourlyPayRate))
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EarningsRequiredCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EarningsRequiredCode = g_lYesCodeID Then
                If g_objXPaymentParms.EarningsPerWeek = 0 And g_objXPaymentParms.EarningsPerMonth = 0 Then
                    m_ErrorMask = g_objXErrorMask.cNoEarnings
                    Exit Function
                End If
                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                If (DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1) Mod 7 <> 0 Then
                    m_Note = "Because you have earnings for this benefit period, the benefit payment Time frame (Beginning-Through) must be in full weeks."
                    Exit Function
                End If
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = "Claimant has been paid or has pending payment " & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks of TTP or TPD." & vbCrLf & "There is no Temporary liability left to pay."
                    Exit Function
                End If
            End If

            If m_PayHighRate = -1 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dMoreWeeks = objBenefitRule.SecondRateMaxWeeks - m_PaidPendingWeeks
                'beware of an included Waiting Period Payment
                If g_objXPaymentParms.WaitingDays > 0 Then
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    d = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1 + g_objXPaymentParms.WaitingDays
                Else
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    d = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1
                End If
                If dMoreWeeks < d / 7 Then
                    m_Note = m_Note & "The Benefit Last Date conflicts with the Jurisdictional Rule about" & vbCrLf
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    m_Note = m_Note & "paying a low Hourly Rate Claimant at a higher rate for " & objBenefitRule.SecondRateMaxWeeks & " weeks." & vbCrLf & vbCrLf
                    If g_objXPaymentParms.WaitingDays > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        m_Note = m_Note & "The Last Date expected is " & DateAdd(Microsoft.VisualBasic.DateInterval.Day, modFunctions.RoundStandard(objBenefitRule.SecondRateMaxWeeks * 7, 0) - 1, CDate(g_objXPaymentParms.WaitingStartDate)) & "."
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        m_Note = m_Note & "The Last Payment Date expected is " & DateAdd(Microsoft.VisualBasic.DateInterval.Day, modFunctions.RoundStandard(objBenefitRule.SecondRateMaxWeeks * 7, 0) - 1, CDate(g_objXPaymentParms.BenefitStartDate)) & "."
                    End If
                    m_Note = m_Note & vbCrLf & "The current Last Date is " & g_objXPaymentParms.BenefitEndDate & "."

                    m_Note = m_Note & vbCrLf & vbCrLf & "Please change the Last Payment Date."
                    Exit Function
                End If
            End If

            'if Claimant has collected maximum weeks at high rate .LoadData will set .PayHighRate to 0 (zero)
            '*********************************************************************
            'do the high limit checks
            If m_PayHighRate = 0 Then
                'beware of an included Waiting Period Payment
                If g_objXPaymentParms.WaitingDays > 0 Then
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    d = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1 + g_objXPaymentParms.WaitingDays
                Else
                    'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                    d = DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.PrimeRateMaxWeeks > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dMoreWeeks = objBenefitRule.PrimeRateMaxWeeks - m_PaidPendingWeeks
                    If dMoreWeeks < d / 7 Then
                        m_Note = m_Note & "The Benefit Last Date conflicts with the Jurisdictional Rule about" & vbCrLf
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        m_Note = m_Note & "paying a Claimant a total of " & objBenefitRule.PrimeRateMaxWeeks & " weeks." & vbCrLf
                        m_Note = m_Note & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks have been paid or are pending payment." & vbCrLf & vbCrLf
                        If g_objXPaymentParms.WaitingDays > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            m_Note = m_Note & "The Last Date expected is " & DateAdd(Microsoft.VisualBasic.DateInterval.Day, modFunctions.RoundStandard((objBenefitRule.PrimeRateMaxWeeks - m_PaidPendingWeeks) * 7, 0) - 1, CDate(g_objXPaymentParms.WaitingStartDate)) & "."
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            m_Note = m_Note & "The Last Payment Date expected is " & DateAdd(Microsoft.VisualBasic.DateInterval.Day, modFunctions.RoundStandard((objBenefitRule.PrimeRateMaxWeeks - m_PaidPendingWeeks) * 7, 0) - 1, CDate(g_objXPaymentParms.BenefitStartDate)) & "."
                        End If
                        m_Note = m_Note & vbCrLf & "The current Last Date is " & g_objXPaymentParms.BenefitEndDate & "."

                        With g_objXPaymentParms
                            If IsDate(.BenefitEndDate) Then
                                m_Note = m_Note & vbCrLf & vbCrLf & "Please reduce the Last Payment Date."
                            End If
                            If .BenefitDays > 0 Then
                                m_Note = m_Note & vbCrLf & vbCrLf & "Please reduce the Number of Days."
                            End If
                            If .TotalNumberofPayments > 0 Then
                                m_Note = m_Note & vbCrLf & vbCrLf & "Please reduce the Number of Payments."
                            End If
                            If .BenefitDays > 0 And .TotalNumberofPayments > 0 Then
                                m_Note = m_Note & vbCrLf & vbCrLf & "Please reduce the Number of Days or the Number of Payments."
                            End If
                        End With

                        Exit Function
                    End If
                End If
            End If
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)

            If m_PayHighRate = -1 Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Claimant is entitled to Higher Rate by low Hourly Rate|")
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Claimant's Hourly Rate                             |" & Format(g_objXClaim.ClaimantHourlyPayRate, "Currency"))
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                colWorkSheet.Add("|Total weeks Claimant is entitled to at high rate   |" & Format(objBenefitRule.SecondRateMaxWeeks, "###00.00"))
                colWorkSheet.Add("|Weeks paid or pending payment before this payment  |" & Format(m_PaidPendingWeeks, "###00.00"))
                colWorkSheet.Add("||")
                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object dNewWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dNewWeeks = g_objXPaymentParms.BenefitDays / objCalcBenefitRule.JurisWorkWeek
            End If

            g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW - g_objXPaymentParms.EarningsPerWeek

            If g_objXPaymentParms.EarningsPerWeek > 0 Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|The Earnings per week are | " & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))
                colWorkSheet.Add("|AWW To Compensate      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                colWorkSheet.Add("|Jurisdiction Imposed Max AWW Limit                       |" & Format(objBenefitRule.MaxAWW, "Currency"))
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If g_objXClaim.AWWToCompensate > objBenefitRule.MaxAWW Then g_objXClaim.AWWToCompensate = objBenefitRule.MaxAWW
                colWorkSheet.Add("|AWW To Compensate After Maximum Limit      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.DollarForDollar <> g_lYesCodeID Then
                sTmp = ""
                sTmp = sTmp & "|Jurisdiction imposed a minimum " & m_sBenefitTypeAbbr
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTmp = sTmp & " Rate of " & Format(objBenefitRule.FloorAmount, "Currency")
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EffectiveDateDTG. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTmp = sTmp & " per week for events after " & sDBDateFormat(objBenefitRule.EffectiveDateDTG, "Short Date")
                sTmp = sTmp & ".  For calculations this is the same as a minimum AWW of "
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PrimeRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sTmp = sTmp & Format(objBenefitRule.FloorAmount / objBenefitRule.PrimeRate, "Currency") & " per Week.|"
                colWorkSheet.Add(sTmp)
                colWorkSheet.Add("|Claimant AWW (After Imposing State Cap and Floor Limits)      |" & Format(Me.ClaimantJurisAWW, "Currency"))
            End If

            GetEffectiveRate(colWorkSheet)

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, m_BenefitRate_Effective)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function

            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If

            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dMaxRate As Double
        Dim lReturn As Integer

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0
            dMaxRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If


            modFunctions.GetMMIImpairment(m_MMIRate, m_MMIDateDTG)
            If m_MMIRate = 0 Then
                m_Warning = "There is no MMI data to calculate rate." & vbCrLf
                Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MildLow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If m_MMIRate < objBenefitRule.MildLow Then
                m_Warning = "The MMI impairment does not justify Temporary Partial payments." & vbCrLf
                Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MildLow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ModerateLow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarkedLow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Select Case m_MMIRate
                Case Is >= objBenefitRule.MarkedLow
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MarkedPay. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = TempTotalRate * m_MMIRate * (objBenefitRule.MarkedPay / 100)
                Case Is >= objBenefitRule.ModerateLow
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ModerateLowPay. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = TempTotalRate * m_MMIRate * (objBenefitRule.ModerateLowPay / 100)
                Case Is >= objBenefitRule.MildLow
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MildPay. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = TempTotalRate * m_MMIRate * (objBenefitRule.MildPay / 100)
            End Select
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (AverageWage < objBenefitRule.FloorAmount) Then
                        dBasicRate = AverageWage
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If (dBasicRate < objBenefitRule.FloorAmount) Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dBasicRate = objBenefitRule.FloorAmount
                        End If
                    End If
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (dBasicRate < objBenefitRule.FloorAmount) Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dBasicRate = objBenefitRule.FloorAmount
                    End If
                End If
            End If
            With objBenefitRule
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If .MaxCompRateWeekly > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dBasicRate > .MaxCompRateWeekly Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dBasicRate = .MaxCompRateWeekly
                    End If
                End If
            End With

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            m_BenefitRate_Original = modFunctions.RoundStandard(dBasicRate, 2)
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function

    Public Function GetPaidPendingWeeks() As Double
        Dim sTemp As String
        GetPaidPendingWeeks = 0
        m_PaidPendingWeeks = 0
        sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitTypeAbbr, m_sBenefittypeAbbr2)
        m_PaidPendingWeeks = GetBenefitWeeksPaidOrPending(sTemp)
        GetPaidPendingWeeks = m_PaidPendingWeeks



    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 1/5/2005 13:48
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    Public Function lLoadData(ByRef dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim lReturn As Integer

        Try

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EarningsRequiredCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_EarningsRequiredCode = objBenefitRule.EarningsRequiredCode


            GetPaidPendingWeeks()

            'UPGRADE_WARNING: Couldn't resolve default property of object Me.objBenefitRule.SecondRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Me.objBenefitRule.SecondRate > 0 Then
                If dEmployeeHourlyRate > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.LessThanHourlyRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dEmployeeHourlyRate < objBenefitRule.LessThanHourlyRate Then
                        m_PayHighRate = -1
                    End If
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.SecondRateMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If m_PaidPendingWeeks >= objBenefitRule.SecondRateMaxWeeks Then
                    m_PayHighRate = 0
                End If
            End If

            Me.ClaimantOriginalAWW = g_objXClaim.ClaimantOriginalAWW
            Me.ClaimantJurisAWW = g_objXClaim.ClaimantOriginalAWW
            'jlt 07/29/2003  cap aww if over the jurisdiction max aww
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If g_objXClaim.ClaimantOriginalAWW > objBenefitRule.MaxAWW And objBenefitRule.MaxAWW > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If Me.ClaimantJurisAWW > objBenefitRule.MaxAWW Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Me.ClaimantJurisAWW = objBenefitRule.MaxAWW
                End If
            End If

            lReturn = modFunctions.GrateBeneRatesDiscountedAWW(Me)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function


    Public Property BenefitRate_Effective() As Double
        Get
            BenefitRate_Effective = m_BenefitRate_Effective
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_Effective = Value
        End Set
    End Property

    Public Property BenefitRate_Original() As Double
        Get
            BenefitRate_Original = m_BenefitRate_Original
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_Original = Value
        End Set
    End Property

    Public Property DaysToPaymentDue() As Short
        Get
            DaysToPaymentDue = m_DaysToPaymentDue
        End Get
        Set(ByVal Value As Short)
            m_DaysToPaymentDue = Value
        End Set
    End Property

    Public Property EarningsPerWeek() As Double
        Get
            EarningsPerWeek = m_EarningsPerWeek
        End Get
        Set(ByVal Value As Double)
            m_EarningsPerWeek = Value
        End Set
    End Property


    Public Property OffsetPerWeek() As Double
        Get
            OffsetPerWeek = m_OffsetPerWeek
        End Get
        Set(ByVal Value As Double)
            m_OffsetPerWeek = Value
        End Set
    End Property

    Public Property PayHighRate() As Integer
        Get
            PayHighRate = m_PayHighRate
        End Get
        Set(ByVal Value As Integer)
            m_PayHighRate = Value
        End Set
    End Property

    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

