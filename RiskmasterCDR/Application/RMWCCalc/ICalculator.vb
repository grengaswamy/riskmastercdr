Option Strict Off
Option Explicit On
Public Interface _ICalculator
	 Property CheatSheetTitle As String
	 Property CalculatedPaymentAuto As Double
	 Property CalculatedPaymentCatchUp As Double
	 Property CalculatedPaymentLateCharge As Double
	 Property CalculatedPaymentPenalty As Double
	 Property CalculatedPaymentRegular As Double
	 Property CalculatedPaymentWaitingPeriod As Double
	 Property EarningsRequiredCode As Integer
	 Property ErrorMask As Integer
	 Property ErrorMaskCalcSetup As Integer
	 Property ErrorMaskSAWW As Integer
	 Property ErrorMaskSpendData As Integer
	 Property ErrorMaskFedTax As Integer
	 Property MMIDateRequiredCode As Integer
	 Property Note As String
	 Property PaidPendingMonths As Double
	 Property PaidPendingWeeks As Double
	 Property PassedInPayPeriodText As String
	 Property PayPeriodName As String
	 Property RuleTotalMonths As Double
	 Property RuleTotalWeeks As Double
	 Property UseBodyMembersCode As Integer
	 Property Warning As String
	Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
	Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer
	Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer
End Interface
Public Class ICalculator
    Implements _ICalculator

    Dim CheatSheetTitle_MemberVariable As String
    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = CheatSheetTitle_MemberVariable
        End Get
        Set(ByVal Value As String)
            CheatSheetTitle_MemberVariable = Value
        End Set
    End Property
    Dim CalculatedPaymentAuto_MemberVariable As Double
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = CalculatedPaymentAuto_MemberVariable
        End Get
        Set(ByVal Value As Double)
            CalculatedPaymentAuto_MemberVariable = Value
        End Set
    End Property
    Dim CalculatedPaymentCatchUp_MemberVariable As Double
    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = CalculatedPaymentCatchUp_MemberVariable
        End Get
        Set(ByVal Value As Double)
            CalculatedPaymentCatchUp_MemberVariable = Value
        End Set
    End Property
    Dim CalculatedPaymentLateCharge_MemberVariable As Double
    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = CalculatedPaymentLateCharge_MemberVariable
        End Get
        Set(ByVal Value As Double)
            CalculatedPaymentLateCharge_MemberVariable = Value
        End Set
    End Property
    Dim CalculatedPaymentPenalty_MemberVariable As Double
    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = CalculatedPaymentPenalty_MemberVariable
        End Get
        Set(ByVal Value As Double)
            CalculatedPaymentPenalty_MemberVariable = Value
        End Set
    End Property
    Dim CalculatedPaymentRegular_MemberVariable As Double
    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = CalculatedPaymentRegular_MemberVariable
        End Get
        Set(ByVal Value As Double)
            CalculatedPaymentRegular_MemberVariable = Value
        End Set
    End Property
    Dim CalculatedPaymentWaitingPeriod_MemberVariable As Double
    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = CalculatedPaymentWaitingPeriod_MemberVariable
        End Get
        Set(ByVal Value As Double)
            CalculatedPaymentWaitingPeriod_MemberVariable = Value
        End Set
    End Property
    Dim EarningsRequiredCode_MemberVariable As Integer
    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = EarningsRequiredCode_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            EarningsRequiredCode_MemberVariable = Value
        End Set
    End Property
    Dim ErrorMask_MemberVariable As Integer
    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = ErrorMask_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            ErrorMask_MemberVariable = Value
        End Set
    End Property
    Dim ErrorMaskCalcSetup_MemberVariable As Integer
    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = ErrorMaskCalcSetup_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            ErrorMaskCalcSetup_MemberVariable = Value
        End Set
    End Property
    Dim ErrorMaskSAWW_MemberVariable As Integer
    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = ErrorMaskSAWW_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            ErrorMaskSAWW_MemberVariable = Value
        End Set
    End Property
    Dim ErrorMaskSpendData_MemberVariable As Integer
    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = ErrorMaskSpendData_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            ErrorMaskSpendData_MemberVariable = Value
        End Set
    End Property
    Dim ErrorMaskFedTax_MemberVariable As Integer
    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = ErrorMaskFedTax_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            ErrorMaskFedTax_MemberVariable = Value
        End Set
    End Property
    Dim MMIDateRequiredCode_MemberVariable As Integer
    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = MMIDateRequiredCode_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            MMIDateRequiredCode_MemberVariable = Value
        End Set
    End Property
    Dim Note_MemberVariable As String
    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = Note_MemberVariable
        End Get
        Set(ByVal Value As String)
            Note_MemberVariable = Value
        End Set
    End Property 'for directions/messages to the user
    Dim PaidPendingMonths_MemberVariable As Double
    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = PaidPendingMonths_MemberVariable
        End Get
        Set(ByVal Value As Double)
            PaidPendingMonths_MemberVariable = Value
        End Set
    End Property
    Dim PaidPendingWeeks_MemberVariable As Double
    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = PaidPendingWeeks_MemberVariable
        End Get
        Set(ByVal Value As Double)
            PaidPendingWeeks_MemberVariable = Value
        End Set
    End Property
    'Public PassedInAbbreviation As String
    Dim PassedInPayPeriodText_MemberVariable As String
    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = PassedInPayPeriodText_MemberVariable
        End Get
        Set(ByVal Value As String)
            PassedInPayPeriodText_MemberVariable = Value
        End Set
    End Property
    Dim PayPeriodName_MemberVariable As String
    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = PayPeriodName_MemberVariable
        End Get
        Set(ByVal Value As String)
            PayPeriodName_MemberVariable = Value
        End Set
    End Property
    Dim RuleTotalMonths_MemberVariable As Double
    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = RuleTotalMonths_MemberVariable
        End Get
        Set(ByVal Value As Double)
            RuleTotalMonths_MemberVariable = Value
        End Set
    End Property
    Dim RuleTotalWeeks_MemberVariable As Double
    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = RuleTotalWeeks_MemberVariable
        End Get
        Set(ByVal Value As Double)
            RuleTotalWeeks_MemberVariable = Value
        End Set
    End Property
    Dim UseBodyMembersCode_MemberVariable As Integer
    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = UseBodyMembersCode_MemberVariable
        End Get
        Set(ByVal Value As Integer)
            UseBodyMembersCode_MemberVariable = Value
        End Set
    End Property
    Dim Warning_MemberVariable As String
    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = Warning_MemberVariable
        End Get
        Set(ByVal Value As String)
            Warning_MemberVariable = Value
        End Set
    End Property 'for warning/error messages to the user

    Public Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate



    End Function

    Public Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment



    End Function

    Public Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData



    End Function
End Class

