Option Strict Off
Option Explicit On
Public Class CWCEnumCommLifeMask
    Const sClassName As String = "CWCEnumCommLifeMask"

    Public clNoBirthdate As Short
    Public clNoGender As Short
    Public clNoPPD As Short
    Public clLowDisRate As Short
    Public clLiabilityNotPaid As Short
    Public clNoBenefitRate As Short
    Public clBadBenefitStartDate As Short
    Public cl30PlusDayWarning As Short
    Public clBadNewRate As Short
    Public clSubZeroNewRate As Short

    Public Function SetConstantValues() As Integer

        clNoBirthdate = 1
        clNoGender = 2
        clNoPPD = 4
        clLowDisRate = 8
        clLiabilityNotPaid = 16
        clNoBenefitRate = 32
        clBadBenefitStartDate = 64
        cl30PlusDayWarning = 128
        clBadNewRate = 256
        clSubZeroNewRate = 512


    End Function
End Class

