Option Strict Off
Option Explicit On
Public Class CWCSuppIncomeTexas
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "SIBS"
    Const sClassName As String = "CWCSuppIncomeTexas"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleSuppIncome"
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objWCPClaim As CWCWCPClaim

    Private m_RuleImpairmentRate As Double

    Private m_Earnings As Double
    Private m_BenefitDays As Integer
    Private m_BenefitEndDate As String
    Private m_BenefitRate_Effective As Double
    Private m_BenefitStartDate As String
    Private m_NumberofPayments As Short
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"

        Dim dMMIDisabilityRate As Double
        Dim sMMIDateDTG As String
        Dim dRateMonthly As Double
        Dim dRateWeekly As Double
        Dim lReturn As Integer
        Dim sCodeDesc As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function


            'If objBenefitRule.AllwaysRequireMMIDate = g_lYesCodeID Then
            lReturn = modFunctions.GetMMIImpairment(dMMIDisabilityRate, sMMIDateDTG)
            If lReturn = 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ImpairmentRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If dMMIDisabilityRate < objBenefitRule.ImpairmentRate Then
                    m_Note = "Claimant does not have the Disability/Impairment to claim SIBS.  " & vbCrLf & m_RuleImpairmentRate & "% is required and the Claimant has " & dMMIDisabilityRate & "%."
                    Exit Function
                End If
                '            If sMMIDateDTG > g_objXPaymentParms.BenefitStartDate Then
                '                m_Warning = "First Date for payment can not be before the MMI date."
                '                Exit Function
                '            End If
            End If
            'End If

            'build top of work sheet
            modFunctions.BuildTopOfWorkSheet(colWorkSheet)

            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            'jtodd22 take great care as AWW is per week and objbenefitrule should be month
            'jtodd22 and the benefit payment could be month, bi-weekly or weekly
            '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sCodeDesc = Trim(UCase(modFunctions.GetCodeDesc_SQL(objBenefitRule.PayPeriodCode)))

            If InStr(1, sCodeDesc, UCase("MONTH"), CompareMethod.Text) > 0 Then
                If InStr(1, m_PassedInPayPeriodText, UCase("MONTH"), CompareMethod.Text) > 0 Then
                    g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.AWWMultiplierPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|Percent of Wage to Consider    |" & objBenefitRule.AWWMultiplierPercentage & "%")
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.AWWMultiplierPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    g_objXClaim.AWWToCompensate = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * (objBenefitRule.AWWMultiplierPercentage / 100), 2)
                    colWorkSheet.Add("|Average Weekly Wage To Consider|" & Format(g_objXClaim.AWWToCompensate, "Currency"))
                    With g_objXPaymentParms
                        If .EarningsPerMonth > 0 And .EarningsPerWeek = 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .EarningsPerWeek = modFunctions.RoundStandard(.EarningsPerMonth / objBenefitRule.WeekToMonthConvFactor, 2)
                        End If
                        If .EarningsPerWeek > 0 And .EarningsPerMonth = 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            .EarningsPerMonth = modFunctions.RoundStandard(.EarningsPerWeek * objBenefitRule.WeekToMonthConvFactor, 2)
                        End If
                    End With
                    colWorkSheet.Add("|Earnings and Offered Work Per Month      |" & Format(g_objXPaymentParms.EarningsPerMonth, "Currency"))
                    colWorkSheet.Add("|Earnings and Offered Work Per Week      |" & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))

                    g_objXClaim.AWWToCompensate = g_objXClaim.AWWToCompensate - g_objXPaymentParms.EarningsPerWeek
                    colWorkSheet.Add("|Average Weekly Wage after Earnings/Offers to Consider|" & Format(g_objXClaim.AWWToCompensate, "Currency"))
                    If g_objXClaim.AWWToCompensate < 0 Then
                        m_Warning = ""
                        m_Warning = m_Warning & "The Earnings have reduced the Average Wage to less than 0 (zero)." & vbCrLf
                        m_Warning = m_Warning & "There is no Benefit payable."
                        Exit Function
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WorkLossPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|Percent of Weekly Wage to Cover|" & objBenefitRule.WorkLossPercentage & "%")
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WorkLossPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dRateWeekly = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * objBenefitRule.WorkLossPercentage / 100, 2)
                    colWorkSheet.Add("|Covered Weekly Wage            |" & Format(dRateWeekly, "Currency"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|Week to Month Converson Factor |" & objBenefitRule.WeekToMonthConvFactor)

                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dRateMonthly = dRateWeekly * objBenefitRule.WeekToMonthConvFactor

                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateMonthly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.MaxCompRateMonthly > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateMonthly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dRateMonthly > objBenefitRule.MaxCompRateMonthly Then
                            colWorkSheet.Add("|Monthly Payment Rate before Maximum Limits|" & Format(dRateMonthly, "Currency"))
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateMonthly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRateMonthly = objBenefitRule.MaxCompRateMonthly
                            colWorkSheet.Add("|Monthly Payment Rate|" & Format(dRateMonthly, "Currency"))
                        End If
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If objBenefitRule.MaxCompRateWeekly > 0 Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxRateAmountWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If dRateWeekly > objBenefitRule.MaxRateAmountWeekly Then
                                colWorkSheet.Add("|Weekly Payment Rate before Maximum Limits|" & Format(dRateWeekly, "Currency"))
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxRateAmountWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                colWorkSheet.Add("|Weekly Maximum Limit|" & Format(objBenefitRule.MaxRateAmountWeekly, "Currency"))
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxRateAmountWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dRateWeekly = objBenefitRule.MaxRateAmountWeekly
                                colWorkSheet.Add("|Weekly Payment Rate|" & Format(dRateWeekly, "Currency"))
                                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dRateMonthly = dRateWeekly * objBenefitRule.WeekToMonthConvFactor
                                colWorkSheet.Add("|Monthly Payment Rate|" & Format(dRateMonthly, "Currency"))
                            End If
                        Else
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dRateMonthly = dRateWeekly * objBenefitRule.WeekToMonthConvFactor
                            colWorkSheet.Add("|Monthly Payment Rate|" & Format(dRateMonthly, "Currency"))
                        End If
                    End If
                    lReturn = modFunctions.PaymentCruncherMonth(Me, colWorkSheet, modFunctions.RoundStandard(dRateMonthly, 2))
                End If


                If InStr(1, m_PassedInPayPeriodText, UCase("WEEK"), CompareMethod.Text) > 0 Then
                    g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.AWWMultiplierPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|Percent of Wage to Consider    |" & objBenefitRule.AWWMultiplierPercentage & "%")
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.AWWMultiplierPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    g_objXClaim.AWWToCompensate = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * (objBenefitRule.AWWMultiplierPercentage / 100), 2)
                    colWorkSheet.Add("|Average Weekly Wage To Consider|" & Format(g_objXClaim.AWWToCompensate, "Currency"))
                    colWorkSheet.Add("|Earnings and Offered Work      |" & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))
                    g_objXClaim.AWWToCompensate = g_objXClaim.AWWToCompensate - g_objXPaymentParms.EarningsPerWeek
                    colWorkSheet.Add("|Average Weekly Wage after Earnings/Offers to Consider|" & Format(g_objXClaim.AWWToCompensate, "Currency"))
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WorkLossPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    colWorkSheet.Add("|Percent of Weekly Wage to Cover|" & objBenefitRule.WorkLossPercentage & "%")
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WorkLossPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dRateWeekly = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * objBenefitRule.WorkLossPercentage / 100, 2)

                    lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRateWeekly)

                    'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
                    If m_Warning > "" Then Exit Function


                End If

            Else
                If InStr(1, sCodeDesc, UCase("WEEK"), CompareMethod.Text) > 0 Then
                End If
            End If
            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                '        colWorkSheet.Add "||"
                '        colWorkSheet.Add "|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency")
                '        Select Case Me.PayLateCharge
                '            Case 0, 2
                '                'do nothing
                '            Case 1
                '                Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                '                colWorkSheet.Add "||"
                '                colWorkSheet.Add "|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency")
                '        End Select
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lCalculatePayment = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim lReturn As Integer

        Try

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EarningsPermittedCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_EarningsRequiredCode = objBenefitRule.EarningsPermittedCode
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.ImpairmentRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_RuleImpairmentRate = objBenefitRule.ImpairmentRate

            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PayPeriodCode > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objBenefitRule.PayPeriodCode)
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Property BenefitDays() As Integer
        Get
            BenefitDays = m_BenefitDays
        End Get
        Set(ByVal Value As Integer)
            m_BenefitDays = Value
        End Set
    End Property

    Public Property BenefitEndDate() As String
        Get
            BenefitEndDate = m_BenefitEndDate
        End Get
        Set(ByVal Value As String)
            m_BenefitEndDate = Value
        End Set
    End Property

    Public Property BenefitStartDate() As String
        Get
            BenefitStartDate = m_BenefitStartDate
        End Get
        Set(ByVal Value As String)
            m_BenefitStartDate = Value
        End Set
    End Property

    Public Property BenefitRate_Effective() As Double
        Get
            BenefitRate_Effective = m_BenefitRate_Effective
        End Get
        Set(ByVal Value As Double)
            m_BenefitRate_Effective = Value
        End Set
    End Property

    Public Property NumberOfPayments() As Short
        Get
            NumberOfPayments = m_NumberofPayments
        End Get
        Set(ByVal Value As Short)
            m_NumberofPayments = Value
        End Set
    End Property

    Public Property RuleImpairmentRate() As Double
        Get
            RuleImpairmentRate = m_RuleImpairmentRate
        End Get
        Set(ByVal Value As Double)
            m_RuleImpairmentRate = Value
        End Set
    End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dMaxRate As Double
        Dim lReturn As Integer
        Dim sCodeDesc As String

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0
            dMaxRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            sCodeDesc = Trim(UCase(modFunctions.GetCodeDesc_SQL(objBenefitRule.PayPeriodCode)))

            'jtodd22 --this follows the Texas example, calculate weekly then convert to monthly
            g_objXClaim.AWWToCompensate = AverageWage
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.AWWMultiplierPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            g_objXClaim.AWWToCompensate = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * (objBenefitRule.AWWMultiplierPercentage / 100), 2)
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WorkLossPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dBasicRate = modFunctions.RoundStandard(g_objXClaim.AWWToCompensate * (objBenefitRule.WorkLossPercentage / 100), 2)

            'jtodd22 remember that Texas is a Weekly or Bi-Weekly payment schedule jurisdiction
            'jtodd22 Supplemental Income is a freak within Texas as it is a monthly payment
            If InStr(1, sCodeDesc, UCase("MONTH"), CompareMethod.Text) > 0 Then
                'jtodd22 --take the weekly BasicRate to a monthly BasicRate
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateMonthly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.MaxCompRateMonthly > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = dBasicRate * objBenefitRule.WeekToMonthConvFactor
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateMonthly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dBasicRate > objBenefitRule.MaxCompRateMonthly Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateMonthly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dBasicRate = objBenefitRule.MaxCompRateMonthly
                    End If
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.MaxCompRateWeekly > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dBasicRate > objBenefitRule.MaxCompRateWeekly Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dBasicRate = objBenefitRule.MaxCompRateWeekly
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            dBasicRate = dBasicRate * objBenefitRule.WeekToMonthConvFactor
                        End If
                    End If
                End If
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.MaxCompRateWeekly > 0 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dBasicRate > objBenefitRule.MaxCompRateWeekly Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dBasicRate = objBenefitRule.MaxCompRateWeekly
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeekToMonthConvFactor. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        dBasicRate = dBasicRate * objBenefitRule.WeekToMonthConvFactor
                    End If
                End If
            End If
            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objWCPClaim may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objWCPClaim = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Supplemental Income Benefits"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

