Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCTempPartSpendAK
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "TPP"
    Const m_sBenefittypeAbbr2 As String = "TTP"
    Const sClassName As String = "CWCTempPartSpendAK"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleTTDAA"

    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objCalculatorSetUp As Object
    Dim objSpendableIncome As Object
    Dim objSpendableIncomeRule As Object

    Public ClaimantJurisAWW As Double
    Public ClaimantOriginalAWW As Double

    Public BenefitDays As Integer
    Public BenefitEndDate As String
    Public BenefitStartDate As String
    Public BenefitRate_Effective As Double
    Public BenefitRate_Original As Double
    Public BenefitRate_TTD As Double
    Public BenefitRate_TPD As Double

    Private m_DaysToPaymentDue As Short


    Public EventDateCalendar As Date
    Public EventDateDTG As String
    Public JurisdictionMaxAww As Double
    Private m_JurisMaxBenRateWeek As Double
    Private m_JurisMaxBenRateDay As Double
    Private m_JurisMinBenRateWeek As Double
    Private m_JurisMinBenRateDay As Double


    Public PayLateCharge As Short
    Public RealMinBenRateWeek As Double
    Public RealMinBenRateDay As Double

    Private m_WaitingPeriodIsPaid As Short

    '---------------------------------------------------------------------------------------
    ' Procedure : LoadData
    ' DateTime  : 1/5/2005 13:48
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    '
    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Try
            Dim objReader As DbReader
            Dim dblNewClaimantOriginalAWW As Double
            Dim sStartDate As String
            Dim dbl As Double
            Dim dDateOfEvent As Date
            Dim lReturn As Integer
            Dim sDateOfEvent_DTG As String
            Dim sDummyDate_DTG As String
            Dim sSQL As String
            Dim sTemp As String

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            m_EarningsRequiredCode = g_lYesCodeID
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.RuleTotalWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_RuleTotalWeeks = objBenefitRule.RuleTotalWeeks
            Me.BenefitDays = g_objXPaymentParms.BenefitDays
            Me.BenefitEndDate = g_objXPaymentParms.BenefitEndDate
            Me.BenefitStartDate = g_objXPaymentParms.BenefitStartDate

            sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitTypeAbbr, m_sBenefittypeAbbr2)
            m_PaidPendingWeeks = modBenefitFunctions.GetBenefitWeeksPaidOrPending(sTemp)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:32
    ' Author    : jtodd22
    ' Purpose   :
    '--------------------------------------------------------------------------------------
    '
    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim dRate As Double
        Dim dRateCalculated As Double
        Dim dRatePost As Double
        Dim lReturn As Integer

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            dRate = 0
            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.EarningsRequiredCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.EarningsRequiredCode = g_lYesCodeID Then
                If g_objXPaymentParms.EarningsPerWeek = 0 And g_objXPaymentParms.EarningsPerMonth = 0 Then
                    m_ErrorMask = g_objXErrorMask.cNoEarnings
                    Exit Function
                End If
                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                If (DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(g_objXPaymentParms.BenefitStartDate), CDate(g_objXPaymentParms.BenefitEndDate)) + 1) Mod 7 <> 0 Then
                    m_Note = "Because you have earnings for this benefit period, the benefit payment Time frame (Beginning-Through) must be in full weeks."
                    Exit Function
                End If
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = "Claimant has been paid or has pending payment " & modFunctions.RoundStandard(m_PaidPendingWeeks, 2) & " weeks of TTP or TPD." & vbCrLf & "There is no Temporary liability left to pay."
                    Exit Function
                End If
            End If

            modFunctions.BuildTopOfWorkSheet(colWorkSheet)

            colWorkSheet.Add("|Claimant's Tax Status |" & modFunctions.GetCodeDesc_SQL((g_objXClaim.JurisTaxStatusCode)))
            colWorkSheet.Add("|Claimant's Exemptions |" & g_objXClaim.JurisTaxExemptions)
            colWorkSheet.Add("||")

            lReturn = modFunctions.GetRateSpendable(dRate, m_ErrorMaskSpendData, g_objXClaim.ClaimantOriginalAWW)
            If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then Exit Function
            If dRate = 0 Then
                m_Warning = ""
                m_Warning = m_Warning & "The compensation rate was not found or determined." & vbCrLf & vbCrLf
                m_Warning = m_Warning & "Jurisdiction is:  " & g_objXClaim.FilingStatePostalCode & vbCrLf
                m_Warning = m_Warning & "Event date:  " & g_objXClaim.DateOfEventCalar
                Exit Function
            End If

            colWorkSheet.Add("|Original Spendable Weekly Wage|" & Format(dRate, "Currency"))
            'because we are handling Temporary Partial subtract the earnings first
            colWorkSheet.Add("|Earnings per Week|" & Format(g_objXPaymentParms.EarningsPerWeek, "Currency"))
            g_objXClaim.AWWToCompensate = g_objXPaymentParms.EarningsPerWeek
            colWorkSheet.Add("|Post Injury AWW      |" & Format(g_objXClaim.AWWToCompensate, "Currency"))
            lReturn = modFunctions.GetRateSpendable(dRatePost, m_ErrorMaskSpendData, g_objXClaim.AWWToCompensate)
            If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData + m_ErrorMaskSAWW) > g_objXErrorMask.cSuccess Then Exit Function

            colWorkSheet.Add("|Post Injury Spendable Weekly Wage|" & Format(dRatePost, "Currency"))

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.FloorAmount > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If dRate < objBenefitRule.FloorAmount And dRatePost < objBenefitRule.FloorAmount Then
                    dRateCalculated = modFunctions.RoundStandard((dRate - dRatePost) * 0.8, 2)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If dRate > objBenefitRule.FloorAmount And dRatePost < objBenefitRule.FloorAmount Then
                    dRateCalculated = modFunctions.RoundStandard(((dRate / 0.8) - dRatePost) * 0.8, 2)
                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If dRate = objBenefitRule.FloorAmount And dRatePost = objBenefitRule.FloorAmount Then
                    dRateCalculated = modFunctions.RoundStandard((dRate - dRatePost) * 0.8, 2)
                End If

                dRate = dRateCalculated
            Else
                dRate = modFunctions.RoundStandard(dRate - dRatePost, 2)
            End If

            colWorkSheet.Add("     |The Effective Weekly Rate is|" & Format(dRate, "Currency"))

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)


            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function

            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|Catch Up Payment For This Period Is|" & Format(Me.Standard.CalculatedPaymentCatchUp, "Currency"))
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lCalculatePayment = g_lErrNum

        End Try

    End Function
    Private Function ClearObject() As Integer
        EventDateCalendar = CDate("12/31/1899")
        EventDateDTG = vbNullString
        JurisdictionMaxAww = -0.01
        ClaimantJurisAWW = -0.01
        BenefitRate_Effective = -0.01
        BenefitRate_TTD = -0.01
        BenefitRate_TPD = -0.01
        JurisMaxBenRateWeek = -0.01
        JurisMaxBenRateDay = -0.01
        RealMinBenRateWeek = -0.01
        RealMinBenRateDay = -0.01
        JurisMinBenRateWeek = -0.01
        JurisMinBenRateDay = -0.01
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalculatorSetUp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalculatorSetUp = Nothing
        'UPGRADE_NOTE: Object objSpendableIncome may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncome = Nothing
        'UPGRADE_NOTE: Object objSpendableIncomeRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncomeRule = Nothing
        'jtodd22 do not reset BenefitDays it is set in calling executable
        m_CheatSheetTitle = "Temporary Partial"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function

    Public Function GrateBeneRatesSpendableIncome(ByRef dAverageWage As Double, Optional ByRef TaxExemptions As Integer = 0, Optional ByRef TaxStatusCode As Integer = 0) As Integer
        Const sFunctionName As String = "GrateBeneRatesSpendableIncome"
        Dim dRate As Double
        Dim lReturn As Integer

        Try

            GrateBeneRatesSpendableIncome = 0

            If objBenefitRule Is Nothing Then
                lReturn = GetBenefitRuleTT(m_ErrorMask, m_ErrorMaskSAWW, "TTD", objBenefitRule, sDLLClassNameJRRule)
                'error messages are generated in GetBenefitRuleTT
                If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function
            End If

            If TaxStatusCode > 0 Then
                g_objXClaim.JurisTaxStatusCode = TaxStatusCode
                g_objXClaim.JurisTaxExemptions = TaxExemptions
                g_objXClaim.ClaimantOriginalAWW = dAverageWage
            End If

            lReturn = modFunctions.GetRateSpendable(dRate, m_ErrorMaskSpendData, g_objXClaim.ClaimantOriginalAWW)
            If (lReturn + m_ErrorMask + m_ErrorMaskCalcSetup + m_ErrorMaskSAWW + m_ErrorMaskSpendData) > g_objXErrorMask.cSuccess Then Exit Function
            If dRate = 0 Then
                m_Warning = ""
                m_Warning = m_Warning & "The compensation rate was not found or determined." & vbCrLf & vbCrLf
                m_Warning = m_Warning & "Jurisdiction is:  " & g_objXClaim.FilingStatePostalCode & vbCrLf
                m_Warning = m_Warning & "Event date:  " & g_objXClaim.DateOfEventCalar
                Exit Function
            End If
            BenefitRate_TTD = dRate

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.JurisMaxBenRateWeek = objBenefitRule.MaxCompRate
            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Me.JurisMaxBenRateDay = modFunctions.RoundStandard(objBenefitRule.MaxCompRate / objCalcBenefitRule.JurisWorkWeek, 2)
            Me.JurisMinBenRateWeek = 0
            Me.JurisMinBenRateDay = 0
            Me.BenefitRate_Effective = BenefitRate_TTD

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If Me.BenefitRate_TTD > objBenefitRule.MaxCompRate And objBenefitRule.MaxCompRate > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                Me.BenefitRate_TTD = objBenefitRule.MaxCompRate
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.payFloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.payFloorAmount = g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.DollarForDollar. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.DollarForDollar = g_lYesCodeID Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (dAverageWage < objBenefitRule.FloorAmount) Then
                        BenefitRate_TTD = dAverageWage
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If (BenefitRate_TTD < objBenefitRule.FloorAmount) Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            BenefitRate_TTD = objBenefitRule.FloorAmount
                        End If
                    End If
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If (BenefitRate_TTD < objBenefitRule.FloorAmount) Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.FloorAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        BenefitRate_TTD = objBenefitRule.FloorAmount
                    End If
                End If
            End If


            Me.BenefitRate_Effective = Me.BenefitRate_TTD
            GrateBeneRatesSpendableIncome = -1

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GrateBeneRatesSpendableIncome = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim lReturn As Integer

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            lReturn = modFunctions.GetRateSpendable(dBasicRate, m_ErrorMaskSpendData, AverageWage)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup + m_ErrorMaskSpendData) > g_objXErrorMask.cSuccess Then Exit Function

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
        End Try

    End Function



    Public Property DaysToPaymentDue() As Short
        Get
            DaysToPaymentDue = m_DaysToPaymentDue
        End Get
        Set(ByVal Value As Short)
            m_DaysToPaymentDue = Value
        End Set
    End Property


    Public Property JurisMaxBenRateDay() As Double
        Get
            JurisMaxBenRateDay = m_JurisMaxBenRateDay
        End Get
        Set(ByVal Value As Double)
            m_JurisMaxBenRateDay = Value
        End Set
    End Property

    Public Property JurisMaxBenRateWeek() As Double
        Get
            JurisMaxBenRateWeek = m_JurisMaxBenRateWeek
        End Get
        Set(ByVal Value As Double)
            m_JurisMaxBenRateWeek = Value
        End Set
    End Property

    Public Property JurisMinBenRateDay() As Double
        Get
            JurisMinBenRateDay = m_JurisMinBenRateDay
        End Get
        Set(ByVal Value As Double)
            m_JurisMinBenRateDay = Value
        End Set
    End Property

    Public Property JurisMinBenRateWeek() As Double
        Get
            JurisMinBenRateWeek = m_JurisMinBenRateWeek
        End Get
        Set(ByVal Value As Double)
            m_JurisMinBenRateWeek = Value
        End Set
    End Property



    'Public Property CalculatedPaymentWaitingPeriod() As Double
    '    Get
    '        CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
    '    End Get
    '    Set(ByVal Value As Double)
    '        m_CalculatedPaymentWaitingPeriod = Value
    '    End Set
    'End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property

    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property
    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalculatorSetUp may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalculatorSetUp = Nothing
        'UPGRADE_NOTE: Object objSpendableIncome may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncome = Nothing
        'UPGRADE_NOTE: Object objSpendableIncomeRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objSpendableIncomeRule = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing



    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

