Option Strict Off
Option Explicit On
Public Class CWCEnumCommutationType

    Public cAllRemaining As Short 'commpp
    Public cBeforeCommencement As Short 'commlife
    Public cFarEndLumpSum As Short 'commpp
    Public cAfterCommencement As Short 'commlife
    Public cUniformReduction As Short 'both

    Public Function SetConstantValues() As Integer

        cAllRemaining = 1 'commpp
        cBeforeCommencement = 1 'commlife
        cFarEndLumpSum = 2 'commpp
        cAfterCommencement = 2 'commlife
        cUniformReduction = 3 'both



    End Function
End Class

