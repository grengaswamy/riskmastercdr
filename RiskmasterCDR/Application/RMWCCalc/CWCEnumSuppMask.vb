Option Strict Off
Option Explicit On
Public Class CWCEnumSuppMask
    Const sClassName As String = "CWCEnumSuppMask"
    Public csuppLiabilityPaid As Short
    Public csuppLiabilityOverPaid As Short
    Public csuppNoPayable As Short
    Public csuppOverPaymentFatal As Short
    Public csuppOverPaymentNonFatal As Short

    Public Function SetConstantValues() As Integer

        csuppLiabilityPaid = 1
        csuppLiabilityOverPaid = 2
        csuppNoPayable = 4
        csuppOverPaymentFatal = 8
        csuppOverPaymentNonFatal = 16


    End Function
End Class

