Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPIXBodyMembers
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CWCPIXBodyMembers"
    Const m_TableName As String = "PI_X_BODY_MEMBER"
    'local variable to hold collection
    Private mCol As Collection
    
    Public Function LoadData(ByRef lPIRowID As Integer) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim objReader As DbReader
        Dim sSQL As String
        Try

            LoadData = 0

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " TABLE_ROW_ID,PI_ROW_ID"
            sSQL = sSQL & ",WCP_BODY_MEMBER_ID,IMPAIRMENT_PCNT, AMPUTATED_CODE"
            sSQL = sSQL & " FROM " & m_TableName
            sSQL = sSQL & " WHERE PI_ROW_ID = " & lPIRowID

            If lPIRowID = 0 Then
                sSQL = ""
                sSQL = sSQL & "SELECT"
                sSQL = sSQL & " PI_X_BODY_MEMBER.TABLE_ROW_ID"
                sSQL = sSQL & ",PI_X_BODY_MEMBER.PI_ROW_ID"
                sSQL = sSQL & ",WCP_BODY_MEMBER_ID,IMPAIRMENT_PCNT, AMPUTATED_CODE"
                sSQL = sSQL & " FROM PI_X_BODY_MEMBER, PERSON_INVOLVED, CLAIM, CLAIMANT"
                sSQL = sSQL & " WHERE CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID"
                sSQL = sSQL & " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID"
                sSQL = sSQL & " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID"
                sSQL = sSQL & " AND PERSON_INVOLVED.PI_ROW_ID = PI_X_BODY_MEMBER.PI_ROW_ID"
                sSQL = sSQL & " AND CLAIM.CLAIM_ID = " & g_objXClaim.ClaimID
            End If

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                While objReader.Read()
                    Add(objReader.GetInt32("AMPUTATED_CODE"), objReader.GetInt32("IMPAIRMENT_PCNT"), objReader.GetInt32("PI_ROW_ID"), objReader.GetInt32("TABLE_ROW_ID"), objReader.GetInt32("WCP_BODY_MEMBER_ID"))
                End While
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function Add(ByRef AmputatedCode As Integer, ByRef ImpairmentPercent As Double, ByRef PiRowId As Integer, ByRef TableRowID As Integer, ByRef WCPBodyMemberID As Integer) As CWCPIXBodyMember
        Const sFunctionName As String = "Add"
        Try
            'create a new object
            Dim objNewMember As CWCPIXBodyMember
            objNewMember = New CWCPIXBodyMember

            'set the properties passed into the method
            objNewMember.AmputatedCode = AmputatedCode
            objNewMember.ImpairmentPercent = ImpairmentPercent
            objNewMember.PiRowId = PiRowId
            objNewMember.TableRowID = TableRowID
            objNewMember.WCPBodyMemberID = WCPBodyMemberID

            mCol.Add(objNewMember)

            'return the object created
            Add = objNewMember
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            Dim ErrNum As Integer
            Dim ErrDesc, ErrSrc As String
            ErrNum = Err.Number
            ErrDesc = Err.Description
            ErrSrc = Err.Source
            Err.Raise(ErrNum, sClassName & "." & sFunctionName & "." & ErrSrc, ErrDesc)
            Exit Function


        Finally
        End Try

    End Function
    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CWCPIXBodyMember
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        GetEnumerator = mCol.GetEnumerator
    End Function

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)
    End Sub

    Public Sub New()
        MyBase.New()
        mCol = New Collection
    End Sub

    Protected Overrides Sub Finalize()
        mCol = Nothing
        MyBase.Finalize()
    End Sub

End Class

