Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCLifePen
    Implements _ICalculator
    '---------------------------------------------------------------------------------------
    ' Module    : CWCLifePen
    ' DateTime  : 1/8/2005 11:45
    ' Author    : jtodd22
    ' Purpose   : Support California's Life Pension, payable after Permanent Partial @ 70% plus expires
    ' Note      : There should be one record written to WCP_CLAIMS when the Life Pension starts
    ' ..........: The only reason to write another record is a benefit rate change.  This can be caused
    ' ..........: by a disability rate change (rare) or a commutation or a jurisdictional rule change.
    ' Note      : Changes in benefit rate caused by a commutation must be written by the commutation.
    ' Note      : Changes in benefit rate caused by jurisdictional rule change are not considered.
    ' Note      : Changes in benefit rate casued by disability rate change force a audit and
    ' ..........: payment corrections, therefore they are not considered.
    '---------------------------------------------------------------------------------------
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "LP"
    Const sClassName As String = "CWCLifePen"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleLifePen"
    Public objBenefitRule As Object
    Public objPPD As New CWCPermPartDis
    Public objCalcBenefitRule As Object
    Private oEnumLPMask As CWCEnumLPMask

    Dim objWCPClaim As CWCWCPClaim

    Private EarliestLifePenStartDate As String

    Public BeginPrntDate_Original As String
    Public BeginPrntDate_PostComm As String
    Public BeginPrntDate_Effective As String

    Public BenefitDays As Integer
    Public BenefitEndDate As String
    Public BenefitStartDate As String

    Public BenefitRate_Original As Double 'pre-commutation rate, if record was commutated
    Public BenefitRate_PostComm As Double 'only exists if a commutation
    Public BenefitRate_Effective As Double

    Public Commutation As Double
    Public DisabilityRate_Original As Double
    Public DisabilityRate_PostComm As Double
    Public DisabilityRate_Effective As Double

    Public EndDateDTGDate_Original As String
    Public EndDateDTGDate_PostComm As String
    Public Estimated_Cost_Effective As Double
    Public Estimated_Cost_Original As Double
    Public Estimated_Cost_PostComm As Double

    Public ImpairmentRate As Double

    Public LastDatePermPartial As String

    Public WarningMessage As String

    Public JurisMaxBenRateDay As Double
    Public JurisMaxBenRateWeek As Double
    Public MinBenRateDay As Double
    Public MinBenRateWeek As Double
    Public PayLateCharge As Short
    Public RecordCount As Integer

    Public StartDateDTGDate_Effective As String
    Public StartDateDTGDate_Original As String
    Public StartDateDTGDate_PostComm As String
    Public TotalDaysOfPPDLiability As Integer
    Public VocRehabDaysAdvance As Integer
    Public VocReHabSuppDaysDelay As Integer
    Public WeeksLiable_Effective As Double
    Public WeeksLiable_Original As Double
    Public WeeksLiable_PostComm As Double

    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Life Pension"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0


    End Function

    '---------------------------------------------------------------------------------------
    ' Procedure : GetBasicRate
    ' DateTime  : 5/17/2005 17:10
    ' Author    : jtodd22
    ' Purpose   : To fetch and calculate a basic rate (not award) for a claimant.  Be aware
    ' ..........: the basic rate may not be the rate used to pay an award.  Commutation can
    ' ..........: change the rate used to pay an award.
    '---------------------------------------------------------------------------------------
    '
    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim dRate As Double
        Dim lReturn As Integer

        Try
            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If g_objXClaim.ClaimantOriginalAWW > objBenefitRule.MaxAWW Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dRate = objBenefitRule.MaxAWW
            Else
                dRate = g_objXClaim.ClaimantOriginalAWW
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If dRate < objBenefitRule.MinAWW Then dRate = objBenefitRule.MinAWW
            '(PD-60) * objBenefitRule.PDMultipler * Earnings

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PDMultipler. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            dGetBasicRate = modFunctions.RoundStandard((99.75 - 60) * objBenefitRule.PDMultipler * dRate, 2)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function

    Public Function GetWeeklyRate_LifePension_NewRecord() As Double
        Const sFunctionName As String = "GetWeeklyRate_LifePension_NewRecord"

        Dim objReader As DbReader
        Dim sSQL As String
        Dim dblMinAWW As Double
        Dim dblMaxAWW As Double
        Dim dblWeeklyRate As Double
        Try
            GetWeeklyRate_LifePension_NewRecord = -0.01

            sSQL = "SELECT * FROM WCP_LIFEPEN_LIMITS" & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND '" & g_objXClaim.DateOfEventDTG & "' >= BEGIN_DATE" & " ORDER BY BEGIN_DATE DESC"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then

                dblMinAWW = objReader.GetInt32("MIN_AWW")
                dblMaxAWW = objReader.GetInt32("MAX_AWW")

                If g_objXClaim.ClaimantOriginalAWW > dblMaxAWW Then
                    dblWeeklyRate = dblMaxAWW
                Else
                    dblWeeklyRate = g_objXClaim.ClaimantOriginalAWW
                End If

                If dblWeeklyRate < dblMinAWW Then dblWeeklyRate = dblMinAWW

                GetWeeklyRate_LifePension_NewRecord = modFunctions.RoundStandard(System.Math.Round(dblWeeklyRate * 0.015, 3) * (Me.objPPD.DisabilityRate_Effective - 60), 2)
            End If



        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)

            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objWCPClaim may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objWCPClaim = Nothing
        'UPGRADE_NOTE: Object objPPD may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objPPD = Nothing
        'UPGRADE_NOTE: Object oEnumLPMask may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        oEnumLPMask = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"

        Dim dMMIDisabilityRate As Double
        Dim sMMIDateDTG As String

        Try
            Dim objReader As DbReader
            Dim sSQL As String
            Dim dBeginPrntDate, dLPStartDate, dDateOfEvent As Date
            Dim lRecordCount As Integer
            Dim lReturn As Integer
            Dim lRowID As Integer
            Dim dblBenefitRate, dblCommutation, dblPayment As Double
            Dim sNow, sBenefitStartDate As String
            'only show LP if claim has a objPPD, impairment rate >= 70%, and no objPPD liability to be paid

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            m_Note = ""

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function

            Select Case UCase(modFunctions.GetStatePostalCode_SQL((g_objXClaim.FilingStateID)))
                Case "CA"
                    lReturn = modFunctions.GetMMIImpairment(dMMIDisabilityRate, sMMIDateDTG)
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinDisability. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.MinDisability > 0 Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinDisability. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        If dMMIDisabilityRate < objBenefitRule.MinDisability Then
                            m_Note = m_Note & "Claimant must have a "
                            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinDisability. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            m_Note = m_Note & Format(objBenefitRule.MinDisability, "99.9") & "% "
                            m_Note = m_Note & "or higher disability for a Life Pension."
                            Exit Function
                        End If
                    End If
                Case Else
            End Select
            oEnumLPMask = New CWCEnumLPMask
            oEnumLPMask.SetConstantValues()

            objWCPClaim = New CWCWCPClaim
            With objPPD
                Standard.LoadData(g_objXClaim.ClaimantHourlyPayRate)
                If .RecordCount = 0 Then
                    m_ErrorMask = m_ErrorMask + oEnumLPMask.clpNoPPD
                    m_Note = m_Note & vbCrLf & "A Permanent Partial Disability is required before you can have a Life Pension." & "Please create the Permanent Partial disability before using Life Pension."
                End If
                If .DisabilityRate_Effective < 70 Then
                    m_ErrorMask = m_ErrorMask + oEnumLPMask.clpLowDisRate
                    m_Note = m_Note & vbCrLf & "This claim does not have a Life Pension Benefit (disability rate is <70%)."
                End If
            End With


            If m_ErrorMask > 0 Then
                WarningMessage = m_Note
                Exit Function
            End If

            With objPPD
                If .Liability < .LiabilityPaid Then
                    m_Note = m_Note & vbCrLf & "Permanent Disability benefit liability has not been paid; a Life Pension benefit cannot be created until the objPPD liability is completely paid."
                    m_ErrorMask = m_ErrorMask + oEnumLPMask.clpLiabilityNotPaid
                End If
            End With
            EarliestLifePenStartDate = CStr(DateAdd(Microsoft.VisualBasic.DateInterval.Day, (Me.objPPD.WeeksPayable_Original * 7) - 1 + 0.5 + 1, CDate(sDBDateFormat((Me.objPPD.BeginPrntDate_Original), "Short Date"))))
            'The Permanent and Stationary Date is payable as PPD, that is the -1
            'The time interval is converted to a long data type by rounding, any part of a day is payable, that is the + .5 to force the rounding up.
            'The first possible day of Life Pension is the day after PPD is complete, that is the + 1
            'UPGRADE_WARNING: Couldn't resolve default property of object PadDateWithZeros(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            EarliestLifePenStartDate = PadDateWithZeros(EarliestLifePenStartDate)
            GetDataBaseClaimRecords()

            ''''    'cannot change start date if commutated, so leave now
            If Me.Commutation > 0 Then Exit Function
            ''''    'if reach this point, LP benefit is not commutated, and lRecordCount is 1
            ''''
            If CDate(EarliestLifePenStartDate) < CDate(sDBDateFormat((Me.BeginPrntDate_Original), "Short Date")) Then
                Me.BeginPrntDate_Original = Mid(EarliestLifePenStartDate, 7, 4) & Mid(EarliestLifePenStartDate, 1, 2) & Mid(EarliestLifePenStartDate, 4, 2)
            End If

            Select Case Me.RecordCount
                Case 0
                    'Not expected, the function GetDataBaseClaimRecords should have insured one record

                Case 1
                    If Me.BenefitRate_Effective <> Me.GetWeeklyRate_LifePension_NewRecord Then
                        m_Note = m_Note & vbCrLf & "The Benefit rate is not as expected." & vbCrLf & "The normal causes are a change in the Employee's Average Weekly Wage (AWW)" & vbCrLf & "or in the Permanent Disability Rating or in the Event Date." & vbCrLf & "Do you want to Save the Benefit Rate matching the current Employee's Average Weekly Wage."
                        m_ErrorMask = m_ErrorMask + oEnumLPMask.clpBenefitRateChanged
                    End If

                Case Else 'commutation

            End Select



            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            lLoadData = g_lErrNum

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : CalculatePayment
    ' DateTime  : 1/8/2005 11:36
    ' Author    : jtodd22
    ' Purpose   :
    '---------------------------------------------------------------------------------------
    '
    Function lCalculatePayment(ByRef colWorkSheet As Collection) As Double
        Const sFunctionName As String = "lCalculatePayment"
        Dim dDateOfEvent As Date
        Dim dblPayment As Double
        Dim dRate As Double
        Dim lReturn As Integer
        Dim sBenefitEndDate As String
        Dim sBenefitStartDate As String

        Try

            lCalculatePayment = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            modFunctions.BuildTopOfWorkSheet(colWorkSheet)

            dblPayment = 0
            dDateOfEvent = CDate(GetFormattedDate(g_objXClaim.DateOfEventDTG))

            sBenefitStartDate = Format(Me.BenefitStartDate, "yyyymmdd")
            If sBenefitStartDate < Me.BeginPrntDate_Original Then
                m_Note = m_Note & vbCrLf & "Warning: This payment is premature." & " The benefit payment period start date you entered (" & Me.BenefitStartDate & ") is earlier than the Life Pension benefit start date (" & GetFormattedDate(Me.BeginPrntDate_Original) & ")."
                m_ErrorMask = m_ErrorMask + oEnumLPMask.clpPrematurePayment
            End If

            'build top of work sheet
            colWorkSheet.Add("|Claim Number                                      |" & g_objXClaim.ClaimNumber, CStr(1))
            colWorkSheet.Add("|Claimant Name                                     |" & g_objXClaim.ClaimantNameLFM)
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Date of Injury                                    |" & g_objXClaim.DateOfEventCalar)
            colWorkSheet.Add("|Benefit Payment Start Date                        |" & CDate(Me.BenefitStartDate))
            colWorkSheet.Add("|Benefit Payment End Date                          |" & CDate(Me.BenefitEndDate))
            colWorkSheet.Add("||")
            colWorkSheet.Add("|Claimant's Original AWW|" & Format(g_objXClaim.ClaimantOriginalAWW, "Currency"))
            'UPGRADE_WARNING: Couldn't resolve default property of object Me.objPPD.objTTD.MaxAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            colWorkSheet.Add("|State Imposed Max AWW Limit|" & Format(Me.objPPD.objTTD.MaxAWW, "Currency"))
            '*********************************************************
            Select Case g_objXClaim.FilingStatePostalCode
                Case "CA"
                    '???? Jim--are you really testing for Me.Commutation > 0 here?
                    If Me.Commutation > 0 Then
                        If Me.BenefitRate_Effective > 0 Then
                            If sBenefitStartDate > Me.BeginPrntDate_Effective Then
                                'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.JurisWorkWeek. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                dblPayment = (Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays
                            Else
                                m_Note = m_Note & vbCrLf & "The commutation date (" & GetFormattedDate(Me.BeginPrntDate_Effective) & ")" & " cannot precede the benefit payment period start date (" & Me.BenefitStartDate & ")" & "Unable to calculate payment."
                                m_ErrorMask = m_ErrorMask + oEnumLPMask.clpBadBenefitStartDate
                                GoTo No_Calculate
                            End If
                        Else
                            dblPayment = 0 'dblPayment = (Me.BenefitRate_Effective / objCalcBenefitRule.JurisWorkWeek) * Me.BenefitDays
                        End If
                    Else
                        dRate = Me.BenefitRate_Effective
                        lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

                        'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
                        If m_Warning > "" Then Exit Function

                    End If
                Case Else
            End Select
            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                Select Case Me.PayLateCharge
                    Case 0, 2
                        'do nothing
                    Case 1
                        Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                        colWorkSheet.Add("||")
                        colWorkSheet.Add("|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency"))
                End Select
            End If

No_Calculate:
            g_lErrNum = g_objXErrorMask.cSuccess
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            lCalculatePayment = g_lErrNum
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Private Function GetDataBaseClaimRecords() As Integer
        Const sFunctionName As String = "GetDataBaseClaimRecords"

        Dim objReader As DbReader
        Dim sSQL As String
        Dim dBeginPrntDate, dLPStartDate, dDateOfEvent As Date
        Dim lRowID As Integer
        Dim dblBenefitRate, dblCommutation, dblPayment As Double
        Dim sNow, sBenefitStartDate As String
        Try
            GetDataBaseClaimRecords = 0
            Me.RecordCount = 0
            Me.Commutation = 0

            sSQL = ""
            sSQL = sSQL & "SELECT * FROM WCP_CLAIMS"
            sSQL = sSQL & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'"
            sSQL = sSQL & " ORDER BY ROW_ID ASC"

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            If (objReader.Read()) Then
                Me.BeginPrntDate_Effective = Me.BeginPrntDate_Original
                Me.BenefitRate_Original = objReader.GetInt32("BENEFIT_RATE")
                Me.BenefitRate_Effective = Me.BenefitRate_Original
                '        Me.EndRecdDate_Original = objReader.GetString( "END_RECVD_DATE")
                '        Me.EstCost_Original = objReader.GetInt32( "EST_COST")
                '        Me.WeeksPayable_Original = objReader.GetInt32( "WEEKS_PAYABLE")
                Me.DisabilityRate_Original = objReader.GetInt32("DISABILITY_RATE")
                Me.DisabilityRate_Effective = Me.DisabilityRate_Original
                dblCommutation = objReader.GetInt32("COMMUTATION")
                Me.RecordCount = Me.RecordCount + 1
                'the post_comm data is the last record when ASC by row id.  will usually be only 2 records
                'need to hit every record to get commutation.  the "...Effective" data becomes the last record hit

                While (objReader.Read())
                    dblCommutation = dblCommutation + objReader.GetInt32("COMMUTATION")
                    Me.BeginPrntDate_PostComm = objReader.GetString("BEGIN_PRNT_DATE")
                    Me.BeginPrntDate_Effective = Me.BeginPrntDate_PostComm
                    Me.BenefitRate_PostComm = objReader.GetInt32("BENEFIT_RATE")
                    Me.BenefitRate_Effective = Me.BenefitRate_PostComm
                    '            Me.EndRecdDate_PostComm = objReader.GetString( "END_RECVD_DATE")
                    '            Me.EstCost_PostComm = objReader.GetInt32( "EST_COST")
                    '            Me.WeeksPayable_PostComm = objReader.GetInt32( "WEEKS_PAYABLE")
                    Me.DisabilityRate_PostComm = objReader.GetInt32("DISABILITY_RATE")
                    Me.DisabilityRate_Effective = Me.DisabilityRate_PostComm
                    Me.RecordCount = Me.RecordCount + 1
                End While
            Else
                'last date of objPPD
                dLPStartDate = f_dtGetLastDayPermPartialPayable((objPPD.WeeksPayable_Original), (objPPD.BeginPrntDate_Original))
                Me.LastDatePermPartial = Format(dLPStartDate, "yyyymmdd")
                'LP start date
                dLPStartDate = DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, dLPStartDate)
                Me.BeginPrntDate_Original = Mid(EarliestLifePenStartDate, 7, 4) & Mid(EarliestLifePenStartDate, 1, 2) & Mid(EarliestLifePenStartDate, 4, 2)
                Me.BeginPrntDate_Effective = Me.BeginPrntDate_Original

                With g_objXClaim
                    Me.BenefitRate_Original = GetWeeklyRate_LifePension_NewRecord()
                End With
                Me.BenefitRate_Effective = Me.BenefitRate_Original

                Me.DisabilityRate_Original = objPPD.DisabilityRate_Effective
                Me.DisabilityRate_Effective = Me.DisabilityRate_Original
                Me.RecordCount = 1
            End If


            GetDataBaseClaimRecords = -1
            Me.Commutation = dblCommutation
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

            GetDataBaseClaimRecords = g_lErrNum

        End Try

    End Function
    Public Function PutClaimBenefitData(ByVal dPaymentRate As Double) As Boolean
        '*************************************************************
        '*** find the record
        '*** if the record is missing (new benefit) then add a record
        '*************************************************************
        Const sFunctionName As String = "PutClaimBenefitData"
        Try
            Dim lRowID As Integer
            Dim sSQL As String

            sSQL = ""
            sSQL = sSQL & "SELECT MIN(ROW_ID) FROM WCP_CLAIM"
            sSQL = sSQL & " WHERE BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'"
            sSQL = sSQL & " AND CLAIM_ID = " & g_objXClaim.ClaimID

            lRowID = DbFactory.ExecuteScalar(g_ConnectionString, sSQL)
            sSQL = ""
            sSQL = sSQL & "SELECT * FROM WCP_CLAIM"
            sSQL = sSQL & " WHERE BENEFIT_TYPE = '" & m_sBenefitTypeAbbr & "'"
            sSQL = sSQL & " AND CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND ROW_ID = " & lRowID
            If lRowID = 0 Then 'new record
                objWCPClaim.LoadDataOriginalRecord(sSQL)
                With objWCPClaim
                    'not used here--.AttyFeeCode
                    'not used here--.AttyFeePercent
                    .BeginPrintDateEffective = CStr(Me.objPPD.LastDayPPDPayable)
                    .BenefitRateEffective = dPaymentRate
                    .BenefitType = m_sBenefitTypeAbbr
                    'not used here--.Commutation
                    .DisabilityRateEffective = Me.objPPD.DisabilityRate_Effective
                    'not used here--.EndRecvdDateEffective
                    'calculated in object--.EstCostEffective
                    'not used here--.FormID
                    'not used here--.Paid
                    'not used here--.WeeksPayableEffective
                End With
                objWCPClaim.SaveDataOriginalRecord()
            Else
                'have multiple records
                'jtodd22 do not change data
            End If
            PutClaimBenefitData = True
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            PutClaimBenefitData = g_lErrNum

        End Try

    End Function

    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property


    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function


    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

