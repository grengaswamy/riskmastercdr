Option Strict Off
Option Explicit On
Public Class CWCXTransSplitReCalc
    Const sClassName As String = "CWCXTransSplitReCalc"

    ' Object properties
    Private m_AmountDifference As Double
    Private m_AmountNew As Double
    Private m_AmountOriginal As Double
    Private m_BenefitACR As Integer
    Private m_ControlNumber As String
    Private m_Days As Integer
    Private m_FromDate As String
    Private m_Months As Integer
    Private m_ReserveTypeCode As Integer
    Private m_SumAmount As Double
    Private m_SplitRowId As Integer
    Private m_ToDate As String
    Private m_TPEarningsAmount As Double
    Private m_TransId As Integer
    Private m_TransTypeCode As Integer

    Public Property AmountDifference() As Double
        Get
            AmountDifference = m_AmountDifference
        End Get
        Set(ByVal Value As Double)
            m_AmountDifference = Value
        End Set
    End Property

    Public Property AmountNew() As Double
        Get
            AmountNew = m_AmountNew
        End Get
        Set(ByVal Value As Double)
            m_AmountNew = Value
        End Set
    End Property

    Public Property AmountOriginal() As Double
        Get
            AmountOriginal = m_AmountOriginal
        End Get
        Set(ByVal Value As Double)
            m_AmountOriginal = Value
        End Set
    End Property

    Public Property BenefitACR() As Integer
        Get
            BenefitACR = m_BenefitACR
        End Get
        Set(ByVal Value As Integer)
            m_BenefitACR = Value
        End Set
    End Property

    Public Property ControlNumber() As String
        Get
            ControlNumber = m_ControlNumber
        End Get
        Set(ByVal Value As String)
            m_ControlNumber = Value
        End Set
    End Property

    Public Property Days() As Integer
        Get
            Days = m_Days
        End Get
        Set(ByVal Value As Integer)
            m_Days = Value
        End Set
    End Property

    Public Property FromDate() As String
        Get
            FromDate = m_FromDate
        End Get
        Set(ByVal Value As String)
            m_FromDate = Value
        End Set
    End Property

    Public Property Months() As Integer
        Get
            Months = m_Months
        End Get
        Set(ByVal Value As Integer)
            m_Months = Value
        End Set
    End Property

    Public Property ReserveTypeCode() As Integer
        Get
            ReserveTypeCode = m_ReserveTypeCode
        End Get
        Set(ByVal Value As Integer)
            m_ReserveTypeCode = Value
        End Set
    End Property

    Public Property SplitRowId() As Integer
        Get
            SplitRowId = m_SplitRowId
        End Get
        Set(ByVal Value As Integer)
            m_SplitRowId = Value
        End Set
    End Property

    Public Property SumAmount() As Double
        Get
            SumAmount = m_SumAmount
        End Get
        Set(ByVal Value As Double)
            m_SumAmount = Value
        End Set
    End Property

    Public Property TPEarningsAmount() As Double
        Get
            TPEarningsAmount = m_TPEarningsAmount
        End Get
        Set(ByVal Value As Double)
            m_TPEarningsAmount = Value
        End Set
    End Property

    Public Property TransId() As Integer
        Get
            TransId = m_TransId
        End Get
        Set(ByVal Value As Integer)
            m_TransId = Value
        End Set
    End Property

    Public Property TransTypeCode() As Integer
        Get
            TransTypeCode = m_TransTypeCode
        End Get
        Set(ByVal Value As Integer)
            m_TransTypeCode = Value
        End Set
    End Property

    Public Property ToDate() As String
        Get
            ToDate = m_ToDate
        End Get
        Set(ByVal Value As String)
            m_ToDate = Value
        End Set
    End Property
End Class

