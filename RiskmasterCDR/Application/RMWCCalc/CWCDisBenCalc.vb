Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Imports Riskmaster.Security
Public Class CWCDisBenCalc
    Implements IDisposable
    Const sClassName As String = "CWCDisBenCalc"

    Private m_bInitialized As Boolean

    Private m_MaxAutoChecksLimit As Integer

    Public AWW As New CWCXAWWCalcluator
    Public BenefitCalculator As New CWCXBenefitCalculator
    Public BenefitStaticData As New CWCXBenefitStaticData
    Public BodyMembersCalc As New CWCXBodyMemberCalc
    Public objCalculator As Object
    Public objErrorMask As New CWCEnumErrorMask
    Public PaymentParms As New CWCXPaymentParm
    Public WaitPeriod As New CWCXWaitPeriod

    Public BenefitsInCalculator As New CWCBenefitsInCalc
    Public WCPClaim As New CWCWCPClaim
    Public XClaim As New CWCXClaim
    Public UserInputsOriginal As New CWCXUserInputs
    Public UserInputsWorking As New CWCXUserInputs
    Public Warning As String

    Public Function ErrorMaskProcessing(ByRef sAbbreviation As String, ByRef lErrorMask As Integer, ByRef lErrorMaskSAWW As Integer, ByRef lErrorMaskCalcSetup As Integer, ByRef lErrorMaskSpendData As Integer) As String
        ErrorMaskProcessing = modFunctions.ErrorMaskProcessing(sAbbreviation, lErrorMask, lErrorMaskSAWW, lErrorMaskCalcSetup, lErrorMaskSpendData)
    End Function

    Public ReadOnly Property Initialized() As Boolean
        Get
            Initialized = m_bInitialized
        End Get
    End Property

    Public ReadOnly Property IsSpendableJurisdiction(ByVal lErrorNumber As Integer) As Integer
        Get
            IsSpendableJurisdiction = modFunctions.IsSpendableJurisdiction(lErrorNumber)
        End Get
    End Property

    Public ReadOnly Property DependentCount(ByVal lClaimantEID As Integer, ByVal lEventID As Integer) As Integer
        Get
            DependentCount = modFunctions.GetDependentCount(lClaimantEID, lEventID)
        End Get
    End Property

    Public Property MaxAutoChecksLimit() As Integer
        Get
            If m_MaxAutoChecksLimit = 0 Then
                m_MaxAutoChecksLimit = modFunctions.GetAutoCheckSystemLimit
            End If
            MaxAutoChecksLimit = m_MaxAutoChecksLimit
        End Get
        Set(ByVal Value As Integer)
            m_MaxAutoChecksLimit = modFunctions.GetAutoCheckSystemLimit
        End Set
    End Property

    Public ReadOnly Property NoCodeID() As Integer
        Get
            NoCodeID = g_lNoCodeID
        End Get
    End Property

    Public ReadOnly Property YesCodeID() As Integer
        Get
            YesCodeID = g_lYesCodeID
        End Get
    End Property
    'Public Function IndemnityPaymtsAdjustments() As Integer
    '	IndemnityPaymtsAdjustments = modIndemnityPaymtAdj.IndemnityPaymtsAdjustments


    'End Function
    'Public Function IndemnityPaymtsDateDataCheck() As String
    '	IndemnityPaymtsDateDataCheck = modIndemnityPaymtAdj.IndemnityPaymtsDateDataCheck


    'End Function

    Public Function InitDisBenCalc(ByRef sLoginName As String, ByRef sPassword As String, ByRef sDSN As String, ByRef iDWFlag As Short, ByRef lClaimID As Integer, ByRef lTaxStatusCode As Integer, ByRef lExemptions As Integer, iClientId As Integer) As Integer
        Const sFunctionName As String = "InitDisBenCalc"
        Dim lErrorMask As Integer
        Dim lErrorNumber As Integer
        Dim lSpendableStatus As Integer
        Dim lTest As Integer
        Dim objUser As Riskmaster.Security.UserLogin
        Dim objLogin As Riskmaster.Security.Login
        Dim sErrProcedure As String
        Dim sErrDescription As String

        Try

            m_bInitialized = False

#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin SubRoutine"
#End If

            lErrorMask = 0
            g_sWarning = ""
            Warning = ""
            'g_ConnectionString = sDSN
            'Create the Login object
            objLogin = New Login(iClientId)

            'Create the user object
            objUser = objLogin.AuthenticateUser(sLoginName, sPassword, sDSN, iClientId)
            g_objUser = objLogin.AuthenticateUser(sLoginName, sPassword, sDSN, iClientId)
            g_ConnectionString = objUser.objRiskmasterDatabase.ConnectionString
            lTest = g_objXClaim.LoadData(lClaimID)
            Select Case lTest
                Case -1, 0 'normal, expected
                Case Else 'error
                    Err.Raise(70001, sClassName & "." & sFunctionName, "Failed to fetch Claim data.")
                    Exit Function
            End Select

            g_objXErrorMask.SetConstantValues()
            objErrorMask.SetConstantValues()

            g_objXClaim.JurisTaxStatusCode = lTaxStatusCode
            g_objXClaim.JurisTaxExemptions = lExemptions
            g_lYesCodeID = modFunctions.GetYesCodeID(lErrorNumber)
            g_lNoCodeID = modFunctions.GetNoCodeID(lErrorNumber)

            'Does claimant have an Average Wage?
            If g_objXClaim.ClaimantOriginalAWW = 0 Then
                'jtodd22 02/13/2008 --fatal lack of data
                Warning = ""
                Warning = Warning & "Indemnity rate(s) cannot be determined as Claimant does not have Average Wage." & vbCrLf
                Warning = Warning & "Claim Number is " & g_objXClaim.ClaimNumber
                sErrDescription = Warning
                sErrProcedure = "RMWCCalc|" & sClassName & "." & sFunctionName & "|"
                LogError(sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, sErrDescription)
                Exit Function
            End If

            GetClientDictionaryPath(iClientId)

            lTest = Me.BenefitStaticData.LoadData

            lTest = WaitPeriod.LoadData

            'Populate the Claim public property with the value from the global Claim variable
            XClaim = g_objXClaim

#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin IsSpendableIncomeJurisdiction"
#End If

            lSpendableStatus = modFunctions.IsSpendableJurisdiction(lErrorNumber)

            lSpendableStatus = lSpendableStatus + 0
            Select Case lSpendableStatus
                Case g_lYesCodeID
                    'Average Wage Calculation Setup Rule was found.
                    'Does claimant have a valid martial status?
                    If g_objXClaim.JurisTaxStatusCode = 0 Then
                        'jtodd22 02/13/2008 Can not resolve indemnity rate(s).
                        Warning = ""
                        Warning = Warning & "Claimant does not have a valid jurisdictional martial status." & vbCrLf
                        Warning = Warning & "Indemnity rate(s) can not be resolved." & vbCrLf
                        Warning = Warning & "Claim Number is " & g_objXClaim.ClaimNumber
                        sErrDescription = Warning
                        sErrProcedure = "RMWCCalc|" & sClassName & "." & sFunctionName & "|"
                        LogError(sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, sErrDescription)
                        Exit Function
                    End If

                Case g_lNoCodeID
                    'Rule for setup of tax tables was found.tIf
                Case Else 'probable errors
                    'Fatal error, tax table status was not resolved.

                    g_sErrDescription = ""
                    g_sErrDescription = g_sErrDescription & "Failed to resolved tax table status." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "An unexpected value of,'" & lSpendableStatus & "', was returned." & vbCrLf
                    g_sErrDescription = g_sErrDescription & "Jurisdiction is:  " & modFunctions.GetStateName_SQL((XClaim.FilingStateID)) & "." & vbCrLf
                    If (lSpendableStatus = 0) Then
                        g_sErrDescription = g_sErrDescription & "The Jurisdictional Rules Set Up may be missing."
                    End If
                    g_lErrNum = vbObjectError + 500
                    g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
                    g_lErrLine = Erl()
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, vbCrLf & g_sErrDescription)
                    Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)
                    Exit Function
            End Select
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "End IsSpendableIncomeJurisdiction"
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "lSpendableStatus value is:  " & lSpendableStatus
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "g_lYesCodeID value is:  " & g_lYesCodeID
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "g_lNoCodeID value is:  " & g_lNoCodeID
#End If

            InitDisBenCalc = lErrorMask

            m_bInitialized = True
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "End SubRoutine"
#End If
        Catch ex As Exception

            If lErrorNumber = g_objXErrorMask.cNoValidAWWCalSetup Then
                Warning = Warning & "No valid AWW Calculation Setup was found for this claim." & vbCrLf
                Warning = Warning & "Jurisdiction is " & g_objXClaim.FilingStatePostalCode & "." & vbCrLf
                Warning = Warning & "Event Date is " & g_objXClaim.DateOfEventCalar & "." & vbCrLf
                sErrProcedure = "RMWCCalc|" & sClassName & "." & sFunctionName & "|"
                sErrDescription = "RMJuRuLib.CJRCalcAWW.LoadDataByJurisRowIDEffectiveDate|RMWCCalc." & sClassName & "." & sFunctionName & "|" & Warning
                LogError(sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, sErrDescription)
                Exit Function
            End If

            With Err()
                g_lErrNum = Err.Number : g_sErrSrc = .Source
                If Trim(Err.Description & "") > "" Then
                    g_sErrDescription = Err.Description
                End If
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(Claim Number:  " & g_objXClaim.ClaimNumber & ")|"
            g_lErrLine = Erl()
            InitDisBenCalc = g_lErrNum
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'Clean up
            objLogin = Nothing
        End Try
    End Function

    Public Function SavePayment_Splits(ByRef BenefitType As String, ByRef dblPayment_1 As Double, ByRef sBenefitStartDate_1 As String, ByRef sBenefitEndDate_1 As String, ByRef lTransTypeCode_1 As Integer, ByRef dblPayment_2 As Double, ByRef sBenefitStartDate_2 As String, ByRef sBenefitEndDate_2 As String, ByRef lTransTypeCode_2 As Integer, ByRef dblPayment_3 As Double, ByRef sBenefitStartDate_3 As String, ByRef sBenefitEndDate_3 As String, ByRef lTransTypeCode_3 As Integer, ByRef sCheckDate As String, ByRef lBankAccId As Integer, ByRef lPaymentStatus As Integer, ByRef PayeeEID As Integer, ByRef PayeeTypeCode As Integer, Optional ByRef dblEarnings As Double = 0) As Integer
        Const sFunctionName As String = "SavePayment_Splits"
        Try

            If Not m_bInitialized Then Err.Raise(7000, "", "CDisBenCalc not initialized")

            SavePayment_Splits = -1

            SavePayment_Splits = Save_Payment_Splits(BenefitType, dblPayment_1, sBenefitStartDate_1, sBenefitEndDate_1, lTransTypeCode_1, dblPayment_2, sBenefitStartDate_2, sBenefitEndDate_2, lTransTypeCode_2, dblPayment_3, sBenefitStartDate_3, sBenefitEndDate_3, lTransTypeCode_3, sCheckDate, lBankAccId, lPaymentStatus, PayeeEID, PayeeTypeCode, dblEarnings)

            If SavePayment_Splits < 1 Then Err.Raise(vbObjectError + 7000, "", "Unable to save payment")

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            'SafeDropRecordset iRdSet
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Protected Overrides Sub Finalize()
        Try
#If DEBUGGER Then
		'UPGRADE_NOTE: #If #EndIf block was not upgraded because the expression DEBUGGER did not evaluate to True or was not evaluated. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="27EE2C3C-05AF-4C04-B2AF-657B4FB6B5FC"'
		LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc, "Begin SubRoutine"
#End If
            'g_objXClaim = Nothing
            'g_objXPaymentParms = Nothing
            'g_objWCPClaim = Nothing
            'AWW = Nothing
            'BenefitCalculator = Nothing
            'BenefitsInCalculator = Nothing
            'BenefitStaticData = Nothing
            'BodyMembersCalc = Nothing
            'objCalculator = Nothing
            'objErrorMask = Nothing
            'PaymentParms = Nothing
            'UserInputsOriginal = Nothing
            'UserInputsWorking = Nothing
            'WaitPeriod = Nothing
            'XClaim = Nothing
            'GC.Collect()
        Catch ex As Exception
#If DEBUGGER Then

			LogError "|RMWCCalc." & sClassName & "." & sFunctionName & "|", g_lErrLine, g_lErrNum, g_sErrSrc,  ex.Message
#End If
            g_lErrNum = 0
            g_sErrSrc = "RMWCCalc.CWCDisBenCalc"
            g_sErrDescription = ex.Message
            g_sErrProcedure = g_sErrSrc & "|" & "Class_Terminate|Recordset handle leaks" & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
        End Try
        MyBase.Finalize()
    End Sub

    Public Function GetPermPartAbbreviation(ByRef sWarning As String, ByRef lJurisRowID As Integer) As String
        GetPermPartAbbreviation = modFunctions.GetPermPartAbbreviation(sWarning, lJurisRowID)
    End Function

    Public Function GetPermTotalAbbreviation(ByRef sWarning As String, ByRef lJurisRowID As Integer) As String
        GetPermTotalAbbreviation = modFunctions.GetPermTotalAbbreviation(sWarning, lJurisRowID)
    End Function

    Public Function GetTempTotalAbbreviation(ByRef sWarning As String, ByRef lJurisRowID As Integer) As String
        GetTempTotalAbbreviation = modFunctions.GetTempTotalAbbreviation(sWarning, lJurisRowID)
    End Function

    Public Function GetTransTypeCodesByMapping(ByVal lBenefitLoopUpID As Integer) As String
        Const sFunctionName As String = "GetTransTypeCodesByMapping"

        Dim objReader As DbReader
        Dim sListOfTransTypeCodes As String
        Dim sSQL As String
        Try
            GetTransTypeCodesByMapping = vbNullString
            sListOfTransTypeCodes = vbNullString
            sSQL = ""
            sSQL = sSQL & "SELECT DISTINCT"
            sSQL = sSQL & " CODE_ID"
            sSQL = sSQL & " FROM WCP_TRANS_TYPES"
            sSQL = sSQL & " WHERE BENEFIT_LKUP_ID = " & lBenefitLoopUpID

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)

            While objReader.Read()
                sListOfTransTypeCodes = sListOfTransTypeCodes & objReader.GetString(0) & ","
            End While

            If Len(sListOfTransTypeCodes) > 0 Then
                sListOfTransTypeCodes = Left(sListOfTransTypeCodes, Len(sListOfTransTypeCodes) - 1)
            End If
            GetTransTypeCodesByMapping = sListOfTransTypeCodes

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try
    End Function

    Public Function GetStaticData() As Object
        With g_objXPaymentParms
            'Me.PaymentParms.AutoEndDate = .AutoEndDate
            Me.PaymentParms.AutoEndDateFirstPayt = .AutoEndDateFirstPayt
            Me.PaymentParms.AutoNumberOfPayments = .AutoNumberOfPayments
            Me.PaymentParms.AutoStartDate = .AutoStartDate
            Me.PaymentParms.BenefitDays = .BenefitDays
            Me.PaymentParms.BenefitEndDate = .BenefitEndDate
            Me.PaymentParms.BenefitStartDate = .BenefitStartDate
            ''Me.PaymentParms.CatchUpDays = .CatchUpDays
            Me.PaymentParms.CatchUpEndDate = .CatchUpEndDate
            Me.PaymentParms.CatchUpNumberOfPayments = .CatchUpNumberOfPayments
            Me.PaymentParms.CatchUpStartDate = .CatchUpStartDate
            ''Me.PaymentParms.RegularDays = .RegularDays
            Me.PaymentParms.RegularEndDate = .RegularEndDate
            Me.PaymentParms.RegularNumberOfPayments = .RegularNumberOfPayments
            Me.PaymentParms.RegularStartDate = .RegularStartDate
            ''Me.TotalNumberofPayments = .NumberofPayments
            Me.PaymentParms.PayTypeShortCode = .PayTypeShortCode
        End With
    End Function

    Public Function SQLSelectClaimDataByRowID(ByVal lRowID As Integer) As String
        Dim sSQL As String
        sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE ROW_ID = " & lRowID
        SQLSelectClaimDataByRowID = sSQL
    End Function

    Public Function SQLSelectClaimDataWithOrderBy(ByVal sBenefitType As String, ByVal sSortOrder As String) As String
        Dim sSQL As String
        sSortOrder = Trim(UCase(sSortOrder))
        sBenefitType = Trim(UCase(sBenefitType))
        sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID
        If sBenefitType > "" Then
            sSQL = sSQL & " AND BENEFIT_TYPE = '" & sBenefitType & "'"
        End If
        'sSQL = sSQL & " AND ROW_ID > 0"
        If sSortOrder > "" Then
            sSQL = sSQL & " ORDER BY ROW_ID " & sSortOrder
        End If
        SQLSelectClaimDataWithOrderBy = sSQL
    End Function

    Public Function GetIndemnityAmountPaid() As Double
        Const sFunctionName As String = "GetIndemnityAmountPaid"
        Dim objReader As DbReader
        Dim sSQL2 As String
        Dim sSQL3 As String
        Dim sTransTypeCodeIDs As String

        Try

            GetIndemnityAmountPaid = 0

            sTransTypeCodeIDs = GetTransTypeCodeIDsIndemnity()

            sSQL2 = ""
            sSQL2 = sSQL2 & ModSQLStatements.SelectCheckStatusCodeIds
            sSQL2 = sSQL2 & " AND (CODES_TEXT.CODE_DESC = 'Printed' OR CODES_TEXT.CODE_DESC = 'PRINTED')"

            sSQL3 = ""
            sSQL3 = sSQL3 & "SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) AS S_AMOUNT"
            sSQL3 = sSQL3 & " FROM FUNDS, FUNDS_TRANS_SPLIT"
            sSQL3 = sSQL3 & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
            sSQL3 = sSQL3 & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL3 = sSQL3 & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL3 = sSQL3 & " AND FUNDS.VOID_FLAG = 0"
            sSQL3 = sSQL3 & " AND FUNDS.STATUS_CODE IN (" & sSQL2 & ")"
            sSQL3 = sSQL3 & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeIDs & ")"

            If modGlobals.g_dbMake <> eDatabaseType.DBMS_IS_ACCESS Then sSQL3 = Replace(sSQL3, " AS ", " ") 'Remove alias As(notneeded except for access)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL3)
            If (objReader.Read()) Then
                GetIndemnityAmountPaid = objReader.GetInt32("S_AMOUNT")
            End If


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function GetIndemnityAmountPending() As Double
        GetIndemnityAmountPending = modFunctions.GetIndemnityAmountPending
    End Function

    Public Function MoveStaticData() As Object
        With g_objXPaymentParms
            .AutoDays = Me.PaymentParms.AutoDays
            .AutoMonths = Me.PaymentParms.AutoMonths
            'Read Only--.AutoEndDate = Me.AutoEndDate
            .AutoEndDateFirstPayt = Me.PaymentParms.AutoEndDateFirstPayt
            .AutoNumberOfPayments = Me.PaymentParms.AutoNumberOfPayments
            .AutoStartDate = Me.PaymentParms.AutoStartDate

            .BenefitDays = Me.PaymentParms.BenefitDays
            .BenefitEndDate = Me.PaymentParms.BenefitEndDate
            .BenefitMonths = Me.PaymentParms.BenefitMonths
            .BenefitStartDate = Me.PaymentParms.BenefitStartDate

            .CatchUpDays = Me.PaymentParms.CatchUpDays
            .CatchUpEndDate = Me.PaymentParms.CatchUpEndDate
            .CatchUpMonths = Me.PaymentParms.CatchUpMonths
            .CatchUpNumberOfPayments = Me.PaymentParms.CatchUpNumberOfPayments
            .CatchUpStartDate = Me.PaymentParms.CatchUpStartDate

            .EarningsPerMonth = Me.PaymentParms.EarningsPerMonth
            .EarningsPerWeek = Me.PaymentParms.EarningsPerWeek

            .RegularDays = Me.PaymentParms.RegularDays
            .RegularEndDate = Me.PaymentParms.RegularEndDate
            .RegularMonths = Me.PaymentParms.RegularMonths
            .RegularNumberOfPayments = Me.PaymentParms.RegularNumberOfPayments
            .RegularStartDate = Me.PaymentParms.RegularStartDate

            .TotalNumberofPayments = Me.PaymentParms.TotalNumberofPayments
            .WaitingDays = Me.PaymentParms.WaitingDays
            .WaitingEndDate = Me.PaymentParms.WaitingEndDate
            .WaitingMonths = Me.PaymentParms.WaitingMonths
            .WaitingNumberOfPayments = Me.PaymentParms.WaitingNumberOfPayments
            .WaitingStartDate = Me.PaymentParms.WaitingStartDate
            .PayTypeShortCode = Me.PaymentParms.PayTypeShortCode
        End With
    End Function

    Public Function GetRegularChecksPendingAmount() As Double
        GetRegularChecksPendingAmount = modFunctions.GetRegularChecksPendingAmount
    End Function

    Public Function GetRhodeIslandSpendable() As Double
        GetRhodeIslandSpendable = modBenefitFunctions.GetRISpendableIncome
    End Function

    Public Function CalculateFirstDateMonths(ByRef oUserInputs As CWCXUserInputs) As Integer
        CalculateFirstDateMonths = modFunctions.CalculateFirstDateMonths(oUserInputs)
    End Function

    Public Function CalculateLastDateMonths(ByRef oUserInputs As CWCXUserInputs) As Integer
        CalculateLastDateMonths = modFunctions.CalculateLastDateMonths(oUserInputs)
    End Function

    Public Function GetCatchupPaymentMonths(ByRef sMessage As String, ByRef oUserInputs As CWCXUserInputs) As Integer
        GetCatchupPaymentMonths = modFunctions.GetCatchupPaymentMonths(sMessage, oUserInputs)
    End Function

    Public Function GetDTGDate(ByRef sDateCal As String) As String
        GetDTGDate = modFunctions.sGetDBDateFormat(sDateCal)


    End Function
    Public Function GetFormattedDate(ByVal sDateDBFormat As String) As String
        GetFormattedDate = modFunctions.GetFormattedDate(sDateDBFormat)


    End Function
    Public Function CheckAnniversaryInCalrDateRange(ByRef sBeginCalrDate As String, ByRef sEndCalrDate As String) As Boolean
        CheckAnniversaryInCalrDateRange = modFunctions.bCheckAnniversaryInCalrDateRange(sBeginCalrDate, sEndCalrDate)


    End Function
    Public Function CheckDatesAreMonthsApart(ByRef sMessage As String, ByRef WorkingInputs As CWCXUserInputs) As Boolean
        CheckDatesAreMonthsApart = modFunctions.CheckDatesAreMonthsApart(sMessage, WorkingInputs)


    End Function
    Public Function CheckEarningsVersusMonthsIsGood(ByRef sMessage As String, ByRef WorkingInputs As CWCXUserInputs) As Boolean
        CheckEarningsVersusMonthsIsGood = modFunctions.CheckEarningsVersusMonthsIsGood(sMessage, WorkingInputs)


    End Function
    Public Function CheckEarningsVersusTimeIsGood(ByRef sMessage As String, ByVal WorkingInputs As CWCXUserInputs) As Boolean
        CheckEarningsVersusTimeIsGood = modFunctions.CheckEarningsVersusTimeIsGood(sMessage, WorkingInputs)


    End Function

    Public Function CheckEarningsVersusWeeksIsGood(ByRef sMessage As String, ByRef WorkingInputs As CWCXUserInputs) As Boolean
        CheckEarningsVersusWeeksIsGood = modFunctions.CheckEarningsVersusWeeksIsGood(sMessage, WorkingInputs)


    End Function
    Public Function CheckTemporaryTotalIsGood(ByRef sMessage As String, ByRef lJurisRowID As Integer, ByRef sDateOfEventDB As String, ByRef dHourlyRate As Double) As Integer
        CheckTemporaryTotalIsGood = modFunctions.CheckTemporaryTotalIsGood(sMessage, lJurisRowID, sDateOfEventDB, dHourlyRate)

    End Function
    Public Function GetAutoChecksPendingAmount() As Double
        GetAutoChecksPendingAmount = modFunctions.GetAutoChecksPendingAmount


    End Function
    'Public Function GetAutoChecksPendingBatches() As String
    '    GetAutoChecksPendingBatches = modIndemnityPaymtAdj.GetAutoChecksPendingBatches


    'End Function
    Public Function GetBankAccountWithSub(ByRef lBankAccId As Integer) As Integer
        GetBankAccountWithSub = modFunctions.GetBankAccountWithSub(lBankAccId)
    End Function

    Public Function GetCalculator(ByRef sAbbreviation As String, ByRef lJurisRowID As Integer, ByRef sDateOfEvent As String) As Integer
        Dim lErrorNumber As Integer

        Const sFunctionName As String = "GetCalculator"

        Try

            GetCalculator = 0

            GetCalculator = modFunctions.GetCalculator(objCalculator, UCase(sAbbreviation), lJurisRowID, sDateOfEvent)


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            GetCalculator = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function


    Public Function GetBasicRate(ByRef sAbbreviation As String) As Double
        Const sFunctionName As String = "GetBasicRate"

        Try

            Dim objCalculator As Object
            Dim lReturn As Integer
            Dim lTest As Integer

            lReturn = modFunctions.GetCalculator(objCalculator, sAbbreviation, (Me.XClaim.FilingStateID), (Me.XClaim.DateOfEventDTG))

            Select Case lReturn
                Case -1
                    'normal, expected
                Case 0
                    'Not normal, not expected
                    g_sErrDescription = "Call to modFunctions.GetCalculator, " & sAbbreviation & " for" & modFunctions.GetStateName_SQL((Me.XClaim.FilingStateID)) & ", did an early exist."
                    g_sErrProcedure = "|" & g_sThisDLLName & "." & sClassName & "." & sFunctionName & "|"
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Case Else
                    g_sErrDescription = "Call to modFunctions.GetCalculator, " & sAbbreviation & " for" & modFunctions.GetStateName_SQL((Me.XClaim.FilingStateID)) & ", errored out."
                    g_sErrProcedure = "|" & g_sThisDLLName & "." & sClassName & "." & sFunctionName & "|"
                    LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)

            End Select
            If Not objCalculator Is Nothing Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.dGetBasicRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                GetBasicRate = objCalculator.dGetBasicRate(Me.XClaim.AWWToCompensate, 0, Me.XClaim.JurisTaxExemptions, Me.XClaim.JurisTaxStatusCode, 0, Me.XClaim.ClaimantHourlyPayRate)

                'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objCalculator.Standard.Warning > "" Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Warning = objCalculator.Standard.Warning
                End If

            Else
                g_sErrDescription = "Failed to load the objCalculator," & sAbbreviation & " for" & modFunctions.GetStateName_SQL((Me.XClaim.FilingStateID)) & "."
                g_sErrProcedure = g_sErrSrc & "|" & g_sThisDLLName & "." & sClassName & "." & sFunctionName & "|"

                LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
                Warning = g_sErrDescription
            End If

            If Not objCalculator Is Nothing Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                With objCalculator.Standard
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    lTest = .ErrorMask
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    lTest = .ErrorMaskSAWW
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    lTest = .ErrorMaskCalcSetup
                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    lTest = .ErrorMaskSpendData

                    'UPGRADE_WARNING: Couldn't resolve default property of object objCalculator.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Warning = Warning & modFunctions.ErrorMaskProcessing(sAbbreviation, .ErrorMask, .ErrorMaskSAWW, .ErrorMaskCalcSetup, .ErrorMaskSpendData)
                End With
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & g_sThisDLLName & "." & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetPayeeType_Claimant() As Integer
        GetPayeeType_Claimant = modFunctions.GetPayeeType_Claimant
    End Function

    Public Function GetPaymentType(ByRef oUserInputs As CWCXUserInputs) As Integer
        GetPaymentType = modFunctions.GetPaymentType(oUserInputs)
    End Function

    Public Function IsPureCatchupPayment(ByRef sMessage As String, ByRef oUserInputs As CWCXUserInputs) As Boolean
        IsPureCatchupPayment = modFunctions.IsPureCatchupPayment(sMessage, oUserInputs)
    End Function

    Public Function MoveXClaimData() As Boolean
        MoveXClaimData = False
        g_objXClaim.ClaimantOriginalAWW = Me.XClaim.ClaimantOriginalAWW
        g_objXClaim.JurisTaxExemptions = Me.XClaim.JurisTaxExemptions
        g_objXClaim.JurisTaxStatusCode = Me.XClaim.JurisTaxStatusCode
        MoveXClaimData = True
    End Function

    Public Sub LogError(ByVal sProcedure As String, ByVal ErrLine As Integer, ByRef ErrNumber As Integer, ByRef ErrSource As String, ByRef ErrDescription As String, Optional ByRef bBubbleErr As Boolean = False)
        ErrorLog.LogError(sProcedure, ErrLine, ErrNumber, ErrSource, ErrDescription, bBubbleErr)
    End Sub

    Public Function GetNumberOfMonths(ByRef sFirstDateofPaymentDB As String, ByRef sLastDateofPaymentDB As String) As Integer
        GetNumberOfMonths = modFunctions.GetNumberOfMonths(sFirstDateofPaymentDB, sLastDateofPaymentDB)
    End Function

    Public Function RoundStandard(ByRef dInput As Double, ByRef lPlaces As Integer) As Double
        RoundStandard = modFunctions.RoundStandard(dInput, lPlaces)
    End Function

    Public Function RoundBankers(ByRef dInput As Double, ByRef lPlaces As Integer) As Double
        RoundBankers = modFunctions.RoundBankers(dInput, lPlaces)
    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : OverRideRateRead
    ' DateTime  : 12/6/2007 09:52
    ' Author    : jtodd22
    ' Purpose   : Returns the most current Benefit Rate of the Benefit Abbreviation
    '---------------------------------------------------------------------------------------
    '
    Public Function OverRideRateRead(ByRef sBenefitAbbr As String) As Double
        OverRideRateRead = modFunctions.OverRideRateRead(sBenefitAbbr)
    End Function

    Public Function OverRideRateWrite(ByRef sBenefitAbbr As String, ByRef dBenefitRate As Double, ByRef sOrderNumber As String, ByRef lOrderType As Integer) As Boolean
        OverRideRateWrite = modFunctions.WriteClaimBenefitData(sBenefitAbbr, dBenefitRate, sOrderNumber, lOrderType)
    End Function

    Public Function ValidateEventDateFirstPaytDate(ByRef oUserInputs As CWCXUserInputs) As Integer
        ValidateEventDateFirstPaytDate = modFunctions.ValidateEventDateFirstPaytDate(oUserInputs)
    End Function

    Public Sub Dispose() Implements IDisposable.Dispose
        Finalize()
    End Sub

End Class

