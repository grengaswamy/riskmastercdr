Option Strict Off
Option Explicit On
Module Registry
	
	Public Const REG_SZ As Integer = 1
	Public Const REG_DWORD As Integer = 4
	Public Const HKEY_CLASSES_ROOT As Integer = &H80000000
	Public Const HKEY_CURRENT_USER As Integer = &H80000001
	Public Const HKEY_LOCAL_MACHINE As Integer = &H80000002
	Public Const HKEY_USERS As Integer = &H80000003
	Public Const ERROR_NONE As Short = 0
	Public Const ERROR_BADDB As Short = 1
	Public Const ERROR_BADKEY As Short = 2
	Public Const ERROR_CANTOPEN As Short = 3
	Public Const ERROR_CANTREAD As Short = 4
	Public Const ERROR_CANTWRITE As Short = 5
	Public Const ERROR_OUTOFMEMORY As Short = 6
	Public Const ERROR_ARENA_TRASHED As Short = 7
	Public Const ERROR_ACCESS_DENIED As Short = 8
	Public Const ERROR_INVALID_PARAMETERS As Short = 87
	Public Const ERROR_NO_MORE_ITEMS As Short = 259
	Public Const KEY_ALL_ACCESS As Integer = &H3F
	Public Const KEY_QUERY_VALUE As Integer = &H1
	Public Const READ_CONTROL As Integer = &H20000
	Public Const STANDARD_RIGHTS_READ As Integer = (READ_CONTROL)
	
	Public Const REG_OPTION_NON_VOLATILE As Short = 0
	Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Integer) As Integer
	Declare Function RegCreateKeyEx Lib "advapi32.dll"  Alias "RegCreateKeyExA"(ByVal hKey As Integer, ByVal lpSubKey As String, ByVal Reserved As Integer, ByVal lpClass As String, ByVal dwOptions As Integer, ByVal samDesired As Integer, ByVal lpSecurityAttributes As Integer, ByRef phkResult As Integer, ByRef lpdwDisposition As Integer) As Integer
	Declare Function RegOpenKeyEx Lib "advapi32.dll"  Alias "RegOpenKeyExA"(ByVal hKey As Integer, ByVal lpSubKey As String, ByVal ulOptions As Integer, ByVal samDesired As Integer, ByRef phkResult As Integer) As Integer
	Declare Function RegQueryValueExString Lib "advapi32.dll"  Alias "RegQueryValueExA"(ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByVal lpData As String, ByRef lpcbData As Integer) As Integer
	Declare Function RegQueryValueExLong Lib "advapi32.dll"  Alias "RegQueryValueExA"(ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByRef lpData As Integer, ByRef lpcbData As Integer) As Integer
	Declare Function RegQueryValueExNULL Lib "advapi32.dll"  Alias "RegQueryValueExA"(ByVal hKey As Integer, ByVal lpValueName As String, ByVal lpReserved As Integer, ByRef lpType As Integer, ByVal lpData As Integer, ByRef lpcbData As Integer) As Integer
	Declare Function RegSetValueExString Lib "advapi32.dll"  Alias "RegSetValueExA"(ByVal hKey As Integer, ByVal lpValueName As String, ByVal Reserved As Integer, ByVal dwType As Integer, ByVal lpValue As String, ByVal cbData As Integer) As Integer
	Declare Function RegSetValueExLong Lib "advapi32.dll"  Alias "RegSetValueExA"(ByVal hKey As Integer, ByVal lpValueName As String, ByVal Reserved As Integer, ByVal dwType As Integer, ByRef lpValue As Integer, ByVal cbData As Integer) As Integer
	
	
	
	Private Function SetValueEx(ByVal hKey As Integer, ByRef sValueName As String, ByRef vValue As Object) As Integer
		Dim lValue As Integer
		Dim sValue As String
		Dim lType As Integer
		
		'UPGRADE_WARNING: VarType has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		Select Case VarType(vValue)
			Case VariantType.Short, VariantType.Integer, VariantType.Byte
				'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				lValue = vValue
				lType = REG_DWORD
				SetValueEx = RegSetValueExLong(hKey, sValueName, 0, lType, lValue, 4)
			Case Else
				'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				sValue = "" & vValue & Chr(0)
				lType = REG_SZ
				SetValueEx = RegSetValueExString(hKey, sValueName, 0, lType, sValue, Len(sValue))
		End Select
		


	End Function
	
	Public Function SetKeyValue(ByVal sKeyName As String, ByVal sValueName As String, ByVal vValueSetting As Object) As Integer
		Dim lRetVal As Integer 'result of the SetValueEx function
		Dim hKey As Integer 'handle of open key
		
		'open the specified key
		'lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sKeyName, 0, KEY_ALL_ACCESS, hKey)
		
		lRetVal = RegCreateKeyEx(HKEY_LOCAL_MACHINE, sKeyName, 0, vbNullString, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 0, hKey, 0)
		
		If lRetVal = 0 Then
			lRetVal = SetValueEx(hKey, sValueName, vValueSetting)
		End If
		
		RegCloseKey(hKey)
		
		SetKeyValue = lRetVal
		


	End Function
	Private Function QueryValueEx(ByVal lhKey As Integer, ByVal szValueName As String, ByRef vValue As Object) As Integer
		Dim cch As Integer
		Dim lrc As Integer
		Dim lType As Integer
		Dim lValue As Integer
		Dim sValue As String
		
        Try
            ' Determine the size and type of data to be read
            lrc = RegQueryValueExNULL(lhKey, szValueName, 0, lType, 0, cch)
            If lrc <> ERROR_NONE Then Exit Function

            Select Case lType
                ' For strings
                Case REG_SZ
                    sValue = New String(Chr(0), cch)
                    lrc = RegQueryValueExString(lhKey, szValueName, 0, lType, sValue, cch)
                    If lrc = ERROR_NONE Then
                        'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        vValue = Left(sValue, cch - 1)
                    Else
                        'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        vValue = Nothing
                    End If ' For DWORDS
                Case REG_DWORD
                    lrc = RegQueryValueExLong(lhKey, szValueName, 0, lType, lValue, cch)
                    'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If lrc = ERROR_NONE Then vValue = lValue
                Case Else
                    'all other data types not supported
                    lrc = -1
            End Select

        Catch ex As Exception

        Finally
            QueryValueEx = lrc

        End Try

	End Function
	
	Public Function QueryValue(ByVal sKeyName As String, ByVal sValueName As String) As Object
		Dim lRetVal As Integer
		'result of the API functions
		Dim hKey As Integer 'handle of opened key
		Dim vValue As Object 'setting of queried value
		lRetVal = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sKeyName, 0, KEY_QUERY_VALUE Or STANDARD_RIGHTS_READ, hKey) ' dbas 06/25 changed from FULL ACCESS to registry key to read-only. When used in IIS context it is a permission issue
		lRetVal = QueryValueEx(hKey, sValueName, vValue)
		RegCloseKey(hKey)
		'UPGRADE_WARNING: Couldn't resolve default property of object vValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: Couldn't resolve default property of object QueryValue. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		QueryValue = vValue


	End Function
End Module

