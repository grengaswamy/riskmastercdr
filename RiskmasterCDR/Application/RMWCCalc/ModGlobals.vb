Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Module modGlobals
	Const sModuleName As String = "modGlobals"
	Public Const g_sThisDLLName As String = "RMWCCalc"
	Public Const g_sWCPClaimsTableName As String = "WCP_CLAIMS"
    Public Const g_sDLLClassCalcSetUp As String = "RMJuRuLib.CJRCalcBenefit"
	
	Public Const g_sCodingWarning1 As String = "Calculation coding is not complete for benefit '"
	Public Const g_sCodingWarning2 As String = "frmDEClaimWCIndemnityBenefits.CalculateNumbers"

    Public g_objLogin As Riskmaster.Security.Login
    Public g_objUser As Riskmaster.Security.UserLogin
    Public g_dbMake As eDatabaseType
	
    Public g_hEnv As Integer

    Public g_ConnectionString As String
	Public g_dbConn1 As Short
	Public g_dbConn2 As Short
	
	Public g_sErrProcedure As String
	Public g_lErrLine As Integer
	Public g_lErrNum As Integer
	Public g_sErrSrc As String
	Public g_sErrDescription As String
	
	Public g_objXClaim As New CWCXClaim
	Public g_objWCPClaim As New CWCWCPClaim
	Public g_objXPaymentParms As New CWCXPaymentParm
	Public g_objXErrorMask As New CWCEnumErrorMask
	
	Public g_bInitialized As Boolean
	Public g_dblMaxBenefit As Double
	Public g_dblMaxAWW As Double
	Public g_dblMinBenefit As Double
	
	Public g_dblClaimantAWW As Double
	
	Public g_lYesCodeID As Integer
	Public g_lNoCodeID As Integer
	
	Public g_sDictPath As String
	Public g_sWarning As String
	
	Public Const INTERVALDAY As String = "d"
	Public Const INTERVALWEEK As String = "ww"
	Public Const INTERVALMONTH As String = "m"
End Module

