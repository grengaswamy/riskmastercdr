Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Module modBenefitFunctions
	Const sModuleName As String = "modBenefitFunctions"
	
	Public Function f_dGetClaimantWeeklyWage(ByRef lClaimID As Integer, ByRef lPiEmpRowId As Integer) As Double
		Dim dbl As Double
        Dim objReader As dbReader
        Dim sSQL As String
        Try

            f_dGetClaimantWeeklyWage = 0
            dbl = 0

            sSQL = "SELECT AWW FROM CLAIM_AWW WHERE CLAIM_ID = " & lClaimID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then dbl = objReader.GetInt32("AWW")


            If dbl <= 0.01 Then
                If lPiEmpRowId > 0 Then
                    sSQL = "SELECT WEEKLY_RATE FROM PERSON_INVOLVED WHERE PI_ROW_ID = " & lPiEmpRowId
                Else
                    lPiEmpRowId = f_lGetPIRowID(lClaimID)
                    sSQL = "SELECT WEEKLY_RATE FROM PERSON_INVOLVED WHERE PI_ROW_ID = " & lPiEmpRowId
                End If
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                If (objReader.Read()) Then dbl = objReader.GetInt32("WEEKLY_RATE")

            End If

            If dbl > 0.01 Then
                f_dGetClaimantWeeklyWage = dbl
                g_objXClaim.ClaimantOriginalAWW = dbl
            End If
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".f_dGetClaimantWeeklyWage|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function
    Public Function f_dtGetLastDayPermPartialPayable(ByRef dblWeeksPayable As Double, ByRef sBeginPrntDate As String) As Date
        '******************************************************************************
        '***General Notes
        '***1)  Commutations do not affect the last date.
        '***2)  The earliest last date is weeks of PPD from start of P & S date.
        '***3)  VocReHab delays the last date.
        '******************************************************************************
        '***calendar days paid of Voc Rehab Supp can not be greater then calendar days
        '***paid of Voc ReHab
        '******************************************************************************
        Try
            Dim lVocRehabDays As Integer
            Dim dblVocRehabSuppDaysDelay, dblTotalDaysPPDLiability As Double
            Dim dBeginPrntDate, dLastPPDDate As Date
            Dim sNow As String

            lVocRehabDays = f_lGetDaysofVocRehab((g_objXClaim.ClaimID), (g_objXClaim.FilingStateID))
            dblVocRehabSuppDaysDelay = f_dGetEffectiveDaysOfVocRehab((g_objXClaim.ClaimID), (g_objXClaim.FilingStateID))
            dblTotalDaysPPDLiability = (dblWeeksPayable * 7 + 0.05) + lVocRehabDays
            If Len(sBeginPrntDate & "") > 7 Then
                dBeginPrntDate = CDate(GetFormattedDate(sBeginPrntDate))
                'last date of PPD
                f_dtGetLastDayPermPartialPayable = DateAdd(Microsoft.VisualBasic.DateInterval.Day, dblTotalDaysPPDLiability - dblVocRehabSuppDaysDelay, dBeginPrntDate)
            End If
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".f_dtGetLastDayPermPartial|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function GetLastDayBenefitPaid(ByVal lClaimID As Integer, ByVal lFilingStateID As Integer, ByVal Screen As Short) As String

        Dim objReader As dbReader
        Dim lTransTypeID As Integer
        Dim sSQL As String
        Dim sTransTypeCodes As String
        Try

            sSQL = "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES" & " WHERE STATE_ROW_ID = " & lFilingStateID & " AND SCREEN_ID = " & Screen
            GetCodeList(sSQL, sTransTypeCodes)
            If Len(Trim(sTransTypeCodes)) = 0 Then Exit Function

            sSQL = "SELECT MAX(TO_DATE) " & " FROM FUNDS_TRANS_SPLIT, FUNDS" & " WHERE TRANS_TYPE_CODE IN (" & sTransTypeCodes & ")" & " AND CLAIM_ID = " & lClaimID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetLastDayBenefitPaid = objReader.GetString(0)
            End If


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".GetLastDayBenefitPaid|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally

        End Try

    End Function
    Public Function f_lGetDaysofVocRehab(ByRef lClaimID As Integer, ByRef lFilingStateID As Integer, Optional ByRef sFromDate As String = "", Optional ByRef sToDate As String = "") As Integer
        Const sFunctionName As String = "f_lGetDaysofVocRehab"
        Dim objReader As dbReader
        Dim objScreens As CWCEnumEnumDisBenCalcScrn
        Dim sSQL As String
        Dim lDays As Integer
        Dim sDateClause As String

        Try

            objScreens = New CWCEnumEnumDisBenCalcScrn
            objScreens.SetConstantValues()

            sSQL = "SELECT  FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE" & " FROM FUNDS, FUNDS_TRANS_SPLIT, WCP_TRANS_TYPES" & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID" & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = WCP_TRANS_TYPES.CODE_ID" & " AND FUNDS.VOID_FLAG = 0" & " AND FUNDS.CLAIM_ID = " & lClaimID & " AND WCP_TRANS_TYPES.SCREEN_ID = " & objScreens.cVocRehabScn & " AND WCP_TRANS_TYPES.STATE_ROW_ID = " & lFilingStateID & " ORDER BY FUNDS_TRANS_SPLIT.TO_DATE"

            If Len(Trim(sFromDate)) > 0 Then
                sDateClause = " AND FUNDS_TRANS_SPLIT.FROM_DATE >= '" & sFromDate & "'"
            End If
            If Len(Trim(sToDate)) > 0 Then
                sDateClause = sDateClause & " AND FUNDS_TRANS_SPLIT.TO_DATE <= '" & sToDate & "'"
            End If
            sSQL = sSQL & sDateClause

            'get number of days in each voc rehab record
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While (objReader.Read())
                'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                lDays = lDays + DateDiff(Microsoft.VisualBasic.DateInterval.Day, CDate(GetFormattedDate(objReader.GetString(0))), CDate(GetFormattedDate(objReader.GetString(1))))
            End While
            f_lGetDaysofVocRehab = lDays


            'UPGRADE_NOTE: Object objScreens may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objScreens = Nothing
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    Public Function f_dGetEffectiveDaysOfVocRehab(ByRef lClaimID As Integer, ByRef lFilingStateID As Integer, Optional ByRef sFromDate As String = "", Optional ByRef sToDate As String = "") As Double
        Const sFunctionName As String = "f_dGetEffectiveDaysOfVocRehab"
        Dim objReader As dbReader
        Dim objScreens As CWCEnumEnumDisBenCalcScrn
        Dim sSQL As String
        Dim col As Collection
        Dim v As Object 'v(0) = amount, v(1) = FROM_DATE, v(2) = Benefit_Type/Short_Code
        Dim sTransTypeCodeList As String
        Dim dblPPDRate, dblEffectiveDays As Double
        Dim sDateClause As String

        Try

            f_dGetEffectiveDaysOfVocRehab = 0
            Exit Function

            objScreens = New CWCEnumEnumDisBenCalcScrn
            objScreens.SetConstantValues()

            'this SQL is wrong, will need new sql statement to grab the proper list of trans type codes
            'that demark SUPP payments.
            sSQL = "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES" & " WHERE STATE_ROW_ID = " & lFilingStateID & " AND UTIL32_CBO_ID = " & objScreens.cVocRehabScn
            GetCodeList(sSQL, sTransTypeCodeList)

            If Len(Trim(sFromDate)) > 0 Then
                sDateClause = " AND FUNDS_TRANS_SPLIT.FROM_DATE >= '" & sFromDate & "'"
            End If
            If Len(Trim(sToDate)) > 0 Then
                sDateClause = sDateClause & " AND FUNDS_TRANS_SPLIT.TO_DATE <= '" & sToDate & "'"
            End If

            'this SQL is also wrong.  when payment is saved the ScreenId is being set according to the trans type.
            'it should be set to 9 (VocRehabSupp)
            sSQL = "SELECT  FUNDS_TRANS_SPLIT.AMOUNT, FUND_TRANS_SPLIT.FROM_DATE, CODES.SHORT_CODE" & " FROM FUNDS, FUNDS_TRANS_SPLIT, WCP_TRANS_TYPES, CODES" & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID" & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = WCP_TRANS_TYPES.CODE_ID" & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE = CODES.CODE_ID" & " AND FUNDS.VOID_FLAG = 0" & " AND FUNDS.CLAIM_ID = " & lClaimID & " AND WCP_TRANS_TYPES.SCREEN_ID =" & objScreens.cVocRehabsuppscn & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodeList & ")" & " AND WCP_TRANS_TYPES.STATE_ROW_ID = " & lFilingStateID & " ORDER BY FUNDS_TRANS_SPLIT.TO_DATE"

            sSQL = sSQL & sDateClause

            col = New Collection
            ReDim v(2)
            'get amount and FROM_DATE for each SUPP record
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While (objReader.Read())
                'UPGRADE_WARNING: Couldn't resolve default property of object v(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                v(0) = objReader.GetInt32(0) 'amount
                'UPGRADE_WARNING: Couldn't resolve default property of object v(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                v(1) = objReader.GetString(1) 'from date
                'UPGRADE_WARNING: Couldn't resolve default property of object v(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                v(2) = objReader.GetString(2) 'Short_Code/Benefit_Type
                col.Add(v)
            End While


            For Each v In col
                'UPGRADE_WARNING: Couldn't resolve default property of object v(1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object v(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sSQL = "SELECT BENEFIT_RATE FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & lClaimID & " AND BENEFIT_TYPE = '" & v(2) & "'" & " AND BEGIN_PRNT_DATE => '" & v(1) & "'"
                objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
                While (objReader.Read())
                    dblPPDRate = objReader.GetInt32(0)
                    'UPGRADE_WARNING: Couldn't resolve default property of object v(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dblPPDRate > 0 Then dblEffectiveDays = dblEffectiveDays + modFunctions.RoundStandard(v(0) / dblPPDRate, 4)
                End While

            Next v

            'UPGRADE_NOTE: Object objScreens may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objScreens = Nothing

            f_dGetEffectiveDaysOfVocRehab = dblEffectiveDays
        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)

        End Try

    End Function

    Public Function f_iCalculateAge(ByRef dBirthDate As Date, ByRef dPermStatDate As Date) As Short
        Dim lTmp As Integer

        'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
        lTmp = Int(DateDiff(Microsoft.VisualBasic.DateInterval.DayOfYear, dBirthDate, dPermStatDate) / 365.25)
        '??? why the first if?  can't be older than 87?
        If (lTmp > 32000 Or lTmp < 0) Then lTmp = 0
        f_iCalculateAge = lTmp



    End Function
    Public Function f_dPVWeeksRemaining(ByVal iWeeks As Short) As Double

        Dim objReader As dbReader
        Dim sSQL As String
        Dim dbl As Double
        Try
            sSQL = "SELECT PRESENT_VALUE FROM WCP_PRSNT_VALUE_PD" & " WHERE WEEKS = " & iWeeks
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then dbl = objReader.GetInt32(0)

            f_dPVWeeksRemaining = modFunctions.RoundStandard(dbl, 4)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".f_dPVWeeksRemaining|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function

    Public Function f_bPVLowerAndWeeks(ByVal dblPV_In As Double, ByRef dblPVLower As Double, ByRef iWeeks As Short) As Boolean

        Dim objReader As dbReader
        Dim sSQL As String
        Try
            dblPVLower = 0
            iWeeks = 0

            sSQL = "SELECT PRESENT_VALUE, WEEKS FROM WCP_PRSNT_VALUE_PD" & " WHERE PRESENT_VALUE <= " & dblPV_In & " ORDER BY PRESENT_VALUE DESC"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                dblPVLower = objReader.GetInt32(0)
                iWeeks = objReader.GetInt32(1)
            End If

            f_bPVLowerAndWeeks = True

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".f_bPVLowerAndWeeks|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function

    Public Function f_dPVHigher(ByVal dblPV As Double) As Double

        Dim objReader As dbReader
        Dim sSQL As String
        Try
            sSQL = "SELECT MIN(PRESENT_VALUE) AS MIN_PV" & " FROM WCP_PRSNT_VALUE_PD" & " WHERE PRESENT_VALUE >= " & dblPV
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                f_dPVHigher = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".f_dPVHigher|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function
    Public Function f_dGetLifeBenefitPV_Age(ByRef sYearColumn As String, ByRef iAge As Short) As Double
        Const sFunctionName As String = "f_dGetLifeBenefitPV_Age"

        Dim objReader As dbReader
        Dim sSQL As String
        Try
            sSQL = "SELECT " & sYearColumn & " FROM WCP_PRSNT_VALUE_LP" & " WHERE AGE = " & iAge & " AND SEX_CODE_ID = " & g_objXClaim.ClaimantSexCode & " AND STATE_ROW_ID = " & g_objXClaim.FilingStateID
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                f_dGetLifeBenefitPV_Age = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function
    Public Function f_lGetPIRowID(ByVal lClaimID As Integer) As Integer
        Const sFunctionName As String = "f_lGetPIRowID"

        Dim objReader As dbReader
        Dim sSQL As String
        Dim sSQLPIType As String
        Try
            f_lGetPIRowID = -1

            sSQLPIType = "(SELECT CODES.CODE_ID" & " FROM GLOSSARY, CODES, CODES_TEXT" & " WHERE GLOSSARY.SYSTEM_TABLE_NAME ='PERSON_INV_TYPE'" & " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID" & " AND CODES.CODE_ID = CODES_TEXT.CODE_ID" & " AND (CODES_TEXT.CODE_DESC ='EMPLOYEE'" & " OR CODES_TEXT.CODE_DESC = 'Employee'))"

            sSQL = "SELECT PERSON_INVOLVED.PI_ROW_ID" & " FROM CLAIM, CLAIMANT, PERSON_INVOLVED" & " WHERE CLAIM.CLAIM_ID = " & lClaimID & " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID" & " AND CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID" & " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID" & " AND PERSON_INVOLVED.PI_TYPE_CODE = " & sSQLPIType

            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                f_lGetPIRowID = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally


        End Try

    End Function
    Public Function GetBenefitAmountPaidOrPending2(ByVal sBenefitLookUpID As String, Optional ByVal sCommutationDate As String = "") As Double
        Const sFunctionName As String = "GetBenefitAmountPaidOrPending2"

        Dim objReader As DbReader
        Dim iRdSt2 As Short
        Dim sSQL As String
        Dim sTransTypeCodes As String
        Dim dblSubTotal As Double
        Dim sFromDate As String
        Try
            GetBenefitAmountPaidOrPending2 = 0
            dblSubTotal = 0
            sFromDate = vbNullString

            If Len(Trim(sCommutationDate)) > 0 Then sFromDate = " AND FROM_DATE >= '" & sCommutationDate & "'"

            sSQL = ""
            sSQL = sSQL & "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES"
            sSQL = sSQL & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID
            sSQL = sSQL & " AND BENEFIT_LKUP_ID IN (" & sBenefitLookUpID & ")"
            GetCodeList(sSQL, sTransTypeCodes)
            If Len(Trim(sTransTypeCodes)) = 0 Then Exit Function 'amount is 0

            sSQL = ""
            sSQL = sSQL & "SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) AS S_AMOUNT"
            sSQL = sSQL & " FROM FUNDS, FUNDS_TRANS_SPLIT"
            sSQL = sSQL & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
            sSQL = sSQL & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL = sSQL & " AND FUNDS.VOID_FLAG = 0"
            sSQL = sSQL & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodes & ")"
            sSQL = sSQL & sFromDate
            'jtodd22 --no STATUS_CODE, we want everything

            If modGlobals.g_dbMake <> eDatabaseType.DBMS_IS_ACCESS Then sSQL = Replace(sSQL, " AS ", " ") 'Remove alias As(notneeded except for access)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                dblSubTotal = modFunctions.RoundStandard(objReader.GetInt32("S_AMOUNT"), 2)
            End If
            objReader.Close()

            sSQL = ""
            sSQL = sSQL & "SELECT"
            sSQL = sSQL & " SUM(FUNDS_AUTO_SPLIT.AMOUNT) AS S_AMOUNT"
            sSQL = sSQL & " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT"
            sSQL = sSQL & " WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID"
            sSQL = sSQL & " AND FUNDS_AUTO.CLAIM_ID = " & g_objXClaim.ClaimID
            sSQL = sSQL & " AND FUNDS_AUTO.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
            sSQL = sSQL & " AND FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE IN (" & sTransTypeCodes & ")"
            sSQL = sSQL & sFromDate

            If modGlobals.g_dbMake <> eDatabaseType.DBMS_IS_ACCESS Then sSQL = Replace(sSQL, " AS ", " ") 'Remove alias As(notneeded except for access)
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If objReader.Read() Then
                dblSubTotal = dblSubTotal + modFunctions.RoundStandard(objReader.GetInt32("S_AMOUNT"), 2)
            End If

            GetBenefitAmountPaidOrPending2 = dblSubTotal

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : GetBenefitWeeksPaidOrPending
    ' DateTime  : 6/7/2005 14:30
    ' Author    : jtodd22
    ' Purpose   : To get the total weeks of indemnity benefit paid or pending(AutoChecks)
    ' Note      : The passed variable sBenfitLookUpID is a CSV list of numbers.
    ' ..........: Normally there is only one value in sBenefitLookUpID.
    ' Note      : Commutations are not addressed in this function.
    '---------------------------------------------------------------------------------------
    '
    Public Function GetBenefitWeeksPaidOrPending(ByVal sBenefitLookUpID As String) As Double
        Const sFunctionName As String = "GetBenefitWeeksPaidOrPending"
        Try
            Dim lReturn As Integer
            Dim sSQL As String
            Dim sTransTypeCodes As String

            GetBenefitWeeksPaidOrPending = 0

            If sBenefitLookUpID & "" = "" Then Exit Function

            sSQL = ""
            sSQL = sSQL & "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES"
            sSQL = sSQL & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID
            sSQL = sSQL & " AND BENEFIT_LKUP_ID IN (" & sBenefitLookUpID & ")"
            GetCodeList(sSQL, sTransTypeCodes)
            If Len(Trim(sTransTypeCodes)) = 0 Then Exit Function 'amount is 0
            Dim colPayments As New CWCPayments
            lReturn = colPayments.LoadData(sTransTypeCodes)
            If lReturn = 0 Then
                GetBenefitWeeksPaidOrPending = CDbl(colPayments.Weeks)
            End If


        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    '---------------------------------------------------------------------------------------
    ' Procedure : GetBenefitWeeksPaidOrPending2
    ' DateTime  : 09/06/2006 14:30
    ' Author    : jtodd22
    ' Purpose   : To get the total weeks of indemnity benefit paid or pending(AutoChecks)
    ' Note      : The passed variable sBenfitLookUpID is a CSV list of numbers.
    ' ..........: Normally there is only one value in sBenefitLookUpID.
    ' Note      : Commutations are not addressed in this function.
    '---------------------------------------------------------------------------------------
    '
    Public Function GetBenefitWeeksPaidOrPending2(ByRef lErrorMask As Integer, ByRef sAbbr1 As String, ByRef sAbbr2 As String) As Double
        Const sFunctionName As String = "GetBenefitWeeksPaidOrPending2"
        Try
            Dim lReturn As Integer
            Dim sBenefitLookUpID As String
            Dim sSQL As String
            Dim sTransTypeCodes As String

            GetBenefitWeeksPaidOrPending2 = 0

            sBenefitLookUpID = modFunctions.GetBenefitLookUpIDs(sAbbr1, sAbbr2)
            If Trim(sBenefitLookUpID & "") = "" Then
                lErrorMask = g_objXErrorMask.cNoTransTypeCode
                Exit Function
            End If

            sSQL = ""
            sSQL = sSQL & "SELECT DISTINCT CODE_ID FROM WCP_TRANS_TYPES"
            sSQL = sSQL & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID
            sSQL = sSQL & " AND BENEFIT_LKUP_ID IN (" & sBenefitLookUpID & ")"
            GetCodeList(sSQL, sTransTypeCodes)
            If Len(Trim(sTransTypeCodes)) = 0 Then
                lErrorMask = g_objXErrorMask.cNoTransTypeCode
                Exit Function 'amount is 0
            End If
            Dim colPayments As New CWCPayments
            lReturn = colPayments.LoadData(sTransTypeCodes)
            If lReturn = 0 Then
                GetBenefitWeeksPaidOrPending2 = CDbl(colPayments.Weeks)
            End If

        Catch ex As Exception
            With Err
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Function GetYearsLifeExpectancy(ByVal sPermDisDate As String) As Double
        'find life expectancy based on birthday for Jurisdiction, age and sex

        Dim objReader As dbReader
        Dim sSQL As String
        Dim iAge As Short
        Dim sSex As String
        Dim dBirthDate As Date
        Dim dPermStatDate As Date
        Try
            sSQL = "SELECT CODES.SHORT_CODE FROM CODES WHERE CODE_ID = " & g_objXClaim.ClaimantSexCode
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                sSex = objReader.GetString(0)
            Else
                Err.Raise(7000, "", "Unable to retrieve sex for sexcode " & g_objXClaim.ClaimantSexCode)
            End If


            dBirthDate = CDate(GetFormattedDate(g_objXClaim.ClaimantBirthDateDTG))
            dPermStatDate = CDate(GetFormattedDate(sPermDisDate))
            iAge = f_iCalculateAge(dBirthDate, dPermStatDate)
            If iAge = 0 Then Err.Raise(7000, "", "Unable to retrieve age for birth date " & dBirthDate & " & permstatdate " & dPermStatDate)

            sSQL = "SELECT YRS_REMAINING" & " FROM WCP_CDC_LIFE_EXPCT" & " WHERE STATE_ROW_ID = " & g_objXClaim.FilingStateID & " AND AGE = " & iAge & " AND SEX = '" & sSex & "'"
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetYearsLifeExpectancy = objReader.GetInt32(0)
            Else
                Err.Raise(7000, "Unable to retrieve Yrs_Remaining.")
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & ".GetYearsLifeExpectancy|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function
    Public Function GetYearsLifeExpectancyB(ByVal sPermDisDate As String) As Double
        Const sRoutineName As String = "GetYearsLifeExpectancyB"
        'find life expectancy based on birthday for Jurisdiction, age and sex

        Dim objReader As dbReader
        Dim sSQL As String
        Dim iAge As Short
        Dim dBirthDate As Date
        Dim dPermStatDate As Date
        Try
            GetYearsLifeExpectancyB = 0

            If (g_objXClaim.ClaimantSexCode + 0) = 0 Then
                Err.Raise(7000, "", "Claimant gender is missing " & g_objXClaim.ClaimantSexCode)
            End If

            dBirthDate = CDate(GetFormattedDate(g_objXClaim.ClaimantBirthDateDTG))
            dPermStatDate = CDate(GetFormattedDate(sPermDisDate))
            iAge = f_iCalculateAge(dBirthDate, dPermStatDate)
            If iAge = 0 Then Err.Raise(7000, "", "Unable to retrieve age for birth date " & dBirthDate & " & permstatdate " & dPermStatDate)

            sSQL = "SELECT YRS_REMAINING" & " FROM WCP_CDC_LIFE_EXPCB" & " WHERE JURIS_ROW_ID = " & g_objXClaim.FilingStateID & " AND AGE = " & iAge & " AND SEXCODE_ID = " & g_objXClaim.ClaimantSexCode
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            If (objReader.Read()) Then
                GetYearsLifeExpectancyB = objReader.GetInt32(0)
            Else
                Err.Raise(7000, "Unable to retrieve Yrs_Remaining.")
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sRoutineName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally


        End Try

    End Function

    Public Function GetParentAccountID(ByRef lSubAccount As Object) As Integer
        Const sFunctionName As String = "GetParentAccountID"

        Dim objReader As dbReader
        Dim lTmp As Integer
        Try
            GetParentAccountID = 0

            'UPGRADE_WARNING: Couldn't resolve default property of object lSubAccount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            objReader = DbFactory.GetDbReader(g_ConnectionString, "SELECT ACCOUNT_ID FROM BANK_ACC_SUB WHERE SUB_ROW_ID=" & lSubAccount)
            If objReader.Read() Then
                GetParentAccountID = objReader.GetInt32(0)
            End If

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()

            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function SQLSelectClaimDataWithOrderBy(ByVal sBenefitType As String, ByVal sSortOrder As String) As String
        Dim sSQL As String

        sSortOrder = Trim(UCase(sSortOrder))
        sBenefitType = Trim(UCase(sBenefitType))
        sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE CLAIM_ID = " & g_objXClaim.ClaimID

        If sBenefitType > "" Then
            sSQL = sSQL & " AND BENEFIT_TYPE = '" & sBenefitType & "'"
        End If

        'sSQL = sSQL & " AND ROW_ID > 0"

        If sSortOrder > "" Then
            sSQL = sSQL & " ORDER BY ROW_ID " & sSortOrder
        End If

        SQLSelectClaimDataWithOrderBy = sSQL



    End Function
    Public Function SQLSelectClaimDataByRowID(ByVal lRowID As Integer) As String
        Dim sSQL As String

        sSQL = "SELECT * FROM WCP_CLAIMS" & " WHERE ROW_ID = " & lRowID

        SQLSelectClaimDataByRowID = sSQL


    End Function

    Public Function GetPPDLimits(ByVal dPercent As Double) As Integer
        Const sFunctionName As String = "GetPPDLimits"
        Dim objReader As dbReader
        Try
            GetPPDLimits = 0

            g_dblMaxBenefit = 0
            g_dblMaxAWW = 0
            g_dblMinBenefit = 0

            If dPercent > 0 Then
                objReader = DbFactory.GetDbReader(g_ConnectionString, ModSQLStatements.SelectPPD_BenefitRateLimits(dPercent))
                If (objReader.Read()) Then
                    g_dblMaxBenefit = objReader.GetInt32("MAX_BENEFIT")
                    g_dblMinBenefit = objReader.GetInt32("MIN_BENEFIT")
                Else

                    Err.Raise(vbObjectError + 7000, sFunctionName, "Unable to retrieve PPD limits")
                End If
            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With

            GetPPDLimits = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)


        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function GetRISpendableIncome() As Double
        Const sFunctionName As String = "GetRISpendableIncome"
        Dim bMustCalculate As Boolean
        Dim dRate As Double
        Dim lErrorMaskSpendData As Integer
        Dim lReturn As Integer
        Dim sTaxStatus As String

        Dim objFed As Object
        Dim objJuris As Object
        Dim EarningsSubject As Double ' Earnings subject to withholding
        Dim SpendableNet As Double ' Spendable Net after taxes
        Dim StandardDeduction As Double
        Dim WithHoldingFed As Double ' Federal withholding
        Dim WithHoldingState As Double ' State withholding
        Dim FICA As Double ' FICA
        Try
            GetRISpendableIncome = 0
            bMustCalculate = False
            sTaxStatus = UCase(GetCodeDesc_SQL((g_objXClaim.JurisTaxStatusCode)))
            If InStr(1, sTaxStatus, "MARRIED") > 0 Then
                If g_objXClaim.JurisTaxExemptions < 2 Then g_objXClaim.JurisTaxExemptions = 2
                If g_objXClaim.JurisTaxExemptions > 11 Then bMustCalculate = True
            End If
            If InStr(1, sTaxStatus, "SINGLE") > 0 Then
                If g_objXClaim.JurisTaxExemptions < 1 Then g_objXClaim.JurisTaxExemptions = 1
                If g_objXClaim.JurisTaxExemptions > 10 Then bMustCalculate = True
            End If
            If Not bMustCalculate Then
                lReturn = modFunctions.GetRateSpendable(dRate, lErrorMaskSpendData, g_objXClaim.ClaimantOriginalAWW)
                If (lReturn + lErrorMaskSpendData) > g_objXErrorMask.cSuccess Then Exit Function
                GetRISpendableIncome = dRate
            Else
                'do the calculation
                objFed = New RMJuRuLib.CJRSpendableIncomeCalcFed()
                objJuris = New RMJuRuLib.CJRSpendableIncomeCalcState()
                'UPGRADE_WARNING: Couldn't resolve default property of object objFed.LoadDataByClaimData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lReturn = objFed.LoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, modFunctions.RoundStandard((g_objXClaim.ClaimantOriginalAWW), 2), g_objXClaim.JurisTaxStatusCode)
                'UPGRADE_WARNING: Couldn't resolve default property of object objJuris.LoadDataByClaimData. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                lReturn = objJuris.LoadDataByClaimData(g_objUser, g_objXClaim.FilingStateID, g_objXClaim.DateOfEventDTG, modFunctions.RoundStandard((g_objXClaim.ClaimantOriginalAWW), 2), g_objXClaim.JurisTaxStatusCode)
                SpendableNet = g_objXClaim.ClaimantOriginalAWW

                'UPGRADE_WARNING: Couldn't resolve default property of object objFed.FICAPercentage. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                FICA = g_objXClaim.ClaimantOriginalAWW * objFed.FICAPercentage

                'UPGRADE_WARNING: Couldn't resolve default property of object objFed.FederalFedExemptionValueAmount. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                EarningsSubject = g_objXClaim.ClaimantOriginalAWW - (g_objXClaim.JurisTaxExemptions * objFed.FederalFedExemptionValueAmount)
                'UPGRADE_WARNING: Couldn't resolve default property of object objFed.FedMultipler. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Use of Null/IsNull() detected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="2EED02CB-5C0E-4DC1-AE94-4FAA3A30F51A"'
                If IsDBNull(objFed.FedMultipler) Or objFed.FedMultipler = 0 Then
                    WithHoldingFed = 0
                Else
                    WithHoldingFed = EarningsSubject - 0
                End If
                SpendableNet = SpendableNet - StandardDeduction - WithHoldingFed = WithHoldingState - FICA
                'UPGRADE_NOTE: Object objFed may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objFed = Nothing
                'UPGRADE_NOTE: Object objJuris may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objJuris = Nothing

            End If
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sModuleName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            
        End Try

    End Function
End Module

