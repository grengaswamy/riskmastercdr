Option Strict Off
Option Explicit On
Public Class CWCImpairIncome
    Implements _ICalculator
    'support for ICalculator
    Private m_CheatSheetTitle As String
    Private m_EarningsRequiredCode As Integer
    Private m_ErrorMask As Integer
    Private m_ErrorMaskFedTax As Integer
    Private m_ErrorMaskSAWW As Integer
    Private m_ErrorMaskCalcSetup As Integer
    Private m_ErrorMaskSpendData As Integer
    Private m_MMIDateRequiredCode As Integer
    Private m_Note As String
    Private m_PaidPendingMonths As Double
    Private m_PaidPendingWeeks As Double
    Private m_PassedInAbbreviation As String
    Private m_PassedInPayPeriodText As String
    Private m_PayPeriodName As String
    Private m_RuleTotalMonths As Double
    Private m_RuleTotalWeeks As Double
    Private m_UseBodyMembersCode As Integer
    Private m_Warning As String

    Private m_CalculatedPaymentAuto As Double
    Private m_CalculatedPaymentCatchUp As Double
    Private m_CalculatedPaymentLateCharge As Double
    Private m_CalculatedPaymentPenalty As Double
    Private m_CalculatedPaymentRegular As Double
    Private m_CalculatedPaymentWaitingPeriod As Double

    Public objUserInputsOriginal As CWCXUserInputs
    Public objUserInputsWorking As CWCXUserInputs

    'support for Class
    Const m_sBenefitTypeAbbr As String = "IIBS" 'Texas
    Const m_sBenefittypeAbbr2 As String = "IB" 'Florida

    Const sClassName As String = "CWCImpairIncome"
    Const sDLLClassNameJRRule As String = "RMJuRuLib.CJRBenefitRuleImpairIncome"
    Public objBenefitRule As Object
    Public objCalcBenefitRule As Object
    Dim objWCPClaim As CWCWCPClaim

    Private m_MMIDateDBFormat As String
    Private m_BeginPrntDate_Original As String

    Private m_DisabilityRate_Original As Double

    Private m_BenefitEndDate As String
    Private m_BenefitStartDate As String
    Private m_NumberofPayments As Short
    Private m_OffsetPerWeek As Double


    Public Function lCalculatePayment(ByRef colWorkSheet As Collection) As Integer
        Const sFunctionName As String = "lCalculatePayment"
        Dim dRate As Double
        Dim dTemp As Double
        Dim lReturn As Integer
        Dim sTemp As String

        Try

            'jtodd22 09/12/2005 presume no failure, let error handling reset CalculatePayment
            lCalculatePayment = g_objXErrorMask.cSuccess

            lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
            If m_Note > "" Then Exit Function
            If m_Warning > "" Then Exit Function

            lReturn = modFunctions.GetMMIImpairment(m_DisabilityRate_Original, m_MMIDateDBFormat)
            If m_DisabilityRate_Original = 0 Then m_ErrorMask = g_objXErrorMask.cNoImpairment
            If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.YesCodeID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MMIDateRequired. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.MMIDateRequired = objBenefitRule.YesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.YesCodeID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                m_MMIDateRequiredCode = objBenefitRule.YesCodeID
                If m_MMIDateDBFormat = "" Then m_ErrorMask = g_objXErrorMask.cNoImpairmentDate
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > 0) Then Exit Function
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.YesCodeID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MMIDateBenefitStartAfterCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.MMIDateBenefitStartAfterCode = objBenefitRule.YesCodeID Then

                    If CDate(g_objXPaymentParms.BenefitStartDate) <= CDate(modFunctions.GetFormattedDate(m_MMIDateDBFormat)) Then
                        m_Warning = "Payment start date can not be on or before the MMI date"
                        Exit Function
                    End If

                End If
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.YesCodeID. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MMIDateBenefitStartOnCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.MMIDateBenefitStartOnCode = objBenefitRule.YesCodeID Then

                    If CDate(g_objXPaymentParms.BenefitStartDate) < CDate(modFunctions.GetFormattedDate(m_MMIDateDBFormat)) Then
                        m_Warning = "Payment start date can not be before the MMI date"
                        Exit Function
                    End If

                End If
            End If

            sTemp = modFunctions.GetBenefitLookUpIDs(m_sBenefitTypeAbbr, m_sBenefittypeAbbr2)
            If Len(sTemp) > 1 Then
                m_PaidPendingWeeks = modBenefitFunctions.GetBenefitWeeksPaidOrPending(sTemp)
            Else
                m_Warning = ""
                m_Warning = "Programmatic error; the Benefit Abbreviation(s) was/were not resolved for payments."
                Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeeksPerPercentagePoint. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.WeeksPerPercentagePoint > 0 Then
                If m_DisabilityRate_Original < 1 Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeeksPerPercentagePoint. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    m_RuleTotalWeeks = objBenefitRule.WeeksPerPercentagePoint * (m_DisabilityRate_Original * 100)
                Else
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeeksPerPercentagePoint. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    m_RuleTotalWeeks = objBenefitRule.WeeksPerPercentagePoint * m_DisabilityRate_Original
                End If
            Else
                m_RuleTotalWeeks = 0
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.PayMaxWeeks > 0 Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If m_RuleTotalWeeks > objBenefitRule.PayMaxWeeks Then m_RuleTotalWeeks = objBenefitRule.PayMaxWeeks
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PayMaxWeeks. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If m_RuleTotalWeeks = 0 Then m_RuleTotalWeeks = objBenefitRule.PayMaxWeeks
            End If

            If m_RuleTotalWeeks > 0 Then
                If m_PaidPendingWeeks >= m_RuleTotalWeeks Then
                    m_Warning = m_Warning & "Claimant has been paid or has pending payments totaling " & modFunctions.RoundStandard(m_PaidPendingWeeks, 3) & " weeks." & vbCrLf
                    m_Warning = m_Warning & "Claimant is entitled to " & modFunctions.RoundStandard(m_RuleTotalWeeks, 3) & " weeks." & vbCrLf & vbCrLf
                    m_Warning = m_Warning & "This Benefit is fully paid."
                    Exit Function
                End If
                With g_objXPaymentParms
                    dTemp = 0
                    dTemp = dTemp + (.AutoDays + .CatchUpDays + .RegularDays + .WaitingDays) / 7
                    dTemp = dTemp + m_PaidPendingWeeks - m_RuleTotalWeeks
                    If dTemp > 0 Then
                        m_Warning = ""
                        m_Warning = m_Warning & "Claimant has been paid or has pending payments totaling " & modFunctions.RoundStandard(m_PaidPendingWeeks, 3) & " weeks." & vbCrLf
                        m_Warning = m_Warning & "Claimant is entitled to " & modFunctions.RoundStandard(m_RuleTotalWeeks, 3) & " weeks." & vbCrLf & vbCrLf
                        m_Warning = m_Warning & "Claimant's entitlement will be pushed over time limit by " & vbCrLf
                        m_Warning = m_Warning & Format(dTemp, "0.0##") & " weeks or " & Format(dTemp * 7, "0") & " days." & vbCrLf
                        m_Warning = m_Warning & "Please reduce the days in the payment by " & Format(dTemp * 7, "0") & " days."
                        Exit Function
                    End If
                End With
            End If

            modFunctions.BuildTopOfWorkSheet(colWorkSheet)
            'build benefit type detail
            If m_OffsetPerWeek > 0 Then
                colWorkSheet.Add("||")
                colWorkSheet.Add("|The Offset per week is | " & Format(m_OffsetPerWeek, "Currency"))
                g_objXClaim.AWWToCompensate = g_objXClaim.ClaimantOriginalAWW - m_OffsetPerWeek
                colWorkSheet.Add("|AWW to compensate      | " & Format(g_objXClaim.AWWToCompensate, "Currency"))
            End If

            dRate = dGetBasicRate(g_objXClaim.AWWToCompensate, (g_objXClaim.ClaimantTTDRate), 0, 0, 0, (g_objXClaim.ClaimantHourlyPayRate))

            colWorkSheet.Add("|The basic weekly benefit rate is| " & Format(dRate, "Currency"))
            colWorkSheet.Add("|The Impairment rating is| " & Format(m_DisabilityRate_Original, "00.00"))

            lReturn = modFunctions.PaymentCruncherWeek(Me, colWorkSheet, dRate)

            'jtodd22 generate m_Warning in modFunctions.PaymentCruncherWeek for over payments
            If m_Warning > "" Then Exit Function


            If Me.Standard.CalculatedPaymentCatchUp > 0.0# Then
                '        Select Case Me.PayLateCharge
                '            Case 0, 2
                '                'do nothing
                '            Case 1
                '                Me.Standard.CalculatedPaymentLateCharge = Me.Standard.CalculatedPaymentCatchUp * 0.1
                '                colWorkSheet.Add "||"
                '                colWorkSheet.Add "|Late Charge Amount|" & Format(Me.Standard.CalculatedPaymentLateCharge, "Currency")
                '        End Select
            End If
            g_lErrNum = g_objXErrorMask.cSuccess

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lCalculatePayment = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "(" & g_objXClaim.FilingStatePostalCode & ")|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function
    Public Function dGetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double
        Const sFunctionName As String = "dGetBasicRate"
        Dim bRuleIsLocal As Boolean
        Dim dBasicRate As Double
        Dim lReturn As Integer
        Dim objRule2 As Object
        Dim sAbbr As String
        Dim sWarning As String

        Try

            dGetBasicRate = 0
            bRuleIsLocal = False
            dBasicRate = 0

            If objBenefitRule Is Nothing Then
                bRuleIsLocal = True
                lReturn = lLoadData(dEmployeeHourlyRate)
                If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then Exit Function
                If m_Note > "" Then Exit Function
                If m_Warning > "" Then Exit Function
            End If

            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseCurrentTTDRules. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseCurrentTTDRules = g_lYesCodeID Then
                sAbbr = modFunctions.GetTempTotalAbbreviation(sWarning, (g_objXClaim.FilingStateID))
                If sAbbr > "" Then
                    lReturn = modFunctions.GetCalculator(objRule2, sAbbr, (g_objXClaim.FilingStateID), (g_objXClaim.DateOfEventDTG))
                    If (lReturn + m_ErrorMask + m_ErrorMaskSAWW + m_ErrorMaskCalcSetup > g_objXErrorMask.cSuccess) Then
                        Exit Function
                    End If
                    'UPGRADE_WARNING: Couldn't resolve default property of object objRule2.Standard. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    dBasicRate = objRule2.Standard.GetBasicRate(AverageWage, TempTotalRate, g_objXClaim.JurisTaxExemptions, g_objXClaim.JurisTaxStatusCode, 0, g_objXClaim.ClaimantHourlyPayRate)
                    'dBasicRate = dBasicRate * objBenefitRule.75 Percent variable when it is ready. not coded Yet

                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If objBenefitRule.MaxCompRateWeekly > 0 And dBasicRate > objBenefitRule.MaxCompRateWeekly Then dBasicRate = objBenefitRule.MaxCompRateWeekly
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If dBasicRate < objBenefitRule.MinCompRate Then dBasicRate = objBenefitRule.MinCompRate
                    'UPGRADE_NOTE: Object objRule2 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                    objRule2 = Nothing
                Else
                    'handle missing TT
                    Exit Function
                End If
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.UseCurrentTTDRules. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            If objBenefitRule.UseCurrentTTDRules <> g_lYesCodeID Then
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.PercentageOfAWW. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                dBasicRate = modFunctions.RoundStandard(AverageWage * (objBenefitRule.PercentageOfAWW / 100), 2)
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MaxCompRateWeekly. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If objBenefitRule.MaxCompRateWeekly > 0 And dBasicRate > objBenefitRule.MaxCompRateWeekly Then dBasicRate = objBenefitRule.MaxCompRateWeekly
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.MinCompRate. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                If dBasicRate < objBenefitRule.MinCompRate Then dBasicRate = objBenefitRule.MinCompRate
                'UPGRADE_NOTE: Object objRule2 may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                objRule2 = Nothing
            End If

            'jtodd22 08/23/2006 always force rate to 2 decimals for use by lCalculatePayment
            'jtodd22 08/23/2006 otherwise you can have a payment low/high by up to .005 cents per week
            dGetBasicRate = modFunctions.RoundStandard(dBasicRate, 2)
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            If bRuleIsLocal Then objBenefitRule = Nothing

        End Try

    End Function

    Private Function lLoadData(ByVal dEmployeeHourlyRate As Double) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim lReturn As Integer

        Try

            lLoadData = g_objXErrorMask.cSuccess
            ClearObject()

            lReturn = modFunctions.GetBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_sBenefitTypeAbbr, objBenefitRule, sDLLClassNameJRRule, False)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function

            lReturn = modFunctions.GetCalcBenefitRule(m_ErrorMask, m_ErrorMaskSAWW, m_ErrorMaskCalcSetup, objCalcBenefitRule)
            If Not (lReturn = g_objXErrorMask.cSuccess And m_ErrorMask = g_objXErrorMask.cSuccess And m_ErrorMaskSAWW = g_objXErrorMask.cSuccess And m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess) Then Exit Function


            'UPGRADE_WARNING: Couldn't resolve default property of object objCalcBenefitRule.TTDPayPeriodCode. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            m_PayPeriodName = modFunctions.GetCodeDesc_SQL(objCalcBenefitRule.TTDPayPeriodCode)

        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            lLoadData = g_lErrNum
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally

        End Try

    End Function

    Public Property BenefitEndDate() As String
        Get
            BenefitEndDate = m_BenefitEndDate
        End Get
        Set(ByVal Value As String)
            m_BenefitEndDate = Value
        End Set
    End Property

    Public Property BenefitStartDate() As String
        Get
            BenefitStartDate = m_BenefitStartDate
        End Get
        Set(ByVal Value As String)
            m_BenefitStartDate = Value
        End Set
    End Property

    Public Property NumberOfPayments() As Short
        Get
            NumberOfPayments = m_NumberofPayments
        End Get
        Set(ByVal Value As Short)
            m_NumberofPayments = Value
        End Set
    End Property

    Public Property OffsetPerWeek() As Double
        Get
            OffsetPerWeek = m_OffsetPerWeek
        End Get
        Set(ByVal Value As Double)
            m_OffsetPerWeek = Value
        End Set
    End Property


    Public ReadOnly Property WeeksIntitled() As Double
        Get
            Dim lReturn As Integer
            WeeksIntitled = 0
            If objBenefitRule Is Nothing Then
                lReturn = lLoadData(g_objXClaim.ClaimantHourlyPayRate)
                If lReturn = g_objXErrorMask.cSuccess Then
                    'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeeksPerPercentagePoint. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    WeeksIntitled = modFunctions.RoundStandard(objBenefitRule.WeeksPerPercentagePoint * g_objXClaim.ClaimantImpairment, 4)
                End If
            Else
                'UPGRADE_WARNING: Couldn't resolve default property of object objBenefitRule.WeeksPerPercentagePoint. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                WeeksIntitled = modFunctions.RoundStandard(objBenefitRule.WeeksPerPercentagePoint * g_objXClaim.ClaimantImpairment, 4)
            End If
        End Get
    End Property

    Public ReadOnly Property WeeksPaidOrPending() As Double
        Get
            Dim sAbbr As String
            Dim sBeneIDs As String
            sAbbr = UCase("IIBS")
            sBeneIDs = modFunctions.GetBenefitLookUpIDs(sAbbr, "")
            If Trim(sBeneIDs & "") <> "" Then
                WeeksPaidOrPending = GetBenefitWeeksPaidOrPending(sBeneIDs)
            Else
                WeeksPaidOrPending = -99999
            End If
        End Get
    End Property

    Public Property DisabilityRate_Original() As Double
        Get
            DisabilityRate_Original = m_DisabilityRate_Original
        End Get
        Set(ByVal Value As Double)
            m_DisabilityRate_Original = Value
        End Set
    End Property


    Public Property BeginPrntDate_Original() As String
        Get
            BeginPrntDate_Original = m_BeginPrntDate_Original
        End Get
        Set(ByVal Value As String)
            m_BeginPrntDate_Original = Value
        End Set
    End Property
    Public ReadOnly Property Standard() As _ICalculator
        Get
            Standard = Me
        End Get
    End Property
    Public Property CalculatedPaymentAuto() As Double Implements _ICalculator.CalculatedPaymentAuto
        Get
            CalculatedPaymentAuto = m_CalculatedPaymentAuto
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentAuto = Value
        End Set
    End Property

    Public Property CalculatedPaymentCatchUp() As Double Implements _ICalculator.CalculatedPaymentCatchUp
        Get
            CalculatedPaymentCatchUp = m_CalculatedPaymentCatchUp
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentCatchUp = Value
        End Set
    End Property

    Public Property CalculatedPaymentLateCharge() As Double Implements _ICalculator.CalculatedPaymentLateCharge
        Get
            CalculatedPaymentLateCharge = m_CalculatedPaymentLateCharge
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentLateCharge = Value
        End Set
    End Property


    Public Property CalculatedPaymentPenalty() As Double Implements _ICalculator.CalculatedPaymentPenalty
        Get
            CalculatedPaymentPenalty = m_CalculatedPaymentPenalty
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentPenalty = Value
        End Set
    End Property

    Public Property CalculatedPaymentRegular() As Double Implements _ICalculator.CalculatedPaymentRegular
        Get
            CalculatedPaymentRegular = m_CalculatedPaymentRegular
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentRegular = Value
        End Set
    End Property

    Public Property CalculatedPaymentWaitingPeriod() As Double Implements _ICalculator.CalculatedPaymentWaitingPeriod
        Get
            CalculatedPaymentWaitingPeriod = m_CalculatedPaymentWaitingPeriod
        End Get
        Set(ByVal Value As Double)
            m_CalculatedPaymentWaitingPeriod = Value
        End Set
    End Property


    Public Property CheatSheetTitle() As String Implements _ICalculator.CheatSheetTitle
        Get
            CheatSheetTitle = m_CheatSheetTitle
        End Get
        Set(ByVal Value As String)
            m_CheatSheetTitle = Value
        End Set
    End Property


    Public Property EarningsRequiredCode() As Integer Implements _ICalculator.EarningsRequiredCode
        Get
            EarningsRequiredCode = m_EarningsRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_EarningsRequiredCode = Value
        End Set
    End Property


    Public Property ErrorMask() As Integer Implements _ICalculator.ErrorMask
        Get
            ErrorMask = m_ErrorMask
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMask = Value
        End Set
    End Property


    Public Property ErrorMaskFedTax() As Integer Implements _ICalculator.ErrorMaskFedTax
        Get
            ErrorMaskFedTax = m_ErrorMaskFedTax
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskFedTax = Value
        End Set
    End Property

    Public Property ErrorMaskCalcSetup() As Integer Implements _ICalculator.ErrorMaskCalcSetup
        Get
            ErrorMaskCalcSetup = m_ErrorMaskCalcSetup
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskCalcSetup = Value
        End Set
    End Property


    Public Property ErrorMaskSAWW() As Integer Implements _ICalculator.ErrorMaskSAWW
        Get
            ErrorMaskSAWW = m_ErrorMaskSAWW
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSAWW = Value
        End Set
    End Property


    Public Property ErrorMaskSpendData() As Integer Implements _ICalculator.ErrorMaskSpendData
        Get
            ErrorMaskSpendData = m_ErrorMaskSpendData
        End Get
        Set(ByVal Value As Integer)
            m_ErrorMaskSpendData = Value
        End Set
    End Property


    Public Property Note() As String Implements _ICalculator.Note
        Get
            Note = m_Note
        End Get
        Set(ByVal Value As String)
            m_Note = Value
        End Set
    End Property


    Public Property PaidPendingMonths() As Double Implements _ICalculator.PaidPendingMonths
        Get
            PaidPendingMonths = m_PaidPendingMonths
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingMonths = Value
        End Set
    End Property


    Public Property PaidPendingWeeks() As Double Implements _ICalculator.PaidPendingWeeks
        Get
            PaidPendingWeeks = m_PaidPendingWeeks
        End Get
        Set(ByVal Value As Double)
            m_PaidPendingWeeks = Value
        End Set
    End Property


    Public Property PassedInPayPeriodText() As String Implements _ICalculator.PassedInPayPeriodText
        Get
            PassedInPayPeriodText = m_PassedInPayPeriodText
        End Get
        Set(ByVal Value As String)
            m_PassedInPayPeriodText = Value
        End Set
    End Property


    Public Property PayPeriodName() As String Implements _ICalculator.PayPeriodName
        Get
            PayPeriodName = m_PayPeriodName
        End Get
        Set(ByVal Value As String)
            m_PayPeriodName = Value
        End Set
    End Property


    Public Property RuleTotalMonths() As Double Implements _ICalculator.RuleTotalMonths
        Get
            RuleTotalMonths = m_RuleTotalMonths
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalMonths = Value
        End Set
    End Property


    Public Property RuleTotalWeeks() As Double Implements _ICalculator.RuleTotalWeeks
        Get
            RuleTotalWeeks = m_RuleTotalWeeks
        End Get
        Set(ByVal Value As Double)
            m_RuleTotalWeeks = Value
        End Set
    End Property


    Public Property UseBodyMembersCode() As Integer Implements _ICalculator.UseBodyMembersCode
        Get
            UseBodyMembersCode = m_UseBodyMembersCode
        End Get
        Set(ByVal Value As Integer)
            m_UseBodyMembersCode = Value
        End Set
    End Property


    Public Property Warning() As String Implements _ICalculator.Warning
        Get
            Warning = m_Warning
        End Get
        Set(ByVal Value As String)
            m_Warning = Value
        End Set
    End Property


    Public Property MMIDateRequiredCode() As Integer Implements _ICalculator.MMIDateRequiredCode
        Get
            MMIDateRequiredCode = m_MMIDateRequiredCode
        End Get
        Set(ByVal Value As Integer)
            m_MMIDateRequiredCode = Value
        End Set
    End Property

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        objUserInputsOriginal = New CWCXUserInputs
        objUserInputsWorking = New CWCXUserInputs



    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object objBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objBenefitRule = Nothing
        'UPGRADE_NOTE: Object objCalcBenefitRule may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objCalcBenefitRule = Nothing
        'UPGRADE_NOTE: Object objWCPClaim may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objWCPClaim = Nothing
        'UPGRADE_NOTE: Object objUserInputsOriginal may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsOriginal = Nothing
        'UPGRADE_NOTE: Object objUserInputsWorking may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        objUserInputsWorking = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()


    End Sub

    Private Function ClearObject() As Integer
        m_CheatSheetTitle = "Impairment Income"
        m_EarningsRequiredCode = g_lNoCodeID
        m_ErrorMask = g_objXErrorMask.cSuccess
        m_ErrorMaskCalcSetup = g_objXErrorMask.cSuccess
        m_ErrorMaskFedTax = g_objXErrorMask.cSuccess
        m_ErrorMaskSAWW = g_objXErrorMask.cSuccess
        m_ErrorMaskSpendData = g_objXErrorMask.cSuccess
        m_MMIDateRequiredCode = g_lNoCodeID
        m_Note = ""
        m_PaidPendingMonths = 0
        m_PaidPendingWeeks = 0
        m_PayPeriodName = ""
        m_RuleTotalMonths = 0
        m_RuleTotalWeeks = 0
        m_UseBodyMembersCode = g_lNoCodeID
        m_Warning = ""

        m_CalculatedPaymentAuto = 0
        m_CalculatedPaymentCatchUp = 0
        m_CalculatedPaymentRegular = 0
        m_CalculatedPaymentWaitingPeriod = 0
        m_PaidPendingWeeks = 0


    End Function
    Private Function GetBasicRate(ByVal AverageWage As Double, ByRef TempTotalRate As Double, ByRef lTaxExemptions As Integer, ByRef lTaxStatusCode As Integer, ByVal dRateBeforeRules As Double, ByRef dEmployeeHourlyRate As Double) As Double Implements _ICalculator.GetBasicRate
        Const sFunctionName As String = "dGetBasicRate"

        GetBasicRate = dGetBasicRate(AverageWage, TempTotalRate, lTaxExemptions, lTaxStatusCode, dRateBeforeRules, dEmployeeHourlyRate)



    End Function

    Private Function CalculatePayment(ByRef colWorkSheet As Collection) As Integer Implements _ICalculator.CalculatePayment
        Const sFunctionName As String = "lCalculatePayment"

        CalculatePayment = lCalculatePayment(colWorkSheet)


    End Function

    Private Function LoadData(ByVal dEmployeeHourlyRate As Double) As Integer Implements _ICalculator.LoadData
        Const sFunctionName As String = "lLoadData"

        LoadData = lLoadData(dEmployeeHourlyRate)



    End Function
End Class

