Option Strict Off
Option Explicit On
Imports Riskmaster.Db
Public Class CWCPayments
    Implements System.Collections.IEnumerable
    Const sClassName As String = "CWCPayments"

    'local variable to hold collection
    Private mCol As Collection
    'local variables
    Private m_Days As Integer
    Private m_Weeks As Integer
    Dim m_TransTypeCodes As String
    Private Function ConvertDTGDate(ByRef DTGDate As String) As String
        ConvertDTGDate = ""
        If Len(DTGDate) < 8 Then Exit Function
        ConvertDTGDate = Mid(DTGDate, 5, 2) & "/" & Mid(DTGDate, 7, 2) & "/" & Mid(DTGDate, 1, 4)


    End Function
    Private Function PaidOrSinglePayments() As String
        Dim sSQL As String
        PaidOrSinglePayments = ""
        sSQL = ""
        sSQL = sSQL & "SELECT DISTINCT"
        sSQL = sSQL & " FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE"
        sSQL = sSQL & " FROM FUNDS, FUNDS_TRANS_SPLIT"
        sSQL = sSQL & " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID"
        sSQL = sSQL & " AND FUNDS.CLAIM_ID = " & g_objXClaim.ClaimID
        sSQL = sSQL & " AND FUNDS.CLAIMANT_EID = " & g_objXClaim.ClaimantEID
        sSQL = sSQL & " AND FUNDS.VOID_FLAG = 0"
        sSQL = sSQL & " AND FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE IN (" & m_TransTypeCodes & ")"
        sSQL = sSQL & " AND FUNDS_TRANS_SPLIT.FROM_DATE > ' ' AND FUNDS_TRANS_SPLIT.TO_DATE > ' '"
        sSQL = sSQL & " ORDER BY FUNDS_TRANS_SPLIT.FROM_DATE ASC"
        PaidOrSinglePayments = sSQL


    End Function
    Private Function AutoPayments() As String
        Dim sSQL As String
        AutoPayments = ""
        sSQL = ""
        sSQL = sSQL & "SELECT DISTINCT"
        sSQL = sSQL & " FUNDS_AUTO_SPLIT.FROM_DATE, FUNDS_AUTO_SPLIT.TO_DATE"
        sSQL = sSQL & " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT"
        sSQL = sSQL & " WHERE FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID"
        sSQL = sSQL & " AND FUNDS_AUTO.CLAIM_ID = " & g_objXClaim.ClaimID
        sSQL = sSQL & " AND FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE IN (" & m_TransTypeCodes & ")"
        sSQL = sSQL & " AND FUNDS_AUTO_SPLIT.FROM_DATE > ' ' AND FUNDS_AUTO_SPLIT.TO_DATE > ' '"
        sSQL = sSQL & " ORDER BY FUNDS_AUTO_SPLIT.FROM_DATE ASC"
        AutoPayments = sSQL


    End Function
    Public Function LoadData(ByRef sTransTypeCodes As String) As Integer
        Const sFunctionName As String = "lLoadData"
        Dim objReader As DbReader
        Dim lCount As Integer
        Dim lIndex As Integer
        Dim lSubTotal As Integer
        Dim sSQL As String
        Try

            LoadData = g_objXErrorMask.cSuccess
            m_Days = 0
            m_TransTypeCodes = sTransTypeCodes
            m_Weeks = 0

            sSQL = PaidOrSinglePayments()
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                Add(ConvertDTGDate(objReader.GetString("FROM_DATE")), ConvertDTGDate(objReader.GetString("TO_DATE")))
            End While

            lCount = mCol.Count()

            sSQL = AutoPayments()
            objReader = DbFactory.GetDbReader(g_ConnectionString, sSQL)
            While objReader.Read()
                Add(ConvertDTGDate(objReader.GetString("FROM_DATE")), ConvertDTGDate(objReader.GetString("TO_DATE")))
            End While

            lCount = mCol.Count()

            With mCol
                If .Count() > 0 Then
                    Debug.Print("**" & .Count())
                    For lIndex = 1 To .Count()
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item(lIndex).Days. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item(lIndex).ToDateCalder. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().FromDateCalder. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: DateDiff behavior may be different. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B38EC3F-686D-4B2E-B5A5-9E8E7A762E32"'
                        .Item(lIndex).Days = DateDiff(Microsoft.VisualBasic.DateInterval.Day, .Item(lIndex).FromDateCalder, .Item(lIndex).ToDateCalder) + 1
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item().Days. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        lSubTotal = lSubTotal + .Item(lIndex).Days
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item(lIndex).Days. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item(lIndex).ToDateCalder. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'UPGRADE_WARNING: Couldn't resolve default property of object mCol.Item(lIndex).FromDateCalder. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        'Debug.Print(TabLayout(.Item(lIndex).FromDateCalder, .Item(lIndex).ToDateCalder, .Item(lIndex).Days))
                    Next
                    m_Days = lSubTotal Mod 7
                    m_Weeks = Int(lSubTotal / 7)
                End If
            End With


        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            LoadData = Err.Number

            g_sErrProcedure = g_sErrSrc & "|" & sClassName & "." & sFunctionName & "|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
            SafeCloseRecordset(objReader)
        End Try

    End Function

    Public Function Add(ByRef FromDate As String, ByRef ToDate As String) As CWCPayment
        Dim lCount As Integer
        Try
            'create a new object
            Dim objNewMember As CWCPayment
            objNewMember = New CWCPayment

            'set the properties passed into the method
            objNewMember.FromDateCalder = FromDate
            objNewMember.ToDateCalder = ToDate

            'return the object created
            mCol.Add(objNewMember)
            Add = objNewMember
            lCount = Count
            'UPGRADE_NOTE: Object objNewMember may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            objNewMember = Nothing
        Catch ex As Exception
            With Err()
                g_lErrNum = Err.Number
                g_sErrSrc = .Source
                g_sErrDescription = Err.Description
            End With
            g_sErrProcedure = g_sErrSrc & "|" & sClassName & ".Add|"
            g_lErrLine = Erl()
            LogError(g_sErrProcedure, g_lErrLine, g_lErrNum, g_sErrSrc, g_sErrDescription)
            Err.Raise(g_lErrNum, g_sErrProcedure, g_sErrDescription)

        Finally
        End Try

    End Function

    Public ReadOnly Property Item(ByVal vntIndexKey As Object) As CWCPayment
        Get
            Item = mCol.Item(vntIndexKey)
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Count = mCol.Count()
        End Get
    End Property

    'UPGRADE_NOTE: NewEnum property was commented out. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B3FC1610-34F3-43F5-86B7-16C984F0E88E"'
    'Public ReadOnly Property NewEnum() As stdole.IUnknown
    'Get
    'NewEnum = mCol._NewEnum
    'End Get
    'End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        'UPGRADE_TODO: Uncomment and change the following line to return the collection enumerator. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="95F9AAD0-1319-4921-95F0-B9D3C4FF7F1C"'
        GetEnumerator = mCol.GetEnumerator
    End Function

    Public Property Days() As Integer
        Get
            Days = m_Days
        End Get
        Set(ByVal Value As Integer)
            m_Days = Value
        End Set
    End Property

    Public Property Weeks() As Integer
        Get
            Weeks = m_Weeks
        End Get
        Set(ByVal Value As Integer)
            m_Weeks = Value
        End Set
    End Property

    Public Sub Remove(ByRef vntIndexKey As Object)
        mCol.Remove(vntIndexKey)


    End Sub

    'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Initialize_Renamed()
        mCol = New Collection


    End Sub
    Public Sub New()
        MyBase.New()
        Class_Initialize_Renamed()


    End Sub

    'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    Private Sub Class_Terminate_Renamed()
        'UPGRADE_NOTE: Object mCol may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
        mCol = Nothing


    End Sub
    Protected Overrides Sub Finalize()
        Class_Terminate_Renamed()
        MyBase.Finalize()
    End Sub
End Class

