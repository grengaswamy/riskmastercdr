﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
using System.Text;
using System.Collections;
using Riskmaster.DataModel;


namespace Riskmaster.Application.BankingInfoManager
{
    public class BankingInfoManager
    {

        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        private string m_sConnectionString = "";

        private const int FIXED_LENGTH_STRING = 10;

        private int m_iClientId = 0;

        public BankingInfoManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;//rkaur27
            UserLogin objUserLogin = new UserLogin(p_sUserName, p_sPassword, p_sDsnName, m_iClientId);//rkaur27
            m_sConnectionString = objUserLogin.objRiskmasterDatabase.ConnectionString;
        }

        public XmlDocument GetClaimStatusHistory(XmlDocument p_objXmlDoc)
        {
            XmlElement objElm = null;
            XmlElement objNewElm = null;
            XmlElement objRowNode = null;
          
      
            DbReader objReader = null;
            LocalCache objCache = null;
  
            BankingInfo objBankinfo = null;
            string sSQL = string.Empty;
            string sCodeID = string.Empty;
            string sBankingRowId = string.Empty;
           
            DataModelFactory m_objDataModelFactory = null;
            int iBankingRowId = 0;

            string sClaimant = string.Empty; 
     

            try
            {
                
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ChangedBy");
                objElm.InnerText = m_sUserName;

                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);//rkaur27
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
         
                if (p_objXmlDoc.SelectSingleNode("//BankingRowId") != null)
                {
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//BankingRowId");
                    sBankingRowId = objElm.InnerText;

                    iBankingRowId = Conversion.ConvertStrToInteger(sBankingRowId);
                }
  
                sSQL = "SELECT DISTINCT CODES.CODE_ID,  CODES.SHORT_CODE , CODE_DESC, CODES.RELATED_CODE_ID ";
                sSQL = sSQL + "FROM CODES, CODES_TEXT, GLOSSARY ";
                sSQL = sSQL + "WHERE GLOSSARY.SYSTEM_TABLE_NAME='EFT_BANKING_STATUS' AND "; // Ijha: MITS 26536: Changed the BANKING_STATUS to EFT_BANKING_STATUS
                sSQL = sSQL + "GLOSSARY.TABLE_ID = CODES.TABLE_ID AND ";
                sSQL = sSQL + "CODES.CODE_ID = CODES_TEXT.CODE_ID AND ";
                sSQL = sSQL + "(CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL) AND ";
                sSQL = sSQL + "CODES_TEXT.LANGUAGE_CODE = 1033";
           
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//CodeId");
                sCodeID = objElm.InnerText;
                                            objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//StatusType");
                                objNewElm = p_objXmlDoc.CreateElement("level");
                objNewElm.SetAttribute("id", "0");
                objNewElm.SetAttribute("name", string.Empty);
                objElm.AppendChild(objNewElm);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                while (objReader.Read())
                {
                    objNewElm = p_objXmlDoc.CreateElement("level");
                    objNewElm.SetAttribute("id", objReader.GetInt32("CODE_ID").ToString());
                    objNewElm.SetAttribute("name", objReader.GetString("SHORT_CODE") + " - " + objReader.GetString("CODE_DESC"));
                    if (objReader.GetInt32("CODE_ID").ToString() == sCodeID)
                        objNewElm.SetAttribute("selected", "1");
                    objElm.AppendChild(objNewElm);
                }
                objReader.Close();

                objCache = new LocalCache(m_sConnectionString, m_iClientId);


                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//BankingStatusHistory");

                objBankinfo = (DataModel.BankingInfo)m_objDataModelFactory.GetDataModelObject("BankingInfo", false);
                if (iBankingRowId > 0)
                {
                    objBankinfo.MoveTo(iBankingRowId);

                    SortedList objSortedList = new SortedList();
                    string sKeyValue = string.Empty;
                    foreach (BankingStatusHist cl in objBankinfo.BankingStatusHistList)
                    {
                        sKeyValue = cl.DateStatusChgd + FixedLengthIdString(cl.BankHistRowId.ToString());
                        objSortedList.Add(sKeyValue, cl);
                    }

                    BankingStatusHist objBankingStatusHist = null;
                    for (int i = objSortedList.Count - 1; i >= 0; i--)
                    {
                        objBankingStatusHist = objSortedList.GetByIndex(i) as BankingStatusHist;

                        objRowNode = objElm.OwnerDocument.CreateElement("Row");

                
                        objNewElm = objRowNode.OwnerDocument.CreateElement("Status");
                        objNewElm.InnerText = objCache.GetCodeDesc(objBankingStatusHist.StatusCode);
                        objRowNode.AppendChild(objNewElm);

                        objNewElm = objRowNode.OwnerDocument.CreateElement("Date");
                        objNewElm.InnerText = Conversion.GetDBDateFormat(objBankingStatusHist.DateStatusChgd, "d");
                        objRowNode.AppendChild(objNewElm);

                        objNewElm = objRowNode.OwnerDocument.CreateElement("ChangedBy");
                        objNewElm.InnerText = objBankingStatusHist.StatusChgdBy;
                        objRowNode.AppendChild(objNewElm);

               
                        objNewElm = objRowNode.OwnerDocument.CreateElement("Reason");
                        objNewElm.InnerText = objBankingStatusHist.Reason;
                        objRowNode.AppendChild(objNewElm);

                        objElm.AppendChild(objRowNode);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimHistory.GetBankingStatusHistory.Error", m_iClientId), p_objEx);//rkaur27
              
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                objElm = null;
                objNewElm = null;
                objRowNode = null;
                if (objCache != null)
                    objCache.Dispose();
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();

            }

            return p_objXmlDoc;
        }

        private string FixedLengthIdString(string p_sId)
        {
            string sReturn = string.Empty;

            for (int i = 0; i < (FIXED_LENGTH_STRING - p_sId.Length); i++)
                sReturn = sReturn + "0";

            sReturn = sReturn + p_sId;

            return sReturn;
        }

    }
}
