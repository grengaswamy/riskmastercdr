﻿///********************************************************************************
/// Amendment History
///********************************************************************************
/// Date Amended        *                Amendment                   *    Author
/// 06/10/2014        MITS 34260 - duplicate claim | policy tree node     ngupta36
///********************************************************************************
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Text;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.Models;
using Riskmaster.Application.FundManagement;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System.Xml.XPath;
using System.Web;


namespace Riskmaster.Application.MDINavigationTreeNodeProfile
{
    public class MDINavigationTreeNodeProfileManager
    {
        private string m_sConnectionString = string.Empty;
        private int m_iClientId = 0;

         private XmlDocument m_ChildScreenDoc = null;
         private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
         private int iLangCode = 0;//JIRA RMA-10751 ajohari2
        #region Constructor

        public MDINavigationTreeNodeProfileManager()
        {
            
        }

        public MDINavigationTreeNodeProfileManager(string p_sConnectionString,UserLogin p_UserLogin,int p_iClientId)
        {
            //smishra25:null check to avaid multiple loads
            m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
            //Deb : ML changes
            string sLangCode = string.Empty;
            if (p_UserLogin != null)
                sLangCode = p_UserLogin.objUser.NlsCode.ToString();
            if (string.IsNullOrEmpty(sLangCode))
                sLangCode = "1033";//For safe side
            int.TryParse(sLangCode, out iLangCode);//JIRA RMA-10751 ajohari2

            if (HttpRuntime.Cache["MDIChildScreen_" + sLangCode] != null)
            {
                m_ChildScreenDoc = HttpRuntime.Cache["MDIChildScreen_" + sLangCode] as XmlDocument;
            }
            //Deb : ML changes
            //smishra25:null check to avaid multiple loads
            if (m_ChildScreenDoc == null)
            {
                string appPath = string.Empty;
                m_ChildScreenDoc = new XmlDocument();
                //load childscreen.xml file 
                //m_ChildScreenDoc.Load(@"C:\Main RMX Source\R5First\R5 Source\UI\RiskmasterUI\MDItest\simulatingMDI\App_Data\childScreen.xml");
                //Deb : ML changes
                //appPath = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                //m_ChildScreenDoc.Load(appPath + "App_Data\\childScreen.xml");
                string str = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), "SELECT CHILDSCREEN_XML FROM MDI_MENU WHERE LANGUAGE_CODE=" + sLangCode));
                if(string.IsNullOrEmpty(str))
                    str = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId), "SELECT CHILDSCREEN_XML FROM MDI_MENU WHERE LANGUAGE_CODE=1033"));
                m_ChildScreenDoc.LoadXml(str);
                HttpRuntime.Cache.Insert("MDIChildScreen_"+sLangCode, m_ChildScreenDoc, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromHours(6),System.Web.Caching.CacheItemPriority.NotRemovable,null);
                //Deb : ML changes
            }
            
        }

        #endregion

        #region Public Methods

        // akaushik5 Changed for MITS 38011 Starts
        //public List<string[]> GetNode(int nodeId, string objectName, int viewId, int dsnId, int objSysUseClaimantName, LocalCache objCache)
        public List<string[]> GetNode(int nodeId, string objectName, int viewId, int dsnId, int objSysUseClaimantName, LocalCache objCache, SysSettings objSysSettings)
        // akaushik5 Changed for MITS 38011 Ends
        {
            XmlNode xCurrentObject = null;
            XmlNode xElement = null;
            DbReader objReader = null;
            bool currentIsOneToOne = false;
            bool currentHasChildren = false;
            string currentDbTableName = string.Empty;
            string currentDbIdField = string.Empty;
            string currentDbJointFields = string.Empty;
            string currentDbUserPromptField = string.Empty;
            string currentRecordTitle = string.Empty;
            string currentListTitle = string.Empty;
            string currentDbClaimLOBField = string.Empty;
            string currentPresentable = Boolean.TrueString;
            string[] nodeDetail = null;
            string[] splitFields = null;
            string userText = string.Empty;
            string elementName = string.Empty;
            string missingOrEmptyElements = string.Empty;
            int i = 0;
            //Added by Amitosh for mits 23476(05/11/2011)
            string sClaimantName = string.Empty;
            //JIRA RMA-10751 ajohari2 : Start
            string sAdjusterType = string.Empty;
            int iAdjusterTypeID = 0;
            //JIRA RMA-10751 ajohari2 : End

            //SysSettings sys = null;
            int iDisplayClaimantName = 0;
            List<string[]> arrListNodeDetail = null;
            int intPerInvolType = 0;//Neha Added for CC
            try
            {
                xCurrentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + objectName);

                if (xCurrentObject != null)
                {
                    elementName = "dbTable";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentDbTableName = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    elementName = "idField";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentDbIdField = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    elementName = "recordTitle";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentRecordTitle = xElement.InnerText;
                        //currentRecordTitle = 
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    elementName = "listTitle";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentListTitle = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    elementName = "oneToOne";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        if (xElement.InnerText.ToUpper() == "YES")
                            currentIsOneToOne = true;
                        else
                            currentIsOneToOne = false;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    if (currentIsOneToOne)
                    {
                        elementName = "childscreen";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            if (xElement.SelectNodes("name").Count > 0)
                            {
                                currentHasChildren = true;
                            }
                            else
                            {
                                currentHasChildren = false;
                            }
                        }

                        elementName = "userPromptField";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null)
                        {
                            currentDbUserPromptField = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }
                    }
                    else
                    {
                        elementName = "userPromptField";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbUserPromptField = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }

                        elementName = "claimLOBField";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbClaimLOBField = xElement.InnerText;
                        }

                        elementName = "jointFields";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbJointFields = xElement.InnerText;
                        }
                    }
                }
                else
                {
                    missingOrEmptyElements += objectName;
                }

                if (!string.IsNullOrEmpty(missingOrEmptyElements))
                {
                    throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                }

                string userDescText = string.Empty;

                if (currentIsOneToOne)
                {
                    if (objectName == "RESERVEWORKSHEET")
                    {
                        //LocalCache objCache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue
                        objReader = GetDbQueryForNodeUserPrompt(currentDbIdField, currentDbUserPromptField, currentDbTableName, currentDbIdField, nodeId, currentDbJointFields, currentDbClaimLOBField, objectName, objCache);
                        if (objReader != null && objReader.Read())
                        {
                            userDescText = objCache.GetCodeDesc(Convert.ToInt32(objReader[1].ToString()));
                        }
                    }
                    /*if (currentHasChildren)
                    {*/
                        /*if (uPrompt == string.Empty)
                        {
                            uPrompt = "-1";
                        }*/
                        nodeDetail = new string[] { nodeId.ToString(), objectName, userDescText, currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                    //}
                    /*else
                    {
                        if (uPrompt == string.Empty)
                        {
                            uPrompt = "-2";
                        }
                        nodeDetail = new string[] { nodeId.ToString(), objectName, uPrompt, currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                    }*/
                }
                else
                {
                    objReader = GetDbQueryForNodeUserPrompt(currentDbIdField, currentDbUserPromptField, currentDbTableName, currentDbIdField, nodeId, currentDbJointFields, currentDbClaimLOBField, objectName, objCache);
                   
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            splitFields = currentDbUserPromptField.Replace(" ", "").Split(',');
                            userText = string.Empty;
                            for (i = 1; i <= splitFields.Length; i++)
                            {
                                /*if (i == 0)
                                {
                                    userText = CheckDate(objReader[i].ToString());
                                }
                                else
                                {*/
                                if (objReader[i] != null && objReader[i].ToString().Replace(" ", "") != string.Empty)
                                {
                                    if (i > 1 && objReader[i - 1] != null && objReader[i - 1].ToString().Replace(" ", "") != string.Empty)
                                    {
                                        userText += GetUserPromptDelimiter(splitFields[i - 1], splitFields[i - 2]);
                                    }
                                    //userText += CheckDate(objReader[i].ToString());
                                    userText += CheckDate(objReader[i].ToString(), splitFields[i - 1]);// MITS 26031
                                }
                                //}
                            }

                            if (currentDbClaimLOBField != string.Empty)
                            {
                                switch (objReader[i++].ToString())
                                {
                                    case "241":
                                        objectName = "claimgc";
                                        break;
                                    case "242":
                                        objectName = "claimva";
                                        break;
                                    case "243":
                                        objectName = "claimwc";
                                        break;
                                    case "844":
                                        objectName = "claimdi";
                                        break;
                                    //smahajan6 11/26/09 MITS:18230 :Start
                                    case "845":
                                        objectName = "claimpc";
                                        break;
                                    //smahajan6 11/26/09 MITS:18230 :End
                                }
                            }
                        }

                        xCurrentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + objectName);
                        if (xCurrentObject != null)
                        {
                            elementName = "recordTitle";
                            xElement = xCurrentObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty)
                            {
                                currentRecordTitle = xElement.InnerText;
                            }
                            else
                            {
                                missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                            }
                        }
                        else
                        {
                            missingOrEmptyElements += objectName;
                        }

                        if (!string.IsNullOrEmpty(missingOrEmptyElements))
                        {
                            throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                        }
                        //Neha R8 enahancement
                        if (string.Compare(objectName, "piother") == 0)
                        {
                            intPerInvolType = CheckRecordType(nodeId, objCache);
                            if (intPerInvolType == 1) //
                            {
                                currentRecordTitle = "PI Driver Other";
                            }
                            else if (intPerInvolType == 2)
                            {
                                currentRecordTitle = "PI Driver Insured";
                            }

                        }//Neha End
                        //Added by amitosh for mits 23476 (05/11/2011)
                         if (currentDbTableName == "CLAIM")
                        {
                            //sys = new SysSettings(m_sConnectionString);    //Mits id:32160 -Performance issue - Start                                                
                            //if ( sys.UseClaimantName == 0)
                            if (objSysUseClaimantName == 0) //Mits id:32160 -Performance issue - End
                            {

                                nodeDetail = new string[] { nodeId.ToString(), objectName, userText, currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                            }
                            else
                            {
                                sClaimantName = GetClaimantName(nodeId);
                                switch (currentRecordTitle)
                                {
                                    case "General Claim":
                                        currentRecordTitle = "GC";
                                        break;
                                    case "Property Claim":
                                        currentRecordTitle = "PC";
                                        break;
                                    case "Workers Comp.":
                                        currentRecordTitle = "WC";
                                        break;
                                    case "Non-occupational":
                                        currentRecordTitle = "Non-Occ";
                                        break;
                                     case "Vehicle Accident":
                                        currentRecordTitle = "VA";
                                        break;
                                }
                                   if(!string.IsNullOrEmpty(sClaimantName))
                                nodeDetail = new string[] { nodeId.ToString(), objectName, userText + " * " + sClaimantName + " ", currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                                   else
                                       nodeDetail = new string[] { nodeId.ToString(), objectName, userText , currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                            }
                        }
                         // akaushik5 Changed for MITS 38011 Starts
                         //else if (currentDbTableName.Contains("UNIT_X_CLAIM") || currentDbTableName.Contains("CLAIM_X_PROPERTYLOSS") || currentDbTableName.Contains("CLAIM_X_SITELOSS") || currentDbTableName.Contains("CLAIM_X_OTHERUNIT"))
                         else if (this.IsBOBEnabled(objSysSettings) && (currentDbTableName.Contains("UNIT_X_CLAIM") || currentDbTableName.Contains("CLAIM_X_PROPERTYLOSS") || currentDbTableName.Contains("CLAIM_X_SITELOSS") || currentDbTableName.Contains("CLAIM_X_OTHERUNIT")))
                         // akaushik5 Changed for MITS 38011 Ends
                         {
                             string sPrefixUnit = GetInsuredflag(nodeId, currentDbTableName);
                             nodeDetail = new string[] { nodeId.ToString(), objectName, sPrefixUnit + userText, currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                         }
                         //JIRA RMA-10751 ajohari2: Start
                         else if (currentDbTableName.Contains("CLAIM_ADJUSTER"))
                         {
                             iAdjusterTypeID = GetAdjusterType(nodeId);
                             sAdjusterType = objCache.GetCodeDesc(iAdjusterTypeID, iLangCode);
                             if (!sAdjusterType.Equals(string.Empty))
                             {
                                 userText += " * " + sAdjusterType + "";
                             }

                             nodeDetail = new string[] { nodeId.ToString(), objectName, userText, currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                         }
                         //JIRA RMA-10751 ajohari2: End
                         else
                         {
                             nodeDetail = new string[] { nodeId.ToString(), objectName, userText, currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                         }
                        //end Amitosh
                         //nodeDetail = new string[] { nodeId.ToString(), objectName, userText, currentRecordTitle, currentListTitle, currentPresentable, currentIsOneToOne.ToString(), currentHasChildren.ToString() };
                    }
                }
                if (nodeDetail != null)
                {
                    arrListNodeDetail = new List<string[]>();
                    arrListNodeDetail.Add(nodeDetail);
                }

                return arrListNodeDetail;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                }
                /*if (sys != null)
                {
                    sys = null;
                }*/
            }

        }

        //JIRA RMA-10751 ajohari2: Start
        private int GetAdjusterType(int conditionNodeId)
        {
            string sQuery = string.Empty;
            int iADJType = 0;

            try
            {
                sQuery = string.Format(@"SELECT CLAIM_ADJUSTER.ADJUSTER_TYPE FROM CLAIM_ADJUSTER WHERE CLAIM_ADJUSTER.ADJ_ROW_ID = {0}", conditionNodeId.ToString());
                iADJType = DbFactory.ExecuteAsType<int>(m_sConnectionString, sQuery);

                return iADJType;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }

        }
        //JIRA RMA-10751 ajohari2: End

        private bool IsBOBEnabled(SysSettings objSys)
        {
            bool bEnabled = false;
            //SysSettings sys = new SysSettings(m_sConnectionString); //Mits id:32160 -Performance issue
            if (objSys.MultiCovgPerClm < 0)
            {
                bEnabled = true;
            }
            else
            {
                bEnabled = false;
            }
            // objSys = null; //Mits id:32160 -Performance issue
            return bEnabled;
        }
        public List<string[]> GetNodeChildren(int currentObjectId, string currentObjectName, int parentId, string parentObjectName, int viewId, int dsnId, SysSettings objSys, LocalCache objCache)
        {
            List<string[]> arrListChildern = new List<string[]>();
            XmlNode xCurrentObject = null;
            XmlNode xElement = null;
            XmlNodeList childrenNodeList = null;

            int matchingId = currentObjectId;
            string matchingObjectName = currentObjectName;

            string currentDbTableName = string.Empty;
            string currentDbIdField = string.Empty;
            //string currentPowerViewName = string.Empty;
            string currentExposeChildren = string.Empty;
            string childExposeChildren = string.Empty;
            bool childHasChildren = false;
            bool childIsOneToOne = false;

            XmlNode xChildObject = null;
            DbReader objReader = null;
            ArrayList powerViewChildrenList = null;
            ArrayList filteredChildrenList = null;

            string childDbIdField = string.Empty;
            string childDbParentIdField = string.Empty;
            string childDbTableName = string.Empty;
            string childDbUserPromptField = string.Empty;
            string childDbClaimLOBField = string.Empty;
            string childDbJointFields = string.Empty;
            string childDbExtraField = string.Empty;
            string childObjectName = string.Empty;
            string childParentName = string.Empty;
            string childRecordTitle = string.Empty;
            string childListTitle = string.Empty;
            string childPresentable = Boolean.TrueString;
            string[] childDetail = null;
            string[] currentFilteredChildArr = null;
            string[] splitFields = null;
            string userText = string.Empty;

            string LOB = string.Empty;
            bool added = false;
            bool addTheChild = false;

            int eventDisplayId = 0;
            int eventIndCode = 0;
            int i = 0;

            string elementName = string.Empty;
            string missingOrEmptyElements = string.Empty;

            //SysSettings objSettings = null; //Mits id:32160 -Performance issue
            try
            {
                if (matchingId == 0)
                {
                    matchingId = parentId;
                    matchingObjectName = parentObjectName;
                }

                xCurrentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + currentObjectName);

                if (xCurrentObject != null)
                {
                    if (currentObjectName == "event")
                    {
                        objReader = GetDbSysEventCatInfo(currentObjectId);
                        if (objReader != null && objReader.Read())
                        {
                            eventDisplayId = objReader.GetInt(0);
                            eventIndCode = objReader.GetInt(1);
                        }
                    }

                    elementName = "dbTable";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentDbTableName = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    elementName = "idField";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentDbIdField = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }
                }
                else
                {
                    missingOrEmptyElements += currentObjectName;
                }

                if (!string.IsNullOrEmpty(missingOrEmptyElements))
                {
                    throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                }

                elementName = "childscreen";
                xElement = xCurrentObject.SelectSingleNode(elementName);
                if (xElement != null && xElement.Attributes.GetNamedItem("exposeChildren") != null)
                {
                    childrenNodeList = xElement.SelectNodes("name");
                    currentExposeChildren = xElement.Attributes.GetNamedItem("exposeChildren").Value;

                    //if (currentExposeChildren == "POWERVIEW") 
                    if (currentExposeChildren.Equals("POWERVIEW")) //Mits id:32160 -Performance issue   
                    {
                        powerViewChildrenList = GetPowerViewChildrenList(currentObjectName, viewId, dsnId, xCurrentObject, parentObjectName);
                        //filteredChildrenList = GetFilteredListForPowerView(powerViewChildrenList, childrenNodeList,currentObjectId,currentObjectName, objSys);                        
                        filteredChildrenList = GetFilteredListForPowerView(powerViewChildrenList, childrenNodeList, currentObjectId, currentObjectName, objSys, objCache);                        
                        
                    }
                    //else if (currentExposeChildren == "CONDITIONAL") //currentObjectName == "reservelisting" || currentObjectName == "reserveworksheet"||currentObjectName=="piother")
                    else if (currentExposeChildren.Equals("CONDITIONAL")) //Mits id:32160 -Performance issue
                    {
                        if (ExposeChildren(matchingId, matchingObjectName, currentObjectName, objCache))
                        {
                            filteredChildrenList = GetFilteredListForOther(childrenNodeList);
                        }
                    }
                    //else if (currentExposeChildren == "YES")
                    else if (currentExposeChildren.Equals("YES")) //Mits id:32160 -Performance issue
                    {
                        filteredChildrenList = GetFilteredListForOther(childrenNodeList);
                    }
                }

                if (filteredChildrenList == null) // In case the list is not instantiated because of no permitted child (another scenario is that there is no view xml file in DB to get permitted children!)
                {
                    filteredChildrenList = new ArrayList();
                }

                for (int count = 0; count < filteredChildrenList.Count; count++)
                {
                    //for each current child

                    currentFilteredChildArr = (string[])filteredChildrenList[count];

                    xChildObject = m_ChildScreenDoc.SelectSingleNode("screen/" + currentFilteredChildArr[0]);

                    if (xChildObject != null)
                    {
                        //populating required data from childscreen.xml file to string variables
                        childObjectName = currentFilteredChildArr[0];

                        elementName = "dbTable";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            childDbTableName = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                        }

                        elementName = "idField";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            childDbIdField = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                        }

                        elementName = "parent";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            childParentName = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                        }

                        elementName = "parentIdField";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            childDbParentIdField = MatchParentIdField(xElement.InnerText, childParentName, matchingObjectName, xChildObject.Name);
                        }
                        else
                        {
                            missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                        }

                        elementName = "recordTitle";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            //childRecordTitle = xElement.InnerText;
                            if (currentFilteredChildArr[1] != string.Empty)
                            {
                                childRecordTitle = currentFilteredChildArr[1];
                            }
                            else
                            {
                                childRecordTitle = xElement.InnerText;
                            }
                        }
                        else
                        {
                            missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                        }

                        elementName = "listTitle";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            //childListTitle = xElement.InnerText;
                            if (currentFilteredChildArr[1] != string.Empty)
                            {
                                childListTitle = currentFilteredChildArr[1];
                            }
                            else
                            {
                                childListTitle = xElement.InnerText;
                            }
                        }
                        else
                        {
                            missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                        }

                        elementName = "oneToOne";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            if (xElement.InnerText.ToUpper() == "YES")
                                childIsOneToOne = true;
                            else
                                childIsOneToOne = false;
                        }
                        else
                        {
                            missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                        }

                        childHasChildren = false;
                        if (childIsOneToOne)
                        {
                            childDbUserPromptField = string.Empty;// for one to one navTree nodes we only show the node title!

                            elementName = "childscreen";
                            xElement = xChildObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty && xElement.Attributes.GetNamedItem("exposeChildren") != null)
                            {
                                childExposeChildren = xElement.Attributes.GetNamedItem("exposeChildren").Value;

                                if (xElement.SelectNodes("name").Count > 0)
                                {
                                    if (childExposeChildren == "YES" || childExposeChildren == "POWERVIEW")
                                    {
                                        childHasChildren = true;
                                    }
                                    else //CONDITIONAL
                                    {
                                        childHasChildren = ExposeChildren(matchingId, matchingObjectName, childObjectName, objCache);
                                    }
                                }
                                else
                                {
                                    childHasChildren = false;
                                }
                            }
                        }
                        //else
                        //{
                            elementName = "userPromptField";
                            xElement = xChildObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty)
                            {
                                childDbUserPromptField = xElement.InnerText;
                            }
                            //else
                            //{
                            //    missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                            //}

                            elementName = "jointFields";
                            xElement = xChildObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty)
                            {
                                childDbJointFields = xElement.InnerText;
                            }
                        //}
                    }
                    else
                    {
                        missingOrEmptyElements += currentObjectName;
                    }

                    if (!string.IsNullOrEmpty(missingOrEmptyElements))
                    {
                        throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                    }

                    if (childObjectName == "claim")  //checking if current child is claim
                    {
                        elementName = "claimLOBField";
                        xElement = xChildObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            childDbClaimLOBField = xElement.InnerText;
                        }
                        objReader = GetDbQueryForNodeUserPrompt(childDbIdField, childDbUserPromptField, childDbTableName, childDbParentIdField, currentObjectId, childDbJointFields, childDbClaimLOBField, childObjectName, objCache); 
                        //objReader = GetDbQueryForChildren(currentDbIdField, currentObjectId, childDbTableName, childDbIdField, childDbUserPromptField, childDbClaimLOBField);

                        if (objReader != null)
                        {
                            //arrListChildern = new List<string[]>();
                            while (objReader.Read())
                            {
                                splitFields = childDbUserPromptField.Replace(" ", "").Split(',');
                                userText = string.Empty;
                                for (i = 1; i <= splitFields.Length; i++)
                                {
                                    /*if (i == 0)
                                    {
                                        userText = CheckDate(objReader[i].ToString());
                                    }
                                    else
                                    {*/
                                    if (objReader[i] != null && objReader[i].ToString().Replace(" ", "") != string.Empty)
                                    {
                                        if (i > 1 && objReader[i - 1] != null && objReader[i - 1].ToString().Replace(" ", "") != string.Empty)
                                        {
                                            userText += GetUserPromptDelimiter(splitFields[i - 1], splitFields[i - 2]);
                                        }
                                        //userText += CheckDate(objReader[i].ToString());
                                        userText += CheckDate(objReader[i].ToString(), splitFields[i - 1]);//MITS 26031
                                    }
                                    //}
                                }

                                if (childDbClaimLOBField != string.Empty)
                                {
                                    switch (objReader[i++].ToString())
                                    {
                                        case "241":
                                            childObjectName = "claimgc";
                                            break;
                                        case "242":
                                            childObjectName = "claimva";
                                            break;
                                        case "243":
                                            childObjectName = "claimwc";
                                            break;
                                        case "844":
                                            childObjectName = "claimdi";
                                            break;
                                        //smahajan6 11/26/09 MITS:18230 :Start
                                        case "845":
                                            childObjectName = "claimpc";
                                            break;
                                        //smahajan6 11/26/09 MITS:18230 :End
                                    }
                                }

                                xCurrentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + childObjectName);

                                if (xCurrentObject != null)
                                {
                                    elementName = "recordTitle";
                                    xElement = xCurrentObject.SelectSingleNode(elementName);
                                    if (xElement != null && xElement.InnerText != string.Empty)
                                    {
                                        //since claim always displays in left tree along with its LOB name so its display
                                        //text i,e title does not depends on title present in its parents powerview xml file
                                        //there fore here its record title should be picked from childsreeen.xml 
                                        //therefore commented by Nitin for Mits 15423 on 28-Apr-2009
                                        //childRecordTitle =  currentChildPowerViewArr[1];
                                        //added by Nitin for Mits 15423 on 28-Apr-2009

                                        childRecordTitle = xElement.InnerText;
                                    }
                                    else
                                    {
                                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                                    }
                                }
                                else
                                {
                                    missingOrEmptyElements += currentObjectName;
                                }
                                if (!string.IsNullOrEmpty(missingOrEmptyElements))
                                {
                                    throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                                }
                                if (objSys.UseClaimantName != 0)
                                {
                                    //Add by ksirigudi for mits:32383 06/24/2013 Start
                                    string sClaimantName = GetClaimantName(Conversion.ConvertObjToInt(objReader[0], m_iClientId));
                                    if (!string.IsNullOrEmpty(sClaimantName))
                                    {
                                        childDetail = new string[] { objReader[0].ToString(), childObjectName, userText + " * " + sClaimantName, childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                    }
                                    else
                                    {
                                        childDetail = new string[] { objReader[0].ToString(), childObjectName, userText, childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                    }

                                }
                                else
                                {
                                    childDetail = new string[] { objReader[0].ToString(), childObjectName, userText, childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                }
                                
                            arrListChildern.Add(childDetail);
                            }
                        }
                    }
                    else if (childIsOneToOne == true) //if current child is onetoone node ,lke OSHA or casemanagement node
                    {
                        //in case of onetoone node , childdbextrafield = "" and userprompt will be -1 or 0 depend on following condition
                        childDbExtraField = "";

                        if (childObjectName.ToLower() == "reservelisting")
                        {
                            if (parentObjectName == "event")
                            {
                                LOB = currentObjectName;
                            }
                            else
                            {
                                LOB = parentObjectName;
                            }
                            objReader = GetDbQueryForDetailTracking(LOB);

                            if (objReader != null)
                            {
                                addTheChild = false;
                                if (objReader.Read())
                                {
                                    switch (objReader[0].ToString())
                                    {
                                        case "0":
                                        case "": //rsushilaggar RMA-11781
                                            {
                                                switch (currentObjectName)
                                                {
                                                    case "claimgc":
                                                    case "claimva":
                                                    case "claimwc":
                                                    case "claimdi":
                                                    //smahajan6 11/26/09 MITS:18230 :Start
                                                    case "claimpc":
                                                    //smahajan6 11/26/09 MITS:18230 :End
                                                        addTheChild = true;
                                                        break;
                                                }
                                            }
                                            break;
                                        case "1":
                                            {
                                                switch (currentObjectName)
                                                {
                                                    case "claimant":
                                                    case "unit":
                                                        {
                                                            //MGaba2: MITS 21763:Detail level and Claim & Detail Level settings are not implemented for Propertry claim
                                                            //if (LOB == "claimgc" || LOB == "claimva")
                                                            if (LOB == "claimgc" || LOB == "claimva" || LOB == "claimpc")
                                                            {
                                                                addTheChild = true;
                                                            }
                                                        }
                                                        break;
                                                    case "claimgc":
                                                    case "claimva":
                                                    case "claimwc":
                                                    case "claimdi":
                                                    //smahajan6 11/26/09 MITS:18230 :Start
                                                    case "claimpc":
                                                    //smahajan6 11/26/09 MITS:18230 :End
                                                        {
                                                            addTheChild = true;
                                                            childHasChildren = false;
                                                        }
                                                        break;
                                                }
                                            }
                                            break;
                                        case "2":
                                            {
                                                switch (currentObjectName)
                                                {
                                                    case "claimant":
                                                    case "unit":
                                                        {
                                                            //MGaba2: MITS 21763:Detail level and Claim & Detail Level settings are not implemented for Propertry claim
                                                            //if (LOB == "claimgc" || LOB == "claimva")
                                                            if (LOB == "claimgc" || LOB == "claimva" || LOB == "claimpc")
                                                            {
                                                                addTheChild = true;
                                                            }
                                                        }
                                                        break;
                                                    case "claimgc":
                                                    case "claimva":
                                                    case "claimwc":
                                                    case "claimdi":
                                                    //smahajan6 11/26/09 MITS:18230 :Start
                                                    case "claimpc":
                                                    //smahajan6 11/26/09 MITS:18230 :End
                                                        addTheChild = true;
                                                        break;
                                                }
                                            }
                                            break;
                                    }
                                    if (addTheChild)
                                    {
                                        //if (childHasChildren == true)
                                        //{
                                        bool bIsBOB = false; 
                                            if (!currentObjectName.Equals("claimdi"))
                                            {
                                                bIsBOB=IsBOBEnabled(objSys);
                                            }
                                            childDetail = new string[] { "0", childObjectName, string.Empty, childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString(), bIsBOB.ToString()};
                                        //}
                                        //else
                                        //{
                                        //    childDetail = new string[] { "0", childObjectName, "-2", childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                        //}
                                        arrListChildern.Add(childDetail);
                                    }
                                }
                            }
                        }
                        else
                        {
                            switch (childObjectName)
                            {
                                case "fallinfo":
                                    {
                                        switch ((Constants.EVENT_CATEGORY_DISPLAY_ID)eventDisplayId)
                                        {
                                            case Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment:
                                            case Constants.EVENT_CATEGORY_DISPLAY_ID.Medication: //Medication or Equipment
                                                childPresentable = Boolean.FalseString;
                                                break;
                                            case Constants.EVENT_CATEGORY_DISPLAY_ID.Fall:
                                                childPresentable = Boolean.TrueString;
                                                break;
                                            default:
                                                childPresentable = Boolean.FalseString;
                                                break;
                                        }
                                    }
                                    break;
                                case "medwatch":
                                    {
                                        switch ((Constants.EVENT_CATEGORY_DISPLAY_ID)eventDisplayId)
                                        {
                                            case Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment:
                                            case Constants.EVENT_CATEGORY_DISPLAY_ID.Medication: //Medication or Equipment
                                                childPresentable = Boolean.TrueString;
                                                break;
                                            case Constants.EVENT_CATEGORY_DISPLAY_ID.Fall:
                                                childPresentable = Boolean.FalseString;
                                                break;
                                            default:
                                                childPresentable = Boolean.FalseString;
                                                break;
                                        }
                                    }
                                    break;
                                case "sentinel":
                                    {
                                        switch (eventIndCode)
                                        {
                                            case 0:
                                                childPresentable = Boolean.FalseString;
                                                break;
                                            default:
                                                childPresentable = Boolean.TrueString;
                                                break;
                                        }
                                    }
                                    break;
                            }
                            //if (currentObjectName != "reservelisting")
                            //{
                            string userDescText = string.Empty;
                            //if (currentObjectName == "reservelisting")
                            //{
                                objReader = GetDbQueryForChildren(childDbParentIdField, matchingId, childDbTableName, childDbIdField, childDbUserPromptField, childDbExtraField, currentObjectName, objCache );
                            //}
                            //else
                            //{
                            //    objReader = GetDbQueryForChildren(currentDbIdField, currentObjectId, childDbTableName, childDbIdField, childDbUserPromptField, childDbExtraField, currentObjectName);
                            //}

                                if (objReader != null)
                                {
                                    //arrListChildern = new List<string[]>();
                                    added = false;
                                    while (added == false && objReader.Read())
                                    {
                                        if (currentObjectName == "reservelisting")
                                        {
                                            //LocalCache objCache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue 
                                            userDescText = objCache.GetCodeDesc(Convert.ToInt32(objReader[1].ToString()));
                                        }
                                        /*if (childHasChildren == true)
                                        {*/
                                            /*if (userDescText == string.Empty)
                                            {
                                                userDescText = "-1";
                                            }*/
                                            childDetail = new string[] { objReader[0].ToString(), childObjectName, userDescText, childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                        //}
                                        /*else
                                        {
                                            if (uPrompt == string.Empty)
                                            {
                                                uPrompt = "-2";
                                            }
                                            childDetail = new string[] { objReader[0].ToString(), childObjectName, uPrompt, childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                        }*/
                                        arrListChildern.Add(childDetail);
                                        added = true;
                                    }
                                    if (added == false && currentObjectName != "reservelisting")
                                    {
                                        //we show the node since it is one to one but since the record has not been created yet we make sure the node is not expandable because there are no children at this time (childHasChildren = false).
                                        childDetail = new string[] { "-1", childObjectName, userDescText, childRecordTitle, string.Empty, childPresentable, childIsOneToOne.ToString(), Boolean.FalseString };
                                        arrListChildern.Add(childDetail);
                                    }
                                }
                            //}
                            /*else
                            {
                                if (childHasChildren == true)
                                {
                                    childDetail = new string[] { "0", childObjectName, "-1", "Reserve Work Sheet", string.Empty, childPresentable };
                                }
                                else
                                {
                                    childDetail = new string[] { "0", childObjectName, "-2", "Reserve Work Sheet", string.Empty, childPresentable };
                                }
                                arrListChildern.Add(childDetail);
                            }*/

                        }
                    }
                    else  //for all other cases like event and other cases
                    {
                        //fetch record count of rest of the cases
                        string sNumberOfRecords = "";
                        objReader = GetDbQueryForNumberOfRecords(matchingId, currentDbIdField, currentDbTableName, childDbIdField, childDbTableName, childDbParentIdField, childObjectName, currentObjectName, objCache );

                        if (objReader != null)
                        {
                            //arrListChildern = new List<string[]>();
                            added = false;
                            while (objReader.Read())
                            {
                                sNumberOfRecords = objReader[0].ToString();
                                childDetail = new string[] { "0", childObjectName, sNumberOfRecords, string.Empty, childListTitle, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                arrListChildern.Add(childDetail);
                                added = true;
                            }
                            if (added == false)
                            {
                                childDetail = new string[] { "0", childObjectName, "0", string.Empty, childListTitle, childPresentable, childIsOneToOne.ToString(), childHasChildren.ToString() };
                                arrListChildern.Add(childDetail);
                            }
                        }
                        //Bkuzhanthaim : MITS- 36027/RMA-342:auto expansion of all adjusters on load of claim.
                        if (childObjectName == "adjuster")
                        {
                            if (sNumberOfRecords != "0")
                            {
                                objReader = GetDbQueryRecords(matchingId, currentDbIdField, currentDbTableName, childDbIdField, childDbTableName, childDbParentIdField, childObjectName, currentObjectName, objCache);
                                {
                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            sNumberOfRecords = sNumberOfRecords+"/"+objReader[0].ToString();
                                        }
                                        if (objSys.AdjusterAutoExpanstioFlag)
                                        {
                                            sNumberOfRecords = sNumberOfRecords + "/" + "1";
                                        }
                                        else
                                        {
                                            sNumberOfRecords = sNumberOfRecords + "/" + "0";
                                        }
                                        arrListChildern[arrListChildern.Count - 1][2] = sNumberOfRecords;
                                    }
                                }
                            }
                        }

                        //ngupta73 : MITS- 34260/RMA-4362:auto expansion of all policies on load of claim.
                        if (childObjectName == "claimxpolicy")
                        {
                            if (sNumberOfRecords != "0")
                            {
                                objReader = GetDbQueryRecords(matchingId, currentDbIdField, currentDbTableName, childDbIdField, childDbTableName, childDbParentIdField, childObjectName, currentObjectName, objCache);
                                {
                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            sNumberOfRecords = sNumberOfRecords + "/" + objReader[0].ToString();
                                        }
                                        if (objSys.PolicyAutoExpansionFlag)
                                        {
                                            sNumberOfRecords = sNumberOfRecords + "/" + "1";
                                        }
                                        else
                                        {
                                            sNumberOfRecords = sNumberOfRecords + "/" + "0";
                                        }
                                        arrListChildern[arrListChildern.Count - 1][2] = sNumberOfRecords;
                                    }
                                }
                            }
                        }

                    }
                    //}

                    if (objReader != null)
                    {
                        if (objReader.IsClosed == false)
                        {
                            objReader.Close();
                        }
                    }
                }

                return arrListChildern;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    if (objReader.IsClosed == false)
                    {
                        objReader.Close();
                    }
                }
            }
        }

        public List<string[]> GetNodeParent(int nodeId, string objectName, int viewId, int dsnId)
        {
            XmlNode xCurrentObject = null;
            XmlNode xElement = null;
            XmlNode xParentObject = null;

            string elementName = string.Empty;
            string missingOrEmptyElements = string.Empty;

            string curretnObjectName = string.Empty;
            string currentDbTableName = string.Empty;
            string currentDbIdField = string.Empty;
            string currentDbParentIdField = string.Empty;
            string currentParentName = string.Empty;

            string[] splitFields = null;
            string[] parentNodeDetail = null;
            string userText = string.Empty;
            string parentObjectName = string.Empty;
            string parentDbTableName = string.Empty;
            string parentDbIdField = string.Empty;
            string parentDbUserPromptField = string.Empty;
            string parentRecordTitle = string.Empty;
            string parentDbClaimLOBField = string.Empty;
            string parentDbJointFields = string.Empty;
            string parentPresentable = Boolean.TrueString;
            string parentId = string.Empty;

            bool parentIsOneToOne = false;
            bool parentHasChildren = false;

            DbReader objReader = null;
            List<string[]> arrListParent = null;
            int i;

            try
            {
                xCurrentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + objectName);

                if (xCurrentObject != null)
                {

                    elementName = "dbTable";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentDbTableName = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }


                    elementName = "idField";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentDbIdField = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }


                    elementName = "parentIdField";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentDbParentIdField = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }


                    elementName = "parent";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        currentParentName = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    if (currentParentName != string.Empty)
                    {
                        elementName = currentParentName;
                        xParentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + elementName);
                        if (xParentObject != null)
                        {
                            elementName = "dbTable";
                            xElement = xParentObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty)
                            {
                                parentDbTableName = xElement.InnerText;
                            }
                            else
                            {
                                missingOrEmptyElements += xParentObject.Name + "." + elementName + " ";
                            }


                            elementName = "idField";
                            xElement = xParentObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty)
                            {
                                parentDbIdField = xElement.InnerText;
                            }
                            else
                            {
                                missingOrEmptyElements += xParentObject.Name + "." + elementName + " ";
                            }

                            elementName = "recordTitle";
                            xElement = xParentObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty)
                            {
                                parentRecordTitle = xElement.InnerText;
                            }
                            else
                            {
                                missingOrEmptyElements += xParentObject.Name + "." + elementName + " ";
                            }

                            elementName = "oneToOne";
                            xElement = xParentObject.SelectSingleNode(elementName);
                            if (xElement != null && xElement.InnerText != string.Empty)
                            {
                                if (xElement.InnerText.ToUpper() == "YES")
                                    parentIsOneToOne = true;
                                else
                                    parentIsOneToOne = false;
                            }
                            else
                            {
                                missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                            }

                            if (parentIsOneToOne)
                            {
                                elementName = "childscreen";
                                xElement = xParentObject.SelectSingleNode(elementName);
                                if (xElement != null && xElement.InnerText != string.Empty)
                                {
                                    if (xElement.SelectNodes("name").Count > 0)
                                    {
                                        parentHasChildren = true;
                                    }
                                    else
                                    {
                                        parentHasChildren = false;
                                    }
                                }
                            }
                            else
                            {
                                elementName = "userPromptField";
                                xElement = xParentObject.SelectSingleNode(elementName);
                                if (xElement != null && xElement.InnerText != string.Empty)
                                {
                                    parentDbUserPromptField = xElement.InnerText;
                                }
                                else
                                {
                                    missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                                }

                                elementName = "claimLOBField";
                                xElement = xParentObject.SelectSingleNode(elementName);
                                if (xElement != null && xElement.InnerText != string.Empty)
                                {
                                    parentDbClaimLOBField = xElement.InnerText;
                                }

                                elementName = "jointFields";
                                xElement = xParentObject.SelectSingleNode(elementName);
                                if (xElement != null && xElement.InnerText != string.Empty)
                                {
                                    parentDbJointFields = xElement.InnerText;
                                }
                            }
                        }
                        else
                        {
                            missingOrEmptyElements += elementName + "(parent for " + objectName + ") ";
                        }
                    }
                }
                else
                {
                    missingOrEmptyElements += objectName;
                }

                if (!string.IsNullOrEmpty(missingOrEmptyElements))
                {
                    throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                }

                objReader = GetDbQueryForNodeParent(parentDbIdField, parentDbUserPromptField, parentDbJointFields, parentDbTableName, currentDbTableName, currentDbIdField, currentDbParentIdField, nodeId, parentDbClaimLOBField);

                if (objReader.Read())
                {
                    i = 1;
                    if (!string.IsNullOrEmpty(parentDbUserPromptField))
                    {
                        splitFields = parentDbUserPromptField.Replace(" ", "").Split(',');
                        userText = string.Empty;
                        for (i = 1; i <= splitFields.Length; i++)
                        {
                            /*if (i == 1)
                            {
                                userText = CheckDate(objReader[i].ToString());
                            }
                            else if (i > 1)
                            {*/
                            if (objReader[i] != null && objReader[i].ToString().Replace(" ", "") != string.Empty)
                            {
                                if (i > 1 && objReader[i - 1] != null && objReader[i - 1].ToString().Replace(" ", "") != string.Empty)
                                {
                                    userText += GetUserPromptDelimiter(splitFields[i - 1], splitFields[i - 2]);
                                }
                                //userText += CheckDate(objReader[i].ToString());
                                userText += CheckDate(objReader[i].ToString(), splitFields[i - 1]);//MITS 26031
                            }
                            //}
                        }
                    }

                    if (parentDbClaimLOBField != string.Empty)
                    {
                        switch (objReader[i++].ToString())
                        {
                            case "241":
                                parentObjectName = "claimgc";
                                break;
                            case "242":
                                parentObjectName = "claimva";
                                break;
                            case "243":
                                parentObjectName = "claimwc";
                                break;
                            case "844":
                                parentObjectName = "claimdi";
                                break;
                            //smahajan6 11/26/09 MITS:18230 :Start
                            case "845":
                                parentObjectName = "claimpc";
                                break;
                            //smahajan6 11/26/09 MITS:18230 :End
                        }
                    }
                    else
                    {
                        parentObjectName = xParentObject.Name;
                    }

                    parentId = objReader[0].ToString();

                    /*if (parentIsOneToOne)
                    {
                        if (parentHasChildren)
                        {
                            parentNodeDetail = new string[] { parentId, parentObjectName, "-1", parentRecordTitle, string.Empty, parentPresentable, parentIsOneToOne.ToString(), parentHasChildren.ToString() };
                        }
                        else
                        {
                            parentNodeDetail = new string[] { parentId, parentObjectName, "-2", parentRecordTitle, string.Empty, parentPresentable, parentIsOneToOne.ToString(), parentHasChildren.ToString() };
                        }
                    }*/
                    //else
                    //{
                        parentNodeDetail = new string[] { parentId, parentObjectName, userText, parentRecordTitle, string.Empty, parentPresentable, parentIsOneToOne.ToString(), parentHasChildren.ToString() };
                    //}

                    if (parentNodeDetail != null)
                    {
                        arrListParent = new List<string[]>();
                        arrListParent.Add(parentNodeDetail);
                    }
                    /*if (objReader.Read())
                    {
                        throw new ApplicationException("More than one parent returned from Db!");
                    }
                    */
                }
                else
                {
                    throw new ApplicationException("Could not find the parent!");
                }

                return arrListParent;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                }
            }
        }

        public List<string[]> GetNodeSiblings(int nodeId, string currentObjectName, string parentObjectName, string parentId, int viewId, int dsnId, int objSysUseClaimantName)
        {
            XmlNode xCurrentObject = null;
            XmlNode xElement = null;

            string userText = string.Empty;
            string currentRecordTitle = string.Empty;
            string currentDbTableName = string.Empty;
            string currentDbIdField = string.Empty;
            string currentDbParentIdField = string.Empty;
            string currentDbUserPromptField = string.Empty;
            string currentDbClaimLOBField = string.Empty;
            string currentDbJointFields = string.Empty;
            string siblingObjectName = currentObjectName;
            string currentPresentable = Boolean.TrueString;
            bool currentIsOneToOne = false;
            string elementName = string.Empty;
            string missingOrEmptyElements = string.Empty;
            string sClaimantName = string.Empty;
            string[] siblingNodeDetail = null;
            List<string[]> arrListSiblings = new List<string[]>();
            DbReader objReader = null;
            //SysSettings sys = null;
            try
            {
                //check if the object name is claim then find its event and then 
                //do find the list of all its sibling claims 
                //add each of them 
                //otherwise,null
                xCurrentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + currentObjectName);

                if (xCurrentObject != null)
                {
                    elementName = "oneToOne";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        if (xElement.InnerText.ToUpper() == "YES")
                            currentIsOneToOne = true;
                        else
                            currentIsOneToOne = false;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    if (!currentIsOneToOne)
                    {
                        elementName = "dbTable";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbTableName = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }

                        elementName = "idField";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbIdField = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }

                        elementName = "parentIdField";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbParentIdField = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }

                        elementName = "userPromptField";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbUserPromptField = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }

                        elementName = "recordTitle";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentRecordTitle = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }

                        elementName = "claimLOBField";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbClaimLOBField = xElement.InnerText;
                        }

                        elementName = "jointFields";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentDbJointFields = xElement.InnerText;
                        }
                    }
                }
                else
                {
                    missingOrEmptyElements += currentObjectName;
                }

                if (!string.IsNullOrEmpty(missingOrEmptyElements))
                {
                    throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                }

                if (!currentIsOneToOne)
                {
                    objReader = GetDbQueryForNodeSiblings(currentDbIdField, currentDbUserPromptField, currentDbParentIdField, currentDbTableName, parentId, nodeId, currentDbClaimLOBField, currentDbJointFields);

                    while (objReader.Read())
                    {
                        if (currentDbClaimLOBField != string.Empty)
                        {
                            switch (objReader[2].ToString())
                            {
                                case "241":
                                    siblingObjectName = "claimgc";
                                    break;
                                case "242":
                                    siblingObjectName = "claimva";
                                    break;
                                case "243":
                                    siblingObjectName = "claimwc";
                                    break;
                                case "844":
                                    siblingObjectName = "claimdi";
                                    break;
                                //smahajan6 11/26/09 MITS:18230 :Start
                                case "845":
                                    siblingObjectName = "claimpc";
                                    break;
                                //smahajan6 11/26/09 MITS:18230 :End
                            }
                            xCurrentObject = m_ChildScreenDoc.SelectSingleNode("screen/" + siblingObjectName);

                            if (xCurrentObject != null)
                            {
                                elementName = "recordTitle";
                                xElement = xCurrentObject.SelectSingleNode(elementName);
                                if (xElement != null && xElement.InnerText != string.Empty)
                                {
                                    currentRecordTitle = xElement.InnerText;
                                }
                                else
                                {
                                    missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                                }
                            }
                            else
                            {
                                missingOrEmptyElements += currentObjectName;
                            }

                            if (!string.IsNullOrEmpty(missingOrEmptyElements))
                            {
                                throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                            }
                        }

                        if (siblingObjectName != string.Empty)
                        {
                            //Added by amitosh for mits 23476 (05/11/2011)
                            if (siblingObjectName.Contains("claim"))
                            {
                                //sys = new SysSettings(m_sConnectionString); //Mits id:32160 -Performance issue - start
                                //if (sys.UseClaimantName == 0)
                                if (objSysUseClaimantName == 0) //Mits id:32160 -Performance issue - End
                                {

                                    siblingNodeDetail = new string[] { objReader[0].ToString(), siblingObjectName, objReader[1].ToString(), currentRecordTitle, string.Empty, currentPresentable, currentIsOneToOne.ToString(), Boolean.FalseString };
                                }
                                else
                                {
                                    sClaimantName = GetClaimantName(Conversion.ConvertObjToInt(objReader[0], m_iClientId));
                                    switch (currentRecordTitle)
                                    {
                                        case "General Claim":
                                            currentRecordTitle = "GC";
                                            break;
                                        case "Property Claim":
                                            currentRecordTitle = "PC";
                                            break;
                                        case "Workers Comp.":
                                            currentRecordTitle = "WC";
                                            break;
                                        case "Non-occupational":
                                            currentRecordTitle = "Non-Occ";
                                            break;
                                        case "Vehicle Accident":
                                            currentRecordTitle = "VA";
                                            break;
                                    }
                                    if (!string.IsNullOrEmpty(sClaimantName))
                                        siblingNodeDetail = new string[] { objReader[0].ToString(), siblingObjectName, objReader[1].ToString() + " * " + sClaimantName + " ", currentRecordTitle, string.Empty, currentPresentable, currentIsOneToOne.ToString(), Boolean.FalseString };
                                    else
                                        siblingNodeDetail = new string[] { objReader[0].ToString(), siblingObjectName, objReader[1].ToString(), currentRecordTitle, string.Empty, currentPresentable, currentIsOneToOne.ToString(), Boolean.FalseString };
                                }
                            }
                            //skhare7 Polint Policy interface
                            //Add by ksirigudi for mits:32383 06/24/2013 Start
                            else if (siblingObjectName.Contains("unit") || siblingObjectName.Contains("propertyloss") || siblingObjectName.Contains("siteloss") || siblingObjectName.Contains("otherunitloss"))
                            {
                                string sPrefixUnit = GetInsuredflag(Conversion.ConvertObjToInt(objReader[0], m_iClientId), siblingObjectName);
                                siblingNodeDetail = new string[] { objReader[0].ToString(), siblingObjectName, objReader[1].ToString() + sPrefixUnit, currentRecordTitle, string.Empty, currentPresentable, currentIsOneToOne.ToString(), Boolean.FalseString };
                            
                            }
                            else
                            {
                                siblingNodeDetail = new string[] { objReader[0].ToString(), siblingObjectName, objReader[1].ToString(), currentRecordTitle, string.Empty, currentPresentable, currentIsOneToOne.ToString(), Boolean.FalseString };
                            }
                            //end Amitosh
                            //siblingNodeDetail = new string[] { objReader[0].ToString(), siblingObjectName, objReader[1].ToString(), currentRecordTitle, string.Empty, currentPresentable, currentIsOneToOne.ToString(), Boolean.FalseString };
                            siblingObjectName = string.Empty;
                        }

                        if (siblingNodeDetail != null)
                        {
                            arrListSiblings.Add(siblingNodeDetail);
                        }
                    }
                }
                return arrListSiblings;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                }
                /*if (sys != null)
                    sys = null;*/
            }
        }

        public List<string> GetLandingItems(int viewId,int dsnId,int userId)
        {
            List<string> homeSysNames = null;
            List<string> landingItems = new List<string>();

            if (viewId == 0) // Non-PowerView
            {
                if (CheckAutoDiaryLaunchFlag() == true)
                {
                    landingItems.Add("diarylist"); //the hard-coded value is the sysName for Diary List(diarylist.aspx).
                }
            }
            else // See if Powerview Home has been setup...
            {
                homeSysNames = GetPowerViewHomeSysNames(viewId, dsnId);
                if (homeSysNames != null && homeSysNames.Count != 0)
                {
                    landingItems.AddRange(homeSysNames);
                }
            }

            landingItems.Add("whatsnew"); // in case already added items get rejected (e.g. no powerview permission), then use "What's New"(default welcome)
                                          // the hard-coded value is the sysName for What's New(whatsnew.aspx).

            if (CheckPrintChequeFlag(userId) == true) // if there are checks to be printed, then also add this. Since it is added at the end, it would be the primary landing item.
            {
                landingItems.Add("StartupPaymentNotification"); // the hard-coded value is the sysName
            }

            return landingItems;
        }

        #endregion

        #region Private Methods

        private DbReader GetDbQueryForChildren(string dbFieldParent, int parentId, string dbChildTableName, string dbChildFieldId, string dbChildFieldPrompt, string dbChildExtraField, string currentObjectName, LocalCache objCache)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            //LocalCache objCache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue
            try
            {
                //in case of child is list node fetch count of dbfieldchildid where 
                //select claimid,claim_number from claim where event_id = 1212

                sQuery = "SELECT " + dbChildFieldId;
                if (dbChildFieldPrompt != string.Empty)
                {
                    sQuery += "," + dbChildFieldPrompt;
                }
                if (dbChildExtraField != string.Empty)
                {
                    sQuery += "," + dbChildExtraField;
                }

                sQuery = sQuery + " FROM " + dbChildTableName + " WHERE " + dbFieldParent + "=" + parentId;
                if (currentObjectName == "reservelisting")
                {
                    if (dbFieldParent == "CLAIM_ID")
                    {
                        sQuery += " AND UNIT_ROW_ID = 0 AND CLAIMANT_ROW_ID = 0";
                    }
                    sQuery += " AND (RSW_STATUS_CODE = " + objCache.GetCodeId("PE", "RSW_STATUS") + " OR RSW_STATUS_CODE = " + objCache.GetCodeId("PA", "RSW_STATUS") + " OR RSW_STATUS_CODE = " + objCache.GetCodeId("RJ", "RSW_STATUS") + ")";
                }
                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            
        }

        private DbReader GetDbQueryForNumberOfRecords(int currentObjectId, string currentDbIdField, string currentDbTableName, string childDbIdField, string childDbTableName, string childDbParentIdField, string childObjectName, string currentObjectName, LocalCache objCache)
        {
            //LocalCache objCache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue 
            DbReader objRdr = null;
            string sQuery = string.Empty;
            try
            {
                if (currentObjectName == "reservelisting")
                {
                    sQuery = "SELECT Count(" + GetFullDbFieldInfo(childDbTableName, childDbIdField) + ") FROM " + GetDbMainTable(childDbTableName) + " WHERE " + GetFullDbFieldInfo(childDbTableName, childDbParentIdField) + "=" + currentObjectId;
                }
                else
                {
                    //in case of child is a list node fetch # of records in the list
                    sQuery = "SELECT Count(" + GetFullDbFieldInfo(childDbTableName, childDbIdField) + ") FROM " + GetDbMainTable(currentDbTableName) + ", " + GetDbMainTable(childDbTableName) + " WHERE " + GetFullDbFieldInfo(currentDbTableName, currentDbIdField) + "=" + GetFullDbFieldInfo(childDbTableName, childDbParentIdField) + " AND " + GetFullDbFieldInfo(currentDbTableName, currentDbIdField) + "=" + currentObjectId;
                }
                /*if (childObjectName == "rswcurrent") {
                    sQuery += " AND (RSW_STATUS_CODE = " + objCache.GetCodeId("PE", "RSW_STATUS") + " OR RSW_STATUS_CODE = " + objCache.GetCodeId("PA", "RSW_STATUS") + " OR RSW_STATUS_CODE = " + objCache.GetCodeId("AP", "RSW_STATUS") + ")";
                }
                else if (childObjectName == "rswrejected") {
                    sQuery += " AND RSW_STATUS_CODE = " + objCache.GetCodeId("RJ", "RSW_STATUS");
                }
                else*/ 
                if (childObjectName == "reserveworksheet") {
                    if (childDbParentIdField == "CLAIM_ID")
                    {
                        sQuery += " AND UNIT_ROW_ID = 0 AND CLAIMANT_ROW_ID = 0";
                    }
                    sQuery += " AND (RSW_STATUS_CODE = " + objCache.GetCodeId("HI", "RSW_STATUS") + " OR RSW_STATUS_CODE = " + objCache.GetCodeId("AP", "RSW_STATUS") + ")";
                }

                //aaggarwal29 start : MITS 34580 Added the InInsured != -2 check to remove units which are nota ssociated with any policy from appearing in MDI menu count.
                if (childObjectName == "unit" || childObjectName == "siteloss" || childObjectName == "otherunitloss")
                {
                    // akaushik5 Changed for MITS 36857 Starts
                    //sQuery += " AND ISINSURED <> -2";
                    sQuery += " AND (ISINSURED <> -2 OR ISINSURED IS NULL)";
                    // akaushik5 Changed for MITS 36857 Ends
                }
                if (childObjectName == "propertyloss")
                {
                    sQuery += " AND INSURED <> -2";
                }
                //aaggarwal29 end : MITS 34580

                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            
            
        }
        
        //Bkuzhanthaim : MITS- 36027/RMA-342:auto expansion of all adjusters on load of claim.
        private DbReader GetDbQueryRecords(int currentObjectId, string currentDbIdField, string currentDbTableName, string childDbIdField, string childDbTableName, string childDbParentIdField, string childObjectName, string currentObjectName, LocalCache objCache)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            try
            {
                if (currentObjectName == "reservelisting")
                {
                    sQuery = "SELECT " + childDbIdField + " FROM " + GetDbMainTable(childDbTableName) + " WHERE " + GetFullDbFieldInfo(childDbTableName, childDbParentIdField) + "=" + currentObjectId;
                }
                else
                {
                    sQuery = "SELECT " + childDbIdField + " FROM " + GetDbMainTable(currentDbTableName) + ", " + GetDbMainTable(childDbTableName) + " WHERE " + GetFullDbFieldInfo(currentDbTableName, currentDbIdField) + "=" + GetFullDbFieldInfo(childDbTableName, childDbParentIdField) + " AND " + GetFullDbFieldInfo(currentDbTableName, currentDbIdField) + "=" + currentObjectId;
                }
                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);
                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }


        }

        private string GetDbJointCondition(string TableInfo, string jointFields)
        {
            string[] splitFields;
            string[] splitTables;
            string mainField = string.Empty;
            string mainTable = string.Empty;
            string condition = string.Empty;

            bool allAgainstMainField = true;

            jointFields = jointFields.Replace(" ", "");
            splitFields = jointFields.Split(',');

            TableInfo = TableInfo.Replace(" ", "");
            splitTables = TableInfo.Split(',');
            mainTable = splitTables[0];

            if (splitFields.Length == 1)
            {
                throw new ApplicationException("Joint fields were not properly provided in childscreen.xml!");
            }

            for (int i = 0; i < splitFields.Length; i++)
            {
                if (i == 0)
                {
                    if (splitFields[i].IndexOf('.') == -1)
                    {
                        splitFields[i] = mainTable + "." + splitFields[i];
                    }
                    mainField = splitFields[i];
                    condition = mainField;
                }
                else if (i == 1)
                {
                    condition += " = ";
                    if (splitFields[i].IndexOf('.') == -1)
                    {
                        splitFields[i] = splitTables[i] + "." + splitFields[i];
                    }
                    condition += splitFields[i];
                }
                else
                {
                    if (i + 1 < splitFields.Length && splitFields[i].IndexOf('.') != -1 && splitFields[i + 1].IndexOf('.') != -1)
                    {
                        condition += " AND " + splitFields[i] + " = " + splitFields[++i];
                    }
                    else
                    {
                        condition += " AND " + mainField + " = ";
                        if (splitFields[i].IndexOf('.') == -1)
                        {
                            splitFields[i] = splitTables[i] + "." + splitFields[i];
                        }
                        condition += splitFields[i];
                    }
                }
            }
            return condition;
        }

        private string GetDbMainTable(string tableInfo)
        {
            string[] splitTables;
            if (!string.IsNullOrEmpty(tableInfo))
            {
                tableInfo = tableInfo.Replace(" ", "");
                splitTables = tableInfo.Split(',');
                tableInfo = splitTables[0];
            }
            return tableInfo;
        }

        private string GetFullDbFieldInfo(string tableInfo, string fieldInfo)
        {
            string[] splitTables;
            string[] splitFields;
            string mainTable;
            if (!string.IsNullOrEmpty(tableInfo) && !string.IsNullOrEmpty(fieldInfo))
            {
                tableInfo = tableInfo.Replace(" ", "");
                splitTables = tableInfo.Split(',');
                mainTable = splitTables[0];

                fieldInfo = fieldInfo.Replace(" ", "");
                splitFields = fieldInfo.Split(',');
                fieldInfo = string.Empty;

                for (int i = 0; i < splitFields.Length; i++)
                {
                    if (splitFields[i].IndexOf('.') == -1)
                    {
                        splitFields[i] = mainTable + "." + splitFields[i];
                    }
                    if (i == 0)
                    {
                        fieldInfo = splitFields[i];
                    }
                    else
                    {
                        fieldInfo += ", " + splitFields[i];
                    }
                }
            }
            return fieldInfo;
        }

        private string MatchParentIdField(string candidateParentIdFields, string candidateParentNames, string actualParentName, string elementName)
        {
            string[] splitNames = candidateParentNames.Replace(" ", "").Split(',');
            string[] splitFields = candidateParentIdFields.Replace(" ", "").Split(',');
            string match = splitFields[0];
            if (splitFields.Length != splitNames.Length)
            {
                throw new ApplicationException("For " + elementName + ", lengths of parentIdFiled and parent elements do not match in childScreen.xml!");
            }
            if (splitFields.Length > 1)
            {
                for (int i = 0; i < splitNames.Length; i++)
                {
                    if (splitNames[i].ToLower() == actualParentName.ToLower())
                    {
                        match = splitFields[i];
                        break;
                    }
                }
            }

            return match;
        }


        private DbReader GetDbQueryForNodeParent(string parentDbIdField, string parentDbUserPromptField, string parentDbJointFields, string parentDbTableName, string currentDbTableName, string currentDbIdField, string currentDbParentIdField, int currentNodeId, string parentDbClaimLOBField)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            try
            {
                sQuery = "SELECT ";
                sQuery += GetFullDbFieldInfo(parentDbTableName, parentDbIdField);

                if (!string.IsNullOrEmpty(parentDbUserPromptField))
                {
                    sQuery += ", " + GetFullDbFieldInfo(parentDbTableName, parentDbUserPromptField);
                }

                if (parentDbClaimLOBField != string.Empty)
                {
                    sQuery += ", " + GetFullDbFieldInfo(parentDbTableName, parentDbClaimLOBField);
                }
                //Changed By Neha To handle casemangement.
                if (string.Compare(currentDbTableName, parentDbTableName, false) != 0)
                    sQuery += " FROM " + currentDbTableName + "," + parentDbTableName + " WHERE " + GetFullDbFieldInfo(parentDbTableName, parentDbIdField) + "=" + GetFullDbFieldInfo(currentDbTableName, currentDbParentIdField) + " AND " + GetFullDbFieldInfo(currentDbTableName, currentDbIdField) + "=" + currentNodeId;
                else
                    sQuery += " FROM " + currentDbTableName + " WHERE " + GetFullDbFieldInfo(currentDbTableName, currentDbIdField) + "=" + currentNodeId;
       
                //sQuery += " FROM " + currentDbTableName + "," + parentDbTableName + " WHERE " + GetFullDbFieldInfo(parentDbTableName, parentDbIdField) + "=" + GetFullDbFieldInfo(currentDbTableName, currentDbParentIdField) + " AND " + GetFullDbFieldInfo(currentDbTableName, currentDbIdField) + "=" + currentNodeId;
                if (!string.IsNullOrEmpty(parentDbJointFields))
                {
                    sQuery += " AND " + GetDbJointCondition(parentDbTableName, parentDbJointFields);
                }
                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private DbReader GetDbQueryForNodeSiblings(string dbFieldSiblingId, string dbFieldSiblingUserPrompt, string dbFieldParentId, string dbCurrentNodeTableName, string parentId, int currentNodeId, string LOBDbField, string jointFields)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            try
            {
                sQuery = "SELECT " + GetFullDbFieldInfo(dbCurrentNodeTableName, dbFieldSiblingId) + ", " + GetFullDbFieldInfo(dbCurrentNodeTableName, dbFieldSiblingUserPrompt);
                if (!string.IsNullOrEmpty(LOBDbField))
                {
                    sQuery += "," + GetFullDbFieldInfo(dbCurrentNodeTableName, LOBDbField);
                }
                sQuery += " FROM " + dbCurrentNodeTableName + " WHERE " + GetFullDbFieldInfo(dbCurrentNodeTableName, dbFieldParentId) + "=" + parentId + " AND " + GetFullDbFieldInfo(dbCurrentNodeTableName, dbFieldSiblingId) + " <> " + currentNodeId;
                if (!string.IsNullOrEmpty(jointFields))
                {
                    sQuery += " AND " + GetDbJointCondition(dbCurrentNodeTableName, jointFields);
                }
				//aaggarwal29 : MITS 37196, If we refresh the unit/property/site/other unit screen under claim MDI, the MDI count is not correct. in synch with code in GetDbQueryForNumberOfRecords method
                if (dbCurrentNodeTableName.Contains("UNIT_X_CLAIM") || dbCurrentNodeTableName.Contains("CLAIM_X_OTHERUNIT") || dbCurrentNodeTableName.Contains("CLAIM_X_SITELOSS"))
                {
                    sQuery += " AND ISINSURED <> -2";
                }
                if (dbCurrentNodeTableName.Contains("CLAIM_X_PROPERTYLOSS"))
                {
                    sQuery += " AND INSURED <> -2";
                }
                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

      
        /// <summary>
        /// FOr mits 23476(05/11/2011)
        /// </summary>
        /// <author>Amitosh</author>
        /// <param name="conditionNodeId"></param>
        /// <returns></returns>
        private string GetClaimantName(int conditionNodeId)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            string sClaimantName = string.Empty;
             string lastname = string.Empty;
             string firstname = string.Empty;
             try
             {
                 //select LAST_NAME,FIRST_NAME from ENTITY where ENTITY_ID=(select CLAIMANT_EID from CLAIMANT where CLAIM_ID=16)
                 sQuery = "SELECT LAST_NAME,FIRST_NAME FROM ENTITY WHERE ENTITY_ID = (SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + conditionNodeId + " AND  PRIMARY_CLMNT_FLAG = -1)";
                 objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);
                 if (objRdr.Read())
                 {
                     lastname = objRdr.GetString("LAST_NAME");
                     firstname = objRdr.GetString("FIRST_NAME");
                     if (!string.IsNullOrEmpty(firstname))
                         sClaimantName = lastname + ", " + firstname; //tmalhotra2
                     else
                         sClaimantName = lastname;
                 }
                 else
                 {
                     // sQuery = "SELECT LAST_NAME,FIRST_NAME FROM ENTITY WHERE ENTITY_ID = (SELECT MIN(CLAIMANT_EID) FROM CLAIMANT WHERE CLAIM_ID = " + conditionNodeId + " )";
                     sQuery = "SELECT LAST_NAME,FIRST_NAME FROM ENTITY WHERE ENTITY_ID = (SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIMANT_ROW_ID = (SELECT MIN(CLAIMANT_ROW_ID) FROM CLAIMANT WHERE CLAIM_ID =" + conditionNodeId + " ) AND CLAIM_ID = " + conditionNodeId + ") "; //tmalhotra2 edit
                     objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);
                     if (objRdr.Read())
                     {
                         lastname = objRdr.GetString("LAST_NAME");
                         firstname = objRdr.GetString("FIRST_NAME");
                         if (!string.IsNullOrEmpty(firstname))
                             sClaimantName = lastname + ", " + firstname; //tmalhotra2
                         else
                             sClaimantName = lastname;
                     }
                 }
                 return sClaimantName;
             }

             catch (Exception ex)
             {
                 throw new ApplicationException(ex.Message);
             }
             finally
             {
                 if(objRdr != null)
                 objRdr.Dispose();
                 

             }
          
        }
        private DbReader GetDbQueryForNodeUserPrompt(string dbIdField, string dbFieldNodeUserPrompt, string dbTableName, string dbConditionIdField, int conditionNodeId, string jointFields, string dbExtraField, string objectName, LocalCache objCache)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            //LocalCache objCache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue 
            try
            {
                sQuery = "SELECT " + GetFullDbFieldInfo(dbTableName, dbIdField);

                sQuery += "," + GetFullDbFieldInfo(dbTableName, dbFieldNodeUserPrompt);

                if (!string.IsNullOrEmpty(dbExtraField))
                {
                    sQuery += "," + GetFullDbFieldInfo(dbTableName, dbExtraField);
                }
                sQuery += " FROM " + dbTableName + " WHERE " + GetFullDbFieldInfo(dbTableName, dbConditionIdField) + "=" + conditionNodeId;
                if (!string.IsNullOrEmpty(jointFields))
                {
                    sQuery += " AND " + GetDbJointCondition(dbTableName, jointFields);
                }
                if (objectName == "RESERVEWORKSHEET")
                {
                    sQuery += " AND (RSW_STATUS_CODE = " + objCache.GetCodeId("PE", "RSW_STATUS") + " OR RSW_STATUS_CODE = " + objCache.GetCodeId("PA", "RSW_STATUS") + " OR RSW_STATUS_CODE = " + objCache.GetCodeId("RJ", "RSW_STATUS") + ")";
                }
                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private DbReader GetDbQueryForPowerViewXml(string objectName, int viewId, int dsnId)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            string netViewConnectionString = string.Empty;

            try
            {
                sQuery = "SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = " + viewId + " AND FORM_NAME = '" + objectName + ".xml' AND DATA_SOURCE_ID = " + dsnId;

                //getting connectionstring for RMX Page page in order to fetch record from NetViewForm
                netViewConnectionString = RMConfigurationManager.GetConnectionString("ViewDataSource",m_iClientId);

                objRdr = DbFactory.GetDbReader(netViewConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message, ex);
            }
        }

        private DbReader GetDbSysEventCatInfo(int EventId)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;

            try
            {
                sQuery = "SELECT SYS_EVENT_CAT.DISPLAY_ID, EVENT.EVENT_IND_CODE FROM SYS_EVENT_CAT, CODES, EVENT";
                sQuery += " WHERE SYS_EVENT_CAT.EVENT_CAT_CODE = CODES.RELATED_CODE_ID";
                sQuery += " AND CODES.CODE_ID = EVENT.EVENT_IND_CODE";
                sQuery += " AND EVENT.EVENT_ID = " + EventId;

                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message, ex);
            }
        }

        private DbReader GetDbQueryForDetailTracking(string LOB)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;

            try
            {
                sQuery = "SELECT RESERVE_TRACKING FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = ";

                switch (LOB)
                {
                    case "claimgc":
                        sQuery += "'241'";
                        break;
                    case "claimva":
                        sQuery += "'242'";
                        break;
                    case "claimwc":
                        sQuery += "'243'";
                        break;
                    case "claimdi":
                        sQuery += "'844'";
                        break;
                    //smahajan6 11/26/09 MITS:18230 :Start
                    case "claimpc":
                        sQuery += "'845'";
                        break;
                    //smahajan6 11/26/09 MITS:18230 :End
                    default:
                        sQuery += "'0'";
                        break;
                }

                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message, ex);
            }
        }

        private DbReader GetDBQueryForSelectedPowerViewUrl(int viewId,int dsnId)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            string netViewConnectionString = string.Empty;

            try
            {
                sQuery = "SELECT HOME_PAGE FROM NET_VIEWS WHERE VIEW_ID = " + viewId + " AND DATA_SOURCE_ID = " + dsnId;
                
                //getting connectionstring for RMX Page page in order to fetch record from NetViewForm
                netViewConnectionString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId);

                objRdr = DbFactory.GetDbReader(netViewConnectionString, sQuery);

                return objRdr;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message, ex);
            }
        }

        private ArrayList GetPowerViewChildrenList(string objectName, int viewId, int dsnId, XmlNode xCurrentObject, string parentObjectName)
        {
            DbReader objReader = null;
            ArrayList arrPowerViewChildrenList = null;
            XElement linqXSelectedView = null;
            IEnumerable<XElement> linqXmlQuery = null;
            XmlNode xElement = null;


            bool currentIsTabInParentScreen = false;
            string currentPowerViewName = string.Empty;
            string elementName = string.Empty;
            string missingOrEmptyElements = string.Empty;
            string sQuery = string.Empty;
            string strSelectedViewXml = string.Empty;
            string strAttributeParam = string.Empty;
            string strAttributeTitle = string.Empty;
            string[] childDetail;

            try
            {

                if (xCurrentObject != null)
                {
                    elementName = "tabInParentScreen";
                    xElement = xCurrentObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        if (xElement.InnerText.ToUpper() == "YES")
                            currentIsTabInParentScreen = true;
                        else
                            currentIsTabInParentScreen = false;
                    }
                    else
                    {
                        missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                    }

                    if (currentIsTabInParentScreen)
                    {
                        elementName = "powerViewName";
                        xElement = xCurrentObject.SelectSingleNode(elementName);
                        if (xElement != null && xElement.InnerText != string.Empty)
                        {
                            currentPowerViewName = xElement.InnerText;
                        }
                        else
                        {
                            missingOrEmptyElements += xCurrentObject.Name + "." + elementName + " ";
                        }
                    }
                }
                else
                {
                    missingOrEmptyElements += objectName;
                }

                if (!string.IsNullOrEmpty(missingOrEmptyElements))
                {
                    throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                }


                if (currentIsTabInParentScreen)
                {
                    objReader = GetDbQueryForPowerViewXml(parentObjectName, viewId, dsnId);
                }
                else
                {
                    objReader = GetDbQueryForPowerViewXml(objectName, viewId, dsnId);
                }

                if (objReader != null && objReader.Read())
                {
                    strSelectedViewXml = objReader[0].ToString();

                    // If Views do not exists, in that case strSelectedViewXml is blank and causes problem 
                    // while loading the strSelectedViewXml into Xml Dom
                    //if (strSelectedViewXml != string.Empty)                        
                    if (!string.IsNullOrEmpty(strSelectedViewXml)) //Mits id:32160 -Performance issue   
                    {

                        //loading xml string in exlement
                        linqXSelectedView = XElement.Parse(strSelectedViewXml); ;

                        if (currentIsTabInParentScreen)
                        {
                            //linqXmlQuery = linqXSelectedView.XPathSelectElements("./group[@name='" + currentPowerViewName + "']/control[@type='button']");
                            linqXmlQuery = from d in
                                               (from c in linqXSelectedView.Descendants("group")
                                                where (string)c.Attribute("name") == currentPowerViewName
                                                select c).Descendants("control")
                                           where (string)d.Attribute("type") == "button"
                                           select d;

                        }
                        else
                        {

                            //Mits id:32160 -Performance issue - Start
                          /*linqXmlQuery = from c in linqXSelectedView.Descendants("button").Concat(linqXSelectedView.Descendants("buttonscript")).Concat(linqXSelectedView.Descendants("group"))
                                           where (string)c.Attribute("type") == null
                                           select c; */

                           linqXmlQuery = from c in linqXSelectedView.Descendants("button").Concat(linqXSelectedView.Descendants("buttonscript")).Concat(linqXSelectedView.Descendants("group"))
                                           where (string)c.Attribute("type") == null && (string)c.Attribute("name") !=null && (string)c.Attribute("title") !=null
                                           select c; 
                            //Mits id:32160 -Performance issue - End
                        }

                        arrPowerViewChildrenList = new ArrayList();

                        foreach (XElement powerViewChild in linqXmlQuery)
                        {
                            //Mits id:32160 -Performance issue - Start
                            if (powerViewChild.Attribute("name") != null && powerViewChild.Attribute("title") != null)
                            {
                                //strAttributeParam = powerViewChild.Attribute("name").Value;
                                childDetail = new string[] { powerViewChild.Attribute("name").Value, powerViewChild.Attribute("title").Value };
                                arrPowerViewChildrenList.Add(childDetail);
                            }

                            
                            /*if (powerViewChild.Attribute("name") != null)
                            {
                                strAttributeParam = powerViewChild.Attribute("name").Value;
                            }
                            
                            if (powerViewChild.Attribute("title") != null)
                            {
                                strAttributeTitle = powerViewChild.Attribute("title").Value;
                            }

                            if (strAttributeParam != string.Empty)
                            {
                                childDetail = new string[] {strAttributeParam,strAttributeTitle};
                                arrPowerViewChildrenList.Add(childDetail);
                            } */

                            //Mits id:32160 -Performance issue - End
                        }
                    }
                }
                return arrPowerViewChildrenList;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    if (objReader.IsClosed == false)
                    {
                        objReader.Close();
                    }
                }
            }
        }
        private bool CheckCaseManagementOn(LocalCache objCache)
        {
            //DbReader objReader = null;
            //string sQuery = string.Empty;
            bool bValue = false;
            //LocalCache cache=null; //Mits id:32160 -Performance issue 
            try
            {
                //R7 Perf Imp
                //sQuery = "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG'";
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                //while (objReader.Read())
                //{
                //    if (objReader[0].ToString() == "-1")
                //    {
                //        return true;
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}
                //cache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue 
                bValue=objCache.IsCaseManagementEnabled();                
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                //cache.Dispose(); //Mits id:32160 -Performance issue 
                //if (objReader != null)
                //{
                //    if (objReader.IsClosed == false)
                //    {
                //        objReader.Close();
                //    }
                //}
            }
            return bValue;
        }
        //Mits id:32160 -Performance issue - Start
        private bool CheckExtraSecurity(string nodetocheck, int p_iCurrentObjectId, string p_sCurrentObjectName, LocalCache objCache)
        {
            bool bShow = true;
            //SysSettings objSys=new SysSettings(m_sConnectionString);
            
            switch (nodetocheck)
            {
                //Neha Changes for R8 case mangement for personinvolved
                case "casemanagement":
                case "casemanagementlist":
                    bShow = CheckCaseManagementOn(objCache);
                    break;
                case "propertyloss":
                    if(p_sCurrentObjectName.ToUpper() =="CLAIMGC")
                        bShow = GetFilteredListForClaimType(p_iCurrentObjectId, objCache.GetCodeId("PL", "LOSS_COMPONENT"));
                    break;
                case "liabilityloss":
                    if (p_sCurrentObjectName.ToUpper() == "CLAIMGC" || p_sCurrentObjectName.ToUpper() == "CLAIMWC" )
                        bShow = GetFilteredListForClaimType(p_iCurrentObjectId, objCache.GetCodeId("LL", "LOSS_COMPONENT"));
                    break;
                case "unit":
                    if(p_sCurrentObjectName.ToUpper() =="CLAIMGC")
                    bShow = GetFilteredListForClaimType(p_iCurrentObjectId, objCache.GetCodeId("VL","LOSS_COMPONENT"));
                    break;
                 case "otherunitloss"://rupal:start
                    if (p_sCurrentObjectName.ToUpper() == "CLAIMGC" || p_sCurrentObjectName.ToUpper() == "CLAIMWC" )
                    bShow = GetFilteredListForClaimType(p_iCurrentObjectId, objCache.GetCodeId("SUL","LOSS_COMPONENT"));
                    break;
                case "siteloss"://rupal:start
                    if (p_sCurrentObjectName.ToUpper() == "CLAIMWC")
                        bShow = GetFilteredListForClaimType(p_iCurrentObjectId, objCache.GetCodeId("SL", "LOSS_COMPONENT"));
                    break;
                    //if (p_sCurrentObjectName.ToUpper() == "CLAIMGC" || p_sCurrentObjectName.ToUpper() == "CLAIMWC")
                    //    bShow = GetFilteredListForOtherUnit(p_iCurrentObjectId);
                    //else
                    //    bShow = false;
                    //break;//rupal:end
               /* case "vssassignment":
                    if (objSys.EnableVSS.Equals(-1))
                    {
                        bShow = true;
                    }
                    else
                    {
                        bShow = false;
                    }
                    break;*/
            }

            return bShow;
        }
        //Mits id:32160 -Performance issue - End
        private ArrayList GetFilteredListForPowerView(ArrayList powerViewChildrenList, XmlNodeList nodeChildrenList, int p_iCurrentObjectId, string p_sCurrentObjectname, SysSettings objSys, LocalCache objCache)
        {
            XmlNode selectedChildNode = null;
            XmlNode selectedPowerViewNameNode = null;
            string strselectedPowerViewName = string.Empty;
            XmlNode selectedExtraSecureNode = null;
            string strExtraSecure = string.Empty;
            ArrayList finalFilteredChildrenList = new ArrayList();
            string[] childDetail = null;

            try
            {
                if (powerViewChildrenList == null)
                {
                    return finalFilteredChildrenList;
                }

                foreach (XmlNode childNode in nodeChildrenList)
                {
                    selectedChildNode = m_ChildScreenDoc.SelectSingleNode("screen/" + childNode.InnerText);

                    if (selectedChildNode != null)
                    {

                        selectedPowerViewNameNode = selectedChildNode.SelectSingleNode("powerViewName");

                        if (selectedPowerViewNameNode != null)
                        {
                            strselectedPowerViewName = selectedPowerViewNameNode.InnerText;

                            for (int count = 0; count < powerViewChildrenList.Count; count++)
                            {
                                childDetail = (string[])powerViewChildrenList[count];
                                if (childDetail[0] == strselectedPowerViewName)
                                {
                                    childDetail[0] = childNode.InnerText;
                                    selectedExtraSecureNode = selectedChildNode.SelectSingleNode("hasExtraSecurity");
                                    if (selectedExtraSecureNode == null)
                                    {
                                        finalFilteredChildrenList.Add(childDetail);
                                    }
                                    else
                                    {
                                        if (selectedExtraSecureNode.InnerText == "Yes")
                                        {
                                            //Mits id:32160 -Performance issue - Start
                                            //if (CheckExtraSecurity(childNode.InnerText, p_iCurrentObjectId,p_sCurrentObjectname))
                                            if(childNode.InnerText.Equals("vssassignment"))
                                            {
                                                if (objSys.EnableVSS.Equals(-1))
                                                {
                                                    finalFilteredChildrenList.Add(childDetail);
                                                }                    
                                                
                                            }
                                            //MITS 34260 Start
                                            else if (childNode.InnerText.Equals("claimxpolicy"))
                                            {
                                                if (objSys.MultiCovgPerClm.Equals(-1))
                                                {
                                                    finalFilteredChildrenList.Add(childDetail);
                                                }

                                            }
                                            //MITS 34260 End
                                            else
                                            {
                                                if (CheckExtraSecurity(childNode.InnerText, p_iCurrentObjectId, p_sCurrentObjectname,objCache))
                                                {
                                                    finalFilteredChildrenList.Add(childDetail);
                                                }
                                            }
                                            //Mits id:32160 -Performance issue - End
                                        }
                                        else
                                        {
                                            finalFilteredChildrenList.Add(childDetail);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return finalFilteredChildrenList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        private ArrayList GetFilteredListForOther(XmlNodeList nodeChildrenList)
        {
            string strselectedPowerViewName = string.Empty;
            ArrayList finalFilteredChildrenList = null;
            string[] childDetail;
            try
            {
                finalFilteredChildrenList = new ArrayList();

                foreach (XmlNode childNode in nodeChildrenList)
                {
                    childDetail = new string[] { childNode.InnerText, "" };
                    finalFilteredChildrenList.Add(childDetail);
                }
                return finalFilteredChildrenList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool CheckAutoDiaryLaunchFlag()
        {
            //DbReader objReader = null;
            //string sQuery = string.Empty;
            SysSettings objSettings = null;
            try
            {
            //    sQuery = "SELECT AUTO_LAUNCH_DIARY FROM SYS_PARMS";
            //    objReader = DbFactory.GetDbReader(m_sConnectionString, sQuery);

            //    while (objReader.Read())
            //    {
                    objSettings = new SysSettings(m_sConnectionString,m_iClientId); //Ash - cloud
                    if (objSettings.AutoLaunchDiary.ToString() == "-1")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                //}

                //return false;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                //if (objReader != null)
                //{
                //    if (objReader.IsClosed == false)
                //    {
                //        objReader.Close();
                //    }
                //}
                objSettings = null;
            }
        }

        private bool CheckPrintChequeFlag(int iUserId)
        {
            bool sRecordFoundStatus = false;
            XmlDocument objPaymentXMLDocument = null;
            XmlElement xmlTotalNumChecks = null;    // Will point to TotalNumChecks in XMLDocument
            
            try
            {
                //Callling PaymentNotification.NotifyUserOfFuturePayments() for XML document
                //PaymentNotification objPaymentNotification = new PaymentNotification(m_sConnectionString);
                PaymentNotification objPaymentNotification = new PaymentNotification(m_sConnectionString,m_iClientId);
                objPaymentXMLDocument = objPaymentNotification.NotifyUserOfFuturePayments(iUserId);
                xmlTotalNumChecks = (XmlElement)objPaymentXMLDocument.SelectSingleNode("/PaymentNotification/TotalNumChecks");

                if (xmlTotalNumChecks != null && xmlTotalNumChecks.InnerText != "0") //both of these conditions may arise for a not having the xml
                    sRecordFoundStatus = true;
                else
                    sRecordFoundStatus = false;

                return sRecordFoundStatus;
            }
            catch (Exception p_objException)
            {
                Log.Write("Message :" + p_objException.Message + "Stack :" + p_objException.StackTrace, m_iClientId);//Deb : Adde extra lgging for better troubleshooting
                throw p_objException;
            }
            finally
            {
                sRecordFoundStatus = false;
            }
        }

        private List<string> GetPowerViewHomeSysNames(int viewId, int dsnId)
        {
            DbReader objReader = null;
            string sQuery = string.Empty;
            string netViewConnectionString = string.Empty;
            List<string> homeSysNames = new List<string>();
            try
            {
                sQuery = "SELECT HOME_PAGE FROM NET_VIEWS WHERE VIEW_ID = " + viewId + " AND DATA_SOURCE_ID = " + dsnId;

                //getting connectionstring for RMX Page page in order to fetch record from NetViewForm
                netViewConnectionString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId);

                objReader = DbFactory.GetDbReader(netViewConnectionString, sQuery);
                while (objReader.Read()) // The last row is the PRIMARY one! So db query has to be modified in order to return the PRIMARY one at the end.
                {
                    if (objReader[0].ToString() != "")
                    {
                        if (objReader[0].ToString().Contains("admintrackinglist"))
                        {
                            homeSysNames.Add("admintrackinglist");
                        }
                        else
                        {
                            homeSysNames.Add(objReader[0].ToString());
                        }
                    }
                }

                return homeSysNames;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    if (objReader.IsClosed == false)
                    {
                        objReader.Close();
                    }
                }
            }
        }

        private bool ExposeChildren(int matchingId, string matchingObjectName, string currentObjectName, LocalCache objCache)
        {
            DbReader objReader = null;
            string sQuery = string.Empty;
            bool expose = false;
            //string netViewConnectionString = string.Empty;
            string matchingIdField = string.Empty;
            string clause = string.Empty;
            //LocalCache objCache = new LocalCache(m_sConnectionString);//neha //Mits id:32160 -Performance issue
            SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);      //Rijul 340
            try
            {
                if (currentObjectName == "reservelisting")
                {
                    switch (matchingObjectName)
                    {
                        case "claimant":
                            clause = ", CLAIMANT WHERE CLAIMANT.CLAIMANT_ROW_ID = " + matchingId + " AND CLAIMANT.CLAIM_ID = CLAIM.CLAIM_ID";
                            break;
                        case "unit":
                            clause = ", UNIT_X_CLAIM WHERE UNIT_X_CLAIM.UNIT_ROW_ID = " + matchingId + " AND UNIT_X_CLAIM.CLAIM_ID = CLAIM.CLAIM_ID";
                            break;
                        default:
                            clause = " WHERE CLAIM.CLAIM_ID = " + matchingId;
                            break;
                    }
                    sQuery = "SELECT SYS_PARMS_LOB.USE_RSV_WK FROM SYS_PARMS_LOB, CLAIM";
                    sQuery += clause;
                    sQuery += " AND CLAIM.LINE_OF_BUS_CODE = SYS_PARMS_LOB.LINE_OF_BUS_CODE";

                    //getting connectionstring for RMX Page page in order to fetch record from NetViewForm
                    //netViewConnectionString = RMConfigurationManager.GetConnectionString("ViewDataSource");

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sQuery);
                    while (objReader.Read()) // The last row is the PRIMARY one! So db query has to be modified in order to return the PRIMARY one at the end.
                    {
                        if (objReader[0].ToString() == "-1")
                        {
                            expose = true;
                        }
                    }
                }//Neha Added to handle left menu
                else if (currentObjectName == "piother")
                {
                    //Rijul 340
                    //sQuery = "SELECT ENTITY_TABLE_ID FROM PERSON_INVOLVED,ENTITY WHERE  PERSON_INVOLVED.PI_EID = ENTITY.ENTITY_ID AND PERSON_INVOLVED.PI_ROW_ID=" + matchingId;
                    if(objSysSettings.UseEntityRole)
                        sQuery = " SELECT EXR.ENTITY_TABLE_ID ";
                    else
                        sQuery = " SELECT E.ENTITY_TABLE_ID ";
                    sQuery = string.Concat(sQuery, " FROM PERSON_INVOLVED PI ");
                    if (objSysSettings.UseEntityRole)
                        sQuery = string.Concat(sQuery, " INNER JOIN ENTITY_X_ROLES EXR ON PI.PI_EID = EXR.ENTITY_ID AND PI.PI_ER_ROW_ID = EXR.ER_ROW_ID ");
                    else
                        sQuery = string.Concat(sQuery, " INNER JOIN ENTITY E ON PI.PI_EID = E.ENTITY_ID  ");
                    sQuery = string.Concat(sQuery, " WHERE PI.PI_ROW_ID = ", matchingId);
                    //Rijul End
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                    while (objReader.Read()) // The last row is the PRIMARY one! So db query has to be modified in order to return the PRIMARY one at the end.
                    {
                        string systablename = objCache.GetTableName(Convert.ToInt32(objReader[0]));
                        if (systablename == "DRIVER_OTHER" || systablename == "DRIVER_INSURED")
                        {
                            expose = true;
                        }
                    }
                }//Neha end
                return expose;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    if (objReader.IsClosed == false)
                    {
                        objReader.Close();
                    }
                }
                objSysSettings = null;      //rijul 340
            }
        }

        private string CheckDate(string sText, string sUserPromptField)
        {
            bool bIsDate = false;
            bool bIsTime = false;
            string yyyy = string.Empty;
            string mm = string.Empty;
            string dd = string.Empty;
            string hour = string.Empty;
            string minute = string.Empty;
            string second = string.Empty;
            string sDbDateTime = sText;
            //MITS 26031 Start
            const string DATE1 = "DATE";
            const string DATE2 = "DTTM";
            Hashtable hDate = new Hashtable()
                {
                    {DATE1, DATE1},
                    {DATE2, DATE2}
                };
            IDictionaryEnumerator enumerator = hDate.GetEnumerator();
            bool bDbField = false;
            int i, j, l;

            while (enumerator.MoveNext())
            {
                if (sUserPromptField.IndexOf(enumerator.Value.ToString()) != -1)
                {
                    bDbField = true;
                }
            }

            if (!bDbField)
            {
                return sText; //if it is actually a Date or Time Stamp field then the DB field name must have "DATE" or "DTTM" in it!
            }

            //so far it looks like it is a DATE or Time Stamp field; lets make sure and then change into a right format...
            //MITS 26031
            l = sText.Length;
            if (l == 8 || l == 14)
            {
                for (i = 0; i < 8; i++)
                {
                    if ((int)sText[i] < 48 || (int)sText[i] > 57)
                    {
                        break;
                    }
                    else
                    {
                        if (i == 3) //year may be complete
                        {
                            yyyy = sText.Substring(0, 4);
                            j = Convert.ToInt32(yyyy);
                            if (j < 1800)
                            {
                                break;
                            }
                        }
                        else if (i == 5) //month may be complete
                        {
                            mm = sText.Substring(4, 2);
                            j = Convert.ToInt32(mm);
                            if (j > 12 || j == 0)
                            {
                                break;
                            }
                        }
                        else if (i == 7) //day may be complete
                        {
                            dd = sText.Substring(6, 2);
                            j = Convert.ToInt32(dd);
                            if (j > 31 || j == 0)
                            {
                                break;
                            }
                            else
                            {
                                bIsDate = true; //Date is complete
                            }
                        }
                    }
                }
                if (l == 14 && bIsDate)
                {
                    //now checking for time
                    for (i = 8; i < l; i++)
                    {
                        if ((int)sText[i] < 48 || (int)sText[i] > 57)
                        {
                            break;
                        }
                        else
                        {
                            if (i == 9) //hour may be complete
                            {
                                hour = sText.Substring(8, 2);
                                j = Convert.ToInt32(hour);
                                if (j > 23)
                                {
                                    break;
                                }
                            }
                            else if (i == 11) //minute may be complete
                            {
                                minute = sText.Substring(10, 2);
                                j = Convert.ToInt32(minute);
                                if (j > 59)
                                {
                                    break;
                                }
                            }
                            else if (i == 13) //second may be complete
                            {
                                second = sText.Substring(12, 2);
                                j = Convert.ToInt32(second);
                                if (j > 59)
                                {
                                    break;
                                }
                                else
                                {
                                    bIsTime = true; //Time is complete
                                }
                            }
                        }
                    }
                }
                if (bIsDate)
                {
                    sText = mm + (char)47 + dd + (char)47 + yyyy;
                    sText = Conversion.GetUIDate(sText,sLangCode, m_iClientId);
                    if (bIsTime)
                    {
                        //sText += (char)32 + hour + (char)58 + minute + (char)58 + second;
                        sText += Conversion.GetUITime(sDbDateTime, sLangCode, m_iClientId);
                    }
                }
            }


            return sText;
        }


        private string GetUserPromptDelimiter(string sUserPromptCurrentItem, string sUserPromptPreviousItem)
        {
            const string firstName = "FIRST_NAME";
            const string lastName = "LAST_NAME";
            const string defaultDelimiter = " * ";
            const string nameDelimiter1 = ", ";
            const string nameDelimiter2 = " ";
            string userPromptDelimiter = string.Empty;

            if (sUserPromptPreviousItem.IndexOf('.') != -1)
            {
                sUserPromptPreviousItem = sUserPromptPreviousItem.Split('.')[1];
            }
            if (sUserPromptCurrentItem.IndexOf('.') != -1)
            {
                sUserPromptCurrentItem = sUserPromptCurrentItem.Split('.')[1];
            }
            if (sUserPromptPreviousItem.ToUpper() == lastName && sUserPromptCurrentItem.ToUpper() == firstName)
            {
                userPromptDelimiter = nameDelimiter1; //LastName, FirstName
            }
            else if (sUserPromptPreviousItem.ToUpper() == firstName && sUserPromptCurrentItem.ToUpper() == lastName)
            {
                userPromptDelimiter = nameDelimiter2; //FirstName LastName
            }
            else
            {
                userPromptDelimiter = defaultDelimiter;
            }

            return userPromptDelimiter;
        }

        /// <summary>
        /// Neha Added to change the Piother node title</summary>
        /// <param name="recordid"></param>
        /// <returns>int</returns>
        private int CheckRecordType(int recordid, LocalCache objCache)
        {
            DbReader objReader = null;
            string sQuery = string.Empty;
            int intType = 0;
            //LocalCache objCache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue
            SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId);      //Rijul 340
            try
            {
                //Rijul 340
                //sQuery = "SELECT ENTITY_TABLE_ID FROM PERSON_INVOLVED,ENTITY WHERE  PERSON_INVOLVED.PI_EID = ENTITY.ENTITY_ID AND PERSON_INVOLVED.PI_ROW_ID=" + recordid;

                if (objSysSettings.UseEntityRole)
                    sQuery = " SELECT EXR.ENTITY_TABLE_ID ";
                else
                    sQuery = " SELECT E.ENTITY_TABLE_ID ";
                sQuery = string.Concat(sQuery, " FROM PERSON_INVOLVED PI ");
                if (objSysSettings.UseEntityRole)
                    sQuery = string.Concat(sQuery, " INNER JOIN ENTITY_X_ROLES EXR ON PI.PI_EID = EXR.ENTITY_ID AND PI.PI_ER_ROW_ID = EXR.ER_ROW_ID ");
                else
                    sQuery = string.Concat(sQuery, " INNER JOIN ENTITY E ON PI.PI_EID = E.ENTITY_ID  ");
                sQuery = string.Concat(sQuery, " WHERE PI.PI_ROW_ID = ", recordid);
                //Rijul End
                objReader = DbFactory.GetDbReader(m_sConnectionString, sQuery);

                while (objReader.Read())
                {
                    string systablename = objCache.GetTableName(Convert.ToInt32(objReader[0]));
                    if (systablename == "DRIVER_OTHER")
                    {
                        intType = 1;
                    }
                    else if (systablename == "DRIVER_INSURED")
                    {
                        intType = 2;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objReader != null)
                {
                    if (objReader.IsClosed == false)
                    {
                        objReader.Close();
                    }
                }
                objSysSettings = null;      //rijul 340
            }
            return intType;
        }
        //Mits id:32160 -Performance issue - Start   
        private bool GetFilteredListForClaimType(int p_sCLaimId, int objCacheGetCodeId)
        {
            string sQuery = null;
            DbReader objRdr = null;
            //LocalCache objCache = new LocalCache(m_sConnectionString); //Mits id:32160 -Performance issue
            ArrayList iCodeArray = new ArrayList();

            try
            {
                //sQuery = "SELECT RELATIONSHIP_ID FROM CODE_X_CODE WHERE  CODE1 = (SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID =" + p_sCLaimId + ") AND CODE2 = " + objCache.GetCodeId(p_sNode, p_sTableName) + "AND DELETED_FLAG = 0";
                sQuery = "SELECT RELATIONSHIP_ID FROM CODE_X_CODE WHERE  CODE1 = (SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID =" + p_sCLaimId + ") AND CODE2 = " + objCacheGetCodeId + "AND DELETED_FLAG = 0";
                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);
                if (objRdr.Read())
                {
                    //   iCodeArray.Add(Conversion.ConvertStrToInteger(objRdr.GetValue("CODE2").ToString()));
                    return true;

                }

                //if (!iCodeArray.Contains(objCache.GetCodeId("LL", "LOSS_COMPONENT")))
                //{
                //   // base.AddKillNode("");
                //    return true;
                //}
                //if (!iCodeArray.Contains(objCache.GetCodeId("VL", "LOSS_COMPONENT")))
                //{
                //    //base.AddKillNode("UnitList");
                //    return true;
                //}

                //if (!iCodeArray.Contains(objCache.GetCodeId("PL", "LOSS_COMPONENT")))
                //{
                //   // base.AddKillNode("btnPropertyloss");
                //    return true;
                //}
                else
                {
                    return false;
                }
            }


            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Close();
                    objRdr.Dispose();
                }
            }
        }
        //Mits id:32160 -Performance issue - End 
        //rupal:start, policy system interface change
        private bool GetFilteredListForOtherUnit(int p_iCurrentObjectId)
        {           
            SysSettings objSysSettings = new SysSettings(m_sConnectionString,m_iClientId); //Ash - cloud
            try
            {
                if (objSysSettings.UsePolicyInterface && GetRecordCountForOtherUnit(p_iCurrentObjectId) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }                
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            finally
            {
                if (objSysSettings != null)
                {
                    objSysSettings = null;
                }
            }
        }
        
        private int GetRecordCountForOtherUnit(int p_iCurrentObjectId)
        {
            #region commented
            /*
            int iRecCount = 0;
            XmlNode xChildObject = null;
            string childObjectName = string.Empty;
            string elementName = string.Empty;
            XmlNode xElement = null;
            XmlNodeList childrenNodeList = null;

            int matchingId = p_iCurrentObjectId;
            string matchingObjectName = string.Empty;
            string currentObjectName = "otherunitloss";

            string currentDbTableName = string.Empty;
            string currentDbIdField = string.Empty;
            //string currentPowerViewName = string.Empty;
            string currentExposeChildren = string.Empty;
            string childExposeChildren = string.Empty;
            bool childHasChildren = false;
            bool childIsOneToOne = false;

            
            DbReader objReader = null;
            ArrayList powerViewChildrenList = null;
            ArrayList filteredChildrenList = null;

            string childDbIdField = string.Empty;
            string childDbParentIdField = string.Empty;
            string childDbTableName = string.Empty;
            string childDbUserPromptField = string.Empty;
            string childDbClaimLOBField = string.Empty;
            string childDbJointFields = string.Empty;
            string childDbExtraField = string.Empty;
            
            string childParentName = string.Empty;
            string childRecordTitle = string.Empty;
            string childListTitle = string.Empty;
            string childPresentable = Boolean.TrueString;
            string[] childDetail = null;
            string[] currentFilteredChildArr = null;
            string[] splitFields = null;
            string userText = string.Empty;

            string LOB = string.Empty;
            bool added = false;
            bool addTheChild = false;

            int eventDisplayId = 0;
            int eventIndCode = 0;
            int i = 0;            
            string missingOrEmptyElements = string.Empty;

            SysSettings objSettings = null;
            
            try
            {
                xChildObject = m_ChildScreenDoc.SelectSingleNode("screen/otherunitloss");
                if (xChildObject != null)
                {
                    //populating required data from childscreen.xml file to string variables
                    childObjectName = "otherunitloss";

                    elementName = "dbTable";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        childDbTableName = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                    }

                    elementName = "idField";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        childDbIdField = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                    }

                    elementName = "parent";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        childParentName = xElement.InnerText;
                    }
                    else
                    {
                        missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                    }

                    elementName = "parentIdField";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        childDbParentIdField = MatchParentIdField(xElement.InnerText, childParentName, matchingObjectName, xChildObject.Name);
                    }
                    else
                    {
                        missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                    }

                    elementName = "recordTitle";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {                        
                        if (currentFilteredChildArr[1] != string.Empty)
                        {
                            childRecordTitle = currentFilteredChildArr[1];
                        }
                        else
                        {
                            childRecordTitle = xElement.InnerText;
                        }
                    }
                    else
                    {
                        missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                    }

                    elementName = "listTitle";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        //childListTitle = xElement.InnerText;
                        if (currentFilteredChildArr[1] != string.Empty)
                        {
                            childListTitle = currentFilteredChildArr[1];
                        }
                        else
                        {
                            childListTitle = xElement.InnerText;
                        }
                    }
                    else
                    {
                        missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                    }

                    elementName = "oneToOne";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        if (xElement.InnerText.ToUpper() == "YES")
                            childIsOneToOne = true;
                        else
                            childIsOneToOne = false;
                    }
                    else
                    {
                        missingOrEmptyElements += xChildObject.Name + "." + elementName + " ";
                    }

                    childHasChildren = false;                  
                    
                    elementName = "userPromptField";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        childDbUserPromptField = xElement.InnerText;
                    }                   

                    elementName = "jointFields";
                    xElement = xChildObject.SelectSingleNode(elementName);
                    if (xElement != null && xElement.InnerText != string.Empty)
                    {
                        childDbJointFields = xElement.InnerText;
                    }                   
                }
                else
                {
                    missingOrEmptyElements += currentObjectName;
                }

                if (!string.IsNullOrEmpty(missingOrEmptyElements))
                {
                    throw new ApplicationException(missingOrEmptyElements + " element(s) do(es) not exist or is/are empty in childScreen.xml!");
                }
                else
                {
                    objReader = GetDbQueryForNumberOfRecords(matchingId, currentDbIdField, currentDbTableName, childDbIdField, childDbTableName, childDbParentIdField, childObjectName, currentObjectName);
                }
                return iRecCount;
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
            finally
            {

            }
            */
            #endregion

            int iRecCount = 0;
            bool bSuccess = false;
            DbReader objReader = null;
            string sSQL = string.Empty;
            try
            {
                sSQL = "SELECT COUNT(ROW_ID) FROM CLAIM_X_OTHERUNIT WHERE CLAIM_ID = " + p_iCurrentObjectId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    iRecCount = Conversion.CastToType<int>(objReader[0].ToString(), out bSuccess);
                }
                return iRecCount;
            }
            catch (Exception ex)
            {
                return iRecCount;
                throw ex;
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }

        }
         
       //rupal:end
        #endregion


        /// <summary>
        /// Poiny Policy interface
        /// </summary>
        /// <author>skhare7</author>
        /// <param name="conditionNodeId"></param>
        /// <returns></returns>
        private string GetInsuredflag(int conditionNodeId,string tablename)
        {
            DbReader objRdr = null;
            string sQuery = string.Empty;
            int iFlagValue=0;
          
            string sPrefix = string.Empty;
            try
            {
                string tbname = "";
                string colname = "";
                string wherecolname = "ROW_ID";
                if (tablename.Contains("_X_"))
                {
                    if (tablename.Contains("UNIT_X_CLAIM"))
                    {
                        tbname = "UNIT_X_CLAIM";
                        colname = "ISINSURED";
                        wherecolname="UNIT_ROW_ID";
                    }
                    else if (tablename.Contains("CLAIM_X_PROPERTYLOSS"))
                    {
                        tbname = "CLAIM_X_PROPERTYLOSS";
                        colname = "INSURED";   
                    }
                    else if (tablename.Contains("CLAIM_X_SITELOSS"))
                    {
                        tbname = "CLAIM_X_SITELOSS";
                        colname = "ISINSURED";
                    }
                    else if (tablename.Contains("CLAIM_X_OTHERUNIT"))
                    {
                        tbname = "CLAIM_X_OTHERUNIT";
                        colname = "ISINSURED";
                    }

                }
                else
                {
                    if (tablename.Contains("unit"))
                    {
                        tbname = "UNIT_X_CLAIM";
                        colname = "ISINSURED";
                        wherecolname="UNIT_ROW_ID";
                    }
                    else if (tablename.Contains("propertyloss"))
                    {
                        tbname = "CLAIM_X_PROPERTYLOSS";
                        colname = "INSURED";
                    }
                    else if (tablename.Contains("siteloss"))
                    {
                        tbname = "CLAIM_X_SITELOSS";
                        colname = "ISINSURED";
                    }
                    else if (tablename.Contains("otherunitloss"))
                    {
                        tbname = "CLAIM_X_OTHERUNIT";
                        colname = "ISINSURED";
                    }
                }
                
                sQuery = "SELECT "+colname+" FROM "+tbname +" WHERE "+ wherecolname +" = "+conditionNodeId;

                objRdr = DbFactory.GetDbReader(m_sConnectionString, sQuery);
                if (objRdr.Read())
                {
                    iFlagValue = objRdr.GetInt(colname);
                }

                if (iFlagValue==-1)
                    sPrefix = "IN-";
               
                else if (iFlagValue == -2)
                    sPrefix = "NA-";//skhare7 Point Policy interface for orphan unit  should be displayed as NA in tree
                else 
                    sPrefix = "TP-";
                 return sPrefix;
            }

            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (objRdr != null)
                    objRdr.Dispose();


            }

        }
    }
}
