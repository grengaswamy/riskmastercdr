﻿using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.Security;
using Riskmaster.Settings;
using System.Globalization;
using System.Configuration;
using System.Linq;
//using System.Web;
using System.Xml.Linq;


namespace Riskmaster.Application.Reserves
{

    public class MoveFinancials
    {
        private struct FinancialKey
        {
            public int iOldRcRowId;
            public int iPolicyCvgRowId;
            public int iClaimId;
            public int iClaimantEid;
            public int iRsrvTypeCode;
            public int iLossType;

        }

        private struct ReserveData
        {
            public int iRcRowId;
            public int iOldClaimantEid;
            public int iOldRsvTypeCode;
            public int iOldLosstype;
            public int iOldCoverageRowId;
            public int iNewCoverageRowId;
            public int iNewClaimantEid;
            public int iNewRsvTypeCode;
            public int iNewPolCovSeq;
            public int iOldPolCovSeq;
            public int iNewLossCode;
            public int iOldCvgLossrowId;
            public int iNewCvgLossrowId;
            public bool bIsLssReserve;
        }
        private struct ActivityTrackData
        {
            public int iActivityRowId;
            public int iClaimId;
            public int iForeignTableId;
            public int iForeignTableKey;
            public int iUploadFlag;
            public int iCheckBatchId;
            public int iPolicySystemId;
            public int iIsUpdated;
            public int iActivityType;
            public int iAccountId;
            public double iRsvAmount;
            public int iRsvStatus;
            public double iChangeAmount;
            public int iCheckStatus;
            public int iVoidFlag;
            public int iIsCollection;
            public bool bFundsVoidFlag;
            public double iRsvcurrBalance;
            public bool bPendingUpload;
        }


        private int m_iClientId = 0;
        UserLogin m_objUserLogin = null;
        public MoveFinancials(UserLogin objUserLogin, int p_iClientId)
        {
            m_objUserLogin = objUserLogin;
            m_iClientId = p_iClientId;
        }


        public XmlDocument GetFinancialData(int iClaimID, int iSelectedPolCvgID, int iSelectedPolicyUnitRowId, int iSelectedPolicyId, int iClaimLob)
        {
            string sSQL = string.Empty;
            XmlDocument objXmlDoc = null;
            XmlDocument objXmlDocRL = null;
            XmlElement objRootElement = null;
            XmlElement objXmlElement = null;
            XmlElement objXmlEle = null;
            XmlNode objXmlReserve = null;
            XmlNode objXmlClaimant = null;
            XmlNode objXmlLoss = null;
            ReserveFunds objReserveFunds = null;
            string policyname = string.Empty;
            int policyid = 0;
            Policy objPolicy = null;
            DataModelFactory objDmf = null;
            string sDisablityTypesSQL = string.Empty;

            try
            {
                objXmlDoc = new XmlDocument();

                objDmf = new DataModelFactory(m_objUserLogin.objRiskmasterDatabase.DataSourceName, m_objUserLogin.LoginName, m_objUserLogin.Password, m_iClientId);

                objPolicy = (Policy)objDmf.GetDataModelObject("Policy", false);

                objRootElement = objXmlDoc.CreateElement("MoveFinancials");
                objXmlEle = objXmlDoc.CreateElement("Policylist");
                objXmlReserve = objXmlDoc.CreateElement("Reservelist");

                objXmlClaimant = objXmlDoc.CreateElement("ClaimantList");

                sSQL = "SELECT POLICY.POLICY_NAME,POLICY.POLICY_ID FROM CLAIM_X_POLICY,POLICY WHERE CLAIM_ID=" + iClaimID + " AND CLAIM_X_POLICY.POLICY_ID= POLICY.POLICY_ID ";

                objPolicy.MoveTo(iSelectedPolicyId);

                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {
                    while (objRdr.Read())
                    {
                        objXmlElement = objXmlDoc.CreateElement("Data");
                        objXmlEle.AppendChild(objXmlElement);
                        objXmlElement.SetAttribute("PolicyName", Conversion.ConvertObjToStr(objRdr.GetValue("POLICY_NAME")));
                        policyid = Convert.ToInt32(objRdr.GetValue("POLICY_ID"));
                        objXmlElement.SetAttribute("PolicyID", policyid.ToString());
                    }
                }
                objRootElement.AppendChild(objXmlEle);
                objXmlElement = null;

                objReserveFunds = new ReserveFunds(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_objUserLogin, m_iClientId);


                objXmlDocRL = new XmlDocument();
                objXmlDocRL.LoadXml(System.Net.WebUtility.HtmlDecode(objReserveFunds.GetLookupXml(iClaimID, m_iClientId).SelectSingleNode("//ReserveValues").InnerXml));

                XmlNodeList xnList = objXmlDocRL.SelectNodes(string.Concat("//BOB/policy[@value='", iSelectedPolicyId, "']/unit[@value='", iSelectedPolicyUnitRowId, "']/coverage"));

                foreach (XmlNode xnode in xnList)
                {
                    if (Convert.ToInt32(xnode.Attributes["value"].Value.Split('#')[0]) == iSelectedPolCvgID)
                    {
                        XmlNodeList Reservelist = xnode.SelectNodes("reserve");
                        foreach (XmlNode reservenode in Reservelist)
                        {
                            XmlNode importNode = objXmlReserve.OwnerDocument.ImportNode(reservenode, true);
                            objXmlReserve.AppendChild(importNode);
                        }


                        if (iClaimLob != 243)
                        {
                            objXmlLoss = objXmlDoc.CreateElement("Losslist");

                            XmlNodeList Losslist = xnode.SelectNodes("loss");
                            foreach (XmlNode lossnode in Losslist)
                            {
                                lossnode.InnerXml = "";
                                XmlNode importNode = objXmlLoss.OwnerDocument.ImportNode(lossnode, true);
                                objXmlLoss.AppendChild(importNode);
                            }
                            objRootElement.AppendChild(objXmlLoss);
                        }


                        objRootElement.AppendChild(objXmlReserve);


                    }
                }

                if (iClaimLob == 243)
                {
                    sDisablityTypesSQL = " SELECT DISTINCT PXCT.COVERAGE_TYPE_CODE,CLLM.LOSS_CODE FROM CVG_LOSS_LOB_MAPPING CLLM INNER JOIN POLICY_X_CVG_TYPE PXCT ON PXCT.COVERAGE_TYPE_CODE = CLLM.CVG_TYPE_CODE";
                    sDisablityTypesSQL = sDisablityTypesSQL + " WHERE POLICY_LOB=" + objPolicy.PolicyLOB + "  AND POLCVG_ROW_ID=" + iSelectedPolCvgID + "  AND CLLM.POLICY_SYSTEM_ID=" + objPolicy.PolicySystemId;


                    objXmlLoss = objXmlDoc.CreateElement("DisabiltyList");

                    using (DbReader objRdr = DbFactory.ExecuteReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sDisablityTypesSQL))
                    {
                        while (objRdr.Read())
                        {
                            objXmlElement = objXmlDoc.CreateElement("disability");
                            objXmlLoss.AppendChild(objXmlElement);
                            objXmlElement.SetAttribute("name", System.Net.WebUtility.HtmlEncode(objDmf.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objRdr.GetValue("LOSS_CODE"), m_iClientId), m_objUserLogin.objUser.NlsCode)));
                            objXmlElement.SetAttribute("value", objRdr.GetValue("LOSS_CODE").ToString());
                        }
                    }
                    objRootElement.AppendChild(objXmlLoss);
                }
                sSQL = @"SELECT ENTITY.LAST_NAME, ENTITY.ENTITY_ID,ENTITY.FIRST_NAME FROM CLAIMANT INNER JOIN CLAIM ON CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID INNER JOIN ENTITY  ON ENTITY.ENTITY_ID = CLAIMANT.CLAIMANT_EID  WHERE CLAIM.CLAIM_ID = '" + iClaimID + "'";
                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {
                    while (objRdr.Read())
                    {
                        objXmlElement = objXmlDoc.CreateElement("claimant");
                        objXmlClaimant.AppendChild(objXmlElement);
                        objXmlElement.SetAttribute("firstname", Conversion.ConvertObjToStr(objRdr.GetValue("FIRST_NAME")));
                        objXmlElement.SetAttribute("lastname", Conversion.ConvertObjToStr(objRdr.GetValue("LAST_NAME")));
                        objXmlElement.SetAttribute("value", Conversion.ConvertObjToStr(objRdr.GetValue("ENTITY_ID")));

                    }
                    objRootElement.AppendChild(objXmlClaimant);
                }


                objXmlDoc.AppendChild(objRootElement);

            }
            catch (Exception e)
            {
                throw new RMAppException(Globalization.GetString("MoveFinancials.GetData", m_iClientId), e);
            }
            finally
            {
                objXmlDocRL = null;
                objRootElement = null;
                objXmlElement = null;
                objXmlEle = null;
                objXmlReserve = null;
                objXmlClaimant = null;
                objXmlLoss = null;
                objReserveFunds = null;
                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                    objPolicy = null;
                }
                if (objDmf != null)
                {
                    objDmf.Dispose();
                    objDmf = null;
                }

            }

            return objXmlDoc;
        }
        private int GetCoverageXLoss(int iCvgRowId, int iLossCode)
        {
            int iCvgLossRowId = 0;
            string sSQL = string.Empty;
            LocalCache objCache = null;
            DataModelFactory objDmf = null;
            CvgXLoss objCvgLoss = null;
            try
            {
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                sSQL = "SELECT CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE POLCVG_ROW_ID=" + iCvgRowId + " AND LOSS_CODE=" + iLossCode + " AND DISABILITY_CAT=" + objCache.GetRelatedCodeId(iLossCode);
                iCvgLossRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL), m_iClientId);

                if (iCvgLossRowId == 0)
                {
                    objDmf = new DataModelFactory(m_objUserLogin.objRiskmasterDatabase.DataSourceName, m_objUserLogin.LoginName, m_objUserLogin.Password, m_iClientId);

                    objCvgLoss = (CvgXLoss)objDmf.GetDataModelObject("CvgXLoss", false);

                    objCvgLoss.PolCvgRowId = iCvgRowId;
                    objCvgLoss.LossCode = iLossCode;
                    objCvgLoss.DisablityCategory = objCache.GetRelatedCodeId(iLossCode);
                    objCvgLoss.Save();

                    iCvgLossRowId = objCvgLoss.CvgLossRowId;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCvgLoss != null)
                {
                    objCvgLoss.Dispose();
                    objCvgLoss = null;
                }
                if (objDmf != null)
                {
                    objDmf.Dispose();
                    objDmf = null;
                }


            }
            return iCvgLossRowId;
        }

        private bool IsDeductiblApplied(int iClaimId, int iPolCvgRowId)
        {
            int iDedtype = 0;
            LocalCache objCache= null;
            try
            {
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);


                iDedtype = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT DED_TYPE_CODE FROM CLAIM_X_POL_DED WHERE CLAIM_ID=" + iClaimId + " AND POLCVG_ROW_ID=" + iPolCvgRowId), m_iClientId);

                if (iDedtype > 0)
                {
                    
                        return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }
            return false;
        }

        /// <summary>
        /// To match deductible amount and deductible type applicable on source and traget coverage
        /// Move will be processed if Source coverage deductible amount is less than or equal to target deductible amount and deductible type is same in source and target deductibles.
        /// </summary>
        /// <param name="iClaimId"></param>
        /// <param name="iPolCvgRowId"></param>
        /// <returns></returns>
        private bool GetCovDeductibleMapping(int iClaimId, int iSourceCovId, int iTargetCovId)
        {
            double dSourceDedAmount = 0;
            double dTargetDedAmount = 0;
            int iSourceDedType = 0;
            int iTargetDedType = 0;
            bool bReturnValue = false;
            LocalCache objCache = null;
            try
            {
                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT SIR_DED_AMT,DED_TYPE_CODE FROM CLAIM_X_POL_DED WHERE CLAIM_ID=" + iClaimId + " AND POLCVG_ROW_ID=" + iSourceCovId))
                {
                    if (objRdr.Read())
                    {
                        dSourceDedAmount = Conversion.ConvertObjToDouble(objRdr.GetValue("SIR_DED_AMT"), m_iClientId);
                        iSourceDedType = Conversion.ConvertObjToInt(objRdr.GetValue("DED_TYPE_CODE"), m_iClientId);
                    }
                }

                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT SIR_DED_AMT,DED_TYPE_CODE FROM CLAIM_X_POL_DED WHERE CLAIM_ID=" + iClaimId + " AND POLCVG_ROW_ID=" + iTargetCovId))
                {
                    if (objRdr.Read())
                    {
                        dTargetDedAmount = Conversion.ConvertObjToDouble(objRdr.GetValue("SIR_DED_AMT"), m_iClientId);
                        iTargetDedType = Conversion.ConvertObjToInt(objRdr.GetValue("DED_TYPE_CODE"), m_iClientId);
                    }
                }
                objCache= new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString,m_iClientId);

                if (iSourceDedType == objCache.GetCodeId("ND", "DEDUCTIBLE_TYPE")
                    ||  
                       (iTargetDedType == objCache.GetCodeId("ND", "DEDUCTIBLE_TYPE"))
                       )
                {
                    throw new RMAppException(Globalization.GetString("MoveFinancials.NDDeductibleApplied", m_iClientId));
                }
                else
                {
                    if (
                        (iSourceDedType == objCache.GetCodeId("None", "DEDUCTIBLE_TYPE"))

                        &&
                       (iTargetDedType == objCache.GetCodeId("None", "DEDUCTIBLE_TYPE"))
                       )
                    {
                        bReturnValue = true;
                    }
                    else
                    {

                        if ((dSourceDedAmount <= dTargetDedAmount) && (iSourceDedType == iTargetDedType))
                        {
                            bReturnValue = true;
                        }
                        else
                        {
                            bReturnValue = false;
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if(objCache!=null)
                {
                    objCache.Dispose();
                }
            }
            return bReturnValue;
        }

        /// <summary>
        /// Get Reserve data for reserves created on source and target policy coverage id
        /// </summary>
        /// <param name="iClaimId"></param>
        /// <param name="iSourceCoverageId"></param>
        /// <param name="iTargetCoverageId"></param>
        /// <param name="objRsvList"></param>
        /// <param name="objSplitList"></param>
        /// <param name="objActivityTrackList"></param>
        /// <param name="objRsrvHistoryList"></param>
        /// <param name="iOldPolicySystemId"></param>
        private void GetRecordIds(int iClaimId, int iSourceCoverageId, int iTargetCoverageId, Dictionary<int, ReserveData> objRsvList, Dictionary<int, string> objSplitList, Dictionary<int, SortedList<int, ActivityTrackData>> objActivityTrackList, Dictionary<int, string> objRsrvHistoryList, int iOldPolicySystemId,int iNewPolicySystemId)
        {
            ReserveData objRsvData;
            LocalCache objCache = null;
            DataSet objDataSet = null;
            DataTable objSourceTable = null;
            DataTable objtargetTable = null;
            FinancialKey objFinancialKey;
            string sReserveTransIds = string.Empty;
            try
            {
                if (!GetCovDeductibleMapping(iClaimId, iSourceCoverageId, iTargetCoverageId))
                {
                    throw new RMAppException(Globalization.GetString("MoveFinancials.DeductibleMisMatch", m_iClientId));
                }



                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);

                objDataSet = DbFactory.GetDataSet(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT RESERVE_CURRENT.*,COVERAGE_X_LOSS.POLCVG_ROW_ID CVG_ID,CVG_LOSS_ROW_ID,COVERAGE_X_LOSS.LOSS_CODE FROM RESERVE_CURRENT,COVERAGE_X_LOSS WHERE CLAIM_ID=" + iClaimId + " AND COVERAGE_X_LOSS.POLCVG_ROW_ID IN (" + iSourceCoverageId + "," + iTargetCoverageId + ") AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID", m_iClientId);
                objDataSet.Tables[0].DefaultView.RowFilter = " CVG_ID= " + iSourceCoverageId;

                objSourceTable = objDataSet.Tables[0].DefaultView.ToTable();

                objDataSet.Tables[0].DefaultView.RowFilter = " CVG_ID= " + iTargetCoverageId;
                objtargetTable = objDataSet.Tables[0].DefaultView.ToTable();

                foreach (DataRow objSourceRow in objSourceTable.Rows)
                {
                    
                    if (objCache.GetRelatedCodeId(Conversion.ConvertObjToInt(objSourceRow["RES_STATUS_CODE"], m_iClientId)) == objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"))
                    {
                        throw new RMAppException(Globalization.GetString("MoveFinancials.ReserveOnhold", m_iClientId));
                    }
                    if (!GetCvgLossCodeMapping(iClaimId, Conversion.ConvertObjToInt(objSourceRow["LOSS_CODE"], m_iClientId), iTargetCoverageId,iNewPolicySystemId))
                    {
                        throw new RMAppException(Globalization.GetString("MoveFinancials.CvgLossMapping", m_iClientId));

                    }

                    objFinancialKey = new FinancialKey();
                    objFinancialKey.iOldRcRowId = Conversion.ConvertObjToInt(objSourceRow["RC_ROW_ID"], m_iClientId);
                    objFinancialKey.iClaimantEid = Conversion.ConvertObjToInt(objSourceRow["CLAIMANT_EID"], m_iClientId);
                    objFinancialKey.iRsrvTypeCode = Conversion.ConvertObjToInt(objSourceRow["RESERVE_TYPE_CODE"], m_iClientId);
                    objFinancialKey.iPolicyCvgRowId = iTargetCoverageId;
                    objFinancialKey.iLossType = Conversion.ConvertObjToInt(objSourceRow["LOSS_CODE"], m_iClientId);
                    if (!GetDuplicateFinancialKey(objtargetTable, objFinancialKey))
                    {
                        throw new RMAppException(Globalization.GetString("MoveFinancials.DuplicatReserve", m_iClientId));

                    }
                    else
                    {
                        objRsvData = new ReserveData();


                        objRsvData.iRcRowId = Conversion.ConvertObjToInt(objSourceRow["RC_ROW_ID"], m_iClientId);

                        objRsvData.iOldPolCovSeq = Conversion.ConvertObjToInt(objSourceRow["POLICY_CVG_SEQNO"], m_iClientId);

                        objRsvData.iNewClaimantEid = Conversion.ConvertObjToInt(objSourceRow["CLAIMANT_EID"], m_iClientId);
                        objRsvData.iNewCoverageRowId = iTargetCoverageId;
                        objRsvData.iOldClaimantEid = Conversion.ConvertObjToInt(objSourceRow["CLAIMANT_EID"], m_iClientId);
                        objRsvData.iOldCoverageRowId = iSourceCoverageId;
                        objRsvData.iNewRsvTypeCode = Conversion.ConvertObjToInt(objSourceRow["RESERVE_TYPE_CODE"], m_iClientId);
                        objRsvData.iOldLosstype = Conversion.ConvertObjToInt(objSourceRow["LOSS_CODE"], m_iClientId);
                        objRsvData.iOldRsvTypeCode = Conversion.ConvertObjToInt(objSourceRow["RESERVE_TYPE_CODE"], m_iClientId);
                        objRsvData.iNewLossCode = Conversion.ConvertObjToInt(objSourceRow["LOSS_CODE"], m_iClientId);
                        objRsvData.iOldCvgLossrowId = Conversion.ConvertObjToInt(objSourceRow["CVG_LOSS_ROW_ID"], m_iClientId);
                        objRsvData.bIsLssReserve = Conversion.ConvertObjToBool(objSourceRow["LSS_RES_EXP_FLAG"], m_iClientId);

                        //if (!GetDeductibleReserveMapping(objRsvData.iOldCoverageRowId, objRsvData.iNewCoverageRowId, iClaimId, objRsvData.iNewRsvTypeCode, objRsvData.iOldRsvTypeCode, objCache.GetRelatedCodeId(objRsvData.iOldRsvTypeCode), objCache.GetRelatedCodeId(objRsvData.iNewRsvTypeCode)))
                        //{
                        //    throw new RMAppException(Globalization.GetString("MoveFinancials.ReserveDeductibleExclusion", m_iClientId));
                        //}

                        objRsvData.iNewCvgLossrowId = GetCoverageXLoss(iTargetCoverageId, Conversion.ConvertObjToInt(objSourceRow["LOSS_CODE"], m_iClientId));
                        objRsvData.iNewPolCovSeq = CommonFunctions.GetPolCovSeq(iClaimId, Conversion.ConvertObjToInt(objSourceRow["CLAIMANT_EID"], m_iClientId), objRsvData.iNewCvgLossrowId, m_objUserLogin.objRiskmasterDatabase.ConnectionString);

                        objRsvList.Add(Conversion.ConvertObjToInt(objSourceRow["RC_ROW_ID"], m_iClientId), objRsvData);


                        sReserveTransIds = GetTransactionIds(objSplitList, Conversion.ConvertObjToInt(objSourceRow["RC_ROW_ID"], m_iClientId), objCache.GetCodeId("H", "CHECK_STATUS"));
                        GetActivityrowIds(objActivityTrackList, sReserveTransIds, Conversion.ConvertObjToInt(objSourceRow["RC_ROW_ID"], m_iClientId), objCache.GetTableId("FUNDS"), objCache.GetTableId("RESERVE_CURRENT"), iOldPolicySystemId);
                        GetReserveHistoryIds(objRsrvHistoryList, Conversion.ConvertObjToInt(objSourceRow["RC_ROW_ID"], m_iClientId));

                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }

        }
        /// <summary>
        /// Get funds and split ids for any reserve
        /// </summary>
        /// <param name="objSplitList"></param>
        /// <param name="iRcRowId"></param>
        /// <param name="iFundsHoldStatus"></param>
        /// <returns></returns>
        private string GetTransactionIds(Dictionary<int, string> objSplitList, int iRcRowId, int iFundsHoldStatus)
        {
            string sSplitRowIds = string.Empty;
            string sReserveTransIds = string.Empty;
            try
            {

                using (DbReader objRdr1 = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT FUNDS.TRANS_ID,SPLIT_ROW_ID,FUNDS.STATUS_CODE FROM FUNDS_TRANS_SPLIT,FUNDS WHERE RC_ROW_ID =" + iRcRowId + " AND FUNDS.TRANS_ID= FUNDS_TRANS_SPLIT.TRANS_ID"))
                {
                    while (objRdr1.Read())
                    {
                        if (Conversion.ConvertObjToInt(objRdr1.GetValue(2), m_iClientId) == iFundsHoldStatus)
                        {
                            throw new RMAppException(Globalization.GetString("MoveFinancials.FundsOnhold", m_iClientId));
                        }


                        if (string.IsNullOrEmpty(sSplitRowIds))
                        {
                            sSplitRowIds = Conversion.ConvertObjToStr(objRdr1.GetValue(0)) + "|" + Conversion.ConvertObjToStr(objRdr1.GetValue(1));
                        }
                        else
                        {
                            sSplitRowIds = sSplitRowIds + "," + Conversion.ConvertObjToStr(objRdr1.GetValue(0)) + "|" + Conversion.ConvertObjToStr(objRdr1.GetValue(1));
                        }

                        if (string.IsNullOrEmpty(sReserveTransIds))
                        {
                            sReserveTransIds = Conversion.ConvertObjToStr(objRdr1.GetValue(0));
                        }
                        else
                        {
                            sReserveTransIds = sReserveTransIds + "," + Conversion.ConvertObjToStr(objRdr1.GetValue(0));
                        }
                    }



                }

                if (!string.IsNullOrEmpty(sSplitRowIds))
                {
                    objSplitList.Add(iRcRowId, sSplitRowIds);
                }

            }

            catch (Exception e)
            {
                throw e;
            }

            return sReserveTransIds;
        }
        /// <summary>
        /// Get reserve history associated to any reserve
        /// </summary>
        /// <param name="objRsrvHistoryList"></param>
        /// <param name="iRcRowId"></param>
        private void GetReserveHistoryIds(Dictionary<int, string> objRsrvHistoryList, int iRcRowId)
        {
            string sRsvHistIds = string.Empty;
            try
            {
                using (DbReader objRdr3 = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT RESERVE_HISTORY.RSV_ROW_ID FROM RESERVE_HISTORY,RESERVE_CURRENT WHERE RC_ROW_ID= " + iRcRowId + " AND RESERVE_HISTORY.CLAIM_ID= RESERVE_CURRENT.CLAIM_ID AND RESERVE_HISTORY.CLAIMANT_EID= RESERVE_CURRENT.CLAIMANT_EID AND RESERVE_HISTORY.RESERVE_TYPE_CODE =RESERVE_CURRENT.RESERVE_TYPE_CODE AND RESERVE_HISTORY.POLCVG_LOSS_ROW_ID=RESERVE_CURRENT.POLCVG_LOSS_ROW_ID"))
                {
                    while (objRdr3.Read())
                    {
                        if (string.IsNullOrEmpty(sRsvHistIds))
                        {
                            sRsvHistIds = Conversion.ConvertObjToStr(objRdr3.GetValue(0));
                        }
                        else
                        {
                            sRsvHistIds = sRsvHistIds + "," + Conversion.ConvertObjToStr(objRdr3.GetValue(0));
                        }

                    }

                    objRsrvHistoryList.Add(iRcRowId, sRsvHistIds);

                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Get activities tracked in ACTIVITY_TRACK table for reserve and payments made on the reserve
        /// </summary>
        /// <param name="objActivityTrackList"></param>
        /// <param name="sReserveTransIds"></param>
        /// <param name="iRcRowId"></param>
        /// <param name="iFundsTableId"></param>
        /// <param name="iRcTableId"></param>
        /// <param name="iOldPolicySystemId"></param>
        private void GetActivityrowIds(Dictionary<int, SortedList<int, ActivityTrackData>> objActivityTrackList, string sReserveTransIds, int iRcRowId, int iFundsTableId, int iRcTableId, int iOldPolicySystemId)
        {
            SortedList<int, ActivityTrackData> objActivityData = null;
            ActivityTrackData objActivityTrackData;
            StringBuilder strSQL = null;
            Dictionary<string, object> dictParams = null;
            try
            {
                objActivityData = new SortedList<int, ActivityTrackData>();
                // PSHARMA206
                string sSQL = "SELECT * FROM ACTIVITY_TRACK WHERE (( FOREIGN_TABLE_ID = " + iRcTableId + " AND FOREIGN_TABLE_KEY =  " + iRcRowId + ") ";

                if (!string.IsNullOrEmpty(sReserveTransIds))
                {
                    sSQL = sSQL + "   OR ( FOREIGN_TABLE_ID =" + iFundsTableId + " AND FOREIGN_TABLE_KEY IN(" + sReserveTransIds + ")) ";
                }

                if (m_objUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_SQLSRVR)
                {

                    sSQL = sSQL + ") AND MOVE_HIST_TABLE_ID=0 AND MOVE_HIST_TABLE_KEY=0 AND ISNULL(ADDITIONAL_UPL_DATA, '') = ''";
                }
                else if (m_objUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_ORACLE)
                {

                    sSQL = sSQL + ") AND MOVE_HIST_TABLE_ID=0 AND MOVE_HIST_TABLE_KEY=0 AND NVL(ADDITIONAL_UPL_DATA, '') = ''";
                }

               
                sSQL = sSQL + "  ORDER BY ACTIVITY_ROW_ID";

                using (DbReader objrdr2 = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL))
                {

                    while (objrdr2.Read())
                    {
                        objActivityTrackData = new ActivityTrackData();
                        objActivityTrackData.iAccountId = Conversion.ConvertObjToInt(objrdr2.GetValue("ACCOUNT_ID"), m_iClientId);
                        objActivityTrackData.iActivityRowId = Conversion.ConvertObjToInt(objrdr2.GetValue("ACTIVITY_ROW_ID"), m_iClientId);
                        objActivityTrackData.iActivityType = Conversion.ConvertObjToInt(objrdr2.GetValue("ACTIVITY_TYPE"), m_iClientId);
                        objActivityTrackData.iChangeAmount = Conversion.ConvertObjToInt(objrdr2.GetValue("CHANGE_AMOUNT"), m_iClientId);
                        objActivityTrackData.iCheckBatchId = Conversion.ConvertObjToInt(objrdr2.GetValue("CHECK_BATCH_ID"), m_iClientId);
                        objActivityTrackData.iCheckStatus = Conversion.ConvertObjToInt(objrdr2.GetValue("CHECK_STATUS"), m_iClientId);
                        objActivityTrackData.iClaimId = Conversion.ConvertObjToInt(objrdr2.GetValue("CLAIM_ID"), m_iClientId);
                        objActivityTrackData.iForeignTableId = Conversion.ConvertObjToInt(objrdr2.GetValue("FOREIGN_TABLE_ID"), m_iClientId);
                        objActivityTrackData.iForeignTableKey = Conversion.ConvertObjToInt(objrdr2.GetValue("FOREIGN_TABLE_KEY"), m_iClientId);
                        objActivityTrackData.iIsCollection = Conversion.ConvertObjToInt(objrdr2.GetValue("IS_COLLECTION"), m_iClientId);
                        objActivityTrackData.iIsUpdated = Conversion.ConvertObjToInt(objrdr2.GetValue("IS_UPDATED"), m_iClientId);
                        objActivityTrackData.iPolicySystemId = Conversion.ConvertObjToInt(objrdr2.GetValue("POLICY_SYSTEM_ID"), m_iClientId);
                        objActivityTrackData.iRsvAmount = Conversion.ConvertObjToInt(objrdr2.GetValue("RESERVE_AMOUNT"), m_iClientId);
                        objActivityTrackData.iRsvStatus = Conversion.ConvertObjToInt(objrdr2.GetValue("RESERVE_STATUS"), m_iClientId);
                        objActivityTrackData.iUploadFlag = Conversion.ConvertObjToInt(objrdr2.GetValue("UPLOAD_FLAG"), m_iClientId);
                        objActivityTrackData.iVoidFlag = Conversion.ConvertObjToInt(objrdr2.GetValue("VOID_FLAG"), m_iClientId);
                        if (!Conversion.ConvertObjToBool(objrdr2.GetValue("UPLOAD_FLAG"), m_iClientId))
                        {
                            if (objActivityTrackData.iForeignTableId == iFundsTableId)
                            {
                                objActivityTrackData.bFundsVoidFlag = Conversion.ConvertObjToBool(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "    SELECT VOID_FLAG FROM FUNDS WHERE TRANS_ID=" + objActivityTrackData.iForeignTableKey), m_iClientId);

                                string sSelectSQL = "SELECT COUNT(1) FROM ACTIVITY_TRACK WHERE  CLAIM_ID=" + objActivityTrackData.iClaimId + " AND FOREIGN_TABLE_ID =" + objActivityTrackData.iForeignTableId + " AND FOREIGN_TABLE_KEY = " + objActivityTrackData.iForeignTableKey + " AND VOID_FLAG = " + objActivityTrackData.iVoidFlag + " AND UPLOAD_FLAG = -1 AND CHECK_STATUS = " + objActivityTrackData.iCheckStatus;

                                if (Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSelectSQL), m_iClientId) > 0)
                                {
                                    objActivityTrackData.bPendingUpload = true;
                                }


                            }
                            else if (objActivityTrackData.iForeignTableId == iRcTableId)
                            {
                                objActivityTrackData.iRsvcurrBalance = Conversion.ConvertObjToDouble(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "    SELECT BALANCE_AMOUNT FROM RESERVE_CURRENT WHERE RC_ROW_ID=" + objActivityTrackData.iForeignTableKey), m_iClientId);

                                dictParams = new Dictionary<string, object>();
                                strSQL = new StringBuilder();

                                strSQL.AppendFormat("SELECT COUNT(1) FROM ACTIVITY_TRACK WHERE CLAIM_ID= {0} AND FOREIGN_TABLE_ID ={1}  AND FOREIGN_TABLE_KEY = {2} AND RESERVE_AMOUNT={3} AND RESERVE_STATUS={4} AND CHANGE_AMOUNT={5} ", "~CLAIM_ID~", "~FOREIGN_TABLE_ID~", "~FOREIGN_TABLE_KEY~", "~RESERVE_AMOUNT~", "~RESERVE_STATUS~", "~CHANGE_AMOUNT~");
                                strSQL.AppendFormat("AND UPLOAD_FLAG = -1 ");

                                dictParams.Add("CLAIM_ID", objActivityTrackData.iClaimId);
                                dictParams.Add("FOREIGN_TABLE_ID", objActivityTrackData.iForeignTableId);
                                dictParams.Add("FOREIGN_TABLE_KEY", objActivityTrackData.iForeignTableKey);
                                dictParams.Add("RESERVE_AMOUNT", objActivityTrackData.iRsvAmount);
                                dictParams.Add("RESERVE_STATUS", objActivityTrackData.iRsvStatus);
                                dictParams.Add("CHANGE_AMOUNT", objActivityTrackData.iChangeAmount);


                                if (Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, strSQL.ToString(), dictParams), m_iClientId) > 0)
                                {
                                    objActivityTrackData.bPendingUpload = true;
                                }

                            }


                        }


                        objActivityData.Add(Conversion.ConvertObjToInt(objrdr2.GetValue("ACTIVITY_ROW_ID"), m_iClientId), objActivityTrackData);
                    }
                }
                objActivityTrackList.Add(iRcRowId, objActivityData);
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        //private bool GetDeductibleReserveMapping(int iSourceCoverageId,int iTargetCoverageId, int iClaimId, int iTargetRsvType,int iSourceReserveType,int iSourceParentRsvType,int iTargetParentRsvType)
        //{
        //    int iTargetExclusionId = 0;
        //    int iSourceExclusionId = 0;
        //    try
        //    {
        //        if ((iSourceCoverageId != iTargetCoverageId)|| (iTargetRsvType != iSourceReserveType))
        //        {
        //            iTargetExclusionId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT CLAIM_X_POL_DED_EXCLUSION.CLM_X_POL_EXCL_ID FROM CLAIM_X_POL_DED,CLAIM_X_POL_DED_EXCLUSION WHERE CLAIM_X_POL_DED.CLAIM_ID=" + iClaimId + " AND CLAIM_X_POL_DED.POLCVG_ROW_ID= " + iTargetCoverageId + " AND  CLAIM_X_POL_DED.CLM_X_POL_DED_ID= CLAIM_X_POL_DED_EXCLUSION.CLM_POL_DED_ID AND ((CLAIM_X_POL_DED.IS_PARENT=0 AND CLAIM_X_POL_DED_EXCLUSION.RESERVE_TYPE_CODE=" + iTargetRsvType + " ) OR (CLAIM_X_POL_DED.IS_PARENT=-1 AND CLAIM_X_POL_DED_EXCLUSION.RESERVE_TYPE_CODE=" + iTargetParentRsvType + " ))"),m_iClientId);
        //            iSourceExclusionId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT CLAIM_X_POL_DED_EXCLUSION.CLM_X_POL_EXCL_ID FROM CLAIM_X_POL_DED,CLAIM_X_POL_DED_EXCLUSION WHERE CLAIM_X_POL_DED.CLAIM_ID=" + iClaimId + " AND CLAIM_X_POL_DED.POLCVG_ROW_ID= " + iSourceCoverageId + " AND  CLAIM_X_POL_DED.CLM_X_POL_DED_ID= CLAIM_X_POL_DED_EXCLUSION.CLM_POL_DED_ID AND ((CLAIM_X_POL_DED.IS_PARENT=0 AND CLAIM_X_POL_DED_EXCLUSION.RESERVE_TYPE_CODE=" + iSourceReserveType + " ) OR (CLAIM_X_POL_DED.IS_PARENT=-1 AND CLAIM_X_POL_DED_EXCLUSION.RESERVE_TYPE_CODE=" + iSourceParentRsvType + " ))"),m_iClientId);

        //            if (
        //            (iTargetExclusionId == 0 && iSourceExclusionId == 0)
        //            || (iTargetExclusionId > 0 && iSourceExclusionId > 0)
        //            )
        //            {
        //                return true;
        //            }
        //        }
        //        else
        //        {
        //            return true;
        //        }

        //        return false;
                
        //        //{
        //        //    return true;
        //        //}
        //        //else
        //        //{
        //        //    return false;
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }
           
        //}
        /// <summary>
        /// Get reserve data for any reserve RC_ROW_ID
        /// </summary>
        /// <param name="iClaimId"></param>
        /// <param name="iClaimantEid"></param>
        /// <param name="iLosstype"></param>
        /// <param name="iRsvtype"></param>
        /// <param name="iRcRowId"></param>
        /// <param name="objRsvList"></param>
        /// <param name="objSplitList"></param>
        /// <param name="objActivityTrackList"></param>
        /// <param name="objRsrvHistoryList"></param>
        /// <param name="iOldPolicySystemId"></param>
        private void GetReserveData(int iClaimId, int iClaimantEid, int iLosstype, int iRsvtype, int iRcRowId, Dictionary<int, ReserveData> objRsvList, Dictionary<int, string> objSplitList, Dictionary<int, SortedList<int, ActivityTrackData>> objActivityTrackList, Dictionary<int, string> objRsrvHistoryList, int iOldPolicySystemId)
        {

            ReserveData objRsvData;
            LocalCache objCache = null;
            FinancialKey objFinancialKey;
            string sReserveTransIds = string.Empty;
            try
            {
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);

                using (DbReader objRdr = DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT RESERVE_CURRENT.*,COVERAGE_X_LOSS.POLCVG_ROW_ID CVG_ID,CVG_LOSS_ROW_ID,COVERAGE_X_LOSS.LOSS_CODE FROM RESERVE_CURRENT,COVERAGE_X_LOSS WHERE RC_ROW_ID=" + iRcRowId + "  AND RESERVE_CURRENT.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID"))
                {
                    if (objRdr.Read())
                    {
                        if (IsDeductiblApplied(iClaimId, Conversion.ConvertObjToInt(objRdr.GetValue("POLCVG_ROW_ID"), m_iClientId)))
                        {
                            throw new RMAppException(Globalization.GetString("MoveFinancials.DeductibleApplied", m_iClientId));
                        }
                        if (objCache.GetRelatedCodeId(Conversion.ConvertObjToInt(objRdr.GetValue("RES_STATUS_CODE"), m_iClientId)) == objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"))
                        {
                            throw new RMAppException(Globalization.GetString("MoveFinancials.ReserveOnhold", m_iClientId));

                        }
                        objFinancialKey = new FinancialKey();
                       // objFinancialKey.iRsrvTypeCode = iRsvtype;
                            objFinancialKey.iRsrvTypeCode = Conversion.ConvertObjToInt(objRdr.GetValue("RESERVE_TYPE_CODE"), m_iClientId); 
                        objFinancialKey.iClaimantEid = iClaimantEid;
                        objFinancialKey.iClaimId = iClaimId;
                        objFinancialKey.iLossType = iLosstype;
                        objFinancialKey.iPolicyCvgRowId = Conversion.ConvertObjToInt(objRdr.GetValue("CVG_ID"), m_iClientId);

                       
                        //if (GetCovDeductibleAmount(iClaimId, Conversion.ConvertObjToInt(objRdr.GetValue("CVG_ID"), m_iClientId)) > 0)
                        //{
                        //    throw new RMAppException(Globalization.GetString("MoveFinancials.DeductibleApplied", m_iClientId));
                        //}

                        if (!GetCvgLossCodeMapping(iClaimId, iLosstype, Conversion.ConvertObjToInt(objRdr.GetValue("CVG_ID"), m_iClientId), iOldPolicySystemId))
                        {
                            throw new RMAppException(Globalization.GetString("MoveFinancials.CvgLossMapping", m_iClientId));

                        }

                        if (!GetDuplicateFinancialKey(null, objFinancialKey))
                        {
                            throw new RMAppException(Globalization.GetString("MoveFinancials.DuplicatReserve", m_iClientId));
                        }
                        
                        else
                        {

                            objRsvData = new ReserveData();


                            objRsvData.iRcRowId = Conversion.ConvertObjToInt(objRdr.GetValue("RC_ROW_ID"), m_iClientId);

                            objRsvData.iOldPolCovSeq = Conversion.ConvertObjToInt(objRdr.GetValue("POLICY_CVG_SEQNO"), m_iClientId);

                            objRsvData.iNewClaimantEid = iClaimantEid;
                            objRsvData.iNewCoverageRowId = Conversion.ConvertObjToInt(objRdr.GetValue("POLCVG_ROW_ID"), m_iClientId);
                            objRsvData.iOldClaimantEid = Conversion.ConvertObjToInt(objRdr.GetValue("CLAIMANT_EID"), m_iClientId);
                            objRsvData.iOldCoverageRowId = Conversion.ConvertObjToInt(objRdr.GetValue("POLCVG_ROW_ID"), m_iClientId);
                            objRsvData.iNewRsvTypeCode = Conversion.ConvertObjToInt(objRdr.GetValue("RESERVE_TYPE_CODE"), m_iClientId);
                            //objRsvData.iNewRsvTypeCode = iRsvtype;
                            objRsvData.iOldLosstype = Conversion.ConvertObjToInt(objRdr.GetValue("LOSS_CODE"), m_iClientId);
                            objRsvData.iOldRsvTypeCode = Conversion.ConvertObjToInt(objRdr.GetValue("RESERVE_TYPE_CODE"), m_iClientId);
                            objRsvData.iNewLossCode = iLosstype;
                            objRsvData.bIsLssReserve = Conversion.ConvertObjToBool(objRdr.GetValue("LSS_RES_EXP_FLAG"), m_iClientId);
                            objRsvData.iOldCvgLossrowId = Conversion.ConvertObjToInt(objRdr.GetValue("CVG_LOSS_ROW_ID"), m_iClientId);

                            //if (!GetDeductibleReserveMapping(objRsvData.iOldCoverageRowId, objRsvData.iNewCoverageRowId, iClaimId, objRsvData.iNewRsvTypeCode, objRsvData.iOldRsvTypeCode, objCache.GetRelatedCodeId(objRsvData.iOldRsvTypeCode), objCache.GetRelatedCodeId(objRsvData.iNewRsvTypeCode)))
                            //{
                            //    throw new RMAppException(Globalization.GetString("MoveFinancials.ReserveDeductibleExclusion", m_iClientId));
                            //}

                            if ((iLosstype != 0) && (iLosstype != objRsvData.iOldLosstype))
                            {

                                objRsvData.iNewCvgLossrowId = GetCoverageXLoss(Conversion.ConvertObjToInt(objRdr.GetValue("POLCVG_ROW_ID"), m_iClientId), iLosstype);

                            }
                            else
                            {
                                objRsvData.iNewCvgLossrowId = Conversion.ConvertObjToInt(objRdr.GetValue("CVG_LOSS_ROW_ID"), m_iClientId);
                            }
                            objRsvData.iNewPolCovSeq = CommonFunctions.GetPolCovSeq(iClaimId, iClaimantEid, objRsvData.iNewCvgLossrowId, m_objUserLogin.objRiskmasterDatabase.ConnectionString);

                            objRsvList.Add(Conversion.ConvertObjToInt(objRdr.GetValue("RC_ROW_ID"), m_iClientId), objRsvData);
                            sReserveTransIds = GetTransactionIds(objSplitList, Conversion.ConvertObjToInt(objRdr.GetValue("RC_ROW_ID"), m_iClientId), objCache.GetCodeId("H", "CHECK_STATUS"));
                            GetActivityrowIds(objActivityTrackList, sReserveTransIds, Conversion.ConvertObjToInt(objRdr.GetValue("RC_ROW_ID"), m_iClientId), objCache.GetTableId("FUNDS"), objCache.GetTableId("RESERVE_CURRENT"), iOldPolicySystemId);
                            GetReserveHistoryIds(objRsrvHistoryList, Conversion.ConvertObjToInt(objRdr.GetValue("RC_ROW_ID"), m_iClientId));

                        }
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }

        }
        public int GetPolicySystemId(int iPolicyId)
        {
            int iPolicysystemId = 0;
            try
            {
                iPolicysystemId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT POLICY_SYSTEM_ID FROM POLICY WHERE  POLICY_ID = " + iPolicyId), m_iClientId);
            }
            catch (Exception e)
            {
                throw e;
            }
            return iPolicysystemId;
        }
        /// <summary>
        /// Move unuploaded activities to the end of table in order to maintain the sequence of unuploaded transactions are move to activities are created
        /// </summary>
        /// <param name="iActivityRowId"></param>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        /// <param name="iPolicySystemId"></param>
        private void MoveActivityToEnd(int iActivityRowId, DbTransaction objTrans, DbConnection objConn, int iPolicySystemId)
        {
            DbWriter objWriter = null;
            try
            {
                objWriter = DbFactory.GetDbWriter(objConn);


                objWriter.Tables.Add("ACTIVITY_TRACK");

                objWriter.Fields.Add("ACTIVITY_ROW_ID", Utilities.GetNextUID(objConn, "ACTIVITY_TRACK", objTrans, m_iClientId));
                objWriter.Fields.Add("POLICY_SYSTEM_ID", iPolicySystemId);
                objWriter.Where.Add("ACTIVITY_ROW_ID = " + iActivityRowId);

                objWriter.Execute(objTrans);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }
        }
        /// <summary>
        /// Create activity track
        /// </summary>
        /// <param name="iRcRowId"></param>
        /// <param name="iTargetPolicySystemId"></param>
        /// <param name="iClaimId"></param>
        /// <param name="objActivityDataList"></param>
        /// <param name="iRsvHistMoveId"></param>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        /// <param name="sSplitList"></param>
        private void UpdateReserveActivities(int iRcRowId, int iTargetPolicySystemId, int iClaimId, SortedList<int, ActivityTrackData> objActivityDataList, int iRsvHistMoveId, DbTransaction objTrans, DbConnection objConn, string sSplitList)
        {
            LocalCache objCache = null;
            bool bNeedClose = false;
            int iSplithistTableKey = 0;
            string sUploadData = string.Empty;
            int iOldPolicySystem = 0;
            double iChangeAmount = 0;
            try
            {
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                foreach (int iKey in objActivityDataList.Keys)
                {
                    if (objActivityDataList[iKey].iUploadFlag == 0)
                    {
                        if (!objActivityDataList[iKey].bPendingUpload)
                        {
                            iOldPolicySystem = objActivityDataList[iKey].iPolicySystemId;

                            switch (objCache.GetShortCode(objActivityDataList[iKey].iActivityType))
                            {

                                case "OC":
                                case "RC":

                                case "OA":
                                case "PO":
                                    bNeedClose = true;
                                    sUploadData = "RES" + iRsvHistMoveId;

                                    break;

                                case "CL":
                                case "NC":
                                    bNeedClose = false;
                                    sUploadData = "RES" + iRsvHistMoveId;

                                    break;
                                case "FP":
                                case "FF":
                                    bNeedClose = false;

                                    iSplithistTableKey = SaveSplitMoveHist(iRsvHistMoveId, objActivityDataList[iKey].iForeignTableKey, objTrans, objConn, sSplitList);
                                    sUploadData = "PAY" + iSplithistTableKey;

                                    break;
                                case "PP":
                                case "SP":
                                    iSplithistTableKey = SaveSplitMoveHist(iRsvHistMoveId, objActivityDataList[iKey].iForeignTableKey, objTrans, objConn, sSplitList);
                                    sUploadData = "PAY" + iSplithistTableKey;

                                    break;

                            }

                            CommonFunctions.CreateLogForPolicyInterface(objConn, objTrans, 0, m_objUserLogin.LoginName, objActivityDataList[iKey].iClaimId, objActivityDataList[iKey].iForeignTableId
                                  , objActivityDataList[iKey].iForeignTableKey, objActivityDataList[iKey].iCheckBatchId, iTargetPolicySystemId, objActivityDataList[iKey].iActivityType,
                                  objActivityDataList[iKey].iAccountId, objActivityDataList[iKey].iRsvAmount, objActivityDataList[iKey].iRsvStatus,
                                  objActivityDataList[iKey].iChangeAmount, objActivityDataList[iKey].iCheckStatus, objActivityDataList[iKey].iVoidFlag,
                                  objActivityDataList[iKey].iIsCollection, false, m_iClientId, 0, 0, sUploadData);

                            if (!objActivityDataList[iKey].bFundsVoidFlag && (objActivityDataList[iKey].iForeignTableId == objCache.GetTableId("FUNDS")))
                            {
                                CommonFunctions.CreateLogForPolicyInterface(objConn, objTrans, 0, m_objUserLogin.LoginName, objActivityDataList[iKey].iClaimId, objActivityDataList[iKey].iForeignTableId
                                     , objActivityDataList[iKey].iForeignTableKey, objActivityDataList[iKey].iCheckBatchId, objActivityDataList[iKey].iPolicySystemId, objCache.GetCodeId("PO", "ACTIVITY_TYPE"),
                                     objActivityDataList[iKey].iAccountId, objActivityDataList[iKey].iRsvAmount, objActivityDataList[iKey].iRsvStatus,
                                        objActivityDataList[iKey].iChangeAmount, objActivityDataList[iKey].iCheckStatus, -1,
                                        objActivityDataList[iKey].iIsCollection, false, m_iClientId, iSplithistTableKey, objCache.GetTableId("SPLIT_MOVE_HIST"), string.Empty);
                            }

                        }
                    }
                    else
                    {
                        MoveActivityToEnd(objActivityDataList[iKey].iActivityRowId, objTrans, objConn, iTargetPolicySystemId);
                    }
                }

                if (bNeedClose)
                {
                    CommonFunctions.CreateLogForPolicyInterface(objConn, objTrans, 0, m_objUserLogin.LoginName, iClaimId, objCache.GetTableId("RESERVE_CURRENT")
                           , iRcRowId, 0, iOldPolicySystem, objCache.GetCodeId("NC", "ACTIVITY_TYPE"), 0, 0, objCache.GetCodeId("C", "RESERVE_STATUS_PARENT"), iChangeAmount, 0, 0, 0, false, m_iClientId, iRsvHistMoveId, objCache.GetTableId("RESERVE_MOVE_HIST"), string.Empty);
                }



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }
        public void MarkPolicyInvalid(int iPolicyId)
        {

            DbWriter objWriter = null;
            try
            {

                objWriter = DbFactory.GetDbWriter(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
                objWriter.Tables.Add("POLICY");

                objWriter.Fields.Add("INVALID_IND", -1);
                objWriter.Fields.Add("UPDATED_BY_USER  ", m_objUserLogin.LoginName);
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD  ", System.DateTime.Now.ToString("yyyyMMddHHmmss"));

                objWriter.Where.Add("POLICY_ID = " + iPolicyId);


                objWriter.Execute();


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }
        }
        /// <summary>
        /// Update reserve history keys to new keys
        /// </summary>
        /// <param name="sRsvHistIds"></param>
        /// <param name="objRsvData"></param>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        private void UpdateRsvHistRecords(string sRsvHistIds, ReserveData objRsvData, DbTransaction objTrans, DbConnection objConn)
        {
            DbWriter objWriter = null;
            try
            {
                foreach (string sRsvHistId in sRsvHistIds.Split(','))
                {
                    objWriter = DbFactory.GetDbWriter(objConn);
                    objWriter.Tables.Add("RESERVE_HISTORY");

                    objWriter.Fields.Add("CLAIMANT_EID", objRsvData.iNewClaimantEid);
                    objWriter.Fields.Add("POLICY_CVG_SEQNO", objRsvData.iNewPolCovSeq);
                    objWriter.Fields.Add("POLCVG_LOSS_ROW_ID", objRsvData.iNewCvgLossrowId);

                    objWriter.Fields.Add("RESERVE_TYPE_CODE", objRsvData.iNewRsvTypeCode);

                    objWriter.Fields.Add("UPDATED_BY_USER  ", m_objUserLogin.LoginName);
                    objWriter.Fields.Add("DTTM_RCD_LAST_UPD  ", System.DateTime.Now.ToString("yyyyMMddHHmmss"));



                    objWriter.Where.Add("RSV_ROW_ID = " + sRsvHistId);


                    objWriter.Execute(objTrans);
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }
        }
        /// <summary>
        /// Function to move reserve from one coverage to another coverage
        /// </summary>
        /// <param name="sMappedRecordIds">mapped coverage ids</param>
        /// <param name="iClaimId"></param>
        /// <param name="iTargetPolicySystemId"></param>
        /// <param name="iSourcePolicySystemId"></param>
        public void BulkFinancialMove(string sMappedRecordIds, int iClaimId, int iTargetPolicySystemId, int iSourcePolicySystemId)
        {
            int iSourceCoverageId = 0;
            int iTargetCoverageId = 0;
            Dictionary<int, ReserveData> objRsvList = new Dictionary<int, ReserveData>();
            Dictionary<int, string> objSplitList = new Dictionary<int, string>();
            Dictionary<int, string> objRsvHistList = new Dictionary<int, string>();
            Dictionary<int, SortedList<int, ActivityTrackData>> objActivityTrackList = new Dictionary<int, SortedList<int, ActivityTrackData>>();
            try
            {


                foreach (string sMappedCoverage in sMappedRecordIds.Split(','))
                {
                    iSourceCoverageId = 0;
                    iTargetCoverageId = 0;

                    if (sMappedCoverage.Split('|')[0].StartsWith("C"))
                    {
                        iSourceCoverageId = Conversion.ConvertStrToInteger(sMappedCoverage.Split('|')[0].Substring(1));
                    }

                    if (sMappedCoverage.Split('|')[1].StartsWith("C"))
                    {
                        iTargetCoverageId = Conversion.ConvertStrToInteger(sMappedCoverage.Split('|')[1].Substring(1));
                    }

                    GetRecordIds(iClaimId, iSourceCoverageId, iTargetCoverageId, objRsvList, objSplitList, objActivityTrackList, objRsvHistList, iSourcePolicySystemId, iTargetPolicySystemId);
                }

                ProcessMoveTo(objRsvList, objSplitList, objActivityTrackList, objRsvHistList, iTargetPolicySystemId, iClaimId,true);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objRsvList = null;
                objSplitList = null;
                objActivityTrackList = null;

            }
        }
        /// <summary>
        /// Edit financial key of any reserve. like claimant,reserve type,loss type.
        /// </summary>
        /// <param name="iRcRowId"></param>
        /// <param name="iClaimId"></param>
        /// <param name="iClaimantEid"></param>
        /// <param name="iLossType"></param>
        /// <param name="iRsvType"></param>
        /// <param name="iClaimLOB"></param>
        /// <param name="iTargetPolicySystemId"></param>
        public void EditReserveKey(int iRcRowId, int iClaimId, int iClaimantEid, int iLossType, int iRsvType, int iClaimLOB, int iTargetPolicySystemId)
        {
            Dictionary<int, ReserveData> objRsvList = new Dictionary<int, ReserveData>();
            Dictionary<int, string> objSplitList = new Dictionary<int, string>();
            Dictionary<int, string> objRsvHistList = new Dictionary<int, string>();
            Dictionary<int, SortedList<int, ActivityTrackData>> objActivityTrackList = new Dictionary<int, SortedList<int, ActivityTrackData>>();
            try
            {
                GetReserveData(iClaimId, iClaimantEid, iLossType, iRsvType, iRcRowId, objRsvList, objSplitList, objActivityTrackList, objRsvHistList, iTargetPolicySystemId);
                ProcessMoveTo(objRsvList, objSplitList, objActivityTrackList, objRsvHistList, iTargetPolicySystemId, iClaimId,false);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objRsvList = null;
                objSplitList = null;
                objActivityTrackList = null;

            }
        }
        /// <summary>
        /// Trigger financial move
        /// </summary>
        /// <param name="objRsvList"></param>
        /// <param name="objSplitList"></param>
        /// <param name="objActivityTrackList"></param>
        /// <param name="objRsvHistList"></param>
        /// <param name="iTargetPolicySystemId"></param>
        /// <param name="iClaimId"></param>
        private void ProcessMoveTo(Dictionary<int, ReserveData> objRsvList, Dictionary<int, string> objSplitList, Dictionary<int, SortedList<int, ActivityTrackData>> objActivityTrackList, Dictionary<int, string> objRsvHistList, int iTargetPolicySystemId, int iClaimId,bool bIsBulkMove)
        {

            DbTransaction objTrans = null;
            DbConnection objConn = null;
            int iRsvhistMoveId = 0;
            ReserveFunds objRsvFunds = null;
            string sReserveIds = string.Empty;
           // SysSettings objSettings = null;
            try
            {
                if (objConn == null)
                    objConn = DbFactory.GetDbConnection(m_objUserLogin.objRiskmasterDatabase.ConnectionString);
                objConn.Open();
                objTrans = objConn.BeginTransaction();
            //    objSettings = new SysSettings(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);

                objRsvFunds = new ReserveFunds(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_objUserLogin, m_iClientId);
                foreach (int iKey in objRsvList.Keys)
                {
                    iRsvhistMoveId = ProcessReserveMove(objRsvList[iKey], objTrans, objConn);

                    if (objRsvHistList.ContainsKey(iKey))
                    {
                        UpdateRsvHistRecords(objRsvHistList[iKey], objRsvList[iKey], objTrans, objConn);
                    }
                    if (objActivityTrackList.ContainsKey(iKey))
                    {
                        if (objSplitList.ContainsKey(iKey))
                        {
                            UpdateReserveActivities(iKey, iTargetPolicySystemId, iClaimId, objActivityTrackList[iKey], iRsvhistMoveId, objTrans, objConn, objSplitList[iKey]);
                        }
                        else
                        {
                            UpdateReserveActivities(iKey, iTargetPolicySystemId, iClaimId, objActivityTrackList[iKey], iRsvhistMoveId, objTrans, objConn, string.Empty);
                        }
                    }
                    if (objRsvList[iKey].bIsLssReserve)
                    {
                        LSSReserveExport(iKey, objConn, objTrans);
                    }
                    //if (objSettings.ClaimActivityLog)
                    //{
                    //    if (!bIsBulkMove)
                    //    {
                           
                    //        CreateClaimActivityLog(objTrans, objConn, iClaimId, objRsvList[iKey], "RESERVE_CURRENT");
                    //    }
                    //}
                }
                SubmitToIso(objConn, objTrans, iClaimId);
                objTrans.Commit();
                objConn.Close();
            }
            catch (Exception e)
            {
                if (objTrans != null)
                {
                    objTrans.Rollback();
                }
                throw e;
            }
            finally
            {
                if (objTrans != null)
                {
                    objTrans.Dispose();
                    objTrans = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }
        private int GetCovTypeCode(int iCovId)
        {
            int iReturn = 0;
            try
            {
                using(DbReader objRdr= DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString,"SELECT COVERAGE_TYPE_CODE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID="+ iCovId))
                {
                    if(objRdr.Read())
                    {
                       iReturn = Conversion.ConvertObjToInt(objRdr.GetValue(0),m_iClientId);
                    }
                }
                return iReturn;
            }

            catch (Exception e)
            {
                throw e;
            }
        }
        private string GetClaimantName(int iEntityId)
        {
            string  sReturn = string.Empty;
            try
            {
                using(DbReader objRdr= DbFactory.GetDbReader(m_objUserLogin.objRiskmasterDatabase.ConnectionString,"SELECT LAST_NAME, FIRST_NAME FROM ENTITY WHERE ENTITY_ID="+ iEntityId))
                {
                    if(objRdr.Read())
                    {
                       sReturn = Conversion.ConvertObjToStr(objRdr.GetValue(1)+" "+ objRdr.GetValue(0));
                    }
                }
                return sReturn;
            }

            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// create activitylog for reserve move
        /// </summary>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        /// <param name="iClaimId"></param>
        /// <param name="objResvData"></param>
        private void CreateClaimActivityLog(DbTransaction objTrans, DbConnection objConn, int iClaimId, ReserveData objResvData, string p_sTableName)
        {

            int iTableId = 0;
            int iOptType = 0;
            bool bAllowLog = false;
            string sLogText = string.Empty;
            StringBuilder sbText = null;
            // SysSettings objSettings = null;
            LocalCache objCache = null;
            int iActType = 0;
            try
            {


                //   bAllowLog = objSettings.ClaimActivityLog;


                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);


                iOptType = objCache.GetCodeId("MF", "OPERATION_TYPE");

                switch (p_sTableName)
                {
                    case "RESERVE_CURRENT":

                        iTableId = objCache.GetTableId("RESERVE_CURRENT");
                        bAllowLog = CommonFunctions.bClaimActLogSetup(m_objUserLogin.objRiskmasterDatabase.ConnectionString, iTableId, iOptType, ref sLogText, ref iActType, m_iClientId);

                        break;
                    case "POLICY_X_CVG_TYPE":

                        iTableId = objCache.GetTableId("RESERVE_CURRENT");
                        bAllowLog = CommonFunctions.bClaimActLogSetup(m_objUserLogin.objRiskmasterDatabase.ConnectionString, iTableId, iOptType, ref sLogText, ref iActType, m_iClientId);



                        break;
                }

                if (bAllowLog)
                {
                    sbText = new StringBuilder();

                    sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.ReserveMoved", m_iClientId)) + ": ");//Reserve Moved from

                    if (objResvData.iNewCoverageRowId != objResvData.iOldCoverageRowId)
                    {
                        sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialCov", m_iClientId)) + ": ");//Reserve Moved from
                        sbText.Append(objCache.GetCodeDesc(GetCovTypeCode(objResvData.iOldCoverageRowId)));
                        sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialTo", m_iClientId)));
                        sbText.Append(objCache.GetCodeDesc(GetCovTypeCode(objResvData.iNewCoverageRowId)));
                    }
                    else
                    {
                        if (objResvData.iOldLosstype != objResvData.iNewLossCode)
                        {
                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialloss", m_iClientId)) + ": ");//Reserve Moved from
                            sbText.Append(objCache.GetCodeDesc(objResvData.iOldLosstype));
                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialTo", m_iClientId)));
                            sbText.Append(objCache.GetCodeDesc(objResvData.iNewLossCode));
                        }

                        if (objResvData.iNewClaimantEid != objResvData.iOldClaimantEid)
                        {
                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialclaimant", m_iClientId)) + ": ");//Reserve Moved from
                            sbText.Append(GetClaimantName(objResvData.iOldClaimantEid));
                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialTo", m_iClientId)));
                            sbText.Append(GetClaimantName(objResvData.iNewClaimantEid));
                        }

                        if (objResvData.iOldRsvTypeCode != objResvData.iNewRsvTypeCode)
                        {
                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialRsvType", m_iClientId)) + ": ");//Reserve Moved from
                            sbText.Append(objCache.GetCodeDesc(objResvData.iOldRsvTypeCode));
                            sbText.Append(CommonFunctions.FilterBusinessMessage(Globalization.GetString("ClmActivityLog.MoveFinancialTo", m_iClientId)));
                            sbText.Append(objCache.GetCodeDesc(objResvData.iNewRsvTypeCode));
                        }
                    }
                    if (sbText.Length > 0)
                        sbText.Append(" - ");

                    sbText.Append(sLogText);



                    CommonFunctions.CreateClaimActivityLog(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_objUserLogin.LoginName, iClaimId.ToString(), iActType, sbText.ToString(), m_iClientId);

                }


            }
            catch (Exception e)
            {
                throw new RMAppException("LogClaimActivity.Save.Exception", e);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                sbText = null;
            }
        }

        /// <summary>
        ///  update reserve current keys to new keys
        /// </summary>
        /// <param name="iRcRowId"></param>
        /// <param name="iClaimantEid"></param>
        /// <param name="iRsvType"></param>
        /// <param name="iCovLossRowId"></param>
        /// <param name="iPolCovSeq"></param>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        /// <param name="iPolCvgRowId"></param>
        private void SaveReserveCurrent(int iRcRowId, int iClaimantEid, int iRsvType, int iCovLossRowId, int iPolCovSeq, DbTransaction objTrans, DbConnection objConn, int iPolCvgRowId)
        {
            DbWriter objWriter = null;
            try
            {
                objWriter = DbFactory.GetDbWriter(objConn);
                objWriter.Tables.Add("RESERVE_CURRENT");
                objWriter.Fields.Add("CLAIMANT_EID", iClaimantEid);
                objWriter.Fields.Add("POLICY_CVG_SEQNO", iPolCovSeq);
                objWriter.Fields.Add("POLCVG_LOSS_ROW_ID", iCovLossRowId);
                objWriter.Fields.Add("RESERVE_TYPE_CODE", iRsvType);
                objWriter.Fields.Add("POLCVG_ROW_ID", iPolCvgRowId);

                objWriter.Fields.Add("UPDATED_BY_USER  ", m_objUserLogin.LoginName);
                objWriter.Fields.Add("DTTM_RCD_LAST_UPD  ", System.DateTime.Now.ToString("yyyyMMddHHmmss"));


                objWriter.Where.Add("RC_ROW_ID = " + iRcRowId);
                objWriter.Execute(objTrans);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }

        }
        /// <summary>
        /// Reset ISO claim flag in order to resubmit claim to ISO
        /// </summary>
        /// <param name="objConn"></param>
        /// <param name="objTrans"></param>
        /// <param name="iClaimId"></param>
        public void SubmitToIso(DbConnection objConn, DbTransaction objTrans, int iClaimId)
        {
            DbWriter objWriter = null;
            bool bIsoClaim = false;
            try
            {
                using (DbReader objRdr = objConn.ExecuteReader("SELECT ISO_CLAIM_SUB_ROW_ID FROM ISO_CLAIM_SUB_HIST WHERE CLAIM_ID=" + iClaimId, objTrans))
                {
                    if (objRdr.Read())
                    {
                        bIsoClaim = true;
                    }
                }
                if (bIsoClaim)
                {
                    objWriter = DbFactory.GetDbWriter(objConn);
                    objWriter.Tables.Add("CLAIM");
                    objWriter.Fields.Add("SUBMIT_TO_ISO_FLAG", -1);

                    objWriter.Where.Add("CLAIM_ID = " + iClaimId);
                    objWriter.Execute(objTrans);
                }

            }
            catch (Exception e)
            {
                throw e;
            }


        }
        /// <summary>
        ///  Reset LSS flag in order to resubmit reserve to LSS
        /// </summary>
        /// <param name="p_iReserveId"></param>
        /// <param name="objConn"></param>
        /// <param name="objTrans"></param>
        public void LSSReserveExport(int p_iReserveId, DbConnection objConn, DbTransaction objTrans)
        {
            string sDTTM = String.Empty;
            string sUser = String.Empty;


            string sSQL = string.Empty;

            int maxGroupID = 0;
            try
            {
                sDTTM = System.DateTime.Now.ToString("yyyyMMddHHmmss");

                using (DbReader objRdr = objConn.ExecuteReader("SELECT RMX_LSS_USER FROM SYS_PARMS", objTrans))
                {
                    if (objRdr.Read())
                    {
                        sUser = Conversion.ConvertObjToStr(objRdr.GetValue(0));
                    }
                }


                if (objConn.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    sSQL = "SELECT ISNULL(MAX(GROUP_ID), 0) FROM RM_LSS_RESERVE_EXP";
                }
                else if (objConn.DatabaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    sSQL = "SELECT NVL(MAX(GROUP_ID), 0) FROM RM_LSS_RESERVE_EXP";
                }
                using (DbReader objRdr = objConn.ExecuteReader(sSQL, objTrans))
                {
                    if (objRdr.Read())
                    {
                        maxGroupID = Conversion.ConvertObjToInt(objRdr.GetValue(0), m_iClientId);
                    }
                }


                maxGroupID = maxGroupID + 1;

                objConn.ExecuteNonQuery("UPDATE RESERVE_CURRENT SET LSS_RES_EXP_FLAG = -1 where RC_ROW_ID = " + p_iReserveId, objTrans);

                if (sUser != m_objUserLogin.LoginName)
                {
                    objConn.ExecuteNonQuery("INSERT INTO RM_LSS_RESERVE_EXP (RESERVE_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG,GROUP_ID) VALUES ('" + p_iReserveId + "', '" + sDTTM + "', 'RESERVE', 0," + maxGroupID + ")", objTrans);
                }

            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }

        }
        /// <summary>
        /// Save  previous reserve key in reserve move history 
        /// </summary>
        /// <param name="objRsvData"></param>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        /// <returns></returns>
        private int SaveReserveMoveHist(ReserveData objRsvData, DbTransaction objTrans, DbConnection objConn)
        {
            DbWriter objWriter = null;
            int iHistMoveId = 0;
            try
            {
                objWriter = DbFactory.GetDbWriter(objConn);
                objWriter.Tables.Add("RESERVE_MOVE_HIST");
                iHistMoveId = Utilities.GetNextUID(objConn, "RESERVE_MOVE_HIST", objTrans, m_iClientId);
                objWriter.Fields.Add("RESERVE_MOVE_HIST_ROW_ID", iHistMoveId);
                objWriter.Fields.Add("POLCVG_LOSS_ROW_ID", objRsvData.iOldCvgLossrowId);

                objWriter.Fields.Add("RC_ROW_ID  ", objRsvData.iRcRowId);

                objWriter.Fields.Add("POLCVG_ROW_ID  ", objRsvData.iOldCoverageRowId);
                objWriter.Fields.Add("LOSS_CODE  ", objRsvData.iOldLosstype);
                objWriter.Fields.Add("CLAIMANT_EID  ", objRsvData.iOldClaimantEid);
                objWriter.Fields.Add("RESERVE_TYPE_CODE  ", objRsvData.iOldRsvTypeCode);
                objWriter.Fields.Add("POLCOVSEQ  ", objRsvData.iOldPolCovSeq);
                objWriter.Fields.Add("ADDED_BY_USER  ", m_objUserLogin.LoginName);
                objWriter.Fields.Add("DTTM_RCD_ADDED  ", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                objWriter.Execute(objTrans);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }
            return iHistMoveId;
        }
        /// <summary>
        /// Save updated funds split with reserve move hist id
        /// </summary>
        /// <param name="iRsvHistMoveId"></param>
        /// <param name="iTransId"></param>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        /// <param name="sSplitList"></param>
        /// <returns></returns>
        private int SaveSplitMoveHist(int iRsvHistMoveId, int iTransId, DbTransaction objTrans, DbConnection objConn, string sSplitList)
        {
            DbWriter objWriter = null;
            int iHistMoveId = 0;
            try
            {
                foreach (string sSplitRowId in sSplitList.Split(','))
                {
                    if (Conversion.ConvertStrToInteger(sSplitRowId.Split('|')[0]) == iTransId)
                    {
                        objWriter = DbFactory.GetDbWriter(objConn);
                        objWriter.Tables.Add("SPLIT_MOVE_HIST");
                        iHistMoveId = Utilities.GetNextUID(objConn, "SPLIT_MOVE_HIST", objTrans, m_iClientId);
                        objWriter.Fields.Add("SPLIT_MOVE_HIST_ROW_ID", iHistMoveId);
                        objWriter.Fields.Add("RESERVE_MOVE_HIST_ROW_ID", iRsvHistMoveId);
                        objWriter.Fields.Add("SPLIT_ROW_ID ", sSplitRowId.Split('|')[1]);
                        objWriter.Fields.Add("TRANS_ID ", iTransId);
                        objWriter.Fields.Add("ADDED_BY_USER  ", m_objUserLogin.LoginName);
                        objWriter.Fields.Add("DTTM_RCD_ADDED  ", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                        objWriter.Execute(objTrans);
                        objWriter = null;
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }
            return iHistMoveId;
        }

        private void SaveCoverageXLoss(int iCoverageId, int iLossTypecode, int iCvgLossRowId, int iDisablityCat, DbTransaction objtrans, DbConnection objConn)
        {
            DbWriter objWriter = null;

            try
            {
                objWriter = DbFactory.GetDbWriter(objConn);
                objWriter.Tables.Add("COVERAGE_X_LOSS");
                objWriter.Fields.Add("LOSS_CODE", iLossTypecode);
                objWriter.Fields.Add("DISABILITY_CAT", iDisablityCat);
                objWriter.Fields.Add("POLCVG_ROW_ID", iCoverageId);
                objWriter.Where.Add("CVG_LOSS_ROW_ID = " + iCvgLossRowId);

                objWriter.Execute(objtrans);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objWriter = null;
            }

        }
        /// <summary>
        /// Initialise reserve move 
        /// </summary>
        /// <param name="objRsvData"></param>
        /// <param name="objTrans"></param>
        /// <param name="objConn"></param>
        /// <returns></returns>
        private int ProcessReserveMove(ReserveData objRsvData, DbTransaction objTrans, DbConnection objConn)
        {
            int iRsvMoveHistRowId = 0;
            LocalCache objCache = null;

            try
            {
                objCache = new LocalCache(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);

                //if (objRsvData.iNewCvgLossrowId == 0)
                //{
                //    SaveCoverageXLoss(objRsvData.iNewCoverageRowId, objRsvData.iNewLossCode, objRsvData.iOldCvgLossrowId, objCache.GetRelatedCodeId(objRsvData.iNewLossCode), objTrans, objConn);
                //}
                //else
                //{
                SaveReserveCurrent(objRsvData.iRcRowId, objRsvData.iNewClaimantEid, objRsvData.iNewRsvTypeCode, objRsvData.iNewCvgLossrowId, objRsvData.iNewPolCovSeq, objTrans, objConn, objRsvData.iNewCoverageRowId);
                //                }

                iRsvMoveHistRowId = SaveReserveMoveHist(objRsvData, objTrans, objConn);

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            return iRsvMoveHistRowId;
        }
        public XmlDocument GetTreeData(int iClaimId, string sPolicyId)
        {
            string sSQL = string.Empty;
            XmlDocument objXmlDoc = null;
            XmlElement objRootElement = null;
            XmlElement objXmlElement = null;
            DataSet objDataSet = null;

            string sUnitName = string.Empty;
            try
            {
                sSQL = @"
                            SELECT POLICY.POLICY_NAME,POLICY.POLICY_ID,POLICY_X_UNIT.POLICY_UNIT_ROW_ID,POLICY_X_UNIT.UNIT_TYPE, POINT_UNIT_DATA.STAT_UNIT_NUMBER,POLICY_X_CVG_TYPE.COVERAGE_TEXT,
                            POLICY_X_CVG_TYPE.POLCVG_ROW_ID,POLICY_X_UNIT.UNIT_ID
                             FROM POLICY,CLAIM_X_POLICY,POLICY_X_UNIT,POINT_UNIT_DATA,POLICY_X_CVG_TYPE
                              WHERE CLAIM_X_POLICY.CLAIM_ID=" + iClaimId+" AND POLICY.POLICY_ID IN ("+ sPolicyId+") ";
                         sSQL = sSQL + @"AND POLICY.POLICY_ID=CLAIM_X_POLICY.POLICY_ID
                            AND POLICY.POLICY_ID=POLICY_X_UNIT.POLICY_ID
                            AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID
                            AND POLICY_X_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE
                            AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID";





                objDataSet = DbFactory.GetDataSet(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL, m_iClientId);

                objXmlDoc = new XmlDocument();
                objRootElement = objXmlDoc.CreateElement("TreeData");

                foreach (DataRow objRow in objDataSet.Tables[0].Rows)
                {
                    objXmlElement = objXmlDoc.CreateElement("Data");
                    objXmlElement.SetAttribute("PolicyName", Conversion.ConvertObjToStr(objRow["POLICY_NAME"]));
                    objXmlElement.SetAttribute("PolUnitRowId", Conversion.ConvertObjToStr(objRow["POLICY_UNIT_ROW_ID"]));


                    switch (Conversion.ConvertObjToStr(objRow["UNIT_TYPE"]))
                    {
                        case "P":
                            sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT PROPERTY_UNIT.PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID=" + Conversion.ConvertObjToStr(objRow["UNIT_ID"])));
                            objXmlElement.SetAttribute("UNITNAME", "Property" + "-" + sUnitName);
                            break;
                        case "V":
                            if (m_objUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_SQLSRVR)
                            {

                                sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT ISNULL(VEHICLE.VEH_DESC, VEHICLE.VIN) FROM VEHICLE WHERE UNIT_ID=" + Conversion.ConvertObjToStr(objRow["UNIT_ID"])));
                            }
                            else if (m_objUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_ORACLE)
                            {

                                sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT nvl(VEHICLE.VEH_DESC, VEHICLE.VIN) FROM VEHICLE WHERE UNIT_ID=" + Conversion.ConvertObjToStr(objRow["UNIT_ID"])));
                            }


                            

                            objXmlElement.SetAttribute("UNITNAME", "Vehicle" + "-" +sUnitName);
                            break;
                        case "SU":

                            if (m_objUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_SQLSRVR)
                            {

                                sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'')))))  FROM ENTITY WHERE ENTITY_ID=" + Conversion.ConvertObjToStr(objRow["UNIT_ID"])));
                            }
                            else if (m_objUserLogin.objRiskmasterDatabase.DbType == eDatabaseType.DBMS_IS_ORACLE)
                            {

                                sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT  nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,''))))) FROM ENTITY WHERE ENTITY_ID=" + Conversion.ConvertObjToStr(objRow["UNIT_ID"])));
                            }

                            objXmlElement.SetAttribute("UNITNAME", "Stat Unit" + "-" +Conversion.ConvertObjToStr(objRow["STAT_UNIT_NUMBER"])+"-"+ sUnitName);
                            break;
                        case "S":


                            sUnitName = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT SITE_UNIT.NAME FROM SITE_UNIT WHERE SITE_ID=" + Conversion.ConvertObjToStr(objRow["UNIT_ID"])));

                            objXmlElement.SetAttribute("UNITNAME", "Site Unit" + "-" + sUnitName);
                            break;
                    }

                    objXmlElement.SetAttribute("CovText", Conversion.ConvertObjToStr(objRow["COVERAGE_TEXT"]));
                    objXmlElement.SetAttribute("PolicyId", Conversion.ConvertObjToStr(objRow["POLICY_ID"]));
                    objXmlElement.SetAttribute("PolCvgRowId", Conversion.ConvertObjToStr(objRow["POLCVG_ROW_ID"]));

                    objRootElement.AppendChild(objXmlElement);
                }
                objXmlDoc.AppendChild(objRootElement);

            }
            catch (Exception e)
            {
                throw e;
            }
            return objXmlDoc;
        }
        private bool GetCvgLossCodeMapping(int iClaimId, int iLossCode, int iTargetCovId,int iPolicySystemId)
        {
            int iPolicyLOB = 0;
            int iCvgTypeCode = 0;
            int iRowId = 0;

            try
            {
                iPolicyLOB = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT POLICY_LOB_CODE FROM CLAIM WHERE CLAIM_ID=" + iClaimId), m_iClientId);
                iCvgTypeCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT COVERAGE_TYPE_CODE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID=" + iTargetCovId), m_iClientId);

                iRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, "SELECT ROW_ID FROM CVG_LOSS_LOB_MAPPING WHERE POLICY_SYSTEM_ID="+iPolicySystemId+" AND  POLICY_LOB=" + iPolicyLOB + " AND CVG_TYPE_CODE=" + iCvgTypeCode + " AND LOSS_CODE=" + iLossCode), m_iClientId);

                if (iRowId > 0)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return false;
        }
        /// <summary>
        /// Get duplicate financial key
        /// </summary>
        /// <param name="objTargetTable"></param>
        /// <param name="objFinancialKey"></param>
        /// <returns></returns>
        private bool GetDuplicateFinancialKey(DataTable objTargetTable, FinancialKey objFinancialKey)
        {
            string sSQL = string.Empty;

            try
            {
                if (objTargetTable == null)
                {

                    sSQL = " SELECT RC_ROW_ID FROM RESERVE_CURRENT,COVERAGE_X_LOSS WHERE CLAIM_ID=" + objFinancialKey.iClaimId + " AND CLAIMANT_EID=" + objFinancialKey.iClaimantEid;
                    sSQL = sSQL + " AND RESERVE_TYPE_CODE =" + objFinancialKey.iRsrvTypeCode + " AND COVERAGE_X_LOSS.POLCVG_ROW_ID=" + objFinancialKey.iPolicyCvgRowId + " AND LOSS_CODE=" + objFinancialKey.iLossType;
                    sSQL = sSQL + " AND    POLCVG_LOSS_ROW_ID= CVG_LOSS_ROW_ID AND RC_ROW_ID <> " + objFinancialKey.iOldRcRowId;

                    if (Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_objUserLogin.objRiskmasterDatabase.ConnectionString, sSQL), m_iClientId) > 0)
                    {
                        return false;
                    }

                }
                else
                {
                    objTargetTable.DefaultView.RowFilter = "LOSS_CODE=" + objFinancialKey.iLossType + " AND CLAIMANT_EID=" + objFinancialKey.iClaimantEid + " AND RESERVE_TYPE_CODE=" + objFinancialKey.iRsrvTypeCode;

                    if ((objTargetTable.DefaultView.ToTable().Rows.Count > 0) && (Conversion.ConvertObjToInt(objTargetTable.DefaultView.ToTable().Rows[0]["RC_ROW_ID"], m_iClientId) > 0))
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
