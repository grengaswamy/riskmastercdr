﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 12/06/2013 | 34082   | pgupta93   | Changes req for Add and Edit Reserve for Multicurrency
 * 14/03/2014 | 34275  | abhadouria |  RMA Swiss Re Financial Summary
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 * 12/26/2014 | rmA-6310 | nshah28  | Checking that proper Parent code is set for Reject
 **********************************************************************************************/
using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.Security;
using Riskmaster.Settings;
using System.Globalization;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

namespace Riskmaster.Application.Reserves
{
    /**************************************************************
     * $File		: ReserveFunds.cs
     * $Revision	: 1.0.0.0
     * $Date		: 11/08/2004
     * $Author		: Navneet Sota
     * $Comment		: A class representing the Reserve Funds. It exposes various methods for retrieving
     *				  and modifying the reserve amounts. 
     * $Source		: Riskmaster.Application 	
    ///* Amendment  -->
    ///  1.Date		: 7 Feb 06 
    ///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
    ///				  violation exception so it can be correctly called. 
    ///    Author	: Sumit
    ///**************************************************************

    **************************************************************/
    public class ReserveFunds
    {
        #region Variable Declarations


        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        internal static string m_sConnectionString = "";

        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private static string m_sDBType = "";

        /// <summary>
        /// Private variable for Claim Number
        /// </summary>
        private string m_sClaimNumber = "";

        // JP 11.20.2005   Passed from business adaptor. Used to check permissions.
        protected UserLogin m_userLogin = null;
        /// <summary>
        /// Private variable for Parent Security Id
        /// </summary>
        private int m_iParentSecurityId = 0;

        /// <summary>
        /// Private variable for Security Id
        /// </summary>
        private int m_iSecurityId = 0;

        /// <summary>
        /// Constant for General Claim Parent Security ID as 150
        /// </summary>
        private int GENERALCLAIMPSID = 150;

        /// <summary>
        /// Constant for Reason of updating the Reserve History, for print cheques functionality
        /// </summary>
        private string REASON = "AUTO ADJUST";

        /// <summary>
        /// Constant for General Claim Parent Security ID as 150
        /// </summary>
        private int VEHICLEACCIDENTPSID = 6000;
        /// <summary>
        /// Do nothing if selected in the LOB setup Mits :7977-- parijat
        /// </summary>
        private bool do_NOTHING = false;
        /// Private variable to store the instance of Datamodel factory object.Added by Shivendu for MITS 11594
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        private int NONOCCPSID = 60000;

        //Start-Mridul Bansal. 12/30/09. MITS#19314. Support for Property Claim.
        /// <summary>
        /// Constant for Property Claim Parent Security ID as 40000
        /// </summary>
        private int PROPERTYCLAIMPSID = 40000;
        //End-Mridul Bansal. 12/30/09. MITS#19314. Support for Property Claim.

        // *BEGIN* JP 11.20.2005   Security ids for parents and reserve screen contexts
        public const int WC_SID = 3000;
        public const int DI_SID = 60000;
        public const int GC_SID = 150;
        public const int VA_SID = 6000;
        //Start-Mridul Bansal. 12/30/09. MITS#19314. Support for Property Claim.
        public const int PC_SID = 40000;
		//Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        //public const int PC_RSRV_SID = 41800;
        public const int PC_RSRV_SID = 43000;
        //public const int PC_RSRV_PAYHIST_SID = 2400;
        public const int PC_RSRV_PAYHIST_SID = 43150;
        //public const int PC_CLMNTS_SID = 41700;
        public const int PC_CLMNTS_SID = 41800;
        //public const int PC_CLMNTS_RSRV_SID = 41730;
        public const int PC_CLMNTS_RSRV_SID = 41950;
        //public const int PC_CLMNTS_RSRV_PAYHIST_SID = 41750;
        public const int PC_CLMNTS_RSRV_PAYHIST_SID = 42100;
        //End:Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        public const int PC_TANDE_SID = 41900;
        //End-Mridul Bansal. 12/30/09. MITS#19314.

        public const int WC_RSRV_SID = 5400;
        public const int WC_RSRV_PAYHIST_SID = 5550;
        public const int WC_TANDE_SID = 5700;	//Mohit for WC Time And Expense

        public const int DI_RSRV_SID = 62400;
        public const int DI_RSRV_PAYHIST_SID = 62550;
        public const int DI_TANDE_SID = 62700;	//Mohit for DI Time And Expense

        public const int VA_RSRV_SID = 8550;
        public const int VA_RSRV_PAYHIST_SID = 8700;
        public const int VA_CLMNTS_SID = 7650;
        public const int VA_CLMNTS_RSRV_SID = 7800;
        public const int VA_CLMNTS_RSRV_PAYHIST_SID = 7950;
        public const int VA_UNIT_SID = 8100;
        public const int VA_UNIT_RSRV_SID = 8250;
        public const int VA_UNIT_RSRV_PAYHIST_SID = 8400;
        public const int VA_TANDE_SID = 8850;	//Mohit for VA Time And Expense

        public const int GC_RSRV_SID = 2250;
        public const int GC_RSRV_PAYHIST_SID = 2400;
        public const int GC_CLMNTS_SID = 1800;
        public const int GC_CLMNTS_RSRV_SID = 1950;
        public const int GC_CLMNTS_RSRV_PAYHIST_SID = 2100;
        public const int GC_TANDE_SID = 2550;	//Mohit for GC Time And Expense
        //Added Rakhi for MITS 13804-START:Control Display of Schedule Checks
        private const int RMB_FUNDS_AUTOCHK = 10400;
        private CommonFunctions.NavFormType eNavType=CommonFunctions.NavFormType.None;//Deb Multi Currency
        //Added Rakhi for MITS 13804-END:Control Display of Schedule Checks
        // *BEGIN* JP 11.20.2005   Security ids for parents and reserve screen contexts
        /// <summary>
        /// Language code of the user logged in
        /// </summary>
        private int m_iLangCode = 0; //Aman ML 

        //Add by kuladeep for SMS Setting MITS:34720 Start
        public const int GC_RSRV_CREATEID = 2253;
        public const int GC_RSRV_UPDATEID = 2252;
        public const int GC_RSRV_VIEWID = 2251;
        public const int WC_RSRV_CREATEID = 5403;
        public const int WC_RSRV_UPDATEID = 5402;
        public const int WC_RSRV_VIEWID = 5401;
        public const int GC_MOVE_TO_FINANCIALS = 2259;
        public const int WC_MOVE_TO_FINANCIALS = 5409;
        //Add by kuladeep for SMS Setting MITS:34720 End

        //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
        public const int GC_RSRV_FIRST_FINAL_PAY = 1220400201;
        public const int WC_RSRV_FIRST_FINAL_PAY = 1220400202;
        //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298

        private int m_iClientId = 0;//rkaur27

        public const int ALLOW_MANUAL_DEDUCTIBLE = 1330400191;// RMA- 7620, aaggarwal29

        #endregion

        #region Constructor
        /// Name		: ReserveFunds
        /// Author		: Navneet Sota
        /// Date Created: 11/08/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>
        /// Constructor for the class
        /// </summary>
        /// <param name="p_sConnectionstring"> The Connection string for database</param>
        /// <param name="p_sClaimNumber">The Claim Number</param>
        /// <param name="p_iParentSecurityId">The Parent Security ID</param>
        /// <param name="p_iSecurityId">The Security ID</param>
        /// 
        public ReserveFunds(string p_sConnectionstring, string p_sClaimNumber,
                            int p_iParentSecurityId, int p_iSecurityId, UserLogin oLogin, int p_iClientId)//rkaur27
        {
            m_sConnectionString = p_sConnectionstring;
            if (p_sClaimNumber != null)
                m_sClaimNumber = p_sClaimNumber;

            m_iParentSecurityId = p_iParentSecurityId;
            m_iSecurityId = p_iSecurityId;
            // JP 11.20.2005
            m_userLogin = oLogin;
            m_iClientId = p_iClientId;//rkaur27
        }

        /// <summary>
        /// Constructor for use *only* for other modules (supervisory approval, printchecks, etc.) that
        /// wish to update reserves. DO NOT USE for any use within the ReserveFunds business adaptor.
        /// </summary>
        /// <param name="p_sConnectionstring"></param>
        /// <param name="p_sClaimNumber"></param>
        /// <param name="p_iParentSecurityId"></param>
        /// <param name="p_iSecurityId"></param>
        public ReserveFunds(string p_sConnectionstring, string p_sClaimNumber,
                         UserLogin oLogin, int p_iClientId)//rkaur27
        {
            m_sConnectionString = p_sConnectionstring;
            if (p_sClaimNumber != null)
                m_sClaimNumber = p_sClaimNumber;

        
            // JP 11.20.2005
            m_userLogin = oLogin;
            m_iClientId = p_iClientId;//rkaur27
        }
        /// <summary>
        /// Constructor for use *only* for other modules (supervisory approval, printchecks, etc.) that
        /// wish to update reserves. DO NOT USE for any use within the ReserveFunds business adaptor.
        /// </summary>
        /// <param name="p_sConnectionstring"></param>
        /// <param name="p_sClaimNumber"></param>
        /// <param name="p_iParentSecurityId"></param>
        /// <param name="p_iSecurityId"></param>
        public ReserveFunds(string p_sConnectionstring, string p_sClaimNumber,
            int p_iParentSecurityId, int p_iSecurityId, int p_iClientId)//rkaur27
        {
            m_sConnectionString = p_sConnectionstring;
            if (p_sClaimNumber != null)
                m_sClaimNumber = p_sClaimNumber;

            m_iParentSecurityId = p_iParentSecurityId;
            m_iSecurityId = p_iSecurityId;
            m_iClientId = p_iClientId;//rkaur27
        }

        /// <summary>
        /// Constructor for BOB Reserves enhancement
        /// </summary>
        /// <param name="p_sConnectionstring"></param>
        /// <param name="oLogin"></param>
        public ReserveFunds(string p_sConnectionstring, UserLogin oLogin, int p_iClientId)//rkaur27
        {
            m_sConnectionString = p_sConnectionstring;            
            m_userLogin = oLogin;
            m_iClientId = p_iClientId;//rkaur27
        }


        #endregion

        #region Reserves Structure
        /// <summary>
        /// Structure containing Reserve's elements.
        /// </summary>
        public struct structReserves
        {
            public int iReserveTypeCode;
            public double dTotalPaid;
            public double dTotalCollected;
            public double dTotalIncurred;
            public double dBalance;
            public string sReserveDesc;
            public string sStatusCode;
            //shobhana
            public string sTableValue;
            public int iReserveTypeId;
            //End
            //BOB Reserve enhancements
            public int iPolicyID;
            public int iPolCvg_Row_ID;
            public int iRC_Row_ID;
            public int iLSS_RES_EXP_FLAG;//Added by sharishkumar for Mits 35472
            public double dReserveAmount;
            //rupal
            public string sStatusCodeDesc;
            public int iUnitID;
            public int iPolicyUnitRowId;
            public string sUnitType;
            public int iCoveTypeCode;
            public string sCoveTypeText; //smahajan22 mits 35587
            public int iClaimantEid;
            //skhare7 Policy Interface 
            public int iPolCvgLossId;
            public int iLossTypeCode;
            public int iDisabilityCode;
            public int iDisabilityCat;
            public int iReserveCat;
            public string sReserveCatDesc;
            public string sDisabilityCatDesc;
   //rupal:start
            public string sCovgSeqNum;
            public string sReason;
            //rupal:end
            //MITS 35453 starts
            public double dClmCurrBalAmount;
            public double dClmCurrResAmount;
            //MITS 35453 ends 
            public string sAdjusterDetails;    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
            public string sRsvStatusParent;
            public string sTransSeqNum;
            public string sCoverageKey;     //Ankit Start : Worked on MITS - 34297
            public string sReserveCurrencyType;//MITS:34082 MultiCurrency
            public double dBaseToReserveCurrRate;//MITS:34082 MultiCurrency
            public double dReserveToClaimCurrRate;//MITS:34082 MultiCurrency
            public string DeductibleTypeCode;
        }
        #endregion

        //mcapps2 MITS 30236 Start
        public class OffsetStruc  
        {
            public int iClaimId;
            public int iUnitId;
            public int iClaimantEId;
            public int iReserveType;
            public int iRCRowId;
            public double dAmount;
        };
        //mcapps2 MITS 30236 End

        #region Transaction Structure
        /// <summary>
        /// Structure containing Reserve's elements.
        /// </summary>
        public struct structTransaction
        {
            public int iTransTypeCode;

            public string sTransDesc;
            
        }
        #endregion

        #region Properties

        /// <summary>
        /// Internal property for do_NOTHING
        /// </summary>
        internal bool Do_Nothing
        {
            get
            {
                return do_NOTHING;
            }
        }
        /// <summary>
        /// Internal property for Claim number
        /// </summary>
        internal string ClaimNumber
        {
            get
            {
                return m_sClaimNumber;
            }
        }

        /// <summary>
        /// Internal property for Parent Security Id
        /// </summary>
        internal int ParentSecurityId
        {
            get
            {
                return m_iParentSecurityId;
            }
        }

        /// <summary>
        /// Internal property for Security Id
        /// </summary>
        internal int SecurityId
        {
            get
            {
                return m_iSecurityId;
            }
        }
        //Aman ML Change
        /// <summary>
        /// Public property for Language Code
        /// </summary>
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion
        
        #region Public Methods
        
        // akaushik5 Added for MITS 33577 Starts
        /// <summary>
        /// Gets the claim reserves summary BOB.
        /// </summary>
        /// <param name="p_iClaimId">The p_i claim id.</param>
        /// <param name="p_iLOB">The p_i LOB.</param>
        /// <param name="sFormTitle">The s form title.</param>
        /// <returns></returns>
        public XmlDocument GetClaimReservesSummaryBOB(int p_iClaimId, int p_iLOB, string sFormTitle)
        {
            string sLOB = string.Empty;
            string sNodeName = string.Empty;
            string sCaption = string.Empty;
            string sSubTitle = string.Empty;
            StringBuilder sbSQL = null;
            int iCStat = 0;
            int iCType = 0;
            int iCodeID = 0;
            int iCnt = 0; 
            double dRAmount = 0;
            double dRPaid = 0;
            double dRCollect = 0;
            double dRBalance = 0;
            double dRIncurred = 0;
            double dBalTotal = 0;
            double dPaidTotal = 0;
            double dCollTotal = 0;
            double dIncTotal = 0;
            bool bCollInRsvBal = false, bPerRsvColInRsvBal = false;//asharma326 JIRA 870
            bool bCollInIncurredBal = false, bPerRsvColInIncrBal = false;//asharma326 JIRA 870
            bool bResByClaimType = false;
            bool bResShowOrphans = false;
            bool bDoNotRecalcReserves = false;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement;
            XmlElement objXmlReservesElement = null;
            XmlElement objXmlReserveElement = null;
            XmlElement objXmlClaimantsElement = null;
            XmlElement objXmlClaimantElement = null;
            DbReader objReader = null;
            DbReader objRdr = null;
            List<structReserves> arrReserves = null;
            LocalCache objCache = null;
            structReserves objReserve;
            objReserve.dBalance = 0;
            objReserve.dTotalCollected = 0;
            objReserve.dTotalIncurred = 0;
            objReserve.dTotalPaid = 0;
            objReserve.iReserveTypeCode = 0;
            objReserve.sReserveDesc = string.Empty;
            objReserve.sStatusCode = string.Empty;
            Claim objClaimNew = null;
            Claim objClaim = null;
            int iClaimantEID = 0;
            int claimantEId = 0;
            bool success;
            ColLobSettings objColLobSettings = null;//asharma326 JIRA 871
            SysSettings objSysSettings = null;
            try 
            {
                bool IsBaseCurrTypeSelected = false;
                if (p_iClaimId != 0)
                {
                    IsBaseCurrTypeSelected = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString) > 0 ? false : true;
                }
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);

                //asharma326 JIRA 871 
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objXmlDoc = new XmlDocument();
                objXmlElement = objXmlDoc.CreateElement("claimreserves");
                objXmlDoc.AppendChild(objXmlElement);
                objXmlElement.SetAttribute("claimid", p_iClaimId.ToString());

                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT CLAIM.LINE_OF_BUS_CODE,CLAIM.CLAIM_NUMBER,CODES.RELATED_CODE_ID,CLAIM.CLAIM_TYPE_CODE, CLAIM.CLAIM_STATUS_CODE ");
                sbSQL.Append("FROM CLAIM, CODES ");
                sbSQL.Append("WHERE CLAIM.CLAIM_STATUS_CODE = CODES.CODE_ID ");
                sbSQL.Append("AND CLAIM_ID = " + p_iClaimId);

                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            //Get the value of "Line Of Business"
                            sLOB = objReader.GetValue("LINE_OF_BUS_CODE").ToString();
                            if (p_iLOB == 0 && !string.IsNullOrEmpty(sLOB))
                            {
                                p_iLOB = Conversion.CastToType<int>(sLOB, out success);
                            }

                            //Get the value of Claim Number
                            if (string.IsNullOrEmpty(m_sClaimNumber.Trim()))
                            {
                                m_sClaimNumber = objReader.GetValue("CLAIM_NUMBER").ToString();
                            }

                            //Get the value of Related Code Id
                            iCStat = Conversion.CastToType<int>(objReader.GetValue("RELATED_CODE_ID").ToString(), out success);

                            //Get the value of Claim Type Code
                            iCType = Conversion.CastToType<int>(objReader.GetValue("CLAIM_TYPE_CODE").ToString(), out success);
                            
                            using (objCache = new LocalCache(m_sConnectionString,m_iClientId))
                            {
                                objXmlElement.SetAttribute("claimstatus", objCache.GetShortCode(objReader.GetInt32("CLAIM_STATUS_CODE")));
                            }
                        }
                        objReader.Close();
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT RESERVE_TRACKING, COLL_IN_RSV_BAL,PER_RSV_COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL,BAL_TO_ZERO , NEG_BAL_TO_ZERO ,SET_TO_ZERO, COLL_IN_INCUR_BAL, RES_BY_CLM_TYPE "); //Parijat: MITS 7977
                sbSQL.Append("FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + p_iLOB);

                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {

                            bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("COLL_IN_RSV_BAL").ToString()), m_iClientId);
                            bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), m_iClientId);
                            bResByClaimType = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("RES_BY_CLM_TYPE").ToString()), m_iClientId);
                            bPerRsvColInRsvBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                            bPerRsvColInIncrBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870

                        }
                        objReader.Close();
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append(" SELECT RES_DO_NOT_RECALC, RES_SHOW_ORPHANS FROM SYS_PARMS");
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            bResShowOrphans = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("RES_SHOW_ORPHANS").ToString()), m_iClientId);

                            bDoNotRecalcReserves = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("RES_DO_NOT_RECALC").ToString()), m_iClientId);
                        }
                        objReader.Close();
                    }
                }
                // akaushik5  Changed for MITS 33577 Starts
                //if (p_iLOB == 243 || p_iLOB == 844)
                //{
                //    m_objDataModelFactory = new DataModelFactory(m_userLogin);
                
                string sLob = ReserveFunds.GetLobString(p_iLOB);
                Dictionary<int, string> claimantList = null;
                
                using (m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId))//rkaur27
                {
                    // akaushik5  Changed for MITS 33577 Ends
                    using (Claim objClm = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false))
                    {
                        objClm.MoveTo(p_iClaimId);

                        // akaushik5  Added for MITS 33577 Starts
                        if (p_iLOB == 243 || p_iLOB == 844)
                        {
                            iClaimantEID = objClm.PrimaryPiEmployee.PiEid;
                        }

                        claimantList = objClm.ClaimantList.Cast<Claimant>().Select(x => new { x.ClaimantEid, ClaimantName = x.ClaimantEntity.GetLastFirstName() }).ToDictionary(x => x.ClaimantEid, x => x.ClaimantName);
                        string caption = GetClaimantFormTitle(objClm, iClaimantEID);
                        sCaption = string.Format("{0} {1}]", sLob, caption.Substring(0, caption.LastIndexOf('*')));
                        // akaushik5  Added for MITS 33577 Ends
                    }
                }
                //}

                // akaushik5 Commented for MITS 33577 Starts
                // Get the caption Starts
                //string sLob = ReserveFunds.GetLobString(p_iLOB);
                //Dictionary<int, string> claimantList = null;

                //if (m_objDataModelFactory == null)
                //{
                //    using (m_objDataModelFactory = new DataModelFactory(m_userLogin))
                //    {
                //        using (objClaimNew = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false))
                //        {
                //            objClaimNew.MoveTo(p_iClaimId);
                //            claimantList = objClaimNew.ClaimantList.Cast<Claimant>().Select(x => new { x.ClaimantEid, ClaimantName = x.ClaimantEntity.GetLastFirstName() }).ToDictionary(x => x.ClaimantEid, x => x.ClaimantName);
                //            string caption = GetClaimantFormTitle(objClaimNew, iClaimantEID);
                //            sCaption = string.Format("{0} {1}]", sLob, caption.Substring(0, caption.LastIndexOf('*')));
                //        }
                //    }
                //}
                // akaushik5 Commented for MITS 33577 Ends


                List<structReserves> claimantTotalReserves = null;

                // akaushik5 Changed for MITS 33577 Starts
                //if (claimantList.Count > 0)
                if (!object.ReferenceEquals(claimantList, null) && claimantList.Count > 0)
                // akaushik5 Changed for MITS 33577 Ends
                {
                    claimantTotalReserves = new List<structReserves>();
                    
                    foreach (KeyValuePair<int, string> claimantInfo in claimantList)
                    {
                        structReserves claimantReserve = default(structReserves);
                        claimantReserve.iClaimantEid = claimantInfo.Key;
                        claimantTotalReserves.Add(claimantReserve);
                    }
                }
                
                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT RESERVE_TYPE_CODE, CODE_DESC,CODES.SHORT_CODE");
                if (bResByClaimType)
                {
                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT,CODES");
                    sbSQL.Append(" WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES.CODE_ID AND");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + p_iLOB);
                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + iCType);
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }
                else 
                {
                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT,CODES ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + p_iLOB);
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }

                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE", this.LanguageCode);

                //Populate the values in Reserves Struct
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    arrReserves = new List<structReserves>();
                    while (objReader.Read())
                    {
                        structReserves currentReserve = default(structReserves);
                        currentReserve.iReserveTypeCode = Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success);
                        currentReserve.sReserveDesc = string.Format("{0} {1}", objReader.GetValue("SHORT_CODE").ToString(), objReader.GetValue("CODE1").ToString());
                        arrReserves.Add(currentReserve);
                    }
                    objReader.Close();
                }
                sbSQL.Remove(0, sbSQL.Length);

                if (bResShowOrphans)
                {
                    //Build Query for Orphaned Reserve Types
                    sbSQL.Append(" SELECT RESERVE_TYPE_CODE,CODE_DESC,CODES.SHORT_CODE FROM RESERVE_CURRENT,CODES_TEXT,CODES ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND ");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" CLAIM_ID = " + p_iClaimId);

                    if (iClaimantEID != 0)
                        sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantEID);

                    sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, CODE_DESC,CODES.SHORT_CODE ");
                    sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE", this.LanguageCode);

                    // Orphaned Reserve Types
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        while(objReader.Read())
                        {
                            if (!arrReserves.Any(x => x.iReserveTypeCode == Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success)))
                            {
                                structReserves currentReserve = default(structReserves);
                                currentReserve.iReserveTypeCode = Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success);
                                currentReserve.sReserveDesc = string.Format("{0} {1}", objReader.GetValue("SHORT_CODE").ToString(), objReader.GetValue("CODE_DESC").ToString());
                                arrReserves.Add(currentReserve);

                            }
                        }
                        objReader.Close();
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);
                iCnt = 0;
                int iAddRecoveryReservetoTotalBalanceAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalBalanceAmount;//asharma326 JIRA 871
                int iAddRecoveryReservetoTotalIncurredAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalIncurredAmount;//asharma326 JIRA 872
                //if (bDoNotRecalcReserves)
                //{
                //    sbSQL.Append("SELECT * FROM RESERVE_CURRENT WHERE CLAIM_ID = " + p_iClaimId);

                //    if (iClaimantEID != 0)
                //    {
                //        sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantEID);
                //    }

                //    sbSQL.Append(" ORDER BY RESERVE_TYPE_CODE, DATE_ENTERED DESC ");
                //}
                //else
                //{
                sbSQL.Append("SELECT RESERVE_TYPE_CODE, CLAIMANT_EID,");
                if (objSysSettings.UseMultiCurrency != 0)
                {
                    if (IsBaseCurrTypeSelected)
                    {
                        eNavType = CommonFunctions.NavFormType.None;
                        sbSQL.Append(" BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL ");
                    }
                    else
                    {
                        eNavType = CommonFunctions.NavFormType.Reserve;
                        sbSQL.Append(" CLAIM_CURRENCY_BALANCE_AMOUNT BALANCE_AMOUNT, CLAIM_CURRENCY_INCURRED_AMOUNT INCURRED_AMOUNT, CLAIM_CURRENCY_RESERVE_AMOUNT RES_AMOUNT, CLAIM_CURRENCY_PAID_TOTAL PAID_TOTAL, CLAIM_CURR_COLLECTION_TOTAL COLL_TOTAL ");
                    }
                }
                else
                {
                    eNavType = CommonFunctions.NavFormType.None;
                    sbSQL.Append("BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL");
                }

                sbSQL.Append(" FROM RESERVE_CURRENT ");
                sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);

                if (iClaimantEID != 0)
                {
                    sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantEID);
                }
                //}

                if (bDoNotRecalcReserves)
                {
                    //Special reserve processing
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        while (objReader.Read())
                        {
                            claimantEId = Conversion.CastToType<int>(objReader.GetValue("CLAIMANT_EID").ToString(), out success);
                            dRBalance = Conversion.CastToType<double>(objReader.GetValue("BALANCE_AMOUNT").ToString(), out success);
                            dRPaid = Conversion.CastToType<double>(objReader.GetValue("PAID_TOTAL").ToString(), out success);
                            dRCollect = Conversion.CastToType<double>(objReader.GetValue("COLL_TOTAL").ToString(), out success);
                            dRIncurred = Conversion.CastToType<double>(objReader.GetValue("INCURRED_AMOUNT").ToString(), out success);
                            iCodeID = Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success);

                            for (int iCount = 0; iCount < claimantTotalReserves.Count; iCount++)
                            {
                                if (claimantTotalReserves[iCount].iClaimantEid == claimantEId)
                                {
                                    structReserves currentReserve = claimantTotalReserves[iCount];
                                    //currentReserve.dBalance += dRBalance;
                                    using (objCache = new LocalCache(m_sConnectionString,m_iClientId)) // mkaran2 - MITS 35029
                                    {
                                        if (objCache.GetRelatedShortCode(iCodeID) == "R")
                                        {
                                            if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                            {
                                                currentReserve.dBalance += dRBalance;
                                            }
                                            else
                                            {
                                                currentReserve.dBalance -= dRBalance;
                                            }
                                            //asharma326 JIRA 872
                                            if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                                currentReserve.dTotalIncurred += dRIncurred;  //MITS 35837
                                            else
                                                currentReserve.dTotalIncurred -= dRIncurred;  //MITS 35837
                                        }
                                        else
                                        {
                                            currentReserve.dBalance += dRBalance;
                                            currentReserve.dTotalIncurred += dRIncurred;  //MITS 35837
                                        }
                                    }                            

                                    currentReserve.dTotalPaid += dRPaid;
                                    currentReserve.dTotalCollected += dRCollect;
                                    //currentReserve.dTotalIncurred += dRIncurred;  //MITS 35837
                                    claimantTotalReserves[iCount] = currentReserve;
                                    break;
                                }
                            }

                            for (int iCount = 0; iCount < arrReserves.Count; iCount++)
                            {
                                if (arrReserves[iCount].iReserveTypeCode == iCodeID)
                                {
                                    structReserves currentreserve = arrReserves[iCount];
                                    currentreserve.dBalance += dRBalance;
                                    currentreserve.dTotalPaid += dRPaid;
                                    currentreserve.dTotalCollected += dRCollect;
                                    currentreserve.dTotalIncurred += dRIncurred;
                                    arrReserves[iCount] = currentreserve;
                                    objCache = new LocalCache(m_sConnectionString, m_iClientId);  //MITS 35029
                                    if (objCache.GetRelatedShortCode(arrReserves[iCount].iReserveTypeCode) == "R")  //MITS 35029
                                    {
                                        //asharma326 JIRA 871
                                        if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                        {
                                            dBalTotal += dRBalance;
                                        }
                                        else
                                        {
                                            dBalTotal -= dRBalance;
                                        }
                                        //asharma326 JIRA 872
                                        if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                            dIncTotal += dRIncurred;  //MITS 35837
                                        else
                                            dIncTotal -= dRIncurred;  //MITS 35837
                                    }
                                    else
                                    {
                                        dBalTotal += dRBalance;
                                        dIncTotal += dRIncurred;  //MITS 35837
                                    }
                                    dPaidTotal += dRPaid;
                                    dCollTotal += dRCollect;
                                    //dIncTotal += dRIncurred;  //MITS 35837
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        while (objReader.Read())
                        {
                            iCodeID = Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success);
                            dRAmount = Conversion.CastToType<double>(objReader.GetValue("RES_AMOUNT").ToString(), out success);
                            dRPaid = Conversion.CastToType<double>(objReader.GetValue("PAID_TOTAL").ToString(), out success);
                            dRCollect = Conversion.CastToType<double>(objReader.GetValue("COLL_TOTAL").ToString(), out success);
                            claimantEId = Conversion.CastToType<int>(objReader.GetValue("CLAIMANT_EID").ToString(), out success);

                            dRBalance = CalculateReserveBalance(dRAmount, dRPaid, dRCollect, bCollInRsvBal, iCStat, iCodeID, bPerRsvColInRsvBal, p_iLOB);//asharma326 JIRA 870 add bPerRsvColInRsvBal, p_iLOB
                            dRIncurred = CalculateIncurred(dRBalance, dRPaid, dRCollect, bCollInRsvBal, bCollInIncurredBal, iCodeID, bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB);//asharma326 JIRA 870 bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB

                            objCache = new LocalCache(m_sConnectionString, m_iClientId);  //MITS 35029

                            for (int iCount = 0; iCount < claimantTotalReserves.Count; iCount++)
                            {
                                if (claimantTotalReserves[iCount].iClaimantEid == claimantEId)
                                {
                                    structReserves currentReserve = claimantTotalReserves[iCount];
                                    dRIncurred = CalculateIncurred(dRBalance, dRPaid, dRCollect, bCollInRsvBal, bCollInIncurredBal, iCodeID, bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB);//asharma326 JIRA 870 bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB
                                    if (objCache.GetRelatedShortCode(iCodeID) == "R")  //MITS 35029
                                    {
                                        //asharma326 JIRA 871
                                        if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                        {
                                            currentReserve.dBalance += dRBalance;
                                        }
                                        else
                                        {
                                            currentReserve.dBalance -= dRBalance;
                                        }
                                        //asharma326 JIRA 872
                                        if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                            currentReserve.dTotalIncurred += dRIncurred;  //MITS 35837
                                        else
                                            currentReserve.dTotalIncurred -= dRIncurred;  //MITS 35837
                                    }
                                    else
                                    {
                                        currentReserve.dBalance += dRBalance;
                                        currentReserve.dTotalIncurred += dRIncurred;  //MITS 35837
                                    }
                                    currentReserve.dTotalPaid += dRPaid;
                                    currentReserve.dTotalCollected += dRCollect;
                                    //currentReserve.dTotalIncurred += dRIncurred;  //MITS 35837
                                    claimantTotalReserves[iCount] = currentReserve;
                                    break;
                                }
                            }
                            for (iCnt = 0; iCnt < arrReserves.Count; iCnt++)
                            {
                                if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                                {
                                    structReserves currentReserve = arrReserves[iCnt];
                                    currentReserve.dBalance += dRBalance;
                                    currentReserve.dTotalPaid += dRPaid;
                                    currentReserve.dTotalCollected += dRCollect;
                                    currentReserve.dTotalIncurred += dRIncurred;
                                    arrReserves[iCnt] = currentReserve;

                                    if (objCache.GetRelatedShortCode(arrReserves[iCnt].iReserveTypeCode) == "R")  //MITS 35029
                                    {
                                        //asharma326 JIRA 871
                                        if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                        {
                                            dBalTotal += dRBalance;  //MITS 35029
                                        }
                                        else
                                        {
                                            dBalTotal -= dRBalance;  //MITS 35029
                                        }
                                        //asharma326 JIRA 872
                                        if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                            dIncTotal += dRIncurred;  //MITS 35837
                                        else
                                            dIncTotal -= dRIncurred;  //MITS 35837
                                    }
                                    else
                                    {
                                        dBalTotal += dRBalance;
                                        dIncTotal += dRIncurred;  //MITS 35837
                                    }
                                    dPaidTotal += dRPaid;
                                    dCollTotal += dRCollect;
                                    //dIncTotal += dRIncurred;  //MITS 35837
                                    break;
                                }
                            }
                        }

                        objReader.Close();
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);

                //Format Results into XML
                
                objXmlReservesElement = objXmlDoc.CreateElement("reserves");
                objXmlElement.AppendChild(objXmlReservesElement);
                objXmlReservesElement.SetAttribute("claimid", p_iClaimId.ToString());
                objXmlReservesElement.SetAttribute("claimant", iClaimantEID.ToString());
                sNodeName = "reservereadonly";

                foreach( structReserves currentReserve in arrReserves)
                {
                    objXmlReserveElement = objXmlDoc.CreateElement(sNodeName);
                    objXmlReservesElement.AppendChild(objXmlReserveElement);

                    objXmlReserveElement.SetAttribute("reservetypecode", currentReserve.iReserveTypeCode.ToString());
                    objXmlReserveElement.SetAttribute("reservename", currentReserve.sReserveDesc);
                    objXmlReserveElement.SetAttribute("reservestatus", currentReserve.sStatusCode);

                    objXmlReserveElement.SetAttribute("balance", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dBalance, eNavType, m_sConnectionString,m_iClientId));
                    objXmlReserveElement.SetAttribute("incurred", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dTotalIncurred, eNavType, m_sConnectionString, m_iClientId));
                    objXmlReserveElement.SetAttribute("paid", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dTotalPaid, eNavType, m_sConnectionString, m_iClientId));
                    objXmlReserveElement.SetAttribute("collected", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dTotalCollected, eNavType, m_sConnectionString, m_iClientId));
                }

                objXmlReserveElement = objXmlDoc.CreateElement("totals");
                objXmlElement.AppendChild(objXmlReserveElement);
                objXmlReserveElement.SetAttribute("balanceamount", CommonFunctions.ConvertCurrency(p_iClaimId, dBalTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("incurredamount", CommonFunctions.ConvertCurrency(p_iClaimId, dIncTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("paidtotal", CommonFunctions.ConvertCurrency(p_iClaimId, dPaidTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("collectedtotal", CommonFunctions.ConvertCurrency(p_iClaimId, dCollTotal, eNavType, m_sConnectionString, m_iClientId));

                objXmlElement.SetAttribute("claimnumber", m_sClaimNumber);
                objXmlElement.SetAttribute("caption", sCaption);
                sSubTitle = sCaption.Replace("'", "\\'");
                objXmlElement.SetAttribute("subtitle", sSubTitle);
                objXmlElement.SetAttribute("FormTitle", sFormTitle);

                objXmlClaimantsElement = objXmlDoc.CreateElement("claimants");
                objXmlElement.AppendChild(objXmlClaimantsElement);
                foreach (structReserves currentReserve in claimantTotalReserves)
                {
                    objXmlClaimantElement = objXmlDoc.CreateElement("claimant");
                    objXmlClaimantsElement.AppendChild(objXmlClaimantElement);
                    objXmlClaimantElement.SetAttribute("claimantid", currentReserve.iClaimantEid.ToString());
                    
                    if (claimantList.ContainsKey(currentReserve.iClaimantEid))
                    {
                        objXmlClaimantElement.SetAttribute("claimantname", claimantList[currentReserve.iClaimantEid]);
                    }

                    objXmlClaimantElement.SetAttribute("balance", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dBalance, eNavType, m_sConnectionString, m_iClientId));
                    objXmlClaimantElement.SetAttribute("incurred", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dTotalIncurred, eNavType, m_sConnectionString, m_iClientId));
                    objXmlClaimantElement.SetAttribute("paid", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dTotalPaid, eNavType, m_sConnectionString, m_iClientId));
                    objXmlClaimantElement.SetAttribute("collected", CommonFunctions.ConvertCurrency(p_iClaimId, currentReserve.dTotalCollected, eNavType, m_sConnectionString, m_iClientId));
                }
            }

            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetClaimReserves.GenericError", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objCache != null)
                    objCache.Dispose();
                objCache = null;
                if (objRdr != null)
                    objRdr.Dispose();
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                }
                if (objClaimNew != null)
                {
                    objClaimNew.Dispose();
                    objClaimNew = null;
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objColLobSettings != null)
                {
                    objColLobSettings = null;
                }
            }

            //Return the result to calling function
            return objXmlDoc;
        }
        // akaushik5 Added for MITS 33577 Ends
        // MITS 34275- RMA Swiss Re Financial Summary START  


        /// <summary>
        /// Gets the total Reserve Incurred amount.
        /// </summary>
        /// <param name="p_objXmlIn">The p_obj XML in.</param>
        /// <param name="sTotalIncurred">The sTotalIncurred out.</param>        
        /// <returns></returns>
        /// Added:Yukti, Dt:12/24/2014
        public string GetTotalReserveBalance(int iClaimID)
        {
            int iLOB = 0;
            string sFormTitle = "";
            XmlDocument p_objXmlOut = new XmlDocument();
            string sTotalIncurred = string.Empty;

            try
            {
                //iClaimId = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//ClaimID").InnerText, out success);
                //objReserveFunds = InitializeReservesBOB(ref p_objXmlIn, ref p_objXmlOut, ref p_objErrOut);                
                p_objXmlOut.InnerXml = GetClaimReservesSummaryBOB(iClaimID, iLOB, sFormTitle).OuterXml;
                //p_objXmlOut.DocumentElement.InnerXml = GetClaimReservesSummaryBOB(iClaimID, iLOB, sFormTitle).OuterXml;

                 sTotalIncurred = p_objXmlOut.SelectSingleNode("//totals").Attributes["balanceamount"].Value;//vkumar258

                sTotalIncurred = sTotalIncurred.Replace("$", "");
                sTotalIncurred = sTotalIncurred.Replace(",", "");//vkumar258 
               
            } 
            finally
            {
                if (p_objXmlOut != null)
                    p_objXmlOut = null;
            }
            return sTotalIncurred;
        }

        /// </summary>
        /// <param name="p_iClaimId"> Claim ID</param>
        /// <param name="p_iLOB">LOB</param>
        /// <param name="p_iPolicyId">PolicyID</param>
        /// <param name="p_iUnitId">PolicyUnitrow ID</param>
        /// <param name="p_iCoverageId">Coverage ID</param>
        /// <param name="sFormTitle">Form</param>
        /// <param name="p_iSummaryLevel">Dropdown index selected in Policy unit coverage dropdown</param>
        /// <param name="p_sCurrCurrencyType">Previous code value in the multicurrency drop down</param>
        /// <param name="p_sDestcurrencyType">Selected code value in the multicurrency dropdown</param>
        /// <param name="sClaimCurrency">Claim Currency</param>
        /// <param name="p_iCoverageId"></param>
        /// <param name="sFormTitle"></param>
        /// <returns></returns>
        public XmlDocument GetPolicyUnitCoverageSummary(int p_iClaimId, int p_iLOB, int p_iPolicyId, int p_iCoverageId, int p_iSummaryLevel, int p_iUnitId, string sFormTitle, string p_sCurrCurrencyType, string p_sDestcurrencyType, string sClaimCurrency)
        {
            string sLOB = string.Empty;
            string sNodeName = string.Empty;
            string sCaption = string.Empty;
            string sSubTitle = string.Empty;
            StringBuilder sbSQL = null;
            int iCStat = 0;
            int iCType = 0;
            int iCodeID = 0;
            //int iCnt = 0;
            double dRAmount = 0;
            double dRPaid = 0;
            double dRCollect = 0;
            double dRBalance = 0;
            double dRIncurred = 0;
            double dBalTotal = 0;
            double dPaidTotal = 0;
            double dCollTotal = 0;
            double dIncTotal = 0;
            bool bCollInRsvBal = false, bPerRsvColInRsvBal = false;//asharma326 JIRA 870
            bool bCollInIncurredBal = false, bPerRsvColInIncrBal = false;//asharma326 JIRA 870
            bool bResByClaimType = false;
            bool bResShowOrphans = false;
            bool bDoNotRecalcReserves = false;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement;
            XmlElement objXmlReservesElement = null;
            XmlElement objXmlReserveElement = null;
            XmlElement objXmlElementTmp = null;
            XmlElement objXmlElementCurrencylist = null;
            XmlElement objXmlElementCurrency = null;
            DbReader objReader = null;
            DbReader objRdr = null;
            List<structReserves> arrReserves = null;
            LocalCache objCache = null;
            structReserves objReserve;
            objReserve.dBalance = 0;
            objReserve.dTotalCollected = 0;
            objReserve.dTotalIncurred = 0;
            objReserve.dTotalPaid = 0;
            objReserve.iReserveTypeCode = 0;
            objReserve.sReserveDesc = string.Empty;
            objReserve.sStatusCode = string.Empty;
            Claim objClaimNew = null;
            Claim objClaim = null;
            int iClaimantEID = 0;
            int claimantEId = 0;
            string[] sDataSeparator = { "~~~" };
            string sText = String.Empty;
            string sValue = String.Empty;
            string sinitialvalue = String.Empty;
            string ssymbol = String.Empty;
            string balance = String.Empty;
            string paid = String.Empty;
            string incurred = String.Empty;
            string collected = String.Empty;
            string tbalance = String.Empty;
            string tpaid = String.Empty;
            string tincurred = String.Empty;
            string tcollected = String.Empty;
            string value = String.Empty;
            string claimcurrency = String.Empty;
            int isourcecurrcode = 0;
            int idestcurrcode = 0;
            bool success;
            ColLobSettings objColLobSettings = null;//asharma326 JIRA 871
            //asharma326 JIRA 4635 Starts
            SysSettings objSysSettings = null;
            try
            {
                bool IsBaseCurrTypeSelected = false;
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                objXmlDoc = new XmlDocument();
                objXmlElement = objXmlDoc.CreateElement("claimreserves");
                objXmlDoc.AppendChild(objXmlElement);
                claimcurrency  = sClaimCurrency ;
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                }
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                objXmlElement.SetAttribute("claimid", p_iClaimId.ToString()); 
                if (p_sCurrCurrencyType != "" || p_sDestcurrencyType != "")
                {
                    if (p_sCurrCurrencyType != "")
                    {
                        sinitialvalue = p_sCurrCurrencyType.Split('_')[0];
                    }
                    else
                    {
                        sinitialvalue = p_sCurrCurrencyType;
                    }

                    idestcurrcode = Conversion.ConvertStrToInteger(p_sDestcurrencyType);

                }
                else
                {
                    idestcurrcode = objClaim.CurrencyType;
                }

                isourcecurrcode =  CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);

                //Get the value of "Line Of Business"
    
                sLOB = objClaim.PolicyLOBCode.ToString();
                
                if (p_iLOB == 0 && !string.IsNullOrEmpty(sLOB))
                {
                    p_iLOB = Conversion.CastToType<int>(sLOB, out success);
                }

                //Get the value of Claim Number
                if (string.IsNullOrEmpty(m_sClaimNumber.Trim()))
                {
                    m_sClaimNumber = objClaim.ClaimNumber;
                }

                //Get the value of Related Code Id
                // objCache = new LocalCache(m_sConnectionString);
                    objCache.GetRelatedCodeId(p_iClaimId);
                iCStat = Conversion.CastToType<int>(objCache.GetRelatedCodeId(p_iClaimId).ToString (), out success);

                //Get the value of Claim Type Code
                iCType = Conversion.CastToType<int>(objClaim.ClaimTypeCode.ToString(), out success);

                //  using (objCache = new LocalCache(m_sConnectionString))
                {
                    objXmlElement.SetAttribute("claimstatus", objCache.GetShortCode(objClaim.ClaimStatusCode));
                }

                sbSQL.Append("SELECT RESERVE_TRACKING, COLL_IN_RSV_BAL,PER_RSV_COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL,BAL_TO_ZERO , NEG_BAL_TO_ZERO ,SET_TO_ZERO, COLL_IN_INCUR_BAL, RES_BY_CLM_TYPE "); //Parijat: MITS 7977
                sbSQL.Append("FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + p_iLOB);

                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {

                            bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("COLL_IN_RSV_BAL").ToString()), m_iClientId);
                            bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), m_iClientId);
                            bResByClaimType = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("RES_BY_CLM_TYPE").ToString()), m_iClientId);
                            bPerRsvColInRsvBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                            bPerRsvColInIncrBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870
                        }
                        objReader.Close();
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append(" SELECT RES_DO_NOT_RECALC, RES_SHOW_ORPHANS FROM SYS_PARMS");
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            bResShowOrphans = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("RES_SHOW_ORPHANS").ToString()), m_iClientId);

                            bDoNotRecalcReserves = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                                objReader.GetValue("RES_DO_NOT_RECALC").ToString()), m_iClientId);
                        }
                        objReader.Close();
                    }
                }
                string sLob = ReserveFunds.GetLobString(p_iLOB);
                Dictionary<int, string> claimantList = null;
                        if (p_iLOB == 243 || p_iLOB == 844)
                        {
                            iClaimantEID = objClaim.PrimaryPiEmployee.PiEid;
                        }

                        claimantList = objClaim.ClaimantList.Cast<Claimant>().Select(x => new { x.ClaimantEid, ClaimantName = x.ClaimantEntity.GetLastFirstName() }).ToDictionary(x => x.ClaimantEid, x => x.ClaimantName);
                        string caption = GetClaimantFormTitle(objClaim, iClaimantEID);
                        sCaption = string.Format("{0} {1}]", sLob, caption.Substring(0, caption.LastIndexOf('*')));
                        // akaushik5  Added for MITS 33577 Ends
                
           

                List<structReserves> claimantTotalReserves = null;

                // akaushik5 Changed for MITS 33577 Starts
                //if (claimantList.Count > 0)
                if (!object.ReferenceEquals(claimantList, null) && claimantList.Count > 0)
                // akaushik5 Changed for MITS 33577 Ends
                {
                    claimantTotalReserves = new List<structReserves>();

                    foreach (KeyValuePair<int, string> claimantInfo in claimantList)
                    {
                        structReserves claimantReserve = default(structReserves);
                        claimantReserve.iClaimantEid = claimantInfo.Key;
                        claimantTotalReserves.Add(claimantReserve);
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT RESERVE_TYPE_CODE, CODE_DESC,CODES.SHORT_CODE");
                if (bResByClaimType)
                {
                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT,CODES");
                    sbSQL.Append(" WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES.CODE_ID AND");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + p_iLOB);
                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + iCType);
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }
                else
                {
                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT,CODES ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + p_iLOB);
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }

                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE", this.LanguageCode);

                //Populate the values in Reserves Struct
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    arrReserves = new List<structReserves>();
                    while (objReader.Read())
                    {
                        structReserves currentReserve = default(structReserves);
                        currentReserve.iReserveTypeCode = Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success);
                        currentReserve.sReserveDesc = string.Format("{0} {1}", objReader.GetValue("SHORT_CODE").ToString(), objReader.GetValue("CODE1").ToString());
                        arrReserves.Add(currentReserve);
                    }
                    objReader.Close();
                }
                sbSQL.Remove(0, sbSQL.Length);

                if (bResShowOrphans)
                {
                    //Build Query for Orphaned Reserve Types
                    sbSQL.Append(" SELECT RESERVE_TYPE_CODE,CODE_DESC,CODES.SHORT_CODE FROM RESERVE_CURRENT,CODES_TEXT,CODES ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND ");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" CLAIM_ID = " + p_iClaimId);

                    if (iClaimantEID != 0)
                        sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantEID);

                    sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, CODE_DESC,CODES.SHORT_CODE ");
                    sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE", this.LanguageCode);

                    // Orphaned Reserve Types
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        while (objReader.Read())
                        {
                            if (!arrReserves.Any(x => x.iReserveTypeCode == Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success)))
                            {
                                structReserves currentReserve = default(structReserves);
                                currentReserve.iReserveTypeCode = Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success);
                                currentReserve.sReserveDesc = string.Format("{0} {1}", objReader.GetValue("SHORT_CODE").ToString(), objReader.GetValue("CODE_DESC").ToString());
                                arrReserves.Add(currentReserve);

                            }
                        }
                        objReader.Close();
                    }
                }

                sbSQL.Remove(0, sbSQL.Length);
                //iCnt = 0;
                int iAddRecoveryReservetoTotalBalanceAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalBalanceAmount;//asharma326 JIRA 871 
                int iAddRecoveryReservetoTotalIncurredAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalIncurredAmount;//asharma326 JIRA 872
                if (p_iSummaryLevel == 1)  //For Policy Level
                {
                    sbSQL.Append("SELECT RESERVE_TYPE_CODE, CLAIMANT_EID,");

                    if (objSysSettings.UseMultiCurrency != 0)
                    {
                        //if (IsBaseCurrTypeSelected)
                        //{
                        //eNavType = CommonFunctions.NavFormType.None;
                        sbSQL.Append(" BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL");
                        //}
                        //else
                        //{
                        eNavType = CommonFunctions.NavFormType.Reserve;
                        //  sbSQL.Append(" CLAIM_CURRENCY_BALANCE_AMOUNT BALANCE_AMOUNT, CLAIM_CURRENCY_INCURRED_AMOUNT INCURRED_AMOUNT, CLAIM_CURRENCY_RESERVE_AMOUNT RES_AMOUNT, CLAIM_CURRENCY_PAID_TOTAL PAID_TOTAL, CLAIM_CURR_COLLECTION_TOTAL COLL_TOTAL");
                        //}
                    }
                    else
                    {
                        eNavType = CommonFunctions.NavFormType.None;
                        sbSQL.Append(" BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL");
                    }
                    sbSQL.Append(" FROM RESERVE_CURRENT R INNER JOIN COVERAGE_X_LOSS CL on CL.CVG_LOSS_ROW_ID=R.POLCVG_LOSS_ROW_ID INNER JOIN  POLICY_X_CVG_TYPE PCT ");

                    sbSQL.Append("ON PCT.POLCVG_ROW_ID=CL.POLCVG_ROW_ID INNER JOIN POLICY_X_UNIT PU ON PU.POLICY_UNIT_ROW_ID=PCT.POLICY_UNIT_ROW_ID ");
                    sbSQL.Append("WHERE PU .POLICY_ID =" + p_iPolicyId + "AND R.CLAIM_ID =" + p_iClaimId);
                }
                else if (p_iSummaryLevel == 2) //For Unit level
                {

                    sbSQL.Append("SELECT RESERVE_TYPE_CODE, CLAIMANT_EID,");

                    if (objSysSettings.UseMultiCurrency != 0)
                    {
                        //if (IsBaseCurrTypeSelected)
                        //{
                        //eNavType = CommonFunctions.NavFormType.None;
                        sbSQL.Append(" BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL");
                        //}
                        //else
                        //{
                        eNavType = CommonFunctions.NavFormType.Reserve;
                        //  sbSQL.Append(" CLAIM_CURRENCY_BALANCE_AMOUNT BALANCE_AMOUNT, CLAIM_CURRENCY_INCURRED_AMOUNT INCURRED_AMOUNT, CLAIM_CURRENCY_RESERVE_AMOUNT RES_AMOUNT, CLAIM_CURRENCY_PAID_TOTAL PAID_TOTAL, CLAIM_CURR_COLLECTION_TOTAL COLL_TOTAL");
                        //}
                    }
                    else
                    {
                        eNavType = CommonFunctions.NavFormType.None;
                        sbSQL.Append(" BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL");
                    }
                    sbSQL.Append(" FROM RESERVE_CURRENT R INNER JOIN COVERAGE_X_LOSS CL on CL.CVG_LOSS_ROW_ID=R.POLCVG_LOSS_ROW_ID INNER JOIN  POLICY_X_CVG_TYPE PCT ");
                    sbSQL.Append("ON PCT.POLCVG_ROW_ID = CL.POLCVG_ROW_ID   WHERE PCT.POLICY_UNIT_ROW_ID =" + p_iUnitId + "AND R.CLAIM_ID =" + p_iClaimId);
                }
                else //For Coverage level
                {
                    sbSQL.Append("SELECT RESERVE_TYPE_CODE, CLAIMANT_EID,");

                    if (objSysSettings.UseMultiCurrency != 0)
                    {
                        //if (IsBaseCurrTypeSelected)
                        //{
                        //eNavType = CommonFunctions.NavFormType.None;
                        sbSQL.Append(" BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL");
                        //}
                        //else
                        //{
                        eNavType = CommonFunctions.NavFormType.Reserve;
                        //  sbSQL.Append(" CLAIM_CURRENCY_BALANCE_AMOUNT BALANCE_AMOUNT, CLAIM_CURRENCY_INCURRED_AMOUNT INCURRED_AMOUNT, CLAIM_CURRENCY_RESERVE_AMOUNT RES_AMOUNT, CLAIM_CURRENCY_PAID_TOTAL PAID_TOTAL, CLAIM_CURR_COLLECTION_TOTAL COLL_TOTAL");
                        //}
                    }
                    else
                    {
                        eNavType = CommonFunctions.NavFormType.None;
                        sbSQL.Append(" BALANCE_AMOUNT,INCURRED_AMOUNT, RESERVE_AMOUNT RES_AMOUNT, PAID_TOTAL PAID_TOTAL, COLLECTION_TOTAL COLL_TOTAL");
                    }

                    sbSQL.Append(" FROM RESERVE_CURRENT R INNER JOIN  COVERAGE_X_LOSS CL ");
                    sbSQL.Append(" ON  R.POLCVG_LOSS_ROW_ID = CL.CVG_LOSS_ROW_ID  ");
                    sbSQL.Append(" WHERE CL.POLCVG_ROW_ID =" + p_iCoverageId + "AND R.CLAIM_ID =" + p_iClaimId);
                }
                //asharma326 JIRA 4635 Starts
                //Special reserve processing
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    while (objReader.Read())
                    {
                        double exchangerate = CommonFunctions.ExchangeRateSrc2Dest(isourcecurrcode, idestcurrcode, m_sConnectionString, m_iClientId);

                        claimantEId = Conversion.CastToType<int>(objReader.GetValue("CLAIMANT_EID").ToString(), out success);
                        dRPaid = Conversion.CastToType<double>(objReader.GetValue("PAID_TOTAL").ToString(), out success);
                        dRCollect = Conversion.CastToType<double>(objReader.GetValue("COLL_TOTAL").ToString(), out success);
                        iCodeID = Conversion.CastToType<int>(objReader.GetValue("RESERVE_TYPE_CODE").ToString(), out success);
                        dRAmount = Conversion.CastToType<double>(objReader.GetValue("RES_AMOUNT").ToString(), out success);

                        dRPaid *= exchangerate;
                        dRCollect *= exchangerate;
                        dRAmount *= exchangerate;

                        if (bDoNotRecalcReserves)
                        {
                            dRBalance = Conversion.CastToType<double>(objReader.GetValue("BALANCE_AMOUNT").ToString(), out success);
                            dRIncurred = Conversion.CastToType<double>(objReader.GetValue("INCURRED_AMOUNT").ToString(), out success);
                            dRBalance *= exchangerate;
                            dRIncurred *= exchangerate;
                        }
                        else
                        {
                            dRBalance = CalculateReserveBalance(dRAmount, dRPaid, dRCollect, bCollInRsvBal, iCStat, iCodeID, bPerRsvColInRsvBal, p_iLOB);//asharma326 JIRA 870 add bPerRsvColInRsvBal, p_iLOB
                            dRIncurred = CalculateIncurred(dRBalance, dRPaid, dRCollect, bCollInRsvBal, bCollInIncurredBal, iCodeID, bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB);//asharma326 JIRA 870 add bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB
                        }


                        for (int iCount = 0; iCount < arrReserves.Count; iCount++)
                        {
                            if (arrReserves[iCount].iReserveTypeCode == iCodeID)
                            {
                                structReserves currentreserve = arrReserves[iCount];
                                currentreserve.dBalance += dRBalance;
                                currentreserve.dTotalPaid += dRPaid;
                                currentreserve.dTotalCollected += dRCollect;
                                currentreserve.dTotalIncurred += dRIncurred;
                                arrReserves[iCount] = currentreserve;
                                //asharma326 JIRA 871
                                if (objCache.GetRelatedShortCode(arrReserves[iCount].iReserveTypeCode) == "R")
                                {
                                    if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                    {
                                        dBalTotal += dRBalance;
                                    }
                                    else
                                        dBalTotal -= dRBalance;
                                    //asharma326 JIRA 872
                                    if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                        dIncTotal += dRIncurred;
                                    else
                                        dIncTotal -= dRIncurred;  //MITS 35837
                                }
                                else
                                {
                                    dBalTotal += dRBalance;
                                    dIncTotal += dRIncurred;  //MITS 35837
                                }
                                //dBalTotal += dRBalance;
                                dPaidTotal += dRPaid;
                                dCollTotal += dRCollect;
                                //dIncTotal += dRIncurred;


                                break;
                            }
                        }
                    }

                }
                //asharma326 JIRA 4635 Ends

                sbSQL.Remove(0, sbSQL.Length);


                //Format Results into XML
                objXmlReservesElement = objXmlDoc.CreateElement("reserves");
                objXmlElement.AppendChild(objXmlReservesElement);
                objXmlReservesElement.SetAttribute("claimid", p_iClaimId.ToString());
                objXmlReservesElement.SetAttribute("claimant", iClaimantEID.ToString());
                sNodeName = "reservereadonly";

                foreach (structReserves currentReserve in arrReserves)
                {
                    objXmlReserveElement = objXmlDoc.CreateElement(sNodeName);
                    objXmlReservesElement.AppendChild(objXmlReserveElement);

                    objXmlReserveElement.SetAttribute("reservetypecode", currentReserve.iReserveTypeCode.ToString());
                    objXmlReserveElement.SetAttribute("reservename", currentReserve.sReserveDesc);
                    objXmlReserveElement.SetAttribute("reservestatus", currentReserve.sStatusCode);
                    objXmlReserveElement.SetAttribute("balance", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, currentReserve.dBalance, m_sConnectionString, m_iClientId));
                    objXmlReserveElement.SetAttribute("incurred", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, currentReserve.dTotalIncurred, m_sConnectionString, m_iClientId));
                    objXmlReserveElement.SetAttribute("paid", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, currentReserve.dTotalPaid, m_sConnectionString, m_iClientId));
                    objXmlReserveElement.SetAttribute("collected", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, currentReserve.dTotalCollected, m_sConnectionString, m_iClientId));
                }

                objXmlReserveElement = objXmlDoc.CreateElement("totals");
                objXmlElement.AppendChild(objXmlReserveElement);
                objXmlReserveElement.SetAttribute("balanceamount", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, dBalTotal, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("incurredamount", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, dIncTotal, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("paidtotal", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, dPaidTotal, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("collectedtotal", CommonFunctions.ConvertByCurrencyCode(idestcurrcode, dCollTotal, m_sConnectionString, m_iClientId));
                //}
                objXmlElement.SetAttribute("claimnumber", m_sClaimNumber);
                objXmlElement.SetAttribute("caption", sCaption);
                sSubTitle = sCaption.Replace("'", "\\'");
                objXmlElement.SetAttribute("subtitle", sSubTitle);
                objXmlElement.SetAttribute("FormTitle", sFormTitle);
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                CreateAndSetElement(objXmlDoc.DocumentElement, "CurrencyType", iClaimCurrCode.ToString(), iClaimCurrCode.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append("SELECT SHORT_CODE,CODE_ID  FROM CODES WHERE DELETED_FLAG = '0' AND TABLE_ID= (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'CURRENCY_TYPE')");//Ashish Ahuja - Gap 17 changes
                string sShortCode = string.Empty;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                { 
                    objXmlElementCurrency = objXmlDoc.CreateElement("Multicurrency");
                    objXmlElement.AppendChild(objXmlElementCurrency);
                    objXmlElementCurrencylist = objXmlDoc.CreateElement("CurrencyTypeList");
                    objXmlElementCurrency.AppendChild(objXmlElementCurrencylist);
                    while (objReader.Read())
                    {
                        sShortCode = objReader.GetValue("SHORT_CODE").ToString();
                        sValue = objReader.GetValue("CODE_ID").ToString();
                        objXmlElementTmp = objXmlDoc.CreateElement("Item");
                        objXmlElementTmp.SetAttribute("value", sValue);
                        objXmlElementCurrencylist.AppendChild(objXmlElementTmp);
                        objXmlElementTmp.InnerText = sShortCode ;
                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);


            }

            catch (PermissionViolationException objException)
            {
                throw objException;
            }
                //Ashish Ahuja - Gap 17 changes
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetPolicyUnitCoverageSummary.GenericError", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objCache != null)
                    objCache.Dispose();
                objCache = null;
                if (objRdr != null)
                    objRdr.Dispose();
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                }
                if (objClaimNew != null)
                {
                    objClaimNew.Dispose();
                    objClaimNew = null;
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objColLobSettings != null)
                {
                    objColLobSettings = null;
                }
            }

            //Return the result to calling function
            return objXmlDoc;
        }

        //   MITS 34275- RMA Swiss Re Financial Summary END  

        /// <summary>
        /// This function is used to call the Payment Offset function from Payment History
        /// </summary>
        public void OffsetReserves(int p_iTransId)
        {
            if (p_iTransId != 0)
            {
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                }
                Funds objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                objFunds.MoveTo(p_iTransId);
                OffSetPayment(objFunds);
            }
        }
        /// <summary>
        /// This function is a wrapper over Riskmaster.Application.Reserves.ReserveFunds.OffsetPayment() function. 
        /// This method is basically used for altering/modifying the Claim reserves associated with a particular payment
        /// </summary>
        public bool OffSetPayment(Funds p_objFunds)
        {
            string sSQL = string.Empty;
            StringBuilder sbSQL = null;
            DbReader objReader = null;
            Claim objClaim = null;
            ReserveCurrentList objResCurrList = null;
            bool bfound = false;
            int iClaimId = p_objFunds.ClaimId;
            int iClaimantEId = p_objFunds.ClaimantEid;
            int iUnitId = p_objFunds.UnitId;
            ArrayList lstReserves = new ArrayList();
            OffsetStruc objResItem = new OffsetStruc();
            LocalCache objLocalCache = null;
            int iStatusCode = 0;
            string sReason = "OFFSET";
            bool bResLimit = false;////Jira 6385- Incurred Limit
            bool bClaimIncLimit = false;////Jira 6385- Incurred Limit
            ReserveCurrent objReserveCurrent = null;//6385
            ColLobSettings objColLobSettings = null;//sachin 6385
            bool b_AddRecoveryReservetoTotalIncurredAmount = false;//sachin 6385
            int iLOB = 0;
            if (m_objDataModelFactory == null)
            {
                m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
            }
            objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
            objClaim.MoveTo(p_objFunds.ClaimId);
            objResCurrList = objClaim.ReserveCurrentList;
            objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
            iStatusCode = objClaim.ClaimStatusCode;
            objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
            

            if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(iStatusCode)).ToUpper() == "C")
            {
                p_objFunds.OffSetFlag = true;
                p_objFunds.Save();
            }
            else
            {
                foreach (FundsTransSplit objSplit in p_objFunds.TransSplitList)
                {
                    foreach (OffsetStruc objResList in lstReserves)
                    {
                        objResItem = null;
                        objResItem = new OffsetStruc();
                        if ((objResList.iRCRowId == objSplit.RCRowId) || ((objResList.iRCRowId == 0) && (objResList.iClaimId == iClaimId) && (objResList.iClaimantEId == iClaimantEId) && (objResList.iUnitId == iUnitId) && (objResList.iReserveType == objSplit.ReserveTypeCode)))
                        {
                            objResList.dAmount = (objResList.dAmount + objSplit.Amount);
                            bfound = true;
                            break;
                        }
                    }
                    if (!bfound)
                    {
                        objResItem.iRCRowId = objSplit.RCRowId;
                        objResItem.iClaimId = iClaimId;
                        objResItem.iClaimantEId = iClaimantEId;
                        objResItem.iUnitId = iUnitId;
                        objResItem.iReserveType = objSplit.ReserveTypeCode;
                        objResItem.dAmount = objSplit.Amount;
                        lstReserves.Add(objResItem);
                    }
                    else
                    {
                        bfound = false;
                    }

                }

                try
                {
                    //Jira 6385 Start 
                    sbSQL = new StringBuilder();
                    sbSQL.Append(" SELECT RES_LMT_FLAG,CLM_INC_LMT_FLAG ,CLAIM_NUMBER,CLAIM.LINE_OF_BUS_CODE");
                    sbSQL.Append(" FROM SYS_PARMS_LOB,CLAIM WHERE CLAIM.CLAIM_ID = " + p_objFunds.ClaimId);
                    sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = SYS_PARMS_LOB.LINE_OF_BUS_CODE ");

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            bResLimit = Conversion.ConvertLongToBool
                                (Conversion.ConvertStrToLong
                                (objReader.GetValue("RES_LMT_FLAG").ToString()), m_iClientId);
                            bClaimIncLimit = Conversion.ConvertLongToBool
                                (Conversion.ConvertStrToLong
                                (objReader.GetValue("CLM_INC_LMT_FLAG").ToString()), m_iClientId);
                            iLOB = Conversion.ConvertStrToInteger(objReader.GetValue("LINE_OF_BUS_CODE").ToString()); 
                        }
                        objReader.Close();
                    }
                    sbSQL.Remove(0, sbSQL.Length);
                    if (objColLobSettings[iLOB].AddRecoveryReservetoTotalIncurredAmount == -1)
                    {
                        b_AddRecoveryReservetoTotalIncurredAmount = true;
                    }
                    objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);
                    ////Jira 6385- Incurred Limit ends
                    LocalCache objCache = null;
                    bool bClaimIncExceedLimit = false;
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                    //Syadav55 Jira-15210 starts
                    //string recoverycode = Convert.ToString(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_TYPE"), objCache.GetCodeId("R", "MASTER_RESERVE")));
                    string recoverycode = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_TYPE") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("R", "MASTER_RESERVE") + "  AND DELETED_FLAG=0"));
                    //int holdreservecode = Convert.ToInt32(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"),objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")));                    
                    //int holdreservecode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")), m_iClientId);
                    int holdreservecode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                    //Syadav55 Jira-15210 ends
                    bClaimIncExceedLimit = CommonFunctions.CheckClaimIncurredLimits(iClaimId, p_objFunds.Amount, holdreservecode, recoverycode, m_userLogin.UserId, m_userLogin.GroupId, objClaim.LineOfBusCode, b_AddRecoveryReservetoTotalIncurredAmount, m_sConnectionString, m_iClientId);
                    if (bClaimIncLimit && objReserveCurrent.ClaimIncLimitsforFNF() && bClaimIncExceedLimit)
                    {
                        RMAppException thisError = new RMAppException();
                        throw new RMAppException(Globalization.GetString("OffsetPayment.ClaimIncurredLimits.Error", m_iClientId), thisError);
                    }
                    //Jira 6385 ends
                    if (bClaimIncLimit && objReserveCurrent.ClaimIncLimitsforFNF() && !bClaimIncExceedLimit)
                    {
                        foreach (OffsetStruc objOffSetReserve in lstReserves)
                        {
                            sbSQL = new StringBuilder();

                        sbSQL.Append("SELECT MAX(MAX_AMOUNT) MAX_AMT FROM RESERVE_LIMITS, CLAIM");
                        sbSQL.Append(" WHERE (USER_ID = " + m_userLogin.UserId); //MITS 34575

                        sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_objFunds.ClaimId);
                        sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = RESERVE_LIMITS.LINE_OF_BUS_CODE");

                        sbSQL.Append(" AND (" + objOffSetReserve.iReserveType + " = RESERVE_LIMITS.RESERVE_TYPE_CODE");
                        sbSQL.Append("  OR RESERVE_LIMITS.RESERVE_TYPE_CODE = 0)");

                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                         
                        if (objReader.Read())
                        {

                                double dThisMaxAmount = 0.0; //MITS 34575
                                dThisMaxAmount = Conversion.ConvertObjToInt(objReader.GetValue("MAX_AMT"), m_iClientId); //MITS 34575
                                foreach (ReserveCurrent objThisReserve in objResCurrList) //MITS 34575
                                {
                                    if ((dThisMaxAmount >= (objThisReserve.ReserveAmount + objOffSetReserve.dAmount)) || (Conversion.ConvertObjToInt(objReader.GetValue("MAX_AMT"), m_iClientId) == 0)) //MITS 34575
                                    {
                                        if (objOffSetReserve.iRCRowId != 0)
                                        {
                                            if (objOffSetReserve.iRCRowId == objThisReserve.RcRowId)
                                            {
                                                objThisReserve.ReserveAmount = objThisReserve.ReserveAmount + objOffSetReserve.dAmount;
                                                objThisReserve.sUpdateType = "Reserves";
                                                objThisReserve.Reason = sReason;
                                                //mkaran2 - MITS 34522 - Start
                                                //mcapps2 - MITS 34897 - Start 
                                                objThisReserve.DateEntered = DateTime.Now.ToShortDateString();
                                                //mcapps2 - MITS 34897 - Start 
                                                objThisReserve.iUserId = m_userLogin.UserId;
                                                objThisReserve.iGroupId = m_userLogin.GroupId;
                                                //mkaran2 - MITS 34522 - End
                                                objThisReserve.Save();
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (objOffSetReserve.iClaimId == objThisReserve.ClaimId && objOffSetReserve.iClaimantEId == objThisReserve.ClaimantEid && objOffSetReserve.iUnitId == objThisReserve.UnitId && objOffSetReserve.iReserveType == objThisReserve.ReserveTypeCode)
                                            {
                                                objThisReserve.ReserveAmount = objThisReserve.ReserveAmount + objOffSetReserve.dAmount;
                                                objThisReserve.sUpdateType = "Reserves";
                                                objThisReserve.Reason = sReason;
                                                //mkaran2 - MITS 34522 - Start
                                                objThisReserve.iUserId = m_userLogin.UserId;
                                                objThisReserve.iGroupId = m_userLogin.GroupId;
                                                //mcapps2 - MITS 34897 - Start 
                                                objThisReserve.DateEntered = DateTime.Now.ToShortDateString();
                                                //mcapps2 - MITS 34897 - Start
                                                //mkaran2 - MITS 34522 - End
                                                objThisReserve.Save();
                                                break;
                                            }
                                        }
                                    }
                                    else  //MITS 34575
                                    {
                                        RMAppException thisError = new RMAppException(); //MITS 34575
                                        throw new RMAppException(Globalization.GetString("OffsetPayment.ReserveLimit.Error", m_iClientId), thisError); //MITS 34575
                                    }
                                }
                            }

                        }
                    }

                    // End JIRA 6385 
                    p_objFunds.OffSetFlag = true;
                    p_objFunds.Save();

                    return true;
                }
                catch (RMAppException e)
                {
                    throw e;
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("OffsetPayment.GenericError", m_iClientId), p_objException); //MITS 34575
                }
                finally
                {
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                    if (objReserveCurrent == null)
                    {
                        objReserveCurrent.Dispose();
                        objReserveCurrent = null;
                    }

                    if (objColLobSettings != null)
                    {
                        objColLobSettings = null;
                    }
                    objClaim = null;
                    objResCurrList = null;
                    objLocalCache = null;
                    sbSQL = null;
                }
            }
            return true;
        }  //mcapps2 MITS 30236 End
        

        //pmittal5  MITS 9906  04/11/08
        //#region public GetClaimReserves (p_iClaimId, p_iClaimantEID, p_iUnitID, p_iLOB, p_bIsTandE)
        #region public GetClaimReserves (p_iClaimId, p_iClaimantEID, p_iUnitID, p_iLOB, sFormTitle, p_bIsTandE)

        /// Name		: GetClaimReserves
        /// Author		: Navneet Sota
        /// Date Created: 11/08/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for get the Claim reserves associated with a 
        /// particular claim
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_iClaimid">The Claim ID</param>
        /// <param name="p_iClaimantEID">The Claimant ID</param>
        /// <param name="p_iUnitID">The Unit ID.</param>
        /// <param name="p_iLOB">The Line of Business</param>
        /// <param name="p_bIsTandE">Boolean: Is Time and Expense</param>
        //pmittal5  MITS 9906  04/11/08
        ///<param name="sFormTitle">String: The Title of the page</param>

        //public XmlDocument GetClaimReserves (int p_iClaimId, int p_iClaimantEID,  int p_iUnitID,  
        //								int p_iLOB, bool p_bIsTandE)
        public XmlDocument GetClaimReserves(int p_iClaimId, int p_iClaimantEID, int p_iUnitID,
                                        int p_iLOB, string sFormTitle, bool p_bIsTandE,int p_iCurrType)//Deb
        {
            //String Variables
            string sLOB = "";
            string sNodeName = "";
            string sCaption = "";
            // Start Naresh MITS 8532 1/3/2007
            string sSubTitle = "";
            // End Naresh MITS 8532 1/3/2007
            DataTable dt = new DataTable();
            dt.AsEnumerable();
            //String Object
            StringBuilder sbSQL = null;

            //Integer Variables
            int iCStat = 0;
            int iCType = 0;
            int iUseTandE = 0;
            int iReserveTracking = 0;
            int iStatus = 0;
            int iCodeID = 0;
            int iCnt = 0; // Variable used in For Loops
            int iTemp = 0;// Variable used in For Loops

            //Double Variables
            double dRAmount = 0;
            double dRPaid = 0;
            double dRCollect = 0;
            double dRBalance = 0;
            double dRIncurred = 0;
            double dBalTotal = 0;
            double dPaidTotal = 0;
            double dCollTotal = 0;
            double dIncTotal = 0;

            //Boolean
            bool bProcess = false;
            bool bDetailTrackingClaimLevel = false;
            bool bCollInRsvBal = false, bPerRsvColInRsvBal = false;//asharma326 JIRA 870
            //Shruti for MITS 8551
            bool bCollInIncurredBal = false, bPerRsvColInIncrBal = false;//asharma326 JIRA 870
            bool bResByClaimType = false;
            bool bResShowOrphans = false;
            bool bDoNotRecalcReserves = false;
            bool bSecTandE = true; //added by Mohit for Time And Expense
            //Parijat: Mits 7977
            bool bBalTOZero = false;
            bool bNegBalToZero = false;
            bool bSetToZero = false;

            //XML Object variables
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement;
            XmlElement objXmlReservesElement = null;
            XmlElement objXmlReserveElement = null;
            XmlDocument objXmlDocument = null;      // Umesh

            //Database objects
            DataSet objDataSet = null;
            DbReader objReader = null;
            DataRow objDataRow = null;
            DbReader objRdr = null;//Umesh

            structReserves[] arrReserves = null;
            LocalCache objCache = null;
            CCacheFunctions objCCacheFunctions = null;

            //Initialize the structReserves struct variables
            structReserves objReserve;
            objReserve.dBalance = 0;
            objReserve.dTotalCollected = 0;
            objReserve.dTotalIncurred = 0;
            objReserve.dTotalPaid = 0;
            objReserve.iReserveTypeCode = 0;
            objReserve.sReserveDesc = "";
            objReserve.sStatusCode = "";
            Claim objClaimNew = null;
            Claim objClaim = null;
            SysSettings objSysSettings = null;//Deb Multi Currency
            ColLobSettings objColLobSettings = null;
            XmlElement objXmlHideEnhancedNotes = null;
            try //Main Try Block
            {
                //Start XML tree
                objXmlDoc = new XmlDocument();
                objXmlElement = objXmlDoc.CreateElement("claimreserves");
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objXmlDoc.AppendChild(objXmlElement);
                objXmlElement.SetAttribute("claimid", p_iClaimId.ToString());
                objXmlElement.SetAttribute("claimant", p_iClaimantEID.ToString());
                objXmlElement.SetAttribute("unit", p_iUnitID.ToString());
                //Deb Multicurrency
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//rkaur27
                int iCurrType = p_iCurrType;
                bool IsBaseCurrTypeSelected = false;
                if (p_iCurrType <= 0)
                {
                    XmlElement objCurrencyListElement = null;
                    CreateElement(objXmlDoc.DocumentElement, "CurrencyTypeList", ref objCurrencyListElement);
                    int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                    int iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                    if (iClaimCurrCode > 0 && objSysSettings.UseMultiCurrency==-1)//skhare7 MITS 27671
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iClaimCurrCode);
                        if(objSysSettings.UseMultiCurrency != 0)
                         CreateAndSetElement(objCurrencyListElement, "Item", "Claim Currency: " + sClaimCurr.ToString(), iClaimCurrCode.ToString());
                        p_iCurrType = int.Parse(iClaimCurrCode.ToString());
                        objXmlElement.SetAttribute("IsClaimCurrencySet", "1");//rupal
                    }
                    else
                    {
                        IsBaseCurrTypeSelected = true;
                        objXmlElement.SetAttribute("IsClaimCurrencySet", "0");//rupal
                    }
                    if (iBaseCurrCode > 0)
                    {
                        object sBaseCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        CreateAndSetElement(objCurrencyListElement, "Item", "Base Currency: " + sBaseCurr.ToString(), iBaseCurrCode.ToString());
                    }
                }
                else
                {
                    int iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                    if (iBaseCurrCode > 0)
                    {
                        if (p_iCurrType == iBaseCurrCode)
                        {
                            IsBaseCurrTypeSelected = true;
                        }
                    }
                }
                //rupal:multicurrency
                if (objSysSettings.UseMultiCurrency == 0)
                {
                    objXmlElement.SetAttribute("IsClaimCurrencySet", "");
                }
                //rupal
                
                // JP 11.20.2005   Moved to below.     if (m_iSecurityId != 0)
                // JP 11.20.2005   Moved to below.     	objXmlElement.SetAttribute("sid", m_iSecurityId.ToString());

                // JP 11.20.2005   Moved to below.     if (m_iParentSecurityId != 0)
                // JP 11.20.2005   Moved to below.     	objXmlElement.SetAttribute("psid", m_iParentSecurityId.ToString());

                sbSQL = new StringBuilder();

                //Table[0]
                sbSQL.Append("SELECT CLAIM.LINE_OF_BUS_CODE,CLAIM.CLAIM_NUMBER,CODES.RELATED_CODE_ID,");
                sbSQL.Append("CLAIM.CLAIM_TYPE_CODE, CLAIM.CLAIM_STATUS_CODE, CLAIM.PAYMNT_FROZEN_FLAG FROM CLAIM, CODES WHERE CLAIM.CLAIM_STATUS_CODE = CODES.CODE_ID");
                sbSQL.Append(" AND CLAIM_ID = " + p_iClaimId);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        //Get the value of "Line Of Business"
                        sLOB = objReader.GetValue("LINE_OF_BUS_CODE").ToString();
                        if (p_iLOB == 0 && sLOB != "")
                            p_iLOB = Conversion.ConvertStrToInteger(sLOB);

                        //Get the value of Claim Number
                        if (m_sClaimNumber.Trim() == "")
                            m_sClaimNumber = objReader.GetValue("CLAIM_NUMBER").ToString();

                        //Get the value of Related Code Id
                        iCStat = Conversion.ConvertStrToInteger(objReader.GetValue("RELATED_CODE_ID").ToString());

                        //Get the value of Claim Type Code
                        iCType = Conversion.ConvertStrToInteger(objReader.GetValue("CLAIM_TYPE_CODE").ToString());
                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
                        objXmlElement.SetAttribute("claimstatus", objCache.GetShortCode(objReader.GetInt32("CLAIM_STATUS_CODE")));
                        objXmlElement.SetAttribute("paymentfrozenflag", Conversion.ConvertObjToStr(objReader.GetValue("PAYMNT_FROZEN_FLAG")));
                        objCache.Dispose();
                        objCache = null;
                    }
                    objReader.Close();

                }//if (objReader != null)
                sbSQL.Remove(0, sbSQL.Length);

                // Derive security ids from how this function is called    JP 11.20.2005
                GetSecurityIDS(p_iClaimId, p_iClaimantEID, p_iUnitID, p_iLOB);

                // BSB - Start support for Reserve Worksheet (RSW) custom extension.
                object[] ctorParams = new object[1];
                ctorParams[0] = m_userLogin;
                IReserveWorksheetManager pRSWManager = Riskmaster.Application.Extensibility.ExtensionFactory.Get("IReserveWorksheetManager", ctorParams,m_iClientId) as IReserveWorksheetManager;
                bool bUserRSWPermission = false;
                if (pRSWManager != null)
                    bUserRSWPermission = pRSWManager.RSWSecurityIsAllowed(m_iParentSecurityId);
                objXmlElement.SetAttribute("ShowRSW", bUserRSWPermission ? "1" : "0");
                // BSB - End for RSW

                //Added Rakhi for MITS 13804-START:Control Display of Schedule Checks
                if (m_userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK))
                {
                    objXmlElement.SetAttribute("isschedulecheck", "1");
                    bool bViewOnly = false;
                    bViewOnly = m_userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMPermissions.RMO_VIEW) && !m_userLogin.IsAllowedEx(RMB_FUNDS_AUTOCHK, RMPermissions.RMO_CREATE);
                    objXmlElement.SetAttribute("viewonly", bViewOnly ? "1" : "0");
                }
                else
                {
                    objXmlElement.SetAttribute("isschedulecheck", "0");
                }
                //Added Raman for fetching claimanteid for WC and DI
                if (p_iLOB == 243 || p_iLOB == 844)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                    using (Claim objClm = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false))
                    {
                        objClm.MoveTo(p_iClaimId);
                        p_iClaimantEID = objClm.PrimaryPiEmployee.PiEid;
                    }

                }

                //Added Rakhi for MITS 13804-END:Control Display of Schedule Checks
                //Table[1] for UseTandE
                bSecTandE = GetTAndESecurity(m_iParentSecurityId);
                sbSQL.Append(" SELECT USETANDE FROM SYS_PARMS ");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        //Set the value for IsTandE
                        iUseTandE = Conversion.ConvertStrToInteger(objReader.GetValue("USETANDE").ToString());
                        if (bSecTandE && (p_bIsTandE && iUseTandE != 0))
                            objXmlElement.SetAttribute("istande", "1");
                        else
                            objXmlElement.SetAttribute("istande", "0");
                    }

                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);
                //Geeta 06/11/07 : Starts Fixing MITS 9496				
                if (!m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_ACCESS) ||
                    !m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_VIEW))
                {
                    throw new PermissionViolationException(RMPermissions.RMO_ACCESS, m_iSecurityId);
                }
                //Geeta 06/11/07 : Ends Fixing MITS 9496

                // Add security ids to instance doc
                if (m_iSecurityId != 0)
                    objXmlElement.SetAttribute("sid", m_iSecurityId.ToString());

                if (m_iParentSecurityId != 0)
                    objXmlElement.SetAttribute("psid", m_iParentSecurityId.ToString());

                //Table[0]
                //Shruti for MITS 8551
                sbSQL.Append("SELECT RESERVE_TRACKING, COLL_IN_RSV_BAL,PER_RSV_COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL,BAL_TO_ZERO , NEG_BAL_TO_ZERO ,SET_TO_ZERO, COLL_IN_INCUR_BAL, RES_BY_CLM_TYPE "); //Parijat: MITS 7977
                sbSQL.Append("FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + p_iLOB);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iReserveTracking = Conversion.ConvertStrToInteger(
                            objReader.GetValue("RESERVE_TRACKING").ToString());

                        bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_RSV_BAL").ToString()), m_iClientId);
                        //Parijat: Mits 7977
                        bBalTOZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("BAL_TO_ZERO").ToString()), m_iClientId);
                        bNegBalToZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("NEG_BAL_TO_ZERO").ToString()), m_iClientId);
                        bSetToZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("SET_TO_ZERO").ToString()), m_iClientId);

                        if (!(bBalTOZero || bNegBalToZero || bSetToZero))
                            do_NOTHING = true;
                        //Shruti for MITS 8551
                        bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), m_iClientId);

                        bResByClaimType = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_BY_CLM_TYPE").ToString()), m_iClientId);
                        bPerRsvColInRsvBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                        bPerRsvColInIncrBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870
                    }
                    objReader.Close();

                }//if (objReader != null)
                sbSQL.Remove(0, sbSQL.Length);

                //if parent is 'General Claims' or 'Vehicle Accident' then 
                //set detail tracking to true
                //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
				//if (m_iParentSecurityId == GENERALCLAIMPSID || m_iParentSecurityId == VEHICLEACCIDENTPSID)
                if (m_iParentSecurityId == GENERALCLAIMPSID || m_iParentSecurityId == VEHICLEACCIDENTPSID || m_iParentSecurityId == PROPERTYCLAIMPSID)
                //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                {
                    //bDetailTrackingClaimLevel = true;
                    //iReserveTracking = 1;
                    bDetailTrackingClaimLevel = (iReserveTracking == 1); // JP 11.20.2005
                }

                if (p_iClaimantEID > 0)
                    sbSQL.Append(" SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + p_iClaimantEID);
                else if (p_iUnitID > 0)
                    sbSQL.Append(" SELECT VEHICLE_MAKE,VEHICLE_MODEL FROM VEHICLE WHERE UNIT_ID = " + p_iUnitID);
                else // TBD -1
                    sbSQL.Append(" SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = -1 ");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        //Caller will never send both ClaimaintID and UnitID
                        string sLob = string.Empty;
                        switch (p_iLOB)
                        {
                            case 241:
                                sLob = "General Claim";
                                break;
                            case 242:
                                sLob = "Vehicle Accident";
                                break;
                            case 844:
                                sLob = "Non Occupational";
                                break;
                            //Start-Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
                            case 845:
                                sLob = "Property Claim";
                                break;
                            //End-//Mridul Bansal. 01/04/10. MITS#18230. Support for Property Claims.
                            default:
                                sLob = "Workers' Compensation";
                                break;

                        }
                        //nadim for 15014
                        if (m_objDataModelFactory == null)
                        {
                            m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                        }

                        objClaimNew = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaimNew.MoveTo(p_iClaimId);
                        //nadim for 15014
                        if (p_iClaimantEID > 0)
                        {
                            //nadim for 15014-start
                            //sCaption = objReader.GetValue("LAST_NAME").ToString();
                            //sCaption += " " + objReader.GetValue("FIRST_NAME").ToString();
                            //sCaption = m_sClaimNumber + " *  Claimant: " + sCaption.Trim();
                            //sCaption = sLob + " [" + sCaption + "]";
                            //sCaption = sLob + GetFormTitle(objClaimNew);
                            sCaption = GetClaimantFormTitle(objClaimNew, p_iClaimantEID);
                            if (objClaimNew != null)
                            {
                                objClaimNew.Dispose();
                                objClaimNew = null;
                            }
                            //nadim for 15014-End
                        }
                        else if (p_iUnitID > 0)
                        {//nadim for 15014-start
                            sCaption = objReader.GetValue("VEHICLE_MAKE").ToString();
                            sCaption += " " + objReader.GetValue("VEHICLE_MODEL").ToString();

                            sCaption = m_sClaimNumber + " *  Unit Involved: " + sCaption.Trim();
                            sCaption = sLob + " [" + sCaption + "]";
                            //nadim for 15014-End
                        }
                        if (objReader != null)
                        {
                            objReader.Close();
                        }
                    }//end if (objReader.Read())
                    else
                    {
                        if (m_objDataModelFactory == null)
                        {
                            m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                        }

                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(p_iClaimId);
                        //Start by Shivendu for MITS 11594   
                        if (m_iParentSecurityId == GENERALCLAIMPSID)
                        {
                            if (!bDetailTrackingClaimLevel)
                            {
                                if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                                {
                                    sCaption = "General Claim " + GetFormTitle(objClaim);

                                    //sCaption = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                                    //sCaption = "General Claim " + m_sClaimNumber + " *  Claimant: " + sCaption.Trim();

                                    //if(m_objDataModelFactory!=null)
                                    //   m_objDataModelFactory.Dispose();
                                    //if (objClaim != null)
                                    //   objClaim.Dispose();
                                }
                                else
                                    //nadim for 15014
                                    //sCaption = "General Claim " + m_sClaimNumber + "";
                                    sCaption = "General Claim " + GetFormTitle(objClaim);
                            }
                            else
                                //nadim for 15014
                                //sCaption = "General Claim " + m_sClaimNumber + "";
                                sCaption = "General Claim " + GetFormTitle(objClaim);
                        }
                        //End by Shivendu for MITS 11594
                        //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                        else if (m_iParentSecurityId == PROPERTYCLAIMPSID)
                        {
                            if (!bDetailTrackingClaimLevel)
                            {
                                if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                                {
                                    sCaption = "Property Claim " + GetFormTitle(objClaim);

                                    //sCaption = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                                    //sCaption = "Property Claim " + m_sClaimNumber + " *  Claimant: " + sCaption.Trim();

                                    //if(m_objDataModelFactory!=null)
                                    //   m_objDataModelFactory.Dispose();
                                    //if (objClaim != null)
                                    //   objClaim.Dispose();
                                }
                                else
                                    //nadim for 15014
                                    //sCaption = "Property Claim " + m_sClaimNumber + "";
                                    sCaption = "Property Claim " + GetFormTitle(objClaim);
                            }
                            else
                                //nadim for 15014
                                //sCaption = "General Claim " + m_sClaimNumber + "";
                                sCaption = "Property Claim " + GetFormTitle(objClaim);
                        }
                        //End-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                        else if (m_iParentSecurityId == VEHICLEACCIDENTPSID)
                        {
                            if (!bDetailTrackingClaimLevel)
                            {
                                if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                                {
                                    sCaption = "Vehicle Accident " + GetFormTitle(objClaim);

                                    //sCaption = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                                    //sCaption = "Vehicle Accident " + m_sClaimNumber + " *  Claimant: " + sCaption.Trim();

                                    //if(m_objDataModelFactory!=null)
                                    //   m_objDataModelFactory.Dispose();
                                    //if (objClaim != null)
                                    //   objClaim.Dispose();
                                }
                                else
                                    //nadim for 15014
                                    //sCaption = "Vehicle Accident " + m_sClaimNumber + "";
                                    sCaption = "Vehicle Accident " + GetFormTitle(objClaim);
                            }
                            else
                                //nadim for 15014
                                //sCaption = "Vehicle Accident " + m_sClaimNumber + "";
                                sCaption = "Vehicle Accident " + GetFormTitle(objClaim);


                            ////////Changed Rakhi for MITS:12271-To display Claimant Name in Caption-Start 
                            ////////sCaption = "Vehicle Accident " + m_sClaimNumber + "";
                            //////int iLength = sFormTitle.LastIndexOf("]") - (sFormTitle.LastIndexOf("*") + 1);
                            //////string sClaimant = sFormTitle.Substring(sFormTitle.LastIndexOf("*") + 1, iLength);
                            //////sCaption = "Vehicle Accident " + m_sClaimNumber;
                            //////if(sClaimant.Trim() != "")
                            //////    sCaption += " *  Claimant: " + sClaimant;
                            ////////Changed Rakhi for MITS:12271-To display Claimant Name in Caption-END 
                        }
                        else if (m_iParentSecurityId == NONOCCPSID)
                        {
                            sCaption = "Non-Occupational " + GetFormTitle(objClaim);
                        }
                        else
                        {
                            sCaption = "Workers' Compensation " + GetFormTitle(objClaim);
                        }
                        //nadim for 15014
                        if (objClaim != null)
                        {
                            objClaim.Dispose();
                            objClaim = null;
                        }
                        //nadim for 15014
                        if (bDetailTrackingClaimLevel)
                            sCaption += " * Aggregate for Claim, Detail-Level Tracking is On";
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                    }

                }//end if (objReader != null)

                sbSQL.Remove(0, sbSQL.Length);

                objXmlElement.SetAttribute("claimnumber", m_sClaimNumber);
                objXmlElement.SetAttribute("caption", sCaption);
                // Start Naresh MITS 8532 1/3/2007
                sSubTitle = sCaption.Replace("'", "\\'");
                objXmlElement.SetAttribute("subtitle", sSubTitle);
                // End Naresh MITS 8532 1/3/2007
                //pmittal5  MITS 9906  04/11/08
                objXmlElement.SetAttribute("FormTitle", sFormTitle);

                sbSQL.Append(" SELECT RES_DO_NOT_RECALC, RES_SHOW_ORPHANS FROM SYS_PARMS");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bResShowOrphans = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_SHOW_ORPHANS").ToString()), m_iClientId);

                        bDoNotRecalcReserves = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_DO_NOT_RECALC").ToString()), m_iClientId);
                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);

                //Table[0]
                sbSQL.Append("SELECT RESERVE_TYPE_CODE, CODE_DESC, CODES_TEXT.SHORT_CODE");
                //Aman ML Changes--start
                if (bResByClaimType)
                {
                    //sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT ");
                    //sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    //sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOB);
                    //sbSQL.Append(" AND CLAIM_TYPE_CODE = " + iCType);
                    //sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033" );
                    //sbSQL.Append(" ORDER BY CODE_DESC ");
                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT,CODES");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND");
                    sbSQL.Append(" SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + p_iLOB);
                    // akaushik5 Added for MITS 33511 Starts
                    sbSQL.Append(" AND SYS_CLM_TYPE_RES.CLAIM_TYPE_CODE = " + iCType);
                    // akaushik5 Added for MITS 33511 Ends
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE =  1033 ");
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }
                else // Get all reserve buckets that are available
                {
                    //sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                    //sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    //sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOB);                 
                    //sbSQL.Append(" ORDER BY CODE_DESC ");
                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT, CODES ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + p_iLOB);
                    sbSQL.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE =  1033 ");
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE",this.LanguageCode); //Aman ML Change--end
                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                if (objDataSet.Tables[0] != null)
                {
                    arrReserves = new structReserves[objDataSet.Tables[0].Rows.Count];
                    //Populate the values in Reserves Struct
                    for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                    {
                        objDataRow = objDataSet.Tables[0].Rows[iCnt];
                        arrReserves[iCnt].iReserveTypeCode =
                            Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                        //arrReserves[iCnt].sReserveDesc = objDataRow["SHORT_CODE"].ToString() + " " + objDataRow["CODE_DESC"].ToString();
                        arrReserves[iCnt].sReserveDesc =objDataRow[2].ToString() + " " + objDataRow[1].ToString();
                    }
                    //Destroy the Data Row object, so that it can be re-used.
                    objDataRow = null;
                }//if (objDataSet.Tables[0]!= null)

                sbSQL.Remove(0, sbSQL.Length);

                //Aman ML Changes --Start
                //Build Query for Orphaned Reserve Types
                if (bResShowOrphans)
                {
                    //sbSQL.Append(" SELECT RESERVE_TYPE_CODE,CODE_DESC FROM RESERVE_CURRENT,CODES_TEXT ");
                    //sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    //sbSQL.Append(" CLAIM_ID = " + p_iClaimId);

                    sbSQL.Append(" SELECT RESERVE_TYPE_CODE,CODE_DESC,CODES_TEXT.SHORT_CODE FROM RESERVE_CURRENT,CODES_TEXT,CODES ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND ");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" CLAIM_ID = " + p_iClaimId);
                    if (p_iClaimantEID != 0)
                        sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);
                    else if (p_iUnitID != 0)
                        sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);

                    sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, CODE_DESC, CODES_TEXT.SHORT_CODE ");
                    sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE", this.LanguageCode);  //Aman ML Changes --end
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                }
                // JP 06.15.2006    Not needed.    else //TBD Just to make sure that Table[1] gets created
                // JP 06.15.2006	sbSQL.Append(" SELECT 1, 2 FROM RESERVE_CURRENT,CODES_TEXT");


                // Orphaned Reserve Types
                if (bResShowOrphans && objDataSet.Tables[0] != null)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (int iTempCount = 0; iTempCount < objDataSet.Tables[0].Rows.Count; iTempCount++)
                        {
                            iCnt = arrReserves.Length;
                            objDataRow = objDataSet.Tables[0].Rows[iTempCount];
                            //Check for Duplicate Reserve Type Code
                            for (int iTempCnt = 0; iTempCnt < arrReserves.Length; iTempCnt++)
                            {
                                if (arrReserves[iTempCnt].iReserveTypeCode ==
                                    Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString()))
                                    goto MoveNext;
                            }

                            structReserves[] arrTempReserves = new structReserves[iCnt + 1];
                            //Copy all the contents of original array to Temp Array for initialising the temp array
                            arrReserves.CopyTo(arrTempReserves, 0);
                            arrReserves = null;

                            arrTempReserves[iCnt].iReserveTypeCode =
                                Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                            arrTempReserves[iCnt].sReserveDesc = objDataRow["SHORT_CODE"].ToString() + " " + objDataRow["CODE_DESC"].ToString();

                            //Destroy the Temp Array and copy it's contents to Original array
                            arrReserves = new structReserves[arrTempReserves.Length];
                            arrTempReserves.CopyTo(arrReserves, 0);
                            arrTempReserves = null;

                            //iCnt = iCnt + 1;

                            //Move on to the next record
                        MoveNext: ;
                        }//for Loop						
                    }//if Table[1] has rows
                }//if(bResShowOrphans && objDataSet.Tables[1]!= null)

                sbSQL.Remove(0, sbSQL.Length);
                iCnt = 0;
                objDataRow = null;

                int iAddRecoveryReservetoTotalBalanceAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalBalanceAmount; //asharma326 JIRA 871
                int iAddRecoveryReservetoTotalIncurredAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalIncurredAmount; //asharma326 JIRA 872
                //Fixed JIRA-4823
                //if (bDoNotRecalcReserves)
                //{
                //    sbSQL.Append("SELECT * FROM RESERVE_CURRENT WHERE CLAIM_ID = " + p_iClaimId);
                //    if (p_iClaimantEID != 0)
                //        sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);
                //    else if (p_iUnitID != 0)
                //        sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);

                //    sbSQL.Append(" ORDER BY RESERVE_TYPE_CODE, DATE_ENTERED DESC ");
                //}
                //Deb Multi Currency
                //SysSettings objSysSettings = null;
                //objSysSettings = new SysSettings(m_sConnectionString);
                if (objSysSettings.UseMultiCurrency != 0)
                {
                    if (IsBaseCurrTypeSelected)//if !(bDoNotRecalcReserves)
                    {
                        sbSQL.Append("SELECT SUM(BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(INCURRED_AMOUNT) INCURRED_AMOUNT,RESERVE_TYPE_CODE, RES_STATUS_CODE, SUM(RESERVE_AMOUNT)  RES_AMOUNT,");
                        sbSQL.Append(" SUM(PAID_TOTAL)  PAID_TOTAL, SUM(COLLECTION_TOTAL)  COLL_TOTAL ");
                        sbSQL.Append(" FROM RESERVE_CURRENT ");
                        sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
                        if (p_iClaimantEID != 0)
                            sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);
                        else if (p_iUnitID != 0)
                            sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);

                        sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, RES_STATUS_CODE ");
                        eNavType = CommonFunctions.NavFormType.None;
                    }
                    else
                    {
                        sbSQL.Append("SELECT SUM(CLAIM_CURRENCY_BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(CLAIM_CURRENCY_INCURRED_AMOUNT) INCURRED_AMOUNT,RESERVE_TYPE_CODE, RES_STATUS_CODE, SUM(CLAIM_CURRENCY_RESERVE_AMOUNT)  RES_AMOUNT,");
                        sbSQL.Append(" SUM(CLAIM_CURRENCY_PAID_TOTAL)  PAID_TOTAL, SUM(CLAIM_CURR_COLLECTION_TOTAL)  COLL_TOTAL ");
                        sbSQL.Append(" FROM RESERVE_CURRENT ");
                        sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
                        if (p_iClaimantEID != 0)
                            sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);
                        else if (p_iUnitID != 0)
                            sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);

                        sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, RES_STATUS_CODE ");
                        eNavType = CommonFunctions.NavFormType.Reserve;
                    }
                }
                else
                {
                    sbSQL.Append("SELECT SUM(BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(INCURRED_AMOUNT) INCURRED_AMOUNT,RESERVE_TYPE_CODE, RES_STATUS_CODE, SUM(RESERVE_AMOUNT)  RES_AMOUNT,");
                    sbSQL.Append(" SUM(PAID_TOTAL)  PAID_TOTAL, SUM(COLLECTION_TOTAL)  COLL_TOTAL ");
                    sbSQL.Append(" FROM RESERVE_CURRENT ");
                    sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
                    if (p_iClaimantEID != 0)
                        sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);
                    else if (p_iUnitID != 0)
                        sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);

                    sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, RES_STATUS_CODE ");
                    eNavType = CommonFunctions.NavFormType.None;
                }
                //Deb
                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);

                if (bDoNotRecalcReserves && objDataSet.Tables[0] != null)
                {
                    //Special reserve processing
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iTemp = 0; iTemp < objDataSet.Tables[0].Rows.Count; iTemp++)
                        {
                            for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                            {
                                iStatus = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RES_STATUS_CODE"].ToString());
                                iCodeID = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RESERVE_TYPE_CODE"].ToString());

                                if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                                {
                                    objDataRow = objDataSet.Tables[0].Rows[iTemp];
                                    arrReserves[iCnt].dBalance += Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                                    arrReserves[iCnt].dTotalPaid += Conversion.ConvertStrToDouble(objDataRow["PAID_TOTAL"].ToString());
                                    arrReserves[iCnt].dTotalCollected += Conversion.ConvertStrToDouble(objDataRow["COLL_TOTAL"].ToString());
                                    arrReserves[iCnt].dTotalIncurred += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());
                                    if (objCache == null)  //MITS 35029
                                    {
                                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
                                    }

                                    if (objCache.GetRelatedShortCode(arrReserves[iCnt].iReserveTypeCode) == "R")    //MITS 35029
                                    {
                                        //asharma326 JIRA 871
                                        if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                        {
                                            dBalTotal += Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());  //MITS 35029
                                        }
                                        else
                                        {
                                            dBalTotal -= Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());  //MITS 35029
                                        }
                                        //asharma326 JIRA 872
                                        if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                            dIncTotal += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837
                                        else
                                            dIncTotal -= Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837
                                    }
                                    else
                                    {
                                        dBalTotal += Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                                        dIncTotal += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837
                                    }
                                    dPaidTotal += Conversion.ConvertStrToDouble(objDataRow["PAID_TOTAL"].ToString());
                                    dCollTotal += Conversion.ConvertStrToDouble(objDataRow["COLL_TOTAL"].ToString());
                                    //dIncTotal += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837

                                    //rsharma220 MITS 34529 start
                                    if (iStatus != 0)
                                    {

                                        if (objCache == null)  // only allocate once and then use for subsequent ones
                                            objCache = new LocalCache(m_sConnectionString, m_iClientId);

                                        if (arrReserves[iCnt].sStatusCode != "")
                                        {
                                            arrReserves[iCnt].sStatusCode = arrReserves[iCnt].sStatusCode;
                                        }
                                        //MITS 25011 mcapps2 Start - I don't think we should be displaying both status codes, that is why we allow them to look at the history
                                        //arrReserves[iCnt].sStatusCode = arrReserves[iCnt].sStatusCode + objCache.GetShortCode(iStatus);
                                        ////rupal
                                        //arrReserves[iCnt].sStatusCodeDesc = arrReserves[iCnt].sStatusCodeDesc + objCache.GetCodeDesc(iStatus);
                                        arrReserves[iCnt].sStatusCode = objCache.GetShortCode(iStatus);
                                        arrReserves[iCnt].sStatusCodeDesc = objCache.GetCodeDesc(iStatus);
                                        //MITS 25011 mcapps2 End
                                    }
                                    else
                                    {
                                        arrReserves[iCnt].sStatusCode = "-";
                                        //rupal
                                        arrReserves[iCnt].sStatusCodeDesc = "-";
                                    }
                                    //rsharma220 MITS 34529 End
                                    //Exit the inner for loop
                                    break;
                                }//if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                            }//for Loop
                        }//for(iTemp=0; iTemp < objDataSet.Tables[0].Rows.Count;iTemp++)
                    }//Special reserve processing
                }//if (bDoNotRecalcReserves && objDataSet.Tables[0] != null)
                else if (objDataSet.Tables[0] != null)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iTemp = 0; iTemp < objDataSet.Tables[0].Rows.Count; iTemp++)
                        {
                            for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                            {
                                iStatus = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RES_STATUS_CODE"].ToString());
                                iCodeID = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RESERVE_TYPE_CODE"].ToString());

                                if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                                {
                                    objDataRow = objDataSet.Tables[0].Rows[iTemp];
                                    dRAmount = Conversion.ConvertStrToDouble(
                                        objDataRow["RES_AMOUNT"].ToString());
                                    dRPaid = Conversion.ConvertStrToDouble(
                                        objDataRow["PAID_TOTAL"].ToString());
                                    dRCollect = Conversion.ConvertStrToDouble(
                                        objDataRow["COLL_TOTAL"].ToString());

                                    dRBalance = CalculateReserveBalance(dRAmount, dRPaid, dRCollect, bCollInRsvBal, iCStat, iCodeID, bPerRsvColInRsvBal, p_iLOB);//asharma326 JIRA 870 add bPerRsvColInRsvBal, p_iLOB
                                    dRIncurred = CalculateIncurred(dRBalance, dRPaid, dRCollect, bCollInRsvBal, bCollInIncurredBal, iCodeID, bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB);//Shruti for MITS 8551//asharma326 JIRA 870 add bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB
                                    if (objCache == null)  //MITS 35029
                                    {
                                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
                                    }

                                                                       
                                    arrReserves[iCnt].dBalance += dRBalance;
                                    arrReserves[iCnt].dTotalPaid += dRPaid;
                                    arrReserves[iCnt].dTotalCollected += dRCollect;
                                    arrReserves[iCnt].dTotalIncurred += dRIncurred;

                                    if (iStatus != 0)
                                    {

                                        if (objCache == null)  // only allocate once and then use for subsequent ones
                                            objCache = new LocalCache(m_sConnectionString, m_iClientId);

                                        if (arrReserves[iCnt].sStatusCode != "")
                                        {
                                            arrReserves[iCnt].sStatusCode = arrReserves[iCnt].sStatusCode;
                                        }
                                        //MITS 25011 mcapps2 Start - I don't think we should be displaying both status codes, that is why we allow them to look at the history
                                        //arrReserves[iCnt].sStatusCode = arrReserves[iCnt].sStatusCode + objCache.GetShortCode(iStatus);
                                        ////rupal
                                        //arrReserves[iCnt].sStatusCodeDesc = arrReserves[iCnt].sStatusCodeDesc + objCache.GetCodeDesc(iStatus);
                                        arrReserves[iCnt].sStatusCode = objCache.GetShortCode(iStatus);
                                        arrReserves[iCnt].sStatusCodeDesc = objCache.GetCodeDesc(iStatus);
                                        //MITS 25011 mcapps2 End
                                    }
                                    else
                                    {
                                        arrReserves[iCnt].sStatusCode = "-";
                                        //rupal
                                        arrReserves[iCnt].sStatusCodeDesc = "-";
                                    }

                                    // dBalTotal += arrReserves[iCnt].dBalance;
                                    // dPaidTotal += arrReserves[iCnt].dTotalPaid;
                                    // dCollTotal += arrReserves[iCnt].dTotalCollected;
                                    // dIncTotal += arrReserves[iCnt].dTotalIncurred;
                                    // JP 1.4.2007: The above code didn't work if there was more than one dataset line per
                                    //              reserve (ex. multiple claimant reserves and one has a reserve status that is different from the other).
                                    if (objCache.GetRelatedShortCode(arrReserves[iCnt].iReserveTypeCode) == "R")    //MITS 35029
                                    {
                                        //asharma326 JIRA 871
                                        if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                        {
                                            dBalTotal += dRBalance;  //MITS 35029
                                        }
                                        else
                                        {
                                            dBalTotal -= dRBalance;  //MITS 35029
                                        }
                                        //asharma326 JIRA 872
                                        if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                            dIncTotal += dRIncurred;  //MITS 35837
                                        else
                                            dIncTotal -= dRIncurred;  //MITS 35837
                                    }
                                    else
                                    {
                                        dBalTotal += dRBalance;
                                        dIncTotal += dRIncurred;  //MITS 35837
                                    }
                                    //dBalTotal += dRBalance;
                                    dPaidTotal += dRPaid;
                                    dCollTotal += dRCollect;
                                    //dIncTotal += dRIncurred;  //MITS 35837

                                    //Exit the inner for loop
                                    break;
                                }//if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                            }//for Loop
                        }//for(iTemp=0; iTemp < objDataSet.Tables[0].Rows.Count;iTemp++)
                    }//if(objDataSet.Tables[0].Rows.Count >0)
                }//Else if !(bDoNotRecalcReserves)

                sbSQL.Remove(0, sbSQL.Length);


                iCnt = 0;

                //Format Results into XML
                objXmlReservesElement = objXmlDoc.CreateElement("reserves");
                objXmlElement.AppendChild(objXmlReservesElement);
                objXmlReservesElement.SetAttribute("claimid", p_iClaimId.ToString());
                objXmlReservesElement.SetAttribute("claimant", p_iClaimantEID.ToString());
                objXmlReservesElement.SetAttribute("unit", p_iUnitID.ToString());

                //Start-Mridul Bansal. 01/04/10. MITS#19314. Support for Property Claims.
                //if((m_iParentSecurityId == GENERALCLAIMPSID || m_iParentSecurityId == VEHICLEACCIDENTPSID) && 
                //    bDetailTrackingClaimLevel)
                if ((m_iParentSecurityId == GENERALCLAIMPSID || m_iParentSecurityId == VEHICLEACCIDENTPSID || m_iParentSecurityId == PROPERTYCLAIMPSID) && bDetailTrackingClaimLevel)
                    sNodeName = "reservereadonly";
                else
                    sNodeName = "reserve";

                

                for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                {
                    //if (!(bDoNotRecalcReserves && arrReserves[iCnt].dBalance == 0 &&
                    //    arrReserves[iCnt].dTotalIncurred == 0 && arrReserves[iCnt].dTotalCollected == 0 &&
                    //    arrReserves[iCnt].dTotalPaid == 0))
                    //    bProcess = true;

                    //if (bProcess)
                    //{
                        objXmlReserveElement = objXmlDoc.CreateElement(sNodeName);
                        objXmlReservesElement.AppendChild(objXmlReserveElement);

                        objXmlReserveElement.SetAttribute("reservetypecode", arrReserves[iCnt].iReserveTypeCode.ToString());
                        objXmlReserveElement.SetAttribute("reservename", arrReserves[iCnt].sReserveDesc);
                        objXmlReserveElement.SetAttribute("reservestatus", arrReserves[iCnt].sStatusCode);
                        //rupal
                        objXmlReserveElement.SetAttribute("reservestatusdesc", arrReserves[iCnt].sStatusCodeDesc);

                        //objXmlReserveElement.SetAttribute("balance", String.Format("{0:c}", arrReserves[iCnt].dBalance));
                        //objXmlReserveElement.SetAttribute("incurred", String.Format("{0:c}", arrReserves[iCnt].dTotalIncurred));
                        //objXmlReserveElement.SetAttribute("paid", String.Format("{0:c}", arrReserves[iCnt].dTotalPaid));
                        //objXmlReserveElement.SetAttribute("collected", String.Format("{0:c}", arrReserves[iCnt].dTotalCollected));
                        //Deb : Multi-Currency
                        objXmlReserveElement.SetAttribute("balance", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dBalance, eNavType, m_sConnectionString, m_iClientId));
                        objXmlReserveElement.SetAttribute("incurred", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dTotalIncurred, eNavType, m_sConnectionString, m_iClientId));
                        objXmlReserveElement.SetAttribute("paid", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dTotalPaid, eNavType, m_sConnectionString, m_iClientId));
                        objXmlReserveElement.SetAttribute("collected", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dTotalCollected, eNavType, m_sConnectionString, m_iClientId));
                        //Deb: Multi-Currency
                        //13509 start:Asif
                        bProcess = false;
                        //13509 end:Asif

                    //}//if(bProcess)
                }//for loop

                objXmlReserveElement = objXmlDoc.CreateElement("totals");
                objXmlElement.AppendChild(objXmlReserveElement);

                //objXmlReserveElement.SetAttribute("balanceamount", String.Format("{0:c}", dBalTotal));
                //objXmlReserveElement.SetAttribute("incurredamount", String.Format("{0:c}", dIncTotal));
                //objXmlReserveElement.SetAttribute("paidtotal", String.Format("{0:c}", dPaidTotal));
                //objXmlReserveElement.SetAttribute("collectedtotal", String.Format("{0:c}", dCollTotal));

                //Deb MultiCurrency 
                objXmlReserveElement.SetAttribute("balanceamount", CommonFunctions.ConvertCurrency(p_iClaimId, dBalTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("incurredamount", CommonFunctions.ConvertCurrency(p_iClaimId, dIncTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("paidtotal", CommonFunctions.ConvertCurrency(p_iClaimId, dPaidTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("collectedtotal", CommonFunctions.ConvertCurrency(p_iClaimId, dCollTotal, eNavType, m_sConnectionString, m_iClientId));
                //Deb MultiCurrency 

                //XSL processing that needs to go in there will be handled at the UI side.

                //TR 2112 Raman Bhatia: Queued Payments Total should be displayed on financials screen

                bool bUseQueuedPayments = false;
                string sSQL = "";
                sSQL = "SELECT * FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    bUseQueuedPayments = objReader.GetBoolean("QUEUED_PAYMENTS");
                objReader.Close();

                //MWC 08/07/2003 If the payment is queued and the pending status in the supp field is Void we don't want to include it in Queued total
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString,m_iClientId);
                bool bPendingStatusPresent = false;
                double dQueued = 0;
                sSQL = "SELECT * FROM FUNDS_SUPP WHERE TRANS_ID = -1";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    for (int i = 0; i <= objReader.FieldCount; i++)
                    {
                        if (objReader.GetName(i) == "PEND_STATUS_CODE")
                            bPendingStatusPresent = true;
                    }
                }
                objReader.Close();
                if (bPendingStatusPresent)
                    sSQL = "SELECT SUM(AMOUNT) AMT FROM FUNDS, FUNDS_SUPP WHERE FUNDS.TRANS_ID = FUNDS_SUPP.TRANS_ID AND CLAIM_ID = " + p_iClaimId + " AND STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS") + " AND VOID_FLAG <> 0 AND PEND_STATUS_CODE <> " + objCCacheFunctions.GetCodeIDWithShort("V", "PENDING_STATUS");
                else
                    sSQL = "SELECT SUM(AMOUNT) AMT FROM FUNDS WHERE CLAIM_ID = " + p_iClaimId + " AND STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS") + " AND VOID_FLAG <> 0";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    objReader.Read();
                    dQueued = objReader.GetDouble("AMT");
                    objReader.Close();
                }

                //rupal:pending changes for multicurrency
                objXmlElement = (XmlElement)objXmlDoc.SelectSingleNode("//totals");
                objXmlElement.SetAttribute("UseQueuedPayments", bUseQueuedPayments.ToString());
                //objXmlElement.SetAttribute("QueuedTotal", string.Format("{0:c}", dQueued));
                //Deb MultiCurrency 
                objXmlElement.SetAttribute("QueuedTotal", CommonFunctions.ConvertCurrency(p_iClaimId, dQueued, eNavType, m_sConnectionString, m_iClientId));
                //Deb MultiCurrency 
                // 01/05/2007 Rem Umesh
                objXmlElement = (XmlElement)objXmlDoc.SelectSingleNode("//claimreserves");
                objXmlElement.SetAttribute("add_payment", "1");
                objXmlElement.SetAttribute("add_collection", "1");
                //mgaba2:mits 28450

                int iUseClaimProgressNotes = objColLobSettings[p_iLOB].ClmProgressNotesFlag;
                objXmlHideEnhancedNotes = objXmlDoc.CreateElement("HideEnhancedNotes");
                if (iUseClaimProgressNotes == 0)
                {
                    objXmlHideEnhancedNotes.InnerText = "True";
                }
                else
                {
                    objXmlHideEnhancedNotes.InnerText = "False";
                }
                objXmlElement.AppendChild(objXmlHideEnhancedNotes);
                objXmlDocument = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);//rkaur27
                if (!string.IsNullOrEmpty(strContent))
                {
                    objXmlDocument.LoadXml(strContent);

                    RemoveButton(objXmlDocument, objXmlDoc, "//RMAdminSettings/ReservesSettings/AddPayment", "//claimreserves/@add_payment");
                    RemoveButton(objXmlDocument, objXmlDoc, "//RMAdminSettings/ReservesSettings/AddCollection", "//claimreserves/@add_collection");

                }
                
                // End
            }//End of Main Try Block
            
            //Catching Permission Violation exception so that it is not embedded inside Generic exception
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.BOBReserves.GenericError", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objDataSet != null)
                    objDataSet.Dispose();

                if (objCache != null)
                    objCache.Dispose();
                objCache = null;
                if (objRdr != null)
                    objRdr.Dispose();
                objDataRow = null;
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                }
                if (objClaimNew != null)
                {
                    objClaimNew.Dispose();
                    objClaimNew = null;
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objSysSettings != null)
                {
                    objSysSettings = null;
                }
                if (objColLobSettings != null)
                {
                    objColLobSettings = null;
                }
            }

            //Return the result to calling function
            return objXmlDoc;
        }
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, string p_iRecordid)
        {
            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            objChildNode.SetAttribute("value", p_iRecordid.ToString());
        }
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
            p_objChildNode.InnerText = p_sText;
        }
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }
        //skhare7

        public XmlDocument GetBOBReserves(int p_iClaimId, int p_iClaimantEID,
                                      int p_iLOB, string sFormTitle)
        {
            //String Variables
            string sLOB = "";
            string sNodeName = "";
            string sCaption = "";
         
            string sSubTitle = "";
            StringBuilder sbSQL = null;

            //Integer Variables
            int iCStat = 0;
            int iCType = 0;
          
            int iReserveTracking = 0;
            int iStatus = 0;
            int iCodeID = 0;
            int iCnt = 0; // Variable used in For Loops
            int iTemp = 0;// Variable used in For Loops

            //Double Variables
            double dRAmount = 0;
            double dRPaid = 0;
            double dRCollect = 0;
            double dRBalance = 0;
            double dRIncurred = 0;
            double dBalTotal = 0;
            double dPaidTotal = 0;
            double dCollTotal = 0;
            double dIncTotal = 0;

            //Boolean
            bool bProcess = false;

            bool bCollInRsvBal = false, bPerRsvColInRsvBal = false;//asharma326 JIRA 870
            //Shruti for MITS 8551
            bool bCollInIncurredBal = false, bPerRsvColInIncrBal = false;//asharma326 JIRA 870
            bool bResByClaimType = false;
            bool bResShowOrphans = false;
            bool bDoNotRecalcReserves = false;
          
         
            bool bBalTOZero = false;
            bool bNegBalToZero = false;
            bool bSetToZero = false;

            //XML Object variables
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement;
            XmlElement objXmlReservesElement = null;
            XmlElement objXmlReserveElement = null;
            XmlDocument objXmlDocument = null;    

            //Database objects
            DataSet objDataSet = null;
            DbReader objReader = null;
            DataRow objDataRow = null;
            DbReader objRdr = null;

            structReserves[] arrReserves = null;
            LocalCache objCache = null;
            CCacheFunctions objCCacheFunctions = null;

            //Initialize the structReserves struct variables
            structReserves objReserve;
            objReserve.dBalance = 0;
            objReserve.dTotalCollected = 0;
            objReserve.dTotalIncurred = 0;
            objReserve.dTotalPaid = 0;
            objReserve.iReserveTypeCode = 0;
            objReserve.sReserveDesc = "";
            objReserve.sStatusCode = "";
            Claim objClaimNew = null;
            Claim objClaim = null;
            ColLobSettings objColLobSettings = null;//asharma326 JIRA 871
            SysSettings objSysSettings = null;
            try //Main Try Block
            {
                //rupal:start, r8 multicurrency
                //get currency type for claim (base currency? or claim currency?)
                bool IsBaseCurrTypeSelected = false;
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);

                int iClaimCurrCode = 0;
                int iBaseCurrCode = 0;
                if (p_iClaimId != 0)
                {
                    iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                    iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                    if (iClaimCurrCode > 0)
                    {
                        IsBaseCurrTypeSelected = false;
                    }
                    else
                    {
                        IsBaseCurrTypeSelected = true;
                    }

                }
                //rupal:end


                //Start XML tree
                objXmlDoc = new XmlDocument();
                objXmlElement = objXmlDoc.CreateElement("claimreserves");

                objXmlDoc.AppendChild(objXmlElement);
                objXmlElement.SetAttribute("claimid", p_iClaimId.ToString());
                objXmlElement.SetAttribute("claimant", p_iClaimantEID.ToString());
              

          
                sbSQL = new StringBuilder();

                //Table[0]
                sbSQL.Append("SELECT CLAIM.LINE_OF_BUS_CODE,CLAIM.CLAIM_NUMBER,CODES.RELATED_CODE_ID,");
                sbSQL.Append("CLAIM.CLAIM_TYPE_CODE, CLAIM.CLAIM_STATUS_CODE, CLAIM.PAYMNT_FROZEN_FLAG FROM CLAIM, CODES WHERE CLAIM.CLAIM_STATUS_CODE = CODES.CODE_ID");
                sbSQL.Append(" AND CLAIM_ID = " + p_iClaimId);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        //Get the value of "Line Of Business"
                        sLOB = objReader.GetValue("LINE_OF_BUS_CODE").ToString();
                        if (p_iLOB == 0 && sLOB != "")
                            p_iLOB = Conversion.ConvertStrToInteger(sLOB);

                        //Get the value of Claim Number
                        if (m_sClaimNumber.Trim() == "")
                            m_sClaimNumber = objReader.GetValue("CLAIM_NUMBER").ToString();

                        //Get the value of Related Code Id
                        iCStat = Conversion.ConvertStrToInteger(objReader.GetValue("RELATED_CODE_ID").ToString());

                        //Get the value of Claim Type Code
                        iCType = Conversion.ConvertStrToInteger(objReader.GetValue("CLAIM_TYPE_CODE").ToString());
                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
                        objXmlElement.SetAttribute("claimstatus", objCache.GetShortCode(objReader.GetInt32("CLAIM_STATUS_CODE")));
                      
                        objCache.Dispose();
                        objCache = null;
                    }
                    objReader.Close();

                }//if (objReader != null)
                sbSQL.Remove(0, sbSQL.Length);

              
                if (p_iLOB == 243 || p_iLOB == 844)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                    using (Claim objClm = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false))
                    {
                        objClm.MoveTo(p_iClaimId);
                        p_iClaimantEID = objClm.PrimaryPiEmployee.PiEid;
                    }

                }

            
                sbSQL.Remove(0, sbSQL.Length);

                sbSQL.Append("SELECT RESERVE_TRACKING, COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL,PER_RSV_COLL_IN_RSV_BAL,BAL_TO_ZERO , NEG_BAL_TO_ZERO ,SET_TO_ZERO, COLL_IN_INCUR_BAL, RES_BY_CLM_TYPE "); //Parijat: MITS 7977
                sbSQL.Append("FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + p_iLOB);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iReserveTracking = Conversion.ConvertStrToInteger(
                            objReader.GetValue("RESERVE_TRACKING").ToString());

                        bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_RSV_BAL").ToString()), m_iClientId);
                        //Parijat: Mits 7977
                        bBalTOZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("BAL_TO_ZERO").ToString()), m_iClientId);
                        bNegBalToZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("NEG_BAL_TO_ZERO").ToString()), m_iClientId);
                        bSetToZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("SET_TO_ZERO").ToString()), m_iClientId);

                        if (!(bBalTOZero || bNegBalToZero || bSetToZero))
                            do_NOTHING = true;
                        //Shruti for MITS 8551
                        bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), m_iClientId);

                        bResByClaimType = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_BY_CLM_TYPE").ToString()), m_iClientId);
                        bPerRsvColInRsvBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                        bPerRsvColInIncrBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870
                    }
                    objReader.Close();

                }//if (objReader != null)
                sbSQL.Remove(0, sbSQL.Length);

            

                if (p_iClaimantEID > 0)
                    sbSQL.Append(" SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + p_iClaimantEID);
             
                else // TBD -1
                    sbSQL.Append(" SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = -1 ");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        //Caller will never send both ClaimaintID and UnitID
                        string sLob = string.Empty;
                        switch (p_iLOB)
                        {
                            case 241:
                                sLob = "General Claim";
                                break;
                            case 242:
                                sLob = "Vehicle Accident";
                                break;
                          
                            case 845:
                                sLob = "Property Claim";
                                break;
                          
                            default:
                                sLob = "Workers' Compensation";
                                break;

                        }
                        //nadim for 15014
                        if (m_objDataModelFactory == null)
                        {
                            m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                        }

                        objClaimNew = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaimNew.MoveTo(p_iClaimId);
                      
                        if (p_iClaimantEID > 0)
                        {
                            // akaushik5 Changed for MITS 33577 Starts
                            //sCaption = GetClaimantFormTitle(objClaimNew, p_iClaimantEID);
                            sCaption = string.Format("{0} {1}", sLob, GetClaimantFormTitle(objClaimNew, p_iClaimantEID));
                            // akaushik5 Changed for MITS 33577 Ends

                            if (objClaimNew != null)
                            {
                                objClaimNew.Dispose();
                                objClaimNew = null;
                            }
                          
                        }
                      
                        if (objReader != null)
                        {
                            objReader.Close();
                        }
                    }//end if (objReader.Read())
                    else
                    {
                        if (m_objDataModelFactory == null)
                        {
                            m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                        }

                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(p_iClaimId);
                  
                        //if (p_iLOB == 241)
                        //{
                          
                        //        sCaption = "General Claim " + GetFormTitle(objClaim);
                        //}
                    
                        //else if (p_iLOB == 845)
                        //{
                           
                        //        sCaption = "Property Claim " + GetFormTitle(objClaim);
                        //}
                     
                        //else if (p_iLOB == 242)
                        //{
                           
                        //        sCaption = "Vehicle Accident " + GetFormTitle(objClaim);


                       
                        //}
                     
                        //else
                        //{
                        //    sCaption = "Workers' Compensation " + GetFormTitle(objClaim);
                        //}
                     
                        if (objClaim != null)
                        {
                            objClaim.Dispose();
                            objClaim = null;
                        }
                        //nadim for 15014
                       // if (bDetailTrackingClaimLevel)
                           // sCaption += " * Aggregate for Claim, Detail-Level Tracking is On";
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                    }

                }//end if (objReader != null)

                sbSQL.Remove(0, sbSQL.Length);

                objXmlElement.SetAttribute("claimnumber", m_sClaimNumber);
                objXmlElement.SetAttribute("caption", sCaption);
               
                sSubTitle = sCaption.Replace("'", "\\'");
                objXmlElement.SetAttribute("subtitle", sSubTitle);
               
                objXmlElement.SetAttribute("FormTitle", sFormTitle);

                sbSQL.Append(" SELECT RES_DO_NOT_RECALC, RES_SHOW_ORPHANS FROM SYS_PARMS");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bResShowOrphans = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_SHOW_ORPHANS").ToString()), m_iClientId);

                        bDoNotRecalcReserves = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_DO_NOT_RECALC").ToString()), m_iClientId);
                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);

                //Table[0]
                //Aman ML Changes --Start
                sbSQL.Append("SELECT RESERVE_TYPE_CODE, CODE_DESC,CODES.SHORT_CODE");
                if (bResByClaimType)
                {
                    //sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT");
                    //sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    //sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOB);
                    //sbSQL.Append(" AND CLAIM_TYPE_CODE = " + iCType);
                    //sbSQL.Append(" ORDER BY CODE_DESC ");
                    sbSQL.Append(" FROM SYS_CLM_TYPE_RES, CODES_TEXT,CODES");
                    sbSQL.Append(" WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES.CODE_ID AND");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + p_iLOB);
                    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + iCType);
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }
                else // Get all reserve buckets that are available
                {
                    //sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT ");
                    //sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    //sbSQL.Append(" LINE_OF_BUS_CODE = " + p_iLOB);
                    //sbSQL.Append(" ORDER BY CODE_DESC ");
                    sbSQL.Append(" FROM SYS_LOB_RESERVES, CODES_TEXT,CODES ");
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + p_iLOB);
                    sbSQL.Append(" ORDER BY CODE_DESC ");
                }
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE", this.LanguageCode);
                //Aman ML Changes --End
                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                if (objDataSet.Tables[0] != null)
                {
                    arrReserves = new structReserves[objDataSet.Tables[0].Rows.Count];
                    //Populate the values in Reserves Struct
                    for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                    {
                        objDataRow = objDataSet.Tables[0].Rows[iCnt];
                        arrReserves[iCnt].iReserveTypeCode =
                            Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                        //arrReserves[iCnt].sReserveDesc = objDataRow["SHORT_CODE"].ToString() + " " + objDataRow["CODE_DESC"].ToString();
                        arrReserves[iCnt].sReserveDesc = objDataRow[2].ToString() + " " + objDataRow[1].ToString(); //Aman ML Change
                    }
                    //Destroy the Data Row object, so that it can be re-used.
                    objDataRow = null;
                }//if (objDataSet.Tables[0]!= null)

                sbSQL.Remove(0, sbSQL.Length);


                //Build Query for Orphaned Reserve Types
                if (bResShowOrphans)
                {
                    //sbSQL.Append(" SELECT RESERVE_TYPE_CODE,CODE_DESC FROM RESERVE_CURRENT,CODES_TEXT ");
                    //sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND ");
                    //sbSQL.Append(" CLAIM_ID = " + p_iClaimId);
                    //kkaur8 added for MITS 33482 to remove SHORT_CODE ambibuity starts
                    //sbSQL.Append(" SELECT RESERVE_TYPE_CODE,CODE_DESC,SHORT_CODE FROM RESERVE_CURRENT,CODES_TEXT,CODES ");
                    sbSQL.Append(" SELECT RESERVE_TYPE_CODE,CODE_DESC,CODES.SHORT_CODE FROM RESERVE_CURRENT,CODES_TEXT,CODES ");
                    //kkaur8 added for MITS 33482 to remove SHORT_CODE ambibuity Ends
                    sbSQL.Append(" WHERE RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSQL.Append(" CODES.CODE_ID = CODES_TEXT.CODE_ID AND ");
                    sbSQL.Append(" CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sbSQL.Append(" CLAIM_ID = " + p_iClaimId);

                    if (p_iClaimantEID != 0)
                        sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);

                    //kkaur8 added for MITS 33482 to remove SHORT_CODE ambibuity starts
                    //sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, CODE_DESC ");
                    sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, CODE_DESC,CODES.SHORT_CODE ");
                    //kkaur8 added for MITS 33482 to remove SHORT_CODE ambibuity ends
                    sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_TYPE", this.LanguageCode); //Aman ML Changes --End
                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                }
          
                // Orphaned Reserve Types
                if (bResShowOrphans && objDataSet.Tables[0] != null)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (int iTempCount = 0; iTempCount < objDataSet.Tables[0].Rows.Count; iTempCount++)
                        {
                            iCnt = arrReserves.Length;
                            objDataRow = objDataSet.Tables[0].Rows[iTempCount];
                            //Check for Duplicate Reserve Type Code
                            for (int iTempCnt = 0; iTempCnt < arrReserves.Length; iTempCnt++)
                            {
                                if (arrReserves[iTempCnt].iReserveTypeCode ==
                                    Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString()))
                                    goto MoveNext;
                            }

                            structReserves[] arrTempReserves = new structReserves[iCnt + 1];
                            //Copy all the contents of original array to Temp Array for initialising the temp array
                            arrReserves.CopyTo(arrTempReserves, 0);
                            arrReserves = null;

                            arrTempReserves[iCnt].iReserveTypeCode =
                                Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                            arrTempReserves[iCnt].sReserveDesc = objDataRow["SHORT_CODE"].ToString() + " " + objDataRow["CODE_DESC"].ToString();

                            //Destroy the Temp Array and copy it's contents to Original array
                            arrReserves = new structReserves[arrTempReserves.Length];
                            arrTempReserves.CopyTo(arrReserves, 0);
                            arrTempReserves = null;

                            //iCnt = iCnt + 1;

                            //Move on to the next record
                        MoveNext: ;
                        }//for Loop						
                    }//if Table[1] has rows
                }//if(bResShowOrphans && objDataSet.Tables[1]!= null)

                sbSQL.Remove(0, sbSQL.Length);
                iCnt = 0;
                objDataRow = null;

                int iAddRecoveryReservetoTotalBalanceAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalBalanceAmount;//asharma326 JIRA 871
                int iAddRecoveryReservetoTotalIncurredAmount = objColLobSettings[p_iLOB].AddRecoveryReservetoTotalIncurredAmount;//asharma326 JIRA 872
                //if (bDoNotRecalcReserves)
                //{
                //    sbSQL.Append("SELECT * FROM RESERVE_CURRENT WHERE CLAIM_ID = " + p_iClaimId);
                //    if (p_iClaimantEID != 0)
                //        sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);


                //    sbSQL.Append(" ORDER BY RESERVE_TYPE_CODE, DATE_ENTERED DESC ");
                //}
                //rupal:start, r8 multicurrency
                
                if (objSysSettings.UseMultiCurrency != 0)
                {
                    if (IsBaseCurrTypeSelected) //if !(bDoNotRecalcReserves)
                    {
                        sbSQL.Append("SELECT SUM(BALANCE_AMOUNT) BALANCE_AMOUNT,SUM(INCURRED_AMOUNT) INCURRED_AMOUNT, RESERVE_TYPE_CODE, RES_STATUS_CODE, SUM(RESERVE_AMOUNT)  RES_AMOUNT,");
                        sbSQL.Append(" SUM(PAID_TOTAL)  PAID_TOTAL, SUM(COLLECTION_TOTAL)  COLL_TOTAL ");
                        sbSQL.Append(" FROM RESERVE_CURRENT ");
                        sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
                        if (p_iClaimantEID != 0)
                            sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);


                        sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, RES_STATUS_CODE ");
                        eNavType = CommonFunctions.NavFormType.None;
                    }
                    else
                    {
                        sbSQL.Append("SELECT SUM(CLAIM_CURRENCY_BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(CLAIM_CURRENCY_INCURRED_AMOUNT) INCURRED_AMOUNT, RESERVE_TYPE_CODE, RES_STATUS_CODE, SUM(CLAIM_CURRENCY_RESERVE_AMOUNT)  RES_AMOUNT,");
                        sbSQL.Append(" SUM(CLAIM_CURRENCY_PAID_TOTAL)  PAID_TOTAL, SUM(CLAIM_CURR_COLLECTION_TOTAL)  COLL_TOTAL ");
                        sbSQL.Append(" FROM RESERVE_CURRENT ");
                        sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
                        if (p_iClaimantEID != 0)
                            sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);

                        sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, RES_STATUS_CODE ");
                        eNavType = CommonFunctions.NavFormType.Reserve;
                    }
                    //rupal:end
                }
                else
                {
                    sbSQL.Append("SELECT SUM(BALANCE_AMOUNT) BALANCE_AMOUNT,SUM(INCURRED_AMOUNT) INCURRED_AMOUNT, RESERVE_TYPE_CODE, RES_STATUS_CODE, SUM(RESERVE_AMOUNT)  RES_AMOUNT,");
                    sbSQL.Append(" SUM(PAID_TOTAL)  PAID_TOTAL, SUM(COLLECTION_TOTAL)  COLL_TOTAL ");
                    sbSQL.Append(" FROM RESERVE_CURRENT ");
                    sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
                    if (p_iClaimantEID != 0)
                        sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);


                    sbSQL.Append(" GROUP BY RESERVE_TYPE_CODE, RES_STATUS_CODE ");
                    eNavType = CommonFunctions.NavFormType.None;
                }
                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);

                if (bDoNotRecalcReserves && objDataSet.Tables[0] != null)
                {
                    //Special reserve processing
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iTemp = 0; iTemp < objDataSet.Tables[0].Rows.Count; iTemp++)
                        {
                            for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                            {
                                iStatus = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RES_STATUS_CODE"].ToString());
                                iCodeID = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RESERVE_TYPE_CODE"].ToString());

                                if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                                {
                                    objDataRow = objDataSet.Tables[0].Rows[iTemp];
                                    arrReserves[iCnt].dBalance += Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                                    arrReserves[iCnt].dTotalPaid += Conversion.ConvertStrToDouble(objDataRow["PAID_TOTAL"].ToString());
                                    arrReserves[iCnt].dTotalCollected += Conversion.ConvertStrToDouble(objDataRow["COLL_TOTAL"].ToString());
                                    arrReserves[iCnt].dTotalIncurred += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());

                                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                                    if (objCache.GetRelatedShortCode(arrReserves[iCnt].iReserveTypeCode) == "R")  //MITS 35029
                                    {
                                        //asharma326 JIRA 871
                                        if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                        {
                                            dBalTotal += Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                                        }
                                        else
                                        {
                                            dBalTotal -= Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                                        }
                                        //asharma326 JIRA 872
                                        if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                            dIncTotal += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837
                                        else
                                            dIncTotal -= Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837
                                    }
                                    else
                                    {
                                        dBalTotal += Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                                        dIncTotal += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837
                                    }
                                    objCache.Dispose();
                                    objCache = null;

                                    dPaidTotal += Conversion.ConvertStrToDouble(objDataRow["PAID_TOTAL"].ToString());
                                    dCollTotal += Conversion.ConvertStrToDouble(objDataRow["COLL_TOTAL"].ToString());
                                    //dIncTotal += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());  //MITS 35837

                                    //Exit the inner for loop
                                    break;
                                }//if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                            }//for Loop
                        }//for(iTemp=0; iTemp < objDataSet.Tables[0].Rows.Count;iTemp++)
                    }//Special reserve processing
                }//if (bDoNotRecalcReserves && objDataSet.Tables[0] != null)
                else if (objDataSet.Tables[0] != null)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iTemp = 0; iTemp < objDataSet.Tables[0].Rows.Count; iTemp++)
                        {
                            for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                            {
                                iStatus = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RES_STATUS_CODE"].ToString());
                                iCodeID = Conversion.ConvertStrToInteger(
                                    objDataSet.Tables[0].Rows[iTemp]["RESERVE_TYPE_CODE"].ToString());

                                if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                                {
                                    objDataRow = objDataSet.Tables[0].Rows[iTemp];
                                    dRAmount = Conversion.ConvertStrToDouble(
                                        objDataRow["RES_AMOUNT"].ToString());
                                    dRPaid = Conversion.ConvertStrToDouble(
                                        objDataRow["PAID_TOTAL"].ToString());
                                    dRCollect = Conversion.ConvertStrToDouble(
                                        objDataRow["COLL_TOTAL"].ToString());

                                    dRBalance = CalculateReserveBalance(dRAmount, dRPaid, dRCollect, bCollInRsvBal, iCStat, iCodeID, bPerRsvColInRsvBal, p_iLOB);//asharma326 JIRA 870 add bPerRsvColInRsvBal, p_iLOB
                                    //MITS 30191 Raman Bhatia: Implementation of recovery reserve
                                    dRIncurred = CalculateIncurred(dRBalance, dRPaid, dRCollect, bCollInRsvBal, bCollInIncurredBal, iCodeID, bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB);//Shruti for MITS 8551//asharma326 JIRA 870 add bPerRsvColInRsvBal, bPerRsvColInIncrBal, p_iLOB

                                    arrReserves[iCnt].dBalance += dRBalance;
                                    arrReserves[iCnt].dTotalPaid += dRPaid;
                                    arrReserves[iCnt].dTotalCollected += dRCollect;
                                    arrReserves[iCnt].dTotalIncurred += dRIncurred;

                                    if (iStatus != 0)
                                    {
                                        if (arrReserves[iCnt].sStatusCode != "")
                                            arrReserves[iCnt].sStatusCode = arrReserves[iCnt].sStatusCode;

                                        if (objCache == null)  // only allocate once and then use for subsequent ones
                                            objCache = new LocalCache(m_sConnectionString,m_iClientId);

                                        arrReserves[iCnt].sStatusCode = arrReserves[iCnt].sStatusCode + objCache.GetShortCode(iStatus);
                                    }
                                    else
									{
                                        arrReserves[iCnt].sStatusCode = "-";
									}
                                   
                                    objCache = new LocalCache(m_sConnectionString,m_iClientId);
                                    if (objCache.GetRelatedShortCode(arrReserves[iCnt].iReserveTypeCode) == "R")  // mkaran2 - MITS 35029 
                                    {
                                        //asharma326 JIRA 871
                                        if (iAddRecoveryReservetoTotalBalanceAmount == -1)
                                        {
                                            dBalTotal += dRBalance;
                                        }
                                        else
                                            dBalTotal -= dRBalance;
                                        //asharma326 JIRA 872
                                        if (iAddRecoveryReservetoTotalIncurredAmount == -1)
                                            dIncTotal += dRIncurred;  //MITS 35837
                                        else
                                            dIncTotal -= dRIncurred;  //MITS 35837
                                    }
                                    else
                                    {
                                        dBalTotal += dRBalance;
                                        dIncTotal += dRIncurred;  //MITS 35837
                                    }
                                    objCache.Dispose();
                                    objCache = null;

                                    dPaidTotal += dRPaid;
                                    dCollTotal += dRCollect;
                                    //dIncTotal += dRIncurred;  //MITS 35837

                                    //Exit the inner for loop
                                    break;
                                }//if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                            }//for Loop
                        }//for(iTemp=0; iTemp < objDataSet.Tables[0].Rows.Count;iTemp++)
                    }//if(objDataSet.Tables[0].Rows.Count >0)
                }//Else if !(bDoNotRecalcReserves)

                sbSQL.Remove(0, sbSQL.Length);


                iCnt = 0;

                //Format Results into XML
                objXmlReservesElement = objXmlDoc.CreateElement("reserves");
                objXmlElement.AppendChild(objXmlReservesElement);
                objXmlReservesElement.SetAttribute("claimid", p_iClaimId.ToString());
                objXmlReservesElement.SetAttribute("claimant", p_iClaimantEID.ToString());
                sNodeName = "reservereadonly";
             

                for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                {
                    //if (!(bDoNotRecalcReserves && arrReserves[iCnt].dBalance == 0 &&
                    //    arrReserves[iCnt].dTotalIncurred == 0 && arrReserves[iCnt].dTotalCollected == 0 &&
                    //    arrReserves[iCnt].dTotalPaid == 0))
                    //    bProcess = true;

                    //if (bProcess)
                    //{
                        objXmlReserveElement = objXmlDoc.CreateElement(sNodeName);
                        objXmlReservesElement.AppendChild(objXmlReserveElement);

                        objXmlReserveElement.SetAttribute("reservetypecode", arrReserves[iCnt].iReserveTypeCode.ToString());
                        objXmlReserveElement.SetAttribute("reservename", arrReserves[iCnt].sReserveDesc);
                        objXmlReserveElement.SetAttribute("reservestatus", arrReserves[iCnt].sStatusCode);
                        //RUPAL:start, r8 multicurrency
                        //objXmlReserveElement.SetAttribute("balance", String.Format("{0:c}", arrReserves[iCnt].dBalance));
                        //objXmlReserveElement.SetAttribute("incurred", String.Format("{0:c}", arrReserves[iCnt].dTotalIncurred));
                        //objXmlReserveElement.SetAttribute("paid", String.Format("{0:c}", arrReserves[iCnt].dTotalPaid));
                        //objXmlReserveElement.SetAttribute("collected", String.Format("{0:c}", arrReserves[iCnt].dTotalCollected));

                        objXmlReserveElement.SetAttribute("balance", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dBalance, eNavType, m_sConnectionString, m_iClientId));
                        objXmlReserveElement.SetAttribute("incurred", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dTotalIncurred, eNavType, m_sConnectionString, m_iClientId));
                        objXmlReserveElement.SetAttribute("paid", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dTotalPaid, eNavType, m_sConnectionString, m_iClientId));
                        objXmlReserveElement.SetAttribute("collected", CommonFunctions.ConvertCurrency(p_iClaimId, arrReserves[iCnt].dTotalCollected, eNavType, m_sConnectionString, m_iClientId));

                        //rupal:end
                        //13509 start:Asif
                        bProcess = false;
                        //13509 end:Asif

                    //}//if(bProcess)
                }//for loop

                objXmlReserveElement = objXmlDoc.CreateElement("totals");
                objXmlElement.AppendChild(objXmlReserveElement);
                //RUPAL:start, r8 multicurrency
                /*
                objXmlReserveElement.SetAttribute("balanceamount", String.Format("{0:c}", dBalTotal));
                objXmlReserveElement.SetAttribute("incurredamount", String.Format("{0:c}", dIncTotal));
                objXmlReserveElement.SetAttribute("paidtotal", String.Format("{0:c}", dPaidTotal));
                objXmlReserveElement.SetAttribute("collectedtotal", String.Format("{0:c}", dCollTotal));
                */
                objXmlReserveElement.SetAttribute("balanceamount", CommonFunctions.ConvertCurrency(p_iClaimId, dBalTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("incurredamount", CommonFunctions.ConvertCurrency(p_iClaimId, dIncTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("paidtotal", CommonFunctions.ConvertCurrency(p_iClaimId, dPaidTotal, eNavType, m_sConnectionString, m_iClientId));
                objXmlReserveElement.SetAttribute("collectedtotal", CommonFunctions.ConvertCurrency(p_iClaimId, dCollTotal, eNavType, m_sConnectionString, m_iClientId));
                //rupal:end
                //XSL processing that needs to go in there will be handled at the UI side.

                //TR 2112 Raman Bhatia: Queued Payments Total should be displayed on financials screen

                bool bUseQueuedPayments = false;
                string sSQL = "";
                sSQL = "SELECT * FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    bUseQueuedPayments = objReader.GetBoolean("QUEUED_PAYMENTS");
                objReader.Close();

                //MWC 08/07/2003 If the payment is queued and the pending status in the supp field is Void we don't want to include it in Queued total
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString,m_iClientId);
                bool bPendingStatusPresent = false;
                double dQueued = 0;
                sSQL = "SELECT * FROM FUNDS_SUPP WHERE TRANS_ID = -1";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    for (int i = 0; i <= objReader.FieldCount; i++)
                    {
                        if (objReader.GetName(i) == "PEND_STATUS_CODE")
                            bPendingStatusPresent = true;
                    }
                }
                objReader.Close();
                if (bPendingStatusPresent)
                    sSQL = "SELECT SUM(AMOUNT) AMT FROM FUNDS, FUNDS_SUPP WHERE FUNDS.TRANS_ID = FUNDS_SUPP.TRANS_ID AND CLAIM_ID = " + p_iClaimId + " AND STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS") + " AND VOID_FLAG <> 0 AND PEND_STATUS_CODE <> " + objCCacheFunctions.GetCodeIDWithShort("V", "PENDING_STATUS");
                else
                    sSQL = "SELECT SUM(AMOUNT) AMT FROM FUNDS WHERE CLAIM_ID = " + p_iClaimId + " AND STATUS_CODE = " + objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS") + " AND VOID_FLAG <> 0";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    objReader.Read();
                    dQueued = objReader.GetDouble("AMT");
                    objReader.Close();
                }

                objXmlElement = (XmlElement)objXmlDoc.SelectSingleNode("//totals");
               


                // End Rem Umesh
            }//End of Main Try Block
           
            //Catching Permission Violation exception so that it is not embedded inside Generic exception
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetClaimReserves.GenericError", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objDataSet != null)
                    objDataSet.Dispose();

                if (objCache != null)
                    objCache.Dispose();
                objCache = null;
                if (objRdr != null)
                    objRdr.Dispose();
                objDataRow = null;
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                }
                if (objClaimNew != null)
                {
                    objClaimNew.Dispose();
                    objClaimNew = null;
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objColLobSettings != null)
                {
                    objColLobSettings = null;
                }
            }

            //Return the result to calling function
            return objXmlDoc;
        }        
        
        /// <summary>
        /// MGaba2:R8: SuperVisory Approval
        /// Getting the  Level of current user in submitted_to user's managerial tree
        /// TO Do: Try to Remove this function from reserve worksheet
        /// </summary>
        /// <param name="p_iSubmittedToID"></param>
        /// <param name="p_iCurrentUserID"></param>
        /// <param name="p_iClaimID"></param>
        /// <param name="p_iDSNId"></param>
        /// <param name="p_sConnStr"></param>
        /// <returns></returns>
        private int GetSupervisoryLevel(int p_iSubmittedToID, int p_iCurrentUserID, int p_iClaimID,int p_iDSNId,string p_sConnStr)
        {
            int iManagerID = 0;
            string sSQL = string.Empty;
          //  DbReader objReader = null;
            bool bInMangChain = false;
            int iReturnVal = 0;

            try
            {
                iManagerID = p_iSubmittedToID;
                if (p_iSubmittedToID != 0 && p_iCurrentUserID != 0)
                {
                    if (p_iCurrentUserID != p_iSubmittedToID) //If a user other than the one submitted to tries to approve the sheet, then perform the following check
                    {
                        while (!bInMangChain && iManagerID != 0) //stop looping if the user is found in the managerial chain
                        {
                            sSQL = "SELECT MANAGER_ID, LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = "
                                    + iManagerID
                                    + " AND USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND DSNID = "
                                    +  p_iDSNId;
                            //skhare7 Defects removed
                            using (DbReader objReader = DbFactory.GetDbReader(RMConfigurationManager.GetConfigConnectionString("RMXSecurity",m_iClientId), sSQL))
                            {
                                //   objReader = DbFactory.GetDbReader(RMConfigurationManager.GetConfigConnectionString("RMXSecurity"), sSQL);
                                if (objReader.Read())
                                {
                                    iManagerID = objReader.GetInt32("MANAGER_ID");
                                    iReturnVal += 1;
                                    if (iManagerID == p_iCurrentUserID)
                                    {
                                        bInMangChain = true;
                                    }
                                    if (iManagerID == 0)
                                    {
                                        int ilevel = 0;
                                        ilevel = GetSupervisorType(p_iCurrentUserID, p_iClaimID, p_sConnStr);
                                        if (ilevel > 0)
                                            iReturnVal += ilevel;
                                        else
                                            iReturnVal = ilevel;
                                    }
                                }
                                else
                                {
                                    iReturnVal = 0;
                                    iManagerID = 0;
                                }
                                objReader.Close();
                            }
                          
                        }
                    }
                    else
                        iReturnVal = 1;
                }
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetSupervisoryLevel.GenericError", m_iClientId), objException);
            }
            finally
            {
               
            }
            return iReturnVal;
        }

        /// <summary>
        /// MGaba2:R8: SuperVisory Approval
        /// To Get the type of supervisor
        ///  TO Do: Try to Remove this function from reserve worksheet
        /// </summary>
        /// <param name="p_iManagerID"></param>
        /// <param name="p_iClaimID"></param>
        /// <param name="p_sConnStr"></param>
        /// <returns></returns>
        private int GetSupervisorType(int p_iManagerID, int p_iClaimID, string p_sConnStr)
        {
            string sSQL = string.Empty;
            int iLOBManagerID = 0;
            int iTopManagerId = 0;
            int iLOB = 0;          

            try
            {
                sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1";

                iTopManagerId = Convert.ToInt32(DbFactory.ExecuteScalar(p_sConnStr, sSQL));

                if (p_iManagerID == iTopManagerId)
                    return 2;

                sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID.ToString();
                iLOB = Convert.ToInt32(DbFactory.ExecuteScalar(p_sConnStr, sSQL));


                sSQL = "SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOB.ToString();
                iLOBManagerID = Convert.ToInt32(DbFactory.ExecuteScalar(p_sConnStr, sSQL));

                if (p_iManagerID == iLOBManagerID)
                    return 1;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetSupervisorType.GenericError", m_iClientId), objException);
            }
            return 0;
        }
        
        /// <summary>
        /// MGaba2: R8: SuperVisory Approval
        /// This function will get the reserves which are pending for approval from the logged in user 
        /// and his subordinates if time has been lapsed for their approval
        /// </summary>
        /// <returns></returns>
        public XmlDocument GetReservesForApproval(XmlDocument p_objXmlIn)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objEle = null;
            XmlElement objEleParent = null;
            StringBuilder sbSql = null;
            LocalCache objCache=null ;
            DataSet objDS = null;
            string sHidden = string.Empty   ;
            DbReader objReader = null;           
            int iDaysAppReserves = 0;
            int iHoursAppReserves = 0;
            bool bNotifySupReserves = false;
            bool bUseCurrAdjReserves = false;  
            bool bAllowSupGrpApproval = false;
            bool bUseSupApproval = false; 
            List<string> alSubordinates = null;
            
            List<string> lReservesWithTimeLapse = null;
            int iSubmittedTo=0;
            int iLevel = 0;
            ReserveCurrent objReserveCurrent = null;
            bool bShowReserve = false;
            int iApproverId = 0;
            XmlNode xShowItem = null;
            XmlNode xShowTrans = null;   // Added By Nitika For JIRA 8253 

            //Add by kualdeep for mits:33322 Start
            bool bUseHoldReserves = false,bUseHoldIncurredLimits =false;
            //Add by kualdeep for mits:33322 End
            //RMA-344       achouhan3       Added for Custom Sorting Starts
            string strSortColumn = String.Empty;
            string strSortOrder = String.Empty;
            //RMA-344       achouhan3       Added for Custom Sorting Starts
            ColLobSettings objColLobSettings = null;//sachin 6385
            bool b_AddRecoveryReservetoTotalIncurredAmount = false;//sachin 6385

            try
            {
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);//sachin 6385
                sbSql = new StringBuilder();
               
                sbSql.Append(@"SELECT DAYS_APP_RESERVES,HOURS_APP_RESERVES,
                                            NOTIFY_SUP_RESERVES,USE_CUR_ADJ_RESERVES,ALLOW_SUP_GRP_APPROVE_RSV,USE_SUP_APP_RESERVES,USE_HOLD_RESERVES,USE_HOLD_INCURRED_LIMITS                                            
                                            FROM CHECK_OPTIONS");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                using (objReader)
                {
                    if (objReader.Read())
                    {
                        iDaysAppReserves = objReader.GetInt("DAYS_APP_RESERVES");
                        iHoursAppReserves = objReader.GetInt("HOURS_APP_RESERVES");
                        bNotifySupReserves = objReader.GetBoolean("NOTIFY_SUP_RESERVES");
                        bUseCurrAdjReserves = objReader.GetBoolean("USE_CUR_ADJ_RESERVES");
                        bAllowSupGrpApproval = objReader.GetBoolean("ALLOW_SUP_GRP_APPROVE_RSV");
                        bUseSupApproval = objReader.GetBoolean("USE_SUP_APP_RESERVES");
                        //Add by kualdeep for mits:33322 Start
                        bUseHoldReserves = objReader.GetBoolean("USE_HOLD_RESERVES");
                        //Add by kualdeep for mits:33322 End
                        bUseHoldIncurredLimits = objReader.GetBoolean("USE_HOLD_INCURRED_LIMITS");//Jira 6385
                    }
                }

                lReservesWithTimeLapse = new List<string>();
                if (iDaysAppReserves > 0 || iHoursAppReserves > 0)
                {
                    alSubordinates = new List<string>();
                    //Getting all subordinates of the logged in user
                    CommonFunctions.GetAllSubordinates(m_userLogin.UserId, m_userLogin.objRiskmasterDatabase.DataSourceId, ref alSubordinates, m_iClientId);                                       
                }
                //rsushilaggar MITS 26332 Date 03/09/2012
                xShowItem = p_objXmlIn.SelectSingleNode("//ShowAllItem");
                xShowTrans = p_objXmlIn.SelectSingleNode("//ShowMyTrans");//RMA-8253 pgupta93
                //RMA-344       achouhan3       Added for Custom Sorting Starts
                if (p_objXmlIn.SelectSingleNode("//SortOrder")!=null)
                    strSortOrder = p_objXmlIn.SelectSingleNode("//SortOrder").InnerText;
                if (p_objXmlIn.SelectSingleNode("//SortColumn") != null) 
                    strSortColumn = p_objXmlIn.SelectSingleNode("//SortColumn").InnerText;
                //RMA-344       achouhan3       Added for Custom Sorting Ends
                
                objXmlDoc = new XmlDocument();
                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                sbSql.Remove(0, sbSql.Length);
                sbSql.Append(@"SELECT CLAIM.CLAIM_NUMBER CLAIM_NUMBER,CLAIM.CLAIM_ID CLAIMID,CLAIM.CLAIM_STATUS_CODE CLAIMSTATUSCODE,
                        COALESCE(CA.ADJUSTER_EID,0) CURRENTADJUSTER , COALESCE(CLAIMANT.CLAIMANT_EID,0) PRIMARYCLAIMANT,SUPHIST2.RSV_HIST_ROW_ID RSWROWID,RES_HIST.CLAIMANT_EID ,RES_HIST.UNIT_ID ,RES_HIST.RESERVE_TYPE_CODE,RES_HIST.RESERVE_AMOUNT,RES_HIST.INCURRED_AMOUNT,
                        SUPHIST2.SUBMITTED_TO,SUPHIST2.SUBMITTED_BY SUBMITTEDBY,SUPHIST2.DTTM_SUBMISSION DATESUBMITTED, CLAIM.LINE_OF_BUS_CODE ");
                if (DbFactory.IsOracleDatabase(m_sConnectionString))
                {
                    sbSql.Append(" ,nvl( (SELECT LISTAGG(CD.SHORT_CODE, ',') ");
                    sbSql.Append(" WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID) AS SHORT_CODE");
                    sbSql.Append(" FROM HOLD_REASON INNER JOIN CODES ON HOLD_REASON.HOLD_REASON_CODE = CODES.CODE_ID");
                    sbSql.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSql.Append(" WHERE HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" AND ON_HOLD_ROW_ID = SUPHIST1.RSV_ROW_ID GROUP BY ON_HOLD_ROW_ID),0)  AS HOLD_REASON_CODE");


                    sbSql.Append(" ,nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID)");
                    sbSql.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                    sbSql.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + m_userLogin.objUser.NlsCode  + " WHERE HOLD_STATUS_CODE =" + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" AND ON_HOLD_ROW_ID = SUPHIST1.RSV_ROW_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");
                }
                else
                {

                    sbSql.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CD.SHORT_CODE   FROM HOLD_REASON  INNER JOIN CODES");
                    sbSql.Append(" ON HOLD_REASON.HOLD_REASON_CODE = CODE_ID");
                    sbSql.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSql.Append(" WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSql.Append(" AND HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = "+objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE");
                    sbSql.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE =" + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = "+ objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append("  AND ON_HOLD_ROW_ID = SUPHIST1.RSV_ROW_ID GROUP BY ON_HOLD_ROW_ID ) ,'') AS HOLD_REASON_CODE ");


                    sbSql.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CT.CODE_DESC   FROM HOLD_REASON  INNER JOIN CODES  C");
                    sbSql.Append(" ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID INNER JOIN CODES_TEXT CT");
                    sbSql.Append(" ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE =" + m_userLogin.objUser.NlsCode + " WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSql.Append(" AND HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID ="+objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                    sbSql.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = "+objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" AND ON_HOLD_ROW_ID =SUPHIST1.RSV_ROW_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");
                    //,ISNULL(HOLD_REASON.HOLD_REASON_CODE,'') HOLDREASON   
                }
                                      
                        sbSql.Append("  FROM  ");
                if (xShowTrans != null && !String.IsNullOrEmpty(xShowTrans.InnerText) && xShowTrans.InnerText == "True")
                {
                    sbSql.Append("(SELECT RSV_HIST_ROW_ID,RSV_ROW_ID FROM RSV_SUP_APPROVAL_HIST WHERE RSV_ROW_ID NOT IN (SELECT DISTINCT ONHOLD_RSV_ROW_ID FROM RESERVE_HISTORY WHERE ONHOLD_RSV_ROW_ID IS NOT NULL) GROUP BY RSV_ROW_ID,RSV_HIST_ROW_ID) SUPHIST1");
                }
                else
                {
                    sbSql.Append("(SELECT MAX(RSV_HIST_ROW_ID) RSV_HIST_ROW_ID,RSV_ROW_ID FROM RSV_SUP_APPROVAL_HIST WHERE RSV_ROW_ID NOT IN (SELECT DISTINCT ONHOLD_RSV_ROW_ID FROM RESERVE_HISTORY WHERE ONHOLD_RSV_ROW_ID IS NOT NULL) GROUP BY RSV_ROW_ID) SUPHIST1");
                }
                sbSql.Append(" INNER JOIN (SELECT  RSV_HIST_ROW_ID,DTTM_SUBMISSION,SUBMITTED_TO,SUBMITTED_BY FROM RSV_SUP_APPROVAL_HIST ");
                //rsushilaggar MITS 26332 Date 03/09/2012
                if (bUseSupApproval)//Add If Condition by kuladeep for mits:33322--If Supp. Approval is not ON in that case Remove where clause for filter record on the basis if UserId.
                {
                    if (xShowItem == null || (xShowItem != null && xShowItem.InnerText != "True"))
                    {
                        // Added By Nitika For JIRA 8253 START 
                        //RMA-10235     achouhan3   Added for null check 
                        if (xShowTrans != null && !String.IsNullOrEmpty(xShowTrans.InnerText) && xShowTrans.InnerText == "True")
                        {
                            sbSql.AppendFormat(" WHERE SUBMITTED_BY = {0}", m_userLogin.UserId);
                        }
                        // Added By Nitika For JIRA 8253 END
                        else
                        {
                            if (alSubordinates != null && alSubordinates.Count != 0)
                            {
                                sbSql.AppendFormat(" WHERE SUBMITTED_TO IN ({0},{1})", m_userLogin.UserId, String.Join(",", alSubordinates.ToArray()));
                            }
                            else
                            {
                                sbSql.AppendFormat(" WHERE SUBMITTED_TO = {0}", m_userLogin.UserId);
                            }
                        }
                    }
                }
                sbSql.Append(@") SUPHIST2
                        ON SUPHIST1.RSV_HIST_ROW_ID = SUPHIST2.RSV_HIST_ROW_ID
                        INNER JOIN 
                        (SELECT RSV_ROW_ID,CLAIM_ID,CLAIMANT_EID,UNIT_ID,RESERVE_TYPE_CODE,RESERVE_AMOUNT,INCURRED_AMOUNT FROM RESERVE_HISTORY ) RES_HIST
                        ON  SUPHIST1.RSV_ROW_ID=RES_HIST.RSV_ROW_ID 
                        INNER JOIN CLAIM
                        ON RES_HIST.CLAIM_ID = CLAIM.CLAIM_ID 
                        LEFT OUTER JOIN CLAIM_ADJUSTER CA 
                        ON CA.CLAIM_ID = CLAIM.CLAIM_ID AND CA.CURRENT_ADJ_FLAG =-1 
                        LEFT OUTER JOIN CLAIMANT 
                        ON CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG =-1 ");
                        
 			//mbahl3 Reserve Approval Enhancement Mits 32471
 			sbSql.Append(@" UNION");
                sbSql.Append(@" SELECT CLAIM.CLAIM_NUMBER CLAIM_NUMBER,CLAIM.CLAIM_ID CLAIMID,CLAIM.CLAIM_STATUS_CODE CLAIMSTATUSCODE,
                        COALESCE(CA.ADJUSTER_EID,0) CURRENTADJUSTER , 
                        COALESCE(CLAIMANT.CLAIMANT_EID,0) 
                        PRIMARYCLAIMANT,SUPHIST2.RSV_HIST_ROW_ID RSWROWID,RES_HIST.CLAIMANT_EID 
                        ,RES_HIST.UNIT_ID ,RES_HIST.RESERVE_TYPE_CODE,RES_HIST.RESERVE_AMOUNT,RES_HIST.INCURRED_AMOUNT, 
                        SUPHIST2.SUBMITTED_TO,SUPHIST2.SUBMITTED_BY 
                        SUBMITTEDBY,SUPHIST2.DTTM_SUBMISSION DATESUBMITTED, CLAIM.LINE_OF_BUS_CODE ");
                if (DbFactory.IsOracleDatabase(m_sConnectionString))
                {
                    sbSql.Append(" ,nvl( (SELECT LISTAGG(CD.SHORT_CODE, ',') ");
                    sbSql.Append(" WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID) AS SHORT_CODE");
                    sbSql.Append(" FROM HOLD_REASON INNER JOIN CODES ON HOLD_REASON.HOLD_REASON_CODE = CODES.CODE_ID");
                    sbSql.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSql.Append(" WHERE HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" AND ON_HOLD_ROW_ID = SUPHIST1.RC_ROW_ID GROUP BY ON_HOLD_ROW_ID),0)  AS HOLD_REASON_CODE");


                    sbSql.Append(" ,nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID)");
                    sbSql.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                    sbSql.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE =" + m_userLogin.objUser.NlsCode + " WHERE HOLD_STATUS_CODE =" + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" AND ON_HOLD_ROW_ID = SUPHIST1.RC_ROW_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");
                }
                else
                {
                    sbSql.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CD.SHORT_CODE   FROM HOLD_REASON  INNER JOIN CODES");
                    sbSql.Append(" ON HOLD_REASON.HOLD_REASON_CODE = CODE_ID");
                    sbSql.Append(" INNER JOIN  CODES CD ON CODES.RELATED_CODE_ID = CD.CODE_ID");
                    sbSql.Append(" WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSql.Append(" AND HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE");
                    sbSql.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE =" + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append("  AND ON_HOLD_ROW_ID = SUPHIST1.RC_ROW_ID GROUP BY ON_HOLD_ROW_ID ) ,'') AS HOLD_REASON_CODE ");



                    sbSql.Append(" ,ISNULL(  (SELECT STUFF((SELECT ', ' + CT.CODE_DESC   FROM HOLD_REASON  INNER JOIN CODES  C");
                    sbSql.Append(" ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID INNER JOIN CODES_TEXT CT");
                    sbSql.Append(" ON C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE =" + m_userLogin.objUser.NlsCode + " WHERE ON_HOLD_ROW_ID = HR.ON_HOLD_ROW_ID");
                    sbSql.Append(" AND HOLD_STATUS_CODE =" + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID =" + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                    sbSql.Append(" FROM HOLD_REASON  HR WHERE HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("RESERVE_CURRENT"));
                    sbSql.Append(" AND ON_HOLD_ROW_ID =SUPHIST1.RC_ROW_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLDREASON");
                    //,ISNULL(HOLD_REASON.HOLD_REASON_CODE,'') HOLDREASON   
                }
                    //, ISNULL(HOLD_REASON.HOLD_REASON_CODE,'') HOLDREASON
                        sbSql.Append(" FROM ");
			if (xShowTrans != null && !String.IsNullOrEmpty(xShowTrans.InnerText) && xShowTrans.InnerText == "True")
                {
                    sbSql.Append("( SELECT  RSV_HIST_ROW_ID,RC_ROW_ID FROM RSV_SUP_APPROVAL_HIST WHERE RC_ROW_ID   IN (SELECT DISTINCT RC_ROW_ID  FROM RESERVE_CURRENT WHERE RES_STATUS_CODE=" + objCache.GetCodeId("H", "RESERVE_STATUS"));
                    sbSql.Append(@" )GROUP BY RC_ROW_ID,RSV_HIST_ROW_ID
                        ) SUPHIST1
                        INNER JOIN 
                        (
                        SELECT  RSV_HIST_ROW_ID,DTTM_SUBMISSION,SUBMITTED_TO,SUBMITTED_BY FROM RSV_SUP_APPROVAL_HIST ");
                }
                else
                {
                    sbSql.Append("( SELECT MAX(RSV_HIST_ROW_ID) RSV_HIST_ROW_ID,RC_ROW_ID FROM RSV_SUP_APPROVAL_HIST WHERE RC_ROW_ID   IN (SELECT DISTINCT RC_ROW_ID  FROM RESERVE_CURRENT WHERE RES_STATUS_CODE=" + objCache.GetCodeId("H", "RESERVE_STATUS"));

                sbSql.Append(@" )GROUP BY RC_ROW_ID
                        ) SUPHIST1
                        INNER JOIN 
                        (
                        SELECT  RSV_HIST_ROW_ID,DTTM_SUBMISSION,SUBMITTED_TO,SUBMITTED_BY FROM RSV_SUP_APPROVAL_HIST ");
                }

 				if (bUseSupApproval)//Add If Condition by kuladeep for mits:33322--If Supp. Approval is not ON in that case Remove where clause for filter record on the basis if UserId.
                {
                    if (xShowItem == null || (xShowItem != null && xShowItem.InnerText != "True"))
                    {
                        // Added By Nitika For JIRA 8253 
                        //RMA-10235     achouhan3   Added for null check 
                        if (xShowTrans != null && !String.IsNullOrEmpty(xShowTrans.InnerText) && xShowTrans.InnerText == "True")
                        {
                            sbSql.AppendFormat(" WHERE SUBMITTED_BY = {0}", m_userLogin.UserId);
                        }
                        // Added By Nitika For JIRA 8253 
                        else
                        {
                            if (alSubordinates != null && alSubordinates.Count != 0)
                            {
                                sbSql.AppendFormat(" WHERE SUBMITTED_TO IN ({0},{1})", m_userLogin.UserId, String.Join(",", alSubordinates.ToArray()));
                            }
                            else
                            {
                                sbSql.AppendFormat(" WHERE SUBMITTED_TO = {0}", m_userLogin.UserId);
                            }
                        }
                    }
                }
				 sbSql.Append(@"                         ) SUPHIST2
                        ON SUPHIST1.RSV_HIST_ROW_ID = SUPHIST2.RSV_HIST_ROW_ID
                        INNER JOIN 
                        (
                        SELECT RC_ROW_ID,CLAIM_ID,CLAIMANT_EID,UNIT_ID,
                        RESERVE_TYPE_CODE,RESERVE_AMOUNT,INCURRED_AMOUNT FROM RESERVE_CURRENT
                         ) RES_HIST
                        ON  SUPHIST1.RC_ROW_ID=RES_HIST.RC_ROW_ID 
                        INNER JOIN CLAIM
                        ON RES_HIST.CLAIM_ID = CLAIM.CLAIM_ID 
                        LEFT OUTER JOIN CLAIM_ADJUSTER CA 
                        ON CA.CLAIM_ID = CLAIM.CLAIM_ID AND CA.CURRENT_ADJ_FLAG =-1 
                        LEFT OUTER JOIN CLAIMANT 
                        ON CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG =-1 ");
                     
				//mbahl3 Reserve Approval Enhancement Mits 32471
                 //RMA-344       achouhan3       Added for Custom Sorting Starts
                 if (!String.IsNullOrEmpty(strSortColumn.Trim()) && !String.IsNullOrEmpty(strSortOrder.Trim()))
                 {
                     sbSql.Append("ORDER BY ");
                     sbSql.Append(strSortColumn);
                     sbSql.Append(" ");
                     sbSql.Append(strSortOrder);
                 }
                 //RMA-344       achouhan3       Added for Custom Sorting Ends
                //JIRA 7810 start snehal
                 if (DbFactory.IsOracleDatabase(m_sConnectionString))
                 {
                     sbSql = sbSql.Replace("ISNULL", "nvl");
                 }
                 //JIRA 7810 end
                 objDS = DbFactory.GetDataSet(m_sConnectionString, sbSql.ToString(), m_iClientId);

                objEle = objXmlDoc.CreateElement("ReserveApprovals");
                objXmlDoc.AppendChild(objEle);
                foreach (DataRow dr in objDS.Tables[0].Rows)
                {
                    iSubmittedTo = Convert.ToInt32(dr["SUBMITTED_TO"]);
                    iLevel = 0;
                    bShowReserve = false;
                    iApproverId = 0;

                    // npadhy The Below condition is not used any more as when bUseSupApproval = false, we do not show the link any more.
                    //Add IF case by kuladeep for displaying all Reserve transaction with in limit if Sup. Approvall off and Hold setting on:Start mits:33322
                    if ((bUseHoldReserves && !bUseSupApproval) || (bUseHoldIncurredLimits && !bUseSupApproval))
                    {
                        int iClaimId = Convert.ToInt32(dr["CLAIMID"]);
                        int iUnitId = Convert.ToInt32(dr["UNIT_ID"]);
                        int iReserveTypeCode = Convert.ToInt32(dr["RESERVE_TYPE_CODE"]);
                        double dReserveAmount = Convert.ToDouble(dr["RESERVE_AMOUNT"]);
                        double dIncurredAmount = Convert.ToDouble(dr["INCURRED_AMOUNT"]);//Get Incurred Amount.
                        int iLOB = Convert.ToInt32(dr["LINE_OF_BUS_CODE"]);
                        if (objColLobSettings[iLOB].AddRecoveryReservetoTotalIncurredAmount == -1)
                        {
                            b_AddRecoveryReservetoTotalIncurredAmount = true;
                        }
                        bool bUnderLimit = false;
                        bool bUnderLimitInc = false;
                        if (bUseHoldReserves && !bUseSupApproval)
                            bUnderLimit = CommonFunctions.CheckReserveLimits(iClaimId, iReserveTypeCode, dReserveAmount, m_userLogin.UserId, m_userLogin.GroupId, iLOB, m_sConnectionString, m_iClientId);
                        //check for incurred.
                        if (bUseHoldIncurredLimits && !bUseSupApproval)
                            //Syadav55 Jira-15210 starts
                            //bUnderLimitInc = CommonFunctions.CheckClaimIncurredLimits(iClaimId, dIncurredAmount, Convert.ToInt32(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"))), Convert.ToString(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_TYPE"), objCache.GetCodeId("R", "MASTER_RESERVE"))), m_userLogin.UserId, m_userLogin.GroupId, iLOB, b_AddRecoveryReservetoTotalIncurredAmount, m_sConnectionString, m_iClientId);
                            bUnderLimitInc = CommonFunctions.CheckClaimIncurredLimits(iClaimId, dIncurredAmount, Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0")), Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_TYPE") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("R", "MASTER_RESERVE") + "  AND DELETED_FLAG=0")), m_userLogin.UserId, m_userLogin.GroupId, iLOB, b_AddRecoveryReservetoTotalIncurredAmount, m_sConnectionString, m_iClientId);
                            //Syadav55 Jira-15210 starts
                        if (!bUnderLimit && !bUnderLimitInc)
                        {
                            bShowReserve = true;
                        }
                    }//Add If case by kuladeep for displaying all Reserve transaction with in limit if Sup. Approvall off and Hold setting on:End mits:33322
                    else
                    {
                        if (iSubmittedTo == m_userLogin.UserId)
                        {
                            bShowReserve = true;
                        }
                        else
                        {
                            //RMA-8253 pgupta93 START
                            if (xShowTrans != null && !String.IsNullOrEmpty(xShowTrans.InnerText) && xShowTrans.InnerText == "True")
                            {
                                bShowReserve = true;
                            }
                            //RMA-8253 pgupta93 END
                            //For Subordinates-we have to find whether time has been lapsed or not
                            //rsushilaggar MITS 26332 Date 03/09/2012
                            if (xShowItem == null || (xShowItem != null && xShowItem.InnerText != "True"))
                            {
                                iLevel = GetSupervisoryLevel(iSubmittedTo, m_userLogin.UserId, Convert.ToInt32(dr["CLAIMID"]), m_userLogin.objRiskmasterDatabase.DataSourceId, m_sConnectionString);
                                if ((iDaysAppReserves > 0 && (Conversion.ToDate(Convert.ToString(dr["DATESUBMITTED"])).AddDays(iLevel * iDaysAppReserves) < DateTime.Now)) ||
                                    (iHoursAppReserves > 0) && (Conversion.ToDate(Convert.ToString(dr["DATESUBMITTED"])).AddHours(iLevel * iHoursAppReserves) < DateTime.Now))
                                {
                                    bShowReserve = true;
                                    //if (m_objDataModelFactory == null)
                                    //{
                                    //    m_objDataModelFactory = new DataModelFactory(m_userLogin);
                                    //}
                                    //objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);
                                    //objReserveCurrent.ClaimId = Convert.ToInt32(dr["CLAIMID"]);
                                    //objReserveCurrent.ClaimantEid = Convert.ToInt32(dr["CLAIMANT_EID"]);
                                    //objReserveCurrent.UnitId = Convert.ToInt32(dr["UNIT_ID"]);
                                    //objReserveCurrent.ReserveTypeCode = Convert.ToInt32(dr["RESERVE_TYPE_CODE"]);
                                    //objReserveCurrent.ReserveAmount = Convert.ToDouble(dr["RESERVE_AMOUNT"]);
                                    //objReserveCurrent.iUserId = m_userLogin.UserId;
                                    //objReserveCurrent.iGroupId = m_userLogin.GroupId;

                                //iApproverId = objReserveCurrent.GetSuperVisor(bUseCurrAdjReserves, bNotifySupReserves);

                                //if (iApproverId == m_userLogin.UserId)
                                //{
                                //    bShowReserve = true;
                                //}
                            }
                        }
                        else
                        {
                            int iClaimId = Convert.ToInt32(dr["CLAIMID"]);
                            int iUnitId = Convert.ToInt32(dr["UNIT_ID"]);
                            int iReserveTypeCode = Convert.ToInt32(dr["RESERVE_TYPE_CODE"]);
                            double dReserveAmount = Convert.ToDouble(dr["RESERVE_AMOUNT"]);
                            double dIncurredAmount = Convert.ToDouble(dr["INCURRED_AMOUNT"]);//Get Incurred Amount.
                            int iLOB = Convert.ToInt32(dr["LINE_OF_BUS_CODE"]);
                            if (objColLobSettings[iLOB].AddRecoveryReservetoTotalIncurredAmount == -1)
                            {
                                b_AddRecoveryReservetoTotalIncurredAmount = true;
                            }
                                //rsharma220 MITS RMA-8220, MITS 37807 Start
                                string sSql = "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + iSubmittedTo; //igupta3: MITS-34389: removing the group restriction 
                                int iGroupId = Convert.ToInt32(DbFactory.ExecuteScalar(m_sConnectionString, sSql)); // MITS-34389
                            bool bUnderLimit = false, bUnderLimitInc = false;
                            if (alSubordinates != null)//MITS 31573-Null check added
                            {
                                if (alSubordinates.Exists(x => x == iSubmittedTo.ToString()))
                                {
                                    iLevel = GetSupervisoryLevel(iSubmittedTo, m_userLogin.UserId, Convert.ToInt32(dr["CLAIMID"]), m_userLogin.objRiskmasterDatabase.DataSourceId, m_sConnectionString);
                                    if ((iDaysAppReserves > 0 && (Conversion.ToDate(Convert.ToString(dr["DATESUBMITTED"])).AddDays(iLevel * iDaysAppReserves) < DateTime.Now)) ||
                                    (iHoursAppReserves > 0) && (Conversion.ToDate(Convert.ToString(dr["DATESUBMITTED"])).AddHours(iLevel * iHoursAppReserves) < DateTime.Now))
                                    {
                                        bShowReserve = true;
                                    }
                                        //else
                                        //{
                                        //if (iGroupId == m_userLogin.GroupId && bUseSupApproval && bAllowSupGrpApproval) igupta3:MITS-34389: removing the group restriction
                                        //     if (!bShowReserve && bUseSupApproval && bAllowSupGrpApproval && iGroupId == m_userLogin.GroupId)
                                        //    {
                                        //        bUnderLimit = CommonFunctions.CheckReserveLimits(iClaimId, iReserveTypeCode, dReserveAmount, m_userLogin.UserId, m_userLogin.GroupId, iLOB, m_sConnectionString, m_iClientId);

                                        //        if (!bUnderLimit)
                                        //        {
                                        //            bShowReserve = true;
                                        //        }
                                        //    }
                                        //}
                                    }
                                }
                                //else
                                //{
                                //if (iGroupId == m_userLogin.GroupId && bUseSupApproval && bAllowSupGrpApproval) igupta3:MITS-34389: removing the group restriction
                                if (!bShowReserve && bUseSupApproval && bAllowSupGrpApproval && iGroupId == m_userLogin.GroupId)
                                {
									if(bUseHoldReserves)
	                                    bUnderLimit = CommonFunctions.CheckReserveLimits(iClaimId, iReserveTypeCode, dReserveAmount, m_userLogin.UserId, m_userLogin.GroupId, iLOB, m_sConnectionString, m_iClientId);

                                    if (bUseHoldIncurredLimits)
                                        //Syadav55 Jira-15210 starts
                                        //bUnderLimitInc = CommonFunctions.CheckClaimIncurredLimits(iClaimId, dIncurredAmount, Convert.ToInt32(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"))), Convert.ToString(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_TYPE"), objCache.GetCodeId("R", "MASTER_RESERVE"))), m_userLogin.UserId, m_userLogin.GroupId, iLOB, b_AddRecoveryReservetoTotalIncurredAmount, m_sConnectionString, m_iClientId);
                                        bUnderLimitInc = CommonFunctions.CheckClaimIncurredLimits(iClaimId, dIncurredAmount, Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0")), Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_TYPE") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("R", "MASTER_RESERVE") + "  AND DELETED_FLAG=0")), m_userLogin.UserId, m_userLogin.GroupId, iLOB, b_AddRecoveryReservetoTotalIncurredAmount, m_sConnectionString, m_iClientId);
                                        //Syadav55 Jira-15210 ends
                                    if (!bUnderLimit && !bUnderLimitInc)
                                    {
                                        bShowReserve = true;
                                    }
                                }
                                //rsharma220 MITS RMA-8220, MITS 37807 End   
                            }
                        }
                    }
                    if (bShowReserve)
                    {
                        objEleParent = objXmlDoc.CreateElement("Approvals");
                        foreach (DataColumn dc in objDS.Tables[0].Columns)
                        {
                            objEle = objXmlDoc.CreateElement(dc.ColumnName);
                            sHidden = "false";
                            if (dc.ColumnName.ToUpper().Contains("CURRENTADJUSTER") || dc.ColumnName.ToUpper().Contains("PRIMARYCLAIMANT"))
                            {
                                objEle.InnerText = objCache.GetEntityLastFirstName(Convert.ToInt32(dr[dc.ColumnName]));
                            }
                            else if (dc.ColumnName.ToUpper().Contains("DATE"))
                            {
                                objEle.InnerText = Conversion.ToDate(dr[dc.ColumnName].ToString()).ToShortDateString();
                                sHidden = "true";
                            }
                            else if (dc.ColumnName.ToUpper().Contains("SUBMITTEDBY"))
                            {
                                objEle.InnerText = objCache.GetSystemLoginName(Convert.ToInt32(dr[dc.ColumnName]), m_userLogin.objRiskmasterDatabase.DataSourceId, SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);//rkaur27
                                sHidden = "true";
                            }
                            else if (dc.ColumnName.ToUpper().Contains("SUBMITTED_TO"))
                            {
                                objEle.InnerText = objCache.GetSystemLoginName(Convert.ToInt32(dr[dc.ColumnName]), m_userLogin.objRiskmasterDatabase.DataSourceId, SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);//rkaur27                               
                            }
                            else if (dc.ColumnName.ToUpper().Contains("RESERVE_TYPE_CODE"))
                            {
                                objEle.InnerText = objCache.GetCodeDesc(Convert.ToInt32(dr[dc.ColumnName]));
                            }
                            else if (dc.ColumnName.ToUpper().Contains("LINE_OF_BUS_CODE"))
                            {
                                objEle.InnerText = objCache.GetCodeDesc(Convert.ToInt32(dr[dc.ColumnName]));
                            }
							//Start Jira 6385
                          
                            else if (dc.ColumnName.ToUpper().Contains("HOLDREASON"))
                            {
                                objEle.InnerText = dr[dc.ColumnName].ToString();
                            }

                               //End Jira 6385
                            else
                            {
                                objEle.InnerText = dr[dc.ColumnName].ToString();
                            }
                            if (dc.ColumnName.ToUpper().Contains("RSWROWID") || dc.ColumnName.ToUpper().Contains("CLAIMSTATUSCODE") ||
                                    dc.ColumnName.ToUpper().Contains("CLAIMID") || dc.ColumnName.ToUpper().Contains("CLAIMANT_EID") || dc.ColumnName.ToUpper().Contains("UNIT_ID") || dc.ColumnName.ToUpper().Contains("INCURRED_AMOUNT") || dc.ColumnName.ToUpper().Contains("HOLD_REASON_CODE"))
                            {
                                sHidden = "true";
                            }
                            objEle.SetAttribute("hidden", sHidden);
                            objEleParent.AppendChild(objEle);
                        }
                        objXmlDoc.FirstChild.AppendChild(objEleParent);
                    }
                }
                //RMA-344       achouhan3       Added for Custom Sorting Starts
                //RMA-10696 Added other columns in sorting
                if (strSortColumn.Contains("RESERVE_TYPE_CODE") || strSortColumn.Contains("SUBMITTEDBY") || strSortColumn.Contains("SUBMITTED_TO")
                    || strSortColumn.Contains("SUBMITTEDBY") || strSortColumn.Contains("CURRENTADJUSTER") || strSortColumn.Contains("PRIMARYCLAIMANT"))
                {
                    XElement xSortElement = XElement.Parse(objXmlDoc.InnerXml);
                    XElement[] sortedTables;
                    if (strSortOrder.Trim() == "asc")
                    {

                        sortedTables = xSortElement.Elements("Approvals").OrderBy(t => (string)t.Element(strSortColumn)).ToArray();

                    }
                    else
                    {
                        sortedTables = xSortElement.Elements("Approvals").OrderByDescending(t => (string)t.Element(strSortColumn)).ToArray();
                    }
                    xSortElement.ReplaceAll(sortedTables);
                    objXmlDoc.LoadXml(xSortElement.ToString());
                }

                //RMA-344       achouhan3       Added for Custom Sorting Starts

                return objXmlDoc;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesForApproval.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objDS != null)
                {
                    objDS.Dispose();
                    objDS = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
        }

        /// <summary>
        ///  MGaba2: R8: SuperVisory Approval
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <returns>true if reserve gets approved. False if rejected</returns>
        // tanwar2 - Added out parameter for deductible implementation
        //public void  ApproveOrRejectReserve(XmlDocument p_objXmlIn)
        //public void ApproveOrRejectReserve(XmlDocument p_objXmlIn, out bool p_bReserveApproved, out double p_dAmount, out int p_iLossCode, out int p_iDisabilityCat, out int p_iAdjusterEid, out int p_iReserveCurrencyType)
        public void ApproveOrRejectReserve(XmlDocument p_objXmlIn,bool b_Msg, out bool p_bReserveApproved, out double p_dAmount, out int p_iLossCode, out int p_iDisabilityCat, out int p_iAdjusterEid, out int p_iReserveCurrencyType, out int p_iReserveStatus, out int p_iClaimId, out int p_iReserveTypeCode, out int p_iClaimantEid, out int p_iCvgId)
        {
            
            XmlElement oElement = null;
            string sRsvHistRowId=string.Empty ;
            StringBuilder sbSql = null;
            DbReader objRdr = null;
            int iTopLvlMgr = 0;
            int iUserWhoWillApprove = 0;
            int iClaimId = 0;
            int iPolCvgRowId = 0;
            int iCvgLossId=0;
            int iResTypeCode = 0;
            int iLOB = 0;
            int iSubmittedTo = 0;
            string sDTTMSubmit = string.Empty;
            double dAmount = 0;
            bool bExceedLimit = false;
            bool bClaimIncExceedLimit = false;//Jira 6385
            int iLevel=0;           
            int iLobMgr = 0;
            int iNewSubmittedTo = 0;
            int iReserveHistoryId = 0;
            LocalCache objCache = null;           
            int iUnitId = 0;
            int iClaimantEid = 0;
            string sAction = string.Empty;
            DataModelFactory DMF = null ;
            ReserveCurrent objRC = null;
            int iRCRowId = 0;
            //skhare7 R8 Supervisory approval
              string sAppRejReason = string.Empty;
            string sClaimNumber=string.Empty;
            bool bDisableDiaryNotify = false;
            bool bDisableEmailNotify = false;
             DbReader objReader = null;
            StringBuilder sbSQL = null ;           
            bool bUseSupAppReserves = false;
            int iDaysAppReserves = 0;
            int iHoursAppReserves = 0;
            bool bNotifySupReserves = false;
            bool bUseCurrAdjReserves = false;        
            bool bAllowSupGrpApprove = false;
            int iApproverId = 0;
            SysSettings objSysSettings = null;            

            //Add by kualdeep for mits:33322 Start
            bool bUseHoldReserves = false;
            bool bUseHoldIncurredLimits = false;//jira 6385
            //Add by kualdeep for mits:33322 End
            //mbahl3 Reserve Approval Enhancement Mits 32471
            int iOrigStatusCode = 0; 
            double dOrigStatusAmount = 0;
            string sDttmRcdAdded = string.Empty;
            bool bHasRsvRowId = false;					//mbahl3 Reserve Approval Enhancement Mits 32471
            int iRsvRowId = 0; //mbahl3 JIRA [RMA-331]
			string sDateEntered = string.Empty;
			int iAprovedID = 0;
			string sSQL = string.Empty;
			 //mbahl3 Reserve Approval Enhancement Mits 32471
            bool bResLimit = false;////Jira 6385- Incurred Limit
            bool bClaimIncLimit = false;////Jira 6385- Incurred Limit
            string StrHoldReasonDesc = string.Empty;//sachin 6385
            bool b_AddRecoveryReservetoTotalIncurredAmount = false;//sachin 6385
            bool bRejectStatus = false;
            ColLobSettings objColLobSettings = null;//sachin 6385
            List<int> lstThisHoldReason = new List<int>(); //Jira 6385
            bool b_HoldReasonRes = false;
            bool b_HoldReasonInc = false;

            try
            {
                sbSql = new StringBuilder();
                DMF = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                objRC = (ReserveCurrent)DMF.GetDataModelObject("ReserveCurrent", false);
                #region getting the settings related to SuperVisory Approval

                sbSQL = new StringBuilder(@"SELECT USE_SUP_APP_RESERVES,DAYS_APP_RESERVES,HOURS_APP_RESERVES,
                                            NOTIFY_SUP_RESERVES,USE_CUR_ADJ_RESERVES,
                                            DISABLE_DIARY_NOTIFY_FOR_SUPV,DISABLE_EMAIL_NOTIFY_FOR_SUPV ,ALLOW_SUP_GRP_APPROVE_RSV,USE_HOLD_RESERVES,USE_HOLD_INCURRED_LIMITS
                                            FROM CHECK_OPTIONS");//Jira 6385
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                using (objReader)
                {
                    if (objReader.Read())
                    {
                        bUseSupAppReserves = objReader.GetBoolean("USE_SUP_APP_RESERVES");
                        iDaysAppReserves = objReader.GetInt("DAYS_APP_RESERVES");
                        iHoursAppReserves = objReader.GetInt("HOURS_APP_RESERVES");
                        bNotifySupReserves = objReader.GetBoolean("NOTIFY_SUP_RESERVES");
                        bUseCurrAdjReserves = objReader.GetBoolean("USE_CUR_ADJ_RESERVES");
                        bDisableDiaryNotify = objReader.GetBoolean("DISABLE_DIARY_NOTIFY_FOR_SUPV");
                        bDisableEmailNotify = objReader.GetBoolean("DISABLE_EMAIL_NOTIFY_FOR_SUPV");
                        bAllowSupGrpApprove = objReader.GetBoolean("ALLOW_SUP_GRP_APPROVE_RSV");
                        //Add by kualdeep for mits:33322 Start
                        bUseHoldReserves = objReader.GetBoolean("USE_HOLD_RESERVES");
                        //Add by kualdeep for mits:33322 End
                        bUseHoldIncurredLimits = objReader.GetBoolean("USE_HOLD_INCURRED_LIMITS");//Jira 6385
                    }
                }
                #endregion 

                //skhare7 R8 Supervisory approval
                oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ApproveReasonCom");
                if (oElement != null)
                {
                    sAppRejReason = oElement.InnerText;
                }
				//mbahl3 Reserve Approval Enhancement Mits 32471
              //  oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ResHistRowId");
                	//mbahl3 Reserve Approval Enhancement Mits 32471
				oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ResHistRowId");
                if (oElement != null)
                {//From Reserve Approval Screen
                   
                   sRsvHistRowId = oElement.InnerText;

			//mbahl3 Reserve Approval Enhancement Mits 32471
                   using (DbReader objReadr = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM RSV_SUP_APPROVAL_HIST WHERE RSV_HIST_ROW_ID=" + sRsvHistRowId))
                   {

                       if (objReadr.Read())
                       {

                           iRCRowId = Conversion.ConvertObjToInt(objReadr.GetValue("RC_ROW_ID"), m_iClientId);
                           iOrigStatusCode = Conversion.ConvertObjToInt(objReadr.GetValue("ORIGINAL_RES_STATUS_CODE"), m_iClientId);
                           dOrigStatusAmount = Conversion.ConvertObjToDouble(objReadr.GetValue("ORIGINAL_RESERVE_AMOUNT"), m_iClientId);
                           dAmount = Conversion.ConvertObjToDouble(objReadr.GetValue("NEW_RESERVE_AMOUNT"), m_iClientId);
                           //sEnteredByUser = Conversion.ConvertObjToStr(objReadr.GetValue("ADDED_BY_USER"));
                           sDateEntered = Conversion.ConvertObjToStr(objReadr.GetValue("DATE_ENTERED"));
                           iRsvRowId = Conversion.ConvertObjToInt(objReadr.GetValue("RSV_ROW_ID"), m_iClientId); //mbahl3 JIRA [RMA-331]

                       }
                   }
                }
                else
                {
					//mbahl3 Reserve Approval Enhancement Mits 32471
                   oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RsvHistRowId");
                    if ((oElement != null) &&  !string.IsNullOrEmpty( oElement.InnerText) && !string.Equals(oElement.InnerText,"0"))
                    {

                        sRsvHistRowId = oElement.InnerText;


                        using (DbReader objReadr = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM RSV_SUP_APPROVAL_HIST WHERE RSV_HIST_ROW_ID=" + sRsvHistRowId))
                        {
                           
                            if (objReadr.Read())
                            {

                                iRCRowId = Conversion.ConvertObjToInt(objReadr.GetValue("RC_ROW_ID"), m_iClientId);
                                iOrigStatusCode = Conversion.ConvertObjToInt(objReadr.GetValue("ORIGINAL_RES_STATUS_CODE"), m_iClientId);
                                dOrigStatusAmount = Conversion.ConvertObjToInt(objReadr.GetValue("ORIGINAL_RESERVE_AMOUNT"), m_iClientId);
                                dAmount = Conversion.ConvertObjToInt(objReadr.GetValue("NEW_RESERVE_AMOUNT"), m_iClientId);
                                //sEnteredByUser=Conversion.ConvertObjToStr(objReadr.GetValue("ADDED_BY_USER"));
                               sDateEntered = Conversion.ConvertObjToStr(objReadr.GetValue("DATE_ENTERED"));
                               iRsvRowId = Conversion.ConvertObjToInt(objReadr.GetValue("RSV_ROW_ID"), m_iClientId); //mbahl3 JIRA [RMA-331]
                            
                            }
                        }
					//mbahl3 Reserve Approval Enhancement Mits 32471
                }
                else
                {//From Modify reserve Screen
                    oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RsvRowId");
                    if (oElement != null)
                    {
                        bHasRsvRowId = true;					//mbahl3 Reserve Approval Enhancement Mits 32471
                        sbSql.AppendFormat("SELECT MAX(RSV_HIST_ROW_ID) RSV_HIST_ROW_ID FROM RSV_SUP_APPROVAL_HIST WHERE RSV_ROW_ID = {0}", oElement.InnerText);
                        objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                        using (objRdr)
                        {
                            if (objRdr.Read())
                            {
                                sRsvHistRowId = objRdr.GetInt32("RSV_HIST_ROW_ID").ToString();
                            }
                        }
                    }
                    else
                    {//From Edit Reserve Screen in case AC is ON
                        oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//RCRowId");
                        if (oElement != null)
                        {
                            iRCRowId = Convert.ToInt32(oElement.InnerText);
                            oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimID");
                            if (oElement != null)
                            {
                                iClaimId  = Convert.ToInt32(oElement.InnerText);
                            }
                            oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ClaimantEID");
                            if (oElement != null)
                            {
                                iClaimantEid = Convert.ToInt32(oElement.InnerText);
                            }
                            oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CoverageID");
                            if (oElement != null)
                            {
                                iPolCvgRowId  = Convert.ToInt32(oElement.InnerText);
                            }
                            oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ReserveTypeCode");
                            if (oElement != null)
                            {
                                iResTypeCode  = Convert.ToInt32(oElement.InnerText);
                            }
							oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//CvgLossId");
                            if (oElement != null)
                            {
                                iCvgLossId = Convert.ToInt32(oElement.InnerText);
                            }

                            

                            //mbahl3 Jira [RMA -RMA-398 ]
//                            sbSql.AppendFormat(@"SELECT MAX(RSV_HIST_ROW_ID) RSV_HIST_ROW_ID FROM RSV_SUP_APPROVAL_HIST 
//                                                 WHERE RSV_ROW_ID = (SELECT MAX(RSV_ROW_ID) FROM RESERVE_HISTORY 
//                                                 WHERE CLAIM_ID ={0} AND CLAIMANT_EID ={1} AND POLCVG_LOSS_ROW_ID ={2} AND RESERVE_TYPE_CODE ={3})",
//                                                 iClaimId, iClaimantEid, iCvgLossId, iResTypeCode);

                            sbSql.AppendFormat(@"SELECT MAX(RSV_HIST_ROW_ID) RSV_HIST_ROW_ID FROM RSV_SUP_APPROVAL_HIST 
                                                WHERE RC_ROW_ID = " + iRCRowId);

                            //mbahl3 Jira [RMA -RMA-398 ]
                                                 
                            objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                            using (objRdr)
                            {
                                if (objRdr.Read())
                                {
                                    sRsvHistRowId = objRdr.GetInt32("RSV_HIST_ROW_ID").ToString();
                                }
                            }
                        }
                    }
                }
                }
                oElement = (XmlElement)p_objXmlIn.SelectSingleNode("//ApproveorRejectRsv");
                if (oElement != null)
                {
                    sAction = oElement.InnerText;
                    
                }

                if (!string.IsNullOrEmpty(sRsvHistRowId) && !string.IsNullOrEmpty(sAction))
                {
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                     sbSql.Remove(0, sbSql.Length);
					 //mbahl3 Reserve Approval Enhancement Mits 32471
                     if ((bHasRsvRowId) || ((iRCRowId==0) && (iRsvRowId >0)))  //mbahl3 JIRA [RMA-331]
                     {
                         //fixed for reserev approval enhancement as new columns added to RSV_SUP_APPROVAL_HIST table 
                        //mbahl3 JIRA RMA-422
                         sbSql.AppendFormat(@"SELECT CLAIM.LINE_OF_BUS_CODE, RSV_HIST.CLAIM_ID, RSV_HIST.RESERVE_TYPE_CODE,CLAIM.CLAIM_NUMBER,
                                                RSV_HIST.UNIT_ID,RSV_HIST.CLAIMANT_EID,RSV_HIST.POLCVG_ROW_ID,
                                                RSV_SUP.RSV_ROW_ID,RSV_HIST.RESERVE_AMOUNT,RSV_SUP.SUBMITTED_TO,
                                                RSV_SUP.DTTM_SUBMISSION ,RSV_HIST.POLCVG_LOSS_ROW_ID,RSV_SUP.DATE_ENTERED
                                                FROM RSV_SUP_APPROVAL_HIST RSV_SUP,RESERVE_HISTORY RSV_HIST,CLAIM 
                                                WHERE  RSV_SUP.RSV_ROW_ID=RSV_HIST.RSV_ROW_ID 
                                                AND  RSV_HIST.CLAIM_ID=CLAIM.CLAIM_ID 
                                                AND RSV_SUP.RSV_HIST_ROW_ID={0}", sRsvHistRowId);
                         //mbahl3 JIRA RMA-422
                     }
                     else
                     {
                         sbSql.AppendFormat(@"SELECT CLAIM.LINE_OF_BUS_CODE, RSV_HIST.CLAIM_ID, RSV_HIST.RESERVE_TYPE_CODE,CLAIM.CLAIM_NUMBER,
                                            RSV_HIST.UNIT_ID,RSV_HIST.CLAIMANT_EID,RSV_HIST.POLCVG_ROW_ID,
                                            RSV_SUP.RSV_ROW_ID,RSV_HIST.RESERVE_AMOUNT,RSV_SUP.SUBMITTED_TO,
                                            RSV_SUP.DTTM_SUBMISSION ,RSV_HIST.POLCVG_LOSS_ROW_ID,RSV_SUP.DATE_ENTERED 
                                            FROM RSV_SUP_APPROVAL_HIST RSV_SUP,RESERVE_CURRENT RSV_HIST,CLAIM  
                                            WHERE  RSV_SUP.RC_ROW_ID=RSV_HIST.RC_ROW_ID 
                                            AND  RSV_HIST.CLAIM_ID=CLAIM.CLAIM_ID 
                                            AND RSV_SUP.RSV_HIST_ROW_ID={0}", sRsvHistRowId);
                          //mbahl3 JIRA RMA-422
                     }
                     //mbahl3 Reserve Approval Enhancement Mits 32471
                    objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                    using (objRdr)
                    {
                        if (objRdr.Read())
                        {
                            iClaimId = objRdr.GetInt32("CLAIM_ID");
                            iLOB = objRdr.GetInt32("LINE_OF_BUS_CODE");
                            iResTypeCode = objRdr.GetInt32("RESERVE_TYPE_CODE");
                            dAmount = objRdr.GetDouble("RESERVE_AMOUNT");
                            iSubmittedTo = objRdr.GetInt32("SUBMITTED_TO");
                            sDTTMSubmit = objRdr.GetString("DTTM_SUBMISSION");
                            iReserveHistoryId = objRdr.GetInt32("RSV_ROW_ID");
                            iUnitId = objRdr.GetInt32("UNIT_ID");
                            iClaimantEid = objRdr.GetInt32("CLAIMANT_EID");
                            iPolCvgRowId = objRdr.GetInt32("POLCVG_ROW_ID");
                            //skhare7
                            sClaimNumber=objRdr.GetString("CLAIM_NUMBER");
							iCvgLossId = objRdr.GetInt32("POLCVG_LOSS_ROW_ID");
                            sDateEntered = Conversion.ConvertObjToStr(objRdr.GetValue("DATE_ENTERED"));//mbahl3 JIRA 422
                           
                        }
                    }
                    objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                    if (objColLobSettings[iLOB].AddRecoveryReservetoTotalIncurredAmount == -1)
                    {
                        b_AddRecoveryReservetoTotalIncurredAmount = true;
                    }


                    //MITS 24259:Top Level and LOB mgrs do not have to be in the submitted_by users managerial tree
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1");
                    objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                    using (objRdr)
                    {
                        if (objRdr.Read())
                        {
                            iTopLvlMgr = objRdr.GetInt32("USER_ID");
                        }
                    }

                    if (iTopLvlMgr == m_userLogin.UserId)
                    {
                        iUserWhoWillApprove = iTopLvlMgr;
                    }
                    else
                    {

                        //sachin 6385
                        sbSQL.Remove(0, sbSQL.Length);
                        sbSQL = new StringBuilder();
                        sbSQL.Append(" SELECT RES_LMT_FLAG,CLM_INC_LMT_FLAG ,CLAIM_NUMBER");
                        sbSQL.Append(" FROM SYS_PARMS_LOB,CLAIM WHERE CLAIM.CLAIM_ID = " + iClaimId);
                        sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = SYS_PARMS_LOB.LINE_OF_BUS_CODE ");

                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                bResLimit = Conversion.ConvertLongToBool
                                    (Conversion.ConvertStrToLong
                                    (objReader.GetValue("RES_LMT_FLAG").ToString()), m_iClientId);
                                bClaimIncLimit = Conversion.ConvertLongToBool
                                    (Conversion.ConvertStrToLong
                                    (objReader.GetValue("CLM_INC_LMT_FLAG").ToString()), m_iClientId);
                            }
                            objReader.Close();
                        }                       
                        sbSQL.Remove(0, sbSQL.Length);
                        //sachin 6385 ends
                        //Check Reserve Limits
                        bExceedLimit = CommonFunctions.CheckReserveLimits(iClaimId, iResTypeCode, dAmount, m_userLogin.UserId, m_userLogin.GroupId, iLOB, m_sConnectionString, m_iClientId);
                        if (bExceedLimit && bResLimit && bUseHoldReserves)
                        {
                            lstThisHoldReason.Add(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                            b_HoldReasonRes = true;
                        }
                        //jira 6385 starts
                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
                        //Syadav55 Jira-15210 starts
                        //string recoverycode = Convert.ToString(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_TYPE"), objCache.GetCodeId("R", "MASTER_RESERVE")));
                        string recoverycode = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_TYPE") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("R", "MASTER_RESERVE") + "  AND DELETED_FLAG=0"));
                        //int holdreservecode = Convert.ToInt32(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")));
                        int holdreservecode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                        //Syadav55 Jira-15210 starts
                        bClaimIncExceedLimit = CommonFunctions.CheckClaimIncurredLimits(iClaimId, dAmount,holdreservecode, recoverycode, m_userLogin.UserId, m_userLogin.GroupId, iLOB, b_AddRecoveryReservetoTotalIncurredAmount, m_sConnectionString, m_iClientId);
                        if (bClaimIncExceedLimit && bClaimIncLimit && bUseHoldIncurredLimits)
                        {
                            lstThisHoldReason.Add(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                            b_HoldReasonInc = true;
                        }
                        //jira 6385 ends

                        //Syadav55 Jira 6385 starts
                        objRC.MoveTo(iRCRowId);
                        if (sAction == "Reject" && !bExceedLimit && !bClaimIncExceedLimit)
                        {
                            foreach (HoldReason objHoldReason in objRC.HoldReasonList)
                            {
                                if (objHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                {
                                    if (!bExceedLimit)
                                    {
                                        if (sAction == "Reject")
                                        {
                                            //Syadav55 Jira-15210 starts
                                            //objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT")), m_iClientId);
                                            objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                            //Syadav55 Jira-15210 ends
                                        }
                                        objHoldReason.Save();
                                    }
                                }
                                if (objHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                {
                                    if (!bClaimIncExceedLimit)
                                    {
                                        if (sAction == "Reject")
                                        {
                                            //Syadav55 Jira-15210 starts
                                            //objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT")), m_iClientId);
                                            objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                            //Syadav55 Jira-15210 ends
                                        }
                                        objHoldReason.Save();
                                    }
                                }
                            }
                            bRejectStatus = true;
                        }
                        //Syadav55 Jira 6385 ends

                        if ((bExceedLimit && bUseHoldReserves && bResLimit) || (bClaimIncExceedLimit && bUseHoldIncurredLimits && bClaimIncLimit))
                        {
                            if (iSubmittedTo == m_userLogin.UserId && !bRejectStatus)
                            {
                                //Syadav55 Jira 13548 starts- When notify to immediate supervisory checkbox is unchecked, we need to recalculate the Incurred amount if its in limit of approver, or else set approver who have sufficent limit to approve the reserve
                                if (!bNotifySupReserves)
                                {   
                                    iNewSubmittedTo = objRC.GetSuperVisorForClaimInc(bUseCurrAdjReserves, bNotifySupReserves, dAmount);
                                }
                                //MITS 24258: Submitted To is updated when Submitted To user doesnt have sufficient limit to approve/reject
                                //This will happen only in case Jump functionality is OFF
                                else
                                { 
                                    iNewSubmittedTo = CommonFunctions.GetManagerID(iSubmittedTo, m_userLogin.objRiskmasterDatabase.DataSourceId, m_iClientId); 
                                }
                                if (iNewSubmittedTo == 0)
                                {
                                    //sbSql.Remove(0, sbSql.Length);
                                    //sbSql.Append("SELECT NOTIFY_SUP_RESERVES FROM CHECK_OPTIONS");
                                    //objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                                    //using (objRdr)
                                    //{
                                    //    if (objRdr.Read())
                                    //    {
                                    //        bNotifySupReserves = objRdr.GetBoolean("NOTIFY_SUP_RESERVES");
                                    //    }
                                    //}
                                    iNewSubmittedTo = CommonFunctions.GetLobOrTopLevelManagerID(iLOB, true, m_sConnectionString, m_userLogin.UserId, dAmount, m_iClientId);
                                }

                               //mbahl3 Reserve Approval Enhancement Mits 32471
                                //CommonFunctions.SaveToSupHistTable(0, iSubmittedTo, iUserId, ResStatusCode, Context.RMDatabase.ConnectionString);

                                CommonFunctions.SaveToSupHistTable(iReserveHistoryId, iNewSubmittedTo, m_userLogin.UserId, objCache.GetCodeId("H", "RESERVE_APPROVAL_STATUS"), m_sConnectionString, iRCRowId, iOrigStatusCode, dOrigStatusAmount, m_userLogin.LoginName, Conversion.ToDbDateTime(System.DateTime.Now), sDateEntered, string.Empty, dAmount, m_iClientId);
                                 //mbahl3 Reserve Approval Enhancement Mits 32471

                                objRC.MoveTo(iRCRowId); 	//mbahl3 JIRA [RMA-318]
                                objRC.EmailAndDiaryGeneration(iNewSubmittedTo, bDisableDiaryNotify, bDisableEmailNotify, sClaimNumber, "", "SUBMIT");
                                HoldReason objHoldReasonApprove = null;
                                HoldReason objHoldReasonReject = null;
                                if (objCache.GetRelatedCodeId(objRC.ResStatusCode) == objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") && (sAction == "Approve"))
                                {
                                    foreach (int iThisHoldReason in lstThisHoldReason)
                                    {
                                        if (b_HoldReasonRes)
                                        {
                                            objHoldReasonApprove = (HoldReason)objRC.Context.Factory.GetDataModelObject("HoldReason", false);

                                            objHoldReasonApprove.MoveToRcRowId(objRC.RcRowId, iThisHoldReason, objCache.GetTableId("RESERVE_CURRENT"));

                                            if (objHoldReasonApprove.RowId > 0 && objRC.HoldReasonList.ContainsKey(objHoldReasonApprove.RowId))
                                            {
                                                objHoldReasonApprove = objRC.HoldReasonList[objHoldReasonApprove.RowId];
                                            }

                                            objHoldReasonApprove.HoldRowID = objRC.RcRowId;
                                            objHoldReasonApprove.TableID = objCache.GetTableId("RESERVE_CURRENT");
                                            objHoldReasonApprove.HoldReasonCode = iThisHoldReason;
                                            //Syadav55 Jira-15210 starts
                                            //objHoldReasonApprove.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")), m_iClientId);
                                            objHoldReasonApprove.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                            //Syadav55 Jira-15210 ends

                                            if (objHoldReasonApprove.RowId <= 0)
                                            {
                                                objRC.HoldReasonList.Add(objHoldReasonApprove);
                                            } 
                                            objHoldReasonApprove.Save();

                                        }
                                        else
                                        {
                                            if (objRC.HoldReasonList.Count > 0)
                                            {
                                                foreach (HoldReason oHoldReason in objRC.HoldReasonList)
                                                {
                                                    //Syadav55 Jira-15210 starts
                                                    //if (oHoldReason.HoldStatusCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")), m_iClientId) &&
                                                    //    oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                                    if (oHoldReason.HoldStatusCode == Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId) &&
                                                        oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                                    {
                                                        //oHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("A", "RESERVE_STATUS_PARENT")), m_iClientId);
                                                        oHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("A", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                                        //Syadav55 Jira-15210 ends
                                                        oHoldReason.Save();
                                                    }
                                                }
                                            }
                                        }
                                        if (b_HoldReasonInc)
                                        {
                                            objHoldReasonApprove = (HoldReason)objRC.Context.Factory.GetDataModelObject("HoldReason", false);

                                            objHoldReasonApprove.MoveToRcRowId(objRC.RcRowId, iThisHoldReason, objCache.GetTableId("RESERVE_CURRENT"));

                                            if (objHoldReasonApprove.RowId > 0 && objRC.HoldReasonList.ContainsKey(objHoldReasonApprove.RowId))
                                            {
                                                objHoldReasonApprove = objRC.HoldReasonList[objHoldReasonApprove.RowId];
                                            }

                                            objHoldReasonApprove.HoldRowID = objRC.RcRowId;
                                            objHoldReasonApprove.TableID = objCache.GetTableId("RESERVE_CURRENT");
                                            objHoldReasonApprove.HoldReasonCode = iThisHoldReason;
                                            //Syadav55 Jira-15210 starts
                                            //objHoldReasonApprove.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")), m_iClientId);
                                            objHoldReasonApprove.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                            //Syadav55 Jira-15210 ends
                                            if (objHoldReasonApprove.RowId <= 0)
                                            {
                                                objRC.HoldReasonList.Add(objHoldReasonApprove);
                                            } 
                                            objHoldReasonApprove.Save();

                                        }
                                        else
                                        {
                                            if (objRC.HoldReasonList.Count > 0)
                                            {
                                                foreach (HoldReason oHoldReason in objRC.HoldReasonList)
                                                {
                                                    //Syadav55 Jira-15210 starts
                                                    //if (oHoldReason.HoldStatusCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")), m_iClientId) &&
                                                    //    oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                                    if (oHoldReason.HoldStatusCode == Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId) &&
                                                        oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                                    {
                                                        //oHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("A", "RESERVE_STATUS_PARENT")), m_iClientId);
                                                        oHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("A", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                                        //Syadav55 Jira-15210 ends
                                                        oHoldReason.Save();
                                                    }
                                                }
                                            }
                                        }
                                    }                                          

                                    foreach (HoldReason oHoldReason in objRC.HoldReasonList.Cast<HoldReason>().OrderBy(h => h.RowId))
                                    {
                                        if (oHoldReason.HoldStatusCode == holdreservecode)
                                        {
                                            if ((oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objRC.Context.LocalCache.GetChildCodeIds(objRC.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objRC.Context.LocalCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId)) ||
                                                (oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objRC.Context.LocalCache.GetChildCodeIds(objRC.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objRC.Context.LocalCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId)))
                                            {
                                                if (StrHoldReasonDesc == "")
                                                    StrHoldReasonDesc = objRC.Context.LocalCache.GetCodeDesc(oHoldReason.HoldReasonCode);
                                                else
                                                    StrHoldReasonDesc = StrHoldReasonDesc + " and " + objRC.Context.LocalCache.GetCodeDesc(oHoldReason.HoldReasonCode);                                                
                                            }                                           
                                        }
                                    }                                   
                                    if (b_Msg == false)
                                        throw new RMAppException(StrHoldReasonDesc);
                                    else
                                        StrHoldReasonDesc = string.IsNullOrEmpty(StrHoldReasonDesc) ? string.Empty : " as " + StrHoldReasonDesc;
                                    throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.NotAuthorised", m_iClientId) + StrHoldReasonDesc);
                                    //throw new RMAppException(StrHoldReasonDesc);
                                }
                                //Syadav55 Jira 6385 starts
                                else if (iSubmittedTo == m_userLogin.UserId && sAction == "Reject" && !bRejectStatus)
                                {
                                    foreach (int iThisHoldReason in lstThisHoldReason)
                                    {
                                        if (b_HoldReasonRes)
                                        {
                                            objHoldReasonReject = (HoldReason)objRC.Context.Factory.GetDataModelObject("HoldReason", false);

                                            objHoldReasonReject.MoveToRcRowId(objRC.RcRowId, iThisHoldReason, objCache.GetTableId("RESERVE_CURRENT"));

                                            if (objHoldReasonReject.RowId > 0 && objRC.HoldReasonList.ContainsKey(objHoldReasonReject.RowId))
                                            {
                                                objHoldReasonReject = objRC.HoldReasonList[objHoldReasonReject.RowId];
                                            }

                                            objHoldReasonReject.HoldRowID = objRC.RcRowId;
                                            objHoldReasonReject.TableID = objCache.GetTableId("RESERVE_CURRENT");
                                            objHoldReasonReject.HoldReasonCode = iThisHoldReason;
                                            //Syadav55 Jira-15210 starts
                                            //objHoldReasonReject.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")), m_iClientId);
                                            objHoldReasonReject.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                            //Syadav55 Jira-15210 ends
                                            
                                            if (objHoldReasonReject.RowId <= 0)
                                            {
                                                objRC.HoldReasonList.Add(objHoldReasonReject);
                                            }
                                            objHoldReasonReject.Save();

                                        }
                                        
                                        if (b_HoldReasonInc)
                                        {
                                            objHoldReasonReject = (HoldReason)objRC.Context.Factory.GetDataModelObject("HoldReason", false);

                                            objHoldReasonReject.MoveToRcRowId(objRC.RcRowId, iThisHoldReason, objCache.GetTableId("RESERVE_CURRENT"));

                                            if (objHoldReasonReject.RowId > 0 && objRC.HoldReasonList.ContainsKey(objHoldReasonReject.RowId))
                                            {
                                                objHoldReasonReject = objRC.HoldReasonList[objHoldReasonReject.RowId];
                                            }

                                            objHoldReasonReject.HoldRowID = objRC.RcRowId;
                                            objHoldReasonReject.TableID = objCache.GetTableId("RESERVE_CURRENT");
                                            objHoldReasonReject.HoldReasonCode = iThisHoldReason;
                                            //Syadav55 Jira-15210 starts
                                            //objHoldReasonReject.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT")), m_iClientId);
                                            objHoldReasonReject.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                            //Syadav55 Jira-15210 ends
                                            
                                            if (objHoldReasonReject.RowId <= 0)
                                            {
                                                objRC.HoldReasonList.Add(objHoldReasonReject);
                                            }
                                            objHoldReasonReject.Save();

                                        }
                                        
                                    } 

                                    foreach (HoldReason objHoldReason in objRC.HoldReasonList)
                                    {
                                        if (objHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                        {
                                            if (!bExceedLimit)
                                            {
                                                if (sAction == "Reject")
                                                {
                                                    //Syadav55 Jira-15210 starts
                                                    //objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT")), m_iClientId);
                                                    objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                                    //Syadav55 Jira-15210 ends
                                                }
                                            }
                                        }
                                        if (objHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                        {
                                            if (!bClaimIncExceedLimit)
                                            {
                                                if (sAction == "Reject")
                                                {
                                                    //Syadav55 Jira-15210 starts
                                                    //objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT")), m_iClientId);
                                                    objHoldReason.HoldStatusCode = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, "SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0"), m_iClientId);
                                                    //Syadav55 Jira-15210 ends
                                                }
                                            }
                                        }
                                    }

                                    foreach (HoldReason oHoldReason in objRC.HoldReasonList.Cast<HoldReason>().OrderBy(h => h.RowId))
                                    {
                                        if (oHoldReason.HoldStatusCode == holdreservecode)
                                        {
                                            if (oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objRC.Context.LocalCache.GetChildCodeIds(objRC.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objRC.Context.LocalCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                            {
                                                StrHoldReasonDesc = objRC.Context.LocalCache.GetCodeDesc(oHoldReason.HoldReasonCode);
                                            }
                                            else if (oHoldReason.HoldReasonCode == Conversion.ConvertObjToInt(objRC.Context.LocalCache.GetChildCodeIds(objRC.Context.LocalCache.GetTableId("HOLD_REASON_CODE"), objRC.Context.LocalCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId))
                                            {
                                                if (StrHoldReasonDesc == "")
                                                    StrHoldReasonDesc = objRC.Context.LocalCache.GetCodeDesc(oHoldReason.HoldReasonCode);
                                                else
                                                    StrHoldReasonDesc = StrHoldReasonDesc + " and " + objRC.Context.LocalCache.GetCodeDesc(oHoldReason.HoldReasonCode);
                                            }
                                        }
                                    }
                                    if (b_Msg == false)
                                        throw new RMAppException(StrHoldReasonDesc);
                                    else
                                        StrHoldReasonDesc = string.IsNullOrEmpty(StrHoldReasonDesc) ? string.Empty : " as " + StrHoldReasonDesc;
                                    throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.NotAuthorised", m_iClientId) + StrHoldReasonDesc);
                                    //throw new RMAppException(StrHoldReasonDesc);
                                    // throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.NotAuthorised", m_iClientId));
                                }
                                //sachin 6385 ends
                                //  (int p_iSubmittedTo, bool p_bDisableDiaryNotify, bool p_bDisableEmailNotify, string p_sClaimnumber, string p_sReason,string p_sModule)
                                //throw error:it exceeds ur limit
                                //throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.NotAuthorised", m_iClientId));
                                // throw new RMAppException(StrHoldReasonDesc);//sachin 6385
                            }
                            else
                            {
                                //sharishkumar Jira 6385 starts
                                if (bExceedLimit && bUseHoldReserves && bResLimit)
                                    StrHoldReasonDesc = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_RES_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                                if (bClaimIncExceedLimit && bUseHoldIncurredLimits && bClaimIncLimit)
                                {
                                    if (StrHoldReasonDesc == "")
                                        StrHoldReasonDesc = objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                                    else
                                        StrHoldReasonDesc = StrHoldReasonDesc + " and " + objCache.GetCodeDesc(Conversion.ConvertObjToInt(objCache.GetChildCodeIds(objCache.GetTableId("HOLD_REASON_CODE"), objCache.GetCodeId("EXC_INC_LIM", "HOLD_REASON_PARENT")), m_iClientId));
                                }
                                //throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.NotAuthorised", m_iClientId));
                                if (b_Msg == false)
                                throw new RMAppException(StrHoldReasonDesc);
                                else
                                StrHoldReasonDesc = string.IsNullOrEmpty(StrHoldReasonDesc) ? string.Empty : " as " + StrHoldReasonDesc;
                                throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.NotAuthorised", m_iClientId) + StrHoldReasonDesc);
                                //sharishkumar Jira 6385 ends
                                //throw error:it exceeds ur limit
                                //Ijha: MITS 26476: The Error thrown is "You do not have the authority to Approve/Reject the Reserve "
                            }
                        }
                        else
                        {
                          if(bUseSupAppReserves)//Add if condition by kuladeep for MITS:33322-If User have permission to approve and Sup Approval OFF in that case skip this block.
                            {
                            if (iSubmittedTo == m_userLogin.UserId)
                            {
                                //MITS 24257: No matter Time has been Lapsed or not,if it is under User's limit,he can approve it
                                iUserWhoWillApprove = iSubmittedTo;
                            }
                            else
                            {
                                //MITS 24259:Top Level and LOB mgrs do not have to be in the submitted_To users managerial tree
                                //Check for LOB Mgr
                                sbSql.Remove(0, sbSql.Length);
                                sbSql.AppendFormat("SELECT USER_ID FROM LOB_SUP_APPROVAL WHERE LOB_CODE = {0}", iLOB);
                                objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                                using (objRdr)
                                {
                                    if (objRdr.Read())
                                    {
                                        iLobMgr = objRdr.GetInt32("USER_ID");
                                    }
                                }

                                if (iLobMgr == m_userLogin.UserId)
                                {
                                    iUserWhoWillApprove = iLobMgr;
                                }
                                else
                                {
                                    //Whether User is in Supervisory Chain 
                                    iLevel = GetSupervisoryLevel(iSubmittedTo, m_userLogin.UserId, iClaimId, m_userLogin.objRiskmasterDatabase.DataSourceId, m_sConnectionString);
                                    if (iLevel == 0)
                                    {
                                        //string sSql = "SELECT GROUP_ID FROM USER_MEMBERSHIP WHERE USER_ID = " + iSubmittedTo; igupta3:MITS-34389: removing the group restriction
                                        //int iGroupId = Convert.ToInt32(DbFactory.ExecuteScalar(m_sConnectionString, sSql));
                                        //if (iGroupId == m_userLogin.GroupId && bUseSupAppReserves && bAllowSupGrpApprove) igupta3:MITS-34389: removing the group restriction
                                        if (bUseSupAppReserves && bAllowSupGrpApprove )
                                        {
                                            iUserWhoWillApprove = m_userLogin.UserId;
                                        }
                                        else
                                        {
                                            //throw error-not in managerial chain
                                            throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.NotAuthorised", m_iClientId));
                                        }
                                    }
                                    else
                                    {
                                        iUserWhoWillApprove = m_userLogin.UserId;
                                    }
                                }
                            }
                            } //Add condition by kuladeep--End mits:33322
                        }
                    }

                    //Add or (!bExceedLimit && !bUseSupAppReserves && bUseHoldReserves) condition by kuladeep for MITS:33322-If User have permission to approve and Sup Approval OFF.
                    //Jira 6385 starts
                    //if (iUserWhoWillApprove != 0 || (!bExceedLimit && !bUseSupAppReserves && bUseHoldReserves))
                    if (iUserWhoWillApprove != 0 || (!bExceedLimit && !bUseSupAppReserves && bUseHoldReserves) || (!bClaimIncExceedLimit && !bUseSupAppReserves && bUseHoldIncurredLimits))
                        //Jira 6385 ends
                    {
                        objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//rkaur27

                        if (iRCRowId == 0)//In case it is not from Edit Reserve Screen when AC is ON,we have to get rcrowid from db
                        {
                            sbSql.Remove(0, sbSql.Length);
                            sbSql.AppendFormat("SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE CLAIM_ID = {0} AND UNIT_ID = {1} AND CLAIMANT_EID = {2} AND RESERVE_TYPE_CODE = {3}", iClaimId, iUnitId, iClaimantEid, iResTypeCode);
                            if (objSysSettings.MultiCovgPerClm != 0)
                            {
                                sbSql.AppendFormat(" AND POLCVG_LOSS_ROW_ID ={0}", iCvgLossId);
                            }
                            objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                            using (objRdr)
                            {
                                if (objRdr.Read())
                                {
                                    iRCRowId = objRdr.GetInt32("RC_ROW_ID");
                                }
                            }
                        }
                         //mbahl3 Reserve Approval Enhancement Mits 32471
                        if (iOrigStatusCode == 0)//Status code
                        {
                            sbSql.Remove(0, sbSql.Length);
                            sbSql.AppendFormat("SELECT RES_STATUS_CODE FROM RESERVE_CURRENT WHERE RC_ROW_ID ={0}", iRCRowId);
                           
                            objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                            using (objRdr)
                            {
                                if (objRdr.Read())
                                {
                                    iOrigStatusCode = Conversion.ConvertObjToInt(objRdr.GetValue("RES_STATUS_CODE"), m_iClientId);
                                }
                            }
                        }
                       
                        DMF = new DataModelFactory(m_userLogin, m_iClientId);//rkaur27
                        objRC = (ReserveCurrent)DMF.GetDataModelObject("ReserveCurrent", false);

                        objRC.MoveTo(iRCRowId);
                        if (dOrigStatusAmount == 0)//reserev amount
                        {
                            sbSql.Remove(0, sbSql.Length);
                            sbSql.AppendFormat("SELECT RESERVE_AMOUNT FROM RESERVE_CURRENT WHERE RC_ROW_ID={0}", iRCRowId);

                            objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                            using (objRdr)
                            {
                                if (objRdr.Read())
                                {
                                    dOrigStatusAmount = Conversion.ConvertObjToDouble(objRdr.GetValue("RESERVE_AMOUNT"), m_iClientId);
                                }
                            }
                        }
                        if (sDttmRcdAdded == null)//Reserve added date
                        {

                            sbSql.Remove(0, sbSql.Length);
                            sbSql.AppendFormat("SELECT DTTM_RCD_ADDED FROM RESERVE_CURRENT WHERE RC_ROW_ID={0}", iRCRowId);

                            objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                            using (objRdr)
                            {
                                if (objRdr.Read())
                                {
                                    sDttmRcdAdded = Conversion.ConvertObjToStr(objRdr.GetValue("DTTM_RCD_ADDED"));

                                }
                            }
                        }
                        if (sDateEntered == null)//Reserve added date
                        {

                            sbSql.Remove(0, sbSql.Length);
                            sbSql.AppendFormat("SELECT DATE_ENTERED FROM RESERVE_CURRENT WHERE RC_ROW_ID={0}", iRCRowId);
                            objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                            using (objRdr)
                            {
                                if (objRdr.Read())
                                {
                                    sDateEntered = objRdr.GetString("DATE_ENTERED");
                                }
                            }
                        }
                      //mbahl3 Reserve Approval Enhancement Mits 32471
                        
                       

                        DMF = new DataModelFactory(m_userLogin, m_iClientId);
                        objRC = (ReserveCurrent)DMF.GetDataModelObject("ReserveCurrent", false);
                        objRC.MoveTo(iRCRowId); 
                        objRC.sUpdateType = "Reserves";                       
                        objRC.iApproverId = m_userLogin.UserId;
                        objRC.iHoldRsvRowId = iReserveHistoryId;
                        objRC.DttmApproval = Conversion.ToDbDateTime(DateTime.Now);   
                        //skhare7 R8 Supervisory approval
                        objRC.sAppRejReason = sAppRejReason;
                        objRC.DateEntered = DateTime.Now.ToShortDateString();  
                        //smishra54: 11/17/2011 - Supervisory approval fix
                        objRC.iUserId = m_userLogin.UserId;
                        objRC.iGroupId = m_userLogin.GroupId;
                        //smishra54:End
                            switch (sAction)
                            {
                                case "Approve":
                                   objRC.sUpdateType = "Approve Reserves"; //MITS 32398 mcapps2
                                  // objRC.ResStatusCode = objCache.GetCodeId("A", "RESERVE_STATUS");

                                   //JIRA RMA-6310 nshah28 start(checking that the parent code for "A" has been set as "A Approved")
                                   sbSql.Remove(0, sbSql.Length);
                                   sbSql.AppendFormat("SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("A", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0");
                                   objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                                   if (objRdr.Read())
                                   {
                                       if (objRdr.GetValue("CODE_ID") != DBNull.Value)
                                           objRC.ResStatusCode = objRdr.GetInt32("CODE_ID");
                                       else
                                           throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.ParentCodeNotSetForApprove", m_iClientId));
                                   }
                                   else
                                   {
                                       throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.ParentCodeNotSetForApprove", m_iClientId));
                                   }

                                   //JIRA RMA-6310 nshah28 end

                                    objRC.RsvSupHistId =Conversion.ConvertStrToInteger( sRsvHistRowId); //mbahl3 Reserve Approval Enhancement Mits 32471
                                   objRC.Save();        
                                    //skhare7 R8
                                   objRC.EmailAndDiaryGeneration(iUserWhoWillApprove,bDisableDiaryNotify, bDisableEmailNotify, sClaimNumber, sAppRejReason, "APPROVE");
                                    break;
                                case "Reject":
                                    objRC.sUpdateType = "Approve Reserves";  //MITS 32398 mcapps2
                                   // objRC.ResStatusCode = objCache.GetCodeId("R", "RESERVE_STATUS");

                                    //JIRA RMA-6310 nshah28 start(Commenting just above line and using below code to checking that the parent code "RE" Rejected has been set)
                                    sbSql.Remove(0, sbSql.Length);
                                    sbSql.AppendFormat("SELECT CODE_ID FROM CODES WHERE TABLE_ID = " + objCache.GetTableId("RESERVE_STATUS") + " AND RELATED_CODE_ID= " + objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT") + "  AND DELETED_FLAG=0");
                                    objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                                    if (objRdr.Read())
                                    {
                                        if (objRdr.GetValue("CODE_ID") != DBNull.Value)
                                            objRC.ResStatusCode = objRdr.GetInt32("CODE_ID");
                                        else
                                            throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.ParentCodeNotSetForReject", m_iClientId));
                                    }
                                    else
                                    {
                                        throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.ParentCodeNotSetForReject", m_iClientId));
                                    }

                                    //JIRA RMA-6310 nshah28 end

                                    objRC.Save();
                                    int iRsvSupHistId = objRC.RsvSupHistId; //mbahl3 Reserve Approval Enhancement Mits 32471
                                    //skhare7 R8
                                    // objRC.EmailAndDiaryGeneration(iUserWhoWillApprove,bDisableDiaryNotify, bDisableEmailNotify, sClaimNumber,sAppRejReason, "REJECT");
                                    //Bringing Reserve Current back to its original state
                                    sbSql.Remove(0, sbSql.Length);
                                    sbSql.AppendFormat("SELECT RESERVE_AMOUNT,DATE_ENTERED,ENTERED_BY_USER,REASON,RES_STATUS_CODE FROM RESERVE_HISTORY WHERE CLAIM_ID={0} AND CLAIMANT_EID = {1} AND UNIT_ID ={2} AND RESERVE_TYPE_CODE = {3} ", iClaimId, iClaimantEid, iUnitId, iResTypeCode);
                                    if (objSysSettings.MultiCovgPerClm != 0)
                                    {
                                        sbSql.AppendFormat(" AND POLCVG_LOSS_ROW_ID ={0}", iCvgLossId);
                                    }
                                    //sbSql.AppendFormat(" AND RES_STATUS_CODE NOT IN ({0},{1})", objCache.GetCodeId("H", "RESERVE_STATUS"), objCache.GetCodeId("R", "RESERVE_STATUS")); 

                                    //JIRA RMA-6310 nshah28 start(Checking parent code of "Reject" and "Hold")
                                    sbSql.AppendFormat(" AND RES_STATUS_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID = {0} AND RELATED_CODE_ID IN({1},{2})", objCache.GetTableId("RESERVE_STATUS"), objCache.GetCodeId("RE", "RESERVE_STATUS_PARENT"), objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"));
                                    sbSql.Append(" AND DELETED_FLAG=0)");
                                    //JIRA RMA-6310 nshah28 end

                                    sbSql.Append(" ORDER BY RSV_ROW_ID DESC");
                                    objRdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                                    
                                    objRC.iApproverId = 0;
                                    objRC.iHoldRsvRowId = 0;
                                    objRC.DttmApproval = string.Empty;
                                    objRC.ByPassSupervisoryApproval = false; 	//mbahl3 JIRA [RMA-308]
                                    using (objRdr)
                                    {
                                        if (objRdr.Read())
                                        {                                           
                                            objRC.ReserveAmount = objRdr.GetDouble("RESERVE_AMOUNT");
                                            objRC.DateEntered = objRdr.GetString("DATE_ENTERED");
                                            objRC.EnteredByUser = objRdr.GetString("ENTERED_BY_USER");
                                            objRC.Reason = objRdr.GetString("REASON");
                                            objRC.ResStatusCode = objRdr.GetInt32("RES_STATUS_CODE");
                                            objRC.ByPassUtilitySetting = true;
                                        }
                                        else
                                        {
                                            objRC.ReserveAmount = 0;
                                            objRC.ClaimCurrReserveAmt = 0; // RMA-12809.relates to RMA-3887
											//mbahl3 Reserve Approval Enhancement Mits 32471
                                            objRC.ResStatusCode = objCache.GetCodeId("O", "RESERVE_STATUS");
                                        }
                                        objRC.RsvSupHistId = iRsvSupHistId;
										//mbahl3 Reserve Approval Enhancement Mits 32471
                                        objRC.Save();
                                    }
                                     objRC.EmailAndDiaryGeneration(iUserWhoWillApprove, bDisableDiaryNotify, bDisableEmailNotify, sClaimNumber, sAppRejReason, "REJECT"); //mbahl3 JIRA[RMA-307]
                                    break;
                                   
                            }
                            
                    }                    
                }
                //tanwar2 - Check if reserve status is approved - start
                //Rechecking here (and not in the switch statement) as reserve sent for approval can theoritically still remain on hold
                //(for example when approver does not have authority to approve)
                p_bReserveApproved = (objRC.Context.LocalCache.GetRelatedCodeId(objRC.ResStatusCode) == objRC.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT"));
                p_dAmount = objRC.ReserveAmount;
                p_iLossCode = objRC.iLossTypecode;
                //iLossTypecode is coming as zero (0). Is this behavior correct?
                if (p_iLossCode == 0)
                {
                    p_iLossCode = objRC.Context.DbConnLookup.ExecuteInt("SELECT LOSS_CODE FROM COVERAGE_X_LOSS WHERE CVG_LOSS_ROW_ID = " + objRC.CoverageLossId);
                }
                p_iDisabilityCat = objRC.iDisabilityCode;
                p_iAdjusterEid = objRC.AssignAdjusterEid;
                p_iReserveCurrencyType = objRC.iReserveCurrencyCode;
                p_iReserveStatus = objRC.ResStatusCode;
                p_iClaimId = objRC.ClaimId;
                p_iReserveTypeCode = objRC.ReserveTypeCode;
                p_iCvgId = objRC.CoverageId;
                p_iClaimantEid = objRC.ClaimantEid;
                //tanwar2 - Check if reserve status is approved - end
            }
            catch (RMAppException p_objRmAEx)
            {
                throw p_objRmAEx;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.ApproveOrRejectReserve.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
            }
            
        }
        //MGaba2:R8: SuperVisory Approval:End

        /// <summary>
        /// Returns Claimants for a Claim 
        /// </summary>
        /// <param name="iClaimID"></param>
        /// <returns></returns>
        public XmlDocument GetClaimants(int iClaimID)
        {
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElement = null;
            XmlElement objXmlElementClaimant = null;
            XmlElement objXmlElementTmp = null; 
            Claim objClaim = null;

            try
            {
                objXmlDoc = new XmlDocument();

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objClaim = (Claim)(m_objDataModelFactory.GetDataModelObject("Claim",false));
                objClaim.MoveTo(iClaimID);
                
                objXmlElement = objXmlDoc.CreateElement("Claimants");
                objXmlDoc.AppendChild(objXmlElement); 

                foreach (Claimant objClaimant in objClaim.ClaimantList)
                {
                    objXmlElementClaimant = objXmlDoc.CreateElement("Claimant");
                    objXmlElement.AppendChild(objXmlElementClaimant);

                    objXmlElementTmp = objXmlDoc.CreateElement("ClaimantEID");
                    objXmlElementTmp.InnerText = objClaimant.ClaimantEid.ToString();
                    objXmlElementClaimant.AppendChild(objXmlElementTmp);

                    //bpaskova JIRA 10347 start
                    objXmlElementTmp = objXmlDoc.CreateElement("ClaimantRowId");
                    objXmlElementTmp.InnerText = objClaimant.ClaimantRowId.ToString();
                    objXmlElementClaimant.AppendChild(objXmlElementTmp);
                    //bpaskova JIRA 10347 end

                    objXmlElementTmp = objXmlDoc.CreateElement("ClaimantName");
                    objXmlElementTmp.InnerText = objClaimant.ClaimantEntity.GetLastFirstName();
                    objXmlElementClaimant.AppendChild(objXmlElementTmp); 

                }
                //rupal:start,multicurrency
                objXmlElementTmp = objXmlDoc.CreateElement("UseMulticurrency");
                objXmlElementTmp.InnerText = objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency.ToString();
                objXmlElement.AppendChild(objXmlElementTmp);
                //rupal:end

                //igupta3 JIRA RMA-616
                objXmlElementTmp = objXmlDoc.CreateElement("PrevtResModZero");
                objXmlElementTmp.InnerText = objClaim.Context.InternalSettings.SysSettings.PrevResModifyzero.ToString();
                objXmlElement.AppendChild(objXmlElementTmp);
                //JIRA end

                objXmlElementTmp = objXmlDoc.CreateElement("lobcode");
                objXmlElementTmp.InnerText = objClaim.LineOfBusCode.ToString(); 
                objXmlElement.AppendChild(objXmlElementTmp);
                //Added by sharishkumar for Mits 35472
                objXmlElementTmp = objXmlDoc.CreateElement("rmxlssenable");
                objXmlElementTmp.InnerText = objClaim.Context.InternalSettings.SysSettings.RMXLSSEnable.ToString();
                objXmlElement.AppendChild(objXmlElementTmp);
                //End Mits 35472
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetClaimants.GenericError", m_iClientId), objException);
            }
            finally
            {               
                objXmlElement = null;
                objXmlElementClaimant = null;
                objXmlElementTmp = null;
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
            }

            //Return the result to calling function
            return objXmlDoc;
        }


        /// <summary>
        /// Get Claim Reserve Information when Multiple Coverages per Claim is ON
        /// </summary>
        public XmlDocument GetReservesBOB(int p_iClaimID, int p_iClaimantEid,int p_iCurrType)
        {            
            XmlElement objXmlElement = null;
            XmlElement objXmlElementReason = null;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlReserveElement = null;
            XmlElement objXmlElementTmp = null;
            XmlElement objXmlElementStatus = null;
            StringBuilder sbSQL = null;
            DbReader objReader = null;
            structReserves[] arrReserves = null;
            Policy objPolicy = null;
            bool bProcess = false;
            //bool bDoNotRecalcReserves = false;
            int iCnt = 0;
            SysSettings objSysSetting = null;
            int iCvgTypeCode = 0;
            LocalCache objCache = null;
            int iLOB = 0;
            Claim objClaim = null;
            string sCaption = String.Empty;
            string sText = String.Empty;
            ColLobSettings objColLobSettings = null;
            XmlElement objXmlHideEnhancedNotes = null;
            string sDBType = string.Empty;
            //tanwar2 - mits 30910 - start
            bool bApplyDedToPayments = false;
            int iDedReserveTypeCode = 0;
            //tanwar2 - mits 30910 - end
            //aaggarwal29 :Start RMA-9856
            string sCancelledCode = string.Empty;
            string sClosedCode = string.Empty;
            int iCancelledParent, iClosedParent,iReserveCodeTable;
            XmlElement objCancelledCode = null, objClosedCode = null;
            //aaggarwal29 :end RMA-9856
            int iDedRcRowId = default(int);
            try
            {
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimID);
                iLOB = objClaim.LineOfBusCode;


                //rupal:start r8 Multicurrency

                objXmlDoc = new XmlDocument();

                objXmlElement = objXmlDoc.CreateElement("Reserves");
                objXmlDoc.AppendChild(objXmlElement);

                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                //mgaba2:mits 28450:start
                objColLobSettings = new ColLobSettings(m_sConnectionString,m_iClientId);

                int iUseClaimProgressNotes = objColLobSettings[iLOB].ClmProgressNotesFlag;
                objXmlHideEnhancedNotes = objXmlDoc.CreateElement("HideEnhancedNotes");
                if (iUseClaimProgressNotes == 0)
                {
                    objXmlHideEnhancedNotes.InnerText = "True";
                }
                else
                {
                    objXmlHideEnhancedNotes.InnerText = "False";
                }
                objXmlElement.AppendChild(objXmlHideEnhancedNotes);
                //mgaba2:mits 28450:end
                //rupal:get currency type for claim. if claim currency is defined then curency type for claim would be claim currency but 
                //if the claim currency is not defined then base currency would be the currency type for claim
                XmlElement objXmlElementCurrType = objXmlDoc.CreateElement("CurrencyTypeForClaim");
                string sShortCode = string.Empty;
                string sDesc = string.Empty;
                int iCurrType = p_iCurrType;
                bool IsBaseCurrTypeSelected = false;
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimID, m_sConnectionString);
                int iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                if (iClaimCurrCode > 0)
                {
                    objCache.GetCodeInfo(iClaimCurrCode, ref sShortCode, ref sDesc);
                    //rupal:multicurrency                                     
                     objXmlElement.SetAttribute("IsClaimCurrencySet", "1");                    
                    //rupal:end
                }
                else
                {
                    objCache.GetCodeInfo(iBaseCurrCode, ref sShortCode, ref sDesc);
                    objXmlElement.SetAttribute("IsClaimCurrencySet", "0");//rupal:multicurrency
                }
                //rupal:multicurrency
                //aaggarwal29 : RMA- 9856 start
                iCancelledParent = objCache.GetCodeId("CN", "RESERVE_STATUS_PARENT");
                iClosedParent = objCache.GetCodeId("C", "RESERVE_STATUS_PARENT");
                iReserveCodeTable = objCache.GetTableId("RESERVE_STATUS");

                object sCode1 = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES WHERE DELETED_FLAG =0 AND RELATED_CODE_ID= " + iCancelledParent + " AND TABLE_ID = " + iReserveCodeTable);
                objCancelledCode = objXmlDoc.CreateElement("CancelledShortCode");
                objCancelledCode.InnerText = Conversion.ConvertObjToStr(sCode1);
                objXmlElement.AppendChild(objCancelledCode);

                object sCode2 = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES WHERE DELETED_FLAG =0 AND RELATED_CODE_ID= " + iClosedParent + " AND TABLE_ID = " + iReserveCodeTable);
                objClosedCode = objXmlDoc.CreateElement("ClosedShortCode");
                objClosedCode.InnerText = Conversion.ConvertObjToStr(sCode2);
                objXmlElement.AppendChild(objClosedCode);

                //aaggarwal29 : RMA- 9856 end
                
                if (objClaim.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                {
                    objXmlElement.SetAttribute("IsClaimCurrencySet", "");
                }
                //rupal

                //RMA#6391--WWIG gap 45--START                
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);
                //RMA#6391--WWIG gap 45--END

                //Add by kuladeep MITS:34720 Start

                if (iLOB == 241)
                {
                    if (m_userLogin.IsAllowedEx(GC_RSRV_SID))
                    {
                        if (m_userLogin.IsAllowedEx(GC_RSRV_CREATEID))
                        {
                            objXmlElement.SetAttribute("IsAddPermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsAddPermission", "1");
                        }

                        if (m_userLogin.IsAllowedEx(GC_RSRV_PAYHIST_SID))
                        {
                            objXmlElement.SetAttribute("IsPaymentHistPermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsPaymentHistPermission", "1");
                        }

                        if(m_userLogin.IsAllowed(GC_MOVE_TO_FINANCIALS))
                        {
                              objXmlElement.SetAttribute("IsMoveToPermission", "0");
                        }
                        else
                        {
                              objXmlElement.SetAttribute("IsMoveToPermission", "1");
                        }
                        if (m_userLogin.IsAllowedEx(GC_RSRV_UPDATEID))
                        {
                            objXmlElement.SetAttribute("IsUpdatePermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsUpdatePermission", "1");
                        }

                        if (m_userLogin.IsAllowedEx(GC_RSRV_VIEWID))
                        {
                            objXmlElement.SetAttribute("IsViewPermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsViewPermission", "1");
                        }
                        //For Top Level Reserve Permission check
                        objXmlElement.SetAttribute("IsReservePermission", "1");

                    }
                    else
                    {
                        objXmlElement.SetAttribute("IsReservePermission", "0");
                    }

                    //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
                    if (objSysSetting.MultiCovgPerClm != 0)
                    {
                        if (m_userLogin.IsAllowedEx(GC_RSRV_FIRST_FINAL_PAY))
                        {
                            objXmlElement.SetAttribute("EnableFirstAndFinalPayment", "true");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("EnableFirstAndFinalPayment", "false");
                        }
                    }
                    else
                    {
                        objXmlElement.SetAttribute("EnableFirstAndFinalPayment", "true");
                    }
                    //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298

                }
                else if (iLOB == 243)
                {
                    if (m_userLogin.IsAllowedEx(WC_RSRV_SID))
                    {
                        if (m_userLogin.IsAllowedEx(WC_RSRV_CREATEID))
                        {
                            objXmlElement.SetAttribute("IsAddPermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsAddPermission", "1");
                        }

                        if (m_userLogin.IsAllowedEx(WC_RSRV_PAYHIST_SID))
                        {
                            objXmlElement.SetAttribute("IsPaymentHistPermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsPaymentHistPermission", "1");
                        }
                        
                        if(m_userLogin.IsAllowed(WC_MOVE_TO_FINANCIALS))
                        {
                              objXmlElement.SetAttribute("IsMoveToPermission", "0");
                        }
                        else
                        {
                              objXmlElement.SetAttribute("IsMoveToPermission", "1");
                        }

                        if (m_userLogin.IsAllowedEx(WC_RSRV_UPDATEID))
                        {
                            objXmlElement.SetAttribute("IsUpdatePermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsUpdatePermission", "1");
                        }

                        if (m_userLogin.IsAllowedEx(WC_RSRV_VIEWID))
                        {
                            objXmlElement.SetAttribute("IsViewPermission", "0");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("IsViewPermission", "1");
                        }
                        //For Top Level Reserve Permission check
                        objXmlElement.SetAttribute("IsReservePermission", "1");

                    }
                    else
                    {
                        objXmlElement.SetAttribute("IsReservePermission", "0");
                    }

                    //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298
                    if (objSysSetting.MultiCovgPerClm != 0)
                    {
                        if (m_userLogin.IsAllowedEx(WC_RSRV_FIRST_FINAL_PAY))
                        {
                            objXmlElement.SetAttribute("EnableFirstAndFinalPayment", "true");
                        }
                        else
                        {
                            objXmlElement.SetAttribute("EnableFirstAndFinalPayment", "false");
                        }
                    }
                    else
                    {
                        objXmlElement.SetAttribute("EnableFirstAndFinalPayment", "true");
                    }
                    //WWIG - Gap 45/RMA-6391/MITS#37569 – Requiring Reserves on All Payments - agupta298

                }

                //Add by kuladeep MITS:34720 End

                objXmlElementCurrType.InnerText = sShortCode + "_" + sDesc;
                objXmlElement.AppendChild(objXmlElementCurrType);                
                //MITS:34082 START
                XmlElement objXmlElementCurrTypeID = objXmlDoc.CreateElement("CurrencyTypeID");
                if (iClaimCurrCode > 0)
                {
                    objXmlElementCurrTypeID.InnerText = Convert.ToString(iClaimCurrCode);
                }
                else
                {
                    objXmlElementCurrTypeID.InnerText = Convert.ToString(iBaseCurrCode);
                }
                objXmlElement.AppendChild(objXmlElementCurrTypeID);
                //MITS:34082 END

                if (p_iCurrType <= 0)
                {
                    XmlElement objCurrencyListElement = null;
                    CreateElement(objXmlDoc.DocumentElement, "CurrencyTypeList", ref objCurrencyListElement);
                    
                    if (iClaimCurrCode > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iClaimCurrCode);
                        CreateAndSetElement(objCurrencyListElement, "Item", "Claim Currency: " + sClaimCurr.ToString(), iClaimCurrCode.ToString());
                        p_iCurrType = int.Parse(iClaimCurrCode.ToString());
                    }
                    else
                    {
                        IsBaseCurrTypeSelected = true;
                    }
                    if (iBaseCurrCode > 0)
                    {
                        object sBaseCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        CreateAndSetElement(objCurrencyListElement, "Item", "Base Currency: " + sBaseCurr.ToString(), iBaseCurrCode.ToString());
                        if (p_iCurrType == iBaseCurrCode)
                        {
                            IsBaseCurrTypeSelected = true;
                        }
                    }
                }
                else
                {
                    iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                    if (iBaseCurrCode > 0)
                    {
                        if (p_iCurrType == iBaseCurrCode)
                        {
                            //MITS:34082 MultiCurrency START
                            objCache.GetCodeInfo(iClaimCurrCode, ref sShortCode, ref sDesc);
                            objXmlElementCurrType.InnerText = sShortCode + "_" + sDesc;
                            objXmlElement.AppendChild(objXmlElementCurrType);
                            //MITS:34082 MultiCurrency END
                            IsBaseCurrTypeSelected = true;
                        }
                    }
                }
                //rupal:end
                

                if (iLOB == 241)
                {                    
                    sCaption = "General Claim " + GetFormTitle(objClaim);
                }
                
                else if (iLOB == 845)
                {                
                    sCaption = "Property Claim " + GetFormTitle(objClaim);
                }
                
                else if (iLOB == 242)
                {                
                    sCaption = "Vehicle Accident " + GetFormTitle(objClaim);
                }                
                else
                {
                    sCaption = "Workers' Compensation " + GetFormTitle(objClaim);
                }



                objXmlElement.SetAttribute("caption", sCaption);
                objXmlElement.SetAttribute("claimid", p_iClaimID.ToString());
                objXmlElement.SetAttribute("claimant", p_iClaimantEid.ToString());
                objXmlElement.SetAttribute("claimnumber", objClaim.ClaimNumber.ToString());
                objXmlElement.SetAttribute("paymentfrozenflag", objClaim.PaymntFrozenFlag.ToString() );
                //rma-857 Starts
                int iFlag = 0; //if collection not allowed at its lob level then set to -1.
                iFlag = objColLobSettings[iLOB].PreventCollectionAllReserve;
                objXmlElement.SetAttribute("preventcollonlob", iFlag.ToString());
                //rma-857 Ends

                //tanwar2 - mits 30910 - 01302013 - start
                bApplyDedToPayments = ((objClaim.Context.InternalSettings.ColLobSettings[iLOB]!= null && objClaim.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1) 
                    && objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface);
                objXmlElement.SetAttribute("ApplyDedToPayments", bApplyDedToPayments ? "-1" : "0");
                if(objClaim.Context.InternalSettings.ColLobSettings[iLOB] !=null && iLOB > 0)
                {
                    iDedReserveTypeCode = objClaim.Context.InternalSettings.ColLobSettings[iLOB].DedRecReserveType;
                }
                objXmlElement.SetAttribute("DedRecoveryReserveCode", iDedReserveTypeCode.ToString());
                //tanwar2 - mits 30910 - 01302013 - end

                // Populate Reason Codes
                if (objSysSetting.MultiCovgPerClm != 0 && objSysSetting.UsePolicyInterface && bApplyDedToPayments && m_userLogin.IsAllowedEx(ALLOW_MANUAL_DEDUCTIBLE))
                {
                    objXmlElement.SetAttribute("AllowManualdeductible", "true");
                }
                else
                {
                    objXmlElement.SetAttribute("AllowManualdeductible", "false");
                }
                objXmlElement.SetAttribute("NotDetDedVal", objCache.GetCodeId("ND", "DEDUCTIBLE_TYPE").ToString());
                objXmlElement.SetAttribute("FPDedVal", objCache.GetCodeId("FP", "DEDUCTIBLE_TYPE").ToString());
                objXmlElement.SetAttribute("NoneDedVal", objCache.GetCodeId("None", "DEDUCTIBLE_TYPE").ToString());
                objXmlElementReason = objXmlDoc.CreateElement("Reason");
                objXmlElement.AppendChild(objXmlElementReason);
                //Aman ML Changes made in the query
                sbSQL = new StringBuilder();
                sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'REASON' AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                //Start PARAG MITS 10269
                sbSQL.Append(" AND CODES.DELETED_FLAG!=-1 ");
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "REASON", this.LanguageCode);
                //Aman ML Changes made in the query
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    while (objReader.Read())
                    {                        
                        objXmlElementTmp = objXmlDoc.CreateElement("ReasonCode");
                        objXmlElementReason.AppendChild(objXmlElementTmp);

                        objXmlElementTmp.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());

                        sText = objReader.GetValue("SHORT_CODE").ToString();
                        sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();

                        objXmlElementTmp.InnerText = sText;
                        
                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);

                // Populate Status Codes
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);

                objXmlElementStatus = objXmlDoc.CreateElement("Status");
                objXmlElement.AppendChild(objXmlElementStatus);
                
             

                sbSQL = new StringBuilder();
                //Aman ML Changes--start
                sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                sbSQL.Append(" AND CODES.DELETED_FLAG = 0 AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                //Ankit Start : Financial Enhancement - Status Code Changes

                //rsharma220 MITS 32331 Start
                if (objSysSetting.MultiCovgPerClm != 0)
                {
                    sbSQL.Append(" AND CODES.RELATED_CODE_ID NOT IN ( SELECT codes.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_STATUS_PARENT' ");
                    sbSQL.Append(" AND CODES.SHORT_CODE IN ('A','H','Re','F') ) "); //rupal:r8 first & final payment, first & final reserve status is created internally only, its should not be displayed in status dropdown
                }
                else
                {
                    sbSQL.Append(" AND CODES.RELATED_CODE_ID NOT IN ( SELECT codes.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_STATUS_PARENT'");
                    sbSQL.Append(" AND CODES.SHORT_CODE IN ('A','H','Re','F','Cn') )");
                }
                //rsharma220 MITS 32331 End
                //Ankit End
                                      
                sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_STATUS' ");
                sbSQL.Append(" AND CODES.SHORT_CODE NOT IN ('A','R','H')");  //mbahl3 Reserve Approval Enhancement Mits 32471 // ASHARMA326 commented for JIRA 10569. 
                sbSQL.Append(" ORDER BY CODES.SHORT_CODE");

                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_STATUS", this.LanguageCode);
                //Aman ML Changes--end
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        //start:rupal, first & final payment
                        //we don't want "First & Final" reserve status to be displayed in dropdown as this status code is used internally only
                        //sText = objReader.GetValue("SHORT_CODE").ToString();
                        sText = objReader.GetValue(0).ToString();
                        //if (sText == "F")
                        //    continue;
                        //sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();                                                
                        sText = sText + " - " + objReader.GetValue(1).ToString();                                                
                        objXmlElementTmp = objXmlDoc.CreateElement("StatusCode");
                        objXmlElementStatus.AppendChild(objXmlElementTmp);

                        //objXmlElementTmp.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());
                        objXmlElementTmp.SetAttribute("value", objReader.GetValue(2).ToString());
                        //RUPAL:first & final payment
                        objXmlElementTmp.SetAttribute("shortcode", objReader.GetValue(0).ToString());

                        objXmlElementTmp.InnerText = sText;
                        //end:rupal, first & final payment


                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);
                
                
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                //rupal:start, r8 multicurrency
                //arrReserves = GetReservesDetails(p_iClaimID, p_iClaimantEid, IsBaseCurrTypeSelected);
                arrReserves = GetReservesDetails(p_iClaimID, p_iClaimantEid, IsBaseCurrTypeSelected);
                //rupal:end

                sbSQL = new StringBuilder();
                
                //sbSQL.Append(" SELECT RES_DO_NOT_RECALC FROM SYS_PARMS");

                //objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                //if (objReader != null)
                //{
                //    if (objReader.Read())
                //    {                        
                //        bDoNotRecalcReserves = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                //            objReader.GetValue("RES_DO_NOT_RECALC").ToString()));
                //    }
                //    objReader.Close();

                //}
                //sbSQL.Remove(0, sbSQL.Length);

                if (arrReserves != null)
                {
                    //asharma326 JIRA 857 Starts
                    List<string> lstPreventCollOnrsv = new List<string>();
                    if (objColLobSettings[iLOB].PreventCollectionPerReserve == -1)
                    {
                        DbReader dbread = DbFactory.GetDbReader(m_sConnectionString, "SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE = " + iLOB);
                        while (dbread.Read())
                        {
                            lstPreventCollOnrsv.Add(Convert.ToString(dbread.GetValue("RES_TYPE_CODE")));
                        }
                    }
                    //asharma326 JIRA 857 end
                    for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                    {
                        //if (!(arrReserves[iCnt].dBalance == 0 &&
                        //    arrReserves[iCnt].dTotalIncurred == 0 && arrReserves[iCnt].dTotalCollected == 0 &&
                        //    arrReserves[iCnt].dTotalPaid == 0))
                        //    bProcess = true;

                        //if (bProcess)
                        //{
                            objXmlReserveElement = objXmlDoc.CreateElement("Reserve");
                            objXmlElement.AppendChild(objXmlReserveElement);

                            objXmlReserveElement.SetAttribute("reservetypecode", arrReserves[iCnt].iReserveTypeCode.ToString());
                            objXmlReserveElement.SetAttribute("reservename", arrReserves[iCnt].sReserveDesc);
                            objXmlReserveElement.SetAttribute("reservetypecode", arrReserves[iCnt].iReserveTypeCode.ToString());
                            objXmlReserveElement.SetAttribute("reservestatus", arrReserves[iCnt].sStatusCode);

                            //Prashbiharis Mits: 30949
                            objXmlReserveElement.SetAttribute("reason", arrReserves[iCnt].sReason.ToString());
                            objXmlReserveElement.SetAttribute("reservestatusdetails", arrReserves[iCnt].sStatusCode.ToString() + " - " + arrReserves[iCnt].sStatusCodeDesc.ToString());
                            //Prashbiharis Mits: 30949
                            //rupal:start, r8 multicurrency
                            objXmlReserveElement.SetAttribute("reserveamount", CommonFunctions.ConvertCurrency(p_iClaimID, arrReserves[iCnt].dReserveAmount, eNavType, m_sConnectionString, m_iClientId));
                            objXmlReserveElement.SetAttribute("balance", CommonFunctions.ConvertCurrency(p_iClaimID, arrReserves[iCnt].dBalance, eNavType, m_sConnectionString, m_iClientId));
                            objXmlReserveElement.SetAttribute("incurred", CommonFunctions.ConvertCurrency(p_iClaimID, arrReserves[iCnt].dTotalIncurred, eNavType, m_sConnectionString, m_iClientId));
                            objXmlReserveElement.SetAttribute("paid", CommonFunctions.ConvertCurrency(p_iClaimID, arrReserves[iCnt].dTotalPaid, eNavType, m_sConnectionString, m_iClientId));
                            objXmlReserveElement.SetAttribute("collected", CommonFunctions.ConvertCurrency(p_iClaimID, arrReserves[iCnt].dTotalCollected, eNavType, m_sConnectionString, m_iClientId));
                            //rma-857 Starts

                            if (objColLobSettings[iLOB].PreventCollectionPerReserve == -1 &&
                                lstPreventCollOnrsv.Contains(Convert.ToString(arrReserves[iCnt].iReserveTypeCode)))
                                 objXmlReserveElement.SetAttribute("preventcollonres", "-1");
                            else
                                objXmlReserveElement.SetAttribute("preventcollonres", "0");
                        //jira 857 ends
                            //rupal:end
                            //MITS 35453 Prashant starts
                            objXmlReserveElement.SetAttribute("clmcurrbalamount", CommonFunctions.ConvertByCurrencyCode(iClaimCurrCode, arrReserves[iCnt].dClmCurrBalAmount, m_sConnectionString, m_iClientId));
                            objXmlReserveElement.SetAttribute("clmcurrresamount", CommonFunctions.ConvertByCurrencyCode(iClaimCurrCode, arrReserves[iCnt].dClmCurrResAmount, m_sConnectionString, m_iClientId));
                            //MITS 35453 Prashant ends
                            objXmlReserveElement.SetAttribute("polcvgrowid", arrReserves[iCnt].iPolCvg_Row_ID.ToString() );
                            objXmlReserveElement.SetAttribute("reserveid", arrReserves[iCnt].iRC_Row_ID.ToString());
                            objXmlReserveElement.SetAttribute("lssresexpflag", arrReserves[iCnt].iLSS_RES_EXP_FLAG.ToString());//Added by sharishkumar for Mits 35472
                            objXmlReserveElement.SetAttribute("policyid", arrReserves[iCnt].iPolicyID.ToString());

                            objPolicy = (Policy)(m_objDataModelFactory.GetDataModelObject("Policy", false));
                            objPolicy.MoveTo(arrReserves[iCnt].iPolicyID);

                            objXmlReserveElement.SetAttribute("policyname", objPolicy.PolicyName);

                            iCvgTypeCode = arrReserves[iCnt].iCoveTypeCode;
                            objXmlReserveElement.SetAttribute("coveragetypecode", iCvgTypeCode.ToString());

                            if (objCache == null)  // only allocate once and then use for subsequent ones
                                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                            //rupal:start,policy system interface
                            if (objPolicy.PolicySystemId > 0)
                            {
                                objXmlReserveElement.SetAttribute("coveragename", GetCoverageText(arrReserves[iCnt].iPolicyUnitRowId,arrReserves[iCnt].iPolCvg_Row_ID,arrReserves[iCnt].iCoveTypeCode));
                            }
                            else
                            {   
                                //rsharma220 MITS 33736
                                objXmlReserveElement.SetAttribute("coveragename", objCache.GetShortCode(iCvgTypeCode) + " " + objCache.GetCodeDesc(iCvgTypeCode, this.LanguageCode));
                            }

                            //rupal
                            objXmlReserveElement.SetAttribute("reservetype", objCache.GetRelatedShortCode(arrReserves[iCnt].iReserveTypeCode));//Added by sharishkumar for Mits 35472
                            objXmlReserveElement.SetAttribute("reservestatusdesc", arrReserves[iCnt].sStatusCodeDesc);

                            objXmlReserveElement.SetAttribute("policyunitrowid", arrReserves[iCnt].iPolicyUnitRowId.ToString());
                            if(string.Compare(arrReserves[iCnt].sUnitType,"V",true) == 0)
                            {
                                
                                if (objSysSetting.MultiCovgPerClm == -1)
                                {
                                    objXmlReserveElement.SetAttribute("unit", objCache.GetUARDetailVIN(arrReserves[iCnt].iUnitID, true));
                                }
                                
                                else
                                {
                                    objXmlReserveElement.SetAttribute("unit", objCache.GetUARDetailVIN(arrReserves[iCnt].iUnitID, false));
                                }
                            }
                            else if (string.Compare(arrReserves[iCnt].sUnitType, "P", true) == 0)
                            {
                                objXmlReserveElement.SetAttribute("unit", objCache.GetUARDetailPIN(arrReserves[iCnt].iUnitID));
                            }
                            //rupal:start, policy interface changes
                            if (string.Compare(arrReserves[iCnt].sUnitType, "S", true) == 0)
                            {
                                objXmlReserveElement.SetAttribute("unit", CommonFunctions.GetSiteUnitDetails(m_sConnectionString, arrReserves[iCnt].iUnitID));
                            }
                            if (string.Compare(arrReserves[iCnt].sUnitType, "SU", true) == 0)
                            {
                                objXmlReserveElement.SetAttribute("unit", CommonFunctions.GetOtherUnitDetails(m_sConnectionString, arrReserves[iCnt].iUnitID, sDBType));
                            }
							//rsharma220 MITS 33736
                            objXmlReserveElement.SetAttribute("losstypename",objCache.GetShortCode(arrReserves[iCnt].iLossTypeCode) + " " + objCache.GetCodeDesc(arrReserves[iCnt].iLossTypeCode, this.LanguageCode));
							objXmlReserveElement.SetAttribute("losstypecode", arrReserves[iCnt].iLossTypeCode.ToString());
                            objXmlReserveElement.SetAttribute("polcvglossrowid", arrReserves[iCnt].iPolCvgLossId.ToString());
                            objXmlReserveElement.SetAttribute("reservecatcode", arrReserves[iCnt].iReserveCat.ToString());
                            objXmlReserveElement.SetAttribute("disabilitycat", arrReserves[iCnt].iDisabilityCat.ToString());
                            objXmlReserveElement.SetAttribute("reservecatdesc", arrReserves[iCnt].sReserveCatDesc);
                            objXmlReserveElement.SetAttribute("disabilitycatdesc", arrReserves[iCnt].sDisabilityCatDesc);
                            objXmlReserveElement.SetAttribute("covgseqnum", arrReserves[iCnt].sCovgSeqNum.ToString());
                            objXmlReserveElement.SetAttribute("adjusterdetails", arrReserves[iCnt].sAdjusterDetails.ToString());    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                            objXmlReserveElement.SetAttribute("RsvStatusParent", arrReserves[iCnt].sRsvStatusParent.ToString());
                            objXmlReserveElement.SetAttribute("transseqnum", arrReserves[iCnt].sTransSeqNum.ToString());
                            objXmlReserveElement.SetAttribute("CoverageKey", arrReserves[iCnt].sCoverageKey.ToString());        //Ankit Start : Worked on MITS - 34297
                            objXmlReserveElement.SetAttribute("DedTypeCode", arrReserves[iCnt].DeductibleTypeCode);
                            iDedRcRowId = GetDeductibleRcRowId(arrReserves[iCnt].iRC_Row_ID, iLOB, arrReserves[iCnt].iReserveTypeCode);
                            objXmlReserveElement.SetAttribute("DedRcRowId", iDedRcRowId.ToString());
                            bProcess = false;
                            
                        //}//if(bProcess)
                    }//for loop
                }
                
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesBOB.GenericError", m_iClientId), objException);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                objXmlElement = null;
                objXmlReserveElement = null;
                objCancelledCode = null;
                objClosedCode = null;
                sbSQL = null;
                arrReserves = null;
                if (objPolicy != null)
                    objPolicy = null;
                if(objCache!=null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objColLobSettings != null)
                {
                    objColLobSettings = null;
                }
            }

            //Return the result to calling function
            return objXmlDoc;
        }

        public int GetDeductibleRcRowId(int p_iPaymentRcRowId, int i_pLOB, int i_pReserveTypeCode)
        {
            int iDeductibleRcRowId = default(int);
            object objResult = null;
            int iDeductibleReserveTypeCode = default(int);
            StringBuilder sbSQL;
            try
            {
                //TODO:later
                // SELECT DED_RES_TYPE_CODE FROM LOB_MAPPINT_TABLE WHERE LOB= i_pLOB AND PAYMENT_RESERVE_TYPE_CODE=i_pReserveTypeCode
                //iDeductibleRcRowId = DED_RES_TYPE_CODE

                if (this.m_objDataModelFactory.Context.InternalSettings.ColLobSettings[i_pLOB] != null)
                    iDeductibleReserveTypeCode = this.m_objDataModelFactory.Context.InternalSettings.ColLobSettings[i_pLOB].DedRecReserveType;

                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT DeductibleReserve.RC_ROW_ID FROM RESERVE_CURRENT DeductibleReserve, RESERVE_CURRENT PaymentReserve");
                sbSQL.Append(" WHERE ");
                sbSQL.Append(String.Format(" PaymentReserve.RC_ROW_ID = {0}", p_iPaymentRcRowId));
                sbSQL.Append(" AND PaymentReserve.CLAIMANT_EID = DeductibleReserve.CLAIMANT_EID");
                sbSQL.Append(" AND PaymentReserve.POLCVG_LOSS_ROW_ID = DeductibleReserve.POLCVG_LOSS_ROW_ID");
                sbSQL.Append(" AND PaymentReserve.POLCVG_ROW_ID = DeductibleReserve.POLCVG_ROW_ID");
                sbSQL.Append(" AND PaymentReserve.CLAIM_ID = DeductibleReserve.CLAIM_ID");
                sbSQL.Append(" AND PaymentReserve.UNIT_ID = DeductibleReserve.UNIT_ID");
                sbSQL.Append(String.Format(" AND DeductibleReserve.RESERVE_TYPE_CODE = {0}", iDeductibleReserveTypeCode));

                objResult = DbFactory.ExecuteScalar(m_sConnectionString, sbSQL.ToString());
                if (objResult != null)
                    iDeductibleRcRowId = Conversion.ConvertObjToInt(objResult, m_iClientId);
                return iDeductibleRcRowId;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesBOB.GenericError", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                objResult = null;
            }
        }

        //rupal:start,policy system interface
        private string GetCoverageText(int iPolicyUnitRowId,int iPolicyCvgRowId,int iCoverageTypeCode)
        {
            string sCoverageText = string.Empty;
            StringBuilder sSQL =null;
            //smahajan22 - mits 35587 start
            System.Collections.Generic.Dictionary<string, string> dictParams = null;
            try
            {
                sSQL = new StringBuilder();
                dictParams = new System.Collections.Generic.Dictionary<string, string>();
                
                //sSQL.Append(" SELECT DISTINCT");
                //sSQL.Append(" CASE(CNT)");
                //if (DbFactory.IsOracleDatabase(m_sConnectionString))
                //{
                //    sSQL.Append(" WHEN 1 THEN A.SHORT_CODE || ' ' || A.CODE_DESC ");
                //}
                //else
                //{
                //    sSQL.Append(" WHEN 1 THEN A.SHORT_CODE + ' ' + A.CODE_DESC ");
                //}
                //sSQL.Append(" ELSE");
                //if (DbFactory.IsOracleDatabase(m_sConnectionString))
                //{
                //    sSQL.Append(" CASE(NVL(COVERAGE_TEXT,''))");
                //}
                //else
                //{
                //    sSQL.Append(" CASE(ISNULL(COVERAGE_TEXT,''))");
                //}
                //sSQL.Append(" WHEN '' THEN A.CODE_DESC ");
                //sSQL.Append(" ELSE COVERAGE_TEXT  ");
                //sSQL.Append(" END  ");
                //sSQL.Append(" END COVERAGE_TEXT");
                //sSQL.Append(" FROM ");
                //sSQL.Append(" ( ");
                //sSQL.Append(" SELECT CODE_DESC, COVERAGE_TEXT, C.COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO,B.CNT, SHORT_CODE, TRANS_SEQ_NO FROM ");
                //sSQL.Append(" ( ");
                //sSQL.Append(" SELECT COVERAGE_TYPE_CODE,COUNT(*) CNT FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE=" + iCoverageTypeCode + " GROUP BY COVERAGE_TYPE_CODE");
                //sSQL.Append(" )B,");
                //sSQL.Append(" (");
                //sSQL.Append(" SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO, TRANS_SEQ_NO");
                //sSQL.Append(" FROM POLICY_X_CVG_TYPE,CODES_TEXT");
                //sSQL.Append(" WHERE");
                //sSQL.Append(" POLICY_UNIT_ROW_ID =" + iPolicyUnitRowId + " AND POLCVG_ROW_ID=" + iPolicyCvgRowId);
                //sSQL.Append(" AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE	");
                //sSQL.Append(" ) C WHERE B.COVERAGE_TYPE_CODE = C.COVERAGE_TYPE_CODE");
                //sSQL.Append(" )A ");
                sSQL.AppendFormat("SELECT COVERAGE_TEXT FROM POLICY_X_CVG_TYPE WHERE  POLCVG_ROW_ID = {0}", "~POLCVGROWID~");
                dictParams.Add("POLCVGROWID", iPolicyCvgRowId.ToString());
                
                using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSQL.ToString(), dictParams))
                {
                    if (objReader.Read())
                    {
                        sCoverageText = objReader.GetValue(0).ToString();
                 //smahajan22 - mits 35587 end
                    }
                }
                return sCoverageText;

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesBOB.GenericError", m_iClientId), objException);
            }
            finally
            {
                sSQL = null;
            }
        }

        /// <summary>
        /// Get Claim Reserve Information when Multiple Coverages per Claim is ON
        /// </summary>
        public XmlDocument GetReservesBOB(int p_iClaimID)
        {
            XmlElement objXmlElement = null;
            //XmlElement objXmlElementReason = null;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlReserveElement = null;
            //XmlElement objXmlElementTmp = null;
            //XmlElement objXmlElementStatus = null;

            StringBuilder sbSQL = null;
            DbReader objReader = null;
            structReserves[] arrReserves = null;
            Policy objPolicy = null;
            Entity objEntity = null;
            bool bProcess = false;
            bool bDoNotRecalcReserves = false;
            int iCnt = 0;

            int iCvgTypeCode = 0;
            LocalCache objCache = null;                     
            int iLOB = 0; //JIRA-857
            Claim objClaim = null; //JIRA-857
            ColLobSettings objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId); //JIRA-857
            string sCaption = String.Empty;
            string sText = String.Empty;
            SysSettings objSettings = null;

            try
            {
                //JIRA-857 Starts
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimID);
                iLOB = objClaim.LineOfBusCode;
                //JIRA-857 Ends

                objXmlDoc = new XmlDocument();

                objXmlElement = objXmlDoc.CreateElement("Reserves");
                objXmlDoc.AppendChild(objXmlElement);

                objSettings = new SysSettings(m_sConnectionString, m_iClientId);
                //rupal:start r8 Multicurrency
                int iCurrType = 0;
                bool IsBaseCurrTypeSelected = false;
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimID, m_sConnectionString);
                int iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                if (iCurrType <= 0)
                {
                    XmlElement objCurrencyListElement = null;
                    CreateElement(objXmlDoc.DocumentElement, "CurrencyTypeList", ref objCurrencyListElement);
                    
                    if (iClaimCurrCode > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iClaimCurrCode);
                        CreateAndSetElement(objCurrencyListElement, "Item", "Claim Currency: " + sClaimCurr.ToString(), iClaimCurrCode.ToString());
                        iCurrType = int.Parse(iClaimCurrCode.ToString());
                    }
                    else
                    {
                        IsBaseCurrTypeSelected = true;
                    }
                    if (iBaseCurrCode > 0)
                    {
                        object sBaseCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        CreateAndSetElement(objCurrencyListElement, "Item", "Base Currency: " + sBaseCurr.ToString(), iBaseCurrCode.ToString());
                    }
                }
                else
                {                    
                    if (iBaseCurrCode > 0)
                    {
                        if (iCurrType == iBaseCurrCode)
                        {
                            IsBaseCurrTypeSelected = true;
                        }
                    }
                }

                //rupal:get currency type for claim. if claim currency is defined then curency type for claim would be claim currency but 
                //if the claim currency is not defined then base currency would be the currency type for claim
                XmlElement objXmlElementCurrType = objXmlDoc.CreateElement("CurrencyTypeForClaim");
                string sShortCode = string.Empty;
                string sDesc = string.Empty;
                
                
                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
                if (iClaimCurrCode > 0)
                {
                    objCache.GetCodeInfo(iClaimCurrCode, ref sShortCode, ref sDesc);
                }
                else
                {
                    objCache.GetCodeInfo(iBaseCurrCode, ref sShortCode, ref sDesc);
                }
                objXmlElementCurrType.InnerText = sShortCode + "_" + sDesc;
                objXmlElement.AppendChild(objXmlElementCurrType);

                //rupal:end
                //rma-857 Starts
                int iFlag = 0; //if collection not allowed at its lob level then set to -1.
                iFlag = objColLobSettings[iLOB].PreventCollectionAllReserve;
                objXmlElement.SetAttribute("preventcollonlob", iFlag.ToString());
                //rma-857 Ends
                //objXmlDoc = new XmlDocument();

                //objXmlElement = objXmlDoc.CreateElement("Reserves");
                //objXmlDoc.AppendChild(objXmlElement);

                //if (iLOB == 241)
                //{
                //    sCaption = "General Claim " + GetFormTitle(objClaim);
                //}

                //else if (iLOB == 845)
                //{
                //    sCaption = "Property Claim " + GetFormTitle(objClaim);
                //}

                //else if (iLOB == 242)
                //{
                //    sCaption = "Vehicle Accident " + GetFormTitle(objClaim);
                //}
                //else
                //{
                //    sCaption = "Workers' Compensation " + GetFormTitle(objClaim);
                //}



                //objXmlElement.SetAttribute("caption", sCaption);
                //objXmlElement.SetAttribute("claimid", p_iClaimID.ToString());
                //objXmlElement.SetAttribute("claimant", p_iClaimantEid.ToString());
                //objXmlElement.SetAttribute("claimnumber", objClaim.ClaimNumber.ToString());
                //objXmlElement.SetAttribute("paymentfrozenflag", objClaim.PaymntFrozenFlag.ToString());


                // Populate Reason Codes


                //objXmlElementReason = objXmlDoc.CreateElement("Reason");
                //objXmlElement.AppendChild(objXmlElementReason);

                //sbSQL = new StringBuilder();
                //sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                //sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                //sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                //sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'REASON' ");
                ////Start PARAG MITS 10269
                //sbSQL.Append(" AND CODES.DELETED_FLAG!=-1 ");


                //objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                //if (objReader != null)
                //{
                //    while (objReader.Read())
                //    {
                //        objXmlElementTmp = objXmlDoc.CreateElement("ReasonCode");
                //        objXmlElementReason.AppendChild(objXmlElementTmp);

                //        objXmlElementTmp.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());

                //        sText = objReader.GetValue("SHORT_CODE").ToString();
                //        sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();

                //        objXmlElementTmp.InnerText = sText;

                //    }
                //    objReader.Close();

                //}
                //sbSQL.Remove(0, sbSQL.Length);

                // Populate Status Codes

                //objXmlElementStatus = objXmlDoc.CreateElement("Status");
                //objXmlElement.AppendChild(objXmlElementStatus);

                //sbSQL = new StringBuilder();
                //sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                //sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                //sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                //sbSQL.Append(" AND CODES.DELETED_FLAG = 0 ");
                //sbSQL.Append(" AND CODES.SHORT_CODE NOT IN ('A','H','R')"); //MGaba2: R8: Supervisory Approval
                //sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_STATUS' ");
                //sbSQL.Append(" ORDER BY CODES.SHORT_CODE ");


                //objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                //if (objReader != null)
                //{
                //    while (objReader.Read())
                //    {
                //        //start:rupal, first & final payment
                //        //we don't want "First & Final" reserve status to be displayed in dropdown as this status code is used internally only
                //        sText = objReader.GetValue("SHORT_CODE").ToString();
                //        //if (sText == "F")
                //        //    continue;
                //        sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();
                //        objXmlElementTmp = objXmlDoc.CreateElement("StatusCode");
                //        objXmlElementStatus.AppendChild(objXmlElementTmp);

                //        objXmlElementTmp.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());
                //        //RUPAL:first & final payment
                //        objXmlElementTmp.SetAttribute("shortcode", objReader.GetValue("SHORT_CODE").ToString());

                //        objXmlElementTmp.InnerText = sText;
                //        //end:rupal, first & final payment
                //    }
                //    objReader.Close();

                //}
                //sbSQL.Remove(0, sbSQL.Length);

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                arrReserves = GetReservesDetails(p_iClaimID, 0, IsBaseCurrTypeSelected);

                sbSQL = new StringBuilder();

                sbSQL.Append(" SELECT RES_DO_NOT_RECALC FROM SYS_PARMS");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bDoNotRecalcReserves = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_DO_NOT_RECALC").ToString()), m_iClientId);
                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);

                if (arrReserves != null)
                {
                    for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                    {
                        //if (!(bDoNotRecalcReserves && arrReserves[iCnt].dBalance == 0 &&
                        //    arrReserves[iCnt].dTotalIncurred == 0 && arrReserves[iCnt].dTotalCollected == 0 &&
                        //    arrReserves[iCnt].dTotalPaid == 0))
                        //    bProcess = true;

                        //if (bProcess)
                        //{
                            objXmlReserveElement = objXmlDoc.CreateElement("Reserve");
                            objXmlElement.AppendChild(objXmlReserveElement);

                            objXmlReserveElement.SetAttribute("reservetypecode", arrReserves[iCnt].iReserveTypeCode.ToString());
                            objXmlReserveElement.SetAttribute("reservename", arrReserves[iCnt].sReserveDesc);
                            objXmlReserveElement.SetAttribute("reservetypecode", arrReserves[iCnt].iReserveTypeCode.ToString());
                            objXmlReserveElement.SetAttribute("reservestatus", arrReserves[iCnt].sStatusCode);
                            objXmlReserveElement.SetAttribute("reserveamount", String.Format("{0:c}", arrReserves[iCnt].dReserveAmount));
                            objXmlReserveElement.SetAttribute("balance", String.Format("{0:c}", arrReserves[iCnt].dBalance));
                            objXmlReserveElement.SetAttribute("incurred", String.Format("{0:c}", arrReserves[iCnt].dTotalIncurred));
                            objXmlReserveElement.SetAttribute("paid", String.Format("{0:c}", arrReserves[iCnt].dTotalPaid));
                            objXmlReserveElement.SetAttribute("collected", String.Format("{0:c}", arrReserves[iCnt].dTotalCollected));
                            //rma-857 Starts
                            iFlag = 0; //if collection not allowed at per reserve level then set to -1.
                            objXmlReserveElement.SetAttribute("preventcollonres", iFlag.ToString());
                            iFlag = objColLobSettings[iLOB].PreventCollectionPerReserve;

                            if (iFlag == -1)
                            {
                                DataSet objDs = DbFactory.GetDataSet(m_sConnectionString, "SELECT * FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE=" + iLOB + " AND RES_TYPE_CODE=" + arrReserves[iCnt].iReserveTypeCode, m_iClientId);
                                if (objDs.Tables[0] != null)
                                {
                                    foreach (DataRow dr in objDs.Tables[0].Rows)
                                    {
                                        if (Conversion.ConvertObjToInt(dr["RES_TYPE_CODE"], m_iClientId) > 0)
                                        {
                                            objXmlReserveElement.SetAttribute("preventcollonres", iFlag.ToString());
                                        }
                                    }
                                }
                            }

                            //rma-857 Ends
                            objXmlReserveElement.SetAttribute("polcvgrowid", arrReserves[iCnt].iPolCvg_Row_ID.ToString());
                            objXmlReserveElement.SetAttribute("reserveid", arrReserves[iCnt].iRC_Row_ID.ToString());
                            objXmlReserveElement.SetAttribute("policyid", arrReserves[iCnt].iPolicyID.ToString());

                            objPolicy = (Policy)(m_objDataModelFactory.GetDataModelObject("Policy", false));
                            objPolicy.MoveTo(arrReserves[iCnt].iPolicyID);

                            objXmlReserveElement.SetAttribute("policyname", objPolicy.PolicyName);

                            objEntity = (Entity)(m_objDataModelFactory.GetDataModelObject("Entity", false));
                            objEntity.MoveTo(arrReserves[iCnt].iClaimantEid);
                            string sClaimantName = string.IsNullOrEmpty(objEntity.FirstName) ? objEntity.LastName : objEntity.LastName + "," + objEntity.FirstName;
                            objXmlReserveElement.SetAttribute("claimantname", sClaimantName);

                            //iCvgTypeCode = objPolicy.PolicyXCvgTypeList[arrReserves[iCnt].iPolCvg_Row_ID].CoverageTypeCode;
                            iCvgTypeCode = arrReserves[iCnt].iCoveTypeCode;
                            objXmlReserveElement.SetAttribute("coveragetypecode", iCvgTypeCode.ToString());

                            if (objCache == null)  // only allocate once and then use for subsequent ones
                                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                            //smahajan22 mits 35587 start
                            if (objPolicy.PolicySystemId > 0)
                            {
                                objXmlReserveElement.SetAttribute("coveragename", arrReserves[iCnt].sCoveTypeText.ToString());
                            }
                            else
                            {
                                objXmlReserveElement.SetAttribute("coveragename", objCache.GetCodeDesc(iCvgTypeCode));
                            }
                            // smahajan22 mits 35587 end
                            //rupal
                            objXmlReserveElement.SetAttribute("reservestatusdesc", arrReserves[iCnt].sStatusCodeDesc);

                            objXmlReserveElement.SetAttribute("policyunitrowid", arrReserves[iCnt].iPolicyUnitRowId.ToString());
							
							//skhare7 Policy interfcae
                            objXmlReserveElement.SetAttribute("policycvglossrowid", arrReserves[iCnt].iPolCvgLossId.ToString());
                            objXmlReserveElement.SetAttribute("policylosstypecode", arrReserves[iCnt].iLossTypeCode.ToString());
                            objXmlReserveElement.SetAttribute("reservecatcode", arrReserves[iCnt].iReserveCat.ToString());
                            objXmlReserveElement.SetAttribute("disabilitycat", arrReserves[iCnt].iDisabilityCat.ToString());
                            objXmlReserveElement.SetAttribute("reservecatdesc", arrReserves[iCnt].sReserveCatDesc);
                            objXmlReserveElement.SetAttribute("disabilitycatdesc", arrReserves[iCnt].sDisabilityCatDesc);
							
                            if (string.Compare(arrReserves[iCnt].sUnitType, "V", true) == 0)
                            {
                                if(objSettings.MultiCovgPerClm == -1)
                                    objXmlReserveElement.SetAttribute("unit", objCache.GetUARDetailVIN(arrReserves[iCnt].iUnitID, true));
                                else
                                    objXmlReserveElement.SetAttribute("unit", objCache.GetUARDetailVIN(arrReserves[iCnt].iUnitID, false));
                            }
                            else
                            {
                                objXmlReserveElement.SetAttribute("unit", objCache.GetUARDetailPIN(arrReserves[iCnt].iUnitID));
                            }
                            bProcess = false;

                        //}//if(bProcess)
                    }//for loop
                }

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesBOB.GenericError", m_iClientId), objException);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                objXmlElement = null;
                objXmlReserveElement = null;
                sbSQL = null;
                arrReserves = null;
                if (objPolicy != null)
                    objPolicy = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objSettings = null;

            }

            //Return the result to calling function
            return objXmlDoc;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_iClaimID"></param>
        /// <param name="p_iClaimantEid"></param>
        /// <returns></returns>
        /// //rupal: r8 multicurrency
        public structReserves[] GetReservesDetails(int p_iClaimID, int p_iClaimantEid, bool IsBaseCurrTypeSelected) 
        {
            
            //String Object
            StringBuilder sbSQL = null;

            //Integer Variables
            int iCStat = 0;
            int iStatus = 0;
            int iCodeID = 0;            
            int iTemp = 0;// Variable used in For Loops

            //Double Variables
            double dRAmount = 0;
            double dRPaid = 0;
            double dRCollect = 0;
            double dRBalance = 0;
            double dRIncurred = 0;


            bool bCollInRsvBal = false, bPerRsvColInRsvBal = false;//asharma326 JIRA 870
            bool bCollInIncurredBal = false, bPerRsvColInIncrBal = false;//asharma326 JIRA 870
            bool bDoNotRecalcReserves = false;      
            
            
            DataSet objDataSet = null;
            DataRow objDataRow = null;
            DbReader objReader = null;          
            
            bool bResByClaimType = false;                     
            
            bool bSuccess = false;
            bool bBalTOZero = false;
            bool bNegBalToZero = false;
            bool bSetToZero = false;
            int iLOB = 0;
            
            structReserves[] arrReserves = null;
            LocalCache objCache = null;                     

            Claim objClaim = null;
            SysSettings objSysSettings = null;   //35453 
            // start - Added by Nikhil.Exclude deductible recovery reserve listing
            int iDedRecoveryResTypeCode = 0;
            // end - Added by Nikhil.Exclude deductible recovery reserve listing
            try
            {

                sbSQL = new StringBuilder();

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimID);
                iLOB = objClaim.LineOfBusCode;

                bool bAddDedCode = false;
                object oData = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT COUNT(*) FROM CLAIM_X_POL_DED WHERE CLAIM_ID =" + p_iClaimID);
                if (Conversion.ConvertObjToInt(oData,m_iClientId) != 0)
                {
                    bAddDedCode = true;
                }
                //start - Added by Nikhil.Exclude deductible recovery reserve listing
                if (iLOB > 0 && objClaim.Context.InternalSettings.ColLobSettings[iLOB] != null)
                {
                    iDedRecoveryResTypeCode = objClaim.Context.InternalSettings.ColLobSettings[iLOB].DedRecReserveType;
                }
                // end - Added by Nikhil.Exclude deductible recovery reserve listing
                sbSQL.Append("SELECT COLL_IN_RSV_BAL,BAL_TO_ZERO , NEG_BAL_TO_ZERO ,SET_TO_ZERO, COLL_IN_INCUR_BAL,PER_RSV_COLL_IN_RSV_BAL,PER_RSV_COLL_IN_INCR_BAL, RES_BY_CLM_TYPE "); //Parijat: MITS 7977
                sbSQL.Append("FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOB);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {

                        bCollInRsvBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_RSV_BAL").ToString()), m_iClientId);
                        //Parijat: Mits 7977
                        bBalTOZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("BAL_TO_ZERO").ToString()), m_iClientId);
                        bNegBalToZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("NEG_BAL_TO_ZERO").ToString()), m_iClientId);
                        bSetToZero = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("SET_TO_ZERO").ToString()), m_iClientId);

                        if (!(bBalTOZero || bNegBalToZero || bSetToZero))
                            do_NOTHING = true;
                        //Shruti for MITS 8551
                        bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("COLL_IN_INCUR_BAL").ToString()), m_iClientId);

                        bResByClaimType = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_BY_CLM_TYPE").ToString()), m_iClientId);
                        bPerRsvColInRsvBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_RSV_BAL"));//asharma326 JIRA 870
                        bPerRsvColInIncrBal = Convert.ToBoolean(objReader.GetInt16("PER_RSV_COLL_IN_INCR_BAL"));//asharma326 JIRA 870
                    }
                    objReader.Close();

                }//if (objReader != null)
                sbSQL.Remove(0, sbSQL.Length);

                // Ankit Start : Worked for Do Not Recalc Reserves Setting
                sbSQL.Append(" SELECT RES_DO_NOT_RECALC FROM SYS_PARMS");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bDoNotRecalcReserves = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
                            objReader.GetValue("RES_DO_NOT_RECALC").ToString()), m_iClientId);
                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);
                //Ankit End
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);// MITS 35453
                //if (bDoNotRecalcReserves)
                //if (bDoNotRecalcReserves && objSysSettings.UseMultiCurrency == 0)
                //// MITS 35453 ends
                //{
                //    //rupal:start
                //    //Ankit Start : Worked on MITS - 32359
                //    sbSQL.Append("SELECT RC.RC_ROW_ID,RC.CLAIMANT_EID,RC.RESERVE_TYPE_CODE,RC.RESERVE_AMOUNT,RC.INCURRED_AMOUNT, ");
                //    sbSQL.Append("RC.BALANCE_AMOUNT,RC.COLLECTION_TOTAL,RC.PAID_TOTAL,RC.RES_STATUS_CODE,RC.POLCVG_ROW_ID, ");
                //    sbSQL.Append("RC.LSS_RES_EXP_FLAG, ");//sharishkumar: Mits35472
                //    //Ankit End
                //    sbSQL.Append("PU.POLICY_ID,PU.UNIT_ID,PU.UNIT_TYPE,PU.POLICY_UNIT_ROW_ID,POLCVG.COVERAGE_TYPE_CODE, ");
                //    sbSQL.Append(" POLCVG.CVG_SEQUENCE_NO,COVERAGE_X_LOSS.LOSS_CODE, COVERAGE_X_LOSS.CVG_LOSS_ROW_ID, RC.RESERVE_CATEGORY, COVERAGE_X_LOSS.DISABILITY_CAT, RC.REASON, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, ");     //Ankit Start : Worked on MITS - 34297
                //    sbSQL.Append(" RC.ASSIGNADJ_EID "); //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                //    sbSQL.Append(" ,POLCVG.COVERAGE_TEXT ");//smahajan22 mits 35587
                //    sbSQL.Append(" FROM RESERVE_CURRENT RC,POLICY_X_CVG_TYPE POLCVG,COVERAGE_X_LOSS,POLICY_X_UNIT PU ");
                //    sbSQL.Append("WHERE RC.CLAIM_ID = " + p_iClaimID);
                //    if (p_iClaimantEid != 0)
                //    {
                //        sbSQL.Append(" AND RC.CLAIMANT_EID = " + p_iClaimantEid);
                //    }
                //    sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID <> 0");
                //    sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID = COVERAGE_X_LOSS.CVG_LOSS_ROW_ID");
                //    sbSQL.Append(" AND  POLCVG.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID");
                //    sbSQL.Append(" AND POLCVG.POLICY_UNIT_ROW_ID = PU.POLICY_UNIT_ROW_ID");

                //    sbSQL.Append(" ORDER BY RESERVE_TYPE_CODE, DATE_ENTERED DESC ");
                //}
                ////rupal:start, r8 multicurrency
                //else
                //{
                if (objSysSettings.UseMultiCurrency != 0)
                {
                    if (IsBaseCurrTypeSelected)
                    {
                        //abisht I think this needs to be done from funds table instead of reserve current - todo 
                        //pgupta93: MITS:34082 START
                        //sbSQL.Append("SELECT PU.POLICY_ID,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE, RC.REASON, CVGLOSS.CVG_LOSS_ROW_ID, SUM(RC.RESERVE_AMOUNT)  RES_AMOUNT,");
                        sbSQL.Append("SELECT PU.POLICY_ID,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE, RC.REASON, CVGLOSS.CVG_LOSS_ROW_ID, SUM(RC.RESERVE_AMOUNT)  RES_AMOUNT,RC.BASE_TO_RESERVE_CUR_RATE,RC.RSV_TO_CLAIM_CUR_RATE,");
                        //pgupta93: MITS:34082 END
                        sbSQL.Append(" SUM(RC.PAID_TOTAL)  PAID_TOTAL, SUM(RC.COLLECTION_TOTAL)  COLL_TOTAL,CVGLOSS.LOSS_CODE,CVGLOSS.DISABILITY_CAT, RC.RESERVE_CATEGORY,SUM (RC.BALANCE_AMOUNT) BALANCE_AMOUNT,SUM(RC.INCURRED_AMOUNT) INCURRED_AMOUNT, SUM (RC.RESERVE_AMOUNT) RESERVE_AMOUNT,");
                        sbSQL.Append(" PU.UNIT_ID,PU.UNIT_TYPE,PU.POLICY_UNIT_ROW_ID,POLCVG.COVERAGE_TYPE_CODE,RC.CLAIMANT_EID,POLCVG.CVG_SEQUENCE_NO, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, ");        //Ankit Start : Worked on MITS - 34297
                        sbSQL.Append("RC.LSS_RES_EXP_FLAG, ");//sharishkumar: Mits35472
                        sbSQL.Append(" RC.ASSIGNADJ_EID "); //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                        sbSQL.Append(" ,POLCVG.COVERAGE_TEXT ");//smahajan22 mits 35587
                        if (bAddDedCode)
                        {
                            sbSQL.Append(" , CPD.DED_TYPE_CODE");
                        }
                        sbSQL.Append(" FROM RESERVE_CURRENT RC,COVERAGE_X_LOSS CVGLOSS,POLICY_X_CVG_TYPE POLCVG,POLICY_X_UNIT PU ");
                        if (bAddDedCode)
                        {
                            sbSQL.Append(", CLAIM_X_POL_DED CPD");
                        }
                        sbSQL.Append(" WHERE RC.CLAIM_ID = " + p_iClaimID);
                        sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID = CVGLOSS.CVG_LOSS_ROW_ID AND POLCVG.POLCVG_ROW_ID=CVGLOSS.POLCVG_ROW_ID ");
                        sbSQL.Append(" AND POLCVG.POLICY_UNIT_ROW_ID = PU.POLICY_UNIT_ROW_ID");
                        if (p_iClaimantEid != 0)
                            sbSQL.Append(" AND RC.CLAIMANT_EID = " + p_iClaimantEid);
                        sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID <> 0");
                        //start - Added by Nikhil.Exclude deductible recovery reserve listing
                        sbSQL.Append(" AND RC.RESERVE_TYPE_CODE <> " + iDedRecoveryResTypeCode);
                        if (bAddDedCode)
                        {
                            sbSQL.Append(" AND CPD.CLAIM_ID = " + p_iClaimID);
                            sbSQL.Append(" AND CPD.POLCVG_ROW_ID = POLCVG.POLCVG_ROW_ID AND CPD.POLICY_UNIT_ROW_ID = POLCVG.POLICY_UNIT_ROW_ID ");
                        }
                        // end - Added by Nikhil.Exclude deductible recovery reserve listing
                        sbSQL.Append(" GROUP BY RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.LSS_RES_EXP_FLAG,PU.POLICY_ID, ");
                        sbSQL.Append(" PU.UNIT_ID,PU.UNIT_TYPE,PU.POLICY_UNIT_ROW_ID,POLCVG.COVERAGE_TYPE_CODE,RC.CLAIMANT_EID,CVGLOSS.CVG_LOSS_ROW_ID, ");
                        //pgupta93: MITS:34082 START
                        //sbSQL.Append(" CVGLOSS.LOSS_CODE,CVGLOSS.DISABILITY_CAT,POLCVG.CVG_SEQUENCE_NO, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, RC.RESERVE_CATEGORY,RC.REASON, RC.ASSIGNADJ_EID ");   //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement       //Ankit Start : Worked on MITS - 34297
                        sbSQL.Append(" CVGLOSS.LOSS_CODE,CVGLOSS.DISABILITY_CAT,POLCVG.CVG_SEQUENCE_NO, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, RC.RESERVE_CATEGORY,RC.BALANCE_AMOUNT,RC.INCURRED_AMOUNT,RC.REASON, RC.ASSIGNADJ_EID,RC.BASE_TO_RESERVE_CUR_RATE,RC.RSV_TO_CLAIM_CUR_RATE,POLCVG.COVERAGE_TEXT ");
                        if (bAddDedCode)
                        {
                            sbSQL.Append(" , CPD.DED_TYPE_CODE");
                        }
                        //pgupta93: MITS:34082 END

                        eNavType = CommonFunctions.NavFormType.None;
                    }
                    else
                    {   //abisht I think this needs to be done from funds table instead of reserve current - todo 
                        sbSQL.Append("SELECT PU.POLICY_ID,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE, RC.REASON, CVGLOSS.CVG_LOSS_ROW_ID, SUM(RC.CLAIM_CURRENCY_RESERVE_AMOUNT)  RES_AMOUNT,");
                        sbSQL.Append(" SUM(RC.CLAIM_CURRENCY_PAID_TOTAL)  PAID_TOTAL, SUM(RC.CLAIM_CURR_COLLECTION_TOTAL)  COLL_TOTAL, CVGLOSS.DISABILITY_CAT, RC.RESERVE_CATEGORY , SUM (RC.CLAIM_CURRENCY_BALANCE_AMOUNT) BALANCE_AMOUNT, SUM(RC.CLAIM_CURRENCY_INCURRED_AMOUNT) INCURRED_AMOUNT, SUM (RC.CLAIM_CURRENCY_RESERVE_AMOUNT) RESERVE_AMOUNT,");
                        sbSQL.Append(" PU.UNIT_ID,PU.UNIT_TYPE,PU.POLICY_UNIT_ROW_ID,POLCVG.COVERAGE_TYPE_CODE,CVGLOSS.LOSS_CODE,POLCVG.CVG_SEQUENCE_NO,RC.CLAIMANT_EID, ");
                        sbSQL.Append(" RC.ASSIGNADJ_EID, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, "); //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement     //Ankit Start : Worked on MITS - 34297
                        sbSQL.Append("RC.LSS_RES_EXP_FLAG, ");//sharishkumar: Mits35472
                        sbSQL.Append("POLCVG.COVERAGE_TEXT ");//smahajan22 mits 35587
                        if (bAddDedCode)
                        {
                            sbSQL.Append(" , CPD.DED_TYPE_CODE");
                        }
                        sbSQL.Append(" FROM COVERAGE_X_LOSS CVGLOSS, RESERVE_CURRENT RC,POLICY_X_CVG_TYPE POLCVG,POLICY_X_UNIT PU ");
                        if (bAddDedCode)
                        {
                            sbSQL.Append(", CLAIM_X_POL_DED CPD ");
                        }
                        sbSQL.Append(" WHERE RC.CLAIM_ID = " + p_iClaimID);

                        sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID = CVGLOSS.CVG_LOSS_ROW_ID AND POLCVG.POLCVG_ROW_ID=CVGLOSS.POLCVG_ROW_ID ");
                        sbSQL.Append(" AND POLCVG.POLICY_UNIT_ROW_ID = PU.POLICY_UNIT_ROW_ID");
                        if (p_iClaimantEid != 0)
                            sbSQL.Append(" AND RC.CLAIMANT_EID = " + p_iClaimantEid);
                        //else if (p_iUnitID != 0)
                        //    sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);
                        sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID <> 0");
                        //start - Added by Nikhil.Exclude deductible recovery reserve listing
                        sbSQL.Append(" AND RC.RESERVE_TYPE_CODE <> " + iDedRecoveryResTypeCode);
                        // end - Added by Nikhil.Exclude deductible recovery reserve listing
                        if (bAddDedCode)
                        {
                            sbSQL.Append(" AND CPD.CLAIM_ID = " + p_iClaimID);
                            sbSQL.Append(" AND CPD.POLCVG_ROW_ID = POLCVG.POLCVG_ROW_ID AND CPD.POLICY_UNIT_ROW_ID = POLCVG.POLICY_UNIT_ROW_ID ");
                        }
                        sbSQL.Append(" GROUP BY RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.LSS_RES_EXP_FLAG,PU.POLICY_ID, ");
                        sbSQL.Append(" PU.UNIT_ID,PU.UNIT_TYPE,PU.POLICY_UNIT_ROW_ID,POLCVG.COVERAGE_TYPE_CODE,RC.CLAIMANT_EID,CVGLOSS.CVG_LOSS_ROW_ID, ");
                        sbSQL.Append(" CVGLOSS.LOSS_CODE,CVGLOSS.DISABILITY_CAT, RC.RESERVE_CATEGORY,RC.BALANCE_AMOUNT,RC.RESERVE_AMOUNT,RC.INCURRED_AMOUNT,POLCVG.CVG_SEQUENCE_NO, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY,RC.REASON, RC.ASSIGNADJ_EID , POLCVG.COVERAGE_TEXT "); //dbisht6 on the behalf of shreya  //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement //Ankit Start : Worked on MITS - 34297
                        if (bAddDedCode)
                        {
                            sbSQL.Append(" , CPD.DED_TYPE_CODE");
                        }
                        eNavType = CommonFunctions.NavFormType.Reserve;
                    }
                    //rupal:end
                }
                else
                {
                    //abisht I think this needs to be done from funds table instead of reserve current - todo 
                    //pgupta93: MITS:34082 START
                    //sbSQL.Append("SELECT PU.POLICY_ID,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE, RC.REASON, CVGLOSS.CVG_LOSS_ROW_ID, SUM(RC.RESERVE_AMOUNT)  RES_AMOUNT,");
                    sbSQL.Append("SELECT PU.POLICY_ID,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE, RC.REASON, CVGLOSS.CVG_LOSS_ROW_ID, SUM(RC.RESERVE_AMOUNT)  RES_AMOUNT,RC.BASE_TO_RESERVE_CUR_RATE,RC.RSV_TO_CLAIM_CUR_RATE,");
                    //pgupta93: MITS:34082 END
                    sbSQL.Append(" SUM(RC.PAID_TOTAL)  PAID_TOTAL, SUM(RC.COLLECTION_TOTAL)  COLL_TOTAL,CVGLOSS.LOSS_CODE,CVGLOSS.DISABILITY_CAT, RC.RESERVE_CATEGORY,SUM (RC.BALANCE_AMOUNT) BALANCE_AMOUNT,SUM(RC.INCURRED_AMOUNT) INCURRED_AMOUNT,SUM (RC.RESERVE_AMOUNT) RESERVE_AMOUNT,");
                    sbSQL.Append(" PU.UNIT_ID,PU.UNIT_TYPE,PU.POLICY_UNIT_ROW_ID,POLCVG.COVERAGE_TYPE_CODE,RC.CLAIMANT_EID,POLCVG.CVG_SEQUENCE_NO, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, ");        //Ankit Start : Worked on MITS - 34297
                    sbSQL.Append("RC.LSS_RES_EXP_FLAG, ");//sharishkumar: Mits35472
                    sbSQL.Append(" RC.ASSIGNADJ_EID "); //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                    sbSQL.Append(" ,POLCVG.COVERAGE_TEXT ");//smahajan22 mits 35587
                    if (bAddDedCode)
                    {
                        sbSQL.Append(" , CPD.DED_TYPE_CODE ");
                    }
                    sbSQL.Append(" FROM RESERVE_CURRENT RC,COVERAGE_X_LOSS CVGLOSS,POLICY_X_CVG_TYPE POLCVG,POLICY_X_UNIT PU ");
                    if (bAddDedCode)
                    {
                        sbSQL.Append(" , CLAIM_X_POL_DED CPD ");
                    }
                    sbSQL.Append(" WHERE RC.CLAIM_ID = " + p_iClaimID);
                    sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID = CVGLOSS.CVG_LOSS_ROW_ID AND POLCVG.POLCVG_ROW_ID=CVGLOSS.POLCVG_ROW_ID ");
                    sbSQL.Append(" AND POLCVG.POLICY_UNIT_ROW_ID = PU.POLICY_UNIT_ROW_ID");
                    if (p_iClaimantEid != 0)
                        sbSQL.Append(" AND RC.CLAIMANT_EID = " + p_iClaimantEid);
                    sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID <> 0");
                    //start - Added by Nikhil.Exclude deductible recovery reserve listing
                    sbSQL.Append(" AND RC.RESERVE_TYPE_CODE <> " + iDedRecoveryResTypeCode);
                    // end - Added by Nikhil.Exclude deductible recovery reserve listing
                    if (bAddDedCode)
                    {
                        sbSQL.Append(" AND CPD.CLAIM_ID =" + p_iClaimID);
                        sbSQL.Append(" AND CPD.POLCVG_ROW_ID = POLCVG.POLCVG_ROW_ID AND CPD.POLICY_UNIT_ROW_ID = POLCVG.POLICY_UNIT_ROW_ID ");
                    }
                    sbSQL.Append(" GROUP BY RC.RESERVE_TYPE_CODE, RC.RES_STATUS_CODE,CVGLOSS.POLCVG_ROW_ID,RC.RC_ROW_ID,RC.LSS_RES_EXP_FLAG,PU.POLICY_ID, ");
                    sbSQL.Append(" PU.UNIT_ID,PU.UNIT_TYPE,PU.POLICY_UNIT_ROW_ID,POLCVG.COVERAGE_TYPE_CODE,RC.CLAIMANT_EID,CVGLOSS.CVG_LOSS_ROW_ID, ");
                    //pgupta93: MITS:34082 START
                    //sbSQL.Append(" CVGLOSS.LOSS_CODE,CVGLOSS.DISABILITY_CAT,POLCVG.CVG_SEQUENCE_NO, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, RC.RESERVE_CATEGORY,RC.REASON, RC.ASSIGNADJ_EID ");   //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement       //Ankit Start : Worked on MITS - 34297
                    sbSQL.Append(" CVGLOSS.LOSS_CODE,CVGLOSS.DISABILITY_CAT,POLCVG.CVG_SEQUENCE_NO, POLCVG.TRANS_SEQ_NO, POLCVG.COVERAGE_KEY, RC.RESERVE_CATEGORY,RC.BALANCE_AMOUNT,RC.INCURRED_AMOUNT,RC.REASON, RC.ASSIGNADJ_EID,RC.BASE_TO_RESERVE_CUR_RATE,RC.RSV_TO_CLAIM_CUR_RATE,POLCVG.COVERAGE_TEXT ");
                    if (bAddDedCode)
                    {
                        sbSQL.Append(", CPD.DED_TYPE_CODE");
                    }
                    //pgupta93: MITS:34082 END

                    eNavType = CommonFunctions.NavFormType.None;
                }



                 objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                arrReserves = new structReserves[objDataSet.Tables[0].Rows.Count];

            //    if (bDoNotRecalcReserves && objDataSet.Tables[0] != null && objSysSettings.UseMultiCurrency == 0) //MITS 35453 MUlticurrency check added 
                if (bDoNotRecalcReserves && objDataSet.Tables[0] != null)//&& objSysSettings.UseMultiCurrency == 0) //MITS 35453 MUlticurrency check added 
                    {
                    //Special reserve processing
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iTemp = 0; iTemp < objDataSet.Tables[0].Rows.Count; iTemp++)
                        {
                            //for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                            // {
                            iStatus = Conversion.ConvertStrToInteger(
                                objDataSet.Tables[0].Rows[iTemp]["RES_STATUS_CODE"].ToString());
                            iCodeID = Conversion.ConvertStrToInteger(
                                objDataSet.Tables[0].Rows[iTemp]["RESERVE_TYPE_CODE"].ToString());

                            //if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                            //{
                            objDataRow = objDataSet.Tables[0].Rows[iTemp];

                            arrReserves[iTemp].iReserveTypeCode = iCodeID;


                            arrReserves[iTemp].dBalance += Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                            arrReserves[iTemp].dTotalPaid += Conversion.ConvertStrToDouble(objDataRow["PAID_TOTAL"].ToString());
                            arrReserves[iTemp].dTotalCollected += Conversion.ConvertStrToDouble(objDataRow["COLL_TOTAL"].ToString());
                            arrReserves[iTemp].dTotalIncurred += Conversion.ConvertStrToDouble(objDataRow["INCURRED_AMOUNT"].ToString());
                            arrReserves[iTemp].dReserveAmount = Conversion.ConvertStrToDouble(objDataRow["RES_AMOUNT"].ToString());
                            if (bAddDedCode)
                            {
                                arrReserves[iTemp].DeductibleTypeCode = Conversion.ConvertObjToStr(objDataRow["DED_TYPE_CODE"]);
                            }
                            if (objCache == null)  // only allocate once and then use for subsequent ones
                                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                            if (iStatus != 0)
                            {
                                if (arrReserves[iTemp].sStatusCode != "")
                                    arrReserves[iTemp].sStatusCode = arrReserves[iTemp].sStatusCode;
                                arrReserves[iTemp].sStatusCode = arrReserves[iTemp].sStatusCode + objCache.GetShortCode(iStatus);
                                //rupal
                                arrReserves[iTemp].sStatusCodeDesc = arrReserves[iTemp].sStatusCodeDesc + objCache.GetCodeDesc(iStatus);
                            }
                            else
                            {
                                arrReserves[iTemp].sStatusCode = "-";
                                //rupal
                                arrReserves[iTemp].sStatusCodeDesc = "-";
                            }

                            arrReserves[iTemp].iPolicyID = Conversion.ConvertStrToInteger(objDataRow["POLICY_ID"].ToString());
                            arrReserves[iTemp].iPolCvg_Row_ID = Conversion.ConvertStrToInteger(objDataRow["POLCVG_ROW_ID"].ToString());
                            arrReserves[iTemp].iRC_Row_ID = Conversion.ConvertStrToInteger(objDataRow["RC_ROW_ID"].ToString());
                            arrReserves[iTemp].iLSS_RES_EXP_FLAG = Conversion.ConvertStrToInteger(objDataRow["LSS_RES_EXP_FLAG"].ToString());//sharishkumar: Mits35472
                            arrReserves[iTemp].iReserveTypeCode = iCodeID;
                            //rsharma220 MITS 33736 Start
                            arrReserves[iTemp].sReserveDesc = objCache.GetShortCode(iCodeID) + " " + objCache.GetCodeDesc(iCodeID, this.LanguageCode);
                            arrReserves[iTemp].iUnitID = Conversion.ConvertStrToInteger(objDataRow["UNIT_ID"].ToString());
                            arrReserves[iTemp].sUnitType = objDataRow["UNIT_TYPE"].ToString();
                            arrReserves[iTemp].iPolicyUnitRowId = Conversion.ConvertStrToInteger(objDataRow["POLICY_UNIT_ROW_ID"].ToString());
                            arrReserves[iTemp].iCoveTypeCode = Conversion.ConvertStrToInteger(objDataRow["COVERAGE_TYPE_CODE"].ToString());
                            //coverage text:smahajan22 mits 35587
                            arrReserves[iTemp].sCoveTypeText = Conversion.ConvertObjToStr(objDataRow["COVERAGE_TEXT"]);
                                                     
                            arrReserves[iTemp].iClaimantEid = Conversion.CastToType<Int32>(objDataRow["CLAIMANT_EID"].ToString(), out bSuccess);
							//skhare7 Policy Interface
                         	//   arrReserves[iTemp].iDisabilityCat = Conversion.ConvertStrToInteger(objDataRow["COVERAGE_TYPE_CODE"].ToString());
                            arrReserves[iTemp].iLossTypeCode = Conversion.ConvertStrToInteger(objDataRow["LOSS_CODE"].ToString());
                            arrReserves[iTemp].iPolCvgLossId = Conversion.ConvertStrToInteger(objDataRow["CVG_LOSS_ROW_ID"].ToString());
                            arrReserves[iTemp].iReserveCat = Conversion.ConvertStrToInteger(objDataRow["RESERVE_CATEGORY"].ToString());
                            arrReserves[iTemp].iDisabilityCat = Conversion.ConvertStrToInteger(objDataRow["DISABILITY_CAT"].ToString());
                            arrReserves[iTemp].sDisabilityCatDesc = objCache.GetCodeDesc(arrReserves[iTemp].iDisabilityCat, this.LanguageCode);
                            arrReserves[iTemp].sReserveCatDesc = objCache.GetCodeDesc(arrReserves[iTemp].iReserveCat, this.LanguageCode);
     						arrReserves[iTemp].sCovgSeqNum = Conversion.ConvertObjToStr(objDataRow["CVG_SEQUENCE_NO"]);
                            arrReserves[iTemp].sReason = Conversion.ConvertObjToStr(objDataRow["REASON"]); //prashbiharis MITS:30949
                            arrReserves[iTemp].sTransSeqNum = Conversion.ConvertObjToStr(objDataRow["TRANS_SEQ_NO"]);
                            arrReserves[iTemp].sCoverageKey = Conversion.ConvertObjToStr(objDataRow["COVERAGE_KEY"]);       //Ankit Start : Worked on MITS - 34297
                            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                            if (objDataRow["ASSIGNADJ_EID"] is DBNull)
                                objDataRow["ASSIGNADJ_EID"] = 0;
                            else if (string.IsNullOrEmpty(objDataRow["ASSIGNADJ_EID"].ToString()))
                                objDataRow["ASSIGNADJ_EID"] = 0;
                            arrReserves[iTemp].sAdjusterDetails = objDataRow["ASSIGNADJ_EID"] + "|@" + objCache.GetEntityLastFirstName(Convert.ToInt32(objDataRow["ASSIGNADJ_EID"]));
                            //Ankit End
                            //MITS 35453 starts
                            if (objSysSettings.UseMultiCurrency == 0)
                            {
                                arrReserves[iTemp].dClmCurrBalAmount = Conversion.ConvertStrToDouble(objDataRow["BALANCE_AMOUNT"].ToString());
                                arrReserves[iTemp].dClmCurrResAmount = Conversion.ConvertStrToDouble(objDataRow["RESERVE_AMOUNT"].ToString());
                            }
                            //MITS 35453 ends 
                            arrReserves[iTemp].sRsvStatusParent = objCache.GetShortCode(objCache.GetRelatedCodeId(iStatus));
                            //pgupta93: MITS:34082 START
                            if (objSysSettings.UseMultiCurrency != 0)
                            {
                                //arrReserves[iTemp].dBaseToReserveCurrRate = Conversion.ConvertObjToDouble(objDataRow["BASE_TO_RESERVE_CUR_RATE"], m_iClientId);
                                //arrReserves[iTemp].dReserveToClaimCurrRate = Conversion.ConvertObjToDouble(objDataRow["RSV_TO_CLAIM_CUR_RATE"], m_iClientId);
                                //MITS 35453 starts
                                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimID, m_sConnectionString);
                                if (iClaimCurrCode > 0)
                                {
                                    if (IsBaseCurrTypeSelected)
                                    {
                                        //pgupta93: MITS:34082 START
                                        //double dExhRateBaseToClaim = CommonFunctions.ExchangeRateSrc2Dest(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), iClaimCurrCode, m_sConnectionString);
                                        //arrReserves[iTemp].dClmCurrBalAmount =dExhRateBaseToClaim * dRBalance
                                        //arrReserves[iTemp].dClmCurrTotalIncurred = dExhRateBaseToClaim * dRIncurred;

                                        arrReserves[iTemp].dBaseToReserveCurrRate = Conversion.ConvertObjToDouble(objDataRow["BASE_TO_RESERVE_CUR_RATE"], m_iClientId);
                                        arrReserves[iTemp].dReserveToClaimCurrRate = Conversion.ConvertObjToDouble(objDataRow["RSV_TO_CLAIM_CUR_RATE"], m_iClientId);

                                        arrReserves[iTemp].dClmCurrBalAmount = arrReserves[iTemp].dBaseToReserveCurrRate * arrReserves[iTemp].dReserveToClaimCurrRate * dRBalance;
                                        arrReserves[iTemp].dClmCurrResAmount = arrReserves[iTemp].dBaseToReserveCurrRate * arrReserves[iTemp].dReserveToClaimCurrRate * dRAmount;

                                        //pgupta93: MITS:34082 END
                                    }

                                    else
                                    {
                                        arrReserves[iTemp].dClmCurrBalAmount = dRBalance;
                                        arrReserves[iTemp].dClmCurrResAmount = dRAmount;
                                    }
                                }
                                //MITS 35453 ends 
                            }
                            //pgupta93: MITS:34082 END

                        }//for(iTemp=0; iTemp < objDataSet.Tables[0].Rows.Count;iTemp++)
                    }//Special reserve processing
                }//if (bDoNotRecalcReserves && objDataSet.Tables[0] != null)
                else if (objDataSet.Tables[0] != null)
                {
                    if (objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iTemp = 0; iTemp < objDataSet.Tables[0].Rows.Count; iTemp++)
                        {
                            //for (iCnt = 0; iCnt < arrReserves.Length; iCnt++)
                            // {
                            iStatus = Conversion.ConvertStrToInteger(
                                objDataSet.Tables[0].Rows[iTemp]["RES_STATUS_CODE"].ToString());
                            iCodeID = Conversion.ConvertStrToInteger(
                                objDataSet.Tables[0].Rows[iTemp]["RESERVE_TYPE_CODE"].ToString());

                            //if (arrReserves[iCnt].iReserveTypeCode == iCodeID)
                            //{
                            arrReserves[iTemp].iReserveTypeCode = iCodeID;
                            objDataRow = objDataSet.Tables[0].Rows[iTemp];
                            dRAmount = Conversion.ConvertStrToDouble(
                                objDataRow["RES_AMOUNT"].ToString());
                            dRPaid = Conversion.ConvertStrToDouble(
                                objDataRow["PAID_TOTAL"].ToString());
                            dRCollect = Conversion.ConvertStrToDouble(
                                objDataRow["COLL_TOTAL"].ToString());

                            dRBalance = CalculateReserveBalance(dRAmount, dRPaid, dRCollect, bCollInRsvBal, iCStat, iCodeID, bPerRsvColInRsvBal, iLOB);//asharma326 JIRA 870 add  bPerRsvColInRsvBal, iLOB
                            dRIncurred = CalculateIncurred(dRBalance, dRPaid, dRCollect, bCollInRsvBal, bCollInIncurredBal, iCodeID, bPerRsvColInRsvBal, bPerRsvColInIncrBal, iLOB);//asharma326 JIRA 870 add  bPerRsvColInRsvBal, bPerRsvColInIncrBal, iLOB

                            arrReserves[iTemp].dBalance += dRBalance;
                            arrReserves[iTemp].dTotalPaid += dRPaid;
                            arrReserves[iTemp].dTotalCollected += dRCollect;
                            arrReserves[iTemp].dTotalIncurred += dRIncurred;
                            arrReserves[iTemp].dReserveAmount = dRAmount;
                            if (bAddDedCode)
                            {
                                arrReserves[iTemp].DeductibleTypeCode = Conversion.ConvertObjToStr(objDataRow["DED_TYPE_CODE"]);
                            }

                            if (objCache == null)  // only allocate once and then use for subsequent ones
                                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                            if (iStatus != 0)
                            {
                                //if (arrReserves[iTemp].sStatusCode != "")
                                //    arrReserves[iTemp].sStatusCode = arrReserves[iTemp].sStatusCode;

                                arrReserves[iTemp].sStatusCode = arrReserves[iTemp].sStatusCode + objCache.GetShortCode(iStatus);
                                //rupal
                                arrReserves[iTemp].sStatusCodeDesc = arrReserves[iTemp].sStatusCodeDesc + objCache.GetCodeDesc(iStatus);
                            }
                            else
                            {
                                arrReserves[iTemp].sStatusCode = "-";
                                //rupal
                                arrReserves[iTemp].sStatusCodeDesc = "-";
                            }
                            arrReserves[iTemp].sReserveDesc = objCache.GetShortCode(iCodeID) + " " + objCache.GetCodeDesc(iCodeID, this.LanguageCode);
                            arrReserves[iTemp].iPolicyID = Conversion.ConvertStrToInteger(objDataRow["POLICY_ID"].ToString());
                            arrReserves[iTemp].iPolCvg_Row_ID = Conversion.ConvertStrToInteger(objDataRow["POLCVG_ROW_ID"].ToString());
                            arrReserves[iTemp].iRC_Row_ID = Conversion.ConvertStrToInteger(objDataRow["RC_ROW_ID"].ToString());
                            arrReserves[iTemp].iLSS_RES_EXP_FLAG = Conversion.ConvertStrToInteger(objDataRow["LSS_RES_EXP_FLAG"].ToString());//sharishkumar: Mits35472
                            arrReserves[iTemp].iReserveTypeCode = iCodeID;
                            arrReserves[iTemp].iUnitID = Conversion.ConvertStrToInteger(objDataRow["UNIT_ID"].ToString());
                            arrReserves[iTemp].sUnitType = objDataRow["UNIT_TYPE"].ToString();
                            arrReserves[iTemp].iPolicyUnitRowId = Conversion.ConvertStrToInteger(objDataRow["POLICY_UNIT_ROW_ID"].ToString());
                            arrReserves[iTemp].iCoveTypeCode = Conversion.ConvertStrToInteger(objDataRow["COVERAGE_TYPE_CODE"].ToString());
                            //coverage text:smahajan22 mits 35587
                            arrReserves[iTemp].sCoveTypeText = Conversion.ConvertObjToStr(objDataRow["COVERAGE_TEXT"]);
                            arrReserves[iTemp].iClaimantEid = Conversion.CastToType<Int32>(objDataRow["CLAIMANT_EID"].ToString(), out bSuccess);
							arrReserves[iTemp].iLossTypeCode = Conversion.ConvertStrToInteger(objDataRow["LOSS_CODE"].ToString());
                            arrReserves[iTemp].iPolCvgLossId = Conversion.ConvertStrToInteger(objDataRow["CVG_LOSS_ROW_ID"].ToString());
                            arrReserves[iTemp].iReserveCat = Conversion.ConvertStrToInteger(objDataRow["RESERVE_CATEGORY"].ToString());
                            arrReserves[iTemp].iDisabilityCat = Conversion.ConvertStrToInteger(objDataRow["DISABILITY_CAT"].ToString());
                            arrReserves[iTemp].sDisabilityCatDesc = objCache.GetCodeDesc(arrReserves[iTemp].iDisabilityCat, this.LanguageCode);
                            arrReserves[iTemp].sReserveCatDesc = objCache.GetCodeDesc(arrReserves[iTemp].iReserveCat, this.LanguageCode);
                            //rsharma220 MITS 33736 End
                            arrReserves[iTemp].sCovgSeqNum = Conversion.ConvertObjToStr(objDataRow["CVG_SEQUENCE_NO"]);
                            arrReserves[iTemp].sReason = Conversion.ConvertObjToStr(objDataRow["REASON"]); //prashbiharis MITS:30949
                            arrReserves[iTemp].sTransSeqNum = Conversion.ConvertObjToStr(objDataRow["TRANS_SEQ_NO"]);
                            arrReserves[iTemp].sCoverageKey = Conversion.ConvertObjToStr(objDataRow["COVERAGE_KEY"]);      //Ankit Start : Worked on MITS - 34297
                            //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                            if (objDataRow["ASSIGNADJ_EID"] is DBNull)
                                objDataRow["ASSIGNADJ_EID"] = 0;
                            arrReserves[iTemp].sAdjusterDetails = objDataRow["ASSIGNADJ_EID"] + "|@" + objCache.GetEntityLastFirstName(Convert.ToInt32(objDataRow["ASSIGNADJ_EID"]));
                            //Ankit End
                            //MITS 35453 starts
                            int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimID, m_sConnectionString);
                            if (iClaimCurrCode > 0)
                            {
                                if (IsBaseCurrTypeSelected)
                                {
                                    //pgupta93: MITS:34082 START
                                    //double dExhRateBaseToClaim = CommonFunctions.ExchangeRateSrc2Dest(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), iClaimCurrCode, m_sConnectionString);
                                    //arrReserves[iTemp].dClmCurrBalAmount =dExhRateBaseToClaim * dRBalance
                                    //arrReserves[iTemp].dClmCurrTotalIncurred = dExhRateBaseToClaim * dRIncurred;
                                    if (objSysSettings.UseMultiCurrency != 0)
                                    {
                                        arrReserves[iTemp].dBaseToReserveCurrRate = Conversion.ConvertObjToDouble(objDataRow["BASE_TO_RESERVE_CUR_RATE"], m_iClientId);
                                        arrReserves[iTemp].dReserveToClaimCurrRate = Conversion.ConvertObjToDouble(objDataRow["RSV_TO_CLAIM_CUR_RATE"], m_iClientId);

                                        arrReserves[iTemp].dClmCurrBalAmount = arrReserves[iTemp].dBaseToReserveCurrRate * arrReserves[iTemp].dReserveToClaimCurrRate * dRBalance;
                                        arrReserves[iTemp].dClmCurrResAmount = arrReserves[iTemp].dBaseToReserveCurrRate * arrReserves[iTemp].dReserveToClaimCurrRate * dRAmount;
                                    }
                                    //pgupta93: MITS:34082 END
                                }
                         
                                else
                                {
                                    arrReserves[iTemp].dClmCurrBalAmount = dRBalance;
                                    arrReserves[iTemp].dClmCurrResAmount = dRAmount;
                                }
                            }
                            //MITS 35453 ends 
                            arrReserves[iTemp].sRsvStatusParent = objCache.GetShortCode(objCache.GetRelatedCodeId(iStatus));
                        }//for(iTemp=0; iTemp < objDataSet.Tables[0].Rows.Count;iTemp++)
                    }//if(objDataSet.Tables[0].Rows.Count >0)
                }//Else if !(bDoNotRecalcReserves)
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesDetails.GenericError", m_iClientId), objException);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                objSysSettings = null;  //35453 
                objDataSet = null;
                objDataRow = null;               
            
            }

            return arrReserves;
        }

        private struct DedInfo
        {
            public string sPolicyName;
            public int iCoverageType;
            public int iDedTypeCode;
            public double dDedAmt;
            public int iPolCvgRowId;
            public int iUnitId;
            public string sUnitType;
            public int iPolicyId;
            public int iUnitRowId;
            public int iPolicyLob;
        }

        /// <summary>
        /// Inserts Deductibles to the Corresponding table. Does NOT update any existing deductible.
        /// </summary>
        /// <param name="p_iClaimID">Claim Id for which deductible is to be inserted</param>
        public void InsertDeductibles(int p_iClaimID)
        {
            DedInfo dedInfo;
            DbReader dbReader = null;
            Dictionary<string, DedInfo> dictDed = null;
            StringBuilder sSql = new StringBuilder(string.Empty);
            string sKey = string.Empty;

            Claim objClaim = null;
            ClaimXPolDed objClmXPolDed = null;
            int iClaimantRowId = Int32.MaxValue;
            int iClaimantEid = 0;

            objClaim = m_objDataModelFactory.GetDataModelObject("Claim", false) as Claim;
            objClaim.MoveTo(p_iClaimID);

            if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1
                    && objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1)
            {
                foreach (Claimant objClaimant in objClaim.ClaimantList)
                {
                    if (iClaimantRowId > objClaimant.ClaimantRowId)
                    {
                        iClaimantRowId = objClaimant.ClaimantRowId;
                        iClaimantEid = objClaimant.ClaimantEid;
                    }

                    objClaimant.Dispose();
                }

                if (iClaimantRowId < Int32.MaxValue)
                {
                    sSql.Append("SELECT P.POLICY_NAME, P.POLICY_ID, P.POLICY_LOB_CODE, CVG.COVERAGE_TYPE_CODE, CVG.DED_TYPE_CODE, CVG.SELF_INSURE_DEDUCT, CVG.POLCVG_ROW_ID, PXU.UNIT_ID, PXU.UNIT_TYPE, PXU.POLICY_UNIT_ROW_ID ");
                    sSql.Append("FROM POLICY_X_CVG_TYPE CVG ");
                    sSql.Append("INNER JOIN POLICY_X_UNIT PXU ON CVG.POLICY_UNIT_ROW_ID=PXU.POLICY_UNIT_ROW_ID ");
                    sSql.Append("INNER JOIN POLICY P ON PXU.POLICY_ID=P.POLICY_ID ");
                    sSql.Append("INNER JOIN CLAIM_X_POLICY CXP ON P.POLICY_ID=CXP.POLICY_ID ");
                    sSql.Append("WHERE CXP.CLAIM_ID=");
                    sSql.Append(p_iClaimID);

                    dedInfo = new DedInfo();
                    dictDed = new Dictionary<string, DedInfo>();
                    using (dbReader = DbFactory.ExecuteReader(m_sConnectionString, sSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            dedInfo.sPolicyName = dbReader.GetString("POLICY_NAME");
                            dedInfo.iCoverageType = dbReader.GetInt32("COVERAGE_TYPE_CODE");
                            dedInfo.iDedTypeCode = dbReader.GetInt32("DED_TYPE_CODE");
                            dedInfo.dDedAmt = dbReader.GetDouble("SELF_INSURE_DEDUCT");
                            dedInfo.iPolCvgRowId = dbReader.GetInt32("POLCVG_ROW_ID");
                            dedInfo.iUnitId = dbReader.GetInt32("UNIT_ID");
                            dedInfo.sUnitType = dbReader.GetString("UNIT_TYPE");
                            dedInfo.iPolicyId = dbReader.GetInt32("POLICY_ID");
                            dedInfo.iUnitRowId = dbReader.GetInt32("POLICY_UNIT_ROW_ID");
                            dedInfo.iPolicyLob = dbReader.GetInt32("POLICY_LOB_CODE");

                            sKey = string.Format("{0}|{1}|{2}", dedInfo.iPolicyId, dedInfo.iUnitId + dedInfo.sUnitType, dedInfo.iCoverageType);
                            if (dictDed.ContainsKey(sKey))
                            {
                                if (dictDed[sKey].dDedAmt < dedInfo.dDedAmt)
                                {
                                    dictDed[sKey] = dedInfo;
                                }
                            }
                            else
                            {
                                dictDed.Add(sKey, dedInfo);
                            }
                        }
                    }

                    int iPolicyId, iUnitId, icvgType;
                    string sUnitType = string.Empty;
                    iPolicyId = iUnitId = icvgType = 0;
                    sSql.Remove(0, sSql.Length);
                    sSql.Append("SELECT CPD.POLICY_ID, PXU.UNIT_ID, PXU.UNIT_TYPE, PXC.COVERAGE_TYPE_CODE ");
                    sSql.Append("FROM CLAIM_X_POL_DED CPD ");
                    sSql.Append("INNER JOIN POLICY_X_UNIT PXU ON PXU.POLICY_UNIT_ROW_ID = CPD.POLICY_UNIT_ROW_ID ");
                    sSql.Append("INNER JOIN POLICY_X_CVG_TYPE PXC ON CPD.POLCVG_ROW_ID = PXC.POLCVG_ROW_ID ");
                    sSql.Append("WHERE CPD.CLAIM_ID=");
                    sSql.Append(p_iClaimID);

                    using (dbReader = DbFactory.ExecuteReader(m_sConnectionString, sSql.ToString()))
                    {
                        while (dbReader.Read())
                        {
                            iPolicyId = Convert.ToInt32(string.IsNullOrEmpty(dbReader["POLICY_ID"].ToString()) ? 0 : dbReader["POLICY_ID"]);
                            iUnitId = Convert.ToInt32(string.IsNullOrEmpty(dbReader["UNIT_ID"].ToString()) ? 0 : dbReader["UNIT_ID"]);
                            icvgType = Convert.ToInt32(string.IsNullOrEmpty(dbReader["COVERAGE_TYPE_CODE"].ToString()) ? 0 : dbReader["COVERAGE_TYPE_CODE"]);
                            sUnitType = string.IsNullOrEmpty(dbReader["UNIT_TYPE"].ToString()) ?
                                string.Empty : dbReader["UNIT_TYPE"].ToString();
                            sKey = string.Format("{0}|{1}|{2}", iPolicyId, iUnitId + sUnitType, icvgType);
                            if (dictDed.ContainsKey(sKey))
                            {
                                //data is already in the data table -- No need to add this
                                dictDed.Remove(sKey);
                            }
                        }
                    }

                    if (m_objDataModelFactory == null)
                    {
                        m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                    }

                    foreach (DedInfo item in dictDed.Values)
                    {
                        objClmXPolDed = m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;

                        //objClmXPolDed.ClmXPolDedId = objClmXPolDed.Context.GetNextUID(objClmXPolDed.Table);
                        objClmXPolDed.PolicyId = item.iPolicyId;
                        objClmXPolDed.ClaimId = p_iClaimID;
                        objClmXPolDed.UnitRowId = item.iUnitRowId;
                        objClmXPolDed.PolcvgRowId = item.iPolCvgRowId;
                        objClmXPolDed.DedTypeCode = item.iDedTypeCode;
                        //objClmXPolDed.DedAppliedCode = 0;
                        //objClmXPolDed.AllowPaymentFlag = true;
                        //objClmXPolDed.ExcludeExpenseFlag = true;
                        objClmXPolDed.SirDedAmt = item.dDedAmt;
                        //objClmXPolDed.RemainingAmt = item.dDedAmt;
                        objClmXPolDed.ClaimantEid = iClaimantEid;
                        objClmXPolDed.AddedByUser = "SYSTEM";
                        objClmXPolDed.UpdatedByUser = "SYSTEM";
                        objClmXPolDed.PolicyLobCode = item.iPolicyLob;
                        //objClmXPolDed.DimPercentNum = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DimPercentNum;

                        //sSql.Remove(0, sSql.Length);
                        //sSql.Append("Select TOP 1 C.DATE_OF_CLAIM from CLAIM C ");
                        //sSql.Append("INNER JOIN CLAIM_X_POLICY CXP ON C.CLAIM_ID = CXP.CLAIM_ID ");
                        //sSql.Append("INNER JOIN POLICY P ON CXP.POLICY_ID = P.POLICY_ID ");
                        //sSql.Append(string.Format("WHERE (P.POLICY_LOB_CODE={0} AND P.POLICY_CUST_NUMBER={1}) ",0,0));
                        //sSql.Append(string.Format("OR (P.POLICY_SYMBOL={0} AND P.POLICY_NUMBER) AND P.CLAIM_ID<>{2} ", 0, 0, 0));
                        //sSql.Append("ORDER BY C.DATE_OF_CLAIM DESC ");
                        
                        objClmXPolDed.Save();

                        if (objClmXPolDed != null)
                        {
                            objClmXPolDed.Dispose();
                        }
                    }
                }
            }
        }

        private int GetClaimLOB(int iClaimID)
        {
            int iLob = 0;
             Dictionary<string, object> dictParams = null;
             string sSQL = string.Empty;
             try
             {
                 dictParams = new Dictionary<string, object>();
                 sSQL = string.Format("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID ={0} ", "~CLAIMID~");
                 dictParams.Add("CLAIMID", iClaimID);
                 
              iLob = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSQL, dictParams),m_iClientId);


                 return iLob;
             }
             catch (Exception e)
             {
                 throw e;
             }
             finally
             {
                 dictParams = null;
             }
        }
        /// <summary>
        /// Get Payments for a Reserve when Multiple Coverages per Claim is ON
        /// </summary>
        public XmlDocument GetPaymentsBOB(int p_iClaimID, int p_iClaimantEid, int p_iPolicyCvgRowID, int p_iReserveTypeCode,int p_iCurrType,int p_iRcRowId,int p_iCvgLossRowId)
        {
            XmlElement objXmlElement = null;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlPaymentElement = null;
            XmlAttribute objAttribute = null;
            StringBuilder sbSQL = null;
            DbReader objReader = null;                                             
            LocalCache objCache = null;
            Entity objEntity = null;
            double dAmount = 0;
            // added by Nikhil on 08/04/14. Check Total field added.
            double dCheckTotal = 0;
            // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
            //code needs to be reverted.
            ColLobSettings objLobSettings = null;
            try
            {
                //rupal:start r8 Multicurrency

                objXmlDoc = new XmlDocument();

                objXmlElement = objXmlDoc.CreateElement("Reserves");
                objXmlDoc.AppendChild(objXmlElement);
                                
                int iCurrType = p_iCurrType;
                bool IsBaseCurrTypeSelected = false;
                bool bPaymentType = false;//pgupta93: RMA-7113
                string sPaymentType = string.Empty;//pgupta93: RMA-7113
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimID, m_sConnectionString);
                int iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);
               
                if (p_iCurrType <= 0)
                {
                    XmlElement objCurrencyListElement = null;
                    CreateElement(objXmlDoc.DocumentElement, "CurrencyTypeList", ref objCurrencyListElement);
                    
                    if (iClaimCurrCode > 0)
                    {
                        object sClaimCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iClaimCurrCode);
                        CreateAndSetElement(objCurrencyListElement, "Item", "Claim Currency: " + sClaimCurr.ToString(), iClaimCurrCode.ToString());
                        p_iCurrType = int.Parse(iClaimCurrCode.ToString());
                    }
                    else
                    {
                        IsBaseCurrTypeSelected = true;
                    }
                    if (iBaseCurrCode > 0)
                    {
                        object sBaseCurr = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT SHORT_CODE FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode);
                        CreateAndSetElement(objCurrencyListElement, "Item", "Base Currency: " + sBaseCurr.ToString(), iBaseCurrCode.ToString());
                    }
                }
                else
                {
                    iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                    if (iBaseCurrCode > 0)
                    {
                        if (p_iCurrType == iBaseCurrCode)
                        {
                            IsBaseCurrTypeSelected = true;
                        }
                    }
                }
                //rupal:end

                objXmlDoc = new XmlDocument();

                objLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                objXmlElement = objXmlDoc.CreateElement("Payments");
                // Added by Amitosh for JIRA 9659. Hiding Check total when apply deductibles is false. since check total is not calculated when check is printed from print check module
                //code needs to be reverted.
               objAttribute= objXmlDoc.CreateAttribute("ApplyDeductibles");
               objAttribute.Value = Conversion.ConvertStrToBool(objLobSettings[GetClaimLOB(p_iClaimID)].ApplyDedToPaymentsFlag.ToString()).ToString();
               objXmlElement.Attributes.Append(objAttribute);
                //end amitosh
             //   Conversion.ConvertStrToBool(objLobSettings[GetClaimLOB(p_iClaimID)].ApplyDedToPaymentsFlag.ToString());


                objXmlDoc.AppendChild(objXmlElement);                

                sbSQL = new StringBuilder();
                //sbSQL.Append(" SELECT FUNDS.PAYEE_EID PAYEE_EID, FUNDS.TRANS_NUMBER CHECK_NUMBER, FUNDS.DATE_OF_CHECK DATE_OF_CHECK, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE TRANS_TYPE_CODE,");
                //sbSQL.Append(" SUM(FUNDS_TRANS_SPLIT.AMOUNT) AMOUNT,");
                //sbSQL.Append(" FUNDS.STATUS_CODE STATUS_CODE");
                //sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT ");
                //sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                //sbSQL.Append(" AND FUNDS.CLAIM_ID = " + p_iClaimID.ToString());
                //sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString());
                //sbSQL.Append(" AND FUNDS.POLICY_ID = " + p_iPolicyId.ToString());
                //sbSQL.Append(" AND FUNDS.POLCVG_ROW_ID = " + p_iPolicyCvgRowID.ToString());        
                //sbSQL.Append(" GROUP BY ");
                //sbSQL.Append(" FUNDS.PAYEE_EID, FUNDS.TRANS_NUMBER, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,");
                //sbSQL.Append(" FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS.DATE_OF_CHECK , FUNDS.STATUS_CODE ");
                //sbSQL.Append(" HAVING RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString());	               
                
                //rupal:start, r8 multicurrency
                if (IsBaseCurrTypeSelected)
                {
                    sbSQL.Append(" SELECT FUNDS.PAYEE_EID PAYEE_EID, FUNDS.TRANS_NUMBER CHECK_NUMBER,FUNDS.CTL_NUMBER CONTROL_NUMBER, FUNDS.DATE_OF_CHECK DATE_OF_CHECK, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE TRANS_TYPE_CODE,");
                    sbSQL.Append(" SUM(FUNDS_TRANS_SPLIT.AMOUNT) AMOUNT,");
                    sbSQL.Append(" FUNDS.STATUS_CODE STATUS_CODE, FUNDS.TRANS_ID ");    //Ankit Start : Worked on MITS - 32120
                    sbSQL.Append(" ,FUNDS.VOID_FLAG,FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG");    //RUPAL:MITS 33152 //Added FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG for MITS 33239 
                    sbSQL.Append(" ,FUNDS.STOP_PAY_FLAG,FUNDS.STOP_PAY_DATE");   //Added by Swati Agarwal MITS # 33431   
                    //START -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" ,FUNDS.CHECK_TOTAL");
                    sbSQL.Append(" ,FUNDS.PAYMENT_FLAG");//pgupta93: RMA-7113
                    //End -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT ");
                    sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                    //sbSQL.Append(" AND FUNDS.CLAIM_ID = " + p_iClaimID.ToString());
                    //sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString());
                    // sbSQL.Append(" AND FUNDS.POLICY_ID = " + p_iPolicyId.ToString());
                    //sbSQL.Append(" AND FUNDS.POLCVG_ROW_ID = " + p_iPolicyCvgRowID.ToString());
                    //sbSQL.Append(" AND  FUNDS_TRANS_SPLIT.RC_ROW_ID = " + p_iRC_ROW_ID.ToString ());
                    sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RC_ROW_ID=" + p_iRcRowId.ToString());
                    //sbSQL.Append("  AND  FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString());


                    sbSQL.Append(" GROUP BY ");
                    sbSQL.Append(" FUNDS.PAYEE_EID, FUNDS.TRANS_NUMBER, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,");
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS.DATE_OF_CHECK , FUNDS.STATUS_CODE ");
                    sbSQL.Append(" FUNDS.DATE_OF_CHECK , FUNDS.STATUS_CODE, ");
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.POLCVG_ROW_ID,FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS.TRANS_ID ");  //Ankit Start : Worked on MITS - 32120
                    sbSQL.Append("  FUNDS.TRANS_ID ");  //Ankit Start : Worked on MITS - 32120
                    //HAVING RC_ROW_ID = 1651
                    //sbSQL.Append(" HAVING RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString());	     
                    sbSQL.Append(" ,FUNDS.VOID_FLAG,FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG");    //RUPAL:MITS 33152 //Added FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG for MITS 33239 
                    sbSQL.Append(" ,FUNDS.STOP_PAY_FLAG,FUNDS.STOP_PAY_DATE");   //Added by Swati Agarwal MITS # 33431   
                    //START -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" ,FUNDS.CHECK_TOTAL");
                    sbSQL.Append(" ,FUNDS.PAYMENT_FLAG");//pgupta93: RMA-7113
                    //End -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" , FUNDS.CTL_NUMBER");
                    eNavType = CommonFunctions.NavFormType.None;
                }
                else
                {
                    sbSQL.Append(" SELECT FUNDS.PAYEE_EID PAYEE_EID, FUNDS.TRANS_NUMBER CHECK_NUMBER,FUNDS.CTL_NUMBER CONTROL_NUMBER, FUNDS.DATE_OF_CHECK DATE_OF_CHECK, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE TRANS_TYPE_CODE,");
                    sbSQL.Append(" SUM(FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_AMOUNT) AMOUNT,");
                    sbSQL.Append(" FUNDS.STATUS_CODE STATUS_CODE, FUNDS.TRANS_ID ");    //Ankit Start : Worked on MITS - 32120
                    sbSQL.Append(" ,FUNDS.VOID_FLAG,FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG");    //RUPAL:MITS 33152 //Added FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG for MITS 33239 
                    sbSQL.Append(" ,FUNDS.STOP_PAY_FLAG,FUNDS.STOP_PAY_DATE");   //Added by Swati Agarwal MITS # 33431   
                    //START -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" ,FUNDS.CHECK_TOTAL"); 
                    sbSQL.Append(" ,FUNDS.PAYMENT_FLAG");//pgupta93: RMA-7113
                    //End -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT ");
                    sbSQL.Append(" WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                    //sbSQL.Append(" AND FUNDS.CLAIM_ID = " + p_iClaimID.ToString());
                    //sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString());
                   
                    sbSQL.Append("  AND FUNDS_TRANS_SPLIT.RC_ROW_ID=" + p_iRcRowId.ToString());
                   // sbSQL.Append("  AND  FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString());

                    sbSQL.Append(" GROUP BY ");
                    sbSQL.Append(" FUNDS.PAYEE_EID, FUNDS.TRANS_NUMBER, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,");                    
                    sbSQL.Append(" FUNDS.DATE_OF_CHECK , FUNDS.STATUS_CODE, ");
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.POLCVG_ROW_ID,FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, FUNDS.TRANS_ID ");  //Ankit Start : Worked on MITS - 32120
                    sbSQL.Append("  FUNDS.TRANS_ID ");  //Ankit Start : Worked on MITS - 32120
                    sbSQL.Append(" ,FUNDS.VOID_FLAG,FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG");    //RUPAL:MITS 33152 //Added FUNDS.VOID_DATE,FUNDS.CLEARED_FLAG for MITS 33239
                    sbSQL.Append(" ,FUNDS.STOP_PAY_FLAG,FUNDS.STOP_PAY_DATE");   //Added by Swati Agarwal MITS # 33431   
                    //START -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" ,FUNDS.CHECK_TOTAL");
                    sbSQL.Append(" ,FUNDS.PAYMENT_FLAG");//pgupta93: RMA-7113
                    //End -  added by Nikhil on 08/04/14. Check Total field added.
                    sbSQL.Append(" , FUNDS.CTL_NUMBER");
                    eNavType = CommonFunctions.NavFormType.Reserve;
                }
                //rupal:end
                
//SELECT FUNDS.PAYEE_EID PAYEE_EID, FUNDS.TRANS_NUMBER CHECK_NUMBER, FUNDS.DATE_OF_CHECK DATE_OF_CHECK,
// FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE TRANS_TYPE_CODE, SUM(FUNDS_TRANS_SPLIT.AMOUNT) AMOUNT, FUNDS.STATUS_CODE STATUS_CODE
//  FROM FUNDS, FUNDS_TRANS_SPLIT  WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID  
//  AND FUNDS.CLAIM_ID = 428 AND FUNDS.CLAIMANT_EID = 354 AND FUNDS.POLICY_ID = 31 AND FUNDS.POLCVG_ROW_ID = 51 GROUP BY 
//FUNDS.PAYEE_EID, FUNDS.TRANS_NUMBER, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, FUNDS_TRANS_SPLIT.RC_ROW_ID, FUNDS.DATE_OF_CHECK , FUNDS.STATUS_CODE  HAVING RC_ROW_ID = 1651
	   
                
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    while (objReader.Read())
                    {                                
                        objXmlPaymentElement = objXmlDoc.CreateElement("Payment");
                        objXmlElement.AppendChild(objXmlPaymentElement);

                        
                        if (objCache == null)  // only allocate once and then use for subsequent ones
                            objCache = new LocalCache(m_sConnectionString, m_iClientId);

                        if (m_objDataModelFactory == null)
                        {
                            m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                        } 
                        objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);

                        dAmount = Conversion.ConvertStrToDouble(objReader.GetValue("AMOUNT").ToString());
                        //START -  added by Nikhil on 08/04/14. Check Total field added.
                        dCheckTotal = Conversion.ConvertStrToDouble(objReader.GetValue("CHECK_TOTAL").ToString());
                        //End -  added by Nikhil on 08/04/14. Check Total field added.
                        //pgupta93: RMA-7113 START
                        bPaymentType = Conversion.ConvertStrToBool(objReader.GetValue("PAYMENT_FLAG").ToString());
                        string sPayment = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Payment", m_iClientId));
                        string sCollection = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Collection", m_iClientId));
                        sPaymentType = bPaymentType ? sPayment : sCollection;
                        //pgupta93: RMA-7113 END
                        objXmlPaymentElement.SetAttribute("payeeeid", objReader.GetValue("PAYEE_EID").ToString());
                        objEntity.MoveTo(Conversion.ConvertStrToInteger(objReader.GetValue("PAYEE_EID").ToString()));
                        objXmlPaymentElement.SetAttribute("payeename", objEntity.LastName + "," + objEntity.FirstName);
                        objXmlPaymentElement.SetAttribute("checknumber", objReader.GetValue("CHECK_NUMBER").ToString());
                        //objXmlPaymentElement.SetAttribute("dateofcheck", objReader.GetValue("DATE_OF_CHECK").ToString());
                        objXmlPaymentElement.SetAttribute("dateofcheck",  (Conversion.ToDate((Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CHECK")))).ToShortDateString()));
                        objXmlPaymentElement.SetAttribute("transactiontype", objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objReader.GetValue("TRANS_TYPE_CODE").ToString())));
                        
                        //rupal:r8 multicurrency
                        //objXmlPaymentElement.SetAttribute("amount", String.Format("{0:c}", dAmount));
                        objXmlPaymentElement.SetAttribute("amount", CommonFunctions.ConvertCurrency(p_iClaimID, dAmount, eNavType, m_sConnectionString, m_iClientId));                        
                        //START -  added by Nikhil on 08/04/14. Check Total field added.
                        objXmlPaymentElement.SetAttribute("checktotal", CommonFunctions.ConvertCurrency(p_iClaimID, dCheckTotal, eNavType, m_sConnectionString, m_iClientId));
                        //End -  added by Nikhil on 08/04/14. Check Total field added.
                        objXmlPaymentElement.SetAttribute("statuscode", objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objReader.GetValue("STATUS_CODE").ToString())));
                        //Ankit Start : Worked on MITS - 32120
                        objXmlPaymentElement.SetAttribute("totalpayees", GetTotalPayees(objReader.GetValue("TRANS_ID").ToString()));
                        //Ankit End
                        //rupal Start : Worked on MITS - 33512

                        //pgupta93: RMA-7113 START
                        objXmlPaymentElement.SetAttribute("type", sPaymentType);
                        //pgupta93: RMA-7113 END
                        if (Conversion.ConvertStrToBool(objReader.GetValue("VOID_FLAG").ToString()))
                            //objXmlPaymentElement.SetAttribute("voidflag", "Yes");
                            objXmlPaymentElement.SetAttribute("voidflag", (Conversion.ToDate((Conversion.ConvertObjToStr(objReader.GetValue("VOID_DATE")))).ToShortDateString()));//Change for MITS 33239
                        else
                            objXmlPaymentElement.SetAttribute("voidflag", "No");

                        //Added by Swati agarwal for MITS # 33431
                        if (Conversion.ConvertStrToBool(objReader.GetValue("STOP_PAY_FLAG").ToString()))
                            //objXmlPaymentElement.SetAttribute("voidflag", "Yes");
                            objXmlPaymentElement.SetAttribute("stoppayflag", (Conversion.ToDate((Conversion.ConvertObjToStr(objReader.GetValue("STOP_PAY_DATE")))).ToShortDateString()));
                        else
                            objXmlPaymentElement.SetAttribute("stoppayflag", "No");
                        //end by Swati

                        //Asharma326 MITS 33239 Starts
                        if (Conversion.ConvertStrToBool(objReader.GetValue("CLEARED_FLAG").ToString()))
                            //objXmlPaymentElement.SetAttribute("voidflag", "Yes");
                            objXmlPaymentElement.SetAttribute("clearedflag", (Conversion.ToDate((Conversion.ConvertObjToStr(objReader.GetValue("VOID_DATE")))).ToShortDateString()));//Change for MITS 33239
                        else
                            objXmlPaymentElement.SetAttribute("clearedflag", "No");
                        //Asharma326 MITS 33239 Ends
                        objXmlPaymentElement.SetAttribute("ctrlnumber", objReader.GetValue("CONTROL_NUMBER").ToString());
                    }
                    objReader.Close();

                }
                sbSQL.Remove(0, sbSQL.Length);

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetPaymentsBOB.GenericError", m_iClientId), objException);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                    objEntity = null;
                }
                objXmlElement = null;                
                objXmlPaymentElement = null;
                sbSQL = null;
                objLobSettings = null;            
            }

            //Return the result to calling function
            return objXmlDoc;
        }
        //Ankit Start : Worked on MITS - 32120
        private string GetTotalPayees(string TransID)
        {
            string sSQL = string.Empty;
            string sRetrunString = string.Empty;
            DbReader objReader = null;
            m_sDBType = DbFactory.GetDatabaseType(m_sConnectionString).ToString();//Deb: Added to get the db type
            sSQL = "SELECT FUNDS_X_PAYEE.FUNDS_TRANS_ID, (ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME) TOTAL_PAYEES_NAME ";
            sSQL = sSQL + " FROM ENTITY INNER JOIN FUNDS_X_PAYEE ON ENTITY.ENTITY_ID = FUNDS_X_PAYEE.PAYEE_EID ";
            sSQL = sSQL + " WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID = " + TransID;
            sSQL = sSQL + " ORDER BY FUNDS_X_PAYEE.FUNDS_TRANS_ID ";
            if (string.Equals(m_sDBType, Constants.DB_ORACLE.ToUpper(), StringComparison.OrdinalIgnoreCase))
            {
                sSQL = sSQL.Replace("ISNULL", "nvl");
                sSQL = sSQL.Replace("+", "||");
            }

            using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        if (string.IsNullOrEmpty(sRetrunString))
                            sRetrunString = objReader.GetValue("TOTAL_PAYEES_NAME").ToString().Trim();
                        else
                            sRetrunString = string.Format(sRetrunString + "{0}" + objReader.GetValue("TOTAL_PAYEES_NAME").ToString().Trim(), Environment.NewLine);
                    }
                }
            }

            return sRetrunString;
        }
        //Ankit End

        private void GetOrgEid(long p_lDeptEId, ref long p_lFacEId, ref long p_lLocEId,
                            ref long p_lDivEId, ref long p_lRegEId, ref long p_lOprEId,
                            ref long p_lComEId, ref long p_lCliEId)
        {
            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object

            sSQL = "SELECT FACILITY_EID, LOCATION_EID, DIVISION_EID, REGION_EID, OPERATION_EID, COMPANY_EID, CLIENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID=" + p_lDeptEId;

            try
            {
                //Fetch the Details for this Department
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                {
                    p_lFacEId = Conversion.ConvertObjToInt64(objReader.GetValue("FACILITY_EID"), m_iClientId);
                    p_lLocEId = Conversion.ConvertObjToInt64(objReader.GetValue("LOCATION_EID"), m_iClientId);
                    p_lDivEId = Conversion.ConvertObjToInt64(objReader.GetValue("DIVISION_EID"), m_iClientId);
                    p_lRegEId = Conversion.ConvertObjToInt64(objReader.GetValue("REGION_EID"), m_iClientId);
                    p_lOprEId = Conversion.ConvertObjToInt64(objReader.GetValue("OPERATION_EID"), m_iClientId);
                    p_lComEId = Conversion.ConvertObjToInt64(objReader.GetValue("COMPANY_EID"), m_iClientId);
                    p_lCliEId = Conversion.ConvertObjToInt64(objReader.GetValue("CLIENT_EID"), m_iClientId);
                }

                objReader.Close();
            }
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("CodesListManager.GetOrgEid.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }
        }

        
        /// <summary>
        /// GetLookupXml
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <returns></returns>
        public XmlDocument GetLookupXml(int p_iClaimID, int p_iReserveID)
        {
            XmlDocument objXmlDoc = null;
            Claim objClaim = null;
            Policy objPolicy = null;
            StringBuilder sb = null;
            StringBuilder sbSQL = null;
            string sReserveSQL = string.Empty;
            StringBuilder sbReserveExternal = new StringBuilder();
            int iLOB = 0;
            //LOB settings
            Riskmaster.Settings.ColLobSettings objLOB = null;
            Riskmaster.Settings.LobSettings objLOBSettings = null;
            int iPolicyID = 0;
            LocalCache objCache = null;
            StringBuilder sbReserves = null;
            int iCnt = 0;
            DataSet objDataSet = null, objDataSetN=null;
            string sReserveTypeCode = String.Empty;
            int iCoverageTypeCode = 0;
			int iLossTypeCode = 0;
            int iPolcvgRowId = 0;
            bool bExternalReserve = false;
            bool bIncludeClaimtype = false;
            DataSet objDS = null; //rupal:mits 27459
            DataSet objDataSetSubType = null;
            string sDisabilityTypeCode = String.Empty;
            string sCodeDesc = string.Empty;
            StringBuilder sbDisCat= null;
            long lFacEId = 0;					//Facility Id
            long lLocEId = 0;					//Location Id
            long lDivEId = 0;					//Division Id
            long lRegEId = 0;					//Region Id
            long lOprEId = 0;					//Operation Id
            long lComEId = 0;					//Company Id
            long lCliEId = 0;					//Client Id
            long lDeptEId = 0;                   //Dept Id
            Event objEvent = null;
            DataSet objDataCvgLossLOB = null;
            string sDBType = string.Empty;
            //Add by kuladeep for mits:32382 Start
            int iMonthNow = 0;				//current month
            int iDay = 0;						//current day
            string sToday = string.Empty;		//string for Date

            iMonthNow = DateTime.Now.Month;
            iDay = DateTime.Now.Day;
            sToday = DateTime.Now.Year.ToString();
            SysSettings objSysSettings;
            //for the date string
            if (iMonthNow < 10)
                sToday = sToday + "0" + iMonthNow.ToString();
            else
                sToday = sToday + iMonthNow.ToString();

            if (iDay < 10)
                sToday = sToday + "0" + iDay.ToString();
            else
                sToday = sToday + iDay.ToString();

            //Add by kuladeep for mits:32382 End
            try
            {
                
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimID);
                iLOB = objClaim.LineOfBusCode;

                objEvent  = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                objEvent.MoveTo(objClaim.EventId);
                lDeptEId = objEvent.DeptEid;

                objXmlDoc = new XmlDocument();
                sb= new StringBuilder ();
                sbSQL = new StringBuilder();

                //Instantiate the LOB Settings Class
                objLOB = new Riskmaster.Settings.ColLobSettings(m_sConnectionString, m_iClientId);
                objLOBSettings = objLOB[iLOB];

                string sTableId =string.Empty;

                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                //bExternalReserve = IsIncludeClaimType("RESERVE_TYPE");

                bool bAddDedTypeCode = false;
                Object oData = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT COUNT(*) FROM CLAIM_X_POL_DED WHERE CLAIM_ID = " + p_iClaimID);
                if (Conversion.ConvertObjToInt(oData,m_iClientId) != 0)
                {
                    bAddDedTypeCode = true;
                }
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                if (objLOBSettings != null)
                {
                    if (objLOBSettings.ResByClmType)
                    {
                        sbSQL.Append(" SELECT SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES,CODES ");
                        sbSQL.Append(" WHERE SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + iLOB);
                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode);
                        //igupta3   Mits:31954
                        //sbSQL.Append(" AND CODES.CODE_ID = SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE ");
                        sbSQL.Append(" AND CODES.CODE_ID = SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE AND CODES.DELETED_FLAG=0");
                    }
                    else
                    {
                        sbSQL.Append(" SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES ");
                        sbSQL.Append(" WHERE SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + iLOB);
                        //igupta3   Mits:31954
                        //sbSQL.Append(" AND CODES.CODE_ID = SYS_LOB_RESERVES.RESERVE_TYPE_CODE ");
                        sbSQL.Append(" AND CODES.CODE_ID = SYS_LOB_RESERVES.RESERVE_TYPE_CODE AND CODES.DELETED_FLAG=0");
                    }

                    //Add by kuladeep for mits:32382 Start
                    sbSQL.Append("	AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE IS NULL) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "')) ");

                    //Add by kuladeep for mits:32382 End

                    if (lDeptEId > 0)
                    {
                        GetOrgEid(lDeptEId, ref lFacEId, ref lLocEId, ref lDivEId, ref lRegEId, ref lOprEId,ref lComEId, ref lCliEId);

                        sbSQL.Append("	AND (CODES.ORG_GROUP_EID=" + lDeptEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        sbSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                    }

                    sReserveSQL = sbSQL.ToString();
                }//end if(objLOBSettings != null)               


                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);

                sbReserves = new StringBuilder();

                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                    if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                        {
                            sReserveTypeCode = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["RESERVE_TYPE_CODE"]);
                            //rsharma220 MITS 33736 Start
                            //sbReserves.Append(@"<reserve name=""" + objCache.GetCodeDesc(int.Parse(sReserveTypeCode), this.LanguageCode) + @"""" + @" value=""" + sReserveTypeCode + @""" />");
                            //mkaran2 - [JIRA] (RMA-4110) MITS 37158  
                            sbReserves.Append(@"<reserve name=""" + System.Net.WebUtility.HtmlEncode(objCache.GetCodeDesc(int.Parse(sReserveTypeCode), this.LanguageCode)) + @"""" + @" value=""" + sReserveTypeCode + @""" />");
                        }
                    }

                    sbSQL.Length = 0;
                    //Aman ML Change
					if (iLOB == 243)
                    {
                        //skhare7 Policy interfcae reserve category
                        sbSQL = new StringBuilder();
                        sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                        sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                        sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                        sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'DISABILITY_CATEGORY' ");
                        sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                        //Start PARAG MITS 10269
                        sbSQL.Append(" AND CODES.DELETED_FLAG!=-1 ");

                        sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "DISABILITY_CATEGORY", this.LanguageCode); //Aman ML Change
                        if (objCache == null)  // only allocate once and then use for subsequent ones
                            objCache = new LocalCache(m_sConnectionString, m_iClientId);

                        sbDisCat = new StringBuilder();

                        objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                        if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                        {
                            for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                            {
                                sDisabilityTypeCode = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["CODE_ID"]);
                                sCodeDesc = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["CODE_DESC"]);
                                sbDisCat.Append(@"<disabilitycat name=""" + sCodeDesc + @"""" + @" value=""" + sDisabilityTypeCode + @"""/>");
                            }
                        }

                        sbSQL.Length = 0;

                    }
                sb.Append("<BOB>"); 
                                
                objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                sTableId = objCache.GetTableId("RESERVE_TYPE").ToString();
                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    iPolicyID = objClaimXPolicy.PolicyId;
                    
                    objPolicy.MoveTo(iPolicyID);
                    bIncludeClaimtype = IsIncludeClaimType("RESERVE_TYPE", objPolicy.PolicySystemId.ToString());
                    //bExternalReserve = IsIncludeClaimType("RESERVE_TYPE");

                    //if (objLOBSettings != null)
                    //{
                        //if (objLOBSettings.ResByClmType)
                        //{
                        //    sbSQL.Append(" SELECT SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES,CODES ");
                        //    sbSQL.Append(" WHERE SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + iLOB);
                        //    sbSQL.Append(" AND CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode);
                        //    sbSQL.Append(" AND CODES.CODE_ID = SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE ");
                        //}
                        //else
                        //{
                        //    sbSQL.Append(" SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES ");
                        //    sbSQL.Append(" WHERE SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + iLOB);
                        //    sbSQL.Append(" AND CODES.CODE_ID = SYS_LOB_RESERVES.RESERVE_TYPE_CODE ");
                        //}

                        //if (lDeptEId > 0)
                        //{
                        //    GetOrgEid(lDeptEId, ref lFacEId, ref lLocEId, ref lDivEId, ref lRegEId, ref lOprEId, ref lComEId, ref lCliEId);

                        //    sbSQL.Append("	AND (CODES.ORG_GROUP_EID=" + lDeptEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lFacEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lLocEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lDivEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lRegEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lOprEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lComEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=" + lCliEId);
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID IS NULL ");
                        //    sbSQL.Append("	OR CODES.ORG_GROUP_EID=0) ");
                        //}
                        if (objPolicy.PolicySystemId > 0 && bIncludeClaimtype && !objLOBSettings.ResByClmType)
                        {
                            bExternalReserve = true;
                            sbSQL.Append(sReserveSQL.ToString());
                            sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID =" + objPolicy.PolicySystemId + " AND RMX_TABLE_ID =" + sTableId + " AND CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode+")");
                            
                            sbReserveExternal = new StringBuilder();
                            objDataSetN = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                            if (objDataSetN != null && objDataSetN.Tables[0].Rows.Count > 0)
                            {
                                for (iCnt = 0; iCnt < objDataSetN.Tables[0].Rows.Count; iCnt++)
                                {
                                    sReserveTypeCode = Conversion.ConvertObjToStr(objDataSetN.Tables[0].Rows[iCnt]["RESERVE_TYPE_CODE"]);
                                    //sbReserveExternal.Append(@"<reserve name=""" + objCache.GetCodeDesc(int.Parse(sReserveTypeCode), this.LanguageCode) + @"""" + @" value=""" + sReserveTypeCode + @"""/>");
                                    //mkaran2 - [JIRA] (RMA-4110) MITS 37158  
                                    sbReserveExternal.Append(@"<reserve name=""" + System.Net.WebUtility.HtmlEncode(objCache.GetCodeDesc(int.Parse(sReserveTypeCode), this.LanguageCode)) + @"""" + @" value=""" + sReserveTypeCode + @"""/>");
                                }
                            }
                        }

                    //}              




                     //sb.Append(@"<policy name=""" + objPolicy.PolicyName + @"""" + @" value=""" + objPolicy.PolicyId + @""">");                          
                      sb.Append(@"<policy name=""" + System.Net.WebUtility.HtmlEncode(objPolicy.PolicyName) + @"""" + @" value=""" + objPolicy.PolicyId + @""">"); //mkaran2 - [JIRA] (RMA-4110) MITS 37158
                    //rupal:mits 27459
                        //RUPAL:MITS 27459
                        sbSQL = new StringBuilder();
                        if (objSysSettings.MultiCovgPerClm == -1)
                        {
                            //if (objPolicy.PolicySystemId > 0)
                            //{
                            if (sDBType == Constants.DB_SQLSRVR)
                            {
                                sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, ISNULL(VEHICLE.VEH_DESC, VEHICLE.VIN) Unit");
                            }
                            else if (sDBType == Constants.DB_ORACLE)
                            {
                                sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, nvl(VEHICLE.VEH_DESC, VEHICLE.VIN) Unit");
                            }
                            //}
                        }
                        else
                        {
                            sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, VEHICLE.VIN Unit");
                        }
                        
                        sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                        if (objPolicy.PolicySystemId > 0)
                        {
                            sbSQL = sbSQL.Append(string.Format(" INNER JOIN UNIT_X_CLAIM ON UNIT_X_CLAIM.UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='V' AND POLICY_X_UNIT.POLICY_ID = {0} AND UNIT_X_CLAIM.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                        }
                        sbSQL = sbSQL.Append(" INNER JOIN VEHICLE ON VEHICLE.UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND POLICY_X_UNIT.POLICY_ID= " + objPolicy.PolicyId.ToString());

                        sbSQL = sbSQL.Append(" UNION");
                        sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, PROPERTY_UNIT.PIN Unit"
                            );
                        sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                        if (objPolicy.PolicySystemId > 0)
                        {
                            sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_PROPERTYLOSS ON CLAIM_X_PROPERTYLOSS.PROPERTY_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID={0} AND CLAIM_X_PROPERTYLOSS.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                        }
                        sbSQL = sbSQL.Append(" INNER JOIN PROPERTY_UNIT ON PROPERTY_UNIT.PROPERTY_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID= " + objPolicy.PolicyId.ToString());

                        
                        //rupal:start, policy system interface changes, as of now site and other unit will be available for policy system interface only                        
                        //tanwar2 - Policy Download from staging
                        //if (objPolicy.PolicySystemId > 0)
                        if (objPolicy.PolicySystemId > 0)
                        {
                            //tanwar2 - Policy Download from staging
                            //if (iLOB == 243)//site unit will be available for WC only
                            if (iLOB == 243)
                            {
                                //SITE UNIT - S
                                sbSQL = sbSQL.Append(" UNION");
                                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, SITE_UNIT.NAME Unit");
                                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                                //tanwar2 - Policy Download from staging - added if condition
                                if (objPolicy.PolicySystemId > 0)
                                {
                                sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_SITELOSS ON CLAIM_X_SITELOSS.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_SITELOSS.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                                }
                                sbSQL = sbSQL.Append(" INNER JOIN SITE_UNIT ON SITE_UNIT.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID= " + objPolicy.PolicyId.ToString());
                            }
                            //OTHER UNIT - SU
                            sbSQL = sbSQL.Append(" UNION");
                            //changes for MITS 31997 start
                            if(sDBType ==Constants.DB_SQLSRVR)
                                //dbisht6 start for mits 35655
                                //    sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,  (ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'')) Unit");
                                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) Unit");
                            //dbisht6 end

                            else if(sDBType ==Constants.DB_ORACLE)
                                //dbisht6 start for mits 35655
                                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,POINT_UNIT_DATA.STAT_UNIT_NUMBER || ' - ' || nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,''))))) Unit");
                                //sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,(nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,'')) Unit");
                            //dbisht6 end
                            //changes for MITS 31997 end
                            sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                            sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_OTHERUNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='SU' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_OTHERUNIT.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                            sbSQL = sbSQL.Append(" INNER JOIN OTHER_UNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=OTHER_UNIT.OTHER_UNIT_ID");
                            sbSQL = sbSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID=OTHER_UNIT.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");

                            //dbisht6 start mits of 35655
                            sbSQL = sbSQL.Append(" INNER JOIN POINT_UNIT_DATA ON	OTHER_UNIT.OTHER_UNIT_ID = POINT_UNIT_DATA.UNIT_ID and POINT_UNIT_DATA.UNIT_TYPE = OTHER_UNIT.UNIT_TYPE ");
                          
                            //dbisht6 end

                        }
                        //rupal:end
                        string sCoverageText = string.Empty;
                        using (objDS = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId))
                        {
                            if (objDS != null && objDS.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < objDS.Tables[0].Rows.Count; i++)
                                {
                                    int iPolicyUnitRowId = Conversion.ConvertObjToInt(objDS.Tables[0].Rows[i]["PolUnitRowId"], m_iClientId);
                                    //sb.Append(@"<unit name=""" + objDS.Tables[0].Rows[i]["Unit"].ToString() + @"""" + @" value=""" + iPolicyUnitRowId + @""">");
                                    sb.Append(@"<unit name=""" + System.Net.WebUtility.HtmlEncode(objDS.Tables[0].Rows[i]["Unit"].ToString()) + @"""" + @" value=""" + iPolicyUnitRowId + @""">"); //mkaran2 - [JIRA] (RMA-4110) MITS 37158  
                                    sbSQL = new StringBuilder();
                                    if (bAddDedTypeCode)
                                    {
                                        sbSQL.Append("SELECT PCT.COVERAGE_TYPE_CODE , PCT.POLCVG_ROW_ID,COVERAGE_TEXT,CVG_SEQUENCE_NO, CPD.DED_TYPE_CODE FROM POLICY_X_CVG_TYPE PCT, CLAIM_X_POL_DED CPD WHERE CPD.POLCVG_ROW_ID = PCT.POLCVG_ROW_ID  AND CPD.CLAIM_ID = " + p_iClaimID + "  AND  PCT.POLICY_UNIT_ROW_ID = " + iPolicyUnitRowId);
                                    }
                                    else
                                    {
                                        sbSQL.Append("SELECT PCT.COVERAGE_TYPE_CODE , PCT.POLCVG_ROW_ID,COVERAGE_TEXT,CVG_SEQUENCE_NO FROM POLICY_X_CVG_TYPE PCT WHERE PCT.POLICY_UNIT_ROW_ID = " + iPolicyUnitRowId);
                                    }
                                    using (objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId))
                                    {
                                        if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                                        {
                                            for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                                            {
                                                iCoverageTypeCode = Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[iCnt]["COVERAGE_TYPE_CODE"], m_iClientId);
                                                iPolcvgRowId = Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"], m_iClientId);

                                                if (objPolicy.PolicySystemId > 0)
                                                {
                                                    sCoverageText = GetCoverageText(iPolicyUnitRowId, Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"],m_iClientId), iCoverageTypeCode);
                                                    sCoverageText = System.Net.WebUtility.HtmlEncode(sCoverageText); //mkaran2 - [JIRA] (RMA-4110) MITS 37158                                                     
                                                        if (bAddDedTypeCode)
                                                        {
                                                            sb.Append(@"<coverage name=""" + sCoverageText + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"]) + "#" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["CVG_SEQUENCE_NO"]) + "#" + objDataSet.Tables[0].Rows[iCnt]["DED_TYPE_CODE"].ToString() + @""">");
                                                        }
                                                        else
                                                        {
                                                    sb.Append(@"<coverage name=""" + sCoverageText + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"]) + "#" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["CVG_SEQUENCE_NO"]) + @""">");
                                                }
                                                        //aaggarwal29: if policysystemid <=0, we dont have deductibles applicable so we dont need to consider that change and add code there .
                                                    }
                                                else
                                                {
                                                    //mkaran2 - [JIRA] (RMA-4110) MITS 37158 : start
                                                    sCoverageText = System.Net.WebUtility.HtmlEncode(objCache.GetCodeDesc(iCoverageTypeCode, this.LanguageCode));
                                                    sb.Append(@"<coverage name=""" + sCoverageText + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"]) + @""">");
                                                    //mkaran2 - end
                                                    //sb.Append(@"<coverage name=""" + objCache.GetCodeDesc(iCoverageTypeCode, this.LanguageCode) + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"]) + @""">");                                                    
                                                }
                                                //sb.Append(sbReserves.ToString());
												if ( bExternalReserve)
                                                 {
                                                     sb.Append(sbReserveExternal.ToString());
                                                 }
                                                 else
                                                 {
                                                     sb.Append(sbReserves.ToString());
                                                 }

                                                //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
                                                if (!int.Equals(p_iReserveID, 0))
                                                {
                                                    //Aman ML Change
                                                    sbSQL = new StringBuilder();
                                                    //sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                                                    //sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                                                    //sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                                                    //sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_SUB_TYPE' ");
                                                    //sbSQL.Append(" AND CODES.DELETED_FLAG != -1 ");
                                                    //sbSQL.Append(" And CODES.RELATED_CODE_ID IN (" + p_iReserveID + ") ");
                                                    //sbSQL.Append(" Order By CODES.RELATED_CODE_ID, CODES_TEXT.CODE_DESC ");
                                                    sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID, CODES.RELATED_CODE_ID ");
                                                    sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                                                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                                                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_SUB_TYPE' ");
                                                    sbSQL.Append(" AND CODES.DELETED_FLAG != -1 AND CODES_TEXT.LANGUAGE_CODE = 1033");
                                                    sbSQL.Append(" AND CODES.RELATED_CODE_ID IN (" + p_iReserveID + ") ");
                                                    sbSQL.Append(" ORDER BY CODES.RELATED_CODE_ID, CODES_TEXT.CODE_DESC ");
                                                    sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_SUB_TYPE", this.LanguageCode);//Aman ML Change --end
                                                    objDataSetSubType = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);

                                                    if (objDataSetSubType != null && objDataSetSubType.Tables[0].Rows.Count > 0)
                                                    {
                                                        for (int iCount = 0; iCount < objDataSetSubType.Tables[0].Rows.Count; iCount++)
                                                        {
                                                            //sb.Append(@"<reservesubtype name=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount]["CODE_DESC"]) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount]["CODE_ID"]) + @""" />");
                                                            //sb.Append(@"<reservesubtype name=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][1]) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][2]) + @""" />"); //Aman ML Change                                                                                                                        
                                                            //mkaran2 - [JIRA] (RMA-4110) MITS 37158  : Start                                                            
                                                            sb.Append(@"<reservesubtype name=""" + System.Net.WebUtility.HtmlEncode(Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][1])) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][2]) + @""" />");
                                                        }
                                                    }
                                                }
                                                //Ankit End

                                                if (iLOB != 243)
                                                {
                                                    sbSQL = new StringBuilder();
                                                    //Ankit Start : Worked on MITS - 34657
                                                    //spahariya MITS 31978 to avoid dups loss type for mu;ltiple reserve types 
                                                    //sbSQL.Append("SELECT POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE,LOSS_CODE  FROM CVG_LOSS_LOB_MAPPING,POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =CVG_LOSS_LOB_MAPPING.CVG_TYPE_CODE AND POLICY_LOB=" + objPolicy.PolicyLOB + " AND POLCVG_ROW_ID = " + iPolcvgRowId);
                                                    //sbSQL.Append("SELECT DISTINCT POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE,LOSS_CODE  FROM CVG_LOSS_LOB_MAPPING,POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =CVG_LOSS_LOB_MAPPING.CVG_TYPE_CODE AND POLICY_LOB=" + objPolicy.PolicyLOB + " AND POLCVG_ROW_ID = " + iPolcvgRowId);
                                                    //using (objDataCvgLossLOB = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString()))
                                                    //{
                                                    //    if (objDataCvgLossLOB != null && objDataCvgLossLOB.Tables[0].Rows.Count > 0)
                                                    //    {
                                                    //        for (int iCounter = 0; iCounter < objDataCvgLossLOB.Tables[0].Rows.Count; iCounter++)
                                                    //        {
                                                    //            iLossTypeCode = Conversion.ConvertObjToInt(objDataCvgLossLOB.Tables[0], m_iClientId.Rows[iCounter]["LOSS_CODE"]);
                                                    //            //    iPolcvgRowId = Conversion.ConvertObjToInt(objDataSet.Tables[0], m_iClientId.Rows[iCnt]["POLCVG_ROW_ID"]);
                                                    //            sb.Append(@"<loss name=""" + objCache.GetCodeDesc(iLossTypeCode, this.LanguageCode) + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataCvgLossLOB.Tables[0].Rows[iCounter]["LOSS_CODE"]) + @""">");
                                                    //            //rsharma220 MITS 33736 End
                                                    //            //sb.Append(sbReserves.ToString());

                                                    System.Collections.Generic.Dictionary<string, string> dictParams = null;
                                                    // Retrofitted by Pshekhawat MITS 36013 06052014 - Start
                                                    string sSQLFormatString = string.Empty;
                                                   
                                                        //sbSQL.Append("SELECT POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE,LOSS_CODE  FROM CVG_LOSS_LOB_MAPPING,POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =CVG_LOSS_LOB_MAPPING.CVG_TYPE_CODE AND POLICY_LOB=" + objPolicy.PolicyLOB + " AND POLCVG_ROW_ID = " + iPolcvgRowId);

                                                        //tanwar2 - Policy download from staging - end
                                                    dictParams = new System.Collections.Generic.Dictionary<string, string>();
                                                    dictParams.Add("POLICYLOB", objPolicy.PolicyLOB.ToString());
                                                    dictParams.Add("POLCVGROWID", iPolcvgRowId.ToString());
                                                    dictParams.Add("POLICYSYSTEMID", objPolicy.PolicySystemId.ToString());

                                                    sbSQL.Append(" SELECT DISTINCT PXCT.COVERAGE_TYPE_CODE,CLLM.LOSS_CODE");
                                                    sbSQL.Append(" FROM CVG_LOSS_LOB_MAPPING CLLM");
                                                    sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE PXCT ON PXCT.COVERAGE_TYPE_CODE = CLLM.CVG_TYPE_CODE");
                                                    sbSQL.Append(" WHERE POLICY_LOB={0}");
                                                    sbSQL.Append(" AND POLCVG_ROW_ID={1}");
                                                    sbSQL.Append(" AND CLLM.POLICY_SYSTEM_ID={2}");

                                                        sSQLFormatString = string.Format(sbSQL.ToString(), "~POLICYLOB~", "~POLCVGROWID~", "~POLICYSYSTEMID~");
                                                    
                                                    using (DbReader objReaderCvgLossLob = DbFactory.ExecuteReader(m_sConnectionString, sSQLFormatString, dictParams))
                                                    {
                                                    // Retrofitted by Pshekhawat MITS 36013 06052014 - End
                                                        if (objReaderCvgLossLob != null)
                                                        {
                                                            while(objReaderCvgLossLob.Read())
                                                            {
                                                                iLossTypeCode = Convert.ToInt32(objReaderCvgLossLob.GetValue("LOSS_CODE"));
                                                                //sb.Append(@"<loss name=""" + objCache.GetCodeDesc(iLossTypeCode, this.LanguageCode) + @"""" + @" value=""" + objReaderCvgLossLob.GetValue("LOSS_CODE").ToString() + @""">");
                                                                //mkaran2 - [JIRA] (RMA-4110) MITS 37158                                                                                                                                
                                                                sb.Append(@"<loss name=""" + System.Net.WebUtility.HtmlEncode(objCache.GetCodeDesc(iLossTypeCode, this.LanguageCode)) + @"""" + @" value=""" + objReaderCvgLossLob.GetValue("LOSS_CODE").ToString() + @""">");
                                                                //Ankit End
                                                                //sb.Append(sbReserves.ToString());
																if ( bExternalReserve)
                                                                {
                                                                    sb.Append(sbReserveExternal.ToString());
                                                                }
                                                                else
                                                                {
                                                                    sb.Append(sbReserves.ToString());
                                                                }

                                                                //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
                                                                if (objDataSetSubType != null && objDataSetSubType.Tables[0].Rows.Count > 0)
                                                                {
                                                                    for (int iCount = 0; iCount < objDataSetSubType.Tables[0].Rows.Count; iCount++)
                                                                    {
                                                                        //sb.Append(@"<reservesubtype name=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][1]) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount]["CODE_ID"]) + @""" />");
                                                                        //mkaran2 - [JIRA] (RMA-4110) MITS 37158                                                                                                
                                                                        sb.Append(@"<reservesubtype name=""" + System.Net.WebUtility.HtmlEncode(Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][1])) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount]["CODE_ID"]) + @""" />");
                                                                    }
                                                                }
                                                                //Ankit End

                                                                sb.Append("</loss>");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    
                                                    sb.Append(sbDisCat.ToString());
                                                
                                                
                                                }
                                                sb.Append("</coverage>");
                                            }
                                        }
                                    }
                                    sb.Append("</unit>");
                                    sbSQL.Length = 0;
                                }
                            }
                        }
                        sb.Append("</policy>");
                }

                sb.Append("</BOB>");
                string sXml = Utilities.AddTags(sb.ToString());
                sXml = System.Net.WebUtility.HtmlEncode(sXml);
                string sFinalXML = "<ReserveValues>" + sXml + "</ReserveValues>";

                objXmlDoc.LoadXml(sFinalXML);
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetLookupXml.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                    objPolicy = null;
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                    objEvent = null;
                }
                if (sb != null)
                {                    
                    sb = null;
                }
                if (sbSQL != null)
                {                    
                    sbSQL = null;
                }
                if (sbReserves != null)
                {
                    sbReserves = null;
                }
                if (sbReserveExternal != null)
                {
                    sbReserveExternal = null;
                }
                if (objLOB != null)
                {                 
                    objLOB = null;
                }
                if (objLOBSettings != null)
                {
                    objLOBSettings = null;
                }
                if (objDataSet != null)
                {
                    objDataSet = null;
                }
                if (objDataCvgLossLOB != null)
                {
                    objDataCvgLossLOB = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                //rupal:mits 27459
                if (objDS != null)
                {
                    objDS = null;
                }
                //rupal

                //rupal:mits 30187
                if (objDataSetSubType != null)
                {
                    objDataSetSubType = null;
                }
				 if (objDataSetN != null)
                 {
                     objDataSetN = null;
                 }
                //rupal
            }

            //Return the result to calling function
            return objXmlDoc;
        }





        /// <summary>
        /// Add Reserve
        /// </summary>
        /// <param name="p_iPolicyID"></param>
        /// <param name="p_iCoverageID"></param>
        /// <param name="p_iReserveTypeCode"></param>
        /// <param name="p_dAmount"></param>
        /// <returns></returns>
        public XmlDocument AddReserve(ref System.Collections.Hashtable p_objScriptErrOut, int p_iClaimID, int p_iClaimantEID, int p_iPolicyID, int p_iCoverageID, int p_iReserveTypeCode, int p_iAdjusterEID, double p_dAmount, int iStatusCode, string sReason, int p_iLossTypeCode, int p_idisabilityCode, int p_iReserveCat, out int p_iRcRowID)//30380
        {
            XmlDocument objXmlDoc = null;
            //StringBuilder sbSQL = null;
            ReserveCurrent objReserveCurrent = null;
            LocalCache objCache = null;
            //akaur9 MITS 27907 ---start
            XmlElement objElement = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            //akaur9 MITS 27907 ---End
            //start:added by nitin goel, MITS 30910,02/05/2013
            ColLobSettings objColLobSettings = null;
            LobSettings objLobSettings = null;
            int iLOB = 0;
            StringBuilder sbSQL = null;
            //end: added by nitin goel
            try
            {
                objXmlDoc = new XmlDocument();

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);

                objReserveCurrent.ClaimId = p_iClaimID;
                objReserveCurrent.ClaimantEid = p_iClaimantEID;
                //objReserveCurrent.PolicyId = p_iPolicyID;
                objReserveCurrent.CoverageId = p_iCoverageID;
                objReserveCurrent.ReserveTypeCode = p_iReserveTypeCode;
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                objReserveCurrent.AssignAdjusterEid = p_iAdjusterEID;
                //Ankit End
                objReserveCurrent.DateEntered = Conversion.ToDbDate(DateTime.Now);
                //Deb Multi Currency
                //objReserveCurrent.ReserveAmount = p_dAmount;
                // bsharma33 RMA-13089 related to RMA-3887 starts
                objReserveCurrent.ClaimCurrReserveAmt = p_dAmount; 
                //int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimID, m_sConnectionString);
                //if (iClaimCurrCode > 0)
                //{
                //    //double dExhRateClaimToBase = objReserveCurrent.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType));
                //    double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                //    objReserveCurrent.ReserveAmount = p_dAmount * dExhRateClaimToBase;
                //    objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                //}
                //else
                //{
                //    objReserveCurrent.ReserveAmount = p_dAmount;
                //}
                // bsharma33 RMA-13089 related to RMA-3887 ends
                //Deb Multi Currency
                objReserveCurrent.sUpdateType = "Reserves";
                //objReserveCurrent.iUserId=userid;
                objReserveCurrent.iUserId = m_userLogin.UserId;//Added by Amitosh for R8 enhancement of Supervisory approval
                objReserveCurrent.iGroupId = m_userLogin.GroupId;//Added by Amitosh for R8 enhancement of Supervisory approval
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);

                objReserveCurrent.EnteredByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                //objReserveCurrent.ResStatusCode = objCache.GetCodeId("O", "RESERVE_STATUS");
                objReserveCurrent.ResStatusCode = iStatusCode;
                objReserveCurrent.Reason = sReason;
                objReserveCurrent.iCoverageId = p_iCoverageID;
                objReserveCurrent.iLossTypecode = p_iLossTypeCode;
                objReserveCurrent.iDisabilityCode = p_idisabilityCode;
                objReserveCurrent.ReserveCategory = p_iReserveCat;
                //rupal:start, first and final, mits 33043
                objReserveCurrent.IsFirstFinalReserve = 0;//when a reserve is manually added, this feild will always contain zero which means the reserve is not first & final
                //rupal:end

                //skhare7 Reserve Listing check Validation
                int iRcRowId = CommonFunctions.GetRCRowId(m_sConnectionString, p_iClaimID, p_iClaimantEID, 0, p_iReserveTypeCode, p_iCoverageID, p_iLossTypeCode, true, m_iClientId);
                if (iRcRowId > 0)
                {
                    throw new RMAppException(Globalization.GetString("ReserveFunds.AddReserves.ExistingReserve", m_iClientId));
                }
			    //Start:added by Nitin goel, MITS 30910,02/05/2013
                sbSQL = new StringBuilder();
                sbSQL.Append(" SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID);

                using (DbReader rdr = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    rdr.Read();
                    iLOB = Conversion.ConvertObjToInt(rdr.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                }
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                objLobSettings = objColLobSettings[iLOB];

                if ((objReserveCurrent.ReserveTypeCode == objLobSettings.DedRecReserveType) && m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm !=0 && objLobSettings.ApplyDedToPaymentsFlag==-1)
                {
                    objReserveCurrent.AddedByUser = "SYSTEM";
                    objReserveCurrent.UpdatedByUser = "SYSTEM";
                }
                    //End:added by Nitin goel
                objReserveCurrent.Save();

                p_iRcRowID = objReserveCurrent.RcRowId;
                //akaur9 MITS 27907 --Start
                if (objReserveCurrent.ResStatusCode != 0)
                {
                    objCache.GetCodeInfo(objReserveCurrent.ResStatusCode, ref sShortCode, ref sCodeDesc);
                    objElement = objXmlDoc.CreateElement("ReserveStatus");
                    objElement.SetAttribute("Status", sShortCode);
                    objElement.InnerText = sShortCode + " - " + sCodeDesc;
                    objXmlDoc.AppendChild(objElement);
                }

                //akaur9 MITS 27907 --End
                //MITS # 34609 tanwar2 - Add validation warnings - start 
                foreach (System.Collections.DictionaryEntry objWarnings in objReserveCurrent.Context.ScriptWarningErrors)
                {
                    p_objScriptErrOut.Add(p_objScriptErrOut.Count + 1, objWarnings.Value.ToString());
                }
                //MITS # 34609 tanwar2 - Add validation warnings - end
            }
            //rupal:start, MITS 27373
            catch (RMAppException p_objEx)
            {
                foreach (System.Collections.DictionaryEntry objErrors in objReserveCurrent.Context.ScriptValidationErrors)//MITS 30380 starts
                    p_objScriptErrOut.Add("ValidationScriptError", objErrors.Value.ToString());//MITS 30380 ends
                throw p_objEx;
            }//rupal:end
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.AddReserve.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objReserveCurrent == null)
                {
                    objReserveCurrent.Dispose();
                    objReserveCurrent = null;
                }
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }

            return objXmlDoc;

        }

        /// <summary>
        /// MITS:34082
        /// Add Reserve
        /// </summary>
        /// <param name="p_iPolicyID"></param>
        /// <param name="p_iCoverageID"></param>
        /// <param name="p_iReserveTypeCode"></param>
        /// <param name="p_dAmount"></param>
        /// <param name="p_ReservecurrencyType"></param>
        /// <returns></returns>
        // //tanwar2 - Changed statusCode to a refernce type
        //public XmlDocument AddReserve(ref System.Collections.Hashtable p_objScriptErrOut, int p_iClaimID, int p_iClaimantEID, int p_iPolicyID, int p_iCoverageID, int p_iReserveTypeCode, int p_iAdjusterEID, double p_dAmount, int iStatusCode, string sReason, int p_iLossTypeCode, int p_idisabilityCode, int p_iReserveCat, string p_ReservecurrencyType, out int p_iRcRowID)
        public XmlDocument AddReserve(ref System.Collections.Hashtable p_objScriptErrOut, int p_iClaimID, int p_iClaimantEID, int p_iPolicyID, int p_iCoverageID, int p_iReserveTypeCode, int p_iAdjusterEID, double p_dAmount, ref int iStatusCode, string sReason, int p_iLossTypeCode, int p_idisabilityCode, int p_iReserveCat, string p_ReservecurrencyType, out int p_iRcRowID, bool p_bForceReserveStatus = false)
        {
            //MITS:34082 MultiCurrency START
            SysSettings objSysSettings = null;
            objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
            //MITS:34082 MultiCurrency END
            XmlDocument objXmlDoc = null;
            //StringBuilder sbSQL = null;
            ReserveCurrent objReserveCurrent = null;
            LocalCache objCache = null;
            //akaur9 MITS 27907 ---start
            XmlElement objElement = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            //akaur9 MITS 27907 ---End
            try
            {
                objXmlDoc = new XmlDocument();

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);

                objReserveCurrent.ClaimId = p_iClaimID;
                objReserveCurrent.ClaimantEid = p_iClaimantEID;
                //objReserveCurrent.PolicyId = p_iPolicyID;
                objReserveCurrent.CoverageId = p_iCoverageID;
                objReserveCurrent.ReserveTypeCode = p_iReserveTypeCode;
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                objReserveCurrent.AssignAdjusterEid = p_iAdjusterEID;
                //Ankit End
                objReserveCurrent.DateEntered = Conversion.ToDbDate(DateTime.Now);
                //Deb Multi Currency
                //objReserveCurrent.ReserveAmount = p_dAmount;
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimID, m_sConnectionString);
                //MITS:34082 MultiCurrency START
                int iReserveCurrCode = 0;
                if (objSysSettings.UseMultiCurrency != 0)
                    iReserveCurrCode = CommonFunctions.iGetReserveCurrencyCode(p_ReservecurrencyType, m_sConnectionString, m_iClientId);
                else
                    iReserveCurrCode = iClaimCurrCode;
                objReserveCurrent.iReserveCurrCode = iReserveCurrCode;
                //MITS:34082 MultiCurrency END
                //bsharma33 RMA-3887 Reserve Supplemetals Starts
                objReserveCurrent.ClaimCurrReserveAmt = p_dAmount;
                //if (iClaimCurrCode > 0)
                //{
                //    //double dExhRateClaimToBase = objReserveCurrent.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType));
                //    double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                //    //MITS:34082 MultiCurrency START
                //    double dExcRateReserveToClaim = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, iClaimCurrCode, m_sConnectionString, m_iClientId);
                //    double dExcRateReserveToBase = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                //    double dExcRateBaseToReserve = CommonFunctions.ExchangeRateSrc2Dest(objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, iReserveCurrCode, m_sConnectionString, m_iClientId);
                //    objReserveCurrent.dReserveToClaimCurRate = dExcRateReserveToClaim;
                //    objReserveCurrent.dReserveToBaseCurRate = dExcRateReserveToBase;
                //    objReserveCurrent.dReserveCurReserveAmount = p_dAmount;
                //    objReserveCurrent.ReserveAmount = p_dAmount * dExcRateReserveToBase;
                //    objReserveCurrent.dBaseToReserveCurrRate = dExcRateBaseToReserve;
                //    //objReserveCurrent.ReserveAmount = p_dAmount * dExhRateClaimToBase;
                //    //MITS:34082 MultiCurrency END
                //    objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                //}
                //else
                //{
                //    objReserveCurrent.ReserveAmount = p_dAmount;
                //}
                //bsharma33 RMA-3887 Reserve Supplemetals ends
                //Deb Multi Currency
                objReserveCurrent.sUpdateType = "Reserves";
                //objReserveCurrent.iUserId=userid;
                objReserveCurrent.iUserId = m_userLogin.UserId;//Added by Amitosh for R8 enhancement of Supervisory approval
                objReserveCurrent.iGroupId = m_userLogin.GroupId;//Added by Amitosh for R8 enhancement of Supervisory approval
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);

                objReserveCurrent.EnteredByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                //objReserveCurrent.ResStatusCode = objCache.GetCodeId("O", "RESERVE_STATUS");
                objReserveCurrent.ResStatusCode = iStatusCode;
                //tanwar2 - JIRA: RMA-7205 - DeductibleImplementation - start
                if (p_bForceReserveStatus)
                {
                    objReserveCurrent.iForcedReserveStatusCode = iStatusCode;
                }
                //tanwar2 - JIRA: RMA-7205 - DeductibleImplementation - end
                objReserveCurrent.Reason = sReason;
                objReserveCurrent.iCoverageId = p_iCoverageID;
                objReserveCurrent.iLossTypecode = p_iLossTypeCode;
                objReserveCurrent.iDisabilityCode = p_idisabilityCode;
                objReserveCurrent.ReserveCategory = p_iReserveCat;
                objReserveCurrent.iReserveCurrencyCode = iReserveCurrCode;//MITS:34082 MultiCurrency
                //rupal:start, first and final, mits 33043
                objReserveCurrent.IsFirstFinalReserve = 0;//when a reserve is manually added, this feild will always contain zero which means the reserve is not first & final
                //rupal:end

                //skhare7 Reserve Listing check Validation
                int iRcRowId = CommonFunctions.GetRCRowId(m_sConnectionString, p_iClaimID, p_iClaimantEID, 0, p_iReserveTypeCode, p_iCoverageID, p_iLossTypeCode, true, m_iClientId);
                if (iRcRowId > 0)
                {
                    throw new RMAppException(Globalization.GetString("ReserveFunds.AddReserves.ExistingReserve", m_iClientId));

                }
                objReserveCurrent.Save();

                p_iRcRowID = objReserveCurrent.RcRowId;
                //akaur9 MITS 27907 --Start
                if (objReserveCurrent.ResStatusCode != 0)
                {
                    objCache.GetCodeInfo(objReserveCurrent.ResStatusCode, ref sShortCode, ref sCodeDesc);
                    objElement = objXmlDoc.CreateElement("ReserveStatus");
                    objElement.SetAttribute("Status", sShortCode);
                    objElement.InnerText = sShortCode + " - " + sCodeDesc;
                    objXmlDoc.AppendChild(objElement);
                }

                //akaur9 MITS 27907 --End

                //tanwar2 - JIRA: RMA-7205 - start
                //iStatusCode is now of reference type
                //This lets the calling method know the final status of the reserve
                iStatusCode = objReserveCurrent.ResStatusCode;
                
                //tanwar2 - JIRA: RMA-7205 - end

                //MITS # 34609 tanwar2 - Add validation warnings - start 
                foreach (System.Collections.DictionaryEntry objWarnings in objReserveCurrent.Context.ScriptWarningErrors)
                {
                    p_objScriptErrOut.Add(p_objScriptErrOut.Count + 1, objWarnings.Value.ToString());
                }
                //MITS # 34609 tanwar2 - Add validation warnings - end
            }
            //rupal:start, MITS 27373
            catch (RMAppException p_objEx)
            {
                foreach (System.Collections.DictionaryEntry objErrors in objReserveCurrent.Context.ScriptValidationErrors)//MITS 30380 starts
                    p_objScriptErrOut.Add("ValidationScriptError", objErrors.Value.ToString());//MITS 30380 ends
                throw p_objEx;
            }//rupal:end
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.AddReserve.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objReserveCurrent == null)
                {
                    objReserveCurrent.Dispose();
                    objReserveCurrent = null;
                }
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objSysSettings = null;//MITS:34082 MultiCurrency
            }

            return objXmlDoc;

        }


        /// tanwar2/nitin goel - mits 30910 changes - 05/26/2014; Function created to handle  the case of different pol coverage seq
        /// Creates the recovery reserve for the claim, policy
        /// </summary>
        /// <param name="p_iClaimID">Claim Id</param>
        /// <param name="p_iPolicyID">Policy Id</param>
        /// <param name="p_iCoverageID">Coverage Id</param>
        /// <param name="p_idisabilityCode">Disability Type Code</param>
        //public void AddRecoveryReserve(int p_iClaimID, int p_iClaimantEID, int p_iPolicyID, int p_iCoverageID, double p_dAmount, int p_iLossTypeCode, int p_idisabilityCode, int p_iAssociatedReserveType, int p_iAdjusterEID, string sReserveCurrencyType)
        public void AddRecoveryReserve(int p_iClaimID, int p_iClaimantEID, int p_iPolicyID, int p_iCoverageID, double p_dAmount, int p_iLossTypeCode, int p_idisabilityCode, int p_iAssociatedReserveType, int p_iAdjusterEID, string sReserveCurrencyType, int p_iReserveStatus)
        {
            int iRecoveryReserveTypeCode;
            int iRecoveryTransTypeCode;
            int iValidateRecResExist;
            int iCoverageLossRowId;
            int iLossTypeCode;
            string sReason;
            bool bUpdateRecovery;
            double dAmount;
            int iStatusCode;
            int iReserveCat;
            int iClaimantEid;
            int iDimTypeCode;
            bool bSuccess;
            StringBuilder sbSQL = null;
            Claim objClaim = null;
            int iTempStatus = 0;
            iRecoveryReserveTypeCode = 0;
            sReason = string.Empty;
            bUpdateRecovery = false;
            iValidateRecResExist = 0;
            iCoverageLossRowId = 0;
            dAmount = 0d;
            iStatusCode = 0;
            iReserveCat = 0;
            iLossTypeCode = 0;
            iClaimantEid = 0;
            bSuccess = false;
            Hashtable hashTable = null;
            double dPercent = 0d;
            double dReducedAmt = 0d;
            int iRcRowId = 0;
            iDimTypeCode = 0;
            int iCovGroupId = 0;
            int iApplicableReserveType = -1;
            int iCountMapReserveType = 0;
            double dParentReserveAmount = 0d;
            double dClaimDedReserve = 0d;
            double dDedAmountToCreate = 0d;
            int iExcludeExpenseReserve = 0;
            double dDedReserveOnGroup = 0d;
            double dAggLimit = 0d;
            double dAggApplicableLimit = 0d;
            //Start -  Added by Nikhil.Handle multicurrency scenario.
            double dExcRateReserveToBase = 0d;
            double dExcRateBaseToReserve = 0d;
          
            //End -  Added by Nikhil.Handle multicurrency scenario.
            try
            {
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin,m_iClientId);
                }
                dParentReserveAmount = p_dAmount;
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
             
                sbSQL = new StringBuilder();
                sbSQL.Length = 0;
                sbSQL.Append("SELECT PXIG.COV_GROUP_CODE FROM POLICY_X_INSLINE_GROUP PXIG INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLICY_X_INSLINE_GROUP_ID=PXIG.POLICY_X_INSLINE_GROUP_ID WHERE CXPD.POLCVG_ROW_ID=").Append(p_iCoverageID);
                sbSQL.Append(" AND CXPD.CLAIM_ID=").Append(p_iClaimID);
                sbSQL.Append(" AND CXPD.DED_TYPE_CODE <>").Append(objClaim.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"));
                iCovGroupId = objClaim.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                //end -  changed by Nikhil.Code review changes
                if (iCovGroupId > 0)
                {
                     iCountMapReserveType = objClaim.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0}" , iCovGroupId));
                    iApplicableReserveType = objClaim.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0} AND RESERVE_TYPE_CODE={1}",iCovGroupId, p_iAssociatedReserveType));
                   
                    if (iApplicableReserveType == 0 && iCountMapReserveType > 0)
                    {
                        return;
                    }

                   
                }
                //}
                ////end:added by Nitin goel
                if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0)
                {
                    objClaim.MoveTo(p_iClaimID);
                    iRecoveryTransTypeCode = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecTransType;
                    
                   
                    iLossTypeCode = p_iLossTypeCode;                    
                    //Changed by Nikhil.Code review changes
                   
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString,
                       String.Format("SELECT CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE POLCVG_ROW_ID = {0} AND LOSS_CODE= {1}" ,p_iCoverageID, iLossTypeCode)))
                    {
                        if (objReader.Read())
                        {
                            iCoverageLossRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        }
                    }
                   
                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT CXP.SIR_DED_AMT, CXP.DIM_PERCENT_NUM, C.CLAIMANT_EID, CXP.DIMNSHNG_TYPE_CODE, CXP.EXCLUDE_EXPENSE_FLAG FROM CLAIMANT C");
                        sbSQL.Append(" INNER JOIN ENTITY E ON C.CLAIMANT_EID = E.ENTITY_ID");
                        sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXP ON C.CLAIMANT_EID = CXP.CLAIMANT_EID ");
                        sbSQL.Append(" WHERE CXP.CLAIM_ID = ").Append(p_iClaimID);
                        sbSQL.Append(" AND CXP.POLCVG_ROW_ID= ").Append(p_iCoverageID);
                        sbSQL.Append(" AND E.DELETED_FLAG<>-1");


                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString,sbSQL.ToString()))
                    //end - changed by Nikhil.Code review changes 
                    {
                        if (objReader.Read())
                        {
                            dAmount = Conversion.ConvertObjToDouble(objReader.GetValue("SIR_DED_AMT"), m_iClientId);
                            iClaimantEid = Conversion.CastToType<int>(objReader.GetValue("CLAIMANT_EID").ToString(), out bSuccess);
                            dPercent = Conversion.CastToType<double>(objReader.GetValue("DIM_PERCENT_NUM").ToString(), out bSuccess);
                            iDimTypeCode = Conversion.CastToType<int>(objReader.GetValue("DIMNSHNG_TYPE_CODE").ToString(), out bSuccess);
                            iExcludeExpenseReserve = Conversion.CastToType<int>(objReader.GetValue("EXCLUDE_EXPENSE_FLAG").ToString(), out bSuccess);
                        }
                    }
                    //Start -  Added by Nikhil.Handle multicurrency scenario.
                    dExcRateBaseToReserve = CommonFunctions.ExchangeRateSrc2Dest(objClaim.Context.InternalSettings.SysSettings.BaseCurrencyType, CommonFunctions.iGetReserveCurrencyCode(sReserveCurrencyType, m_sConnectionString, m_iClientId), m_sConnectionString, m_iClientId);
                    dExcRateReserveToBase = CommonFunctions.ExchangeRateSrc2Dest(CommonFunctions.iGetReserveCurrencyCode(sReserveCurrencyType, m_sConnectionString, m_iClientId), objClaim.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    dParentReserveAmount = dParentReserveAmount * dExcRateReserveToBase;
                
                    

                    //End -  Added by Nikhil.Handle multicurrency scenario.
                    if (iExcludeExpenseReserve == -1 && objClaim.Context.LocalCache.GetRelatedCodeId(p_iAssociatedReserveType) == objClaim.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE")) //Deductible reserve should not be created when Expense reserve is setup if expenses are excluded for deductible calculation.
                    {
                        return;
                    }
                    if (dAmount > 0 && dPercent < 100)
                    {
                       
                        if ((iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE")) || (iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("E", "DIMINISHING_TYPE")))
                        {
                            dReducedAmt = dAmount * ((100d - dPercent) / 100d);
                        }
                        else
                        {
                            dReducedAmt = dAmount;
                        }
                    }

                    //tanwar2: In case of group - we need to consider the Aggregate Limit (balance)
                    //We calculate the difference between Aggregate Limit and total deductible reserve on group.
                    //This amount is the limit for this deductible reserve.
                    if (iRecoveryTransTypeCode > 0)
                    {
                        iRecoveryReserveTypeCode = objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(iRecoveryTransTypeCode);
                    }
                 
                    if (iCovGroupId > 0)
                    {
                        sbSQL.Length = 0;
                        sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                        sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON RC.POLCVG_ROW_ID = CXPD.POLCVG_ROW_ID ");
                        sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                       
                        sbSQL.Append(" AND PXIG.COV_GROUP_CODE = ").Append(iCovGroupId);
                        sbSQL.Append(" AND RC.RESERVE_TYPE_CODE = ").Append(iRecoveryReserveTypeCode);
                        
                      
                        sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID <> ").Append(iCoverageLossRowId);
                        //End -  changed by Nikhil.Code review changes
                        dDedReserveOnGroup = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                     dAggLimit = objClaim.Context.DbConnLookup.ExecuteDouble(String.Format("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE ={0} ", iCovGroupId));
                        //start - Added by Nikhil to check for aggregate limit if greater then 0.
                       
                        if (dAggLimit > 0)
                        {
                            dAggApplicableLimit = dAggLimit - dDedReserveOnGroup;
                        }

                    }

                    

                    iClaimantEid = p_iClaimantEID; //To assign claimant of parent reserve
                    if (iClaimantEid > 0)
                    {
                        iStatusCode = p_iReserveStatus;
                        //iStatusCode = objClaim.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("O", "RESERVE_TYPE");
                        
                        //tanwar2 - Calculate sum of deductible reserve created so far on claim.
                        //Added by Nitin goel , to handle the case of coverage group deductible amount 
                        if (iCovGroupId>0 && GetDeductiblePerEventFlag(iCovGroupId)) //Added Flag check
                            {
                             sbSQL.Length=0;
                          
                             sbSQL.Append("SELECT RESERVE_AMOUNT FROM RESERVE_CURRENT RC");
                             sbSQL.Append(" INNER JOIN CLAIM C ON RC.CLAIM_ID = C.CLAIM_ID");
                             sbSQL.Append(" WHERE RC.POLCVG_ROW_ID IN ");
                             sbSQL.Append(GetCoverageGroupQuery(objClaim.EventId,iCovGroupId));
                             sbSQL.Append(" AND RC.RESERVE_TYPE_CODE = ").Append(iRecoveryReserveTypeCode);
                             sbSQL.Append(" AND C.EVENT_ID = ").Append(objClaim.EventId);
                             //end - Changed by Nikhil on 10/28/14
                            dClaimDedReserve = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                            }
                        //emd :added by Nitin goel 
                        else//: To Do need to include the case of Remaining aggregate amount
                            { 
                                sbSQL.Length=0;
                                sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                           
                                sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimID);
                                sbSQL.Append(" AND POLCVG_ROW_ID = ").Append(p_iCoverageID);
                                sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryReserveTypeCode);
                                //end -  changed by Nikhil.code review changes
                                dClaimDedReserve = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                            }

                        dDedAmountToCreate = dReducedAmt - dClaimDedReserve;
                        if (dDedAmountToCreate < 0d)
                        {
                            dDedAmountToCreate = 0d;
                        }

                        if (dParentReserveAmount < dDedAmountToCreate)
                        {
                            dDedAmountToCreate = dParentReserveAmount;
                        }
                        //start - Added by Nikhil to check for aggregate limit if greater then 0.
                      
                        if (iCovGroupId > 0 && dDedAmountToCreate > dAggApplicableLimit && dAggLimit > 0)
                        //END - Added by Nikhil to check for aggregate limit if greater then 0.
                        {
                            dDedAmountToCreate = dAggApplicableLimit;
                        }
                        //tanwar2 - end

                        sbSQL.Length = 0;
                        sbSQL.Append("SELECT COUNT(RC_ROW_ID) FROM RESERVE_CURRENT WHERE ");
                      
                        sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimID);
                        sbSQL.Append(" AND CLAIMANT_EID = ").Append(iClaimantEid);
                        sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryReserveTypeCode);
                        sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = ").Append(iCoverageLossRowId);
                        //end - changed by Nikhil.Code review changes.

                        iValidateRecResExist = Convert.ToInt32(DbFactory.ExecuteScalar(m_sConnectionString, Convert.ToString(sbSQL)));
                        if (objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1
                            && iRecoveryReserveTypeCode > 0 && objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                        {
                            hashTable = new Hashtable();

                            if (iValidateRecResExist > 0)
                            {
                                bUpdateRecovery = true;
                            }
                          //start -   Added by Nikhil.Handle multicurrency scenario.
                            dParentReserveAmount = dParentReserveAmount * dExcRateBaseToReserve;
                            dDedAmountToCreate = dDedAmountToCreate * dExcRateBaseToReserve;
                            // end  -  Added by Nikhil.Handle multicurrency scenario.
                            if (bUpdateRecovery)
                            {
                                //If status is not open (for example hold) - then there is no need to edit the recovery reserve.
                                //but we may still need to add (if it doesnot already exist)
                                if (objClaim.Context.LocalCache.GetRelatedCodeId(iStatusCode) == objClaim.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT"))
                                {
                                    //hashTable = new Hashtable();
                                    this.EditRecoveryReserve(ref hashTable, p_iClaimID, iClaimantEid, p_iPolicyID, p_iCoverageID, iRecoveryReserveTypeCode, p_iAdjusterEID, dParentReserveAmount, iStatusCode, sReason, iCoverageLossRowId, sReserveCurrencyType);
                                }
                            }
                            else
                            {
                                if (objClaim.Context.LocalCache.GetRelatedCodeId(iStatusCode) == objClaim.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS_PARENT"))
                                {
                                    iStatusCode = objClaim.Context.LocalCache.GetCodeId("O", "RESERVE_STATUS");
                                }
                                this.AddReserve(ref hashTable, p_iClaimID, iClaimantEid, p_iPolicyID, p_iCoverageID, iRecoveryReserveTypeCode, p_iAdjusterEID, dDedAmountToCreate, ref iStatusCode,
                                sReason, iLossTypeCode, p_idisabilityCode, iReserveCat, sReserveCurrencyType, out iRcRowId);
                                
                            }
                        }
                    }
                }


            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                //Start - added by Nikhil.Code review changes
                if (sbSQL != null)
                {
                    sbSQL = null;
                }
                //End

            }

        }
        //tanwar2/nitin goel - mits - NIS - end
        /// <summary>
        /// tanwar2/nitin goel - mits 30910 changes - 01/23/2013
        /// Creates the recovery reserve for the claim, policy
        /// </summary>
        /// <param name="p_iClaimID">Claim Id</param>
        /// <param name="p_iPolicyID">Policy Id</param>
        /// <param name="p_iCoverageID">Coverage Id</param>
        /// <param name="p_idisabilityCode">Disability Type Code</param>
        [Obsolete("This method is obsolete. Please use its overload method instead.", true)]
        public void AddRecoveryReserve(int p_iClaimID, int p_iPolicyID, int p_iCoverageID, int p_idisabilityCode, int p_iAssociatedReserveType, int p_iAdjusterEID)
        {
            int iRecoveryReserveTypeCode;
            int iRecoveryTransTypeCode;
            int iValidateRecResExist;
            int iCoverageLossRowId;
            int iLossTypeCode;
            string sReason;
            bool bCreateRecovery;
            double dAmount;
            int iStatusCode;
            int iReserveCat;
            int iClaimantEid;
            int iDimTypeCode;
            bool bSuccess;
            StringBuilder sbSQL=null;
            Claim objClaim = null;
            iRecoveryReserveTypeCode = 0;
            sReason = string.Empty;
            bCreateRecovery = false;            
            iValidateRecResExist = 0;            
            iCoverageLossRowId = 0;
            dAmount = 0;
            iStatusCode = 0;
            iReserveCat = 0;
            iLossTypeCode = 0;
            iClaimantEid = 0;
            bSuccess = false;
            Hashtable hashTable = null;
            double dPercent = 0d;
            double dReducedAmt = 0d;
            int iRcRowId = 0;
            iDimTypeCode = 0;
            int iCovGroupId = 0;
            int iApplicableReserveType = -1;
            int iCountMapReserveType = 0;
            try
            {
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin,m_iClientId);
                }
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
            
                    iCovGroupId = objClaim.Context.DbConnLookup.ExecuteInt("SELECT PXIG.COV_GROUP_CODE FROM POLICY_X_INSLINE_GROUP PXIG INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLICY_X_INSLINE_GROUP_ID=PXIG.POLICY_X_INSLINE_GROUP_ID WHERE CXPD.POLCVG_ROW_ID=" + p_iCoverageID + " AND CXPD.CLAIM_ID=" + p_iClaimID);
                    if (iCovGroupId > 0)
                    {
                        iCountMapReserveType = objClaim.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID=" + iCovGroupId);
                        iApplicableReserveType = objClaim.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID=" + iCovGroupId + " AND RESERVE_TYPE_CODE=" + p_iAssociatedReserveType);
                        if (iApplicableReserveType == 0 && iCountMapReserveType > 0)                        
                        {
                            return;
                        }
                    }
                //}
                ////end:added by Nitin goel
                        if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0)
                        {
                            objClaim.MoveTo(p_iClaimID);
                            iRecoveryTransTypeCode = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecTransType;
                           // iLossTypeCode = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecReserveLossType;
                            iLossTypeCode = 0;
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString,
                                "SELECT CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE POLCVG_ROW_ID = " + p_iCoverageID + " AND LOSS_CODE=" + iLossTypeCode))
                            {
                                if (objReader.Read())
                                {
                                    iCoverageLossRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                                }
                            }

                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString,
                                String.Format("SELECT CXP.SIR_DED_AMT, CXP.DIM_PERCENT_NUM, C.CLAIMANT_EID, CXP.DIMNSHNG_TYPE_CODE FROM CLAIMANT C INNER JOIN ENTITY E ON C.CLAIMANT_EID = E.ENTITY_ID " +
                                "INNER JOIN CLAIM_X_POL_DED CXP ON C.CLAIMANT_EID = CXP.CLAIMANT_EID " +
                                "WHERE CXP.CLAIM_ID = {0} AND CXP.POLCVG_ROW_ID={1} AND E.DELETED_FLAG<>-1", p_iClaimID, p_iCoverageID)))
                            {
                                if (objReader.Read())
                                {
                                    dAmount = Conversion.ConvertObjToDouble(objReader.GetValue("SIR_DED_AMT"), m_iClientId);
                                    iClaimantEid = Conversion.CastToType<int>(objReader.GetValue("CLAIMANT_EID").ToString(), out bSuccess);
                                    dPercent = Conversion.CastToType<double>(objReader.GetValue("DIM_PERCENT_NUM").ToString(), out bSuccess);
                                    iDimTypeCode = Conversion.CastToType<int>(objReader.GetValue("DIMNSHNG_TYPE_CODE").ToString(), out bSuccess);
                                }
                            }

                            if (dAmount > 0 && dPercent < 100)
                            {
                                //if (iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE"))
                                if ((iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE")) || (iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("E", "DIMINISHING_TYPE")))
                                {
                                    dReducedAmt = dAmount * ((100d - dPercent) / 100d);
                                }
                                else
                                {
                                    dReducedAmt = dAmount;
                                }
                            }

                            if (iClaimantEid > 0)
                            {
                                iStatusCode = objClaim.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("O", "RESERVE_TYPE");
                                if (iRecoveryTransTypeCode > 0)
                                {
                                    iRecoveryReserveTypeCode = objClaim.Context.InternalSettings.CacheFunctions.GetRelatedCodeID(iRecoveryTransTypeCode);
                                }
                                sbSQL = new StringBuilder();
                                sbSQL.Append("SELECT COUNT (RC_ROW_ID) FROM RESERVE_CURRENT WHERE ");
                                sbSQL.Append(" CLAIM_ID = " + p_iClaimID);
                                sbSQL.Append(" AND RESERVE_TYPE_CODE = " + iRecoveryReserveTypeCode);
                                sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + iCoverageLossRowId);

                                iValidateRecResExist = Convert.ToInt32(DbFactory.ExecuteScalar(m_sConnectionString, Convert.ToString(sbSQL)));

                                if (iValidateRecResExist == 0 && objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].ApplyDedToPaymentsFlag == -1
                                    && iRecoveryReserveTypeCode > 0 && objClaim.Context.InternalSettings.SysSettings.UsePolicyInterface)
                                {
                                    bCreateRecovery = true;
                                }
                                if (bCreateRecovery)
                                {
                                    hashTable = new Hashtable();
                                    this.AddReserve(ref hashTable, p_iClaimID, iClaimantEid, p_iPolicyID, p_iCoverageID, iRecoveryReserveTypeCode, p_iAdjusterEID, dReducedAmt, iStatusCode,
                                        sReason, iLossTypeCode, p_idisabilityCode, iReserveCat, out iRcRowId);
                                }
                            }
                        }
                    
                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }                           
               
            }

        }
        //tanwar2/nitin goel - mits - NIS - end


        /// <summary>
        /// MITS:34082
        /// Edit a Reserve Amount
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <param name="p_iClaimantEID"></param>
        /// <param name="p_iPolicyID"></param>
        /// <param name="p_iPolCvgID"></param>
        /// <param name="p_iReserveTypeCode"></param>
        /// <param name="sReserveCurrencyType"></param>
        /// <param name="p_dAmount"></param>
        /// <returns></returns>
        // tanwar2 - made status code as reference type
        public XmlDocument EditReserve(ref System.Collections.Hashtable p_objScriptErrOut, int p_iClaimId, int p_iClaimantEID, int p_iPolicyID, int p_iPolCvgID, int p_iReserveTypeCode, int p_iAdjusterEID, double p_dAmount, ref int iStatusCode, string sReason, int p_iCvgLossId, string sReserveCurrencyType, out int p_iRcRowID)
        {
            XmlDocument objXmlDoc = null;
            //StringBuilder sbSQL = null;
            ReserveCurrent objReserveCurrent = null;
            LocalCache objCache = null;

            //akaur9 MITS 27907 ---start
            XmlElement objElement = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            //akaur9 MITS 27907 ---End
            //MITS:34082 MultiCurrency START
            SysSettings objSysSettings = null;
            objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
            //MITS:34082 MultiCurrency END
            try
            {
                objXmlDoc = new XmlDocument();

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);

                //objReserveCurrent. 
                objReserveCurrent.ClaimId = p_iClaimId;
                objReserveCurrent.ClaimantEid = p_iClaimantEID;
                //objReserveCurrent.PolicyId = p_iPolicyID;
                objReserveCurrent.CoverageLossId = p_iCvgLossId;
                objReserveCurrent.CoverageId = p_iPolCvgID;
                //objReserveCurrent.UnitId = iUnitID;
                objReserveCurrent.ReserveTypeCode = p_iReserveTypeCode;
                //objReserveCurrent.DateEntered = sDateEntered;
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                objReserveCurrent.AssignAdjusterEid = p_iAdjusterEID;
                //Ankit End
                //Mits 35453 starts
                //objReserveCurrent.ReserveAmount = p_dAmount;
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                //MITS:34082 MultiCurrency START
                int iReserveCurrCode = 0;
                if (objSysSettings.UseMultiCurrency != 0) iReserveCurrCode = CommonFunctions.iGetReserveCurrencyCode(sReserveCurrencyType, m_sConnectionString, m_iClientId);
                else iReserveCurrCode = iClaimCurrCode;
                objReserveCurrent.iReserveCurrCode = iReserveCurrCode;
                //MITS:34082 MultiCurrency END
                if (iClaimCurrCode > 0)
                {
                    double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    //MITS:34082 MultiCurrency START
                    double dExcRateReserveToClaim = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, iClaimCurrCode, m_sConnectionString, m_iClientId);
                    double dExcRateReserveToBase = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    double dExcRateBaseToReserve = CommonFunctions.ExchangeRateSrc2Dest(objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, iReserveCurrCode, m_sConnectionString, m_iClientId);
                    objReserveCurrent.dReserveToClaimCurRate = dExcRateReserveToClaim;
                    objReserveCurrent.dReserveToBaseCurRate = dExcRateReserveToBase;
                    objReserveCurrent.dReserveCurReserveAmount = p_dAmount;
                    objReserveCurrent.dReserveCurrReserveAmount = p_dAmount;
                    objReserveCurrent.ReserveAmount = p_dAmount * dExcRateReserveToBase;
                    objReserveCurrent.dBaseToReserveCurrRate = dExcRateBaseToReserve;
                    //objReserveCurrent.ReserveAmount = p_dAmount * dExhRateClaimToBase;
                    //MITS:34082 MultiCurrency END
                    objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                }
                else
                {
                    objReserveCurrent.ReserveAmount = p_dAmount;
                }
                //Mits 35453 ends
                objReserveCurrent.DateEntered = Conversion.ToDbDate(DateTime.Now);
                //objReserveCurrent.ResStatusCode = iStatusCodeID;
                //objReserveCurrent.Reason = sReason;
                //objReserveCurrent.sUser = sUser;
                //objReserveCurrent.iUserId = iUserId;
                //objReserveCurrent.iGroupId = iGroupId;
                objReserveCurrent.sUpdateType = "Reserves";

                //objReserveCurrent.MoveTo(p_iReserveID);
                objReserveCurrent.iUserId = m_userLogin.UserId;
                objReserveCurrent.iGroupId = m_userLogin.GroupId;
                //objReserveCurrent.ReserveAmount = p_dAmount;

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }


                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);

                objReserveCurrent.EnteredByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                objReserveCurrent.ResStatusCode = iStatusCode;
                objReserveCurrent.Reason = sReason;
                //objReserveCurrent.ResStatusCode = objCache.GetCodeId("O", "RESERVE_STATUS");
                objReserveCurrent.Save();

                p_iRcRowID = objReserveCurrent.RcRowId;
                ////Start - averma62 - Check wheather claim is marked for vss or not
                //#region VSS Adjuster Export
                //string sVssFlag = string.Empty;
                //sVssFlag = Conversion.ConvertObjToStr(objReserveCurrent.Context.DbConn.ExecuteScalar("SELECT VSS_CLAIM_IND FROM CLAIM WHERE CLAIM_ID = " + objReserveCurrent.ClaimId));

                //if (sVssFlag.Equals("-1"))
                //{
                //    VssExportAsynCall objVss = new VssExportAsynCall(objReserveCurrent.Context.DbConn.Database, m_userLogin.LoginName, m_userLogin.Password);
                //    objVss.AsynchVssReserveExport(objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid, objReserveCurrent.RcRowId, 0, 0, "", "", "", "", "Reserve");
                //}
                //#endregion
                ////End - averma62 - Check wheather claim is marked for vss or not

                //akaur9 MITS 27907 --Start               
                if (objReserveCurrent.ResStatusCode != 0)
                {
                    objCache.GetCodeInfo(objReserveCurrent.ResStatusCode, ref sShortCode, ref sCodeDesc);
                    objElement = objXmlDoc.CreateElement("ReserveStatus");
                    objElement.SetAttribute("Status", sShortCode);
                    objElement.InnerText = sShortCode + " - " + sCodeDesc;
                    objXmlDoc.AppendChild(objElement);
                }

                //tanwar2 - JIRA: RMA-7205 - start 
                //Setting value back so that calling method may know the final status of the reserve.
                iStatusCode = objReserveCurrent.ResStatusCode;
                //tanwar2 - JIRA: RMA-7205 - end
                //akaur9 MITS 27907 --End
                //tanwar2 - Add validation warnings - start
                foreach (System.Collections.DictionaryEntry objWarnings in objReserveCurrent.Context.ScriptWarningErrors)
                {
                    p_objScriptErrOut.Add(p_objScriptErrOut.Count + 1, objWarnings.Value.ToString());
                }
                //tanwar2 - Add validation warnings - end
            }
            //rupal:start, MITS 27373
            catch (RMAppException p_objEx)
            {
                foreach (System.Collections.DictionaryEntry objErrors in objReserveCurrent.Context.ScriptValidationErrors)//MITS 30380 starts
                    p_objScriptErrOut.Add("ValidationScriptError", objErrors.Value.ToString());//MITS 30380 ends

                throw p_objEx;
            }//rupal:end
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.EditReserve.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objReserveCurrent == null)
                {
                    objReserveCurrent.Dispose();
                    objReserveCurrent = null;
                }
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objSysSettings = null;//MITS:34082 MultiCurrency
            }

            return objXmlDoc;

        }


        /// <summary>
        /// Edit a Reserve Amount
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <param name="p_iClaimantEID"></param>
        /// <param name="p_iPolicyID"></param>
        /// <param name="p_iPolCvgID"></param>
        /// <param name="p_iReserveTypeCode"></param>
        /// <param name="p_dAmount"></param>
        /// <returns></returns>
        public XmlDocument EditReserve(ref System.Collections.Hashtable p_objScriptErrOut, int p_iClaimId, int p_iClaimantEID, int p_iPolicyID, int p_iPolCvgID, int p_iReserveTypeCode, int p_iAdjusterEID, double p_dAmount, int iStatusCode, string sReason, int p_iCvgLossId, out int p_iRcRowID)//30380
        {
            XmlDocument objXmlDoc = null;
            //StringBuilder sbSQL = null;
            ReserveCurrent objReserveCurrent = null;
            LocalCache objCache = null;

            //akaur9 MITS 27907 ---start
            XmlElement objElement = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            //akaur9 MITS 27907 ---End

            try
            {
                objXmlDoc = new XmlDocument();

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);

                //objReserveCurrent. 
                objReserveCurrent.ClaimId = p_iClaimId;
                objReserveCurrent.ClaimantEid = p_iClaimantEID;
                //objReserveCurrent.PolicyId = p_iPolicyID;
                objReserveCurrent.CoverageLossId = p_iCvgLossId;
                objReserveCurrent.CoverageId = p_iPolCvgID;
                //objReserveCurrent.UnitId = iUnitID;
                objReserveCurrent.ReserveTypeCode = p_iReserveTypeCode;
                //objReserveCurrent.DateEntered = sDateEntered;
                //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                objReserveCurrent.AssignAdjusterEid = p_iAdjusterEID;
                //Ankit End
                //Mits 35453 starts
                //objReserveCurrent.ReserveAmount = p_dAmount;
                int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                if (iClaimCurrCode > 0)
                {
                    double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    objReserveCurrent.ReserveAmount = p_dAmount * dExhRateClaimToBase;
                    objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                }
                else
                {
                    objReserveCurrent.ReserveAmount = p_dAmount;
                }
                //Mits 35453 ends
                objReserveCurrent.DateEntered = Conversion.ToDbDate(DateTime.Now);
                //objReserveCurrent.ResStatusCode = iStatusCodeID;
                //objReserveCurrent.Reason = sReason;
                //objReserveCurrent.sUser = sUser;
                //objReserveCurrent.iUserId = iUserId;
                //objReserveCurrent.iGroupId = iGroupId;
                objReserveCurrent.sUpdateType = "Reserves";

                //objReserveCurrent.MoveTo(p_iReserveID);
                objReserveCurrent.iUserId = m_userLogin.UserId;
                objReserveCurrent.iGroupId = m_userLogin.GroupId;
                //objReserveCurrent.ReserveAmount = p_dAmount;

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }


                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);

                objReserveCurrent.EnteredByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                objReserveCurrent.ResStatusCode = iStatusCode;
                objReserveCurrent.Reason = sReason;
                //objReserveCurrent.ResStatusCode = objCache.GetCodeId("O", "RESERVE_STATUS");
                objReserveCurrent.Save();

                p_iRcRowID = objReserveCurrent.RcRowId;
                ////Start - averma62 - Check wheather claim is marked for vss or not
                //#region VSS Adjuster Export
                //string sVssFlag = string.Empty;
                //sVssFlag = Conversion.ConvertObjToStr(objReserveCurrent.Context.DbConn.ExecuteScalar("SELECT VSS_CLAIM_IND FROM CLAIM WHERE CLAIM_ID = " + objReserveCurrent.ClaimId));

                //if (sVssFlag.Equals("-1"))
                //{
                //    VssExportAsynCall objVss = new VssExportAsynCall(objReserveCurrent.Context.DbConn.Database, m_userLogin.LoginName, m_userLogin.Password);
                //    objVss.AsynchVssReserveExport(objReserveCurrent.ClaimId, objReserveCurrent.ClaimantEid, objReserveCurrent.RcRowId, 0, 0, "", "", "", "", "Reserve");
                //}
                //#endregion
                ////End - averma62 - Check wheather claim is marked for vss or not

                //akaur9 MITS 27907 --Start               
                if (objReserveCurrent.ResStatusCode != 0)
                {
                    objCache.GetCodeInfo(objReserveCurrent.ResStatusCode, ref sShortCode, ref sCodeDesc);
                    objElement = objXmlDoc.CreateElement("ReserveStatus");
                    objElement.SetAttribute("Status", sShortCode);
                    objElement.InnerText = sShortCode + " - " + sCodeDesc;
                    objXmlDoc.AppendChild(objElement);
                }

                //akaur9 MITS 27907 --End
                //tanwar2 - Add validation warnings - start
                foreach (System.Collections.DictionaryEntry objWarnings in objReserveCurrent.Context.ScriptWarningErrors)
                {
                    p_objScriptErrOut.Add(p_objScriptErrOut.Count + 1, objWarnings.Value.ToString());
                }
                //tanwar2 - Add validation warnings - end
            }
            //rupal:start, MITS 27373
            catch (RMAppException p_objEx)
            {
                foreach (System.Collections.DictionaryEntry objErrors in objReserveCurrent.Context.ScriptValidationErrors)//MITS 30380 starts
                    p_objScriptErrOut.Add("ValidationScriptError", objErrors.Value.ToString());//MITS 30380 ends

                throw p_objEx;
            }//rupal:end
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.EditReserve.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objReserveCurrent == null)
                {
                    objReserveCurrent.Dispose();
                    objReserveCurrent = null;
                }
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }

            return objXmlDoc;

        }

        /// <summary>
        /// Added by nitin goel, to edit recovery reserve when parent reserve is edited.
        /// </summary>
        /// <param name="p_objScriptErrOut"></param>
        /// <param name="p_iClaimId"></param>
        /// <param name="p_iClaimantEID"></param>
        /// <param name="p_iPolicyID"></param>
        /// <param name="p_iPolCvgID"></param>
        /// <param name="p_iReserveTypeCode"></param>
        /// <param name="p_iAdjusterEID"></param>
        /// <param name="p_dAmount"></param>
        /// <param name="iStatusCode"></param>
        /// <param name="sReason"></param>
        /// <param name="p_iCvgLossId"></param>
        /// <param name="p_iRcRowID"></param>
        /// <returns></returns>
        public void EditRecoveryReserve(ref System.Collections.Hashtable p_objScriptErrOut, int p_iClaimId, int p_iClaimantEID, int p_iPolicyID, int p_iPolCvgID, int p_iReserveTypeCode, int p_iAdjusterEID, double p_dAmount, int iStatusCode, string sReason, int p_iCvgLossId, string sReserveCurrencyType)//30380
        {
            XmlDocument objXmlDoc = null;
            //StringBuilder sbSQL = null;
            ReserveCurrent objReserveCurrent = null;
            LocalCache objCache = null;

            //akaur9 MITS 27907 ---start
            XmlElement objElement = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            //akaur9 MITS 27907 ---End
            Claim objClaim = null;
            ClaimXPolDed objClaimXPolDed = null;
            int iRecoveryResTypeCode=0;
            int iClaimXPolDedId=0;
            StringBuilder sbSQL = null;
            double dSirDedAmt = 0d;
            double dPercent = 0d;
            int iDimTypeCode = 0;
            double dReducedAmt = 0d;
            double dSumParentReserveAmount = 0d;
            double dParentOtherDedReserve = 0d;
            double dSumOverrideDedExcludeAmount = 0d;
            bool bRedistributePossible = false;
            double dDedReducedAmount = 0d;
            double dChangeAmount = 0d;
            double dDedReserveOnGroup = 0d;
            double dAggLimit = 0d;

            int iCovGroupId = 0;
            double dAggApplicableLimit = 0d;
            //Start - Added by Nikhil -  Include applicable reserve type and check for expense reserve
            int iCountMapReserveType = 0;
            int iExcludeExpenseReserve = 0;
            //End - Added by Nikhil -  Include applicable reserve type and check for expense reserve
            //Start -  Added by Nikhil.Handle multicurrency scenario.
            double dExcRateReserveToBase = 0d;
            double dExcRateReserveToClaim = 0d;
            double dExcRateBaseToReserve = 0d;
            double dExhRateClaimToBase = 0d;
            SysSettings objSysSettings = null;
            //End -  Added by Nikhil.Handle multicurrency scenario.
            try
            {
                // Start - Added by Nikhil on 07/07/14 to initialize datamodel object
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin,m_iClientId);
                }
                
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                //End - Added by Nikhil on 07/07/14 to initialize datamodel object
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                iRecoveryResTypeCode = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecReserveType;
                iClaimXPolDedId = objClaim.Context.DbConnLookup.ExecuteInt(string.Format("SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} and POLCVG_ROW_ID={1}", p_iClaimId, p_iPolCvgID));
               //Start -  changed by Nikhil.Code review changes
                sbSQL = new StringBuilder();

               
                sbSQL.Length = 0;
                sbSQL.Append("SELECT PXIG.COV_GROUP_CODE FROM POLICY_X_INSLINE_GROUP PXIG INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLICY_X_INSLINE_GROUP_ID=PXIG.POLICY_X_INSLINE_GROUP_ID WHERE CXPD.POLCVG_ROW_ID=").Append(p_iPolCvgID);
                sbSQL.Append(" AND CXPD.CLAIM_ID=").Append(p_iClaimId);
                sbSQL.Append(" AND CXPD.DED_TYPE_CODE <>").Append(objClaim.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"));
                iCovGroupId = objClaim.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                //end -  changed by Nikhil.Code review changes
                //Added by Nikhil -  check for expense reserve
                iExcludeExpenseReserve = objClaim.Context.DbConnLookup.ExecuteInt(String.Format("SELECT CXP.EXCLUDE_EXPENSE_FLAG FROM CLAIM_X_POL_DED CXP WHERE CXP.CLAIM_ID = {0} AND CXP.POLCVG_ROW_ID={1}", p_iClaimId, p_iPolCvgID));
                //Code review changes
               
                sbSQL.Length = 0;
                sbSQL.Append("SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE ");
            
                sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                sbSQL.Append(" AND CLAIMANT_EID = ").Append(p_iClaimantEID);
                sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = ").Append(p_iCvgLossId);
                sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
              
                int iRcRowId = objClaim.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                if (iRcRowId > 0)
                {
                    //TO DO: Still need to include the case of non-applicable reserve type and exclude  expense reserve type.
                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                  
                    sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                    sbSQL.Append(" AND CLAIMANT_EID = ").Append(p_iClaimantEID);
                    sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = ").Append(p_iCvgLossId);
                    sbSQL.Append(" AND RESERVE_TYPE_CODE <> ").Append(iRecoveryResTypeCode);
                    //End    - changed by Nikhil.code review changes
                    //Start - Added by Nikhil -  Include applicable reserve type and check for expense reserve
                    if (iCovGroupId > 0)
                    {
                        //changed by Nikhil.code review changes
                  
                        iCountMapReserveType = objClaim.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0}",iCovGroupId));
                        if (iCountMapReserveType > 0)
                            //changed by Nikhil.code review changes
                           
                            sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = ").Append(iCovGroupId).Append(")");
                    }

                    if (iExcludeExpenseReserve == -1)
                    {
                        //changed by Nikhil.code review changes
                    
                        sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE")).Append(")");
                    }

                    //End - Added by Nikhil -  Include applicable reserve type and check for expense reserve

                    dSumParentReserveAmount = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());


                    //Calculate amount with override deductible flag true

                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT SUM(FTS.AMOUNT) AMOUNT ");
                    sbSQL.Append(" FROM FUNDS_TRANS_SPLIT FTS ");
                    sbSQL.Append(" INNER JOIN FUNDS F ON F.TRANS_ID =FTS.TRANS_ID ");
                    sbSQL.Append(" INNER JOIN RESERVE_CURRENT RC ON FTS.RC_ROW_ID = RC.RC_ROW_ID WHERE ");
                  
                    sbSQL.Append(" RC.CLAIM_ID = ").Append(p_iClaimId);
                    sbSQL.Append(" AND RC.CLAIMANT_EID = ").Append(p_iClaimantEID);
                    sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID = ").Append(p_iCvgLossId);
                    sbSQL.Append(" AND RC.RESERVE_TYPE_CODE <> ").Append(iRecoveryResTypeCode);
                    //end - changed by Nikhil.Code review changes
                    sbSQL.Append(" AND FTS.OVERRIDE_DED_FLAG = -1 ");
                    sbSQL.Append(" AND (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND (F.PAYMENT_FLAG IS NOT NULL AND F.PAYMENT_FLAG <>0) ");
                    //Start - Added by Nikhil -  Include applicable reserve type and check for expense reserve
                    if (iCovGroupId > 0)
                    {
                        //Changed by Nikhil.Code review changes
                       
                        iCountMapReserveType = objClaim.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0}", iCovGroupId));
                        if (iCountMapReserveType > 0)
                            //Changed by Nikhil.Code review changes
                         
                        sbSQL.Append(" AND RC.RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = ").Append(iCovGroupId).Append(")");
                    }

                    if (iExcludeExpenseReserve == -1)
                    {
                        //Changed by Nikhil.Code review changes
                        
                        sbSQL.Append(" AND RC.RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE")).Append(")");

                    }
                    dSumOverrideDedExcludeAmount = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    //End

                    dSumParentReserveAmount = dSumParentReserveAmount - dSumOverrideDedExcludeAmount;

                    //Start -  Added by Nikhil.Handle multicurrency scenario.

                    dExcRateReserveToBase = CommonFunctions.ExchangeRateSrc2Dest(CommonFunctions.iGetReserveCurrencyCode(sReserveCurrencyType, m_sConnectionString, m_iClientId), objClaim.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    p_dAmount = p_dAmount * dExcRateReserveToBase;
                    
                    //END -  Added by Nikhil.Handle multicurrency scenario.
                    if (dSumParentReserveAmount!=p_dAmount)
                    {
                        p_dAmount=dSumParentReserveAmount;
                    }
                    objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);
                    objReserveCurrent.MoveTo(iRcRowId);
                    objClaimXPolDed = (ClaimXPolDed)m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false);
                    
                    objClaimXPolDed.MoveTo(iClaimXPolDedId);

                    dSirDedAmt = objClaimXPolDed.SirDedAmt;
                    dPercent = objClaimXPolDed.DimPercentNum;
                    iDimTypeCode = objClaimXPolDed.DiminishingTypeCode;
                    
                    if (dSirDedAmt > 0 && dPercent < 100)
                    {
                       
                        if ((iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE")) || (iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("E", "DIMINISHING_TYPE")))
                        {
                            dReducedAmt = dSirDedAmt * ((100d - dPercent) / 100d);
                        }
                        else
                        {
                            dReducedAmt = dSirDedAmt;
                        }
                    }

                    //tanwar2 - Save this amount for future use
                    dDedReducedAmount = dReducedAmt;

                    if (iCovGroupId > 0)
                    {
                        sbSQL.Length = 0;
                        sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                        sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON RC.POLCVG_ROW_ID = CXPD.POLCVG_ROW_ID ");
                        sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                        
                        sbSQL.Append(" AND PXIG.COV_GROUP_CODE = ").Append(iCovGroupId);
                        sbSQL.Append(" AND RC.RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
                        //Do not include current deductible reserve.
                        sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID <> ").Append(p_iCvgLossId);
                        //end - CHANGED by Nikhil.code review changes
                        dDedReserveOnGroup = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                        //Changed by Nikhil.Code review changes
                     
                        dAggLimit = objClaim.Context.DbConnLookup.ExecuteDouble(String.Format("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = {0}", iCovGroupId));

                     
                        if (dAggLimit > 0)
                        {
                              dAggApplicableLimit = dAggLimit - dDedReserveOnGroup;
                        }
                        //End - Added by Nikhil to check for aggregate limit if greater then 0.
                    }

                    

                    

                    if (iCovGroupId > 0 && GetDeductiblePerEventFlag(iCovGroupId)) //Added Enable deductible per event flag check
                    {
                        sbSQL.Length = 0;
                        sbSQL.Append(" SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                        sbSQL.Append(" INNER JOIN CLAIM C ON RC.CLAIM_ID = C.CLAIM_ID ");
                        //Changed by Nikhil.Code review changes
                 
                        sbSQL.Append(" WHERE C.EVENT_ID = ").Append(objClaimXPolDed.EventId);
                        sbSQL.Append(" AND POLCVG_ROW_ID IN ");
                        sbSQL.Append(GetCoverageGroupQuery(objClaimXPolDed.EventId, iCovGroupId));
                        //Changed by Nikhil.Code review changes
                 
                        sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
                        sbSQL.Append(" AND (POLCVG_LOSS_ROW_ID <> ").Append(objReserveCurrent.CoverageLossId);
                        sbSQL.Append(" OR CLAIMANT_EID <> ").Append(p_iClaimantEID).Append(") ");
                        dParentOtherDedReserve = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    }
                    else
                    { //To Do: Need to check if we'll need to include case of remaining aggregate here.
                        //tanwar2 - Calculate Deductible reserve on the current coverage but different loss type
                        sbSQL.Length = 0;
                        sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                    
                        sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                        sbSQL.Append(" AND POLCVG_ROW_ID = ").Append(objReserveCurrent.CoverageId);
                        sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
                        //end - changed by Nikhil.Code review changes
                        sbSQL.Append(" AND (POLCVG_LOSS_ROW_ID <> ").Append(objReserveCurrent.CoverageLossId);
                        sbSQL.Append(" OR CLAIMANT_EID <> ").Append(p_iClaimantEID).Append(") ");
                        dParentOtherDedReserve = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    }

                    dReducedAmt = dReducedAmt - dParentOtherDedReserve;
                    //Start - Added by Nikhil to check for aggregate limit if greater then 0.
               
                    if (iCovGroupId > 0 && dAggApplicableLimit < dReducedAmt && dAggLimit > 0)
                    //END - Added by Nikhil to check for aggregate limit if greater then 0.
                    {
                        dReducedAmt = dAggApplicableLimit;
                    }
                    if (dReducedAmt < 0d)
                    {
                        dReducedAmt = 0d;
                    }
                     int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
                //Start -.JIRA 6135. MultiCurrency START
                int iReserveCurrCode = 0;
                if (objSysSettings.UseMultiCurrency != 0)
                {
                    iReserveCurrCode = CommonFunctions.iGetReserveCurrencyCode(sReserveCurrencyType, m_sConnectionString, m_iClientId);
                }
                else
                {
                    iReserveCurrCode = iClaimCurrCode;
                }

              
                if (iClaimCurrCode > 0)
                {
                    dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    //MITS:34082 MultiCurrency START
                    dExcRateReserveToClaim = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, iClaimCurrCode, m_sConnectionString, m_iClientId);
                    dExcRateReserveToBase = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                    dExcRateBaseToReserve = CommonFunctions.ExchangeRateSrc2Dest(objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, iReserveCurrCode, m_sConnectionString, m_iClientId);
                }
                //END -.JIRA 6135. MultiCurrency START
                    if (dReducedAmt > p_dAmount && objReserveCurrent.ReserveAmount != p_dAmount) //Still need to include the case of diminshing deductible
                    {
                        //tanwar2 - Redistribution is possible only when the deductible reserve amount is decreasing.
                        if (objReserveCurrent.ReserveAmount > p_dAmount)
                        {
                            bRedistributePossible = true;
                            dChangeAmount = objReserveCurrent.ReserveAmount - p_dAmount;
                        }
                        //start -.JIRA 6135. MultiCurrency START
                        if(iClaimCurrCode > 0)
                        {
                            objReserveCurrent.dReserveToClaimCurRate = dExcRateReserveToClaim;
                            objReserveCurrent.dReserveToBaseCurRate = dExcRateReserveToBase;
                            objReserveCurrent.dReserveCurReserveAmount = p_dAmount * dExcRateBaseToReserve;
                            objReserveCurrent.dReserveCurrReserveAmount = p_dAmount * dExcRateBaseToReserve;
                            
                            objReserveCurrent.dBaseToReserveCurrRate = dExcRateBaseToReserve;
                            //objReserveCurrent.ReserveAmount = p_dAmount * dExhRateClaimToBase;
                            //MITS:34082 MultiCurrency END
                            objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                        }
                        //END -.JIRA 6135. MultiCurrency START
                        objReserveCurrent.ReserveAmount = p_dAmount;
                        objReserveCurrent.ClaimCurrReserveAmt = p_dAmount; //bsharma33 RMA-3887 Res. supp
                        objReserveCurrent.sUpdateType = "Reserves";
                        objReserveCurrent.iUserId = m_userLogin.UserId;
                        objReserveCurrent.iGroupId = m_userLogin.GroupId;
                        objReserveCurrent.EnteredByUser = objClaim.Context.RMUser.LoginName;
                        objReserveCurrent.ResStatusCode = iStatusCode;
                        //tanwar2 - mits 30910 - Reserve status handling for deductible - start
                        objReserveCurrent.iForcedReserveStatusCode = iStatusCode;
                        //tanwar2 - mits 30910 - Reserve status handling for deductible - end
                        objReserveCurrent.Reason = sReason;
                        objReserveCurrent.Save();
                    }
                    // Do we need condition at this point?

                    else if (dReducedAmt <= p_dAmount && objReserveCurrent.ReserveAmount != dReducedAmt)
                    {
                        //tanwar2 - Redistribution is possible only when the deductible reserve amount is decreasing.
                        if (objReserveCurrent.ReserveAmount > dReducedAmt)
                        {
                            bRedistributePossible = true;
                            dChangeAmount = objReserveCurrent.ReserveAmount - dReducedAmt;
                        }
                        //start -.JIRA 6135. MultiCurrency START
                        if (iClaimCurrCode > 0)
                        {
                            objReserveCurrent.dReserveToClaimCurRate = dExcRateReserveToClaim;
                            objReserveCurrent.dReserveToBaseCurRate = dExcRateReserveToBase;
                            objReserveCurrent.dReserveCurReserveAmount = dReducedAmt * dExcRateBaseToReserve;
                            objReserveCurrent.dReserveCurrReserveAmount = dReducedAmt * dExcRateBaseToReserve;

                            objReserveCurrent.dBaseToReserveCurrRate = dExcRateBaseToReserve;
                            //objReserveCurrent.ReserveAmount = p_dAmount * dExhRateClaimToBase;
                            //MITS:34082 MultiCurrency END
                            objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                        }
                        //END -.JIRA 6135. MultiCurrency START
                        objReserveCurrent.ReserveAmount = dReducedAmt;
                        objReserveCurrent.ClaimCurrReserveAmt = dReducedAmt; //bsharma33 RMA-3887 Res. supp
                        objReserveCurrent.sUpdateType = "Reserves";
                        objReserveCurrent.iUserId = m_userLogin.UserId;
                        objReserveCurrent.iGroupId = m_userLogin.GroupId;
                        objReserveCurrent.EnteredByUser = objClaim.Context.RMUser.LoginName;
                        objReserveCurrent.ResStatusCode = iStatusCode;
                        //tanwar2 - mits 30910 - Reserve status handling for deductible - start
                        objReserveCurrent.iForcedReserveStatusCode = iStatusCode;
                        //tanwar2 - mits 30910 - Reserve status handling for deductible - end
                        objReserveCurrent.Reason = sReason;
                        objReserveCurrent.Save();

                    }
                    //start -.JIRA 6135. MultiCurrency START
                    else
                    {
                        if (iClaimCurrCode > 0)
                        {
                            objReserveCurrent.dReserveToClaimCurRate = dExcRateReserveToClaim;
                            objReserveCurrent.dReserveToBaseCurRate = dExcRateReserveToBase;
                            objReserveCurrent.dReserveCurReserveAmount = objReserveCurrent.ReserveAmount * dExcRateBaseToReserve;
                            objReserveCurrent.dReserveCurrReserveAmount = objReserveCurrent.ReserveAmount * dExcRateBaseToReserve;

                            objReserveCurrent.dBaseToReserveCurrRate = dExcRateBaseToReserve;
                            //objReserveCurrent.ReserveAmount = p_dAmount * dExhRateClaimToBase;
                            //MITS:34082 MultiCurrency END
                            objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                            objReserveCurrent.sUpdateType = "Reserves";
                            objReserveCurrent.iUserId = m_userLogin.UserId;
                            objReserveCurrent.iGroupId = m_userLogin.GroupId;
                            objReserveCurrent.EnteredByUser = objClaim.Context.RMUser.LoginName;
                            objReserveCurrent.ResStatusCode = iStatusCode;
                            //tanwar2 - mits 30910 - Reserve status handling for deductible - start
                            objReserveCurrent.iForcedReserveStatusCode = iStatusCode;
                            //tanwar2 - mits 30910 - Reserve status handling for deductible - end
                            objReserveCurrent.Reason = sReason;
                            objReserveCurrent.Save();
                        }
                    
                    }
                    //END -.JIRA 6135. MultiCurrency START
                    //tanwar2 - Add validation warnings - start
                    foreach (System.Collections.DictionaryEntry objWarnings in objReserveCurrent.Context.ScriptWarningErrors)
                    {
                        p_objScriptErrOut.Add(p_objScriptErrOut.Count + 1, objWarnings.Value.ToString());
                    }
                    //tanwar2 - Add validation warnings - end
                }
               

                //tanwar2 - Redistribute deductible reserve
                if (bRedistributePossible)
                {
                    RedistributeDeductibleReserve(iRecoveryResTypeCode, p_iPolCvgID, p_iClaimId, p_iCvgLossId, iRcRowId, dDedReducedAmount, objClaimXPolDed.ExcludeExpenseFlag, 
                        dChangeAmount, iCovGroupId, objClaimXPolDed.EventId);
                }
            }
            //rupal:start, MITS 27373
            catch (RMAppException p_objEx)
            {
                foreach (System.Collections.DictionaryEntry objErrors in objReserveCurrent.Context.ScriptValidationErrors)//MITS 30380 starts
                    p_objScriptErrOut.Add("ValidationScriptError", objErrors.Value.ToString());//MITS 30380 ends

                throw p_objEx;
            }//rupal:end
            catch (Exception objException)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("ReserveFunds.EditReserve.GenericError",m_iClientId), objException);
            }
            finally
            {
                if (objReserveCurrent != null)//asharma326 JIRA 5981
                {
                    objReserveCurrent.Dispose();
                    objReserveCurrent = null;
                }              
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objClaimXPolDed != null)
                {
                    objClaimXPolDed.Dispose();
                    objClaimXPolDed = null;
                }
                //Added by Nikhil.Code review changes
                if (sbSQL != null)
                {
                    sbSQL = null;
                }
            }      

        }
        /// <summary>
        /// tanwar2/nitin goel - mits 30910 changes - 05/26/2014;
        /// Balance/adjust recovery reserves on change of deductible amount
        /// </summary>
        /// <param name="p_objScriptErrOut">Error object</param>
        /// <param name="p_iClaimId">Claim ID</param>
        /// <param name="p_iClaimantEID">Claimant EId</param>
        /// <param name="p_iPolicyID">Policy ID</param>
        /// <param name="p_iPolCvgID">Coverage ID</param>
        /// <param name="p_iReserveTypeCode">Reserve type code </param>
        /// <param name="p_iAdjusterEID">Adjuster EID</param>
        /// <param name="p_dAmount">Amount</param>
        /// <param name="iStatusCode">Reserve  status code</param>
        /// <param name="sReason">Reserve creation reason</param>
        /// <param name="p_iCvgLossId">Coverage Loss ID</param>
                
        public void EditRecoveryReserveBasedOnDedAmt(ref System.Collections.Hashtable p_objScriptErrOut, int p_iClaimId, int p_iClaimantEID, int p_iPolicyID, int p_iPolCvgID, int p_iReserveTypeCode, int p_iAdjusterEID, double p_dAmount, int iStatusCode, string sReason, int p_iCvgLossId)//30380
        {
            XmlDocument objXmlDoc = null;
            //StringBuilder sbSQL = null;
            ReserveCurrent objReserveCurrent = null;
            LocalCache objCache = null;

            //akaur9 MITS 27907 ---start
            XmlElement objElement = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            //akaur9 MITS 27907 ---End
            Claim objClaim = null;
            ClaimXPolDed objClaimXPolDed = null;
            int iRecoveryResTypeCode = 0;
            int iClaimXPolDedId = 0;
            StringBuilder sbSQL = null;
            double dSirDedAmt = 0d;
            double dPercent = 0d;
            int iDimTypeCode = 0;
            double dReducedAmt = 0d;
            double dSumParentReserveAmount = 0d;
            double dParentOtherDedReserve = 0d;

            bool bRedistributePossible = false;
            double dDedReducedAmount = 0d;
            double dChangeAmount = 0d;
            double dDedReserveOnGroup = 0d;
            double dAggLimit = 0d;

            int iCovGroupId = 0;
            double dAggApplicableLimit = 0d;
            //Start - Added by Nikhil -  Include applicable reserve type and check for expense reserve
            int iCountMapReserveType = 0;
            int iExcludeExpenseReserve = 0;
            //End - Added by Nikhil -  Include applicable reserve type and check for expense reserve

            try
            {
                // Start - Added by Nikhil on 07/07/14 to initialize datamodel object
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin,m_iClientId);
                }
                //End - Added by Nikhil on 07/07/14 to initialize datamodel object
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                iRecoveryResTypeCode = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode].DedRecReserveType;
                iClaimXPolDedId = objClaim.Context.DbConnLookup.ExecuteInt(string.Format("SELECT CLM_X_POL_DED_ID FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} and POLCVG_ROW_ID={1}", p_iClaimId, p_iPolCvgID));
                //Start -  changed by Nikhil.code review changes
             
                sbSQL = new StringBuilder();
                sbSQL.Length = 0;
                sbSQL.Append("SELECT PXIG.COV_GROUP_CODE FROM POLICY_X_INSLINE_GROUP PXIG INNER JOIN CLAIM_X_POL_DED CXPD ON CXPD.POLICY_X_INSLINE_GROUP_ID=PXIG.POLICY_X_INSLINE_GROUP_ID WHERE CXPD.POLCVG_ROW_ID=").Append(p_iPolCvgID);
                sbSQL.Append(" AND CXPD.CLAIM_ID=").Append(p_iClaimId);
                sbSQL.Append(" AND CXPD.DED_TYPE_CODE <>").Append(objClaim.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"));
                iCovGroupId = objClaim.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                //END - changed by Nikhil.Code review changes
                //Added by Nikhil -  check for expense reserve
                iExcludeExpenseReserve = objClaim.Context.DbConnLookup.ExecuteInt(String.Format("SELECT CXP.EXCLUDE_EXPENSE_FLAG FROM CLAIM_X_POL_DED CXP WHERE CXP.CLAIM_ID = {0} AND CXP.POLCVG_ROW_ID={1}", p_iClaimId, p_iPolCvgID));

                //Start -  changed by Nikhil.Code review changes.
            
                sbSQL.Length = 0;
                //End -  changed by Nikhil.Code review changes.
                sbSQL.Append("SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE ");
                //Start -  changed by Nikhil.Code review changes.
           
                sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                sbSQL.Append(" AND CLAIMANT_EID = ").Append(p_iClaimantEID);
                sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = ").Append(p_iCvgLossId);
                sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
                //end -  changed by Nikhil.Code review changes.
                int iRcRowId = objClaim.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                if (iRcRowId > 0)
                {
                    //TO DO: Still need to include the case of non-applicable reserve type and exclude  expense reserve type.
                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                    //start -  changed by Nikhil.Code review changes.
                  
                    sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                    sbSQL.Append(" AND CLAIMANT_EID = ").Append(p_iClaimantEID);
                    sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = ").Append(p_iCvgLossId);
                    sbSQL.Append(" AND RESERVE_TYPE_CODE <> ").Append(iRecoveryResTypeCode);
                    //end -  changed by Nikhil.Code review changes.
                    //Start - Added by Nikhil -  Include applicable reserve type and check for expense reserve
                    if (iCovGroupId > 0)
                    {
                        //Changed by Nikhil.Code review changes.
                      
                        iCountMapReserveType = objClaim.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0}" , iCovGroupId));
                        if (iCountMapReserveType > 0)
                            //Changed by Nikhil.Code review changes.
                         
                        sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = ").Append(iCovGroupId).Append( ")");
                    }

                    if (iExcludeExpenseReserve == -1)
                    {
                        //Changed by Nikhil.Code review changes
                       
                        sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE")).Append(")");
                    }

                    //End - Added by Nikhil -  Include applicable reserve type and check for expense reserve

                    dSumParentReserveAmount = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                  
                    objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", false);
                    objReserveCurrent.MoveTo(iRcRowId);
                    objClaimXPolDed = (ClaimXPolDed)m_objDataModelFactory.GetDataModelObject("ClaimXPolDed", false);

                    objClaimXPolDed.MoveTo(iClaimXPolDedId);

                  
                    dSirDedAmt = p_dAmount;
                    dPercent = objClaimXPolDed.DimPercentNum;
                    iDimTypeCode = objClaimXPolDed.DiminishingTypeCode;

                    if (dSirDedAmt > 0 && dPercent < 100)
                    {
                        
                        if ((iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("B", "DIMINISHING_TYPE")) || (iDimTypeCode == objClaim.Context.LocalCache.GetCodeId("E", "DIMINISHING_TYPE")))
                        {
                            dReducedAmt = dSirDedAmt * ((100d - dPercent) / 100d);
                        }
                        else
                        {
                            dReducedAmt = dSirDedAmt;
                        }
                    }

                    //tanwar2 - Save this amount for future use
                    dDedReducedAmount = dReducedAmt;

                    if (iCovGroupId > 0)
                    {
                        sbSQL.Length = 0;
                        sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                        sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON RC.POLCVG_ROW_ID = CXPD.POLCVG_ROW_ID ");
                        sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                        
                        sbSQL.Append(" AND PXIG.COV_GROUP_CODE = ").Append(iCovGroupId);
                        sbSQL.Append(" AND RC.RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
                        //Do not include current deductible reserve.
                        sbSQL.Append(" AND RC.POLCVG_LOSS_ROW_ID <> ").Append(p_iCvgLossId);
                        //end - changed by Nikhil.Code review changes
                        dDedReserveOnGroup = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                        //Changed by Nikhil.Code review changes.
                       
                        dAggLimit = objClaim.Context.DbConnLookup.ExecuteDouble(String.Format("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = {0}",iCovGroupId));

                       
                        if (dAggLimit > 0)
                        {
                            dAggApplicableLimit = dAggLimit - dDedReserveOnGroup;
                        }
                        //End - Added by Nikhil to check for aggregate limit if greater then 0.
                    }





                    if (iCovGroupId > 0 && GetDeductiblePerEventFlag(iCovGroupId)) //Added Enable deductible per event flag check
                    {
                        sbSQL.Length = 0;
                        sbSQL.Append(" SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                        sbSQL.Append(" INNER JOIN CLAIM C ON RC.CLAIM_ID = C.CLAIM_ID ");
                        //Changed by Nikhil.Code review changes
                  
                        sbSQL.Append(" WHERE C.EVENT_ID = ").Append(objClaimXPolDed.EventId);
                        sbSQL.Append(" AND POLCVG_ROW_ID IN ");
                        sbSQL.Append(GetCoverageGroupQuery(objClaimXPolDed.EventId, iCovGroupId));
                        //Changed by Nikhil.Code review changes.
                       
                        sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
                        sbSQL.Append(" AND (POLCVG_LOSS_ROW_ID <> ").Append(objReserveCurrent.CoverageLossId);
                        sbSQL.Append(" OR CLAIMANT_EID <> ").Append(p_iClaimantEID).Append(") ");
                        dParentOtherDedReserve = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    }
                    else
                    { //To Do: Need to check if we'll need to include case of remaining aggregate here.
                        //tanwar2 - Calculate Deductible reserve on the current coverage but different loss type
                        sbSQL.Length = 0;
                        sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                       
                        sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                        sbSQL.Append(" AND POLCVG_ROW_ID = ").Append(objReserveCurrent.CoverageId);
                        sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(iRecoveryResTypeCode);
                        //End - Changed by Nikhil.Code review changes.
                        sbSQL.Append(" AND (POLCVG_LOSS_ROW_ID <> ").Append(objReserveCurrent.CoverageLossId);
                        sbSQL.Append(" OR CLAIMANT_EID <> ").Append(p_iClaimantEID).Append(") ");
                        dParentOtherDedReserve = objClaim.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    }

                    dReducedAmt = dReducedAmt - dParentOtherDedReserve;
                    //Start - Added by Nikhil to check for aggregate limit if greater then 0.
                 
                        if (iCovGroupId > 0 && dAggApplicableLimit < dReducedAmt && dAggLimit > 0)
                    //end - Added by Nikhil to check for aggregate limit if greater then 0.
                    {
                        dReducedAmt = dAggApplicableLimit;
                    }
                    if (dReducedAmt < 0d)
                    {
                        dReducedAmt = 0d;
                    }

                    if (objReserveCurrent.ReserveAmount != dReducedAmt)
                    {

                        if (dReducedAmt > dSumParentReserveAmount && objReserveCurrent.ReserveAmount != dSumParentReserveAmount) //Still need to include the case of diminshing deductible
                        {
                            //tanwar2 - Redistribution is possible only when the deductible reserve amount is decreasing.
                           
                            bRedistributePossible = true;
                            dChangeAmount = objReserveCurrent.ReserveAmount - dSumParentReserveAmount;
                            //}

                            objReserveCurrent.ReserveAmount = dSumParentReserveAmount;
                            objReserveCurrent.sUpdateType = "Reserves";
                            objReserveCurrent.iUserId = m_userLogin.UserId;
                            objReserveCurrent.iGroupId = m_userLogin.GroupId;
                            objReserveCurrent.EnteredByUser = objClaim.Context.RMUser.LoginName;
                            //tanwar2 - Deductible Reserve status should not change on update of reserve
                            //objReserveCurrent.ResStatusCode = iStatusCode;
                            objReserveCurrent.iForcedReserveStatusCode = objReserveCurrent.ResStatusCode;
                            objReserveCurrent.Reason = sReason;
                            objReserveCurrent.Save();
                        }
                        // Do we need condition at this point?

                        else if (dReducedAmt <= dSumParentReserveAmount && objReserveCurrent.ReserveAmount != dReducedAmt)
                        {
                           
                            objReserveCurrent.ReserveAmount = dReducedAmt;
                            objReserveCurrent.sUpdateType = "Reserves";
                            objReserveCurrent.iUserId = m_userLogin.UserId;
                            objReserveCurrent.iGroupId = m_userLogin.GroupId;
                            objReserveCurrent.EnteredByUser = objClaim.Context.RMUser.LoginName;
                            objReserveCurrent.ResStatusCode = iStatusCode;
                            objReserveCurrent.Reason = sReason;
                            objReserveCurrent.Save();

                        }
                    }
                }
                //tanwar2 - Add validation warnings - start
                foreach (System.Collections.DictionaryEntry objWarnings in objReserveCurrent.Context.ScriptWarningErrors)
                {
                    p_objScriptErrOut.Add(p_objScriptErrOut.Count + 1, objWarnings.Value.ToString());
                }
                //tanwar2 - Add validation warnings - end

                //tanwar2 - Redistribute deductible reserve
                //Start - Commented by Nikhil on 07/10/14
             
                if (bRedistributePossible && dChangeAmount > 0)
                //end - Commented by Nikhil on 07/10/14
                {
                    RedistributeDeductibleReserve(iRecoveryResTypeCode, p_iPolCvgID, p_iClaimId, p_iCvgLossId, iRcRowId, dDedReducedAmount, objClaimXPolDed.ExcludeExpenseFlag, dChangeAmount, iCovGroupId, objClaimXPolDed.EventId);
                }
            }
            //rupal:start, MITS 27373
            catch (RMAppException p_objEx)
            {
                foreach (System.Collections.DictionaryEntry objErrors in objReserveCurrent.Context.ScriptValidationErrors)//MITS 30380 starts
                    p_objScriptErrOut.Add("ValidationScriptError", objErrors.Value.ToString());//MITS 30380 ends

                throw p_objEx;
            }//rupal:end
            catch (Exception objException)
            {
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("ReserveFunds.EditReserve.GenericError",m_iClientId), objException);
            }
            finally
            {
                if (objReserveCurrent == null)
                {
                    objReserveCurrent.Dispose();
                    objReserveCurrent = null;
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objClaimXPolDed != null)
                {
                    objClaimXPolDed.Dispose();
                    objClaimXPolDed = null;
                }
                //Changed by Nikhil.Code review changes.
                if (sbSQL != null)
                {
                    sbSQL = null;
                }
            }

        }

        #endregion

        #region Overloaded public GetClaimReserves (p_iClaimId, p_iClaimantEID,p_iUnitID, p_iLOB)

        /// Name		: GetClaimReserves
        /// Author		: Navneet Sota
        /// Date Created: 11/08/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This is an overloaded method basically used for get the Claim reserves associated with a 
        /// particular claim
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_iClaimid">The Claim ID</param>
        /// <param name="p_iClaimantEID">The Claimant ID</param>
        /// <param name="p_iUnitID">The Unit ID.</param>
        /// <param name="p_iLOB">The Line of Business</param>
        //pmittal5 MITS 9906 04/11/08
        //public XmlDocument GetClaimReserves (int p_iClaimId, int p_iClaimantEID,  int p_iUnitID, int p_iLOB)
        public XmlDocument GetClaimReserves(int p_iClaimId, int p_iClaimantEID, int p_iUnitID, int p_iLOB, string sFormTitle,int p_iCurrType)
        {
            return GetClaimReserves(p_iClaimId, p_iClaimantEID, p_iUnitID, p_iLOB, sFormTitle, false, p_iCurrType);//Deb
            //return GetClaimReserves(p_iClaimId, p_iClaimantEID, p_iUnitID, p_iLOB, false);			
        }
        #endregion

        //#region public ModifyReserves (p_iClaimId, p_iClaimantEID,  p_iUnitID, p_iReserveTypeCode, p_sDateEntered, p_dAmount, p_iStatusCodeID, p_sUSer, p_sReason,p_iUserId, p_iGroupId)
        ///// Name		: ModifyReserves
        ///// Author		: Navneet Sota
        ///// Date Created: 11/08/2004
        ///// ************************************************************
        ///// Amendment History
        ///// ************************************************************
        ///// Date Amended   *   Amendment   *    Author
        ///// ************************************************************
        ///// <summary>		
        ///// This method is basically used for adding/modifying the Claim reserves associated with a 
        ///// particular claim
        ///// </summary>
        ///// <remarks>		
        ///// </remarks>
        ///// <param name="p_iClaimid">The Claim ID</param>
        ///// <param name="p_iClaimantEID">The Claimant ID</param>
        ///// <param name="p_iUnitID">The Unit ID.</param>
        ///// <param name="p_iReserveTypeCode"></param>
        ///// <param name="p_sDateEntered"> Date when the reserve is being added or modified</param>
        ///// <param name="p_dAmount"> Amount being added or modified</param>
        ///// <param name="p_iStatusCodeID"></param>
        ///// <param name="p_sUser"> User Modifying/Adding the reserve</param>
        ///// <param name="p_sReason"> Reason for Modifying/Adding the reserve</param>
        ///// <param name="p_iUserId"> User-Id</param>
        ///// <param name="p_iGroupId"> Group-Id</param>

        //public bool ModifyReserves(int p_iClaimId, int p_iClaimantEID, int p_iUnitID, int p_iReserveTypeCode,
        //    string p_sDateEntered, double p_dAmount, int p_iStatusCodeID, string p_sUser, string p_sReason,
        //    int p_iUserId, int p_iGroupId)
        //{

        //    //String Objects
        //    StringBuilder sbSQL = null;
        //    StringBuilder sbTempSQL = null;

        //    //String variables
        //    string sDateOfUpdate = "";
        //    string sTempDate = "";

        //    //Integer variables
        //    int iLOB = 0;
        //    int iCRC = 0;
        //    int iRowId = 0;
        //    int iResCurrentGUID = 0;
        //    int iResHistoryGUID = 0;

        //    //Double variables
        //    double dChangeAmount = 0;
        //    double dCurrentReserve = 0;
        //    double dPaidtotal = 0;
        //    double dCollectTotal = 0;
        //    double dBalance = 0;
        //    double dIncurred = 0;
        //    double dNewAmount = 0;
        //    double dPreviousAmount = 0;
        //    double dAmountPaid = 0;//pmahli MITS 11038 12/25/2007

        //    //Boolean variables
        //    bool bCollInRsvBal = false;
        //    //Shruti for MITS 8551
        //    bool bCollInIncurredBal = false;
        //    bool bResLimit = false;
        //    bool bPrevResBackdating = false;
        //    bool bPrevReservesZero = false;
        //    bool bPreventReservesLessThanPaid = false; //Nitesh TR:001293 23jan2006
        //    bool bNewRecord = false;
        //    bool bReturnValue = false;

        //    //Database objects
        //    DataSet objDataSet = null;
        //    DbReader objReader = null;
        //    DbTransaction objTrans = null;
        //    DbConnection objConn = null;
        //    DbCommand objCommand = null;

        //    // First get security IDs and check security
        //    GetSecurityIDS(p_iClaimId, p_iClaimantEID, p_iUnitID, 0);

        //    // Check security and fail if sufficient privilege doesn't exist
        //    if (!m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_ACCESS))
        //    {
        //        throw new PermissionViolationException(RMPermissions.RMO_ACCESS, m_iSecurityId);
        //    }
        //    if (!m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
        //    {
        //        throw new PermissionViolationException(RMPermissions.RMO_UPDATE, m_iSecurityId);
        //    }

        //    try // Main Try Block
        //    {
        //        //Instantiate the sbSQL object for appending the SQL's	
        //        sbSQL = new StringBuilder();
        //        sbTempSQL = new StringBuilder();

        //        //Table[0]//Nitesh :TR 001293-Added PREV_RES_BELOWPAID in Query for Prevent reserve amount less paid than 23jan2006  
        //        sbSQL.Append("SELECT PREV_RES_BACKDATING,PREV_RES_MODIFYZERO,PREV_RES_BELOWPAID FROM SYS_PARMS");

        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                bPrevResBackdating = Conversion.ConvertLongToBool
        //                    (Conversion.ConvertStrToLong
        //                    (objReader.GetValue("PREV_RES_BACKDATING").ToString()));

        //                bPrevReservesZero = Conversion.ConvertLongToBool
        //                    (Conversion.ConvertStrToLong
        //                    (objReader.GetValue("PREV_RES_MODIFYZERO").ToString()));
        //                //Nitesh :TR 001293-Added Prevent reserve amount less paid than 23jan2006  -Starts
        //                bPreventReservesLessThanPaid = Conversion.ConvertLongToBool
        //                    (Conversion.ConvertStrToLong
        //                    (objReader.GetValue("PREV_RES_BELOWPAID").ToString()));
        //                //Nitesh :TR 001293-Added Prevent reserve amount less paid than 23jan2006 -Ends
        //            }
        //            objReader.Close();

        //        }
        //        sbSQL.Remove(0, sbSQL.Length);

        //        sbSQL.Append(" SELECT CLAIM.LINE_OF_BUS_CODE  LOB,RES_LMT_FLAG,COLL_IN_RSV_BAL,COLL_IN_INCUR_BAL ");
        //        sbSQL.Append(" FROM SYS_PARMS_LOB,CLAIM WHERE CLAIM.CLAIM_ID = " + p_iClaimId);
        //        sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = SYS_PARMS_LOB.LINE_OF_BUS_CODE ");

        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                //Get the value of "Line Of Business"
        //                iLOB = Conversion.ConvertStrToInteger
        //                    (objReader.GetValue("LOB").ToString());

        //                bResLimit = Conversion.ConvertLongToBool
        //                    (Conversion.ConvertStrToLong
        //                    (objReader.GetValue("RES_LMT_FLAG").ToString()));

        //                bCollInRsvBal = Conversion.ConvertLongToBool
        //                    (Conversion.ConvertStrToLong
        //                    (objReader.GetValue("COLL_IN_RSV_BAL").ToString()));

        //                //Shruti for MITS 8551
        //                bCollInIncurredBal = Conversion.ConvertLongToBool(Conversion.ConvertStrToLong(
        //                    objReader.GetValue("COLL_IN_INCUR_BAL").ToString()));
        //            }
        //            objReader.Close();

        //        }
        //        sbSQL.Remove(0, sbSQL.Length);

        //        //Check if Back Date Reserve Entry is allowed.
        //        if (bPrevResBackdating &&
        //                (DateTime.Parse(p_sDateEntered) != DateTime.Parse(DateTime.Now.ToShortDateString())))
        //        {
        //            bReturnValue = false;
        //            throw new RMAppException(Globalization.GetString("ReserveFunds.ModifyReserves.DateError"));
        //        }

        //        //only make this check when reserves are directly being updated
        //        //Check if Date exceeds todays date.
        //        if (DateTime.Parse(p_sDateEntered) > DateTime.Parse(DateTime.Now.ToString()) && p_sReason != "Insuff. Reserves")
        //        {
        //            bReturnValue = false;
        //            throw new RMAppException(Globalization.GetString("ReserveFunds.ModifyReserves.DateErr"));
        //        }

        //        //Check if Zero amount is allowed as Reserve.
        //        //MITS 9515: Rmx forces reserves to be greater than zero..It should only give a warning
        //        /*
        //        if (bPrevReservesZero && p_dAmount == 0.0)
        //        {
        //            bReturnValue = false;
        //            throw new RMAppException(Globalization.GetString("ReserveFunds.ModifyReserves.AmountErr"));
        //        }
        //        */

        //        //Check if user is allowed to set the particular Reserve Limits
        //        if (bResLimit && CheckReserveLimits(p_iClaimId, p_iReserveTypeCode, p_dAmount, p_iUserId, p_iGroupId))
        //        {
        //            bReturnValue = false;
        //            throw new RMAppException(Globalization.GetString("ReserveFunds.ModifyReserves.PermissionErr"));
        //        }

        //        //Check if a reserve record exists already
        //        sbSQL.Append(" SELECT RESERVE_AMOUNT FROM RESERVE_CURRENT WHERE ");
        //        sbSQL.Append(" CLAIM_ID = " + p_iClaimId);
        //        sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);
        //        sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);
        //        sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);

        //        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //        if (objReader != null)
        //        {
        //            if (objReader.Read())
        //            {
        //                dCurrentReserve = Conversion.ConvertStrToDouble(objReader.GetValue("RESERVE_AMOUNT").ToString());
        //                dPaidtotal = Conversion.Round(GetFundsAmount(p_iClaimId, p_iClaimantEID, p_iUnitID, p_iReserveTypeCode, true), 2);
        //                dCollectTotal = GetFundsAmount(p_iClaimId, p_iClaimantEID, p_iUnitID, p_iReserveTypeCode, false);
        //                //pmahli MITS 11038 12/25/2007
        //                //Amount paid is calculated
        //                dAmountPaid = dPaidtotal - dCollectTotal;
        //            }
        //            else
        //                bNewRecord = true;

        //            objReader.Close();

        //        }
        //        sbSQL.Remove(0, sbSQL.Length);
        //        //Nitesh :TR 001293-Added Prevent reserve amount less paid than 23jan2006  -Starts
        //        //pmahli MITS 11038 12/25/2007 
        //        //p_dAmount to be compared with dAmountPaid instead of dPaidTotal 
        //        //if (bPreventReservesLessThanPaid && (p_dAmount < dPaidtotal))- commented by pmahli
        //        if (bPreventReservesLessThanPaid && (p_dAmount < dAmountPaid))
        //        {
        //            bReturnValue = false;
        //            throw new RMAppException(Globalization.GetString("ReserveFunds.ModifyReserves.LessThanPaidErr"));
        //        }
        //        //Nitesh :TR 001293-Added Prevent reserve amount less paid than 23jan2006  -Ends
        //        //Format values for insert statement
        //        if (p_sReason.Length > 22)
        //            p_sReason = p_sReason.Substring(0, 22);
        //        //Mjain8 Added MITS 7735
        //        p_sReason = p_sReason.Replace("'", "''");
        //        if (p_sUser.Length > 8)
        //            p_sUser = p_sUser.Substring(0, 8);

        //        dChangeAmount = p_dAmount - dCurrentReserve;

        //        //Format the Dates in YYYYMMDD format
        //        p_sDateEntered = Conversion.ToDbDate(Convert.ToDateTime(p_sDateEntered));
        //        sDateOfUpdate = Conversion.ToDbDateTime(System.DateTime.Now);

        //        //Compute balance and incurred figures
        //        dBalance = CalculateReserveBalance(p_dAmount, dPaidtotal, dCollectTotal, bCollInRsvBal);

        //        //BSB 11.10.2006 Fix Reserve Calculations
        //        dIncurred = CalculateIncurred(dBalance, dPaidtotal, dCollectTotal, bCollInRsvBal, bCollInIncurredBal); //Shruti for MITS 8551
        //        //dIncurred = CalculateIncurred(p_dAmount,dPaidtotal,dCollectTotal,bCollInRsvBal);

        //        //Insert or update record
        //        if (bNewRecord)
        //        {
        //            //Get new GUID for RESERVE_CURRENT
        //            iResCurrentGUID = Utilities.GetNextUID(m_sConnectionString, "RESERVE_CURRENT");

        //            //Table[0]
        //            sbSQL.Append(" INSERT INTO RESERVE_CURRENT(RC_ROW_ID,CLAIM_ID,CLAIMANT_EID,UNIT_ID, ");
        //            sbSQL.Append(" RESERVE_TYPE_CODE,RESERVE_AMOUNT, BALANCE_AMOUNT, INCURRED_AMOUNT, ");
        //            sbSQL.Append(" PAID_TOTAL, COLLECTION_TOTAL,DATE_ENTERED,ENTERED_BY_USER,DTTM_RCD_ADDED,");
        //            sbSQL.Append(" ADDED_BY_USER, REASON,DTTM_RCD_LAST_UPD,UPDATED_BY_USER,CRC,RES_STATUS_CODE)");
        //            sbSQL.Append(" VALUES (" + iResCurrentGUID + ", " + p_iClaimId + ", " + p_iClaimantEID + ", " + p_iUnitID);
        //            sbSQL.Append(", " + p_iReserveTypeCode + ", " + p_dAmount + ", " + dBalance + ", " + dIncurred);
        //            sbSQL.Append(", " + dPaidtotal + ", " + dCollectTotal + ", '" + p_sDateEntered + "', '" + p_sUser);
        //            sbSQL.Append("', '" + sDateOfUpdate + "', '" + p_sUser + "' , '" + p_sReason + "', '" + sDateOfUpdate);
        //            sbSQL.Append("', '" + p_sUser + "', " + iCRC + ", " + p_iStatusCodeID + ") ");

        //            if (objConn == null)
        //                objConn = DbFactory.GetDbConnection(m_sConnectionString);

        //            objConn.Open();
        //            objConn.ExecuteNonQuery(sbSQL.ToString());
        //            objConn.Close();

        //        }//end if (bNewRecord)
        //        else
        //        {
        //            //Table[0]
        //            sbTempSQL.Append("SELECT MAX(DATE_ENTERED)  DTM_ENTERED FROM RESERVE_HISTORY ");
        //            sbTempSQL.Append(" WHERE CLAIM_ID=" + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
        //            sbTempSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID + " AND UNIT_ID = " + p_iUnitID);

        //            objReader = DbFactory.GetDbReader(m_sConnectionString, sbTempSQL.ToString());
        //            if (objReader != null)
        //            {
        //                if (objReader.Read())
        //                    sTempDate = objReader.GetValue("DTM_ENTERED").ToString();

        //                objReader.Close();
        //                objReader = null;
        //            }
        //            sbTempSQL.Remove(0, sbTempSQL.Length);

        //            //Table[0]
        //            if (Conversion.ToDate(p_sDateEntered) >= Conversion.ToDate(sTempDate))
        //            {
        //                sbSQL.Append(" UPDATE RESERVE_CURRENT SET ");
        //                sbSQL.Append(" RESERVE_AMOUNT = " + p_dAmount + ", ");
        //                sbSQL.Append(" REASON = '" + p_sReason + "', ");
        //                sbSQL.Append(" DATE_ENTERED = '" + p_sDateEntered + "', ");
        //                sbSQL.Append(" ENTERED_BY_USER = '" + p_sUser + "', ");
        //                sbSQL.Append(" DTTM_RCD_LAST_UPD = '" + sDateOfUpdate + "', ");
        //                sbSQL.Append(" UPDATED_BY_USER = '" + p_sUser + "', ");
        //                sbSQL.Append(" CRC = " + iCRC + ", ");
        //                sbSQL.Append(" PAID_TOTAL = " + dPaidtotal + ", COLLECTION_TOTAL = " + dCollectTotal + ", ");
        //                sbSQL.Append(" INCURRED_AMOUNT = " + dIncurred + ", BALANCE_AMOUNT = " + dBalance + ", ");
        //                sbSQL.Append(" RES_STATUS_CODE = " + p_iStatusCodeID);
        //                sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
        //                sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID + " AND UNIT_ID = " + p_iUnitID);

        //                if (objConn == null)
        //                    objConn = DbFactory.GetDbConnection(m_sConnectionString);

        //                objConn.Open();
        //                objConn.ExecuteNonQuery(sbSQL.ToString());
        //                objConn.Close();
        //            }
        //        }//end if (!bNewRecord)

        //        sbSQL.Remove(0, sbSQL.Length);
        //        objDataSet = null;

        //        //Log changes in RESERVE_HISTORY table
        //        //Get new GUID for RESERVE_HISTORY
        //        iResHistoryGUID = Utilities.GetNextUID(m_sConnectionString, "RESERVE_HISTORY");

        //        sbSQL.Append(" INSERT INTO RESERVE_HISTORY(RSV_ROW_ID,CLAIM_ID,CLAIMANT_EID,UNIT_ID, ");
        //        sbSQL.Append(" RESERVE_TYPE_CODE, RESERVE_AMOUNT, CHANGE_AMOUNT, BALANCE_AMOUNT, ");
        //        sbSQL.Append(" INCURRED_AMOUNT, PAID_TOTAL, COLLECTION_TOTAL, DATE_ENTERED,ENTERED_BY_USER, ");
        //        sbSQL.Append(" DTTM_RCD_ADDED,ADDED_BY_USER,REASON,DTTM_RCD_LAST_UPD,UPDATED_BY_USER,CRC,RES_STATUS_CODE) ");
        //        sbSQL.Append(" VALUES (" + iResHistoryGUID + ", " + p_iClaimId + ", " + p_iClaimantEID);
        //        sbSQL.Append(", " + p_iUnitID + ", " + p_iReserveTypeCode + ", " + p_dAmount);
        //        sbSQL.Append(", " + dChangeAmount + ", " + dBalance + ", " + dIncurred + ", " + dPaidtotal + ", " + dCollectTotal + ", '");
        //        sbSQL.Append(p_sDateEntered + "', '" + p_sUser + "','" + sDateOfUpdate + "', '");
        //        sbSQL.Append(p_sUser + "', '" + p_sReason + "', '" + sDateOfUpdate + "', '" + p_sUser);
        //        sbSQL.Append("', " + iCRC + ", " + p_iStatusCodeID + ")");

        //        if (objConn == null)
        //            objConn = DbFactory.GetDbConnection(m_sConnectionString);

        //        objConn.Open();
        //        objConn.ExecuteNonQuery(sbSQL.ToString());
        //        objConn.Close();
        //        sbSQL.Remove(0, sbSQL.Length);

        //        //Now update change amounts for all reserve history records for this claim / reserve type
        //        sbTempSQL.Append("SELECT RSV_ROW_ID,RESERVE_AMOUNT,DATE_ENTERED FROM RESERVE_HISTORY ");
        //        sbTempSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId + " AND CLAIMANT_EID = " + p_iClaimantEID);
        //        sbTempSQL.Append(" AND UNIT_ID = " + p_iUnitID + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
        //        sbTempSQL.Append(" ORDER BY DATE_ENTERED,RSV_ROW_ID");

        //        objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbTempSQL.ToString());
        //        if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
        //        {
        //            objConn = DbFactory.GetDbConnection(m_sConnectionString);
        //            objConn.Open();
        //            objCommand = objConn.CreateCommand();
        //            objTrans = objConn.BeginTransaction();
        //            objCommand.Transaction = objTrans;

        //            for (int iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
        //            {
        //                dNewAmount = Conversion.ConvertStrToDouble(
        //                    objDataSet.Tables[0].Rows[iCnt]["RESERVE_AMOUNT"].ToString());
        //                iRowId = Conversion.ConvertStrToInteger(
        //                    objDataSet.Tables[0].Rows[iCnt]["RSV_ROW_ID"].ToString());
        //                dChangeAmount = dNewAmount - dPreviousAmount;

        //                objCommand.CommandText = "UPDATE RESERVE_HISTORY SET CHANGE_AMOUNT = " + dChangeAmount +
        //                                        " WHERE RSV_ROW_ID = " + iRowId;
        //                objCommand.ExecuteNonQuery();

        //                dPreviousAmount = dNewAmount;
        //            }

        //            //Success: Commit Transaction
        //            objTrans.Commit();
        //            objConn.Close();

        //            bReturnValue = true;
        //        }
        //        sbTempSQL.Remove(0, sbTempSQL.Length);
        //        objDataSet = null;

        //    }//End Main Try
        //    catch (RMAppException p_objRmAEx)
        //    {
        //        throw p_objRmAEx;
        //    }
        //    catch (Exception objException)
        //    {
        //        throw new RMAppException(Globalization.GetString("ReserveFunds.ModifyReserves.DataErr"), objException);
        //    }
        //    finally
        //    {
        //        sbSQL = null;
        //        sbTempSQL = null;

        //        if (objTrans != null)
        //            objTrans.Dispose();

        //        if (objDataSet != null)
        //            objDataSet.Dispose();

        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objConn != null)
        //        {
        //            objConn.Close();
        //            objConn.Dispose();
        //        }
        //    }// End finally

        //    //Return the Result to calling function
        //    return bReturnValue;
        //}//End Function

        //#endregion

        #region public GetReservesXML (p_iClaimId, p_iClaimantEID, p_iUnitID, p_iReserve)

        /// Name		: GetReservesXML
        /// Author		: Navneet Sota
        /// Date Created: 11/16/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for forming the XML to be used in "Modify Reserves page"
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_iClaimId">Claim ID for particular reserve</param>
        /// <param name="p_iClaimantEID">Claimant ID for particular reserve</param>
        /// <param name="p_iUnitID">Unit ID </param>
        /// <param name="p_iReserve">Reserve Type Code</param>


        public void GetReservesXML(ref XmlDocument objXML, int p_iClaimId, int p_iClaimantEID, int p_iUnitID, int p_iReserve, string p_sUserName, int p_iPolicyID, int p_iPolCvgID ,int p_iCvgLossId)
        {
            //XML objects
            XmlElement objXMLResCodeElement = null;
            XmlElement objXMLResTextElement = null;
            XmlElement objXMLParentEle = null;
            XmlElement objXMLNodeEle = null;
            SysSettings objSysSettings = null;
            //String Objects
            StringBuilder sbSQL = null;


            //String Variables
            string sSelect = "";
            string sFrom = "";
            string sWhere = "";
            string sOrderBy = "";
            string sClaim = "";
            string sClaimant = "";
            string sUnit = "";
            string sReserve = "";
            string sText = "";
            string sDate = "";
            string sShortCode = "";
            string sTemp = "";

            //bool Variable
            bool bHideForm = false;

            //Database objects
            DbReader objReader = null;

            //pmittal5  MITS:11756  06/03/08
            string sDateOfEvent = "";         //Date of Event
            string sDateOfClaim = "";         //Date of Claim
            string sDateOfPolicy = "";        //Effective Date of Policy attached
            string sToday = "";               //Current Date
            int iMonthNow = 0;      		  //Current Month
            int iDay = 0;					  //Current Day
            //End - pmittal5

            //Deb Multi Currency
            XmlElement objCurrencytype = null;
            XmlElement objCurrencytypeID = null;//MITS:34082
            bool IsBaseCurrencySelected = false;
            int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(p_iClaimId, m_sConnectionString);
            LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId);
            if (iClaimCurrCode > 0)
            {
                using (DbReader rdrCurr = DbFactory.ExecuteReader(m_sConnectionString, "SELECT SHORT_CODE,CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iClaimCurrCode))
                {
                    while (rdrCurr.Read())
                    {
                        CreateElement(objXML.DocumentElement, "CurrencyType", ref objCurrencytype);
                        CreateElement(objXML.DocumentElement, "CurrencyTypeID", ref objCurrencytypeID);//MITS:34082
                        objCurrencytype.InnerText = Conversion.ConvertObjToStr(rdrCurr.GetValue("SHORT_CODE")) + "_" + Conversion.ConvertObjToStr(rdrCurr.GetValue("CODE_DESC"));
                        objCurrencytypeID.InnerText = iClaimCurrCode.ToString();//MITS:34082
                        eNavType = CommonFunctions.NavFormType.Reserve;
                    }
                }
            }
            else
            {
                int iBaseCurrCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                if (iBaseCurrCode > 0)
                {
                    using (DbReader rdrBCurr = DbFactory.ExecuteReader(m_sConnectionString, "SELECT SHORT_CODE,CODE_DESC FROM CODES_TEXT WHERE CODE_ID=" + iBaseCurrCode))
                    {
                        while (rdrBCurr.Read())
                        {
                            CreateElement(objXML.DocumentElement, "CurrencyType", ref objCurrencytype);
                            CreateElement(objXML.DocumentElement, "CurrencyTypeID", ref objCurrencytypeID);//MITS:34082
                            objCurrencytype.InnerText = Conversion.ConvertObjToStr(rdrBCurr.GetValue("SHORT_CODE")) + "_" + Conversion.ConvertObjToStr(rdrBCurr.GetValue("CODE_DESC"));
                            objCurrencytypeID.InnerText = iBaseCurrCode.ToString();//MITS:34082
                        }
                    }
                }
                IsBaseCurrencySelected = true;
            }
            //Deb


            // First get security IDs and check security
            GetSecurityIDS(p_iClaimId, p_iClaimantEID, p_iUnitID, 0);

            // Check security and fail if sufficient privilege doesn't exist
            if (!m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_ACCESS))
            {
                throw new PermissionViolationException(RMPermissions.RMO_ACCESS, m_iSecurityId);
            }
            if (!m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_VIEW)
                && !m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
            {
                if (!m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_VIEW))
                    throw new PermissionViolationException(RMPermissions.RMO_VIEW, m_iSecurityId);
                else
                    throw new PermissionViolationException(RMPermissions.RMO_UPDATE, m_iSecurityId);
            }

            // Determine read-only status based on security permissions (read only can also be triggered by closed claim status)
            if (!m_userLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                bHideForm = true;

            //MGaba2: R6 Reserve WorkSheet:Start
            //Reserve will be set from Reserve WorkSheet if Use Reserve WorkSheet chkbox is checked in LOB Paremeters
            sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = '" + p_iClaimId + "'");
            int iLOBCode = 0;
            int iUseRsvWk = 0;
            //Get the Data
            objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    iLOBCode = Conversion.ConvertStrToInteger(objReader.GetValue("LINE_OF_BUS_CODE").ToString());
                }
                objReader.Close();

            }

            sbSQL.Remove(0, sbSQL.Length);

            sbSQL.Append(" SELECT USE_RSV_WK FROM SYS_PARMS_LOB ");
            sbSQL.Append(" WHERE LINE_OF_BUS_CODE = " + iLOBCode);
            objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    //Set the value for ShowLOBRSW
                    iUseRsvWk = Conversion.ConvertStrToInteger(objReader.GetValue("USE_RSV_WK").ToString());
                    //Raman 03/08/2010 : MITS 19953: In case Reserve Worksheet is not used still module security on legacy reserves should dictate this variable value
                    //Hence commenting else block
                    if (iUseRsvWk != 0)
                        bHideForm = true;
                    //else
                    //    bHideForm = false;
                }
                objReader.Close();
            }
            sbSQL.Remove(0, sbSQL.Length);

            //MGaba2: R6 Reserve WorkSheet:End
            try
            {
                //If the Reserve Tracking is set to detail Level and we are on a gc/va screen, then don't display the page and throw error.
                if ((GetReserveTracking(p_iClaimId, ref sShortCode) == 1) && p_iClaimantEID == 0 && p_iUnitID == 0)
                    throw new RMAppException(Globalization.GetString("ReserveFunds.GetReserveTracking.ClaimantLevelReserveMsg", m_iClientId) + sShortCode);
                else
                {
                    //Instantiate the sbSQL object for appending the SQL's	
                    sbSQL = new StringBuilder();

                    //Condition to be checked to see if the claim is readonly, if true then
                    //display only reserve history part else show full form.

                    sbSQL.Append(" SELECT CODES.RELATED_CODE_ID FROM CODES,CLAIM WHERE CLAIM.CLAIM_ID = '" + p_iClaimId);
                    sbSQL.Append(" ' AND CLAIM.CLAIM_STATUS_CODE=CODES.CODE_ID");

                    //Get the Data
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            if (Conversion.ConvertStrToInteger(objReader.GetValue("RELATED_CODE_ID").ToString()) == 8)
                                bHideForm = true;
                        }
                        objReader.Close();

                    }
                    sbSQL.Remove(0, sbSQL.Length);

                    //Get the Claim, Claimant, Unit, Reserve Desc
                    //Table[0] Claim
                    sbSQL.Append(" SELECT CLAIM_NUMBER FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId);
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        //Claim
                        if (objReader.Read())
                            sClaim = objReader.GetValue("CLAIM_NUMBER").ToString();
                        objReader.Close();

                    }
                    sbSQL.Remove(0, sbSQL.Length);

                    //Claimant
                    sbSQL.Append(" SELECT LAST_NAME,FIRST_NAME FROM CLAIMANT,ENTITY ");
                    sbSQL.Append(" WHERE CLAIMANT.CLAIM_ID = " + p_iClaimId);
                    sbSQL.Append(" AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID");

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            //Claimant
                            sClaimant = objReader.GetValue("LAST_NAME").ToString();
                            if (objReader.GetValue("FIRST_NAME").ToString() != "")
                                sClaimant = sClaimant + ", " + objReader.GetValue("FIRST_NAME").ToString();
                        }
                        objReader.Close();

                    }
                    sbSQL.Remove(0, sbSQL.Length);

                    //Unit
                    if (p_iUnitID != 0)
                    {
                        sbSQL.Append(" SELECT VEHICLE_MAKE,VEHICLE_MODEL,VIN FROM VEHICLE WHERE UNIT_ID = " + p_iUnitID);
                        // else
                        //	sbSQL.Append(" SELECT 1  VEHICLE_MAKE, 2  VEHICLE_MODEL, 3  VIN FROM VEHICLE ");

                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                if (p_iUnitID != 0 && objReader.GetValue("VEHICLE_MAKE") != null)
                                {
                                    sUnit = objReader.GetValue("VEHICLE_MAKE").ToString();
                                    sUnit = sUnit + " " + objReader.GetValue("VEHICLE_MODEL").ToString();
                                    if (objReader.GetValue("VIN").ToString() != "")
                                        sUnit = sUnit + " VIN: " + objReader.GetValue("VIN").ToString();
                                }
                            }
                            objReader.Close();

                        }
                        sbSQL.Remove(0, sbSQL.Length);
                    }
                    //Aman ML Changes -- Commented the below code
                    //Reserve Desc
                    //sbSQL.Append(" SELECT CODE_DESC FROM CODES_TEXT WHERE CODE_ID = " + p_iReserve);
                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    //if (objReader != null)
                    //{
                    //    if (objReader.Read())
                    //        sReserve = objReader.GetValue("CODE_DESC").ToString();
                    //    objReader.Close();

                    //}
                    //sbSQL.Remove(0, sbSQL.Length);
                    sReserve = objCache.GetCodeDesc(p_iReserve, this.LanguageCode);  //Aman ML Change-- End

                    objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves");
                    objXMLParentEle.SetAttribute("LoginUser", p_sUserName);
                    objXMLParentEle.SetAttribute("Claim", sClaim);
                    objXMLParentEle.SetAttribute("Claimant", sClaimant);
                    objXMLParentEle.SetAttribute("Unit", sUnit);
                    objXMLParentEle.SetAttribute("ReserveDesc", sReserve);
                    objXMLParentEle.SetAttribute("HideForm", bHideForm.ToString());

                    //Status Dropdown values
                    if (bHideForm) // Just to make sure table[4] gets generated
                        sbSQL.Append(" SELECT 1,2,3 ");
                    else
                        sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");

                    sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND CODES.DELETED_FLAG = 0 ");// Nitesh RM/RMX Gaps MITS-5992
                    //rsharma220 MITS 32331 Start
                    sbSQL.Append(" AND CODES.RELATED_CODE_ID NOT IN ( SELECT CODES.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_STATUS_PARENT' ");
                    sbSQL.Append(" AND CODES.SHORT_CODE IN ('A','H','Re','F', 'Cn') ) ");
                    //rsharma220 MITS 32331 End
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_STATUS' ");
                    sbSQL.Append(" AND CODES.SHORT_CODE NOT IN ('A','R','H')");  //mbahl3 Reserve Approval Enhancement Mits 32471  line commented by asharma2326 for jira 10569
                    sbSQL.Append(" ORDER BY CODES.SHORT_CODE ");
                    if (!bHideForm)
                        sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_STATUS", this.LanguageCode); //Aman ML Change
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("Name"), true);
                        objXMLNodeEle.SetAttribute("value", "");
                        //for R5.. Raman Bhatia: changing format of XML to enable autobinding
                        //objXMLNodeEle.SetAttribute("Text","");
                        objXMLNodeEle.SetAttribute("Selected", "");

                        objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves/Dropdowns/Status");
                        objXMLParentEle.AppendChild(objXMLNodeEle);

                        while (objReader.Read())
                        {
                            //Create the nodes for Status dropdown 
                            if (!bHideForm)
                            {
                                objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("Name"), true);

                                //objXMLNodeEle.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());
                                objXMLNodeEle.SetAttribute("value", objReader.GetValue(2).ToString());  //Aman ML-- used the indexes

                                //sText = objReader.GetValue("SHORT_CODE").ToString();
                                sText = objReader.GetValue(0).ToString();
                                //sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();
                                sText = sText + " - " + objReader.GetValue(1).ToString();
                                //for R5.. Raman Bhatia: changing format of XML to enable autobinding
                                //objXMLNodeEle.SetAttribute("Text",sText);
                                objXMLNodeEle.InnerText = sText;

                                objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves/Dropdowns/Status");
                                objXMLParentEle.AppendChild(objXMLNodeEle);
                            }
                        }
                        objReader.Close();

                    }
                    sbSQL.Remove(0, sbSQL.Length);

                    //Table[5] Reason Dropdown values
                    if (bHideForm)
                        sbSQL.Append(" SELECT 1,2,3 ");
                    else // Just to make sure table[5] gets generated
                        sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");

                    sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'REASON' ");
                    //Start PARAG MITS 10269
                    sbSQL.Append(" AND CODES.DELETED_FLAG!=-1  AND CODES_TEXT.LANGUAGE_CODE = 1033 ");  //Aman ML Change
                    //End PARAG MITS 10269
                    //pmittal5  MITS:11756  06/03/08   Applying triggers condition to 'REASON'
                    GetClaimEventAndPolicyDate(p_iClaimId, ref sDateOfClaim, ref sDateOfEvent, ref sDateOfPolicy);

                    iMonthNow = DateTime.Now.Month;
                    iDay = DateTime.Now.Day;

                    sToday = DateTime.Now.Year.ToString();

                    if (iMonthNow < 10)
                        sToday = sToday + "0" + iMonthNow.ToString();
                    else
                        sToday = sToday + iMonthNow.ToString();

                    if (iDay < 10)
                        sToday = sToday + "0" + iDay.ToString();
                    else
                        sToday = sToday + iDay.ToString();

                    sbSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='EVENT.DATE_OF_EVENT' AND CODES.EFF_START_DATE<='" + sDateOfEvent + "' AND CODES.EFF_END_DATE>='" + sDateOfEvent + "') ");

                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='CLAIM.DATE_OF_CLAIM' AND CODES.EFF_START_DATE<='" + sDateOfClaim + "' AND CODES.EFF_END_DATE>='" + sDateOfClaim + "') ");

                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='POLICY.EFFECTIVE_DATE' AND CODES.EFF_START_DATE<='" + sDateOfPolicy + "' AND CODES.EFF_END_DATE>='" + sDateOfPolicy + "') ");

                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND (CODES.EFF_END_DATE IS NULL OR CODES.EFF_END_DATE='')) ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND (CODES.EFF_START_DATE IS NULL OR CODES.EFF_START_DATE='') AND CODES.EFF_END_DATE>='" + sToday + "') ");
                    sbSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sToday + "' AND CODES.EFF_END_DATE>='" + sToday + "'))");
                    //End - pmittal5
                    sbSQL.Append(" ORDER BY CODES.SHORT_CODE ");
                    if (!bHideForm)
                        sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "REASON", this.LanguageCode);
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("Name"), true);
                        objXMLNodeEle.SetAttribute("value", "");
                        //for R5.. Raman Bhatia: changing format of XML to enable autobinding
                        //objXMLNodeEle.SetAttribute("Text","");
                        objXMLNodeEle.SetAttribute("Selected", "");

                        objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves/Dropdowns/Reason");
                        objXMLParentEle.AppendChild(objXMLNodeEle);

                        objXMLResCodeElement = (XmlElement)objXML.SelectSingleNode("//ReasonCode");

                        while (objReader.Read())
                        {
                            //Create the nodes for Reason dropdown 
                            if (!bHideForm)
                            {
                                objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("Name"), true);

                                objXMLNodeEle.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());

                                //sText = objReader.GetValue("SHORT_CODE").ToString();
                                sText = objReader.GetValue(0).ToString();
                                //sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();
                                sText = sText + " - " + objReader.GetValue(1).ToString();
                                //for R5.. Raman Bhatia: changing format of XML to enable autobinding
                                //objXMLNodeEle.SetAttribute("Text",sText);
                                objXMLNodeEle.InnerText = sText;
                                if (objXMLResCodeElement.InnerText == objReader.GetValue(2).ToString())
                                {
                                    objXMLResTextElement = (XmlElement)objXML.SelectSingleNode("//Reason");
                                    objXMLResTextElement.InnerText = sText;
                                }

                                objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves/Dropdowns/Reason");
                                objXMLParentEle.AppendChild(objXMLNodeEle);
                            }
                        }
                        objReader.Close();

                    }
                    sbSQL.Remove(0, sbSQL.Length);

                    //mbahl3 Reserve Approval Enhancement Mits 32471
                    //JIRA RMA-7078 nshah28 start
                   /* sbSQL.Append(@"SELECT RSV_SUP_APPROVAL_HIST.SUBMITTED_TO,RSV_SUP_APPROVAL_HIST.SUBMITTED_BY,RSV_SUP_APPROVAL_HIST.DTTM_RCD_ADDED,RSV_SUP_APPROVAL_HIST.RSV_HIST_ROW_ID,RESERVE_AMOUNT BASE_AMOUNT,
                                     RESERVE_CURRENT.CLAIM_CURR_CODE,RSV_SUP_APPROVAL_HIST.DATE_ENTERED,RSV_ROW_ID,CODES.SHORT_CODE,CLAIM_CURRENCY_RESERVE_AMOUNT CLAIM_CURR_AMOUNT,
                                     NEW_RESERVE_AMOUNT-ORIGINAL_RESERVE_AMOUNT CHANGE_AMOUNT,RESERVE_CURRENT.ASSIGNADJ_EID,RSV_SUP_APPROVAL_HIST.ADDED_BY_USER ENTERED_BY_USER ");
                             sbSQL.Append(" FROM RSV_SUP_APPROVAL_HIST,  RESERVE_CURRENT,CODES"); */

                   
                    sbSQL.Append(@"SELECT RSV_SUP_APPROVAL_HIST.SUBMITTED_TO,RSV_SUP_APPROVAL_HIST.SUBMITTED_BY,RSV_SUP_APPROVAL_HIST.DTTM_RCD_ADDED,RSV_SUP_APPROVAL_HIST.RSV_HIST_ROW_ID,RESERVE_AMOUNT BASE_AMOUNT,
                                     RESERVE_CURRENT.CLAIM_CURR_CODE,RSV_SUP_APPROVAL_HIST.DATE_ENTERED,RSV_ROW_ID,CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE,CLAIM_CURRENCY_RESERVE_AMOUNT CLAIM_CURR_AMOUNT,
                                     NEW_RESERVE_AMOUNT-ORIGINAL_RESERVE_AMOUNT CHANGE_AMOUNT,RESERVE_CURRENT.ASSIGNADJ_EID,RSV_SUP_APPROVAL_HIST.ADDED_BY_USER ENTERED_BY_USER ");
                    sbSQL.Append(" FROM RSV_SUP_APPROVAL_HIST,  RESERVE_CURRENT,CODES_TEXT");
                    //JIRA RMA-7078 nshah28 end
                    sbSQL.Append(" WHERE CLAIM_ID = " + p_iClaimId);
                    sbSQL.Append(" AND CLAIMANT_EID = " + p_iClaimantEID);
                    sbSQL.Append(" AND UNIT_ID = " + p_iUnitID);

                    sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iReserve);
                    sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + p_iCvgLossId);
                    sbSQL.Append(" AND RESERVE_CURRENT.RES_STATUS_CODE=" + objCache.GetCodeId("H", "RESERVE_STATUS"));

                    sbSQL.Append(" AND RESERVE_CURRENT.RC_ROW_ID=RSV_SUP_APPROVAL_HIST.RC_ROW_ID");

                             //JIRA RMA-7078 nshah28 start
                             //sbSQL.Append(" AND RSV_SUP_APPROVAL_HIST.RES_STATUS_CODE= CODES.CODE_ID");
                             sbSQL.Append(" AND RSV_SUP_APPROVAL_HIST.RES_STATUS_CODE= CODES_TEXT.CODE_ID");
                             //JIRA RMA-7078 nshah28 end
                    sbSQL.Append(" ORDER BY RSV_SUP_APPROVAL_HIST.RSV_HIST_ROW_ID  DESC");


                    objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {

                            objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("Reserve"), true);

                            if (objReader.GetValue("DATE_ENTERED").ToString() != "")
                                sDate = Conversion.GetDBDateFormat(objReader.GetValue("DATE_ENTERED").ToString(), "MM/dd/yyyy");

                            objXMLNodeEle.SetAttribute("Date", sDate);


                            if (objSysSettings.UseMultiCurrency != 0)
                            {
                                objXMLNodeEle.SetAttribute("Amount", CommonFunctions.ConvertCurrency(p_iClaimId, Conversion.ConvertStrToDouble(objReader.GetValue("CLAIM_CURR_AMOUNT").ToString()), eNavType, m_sConnectionString, m_iClientId));
                            }
                            else
                            {
                                objXMLNodeEle.SetAttribute("Amount", CommonFunctions.ConvertCurrency(p_iClaimId, Conversion.ConvertStrToDouble(objReader.GetValue("BASE_AMOUNT").ToString()), eNavType, m_sConnectionString, m_iClientId));
                            }

                            //JIRA RMA-7078 nshah28 start
                            string sStatusParentCode = objReader.GetValue("SHORT_CODE").ToString().Trim();

                            objXMLNodeEle.SetAttribute("StatusParentCode", sStatusParentCode);
                            // sTemp = objReader.GetValue("SHORT_CODE").ToString().Trim();
                             sTemp = objReader.GetValue("CODE_DESC").ToString().Trim();
                             //JIRA RMA-7078 nshah28 end


                            if (sTemp == "\" \"")
                                sTemp = "";

                            objXMLNodeEle.SetAttribute("Status", sTemp);
                            objXMLNodeEle.SetAttribute("User", objReader.GetValue("ENTERED_BY_USER").ToString());

                            objXMLNodeEle.SetAttribute("Reason", "");

                            objXMLNodeEle.SetAttribute("ApproveReasonCom", "");

                            objXMLNodeEle.SetAttribute("RsvRowId", objReader.GetValue("RSV_ROW_ID").ToString());

                            objXMLNodeEle.SetAttribute("RsvHistRowId", objReader.GetValue("RSV_HIST_ROW_ID").ToString());    //mbahl3 Reserve Approval Enhancement Mits 32471
                            objXMLNodeEle.SetAttribute("ChangeAmount", Math.Round(Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), 2).ToString());

                            objXMLNodeEle.SetAttribute("AssignedAdjuster", objCache.GetEntityLastFirstName(Conversion.ConvertObjToInt(objReader.GetValue("ASSIGNADJ_EID"), m_iClientId)));    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                            objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves/ReserveHistory");
                            objXMLParentEle.AppendChild(objXMLNodeEle);

                        }
                        objReader.Close();
                    }
                    sbSQL.Remove(0, sbSQL.Length);
                    //mbahl3 Reserve Approval Enhancement Mits 32471
                    //Table[6] Reserve History
                    //Deb Multi Currency
                    //objSysSettings = new SysSettings(m_sConnectionString);
                    //if (objSysSettings.UseMultiCurrency != 0)
                    //{
                    //    if (IsBaseCurrencySelected)
                    //    {
                    //        sSelect = " SELECT RESERVE_AMOUNT AMOUNT,DATE_ENTERED,REASON_APPROVAL,ENTERED_BY_USER,RSV_ROW_ID,REASON,CODES.SHORT_CODE ";
                    //    }
                    //    else
                    //    {
                    //        sSelect = " SELECT CLAIM_CURRENCY_RESERVE_AMOUNT AMOUNT,DATE_ENTERED,REASON_APPROVAL,ENTERED_BY_USER,RSV_ROW_ID,REASON,CODES.SHORT_CODE ";
                    //    }
                    //}
                    //else
                    //{
                    //    sSelect = " SELECT CLAIM_CURRENCY_RESERVE_AMOUNT AMOUNT,DATE_ENTERED,REASON_APPROVAL,ENTERED_BY_USER,RSV_ROW_ID,REASON,CODES.SHORT_CODE ";RED_BY_USER,RSV_ROW_ID,REASON,CODES.SHORT_CODE ";
                    //}
                     //pgupta: RMA-7078 START
                     //sSelect = " SELECT RESERVE_AMOUNT BASE_AMOUNT,CLAIM_CURRENCY_RESERVE_AMOUNT CLAIM_CURR_AMOUNT,CLAIM_CURR_CODE, DATE_ENTERED,REASON_APPROVAL,ENTERED_BY_USER,RSV_ROW_ID,REASON,CODES.SHORT_CODE,CHANGE_AMOUNT,ASSIGNADJ_EID,0 RSV_HIST_ROW_ID "; //mbahl3 Reserve Approval Enhancement Mits 32471
                    sSelect = " SELECT RESERVE_AMOUNT BASE_AMOUNT,CLAIM_CURRENCY_RESERVE_AMOUNT CLAIM_CURR_AMOUNT,CLAIM_CURR_CODE, DATE_ENTERED,REASON_APPROVAL,ENTERED_BY_USER,RSV_ROW_ID,REASON,CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE,CHANGE_AMOUNT,ASSIGNADJ_EID,0 RSV_HIST_ROW_ID "; //mbahl3 Reserve Approval Enhancement Mits 32471
                     //pgupta: RMA-7078 END
                    //Deb Multi Currency
                    sFrom = " RESERVE_HISTORY ";
                    sWhere = "CLAIM_ID = " + p_iClaimId;
                    sWhere = sWhere + " AND CLAIMANT_EID = " + p_iClaimantEID;
                    sWhere = sWhere + " AND UNIT_ID = " + p_iUnitID;
                    sWhere = sWhere + " AND RESERVE_TYPE_CODE = " + p_iReserve;
                    //BOB Reserves
                    //sWhere = sWhere + " AND POLICY_ID = " + p_iPolicyID;
                    sWhere = sWhere + " AND POLCVG_LOSS_ROW_ID = " + p_iCvgLossId;
                    //sWhere = sWhere + " AND RESERVE_TYPE_CODE = " + p_iReserve;
                    sOrderBy = " ORDER BY DATE_ENTERED DESC,RSV_ROW_ID DESC ";

                    //TODO: Needs to go to common, Format the SQL with Outer/Inner Joins
                    //pgupta: RMA-7078 START
                    //JoinIt(ref sFrom, ref sWhere, "RESERVE_HISTORY", "CODES", "RES_STATUS_CODE", "CODE_ID");
                    JoinIt(ref sFrom, ref sWhere, "RESERVE_HISTORY", "CODES_TEXT", "RES_STATUS_CODE", "CODE_ID");
                    //pgupta: RMA-7078 END

                    sbSQL.Append(sSelect);
                    sbSQL.Append(" FROM " + sFrom);
                    sbSQL.Append(" WHERE " + sWhere);
                    sbSQL.Append(sOrderBy);

                    //Deb: MITS 34527 : REM Custom Changes For Multi Currency Not Enabled and Triggers Setting Currency Fields to zero.
                    //objSysSettings = new SysSettings(m_sConnectionString);
                    //Deb: MITS 34527 : REM Custom Changes For Multi Currency Not Enabled and Triggers Setting Currency Fields to zero.

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            //Create the nodes for Reserve History 
                            objXMLNodeEle = (XmlElement)objXML.ImportNode(GetNewElement("Reserve"), true);

                            if (objReader.GetValue("DATE_ENTERED").ToString() != "")
                                sDate = Conversion.GetDBDateFormat(objReader.GetValue("DATE_ENTERED").ToString(), "MM/dd/yyyy");

                            objXMLNodeEle.SetAttribute("Date", sDate);

                            //objXMLNodeEle.SetAttribute("Amount", String.Format("{0:c}",
                            //    Conversion.ConvertStrToDouble(objReader.GetValue("RESERVE_AMOUNT").ToString())));
                            //Deb Multi Currrency
                            //Deb: MITS 34527 : REM Custom Changes For Multi Currency Not Enabled and Triggers Setting Currency Fields to zero.
                            //if (objReader.GetValue("CLAIM_CURR_CODE").ToString() != "0")
                            if (objSysSettings.UseMultiCurrency != 0)
                            //Deb: MITS 34527 : REM Custom Changes For Multi Currency Not Enabled and Triggers Setting Currency Fields to zero.
                            {
                                objXMLNodeEle.SetAttribute("Amount", CommonFunctions.ConvertCurrency(p_iClaimId, Conversion.ConvertStrToDouble(objReader.GetValue("CLAIM_CURR_AMOUNT").ToString()), eNavType, m_sConnectionString, m_iClientId));
                            }
                            else
                            {
                                objXMLNodeEle.SetAttribute("Amount", CommonFunctions.ConvertCurrency(p_iClaimId, Conversion.ConvertStrToDouble(objReader.GetValue("BASE_AMOUNT").ToString()), eNavType, m_sConnectionString, m_iClientId));
                            }
                            //Deb Multi Currrency
                            //pgupta: RMA-7078 START
                            string sStatusParentCode = objReader.GetValue("SHORT_CODE").ToString().Trim();

                            objXMLNodeEle.SetAttribute("StatusParentCode", sStatusParentCode);
                            //sTemp = objReader.GetValue("SHORT_CODE").ToString().Trim();
                            sTemp = objReader.GetValue("CODE_DESC").ToString().Trim();
                            //pgupta: RMA-7078 END
                            if (sTemp == "\" \"")
                                sTemp = "";

                            objXMLNodeEle.SetAttribute("Status", sTemp);
                            objXMLNodeEle.SetAttribute("User", objReader.GetValue("ENTERED_BY_USER").ToString());
                            objXMLNodeEle.SetAttribute("Reason", objReader.GetValue("REASON").ToString());
                            //skhare7 R8: Supervisory Approval
                            objXMLNodeEle.SetAttribute("ApproveReasonCom", objReader.GetValue("REASON_APPROVAL").ToString());
                            //Mgaba2: R8: Supervisory Approval
                            objXMLNodeEle.SetAttribute("RsvRowId", objReader.GetValue("RSV_ROW_ID").ToString());
                            //Ankit Start : Financial Enhancements - Change Amount change
                            objXMLNodeEle.SetAttribute("RsvHistRowId", objReader.GetValue("RSV_HIST_ROW_ID").ToString());    //mbahl3 Reserve Approval Enhancement Mits 32471
                            objXMLNodeEle.SetAttribute("ChangeAmount", Math.Round(Conversion.ConvertObjToDouble(objReader.GetValue("CHANGE_AMOUNT"), m_iClientId), 2).ToString());
                            //Ankit End
                            objXMLNodeEle.SetAttribute("AssignedAdjuster", objCache.GetEntityLastFirstName(Conversion.ConvertObjToInt(objReader.GetValue("ASSIGNADJ_EID"), m_iClientId)));    //Ankit Start : Worked on MITS - 29721 - Adjuster Enhancement
                            objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves/ReserveHistory");
                            objXMLParentEle.AppendChild(objXMLNodeEle);
                        }
                        objReader.Close();

                    }
                    sbSQL.Remove(0, sbSQL.Length);

                }//else if (GetReserveTracking(p_iClaimId, ref sShortCode) != 1)

                //MITS 9515 : Prevent Modifying Reserves to Zero should appear as a warning..
                //Adding an attribute to achieve the same.. modified by Raman Bhatia
                objXMLParentEle = (XmlElement)objXML.SelectSingleNode("//Reserves");
                if (objXMLParentEle != null)
                {
                    if (objSysSettings == null)
                    {
                        objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                    }
                    objXMLParentEle.SetAttribute("PrevResModifyzero", objSysSettings.PrevResModifyzero.ToString());
                    objXMLParentEle.SetAttribute("IsMultiCurrencyOn", objSysSettings.UseMultiCurrency.ToString());//skhare7 MITS 30196

                }
            }//End Main Try
            catch (RMAppException p_objAppException)
            {
                throw p_objAppException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesXML.DataErr", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                objXMLNodeEle = null;
                objXMLParentEle = null;
                objSysSettings = null;
            }
        }//End Function

        #endregion

        #region public GetXmlForInsResrvAmnt (p_sXML, p_sUser)

        /// Name		: GetXmlForInsResrvAmnt
        /// Author		: Navneet Sota
        /// Date Created: 11/16/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for getting XML in case the Payment exceeds the Reserve amount.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_sXML">The XML being passed</param>
        /// <param name="p_sUser">The User Name</param>
        public XmlDocument GetXmlForInsReserveAmount(string p_sXML, string p_sUser, string p_sReason)
        {
            //XML Objects
            XmlDocument objXML = null;
            XmlNode objXMLParentNode = null;
            XmlElement objXMLParentEle = null;
            XmlElement objXMLErrorElem = null;

            //Integer Variables
            int iUserID = 0;
            int iGroupID = 0;
            int iClaimID = 0;
            int iUnit = 0;
            int iClaimantID = 0;
            int iReserveCode = 0;
            //rupal: BOB Change
            int iPolCvgID = 0;
            int iPolCvgLossID = 0; //rupal:mits 35837,rma-196,rma-197
            //Double Variables
            double dAmount = 0;
            double dCurrentReserve = 0;
            double dPayment = 0;
            double dBalance = 0;
            double dSetResv = 0;
            double dAddResv = 0;

            //Boolean Variables
            bool bIsModify = false;
            bool bIsShortPayment = false;

            //String Objects
            StringBuilder sbSQL = null;

            //String Variables
            string sErrorMsg = "";

            //Database objects
            DbReader objReader = null;
            //Start by Shivendu for MITS 18384
            if (m_objDataModelFactory == null)
            {
                m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
            }
            //End by Shivendu for MITS 18384
            ReserveCurrent objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", true); 

            // LOB Settings: Added by Mihika for Defect# 2122
            LobSettings objLobSettings = null;
            ColLobSettings objColLobSettings = null;
            int iLOB = 0;
            try
            {
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objXML = new XmlDocument();
                objXML.LoadXml(p_sXML);
                objXMLParentNode = objXML.SelectSingleNode("//insufres");
                objXMLErrorElem = (XmlElement)objXMLParentNode;

                iUserID = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("userid").ToString());
                iGroupID = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("groupid").ToString());
                iClaimID = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("claimid").ToString());

                for (int iCnt = 0; iCnt < objXMLParentNode.ChildNodes.Count; iCnt++)
                {
                    switch (objXMLParentNode.ChildNodes[iCnt].Name.ToString())
                    {
                        case "unit":
                            iUnit = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "claimant":
                            iClaimantID = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "balance":
                            break;
                        case "newamount":
                            dAmount = Conversion.ConvertStrToDouble(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "reserve":
                            dCurrentReserve = Conversion.ConvertStrToDouble(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "payment":
                            dPayment = Conversion.ConvertStrToDouble(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "rcode":
                            objXMLErrorElem = (XmlElement)objXMLParentNode.ChildNodes[iCnt];
                            iReserveCode = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("id").ToString());
                            break;
                        //rupal:start, BOB change (added PolCvgID)
                        case "PolCvgId":
                            iPolCvgID = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                            //rupal:start,mits 35837, rma-196,rma-197
                        case "CvgLossId":
                            iPolCvgLossID = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                            //rupal:end
                        //rupal:end, BOB change


                        //rupal:end, BOB change
                    }//End Switch
                }//End For

                try
                {
                    objReserveCurrent.ClaimId = iClaimID;
                    objReserveCurrent.ClaimantEid = iClaimantID;
                    objReserveCurrent.UnitId = iUnit;
                    objReserveCurrent.ReserveTypeCode = iReserveCode;
                    objReserveCurrent.DateEntered = DateTime.Now.ToShortDateString();
                    //Deb Multi Currency
                    if (objReserveCurrent.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(iClaimID, m_sConnectionString);
                        if (iClaimCurrCode > 0)
                        {
                            //double dExhRateClaimToBase = objReserveCurrent.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType));
                            double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dAmount = dAmount * dExhRateClaimToBase;
                            objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                        }
                    }
                    //Deb Multi Currency
                    objReserveCurrent.ReserveAmount = dAmount;
                    //rupal:start, BOB Change,
                    // reserve status is always updated to zero
                    //we want to retain reserve status for existing reserves, for new reserve: datamodel will
                    //automatically set the reserve status to "Open"
                    //objReserveCurrent.ResStatusCode = 0; //commented by rupal
                    objReserveCurrent.Reason = p_sReason;
                    objReserveCurrent.sUser = p_sUser;
                    objReserveCurrent.iUserId = iUserID;
                    objReserveCurrent.iGroupId = iGroupID;
                    objReserveCurrent.sUpdateType = "Reserves";
                    //rupal:start, BOB Change, added PolCvgID
                    //rupal:start, mits 35837, rma-196,197
                    objReserveCurrent.CoverageLossId = iPolCvgLossID;
                    objReserveCurrent.CoverageId = iPolCvgID;
                    //rupal:end,mits 35837, rma-196,197
                    //rupal:end, BOB Change

                    // Mihika 1-Mar-2006 Defect# 2122
                    // Modify Reserves only in case when user clicked on either 'Add Reserve' or 'Edit Reserve'
                    // and the following settings in Utilities are false
                    // 1. Auto Adjust Negative Reserves to Zero
                    // 2. Auto Adjust Negative Reserves to Zero (per claim)
                    sbSQL = new StringBuilder();
                    sbSQL.Append(" SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimID);

                    using (DbReader rdr = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        rdr.Read();
                        iLOB = Conversion.ConvertObjToInt(rdr.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                    }

                    objLobSettings = objColLobSettings[iLOB];
                    if (p_sReason == "AUTO-ADJUST")
                    {
                        if (objLobSettings.AdjRsv || (objLobSettings.AdjRsvPerClaim && objLobSettings.AdjNegResTypesPerRsv.ContainsKey(iReserveCode + "0")))
                        {
                            objReserveCurrent.Save();
                            bIsModify = true;
                            //bIsModify = ModifyReserves(iClaimID, iClaimantID, iUnit, iReserveCode, DateTime.Now.ToShortDateString(), dAmount,
                            //    0, p_sUser, p_sReason, iUserID, iGroupID);
                        }
                        else
                        {
                            bIsModify = true;
                        }
                    }
                    else
                    {
                        objReserveCurrent.Save();
                        bIsModify = true;
                    }
                        //bIsModify = ModifyReserves(iClaimID, iClaimantID, iUnit, iReserveCode, DateTime.Now.ToShortDateString(), dAmount,
                        //    0, p_sUser, p_sReason, iUserID, iGroupID);
                }
                catch (Exception objInnerException)
                {
                    sErrorMsg = objInnerException.Message;
                }

                if (bIsModify)
                {
                    //Instantiate the sbSQL object for appending the SQL's	
                    sbSQL = new StringBuilder();

                    sbSQL.Append(" SELECT RESERVE_AMOUNT,BALANCE_AMOUNT FROM RESERVE_CURRENT WHERE ");
                    sbSQL.Append(" CLAIM_ID = " + iClaimID);
                    sbSQL.Append(" AND UNIT_ID = " + iUnit);
                    sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantID);
                    sbSQL.Append(" AND RESERVE_TYPE_CODE = " + iReserveCode);
                    sbSQL.Append(" AND POLCVG_ROW_ID = " + iPolCvgID);
                    

                    //rupal:start, BOB Change (need to include PolCvgId also)
                    sbSQL.Append(" AND POLCVG_LOSS_ROW_ID  = " + iPolCvgLossID);//rupal:mits 35837,rma-196,rma-197
                    //Rupal:end, BOB Change 

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dCurrentReserve = Conversion.ConvertStrToDouble(objReader.GetValue("RESERVE_AMOUNT").ToString());
                            dBalance = Conversion.ConvertStrToDouble(objReader.GetValue("BALANCE_AMOUNT").ToString());
                        }
                        objReader.Close();
                    }

                    sbSQL.Remove(0, sbSQL.Length);

                    if ((dBalance - dAmount) < 0)
                    {
                        dSetResv = dBalance - (dPayment + dCurrentReserve);
                        if (dSetResv < 0)
                            dSetResv *= -1;

                        dAddResv = dBalance - dPayment;
                        if (dAddResv < 0)
                            dAddResv *= -1;

                        bIsShortPayment = true;
                    }
                    else
                        dSetResv = dCurrentReserve;

                    objXMLParentEle = (XmlElement)objXMLParentNode;
                    if (bIsShortPayment)
                        objXMLParentEle.SetAttribute("scr", "1");
                    else
                        objXMLParentEle.SetAttribute("scr", "0");

                    for (int iCnt = 0; iCnt < objXMLParentNode.ChildNodes.Count; iCnt++)
                    {
                        switch (objXMLParentNode.ChildNodes[iCnt].Name.ToString())
                        {
                            case "balance":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dBalance, 2, MidpointRounding.AwayFromZero));
                                break;
                            case "reserve":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dCurrentReserve, 2, MidpointRounding.AwayFromZero));
                                break;
                            case "addreserve":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dAddResv, 2, MidpointRounding.AwayFromZero));
                                break;
                            case "setreserve":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dSetResv, 2, MidpointRounding.AwayFromZero));
                                break;
                        }//End Switch
                    }//End For
                }//if (bIsModify)
                else
                {
                    objXMLErrorElem = objXML.CreateElement("errmsg");
                    objXMLErrorElem.SetAttribute("id", "1");
                    objXMLErrorElem.InnerText = sErrorMsg;
                    objXMLParentNode.AppendChild(objXMLErrorElem);
                }

            }//End Main Try
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetXmlForInsReserveAmount.DataErr", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                objXMLErrorElem = null;
                objXMLParentEle = null;
                objXMLParentNode = null;
                objColLobSettings = null;
                objLobSettings = null;
            }
            //Return the Result to calling function
            return objXML;
        }


        /// Name		: GetXmlForInsReserve
        /// Author		: Shruti Choudhary
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for getting XML in case the Payment exceeds the Reserve amount.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_sXML">The XML being passed</param>
        /// <param name="p_sUser">The User Name</param>
        public XmlDocument GetXmlForInsReserve(string p_sXML, string p_sUser, string p_sReason, SessionManager p_objSessionManager)
        {
            //XML Objects
            XmlDocument objXML = null;
            XmlNode objXMLParentNode = null;
            XmlElement objXMLParentEle = null;
            XmlElement objXMLErrorElem = null;

            //Integer Variables
            int iUserID = 0;
            int iGroupID = 0;
            int iClaimID = 0;
            int iUnit = 0;
            int iClaimantID = 0;
            int iReserveCode = 0;
            //rupal
            int iPolCvgLossID = 0;
            int iPolCvgID = 0; //mits 35837, rma196,197
            //Double Variables
            double dAmount = 0;
            double dCurrentReserve = 0;
            double dPayment = 0;
            double dBalance = 0;
            double dSetResv = 0;
            double dAddResv = 0;

            //Boolean Variables
            bool bIsModify = false;
            bool bIsShortPayment = false;

            //String Objects
            StringBuilder sbSQL = null;

            //String Variables
            string sErrorMsg = "";

            //Database objects
            DbReader objReader = null;
            //Start by Shivendu for MITS 18384
            if (m_objDataModelFactory == null)
            {
                m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
            }
            //End by Shivendu for MITS 18384

            ReserveCurrent objReserveCurrent = (ReserveCurrent)m_objDataModelFactory.GetDataModelObject("ReserveCurrent", true);

            LobSettings objLobSettings = null;
            ColLobSettings objColLobSettings = null;
            int iLOB = 0;
            try
            {
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objXML = new XmlDocument();
                objXML.LoadXml(p_sXML);
                objXMLParentNode = objXML.SelectSingleNode("//insufres");
                objXMLErrorElem = (XmlElement)objXMLParentNode;

                iUserID = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("userid").ToString());
                iGroupID = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("groupid").ToString());
                iClaimID = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("claimid").ToString());

                for (int iCnt = 0; iCnt < objXMLParentNode.ChildNodes.Count; iCnt++)
                {
                    switch (objXMLParentNode.ChildNodes[iCnt].Name.ToString())
                    {
                        case "unit":
                            iUnit = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "claimant":
                            iClaimantID = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "balance":
                            break;
                        case "newamount":
                            dAmount = Conversion.ConvertStrToDouble(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "reserve":
                            dCurrentReserve = Conversion.ConvertStrToDouble(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "payment":
                            dPayment = Conversion.ConvertStrToDouble(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        case "rcode":
                            objXMLErrorElem = (XmlElement)objXMLParentNode.ChildNodes[iCnt];
                            iReserveCode = Conversion.ConvertStrToInteger(objXMLErrorElem.GetAttribute("id").ToString());
                            break;
                        //rupal:start, BOB change (added PolCvgID)
                        case "PolCvgId":
                            iPolCvgID = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText); //rupal:mits 35837,rma-196,rma-197
							    //iPolCvgID = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        //rupal:start,mits 35837, rma-196,rma-197
                        case "CvgLossId":
                            iPolCvgLossID = Conversion.ConvertStrToInteger(objXMLParentNode.ChildNodes[iCnt].InnerText);
                            break;
                        //rupal:end
                        //rupal:end, BOB change
                    }//End Switch
                }//End For

                try
                {
                    objReserveCurrent.ClaimId = iClaimID;
                    objReserveCurrent.ClaimantEid = iClaimantID;
                    objReserveCurrent.UnitId = iUnit;
                    objReserveCurrent.ReserveTypeCode = iReserveCode;
                    objReserveCurrent.DateEntered = DateTime.Now.ToShortDateString();
                    //Deb Multi Currency
                    if (objReserveCurrent.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                    {
                        int iClaimCurrCode = CommonFunctions.GetClaimCurrencyCode(iClaimID, m_sConnectionString);
                        if (iClaimCurrCode > 0)
                        {
                            //double dExhRateClaimToBase = objReserveCurrent.Context.DbConn.ExecuteDouble(string.Format("SELECT EXCHANGE_RATE FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE={0} AND DESTINATION_CURRENCY_CODE={1}", iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType));
                            double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objReserveCurrent.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                            dAmount = dAmount * dExhRateClaimToBase;
                            objReserveCurrent.ClaimToBaseCurrRate = dExhRateClaimToBase;
                        }
                    }
                    //Deb Multi Currency
                    objReserveCurrent.ReserveAmount = dAmount;
                    //rupal:start, BOB Change,
                    // reserve status is always updated to zero
                    //we want to retain reserve status for existing reserves, for new reserve: datamodel will
                    //automatically set the reserve status to "Open"
                    //objReserveCurrent.ResStatusCode = 0; //commented by rupal
                    objReserveCurrent.Reason = p_sReason;
                    
                    objReserveCurrent.sUser = p_sUser;
                    objReserveCurrent.iUserId = iUserID;
                    objReserveCurrent.iGroupId = iGroupID;
                    objReserveCurrent.sUpdateType = "Reserves";
                    //rupal:start, BOB change (added PolCvgID)
                    objReserveCurrent.CoverageId = iPolCvgID;//rupal:start,mits 35837, rma-196,rma-197
                    objReserveCurrent.CoverageLossId = iPolCvgLossID;
				//	  objReserveCurrent.CoverageId = iPolCvgID;
                    //rupal:end, BOB Change
                    // Modify Reserves only in case when user clicked on either 'Add Reserve' or 'Edit Reserve'
                    // and the following settings in Utilities are false
                    // 1. Auto Adjust Negative Reserves to Zero
                    // 2. Auto Adjust Negative Reserves to Zero (per claim)
                    sbSQL = new StringBuilder();
                    sbSQL.Append(" SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + iClaimID);

                    using (DbReader rdr = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        rdr.Read();
                        iLOB = Conversion.ConvertObjToInt(rdr.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                    }

                    objLobSettings = objColLobSettings[iLOB];
                    if (p_sReason == "AUTO-ADJUST")
                    {
                        if (objLobSettings.AdjRsv || (objLobSettings.AdjRsvPerClaim && objLobSettings.AdjNegResTypesPerRsv.ContainsKey(iReserveCode + "0")))
                        {
                            objReserveCurrent.Save();
                            bIsModify = true;
                            //bIsModify = ModifyReserves(iClaimID, iClaimantID, iUnit, iReserveCode, DateTime.Now.ToShortDateString(), dAmount,
                            //    0, p_sUser, p_sReason, iUserID, iGroupID);
                        }
                        else
                        {
                            bIsModify = true;
                        }
                    }
                    else
                    {
                        objReserveCurrent.Save();
                        bIsModify = true;
                        //bIsModify = ModifyReserves(iClaimID, iClaimantID, iUnit, iReserveCode, DateTime.Now.ToShortDateString(), dAmount,
                        //    0, p_sUser, p_sReason, iUserID, iGroupID);
                    }
                }
                catch (Exception objInnerException)
                {
                    sErrorMsg = objInnerException.Message;
                }

                if (bIsModify)
                {
                    //Instantiate the sbSQL object for appending the SQL's	
                    sbSQL = new StringBuilder();
                    if (objReserveCurrent.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                    {
                        sbSQL.Append(" SELECT RESERVE_AMOUNT,BALANCE_AMOUNT FROM RESERVE_CURRENT WHERE ");
                        sbSQL.Append(" CLAIM_ID = " + iClaimID);
                        sbSQL.Append(" AND UNIT_ID = " + iUnit);
                        sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantID);
                        sbSQL.Append(" AND RESERVE_TYPE_CODE = " + iReserveCode);
                        //sbSQL.Append(" AND POLCVG_ROW_ID = " + iPolCvgLossID);//rupal:start,mits 35837, rma-196,rma-197
                    }
                    else
                    {
                        sbSQL.Append(" SELECT CLAIM_CURRENCY_RESERVE_AMOUNT RESERVE_AMOUNT,CLAIM_CURRENCY_BALANCE_AMOUNT BALANCE_AMOUNT FROM RESERVE_CURRENT WHERE ");
                        sbSQL.Append(" CLAIM_ID = " + iClaimID);
                        sbSQL.Append(" AND UNIT_ID = " + iUnit);
                        sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantID);
                        sbSQL.Append(" AND RESERVE_TYPE_CODE = " + iReserveCode);
                        //sbSQL.Append(" AND POLCVG_ROW_ID = " + iPolCvgLossID);//rupal:start,mits 35837, rma-196,rma-197
                    }
                    //rupal:start, BOB Changec (need to include PolCvgId also)
                    sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + iPolCvgLossID); //rupal:start,mits 35837, rma-196,rma-197
                    //Rupal:end, BOB Change 


                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dCurrentReserve = Conversion.ConvertStrToDouble(objReader.GetValue("RESERVE_AMOUNT").ToString());
                            dBalance = Conversion.ConvertStrToDouble(objReader.GetValue("BALANCE_AMOUNT").ToString());
                        }
                        objReader.Close();
                    }

                    sbSQL.Remove(0, sbSQL.Length);

                    if ((dBalance - dAmount) < 0)
                    {
                        dSetResv = dBalance - (dPayment + dCurrentReserve);
                        if (dSetResv < 0)
                            dSetResv *= -1;

                        dAddResv = dBalance - dPayment;
                        if (dAddResv < 0)
                            dAddResv *= -1;

                        bIsShortPayment = true;
                    }
                    else
                        dSetResv = dCurrentReserve;

                    objXMLParentEle = (XmlElement)objXMLParentNode;
                    if (bIsShortPayment)
                        objXMLParentEle.SetAttribute("scr", "1");
                    else
                        objXMLParentEle.SetAttribute("scr", "0");

                    for (int iCnt = 0; iCnt < objXMLParentNode.ChildNodes.Count; iCnt++)
                    {
                        switch (objXMLParentNode.ChildNodes[iCnt].Name.ToString())
                        {
                            case "balance":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dBalance, 2, MidpointRounding.AwayFromZero));
                                break;
                            case "reserve":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dCurrentReserve, 2, MidpointRounding.AwayFromZero));
                                break;
                            case "addreserve":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dAddResv, 2, MidpointRounding.AwayFromZero));
                                break;
                            case "setreserve":
                                objXMLParentNode.ChildNodes[iCnt].InnerText =
                                    Conversion.ConvertObjToStr(Math.Round(dSetResv, 2, MidpointRounding.AwayFromZero));
                                break;
                        }//End Switch
                    }//End For

                    //TODO: This code is not mapped to the user session therefore user context is lost                    
                    p_objSessionManager.SetBinaryItem("InsufAmntXml", Utilities.BinarySerialize(objXML.OuterXml), m_iClientId);

                }//if (bIsModify)
                else
                {
                    objXMLErrorElem = objXML.CreateElement("errmsg");
                    objXMLErrorElem.SetAttribute("id", "1");
                    objXMLErrorElem.InnerText = sErrorMsg;
                    objXMLParentNode.AppendChild(objXMLErrorElem);
                }

            }//End Main Try
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetXmlForInsReserveAmount.DataErr", m_iClientId), objException);
            }
            finally
            {
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();

                objXMLErrorElem = null;
                objXMLParentEle = null;
                objXMLParentNode = null;
                objColLobSettings = null;
                objLobSettings = null;
            }
            //Return the Result to calling function
            return objXML;
        }

        /// <summary>
        /// Get Xml For Insufficient Reserve Amount
        /// </summary>
        /// <param name="p_dblBalance">Balance Amount</param>
        /// <param name="p_dblPayment">Payment Amount</param>
        /// <param name="p_dblReserve">Reserve Amount</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        private XmlDocument AppendInsResrvAmntXml(double p_dblBalance, double p_dblPayment, double p_dblReserve, int p_iReserveTypeCode, FundsAuto p_FundsAuto)
        {
            XmlDocument objDoc;

            double dblAddReserve = 0.0;
            double dblSetReserve = 0.0;
            string sXmlForInsResrvAmnt = "";
            string sDesc = "";
            string sShortCode = "";

            try
            {
                dblAddReserve = p_dblPayment - p_dblBalance;
                if (dblAddReserve < 0.0)
                    dblAddReserve = -dblAddReserve;

                dblSetReserve = p_dblBalance - (p_dblPayment + p_dblReserve);
                if (dblSetReserve < 0.0)
                    dblSetReserve = -dblSetReserve;


                sXmlForInsResrvAmnt = "~insufres scr=\"1\" userid=\"" + p_FundsAuto.Context.RMUser.UserId.ToString() + "\" groupid=\"" + p_FundsAuto.Context.RMUser.GroupId.ToString() + "\" claimid=\""
                    + p_FundsAuto.ClaimId.ToString() + "\"|"
                    + "~unit|" + p_FundsAuto.UnitId.ToString() + "~/unit|"
                    + "~claimant|" + p_FundsAuto.ClaimantEid.ToString() + "~/claimant|"
                    + "~balance|" + Math.Round(p_dblBalance, 2, MidpointRounding.AwayFromZero) + "~/balance|"
                    + "~reserve|" + Math.Round(p_dblReserve, 2, MidpointRounding.AwayFromZero) + "~/reserve|"
                    + "~payment|" + Math.Round(p_dblPayment, 2, MidpointRounding.AwayFromZero) + "~/payment|"
                    + "~setreserve|" + Math.Round(dblSetReserve, 2, MidpointRounding.AwayFromZero) + "~/setreserve|"
                    + "~addreserve|" + Math.Round(dblAddReserve, 2, MidpointRounding.AwayFromZero) + "~/addreserve|"
                    + "~newamount|0~/newamount|";



                p_FundsAuto.Context.LocalCache.GetCodeInfo(p_iReserveTypeCode, ref sShortCode, ref sDesc);

                sXmlForInsResrvAmnt += "~rcode id=\"" + p_iReserveTypeCode.ToString()
                    + "\"|" + sDesc + "~/rcode|" + "~/insufres|";

                sXmlForInsResrvAmnt = sXmlForInsResrvAmnt.Replace('~', '<');
                sXmlForInsResrvAmnt = sXmlForInsResrvAmnt.Replace('|', '>');

                objDoc = new XmlDocument();
                objDoc.LoadXml(sXmlForInsResrvAmnt);


                return objDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetXmlForInsReserveAmount.DataErr", m_iClientId), p_objEx);
            }
            finally
            {

            }
        }

        #endregion

        #region public UpdateFundsReserveCurrent (p_iClaimID, p_iClaimantEID, p_iUnitID, p_iLOB, p_sChequeDate)

        /// Name		: UpdateFundsReserveCurrent
        /// Author		: Navneet Sota
        /// Date Created: 12/30/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is used for updating current funds reserve.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_iClaimID">Claim ID</param>
        /// <param name="p_iClaimantEID">Claimaint ID</param>
        /// <param name="p_iUnitID">The Unit ID</param>
        /// <param name="p_iLOB">The Line of Business Code</param>
        /// <param name="p_sCheckDate">The Date on which check is issued</param>
        /// <param name="p_sLoginName">Login Name </param>
        public void UpdateFundsReserveCurrent(int p_iClaimID, int p_iClaimantEID, int p_iUnitID,
                                               int p_iLOB, string p_sChequeDate, string p_sLoginName)
        {
            //Integer Variables
            int iCnt = 0;
            int iCStat = 0;
            int iReserveID = 0;
            int iResCurrentGUID = 0;
            int iResHistoryGUID = 0;

            //Double Variables
            double dblAmount = 0;
            double dblBalance = 0;
            double dblPaid = 0;
            double dblCollect = 0;
            double dblIncurred = 0;
            double dblChangeAmount = 0;

            //Boolean Variables
            bool bAutoAdjLog = false;
            bool bTemp = false;

            //String Objects
            StringBuilder sbSQL = null;

            //String Variables
            string sTodaysdate = "";
            string sSQLClaimR = "";
            string sSQLClaimF = "";
            string sSQLPayment = "";
            string sSQLCollect = "";

            //Database objects
            DataSet objDataSet = null;
            DataRow objDataRow = null;
            DbTransaction objTrans = null;
            DbConnection objConn = null;
            DbCommand objCommand = null;

            //LOB settings
            Riskmaster.Settings.ColLobSettings objLOB = null;
            Riskmaster.Settings.LobSettings objLOBSettings = null;

            try
            {
                //Format the Dates in YYYYMMDD format
                sTodaysdate = Conversion.ToDbDate(System.DateTime.Now).ToString();
                if (Conversion.ToDate(p_sChequeDate) > Conversion.ToDate(sTodaysdate))
                    sTodaysdate = p_sChequeDate;

                if (p_iClaimID != 0 && p_iLOB != 0)
                {
                    bTemp = this.IsClaimOpen(p_iClaimID, ref iCStat);

                    sSQLClaimR = sSQLClaimR + " RESERVE_CURRENT.CLAIM_ID =  " + p_iClaimID;
                    sSQLClaimR = sSQLClaimR + " AND RESERVE_CURRENT.CLAIMANT_EID = " + p_iClaimantEID;
                    sSQLClaimR = sSQLClaimR + " AND RESERVE_CURRENT.UNIT_ID = " + p_iUnitID;

                    sSQLClaimF = sSQLClaimF + " FUNDS.CLAIM_ID = " + p_iClaimID;
                    sSQLClaimF = sSQLClaimF + " AND FUNDS.CLAIMANT_EID = " + p_iClaimantEID;
                    sSQLClaimF = sSQLClaimF + " AND FUNDS.UNIT_ID = " + p_iUnitID;

                    //Instantiate the sbSQL object for appending the SQL's	
                    sbSQL = new StringBuilder();
                    sbSQL.Append(" UPDATE RESERVE_CURRENT SET PAID_TOTAL = 0.0, COLLECTION_TOTAL = 0.0, ");
                    sbSQL.Append(" INCURRED_AMOUNT = RESERVE_AMOUNT, BALANCE_AMOUNT = RESERVE_AMOUNT WHERE ");
                    sbSQL.Append(sSQLClaimR);

                    if (objConn == null)
                        objConn = DbFactory.GetDbConnection(m_sConnectionString);
                    objConn.Open();
                    objConn.ExecuteNonQuery(sbSQL.ToString());
                    objConn.Close();

                    sbSQL.Remove(0, sbSQL.Length);

                    //Instantiate the LOB Settings Class
                    objLOB = new Riskmaster.Settings.ColLobSettings(m_sConnectionString, m_iClientId);
                    objLOBSettings = objLOB[p_iLOB];
                    if (objLOBSettings != null)
                    {
                        if (objLOBSettings.ResByClmType)
                        {
                            sbSQL.Append(" SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES ");
                            sbSQL.Append(" WHERE LINE_OF_BUS_CODE = " + p_iLOB);
                            sbSQL.Append(" AND CLAIM_TYPE_CODE = " + p_iClaimID);
                        }
                        else
                        {
                            sbSQL.Append(" SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES ");
                            sbSQL.Append(" WHERE LINE_OF_BUS_CODE = " + p_iLOB);
                        }
                    }//end if(objLOBSettings != null)
                    else
                        throw new RMAppException(Globalization.GetString("ReserveFunds.UpdateFundsReserveCurrent.InvaliedLOB", m_iClientId));

                    sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN ");
                    sbSQL.Append(" (SELECT DISTINCT RESERVE_TYPE_CODE FROM RESERVE_CURRENT WHERE ");
                    sbSQL.Append(sSQLClaimR + ")");

                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);//rkaur27
                    if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                    {
                        sbSQL.Remove(0, sbSQL.Length);
                        if (objConn == null)
                            objConn = DbFactory.GetDbConnection(m_sConnectionString);
                        objConn.Open();

                        for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                        {
                            sbSQL.Append(" INSERT INTO RESERVE_CURRENT(RC_ROW_ID,CLAIM_ID,CLAIMANT_EID, ");
                            sbSQL.Append(" UNIT_ID,RESERVE_TYPE_CODE, ");
                            sbSQL.Append(" RESERVE_AMOUNT, BALANCE_AMOUNT,INCURRED_AMOUNT, PAID_TOTAL, COLLECTION_TOTAL, ");
                            sbSQL.Append(" DATE_ENTERED,DTTM_RCD_ADDED,ADDED_BY_USER,REASON, ");
                            sbSQL.Append(" DTTM_RCD_LAST_UPD,UPDATED_BY_USER )");

                            //Get new GUID for RESERVE_CURRENT
                            iResCurrentGUID = Utilities.GetNextUID(m_sConnectionString, "RESERVE_CURRENT", m_iClientId);
                            objDataRow = objDataSet.Tables[0].Rows[iCnt];

                            sbSQL.Append(" VALUES ( " + iResCurrentGUID + ", " + p_iClaimID + ", " + p_iClaimantEID);
                            sbSQL.Append(", " + p_iUnitID + ", " + objDataRow["RESERVE_TYPE_CODE"]);
                            sbSQL.Append(", 0.0,0.0,0.0,0.0,0.0, ");
                            sbSQL.Append(" '" + Conversion.ToDbDate(System.DateTime.Now) + "', ");
                            sbSQL.Append(" '" + Conversion.ToDbDateTime(System.DateTime.Now) + "', ");
                            sbSQL.Append(" '" + p_sLoginName + "', 'AUTO CREATE',");
                            sbSQL.Append(" '" + Conversion.ToDbDateTime(System.DateTime.Now) + "', ");
                            sbSQL.Append(" '" + p_sLoginName + "' )");

                            objConn.ExecuteNonQuery(sbSQL.ToString());
                            sbSQL.Remove(0, sbSQL.Length);
                        }
                        objConn.Close();
                    }//end if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)

                    sbSQL.Remove(0, sbSQL.Length);
                    if (m_sDBType == Constants.DB_ACCESS.ToUpper())
                    {
                        sSQLPayment = " UPDATE RESERVE_CURRENT,FUNDS,FUNDS_TRANS_SPLIT,CODES ";
                        sSQLPayment = " SET RESERVE_CURRENT.PAID_TOTAL = RESERVE_CURRENT.PAID_TOTAL + FUNDS_TRANS_SPLIT.AMOUNT,";
                        sSQLPayment = " RESERVE_CURRENT.DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(System.DateTime.Now) + "', ";

                        sSQLCollect = " UPDATE RESERVE_CURRENT,FUNDS,FUNDS_TRANS_SPLIT,CODES ";
                        sSQLCollect = " SET RESERVE_CURRENT.COLLECTION_TOTAL = RESERVE_CURRENT.COLLECTION_TOTAL + FUNDS_TRANS_SPLIT.AMOUNT, ";
                        sSQLCollect = " RESERVE_CURRENT.DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(System.DateTime.Now) + "', ";

                        sbSQL.Append(" WHERE " + sSQLClaimR + " AND " + sSQLClaimF);
                        sbSQL.Append(" AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND CODES.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE ");
                        sbSQL.Append(" AND CODES.RELATED_CODE_ID = RESERVE_CURRENT.RESERVE_TYPE_CODE ");
                        sbSQL.Append(" AND FUNDS.TRANS_DATE <= '" + sTodaysdate + "' ");
                        sbSQL.Append(" AND FUNDS.VOID_FLAG = 0");

                        sSQLPayment = sSQLPayment + sbSQL.ToString() + " AND FUNDS.PAYMENT_FLAG <> 0";
                        sSQLCollect = sSQLCollect + sbSQL.ToString() + " AND FUNDS.PAYMENT_FLAG = 0";
                    }
                    else
                    {
                        sSQLPayment = " UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.PAID_TOTAL = ";
                        sSQLCollect = " UPDATE RESERVE_CURRENT SET RESERVE_CURRENT.COLLECTION_TOTAL = ";

                        sbSQL.Append(" (SELECT SUM(FTS.AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS,CODES WHERE ");
                        sbSQL.Append(sSQLClaimF + " AND FUNDS.TRANS_ID = FTS.TRANS_ID ");
                        sbSQL.Append(" AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE AND CODES.RELATED_CODE_ID = RESERVE_CURRENT.RESERVE_TYPE_CODE ");
                        sbSQL.Append(" AND FUNDS.TRANS_DATE <= '" + sTodaysdate + "'");
                        sbSQL.Append(" AND FUNDS.VOID_FLAG = 0 ");

                        sSQLPayment = sSQLPayment + sbSQL.ToString();
                        sSQLPayment = sSQLPayment + " AND FUNDS.PAYMENT_FLAG <> 0), RESERVE_CURRENT.DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(System.DateTime.Now) + "' WHERE " + sSQLClaimR;

                        sSQLCollect = sSQLCollect + sbSQL.ToString() + " AND FUNDS.PAYMENT_FLAG = 0), RESERVE_CURRENT.DTTM_RCD_LAST_UPD = '" + Conversion.ToDbDateTime(System.DateTime.Now) + "' WHERE " + sSQLClaimR;
                    }

                    // Execute the Insert/Update in transaction mode.
                    try
                    {
                        if (objConn == null)
                            objConn = DbFactory.GetDbConnection(m_sConnectionString);
                        objConn.Open();

                        objCommand = objConn.CreateCommand();
                        objTrans = objConn.BeginTransaction();
                        objCommand.Transaction = objTrans;

                        objCommand.CommandText = sSQLPayment.ToString();
                        objCommand.ExecuteNonQuery();

                        objCommand.CommandText = sSQLCollect.ToString();
                        objCommand.ExecuteNonQuery();

                        //Success: Commit Transaction
                        objTrans.Commit();
                        objConn.Close();
                    }
                    catch (Exception objException)
                    {
                        //Failure: Rollback Transaction
                        objTrans.Rollback();
                        objConn.Close();
                        throw new RMAppException(Globalization.GetString("ReserveFunds.UpdateFundsReserveCurrent.DBTransErr", m_iClientId), objException);
                    }

                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append("SELECT PAID_TOTAL,COLLECTION_TOTAL,RESERVE_AMOUNT,");
                    sbSQL.Append("RESERVE_TYPE_CODE FROM RESERVE_CURRENT WHERE " + sSQLClaimR);

                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                    if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                    {
                        sbSQL.Remove(0, sbSQL.Length);
                        try
                        {
                            if (objConn == null)
                                objConn = DbFactory.GetDbConnection(m_sConnectionString);

                            objConn.Open();
                            objCommand = objConn.CreateCommand();
                            objTrans = objConn.BeginTransaction();
                            objCommand.Transaction = objTrans;

                            for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                            {
                                objDataRow = objDataSet.Tables[0].Rows[iCnt];
                                dblPaid = Conversion.ConvertStrToDouble(objDataRow["PAID_TOTAL"].ToString());
                                dblCollect = Conversion.ConvertStrToDouble(objDataRow["COLLECTION_TOTAL"].ToString());
                                dblAmount = Conversion.ConvertStrToDouble(objDataRow["RESERVE_AMOUNT"].ToString());
                                iReserveID = Conversion.ConvertStrToInteger(objDataRow["RESERVE_TYPE_CODE"].ToString());

                                //Compute balance and incurred
                                dblBalance = CalculateReserveBalance(dblAmount, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, iCStat, iReserveID, objLOBSettings.PerRsvCollInRsvBal, p_iLOB);// asharma326 JIRA 870 add objLOBSettings.PerRsvCollInRsvBal, p_iLOB
                             //Change by kuladeep for mits:25817 Start
                                    //dblIncurred = CalculateIncurred(dblAmount, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, objLOBSettings.CollInIncurred);
                                dblIncurred = CalculateIncurred(dblBalance, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, objLOBSettings.CollInIncurred, iReserveID, objLOBSettings.PerRsvCollInRsvBal, objLOBSettings.PerRsvCollInIncurred, p_iLOB);//asharma326 JIRA 870 add objLOBSettings.PerRsvCollInRsvBal, objLOBSettings.PerRsvCollInIncurred, p_iLOB
                                    //Change by kuladeep for mits:25817 End

                                //Auto adjust reserve amount if necessary
                                if (objLOBSettings.AdjRsv && dblBalance < -0.009)
                                {
                                    dblAmount = dblAmount + Math.Abs(dblBalance);
                                    dblChangeAmount = Math.Abs(dblBalance);

                                    //Compute balance and incurred
                                    dblBalance = CalculateReserveBalance(dblAmount, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, iCStat, iReserveID, objLOBSettings.PerRsvCollInRsvBal, p_iLOB);//asharma326 JIRA 870 add objLOBSettings.PerRsvCollInRsvBal, p_iLOB
                                   //Change by kuladeep for mits:25817 Start
                                        //dblIncurred = CalculateIncurred(dblAmount, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, objLOBSettings.CollInIncurred);
                                    dblIncurred = CalculateIncurred(dblBalance, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, objLOBSettings.CollInIncurred, iReserveID, objLOBSettings.PerRsvCollInRsvBal, objLOBSettings.PerRsvCollInIncurred, p_iLOB);//asharma326 JIRA 870 add objLOBSettings.PerRsvCollInRsvBal, objLOBSettings.PerRsvCollInIncurred, p_iLOB


                                    //flag need to post reserve history entry
                                    bAutoAdjLog = true;
                                }

                                //AutoAdjust Reserves Setting Per Reserve Type
                                if (objLOBSettings.AdjRsvPerClaim && dblBalance < -0.009)
                                {
                                    if (objLOBSettings.AdjNegResTypesPerRsv.ContainsKey(iReserveID + "0"))   // "0" is for claim type - not used in this collection but the syssettings code is generic and puts it in anyhow
                                    {
                                        // add amount to reserve to get balance up to 0
                                        dblAmount = dblAmount + Math.Abs(dblBalance);
                                        dblChangeAmount = Math.Abs(dblBalance);

                                        //Compute balance and incurred
                                        dblBalance = CalculateReserveBalance(dblAmount, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, iCStat, iReserveID, objLOBSettings.PerRsvCollInRsvBal, p_iLOB);//asharma326 JIRA 870 add objLOBSettings.PerRsvCollInRsvBal, p_iLOB
                                          //Change by kuladeep for mits:25817 Start
                                        //dblIncurred = CalculateIncurred(dblAmount, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, objLOBSettings.CollInIncurred);
                                        dblIncurred = CalculateIncurred(dblBalance, dblPaid, dblCollect, objLOBSettings.CollInRsvBal, objLOBSettings.CollInIncurred, iReserveID, objLOBSettings.PerRsvCollInRsvBal, objLOBSettings.PerRsvCollInIncurred, p_iLOB);//asharma326 JIRA 870 add objLOBSettings.PerRsvCollInRsvBal, objLOBSettings.PerRsvCollInIncurred, p_iLOB


                                        //flag need to post reserve history entry
                                        bAutoAdjLog = true;
                                    }
                                }

                                sbSQL.Remove(0, sbSQL.Length);
                                sbSQL.Append(" UPDATE RESERVE_CURRENT SET RESERVE_AMOUNT = " + dblAmount);
                                sbSQL.Append(", BALANCE_AMOUNT = " + dblBalance.ToString());
                                sbSQL.Append(", INCURRED_AMOUNT = " + dblIncurred.ToString());
                                sbSQL.Append(", DTTM_RCD_LAST_UPD = " + Conversion.ToDbDateTime(System.DateTime.Now));
                                sbSQL.Append(" WHERE " + sSQLClaimR + " AND RESERVE_CURRENT.RESERVE_TYPE_CODE = " + iReserveID);

                                objCommand.CommandText = sbSQL.ToString();
                                objCommand.ExecuteNonQuery();

                                if (bAutoAdjLog)
                                {
                                    //Log changes in RESERVE_HISTORY table
                                    //Get new GUID for RESERVE_HISTORY
                                    iResHistoryGUID = Utilities.GetNextUID(objConn, "RESERVE_HISTORY", objTrans, m_iClientId);

                                    sbSQL.Remove(0, sbSQL.Length);
                                    sbSQL.Append(" INSERT INTO RESERVE_HISTORY(RSV_ROW_ID,CLAIM_ID,RESERVE_TYPE_CODE,CLAIMANT_EID,UNIT_ID, ");
                                    sbSQL.Append(" RESERVE_AMOUNT,PAID_TOTAL,COLLECTION_TOTAL,INCURRED_AMOUNT,BALANCE_AMOUNT,CHANGE_AMOUNT, ");
                                    sbSQL.Append(" REASON,DATE_ENTERED,DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER,CLOSED_FLAG,AUTO_ADJ_FLAG)");
                                    sbSQL.Append(" VALUES (" + iResHistoryGUID + ", " + p_iClaimID + "," + iReserveID + ", " + p_iClaimantEID);
                                    sbSQL.Append(", " + p_iUnitID + ", " + dblAmount + ", " + dblPaid);
                                    sbSQL.Append(", " + dblCollect + ", " + dblIncurred + ", " + dblBalance + ", " + dblChangeAmount + ", '" + REASON);
                                    sbSQL.Append("' , '" + System.DateTime.Now.ToString("d") + "', '" + Conversion.ToDbDateTime(System.DateTime.Now) + "', '");
                                    sbSQL.Append(p_sLoginName + "', '" + Conversion.ToDbDateTime(System.DateTime.Now) + "', '");
                                    sbSQL.Append(p_sLoginName + "', 0 , " + (bAutoAdjLog ? -1 : 0).ToString() + " )");

                                    objCommand.CommandText = sbSQL.ToString();
                                    objCommand.ExecuteNonQuery();

                                    sbSQL.Remove(0, sbSQL.Length);
                                    sbSQL.Append(" UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + Conversion.ToDbDateTime(System.DateTime.Now));
                                    sbSQL.Append("'  WHERE SYSTEM_TABLE_NAME = 'RESERVE_HISTORY' ");

                                    objCommand.CommandText = sbSQL.ToString();
                                    objCommand.ExecuteNonQuery();

                                    sbSQL.Remove(0, sbSQL.Length);

                                }//end if(bAutoAdjLog)
                            }//end for(iCnt=0; iCnt< objDataSet.Tables[0].Rows.Count; iCnt++)

                            //Success: Commit Transaction
                            objTrans.Commit();
                            objConn.Close();
                        }//end try
                        catch (Exception objException)
                        {
                            //Failure: Rollback Transaction
                            objTrans.Rollback();
                            objConn.Close();
                            throw new RMAppException(Globalization.GetString("ReserveFunds.UpdateFundsReserveCurrent.DBTransErr", m_iClientId), objException);
                        }
                    }//end if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)					
                }//if(p_iClaimID != 0 && p_iLOB != 0)
            }//end try
            catch (Exception objException)
            {
                throw objException;
            }
            finally
            {
                sbSQL = null;
                objLOB = null;
                objLOBSettings = null;
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
                objDataRow = null;
                if (objTrans != null)
                {
                    objTrans.Dispose();
                    objTrans = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                objCommand = null;
            }
        }//End function UpdateFundsReserveCurrent
        #endregion

        #endregion

        #region Private Methods

        protected void GetSecurityIDS(int lClaimID, int lClaimantEID, int lUnitID, int lLOB)
        {
            if (m_iParentSecurityId == 0 || m_iSecurityId == 0)
            {
                // Look up LOB if it is not passed - some methods don't have it
                if (lLOB == 0)
                {
                    DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT CLAIM.LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + lClaimID);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            lLOB = objReader.GetInt32("LINE_OF_BUS_CODE");
                        }
                        objReader.Close();
                        objReader.Dispose();
                    }//if (objReader != null)
                }

                switch (lLOB)
                {
                    case 241:
                        if (lClaimantEID == 0)
                        { // claim level
                            m_iParentSecurityId = GC_SID;
                            m_iSecurityId = GC_RSRV_SID;
                        }
                        else
                        { // detail level - assume coming from claimant screen
                            m_iParentSecurityId = GC_CLMNTS_SID;
                            m_iSecurityId = GC_CLMNTS_RSRV_SID;
                        }
                        break;
                    case 242:
                        if (lClaimantEID != 0)
                        { // detail level - coming from claimant screen
                            m_iParentSecurityId = VA_CLMNTS_SID;
                            m_iSecurityId = VA_CLMNTS_RSRV_SID;
                        }
                        else if (lUnitID != 0)
                        { // detail level - coming from unit screen
                            m_iParentSecurityId = VA_UNIT_SID;
                            m_iSecurityId = VA_UNIT_RSRV_SID;
                        }
                        else
                        { // claim level
                            m_iParentSecurityId = VA_SID;
                            m_iSecurityId = VA_RSRV_SID;
                        }
                        break;
                    case 243:
                        m_iParentSecurityId = WC_SID;
                        m_iSecurityId = WC_RSRV_SID;
                        break;
                    case 844:
                        m_iParentSecurityId = DI_SID;
                        m_iSecurityId = DI_RSRV_SID;
                        break;
                    case 845://MGaba2:MITS 21484:Adding case for property claims
                        if (lClaimantEID == 0)
                        {
                            m_iParentSecurityId = PC_SID;
                            m_iSecurityId = PC_RSRV_SID;
                        }
                        else
                        {
                            m_iParentSecurityId = PC_CLMNTS_SID;
                            m_iSecurityId = PC_CLMNTS_RSRV_SID;
                        }
                        break;
                    default:
                        throw new RMAppException("Can't find line of business for security context in ReserveFunds (ClaimID: " + lClaimID + ")");
                };
            }

            // Check 
        }

        #region private GetReserveTracking(p_iClaimId, p_sShortCode)

        /// Name		: GetReserveTracking
        /// Author		: Navneet Sota
        /// Date Created: 11/18/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used for Retreiving the level of ReserveTracking
        /// </summary>
        /// <remarks>		
        /// Retreive the level of Reserve-Tracking
        /// </remarks>
        /// <param name="p_iClaimId">The Claim Id</param>
        /// <param name="p_sShortCode">Reference Variable: Short Code</param>
        private int GetReserveTracking(int p_iClaimId, ref string p_sShortCode)
        {
            //Constant
            const int LINEOFBUSINESS = 243;
            //Integer Variables
            int iLOB = 0;
            int iReserveTracking = 0;

            //LOB settings
            Riskmaster.Settings.ColLobSettings objLOB = null;
            Riskmaster.Settings.LobSettings objLOBSettings = null;

            //String Objects
            StringBuilder sbSQL = null;

            //Database objects
            DbReader objReader = null;
            DbConnection objConn = null;

            try
            {
                //Get the Values of LOB and Code Description
                sbSQL = new StringBuilder();

                //Table[0]
                sbSQL.Append("SELECT CLAIM.LINE_OF_BUS_CODE  LOB, CODES_TEXT.Code_Desc  CODEDESC");
                sbSQL.Append(" FROM CLAIM, CODES, CODES_TEXT ");
                sbSQL.Append(" WHERE CLAIM.CLAIM_STATUS_CODE = CODES.CODE_ID ");
                sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = CODES_TEXT.CODE_ID ");
                sbSQL.Append(" AND CLAIM_ID = " + p_iClaimId);

                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();

                objReader = objConn.ExecuteReader(sbSQL.ToString());
                if (objReader.Read())
                {
                    iLOB = Conversion.ConvertStrToInteger(objReader.GetValue("LOB").ToString());
                    p_sShortCode = objReader.GetValue("CODEDESC").ToString();
                    objReader.Close();
                }

                objConn.Close();

                //Retreive the level of ReserveTracking
                if (iLOB == LINEOFBUSINESS)
                    iReserveTracking = 2;
                else
                {
                    //Instantiate the LOB Settings Class
                    objLOB = new Riskmaster.Settings.ColLobSettings(m_sConnectionString, m_iClientId);
                    objLOBSettings = objLOB[iLOB];
                    if (objLOBSettings != null)
                        iReserveTracking = objLOBSettings.ReserveTracking;
                    else
                        iReserveTracking = 2;
                }
            }//End Main Try
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesTracking.DataErr", m_iClientId), objException);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();

                if (objConn != null)
                    objConn.Dispose();

                sbSQL = null;
                objLOB = null;
                objLOBSettings = null;
            }
            //Return the Result to calling function
            return iReserveTracking;
        }
        #endregion

        #region private CalculateIncurred (p_dBalance, p_dPaid, p_dCollect, p_bCollInRsvBal)

        /// Name		: CalculateIncurred
        /// Author		: Navneet Sota
        /// Date Created: 11/01/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used calculating the Incurred amount on the basis of 
        /// total amount collected and paid.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_dBalance">The remaining balance</param>
        /// <param name="p_dPaid">The amount already paid to the Claimant</param>
        /// <param name="p_dCollect">The amount already collected.</param>
        /// <param name="p_bCollInRsvBal"></param>

        private double CalculateIncurred(double p_dBalance, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, bool p_bCollInIncurredBal, int p_iReserveTypeCode, bool p_bCollInPerRsvBal, bool p_bCollInPerIncurredBal, int p_iLOB)//Shruti for MITS 8551//asharma326 JIRA 870 add int p_iReserveTypeCode, bool p_bCollInPerRsvBal, bool p_bCollInPerIncurredBal,int p_iLOB
        {
            double dTmp2 = 0;
            double dTmp = 0;

            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            bool bIsRecoveryReserve = false;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString, m_iClientId);
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            if (p_iReserveTypeCode != 0 && (objCache.GetRelatedShortCode(p_iReserveTypeCode) == "R"))
            {
                //This is a recovery reserve.. Incurred would be calculated differently
                //for calculation details please refer to rmA13.1 financial equivalency design document

                bIsRecoveryReserve = true;
            }
            objCache.Dispose();
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            if (bIsRecoveryReserve)
            {
                //dTmp = p_dCollect * -1; // For recovery reserves Incurred is just what has been collected so far  //MITS 35837
                if (p_dBalance < 0)
                {
                    dTmp = p_dCollect;  //MITS 35837
                }
                else
                {
                    dTmp = p_dBalance + p_dCollect;  //MITS 35837
                }
            }
            else
            {
                //asharma326 JIRA 870 starts
                if (p_bCollInPerRsvBal)
                    p_bCollInPerRsvBal = this.CheckPerRsvSettings(-1, p_iReserveTypeCode, p_iLOB);
                if (p_bCollInPerIncurredBal)
                    p_bCollInPerIncurredBal = this.CheckPerRsvSettings(0, p_iReserveTypeCode, p_iLOB);
                //asharma326 JIRA 870 ends
                if (p_bCollInRsvBal || p_bCollInPerRsvBal)
                {
                    dTmp2 = p_dPaid - p_dCollect;
                    //if (dTmp2 < 0) //rsushilaggar MITS 29710 Date 12/03/2012
                    //{
                    //    dTmp2 = 0;
                    //}
                    if (p_dBalance < 0)
                    {
                        dTmp = dTmp2;
                    }
                    else
                    {
                        dTmp = p_dBalance + dTmp2;
                    }
                }
                else
                {
                    if (p_dBalance < 0)
                    {
                        dTmp = p_dPaid;
                    }
                    else
                    {
                        dTmp = p_dBalance + p_dPaid;
                    }
                }
            }
            if (((p_bCollInIncurredBal) && (!bIsRecoveryReserve)) || p_bCollInPerIncurredBal)
            {
                dTmp = dTmp - p_dCollect;
            }
            //if (dTmp < 0)  //mcapps2 We are no longer going to set negative amounts to zero
            //{
            //    dTmp = 0; //Leave this because of conversions, to make sure there is not a negative incurred
            //}
            return dTmp;
        }
        #endregion
        //asharma326 JIRA 870 Starts
        private bool CheckPerRsvSettings(int p_iPerRsvFlag, int p_iReserveTypeCode, int p_iLob)
        {
            List<string> lstResTypeCode = null;
            string sSQL = string.Empty;
            //string sConnectionString = m_sConnectionString;
            try
            {
                lstResTypeCode = new List<string>();
                sSQL = string.Format("SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL WHERE LINE_OF_BUS_CODE = {0}" +
                                   "AND COLL_IN_RSV_BAL = {1}", p_iLob, p_iPerRsvFlag);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        lstResTypeCode.Add(Convert.ToString(objReader.GetValue("RES_TYPE_CODE")));
                    }
                }

                if (!lstResTypeCode.Contains(Convert.ToString(p_iReserveTypeCode)))
                {
                    return false;
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            finally
            {
                lstResTypeCode = null;
            }

            return true;
        }
        //asharma326 JIRA 870 Ends
        #region private CalculateReserveBalance (p_dReserve, p_dPaid, p_dCollect, p_bCollInRsvBal, p_iCStat)
        /// Name		: CalculateReserveBalance
        /// Author		: Navneet Sota
        /// Date Created: 11/02/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used calculating the Reserve balance on the basis of 
        /// total amount collected and paid.
        /// </summary>
        /// <remarks>		
        /// </remarks>
        /// <param name="p_dReserve">The Reserve amount</param>
        /// <param name="p_dPaid">The amount already paid to the Claimant</param>
        /// <param name="p_dCollect">The amount already collected.</param>
        /// <param name="p_bCollInRsvBal"></param>
        /// <param name="p_iCStat">Optional parameter</param>
        private double CalculateReserveBalance(double p_dReserve, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, int p_iCStat, int p_iReserveTypeCode, bool p_bCollInPerRsvBal, int p_iLOB)//asharma326 JIRA 870 int p_iReserveTypeCode, bool p_bCollInPerRsvBal, int p_iLOB)
        {
            double dReturnValue = 0;
            double dTemp = 0;

            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            bool bIsRecoveryReserve = false;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString, m_iClientId);
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            if (objCache.GetRelatedShortCode(p_iReserveTypeCode) == "R")
            {
                //This is a recovery reserve.. Balance would be calculated differently
                //for calculation details please refer to rmA13.1 financial equivalency design document

                bIsRecoveryReserve = true;
            }
           objCache.Dispose();
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
           //asharma326 JIRA 870 Starts
           if (p_bCollInPerRsvBal && !bIsRecoveryReserve)
           {
               p_bCollInPerRsvBal = this.CheckPerRsvSettings(-1, p_iReserveTypeCode, p_iLOB);
           }
           //asharma326 JIRA 870 ends
            if (bIsRecoveryReserve)
            {
                //dTemp = p_dCollect; //Paid Total would always be 0 for recovery reserves
                dReturnValue = p_dReserve - p_dCollect;
            }
            else if (p_bCollInRsvBal || p_bCollInPerRsvBal)
            {
                dTemp = p_dPaid - p_dCollect;
                //if (dTemp < 0) //rsushilaggar MITS 29710 Date 12/03/2012
                //    dTemp = 0;
                dReturnValue = p_dReserve - dTemp;
            }
            else
            {
                dReturnValue = p_dReserve - p_dPaid;
            }
            //if (p_iCStat == 8 && dReturnValue < 0 && Do_Nothing != true)  // mcapps2  we are no longer going to set negative amounts to zero
            //{
            //    dReturnValue = 0;
            //}
            return dReturnValue;
        }
        # endregion

        #region (Overloaded)private CalculateReserveBalance (p_dReserve, p_dPaid, p_dCollect, p_bCollInRsvBal)

        /// Name		: CalculateReserveBalance
        /// Author		: Navneet Sota
        /// Date Created: 11/02/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This method is basically used calculating the Reserve balance on the basis of 
        /// total amount collected and paid.
        /// </summary>
        /// <remarks>		
        /// This is an overloaded function for calling the function without p_iCStat value.
        /// This internally calls the same function with p_iCStat = 0
        /// </remarks>
        /// <param name="p_dReserve">The Reserve amount</param>
        /// <param name="p_dPaid">The amount already paid to the Claimant</param>
        /// <param name="p_dCollect">The amount already collected.</param>
        /// <param name="p_bCollInRsvBal"></param>
        /// <param name="p_iCStat">Optional parameter</param>
        private double CalculateReserveBalance(double p_dReserve, double p_dPaid, double p_dCollect,
            bool p_bCollInRsvBal, bool p_bCollInPerRsvBal, int p_ilob)//asharma326 JIRA 870 add bool p_bCollInPerRsvBal,int p_ilob
        {
            double dReturnValue = 0;
            dReturnValue = CalculateReserveBalance(p_dReserve, p_dPaid, p_dCollect, p_bCollInRsvBal, 0, 0, p_bCollInPerRsvBal, p_ilob);//asharma326 JIRA 870 add  p_bCollInPerRsvBal, p_ilob

            return dReturnValue;
        }   
        #endregion

        //#region private CheckReserveLimits(p_iClaimId, p_iReserveTypeCode, p_dAmount, p_iUserId, p_iGroupId)

        ///// Name		: CheckReserveLimits
        ///// Author		: Navneet Sota
        ///// Date Created: 11/02/2004
        ///// ************************************************************
        ///// Amendment History
        ///// ************************************************************
        ///// Date Amended   *   Amendment   *    Author
        ///// ************************************************************
        ///// <summary>		
        ///// This method is used for checking the reserve limits from database against a particular claim
        ///// and reserve type.
        ///// </summary>
        ///// <remarks>		
        ///// </remarks>
        ///// <param name="p_iClaimId">Claim Id</param>
        ///// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        ///// <param name="p_dAmount">amount</param>
        ///// <param name="p_iUserId">User Id</param>
        ///// <param name="p_iGroupId">Group Id</param>

        //private bool CheckReserveLimits(int p_iClaimId, int p_iReserveTypeCode, double p_dAmount,
        //    int p_iUserId, int p_iGroupId)
        //{
        //    //double
        //    double dMaxAmount = 0.0;

        //    //String Objects
        //    StringBuilder sbSQL = null;

        //    //Database objects
        //    DbReader objReader = null;
        //    DbConnection objConn = null;

        //    bool bReturnValue = true;

        //    try
        //    {
        //        sbSQL = new StringBuilder();

        //        sbSQL.Append("SELECT MAX(MAX_AMOUNT)  MAX_AMT FROM RESERVE_LIMITS,CLAIM ");
        //        sbSQL.Append(" WHERE (USER_ID = " + p_iUserId);
        //        if (p_iGroupId != 0)
        //            sbSQL.Append(" OR GROUP_ID = " + p_iGroupId);

        //        sbSQL.Append(" ) AND CLAIM.CLAIM_ID = " + p_iClaimId);
        //        sbSQL.Append(" AND CLAIM.LINE_OF_BUS_CODE = RESERVE_LIMITS.LINE_OF_BUS_CODE");
        //        sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode);

        //        objConn = DbFactory.GetDbConnection(m_sConnectionString);
        //        objConn.Open();

        //        objReader = objConn.ExecuteReader(sbSQL.ToString());

        //        if (objReader.Read())
        //        {
        //            if (objReader.GetValue("MAX_AMT") is DBNull)
        //                bReturnValue = false;

        //            if (bReturnValue)
        //            {
        //                dMaxAmount = Conversion.ConvertStrToDouble(objReader.GetValue("MAX_AMT").ToString());
        //                if (p_dAmount > dMaxAmount)
        //                    bReturnValue = true;
        //                else
        //                    bReturnValue = false;
        //            }
        //            objReader.Close();
        //        }
        //        objConn.Close();

        //    }
        //    catch (Exception objException)
        //    {
        //        throw new RMAppException(Globalization.GetString("ReserveFunds.CheckReserveLimits.DataErr"), objException);
        //    }
        //    finally
        //    {
        //        sbSQL = null;
        //        if (objReader != null)
        //            objReader.Dispose();

        //        if (objConn != null)
        //            objConn.Dispose();
        //    }//End finally

        //    return bReturnValue;
        //}
        //#endregion

        //#region private GetFundsAmount(p_iClaimid, p_iClaimantEID, p_iUnitID, p_iReserveType, p_bPayments)

        ///// Name		: GetFundsAmount
        ///// Author		: Navneet Sota
        ///// Date Created: 11/02/2004
        ///// ************************************************************
        ///// Amendment History
        ///// ************************************************************
        ///// Date Amended   *   Amendment   *    Author
        ///// ************************************************************
        ///// <summary>		
        ///// This method is used for fetching the total funds amount from database.
        ///// </summary>
        ///// <remarks>		
        ///// </remarks>
        ///// <param name="p_iClaimId">Claim Id</param>
        ///// <param name="p_iClaimantEID">Claimant Id</param>
        ///// <param name="p_iUnitID">Unit Id</param>
        ///// <param name="p_iReserveType">Reserve Type</param>
        ///// <param name="p_bPayments">Payments done or not</param>

        //private double GetFundsAmount(int p_iClaimid, int p_iClaimantEID, int p_iUnitID,
        //    int p_iReserveType, bool p_bPayments)
        //{
        //    //String Objects
        //    StringBuilder sbSQL = null;

        //    //Database objects
        //    DbReader objReader = null;
        //    DbConnection objConn = null;

        //    double dReturnValue = 0.0;

        //    try
        //    {
        //        sbSQL = new StringBuilder();

        //        sbSQL.Append(" SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT)  AMOUNT FROM FUNDS,FUNDS_TRANS_SPLIT,CODES WHERE ");
        //        sbSQL.Append(" FUNDS.CLAIM_ID = " + p_iClaimid);
        //        sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEID);
        //        sbSQL.Append(" AND FUNDS.UNIT_ID = " + p_iUnitID);
        //        sbSQL.Append(" AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
        //        sbSQL.Append(" AND CODES.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE ");
        //        sbSQL.Append(" AND CODES.RELATED_CODE_ID = " + p_iReserveType);
        //        sbSQL.Append(" AND FUNDS.TRANS_DATE <= '" + Conversion.ToDbDate(System.DateTime.Now) + "' ");
        //        sbSQL.Append(" AND FUNDS.VOID_FLAG = 0 ");

        //        if (p_bPayments)
        //            sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0 ");
        //        else
        //            sbSQL.Append(" AND FUNDS.PAYMENT_FLAG = 0 ");

        //        objConn = DbFactory.GetDbConnection(m_sConnectionString);
        //        objConn.Open();

        //        objReader = objConn.ExecuteReader(sbSQL.ToString());

        //        if (objReader.Read())
        //        {
        //            dReturnValue = Conversion.ConvertStrToDouble(objReader.GetValue("AMOUNT").ToString());
        //            objReader.Close();
        //        }

        //        objConn.Close();

        //        return dReturnValue;
        //    }
        //    catch (Exception objException)
        //    {
        //        throw new RMAppException(Globalization.GetString("ReserveFunds.GetFundsAmount.DataErr"), objException);
        //    }
        //    finally
        //    {
        //        sbSQL = null;
        //        if (objReader != null)
        //            objReader.Dispose();
        //        if (objConn != null)
        //            objConn.Dispose();
        //    }//End finally
        //}
        //#endregion

        #region JoinIt(p_sFrom, p_sWhere,p_sTempJoin,p_sTempJoinAnother,p_sField1,p_sField2)
        /// Name		: JoinIt
        /// Author		: Navneet Sota
        /// Date Created: 11/18/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>		
        /// This function forms the FROM & WHERE part of the SQL query.
        /// </summary>
        /// <param name="p_sFrom">FROM part of the query</param>
        /// <param name="p_sWhere">WHERE part of the query</param>
        /// <param name="p_sTempJoin">Join statement before column name</param>
        /// <param name="p_sTempJoinAnother">Join statement after column name</param>
        /// <param name="p_sField">Field of the database</param>
        private void JoinIt(ref string p_sFrom, ref string p_sWhere, string p_sTempJoin, string p_sTempJoinAnother, string p_sField1, string p_sField2)
        {
            DbConnection objConn = null;
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                m_sDBType = objConn.DatabaseType.ToString().ToUpper();
                objConn.Close();

                if (m_sDBType == Constants.DB_ACCESS.ToUpper())
                {
                    p_sFrom = "(" + p_sFrom + " LEFT JOIN " + p_sTempJoinAnother;
                    p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + ")";
                }
                else if ((m_sDBType == Constants.DB_SQLSRVR.ToUpper()) || (m_sDBType == Constants.DB_SYBASE.ToUpper()))
                {
                    //compatibility Issue 90
                    p_sFrom = "(" + p_sFrom + " LEFT OUTER JOIN " + p_sTempJoinAnother;
                    p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + ")";
                    //p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    //if (p_sWhere != "")
                    //    p_sWhere = p_sWhere + " AND ";
                    //p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField1 + " *= " + p_sTempJoinAnother + "." + p_sField2;
                }
                else if (m_sDBType == Constants.DB_INFORMIX.ToUpper())
                {
                    p_sFrom = p_sFrom + ",OUTER " + p_sTempJoinAnother;
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2;
                }
                else if (m_sDBType == Constants.DB_ORACLE.ToUpper())
                {
                    p_sFrom = p_sFrom + "," + p_sTempJoinAnother;
                    if (p_sWhere != "")
                        p_sWhere = p_sWhere + " AND ";
                    p_sWhere = p_sWhere + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + "(+)";
                }
                else if (m_sDBType == Constants.DB_DB2.ToUpper()) //DB2 Support
                {
                    p_sFrom = "(" + p_sFrom + " LEFT OUTER JOIN " + p_sTempJoinAnother;
                    p_sFrom = p_sFrom + " ON " + p_sTempJoin + "." + p_sField1 + " = " + p_sTempJoinAnother + "." + p_sField2 + ")";
                }
            }//End Main try block
            catch (Exception objException)
            {
                throw new StringException(Globalization.GetString("ReserveFunds.JoinIt.JoinQueries", m_iClientId), objException);
            }
            finally
            {
                if (objConn != null)
                    objConn.Dispose();
            }
        }//End function
        #endregion

        #region RemoveButton(XmlDocument p_objSearchDocument,XmlDocument p_objRemoveDocument, string p_sNodeSearchName,string p_sAttrRemoveName)
        /// Name			: RemoveButton
        /// Author			: Umesh
        /// Date Created	: 05-Jan-2007
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended	*   Amendment   *    Author
        ///					*				*
        ///					*				*	
        /// ************************************************************
        /// <summary>
        /// Remove Buttons on basis of Customization-> Custom setting
        /// <param name="p_objSearchDocument">
        /// Input Xml from which the setting of buttons is decided.
        /// </param>
        /// <param name="p_objRemoveDocument">
        /// Input Xml in which the attibutes corresponding to buttons are set
        /// </param>
        /// </summary>
        private void RemoveButton(XmlDocument p_objSearchDocument, XmlDocument p_objRemoveDocument,
            string p_sNodeSearchName, string p_sAttrRemoveName)
        {
            try
            {
                XmlNode objSearch = p_objSearchDocument.SelectSingleNode(p_sNodeSearchName);
                if (objSearch != null)
                {

                    if (objSearch.InnerText != "-1")
                    {

                        XmlNode objAttr = p_objRemoveDocument.SelectSingleNode(p_sAttrRemoveName);
                        objAttr.InnerText = "0";
                    }

                }
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.RemoveButton.error", m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("DocumentManager.RemoveButton.error", m_iClientId), p_objException);
            }
            finally
            {

            }
        }
        #endregion
        #region GetNewElement(p_sNodeName)
        /// Name		: GetNewElement
        /// Author		: Navneet Sota
        /// Date Created: 11/18/2004
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        /// ************************************************************
        /// <summary>
        /// Creats a new XMLElement with the nodename passed as parameter
        /// </summary>
        /// <param name="p_sNodeName">Node Name</param>
        /// <returns>New XML Element</returns>
        private XmlElement GetNewElement(string p_sNodeName)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;
            try
            {
                objXDoc = new XmlDocument();
                objXMLElement = objXDoc.CreateElement(p_sNodeName);
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetNewElement.XMLError", m_iClientId), objException);
            }
            finally
            {
                objXDoc = null;
            }
            //Return the Result to calling function
            return objXMLElement;

        }//End function
        #endregion

        #region IsClaimOpen( int p_iClaimId , ref int p_iStatusCode )
        /// <summary>
        /// Return true if Claim is Open.
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_iStatusCode">Status Code</param>
        /// <returns>Claim Open Flag</returns>
        private bool IsClaimOpen(int p_iClaimId, ref int p_iStatusCode)
        {
            DbReader objReader = null;
            LocalCache objLocalCache = null;

            bool bIsClaimOpen = false;
            string sSQL = "";

            try
            {
                sSQL = "SELECT CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        p_iStatusCode = Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_STATUS_CODE"), m_iClientId);
                        objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                        if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(p_iStatusCode)).ToUpper() == "O")
                            bIsClaimOpen = true;
                    }
                    objReader.Close();
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.IsClaimOpen.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objLocalCache = null;
            }
            return (bIsClaimOpen);
        }

        #endregion

        #region private GetTAndESecurity(int p_iSecId)
        private bool GetTAndESecurity(int p_iSecId)
        {
            bool bReturnValue = true;

            try
            {
                switch (p_iSecId)
                {
                    case GC_SID:
                        if (!m_userLogin.IsAllowedEx(GC_TANDE_SID))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case WC_SID:
                        if (!m_userLogin.IsAllowedEx(WC_TANDE_SID))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case VA_SID:
                        if (!m_userLogin.IsAllowedEx(VA_TANDE_SID))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    case DI_SID:
                        if (!m_userLogin.IsAllowedEx(DI_TANDE_SID))
                            bReturnValue = false;
                        else
                            bReturnValue = true;
                        break;
                    default:
                        bReturnValue = true;
                        break;
                }
                return bReturnValue;
            }
            catch (Exception ex)
            {
                throw new RMAppException("Can't find security context for Time And Expense in ReserveFunds.", ex);
            }
        }
        #endregion

        #endregion







        /// Name		: GetClaimEventAndPolicyDate
        /// Author		: Priya Mittal
        /// Date Created: 06/03/08
        /// <summary>
        /// MITS 11756-Gets the Claim,Event and policy date corresponding to a claim-used for filtering
        /// </summary>
        /// <param name="p_sClaimId">Claim Id</param>
        /// <param name="p_sDateOfClaim">Claim Date</param>
        /// <param name="p_sDateOfEvent">Event Date</param>
        /// <param name="p_sDateOfPolicy">Policy Date</param>
        /// <returns>All values are returned in ref variables</returns>
        private void GetClaimEventAndPolicyDate(int p_sClaimId, ref string p_sDateOfClaim, ref string p_sDateOfEvent, ref string p_sDateOfPolicy)
        {
            string sSQL = string.Empty;	//sql string
            DbReader objReader = null;	//Data reader object
            bool bEnhPolicySystem = false;

            //Start - 07/23/2010: Sumit - Added CLAIM.LINE_OF_BUS_CODE to fetch LOB which is used to check if Enh policy is active for this fetched LOB.
            int iLOB = 0;
            bool bSuccess = false;
            //sSQL = "SELECT CLAIM.DATE_OF_CLAIM,EVENT.DATE_OF_EVENT FROM CLAIM,EVENT WHERE CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + p_sClaimId;
            sSQL = "SELECT CLAIM.DATE_OF_CLAIM,CLAIM.LINE_OF_BUS_CODE,EVENT.DATE_OF_EVENT FROM CLAIM,EVENT WHERE CLAIM.EVENT_ID=EVENT.EVENT_ID AND CLAIM.CLAIM_ID=" + p_sClaimId;

            try
            {
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    p_sDateOfClaim = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CLAIM"));
                    p_sDateOfEvent = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_EVENT"));
                    iLOB = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("LINE_OF_BUS_CODE")), out bSuccess);
                }
                objReader.Close();

                //sSQL = "SELECT USE_ENH_POL_FLAG FROM SYS_PARMS";
                //objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                //if (objReader.Read())
                //{
                //    bEnhPolicySystem = Conversion.ConvertObjToBool(objReader.GetValue("USE_ENH_POL_FLAG"));
                //}
                //objReader.Close();

                ColLobSettings objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                if (objColLobSettings[iLOB].UseEnhPolFlag == -1)
                {
                    bEnhPolicySystem = true;
                }
                //End: Sumit

                if (bEnhPolicySystem)
                {
                    int iPolicyId = 0;
                    sSQL = "SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE CLAIM_ID =" + p_sClaimId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        iPolicyId = Conversion.ConvertObjToInt(objReader.GetValue("PRIMARY_POLICY_ID"), m_iClientId);
                    }
                    objReader.Close();

                    sSQL = "SELECT EFFECTIVE_DATE FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = (SELECT MAX(POLICY_X_TERM_ENH.TERM_NUMBER) FROM POLICY_X_TERM_ENH,POLICY_ENH,CLAIM WHERE POLICY_X_TERM_ENH.POLICY_ID = POLICY_ENH.POLICY_ID AND CLAIM.PRIMARY_POLICY_ID = POLICY_ENH.POLICY_ID AND CLAIM.CLAIM_ID =" + p_sClaimId + ")";
                }
                else
                {
                    sSQL = "SELECT POLICY.EFFECTIVE_DATE FROM POLICY,CLAIM WHERE CLAIM.PRIMARY_POLICY_ID = POLICY.POLICY_ID AND CLAIM.CLAIM_ID =" + p_sClaimId;
                }

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    p_sDateOfPolicy = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                }
                objReader.Close();
            }
            catch (DataModelException p_objExp)
            {
                throw p_objExp;
            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetClaimEventAndPolicyDate.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objReader != null) objReader.Dispose();
            }
        }

        private string GetFormTitle(Claim objClaim)
        {
            string sCaption = "";
            int captionLevel = 0;
            string sTmp = "";
            Event objEvent = (objClaim.Parent as Event);
            LobSettings objLobSettings = null;
            //nadim for 15014
            string sClaimant = "";
            string sDepartment = "";
            //nadim for 15014

            //Handle User Specific Caption Level
            if (!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId != 0)
            {
                objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
                if (objLobSettings.CaptionLevel != 0)
                    captionLevel = objLobSettings.CaptionLevel;
                else
                    captionLevel = 1006;
                //nadim for 15014
                if (captionLevel < 1005 || captionLevel > 1012)
                    captionLevel = 0;

                if (captionLevel != 1012)
                    //nadim for 15014
                    //sCaption = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                    sDepartment = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                else
                {
                    objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid, ref sCaption, ref sTmp);
                    //nadim for 15014
                    //sCaption += sTmp;
                    sDepartment += sTmp;
                }
                //nadim for 15014
                //sCaption = sCaption.Substring(0, Math.Min(sCaption.Length, 25));
                sDepartment = sDepartment.Substring(0, Math.Min(sDepartment.Length, 25));
                //nadim for 15014
                //sCaption = " [" + objClaim.ClaimNumber + " * " + sDepartment+" ]";
                if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                {
                    sClaimant = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                }         //nadim for 15014      

                else
                {
                    //sCaption = " [" + objClaim.ClaimNumber + " * " + sCaption + " * - ]";
                    sClaimant = objClaim.PrimaryPiEmployee.PiEntity.LastName + "," + objClaim.PrimaryPiEmployee.PiEntity.FirstName;
                }
                if (sClaimant != "")
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sDepartment + " * " + sClaimant + " ]"; //tmalhotra2 MITS-29967 edit
                else
                    sCaption = " [" + objClaim.ClaimNumber + " * " + sDepartment + " ]";
                //nadim for 15014
            }
            return sCaption;
        }
        private string GetClaimantFormTitle(Claim objClaim, int iClaimantEID)
        {
            string sCaption = "";
            int captionLevel = 0;
            string sTmp = "";
            Event objEvent = (objClaim.Parent as Event);
            LobSettings objLobSettings = null;
            string sClaimant = "";
            string sDepartment = "";

            //Handle User Specific Caption Level
            if (!objClaim.IsNew || objClaim.PrimaryClaimant.ClaimantRowId != 0)
            {
                objLobSettings = objClaim.Context.InternalSettings.ColLobSettings[objClaim.LineOfBusCode];
                if (objLobSettings.CaptionLevel != 0)
                    captionLevel = objLobSettings.CaptionLevel;
                else
                    captionLevel = 1006;
                if (captionLevel < 1005 || captionLevel > 1012)
                    captionLevel = 0;

                if (captionLevel != 1012)
                    sDepartment = objClaim.Context.InternalSettings.CacheFunctions.GetOrgParent(objEvent.DeptEid, captionLevel, 0, ref captionLevel);//captionLevel discarded
                else
                {
                    objClaim.Context.InternalSettings.CacheFunctions.GetOrgInfo(objEvent.DeptEid, ref sCaption, ref sTmp);
                    sDepartment += sTmp;
                }
                sDepartment = sDepartment.Substring(0, Math.Min(sDepartment.Length, 25));
                if (iClaimantEID > 0)
                {
                    sClaimant = sGetClaimantName(iClaimantEID);
                    if (sClaimant != "")
                    {
                        sCaption = " [" + objClaim.ClaimNumber + " * " + sDepartment + " * " + sClaimant + " ]";
                    }
                    else
                    {
                        sCaption = " [" + objClaim.ClaimNumber + " * " + sDepartment + " ]";
                    }
                }
                else
                {
                    if (objClaim.EventId != 0 && objClaim.PrimaryClaimant != null && objClaim.PrimaryClaimant.ClaimantEntity != null)
                    {
                        sClaimant = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                    }
                    else
                    {
                        sClaimant = objClaim.PrimaryPiEmployee.PiEntity.LastName + "," + objClaim.PrimaryPiEmployee.PiEntity.FirstName;
                    }
                    if (sClaimant != "")
                    {
                        sCaption = " [" + objClaim.ClaimNumber + " * " + sDepartment + " * " + sClaimant + " ]";
                    }
                    else
                    {
                        sCaption = " [" + objClaim.ClaimNumber + " * " + sDepartment + " ]";
                    }
                }
            }
            return sCaption;
        }
        private string sGetClaimantName(int iClaimantEID)
        {
            string sSQL = string.Empty;
            string sCaption = string.Empty;
            DbReader objReader = null;

            if (iClaimantEID > 0)
            {
                sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + iClaimantEID;
            }
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    sCaption = objReader.GetValue("LAST_NAME").ToString();
                    // akaushik5 Changed for MITS 33577 Starts
                    //if (objReader.GetValue("FIRST_NAME").ToString() == "")
                    //{
                    //    sCaption += " " + objReader.GetValue("FIRST_NAME").ToString();
                    //}
                    string sFirstName = objReader.GetValue("FIRST_NAME").ToString();
                    if (!string.IsNullOrEmpty(sFirstName))
                    {
                        sCaption += ", " + sFirstName;
                    }
                    // akaushik5 Changed for MITS 33577 Ends
                    sCaption = sCaption.Trim();
                }
            }
            if (objReader != null)
            {
                objReader.Close();
                objReader.Dispose();
            }
            return sCaption;
        }

        // akaushik5 Added for MITS 33577 Starts
        /// <summary>
        /// Gets the lob string.
        /// </summary>
        /// <param name="lobCode">The lob code.</param>
        /// <returns></returns>
        private static string GetLobString(int lobCode)
        {
            string sLob = string.Empty;

            switch (lobCode)
            {
                case 241:
                    sLob = "General Claim";
                    break;
                case 242:
                    sLob = "Vehicle Accident";
                    break;
                case 844:
                    sLob = "Non Occupational";
                    break;
                case 845:
                    sLob = "Property Claim";
                    break;
                default:
                    sLob = "Workers' Compensation";
                    break;
            }
            return sLob;
        }
        // akaushik5 Added for MITS 33577 Ends

        public bool IsIncludeClaimType(string p_sTableNm, string p_sPolicySystemId)
        {
            bool bResult = false;
            int iTableId = Int32.MinValue, iIncludeClmType = Int32.MinValue;
            string sSQL = string.Empty;

            LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId);
            iTableId = objCache.GetTableId(p_sTableNm);

            try
            {
                sSQL = "SELECT INCLUDE_CLAIM_TYPE FROM PS_MAP_TABLES, POLICY_X_WEB WHERE RMX_TABLE_ID= " + iTableId.ToString();
                sSQL = sSQL + " AND POLICY_X_WEB.POLICY_SYSTEM_ID = " + p_sPolicySystemId + " AND POLICY_X_WEB.POLICY_SYSTEM_CODE = PS_MAP_TABLES.POLICY_SYSTEM_TYPE_ID";
                using (DbReader oReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (oReader.Read())
                    {
                        iIncludeClmType = Conversion.ConvertObjToInt(oReader.GetValue("INCLUDE_CLAIM_TYPE"), m_iClientId);
                    }
                }
                if (iIncludeClmType == 1)
                    bResult = true;
            }
            catch (Exception e)
            {
                bResult = false;
                throw e;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }

            return bResult;
        }

        //spahariya MITS 30911 overload method for Get look up data with loss short code - start
        /// <summary>
        /// GetLookupXml
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <returns></returns>
        public XmlDocument GetLookupXml(int p_iClaimID, int p_iReserveID, bool p_bFNOL)
        {
            XmlDocument objXmlDoc = null;
            Claim objClaim = null;
            Policy objPolicy = null;
            StringBuilder sb = null;
            StringBuilder sbSQL = null;
            int iLOB = 0;
            //LOB settings
            Riskmaster.Settings.ColLobSettings objLOB = null;
            Riskmaster.Settings.LobSettings objLOBSettings = null;
            int iPolicyID = 0;
            LocalCache objCache = null;
            StringBuilder sbReserves = null;
            int iCnt = 0;
            DataSet objDataSet = null;
            DataSet objDataCvgLossLOB = null;
            string sReserveTypeCode = String.Empty;
            int iCoverageTypeCode = 0;
            int iLossTypeCode = 0;
            int iPolcvgRowId = 0;
            DataSet objDS = null; //rupal:mits 27459
            DataSet objDataSetSubType = null;
            string sDisabilityTypeCode = String.Empty;
            string sCodeDesc = string.Empty;
            StringBuilder sbDisCat = null;
            try
            {

                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimID);
                iLOB = objClaim.LineOfBusCode;

                objXmlDoc = new XmlDocument();
                sb = new StringBuilder();
                sbSQL = new StringBuilder();

                //Instantiate the LOB Settings Class
                objLOB = new Riskmaster.Settings.ColLobSettings(m_sConnectionString, m_iClientId);
                objLOBSettings = objLOB[iLOB];
                if (objLOBSettings != null)
                {
                    if (objLOBSettings.ResByClmType)
                    {
                        sbSQL.Append(" SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES ");
                        sbSQL.Append(" WHERE LINE_OF_BUS_CODE = " + iLOB);
                        sbSQL.Append(" AND CLAIM_TYPE_CODE = " + objClaim.ClaimTypeCode);
                    }
                    else
                    {
                        sbSQL.Append(" SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES ");
                        sbSQL.Append(" WHERE LINE_OF_BUS_CODE = " + iLOB);
                    }
                }//end if(objLOBSettings != null)               


                if (objCache == null)  // only allocate once and then use for subsequent ones
                    objCache = new LocalCache(m_sConnectionString, m_iClientId);

                sbReserves = new StringBuilder();

                objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                {
                    for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                    {
                        sReserveTypeCode = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["RESERVE_TYPE_CODE"]);
                        sbReserves.Append(@"<reserve name=""" + objCache.GetCodeDesc(int.Parse(sReserveTypeCode)) + @"""" + @" value=""" + sReserveTypeCode + @""" />");
                    }
                }

                sbSQL.Length = 0;
                //Aman ML Change
                if (iLOB == 243)
                {
                    //skhare7 Policy interfcae reserve category
                    sbSQL = new StringBuilder();
                    sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                    sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'DISABILITY_CATEGORY' ");
                    sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                    //Start PARAG MITS 10269
                    sbSQL.Append(" AND CODES.DELETED_FLAG!=-1 ");

                    sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "DISABILITY_CATEGORY", this.LanguageCode); //Aman ML Change
                    if (objCache == null)  // only allocate once and then use for subsequent ones
                        objCache = new LocalCache(m_sConnectionString, m_iClientId);

                    sbDisCat = new StringBuilder();

                    objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
                    if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                    {
                        for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                        {
                            sDisabilityTypeCode = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["CODE_ID"]);
                            sCodeDesc = Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["CODE_DESC"]);
                            sCodeDesc = System.Net.WebUtility.HtmlEncode(sCodeDesc);  //mkaran2 - [JIRA] (RMA-4110) MITS 37158
                            sbDisCat.Append(@"<disabilitycat name=""" + sCodeDesc + @"""" + @" value=""" + sDisabilityTypeCode + @"""/>");
                        }
                    }

                    sbSQL.Length = 0;

                }
                sb.Append("<BOB>");

                objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);

                foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                {
                    iPolicyID = objClaimXPolicy.PolicyId;

                    objPolicy.MoveTo(iPolicyID);
                    sb.Append(@"<policy name=""" + objPolicy.PolicyName + @"""" + @" value=""" + objPolicy.PolicyId + @""">");
                    //rupal:mits 27459
                    //RUPAL:MITS 27459
                    sbSQL = new StringBuilder();
                    sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, VEHICLE.VIN Unit");
                    sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                    if (objPolicy.PolicySystemId > 0)
                    {
                        sbSQL = sbSQL.Append(string.Format(" INNER JOIN UNIT_X_CLAIM ON UNIT_X_CLAIM.UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='V' AND POLICY_X_UNIT.POLICY_ID = {0} AND UNIT_X_CLAIM.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                    }
                    sbSQL = sbSQL.Append(" INNER JOIN VEHICLE ON VEHICLE.UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND POLICY_X_UNIT.POLICY_ID= " + objPolicy.PolicyId.ToString());

                    sbSQL = sbSQL.Append(" UNION");
                    sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, PROPERTY_UNIT.PIN Unit"
                        );
                    sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                    if (objPolicy.PolicySystemId > 0)
                    {
                        sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_PROPERTYLOSS ON CLAIM_X_PROPERTYLOSS.PROPERTY_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID={0} AND CLAIM_X_PROPERTYLOSS.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                    }
                    sbSQL = sbSQL.Append(" INNER JOIN PROPERTY_UNIT ON PROPERTY_UNIT.PROPERTY_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID= " + objPolicy.PolicyId.ToString());


                    //rupal:start, policy system interface changes, as of now site and other unit will be available for policy system interface only                        
                    if (objPolicy.PolicySystemId > 0)
                    {
                        if (iLOB == 243)//site unit will be available for WC only
                        {
                            //SITE UNIT - S
                            sbSQL = sbSQL.Append(" UNION");
                            sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, SITE_UNIT.NAME Unit");
                            sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                            sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_SITELOSS ON CLAIM_X_SITELOSS.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_SITELOSS.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                            sbSQL = sbSQL.Append(" INNER JOIN SITE_UNIT ON SITE_UNIT.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID= " + objPolicy.PolicyId.ToString());
                        }
                        //OTHER UNIT - SU
                        sbSQL = sbSQL.Append(" UNION");
                        sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId, ENTITY.LAST_NAME Unit");
                        sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                        sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_OTHERUNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='SU' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_OTHERUNIT.CLAIM_ID={1} ", objPolicy.PolicyId.ToString(), objClaim.ClaimId.ToString()));
                        sbSQL = sbSQL.Append(" INNER JOIN OTHER_UNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=OTHER_UNIT.OTHER_UNIT_ID");
                        sbSQL = sbSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID=OTHER_UNIT.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");
                    }
                    //rupal:end
                    string sCoverageText = string.Empty;
                    using (objDS = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId))
                    {
                        if (objDS != null && objDS.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < objDS.Tables[0].Rows.Count; i++)
                            {
                                int iPolicyUnitRowId = Conversion.ConvertObjToInt(objDS.Tables[0].Rows[i]["PolUnitRowId"], m_iClientId);
                                sb.Append(@"<unit name=""" + objDS.Tables[0].Rows[i]["Unit"].ToString() + @"""" + @" value=""" + iPolicyUnitRowId + @""">");
                                sbSQL = new StringBuilder();
                                sbSQL.Append("SELECT COVERAGE_TYPE_CODE , POLCVG_ROW_ID,COVERAGE_TEXT,CVG_SEQUENCE_NO FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID = " + iPolicyUnitRowId);
                                using (objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId))
                                {
                                    if (objDataSet != null && objDataSet.Tables[0].Rows.Count > 0)
                                    {
                                        for (iCnt = 0; iCnt < objDataSet.Tables[0].Rows.Count; iCnt++)
                                        {
                                            iCoverageTypeCode = Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[iCnt]["COVERAGE_TYPE_CODE"], m_iClientId);
                                            iPolcvgRowId = Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"], m_iClientId);

                                            if (objPolicy.PolicySystemId > 0)
                                            {
                                                sCoverageText = GetCoverageText(iPolicyUnitRowId, Conversion.ConvertObjToInt(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"], m_iClientId), iCoverageTypeCode);
                                                sb.Append(@"<coverage name=""" + sCoverageText + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"]) + "#" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["CVG_SEQUENCE_NO"]) + @""">");
                                            }
                                            else
                                            {
                                                sb.Append(@"<coverage name=""" + objCache.GetCodeDesc(iCoverageTypeCode) + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataSet.Tables[0].Rows[iCnt]["POLCVG_ROW_ID"]) + @""">");
                                            }
                                            sb.Append(sbReserves.ToString());

                                            //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
                                            if (!int.Equals(p_iReserveID, 0))
                                            {
                                                //Aman ML Change
                                                sbSQL = new StringBuilder();
                                                //sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                                                //sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                                                //sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                                                //sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_SUB_TYPE' ");
                                                //sbSQL.Append(" AND CODES.DELETED_FLAG != -1 ");
                                                //sbSQL.Append(" And CODES.RELATED_CODE_ID IN (" + p_iReserveID + ") ");
                                                //sbSQL.Append(" Order By CODES.RELATED_CODE_ID, CODES_TEXT.CODE_DESC ");
                                                sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID, CODES.RELATED_CODE_ID ");
                                                sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                                                sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                                                sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_SUB_TYPE' ");
                                                sbSQL.Append(" AND CODES.DELETED_FLAG != -1 AND CODES_TEXT.LANGUAGE_CODE = 1033");
                                                sbSQL.Append(" AND CODES.RELATED_CODE_ID IN (" + p_iReserveID + ") ");
                                                sbSQL.Append(" ORDER BY CODES.RELATED_CODE_ID, CODES_TEXT.CODE_DESC ");
                                                sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_SUB_TYPE", this.LanguageCode);//Aman ML Change --end
                                                objDataSetSubType = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);

                                                if (objDataSetSubType != null && objDataSetSubType.Tables[0].Rows.Count > 0)
                                                {
                                                    for (int iCount = 0; iCount < objDataSetSubType.Tables[0].Rows.Count; iCount++)
                                                    {
                                                        //sb.Append(@"<reservesubtype name=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount]["CODE_DESC"]) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount]["CODE_ID"]) + @""" />");
                                                        sb.Append(@"<reservesubtype name=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][1]) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][2]) + @""" />"); //Aman ML Change
                                                    }
                                                }
                                            }
                                            //Ankit End

                                            if (iLOB != 243)
                                            {
                                                sbSQL = new StringBuilder();
                                                //spahariya MITS 31978 to avoid dups loss type for mu;ltiple reserve types 
                                                //sbSQL.Append("SELECT POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE,LOSS_CODE  FROM CVG_LOSS_LOB_MAPPING,POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =CVG_LOSS_LOB_MAPPING.CVG_TYPE_CODE AND POLICY_LOB=" + objPolicy.PolicyLOB + " AND POLCVG_ROW_ID = " + iPolcvgRowId);
                                                sbSQL.Append("SELECT DISTINCT POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE,LOSS_CODE  FROM CVG_LOSS_LOB_MAPPING,POLICY_X_CVG_TYPE WHERE POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE =CVG_LOSS_LOB_MAPPING.CVG_TYPE_CODE AND POLICY_LOB=" + objPolicy.PolicyLOB + " AND POLCVG_ROW_ID = " + iPolcvgRowId);
                                                using (objDataCvgLossLOB = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId))
                                                {
                                                    if (objDataCvgLossLOB != null && objDataCvgLossLOB.Tables[0].Rows.Count > 0)
                                                    {
                                                        for (int iCounter = 0; iCounter < objDataCvgLossLOB.Tables[0].Rows.Count; iCounter++)
                                                        {
                                                            iLossTypeCode = Conversion.ConvertObjToInt(objDataCvgLossLOB.Tables[0].Rows[iCounter]["LOSS_CODE"], m_iClientId);
                                                            //    iPolcvgRowId = Conversion.ConvertObjToInt(objDataSet.Tables[0], m_iClientId.Rows[iCnt]["POLCVG_ROW_ID"]);
                                                            //spahariya MITS30911 added short code for Loss code in FNOL
                                                            sb.Append(@"<loss name=""" + objCache.GetCodeDesc(iLossTypeCode) + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataCvgLossLOB.Tables[0].Rows[iCounter]["LOSS_CODE"]) + @""">");
                                                            //sb.Append(@"<loss name=""" + objCache.GetShortCode(iLossTypeCode) + " " + objCache.GetCodeDesc(iLossTypeCode) + @"""" + @" value=""" + Conversion.ConvertObjToStr(objDataCvgLossLOB.Tables[0].Rows[iCounter]["LOSS_CODE"]) + @""">");
                                                            sb.Append(sbReserves.ToString());

                                                            //Ankit Start : Financial Enhancement - Reserve Sub Type Changes
                                                            if (objDataSetSubType != null && objDataSetSubType.Tables[0].Rows.Count > 0)
                                                            {
                                                                for (int iCount = 0; iCount < objDataSetSubType.Tables[0].Rows.Count; iCount++)
                                                                {
                                                                    sb.Append(@"<reservesubtype name=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount][1]) + @"""" + @" value=""" + Convert.ToString(objDataSetSubType.Tables[0].Rows[iCount]["CODE_ID"]) + @""" />");
                                                                }
                                                            }
                                                            //Ankit End
                                                            sb.Append("</loss>");
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                sb.Append(sbDisCat.ToString());
                                            }
                                            sb.Append("</coverage>");
                                        }
                                    }
                                }
                                sb.Append("</unit>");
                                sbSQL.Length = 0;
                            }
                        }
                    }
                    sb.Append("</policy>");
                }


                sb.Append("</BOB>");
                string sXml = Utilities.AddTags(sb.ToString());

                objXmlDoc.LoadXml(sXml);
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetLookupXml.GenericError", m_iClientId), objException);
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                    objPolicy = null;
                }
                if (sb != null)
                {
                    sb = null;
                }
                if (sbSQL != null)
                {
                    sbSQL = null;
                }
                if (sbReserves != null)
                {
                    sbReserves = null;
                }
                if (objLOB != null)
                {
                    objLOB = null;
                }
                if (objLOBSettings != null)
                {
                    objLOBSettings = null;
                }
                if (objDataSet != null)
                {
                    objDataSet = null;
                }
                if (objDataCvgLossLOB != null)
                {
                    objDataCvgLossLOB = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                //rupal:mits 27459
                if (objDS != null)
                {
                    objDS = null;
                }
                //rupal

                //rupal:mits 30187
                if (objDataSetSubType != null)
                {
                    objDataSetSubType = null;
                }
                //rupal
            }

            //Return the result to calling function
            return objXmlDoc;
        }

        //spahariya MITS 30911 - End

        //sharishkumar: start Mits35472       

        public void LSSReserveExport(string p_iReserveId)
        {
            string sDTTM = String.Empty;
            string sUser = String.Empty;
            sDTTM = System.DateTime.Now.ToString("yyyyMMddHHmmss");
            DbConnection objConn = null;
            ReserveCurrent objReserveCurrent = null;
            string[] rcRowId = new string[0];
            string sDBType = string.Empty;
            string sSQL = string.Empty;
            // this will pick user who is being used for RMX LSS interface. Update done by this user won't trigger export process otherwise it will be a circular process between RMX and LSS
            objConn = DbFactory.GetDbConnection(m_sConnectionString);
            objConn.Open();
            try
            {
                sUser = objConn.ExecuteString("SELECT RMX_LSS_USER FROM SYS_PARMS");
                sDBType = DbFactory.GetDatabaseType(m_sConnectionString).ToString();

                if (sDBType == Constants.DB_SQLSRVR)
                            {
                                 sSQL = "SELECT ISNULL(MAX(GROUP_ID), 0) FROM RM_LSS_RESERVE_EXP";
                            }
                else if (sDBType == Constants.DB_ORACLE)
                {
                     sSQL = "SELECT NVL(MAX(GROUP_ID), 0) FROM RM_LSS_RESERVE_EXP";
                }                

                int maxGroupID = Convert.ToInt32(DbFactory.ExecuteScalar(m_sConnectionString, sSQL));
                maxGroupID = maxGroupID + 1;
                if (m_objDataModelFactory == null)
                {
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                }
                if (p_iReserveId.Contains(","))
                {
                    rcRowId = p_iReserveId.Split(',');
                }

                if (rcRowId.Length > 0)
                {
                    for (int i = 0; i < rcRowId.Length; i++)
                    {
                        objConn.ExecuteString("UPDATE RESERVE_CURRENT SET LSS_RES_EXP_FLAG = -1 where RC_ROW_ID = " + rcRowId[i]);

                        if (sUser != m_objDataModelFactory.Context.RMUser.LoginName)
                        {
                            objConn.ExecuteString("INSERT INTO RM_LSS_RESERVE_EXP (RESERVE_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG,GROUP_ID) VALUES ('" + rcRowId[i] + "', '" + sDTTM + "', 'RESERVE', 0," + maxGroupID + ")");
                        }
                    }
                }
                else
                {
                    objConn.ExecuteString("UPDATE RESERVE_CURRENT SET LSS_RES_EXP_FLAG = -1 where RC_ROW_ID = " + p_iReserveId);
                    
                    if (sUser != m_objDataModelFactory.Context.RMUser.LoginName)
                    {
                        objConn.ExecuteString("INSERT INTO RM_LSS_RESERVE_EXP (RESERVE_ID, DTTM_CHANGED, TYPE_TEXT, SENT_FLAG,GROUP_ID) VALUES ('" + p_iReserveId + "', '" + sDTTM + "', 'RESERVE', 0," + maxGroupID + ")");
                    }
                }               
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objReserveCurrent = null;
                if (objConn != null)
                {
                    if (objConn.State == ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn = null;
                }
            }
        }
        //End Mits 35472
        /// <summary>
        /// Will probably need to move this code to datamodel layer.
        /// Redistributes the deductible reserve in accordance with changes in the Indemnity/Expense reserves.
        /// By the time the control reaches here the deductible reserve corresponding to the edited reserve is already modified
        /// </summary>
        private void RedistributeDeductibleReserve(int p_iRecoveryReserveTypeCode, int p_iCoverageId, int p_iClaimId, int p_iPolCvgLossRowId, int p_iCurrentDedRcRowId, double p_dReducedAmount, 
            bool p_bExcludeExpense, double p_dChangedAmount, int p_iCovGroupId, int p_iEventId)
        {
            double dTotalReserveOnClaim = 0d;
            double dTotalDeductibleReserveOnClaim = 0d;
            double dTotalReserveOnEvent = 0d;
            double dTotalDeductibleReserveOnEvent = 0d;
            double dAggLimitOnGroup = 0d;
            double dTotalDeductibleReserveOnGroup = 0d;
            StringBuilder sbSQL = null;
            DataModelFactory oFactory = null;
            ReserveCurrent oReserveCurrent = null;
            bool bRedestributionComplete = false;
            //HashSet<int> hSetRcRowIds = null;
            Dictionary<int, int> dictinRcRowClaimant = null;
            double dSumReserveAmountOnLossId = 0d;
            double dDedReserveOnLossId = 0d;
            double dTotalAmountToRedistribute = p_dChangedAmount;
            int iRcRowId = 0;
            int iClaimantEid = 0;
            int iCurrentPolCvgLossRowId = 0;
            bool bDeductiblePerEventFlag = false;
            double dDedPerEventAmt = 0d;
            double dApplicableLimit = 0d;
             //Added by Nikhil -  Include applicable reserve type 
            int iCountMapReserveType = 0;

            try
            {
                oFactory = new DataModelFactory(m_userLogin,m_iClientId);
                sbSQL = new StringBuilder();

                if (p_iCovGroupId > 0)
                {
                    bDeductiblePerEventFlag = GetDeductiblePerEventFlag(p_iCovGroupId);
                    if (bDeductiblePerEventFlag)
                    {
                    sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                    sbSQL.Append(" INNER JOIN CLAIM C ON RC.CLAIM_ID = C.CLAIM_ID ");
                    //changed by Nikhil.Code review changes
                    //sbSQL.Append(" WHERE C.EVENT_ID = " + p_iEventId);
                    sbSQL.Append(" WHERE C.EVENT_ID = ").Append(p_iEventId);
                    sbSQL.Append(" AND POLCVG_ROW_ID IN ");
                    sbSQL.Append(GetCoverageGroupQuery(p_iEventId, p_iCovGroupId));
                    //changed by Nikhil.Code review changes
                  //  sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iRecoveryReserveTypeCode);
                    sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(p_iRecoveryReserveTypeCode);

                        dTotalDeductibleReserveOnEvent = oFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    }

                    sbSQL.Length = 0;
                    sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                    sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON RC.POLCVG_ROW_ID = CXPD.POLCVG_ROW_ID ");
                    sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                    //start -  changed by Nikhil.Code review changes
                    //sbSQL.Append(" AND PXIG.COV_GROUP_CODE = " + p_iCovGroupId);
                   // sbSQL.Append(" AND RC.RESERVE_TYPE_CODE = " + p_iRecoveryReserveTypeCode);
                    sbSQL.Append(" AND PXIG.COV_GROUP_CODE = ").Append(p_iCovGroupId);
                    sbSQL.Append(" AND RC.RESERVE_TYPE_CODE = ").Append(p_iRecoveryReserveTypeCode);
                    //end -  changed by Nikhil.Code review changes
                    dTotalDeductibleReserveOnGroup = oFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    //Changed by Nikhil.Code review changes
                    //dAggLimitOnGroup = oFactory.Context.DbConnLookup.ExecuteDouble("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = " + p_iCovGroupId);
                    dAggLimitOnGroup = oFactory.Context.DbConnLookup.ExecuteDouble(String.Format("SELECT AGG_LIMIT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = {0}",p_iCovGroupId));
                }
                else
                {
                    //We need to calculate the total reserve on claim on this coverage and the total deductible reserve on claim on this coverage.
                    sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                    //start -  changed by Nikhil.Code review changes
                    //sbSQL.Append(" CLAIM_ID = " + p_iClaimId);
                    //sbSQL.Append(" AND POLCVG_ROW_ID = " + p_iCoverageId);
                    //sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iRecoveryReserveTypeCode);
                    sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                    sbSQL.Append(" AND POLCVG_ROW_ID = ").Append(p_iCoverageId);
                    sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(p_iRecoveryReserveTypeCode);
                    //end -  changed by Nikhil.Code review changes
                    dTotalDeductibleReserveOnClaim = oFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                }
                

                //Sum of deductible recovery reserve on coverage can never be greater than the deductible on coverage.
                //If the deductible recovery reserve on coverage is still equal to deductible on coverage then redistribution is not required. (Redistribution NOT required if dTotalDeductibleReserveOnClaim == p_dReducedAmount)
                //If the deductible recovery reserve on coverage was already less than deductible on coverage then redistribution is not required. (Redistribution NOT required if dTotalDeductibleReserveOnClaim + p_dChangedAmount < p_dReducedAmount)
                //This leaves us with only one condition when redistribution is required
                //Start - Added by Nikhil to check for aggregate limit if greater then 0.
                //if ((dTotalDeductibleReserveOnClaim + p_dChangedAmount == p_dReducedAmount) 
                //    || (dTotalDeductibleReserveOnEvent + p_dChangedAmount == p_dReducedAmount)
                //    || (dTotalDeductibleReserveOnGroup < dAggLimitOnGroup))
                    if ((dTotalDeductibleReserveOnClaim + p_dChangedAmount == p_dReducedAmount)
             || (dTotalDeductibleReserveOnEvent + p_dChangedAmount == p_dReducedAmount)
             || (dTotalDeductibleReserveOnGroup + p_dChangedAmount == p_dReducedAmount)
             || (dTotalDeductibleReserveOnGroup < dAggLimitOnGroup && dAggLimitOnGroup > 0))
                    //End - Added by Nikhil to check for aggregate limit if greater then 0.
                {
                    sbSQL.Length = 0;
                    if (p_iCovGroupId > 0 && bDeductiblePerEventFlag)
                    {
                        sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                        sbSQL.Append(" INNER JOIN CLAIM C ON RC.CLAIM_ID = C.CLAIM_ID ");
                        //  changed by Nikhil.Code review changes
                        //sbSQL.Append(" WHERE C.EVENT_ID = " + p_iEventId);
                        sbSQL.Append(" WHERE C.EVENT_ID = ").Append(p_iEventId);
                        sbSQL.Append(" AND POLCVG_ROW_ID IN ");
                        sbSQL.Append(GetCoverageGroupQuery(p_iEventId, p_iCovGroupId));
                         //changed by Nikhil.Code review changes
                        //sbSQL.Append(" AND RESERVE_TYPE_CODE <> " + p_iRecoveryReserveTypeCode);
                        sbSQL.Append(" AND RESERVE_TYPE_CODE <> ").Append(p_iRecoveryReserveTypeCode);
                        if (p_bExcludeExpense)
                        {
                            // changed by Nikhil.Code review changes
                           // sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  " + oFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE") + ")");
                            sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  ").Append(oFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE")).Append(")");
                        }

                        //Start - Added by Nikhil -  Include applicable reserve type 
                        //changed by Nikhil.Code review changes
                        //iCountMapReserveType = oFactory.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID=" + p_iCovGroupId);
                        iCountMapReserveType = oFactory.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0}" , p_iCovGroupId));
                        if (iCountMapReserveType > 0)
                            // changed by Nikhil.Code review changes
                            //sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = " + p_iCovGroupId + ")");
                            sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = ").Append(p_iCovGroupId).Append(")");
                       
                        //End - Added by Nikhil -  Include applicable reserve type 

                        dTotalReserveOnEvent = oFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    }
                    else
                    {
                        sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                        //start - changed by Nikhil.Code review changes
                        //sbSQL.Append(" CLAIM_ID = " + p_iClaimId);
                        //sbSQL.Append(" AND POLCVG_ROW_ID = " + p_iCoverageId);
                        //sbSQL.Append(" AND RESERVE_TYPE_CODE <> " + p_iRecoveryReserveTypeCode);
                        sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                        sbSQL.Append(" AND POLCVG_ROW_ID = ").Append(p_iCoverageId);
                        sbSQL.Append(" AND RESERVE_TYPE_CODE <> ").Append(p_iRecoveryReserveTypeCode);
                        //end -  changed by Nikhil.Code review changes
                        if (p_bExcludeExpense)
                        {
                            //sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  " + oFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE") + ")");
                            sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  ").Append(oFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE")).Append(")");
                        }

                        //Start - Added by Nikhil -  Include applicable reserve type 
                        if (p_iCovGroupId > 0)
                        {
                            //changed by Nikhil.Code review changes
                            //iCountMapReserveType = oFactory.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID=" + p_iCovGroupId);
                            iCountMapReserveType = oFactory.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0}",p_iCovGroupId));
                            if (iCountMapReserveType > 0)
                                //changed by Nikhil.Code review changes
                        //        sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = " + p_iCovGroupId + ")");
                                sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = ").Append(p_iCovGroupId).Append(")");
                        }
                        //End - Added by Nikhil -  Include applicable reserve type 
                        dTotalReserveOnClaim = oFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                    }
                    
                    

                    //Redistribution is required only when Reserve on coverage is more than deductible reserve on claim
                    //Start - Added by Nikhil to check for aggregate limit if greater then 0.
                  //  if (dTotalReserveOnClaim > dTotalDeductibleReserveOnClaim || dTotalReserveOnEvent > dTotalDeductibleReserveOnEvent || dTotalDeductibleReserveOnGroup < dAggLimitOnGroup)
                    if (dTotalReserveOnClaim > dTotalDeductibleReserveOnClaim || dTotalReserveOnEvent > dTotalDeductibleReserveOnEvent || (dTotalDeductibleReserveOnGroup < dAggLimitOnGroup && dAggLimitOnGroup > 0))
                    //End - Added by Nikhil to check for aggregate limit if greater then 0.
                        {
                        dictinRcRowClaimant = new Dictionary<int, int>();
                        //hSetRcRowIds = new HashSet<int>();

                        //We can skip the current deductible reserve (controlled by RcRowId)
                        sbSQL.Length = 0;
                        if (p_iCovGroupId>0)
                        {
                            if (bDeductiblePerEventFlag)
                            {
                                sbSQL.Append("SELECT RC_ROW_ID, RC.CLAIMANT_EID FROM RESERVE_CURRENT RC ");
                                sbSQL.Append(" INNER JOIN CLAIM C ON RC.CLAIM_ID = C.CLAIM_ID ");
                                //Changed by Nikhil.Code review changes.
                               // sbSQL.Append(" WHERE C.EVENT_ID = " + p_iEventId);
                                sbSQL.Append(" WHERE C.EVENT_ID = ").Append(p_iEventId);
                                sbSQL.Append(" AND POLCVG_ROW_ID IN ");
                                sbSQL.Append(GetCoverageGroupQuery(p_iEventId, p_iCovGroupId));
                                //Start - Changed by Nikhil.Code review changes.
                                //sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iRecoveryReserveTypeCode);
                                //sbSQL.Append(" AND RC_ROW_ID <> " + p_iCurrentDedRcRowId);
                                sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(p_iRecoveryReserveTypeCode);
                                sbSQL.Append(" AND RC_ROW_ID <> ").Append(p_iCurrentDedRcRowId);
                                //end - Changed by Nikhil.Code review changes.
                                sbSQL.Append(" UNION ");
                            }

                            sbSQL.Append(" SELECT RC_ROW_ID, RC.CLAIMANT_EID FROM RESERVE_CURRENT RC ");
                            sbSQL.Append(" INNER JOIN CLAIM_X_POL_DED CXPD ON RC.POLCVG_ROW_ID = CXPD.POLCVG_ROW_ID ");
                            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                            //Start - Changed by Nikhil.Code review changes.
                            //sbSQL.Append(" AND PXIG.COV_GROUP_CODE = " + p_iCovGroupId);
                            //sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iRecoveryReserveTypeCode);
                            //sbSQL.Append(" AND RC_ROW_ID <> " + p_iCurrentDedRcRowId);
                            sbSQL.Append(" AND PXIG.COV_GROUP_CODE = ").Append(p_iCovGroupId);
                            sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(p_iRecoveryReserveTypeCode);
                            sbSQL.Append(" AND RC_ROW_ID <> ").Append(p_iCurrentDedRcRowId);
                            //end - Changed by Nikhil.Code review changes.
                        }
                        else
                        {
                            sbSQL.Append("SELECT RC_ROW_ID, CLAIMANT_EID FROM RESERVE_CURRENT WHERE ");
                            //Start - Changed by Nikhil.Code review changes.
                            //sbSQL.Append(" CLAIM_ID = " + p_iClaimId);
                            //sbSQL.Append(" AND POLCVG_ROW_ID = " + p_iCoverageId);
                            //sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iRecoveryReserveTypeCode);
                            //sbSQL.Append(" AND RC_ROW_ID <> " + p_iCurrentDedRcRowId);
                            sbSQL.Append(" CLAIM_ID = ").Append(p_iClaimId);
                            sbSQL.Append(" AND POLCVG_ROW_ID = ").Append(p_iCoverageId);
                            sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(p_iRecoveryReserveTypeCode);
                            sbSQL.Append(" AND RC_ROW_ID <> ").Append(p_iCurrentDedRcRowId);
                            //end - Changed by Nikhil.Code review changes.
                        }

                        using (DbReader oDbReader = oFactory.Context.DbConnLookup.ExecuteReader(sbSQL.ToString()))
                        {
                            while (oDbReader.Read())
                            {
                                iRcRowId = oDbReader.GetInt32("RC_ROW_ID");
                                if (!dictinRcRowClaimant.ContainsKey(iRcRowId))
                                {
                                    dictinRcRowClaimant.Add(iRcRowId, oDbReader.GetInt32("CLAIMANT_EID"));
                                }
                            }
                        }

                        if (p_iCovGroupId>0)
                        {
                            //Start - Added by Nikhil to check for aggregate limit if greater then 0.
                          //  if (dTotalAmountToRedistribute + dTotalDeductibleReserveOnGroup > dAggLimitOnGroup)
                            if ((dTotalAmountToRedistribute + dTotalDeductibleReserveOnGroup > dAggLimitOnGroup ) && dAggLimitOnGroup > 0 )
                            //End - Added by Nikhil to check for aggregate limit if greater then 0.
                            {
                                dTotalAmountToRedistribute = dAggLimitOnGroup - dTotalDeductibleReserveOnGroup;
                            }
                        }

                        foreach(KeyValuePair<int, int> kValue in dictinRcRowClaimant)
                        //foreach (int iRcRowId in hSetRcRowIds)
                        {
                            iRcRowId = kValue.Key;
                            iClaimantEid = kValue.Value;

                            sbSQL.Length = 0;
                            //Changed by Nikhil.Code review changes.
                           // sbSQL.Append("SELECT RESERVE_AMOUNT, POLCVG_LOSS_ROW_ID FROM RESERVE_CURRENT WHERE RC_ROW_ID= " + iRcRowId);
                            sbSQL.Append("SELECT RESERVE_AMOUNT, POLCVG_LOSS_ROW_ID FROM RESERVE_CURRENT WHERE RC_ROW_ID= ").Append(iRcRowId);
                            using (DbReader oDbReader = m_objDataModelFactory.Context.DbConnLookup.ExecuteReader(sbSQL.ToString()))
                            {
                                if (oDbReader.Read())
                                {
                                    iCurrentPolCvgLossRowId = oDbReader.GetInt32("POLCVG_LOSS_ROW_ID");
                                    dDedReserveOnLossId = oDbReader.GetDouble("RESERVE_AMOUNT");
                                }
                            }

                            sbSQL.Length = 0;
                            sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE ");
                            //sbSQL.Append(" CLAIM_ID = " + p_iClaimId);
                            //sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + p_iPolCvgLossRowId);
                            //Start - Changed by Nikhil.Code review changes.
                            //sbSQL.Append(" POLCVG_LOSS_ROW_ID = " + iCurrentPolCvgLossRowId);
                            //sbSQL.Append(" AND RC_ROW_ID <> " + iRcRowId);
                            //sbSQL.Append(" AND RESERVE_TYPE_CODE <> " + p_iRecoveryReserveTypeCode);
                            //sbSQL.Append(" AND CLAIMANT_EID = " + iClaimantEid);
                            sbSQL.Append(" POLCVG_LOSS_ROW_ID = ").Append(iCurrentPolCvgLossRowId);
                            sbSQL.Append(" AND RC_ROW_ID <> ").Append(iRcRowId);
                            sbSQL.Append(" AND RESERVE_TYPE_CODE <> ").Append(p_iRecoveryReserveTypeCode);
                            sbSQL.Append(" AND CLAIMANT_EID = ").Append(iClaimantEid);
                            //end - Changed by Nikhil.Code review changes.
                            //Start - Added by Nikhil -   Added by Nikhil -  Include applicable reserve type and check for expense reserve
                            if (p_bExcludeExpense)
                            {
                                // Changed by Nikhil.Code review changes.
                                //sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  " + oFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE") + ")");
                                sbSQL.Append(" AND RESERVE_TYPE_CODE NOT IN (SELECT CODE_ID FROM CODES WHERE RELATED_CODE_ID =  ").Append(oFactory.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE")).Append(")");
                            }

                            if (p_iCovGroupId > 0)
                            {
                                // Changed by Nikhil.Code review changes.
                                //iCountMapReserveType = oFactory.Context.DbConnLookup.ExecuteInt("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID=" + p_iCovGroupId);
                                iCountMapReserveType = oFactory.Context.DbConnLookup.ExecuteInt(String.Format("SELECT COUNT(*) FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID={0}", p_iCovGroupId));
                                if (iCountMapReserveType > 0)
                                    //Changed by Nikhil.Code review changes.
                                    //sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = " + p_iCovGroupId + ")");
                                sbSQL.Append(" AND RESERVE_TYPE_CODE IN (SELECT RESERVE_TYPE_CODE FROM COV_GROUP_RESERVES WHERE COVERAGE_GROUP_ID = ").Append(p_iCovGroupId).Append(")");

                            }

                            //End - Added by Nikhil -   Added by Nikhil -  Include applicable reserve type and check for expense reserve

                            dSumReserveAmountOnLossId = oFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());

                            if (dSumReserveAmountOnLossId > dDedReserveOnLossId)
                            {
                                if (p_iCovGroupId>0 && bDeductiblePerEventFlag == true)
                                {
                                    // Changed by Nikhil.Code review changes.
                                    //dDedPerEventAmt = oFactory.Context.DbConnLookup.ExecuteDouble("SELECT SIR_DED_PEREVENT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = " + p_iCovGroupId);
                                    dDedPerEventAmt = oFactory.Context.DbConnLookup.ExecuteDouble(String.Format("SELECT SIR_DED_PEREVENT_AMT FROM CLAIM_X_POL_DED_AGG_LIMIT WHERE COV_GROUP_CODE = {0}",p_iCovGroupId));
                                    if (dDedPerEventAmt <= dDedReserveOnLossId)
                                    {
                                        continue;
                                    }
                                    sbSQL.Length = 0;
                                    // Changed by Nikhil.Code review changes.
                                    //int iEventId = oFactory.Context.DbConnLookup.ExecuteInt("SELECT EVENT_ID FROM CLAIM C INNER JOIN RESERVE_CURRENT RC ON C.CLAIM_ID = RC.CLAIM_ID WHERE RC.RC_ROW_ID=" + iRcRowId);
                                    int iEventId = oFactory.Context.DbConnLookup.ExecuteInt(String.Format("SELECT EVENT_ID FROM CLAIM C INNER JOIN RESERVE_CURRENT RC ON C.CLAIM_ID = RC.CLAIM_ID WHERE RC.RC_ROW_ID={0}", iRcRowId));
                                    
                                    sbSQL.Append("SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT RC ");
                                    sbSQL.Append(" INNER JOIN CLAIM C ON RC.CLAIM_ID = C.CLAIM_ID ");

                                    // Changed by Nikhil.Code review changes.
                                    //sbSQL.Append(" WHERE C.EVENT_ID = " + iEventId);
                                    sbSQL.Append(" WHERE C.EVENT_ID = ").Append(iEventId);
                                    sbSQL.Append(" AND POLCVG_ROW_ID IN ");
                                    sbSQL.Append(GetCoverageGroupQuery(iEventId, p_iCovGroupId));
                                    //Changed by Nikhil.Code review changes.
                                   // sbSQL.Append(" AND RESERVE_TYPE_CODE = " + p_iRecoveryReserveTypeCode);
                                    sbSQL.Append(" AND RESERVE_TYPE_CODE = ").Append(p_iRecoveryReserveTypeCode);

                                    dTotalDeductibleReserveOnEvent = oFactory.Context.DbConnLookup.ExecuteDouble(sbSQL.ToString());
                                    if (dDedPerEventAmt <= dTotalDeductibleReserveOnEvent)
                                    {
                                        continue;
                                    }
                                }
                                oReserveCurrent = oFactory.GetDataModelObject("ReserveCurrent", false) as ReserveCurrent;
                                oReserveCurrent.MoveTo(iRcRowId);

                                oReserveCurrent.sUpdateType = "Reserves";
                                oReserveCurrent.iUserId = m_userLogin.UserId;
                                oReserveCurrent.iGroupId = m_userLogin.GroupId;
                                oReserveCurrent.EnteredByUser = oFactory.Context.RMUser.LoginName;
                                oReserveCurrent.ResStatusCode = oFactory.Context.InternalSettings.CacheFunctions.GetCodeIDWithShort("O", "RESERVE_TYPE"); ;
                                oReserveCurrent.Reason = string.Empty;

                                //if (p_iCovGroupId>0)
                                //{
                                //    dApplicableLimit = dAggLimitOnGroup;
                                //    if (bDeductiblePerEventFlag && dDedPerEventAmt < dApplicableLimit)
                                //    {
                                //        dApplicableLimit = dDedPerEventAmt;
                                //    }
                                //}

                                if ((dSumReserveAmountOnLossId - dDedReserveOnLossId) >= dTotalAmountToRedistribute)
                                {
                                    if (bDeductiblePerEventFlag && (dDedPerEventAmt < oReserveCurrent.ReserveAmount + dTotalAmountToRedistribute))
                                    {
                                        dTotalAmountToRedistribute = dDedPerEventAmt - oReserveCurrent.ReserveAmount;
                                        oReserveCurrent.ReserveAmount = dDedPerEventAmt;
                                        bRedestributionComplete = false;
                                    }
                                    else
                                    {
                                        //Redistribution is complete.
                                        oReserveCurrent.ReserveAmount = oReserveCurrent.ReserveAmount + dTotalAmountToRedistribute;
                                        bRedestributionComplete = true;
                                    }
                                    
                                }
                                else
                                {
                                    if (bDeductiblePerEventFlag && (dDedPerEventAmt < oReserveCurrent.ReserveAmount + (dSumReserveAmountOnLossId - dDedReserveOnLossId)))
                                    {
                                        dTotalAmountToRedistribute = dDedPerEventAmt - oReserveCurrent.ReserveAmount;
                                        oReserveCurrent.ReserveAmount = dDedPerEventAmt;
                                    }
                                    else
                                    {
                                        oReserveCurrent.ReserveAmount = oReserveCurrent.ReserveAmount + (dSumReserveAmountOnLossId - dDedReserveOnLossId);
                                        dTotalAmountToRedistribute = dTotalAmountToRedistribute - (dSumReserveAmountOnLossId - dDedReserveOnLossId);
                                    }
                                    bRedestributionComplete = false;
                                }

                                oReserveCurrent.Save();

                                if (bRedestributionComplete)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                //TODO: Read exception message from view database
                //Cloud changes by Nikhil for NI
                throw new RMAppException(Globalization.GetString("ReserveFunds.RedistributeDedReserve.GenericError",m_iClientId), ex);
            
            }
            finally
            {
                if (oFactory!=null)
                {
                    oFactory.Dispose();
                }
            }
        }

        private string GetCoverageGroupQuery(int p_iEventId, int p_iCvgGroupCode)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append(" (SELECT CXPD.POLCVG_ROW_ID FROM CLAIM_X_POL_DED CXPD  ");
            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXPD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
            //Start - Changed by Nikhil.Code review changes.
           //sbSQL.Append(" WHERE EVENT_ID = " + p_iEventId);
           // sbSQL.Append(" AND PXIG.COV_GROUP_CODE = " + p_iCvgGroupCode);
          //  sbSQL.Append(" AND CXPD.DED_TYPE_CODE = " + m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE"));
            sbSQL.Append(" WHERE EVENT_ID = ").Append(p_iEventId);
            sbSQL.Append(" AND PXIG.COV_GROUP_CODE = ").Append(p_iCvgGroupCode);
            sbSQL.Append(" AND CXPD.DED_TYPE_CODE = ").Append(m_objDataModelFactory.Context.LocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE"));
            //end - Changed by Nikhil.Code review changes.
            sbSQL.Append(")");

            return sbSQL.ToString();
        }

        private bool GetDeductiblePerEventFlag(int p_iCovGroupId)
        {
            int iPreventEditDedPerEvent = 0;
            if (p_iCovGroupId > 0)
            {
                //- Changed by Nikhil.Code review changes.
              //  iPreventEditDedPerEvent = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt("SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + p_iCovGroupId);
                iPreventEditDedPerEvent = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(String.Format("SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= {0}", p_iCovGroupId));
            }

            return (iPreventEditDedPerEvent == 0 ? false : true);
        }

        private bool GetExcludeExpenseFlag(int p_iCovGroupId)
        {
            int iExcludeExpenseFlag = 0;
            if (p_iCovGroupId > 0)
            {
                // Changed by Nikhil.Code review changes.
                //iExcludeExpenseFlag = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt("SELECT EXCLUDE_EXPENSE_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + p_iCovGroupId);
                iExcludeExpenseFlag = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(String.Format("SELECT EXCLUDE_EXPENSE_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= {0}", p_iCovGroupId));
            }

            return (iExcludeExpenseFlag == 0 ? false : true);
        }

        //rkotak:FDH 4608,9681 start

        public enum FINANCIAL_LEVEL
        {
            Claim = 1,
            Claimant = 2,
            Reserve = 3,
            Policy = 4,
            Unit = 5,
            Coverages = 6

        }

        private StringBuilder GetQueryForFDH(int iMultiCovgPerClm, int p_iSummaryLevel, string sBaseLangCode, int p_iClaimId, int p_iClaimantId, string sDBType, int iLangCode, int p_iRCRowId, int p_iReserveTypeCode, int p_iCoverageLossID, int p_iPolicyId, int p_iUnitId, int p_iCoverageId)
        {
            StringBuilder sbSQL = new StringBuilder();
            string sNo = CommonFunctions.FilterBusinessMessage(Globalization.GetString("No", m_iClientId, Convert.ToString(iLangCode)));
            LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId);
            if (iMultiCovgPerClm == 0)
            {

                #region AC OFF Query
                if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Claim)  //For Claim Level and Claimant Level Both - In Claimant Only Claimant Id will be there Thats why didnt right 2 Different Queries.
                {
                    #region Reserve History Query
                    sbSQL.Append("SELECT ");
                    sbSQL.Append(" R.DTTM_RCD_ADDED TRANSDATE,");
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                        sbSQL.Append(" 1 AS FLAG,");
                        sbSQL.Append("(A.SHORT_CODE + ' ' + A.CODE_DESC) FinancialTYPE,");

                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                        sbSQL.Append(" 1 AS FLAG,");
                        sbSQL.Append("(A.SHORT_CODE || ' ' || A.CODE_DESC) FinancialTYPE,");
                    }

                    sbSQL.Append("R.CLAIMANT_EID CLAIMANT,");
                    sbSQL.Append("0 PAYEE,");
                    sbSQL.Append("R.RESERVE_AMOUNT AMOUNT,");
                    sbSQL.Append("R.CLAIM_CURRENCY_RESERVE_AMOUNT AMTCLAIMCURRENCY,");
                    sbSQL.Append("0 PAYMENTAMOUNT,");
                    sbSQL.Append("0 PAYMENTAMTPAYMENTCURRENCY,");
                    sbSQL.Append("0 PAYMENTAMTCLAIMCURRENCY,");
                    sbSQL.Append("R.CHANGE_AMOUNT CHANGEAMOUNT,");
                    sbSQL.Append("R.CHANGE_AMOUNT * R.BASE_TO_CLAIM_CUR_RATE CHANGEAMTINCLAIMCURRENCY,");
                    sbSQL.Append("'' CHECKCONTROLNUMBER,");
                    sbSQL.Append("0 CHECK#,");
                    sbSQL.Append("'' CHECKDATE,");
                    sbSQL.Append("'' CHECKCLEARDATE,");
                    sbSQL.Append("R.UPDATED_BY_USER LOGINBY,");
                    //sbSQL.Append(sEFFECTIVE_DATE + " CURRENCYCONVDATE,");     RMA-8507    achouhan3   Commented to remove ConvCurrency Date
                    sbSQL.Append("R.BASE_TO_CLAIM_CUR_RATE CURRENCYCONVRATE,");
                    sbSQL.Append("R.CLAIM_CURR_CODE CLAIMCURRENCY,");
                    sbSQL.Append("0 PMTCURRENCYCODE ");
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL.Append(", ISNULL(ENTITY.LAST_NAME,' ') + ',' + ISNULL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                        sbSQL.Append(",' '  PAY_TO_THE_ORDER_OF ");
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL.Append(", NVL(ENTITY.LAST_NAME,' ') || ',' || NVL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                        sbSQL.Append(",TO_CLOB(' ') PAY_TO_THE_ORDER_OF ");
                    }
                    sbSQL.Append(" FROM RESERVE_HISTORY R");
                    sbSQL.Append(" LEFT JOIN ENTITY ENTITY ON R.CLAIMANT_EID = ENTITY.ENTITY_ID ");
                    sbSQL.Append(" INNER JOIN CODES_TEXT A On A.CODE_ID = R.RESERVE_TYPE_CODE AND A.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" INNER JOIN CODES_TEXT B On B.CODE_ID = R.RES_STATUS_CODE ");
                    sbSQL.Append(" WHERE R.CLAIM_ID = " + p_iClaimId);
                    if (p_iClaimantId > 0)
                    {
                        sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);
                    }
                    #endregion
                    sbSQL.Append(" UNION All ");

                    #region Reserve hold query
                    //pgupta93 FDH RMA-10972 START
                    //sbSQL.Append("SELECT R.DTTM_RCD_ADDED TRANSDATE,");
                    sbSQL.Append("SELECT RAPPROVAL.DTTM_RCD_ADDED TRANSDATE,");
                    //pgupta93 FDH RMA-10972 END
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                        sbSQL.Append(" 1 AS FLAG,");
                        sbSQL.Append("(A.SHORT_CODE + ' ' + A.CODE_DESC) FinancialTYPE,");

                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                        sbSQL.Append(" 1 AS FLAG,");
                        sbSQL.Append("(A.SHORT_CODE || ' ' || A.CODE_DESC) FinancialTYPE,");
                    }
                    sbSQL.Append("R.CLAIMANT_EID CLAIMANT,0 PAYEE,R.RESERVE_AMOUNT AMOUNT,R.CLAIM_CURRENCY_RESERVE_AMOUNT AMTCLAIMCURRENCY,0 PAYMENTAMOUNT,0 PAYMENTAMTPAYMENTCURRENCY,0 PAYMENTAMTCLAIMCURRENCY,(RAPPROVAL.NEW_RESERVE_AMOUNT - RAPPROVAL.ORIGINAL_RESERVE_AMOUNT) CHANGEAMOUNT,");
                    sbSQL.Append("((RAPPROVAL.NEW_RESERVE_AMOUNT - RAPPROVAL.ORIGINAL_RESERVE_AMOUNT) * R.BASE_TO_CLAIM_CUR_RATE) CHANGEAMTINCLAIMCURRENCY,");
                    sbSQL.Append("'' CHECKCONTROLNUMBER,0 CHECK#,'' CHECKDATE,");
                    sbSQL.Append("'' CHECKCLEARDATE,R.UPDATED_BY_USER LOGINBY,");
                    //RMA-8507  achouhan3       Commented to remove ConvCurrency Date
                    //sbSQL.Append(sEFFECTIVE_DATE + " CURRENCYCONVDATE, R.BASE_TO_CLAIM_CUR_RATE CURRENCYCONVRATE,R.CLAIM_CURR_CODE CLAIMCURRENCY,");
                    sbSQL.Append("R.BASE_TO_CLAIM_CUR_RATE CURRENCYCONVRATE,R.CLAIM_CURR_CODE CLAIMCURRENCY,");
                    sbSQL.Append("0 PMTCURRENCYCODE");
                    //sbSQL.Append(" FROM RESERVE_CURRENT R INNER JOIN RSV_SUP_APPROVAL_HIST RAPPROVAL ON R.RC_ROW_ID = RAPPROVAL.RC_ROW_ID");//RMA 8435
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL.Append(", ISNULL(ENTITY.LAST_NAME,' ') + ',' + ISNULL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                        sbSQL.Append(",' '  PAY_TO_THE_ORDER_OF ");
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL.Append(", NVL(ENTITY.LAST_NAME,' ') || ',' || NVL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                        sbSQL.Append(",TO_CLOB(' ') PAY_TO_THE_ORDER_OF ");
                    }
                    sbSQL.Append(" FROM RESERVE_CURRENT R INNER JOIN RSV_SUP_APPROVAL_HIST RAPPROVAL ON R.RC_ROW_ID = RAPPROVAL.RC_ROW_ID  AND RAPPROVAL.RSV_HIST_ROW_ID = (SELECT MAX(RSV_HIST_ROW_ID) FROM RSV_SUP_APPROVAL_HIST WHERE RC_ROW_ID= R.RC_ROW_ID)"); //RKOTAK :RMA-8435
                    sbSQL.Append(" LEFT JOIN ENTITY ENTITY ON R.CLAIMANT_EID = ENTITY.ENTITY_ID ");
                    sbSQL.Append(" INNER JOIN CODES_TEXT A On A.CODE_ID = R.RESERVE_TYPE_CODE AND A.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" INNER JOIN CODES_TEXT B On B.CODE_ID = R.RES_STATUS_CODE ");
                    sbSQL.Append(" INNER JOIN CODES RES_STATUS_CODE On B.CODE_ID = RES_STATUS_CODE.CODE_ID ");
                    sbSQL.Append(" WHERE R.CLAIM_ID = " + p_iClaimId);
                    sbSQL.Append(" AND RES_STATUS_CODE.RELATED_CODE_ID=" + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"));
                    if (p_iClaimantId > 0)
                    {
                        sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);
                    }
                    #endregion

                    sbSQL.Append(" UNION All ");
                    #region Payment History Query
                    sbSQL.Append(" SELECT FUNDS.DTTM_RCD_ADDED TRANSDATE,");
                    if (sDBType == Constants.DB_SQLSRVR)
                    {

                        sbSQL.Append("FUNDS.STATUS_CODE AS RSTATUS,");
                        sbSQL.Append(" FUNDS.PAYMENT_FLAG AS FLAG,");
                        sbSQL.Append("(A.SHORT_CODE + ' ' + A.CODE_DESC) FinancialTYPE,");

                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL.Append("FUNDS.STATUS_CODE AS RSTATUS,");
                        sbSQL.Append(" FUNDS.PAYMENT_FLAG AS FLAG,");
                        sbSQL.Append("(A.SHORT_CODE || ' ' || A.CODE_DESC) FinancialTYPE,");
                    }
                    sbSQL.Append("FUNDS.CLAIMANT_EID CLAIMANT,FUNDS.TRANS_ID PAYEE ,FUNDS.AMOUNT AMOUNT,FUNDS.CLAIM_CURRENCY_AMOUNT AMTCLAIMCURRENCY,FUNDS_TRANS_SPLIT.AMOUNT PAYMENTAMOUNT,FUNDS_TRANS_SPLIT.PMT_CURRENCY_AMOUNT PAYMENTAMTPAYMENTCURRENCY,FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_AMOUNT PAYMENTAMTCLAIMCURRENCY,0 CHANGEAMOUNT,");
                    sbSQL.Append("0 CHANGEAMTINCLAIMCURRENCY,FUNDS.CTL_NUMBER CHECKCONTROLNUMBER,FUNDS.TRANS_NUMBER CHECK#,");
                    sbSQL.Append("FUNDS.DATE_OF_CHECK CHECKDATE,");
                    sbSQL.Append("CASE  WHEN FUNDS.CLEARED_FLAG = 0 THEN ");
                    sbSQL.Append("'");
                    sbSQL.Append(sNo);
                    sbSQL.Append("'");
                    sbSQL.Append(" ELSE FUNDS.VOID_DATE END as CHECKCLEARDATE,");
                    sbSQL.Append("FUNDS.UPDATED_BY_USER LOGINBY,");
                    //RMA-8057  achouhan3       Commented to remove Currency Conv Date
                    //sbSQL.Append(sEFFECTIVE_DATE + " CURRENCYCONVDATE, FUNDS.BASE_TO_PMT_CUR_RATE CURRENCYCONVRATE,FUNDS.PMT_CURRENCY_CODE CLAIMCURRENCY,");
                    sbSQL.Append("FUNDS.BASE_TO_PMT_CUR_RATE CURRENCYCONVRATE,FUNDS.CLAIM_CURRENCY_CODE CLAIMCURRENCY,"); //rkotak 8554
                    sbSQL.Append("FUNDS.PMT_CURRENCY_CODE PMTCURRENCYCODE");
                    if (sDBType == Constants.DB_SQLSRVR)
                        sbSQL.Append(", ISNULL(ENTITY.LAST_NAME,' ') + ',' + ISNULL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                    else if (sDBType == Constants.DB_ORACLE)
                        sbSQL.Append(", NVL(ENTITY.LAST_NAME,' ') || ',' || NVL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                    sbSQL.Append(",FUNDS.PAY_TO_THE_ORDER_OF PAY_TO_THE_ORDER_OF ");
                    sbSQL.Append(" FROM FUNDS_TRANS_SPLIT");
                    sbSQL.Append(" INNER JOIN CODES_TEXT A On A.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE AND A.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" INNER JOIN FUNDS On FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID  INNER JOIN CODES_TEXT B On b.CODE_ID = FUNDS.STATUS_CODE ");
                    sbSQL.Append(" LEFT JOIN ENTITY ENTITY ON FUNDS.CLAIMANT_EID = ENTITY.ENTITY_ID ");
                    //sbSQL.Append(" INNER JOIN RESERVE_CURRENT On FUNDS_TRANS_SPLIT.RC_ROW_ID= RESERVE_CURRENT.RC_ROW_ID ");
                    sbSQL.Append(" WHERE FUNDS.CLAIM_ID = " + p_iClaimId);
                    if (p_iClaimantId > 0)
                    {
                        sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantId);
                    }
                    //sbSQL.Append(" ORDER BY TRANSDATE");
                    #endregion
                }
            }
                #endregion
            else
            {
                #region AC ON Query
                #region Reserve history
                sbSQL.Append("SELECT R.DTTM_RCD_ADDED TRANSDATE,");
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                    sbSQL.Append(" 1 AS FLAG,");

                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                    sbSQL.Append(" 1 AS FLAG,");
                }

                sbSQL.Append("POLICY.POLICY_NAME POLICYNAME,");
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append("CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT ISNULL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT,");

                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append("CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT NVL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT CONCAT(POINT_UNIT_DATA.STAT_UNIT_NUMBER,CONCAT(' - ',nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM( CONCAT(nvl(ENTITY.FIRST_NAME,'') ,CONCAT( ' ' ,CONCAT( nvl(ENTITY.MIDDLE_NAME,'') ,CONCAT(' ', nvl(ENTITY.LAST_NAME,'')))))))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT,");
                }

                sbSQL.Append("POLICY_X_CVG_TYPE.COVERAGE_TEXT COVERAGETYPE,");
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append("(LOSSCODE.SHORT_CODE + ' ' + LOSSCODE.CODE_DESC) CAUSETYPEOFLOSS,");
                    sbSQL.Append("(A.SHORT_CODE + ' ' + A.CODE_DESC) FinancialTYPE,");
                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append("(LOSSCODE.SHORT_CODE || ' ' || LOSSCODE.CODE_DESC) CAUSETYPEOFLOSS,");
                    sbSQL.Append("(A.SHORT_CODE || ' ' || A.CODE_DESC) FinancialTYPE,");
                }
                sbSQL.Append("R.CLAIMANT_EID CLAIMANT,0 PAYEE,R.RESERVE_AMOUNT AMOUNT,R.CLAIM_CURRENCY_RESERVE_AMOUNT AMTCLAIMCURRENCY,0 PAYMENTAMOUNT,0 PAYMENTAMTPAYMENTCURRENCY,0 PAYMENTAMTCLAIMCURRENCY,R.CHANGE_AMOUNT CHANGEAMOUNT,");
                sbSQL.Append("R.CHANGE_AMOUNT * R.BASE_TO_CLAIM_CUR_RATE CHANGEAMTINCLAIMCURRENCY,");
                sbSQL.Append("'' CHECKCONTROLNUMBER,0 CHECK#,'' CHECKDATE,'' CHECKCLEARDATE,R.UPDATED_BY_USER LOGINBY,");
                //RMA-8057  achouhan3       Commented to remove Conv currency Date Column
                //sbSQL.Append(sEFFECTIVE_DATE + " CURRENCYCONVDATE, R.BASE_TO_CLAIM_CUR_RATE CURRENCYCONVRATE,R.CLAIM_CURR_CODE CLAIMCURRENCY,");
                sbSQL.Append("R.BASE_TO_CLAIM_CUR_RATE CURRENCYCONVRATE,R.CLAIM_CURR_CODE CLAIMCURRENCY,");
                sbSQL.Append("0 PMTCURRENCYCODE,");
                if (sDBType == Constants.DB_SQLSRVR)
                    sbSQL.Append("COV_DESC_CODE.SHORT_CODE + ' ' + COV_DESC_CODE.CODE_DESC COVERAGE_TYPE_CODE_TEXT ");//RKOTAK 8450
                else if (sDBType == Constants.DB_ORACLE)
                    sbSQL.Append("COV_DESC_CODE.SHORT_CODE || ' ' || COV_DESC_CODE.CODE_DESC COVERAGE_TYPE_CODE_TEXT ");//RKOTAK 8450
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append(", ISNULL(ENTITY.LAST_NAME,' ') + ',' + ISNULL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                    sbSQL.Append(",' '  PAY_TO_THE_ORDER_OF ");
                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append(", NVL(ENTITY.LAST_NAME,' ') || ',' || NVL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                    sbSQL.Append(",TO_CLOB(' ') PAY_TO_THE_ORDER_OF ");
                }
                sbSQL.Append(" FROM RESERVE_HISTORY R");
                sbSQL.Append(" INNER JOIN ENTITY ENTITY ON R.CLAIMANT_EID = ENTITY.ENTITY_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT A On A.CODE_ID = R.RESERVE_TYPE_CODE AND A.LANGUAGE_CODE = ");
                sbSQL.Append(sBaseLangCode);
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UA On UA.CODE_ID = R.RESERVE_TYPE_CODE AND UA.LANGUAGE_CODE = ");
                sbSQL.Append(iLangCode);
                sbSQL.Append(" INNER JOIN CODES_TEXT B On B.CODE_ID = R.RES_STATUS_CODE ");
                sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS On COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = R.POLCVG_LOSS_ROW_ID ");
                sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE On POLICY_X_CVG_TYPE.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID ");
                sbSQL.Append(" INNER JOIN POLICY_X_UNIT ON POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                sbSQL.Append(" INNER JOIN POLICY On POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT LOSSCODE On LOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE ");
                sbSQL.Append(" INNER JOIN CODES_TEXT COV_DESC_CODE On POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE=COV_DESC_CODE.CODE_ID AND COV_DESC_CODE.LANGUAGE_CODE= " + sBaseLangCode); //RKOTAK 8450
                sbSQL.Append(" WHERE R.CLAIM_ID = " + p_iClaimId);
                if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Claimant)//claimant level
                {
                    sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Reserve)//reserve level
                {
                    //sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    sbSQL.Append(" AND R.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID);//multile jira - rkotak,8608,8614

                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Policy)//policy level
                {
                    //sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId); //rma-8546
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    //sbSQL.Append(" AND R.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);//rma - 8454,rkotak
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID);//rma - 8454
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Unit) //unit level
                {
                    //sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = " + p_iUnitId);
                    //sbSQL.Append(" AND R.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);////rma - 8454,rkotak
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID); //rma - 8454

                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Coverages)//coverage level
                {
                    ////sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = " + p_iUnitId);
                    sbSQL.Append(" AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + p_iCoverageId);
                    //sbSQL.Append(" AND R.RESERVE_TYPE_CODE = " + p_iReserveTypeCode); //rma - 8454,rkotak
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID); //rma - 8454

                }
                #endregion
                sbSQL.Append(" UNION All ");
                #region reserve hold query
                //pgupta93 FDH RMA-10972 START
                //sbSQL.Append("SELECT R.DTTM_RCD_ADDED TRANSDATE,");
                sbSQL.Append("SELECT RAPPROVAL.DTTM_RCD_ADDED TRANSDATE,");
                //pgupta93 FDH RMA-10972 END
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                    sbSQL.Append(" 1 AS FLAG,");
                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append("R.RES_STATUS_CODE AS RSTATUS,");
                    sbSQL.Append(" 1 AS FLAG,");
                }

                sbSQL.Append("POLICY.POLICY_NAME POLICYNAME,");
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append(" CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT ISNULL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT,");

                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append(" CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT NVL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT CONCAT(POINT_UNIT_DATA.STAT_UNIT_NUMBER,CONCAT(' - ',nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM( CONCAT(nvl(ENTITY.FIRST_NAME,'') ,CONCAT( ' ' ,CONCAT( nvl(ENTITY.MIDDLE_NAME,'') ,CONCAT(' ', nvl(ENTITY.LAST_NAME,'')))))))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT,");
                }
                sbSQL.Append("POLICY_X_CVG_TYPE.COVERAGE_TEXT COVERAGETYPE,");
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append("(LOSSCODE.SHORT_CODE + ' ' + LOSSCODE.CODE_DESC) CAUSETYPEOFLOSS,");
                    sbSQL.Append("(A.SHORT_CODE + ' ' + A.CODE_DESC) FinancialTYPE,");
                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append("(LOSSCODE.SHORT_CODE || ' ' || LOSSCODE.CODE_DESC) CAUSETYPEOFLOSS,");
                    sbSQL.Append("(A.SHORT_CODE || ' ' || A.CODE_DESC) FinancialTYPE,");
                }

                sbSQL.Append("R.CLAIMANT_EID CLAIMANT,0 PAYEE,R.RESERVE_AMOUNT AMOUNT,R.CLAIM_CURRENCY_RESERVE_AMOUNT AMTCLAIMCURRENCY,0 PAYMENTAMOUNT,0 PAYMENTAMTPAYMENTCURRENCY,0 PAYMENTAMTCLAIMCURRENCY,(RAPPROVAL.NEW_RESERVE_AMOUNT - RAPPROVAL.ORIGINAL_RESERVE_AMOUNT) CHANGEAMOUNT,");
                sbSQL.Append("((RAPPROVAL.NEW_RESERVE_AMOUNT - RAPPROVAL.ORIGINAL_RESERVE_AMOUNT) * R.BASE_TO_CLAIM_CUR_RATE) CHANGEAMTINCLAIMCURRENCY,");
                sbSQL.Append("'' CHECKCONTROLNUMBER,0 CHECK#,'' CHECKDATE,'' CHECKCLEARDATE,R.UPDATED_BY_USER LOGINBY,");
                ////RMA-8057  achouhan3       Commented to remove Conv currency Date Column
                //sbSQL.Append(sEFFECTIVE_DATE + " CURRENCYCONVDATE, R.BASE_TO_CLAIM_CUR_RATE CURRENCYCONVRATE,R.CLAIM_CURR_CODE CLAIMCURRENCY,");
                sbSQL.Append("R.BASE_TO_CLAIM_CUR_RATE CURRENCYCONVRATE,R.CLAIM_CURR_CODE CLAIMCURRENCY,");
                sbSQL.Append("0 PMTCURRENCYCODE,");
                if (sDBType == Constants.DB_SQLSRVR)
                    sbSQL.Append("COV_DESC_CODE.SHORT_CODE + ' ' + COV_DESC_CODE.CODE_DESC COVERAGE_TYPE_CODE_TEXT ");//RKOTAK 8450
                else if (sDBType == Constants.DB_ORACLE)
                    sbSQL.Append("COV_DESC_CODE.SHORT_CODE || ' ' || COV_DESC_CODE.CODE_DESC COVERAGE_TYPE_CODE_TEXT ");//RKOTAK 8450    
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append(", ISNULL(ENTITY.LAST_NAME,' ') + ',' + ISNULL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                    sbSQL.Append(",' '  PAY_TO_THE_ORDER_OF ");
                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append(", NVL(ENTITY.LAST_NAME,' ') || ',' || NVL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                    sbSQL.Append(", TO_CLOB('') PAY_TO_THE_ORDER_OF ");
                }
                sbSQL.Append(" FROM RESERVE_CURRENT R INNER JOIN RSV_SUP_APPROVAL_HIST RAPPROVAL ON R.RC_ROW_ID = RAPPROVAL.RC_ROW_ID  AND RAPPROVAL.RSV_HIST_ROW_ID = (SELECT MAX(RSV_HIST_ROW_ID) FROM RSV_SUP_APPROVAL_HIST WHERE RC_ROW_ID= R.RC_ROW_ID)"); //RKOTAK :RMA-8435
                sbSQL.Append(" INNER JOIN ENTITY ENTITY ON R.CLAIMANT_EID = ENTITY.ENTITY_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT A On A.CODE_ID = R.RESERVE_TYPE_CODE AND A.LANGUAGE_CODE = ");
                sbSQL.Append(sBaseLangCode);
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UA On UA.CODE_ID = R.RESERVE_TYPE_CODE AND UA.LANGUAGE_CODE = ");
                sbSQL.Append(iLangCode);
                sbSQL.Append(" INNER JOIN CODES_TEXT B On B.CODE_ID = R.RES_STATUS_CODE ");
                sbSQL.Append(" INNER JOIN COVERAGE_X_LOSS On COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = R.POLCVG_LOSS_ROW_ID ");
                sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE On POLICY_X_CVG_TYPE.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID ");
                sbSQL.Append(" INNER JOIN POLICY_X_UNIT ON POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                sbSQL.Append(" INNER JOIN POLICY On POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT LOSSCODE On LOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE ");
                sbSQL.Append(" INNER JOIN CODES RES_STATUS_CODE On B.CODE_ID = RES_STATUS_CODE.CODE_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT COV_DESC_CODE On POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE=COV_DESC_CODE.CODE_ID AND COV_DESC_CODE.LANGUAGE_CODE= " + sBaseLangCode); //RKOTAK 8450
                sbSQL.Append(" AND RES_STATUS_CODE.RELATED_CODE_ID=" + objCache.GetCodeId("H", "RESERVE_STATUS_PARENT"));
                sbSQL.Append(" WHERE R.CLAIM_ID = " + p_iClaimId);
                //sbSQL.Append(" AND R.RES_STATUS_CODE=" + objCache.GetCodeId("H", "RESERVE_STATUS")); 
                //rma - 8607 starts
                /*if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Claim || p_iSummaryLevel == (int)FINANCIAL_LEVEL.Claimant)//claimant level
                {
                    if (p_iRCRowId > 0)
                    {
                        sbSQL.Append(" AND R.RC_ROW_ID = " + p_iRCRowId);
                    }
                }
                 */
                //rma 8607 ends
                if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Claimant)//claimant level
                {
                    sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Reserve)//reserve level
                {
                    //sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    //sbSQL.Append(" AND R.RC_ROW_ID = " + p_iRCRowId);//rkotak : multiple jira, 8608,8614                    
                    sbSQL.Append(" AND R.RESERVE_TYPE_CODE  = " + p_iReserveTypeCode);
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID); rkotak:multiple jira
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Policy)//policy
                {
                    //sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    //sbSQL.Append(" AND R.RC_ROW_ID = " + p_iRCRowId);//rkotak : multiple jira,8608,8614
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    //sbSQL.Append(" AND R.RESERVE_TYPE_CODE  = " + p_iReserveTypeCode); //rma - 8454
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID);//rma - 8454
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Unit)//unit
                {
                    //sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    //sbSQL.Append(" AND R.RC_ROW_ID = " + p_iRCRowId); //rkotak : multiple jira,8608,8614
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = " + p_iUnitId);
                    //sbSQL.Append(" AND R.RESERVE_TYPE_CODE  = " + p_iReserveTypeCode); //rma - 8454
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID); //rma - 8454
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Coverages)//coverage level
                {
                    //sbSQL.Append(" AND R.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    //sbSQL.Append(" AND R.RC_ROW_ID = " + p_iRCRowId);//rkotak 8608,8614
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = " + p_iUnitId);
                    //sbSQL.Append(" AND R.RESERVE_TYPE_CODE  = " + p_iReserveTypeCode); //rma - 8454
                    //sbSQL.Append(" AND R.POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID); //multile jira ,rkotak 8608,8614: rkotak
                    //sbSQL.Append(" AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + p_iCoverageId);//rma - 8454
                    sbSQL.Append(" AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + p_iCoverageId);//8608,8614
                }



                #endregion

                sbSQL.Append(" UNION All ");
                #region Payment history query
                sbSQL.Append(" SELECT FUNDS.DTTM_RCD_ADDED TRANSDATE,");
                if (sDBType == Constants.DB_SQLSRVR)
                {

                    sbSQL.Append("FUNDS.STATUS_CODE AS RSTATUS,");
                    sbSQL.Append(" FUNDS.PAYMENT_FLAG AS FLAG,");
                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append("FUNDS.STATUS_CODE AS RSTATUS,");
                    sbSQL.Append(" FUNDS.PAYMENT_FLAG AS FLAG,");
                }


                sbSQL.Append("POLICY.POLICY_NAME POLICYNAME ");
                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append(" , CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT ISNULL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT,");

                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append(", CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT NVL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT CONCAT(POINT_UNIT_DATA.STAT_UNIT_NUMBER,CONCAT(' - ',nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM( CONCAT(nvl(ENTITY.FIRST_NAME,'') ,CONCAT( ' ' ,CONCAT( nvl(ENTITY.MIDDLE_NAME,'') ,CONCAT(' ', nvl(ENTITY.LAST_NAME,'')))))))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT,");
                }
                sbSQL.Append("POLICY_X_CVG_TYPE.COVERAGE_TEXT COVERAGETYPE,");

                if (sDBType == Constants.DB_SQLSRVR)
                {
                    sbSQL.Append("(LOSSCODE.SHORT_CODE + ' ' + LOSSCODE.CODE_DESC) CAUSETYPEOFLOSS,");
                    sbSQL.Append("(A.SHORT_CODE + ' ' + A.CODE_DESC) FinancialTYPE,");
                }
                else if (sDBType == Constants.DB_ORACLE)
                {
                    sbSQL.Append("(LOSSCODE.SHORT_CODE || ' ' || LOSSCODE.CODE_DESC) CAUSETYPEOFLOSS,");
                    sbSQL.Append("(A.SHORT_CODE || ' ' || A.CODE_DESC) FinancialTYPE,");
                }

                sbSQL.Append("RESERVE_CURRENT.CLAIMANT_EID CLAIMANT,FUNDS.TRANS_ID PAYEE,FUNDS.AMOUNT AMOUNT,FUNDS.CLAIM_CURRENCY_AMOUNT AMTCLAIMCURRENCY,FUNDS_TRANS_SPLIT.AMOUNT PAYMENTAMOUNT,FUNDS_TRANS_SPLIT.PMT_CURRENCY_AMOUNT PAYMENTAMTPAYMENTCURRENCY,FUNDS_TRANS_SPLIT.CLAIM_CURRENCY_AMOUNT PAYMENTAMTCLAIMCURRENCY,0 CHANGEAMOUNT,");
                sbSQL.Append("0 CHANGEAMTINCLAIMCURRENCY,FUNDS.CTL_NUMBER CHECKCONTROLNUMBER,FUNDS.TRANS_NUMBER CHECK#,");
                sbSQL.Append("FUNDS.DATE_OF_CHECK CHECKDATE,");
                sbSQL.Append("CASE  WHEN FUNDS.CLEARED_FLAG = 0 THEN ");
                sbSQL.Append("'");
                sbSQL.Append(sNo);
                sbSQL.Append("'");
                sbSQL.Append(" ELSE FUNDS.VOID_DATE END as CHECKCLEARDATE,");
                sbSQL.Append("FUNDS.UPDATED_BY_USER LOGINBY,");
                //RMA-8057      achouhan3       commented to remove Conv Currency Date column
                //sbSQL.Append(sEFFECTIVE_DATE + " CURRENCYCONVDATE, FUNDS.BASE_TO_PMT_CUR_RATE CURRENCYCONVRATE,FUNDS.CLAIM_CURRENCY_CODE CLAIMCURRENCY,");
                sbSQL.Append("FUNDS.BASE_TO_PMT_CUR_RATE CURRENCYCONVRATE,FUNDS.CLAIM_CURRENCY_CODE CLAIMCURRENCY,");
                sbSQL.Append("FUNDS.PMT_CURRENCY_CODE PMTCURRENCYCODE,");
                if (sDBType == Constants.DB_SQLSRVR)
                    sbSQL.Append("COV_DESC_CODE.SHORT_CODE + ' ' + COV_DESC_CODE.CODE_DESC COVERAGE_TYPE_CODE_TEXT ");//RKOTAK 8450
                else if (sDBType == Constants.DB_ORACLE)
                    sbSQL.Append("COV_DESC_CODE.SHORT_CODE || ' ' || COV_DESC_CODE.CODE_DESC COVERAGE_TYPE_CODE_TEXT ");//RKOTAK 8450
                //sbSQL.Append
                if (sDBType == Constants.DB_SQLSRVR)
                    sbSQL.Append(", ISNULL(ENTITY.LAST_NAME,' ') + ',' + ISNULL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                else if (sDBType == Constants.DB_ORACLE)
                    sbSQL.Append(", NVL(ENTITY.LAST_NAME,' ') || ',' || NVL(ENTITY.FIRST_NAME,'') CLAIMANT_NAME");
                sbSQL.Append(",FUNDS.PAY_TO_THE_ORDER_OF PAY_TO_THE_ORDER_OF ");
                sbSQL.Append(" FROM FUNDS_TRANS_SPLIT");
                sbSQL.Append(" INNER JOIN CODES_TEXT A On A.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE AND A.LANGUAGE_CODE = ");
                sbSQL.Append(sBaseLangCode);
                sbSQL.Append(" INNER JOIN FUNDS On FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID  INNER JOIN CODES_TEXT B On b.CODE_ID = FUNDS.STATUS_CODE ");
                sbSQL.Append(" INNER JOIN RESERVE_CURRENT On FUNDS_TRANS_SPLIT.RC_ROW_ID= RESERVE_CURRENT.RC_ROW_ID  INNER JOIN COVERAGE_X_LOSS On COVERAGE_X_LOSS.CVG_LOSS_ROW_ID = RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                sbSQL.Append(" INNER JOIN ENTITY ENTITY ON RESERVE_CURRENT.CLAIMANT_EID = ENTITY.ENTITY_ID ");
                sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE On POLICY_X_CVG_TYPE.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT POL On POL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE AND POL.LANGUAGE_CODE = ");
                sbSQL.Append(sBaseLangCode);
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UPOL On UPOL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE AND UPOL.LANGUAGE_CODE = ");
                sbSQL.Append(iLangCode);
                sbSQL.Append(" INNER JOIN POLICY_X_UNIT ON POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                sbSQL.Append(" INNER JOIN POLICY On POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID ");
                sbSQL.Append(" INNER JOIN CODES_TEXT LOSSCODE On LOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE AND LOSSCODE.LANGUAGE_CODE = ");
                sbSQL.Append(sBaseLangCode);
                sbSQL.Append(" INNER JOIN CODES_TEXT COV_DESC_CODE On POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE=COV_DESC_CODE.CODE_ID AND COV_DESC_CODE.LANGUAGE_CODE= " + sBaseLangCode); //RKOTAK 8450

                sbSQL.Append(" WHERE FUNDS.CLAIM_ID = " + p_iClaimId);
                if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Claimant)//claimant
                {
                    sbSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID = " + p_iClaimantId);
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Reserve) //reserve level//i think we need to add rc_row_id here..come again
                {
                    //sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    sbSQL.Append(" AND RESERVE_CURRENT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode);
                    //sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID);//rma - multiple jira, 8608,8614
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Policy) // policy level
                {
                    //sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    //sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode); //rma - 8454
                    //sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID);//rma - 8454
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Unit) //unit level
                {
                    //sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantId);  //rma-8546                  
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = " + p_iUnitId);
                    //sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE  = " + p_iReserveTypeCode);//rma - 8454
                    //sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID);//rma - 8454
                }
                else if (p_iSummaryLevel == (int)FINANCIAL_LEVEL.Coverages) //covearge level...come again for coveragfe_x_loss join
                {
                    //sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantId);//rma-8546
                    sbSQL.Append(" AND POLICY_X_UNIT.POLICY_ID = " + p_iPolicyId);
                    sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = " + p_iUnitId);
                    sbSQL.Append(" AND COVERAGE_X_LOSS.POLCVG_ROW_ID = " + p_iCoverageId);
                    //sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iReserveTypeCode); //rma - 8454
                    //sbSQL.Append(" AND POLCVG_LOSS_ROW_ID = " + p_iCoverageLossID); //rma - 8454
                }
                //sbSQL.Append(" ORDER BY TRANSDATE");
                #endregion
                #endregion
            }

            return sbSQL;
        }


        //with reader and json string in string builder, this shows good result
        public XmlDocument GetFinancialDetailHistory(int p_iClaimId, int p_iClaimantId, int p_iPolicyId, int p_iUnitId, int p_iCoverageId, string sClaimCurrency, int p_iSummaryLevel, UserLogin p_objUserLogin, int p_iCoverageLossID, int p_iReserveTypeCode, int p_iRCRowId, string sReserveStatus, string p_sPageName, string p_sGridId)
        {
            #region var dec//rkotak 4608
            int iUserID = 0; int iDSNId = 0;
            string strNode = String.Empty;
            int i = 0;
            double dAmount = 0;
            double dClaimCurrencyAmount = 0;
            double dPaymentAmount = 0;
            double dPmtAmountPmtCurr = 0;
            double dPmtAmtClaimCurr = 0;
            double dChangeAmount = 0;
            double dChgAmtClaimCurr = 0;
            string sCheckClearDate = string.Empty;
            XmlDocument objXmlDoc = null;
            XmlElement objFinancialXmlElement = null;
            string sDBType = string.Empty;
            string claimcurrency = String.Empty;
            int iBaseCurCode = 0;
            int iClaimCurCode = 0;
            StringBuilder sbSQL = null;
            DbReader objReader = null;
            DbReader objRdr = null;
            LocalCache objCache = null;
            Claim objClaim = null;
            string sinitialvalue = String.Empty;
            string sValue = String.Empty;
            string sCaption = String.Empty;
            SysParms objSysSetting = null;
            string sClaimant = string.Empty;
            //string sClaimantFirstName = string.Empty;
            //string sClaimantLastName = string.Empty;
            int iTransID = 0;
            //string sPayeesName = string.Empty;
            string sPaytoTheorderOf = string.Empty;
            int iPaymentCurrencyCode = 0;
            string sPayment = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Payment", m_iClientId));
            string sCollection = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Collection", m_iClientId));
            string sReserve = CommonFunctions.FilterBusinessMessage(Globalization.GetString("Reserve", m_iClientId));
            string sAnd = CommonFunctions.FilterBusinessMessage(Globalization.GetString("and", m_iClientId));
            string sReserveorpaymentStatus = string.Empty;
            int iStatus = 0;
            string sPaymentType = string.Empty;
            string sStatusType = string.Empty;
            string sFinancialStatus = string.Empty;
            int iPaymentCollectionorreserveFlag = 0;
            //string strJsonResponse = "";
            string strJsonUserPref = "";
            string sUserPrefsXML = string.Empty;
            string sDbDateValue = string.Empty;
            string sUiDate = string.Empty;
            //perf code starts
            StringBuilder strJsonResponse = new StringBuilder();
            //StringBuilder strJsonUserPref = new StringBuilder();
            //perf code ends
            #endregion //rkotak 4608
            try
            {
                #region initial //rkotak 4608
                //rkotak 9681
                if (p_objUserLogin != null)
                {
                    iUserID = p_objUserLogin.UserId;
                    iDSNId = p_objUserLogin.DatabaseId;
                    if (m_iLangCode == 0)
                        m_iLangCode = p_objUserLogin.objUser.NlsCode;
                }
                //rkotak 9681
                objXmlDoc = new XmlDocument();
                objSysSetting = new SysParms(m_sConnectionString, m_iClientId);
                //CreateElement(objXmlDoc.DocumentElement, "CurrencyTypeList", ref objCurrencyListElement));
                objFinancialXmlElement = objXmlDoc.CreateElement("FinancialHistory");
                objXmlDoc.AppendChild(objFinancialXmlElement);
                XmlElement objDataElement = null;
                CreateElement(objXmlDoc.DocumentElement, "Data", ref objDataElement);
                claimcurrency = sClaimCurrency;
                sbSQL = new StringBuilder();
                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                if (m_objDataModelFactory == null)
                    m_objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                //if (p_sCurrCurrencyType != "" || p_sDestcurrencyType != "")
                //{
                //    if (p_sCurrCurrencyType != "")                    
                //        sinitialvalue = p_sCurrCurrencyType.Split('_')[0];                    
                //    else                    
                //        sinitialvalue = p_sCurrCurrencyType;                    

                //    iClaimCurCode = Conversion.ConvertStrToInteger(p_sDestcurrencyType));
                //}
                //else                
                iClaimCurCode = objClaim.CurrencyType;
                iBaseCurCode = CommonFunctions.GetBaseCurrencyCode(m_sConnectionString);
                //Get the value of Claim Number
                if (string.IsNullOrEmpty(m_sClaimNumber.Trim()))
                    m_sClaimNumber = objClaim.ClaimNumber;
                string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                int iLangCode = p_objUserLogin.objUser.NlsCode;
                int iMultiCovgPerClm = m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm;
                #endregion //rkotak 4608

                sbSQL = GetQueryForFDH(iMultiCovgPerClm, p_iSummaryLevel, sBaseLangCode, p_iClaimId, p_iClaimantId, sDBType, iLangCode, p_iRCRowId, p_iReserveTypeCode, p_iCoverageLossID, p_iPolicyId, p_iUnitId, p_iCoverageId);

                System.Collections.Generic.List<Dictionary<string, string>> lstDictData = new List<Dictionary<string, string>>();
                //Dictionary<string, string> dicGridData = null;
                //Dictionary<string, object> dicDateTime = null;
                //Dictionary<string, object> dicCheckDate = null;

                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    #region loop through reader and prepare json string for grid data
                    while (objReader.Read())
                    {
                        i = i + 1;
                        //perf code starts
                        //        i = i + 1;
                        if (i > 1)
                            strJsonResponse.Append(",");
                        if (i == 1)
                        {
                            strJsonResponse.Append("[");
                        }
                        strJsonResponse.Append("{ ");
                        // //perf code ends

                        //dicGridData = new Dictionary<string, string>());

                        //rkotak:starts, Date field custom handling for sorting on raw value
                        string sDbDate = objReader.GetString("TRANSDATE");
                        strJsonResponse.Append(Conversion.GetJsonNode("DateTime", Conversion.GetUIDate(sDbDate, m_iLangCode.ToString(), m_iClientId) + " " + Conversion.GetUITime(sDbDate, m_iLangCode.ToString(), m_iClientId), true));
                        strJsonResponse.Append(Conversion.GetJsonNode("DateTime.DbValue", GridCommonFunctions.GetSortColumnValueForDateField(sDbDate)));//CUSTOM SORTING FEILD

                        //GridCommonFunctions.GetSortColumnForDateField(ref dicGridData, "DateTime", sDbDate));
                        //rkotak:ends, date custom handling

                        sReserveorpaymentStatus = string.Empty;
                        sStatusType = string.Empty;
                        sFinancialStatus = string.Empty;
                        sPaymentType = string.Empty;
                        iStatus = 0;
                        iPaymentCollectionorreserveFlag = 0;
                        bool bCollection = false;

                        #region check if payment row or collection row

                        iPaymentCollectionorreserveFlag = Conversion.ConvertObjToInt(objReader.GetValue("FLAG"), m_iClientId);
                        if (iPaymentCollectionorreserveFlag == 1)
                        {
                            sStatusType = sReserve;
                        }
                        else
                        {
                            if (iPaymentCollectionorreserveFlag == -1)
                            {
                                sStatusType = sPayment;
                            }
                            else
                            {
                                sStatusType = sCollection;
                                bCollection = true;
                            }
                        }
                        #endregion

                        #region check if reserve or payment/collection row

                        iStatus = Conversion.ConvertObjToInt(objReader.GetValue("RSTATUS"), m_iClientId);
                        sReserveorpaymentStatus = objClaim.Context.LocalCache.GetCodeDesc(iStatus).ToString();

                        if (sStatusType == sPayment & iStatus == objClaim.Context.LocalCache.GetCodeId("Q", "CHECK_STATUS"))
                        {
                            sFinancialStatus = sReserveorpaymentStatus;
                        }

                        else
                        {
                            sFinancialStatus = sReserveorpaymentStatus + ' ' + sStatusType;
                        }
                        #endregion


                        strJsonResponse.Append(Conversion.GetJsonNode("Status", sFinancialStatus));
                        if (iMultiCovgPerClm == -1)
                        {
                            strJsonResponse.Append(Conversion.GetJsonNode("PolicyName", Conversion.ConvertObjToStr(objReader.GetValue("POLICYNAME")).Trim()));
                            strJsonResponse.Append(Conversion.GetJsonNode("Unit", Conversion.ConvertObjToStr(objReader.GetValue("UNIT")).Trim()));
                            strJsonResponse.Append(Conversion.GetJsonNode("Coveragetype", string.IsNullOrEmpty(Conversion.ConvertObjToStr(objReader.GetValue("COVERAGETYPE"))) ? Conversion.ConvertObjToStr(objReader.GetValue("COVERAGE_TYPE_CODE_TEXT")).Trim() : Conversion.ConvertObjToStr(objReader.GetValue("COVERAGETYPE")).Trim()));
                            strJsonResponse.Append(Conversion.GetJsonNode("CausetypofLoss", Conversion.ConvertObjToStr(objReader.GetValue("CAUSETYPEOFLOSS")).Trim()));
                        }
                        strJsonResponse.Append(Conversion.GetJsonNode("Financialtype", Conversion.ConvertObjToStr(objReader.GetValue("FinancialTYPE")).Trim()));
                        //strJsonResponse.Append(Conversion.GetJsonNode("Claimant", getEntityName(Conversion.ConvertObjToInt64(objReader.GetValue("CLAIMANT"), m_iClientId))));//For P
                        sClaimant = objReader.GetString("CLAIMANT_NAME");
                        if (sClaimant.EndsWith(",") == true)
                            sClaimant = sClaimant.Substring(0, (sClaimant.Length - 1));
                        strJsonResponse.Append(Conversion.GetJsonNode("Claimant", sClaimant));
                        //strJsonResponse.Append(Conversion.GetJsonNode("Claimant", objReader.GetString("CLAIMANT_NAME")));

                        //sPayeesName = string.Empty;
                        //iTransID = (Conversion.ConvertObjToInt(objReader.GetValue("PAYEE"), m_iClientId));
                        //sPayeesName = GetPayeeName(iTransID, sAnd, sClaimantFirstName, sClaimantLastName);//For P
                        sPaytoTheorderOf = objReader.GetString("PAY_TO_THE_ORDER_OF");
                        strJsonResponse.Append(Conversion.GetJsonNode("Payee", sPaytoTheorderOf));

                        int iClaimCurrencyCode = (Conversion.ConvertObjToInt(objReader.GetValue("CLAIMCURRENCY"), m_iClientId)); //rkotak 8554


                        dAmount = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        if (bCollection)
                            dAmount = -dAmount;
                        //pgupta93 FDH RMA-10920 END
                        strJsonResponse.Append(Conversion.GetJsonNode("AmountUSD", CommonFunctions.ConvertByCurrencyCode(iBaseCurCode, dAmount, m_sConnectionString, m_iClientId)));//R
                        //rkotak:starts, custom sorting to sort on underlying data and not on the data that is displayed to user   
                        strJsonResponse.Append(Conversion.GetJsonNode("AmountUSD.DbValue", dAmount.ToString()));//CUSTOM SORTING FEILD
                        //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "AmountUSD", dAmount.ToString()));
                        //rkotak:ends, custom sorting

                        dClaimCurrencyAmount = Conversion.ConvertObjToDouble(objReader.GetValue("AMTCLAIMCURRENCY"), m_iClientId);
                        //pgupta93 FDH RMA-10920 START
                        //if (sStatusType != sReserve && iPaymentCollectionorreserveFlag != -1)
                        //    dClaimCurrencyAmount = -dClaimCurrencyAmount;
                        if (bCollection)
                            dClaimCurrencyAmount = -dClaimCurrencyAmount;
                        //pgupta93 FDH RMA-10920 END
                        strJsonResponse.Append(Conversion.GetJsonNode("AmountinClaimcurrency", CommonFunctions.ConvertByCurrencyCode(iClaimCurrencyCode, dClaimCurrencyAmount, m_sConnectionString, m_iClientId)));//R
                        strJsonResponse.Append(Conversion.GetJsonNode("AmountinClaimcurrency.DbValue", dClaimCurrencyAmount.ToString()));//CUSTOM SORTING FEILD
                        //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "AmountinClaimcurrency", dClaimCurrencyAmount.ToString());

                        dPaymentAmount = Conversion.ConvertObjToDouble(objReader.GetValue("PAYMENTAMOUNT"), m_iClientId);
                        iPaymentCurrencyCode = (Conversion.ConvertObjToInt(objReader.GetValue("PMTCURRENCYCODE"), m_iClientId));
                        dPmtAmountPmtCurr = Conversion.ConvertObjToDouble(objReader.GetValue("PAYMENTAMTPAYMENTCURRENCY"), m_iClientId);
                        dPmtAmtClaimCurr = Conversion.ConvertObjToDouble(objReader.GetValue("PAYMENTAMTCLAIMCURRENCY"), m_iClientId); //rkotak 8554  
                        dChangeAmount = Conversion.ConvertObjToDouble(objReader.GetValue("CHANGEAMOUNT"), m_iClientId);
                        dChgAmtClaimCurr = Conversion.ConvertObjToDouble(objReader.GetValue("CHANGEAMTINCLAIMCURRENCY"), m_iClientId);

                        if (sStatusType == sReserve)
                        {
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountUSD", String.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountUSD.DbValue", String.Empty));//CUSTOM SORTING FEILD
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "PaymentAmountUSD", string.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMOUNT", CommonFunctions.ConvertByCurrencyCode(iBaseCurCode, dChangeAmount, m_sConnectionString, m_iClientId)));//R
                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMOUNT.DbValue", dChangeAmount.ToString()));//CUSTOM SORTING FEILD
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "CHANGEAMOUNT", dChangeAmount.ToString()));
                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMTINCLAIMCURRENCY", CommonFunctions.ConvertByCurrencyCode(iClaimCurCode, dChgAmtClaimCurr, m_sConnectionString, m_iClientId)));//R
                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMTINCLAIMCURRENCY.DbValue", dChgAmtClaimCurr.ToString()));//CUSTOM SORTING FEILD
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "CHANGEAMTINCLAIMCURRENCY", dChgAmtClaimCurr.ToString()));
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinClaimCurrency", String.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinClaimCurrency.DbValue", String.Empty));//CUSTOM SORTING FEILD
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "PaymentAmountinClaimCurrency", string.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinPaymentCurrency", String.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinPaymentCurrency.DbValue", String.Empty));//CUSTOM SORTING FEILD
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "PaymentAmountinPaymentCurrency", string.Empty));//rma-10997
                            strJsonResponse.Append(Conversion.GetJsonNode("CheckControlNumber", string.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("Check", string.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("CHECKDATE", string.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("CHECKDATE.DbValue", string.Empty));//CUSTOM SORTING FEILD
                            //GridCommonFunctions.GetSortColumnForDateField(ref dicGridData, "CHECKDATE", string.Empty));

                            strJsonResponse.Append(Conversion.GetJsonNode("Checkcleardate", string.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("ClaimCurrency", objClaim.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("CLAIMCURRENCY"), m_iClientId))));
                            strJsonResponse.Append(Conversion.GetJsonNode("TransId", string.Empty));//rma-10914
                        }
                        else
                        {
                            //pgupta93 FDH RMA-10920 START
                            if (bCollection)
                                dPaymentAmount = -dPaymentAmount;
                            //pgupta93 FDH RMA-10920 END                            
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountUSD", CommonFunctions.ConvertByCurrencyCode(iBaseCurCode, dPaymentAmount, m_sConnectionString, m_iClientId)));//R
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountUSD.DbValue", dPaymentAmount.ToString()));
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "PaymentAmountUSD", dPaymentAmount.ToString()));

                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMOUNT", String.Empty));
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "CHANGEAMOUNT", String.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMOUNT.DbValue", String.Empty));

                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMTINCLAIMCURRENCY", String.Empty));
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "CHANGEAMTINCLAIMCURRENCY", String.Empty));
                            strJsonResponse.Append(Conversion.GetJsonNode("CHANGEAMTINCLAIMCURRENCY.DbValue", String.Empty));

                            if (bCollection)
                                dPmtAmtClaimCurr = -dPmtAmtClaimCurr;
                            //pgupta93 FDH RMA-10920 END                            
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinClaimCurrency", CommonFunctions.ConvertByCurrencyCode(iClaimCurrencyCode, dPmtAmtClaimCurr, m_sConnectionString, m_iClientId)));//R
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "PaymentAmountinClaimCurrency", dPmtAmtClaimCurr.ToString()));
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinClaimCurrency.DbValue", dPmtAmtClaimCurr.ToString()));

                            //pgupta93 FDH RMA-10920 START
                            if (bCollection)
                                dPmtAmountPmtCurr = -dPmtAmountPmtCurr;
                            //pgupta93 FDH RMA-10920 END
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinPaymentCurrency", CommonFunctions.ConvertByCurrencyCode(iPaymentCurrencyCode, dPmtAmountPmtCurr, m_sConnectionString, m_iClientId)));//R
                            //GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "PaymentAmountinPaymentCurrency", dPmtAmountPmtCurr.ToString()));//rma-10997
                            strJsonResponse.Append(Conversion.GetJsonNode("PaymentAmountinPaymentCurrency.DbValue", dPmtAmountPmtCurr.ToString()));

                            strJsonResponse.Append(Conversion.GetJsonNode("CheckControlNumber", Conversion.ConvertObjToStr(objReader.GetValue("CHECKCONTROLNUMBER")).Trim()));
                            strJsonResponse.Append(Conversion.GetJsonNode("Check", Conversion.ConvertObjToStr(objReader.GetValue("CHECK#")).Trim()));

                            sDbDateValue = objReader.GetString("CHECKDATE").Trim();
                            strJsonResponse.Append(Conversion.GetJsonNode("CHECKDATE", Conversion.GetUIDate(sDbDateValue, m_iLangCode.ToString(), m_iClientId)));
                            //GridCommonFunctions.GetSortColumnForDateField(ref dicGridData, "CHECKDATE", sDbDateValue));
                            strJsonResponse.Append(Conversion.GetJsonNode("CHECKDATE.DbValue", GridCommonFunctions.GetSortColumnValueForDateField(sDbDateValue)));//custom sorting field
                            //rkotak:ends, custom sorting

                            sCheckClearDate = Conversion.ConvertObjToStr(objReader.GetValue("CHECKCLEARDATE")).Trim();
                            if (sCheckClearDate != "No" && !string.IsNullOrEmpty(sCheckClearDate))
                            {
                                strJsonResponse.Append(Conversion.GetJsonNode("Checkcleardate", Conversion.GetUIDate(objReader.GetString("CHECKCLEARDATE"), m_iLangCode.ToString(), m_iClientId)));
                            }
                            else
                            {
                                strJsonResponse.Append(Conversion.GetJsonNode("Checkcleardate", sCheckClearDate));
                            }
                            if (iPaymentCurrencyCode > 0)
                            {
                                strJsonResponse.Append(Conversion.GetJsonNode("ClaimCurrency", objClaim.Context.LocalCache.GetShortCode(iPaymentCurrencyCode)));
                            }
                            strJsonResponse.Append(Conversion.GetJsonNode("TransId", Conversion.ConvertObjToStr(objReader.GetValue("PAYEE"))));//rma-10914
                        }

                        strJsonResponse.Append(Conversion.GetJsonNode("By", Conversion.ConvertObjToStr(objReader.GetValue("LOGINBY")).Trim()));
                        strJsonResponse.Append(Conversion.GetJsonNode("CurrencyConvRate", Conversion.ConvertObjToStr(objReader.GetValue("CURRENCYCONVRATE"))));
                        //lstDictData.Add(dicGridData));
                        strJsonResponse.Append(" }");
                    }
                    if (i != 0)//if no records found
                        strJsonResponse.Append("]");
                    else //empty aray
                        strJsonResponse.Append("[]");
                    //strJsonResponse.Append(" }");
                    //strJsonResponse = Conversion.CreateJSONStringFromDictionary(lstDictData));
                    // objDataElement.InnerText = strJsonResponse;
                    //objFinancialXmlElement.AppendChild(objDataElement));

                    objDataElement.InnerText = strJsonResponse.ToString();
                    objFinancialXmlElement.AppendChild(objDataElement);
                    #endregion
                }

                #region Prepare Json for UserPref and add to XMl doc
                strJsonUserPref = "";
                strJsonUserPref = GetOrCreateUserPreference(iUserID, p_sPageName, iDSNId, p_sGridId, iMultiCovgPerClm);

                XmlElement objUserPrefXmlElement = objXmlDoc.CreateElement("UserPref");
                objUserPrefXmlElement.InnerText = strJsonUserPref;
                objFinancialXmlElement.AppendChild(objUserPrefXmlElement);
                #endregion

                #region Prepare Json string for additional data and add to XMl doc
                XmlElement objAdditionalDataXmlElement = objXmlDoc.CreateElement("AdditionalData");
                strJsonResponse = new StringBuilder();
                //dicGridData = new Dictionary<string, string>();
                // lstDictData.Clear();
                strJsonResponse.Append("[{");
                if (objSysSetting.SysSettings.MultiCovgPerClm == -1)
                {
                    strJsonResponse.Append(Conversion.GetJsonNode("CarrierClaim", Boolean.TrueString, true));
                    if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                        strJsonResponse.Append(Conversion.GetJsonNode("Multicurrency", Boolean.TrueString));
                    else
                        strJsonResponse.Append(Conversion.GetJsonNode("Multicurrency", Boolean.FalseString));
                }
                else
                {
                    strJsonResponse.Append(Conversion.GetJsonNode("CarrierClaim", Boolean.TrueString, true));
                    if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                        strJsonResponse.Append(Conversion.GetJsonNode("Multicurrency", Boolean.TrueString));
                    else
                        strJsonResponse.Append(Conversion.GetJsonNode("Multicurrency", Boolean.FalseString));
                }

                strJsonResponse.Append(Conversion.GetJsonNode("TotalCount", i.ToString()));
                //lstDictData.Add(dicGridData);
                //strJsonResponse = Conversion.CreateJSONStringFromDictionary(lstDictData);
                strJsonResponse.Append("}]");
                objAdditionalDataXmlElement.InnerText = strJsonResponse.ToString();
                objFinancialXmlElement.AppendChild(objAdditionalDataXmlElement);
                #endregion

            }

            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetFinancialDetailSummary.GenericError", m_iClientId), objException);
            }
            finally
            {
                #region dispose resources
                sbSQL = null;
                if (objReader != null)
                    objReader.Dispose();
                if (objCache != null)
                    objCache.Dispose();
                objCache = null;
                if (objRdr != null)
                    objRdr.Dispose();
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                #endregion
            }
            //Return the result to calling function
            return objXmlDoc;
        }

        /// <summary>
        /// this function will create user pref for the grid if it does not exist in the database
        /// </summary>
        /// 

        private string GetOrCreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId, int iCarrierClaim)
        {
            string sJsonUserPref = string.Empty;
            bool IsUserPrefExist = false;

            try
            {
                Riskmaster.Common.GridCommonFunctions.GetUserHeaderAndPreference(ref sJsonUserPref, p_iUserID, m_iClientId, p_sPageName, p_iDSNId.ToString(), p_sGridId);
                if (String.IsNullOrEmpty(sJsonUserPref))
                    IsUserPrefExist = false;
                else
                    IsUserPrefExist = true;

                if (!IsUserPrefExist)
                {
                    return CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }


        /// <summary>
        /// This function is used to Create default User Pref when it does not exist in database
        /// </summary>
        /// <param name="p_iUserID"></param>
        /// <param name="p_sPageName"></param>
        /// <param name="p_iDSNId"></param>
        /// <param name="p_sGridId"></param>
        /// <returns></returns>
        private string CreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            try
            {
                //if there are multiple grids on the page, each grid can have different user pref.                
                if (p_sGridId == "gridFDH")
                    sJsonUserPref = CreateUserPreferenceForFDH(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;

        }

        private string getMLHeaderText(string key, string sPageID)
        {
            string sBaseLangCode = "0";
            string sUserLangCode = "0";
            try
            {
                sUserLangCode = m_iLangCode.ToString();
                if (sUserLangCode == "0")
                {
                    //set base lang code as user lang code if it is not defined
                    sUserLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                }
                return CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString(key, 0, sPageID, m_iClientId), sUserLangCode);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        private string CreateUserPreferenceForFDH(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            int iCarrierClaim;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            string sPageID = "0";
            int iIsMultiCurrencyOn = -1;
            string sPayeeAndClaimantCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><span ng-cell-text>{{COL_FIELD }}</span></div>"; //rma-8617            
            string sHyperlinkCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=parent.MDIShowScreen(\'{{row.getProperty(\'TransId\')}}\',\'funds\');return false; href=\"#\">{{row.getProperty(col.field)}}</a></div>";            ////rma-10914
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();

            try
            {
                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);
                iIsMultiCurrencyOn = objSysSettings.UseMultiCurrency;
                iCarrierClaim = objSysSettings.MultiCovgPerClm;
                lstColDef = new List<GridColumnDefHelper>();

                
                lstColDef.Add(new GridColumnDefHelper("TransId", "TransId", false, "", "", "", false, true));             ////rma-10914                   
                lstColDef.Add(new GridColumnDefHelper("DateTime", getMLHeaderText("lblDateTime", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                lstColDef.Add(new GridColumnDefHelper("DateTime.DbValue", "DateTime.DbValue", false, "", "", "", false, true, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));//rkotak:custom sorting,need to add sorting function in the arguments for this column because this is the default sorting column as well               
                lstColDef.Add(new GridColumnDefHelper("Status", getMLHeaderText("lblFinStatus", sPageID)));
                lstColDef.Add(new GridColumnDefHelper("CheckControlNumber", getMLHeaderText("lblCheckControlNumber", sPageID), true, sHyperlinkCellTemplate));
                if (iCarrierClaim == -1)
                {
                    lstColDef.Add(new GridColumnDefHelper("PolicyName", getMLHeaderText("lblFinPolicyName", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("Unit", getMLHeaderText("lblFinUnit", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("Coveragetype", getMLHeaderText("lblFinCoverageType", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("CausetypofLoss", getMLHeaderText("lblCausetypeofLoss", sPageID)));
                }
                lstColDef.Add(new GridColumnDefHelper("Financialtype", getMLHeaderText("lblFinancialType", sPageID)));
                lstColDef.Add(new GridColumnDefHelper("Claimant", getMLHeaderText("lblFinClaimant", sPageID), true, sPayeeAndClaimantCellTemplate));
                lstColDef.Add(new GridColumnDefHelper("Payee", getMLHeaderText("lblFinPayeeName", sPageID), true, sPayeeAndClaimantCellTemplate));
                lstColDef.Add(new GridColumnDefHelper("AmountUSD", getMLHeaderText("lblAmount", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper("AmountUSD.DbValue", "AmountUSD.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                if (iIsMultiCurrencyOn != 0)
                {
                    lstColDef.Add(new GridColumnDefHelper("AmountinClaimcurrency", getMLHeaderText("lblAmountinClaimcurrency", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("AmountinClaimcurrency.DbValue", "AmountinClaimcurrency.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                }
                // rkotak: multiple if conditions are intentional. Maintain strict order of columns
                lstColDef.Add(new GridColumnDefHelper("PaymentAmountUSD", getMLHeaderText("lblPaymentAmount", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper("PaymentAmountUSD.DbValue", "PaymentAmountUSD.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                if (iIsMultiCurrencyOn != 0)
                {
                    lstColDef.Add(new GridColumnDefHelper("PaymentAmountinPaymentCurrency", getMLHeaderText("lblPaymentAmountinPaymentCurrency", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("PaymentAmountinPaymentCurrency.DbValue", "PaymentAmountinPaymentCurrency.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                    lstColDef.Add(new GridColumnDefHelper("PaymentAmountinClaimCurrency", getMLHeaderText("lblPaymentAmountinClaimCurrency", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("PaymentAmountinClaimCurrency.DbValue", "PaymentAmountinClaimCurrency.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                }

                lstColDef.Add(new GridColumnDefHelper("CHANGEAMOUNT", getMLHeaderText("lblChangeAmt", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper("CHANGEAMOUNT.DbValue", "CHANGEAMOUNT.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                if (iIsMultiCurrencyOn != 0)
                {
                    lstColDef.Add(new GridColumnDefHelper("CHANGEAMTINCLAIMCURRENCY", getMLHeaderText("lblChangeAmtinClaimCurrency", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                    lstColDef.Add(new GridColumnDefHelper("CHANGEAMTINCLAIMCURRENCY.DbValue", "CHANGEAMTINCLAIMCURRENCY.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                }

                lstColDef.Add(new GridColumnDefHelper("Check", getMLHeaderText("lblFinCheck", sPageID)));
                lstColDef.Add(new GridColumnDefHelper("CHECKDATE", getMLHeaderText("lblFinCheckDate", sPageID), true, "", "", "", false, false, GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));//rma-10414
                lstColDef.Add(new GridColumnDefHelper("CHECKDATE.DbValue", "CHECKDATE.DbValue", false, "", "", "", false, true));//rkotak:custom sorting
                lstColDef.Add(new GridColumnDefHelper("Checkcleardate", getMLHeaderText("lblCheckcleardate", sPageID))); //we have not given ate sot function for this column because this col contains mix values of string and date ("No" or cleared date")
                lstColDef.Add(new GridColumnDefHelper("By", getMLHeaderText("lblBy", sPageID)));
                if (iIsMultiCurrencyOn != 0)
                {
                    lstColDef.Add(new GridColumnDefHelper("ClaimCurrency", getMLHeaderText("lblClaimCurrency", sPageID)));
                    lstColDef.Add(new GridColumnDefHelper("CurrencyConvRate", getMLHeaderText("lblCurrencyConvRate", sPageID)));
                }

                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.SortColumn = new string[1] { "DateTime.DbValue" }; // should match with sql query order by clause for default sorting
                oGridPref.SortDirection = new string[1] { "asc" };
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;

                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }

        /// <summary>
        /// This function is used to restore default user pref
        /// </summary>
        /// <param name="p_iUserID"></param>
        /// <param name="p_sPageName"></param>
        /// <param name="p_iDSNId"></param>
        /// <param name="p_sGridId"></param>
        /// <returns></returns>
        public string RestoreDefaultUserPref(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = "";
            try
            {
                if (Riskmaster.Common.GridCommonFunctions.DeleteUserPreference(p_iUserID, m_iClientId, p_sPageName, p_iDSNId, p_sGridId))
                    sJsonUserPref = CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.RestoreDefaultUserPref.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }

        /// <summary>
        /// This method would return a string containing FIRST NAME , LAST NAME of the entity.
        /// </summary>
        public string getEntityName(long lEntityID)
        {
            string sSql = "";
            DbReader objEntity = null;
            string sTmp = "";
            string sEntity = "";
            try
            {
                sSql = "SELECT LAST_NAME,FIRST_NAME FROM ENTITY WHERE ENTITY_ID = " + lEntityID;
                objEntity = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objEntity.Read())
                {
                    sTmp = Conversion.ConvertObjToStr(objEntity.GetValue("FIRST_NAME"));
                    sEntity = sTmp + "";
                    sTmp = Conversion.ConvertObjToStr(objEntity.GetValue("LAST_NAME"));
                    if (sTmp != "")
                        sEntity = sEntity + " " + sTmp;

                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.getEntityName.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objEntity != null)
                {
                    objEntity.Close();
                    objEntity.Dispose();
                }
            }
            return sEntity.Trim();
        }

        private string GetPayeeName(int iTransID, string sAnd, string sClaimantFirstName, string sClaimantLastName)
        {
            string sPayeesName = "";
            if (iTransID > 0)
            {
                string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + iTransID;
                if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                {
                    sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                    sEntSQL = sEntSQL.Replace("+", "||");

                }
                using (DbReader objeReader = DbFactory.GetDbReader(m_sConnectionString, sEntSQL))
                {
                    while (objeReader.Read())
                    {
                        if (sPayeesName == string.Empty)
                        {
                            sPayeesName = objeReader.GetString("PAYEES_NAME");
                        }
                        else
                        {
                            sPayeesName = sPayeesName + " " + sAnd + " " + objeReader.GetString("PAYEES_NAME");
                        }
                    }
                }

                if (string.IsNullOrEmpty(sPayeesName))
                {
                    if (!string.IsNullOrEmpty(sClaimantFirstName))
                    {
                        sPayeesName = sClaimantFirstName + " " + sClaimantLastName;
                    }
                    else
                    {
                        sPayeesName = sClaimantLastName;
                    }
                }

            }
            return sPayeesName.Trim();
        }
        //rkotak:FDH 4608,9681 end
        //bsharma33: RMA-3887 Reserve supplemental Start
        public XmlDocument GetReserveStatusReasons(string p_sParentShortCode, int p_iCodeId)
        {
            XmlElement objXmlElementReason = null;
            XmlDocument objXmlDoc = null;
            XmlElement objXmlElementTmp = null;
            XmlElement objXmlElement = null;
            SysSettings objSysSetting = null;
            XmlElement objXmlElementStatus = null;
            DbReader objReader = null;
            StringBuilder sbSQL = null;
            string sText = string.Empty;
            objXmlDoc = new XmlDocument();

            objXmlElement = objXmlDoc.CreateElement("ReservesReasonStatus");
            objXmlDoc.AppendChild(objXmlElement);
            objXmlElementReason = objXmlDoc.CreateElement("Reason");
            objXmlElement.AppendChild(objXmlElementReason);
            sbSQL = new StringBuilder();
            sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
            sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
            sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
            sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'REASON' AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
            //Start PARAG MITS 10269
            sbSQL.Append(" AND CODES.DELETED_FLAG!=-1 ");
            sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "REASON", this.LanguageCode);
            objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
            if (objReader != null)
            {
                while (objReader.Read())
                {
                    objXmlElementTmp = objXmlDoc.CreateElement("ReasonCode");
                    objXmlElementReason.AppendChild(objXmlElementTmp);

                    objXmlElementTmp.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());

                    sText = objReader.GetValue("SHORT_CODE").ToString();
                    sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();

                    objXmlElementTmp.InnerText = sText;

                }
                objReader.Close();

            }
            sbSQL.Remove(0, sbSQL.Length);

            objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);

            objXmlElementStatus = objXmlDoc.CreateElement("Status");
            objXmlElement.AppendChild(objXmlElementStatus);



            sbSQL = new StringBuilder();
            if (p_sParentShortCode == "H")
            {
                sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                sbSQL.Append(" FROM CODES,CODES_TEXT");
                sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.CODE_ID = " + p_iCodeId.ToString());
                sbSQL.Append(" AND CODES.DELETED_FLAG = 0 AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
            }
            else
            {
                //Aman ML Changes--start
                sbSQL.Append(" SELECT CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.CODE_ID ");
                sbSQL.Append(" FROM CODES,CODES_TEXT,GLOSSARY ");
                sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                sbSQL.Append(" AND CODES.DELETED_FLAG = 0 AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                //Ankit Start : Financial Enhancement - Status Code Changes

                //rsharma220 MITS 32331 Start
                if (objSysSetting.MultiCovgPerClm != 0)
                {
                    sbSQL.Append(" AND CODES.RELATED_CODE_ID NOT IN ( SELECT codes.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_STATUS_PARENT' ");
                    sbSQL.Append(" AND CODES.SHORT_CODE IN ('A','H','Re','F') ) "); //rupal:r8 first & final payment, first & final reserve status is created internally only, its should not be displayed in status dropdown
                }
                else
                {
                    sbSQL.Append(" AND CODES.RELATED_CODE_ID NOT IN ( SELECT codes.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY ");
                    sbSQL.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = GLOSSARY.TABLE_ID ");
                    sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME='RESERVE_STATUS_PARENT'");
                    sbSQL.Append(" AND CODES.SHORT_CODE IN ('A','H','Re','F','Cn') )");
                }
                //rsharma220 MITS 32331 End
                //Ankit End

                sbSQL.Append(" AND GLOSSARY.SYSTEM_TABLE_NAME = 'RESERVE_STATUS' ");
                sbSQL.Append(" AND CODES.SHORT_CODE NOT IN ('A','R','H')");  //mbahl3 Reserve Approval Enhancement Mits 32471
                sbSQL.Append(" ORDER BY CODES.SHORT_CODE");
            }
            sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "RESERVE_STATUS", this.LanguageCode);
            //Aman ML Changes--end
            objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
            if (objReader != null)
            {
                while (objReader.Read())
                {
                    //start:rupal, first & final payment
                    //we don't want "First & Final" reserve status to be displayed in dropdown as this status code is used internally only
                    //sText = objReader.GetValue("SHORT_CODE").ToString();
                    sText = objReader.GetValue(0).ToString();
                    //if (sText == "F")
                    //    continue;
                    //sText = sText + " - " + objReader.GetValue("CODE_DESC").ToString();                                                
                    sText = sText + " - " + objReader.GetValue(1).ToString();
                    objXmlElementTmp = objXmlDoc.CreateElement("StatusCode");
                    objXmlElementStatus.AppendChild(objXmlElementTmp);

                    //objXmlElementTmp.SetAttribute("value", objReader.GetValue("CODE_ID").ToString());
                    objXmlElementTmp.SetAttribute("value", objReader.GetValue(2).ToString());
                    //RUPAL:first & final payment
                    objXmlElementTmp.SetAttribute("shortcode", objReader.GetValue(0).ToString());

                    objXmlElementTmp.InnerText = sText;
                    //end:rupal, first & final payment


                }
                objReader.Close();

            }
            sbSQL.Remove(0, sbSQL.Length);

            return objXmlDoc;
        }
        //bsharma33: RMA-3887 Reserve supplemental end
    }// End class
} // End namespace
