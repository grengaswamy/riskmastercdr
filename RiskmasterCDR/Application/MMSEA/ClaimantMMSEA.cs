﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.Application.MMSEA
{
    public class ClaimantMMSEA
    {
        #region Member Variables

        /// <summary>
        /// DSN
        /// </summary>
        private string m_sDatabaseName = "";

        /// <summary>
        /// User Id
        /// </summary>
        private string m_sUserName = "";

        /// <summary>
        /// Password of the user
        /// </summary>
        private string m_sPassword = "";
        private bool m_Clone = false;
        private bool m_New = false;
        private string m_sConnectionString = string.Empty;
        private DataModelFactory m_objDataModelFactory = null;
        //skhare7 MITS 18069
       
        private int m_iSecurityId = 0;
        private   int iLOB = 0;
        private int m_iClientId = 0;//sonali jira 880
        //skhare7 MITS 18069 end

        private LocalCache m_objLocalCache = null;
        /// <summary>
        /// Read Property for LocalCache.
        /// </summary>
        private LocalCache LocalCache
        {
            get
            {
                if (m_objLocalCache == null)
                {
                    m_objLocalCache = m_objDataModelFactory.Context.LocalCache;
                }
                return (m_objLocalCache);
            }
        }
        /// <summary>
        /// DataModelFactory object
        /// </summary>
        DataModelFactory m_objDMF = null;

        #endregion
        #region Constructor
        public ClaimantMMSEA(string p_sDatabaseName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
			m_sDatabaseName = p_sDatabaseName;
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
            m_iClientId = p_iClientId;//sonali jira- 880

        }

        #endregion

         #region Destructor
        ~ClaimantMMSEA()
        {
            Dispose();

        }
        #endregion

        #region IDisposable Method
       
        /// <summary>
        ///		This method will be called by the caller of this library to nullify class level object.		
        /// </summary>				
        public void Dispose()
        {
            if (m_objDMF != null)
            {
                m_objDMF.UnInitialize();
                m_objDMF.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }

        }

        #endregion



        #region Methods
        /// <summary>
        /// XML Format
//<ClaimantMMSEA>
//  <ClaimantRowId>0</ClaimantRowId> 
//  <IsBenificaryCode codeid="" /> 
//  <IsPaymentNonMedical codeid="">Yes</IsPaymentNonMedical> 
//  <ConfirmedDate /> 
//  <HealthClaimNumber /> 
//  <ICDEventCode codeid="">fgdfgfg</ICDEventCode> 
//  <LastExtractDate /> 
//  <ResponsiblityForMedicalsCode codeid="">Yes</ResponsiblityForMedicalsCode> 
//  <ResponsibilityTerminationDate>5/4/2009</ResponsibilityTerminationDate> 
//- <TPOC>
//- <listhead>
//  <DelayedBeyongTPOCStartDate>Funding Delayed Beyond TPOC Start Date</DelayedBeyongTPOCStartDate> 
//  <PaymentObligationAmount>TPOC Amount</PaymentObligationAmount> 
//  <DateofWrittenAgreement>Date Of Agreement</DateofWrittenAgreement> 
//  <DateofCourtApproval>Date Of Court Approval</DateofCourtApproval> 
//  <DateofPaymentIssue>Date Of Payment Issue</DateofPaymentIssue> 
//  <DeletedFlag>Deleted</DeletedFlag> 
//  <FilMdcreRcdDATE>Date Filed</FilMdcreRcdDATE> 
//  </listhead>
//- <option ref="&nbsp;">
//  <DelayedBeyongTPOCStartDate>20090504</DelayedBeyongTPOCStartDate> 
//  <PaymentObligationAmount>6</PaymentObligationAmount> 
//  <DateofWrittenAgreement>20090505</DateofWrittenAgreement> 
//  <DateofCourtApproval>20090505</DateofCourtApproval> 
//  <DateofPaymentIssue>20090505</DateofPaymentIssue> 
//  <DeletedFlag>0</DeletedFlag> 
//  <FilMdcreRcdDATE /> 
//  <TpocEditFlag>-1</TpocEditFlag> 
//  <ClaimantRowID>4654</ClaimantRowID> 
//  <TpocRowID>1</TpocRowID> 
//  </option>
//  </TPOC>
//- <CSCDiagnosisList>
//  <Diagnosis codeid="" /> 
//  </CSCDiagnosisList>
//- <MMSEAParty>
//- <listhead>
//  <PartyName>Party Name</PartyName> 
//  <RelationToBenificiary>Relationship to Beneficiary</RelationToBenificiary> 
//  <TypeOfRepresentative>Type of Representative</TypeOfRepresentative> 
//  <PartyToClaim>Party to Claim Representative</PartyToClaim> 
//  </listhead>
//- <option ref="&nbsp;">
//  <PartyName>Goel, Sameer</PartyName> 
//  <RelationToBenificiary codeid="5412">Family</RelationToBenificiary> 
//  <TypeOfRepresentative codeid="5425">Attorney</TypeOfRepresentative> 
//  <PartyToClaim entityid="315">Mattlock, Matthew</PartyToClaim> 
//  <ClaimantRowID>4654</ClaimantRowID> 
//  <MMSEAClaimantRowID>1</MMSEAClaimantRowID> 
//  </option>
//  </MMSEAParty>
//  <ProductLiabilityCode codeid="5423">Yes-but not a mass tort situation</ProductLiabilityCode> 
//  <ProductGenericName>ttt</ProductGenericName> 
//  <ProductBrandName>ttt</ProductBrandName> 
//  <ProductManufacturer>ttt</ProductManufacturer> 
//  <ProductHarmText>test22</ProductHarmText> 
//  <MSPEffectiveDate /> 
//  <MSPTerminationDate /> 
//  <MSPTypeIndicator codeid="">0</MSPTypeIndicator> 
//  <DispositionCode codeid="">0</DispositionCode> 
//  </ClaimantMMSEA>
        /// </summary>
        /// <param name="inputDoc"></param>
        /// <returns></returns>
        public bool SaveClaimantMMSEAData(XmlDocument inputDoc, UserLogin p_objLogin)
        {
             ClaimantMmsea objClaimantMmsea = null;
             InitializeDataModel();
             objClaimantMmsea = (ClaimantMmsea)m_objDMF.GetDataModelObject("ClaimantMmsea", false);

             int intClaimantRowId;
             XmlElement objNode = null;
             DbConnection objDbConnection = null;
             try
             {
                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ClaimantRowID");
                 objClaimantMmsea.ClaimantRowID = ConvetToInt(objNode.InnerXml);
                 intClaimantRowId = objClaimantMmsea.ClaimantRowID;

                 
                 objNode = (XmlElement)inputDoc.SelectSingleNode("//DttmRcdLastUpd");
                 objClaimantMmsea.DttmRcdLastUpd = objNode.InnerXml.Replace("/", "");
                 

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//IsBenificaryCode");
                 objClaimantMmsea.IsBenificaryCode = ConvetToInt(objNode.GetAttribute("codeid"));

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//IsPaymentNonMedical");
                 objClaimantMmsea.IsPaymentNonMedical = ConvetToInt(objNode.GetAttribute("codeid"));

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ConfirmedDate");
                 objClaimantMmsea.ConfirmedDate = ToDate(objNode.InnerXml);
                

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//HealthClaimNumber");
                 objClaimantMmsea.HealthClaimNumber = objNode.InnerXml;

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ICDEventCode");
                 objClaimantMmsea.ICDEventCode = ConvetToInt(objNode.GetAttribute("codeid"));

                 //prashbiharis MITS: 32423 ICD10 Event Code implementation -start
                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ICD10EventCode");
                 objClaimantMmsea.ICD10EventCode = ConvetToInt(objNode.GetAttribute("codeid"));
                 //prashbiharis MITS: 32423 ICD10 Event Code implementation -End
                 objNode = (XmlElement)inputDoc.SelectSingleNode("//LastExtractDate");
                 objClaimantMmsea.LastExtractDate = ToDate(objNode.InnerXml);


                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ProductLiabilityCode");
                 objClaimantMmsea.ProductLiabilityCode = ConvetToInt(objNode.GetAttribute("codeid"));

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ProductGenericName");
                 objClaimantMmsea.ProductGenericName = objNode.InnerXml;

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ProductBrandName");
                 objClaimantMmsea.ProductBrandName = objNode.InnerXml;

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ProductManufacturer");
                 objClaimantMmsea.ProductManufacturer = objNode.InnerXml;


                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ProductHarmText");
                 objClaimantMmsea.ProductHarmText = objNode.InnerXml;

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ResponsiblityForMedicalsCode");
                 objClaimantMmsea.ResponsiblityForMedicalsCode = ConvetToInt(objNode.GetAttribute("codeid"));

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ResponsibilityTerminationDate");
                 objClaimantMmsea.ResponsibilityTerminationDate = ToDate(objNode.InnerXml);
               
                 //objNode = (XmlElement)inputDoc.SelectSingleNode("//FirstExtractDate");
                 //objClaimantMmsea.FirstExtractDate = objNode.InnerXml;

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//MMSEAEditedFlag");
                 objClaimantMmsea.MMSEAEditedFlag = ConvetToInt(objNode.InnerXml);

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//MSPTerminationDate");
                 objClaimantMmsea.MSPTerminationDate =  ToDate(objNode.InnerXml); ;

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//MSPTypeIndicator");
                 objClaimantMmsea.MSPTypeIndicator = ConvetToInt(objNode.GetAttribute("codeid")); 

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//DispositionCode");
                 objClaimantMmsea.DispositionCode = ConvetToInt(objNode.GetAttribute("codeid"));

                 

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//DelMDCRERCDFlag");
                 objClaimantMmsea.DelMDCRERCDFlag = ConvetToInt(objNode.InnerXml);

                 objNode = (XmlElement)inputDoc.SelectSingleNode("//DelMDCRERCDDate");
                 objClaimantMmsea.DelMDCRERCDDate =  ToDate(objNode.InnerXml) ;

                 //Added by Amitosh for Mits 24380 (03/16/2011)
				 //Added by asingh263 for mits 30525 starts
                int isCarrierClaimOn = objClaimantMmsea.Context.InternalSettings.SysSettings.MultiCovgPerClm;

                if (isCarrierClaimOn.Equals(0))
                {
                    objNode = (XmlElement)inputDoc.SelectSingleNode("//NoFaultIndicator");
                    objClaimantMmsea.NoFaultIndicator = ConvetToInt(objNode.GetAttribute("codeid"));

                    objNode = (XmlElement)inputDoc.SelectSingleNode("//NoFaultInsuranceLimit");
                    objClaimantMmsea.NoFaultInsuranceLimit = Conversion.ConvertStrToDouble(objNode.InnerText);

                    objNode = (XmlElement)inputDoc.SelectSingleNode("//NoFaultExhaustDate");
                    objClaimantMmsea.NoFaultExhaustDate = ToDate(objNode.InnerXml);
                     //end Amitosh
                }
                else if (isCarrierClaimOn.Equals(-1))
                {

                    objNode = (XmlElement)inputDoc.SelectSingleNode("//CarrierNoFaultIndicator");
                    objClaimantMmsea.NoFaultIndicator = ConvetToInt(objNode.GetAttribute("codeid"));

                    objNode = (XmlElement)inputDoc.SelectSingleNode("//CarrierNoFaultInsuranceLimit");
                    objClaimantMmsea.NoFaultInsuranceLimit = Conversion.ConvertStrToDouble(objNode.InnerText);

                    objNode = (XmlElement)inputDoc.SelectSingleNode("//CarrierNoFaultExhaustDate");
                    objClaimantMmsea.NoFaultExhaustDate = ToDate(objNode.InnerXml);
                }
                    //end asingh263 mits 30525
                    //objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("AddedByUser", objClaimantMmsea.AddedByUser.ToString()), true));
                   //objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("UpdatedByUser", objClaimantMmsea.UpdatedByUser.ToString()), true));
                 //Added by Kiran for MITS 24985 02/22/2013
                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ClaimantsRepresentativeCode");
                 objClaimantMmsea.ClaimantRepresentativeCode = ConvetToInt(objNode.GetAttribute("codeid"));

                 //added by asingh263 for mits 32440
                 objNode = (XmlElement)inputDoc.SelectSingleNode("//ClaimantRepresentativeName");
                 objClaimantMmsea.ClaimantRepresentativeEID = ConvetToInt(objNode.GetAttribute("codeid"));
                 //ends mits 32440


                 ClmtMmseaTpoc objClmtMmseaTpoc = null;

                 XmlNodeList objNodeCollection = inputDoc.SelectNodes("//TPOC/option");
                 XmlNode objXnode = null;
                 
    
                 foreach (XmlNode objXmlNode in objNodeCollection)
                 {
                    
                    
                     objClmtMmseaTpoc = objClaimantMmsea.ClmtMmseaTpocList.AddNew();

                     objXnode = objXmlNode.SelectSingleNode("TpocRowID");
                     objClmtMmseaTpoc.TpocRowID = ConvetToInt(objXnode.InnerXml);
                     
                     objXnode = objXmlNode.SelectSingleNode("DelayedBeyongTPOCStartDate");
                     objClmtMmseaTpoc.DelayedBeyongTPOCStartDate = ToDate(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("PaymentObligationAmount");
                     if (objXnode != null && objXnode.InnerXml != "")
                         objClmtMmseaTpoc.PaymentObligationAmount = Conversion.ConvertStrToDouble(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("DateofWrittenAgreement");
                     objClmtMmseaTpoc.DateofWrittenAgreement =  ToDate(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("DateofCourtApproval");
                     objClmtMmseaTpoc.DateofCourtApproval =  ToDate(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("DateofPaymentIssue");
                     objClmtMmseaTpoc.DateofPaymentIssue =  ToDate(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("DeletedFlag");
                     if (objXnode.InnerXml.ToUpper() == "NO" 
                         //sgoel6 MITS 16899
                         || objXnode.InnerXml.ToUpper() == "FALSE")
                     {
                         objClmtMmseaTpoc.DeletedFlag = 0;
                     }
                     else
                     {
                         objClmtMmseaTpoc.DeletedFlag = 1;
                     }

                     objXnode = objXmlNode.SelectSingleNode("FilMdcreRcdDATE");
                     objClmtMmseaTpoc.FilMdcreRcdDATE =  ToDate(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("TpocEditFlag");
                     objClmtMmseaTpoc.TpocEditFlag = ConvetToInt(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("ClaimantRowID");
                     objClmtMmseaTpoc.ClaimantRowID = intClaimantRowId;

                     
                     objXnode = objXmlNode.SelectSingleNode("DTTM_RCD_LAST_UPD");
                     objClmtMmseaTpoc.DttmRcdLastUpd = objXnode.InnerXml.Replace("/", "");

                     objXnode = objXmlNode.SelectSingleNode("UpdatedByUser");
                     if (objXnode != null)
                        objClmtMmseaTpoc.UpdatedByUser = objXnode.InnerXml;

                     objXnode = objXmlNode.SelectSingleNode("AddedByUser");
                     if(objXnode!=null)
                        objClmtMmseaTpoc.AddedByUser = objXnode.InnerXml;

                    
                     objXnode = objXmlNode.SelectSingleNode("DttmRcdAdded");
                     objClmtMmseaTpoc.DttmRcdAdded = objXnode.InnerXml;
                    

                 }



                 //---------------For ClaimantMMSEAXParty

                 ClaimantMMSEAXParty objClaimantMMSEAXParty = null;
                 objNodeCollection = inputDoc.SelectNodes("//MMSEAParty/option");
                
                 
                 foreach (XmlNode objXmlNode in objNodeCollection)
                 {
                     objClaimantMMSEAXParty = objClaimantMmsea.ClaimantMMSEAXPartyList.AddNew();

                     objClaimantMMSEAXParty.MMSEAClaimantRowID = intClaimantRowId;

                     objXnode = objXmlNode.SelectSingleNode("MMSEAClaimantRowID");
                     objClaimantMMSEAXParty.MMSEAClaimantRowID = ConvetToInt(objXnode.InnerXml);

                     objXnode = objXmlNode.SelectSingleNode("RelationToBenificiary");
                     objClaimantMMSEAXParty.PartyToClaimRelationshipCode = ConvetToInt(objXnode.Attributes["codeid"].Value);

                     objXnode = objXmlNode.SelectSingleNode("att1lastfirstname");
                     objClaimantMMSEAXParty.PartyToClaimEntityEID = ConvetToInt(objXnode.Attributes["codeid"].Value);

                     objXnode = objXmlNode.SelectSingleNode("TypeOfRepresentative");
                     objClaimantMMSEAXParty.PartyToClaimReprsesentativeTypeCode = ConvetToInt(objXnode.Attributes["codeid"].Value);

                     objXnode = objXmlNode.SelectSingleNode("attlastfirstname");
                     objClaimantMMSEAXParty.PartyToClaimReprsesentativeEntityEID = ConvetToInt(objXnode.Attributes["codeid"].Value);

                     

                     objXnode = objXmlNode.SelectSingleNode("DttmRcdLastUpd");
                     objClaimantMMSEAXParty.DttmRcdLastUpd = objXnode.InnerXml.Replace("/","");

                     objXnode = objXmlNode.SelectSingleNode("UpdatedByUser");
                     if (objXnode != null)
                        objClaimantMMSEAXParty.UpdatedByUser = objXnode.InnerXml;

                     objXnode = objXmlNode.SelectSingleNode("AddedByUser");
                     if (objXnode != null)
                        objClaimantMMSEAXParty.AddedByUser = objXnode.InnerXml;

                 }

                 
                 //------------------------------------------------
                 //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen
                 //skhare7 MITS 18069
                 GetSecurityIDS(intClaimantRowId);
                 //skhare7 MITS 18069 end
                 using (DbConnection objConnection = DbFactory.GetDbConnection(m_sConnectionString))
                 {
                     string sSql = string.Empty;
                     object objExecuteQuery = null;
                     sSql = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT_MMSEA WHERE CLAIMANT_ROW_ID=" + intClaimantRowId;
                     objConnection.Open();
                     objExecuteQuery = objConnection.ExecuteScalar(sSql);
                     if (objExecuteQuery == null)
                     {//skhare7 MITS 18069
                         switch (iLOB)
                         {
                             case 241:

                                 m_iSecurityId = RMPermissions.RMO_GC_MMSEA;
                                 break;
                             case 242:

                                 m_iSecurityId = RMPermissions.RMO_VA_MMSEA;
                                 break;
                             case 243:

                                 m_iSecurityId = RMPermissions.RMO_WC_MMSEA;
                                 break;

                         }

                         if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_CREATE))
                         {
                            throw new RMAppException("No permissions to save");
                            
                         }
                         //skhare7 MITS 18069 end
                         objClaimantMmsea.DttmRcdLastUpd = CreateRecord(Convert.ToString(intClaimantRowId));


                     }
                     //skhare7 MITS 18069
                     else
                     {

                         switch (iLOB)
                         {
                             case 241:

                                 m_iSecurityId = RMPermissions.RMO_GC_MMSEA;
                                 break;
                             case 242:

                                 m_iSecurityId = RMPermissions.RMO_VA_MMSEA;
                                 break;
                             case 243:

                                 m_iSecurityId = RMPermissions.RMO_WC_MMSEA;
                                 break;
                                 

                         }
                         if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                         {
                             throw new RMAppException("No permissions to edit.");

                         }
                     
                     }
                     //skhare7 MITS 18069 end

                 }
                 //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen


                 //skhare7



                 objClaimantMmsea.Save();

                 string sSQL = "";
                 objXnode = inputDoc.SelectSingleNode("//TPOCDeletedValues");
                 string[] strSplit = objXnode.InnerXml.Split(new Char[] { ',' });
                 for (int count = 0; count < strSplit.Length; count++)
                 {
                     if (strSplit[count] != "")
                     {
                         sSQL = "DELETE FROM CLMT_MMSEA_TPOC WHERE TPOC_ROW_ID = " + strSplit[count];
                         objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                         if (objDbConnection != null)
                             objDbConnection.Open();
                         objDbConnection.ExecuteNonQuery(sSQL);
                         objDbConnection.Close();
                     }
                     
                 }


                 objXnode = inputDoc.SelectSingleNode("//PartyDeletedValues");
                 strSplit = objXnode.InnerXml.Split(new Char[] { ',' });
                 for (int count = 0; count < strSplit.Length; count++)
                 {
                     if (strSplit[count] != "")
                     {
                         sSQL = "DELETE FROM CLAIMANT_X_MMS_CLT WHERE MMSEA_CLMT_ROW_ID = " + strSplit[count];
                         objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                         if (objDbConnection != null)
                             objDbConnection.Open();
                         objDbConnection.ExecuteNonQuery(sSQL);
                         objDbConnection.Close();
                     }
                 }


                 objNodeCollection = inputDoc.SelectNodes("//CSCDiagnosisList/DiagnosisList/Item");

                 sSQL = "DELETE FROM CT_MMS_X_DIAGNOSIS WHERE CLAIMANT_ROW_ID =" + intClaimantRowId;
                 objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                 if (objDbConnection != null)
                     objDbConnection.Open();
                 objDbConnection.ExecuteNonQuery(sSQL);
                 objDbConnection.Close();

                 foreach (XmlNode objXmlNode in objNodeCollection)
                 {
                     //objXnode = objXmlNode.SelectSingleNode("DiagnosisList/Item");
                     if (objXmlNode.Attributes["codeid"].Value != "")
                     {
                         sSQL = "INSERT INTO CT_MMS_X_DIAGNOSIS (CLAIMANT_ROW_ID, CDC_ICD_DIGNS_CODE) VALUES (" + intClaimantRowId.ToString() + ", " + objXmlNode.Attributes["codeid"].Value + ")";
 
                         objDbConnection.Open();
                         objDbConnection.ExecuteNonQuery(sSQL);
                         objDbConnection.Close();
                     }
                 }

                 //MITS 32423 : Praveen  ICD10 changes start
                 objNodeCollection = inputDoc.SelectNodes("//CSCICD10DiagnosisList/DiagnosisList/Item");

                 sSQL = "DELETE FROM CT_MMS_X_DIAGNOSIS_ICD10 WHERE CLAIMANT_ROW_ID =" + intClaimantRowId;
                 objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                 if (objDbConnection != null)
                     objDbConnection.Open();
                 objDbConnection.ExecuteNonQuery(sSQL);
                 objDbConnection.Close();

                 foreach (XmlNode objXmlNode in objNodeCollection)
                 {
                     //objXnode = objXmlNode.SelectSingleNode("DiagnosisList/Item");
                     if (objXmlNode.Attributes["codeid"].Value != "")
                     {
                         sSQL = "INSERT INTO CT_MMS_X_DIAGNOSIS_ICD10 (CLAIMANT_ROW_ID, CDC_ICD_DIGNS_CODE) VALUES (" + intClaimantRowId.ToString() + ", " + objXmlNode.Attributes["codeid"].Value + ")";

                         objDbConnection.Open();
                         objDbConnection.ExecuteNonQuery(sSQL);
                         objDbConnection.Close();
                     }
                 }
                 //MITS 32423 : Praveen  ICD10 changes End
         

                 return true;
             }
             catch (RMAppException p_objExp)
             {
                 throw p_objExp;
             }
             catch(Exception Ex)
             {
                 throw new RMAppException("Error in SaveClaimantMMSEAData", Ex.InnerException);
                 
             }
            
        }
          //skhare7 MITS 18069
        private void GetSecurityIDS(int lClaimantRowID)
        {
            
            if ( m_iSecurityId == 0)
            {
               
                DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "select CLAIM.LINE_OF_BUS_CODE from CLAIM,CLAIMANT where CLAIM.CLAIM_ID=CLAIMANT.CLAIM_ID and CLAIMANT.CLAIMANT_ROW_ID  = " + lClaimantRowID);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iLOB = objReader.GetInt32("LINE_OF_BUS_CODE");
                        }
                        objReader.Close();
                        objReader.Dispose();
                    }
                

                switch (iLOB)
                {
                    case 241:

                        m_iSecurityId = RMPermissions.RMO_GC_MMSEA;
                        
                        break;
                    case 242:
                       
                        m_iSecurityId = RMPermissions.RMO_VA_MMSEA;
                      
                        break;
                    case 243:
                      
                        m_iSecurityId = RMPermissions.RMO_WC_MMSEA;
                        break;
                    //no need to throw error bcz it will break other claims functionlity. JIRA 5416
                        //default:
                        //throw new RMAppException("Can't find line of business for security context ");
                };
            }

           
        }     //skhare7 MITS 18069 end
        private string ToDate(string sDate)
        {
            if (sDate != "")
            {
                sDate = Conversion.ToDbDateTime(DateTime.Parse(sDate));
            }
            else
            {
                sDate = "";
            }

            return sDate;
        }

        private string CreateRecord(string sClaimantRowId) 
        {
            string dtAdded = System.DateTime.Now.ToString("yyyyMMddHHmmss");
            string sSQL = "INSERT INTO CLAIMANT_MMSEA (CLAIMANT_ROW_ID, ADDED_BY_USER, UPDATED_BY_USER, DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD) VALUES(" + sClaimantRowId + ", '" + this.m_sUserName + "', '" + this.m_sUserName + "', '" + dtAdded + "', '" + dtAdded + "')";
           DbConnection objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
            if (objDbConnection != null)
               objDbConnection.Open();
           objDbConnection.ExecuteNonQuery(sSQL);

           objDbConnection.Close();

           return dtAdded;
        }

        private int ConvetToInt(string sNumber)
        {
            try
            {
                if (sNumber.Trim() != "")
                {
                    return Int32.Parse(sNumber);
                }
                else
                {
                    return 0;
                }
            }
            catch(Exception ex)
            {
                return 0;
            }
        }


        public XmlDocument GetClaimantRowIdIfExist(int p_iClaimantEid, int p_iClaimId)
        {
            int CLAIMANT_ROW_ID = -1;
            string sSQL = "";
                
            sSQL = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT WHERE CLAIMANT_EID =" + p_iClaimantEid + " AND CLAIM_ID =" + p_iClaimId;
            //bool bIsNew = true; //Commented For Mits 19213-Variable is no more being used.
            XmlDocument objOutXML  = new XmlDocument();
            DbReader objDbReader = null;
            try
            {
                InitializeDataModel();

                

                if (p_iClaimId != -1)
                {
                    objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());
                    if (objDbReader != null)
                    {

                        while (objDbReader.Read())
                        {
                            CLAIMANT_ROW_ID = objDbReader.GetInt("CLAIMANT_ROW_ID");
                        }
                        objDbReader.Close();
                    }
                }
                else
                {
                    CLAIMANT_ROW_ID = p_iClaimantEid;
                }

                //Commented for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen
                //if (CLAIMANT_ROW_ID > -1)
                //{
                //    sSQL = "SELECT * FROM CLAIMANT_MMSEA WHERE CLAIMANT_ROW_ID = " + CLAIMANT_ROW_ID.ToString();
                //    objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());

                //    if (objDbReader != null)
                //    {

                //        while (objDbReader.Read())
                //        {
                //            //bIsNew = false;
                //            break;
                //        }
                //        objDbReader.Close();
                //    }
                //}
                //if (bIsNew)
                //{
                //    CreateRecord(CLAIMANT_ROW_ID.ToString());
                //}
                //Commented for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen

                XmlElement objRoot = objOutXML.CreateElement("ClaimantMMSEA");

                string sNode = "<ClaimantRowID>" + CLAIMANT_ROW_ID.ToString() + "</ClaimantRowID>";
                //sNode = sNode + "<IsNew>" + bIsNew.ToString() + "</IsNew>";
                objRoot.InnerXml = sNode;
                objOutXML.AppendChild(objRoot);

            }
            catch(Exception Ex)
            {
                throw new Exception("Error in ClaimantMMSEA.GetClaimantRowIdIfExist", Ex.InnerException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }
            return objOutXML;
            
        }
        //Asharma326 25824 Check permissions Starts
        private void CheckPermission(ref XmlDocument objOutXML, UserLogin p_objLogin)
        {
            XmlElement objRoot = objOutXML.CreateElement("IsReadOnly");

            XmlElement objNode = null;
            int intClaimantRowId;
            //------------------------------------------------
            //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen
            //skhare7 MITS 18069
            objNode = (XmlElement)objOutXML.SelectSingleNode("//ClaimantRowID");
            intClaimantRowId = ConvetToInt(objNode.InnerXml);
            GetSecurityIDS(intClaimantRowId);
            using (DbConnection objConnection = DbFactory.GetDbConnection(m_sConnectionString))
            {
                string sSql = string.Empty;
                object objExecuteQuery = null;
                sSql = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT_MMSEA WHERE CLAIMANT_ROW_ID=" + intClaimantRowId;
                objConnection.Open();
                objExecuteQuery = objConnection.ExecuteScalar(sSql);
                if (objExecuteQuery == null)
                {//skhare7 MITS 18069
                    switch (iLOB)
                    {
                        case 241:

                            m_iSecurityId = RMPermissions.RMO_GC_MMSEA;
                            break;
                        case 242:

                            m_iSecurityId = RMPermissions.RMO_VA_MMSEA;
                            break;
                        case 243:

                            m_iSecurityId = RMPermissions.RMO_WC_MMSEA;
                            break;

                    }

                    if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_CREATE))
                    {
                        objRoot.InnerXml = "true";
                        objOutXML.DocumentElement.AppendChild(objRoot);

                    }
                    //skhare7 MITS 18069 end
                    //objClaimantMmsea.DttmRcdLastUpd = CreateRecord(Convert.ToString(intClaimantRowId));


                }
                //skhare7 MITS 18069
                else
                {

                    switch (iLOB)
                    {
                        case 241:

                            m_iSecurityId = RMPermissions.RMO_GC_MMSEA;
                            break;
                        case 242:

                            m_iSecurityId = RMPermissions.RMO_VA_MMSEA;
                            break;
                        case 243:

                            m_iSecurityId = RMPermissions.RMO_WC_MMSEA;
                            break;


                    }
                    if (!p_objLogin.IsAllowedEx(m_iSecurityId, RMPermissions.RMO_UPDATE))
                    {
                        objRoot.InnerXml = "true";
                        objOutXML.DocumentElement.AppendChild(objRoot);

                    }

                }
                //skhare7 MITS 18069 end

            }
            //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen
            //skhare7
        }
        //Ashama326 25824 Check permissions Ends


        public XmlDocument GetClaimantMMSEAData(int p_iclaimantrowid, UserLogin p_objLogin,string p_sPageId, string p_sLangCode)//rkulavil - ML Changes - MITS 34107 -PageId and Lang ID added
        {
            ClaimantMmsea objClaimantMmsea = null;
            ClaimantMMSEAXParty objClaimantMMSEAXParty = null;
            XmlElement objXMLElem = null;
            XmlDocument objOutXML = null;
            XmlElement objElement = null;
            RMConfigurator objConfig = null;
            XmlElement objRoot = null;
            StringBuilder sSQL = new StringBuilder();
            string sConnectionString = "";
            DbReader objDbReader = null;
            string sShortCode = "";
            string sCodeDesc = "";
            InitializeDataModel();

            //Manish Multicurrency
            string sCurrCode = string.Empty;
            string sCurrDesc = string.Empty;
            string sCurrency = string.Empty;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString,m_iClientId);

			//rkulavil - ML Changes - MITS 34107 -PageId and Lang ID added
			string sPageID = string.Empty;
            string sLangCode=string.Empty;
			
            m_objDataModelFactory = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword,m_iClientId);//sonali jira 880
            m_objLocalCache = m_objDataModelFactory.Context.LocalCache;
            objClaimantMmsea = (ClaimantMmsea)m_objDMF.GetDataModelObject("ClaimantMmsea", false);
           
            try
            {
                //Manish Multicurrency
                objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), ref sCurrCode, ref sCurrDesc);
                sCurrency = sCurrDesc.Split('|')[1];
                //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen
                using (DbConnection objDbConnection = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    object objExecuteQuery = null;
                    string sSql = string.Empty;
                    sSql = "SELECT CLAIMANT_ROW_ID FROM CLAIMANT_MMSEA WHERE CLAIMANT_ROW_ID=" + p_iclaimantrowid;
                    objDbConnection.Open();
                    objExecuteQuery = objDbConnection.ExecuteScalar(sSql);
                    if (objExecuteQuery != null)
                    {
                        objClaimantMmsea.MoveTo(p_iclaimantrowid);
                    }  
                }
                //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen

                objOutXML = new XmlDocument(); 
                objRoot = objOutXML.CreateElement("ClaimantMMSEA");
                objOutXML.AppendChild(objRoot);
                              
                //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen
                if (objClaimantMmsea.ClaimantRowID == 0)
                {
                    objClaimantMmsea.ClaimantRowID = p_iclaimantrowid;
                }
                //Added for Mits 19213:Claimant_MMSEA row created without saving on Claimant MMSEA Data screen
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("ClaimantRowID", objClaimantMmsea.ClaimantRowID.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("IsBenificaryCode", objClaimantMmsea.IsBenificaryCode), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("IsPaymentNonMedical", objClaimantMmsea.IsPaymentNonMedical), true));
                
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("HealthClaimNumber", objClaimantMmsea.HealthClaimNumber.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("ConfirmedDate", Conversion.GetDBDateFormat(objClaimantMmsea.ConfirmedDate.ToString(), "d")), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("ICDEventCode", objClaimantMmsea.ICDEventCode), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("ICD10EventCode", objClaimantMmsea.ICD10EventCode), true)); //MITS 32423 :Praveen ICD10 Changes 

                //pmittal5 Mits 18604 01/07/10 - Product Liability Indicator defaulted to 'No' value
                if (objClaimantMmsea.ProductLiabilityCode == 0)
                    objClaimantMmsea.ProductLiabilityCode = m_objLocalCache.GetCodeId("1", "MMSEA_PRO_LIA_CODE");
                //End - pmittal5
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("ProductLiabilityCode", objClaimantMmsea.ProductLiabilityCode), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("ProductGenericName", objClaimantMmsea.ProductGenericName.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("ProductBrandName", objClaimantMmsea.ProductBrandName.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("ProductManufacturer", objClaimantMmsea.ProductManufacturer.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("ProductHarmText", objClaimantMmsea.ProductHarmText.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("ResponsiblityForMedicalsCode", objClaimantMmsea.ResponsiblityForMedicalsCode), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("ResponsibilityTerminationDate", Conversion.GetDBDateFormat(objClaimantMmsea.ResponsibilityTerminationDate.ToString(), "d")), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("FirstExtractDate", objClaimantMmsea.FirstExtractDate.ToString()), true));

                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("LastExtractDate", Conversion.GetDBDateFormat(objClaimantMmsea.LastExtractDate.ToString(),"d")), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("MMSEAEditedFlag", objClaimantMmsea.MMSEAEditedFlag.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("MSPEffectiveDate", Conversion.GetDBDateFormat(objClaimantMmsea.MSPEffectiveDate.ToString(),"d")), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("MSPTerminationDate", Conversion.GetDBDateFormat(objClaimantMmsea.MSPTerminationDate.ToString(),"d")), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("MSPTypeIndicator", objClaimantMmsea.MSPTypeIndicator), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("DispositionCode", objClaimantMmsea.DispositionCode), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("AddedByUser", objClaimantMmsea.AddedByUser.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("DttmRcdLastUpd", objClaimantMmsea.DttmRcdLastUpd.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("UpdatedByUser", objClaimantMmsea.UpdatedByUser.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("DelMDCRERCDFlag", objClaimantMmsea.DelMDCRERCDFlag.ToString()), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("DelMDCRERCDDate", Conversion.GetDBDateFormat(objClaimantMmsea.DelMDCRERCDDate.ToString(),"d")), true));

                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("TPOCDeletedValues", ""), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("PartyDeletedValues", ""), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("SelectedId", ""), true));
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("IsNew", ""), true));

                //Added by Amitosh for Mits 24380 (03/16/2011)
				//Added by asingh263 for mits 30525
                int isCarrierClaimOn = objClaimantMmsea.Context.InternalSettings.SysSettings.MultiCovgPerClm;
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("IsCarrierClaimOn", isCarrierClaimOn.ToString()), true));
                if (isCarrierClaimOn.Equals(0))
                {
                    objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("NoFaultIndicator", objClaimantMmsea.NoFaultIndicator), true));
                    objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("NoFaultInsuranceLimit", objClaimantMmsea.NoFaultInsuranceLimit.ToString()), true));
                    objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("NoFaultExhaustDate", Conversion.GetDBDateFormat(objClaimantMmsea.NoFaultExhaustDate.ToString(), "d")), true));
                }
                else if (isCarrierClaimOn.Equals(-1))
                {
                    objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("CarrierNoFaultIndicator", objClaimantMmsea.NoFaultIndicator), true));
                    objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("CarrierNoFaultInsuranceLimit", objClaimantMmsea.NoFaultInsuranceLimit.ToString()), true));
                    objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("CarrierNoFaultExhaustDate", Conversion.GetDBDateFormat(objClaimantMmsea.NoFaultExhaustDate.ToString(), "d")), true));
                }
                    //Added by Kiran for Mits 24985 (02/22/2013)
                objRoot.AppendChild(objOutXML.ImportNode(GetCodeXml("ClaimantsRepresentativeCode", objClaimantMmsea.ClaimantRepresentativeCode), true));
                //end Kiran
                string ClaimantRepresentativeName = m_objLocalCache.GetEntityLastFirstName(objClaimantMmsea.ClaimantRepresentativeEID);
                if (ClaimantRepresentativeName.Trim().StartsWith(","))
                {
                    ClaimantRepresentativeName = ClaimantRepresentativeName.Replace(",", "");
                }
                XmlElement ClaimantRepresentativeNode = objOutXML.CreateElement("ClaimantRepresentativeName");
                XmlAttribute attribute = objOutXML.CreateAttribute("codeid");
                attribute.Value = objClaimantMmsea.ClaimantRepresentativeEID.ToString();
                ClaimantRepresentativeNode.InnerText = ClaimantRepresentativeName;
                ClaimantRepresentativeNode.Attributes.Append(attribute);
                objRoot.AppendChild(ClaimantRepresentativeNode);
                
                //end asingh263
                //end Amitosh

                //Manish Multicurrency
                objRoot.AppendChild(objOutXML.ImportNode(GetNewEleWithValue("BaseCurrencyType", sCurrency), true));
         
                //Asharma326 25824 Check permissions Start
                CheckPermission(ref objOutXML, p_objLogin);
                //Asharma326 25824 Check permissions Ends


                sConnectionString = m_sConnectionString;
                sSQL.Append("SELECT CDC_ICD_DIGNS_CODE FROM CT_MMS_X_DIAGNOSIS WHERE CLAIMANT_ROW_ID =" + p_iclaimantrowid);
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());

                objXMLElem = objOutXML.CreateElement("CSCDiagnosisList");
                string sDiagnosis = "";
                sDiagnosis = "";
                if (objDbReader != null)
                {
                    
                    while (objDbReader.Read())
                    {  
                        m_objLocalCache.GetCodeInfo(objDbReader.GetInt("CDC_ICD_DIGNS_CODE"), ref sShortCode, ref sCodeDesc);

                        sDiagnosis = sDiagnosis + "<Item value='" + objDbReader.GetInt("CDC_ICD_DIGNS_CODE").ToString() + "' codeid='" + objDbReader.GetInt("CDC_ICD_DIGNS_CODE").ToString() + "'><![CDATA[" + sShortCode + " " + sCodeDesc + "]]></Item>";
                     }
                    objDbReader.Close();
                }
                sDiagnosis = "<DiagnosisList  type='codelist'>" + sDiagnosis + "</DiagnosisList>";
                objXMLElem.InnerXml = sDiagnosis;
                objRoot.AppendChild(objXMLElem);

                 //MITS 32423 : Praveen  ICD10 changes Start

                string Sql = string.Empty;
                Sql ="SELECT CDC_ICD_DIGNS_CODE FROM CT_MMS_X_DIAGNOSIS_ICD10 WHERE CLAIMANT_ROW_ID =" + p_iclaimantrowid;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, Sql.ToString());
                objXMLElem = objOutXML.CreateElement("CSCICD10DiagnosisList");
                sDiagnosis = "";
                if (objDbReader != null)
                {

                    while (objDbReader.Read())
                    {
                        m_objLocalCache.GetCodeInfo(objDbReader.GetInt("CDC_ICD_DIGNS_CODE"), ref sShortCode, ref sCodeDesc);

                        sDiagnosis = sDiagnosis + "<Item value='" + objDbReader.GetInt("CDC_ICD_DIGNS_CODE").ToString() + "' codeid='" + objDbReader.GetInt("CDC_ICD_DIGNS_CODE").ToString() + "'><![CDATA[" + sShortCode + " " + sCodeDesc + "]]></Item>";
                    }
                    objDbReader.Close();
                }
                sDiagnosis = "<DiagnosisList  type='codelist'>" + sDiagnosis + "</DiagnosisList>";
                objXMLElem.InnerXml = sDiagnosis;
                objRoot.AppendChild(objXMLElem);

                 //MITS 32423 : Praveen  ICD10 changes End

                objXMLElem = objOutXML.CreateElement("TPOC");
                XmlElement objListHead = null;
                XmlElement objListOption = null;

				//rkulavil - ML Changes - MITS 34107 -start
                sPageID = p_sPageId;
                sLangCode = p_sLangCode;

                objListHead = objOutXML.CreateElement("listhead");
                
                //string sNode = "<DelayedBeyongTPOCStartDate>Funding Delayed Beyond TPOC Start Date</DelayedBeyongTPOCStartDate>";
                //sNode = sNode + "<PaymentObligationAmount>TPOC Amount</PaymentObligationAmount>";
                //sNode = sNode + "<DateofWrittenAgreement>Date Of Agreement</DateofWrittenAgreement>";
                //sNode = sNode + "<DateofCourtApproval>Date Of Court Approval</DateofCourtApproval>";
                //sNode = sNode + "<DateofPaymentIssue>Date Of Payment Issue</DateofPaymentIssue>";
                //sNode = sNode + "<DeletedFlag>Deleted</DeletedFlag>";
                //sNode = sNode + "<FilMdcreRcdDATE>Date Filed</FilMdcreRcdDATE>";

                string sNode = "<DelayedBeyongTPOCStartDate>"+ CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDelayedBeyongTPOCStartDate",0,sPageID,m_iClientId),sLangCode) +"</DelayedBeyongTPOCStartDate>";//sonali
                sNode = sNode + "<PaymentObligationAmount>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrPaymentObligationAmount", 0, sPageID,m_iClientId), sLangCode) + "</PaymentObligationAmount>";
                sNode = sNode + "<DateofWrittenAgreement>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDateofWrittenAgreement", 0, sPageID,m_iClientId), sLangCode) + "</DateofWrittenAgreement>";
                sNode = sNode + "<DateofCourtApproval>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDateofCourtApproval", 0, sPageID,m_iClientId), sLangCode) + "</DateofCourtApproval>";
                sNode = sNode + "<DateofPaymentIssue>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDateofPaymentIssue", 0, sPageID,m_iClientId), sLangCode) + "</DateofPaymentIssue>";
                sNode = sNode + "<DeletedFlag>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDeletedFlag", 0, sPageID,m_iClientId), sLangCode) + "</DeletedFlag>";
                sNode = sNode + "<FilMdcreRcdDATE>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvhdrFilMdcreRcdDATE", 0, sPageID,m_iClientId), sLangCode) + "</FilMdcreRcdDATE>";
				//rkulavil - ML Changes - MITS 34107 -end
                objListHead.InnerXml = sNode;
                objXMLElem.AppendChild(objListHead);

                foreach (ClmtMmseaTpoc objClmtMmseaTpoc in objClaimantMmsea.ClmtMmseaTpocList)
                {
                    objListOption = objOutXML.CreateElement("option");
                    sNode = "<DelayedBeyongTPOCStartDate>";
                    sNode = sNode + Conversion.GetDBDateFormat(objClmtMmseaTpoc.DelayedBeyongTPOCStartDate, "d") + "</DelayedBeyongTPOCStartDate>";
                    sNode = sNode + "<PaymentObligationAmount>$" + objClmtMmseaTpoc.PaymentObligationAmount.ToString() + "</PaymentObligationAmount>";
                    sNode = sNode + "<DateofWrittenAgreement>" +  Conversion.GetDBDateFormat(objClmtMmseaTpoc.DateofWrittenAgreement, "d") + "</DateofWrittenAgreement>";
                    sNode = sNode + "<DateofCourtApproval>" + Conversion.GetDBDateFormat(objClmtMmseaTpoc.DateofCourtApproval,"d") + "</DateofCourtApproval>";
                    sNode = sNode + "<DateofPaymentIssue>" + Conversion.GetDBDateFormat(objClmtMmseaTpoc.DateofPaymentIssue, "d") + "</DateofPaymentIssue>";

                    if (objClmtMmseaTpoc.DeletedFlag == 0)
                    {
                        sNode = sNode + "<DeletedFlag>No</DeletedFlag>";
                    }
                    else
                    {
                        sNode = sNode + "<DeletedFlag>Yes</DeletedFlag>";
                    }
                    sNode = sNode + "<FilMdcreRcdDATE>" +  Conversion.GetDBDateFormat(objClmtMmseaTpoc.FilMdcreRcdDATE, "d") + "</FilMdcreRcdDATE>";
                    sNode = sNode + "<TpocEditFlag>" + objClmtMmseaTpoc.TpocEditFlag.ToString() + "</TpocEditFlag>";
                    sNode = sNode + "<ClaimantRowID>" + objClmtMmseaTpoc.ClaimantRowID.ToString() + "</ClaimantRowID>";
                    sNode = sNode + "<TpocRowID>" + objClmtMmseaTpoc.TpocRowID.ToString() + "</TpocRowID>";
                    sNode = sNode + "<DTTM_RCD_LAST_UPD>" + objClmtMmseaTpoc.DttmRcdLastUpd.ToString() + "</DTTM_RCD_LAST_UPD>";
                    sNode = sNode + "<AddedByUser>" + objClmtMmseaTpoc.AddedByUser + "</AddedByUser>";
                    sNode = sNode + "<UpdatedByUser>" + objClmtMmseaTpoc.AddedByUser + "</UpdatedByUser>";
                    sNode = sNode + "<DttmRcdAdded>" + objClmtMmseaTpoc.DttmRcdAdded + "</DttmRcdAdded>";
                   
                    
                    objListOption.InnerXml = sNode;
                    objXMLElem.AppendChild(objListOption);
                 }
                //Inserting blank row for grid
                objListOption = objOutXML.CreateElement("option");
                 XmlAttribute objXmlAttribute = objOutXML.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objListOption.Attributes.Append(objXmlAttribute);

                sNode = "<DelayedBeyongTPOCStartDate></DelayedBeyongTPOCStartDate>";
                sNode = sNode + "<PaymentObligationAmount></PaymentObligationAmount>";
                sNode = sNode + "<DateofWrittenAgreement></DateofWrittenAgreement>";
                sNode = sNode + "<DateofCourtApproval></DateofCourtApproval>";
                sNode = sNode + "<DateofPaymentIssue></DateofPaymentIssue>";
                sNode = sNode + "<DeletedFlag></DeletedFlag>";
                sNode = sNode + "<FilMdcreRcdDATE></FilMdcreRcdDATE>";
                sNode = sNode + "<TpocEditFlag></TpocEditFlag>";
                sNode = sNode + "<ClaimantRowID></ClaimantRowID>";
                sNode = sNode + "<TpocRowID></TpocRowID>";
                sNode = sNode + "<DTTM_RCD_LAST_UPD></DTTM_RCD_LAST_UPD>";
                sNode = sNode + "<AddedByUser></AddedByUser>";
                sNode = sNode + "<UpdatedByUser></UpdatedByUser>";
                sNode = sNode + "<DttmRcdAdded></DttmRcdAdded>";    
                objListOption.InnerXml = sNode;
                objXMLElem.AppendChild(objListOption);

                objRoot.AppendChild(objXMLElem);


                //------------------MMSEA Parties to claim

                objXMLElem = objOutXML.CreateElement("MMSEAParty");
                objListHead = null;
                objListOption = null;

                objListHead = objOutXML.CreateElement("listhead");
                //Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
				//rkulavil - ML Changes - MITS 34107 -start
                //sNode = "<RelationToBenificiary>Relationship to Beneficiary</RelationToBenificiary>";
                //sNode = sNode + "<att1lastfirstname>Party Name</att1lastfirstname>";
                ////Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
                //sNode = sNode + "<TypeOfRepresentative>Type of Representative</TypeOfRepresentative>";
                //sNode = sNode + "<attlastfirstname>Party to Claim Representative</attlastfirstname>";

                sNode = "<RelationToBenificiary>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrRelationToBenificiary", 0, sPageID,m_iClientId), sLangCode) + "</RelationToBenificiary>";//sharishkumar Jira 830
                sNode = sNode + "<att1lastfirstname>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdratt1lastfirstname", 0, sPageID, m_iClientId), sLangCode) + "</att1lastfirstname>";//sharishkumar Jira 830
				//Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
                sNode = sNode + "<TypeOfRepresentative>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrTypeOfRepresentative", 0, sPageID, m_iClientId), sLangCode) + "</TypeOfRepresentative>";//sharishkumar Jira 830
                sNode = sNode + "<attlastfirstname>" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrattlastfirstname", 0, sPageID, m_iClientId), sLangCode) + "</attlastfirstname>";//sharishkumar Jira 830
        		//rkulavil - ML Changes - MITS 34107 -end     
                objListHead.InnerXml = sNode;
                objXMLElem.AppendChild(objListHead);
                string sName = string.Empty;
                string sRepresentativeCode = string.Empty;
                foreach (ClaimantMMSEAXParty objClmtMmseaParty in objClaimantMmsea.ClaimantMMSEAXPartyList)
                {
                    objListOption = objOutXML.CreateElement("option");
                    sName = m_objLocalCache.GetEntityLastFirstName(objClmtMmseaParty.PartyToClaimEntityEID);
                    if (sName.Trim().StartsWith(","))
                    {
                        sName = sName.Replace(",", "");
                    }
                    //Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
                    sNode = "<RelationToBenificiary codeid='" + objClmtMmseaParty.PartyToClaimRelationshipCode.ToString() + "'>" + m_objLocalCache.GetShortCode(objClmtMmseaParty.PartyToClaimRelationshipCode) + " " +m_objLocalCache.GetCodeDesc(objClmtMmseaParty.PartyToClaimRelationshipCode) + "</RelationToBenificiary>";
                    //rjhamb-Mits17059:Changed for apostrophe and ampersand 
                    //sNode = "<att1lastfirstname codeid='" + objClmtMmseaParty.PartyToClaimEntityEID + "'>" + sName + "</att1lastfirstname>";
                    sNode = sNode + "<att1lastfirstname codeid='" + objClmtMmseaParty.PartyToClaimEntityEID + "'><![CDATA[" + sName + "]]></att1lastfirstname>";
                    //rjhamb-Mits17059:Changed for apostrophe and ampersand
                   //Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
                    sRepresentativeCode = m_objLocalCache.GetShortCode(objClmtMmseaParty.PartyToClaimReprsesentativeTypeCode);
                    if (sRepresentativeCode.Trim() == "\" \"")
                    {
                        sRepresentativeCode = sRepresentativeCode.Replace("\" \"", "");
                    }
                    //sNode = sNode + "<TypeOfRepresentative codeid='" + objClmtMmseaParty.PartyToClaimReprsesentativeTypeCode.ToString() + "'>" + m_objLocalCache.GetCodeDesc(objClmtMmseaParty.PartyToClaimReprsesentativeTypeCode) + "</TypeOfRepresentative>";
                    sNode = sNode + "<TypeOfRepresentative codeid='" + objClmtMmseaParty.PartyToClaimReprsesentativeTypeCode.ToString() + "'>" + sRepresentativeCode + " " + m_objLocalCache.GetCodeDesc(objClmtMmseaParty.PartyToClaimReprsesentativeTypeCode) + "</TypeOfRepresentative>";

                   sName = m_objLocalCache.GetEntityLastFirstName(objClmtMmseaParty.PartyToClaimReprsesentativeEntityEID);
                   if (sName.Trim().StartsWith(","))
                   {
                       sName = sName.Replace(",", "");
                   }
                   //rjhamb-Mits17059:Changed for apostrophe and ampersand 
                   //sNode = sNode + "<attlastfirstname codeid='" + objClmtMmseaParty.PartyToClaimReprsesentativeEntityEID.ToString() + "'>" + sName + "</attlastfirstname>";
                   sNode = sNode + "<attlastfirstname codeid='" + objClmtMmseaParty.PartyToClaimReprsesentativeEntityEID.ToString() + "'><![CDATA[" + sName + "]]></attlastfirstname>";
                   //rjhamb-Mits17059:Changed for apostrophe and ampersand
 
                    sNode = sNode + "<ClaimantRowID>" + objClmtMmseaParty.ClaimantRowID.ToString() + "</ClaimantRowID>";
                    sNode = sNode + "<MMSEAClaimantRowID>" + objClmtMmseaParty.MMSEAClaimantRowID.ToString() + "</MMSEAClaimantRowID>";
                    sNode = sNode + "<DttmRcdAdded>" + objClmtMmseaParty.DttmRcdAdded.ToString() + "</DttmRcdAdded>";
                    sNode = sNode + "<DttmRcdLastUpd>" + objClmtMmseaParty.DttmRcdLastUpd.ToString() + "</DttmRcdLastUpd>";
                    sNode = sNode + "<AddedByUser>" + objClmtMmseaParty.AddedByUser + "</AddedByUser>";
                    sNode = sNode + "<UpdatedByUser>" + objClmtMmseaParty.AddedByUser + "</UpdatedByUser>";


                    objListOption.InnerXml = sNode;
                    objXMLElem.AppendChild(objListOption);
                }
                sNode = "";
                //Inserting blank row for grid
                objListOption = objOutXML.CreateElement("option");
                objXmlAttribute = objOutXML.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objListOption.Attributes.Append(objXmlAttribute);
                //Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
                sNode = sNode + "<RelationToBenificiary codeid='-1'></RelationToBenificiary>";
                sNode = sNode + "<att1lastfirstname codeid='-1'></att1lastfirstname>";
                //Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
                sNode = sNode + "<TypeOfRepresentative codeid='-1'></TypeOfRepresentative>";
                sNode = sNode + "<attlastfirstname codeid='-1'></attlastfirstname>";
                sNode = sNode + "<ClaimantRowID></ClaimantRowID>";
                sNode = sNode + "<MMSEAClaimantRowID></MMSEAClaimantRowID>";
                sNode = sNode + "<DttmRcdAdded></DttmRcdAdded>";
                sNode = sNode + "<DttmRcdLastUpd></DttmRcdLastUpd>";
                sNode = sNode + "<AddedByUser></AddedByUser>";
                sNode = sNode + "<UpdatedByUser></UpdatedByUser>";

                objListOption.InnerXml = sNode;
                objXMLElem.AppendChild(objListOption);

                objRoot.AppendChild(objXMLElem);

            }
            catch (RecordNotFoundException p_objException)
            {
                throw new InvalidValueException("ClaimantMmsea.GetClaimantMMSEAData.ClaimantNotFound", p_objException);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error in ClaimantMMSEA.GetClaimantMMSEAData", Ex.InnerException);

            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }
            return objOutXML;
        }


        public XmlDocument GetData(XmlDocument p_XmlDoc)
        {
            //Inserting blank row for grid

            XmlNode objTPOC = null;
            XmlNode objMMSEAParty = null;
            XmlElement objListOption = null;

            string sNode = string.Empty;



            objTPOC = p_XmlDoc.SelectSingleNode("//ClaimantMMSEA/TPOC");

            objListOption = p_XmlDoc.CreateElement("option");
            XmlAttribute objXmlAttribute = p_XmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objListOption.Attributes.Append(objXmlAttribute);

            sNode = "<DelayedBeyongTPOCStartDate></DelayedBeyongTPOCStartDate>";
            sNode = sNode + "<PaymentObligationAmount></PaymentObligationAmount>";
            sNode = sNode + "<DateofWrittenAgreement></DateofWrittenAgreement>";
            sNode = sNode + "<DateofCourtApproval></DateofCourtApproval>";
            sNode = sNode + "<DateofPaymentIssue></DateofPaymentIssue>";
            sNode = sNode + "<DeletedFlag></DeletedFlag>";
            sNode = sNode + "<FilMdcreRcdDATE></FilMdcreRcdDATE>";
            sNode = sNode + "<TpocEditFlag></TpocEditFlag>";
            sNode = sNode + "<ClaimantRowID></ClaimantRowID>";
            sNode = sNode + "<TpocRowID></TpocRowID>";
            sNode = sNode + "<DTTM_RCD_LAST_UPD></DTTM_RCD_LAST_UPD>";
            sNode = sNode + "<AddedByUser></AddedByUser>";
            sNode = sNode + "<UpdatedByUser></UpdatedByUser>";
            sNode = sNode + "<DttmRcdAdded></DttmRcdAdded>";
            objListOption.InnerXml = sNode;
            objTPOC.AppendChild(objListOption);

            sNode = "";
            //Inserting blank row for grid
            objMMSEAParty = p_XmlDoc.SelectSingleNode("//ClaimantMMSEA/MMSEAParty");


            objListOption = p_XmlDoc.CreateElement("option");
            objXmlAttribute = p_XmlDoc.CreateAttribute("type");
            objXmlAttribute.InnerText = "new";
            objListOption.Attributes.Append(objXmlAttribute);
            //Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
            sNode = sNode + "<RelationToBenificiary codeid='-1'></RelationToBenificiary>";
            sNode = sNode + "<att1lastfirstname codeid='-1'></att1lastfirstname>";
            //Changed order of Columns for Mits 17762:RelationToBeneficary is required before filling EntityName
            sNode = sNode + "<TypeOfRepresentative codeid='-1'></TypeOfRepresentative>";
            sNode = sNode + "<attlastfirstname codeid='-1'></attlastfirstname>";
            sNode = sNode + "<ClaimantRowID></ClaimantRowID>";
            sNode = sNode + "<MMSEAClaimantRowID></MMSEAClaimantRowID>";
            sNode = sNode + "<DttmRcdAdded></DttmRcdAdded>";
            sNode = sNode + "<DttmRcdLastUpd></DttmRcdLastUpd>";
            sNode = sNode + "<AddedByUser></AddedByUser>";
            sNode = sNode + "<UpdatedByUser></UpdatedByUser>";
            objListOption.InnerXml = sNode;
            objMMSEAParty.AppendChild(objListOption);


            XmlNode objXNode = p_XmlDoc.SelectSingleNode("//CSCDiagnosisList");
            XmlNodeList objNodeCollection = p_XmlDoc.SelectNodes("//CSCDiagnosisList/DiagnosisList/Item");
            XmlNode objEle = null;
            string sShortCode = "";
            string sCodeDes = "";
            sNode = "";

           
            foreach (XmlNode objXmlNode in objNodeCollection)
            {
               // objEle = objXmlNode.SelectSingleNode("Item");
                
                    if (objXmlNode.Attributes["codeid"].Value != "")
                    {
                        GetCode(Int32.Parse(objXmlNode.Attributes["codeid"].Value), ref sShortCode, ref sCodeDes);

                        sNode = sNode + "<Item value='" + objXmlNode.Attributes["codeid"].Value + "' codeid='" + objXmlNode.Attributes["codeid"].Value + "'><![CDATA[" + sShortCode + " " + sCodeDes + "]]></Item>";

                    }
               
            }
            sNode = "<DiagnosisList  type='codelist'>" + sNode + "</DiagnosisList>";
            objXNode.InnerXml = sNode;
            
                 //MITS 32423 : Praveen  ICD10 changes Start
          
            XmlNode objXNode2 = p_XmlDoc.SelectSingleNode("//CSCICD10DiagnosisList");
            XmlNodeList objNodeCollection2 = p_XmlDoc.SelectNodes("//CSCICD10DiagnosisList/DiagnosisList/Item");
            sShortCode = "";
            sCodeDes = "";
            sNode = "";


            foreach (XmlNode objXmlNode in objNodeCollection2)
            {

                if (objXmlNode.Attributes["codeid"].Value != "")
                {
                    GetCode(Int32.Parse(objXmlNode.Attributes["codeid"].Value), ref sShortCode, ref sCodeDes);

                    sNode = sNode + "<Item value='" + objXmlNode.Attributes["codeid"].Value + "' codeid='" + objXmlNode.Attributes["codeid"].Value + "'><![CDATA[" + sShortCode + " " + sCodeDes + "]]></Item>";

                }

            }
            sNode = "<DiagnosisList type='codelist'>" + sNode + "</DiagnosisList>";
            objXNode2.InnerXml = sNode;
                 //MITS 32423 : Praveen  ICD10 changes End
            return p_XmlDoc;
        }



        public void GetCode(int p_iCode, ref string sShortCode, ref string sCodeDesc)
        {
            m_objDataModelFactory = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword,m_iClientId);//sonali
            m_objLocalCache = m_objDataModelFactory.Context.LocalCache;
            m_objLocalCache.GetCodeInfo(p_iCode, ref sShortCode, ref sCodeDesc);
        }

        private XmlElement GetCodeXml(string p_sNewNodeName, int p_iCode)
        {
            XmlDocument objXmDoc;
            XmlElement objXMLElement;
            objXmDoc = new XmlDocument();
            string sShortCode = "";
            string sCodeDesc = "";
            objXMLElement = objXmDoc.CreateElement(p_sNewNodeName);

            m_objLocalCache.GetCodeInfo(p_iCode, ref sShortCode, ref sCodeDesc);
            objXMLElement.SetAttribute("codeid", p_iCode.ToString());
            objXMLElement.SetAttribute("shortcode", sShortCode);
            objXMLElement.SetAttribute("codetable", m_objLocalCache.GetTableName(p_iCode));
            //sgoel6 MITS 16899
            //objXMLElement.InnerXml = sShortCode.Replace("'", "&amp;") + " " + sCodeDesc.Replace("'", "&amp;");
            objXMLElement.InnerText = sShortCode + " " + sCodeDesc;

            return objXMLElement;           
        }

        public static XmlElement GetNewEleWithValue(string p_sNewNodeName, string p_sNodeValue)
        {
            XmlDocument objXDoc;
            XmlElement objXMLElement;
            objXDoc = new XmlDocument();
            objXMLElement = objXDoc.CreateElement(p_sNewNodeName);
            objXMLElement.InnerText = p_sNodeValue;
            objXDoc = null;
            return objXMLElement;
        }


        private void InitializeDataModel()
        {
            if (m_objDMF == null)
                m_objDMF = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword,m_iClientId);//sonali jira 880

            m_sConnectionString = m_objDMF.Context.DbConn.ConnectionString;
        }

        private void CleanDataModel()
        {
            if (m_objDMF != null)
            {
                m_objDMF.UnInitialize();
                m_objDMF.Dispose();
            }
            m_objDMF = null;
        }
        
        #endregion

    }
}
