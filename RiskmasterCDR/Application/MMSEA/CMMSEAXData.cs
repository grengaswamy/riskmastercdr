﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.DataModel;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;

namespace Riskmaster.Application.MMSEA
{
    public class CMMSEAXDATA
    {
        #region Member Variables
        /// <summary>
        /// DSN
        /// </summary>
        private string m_sDatabaseName = "";

        /// <summary>
        /// User Id
        /// </summary>
        private string m_sUserName = "";

        /// <summary>
        /// Password of the user
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// DataModelFactory object
        /// </summary>
        DataModelFactory m_objDMF = null;
        const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData";

        private LocalCache objCache = null;
        private Entity objEntity =null;
        private int m_iClientId = 0; //rkaur27
    #endregion

          #region Constructor
        public CMMSEAXDATA(string p_sDatabaseName, string p_sUserName, string p_sPassword, int p_iClientId)//rkaur27
		{
			m_sDatabaseName = p_sDatabaseName;
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
            m_iClientId = p_iClientId; //rkaur27

        }

        #endregion

         #region Destructor
        ~CMMSEAXDATA()
        {
            Dispose();
        }
        #endregion

        #region IDisposable Method
       
        /// <summary>
        ///		This method will be called by the caller of this library to nullify class level object.		
        /// </summary>				
        public void Dispose()
        {
            if (m_objDMF != null)
            {
                m_objDMF.UnInitialize();
                m_objDMF.Dispose();
            }


        }

        #endregion

        public XmlDocument GetMMSEAXDataList(int p_iEntityId,string sConnectionString)
        {

            XmlDocument CMMSEAXDataDoc = null;
            try
            {
                m_objDMF = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword, m_iClientId);//rkaur27
                objEntity = (Entity)m_objDMF.GetDataModelObject("Entity", false);
                if (p_iEntityId > 0)
                    objEntity.MoveTo(p_iEntityId);
                else
                    throw new RMAppException("MMSEA.EntityId.Invalid");

                
                CMMSEAXDataDoc = new XmlDocument();
                objCache = new LocalCache(sConnectionString,m_iClientId);

                XmlElement objRootElement = CMMSEAXDataDoc.CreateElement("CMMSEAXData");
                CMMSEAXDataDoc.AppendChild(objRootElement);

                XmlElement objOptionXmlElement = null;
                XmlElement objXmlElement = null;
                XmlElement objListHeadXmlElement = null;
                XmlAttribute objXmlAttribute = null;
                int iCMMSEAXDataCount = 0;
                string sCodeType = "";
                string sCodeDesc = "";

                objListHeadXmlElement = CMMSEAXDataDoc.CreateElement("listhead");

                objXmlElement = CMMSEAXDataDoc.CreateElement("LobCode");
                objXmlElement.InnerText = "Line Of Business";
                objListHeadXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC
                objXmlElement = CMMSEAXDataDoc.CreateElement("SecondLobCode");
                objXmlElement.InnerText = "Second Line Of Business";
                objListHeadXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC

                objXmlElement = CMMSEAXDataDoc.CreateElement("ReporterId");
                objXmlElement.InnerText = "Reporter ID";
                objListHeadXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("SiteId");
                objXmlElement.InnerText = "Office Code/Site ID";
                objListHeadXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("ScheduleCode");
                objXmlElement.InnerText = "Medicare Filing Schedule";
                objListHeadXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objRootElement.AppendChild(objListHeadXmlElement);
                objListHeadXmlElement = null;

                foreach (CMMSEAXData objCMMSEAXData in objEntity.CMMSEAXDataList)
                {

                    iCMMSEAXDataCount++;

                    objOptionXmlElement = CMMSEAXDataDoc.CreateElement("option");

                    objXmlAttribute = CMMSEAXDataDoc.CreateAttribute("ref");
                    objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                        + "/" + CMMSEAXDataDoc.DocumentElement.LocalName
                        + "/option[" + iCMMSEAXDataCount.ToString() + "]";
                    objOptionXmlElement.Attributes.Append(objXmlAttribute);
                    objXmlAttribute = null;

                    //objXmlElement = CMMSEAXDataDoc.CreateElement("LobCode");
                    ////this.objCache.GetCodeInfo(objCMMSEAXData.LineOfBusCode, ref sCodeType, ref sCodeDesc);
                    ////objXmlElement.InnerText = sCodeType + " " + sCodeDesc;
                    //objXmlAttribute = CMMSEAXDataDoc.CreateAttribute("codeid");
                    //objXmlAttribute.InnerText = objCMMSEAXData.LineOfBusCode.ToString();
                    //objXmlElement.Attributes.Append(objXmlAttribute);
                    //objOptionXmlElement.AppendChild(objXmlElement);
                    //objXmlAttribute = null;
                    //objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("LobCode");
                    sCodeDesc = GetCodeInfo(objCMMSEAXData.LineOfBusCode);
                    objXmlElement.InnerText = sCodeDesc;
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC
                    objXmlElement = CMMSEAXDataDoc.CreateElement("SecondLobCode");
                    sCodeDesc = GetCodeInfo(objCMMSEAXData.SecondLobCode);
                    objXmlElement.InnerText = sCodeDesc;
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;
                    //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC

                    objXmlElement = CMMSEAXDataDoc.CreateElement("ReporterId");
                    objXmlElement.InnerText = objCMMSEAXData.MMSEAReporterID.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("SiteId");
                    objXmlElement.InnerText = objCMMSEAXData.MMSEAOfficeSiteID.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("ScheduleCode");
                    this.objCache.GetCodeInfo(objCMMSEAXData.ScheduleCode, ref sCodeType, ref sCodeDesc);
                    objXmlElement.InnerText = sCodeType + " " + sCodeDesc;
                    objXmlAttribute = CMMSEAXDataDoc.CreateAttribute("codeid");
                    objXmlAttribute.InnerText = objCMMSEAXData.ScheduleCode.ToString();
                    objXmlElement.Attributes.Append(objXmlAttribute);
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlAttribute = null;
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("EntMMSEARowID");
                    objXmlElement.InnerText = objCMMSEAXData.EntMMSEARowID.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("EntityEID");
                    objXmlElement.InnerText = objCMMSEAXData.EntityEID.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("DttmRcdAdded");
                    objXmlElement.InnerText = objCMMSEAXData.DttmRcdAdded.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("DttmRcdLastUpd");
                    objXmlElement.InnerText = objCMMSEAXData.DttmRcdLastUpd.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("UpdatedByUser");
                    objXmlElement.InnerText = objCMMSEAXData.UpdatedByUser.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = CMMSEAXDataDoc.CreateElement("AddedByUser");
                    objXmlElement.InnerText = objCMMSEAXData.AddedByUser.ToString();
                    objOptionXmlElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objRootElement.AppendChild(objOptionXmlElement);
                    objOptionXmlElement = null;

                }

                iCMMSEAXDataCount++;

                objOptionXmlElement = CMMSEAXDataDoc.CreateElement("option");

                objXmlAttribute = CMMSEAXDataDoc.CreateAttribute("ref");
                objXmlAttribute.InnerText = INSTANCE_SYSEXDATA_PATH
                    + "/" + CMMSEAXDataDoc.DocumentElement.LocalName
                    + "/option[" + iCMMSEAXDataCount.ToString() + "]";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                objXmlAttribute = CMMSEAXDataDoc.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                objXmlAttribute = null;

                //objXmlElement = CMMSEAXDataDoc.CreateElement("LobCode");
                //objXmlAttribute = CMMSEAXDataDoc.CreateAttribute("codeid");
                //objXmlAttribute.InnerText = "-1";
                //objXmlElement.Attributes.Append(objXmlAttribute);
                //objOptionXmlElement.AppendChild(objXmlElement);
                //objXmlAttribute = null;
                //objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("LobCode");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC
                objXmlElement = CMMSEAXDataDoc.CreateElement("SecondLobCode");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;
                //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC

                objXmlElement = CMMSEAXDataDoc.CreateElement("ReporterId");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("SiteId");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("ScheduleCode");
                objXmlAttribute = CMMSEAXDataDoc.CreateAttribute("codeid");
                objXmlAttribute.InnerText = "-1";
                objXmlElement.Attributes.Append(objXmlAttribute);
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlAttribute = null;
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("EntMMSEARowID");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("EntityEID");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("DttmRcdAdded");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("DttmRcdLastUpd");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("UpdatedByUser");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;

                objXmlElement = CMMSEAXDataDoc.CreateElement("AddedByUser");
                objOptionXmlElement.AppendChild(objXmlElement);
                objXmlElement = null;


                objRootElement.AppendChild(objOptionXmlElement);
                objOptionXmlElement = null;

                objRootElement = null;

                return CMMSEAXDataDoc;

            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    ("MMSEA.GetMMSEAData.GeneralError", p_objException);
            }
            finally
            {
                if (m_objDMF != null)
                {
                    m_objDMF.Dispose();
                }
                if (objEntity != null)
                {
                    objEntity.Dispose();
                }

            }
            
        }
        
        public bool SaveMMSEAXData(string p_sInputXml)
        {
            bool bReturnValue = false;
            XmlDocument objXML = new XmlDocument();
            string sRowId = string.Empty;
            CMMSEAXData objCMMSEAXData = null;
            int iEntityId;
            string sOfficeId = string.Empty;
            int iLob = 0;
            string sLob = string.Empty;
            //sgoel6 Medicare
            string sSql = string.Empty;
            int iCount = 0;
            //Added Mits 20818-Start
            bool bSuccess = false;
            int iSecondLobCode = 0; 
            int iLINESECode = 0;
            int iLobCode=0;
            int iMMSEARowId=0;
            //Added Mits 20818-End
            string sLangCode = string.Empty;
            string sPageId = string.Empty;

            try
            {
                m_objDMF = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword, m_iClientId);//rkaur27
                objXML.LoadXml(p_sInputXml);
                sRowId = objXML.SelectSingleNode("//EntMMSEARowID").InnerText;
                iEntityId = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//EntityEID").InnerText);

                int iRowId = Conversion.ConvertStrToInteger(sRowId);
                objCMMSEAXData = (CMMSEAXData)m_objDMF.GetDataModelObject("CMMSEAXData", false);
                iLob = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//LobCode").InnerText);
                objEntity = (Entity)m_objDMF.GetDataModelObject("Entity", false);

                iSecondLobCode = Conversion.CastToType<int>(objXML.SelectSingleNode("//SecondLobCode").InnerText, out bSuccess);
                //sgoel6 Medicare Client Information
                sLangCode = objXML.SelectSingleNode("//LangCode").InnerText;
                sPageId = objXML.SelectSingleNode("//PageId").InnerText;
                //Get the count
                sSql = "SELECT COUNT(*) FROM ENTITY_X_MMSEA WHERE ENTITY_ID = " + iEntityId;
                iCount = Conversion.ConvertObjToInt(m_objDMF.Context.DbConn.ExecuteScalar(sSql), m_iClientId);                
                //Get the data
                sSql = string.Empty;
                sSql = "SELECT * FROM ENTITY_X_MMSEA WHERE ENTITY_ID = " + iEntityId;
                using (DbReader drEntXMMSEA = m_objDMF.Context.DbConn.ExecuteReader(sSql))
                {
                    while (drEntXMMSEA.Read())
                    {
                        iLobCode=Conversion.ConvertObjToInt(drEntXMMSEA["LINE_OF_BUS_CODE"], m_iClientId);
                        iLINESECode = Conversion.ConvertObjToInt(drEntXMMSEA["SECOND_LOB_CODE"], m_iClientId);
                        iMMSEARowId = Conversion.ConvertObjToInt(drEntXMMSEA["ENT_MMSEA_ROW_ID"], m_iClientId);
                        if (iLob != -3)
                        {
                            if (iLINESECode == 0) //Added If clause for Mits 20818
                            {
                                if (iLobCode == -3 && iRowId < 0) //If it is  All then no new record for other Lob's can be created.
                                {
                                    //throw new RMAppException("Line of Business already used for this organization Entity.");
                                    throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorLOBAlreadyUsed", 3, sPageId, m_iClientId),sLangCode));
                                }
                                else if (iLobCode == -3 && iRowId > 0)//If it is All then it can be edited to some other Lob.In this case Count would be 1.
                                {
                                }
                                //rjhamb Mits 20818-Start
                                //else
                                //{
                                //    if (iLobCode == iLob && iRowId != iMMSEARowId) //If it is not All then checking for records of other Lob
                                //        throw new RMAppException("Line of Business already used for this organization Entity.");
                                //}
                                else if (iRowId != iMMSEARowId)
                                {
                                    if (iLobCode == iLob)
                                    {
                                        //throw new RMAppException("Line of Business already used for this organization Entity.");
                                        throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorLOBAlreadyUsed", 3, sPageId, m_iClientId), sLangCode));
                                    }
                                    else if (iSecondLobCode != 0 && iLobCode == iSecondLobCode)
                                    {
                                        //throw new RMAppException("Second Line of Business already used for this organization Entity.");
                                        throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorSecondLOBAlreadyUsed", 3, sPageId, m_iClientId),sLangCode));
                                    }
                                }
                                //rjhamb Mits 20818-End
                            }
                            else //Mits 20818:Second Line of Business is added.Need to consider this only when iLob !=-3
                            {
                                if (iRowId != iMMSEARowId && (iLobCode == iLob || iLINESECode == iLob || iLobCode == iSecondLobCode || iLINESECode == iSecondLobCode))
                                {
                                    if((iLobCode == iLob ) || (iLINESECode == iLob))
                                    {
                                        //throw new RMAppException("Line of Business already used for this organization Entity.");
                                        throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorLOBAlreadyUsed", 3, sPageId, m_iClientId), sLangCode));
                                    }
                                    else  
                                    {
                                        //throw new RMAppException("Second Line of Business already used for this organization Entity.");
                                        throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorSecondLOBAlreadyUsed", 3, sPageId, m_iClientId), sLangCode));
                                    }
                                }
                            }
                        }
                        else //If iLob==3,Second Line of Business can't exists.
                        {
                            if (iLobCode != -3 && iRowId < 0) //Any other Lob value exists..so we can't create a new record with All.
                               // throw new RMAppException("Line of Business already used for this organization Entity.");
                                throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorLOBAlreadyUsed", 3, sPageId, m_iClientId), sLangCode));
                            else if (iLobCode != -3 && iRowId > 0 && iCount == 1)//If it is any other lob then it can be edited to All.
                            {
                            }
                            else if (iLobCode != -3 && iRowId > 0 && iCount > 1)
                            {
                                //throw new RMAppException("Line of Business already used for this organization Entity."); //Record for more than one Lob exists so can't be set to All
                                throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorLOBAlreadyUsed", 3, sPageId, m_iClientId), sLangCode));
                            }
                            else
                            {
                                if (iLobCode == iLob && iRowId != iMMSEARowId)
                                    //throw new RMAppException("Line of Business already used for this organization Entity."); //Record for All already exists
                                    throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("ErrorLOBAlreadyUsed", 3, sPageId, m_iClientId), sLangCode));
                            }
                        }
                    }
                }             

                //Commented by sgoel6 to handle the condition when Entity ID is -2
                //    if (iEntityId > 0)
                //        objEntity.MoveTo(iEntityId);

                //    if (objEntity.CMMSEAXDataList.Count != 0)
                //    {
                //        foreach (CMMSEAXData CMMSEAXData in objEntity.CMMSEAXDataList)
                //        {
                //            if (iLob != -3)
                //            {
                //                if (CMMSEAXData.LineOfBusCode == -3 && iRowId < 0) //If it is  All then no new record for other Lob's can be created.
                //                {
                //                    throw new RMAppException("Line of Business already used for this organization Entity.");
                //                }
                //                else if (CMMSEAXData.LineOfBusCode == -3 && iRowId > 0)//If it is All then it can be edited to some other Lob.In this case Count would be 1.
                //                {
                //                }
                //                else
                //                {
                //                    if (CMMSEAXData.LineOfBusCode == iLob && iRowId != CMMSEAXData.EntMMSEARowID) //If it is not All then checking for records of other Lob
                //                        throw new RMAppException("Line of Business already used for this organization Entity.");
                //                }
                //            }
                //            else
                //            {
                //                if (CMMSEAXData.LineOfBusCode != -3 && iRowId < 0) //Any other Lob value exists..so we can't create a new record with All.
                //                    throw new RMAppException("Line of Business already used for this organization Entity.");
                //                else if (CMMSEAXData.LineOfBusCode != -3 && iRowId > 0 && objEntity.CMMSEAXDataList.Count == 1)//If it is any other lob then it can be edited to All.
                //                {
                //                }
                //                else if (CMMSEAXData.LineOfBusCode != -3 && iRowId > 0 && objEntity.CMMSEAXDataList.Count > 1)
                //                {
                //                    throw new RMAppException("Line of Business already used for this organization Entity."); //Record for All already exists
                //                }
                //                else
                //                {
                //                    if (CMMSEAXData.LineOfBusCode == iLob && iRowId != CMMSEAXData.EntMMSEARowID)
                //                        throw new RMAppException("Line of Business already used for this organization Entity."); //Record for All already exists
                //                }
                //            }
                //        }
                //    }
            
                if (iRowId > 0)
                {
                    objCMMSEAXData.MoveTo(iRowId);
                    sOfficeId = objXML.SelectSingleNode("//SiteId").InnerText;
                    if (objCMMSEAXData.MMSEAOfficeSiteID != sOfficeId)
                        objCMMSEAXData.MMSEATINEditFlag = -1; //Flag would be -1 if New record is added or OfficeId is changed.
                }
                else
                {
                    objCMMSEAXData.MMSEATINEditFlag = -1; //Flag would be -1 if New record is added or OfficeId is changed.
                }
                objCMMSEAXData.EntMMSEARowID = iRowId;
                objCMMSEAXData.EntityEID = iEntityId;
                objCMMSEAXData.LineOfBusCode = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//LobCode").InnerText);
                //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC
                objCMMSEAXData.SecondLobCode = Conversion.CastToType<Int32>(objXML.SelectSingleNode("//SecondLobCode").InnerText,out bSuccess);
                //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC
                objCMMSEAXData.MMSEAReporterID = objXML.SelectSingleNode("//ReporterId").InnerText;
                objCMMSEAXData.MMSEAOfficeSiteID = objXML.SelectSingleNode("//SiteId").InnerText;
                objCMMSEAXData.ScheduleCode = Conversion.ConvertStrToInteger(objXML.SelectSingleNode("//ScheduleCode").Attributes["codeid"].Value);
                //objCMMSEAXData.MMSEATINEditFlag = -1; 
                objCMMSEAXData.DttmRcdAdded = objXML.SelectSingleNode("//DttmRcdAdded").InnerText;
                objCMMSEAXData.DttmRcdLastUpd = objXML.SelectSingleNode("//DttmRcdLastUpd").InnerText;
                objCMMSEAXData.UpdatedByUser = objXML.SelectSingleNode("//UpdatedByUser").InnerText;
                objCMMSEAXData.AddedByUser = objXML.SelectSingleNode("//AddedByUser").InnerText;

                objCMMSEAXData.Save();

                bReturnValue = true;

            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("MMSEA.Save.Error", p_objEx);
            }
            finally
            {
                if (objCMMSEAXData != null)
                {
                    objCMMSEAXData.Dispose();
                }


            }
            return bReturnValue;
        }
        public bool DeleteMMSEAXData(int iRowId)
        {
            bool bReturnValue = false;
            XmlDocument objXML = null;
            CMMSEAXData objCMMSEAXData = null;
            try
            {
                objXML = new XmlDocument();
                m_objDMF = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword, m_iClientId);
                objCMMSEAXData = (CMMSEAXData)m_objDMF.GetDataModelObject("CMMSEAXData", false);
                if (iRowId > 0)
                    objCMMSEAXData.MoveTo(iRowId);
                else
                    throw new RMAppException(
                        "MMSEA.EntityId.Invalid");

                objCMMSEAXData.Delete();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("MMSEA.Delete.GeneralError", p_objEx);
            }
            finally
            {
                if (objCMMSEAXData != null)
                {
                    objCMMSEAXData.Dispose();
                }


            }
            return bReturnValue;
        }
        public XmlDocument GetMMSEAXData(int iRowId)
        {
            XmlDocument objXML = null;
            XmlElement objRoot = null;
             XmlElement objRootSub = null;
            CMMSEAXData objCMMSEAXData = null;
            try
            {

                objXML = new XmlDocument();
                m_objDMF = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword, m_iClientId);
                objCMMSEAXData = (CMMSEAXData)m_objDMF.GetDataModelObject("CMMSEAXData", false);
                if (iRowId > 0)
                    objCMMSEAXData.MoveTo(iRowId);
                else
                    throw new RMAppException(Globalization.GetString
                        ("MMSEA.EntityId.Invalid", m_iClientId));

                    objRoot = objXML.CreateElement("MMSEAXDataDetails");
                    objXML.AppendChild(objRoot);
                    objRootSub=objXML.CreateElement("MMSEAXData");
                      

                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("EntMMSEARowID", objCMMSEAXData.EntMMSEARowID.ToString()), true));
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("EntityEID", objCMMSEAXData.EntityEID.ToString()), true));
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("LobCode", objCMMSEAXData.LineOfBusCode.ToString()), true));
                    //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("SecondLobCode", objCMMSEAXData.SecondLobCode.ToString()), true));
                    //Added rjhamb Mits 20818:MMSEA Claim Input job not extracting claim records for VA/GC
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("ReporterId", objCMMSEAXData.MMSEAReporterID.ToString()), true));
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("SiteId", objCMMSEAXData.MMSEAOfficeSiteID.ToString()), true));
                    //objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("ScheduleCode", objCMMSEAXData.ScheduleCode.ToString()), true));
                    objRootSub.AppendChild(objXML.ImportNode(GetCodeXml("ScheduleCode", objCMMSEAXData.ScheduleCode), true));
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("AddedByUser", objCMMSEAXData.AddedByUser.ToString()), true));
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("DttmRcdLastUpd", objCMMSEAXData.DttmRcdLastUpd.ToString()), true));
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("DttmRcdLastUpd", objCMMSEAXData.DttmRcdAdded.ToString()), true));
                    objRootSub.AppendChild(objXML.ImportNode(ClaimantMMSEA.GetNewEleWithValue("UpdatedByUser", objCMMSEAXData.UpdatedByUser.ToString()), true));
                    objRoot.AppendChild(objRootSub);  
               
                return objXML;

            }
            catch (RMAppException p_objExp)
            {
                throw p_objExp;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException
                    ("MMSEA.GetMMSEAData.GeneralError", p_objException);
            }
            finally
            {
                if (m_objDMF != null)
                {
                    m_objDMF.Dispose();
                }
                if (objCMMSEAXData != null)
                {
                    objCMMSEAXData.Dispose();
                }

            }

        }
        private string GetCodeInfo(int sLobCode)
        {
            switch (sLobCode)
            {
                case 0: //Added Mits 20818
                    return "";
                case 241:
                    return "GC  General Claims";
                case 242:
                    return "VA  Vehicle Accident Claims";
                case 243:
                    return "WC  Workers' Compensation";
                default:
                    return "All";
  
            }
        }
        private XmlElement GetCodeXml(string p_sNewNodeName, int p_iCode)
        {
            XmlDocument objXmDoc;
            XmlElement objXMLElement;
            objXmDoc = new XmlDocument();
            string sCodeDesc = "";
            string sShortCode = "";

            m_objDMF = new DataModelFactory(m_sDatabaseName, m_sUserName, m_sPassword, m_iClientId);
            objCache = m_objDMF.Context.LocalCache;
            objXMLElement = objXmDoc.CreateElement(p_sNewNodeName);

            objCache.GetCodeInfo(p_iCode, ref sShortCode, ref sCodeDesc);
            objXMLElement.SetAttribute("codeid", p_iCode.ToString());
            objXMLElement.SetAttribute("shortcode", sShortCode);
            objXMLElement.InnerXml = sShortCode + " " + sCodeDesc;

            return objXMLElement;
        }

    }
}
