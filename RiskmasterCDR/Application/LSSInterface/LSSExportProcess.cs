﻿
using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;

namespace Riskmaster.Application.LSSInterface
{
	/**************************************************************
	 * $File		: LSSExportProcess.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 22/09/2005
	 * $Author		: Parag Sarin/Manoj Agrawal
	 * $Comment		: LSSExportProcess class has functions to export Claim, Code, Admin, Payment Status data from RMX to LSS
	 * $Source		:  	
	 ************************************************************
	 * Amendment History
	 * ************************************************************
	 * Date Amended		*	Author		*	Amendment   
	 * 07/31/2006			Manoj			Added Code Export, Admin Export, Payment Status Export.
	 *										Changed existing Claim Export and included other new Export interfaces.
	 *										Changed as per latest design for TASB.
	 *										Names have been changed for class, functions, files.
	 **************************************************************/
	/// <summary>
	/// LSSExportProcess class has functions to export Claims, Admin, Code, Payment Status from RMX to LSS
	/// </summary>


	//Manoj - Changed name and structure to include other interfaces
	internal class LSSExportProcess:IExportProcess
	{
		#region Variables Declaration
      
        /// <summary>
        /// Gets and sets the Riskmaster Database Connection String
        /// </summary>
        public string RiskmasterConnectionString
        {
            get;
            set;
        } // property RiskmasterConnectionString


		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Password
		/// </summary>
		private string m_sPassword="";
		/// <summary>
		/// DSN Name
		/// </summary>
		private string m_sDsnName="";

        internal string m_strAppFilesPath = string.Empty;

        /// <summary>
        /// Gets and sets the AppFiles Path
        /// </summary>
        internal string AppFilesPath
        {
            get{return m_strAppFilesPath;}
            set{m_strAppFilesPath = value;}
        } // property AppFilesPath

        UserLogin UserLoginInstance
        {
            get;
            set;
        }//property UserLoginInstance


        string sAdminExportXmlPath = string.Empty;
        string sCodeExportXmlPath = string.Empty;
        string sPaymentExportXmlPath = string.Empty;
        string sClaimExportXmlPath = string.Empty;
        string sPayeeExportXmlPath = string.Empty;  //Debabrata Biswas LSS Payee data Exchange MITS# 20073
        string sCheckExportXmlPath= string.Empty;   //Debabrata Biswas LSS Multidata exchange MITS# 20036
        string sReserveExportXmlPath = string.Empty;//Added by sharishkumar for Mits 35472

        //Start rsushilaggar MITS 21984 Date 14-09-2010
        //This variable is declared because LSS doesn't support large msg. So This variable will be used to truncate the large event desc. 
        int EventDescLen = 1000; 
        //End rsushilaggar 

        private int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		/// Overloaded Class constructor
		/// </summary>
        /// 
		/// <param name="p_sUserName">string containing the user name to log into the Riskmaster security database</param>
		/// <param name="p_sPassword">string containing the password to log into the Riskmaster security database</param>
		/// <param name="p_sDsnName">string containing the DSN Name selection</param>
        public LSSExportProcess(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
		{
            m_iClientId = p_iClientId;
		    m_sUserName=p_sUserName;
		    m_sPassword=p_sPassword;
		    m_sDsnName=p_sDsnName;

            //Initialize the LSS Path values
            InitializeLSSValues();

            //Set the connection string for the Riskmaster Database
            this.RiskmasterConnectionString = CommonFunctions.GetRiskmasterConnectionString(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
            this.UserLoginInstance = CommonFunctions.GetUserLoginInstance(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);

		}
		#endregion

        /// <summary>
        /// 
        /// </summary>
        //<ClaimExportXml>%AppFilesPath%\LSSInterface\ClaimExport.xml</ClaimExportXml>
        //<AdminExportXml>%AppFilesPath%\LSSInterface\AdminExport.xml</AdminExportXml>
        //<CodeExportXml>%AppFilesPath%\LSSInterface\CodeExport.xml</CodeExportXml>
        //<PaymentStatusExportXml>%AppFilesPath%\LSSInterface\PaymentStatusExport.xml</PaymentStatusExportXml>
        //<AdminResponse>%AppFilesPath%\LSSInterface\AdminResponse.xml</AdminResponse>
        //<CodeResponse>%AppFilesPath%\LSSInterface\CodeResponse.xml</CodeResponse>
        //<DiaryResponse>%AppFilesPath%\LSSInterface\DiaryResponse.xml</DiaryResponse>
        //<DocumentResponse>%AppFilesPath%\LSSInterface\DocumentResponse.xml</DocumentResponse>
        //<MatterResponse>%AppFilesPath%\LSSInterface\MatterResponse.xml</MatterResponse>
        //<PaymentResponse>%AppFilesPath%\LSSInterface\PaymentResponse.xml</PaymentResponse>
        public void InitializeLSSValues()
        {
            this.AppFilesPath = RMConfigurator.AppFilesPath;
            sAdminExportXmlPath = Path.Combine(this.AppFilesPath, @"LSSInterface\AdminExport.xml");
            sPaymentExportXmlPath = Path.Combine(this.AppFilesPath, @"LSSInterface\PaymentStatusExport.xml");
            sCodeExportXmlPath = Path.Combine(this.AppFilesPath, @"LSSInterface\CodeExport.xml");
            sClaimExportXmlPath = Path.Combine(this.AppFilesPath, @"LSSInterface\ClaimExport.xml");
            sReserveExportXmlPath = Path.Combine(this.AppFilesPath, @"LSSInterface\ReserveExport.xml");//Added by sharishkumar for Mits 35472
            sPayeeExportXmlPath = Path.Combine(this.AppFilesPath, @"LSSInterface\PayeeExport.xml"); //Debabrata BiswasPayee Data Exchange MITS# 20073
            sCheckExportXmlPath = Path.Combine(this.AppFilesPath, @"LSSInterface\CheckExport.xml"); //Debabrata Biswas LSS Multidata exchange MITS# 20036
        } // method: InitializeLSSValues
        

		#region Code Export function
		/// Name		: CodeExport
		/// Author		: Manoj Agrawal
		/// Date Created: 07/31/2006
		/// ************************************************************
		/// <summary>		
		/// This method is used to export Code data from RMX to LSS
		/// </summary>
		public bool CodeExport(ref string p_sXml, ref string sExpCodes, string sCurrentTime)
		{
			string sCodeId = "";
			string sCodes = "";
			string sRqUID = "";
			string sMainRqUID = "";
			string sShortCodeOld = "";
			string sCodeType = "";
            string sCodeDescription = "";

			DataModelFactory objDMF=null;
			DbReader objReader=null;
			
			DbConnection objConn=null;

			Code objCode = null;
			LocalCache objCache = null;

			XmlDocument objCodeXml = null;
			XmlNamespaceManager nsmgr = null;
			XmlNode objCodeNode=null;

			StringBuilder sbSQL=null;

			

			int iCodeCount;


			sExpCodes = "";

			try
			{
				if(!File.Exists(sCodeExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("LSS.LoadFile.FileNotFound", m_iClientId)));

				objCodeXml=new XmlDocument();
				try
				{
					objCodeXml.Load(sCodeExportXmlPath);
				}
				catch(Exception p_objException)
				{
                    throw new RMAppException(Globalization.GetString("LSSExportProcess.CodeExportXmlNotFound", m_iClientId), p_objException);
				}

				sbSQL=new StringBuilder();

				sbSQL.Append("SELECT DISTINCT C.CODE_ID, E.OLD_SHORT_CODE, CT.CODE_DESC FROM CODES C, RM_LSS_CODE_EXP E, CODES_TEXT CT WHERE C.CODE_ID = E.CODE_ID AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0) AND CT.CODE_ID = E.CODE_ID ");
			
				objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString,sbSQL.ToString());

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);

				objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString, m_iClientId);

				iCodeCount = 0;

				sMainRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time
				sRqUID = sMainRqUID;			//set the default value for sRqUID same as sMainRqUID
				foreach (XmlNode objRqNode in objCodeXml.GetElementsByTagName("RqUID"))
				{
					objRqNode.InnerText = sMainRqUID;
				}

				foreach (XmlNode objOrg in objCodeXml.GetElementsByTagName("Org"))
				{
					objOrg.InnerText = m_sDsnName;
				}

				nsmgr = new XmlNamespaceManager(objCodeXml.NameTable);
				nsmgr.AddNamespace("csc", objCodeXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				while (objReader.Read())
				{
					objCode = null;

					sCodeId = "";
					sShortCodeOld = "";
					sCodeType = "";
                    sCodeDescription = "";

					sCodeId=Conversion.ConvertObjToStr(objReader.GetValue(0));
                    sShortCodeOld = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    sCodeDescription = Conversion.ConvertObjToStr(objReader.GetValue(2));

					objCode = (Code)objDMF.GetDataModelObject("Code",false);
					objCode.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId));

					//TODO: confirm what all code type we need to send
					//Manoj: As per the latest e-mail from Chris on 09/07/2006 we will send only Claim Type and Nature of Injury
					switch(objCache.GetTableName(objCode.TableId))
					{
						case "CLAIM_TYPE":
							sCodeType = "LINEOFBUSINESS";
							break;

						case "INJURY_TYPE":
							sCodeType = "NATUREOFINJURY";
							break;

//						case "TRANS_TYPES":		//TODO: Confirm if we need to send Trans Type or something else. There is one PAY_TYPES (Pay Type Codes) also in RMX.
//							sCodeType = "PAYMENTTYPE";
//							break;

						default:
							continue;		//if code is not of any of 3 types then continue with next code.
					}

					iCodeCount = iCodeCount + 1;

				
					objCodeNode = objCodeXml.GetElementsByTagName("InternalCode", objCodeXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc")).Item(0).Clone();


					sCodes = CommonFunctions.RemoveAmparsand(objCode.ShortCode);
					sExpCodes = sExpCodes + " " + sCodes;
					sExpCodes = sExpCodes.Trim();


					if(objCodeNode.SelectNodes("RqUID").Count > 0)
					{
						sRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time
						objCodeNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
					}

					objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
					objConn.Open();
					objConn.ExecuteNonQuery("UPDATE RM_LSS_CODE_EXP SET REQ_ID = '" + sRqUID + "' WHERE CODE_ID = " + sCodeId + " AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
					objConn.Close();
					//objConn=null;

					if(objCodeNode.SelectNodes("//csc:CodeType", nsmgr).Count > 0)
					{
						objCodeNode.SelectNodes("//csc:CodeType", nsmgr).Item(0).InnerText = sCodeType;		//this will help LSS to identify which type of Entity it is. Whether Adjuster or something else
					}

					if(objCodeNode.SelectNodes("//csc:Code", nsmgr).Count > 0)
					{
						objCodeNode.SelectNodes("//csc:Code", nsmgr).Item(0).InnerText = sShortCodeOld;
					}


					if(objCode.DeletedFlag == true)
					{
						if(objCodeNode.SelectNodes("//csc:CRUD", nsmgr).Count > 0)
						{
							objCodeNode.SelectNodes("//csc:CRUD", nsmgr).Item(0).InnerText = "D";
						}

						RemoveTag(objCodeNode, "csc:InternalCode", "csc:NewCode", nsmgr);

						RemoveTag(objCodeNode, "csc:InternalCode", "csc:CodeDescription", nsmgr);
					}
					else
					{
						if(objCodeNode.SelectNodes("//csc:CRUD", nsmgr).Count > 0)
						{
							objCodeNode.SelectNodes("//csc:CRUD", nsmgr).Item(0).InnerText = "U";
						}

						
						if(objCodeNode.SelectNodes("//csc:NewCode", nsmgr).Count > 0)
						{
							objCodeNode.SelectNodes("//csc:NewCode", nsmgr).Item(0).InnerText = objCode.ShortCode;
						}


						if(objCodeNode.SelectNodes("//csc:CodeDescription", nsmgr).Count > 0)
						{
                            //gagnihotri 08/31/2009: Commented, as it was picking up the old value from the cache.
							//objCodeNode.SelectNodes("//csc:CodeDescription", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objCode.CodeId);
                            objCodeNode.SelectNodes("//csc:CodeDescription", nsmgr).Item(0).InnerText = sCodeDescription;
						}
					}

					objCodeXml.SelectSingleNode("/ACORD/ExtensionsSvcRq").AppendChild(objCodeNode);
				}
				sCodeId="";
				objReader.Close();
				objDMF.Dispose();


				objCodeXml.SelectSingleNode("/ACORD/ExtensionsSvcRq").RemoveChild(objCodeXml.SelectNodes("/ACORD/ExtensionsSvcRq/csc:InternalCode", nsmgr).Item(0));

				if(iCodeCount > 0) 
					p_sXml=objCodeXml.OuterXml;
				else
					p_sXml = "";			//Manoj: if there is no Code to export then don't call web service

			}
			//catch (RMAppException p_objException)
			//{
			//	throw p_objException;
			//}
			catch (Exception p_objException)
			{
				//throw new RMAppException(Globalization.GetString("LSSExportProcess.Export.Error"), p_objException);
				throw p_objException;
			}
			finally
			{
				sbSQL=null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
				objCode=null;
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
				if (objReader!=null)
				{
					if (!objReader.IsClosed)
						objReader.Close();
					objReader=null;
				}
				if (objConn!=null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn=null;
				}
				objCodeNode = null;
				nsmgr = null;
				objCodeXml=null;
			}

			return true;
		}
		#endregion

		#region Administrative/Entity Export function
		/// Name		: AdminExport
		/// Author		: Manoj Agrawal
		/// Date Created: 07/31/2006
		/// ************************************************************
		/// <summary>		
		/// This method is used to export Admin data from RMX to LSS
		/// </summary>
		public bool AdminExport(ref string p_sXml, ref string sExpAdmins, string sCurrentTime)
		{

			string sEntityId="";
			string sAdminId = "";
			string sAdminType = "";
			string sRqUID = "";
			string sMainRqUID = "";

			string sAdjusterUserId = "";	//login name
			int iAdjusterUserId = 0;		//SMS user id

			string sSupervisorUserId = "";	//login name
			int iSupervisorUserId = 0;		//SMS user id

			string sManagerUserId = "";	//login name
			int iManagerUserId = 0;		//SMS user id

			//string sAttorneyId = "";			//we don't export Attorney and Firm from RMX
			//string sFirmId = "";

			string sOfficePhone = "";
			string sCellPhone = "";
			string sFax = "";

			DataModelFactory objDMF=null;
			DbReader objReader=null;
			DbReader objReaderTemp=null;
			DbConnection objConn=null;



			Entity objEntity = null;
			//LocalCache objCache = null;

			XmlDocument objAdminXml = null;
			XmlNamespaceManager nsmgr = null;
			XmlNode objAdminNode=null;

			StringBuilder sbSQL=null;
            Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(this.RiskmasterConnectionString, m_iClientId); //RMA 7914 Rijul

			int iAdminCount;

			sExpAdmins = "";

			try
			{
				
				if(!File.Exists(sAdminExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("LSS.LoadFile.FileNotFound", m_iClientId)));

				objAdminXml=new XmlDocument();
				try
				{
					objAdminXml.Load(sAdminExportXmlPath);
				}
				catch(Exception p_objException)
				{
                    throw new RMAppException(Globalization.GetString("LSSExportProcess.AdminExportXmlNotFound", m_iClientId), p_objException);
				}

				sbSQL=new StringBuilder();
                //Rijul RMA 7914 start // commented as per code review  //Code uncommented for JIRA 15188
                if (objSettings.UseEntityRole)
                    sbSQL.Append("SELECT DISTINCT EN.ENTITY_ID, E.TYPE_TEXT FROM ENTITY_X_ROLES EN, RM_LSS_ADMIN_EXP E WHERE EN.ENTITY_ID = E.ENTITY_ID AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0)");
                else
                    sbSQL.Append("SELECT DISTINCT EN.ENTITY_ID, E.TYPE_TEXT FROM ENTITY EN, RM_LSS_ADMIN_EXP E WHERE EN.ENTITY_ID = E.ENTITY_ID AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0)");
                //Rijul RMA 7914 end
			
				objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString,sbSQL.ToString());
                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);

				iAdminCount = 0;

				sMainRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time
				sRqUID = sMainRqUID;			//set the default value for sRqUID same as sMainRqUID
				foreach (XmlNode objRqNode in objAdminXml.GetElementsByTagName("RqUID"))
				{
					objRqNode.InnerText = sMainRqUID;
				}

				foreach (XmlNode objOrg in objAdminXml.GetElementsByTagName("Org"))
				{
					objOrg.InnerText = m_sDsnName;
				}

				nsmgr = new XmlNamespaceManager(objAdminXml.NameTable);
				nsmgr.AddNamespace("csc", objAdminXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				while (objReader.Read())
				{
					objEntity=null;

					sEntityId = "";
					sAdminId = "";
					sAdminType = "";
					//sTransactionRequestDt = "";
					sAdjusterUserId = "";
					iAdjusterUserId = 0;

					sSupervisorUserId = "";
					iSupervisorUserId = 0;

					sManagerUserId = "";
					iManagerUserId = 0;

					//sAttorneyId = "";			//we don't export Attorney and Firm from RMX
					//sFirmId = "";

					sOfficePhone = "";
					sCellPhone = "";
					sFax = "";

					sEntityId=Conversion.ConvertObjToStr(objReader.GetValue(0));
					sAdminType=Conversion.ConvertObjToStr(objReader.GetValue(1));
					switch(sAdminType)
					{
						case "ADJUSTER":
							sAdminType = "csc:Adjuster";
							break;

						case "SUPERVISOR":
							sAdminType = "csc:SuperVisor";
							break;

						case "MANAGER":
							sAdminType = "csc:Manager";
							break;

						//case "ATTORNEY":						//we don't export Attorney and Firm from RMX
						//	sAdminType = "csc:Attorney";
						//	break;

						default:
							continue;
					}

					iAdminCount = iAdminCount + 1;

					objAdminNode = objAdminXml.GetElementsByTagName("ClaimsSubsequentRptSubmitRq").Item(0).Clone();

					objEntity = (Entity)objDMF.GetDataModelObject("Entity",false);
					objEntity.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId));

					//select the login name from SMS
					if(sAdminType.Equals("csc:Adjuster") || sAdminType.Equals("csc:SuperVisor") || sAdminType.Equals("csc:Manager"))		//In case of Adjuster, Manager, Supervisor we send SMS User login name
					{
						objReaderTemp=DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + objEntity.RMUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");//rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
							sAdminId = Conversion.ConvertObjToStr(objReaderTemp.GetValue(0));
						}
						objReaderTemp.Close();
						//objReaderTemp = null;
					}

					//In case of Attorney we send Entity Id and not the SMS User login name
					//else if(sAdminType.Equals("csc:Attorney"))			//we don't export Attorney and Firm from RMX
					//{
					//	sAdminId = objEntity.EntityId.ToString();
					//}					

					sExpAdmins = sExpAdmins + " " + sAdminId;
					sExpAdmins = sExpAdmins.Trim();

					if(objAdminNode.SelectNodes("//ClaimsPartyRoleCd").Count > 0)
					{
						objAdminNode.SelectNodes("//ClaimsPartyRoleCd").Item(0).InnerText = sAdminType;		//this will help LSS to identify which type of Entity it is. Whether Adjuster or something else
					}


					if(objAdminNode.SelectNodes("RqUID").Count > 0)
					{
						sRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time
						objAdminNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
					}

					objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
					objConn.Open();
					objConn.ExecuteNonQuery("UPDATE RM_LSS_ADMIN_EXP SET REQ_ID = '" + sRqUID + "' WHERE ENTITY_ID = " + sEntityId + " AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
					objConn.Close();
					//objConn=null;

					if(objAdminNode.SelectNodes("//TransactionRequestDt").Count > 0)
					{
						objAdminNode.SelectNodes("//TransactionRequestDt").Item(0).InnerText = sCurrentTime;
					}


					if(sAdminType.Equals("csc:Adjuster"))
					{
						iAdjusterUserId = objEntity.RMUserId;
						sAdjusterUserId = sAdminId;	//Put Login name from Security database into sAdjusterUserId

						//fetch from database based on Adjuster and put Supervisor Login name into sSupervisorUserId
						objReaderTemp=DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iAdjusterUserId.ToString());// rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
							iSupervisorUserId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
						}
						objReaderTemp.Close();
						//objReaderTemp = null;


                        objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + iSupervisorUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");// rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
							sSupervisorUserId = Conversion.ConvertObjToStr(objReaderTemp.GetValue(0));
						}
						objReaderTemp.Close();
						//objReaderTemp = null;


						//fetch from database based on Adjuster and put Manager Login name into sManagerUserId
                        objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iSupervisorUserId.ToString());// rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
							iManagerUserId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
						}
						objReaderTemp.Close();
						//objReaderTemp = null;


                        objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + iManagerUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");// rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
							sManagerUserId = Conversion.ConvertObjToStr(objReaderTemp.GetValue(0));
						}
						objReaderTemp.Close();
						//objReaderTemp = null;
						
						//sAttorneyId = "";					//we don't export Attorney and Firm from RMX
						//sFirmId = "";
					}

					else if(sAdminType.Equals("csc:SuperVisor"))
					{
						iAdjusterUserId = 0;
						sAdjusterUserId = "";

						iSupervisorUserId = objEntity.RMUserId;
						sSupervisorUserId = sAdminId;	//Put Login name from Security database into sSupervisorUserId

						//fetch from database based on Adjuster and put Manager Login name into sManagerUserId
                        objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iSupervisorUserId.ToString());// rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
							iManagerUserId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
						}
						objReaderTemp.Close();
						//objReaderTemp = null;


                        objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + iManagerUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");// rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
							sManagerUserId = Conversion.ConvertObjToStr(objReaderTemp.GetValue(0));
						}
						objReaderTemp.Close();
						//objReaderTemp = null;

						//sAttorneyId = "";					//we don't export Attorney and Firm from RMX
						//sFirmId = "";
					}

					else if(sAdminType.Equals("csc:Manager"))
					{
						iAdjusterUserId = 0;
						sAdjusterUserId = "";

						iSupervisorUserId = 0;
						sSupervisorUserId = "";

						iManagerUserId = objEntity.RMUserId;
						sManagerUserId = sAdminId;	//Put Login name from Security database into sManagerUserId

						//sAttorneyId = "";					//we don't export Attorney and Firm from RMX
						//sFirmId = "";
					}

					//else if(sAdminType.Equals("csc:Attorney"))				//we don't export Attorney and Firm from RMX
					//{
					//	sAdjusterUserId = "";
					//	sSupervisorUserId = "";
					//	sManagerUserId = "";
					//	sAttorneyId = sAdminId;				//it contains objEntity.EntityId
					//	sFirmId = objEntity.ParentEid.ToString();
					//}


					if(!sAdjusterUserId.Equals(""))
					{
						AddTagValue(objAdminNode, "OtherIdentifier", "csc:AdjusterUserId", "OtherId", sAdjusterUserId, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Adjuster login is blank
					{
						RemoveTag(objAdminNode, "OtherIdentifier", "csc:AdjusterUserId", nsmgr);
					}



					if(!sSupervisorUserId.Equals(""))
					{
						AddTagValue(objAdminNode, "OtherIdentifier", "csc:SupervisorUserId", "OtherId", sSupervisorUserId, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Supervisor login is blank
					{
						RemoveTag(objAdminNode, "OtherIdentifier", "csc:SupervisorUserId", nsmgr);
					}

								

					if(!sManagerUserId.Equals(""))
					{
						AddTagValue(objAdminNode, "OtherIdentifier", "csc:ManagerUserId", "OtherId", sManagerUserId, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Manager login is blank
					{
						RemoveTag(objAdminNode, "OtherIdentifier", "csc:ManagerUserId", nsmgr);
					}


					//we don't export Attorney and Firm from RMX

					//if(!sAttorneyId.Equals(""))			
					//{
					//	AddTagValue(objAdminNode, "OtherIdentifier", "csc:AttorneyId", "OtherId", sAttorneyId, nsmgr);
					//}
					//else	//remove the OtherIdentifier tag if Adjuster login is blank
					//{
					//	RemoveTag(objAdminNode, "OtherIdentifier", "csc:AttorneyId", nsmgr);
					//}
					//
					//if(!sFirmId.Equals(""))
					//{
					//	AddTagValue(objAdminNode, "OtherIdentifier", "csc:FirmOfficeId", "OtherId", sFirmId, nsmgr);
					//}
					//else	//remove the OtherIdentifier tag if Adjuster login is blank
					//{
					//	RemoveTag(objAdminNode, "OtherIdentifier", "csc:FirmOfficeId", nsmgr);
					//}


					sOfficePhone = objEntity.Phone1;

					if(!sOfficePhone.Equals(""))
					{
						AddTagValue(objAdminNode, "PhoneInfo", "Office", "PhoneNumber", sOfficePhone, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Office phone is blank
					{
						RemoveTag(objAdminNode, "PhoneInfo", "Office", nsmgr);
					}



					sCellPhone = objEntity.Phone2;

					if(!sCellPhone.Equals(""))
					{
						AddTagValue(objAdminNode, "PhoneInfo", "Cell", "PhoneNumber", sCellPhone, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Cell phone is blank
					{
						RemoveTag(objAdminNode, "PhoneInfo", "Cell", nsmgr);
					}


					sFax = objEntity.FaxNumber;

					if(!sFax.Equals(""))
					{
						AddTagValue(objAdminNode, "PhoneInfo", "Fax", "PhoneNumber", sFax, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Fax number is blank
					{
						RemoveTag(objAdminNode, "PhoneInfo", "Fax", nsmgr);
					}



					if(objAdminNode.SelectNodes("//csc:SurnameLong", nsmgr).Count > 0)
					{
						objAdminNode.SelectNodes("//csc:SurnameLong", nsmgr).Item(0).InnerText = objEntity.LastName;
					}


					if(objAdminNode.SelectNodes("//csc:GivenNameLong", nsmgr).Count > 0)
					{
						objAdminNode.SelectNodes("//csc:GivenNameLong", nsmgr).Item(0).InnerText = objEntity.FirstName;
					}


					if(objAdminNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Count > 0)
					{
						objAdminNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Item(0).InnerText = objEntity.MiddleName;
					}


					if(objAdminNode.SelectNodes("//EmailAddr").Count > 0)
					{
						objAdminNode.SelectNodes("//EmailAddr").Item(0).InnerText = objEntity.EmailAddress;
					}

					objAdminXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objAdminNode);
				}
				sEntityId="";
				objReader.Close();
				objDMF.Dispose();


				objAdminXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objAdminXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq").Item(0));

				if(iAdminCount > 0) 
					p_sXml=objAdminXml.OuterXml;
				else
					p_sXml = "";			//Manoj: if there is no Admin to export then don't call web service

			}
			//catch (RMAppException p_objException)
			//{
			//	throw p_objException;
			//}
			catch (Exception p_objException)
			{
				//throw new RMAppException(Globalization.GetString("LSSExportProcess.Export.Error"), p_objException);
				throw p_objException;
			}
			finally
			{
				sbSQL=null;
                if (objEntity != null)
                {
                    objEntity.Dispose();
                }
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
				if (objReader!=null)
				{
					if (!objReader.IsClosed)
						objReader.Close();
					objReader=null;
				}
				if (objConn!=null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn=null;
				}
                if (objReaderTemp != null)
                {
                    objReaderTemp.Dispose();
                }
				objAdminNode = null;
				nsmgr = null;
				objAdminXml=null;
			}
			return true;
		}
		#endregion
        #region Payee Export function
        /// Name		: PayeeExport MITS 20073
        /// Author		: Sumit Naresh Kumar
        /// Date Created: 12/10/2008
        /// ************************************************************
        /// <summary>		
        /// This method is used to export Payee data from RMX to LSS
        /// </summary>
        public bool PayeeExport(ref string p_sXml, ref string sExpPayees, string sCurrentTime)
        {

            string sEntityId = "";
            string sPayeeId = "";
            string sPayeeType = "";
            string sRqUID = "";
            string sMainRqUID = "";
            string sRMXEntityTypeId = "";
            string sOfficePhone = "";
            string sCellPhone = "";
            string sFax = "";
            string sPayeeCompanyId = "";
            string sPayeeCompanyName = "";
            string sPayeeCompanyAbbr = "";
            string sPayeeCompanyAKA = "";

            DataModelFactory objDMF = null;
            DbReader objReader = null;
            DbReader objReaderTemp = null;
            DbConnection objConn = null;

            UserLogin objUserLogin = null;

            Entity objEntity = null;
            Entity objCompany = null;
            LocalCache objCache = null;        

            XmlDocument objPayeeXml = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode objPayeeNode = null;

            StringBuilder sbSQL = null;
            Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(this.RiskmasterConnectionString, m_iClientId); //RMA 7914 Rijul
            //rsushilaggar modified the file to fix the LSS issue
            //string sPayeeExportXmlPath = "";

            int iPayeeCount;

            sExpPayees = "";

            try
            {
                if (!File.Exists(sPayeeExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("LSS.LoadFile.FileNotFound", m_iClientId)));

                objPayeeXml = new XmlDocument();
                try
                {
                    objPayeeXml.Load(sPayeeExportXmlPath);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("LSSExportProcess.PayeeExportXmlNotFound", m_iClientId), p_objException);
                }

                sbSQL = new StringBuilder();

                //Debabrata Biswas Payee Data Exchange MITS# 20073
                //Debabrata Biswas: 09/16/2009 MITS #17971 
                //The line below had been modified to include glossary_type_code to identify
                //whether an entity if of type Entity or People
                //7-People
                //4-Entity
                //sbSQL.Append("SELECT DISTINCT EN.ENTITY_ID, E.TYPE_TEXT FROM ENTITY EN, RM_LSS_PAYEE_EXP E WHERE EN.ENTITY_ID = E.ENTITY_ID AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0)");
                //Rijul RMA 7914 start  //comented as per code review //Code uncommented for JIRA 15188
                if (objSettings.UseEntityRole)
                    sbSQL.Append("SELECT DISTINCT EN.ENTITY_ID, E.TYPE_TEXT, G.GLOSSARY_TYPE_CODE FROM ENTITY_X_ROLES EN, RM_LSS_PAYEE_EXP E, GLOSSARY G WHERE EN.ENTITY_ID = E.ENTITY_ID AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0) AND EN.ENTITY_TABLE_ID=G.TABLE_ID");
                else
                    sbSQL.Append("SELECT DISTINCT EN.ENTITY_ID, E.TYPE_TEXT, G.GLOSSARY_TYPE_CODE FROM ENTITY EN, RM_LSS_PAYEE_EXP E, GLOSSARY G WHERE EN.ENTITY_ID = E.ENTITY_ID AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0) AND EN.ENTITY_TABLE_ID=G.TABLE_ID");
                //Rijul RMA 7914 end

                objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sbSQL.ToString());
                objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                objDMF = new DataModelFactory(objUserLogin, m_iClientId);

                objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString, m_iClientId);

                iPayeeCount = 0;

                sMainRqUID = Guid.NewGuid().ToString();						//Sumit - this will generate a unique Req ID each time Payee Data Exchange MITS# 20073
                sRqUID = sMainRqUID;			//set the default value for sRqUID same as sMainRqUID
                foreach (XmlNode objRqNode in objPayeeXml.GetElementsByTagName("RqUID"))
                {
                    objRqNode.InnerText = sMainRqUID;
                }

                foreach (XmlNode objOrg in objPayeeXml.GetElementsByTagName("Org"))
                {
                    objOrg.InnerText = m_sDsnName;
                }

                nsmgr = new XmlNamespaceManager(objPayeeXml.NameTable);
                nsmgr.AddNamespace("csc", objPayeeXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                while (objReader.Read())
                {
                    objEntity = null;
                    objCompany = null;

                    sEntityId = "";
                    sPayeeId = "";
                    sPayeeType = "";
                    sOfficePhone = "";
                    sCellPhone = "";
                    sFax = "";
                    sPayeeCompanyId = "";
                    sPayeeCompanyName = "";
                    sPayeeCompanyAbbr = "";
                    sPayeeCompanyAKA = "";
                    int iEntityType;            //Debabrata Biswas 09162009 MITS# 17971 ; This will be used to store entity or people type. Payee Data Exchange R6 Retrofit MITS# 20073

                    sEntityId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    sPayeeType = Conversion.ConvertObjToStr(objReader.GetValue(1));
                    iEntityType = Conversion.ConvertObjToInt(objReader.GetValue(2), m_iClientId);//Debabrata Biswas 09162009 MITS# 17971  This will be used to store entity or people type.Payee Data Exchange R6 Retrofit MITS# 20073

                    iPayeeCount = iPayeeCount + 1;

                    objPayeeNode = objPayeeXml.GetElementsByTagName("ClaimsSubsequentRptSubmitRq").Item(0).Clone();

                    objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                    objEntity.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId));

                    sPayeeId = objEntity.EntityId.ToString();
                    sRMXEntityTypeId = sPayeeId;

                    sExpPayees = sExpPayees + " " + sPayeeId;
                    sExpPayees = sExpPayees.Trim();
					//rsushilaggar MITS 22033 DATE 09/22/2010
                    if (objEntity.Supplementals["PAYEE_COMPANY"] != null)
                    {
                        sPayeeCompanyId = objEntity.Supplementals["PAYEE_COMPANY"].Value.ToString();
                        if (sPayeeCompanyId != null)
                        {
                            objCompany = (Entity)objDMF.GetDataModelObject("Entity", false);
                            objCompany.MoveTo(Conversion.ConvertStrToInteger(sPayeeCompanyId));
                            sPayeeCompanyName = objCompany.LastName;
                            sPayeeCompanyAbbr = objCompany.Abbreviation;
                            sPayeeCompanyAKA = objCompany.AlsoKnownAs;
                        }
                    }

                    if (objPayeeNode.SelectNodes("//ClaimsPartyRoleCd").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//ClaimsPartyRoleCd").Item(0).InnerText = sPayeeType;		//this will help LSS to identify which type of Entity it is. Whether Adjuster or something else
                    }


                    if (objPayeeNode.SelectNodes("RqUID").Count > 0)
                    {
                        sRqUID = Guid.NewGuid().ToString();						//Sumit - this will generate a unique Req ID each time
                        objPayeeNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
                    }

                    objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                    objConn.Open();
                    objConn.ExecuteNonQuery("UPDATE RM_LSS_PAYEE_EXP SET REQ_ID = '" + sRqUID + "' WHERE ENTITY_ID = " + sEntityId + " AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
                    objConn.Close();
                    //objConn=null;

                    if (objPayeeNode.SelectNodes("//TransactionRequestDt").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//TransactionRequestDt").Item(0).InnerText = sCurrentTime;
                    }

                    if (!sRMXEntityTypeId.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "OtherIdentifier", "csc:RMXEntityId", "OtherId", sRMXEntityTypeId, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Payee login is blank
                    {
                        RemoveTag(objPayeeNode, "OtherIdentifier", "csc:RMXEntityId", nsmgr);
                    }

                    if (!sPayeeCompanyId.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyId", "OtherId", sPayeeCompanyId, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Payee login is blank
                    {
                        RemoveTag(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyId", nsmgr);
                    }

                    if (!sPayeeCompanyName.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyName", "OtherId", sPayeeCompanyName, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Payee login is blank
                    {
                        RemoveTag(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyName", nsmgr);
                    }

                    if (!sPayeeCompanyAbbr.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyAbbr", "OtherId", sPayeeCompanyAbbr, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Payee login is blank
                    {
                        RemoveTag(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyAbbr", nsmgr);
                    }

                    if (!sPayeeCompanyAKA.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyAKA", "OtherId", sPayeeCompanyAKA, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Payee login is blank
                    {
                        RemoveTag(objPayeeNode, "OtherIdentifier", "csc:RMXCompanyAKA", nsmgr);
                    }

                    if (objEntity.DeletedFlag == true)
                    {
                        if (objPayeeNode.SelectNodes("//csc:CRUD", nsmgr).Count > 0)
                        {
                            objPayeeNode.SelectNodes("//csc:CRUD", nsmgr).Item(0).InnerText = "D";
                        }
                    }
                    else
                    {
                        if (objPayeeNode.SelectNodes("//csc:CRUD", nsmgr).Count > 0)
                        {
                            objPayeeNode.SelectNodes("//csc:CRUD", nsmgr).Item(0).InnerText = "U";
                        }
                    }

                    sOfficePhone = objEntity.Phone1;

                    if (!sOfficePhone.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "PhoneInfo", "Office", "PhoneNumber", sOfficePhone, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Office phone is blank
                    {
                        RemoveTag(objPayeeNode, "PhoneInfo", "Office", nsmgr);
                    }


                    sCellPhone = objEntity.Phone2;

                    if (!sCellPhone.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "PhoneInfo", "Cell", "PhoneNumber", sCellPhone, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Cell phone is blank
                    {
                        RemoveTag(objPayeeNode, "PhoneInfo", "Cell", nsmgr);
                    }


                    sFax = objEntity.FaxNumber;

                    if (!sFax.Equals(""))
                    {
                        AddTagValue(objPayeeNode, "PhoneInfo", "Fax", "PhoneNumber", sFax, nsmgr);
                    }
                    else	//remove the OtherIdentifier tag if Fax number is blank
                    {
                        RemoveTag(objPayeeNode, "PhoneInfo", "Fax", nsmgr);
                    }

                    //Debabrata Biswas Payee Data Exchange MITS# 20073
                    //Debabrata Biswas 09162009 MITS # 17971 
                    //Commented below statement to include the logic for populating last name in case of entity
                    //and populating surname,lastname and middle name for people.

                    //if (objPayeeNode.SelectNodes("//CommercialName", nsmgr).Count > 0)
                    //{
                    //    objPayeeNode.SelectNodes("//CommercialName", nsmgr).Item(0).InnerText = objEntity.LastName;
                    //}

                    if(iEntityType==4)
                    {
                        if (objPayeeNode.SelectNodes("//CommercialName", nsmgr).Count > 0)
                        {
                            objPayeeNode.SelectNodes("//CommercialName", nsmgr).Item(0).InnerText = objEntity.LastName;
                        }
                    }
                    else if(iEntityType==7)
                    {
                        if (objPayeeNode.SelectNodes("//csc:SurnameLong", nsmgr).Count > 0)
					    {
                            objPayeeNode.SelectNodes("//csc:SurnameLong", nsmgr).Item(0).InnerText = objEntity.LastName;
					    }


                        if (objPayeeNode.SelectNodes("//csc:GivenNameLong", nsmgr).Count > 0)
					    {
                            objPayeeNode.SelectNodes("//csc:GivenNameLong", nsmgr).Item(0).InnerText = objEntity.FirstName;
					    }


                        if (objPayeeNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Count > 0)
					    {
                            objPayeeNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Item(0).InnerText = objEntity.MiddleName;
					    }
                    }

                    if (objPayeeNode.SelectNodes("//EmailAddr").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//EmailAddr").Item(0).InnerText = objEntity.EmailAddress;
                    }

                    if (objPayeeNode.SelectNodes("//Addr1").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//Addr1").Item(0).InnerText = objEntity.Addr1;
                    }

                    if (objPayeeNode.SelectNodes("//Addr2").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//Addr2").Item(0).InnerText = objEntity.Addr2;
                    }

                    if (objPayeeNode.SelectNodes("//City").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//City").Item(0).InnerText = objEntity.City;
                    }

                    if (objPayeeNode.SelectNodes("//StateProvCd").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//StateProvCd").Item(0).InnerText = objCache.GetStateCode(objEntity.StateId);
                    }

                    if (objPayeeNode.SelectNodes("//PostalCode").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//PostalCode").Item(0).InnerText = objEntity.ZipCode;
                    }

                    if (objPayeeNode.SelectNodes("//CountryCd").Count > 0)
                    {
                        objPayeeNode.SelectNodes("//CountryCd").Item(0).InnerText = objCache.GetShortCode(objEntity.CountryCode);
                    }

                    if (objPayeeNode.SelectNodes("//TaxId", nsmgr).Count > 0)
                    {
                        objPayeeNode.SelectNodes("//TaxId", nsmgr).Item(0).InnerText = objEntity.TaxId;
                    }

                    objPayeeXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objPayeeNode);
                }
                sEntityId = "";
                objReader.Close();
                objDMF.Dispose();


                objPayeeXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objPayeeXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq").Item(0));

                if (iPayeeCount > 0)
                    p_sXml = objPayeeXml.OuterXml;
                else
                    p_sXml = "";			//Sumit: if there is no Admin to export then don't call web service

            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                sbSQL = null;

                if (objCache != null)
                {
                    objCache.Dispose();
                }

                if (objEntity != null)
                {
                    objEntity.Dispose();
                }

                if (objCompany != null)
                {
                    objCompany.Dispose();
                }

                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                        objReader.Close();
                    objReader = null;
                }
                if (objConn != null)
                {
                    if (objConn.State == ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn = null;
                }
                if (objReaderTemp != null)
                {
                    objReaderTemp.Dispose();
                }
                objUserLogin = null;
                objPayeeNode = null;
                nsmgr = null;
                objPayeeXml = null;
            }
            return true;
        }
        #endregion

		#region Claim Export function
		/// Name		: ClaimExport
		/// Author		: Parag Sarin
		/// Date Created: 09/23/2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *    Author		*   Amendment   
		/// 08/01/2006			Manoj			Almost changed everything as per latest design for TASB
		/// ************************************************************
		/// <summary>		
		/// This method is used to export Claims data from RMX to LSS
		/// </summary>
		//public bool ClaimExport()		//Manoj
		public bool ClaimExport(ref string p_sXml, ref string sExpClaimNumbers, string sCurrentTime)
		{
			string sClaimId="";
			string sClaimNumber = "";
			string sRqUID = "";
			string sMainRqUID = "";
			//string sTransactionRequestDt = "";		//Manoj
			string sLOBCd = "";
			string sDept = "";
			//string sAbbr = "";				//Manoj
			//string sName = "";				//Manoj

			//Manoj - added new parameters as per latest design			
			int iAdjusterUserId = 0;
			string sAdjusterUserId = "";

			int iSupervisorUserId = 0;
			string sSupervisorUserId = "";

			int iManagerUserId = 0;
			string sManagerUserId = "";

			string sLossDt = "";		//YYYYMMDD
			string sReportedDt = "";		//YYYYMMDD
			string sEventDesc = "";
			string sClaimantEid = "";
			string sClaimantLastName = "";
			string sClaimantFirstName = "";
			string sClaimantMiddleName = "";
			string sClaimantBirthDt = "";	//YYYYMMDD
			string sInjuryDesc = "";

			StringBuilder sbSQL=null;
			//StringBuilder sbClaimXml=null;	//Manoj
			//string sLastUpdatedTime="";		
			//string sCurrentTime="";

			DataModelFactory objDMF=null;
			DbReader objReader=null;
			DbReader objReaderTemp = null;
			DbConnection objConn=null;

			//XmlDocument objXmlAppConfig=null;	//Manoj
			XmlDocument objClaimXml = null;
			XmlNamespaceManager nsmgr = null;

			Claim objClaim=null;
			Event objEvent = null;
			LocalCache objCache = null;

			int iClaimCount;

			//Manoj - no need of following parameters
			
			//Values will be read from RMConfigurator/Any Xml File
			//string sXmlRoot="";
			//string sXmlClaimRecord="";
			//string sXmlClaimNumber="";
			//string sXmlClaimantData="";
			//string sXmlClaimantName="";
			//string sXmlCoverageData="";
			//string sXmlCoverageCd="";
			//string sXmlCoverageCdDesc="";
			//string sRestXml="";
			//string sXmlSeverityCd="";
			//string sXmlSeverityDesc="";
			//string sXmlConfigPath="";
			//string sClaimExportXmlPath="";

			//string sXmlClaimProcess="";

			XmlNode objClaimNode=null;


			sExpClaimNumbers = "";
			int x = -1;

			try
			{
				
				if(!File.Exists(sClaimExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("LSS.LoadFile.FileNotFound", m_iClientId)));

				objClaimXml=new XmlDocument();
				try
				{
					objClaimXml.Load(sClaimExportXmlPath);
				}
				catch(Exception p_objException)
				{
                    throw new RMAppException(Globalization.GetString("LSSExportProcess.ClaimExportXmlNotFound", m_iClientId), p_objException);
				}

				//Manoj - Changed the below logic. Now I fetch whole XML template from app folder and fill that XML with values from database according to claims being exported.
				//sXmlRoot=CommonFunctions.GetValue(objXmlAppConfig,"//XmlRoot");
				//sXmlClaimRecord=CommonFunctions.GetValue(objXmlAppConfig,"//XmlClaimRecord");
				//sXmlClaimNumber=CommonFunctions.GetValue(objXmlAppConfig,"//XmlClaimNumber");
				//sXmlClaimantData=CommonFunctions.GetValue(objXmlAppConfig,"//XmlClaimantData");
				//sXmlClaimantName=CommonFunctions.GetValue(objXmlAppConfig,"//XmlClaimantName");
				//sXmlCoverageData=CommonFunctions.GetValue(objXmlAppConfig,"//XmlCoverageData");
				//sXmlCoverageCd=CommonFunctions.GetValue(objXmlAppConfig,"//XmlCoverageCd");
				//sXmlCoverageCdDesc=CommonFunctions.GetValue(objXmlAppConfig,"//XmlCoverageCdDesc");
				//sRestXml=CommonFunctions.GetValue(objXmlAppConfig,"//RestXml");
				//sXmlSeverityCd=CommonFunctions.GetValue(objXmlAppConfig,"//XmlSeverityCd");
				//sXmlSeverityDesc=CommonFunctions.GetValue(objXmlAppConfig,"//XmlSeverityDesc");
				//sCoverageCode=CommonFunctions.GetValue(objXmlAppConfig,"//CoverageCode");
				//sCoverageCode=CommonFunctions.GetValue(objXmlAppConfig,"//CoverageCode");
				//sCoverageCodeDesc=CommonFunctions.GetValue(objXmlAppConfig,"//CoverageCodeDesc");
				//sXmlClaimProcess=CommonFunctions.GetValue(objXmlAppConfig,"//ClaimProcessLog");
				//objXmlAppConfig=null;


				//sCurrentTime=Conversion.GetDateTime(DateTime.Now.ToString());
				sbSQL=new StringBuilder();

				//Manoj - Changed below logic

				//sbSQL.Append("SELECT CLAIM_DTTM FROM WZ_LA_INFO WHERE ROW_ID=1");
				//objReader=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				//if (objReader.Read())
				//{
				//	sLastUpdatedTime=Conversion.ConvertObjToStr(objReader.GetValue(0));
				//}
				//objReader.Close();
				//objReader=null;
				//sbSQL.Remove(0,sbSQL.ToString().Length);
				//
				//sbSQL.Append("SELECT  CLAIM_ID FROM CLAIM C WHERE C.DTTM_RCD_LAST_UPD >=");
				//sbSQL.Append(sLastUpdatedTime);

				sbSQL.Append("SELECT DISTINCT C.CLAIM_ID FROM CLAIM C WHERE C.LSS_CLAIM_IND = -1 AND C.CLAIM_ID IN (SELECT E.CLAIM_ID FROM RM_LSS_CLAIM_EXP E WHERE E.TYPE_TEXT = 'CLAIM' AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0))");
				objReader=DbFactory.GetDbReader(this.RiskmasterConnectionString,sbSQL.ToString());
                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				//sbClaimXml=new StringBuilder();
				//sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlRoot));

				objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString, m_iClientId);

				iClaimCount = 0;

				//Manoj - Changed all below logic and flow as per latest design

				sMainRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time for each record
				sRqUID = sMainRqUID;			//set the default value for sRqUID same as sMainRqUID
				foreach (XmlNode objRqNode in objClaimXml.GetElementsByTagName("RqUID"))
				{
					objRqNode.InnerText = sMainRqUID;
				}

				foreach (XmlNode objOrg in objClaimXml.GetElementsByTagName("Org"))
				{
					objOrg.InnerText = m_sDsnName;
				}

				nsmgr = new XmlNamespaceManager(objClaimXml.NameTable);
				nsmgr.AddNamespace("csc", objClaimXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				while (objReader.Read())
				{
					objClaimNode = objClaimXml.GetElementsByTagName("ClaimsNotificationAddRq").Item(0).Clone();
				
					objClaim=null;

					sClaimId = "";
					sClaimNumber = "";
					//sTransactionRequestDt = "";
					sLOBCd = "";
					sDept = "";

					iAdjusterUserId = 0;
					sAdjusterUserId = "";

					iSupervisorUserId = 0;
					sSupervisorUserId = "";

					iManagerUserId = 0;
					sManagerUserId = "";

					sLossDt = "";				//YYYYMMDD
					sReportedDt = "";		//YYYYMMDD
					sEventDesc = "";
					sClaimantEid = "";
					sClaimantLastName = "";
					sClaimantFirstName = "";
					sClaimantMiddleName = "";
					sClaimantBirthDt = "";	//YYYYMMDD
					sInjuryDesc = "";

					iClaimCount = iClaimCount + 1;

					//sCoverageCd = "";			//Manoj
					//sCoverageCdDesc = "";
					//sNatureOfInjCd = "";
					//sNatureOfInjCdDesc = "";
					//sIndemnityIncurredAmt = "";
					//sCoverageCd = new String[0];
					//sCoverageCdDesc = new String[0];
					//sNatureOfInjCd = new String[0];
					//sNatureOfInjCdDesc = new String[0];
					//sIndemnityIncurredAmt = new String[0];


					sClaimId=Conversion.ConvertObjToStr(objReader.GetValue(0));
					//sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlClaimRecord));
					objClaim = (Claim)objDMF.GetDataModelObject("Claim",false);
					objClaim.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId));
					//sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlClaimNumber));
					sClaimNumber=CommonFunctions.RemoveAmparsand(objClaim.ClaimNumber);
					sExpClaimNumbers = sExpClaimNumbers + " " + sClaimNumber;
					sExpClaimNumbers = sExpClaimNumbers.Trim();
					//sbClaimXml.Append(sClaimNumber);
					//sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlClaimNumber));

					foreach (XmlNode objInsNode in objClaimNode.SelectNodes("//csc:InsurerId", nsmgr))
					{
						objInsNode.InnerText = sClaimNumber;
					}


					if(objClaimNode.SelectNodes("RqUID").Count > 0)
					{
						sRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time
						objClaimNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
					}

					objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
					objConn.Open();
					objConn.ExecuteNonQuery("UPDATE RM_LSS_CLAIM_EXP SET REQ_ID = '" + sRqUID + "' WHERE CLAIM_ID = " + sClaimId + " AND TYPE_TEXT = 'CLAIM' AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
					objConn.Close();
					//objConn=null;

					if(objClaimNode.SelectNodes("//TransactionRequestDt").Count > 0)
					{
						objClaimNode.SelectNodes("//TransactionRequestDt").Item(0).InnerText = sCurrentTime;
					}

					sLOBCd = objCache.GetShortCode(objClaim.ClaimTypeCode);
					if(objClaimNode.SelectNodes("//LOBCd").Count > 0)
					{
						objClaimNode.SelectNodes("//LOBCd").Item(0).InnerText = sLOBCd;
					}

					objEvent = (Event)objClaim.Parent;
					//objCache.GetOrgInfo(objEvent.DeptEid, ref sAbbr, ref sName);
					//sDept = sAbbr + " - " + sName;
					sDept = objEvent.DeptEid.ToString();		//Manoj - Currently I am sending Dept Eid as per tech design mapping. If Abbr, Last name need to be sent then uncomment above lines and comment this line.
					if(objClaimNode.SelectNodes("//CommercialName").Count > 0)
					{
						objClaimNode.SelectNodes("//CommercialName").Item(0).InnerText = sDept;
					}


					//Manoj - fetch from database based on current adjuster flag and put into sAdjusterUserId
					foreach(ClaimAdjuster objAdjuster in objClaim.AdjusterList)
					{
						if(objAdjuster.CurrentAdjFlag == true)
						{
							//Put Login name from Security database into sAdjusterUserId
                            objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + objAdjuster.AdjusterEntity.RMUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");// rkaur27 : Cloud Change
							if(objReaderTemp.Read())
							{
                                iAdjusterUserId = objAdjuster.AdjusterEntity.RMUserId;
								sAdjusterUserId = Conversion.ConvertObjToStr(objReaderTemp.GetValue(0));
							}
							objReaderTemp.Close();
							//objReaderTemp = null;
						}
					}

					if(!sAdjusterUserId.Equals(""))
					{
						AddTagValue(objClaimNode, "OtherIdentifier", "csc:AdjusterUserId", "OtherId", sAdjusterUserId, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Adjuster login is blank
					{
						RemoveTag(objClaimNode, "OtherIdentifier", "csc:AdjusterUserId", nsmgr);
					}


					//Manoj - fetch from database based on Adjuster and put Supervisor Login name into sSupervisorUserId
                    objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iAdjusterUserId.ToString());// rkaur27 : Cloud Change
					if(objReaderTemp.Read())
					{
						iSupervisorUserId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
					}
					objReaderTemp.Close();
					//objReaderTemp = null;


                    objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + iSupervisorUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");// rkaur27 : Cloud Change
					if(objReaderTemp.Read())
					{
						sSupervisorUserId = Conversion.ConvertObjToStr(objReaderTemp.GetValue(0));
					}
					objReaderTemp.Close();
					objReaderTemp = null;



					if(!sSupervisorUserId.Equals(""))
					{
						AddTagValue(objClaimNode, "OtherIdentifier", "csc:SupervisorUserId", "OtherId", sSupervisorUserId, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Supervisor login is blank
					{
						RemoveTag(objClaimNode, "OtherIdentifier", "csc:SupervisorUserId", nsmgr);
					}

								

					//Manoj - fetch from database based on Adjuster and put Manager Login name into sManagerUserId
                    objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iSupervisorUserId.ToString());// rkaur27 : Cloud Change
					if(objReaderTemp.Read())
					{
						iManagerUserId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
					}
					objReaderTemp.Close();
					//objReaderTemp = null;


                    objReaderTemp = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + iManagerUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");// rkaur27 : Cloud Change
					if(objReaderTemp.Read())
					{
						sManagerUserId = Conversion.ConvertObjToStr(objReaderTemp.GetValue(0));
					}
					objReaderTemp.Close();
					//objReaderTemp = null;



					if(!sManagerUserId.Equals(""))
					{
						AddTagValue(objClaimNode, "OtherIdentifier", "csc:ManagerUserId", "OtherId", sManagerUserId, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Manager login is blank
					{
						RemoveTag(objClaimNode, "OtherIdentifier", "csc:ManagerUserId", nsmgr);
					}


					sReportedDt = objEvent.DateReported;			//YYYYMMDD
					if(objClaimNode.SelectNodes("//ReportedDt").Count > 0)
					{
						objClaimNode.SelectNodes("//ReportedDt").Item(0).InnerText = sReportedDt;
					}

										
					sLossDt = objEvent.DateOfEvent;			//YYYYMMDD
					if(objClaimNode.SelectNodes("//LossDt").Count > 0)
					{
						objClaimNode.SelectNodes("//LossDt").Item(0).InnerText = sLossDt;
					}

                    //Start rsushilaggar MITS 21984 MITS 14-09-2010
					sEventDesc = objEvent.EventDescription;
                    EventDescLen = Conversion.ConvertStrToInteger(RMConfigurationManager.GetNameValueSectionSettings("LSSInterface", this.RiskmasterConnectionString, m_iClientId)["MaxEventDescLengthAllowed"]);
                    if (sEventDesc.Length > EventDescLen)
                    {
                        sEventDesc = sEventDesc.Substring(0, EventDescLen);
                        sEventDesc = sEventDesc + "...";
                    }
                    //End rsushilaggar

					if(objClaimNode.SelectNodes("//IncidentDesc").Count > 0)
					{
						objClaimNode.SelectNodes("//IncidentDesc").Item(0).InnerText = sEventDesc;
					}


					if(objClaim.PrimaryClaimant != null)
					{
						sClaimantEid = objClaim.PrimaryClaimant.ClaimantEid.ToString();
						sClaimantLastName = objClaim.PrimaryClaimant.ClaimantEntity.LastName;
						sClaimantFirstName = objClaim.PrimaryClaimant.ClaimantEntity.FirstName;
						sClaimantMiddleName = objClaim.PrimaryClaimant.ClaimantEntity.MiddleName;
						sClaimantBirthDt = objClaim.PrimaryClaimant.ClaimantEntity.BirthDate;    	//YYYYMMDD
						sInjuryDesc = objClaim.PrimaryClaimant.InjuryDescription;

                        //Start rsushilaggar MITS 21984 MITS 14-09-2010
                        if (sInjuryDesc.Length > EventDescLen)
                        {
                            sInjuryDesc = sInjuryDesc.Substring(0, EventDescLen);
                            sInjuryDesc = sInjuryDesc + "...";
                        }
                        //End rsushilaggar
					}

					//sClaimantEid
					if(!sClaimantEid.Equals("") && !sClaimantEid.Equals("0"))
					{
						AddTagValue(objClaimNode, "OtherIdentifier", "csc:ClaimantId", "OtherId", sClaimantEid, nsmgr);
					}
					else	//remove the OtherIdentifier tag if Claimant Eid is null or 0
					{
						RemoveTag(objClaimNode, "OtherIdentifier", "csc:ClaimantId", nsmgr);
					}


					if(objClaimNode.SelectNodes("//csc:SurnameLong", nsmgr).Count > 0)
					{
						objClaimNode.SelectNodes("//csc:SurnameLong", nsmgr).Item(0).InnerText = sClaimantLastName;
					}


					if(objClaimNode.SelectNodes("//csc:GivenNameLong", nsmgr).Count > 0)
					{
						objClaimNode.SelectNodes("//csc:GivenNameLong", nsmgr).Item(0).InnerText = sClaimantFirstName;
					}


					if(objClaimNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Count > 0)
					{
						objClaimNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Item(0).InnerText = sClaimantMiddleName;
					}


					if(objClaimNode.SelectNodes("//BirthDt").Count > 0)
					{
						objClaimNode.SelectNodes("//BirthDt").Item(0).InnerText = sClaimantBirthDt;
					}


					if(objClaimNode.SelectNodes("//EventsDesc", nsmgr).Count > 0)
					{
						objClaimNode.SelectNodes("//EventsDesc", nsmgr).Item(0).InnerText = sInjuryDesc;
					}


//TODO: Manoj- Following code commented because we won't send any Policy data from RMX to LSS as per PCR 1
					x = -1;
//					iCovCount = objClaim.PrimaryPolicy.PolicyXCvgTypeList.Count;
//					if(iCovCount > 0)
//					{
//						sCoverageCd = new String[iCovCount];
//						sCoverageCdDesc = new String[iCovCount];
//						sNatureOfInjCd = new String[iCovCount];
//						sNatureOfInjCdDesc = new String[iCovCount];
//						sIndemnityIncurredAmt = new String[iCovCount];
//
//						//objNode = objClaimXml.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq/ClaimsParty");
//						//objNode.ParentNode.InsertAfter(objNode.Clone(), objNode);
//
//						x = -1;
//						foreach (PolicyXCvgType objPolicyXCvgType in objClaim.PrimaryPolicy.PolicyXCvgTypeList)
//						{
//							x = x + 1;
//
//							sCoverageCd[x] = objCache.GetShortCode(objPolicyXCvgType.CoverageTypeCode);
//							sCoverageCdDesc[x] = objCache.GetCodeDesc(objPolicyXCvgType.CoverageTypeCode);
//							//sNatureOfInjCd[x] = 
//							//sNatureOfInjCdDesc[x] = 
//							//sIndemnityIncurredAmt[x] = 
//
//							if(objClaimNode.SelectNodes("//csc:CoverageCd", nsmgr).Count > 0)
//							{
//								objClaimNode.SelectNodes("//csc:CoverageCd", nsmgr).Item(0).InnerText = sCoverageCd[x];
//							}
//		
//							if(objClaimNode.SelectNodes("//csc:CoverageCdDesc", nsmgr).Count > 0)
//							{
//								objClaimNode.SelectNodes("//csc:CoverageCdDesc", nsmgr).Item(0).InnerText = sCoverageCdDesc[x];
//							}
//						}
//					}


					//If there is no cvg to export and Coverage tag exists in XML then remove all it's children from XML
					if(x < 0 && objClaimNode.SelectNodes("//csc:Coverage", nsmgr).Count > 0)
					{
						objClaimNode.SelectNodes("//csc:Coverage", nsmgr).Item(0).ParentNode.RemoveChild(objClaimNode.SelectNodes("//csc:Coverage", nsmgr).Item(0));
					}


					//Manoj - Changed logic

					//sbClaimXml.Append(sRestXml);
					//foreach (Claimant objClaimant in objClaim.ClaimantList)
					//{
					//	sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlClaimantData));
					//	sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlClaimantName));
					//	sClaimantName=objClaimant.ClaimantEntity.FirstName +" "+objClaimant.ClaimantEntity.LastName;
					//	sClaimantName=CommonFunctions.RemoveAmparsand(sClaimantName);
					//	sbClaimXml.Append(sClaimantName);
					//	sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlClaimantName));
					//	sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlCoverageData));
					//	sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlCoverageCd));
					//	sCoverageCode=CommonFunctions.RemoveAmparsand(sCoverageCode);
					//	sbClaimXml.Append(sCoverageCode);
					//	sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlCoverageCd));
					//
					//	sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlCoverageCdDesc));
					//	sCoverageCodeDesc=CommonFunctions.RemoveAmparsand(sCoverageCodeDesc);
					//	sbClaimXml.Append(sCoverageCodeDesc);
					//	sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlCoverageCdDesc));
					//	sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlSeverityCd));
					//	sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlSeverityCd));
					//	sbClaimXml.Append(CommonFunctions.CreateStartTag(sXmlSeverityDesc));
					//	sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlSeverityDesc));
					//	sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlCoverageData));
					//	sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlClaimantData));
					//}
					//sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlClaimRecord));


					objClaimXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objClaimNode);
				}
				sClaimId="";
				//sbClaimXml.Append(CommonFunctions.CreateEndTag(sXmlRoot));
				objReader.Close();
				objDMF.Dispose();


				//Manoj - Changed logic
				//sbSQL.Remove(0,sbSQL.ToString().Length);
				//
				////sbSQL.Append("UPDATE WZ_LA_INFO SET CLAIM_DTTM='"+sCurrentTime+"' WHERE ROW_ID=1");
				//sbSQL.Append("UPDATE RM_LSS_CLAIM_EXP SET SENT_FLAG = -1, DTTM_SENT = '" + sCurrentTime + "' WHERE TYPE_TEXT = 'CLAIM' AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
				//
				//objConn=DbFactory.GetDbConnection(m_sDSN);
				//objConn.Open();
				//objConn.ExecuteNonQuery(sbSQL.ToString());
				//objConn.Close();
				//objConn=null;
				//
				//p_sXml=sbClaimXml.ToString();

				objClaimXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objClaimXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq").Item(0));

				if(iClaimCount > 0) 
					p_sXml=objClaimXml.OuterXml;
				else
					p_sXml = "";			//Manoj: if there is no claim to export then don't call web service

			}
			//catch (RMAppException p_objException)
			//{
			//	throw p_objException;
			//}
			catch (Exception p_objException)
			{
				//throw new RMAppException(Globalization.GetString("LSSExportProcess.Export.Error"), p_objException);
				throw p_objException;
			}
			finally
			{
				sbSQL=null;
				//sbClaimXml=null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objClaim != null)
                {
                    objClaim.Dispose();
                }
                if (objEvent != null)
                {
                    objEvent.Dispose();
                }
                if (objReaderTemp != null)
                {
                    objReaderTemp.Dispose();
                }
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
				if (objReader!=null)
				{
					if (!objReader.IsClosed)
						objReader.Close();
					objReader=null;
				}
				if (objConn!=null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn=null;
				}
				//objXmlAppConfig=null;
				objClaimNode = null;
				nsmgr = null;
				objClaimXml=null;
			}
			return true;
		}
		#endregion

		#region Payment Status Export function
		/// Name		: PaymentStatusExport
		/// Author		: Manoj Agrawal
		/// Date Created: 07/31/2006
		/// ************************************************************
		/// <summary>		
		/// This method is used to export Diary from RMX to LSS on Payment status update
		/// </summary>
		public bool PaymentStatusExport(ref string p_sXml, ref string sExpPayments, string sCurrentTime)
			{
			string sRqUID = "";
			string sMainRqUID = "";
			string sInvoiceId = "";
			string sTransId = "";

			StringBuilder sbSQL=null;

			DataModelFactory objDMF=null;
			DbReader objReader=null;
			DbConnection objConn=null;

			XmlDocument objPaymentXml = null;
			XmlNamespaceManager nsmgr = null;

            Funds objFunds = null;//Sumit
			int iPaymentCount;

			

			XmlNode objPaymentNode=null;

			sExpPayments = "";

			try
			{
				
				if(!File.Exists(sPaymentExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("LSS.LoadFile.FileNotFound", m_iClientId)));

				objPaymentXml=new XmlDocument();
				try
				{
					objPaymentXml.Load(sPaymentExportXmlPath);
				}
				catch(Exception p_objException)
				{
                    throw new RMAppException(Globalization.GetString("LSSExportProcess.PaymentStatusExportXmlNotFound", m_iClientId), p_objException);
				}

				sbSQL=new StringBuilder();
				//sbSQL.Append("SELECT DISTINCT FS.LSS_INVOICE_ID FROM FUNDS_SUPP FS WHERE FS.TRANS_ID IN (SELECT E.TRANS_ID FROM RM_LSS_PAYMNT_EXP E WHERE E.TYPE_TEXT = 'PAYMENT' AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0))");
				sbSQL.Append("SELECT DISTINCT FS.LSS_INVOICE_ID, E.TRANS_ID FROM FUNDS_SUPP FS, RM_LSS_PAYMNT_EXP E WHERE FS.TRANS_ID = E.TRANS_ID AND E.TYPE_TEXT = 'PAYMENT' AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0)");
				objReader=DbFactory.GetDbReader(this.RiskmasterConnectionString,sbSQL.ToString());
                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);

				iPaymentCount = 0;

				sMainRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time
				sRqUID = sMainRqUID;			//set the default value for sRqUID same as sMainRqUID
				foreach (XmlNode objRqNode in objPaymentXml.GetElementsByTagName("RqUID"))
				{
					objRqNode.InnerText = sMainRqUID;
				}

				foreach (XmlNode objOrg in objPaymentXml.GetElementsByTagName("Org"))
				{
					objOrg.InnerText = m_sDsnName;
				}

				nsmgr = new XmlNamespaceManager(objPaymentXml.NameTable);
				nsmgr.AddNamespace("csc", objPaymentXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				while (objReader.Read())
				{
					sInvoiceId = "";
					sTransId = "";
                    objFunds = null;//Sumit

					sInvoiceId = Conversion.ConvertObjToStr(objReader.GetValue(0));
					sTransId = Conversion.ConvertObjToStr(objReader.GetValue(1));

					iPaymentCount = iPaymentCount + 1;

					objPaymentNode = objPaymentXml.GetElementsByTagName("Diary", objPaymentXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc")).Item(0).Clone();

					sExpPayments = sExpPayments + " " + sInvoiceId;
					sExpPayments = sExpPayments.Trim();


					if(objPaymentNode.SelectNodes("RqUID").Count > 0)
					{
						sRqUID = Guid.NewGuid().ToString();						//Manoj - this will generate a unique Req ID each time
						objPaymentNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
					}


					objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
					objConn.Open();
					objConn.ExecuteNonQuery("UPDATE RM_LSS_PAYMNT_EXP SET REQ_ID = '" + sRqUID + "' WHERE TRANS_ID = " + sTransId + " AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
					objConn.Close();
					//objConn=null;


					if(!sInvoiceId.Equals(""))
					{
						AddTagValue(objPaymentNode, "OtherIdentifier", "csc:InvoiceId", "OtherId", sInvoiceId, nsmgr);
					}
					else	//remove the OtherIdentifier tag if sInvoiceId is blank
					{
						RemoveTag(objPaymentNode, "OtherIdentifier", "csc:InvoiceId", nsmgr);
					}

                    if (!sInvoiceId.Equals(""))//Sumit
                    {
                        objFunds = (Funds)objDMF.GetDataModelObject("Funds", false);//Sumit
                        objFunds.MoveTo(Conversion.ConvertStrToInteger(sTransId));//Sumit
                        if (objPaymentNode.SelectNodes("//csc:TaskDescription", nsmgr).Count > 0)
                        {
                            objPaymentNode.SelectNodes("//csc:TaskDescription", nsmgr).Item(0).InnerText = objFunds.VoidReason;
                        }
                    }//End-Sumit
					objPaymentXml.SelectSingleNode("/ACORD/ExtensionsSvcRq").AppendChild(objPaymentNode);
				}
				sInvoiceId = "";
				objReader.Close();
				objDMF.Dispose();

				objPaymentXml.SelectSingleNode("/ACORD/ExtensionsSvcRq").RemoveChild(objPaymentXml.SelectNodes("/ACORD/ExtensionsSvcRq/csc:Diary", nsmgr).Item(0));

				if(iPaymentCount > 0) 
					p_sXml=objPaymentXml.OuterXml;
				else
					p_sXml = "";			//Manoj: if there is no record to export then don't call web service

			}
			//catch (RMAppException p_objException)
			//{
			//	throw p_objException;
			//}
			catch (Exception p_objException)
			{
				//throw new RMAppException(Globalization.GetString("LSSExportProcess.Export.Error"), p_objException);
				throw p_objException;
			}
			finally
			{
				sbSQL=null;
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
				if (objReader!=null)
				{
					if (!objReader.IsClosed)
						objReader.Close();
					objReader=null;
				}
				if (objConn!=null)
				{
					if (objConn.State == ConnectionState.Open)
					{
						objConn.Close();
					}
					objConn=null;
				}
				objPaymentNode = null;
				nsmgr = null;
				objPaymentXml=null;
			}
			return true;
		}
		#endregion

        #region Check Export function
        /// Name		: CheckExport
        /// Author		: Sumit Naresh Kumar
        /// Date Created: 12/29/2008
        /// ************************************************************
        /// <summary>		
        /// This method is used to export Check details from RMX to LSS when checks are marked as cleared
        /// </summary>
        public bool CheckExport(ref string p_sXml, ref string sExpChecks, string sCurrentTime)
        {
            int iCheckCount;
            int iTransId;

            string sRqUID = "";
            string sMainRqUID = "";
            string sInvoiceId = "";
            string sInvoiceHistoryId = "";

            //rsushilaggar modified the file to fix the LSS issue
            //string sCheckExportXmlPath = "";

            StringBuilder sbSQL = null;

            DataModelFactory objDMF = null;
            DbReader objReader = null;
            UserLogin objUserLogin = null;
            DbConnection objConn = null;

            XmlDocument objCheckXml = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode objCheckNode = null;

            Funds objFunds = null;

            sExpChecks = "";
            //rsushilaggar Date 10/01/2010 MITS 21970
            Riskmaster.Settings.SysSettings objSettings = new Riskmaster.Settings.SysSettings(this.RiskmasterConnectionString, m_iClientId); 
            
            try
            {
                //rsushilaggar MITS 21970 /22035 DATE 10/01/2010 
                if (objSettings != null && !objSettings.LssChecksOnHold)
                {
                    //LocalCache objChache = 
                    if (!File.Exists(sCheckExportXmlPath))
                        throw new FileInputOutputException(String.Format(Globalization.GetString("LSS.LoadFile.FileNotFound", m_iClientId)));

                    objCheckXml = new XmlDocument();
                    try
                    {
                        objCheckXml.Load(sCheckExportXmlPath);
                    }
                    catch (Exception p_objException)
                    {
                        throw new RMAppException(Globalization.GetString("LSSExportProcess.CheckExportXmlNotFound", m_iClientId), p_objException);
                    }

                    sbSQL = new StringBuilder();
                    sbSQL.Append("SELECT DISTINCT FS.LSS_INVOICE_ID, E.TRANS_ID, FS.LSS_HISTORY_ID FROM FUNDS_SUPP FS, RM_LSS_CHECK_EXP E WHERE FS.TRANS_ID = E.TRANS_ID AND E.TYPE_TEXT = 'CHECK' AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0)");
                    objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sbSQL.ToString());
                    objUserLogin = new UserLogin(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                    objDMF = new DataModelFactory(objUserLogin, m_iClientId);

                    iCheckCount = 0;

                    sMainRqUID = Guid.NewGuid().ToString();						//Sumit - this will generate a unique Req ID each time
                    sRqUID = sMainRqUID;			//set the default value for sRqUID same as sMainRqUID
                    foreach (XmlNode objRqNode in objCheckXml.GetElementsByTagName("RqUID"))
                    {
                        objRqNode.InnerText = sMainRqUID;
                    }

                    foreach (XmlNode objOrg in objCheckXml.GetElementsByTagName("Org"))
                    {
                        objOrg.InnerText = m_sDsnName;
                    }

                    nsmgr = new XmlNamespaceManager(objCheckXml.NameTable);
                    nsmgr.AddNamespace("csc", objCheckXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                    while (objReader.Read())
                    {
                        objFunds = null;

                        sInvoiceId = "";
                        iTransId = 0;

                        sInvoiceId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        iTransId = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);
                        sInvoiceHistoryId = Conversion.ConvertObjToStr(objReader.GetValue(2));

                        iCheckCount = iCheckCount + 1;
                        objCheckNode = objCheckXml.GetElementsByTagName("ClaimsSubsequentRptSubmitRq").Item(0).Clone();

                        objFunds = (Funds)objDMF.GetDataModelObject("Funds", false);
                        objFunds.MoveTo(iTransId);

                        sExpChecks = sExpChecks + " " + sInvoiceId;
                        sExpChecks = sExpChecks.Trim();


                        if (objCheckNode.SelectNodes("RqUID").Count > 0)
                        {
                            sRqUID = Guid.NewGuid().ToString();						//Sumit - this will generate a unique Req ID each time
                            objCheckNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
                        }

                        objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                        objConn.Open();
                        objConn.ExecuteNonQuery("UPDATE RM_LSS_CHECK_EXP SET REQ_ID = '" + sRqUID + "' WHERE TRANS_ID = " + iTransId + " AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
                        objConn.Close();

                        if (!sInvoiceId.Equals(""))
                        {
                            AddTagValue(objCheckNode, "OtherIdentifier", "csc:InvoiceId", "OtherId", sInvoiceId, nsmgr);
                        }
                        else	//remove the OtherIdentifier tag if sInvoiceId is blank
                        {
                            RemoveTag(objCheckNode, "OtherIdentifier", "csc:InvoiceId", nsmgr);
                        }

                        if (!sInvoiceHistoryId.Equals(""))
                        {
                            AddTagValue(objCheckNode, "OtherIdentifier", "csc:InvoiceHistoryId", "OtherId", sInvoiceHistoryId, nsmgr);
                        }
                        else	//remove the OtherIdentifier tag if sInvoiceId is blank
                        {
                            RemoveTag(objCheckNode, "OtherIdentifier", "csc:InvoiceHistoryId", nsmgr);
                        }

                        foreach (XmlNode objInsurerNode in objCheckNode.SelectNodes("//csc:InsurerId", nsmgr))
                        {
                            objInsurerNode.InnerText = objFunds.ClaimNumber;
                        }

                        if (objCheckNode.SelectNodes("//Amt", nsmgr).Count > 0)
                        {
                            if(objSettings.UseMultiCurrency==-1)
                                //skhare7 r8 Multicurrency as In LSS Claim currency amount will be used
                                objCheckNode.SelectNodes("//Amt", nsmgr).Item(0).InnerText = Conversion.ConvertObjToStr(objFunds.ClaimCurrencyAmount);
                            else
                           objCheckNode.SelectNodes("//Amt", nsmgr).Item(0).InnerText = Conversion.ConvertObjToStr(objFunds.Amount);
                          
                        }

                        if (objCheckNode.SelectNodes("//PaymentDt", nsmgr).Count > 0)
                        {
                            objCheckNode.SelectNodes("//PaymentDt", nsmgr).Item(0).InnerText = objFunds.DateOfCheck;
                        }

                        if (objCheckNode.SelectNodes("//CheckNumber", nsmgr).Count > 0)
                        {
                            objCheckNode.SelectNodes("//CheckNumber", nsmgr).Item(0).InnerText = Conversion.ConvertObjToStr(objFunds.TransNumber);
                        }

                        if (objCheckNode.SelectNodes("//csc:ClearedDT", nsmgr).Count > 0)
                        {
                            objCheckNode.SelectNodes("//csc:ClearedDT", nsmgr).Item(0).InnerText = objFunds.VoidDate;
                        }

                        objCheckXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objCheckNode);
                    }
                    objReader.Close();
                    objDMF.Dispose();

                    objCheckXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objCheckXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq").Item(0));

                    if (iCheckCount > 0)
                        p_sXml = objCheckXml.OuterXml;
                    else
                        p_sXml = "";			//Sumit: if there is no record to export then don't call web service
                }//rsushilaggar MITS 21970
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
            finally
            {
                sbSQL = null;
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                        objReader.Close();
                    objReader = null;
                }
                if (objConn != null)
                {
                    if (objConn.State == ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn = null;
                }

                if (objFunds != null)
                {
                    objFunds.Dispose();
                }

                objUserLogin = null;
                objCheckNode = null;
                nsmgr = null;
                objCheckXml = null;
            }
            return true;
        }
        //Added by sharishkumar for Mits 35472
        public bool ReserveExport(ref string p_sXml, ref string sExpClaimNumbers, string sCurrentTime)
        {
            //Variable for claim node child
            int iClaimantId = 0;
            int iClaimantEid = 0;
            int iClaimId = 0;
            string sClaimNumber = string.Empty;
            //Variable for claimant node child
            
            string sRqUID = string.Empty;
            string sMainRqUID = string.Empty;
           
          
            string sLOBCd = string.Empty;
            string sClaimtypeCode = string.Empty;
            string sEventDesc = string.Empty;
            string sLossDt = string.Empty;		//YYYYMMDD
            string sReportedDt = string.Empty;		//YYYYMMDD            
           
            DataModelFactory objDMF = null;
            DbReader objReader = null;
            DbConnection objConn = null;
            DbReader objReaderLssRes = null;                      
            int iAdjusterUserId = 0;
            string sAdjusterUserId = string.Empty;
            int iSupervisorUserId = 0;
            string sSupervisorUserId = string.Empty;

            int iManagerUserId = 0;
            string sManagerUserId = string.Empty;            
            
           
            Claim objClaim = null;
            Claimant objClaimant = null;
            Entity objEntity = null;
            ReserveCurrent objReserveCurr = null;
            CvgXLoss objCvgXLoss = null;
            PolicyXCvgType objPolicyXCvgType = null;
            PolicyXUnit objPolicyXUnit = null;
            Policy objPolicy = null;

            LocalCache objCache = null;
            XmlDocument objReserveXml = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode objReserveNode = null;
            XmlNode objFinancialNode = null;
            Event objEvent = null;
          
            string sXml = string.Empty;
            

            StringBuilder sbSQL = null;
            
            int iClaimCount;
            int sRcRowId = 0;
            int iOldResGroupId = 0;
            int sResGroupId = 0;
            string ResRowId = string.Empty;           

            int iPolicyID = 0;
            string sPolicyName = string.Empty;
            string sPolicySymbol = string.Empty;
            string sPolicyNumber = string.Empty;
            int iPolicyStatusCode = 0;
            int iPolicyInsurerEID = 0;
            string sPolictEffDate = string.Empty;
            string sReserveTypeCode = string.Empty;
            string sReserveTypeDesc = string.Empty;
            int iReserveBalance= 0;
            string sCoverageCode = string.Empty;
            string scoverageDesc=string.Empty;
            string sLossTypeCd = string.Empty;
            string sLossTypeDesc=string.Empty;
            int iPolicyXUnitId = 0;
            int iCoverageXUnitId = 0;
            string sUnitType = string.Empty;
            
            string sSiteNumber = string.Empty;
            string sSiteName = string.Empty;
            string sSiteSic = string.Empty;
            string sVIN = string.Empty;
            string sVehShortCode = string.Empty;
            string sVehShortDesc = string.Empty;
            string sPIN = string.Empty;
            try
            {
                if (!File.Exists(sReserveExportXmlPath))
                    throw new FileInputOutputException(String.Format(Globalization.GetString("LSS.LoadFile.FileNotFound", m_iClientId)));

                objReserveXml = new XmlDocument();
                try
                {
                    objReserveXml.Load(sReserveExportXmlPath);
                }
                catch (Exception p_objException)
                {
                    throw new RMAppException(Globalization.GetString("LSSExportProcess.ReserveExportXmlNotFound", m_iClientId), p_objException);
                }

                sbSQL = new StringBuilder();                
                
                sbSQL.Append("SELECT RESERVE_ID,GROUP_ID FROM RM_LSS_RESERVE_EXP E WHERE E.TYPE_TEXT = 'RESERVE' AND E.DTTM_CHANGED <= '" + sCurrentTime + "' AND (E.SENT_FLAG IS NULL OR E.SENT_FLAG = 0)");
                objReaderLssRes = DbFactory.GetDbReader(this.RiskmasterConnectionString, sbSQL.ToString());
                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);

                objCache = new LocalCache(objDMF.Context.DbConn.ConnectionString, m_iClientId);

                iClaimCount = 0;

                sMainRqUID = Guid.NewGuid().ToString();						// this will generate a unique Req ID each time Reserve Data Exchange 
                sRqUID = sMainRqUID;			                            //set the default value for sRqUID same as sMainRqUID
                foreach (XmlNode objRqNode in objReserveXml.GetElementsByTagName("RqUID"))
                {
                    objRqNode.InnerText = sMainRqUID;
                }

                foreach (XmlNode objOrg in objReserveXml.GetElementsByTagName("Org"))
                {
                    objOrg.InnerText = m_sDsnName;
                }

                nsmgr = new XmlNamespaceManager(objReserveXml.NameTable);
                nsmgr.AddNamespace("csc", objReserveXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                while (objReaderLssRes.Read())
                {
                    sRcRowId = Conversion.ConvertObjToInt(objReaderLssRes.GetValue("RESERVE_ID"), m_iClientId);
                    sResGroupId = Conversion.ConvertObjToInt(objReaderLssRes.GetValue("GROUP_ID"), m_iClientId);

                    if (iOldResGroupId != sResGroupId)
                    {
                        objReserveNode = objReserveXml.GetElementsByTagName("ClaimsNotificationAddRq").Item(0).Clone();

                        iClaimCount = iClaimCount + 1;

                        objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT RC.CLAIM_ID, RC.CLAIMANT_EID,C.CLAIMANT_ROW_ID FROM RESERVE_CURRENT RC,CLAIMANT C WHERE C.CLAIMANT_EID = RC.CLAIMANT_EID AND RC.CLAIM_ID = C.CLAIM_ID AND RC.RC_ROW_ID = " + sRcRowId);

                        if (objReader.Read())
                        {
                            iClaimId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                            iClaimantEid = Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);
                            iClaimantId = Conversion.ConvertObjToInt(objReader.GetValue(2), m_iClientId);
                        }
                        objReader.Close();

                        objClaim = (Claim)objDMF.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(iClaimId);                        
                       
                        sClaimNumber = CommonFunctions.RemoveAmparsand(objClaim.ClaimNumber);                      


                        foreach (XmlNode objInsNode in objReserveNode.SelectNodes("//csc:InsurerId", nsmgr))
                        {
                            objInsNode.InnerText = sClaimNumber;
                        }

                        objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                        objEntity.MoveTo(iClaimantEid);

                        if (objReserveNode.SelectNodes("RqUID").Count > 0)
                        {
                            sRqUID = Guid.NewGuid().ToString();						// this will generate a unique Req ID each time
                            objReserveNode.SelectNodes("RqUID").Item(0).InnerText = sRqUID;
                        }
                       
                            objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                            objConn.Open();
                            objConn.ExecuteNonQuery("UPDATE RM_LSS_RESERVE_EXP SET REQ_ID = '" + sRqUID + "' WHERE RESERVE_ID = '" + sRcRowId + "' AND TYPE_TEXT = 'RESERVE' AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
                            objConn.Close();                       

                        if (objReserveNode.SelectNodes("//TransactionRequestDt", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//TransactionRequestDt", nsmgr).Item(0).InnerText = Conversion.GetDate(DateTime.Now.ToString());
                        }

                        foreach (ClaimAdjuster objAdjuster in objClaim.AdjusterList)
                        {
                            if (objAdjuster.CurrentAdjFlag == true)
                            {
                                //Put Login name from Security database into sAdjusterUserId
                                objReader = DbFactory.GetDbReader(SecurityDatabase.Dsn, "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + objAdjuster.AdjusterEntity.RMUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");
                                if (objReader.Read())
                                {
                                    iAdjusterUserId = objAdjuster.AdjusterEntity.RMUserId;
                                    sAdjusterUserId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                                }
                                objReader.Close();

                            }
                        }

                        if (!sAdjusterUserId.Equals(""))
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "csc:AdjusterUserId", "OtherId", sAdjusterUserId, nsmgr);
                        }
                        else	//remove the OtherIdentifier tag if Adjuster login is blank
                        {
                            RemoveTag(objReserveNode, "OtherIdentifier", "csc:AdjusterUserId", nsmgr);
                        }

                        //fetch from database based on Adjuster and put Supervisor Login name into sSupervisorUserId
                        objReader = DbFactory.GetDbReader(SecurityDatabase.Dsn, "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iAdjusterUserId.ToString());
                        if (objReader.Read())
                        {
                            iSupervisorUserId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        }
                        objReader.Close();

                        objReader = DbFactory.GetDbReader(SecurityDatabase.Dsn, "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + iSupervisorUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");
                        if (objReader.Read())
                        {
                            sSupervisorUserId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                        objReader.Close();

                        if (!sSupervisorUserId.Equals(""))
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "csc:SupervisorUserId", "OtherId", sSupervisorUserId, nsmgr);
                        }
                        else	//remove the OtherIdentifier tag if Supervisor login is blank
                        {
                            RemoveTag(objReserveNode, "OtherIdentifier", "csc:SupervisorUserId", nsmgr);
                        }

                        // fetch from database based on Adjuster and put Manager Login name into sManagerUserId
                        objReader = DbFactory.GetDbReader(SecurityDatabase.Dsn, "SELECT MANAGER_ID FROM USER_TABLE WHERE USER_ID = " + iSupervisorUserId.ToString());
                        if (objReader.Read())
                        {
                            iManagerUserId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        }
                        objReader.Close();

                        objReader = DbFactory.GetDbReader(SecurityDatabase.Dsn, "SELECT LOGIN_NAME FROM USER_DETAILS_TABLE WHERE USER_ID = " + iManagerUserId.ToString() + " AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");
                        if (objReader.Read())
                        {
                            sManagerUserId = Conversion.ConvertObjToStr(objReader.GetValue(0));
                        }
                        objReader.Close();

                        if (!sManagerUserId.Equals(""))
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "csc:ManagerUserId", "OtherId", sManagerUserId, nsmgr);
                        }
                        else	//remove the OtherIdentifier tag if Manager login is blank
                        {
                            RemoveTag(objReserveNode, "OtherIdentifier", "csc:ManagerUserId", nsmgr);
                        }

                        objEvent = (Event)objClaim.Parent;
                        sReportedDt = objEvent.DateReported;			//YYYYMMDD
                        if (objReserveNode.SelectNodes("//ReportedDt").Count > 0)
                        {
                            objReserveNode.SelectNodes("//ReportedDt").Item(0).InnerText = sReportedDt;
                        }

                        sLossDt = objEvent.DateOfEvent;			//YYYYMMDD
                        if (objReserveNode.SelectNodes("//LossDt").Count > 0)
                        {
                            objReserveNode.SelectNodes("//LossDt").Item(0).InnerText = sLossDt;
                        }

                        sEventDesc = objEvent.EventDescription;
                        EventDescLen = Conversion.ConvertStrToInteger(RMConfigurationManager.GetNameValueSectionSettings("LSSInterface", this.RiskmasterConnectionString, m_iClientId)["MaxEventDescLengthAllowed"]);
                        if (sEventDesc.Length > EventDescLen)
                        {
                            sEventDesc = sEventDesc.Substring(0, EventDescLen);
                            sEventDesc = sEventDesc + "...";
                        }

                        if (objReserveNode.SelectNodes("//IncidentDesc").Count > 0)
                        {
                            objReserveNode.SelectNodes("//IncidentDesc").Item(0).InnerText = sEventDesc;
                        }

                        
                        objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                        objEntity.MoveTo(iClaimantEid);

                        if (!iClaimantId.Equals(0))
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "csc:ClaimantId", "OtherId", iClaimantId.ToString(), nsmgr);
                        }

                        if (objReserveNode.SelectNodes("//csc:SurnameLong", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//csc:SurnameLong", nsmgr).Item(0).InnerText = objEntity.LastName;
                        }
                        if (objReserveNode.SelectNodes("//csc:GivenNameLong", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//csc:GivenNameLong", nsmgr).Item(0).InnerText = objEntity.FirstName;
                        }
                        if (objReserveNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//csc:OtherGivenNameLong", nsmgr).Item(0).InnerText = objEntity.MiddleName;
                        }
                        if (objReserveNode.SelectNodes("//BirthDt").Count > 0)
                        {
                            objReserveNode.SelectNodes("//BirthDt").Item(0).InnerText = objEntity.BirthDate;
                        }                       

                        if (objReserveNode.SelectNodes("//CarrierFlag").Count > 0)
                        {
                            objReserveNode.SelectNodes("//CarrierFlag").Item(0).InnerText = "Y";
                        }

                        sClaimtypeCode = objCache.GetShortCode(objClaim.ClaimTypeCode);
                        if (objReserveNode.SelectNodes("//LOBCd").Count > 0)
                        {
                            objReserveNode.SelectNodes("//LOBCd").Item(0).InnerText = sClaimtypeCode;
                        }

                        if (!(sRcRowId==0))
                        {
                            string strPolDetails = "SELECT POLICY.POLICY_ID,POLICY.POLICY_NAME,POLICY.POLICY_NUMBER,POLICY.POLICY_STATUS_CODE,POLICY.EFFECTIVE_DATE,POLICY.POLICY_SYMBOL,POLICY.INSURER_EID,A.SHORT_CODE,A.CODE_DESC,RC.BALANCE_AMOUNT,B.SHORT_CODE,B.CODE_DESC,C.SHORT_CODE,C.CODE_DESC,PCVG.POLICY_UNIT_ROW_ID,PCVG.POLCVG_ROW_ID,POLICY_X_UNIT.UNIT_TYPE"
                                          + " FROM RESERVE_CURRENT RC,COVERAGE_X_LOSS CVG,POLICY_X_CVG_TYPE PCVG,CODES_TEXT A, CODES_TEXT B ,CODES_TEXT C,POLICY_X_UNIT,POLICY  WHERE RC.RC_ROW_ID = " + sRcRowId + " AND RC.RESERVE_TYPE_CODE = A.CODE_ID AND RC.POLCVG_LOSS_ROW_ID=CVG.CVG_LOSS_ROW_ID AND PCVG.POLCVG_ROW_ID=CVG.POLCVG_ROW_ID AND PCVG.COVERAGE_TYPE_CODE =B.CODE_ID"
                                          + " AND CVG.LOSS_CODE= C.CODE_ID AND PCVG.POLICY_UNIT_ROW_ID=POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_ID= POLICY.POLICY_ID";

                            objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, strPolDetails);
                            if (objReader.Read())
                            {
                                iPolicyID = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                                sPolicyName = Conversion.ConvertObjToStr(objReader.GetValue(1));
                                sPolicyNumber = Conversion.ConvertObjToStr(objReader.GetValue(2));
                                iPolicyStatusCode = Conversion.ConvertObjToInt(objReader.GetValue(3), m_iClientId);
                                sPolictEffDate=Conversion.ConvertObjToStr(objReader.GetValue(4));
                                sPolicySymbol = Conversion.ConvertObjToStr(objReader.GetValue(5));
                                iPolicyInsurerEID = Conversion.ConvertObjToInt(objReader.GetValue(6), m_iClientId);
                                sReserveTypeCode = Conversion.ConvertObjToStr(objReader.GetValue(7));
                                sReserveTypeDesc = Conversion.ConvertObjToStr(objReader.GetValue(8));
                                iReserveBalance = Conversion.ConvertObjToInt(objReader.GetValue(9), m_iClientId);
                                sCoverageCode = Conversion.ConvertObjToStr(objReader.GetValue(10));
                                scoverageDesc = Conversion.ConvertObjToStr(objReader.GetValue(11));
                                sLossTypeCd=Conversion.ConvertObjToStr(objReader.GetValue(12));
                                sLossTypeDesc = Conversion.ConvertObjToStr(objReader.GetValue(13));
                                iPolicyXUnitId = Conversion.ConvertObjToInt(objReader.GetValue(14), m_iClientId);
                                iCoverageXUnitId = Conversion.ConvertObjToInt(objReader.GetValue(15), m_iClientId);
                                sUnitType = Conversion.ConvertObjToStr(objReader.GetValue(16));
                            }
                            objReader.Close();
                        }

                        if (!sRcRowId.Equals(0))            //Reserve Id Table - RESERVE_CURRENT
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "RcRowId", "OtherId", sRcRowId.ToString(), nsmgr);
                        }

                        if (objReserveNode.SelectNodes("//ReserveTypeCd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ReserveTypeCd", nsmgr).Item(0).InnerText = sReserveTypeCode;
                        }

                        if (objReserveNode.SelectNodes("//ReserveTypeDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ReserveTypeDesc", nsmgr).Item(0).InnerText = sReserveTypeDesc;
                        }

                        if (objReserveNode.SelectNodes("//ReserveAmt", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//ReserveAmt", nsmgr).Item(0).InnerText = iReserveBalance.ToString();
                        }

                        //if (!objCvgXLoss.PolCvgRowId.Equals(0)) //Coverage id related to reserve Table - COVERAGE_X_LOSS
                        if(!iCoverageXUnitId.Equals(0))
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "CoverageXUnitId", "OtherId", iCoverageXUnitId.ToString(), nsmgr);
                        }

                        if (objReserveNode.SelectNodes("//CoverageCd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//CoverageCd", nsmgr).Item(0).InnerText = sCoverageCode;
                        }

                        if (objReserveNode.SelectNodes("//CoverageDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//CoverageDesc", nsmgr).Item(0).InnerText = scoverageDesc;
                        }

                        //        //Start Loss Type
                        sLOBCd = objClaim.LineOfBusCode.ToString();

                        if (sLOBCd == "241")
                        {
                            if (objReserveNode.SelectNodes("//TypeLossCauseCd", nsmgr).Count > 0)
                            {
                                objReserveNode.SelectNodes("//TypeLossCauseCd", nsmgr).Item(0).InnerText = sLossTypeCd;
                            }
                            if (objReserveNode.SelectNodes("//TypeLossCauseCdDesc", nsmgr).Count > 0)
                            {
                                objReserveNode.SelectNodes("//TypeLossCauseCdDesc", nsmgr).Item(0).InnerText = sLossTypeDesc;
                            }

                            //if (objReserveNode.SelectNodes("//LossCauseCdDesc", nsmgr).Count > 0)
                            //{
                            //    objReserveNode.SelectNodes("//LossCauseCdDesc", nsmgr).Item(0).InnerText = objClaim.LossDescription;
                            //}
                        }
                        else
                        {
                            XmlNode node = objReserveNode.SelectSingleNode("//Loss");
                            node.ParentNode.RemoveChild(node);                         
                        }
                        //End Loss Type

                        //   OR

                        //Start Disablity Type
                        if (sLOBCd == "243")
                        {
                            //if (objReserveNode.SelectNodes("//DisabilityCat", nsmgr).Count > 0)
                            //{
                            //    objReserveNode.SelectNodes("//DisabilityCat", nsmgr).Item(0).InnerText = objCache.GetShortCode(objCvgXLoss.DisablityCategory);
                            //}

                            if (objReserveNode.SelectNodes("//DisabilityCd", nsmgr).Count > 0)
                            {
                                objReserveNode.SelectNodes("//DisabilityCd", nsmgr).Item(0).InnerText = sLossTypeCd;
                            }

                            if (objReserveNode.SelectNodes("//DisabilityDesc", nsmgr).Count > 0)
                            {
                                objReserveNode.SelectNodes("//DisabilityDesc", nsmgr).Item(0).InnerText = sLossTypeDesc;
                            }
                        }
                        else
                        {
                            XmlNode node = objReserveNode.SelectSingleNode("//Disability");
                            node.ParentNode.RemoveChild(node);                         
                       
                        }
                        //End Disablity Type

                        if (!iPolicyXUnitId.Equals(0)) //Coverage id related to reserve Table - POLICY_X_UNIT
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "PolicyXUnitId", "OtherId", iPolicyXUnitId.ToString(), nsmgr);
                        }

                        if (objReserveNode.SelectNodes("//UnitType", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//UnitType", nsmgr).Item(0).InnerText = sUnitType;
                        }

                        switch (sUnitType)
                        {
                            case "V":
                                  string sVehDetails = "SELECT VIN,A.SHORT_CODE,A.CODE_DESC FROM VEHICLE,CODES_TEXT A WHERE UNIT_ID =" + iPolicyXUnitId.ToString() + "  AND A.CODE_ID= VEHICLE.UNIT_TYPE_CODE";
                                  objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sVehDetails);
                                    if (objReader.Read())
                                    {
                                         sVIN = Conversion.ConvertObjToStr(objReader.GetValue(0));
                                         sVehShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1));
                                         sVehShortDesc = Conversion.ConvertObjToStr(objReader.GetValue(2));
                                    }
                                    objReader.Close();

                                if (objReserveNode.SelectNodes("//Vin", nsmgr).Count > 0)
                                {
                                    objReserveNode.SelectNodes("//Vin", nsmgr).Item(0).InnerText = sVIN;
                                }
                                if (objReserveNode.SelectNodes("//UnitTypeCode", nsmgr).Count > 0)
                                {
                                    objReserveNode.SelectNodes("//UnitTypeCode", nsmgr).Item(0).InnerText = sVehShortCode;
                                }
                                if (objReserveNode.SelectNodes("//UnitTypeCodeDesc", nsmgr).Count > 0)
                                {
                                    objReserveNode.SelectNodes("//UnitTypeCodeDesc", nsmgr).Item(0).InnerText = sVehShortDesc;
                                }

                                break;

                            case "P":
                                string sPropDetails = "SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID =" + iPolicyXUnitId.ToString();
                                objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sPropDetails);
                                    if (objReader.Read())
                                    {
                                         sPIN = Conversion.ConvertObjToStr(objReader.GetValue(0));                                        
                                    }
                                    objReader.Close();
                                if (objReserveNode.SelectNodes("//Pin", nsmgr).Count > 0)
                                {
                                    objReserveNode.SelectNodes("//Pin", nsmgr).Item(0).InnerText = sPIN;
                                }

                                break;

                            case "S":
                                string sSiteDetails = "SELECT SITE_NUMBER,NAME,SIC FROM SITE_UNIT WHERE SITE_ID =" + iPolicyXUnitId.ToString();
                                objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSiteDetails);
                                    if (objReader.Read())
                                    {
                                        sSiteNumber = Conversion.ConvertObjToStr(objReader.GetValue(0));
                                        sSiteName = Conversion.ConvertObjToStr(objReader.GetValue(1));
                                        sSiteSic = Conversion.ConvertObjToStr(objReader.GetValue(2));
                                    }
                                    objReader.Close();                              
                                if (objReserveNode.SelectNodes("//SiteNumber", nsmgr).Count > 0)
                                {
                                    objReserveNode.SelectNodes("//SiteNumber", nsmgr).Item(0).InnerText = sSiteNumber;
                                }
                                if (objReserveNode.SelectNodes("//Name", nsmgr).Count > 0)
                                {
                                    objReserveNode.SelectNodes("//Name", nsmgr).Item(0).InnerText = sSiteName;
                                }
                                if (objReserveNode.SelectNodes("//SIC", nsmgr).Count > 0)
                                {
                                    objReserveNode.SelectNodes("//SIC", nsmgr).Item(0).InnerText = sSiteSic;
                                }

                                break;
                        }

                        if(!iPolicyID.Equals(0))
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "PolicyId", "OtherId", iPolicyID.ToString(), nsmgr);
                        }

                        if (objReserveNode.SelectNodes("//PolicyName", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PolicyName", nsmgr).Item(0).InnerText = sPolicyName;
                        }
                        if (objReserveNode.SelectNodes("//PolicyNumber", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PolicyNumber", nsmgr).Item(0).InnerText = sPolicyNumber;
                        }
                        if (objReserveNode.SelectNodes("//PolicyStatusCd", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PolicyStatusCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(iPolicyStatusCode);
                        }
                        if (objReserveNode.SelectNodes("//PolicyStatusDesc", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PolicyStatusDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(iPolicyStatusCode);
                        }
                        if (objReserveNode.SelectNodes("//PolicyEffDt", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PolicyEffDt", nsmgr).Item(0).InnerText = sPolictEffDate;
                        }
                        if (objReserveNode.SelectNodes("//PolicySymbol", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//PolicySymbol", nsmgr).Item(0).InnerText = sPolicySymbol;
                        }

                        objClaimant = (Claimant)objDMF.GetDataModelObject("Claimant", false);
                        objClaimant.MoveTo(iClaimantId);

                        if (objReserveNode.SelectNodes("//InsurerId", nsmgr).Count > 0)
                        {
                            objEntity.MoveTo(iPolicyInsurerEID);
                            string name = "";
                            if (!string.IsNullOrEmpty(objEntity.FirstName))
                                name = objEntity.LastName + ", " + objEntity.FirstName;
                            else
                                name = objEntity.LastName;
                            objReserveNode.SelectNodes("//InsurerId", nsmgr).Item(0).InnerText = name;
                        }

                        objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                        objEntity.MoveTo(iClaimantEid);


                        if (!iClaimantId.Equals(0))
                        {
                            AddTagValue(objReserveNode, "OtherIdentifier", "ClaimantId", "OtherId", iClaimantId.ToString(), nsmgr);
                        }

                        if (objReserveNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr).Item(0).InnerText = objEntity.LastName;
                        }
                        if (objReserveNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr).Item(0).InnerText = objEntity.FirstName;
                        }
                        if (objReserveNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr).Count > 0)
                        {
                            objReserveNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr).Item(0).InnerText = objEntity.MiddleName;
                        }
                        objReserveXml.SelectSingleNode("/ACORD/ClaimsSvcRq").AppendChild(objReserveNode);
                    }
                    else
                    {
                        objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                        objConn.Open();
                        objConn.ExecuteNonQuery("UPDATE RM_LSS_RESERVE_EXP SET REQ_ID = '" + sRqUID + "' WHERE RESERVE_ID = '" + sRcRowId + "' AND TYPE_TEXT = 'RESERVE' AND DTTM_CHANGED <= '" + sCurrentTime + "' AND (SENT_FLAG IS NULL OR SENT_FLAG = 0)");
                        objConn.Close();

                        objFinancialNode = objReserveXml.GetElementsByTagName("ReserveKey").Item(0).Clone();


                        if (!(sRcRowId == 0))
                        {
                            string strPolDetails = "SELECT POLICY.POLICY_ID,POLICY.POLICY_NAME,POLICY.POLICY_NUMBER,POLICY.POLICY_STATUS_CODE,POLICY.EFFECTIVE_DATE,POLICY.POLICY_SYMBOL,POLICY.INSURER_EID,A.SHORT_CODE,A.CODE_DESC,RC.BALANCE_AMOUNT,B.SHORT_CODE,B.CODE_DESC,C.SHORT_CODE,C.CODE_DESC,PCVG.POLICY_UNIT_ROW_ID,PCVG.POLCVG_ROW_ID,POLICY_X_UNIT.UNIT_TYPE"
                                          + " FROM RESERVE_CURRENT RC,COVERAGE_X_LOSS CVG,POLICY_X_CVG_TYPE PCVG,CODES_TEXT A, CODES_TEXT B ,CODES_TEXT C,POLICY_X_UNIT,POLICY  WHERE RC.RC_ROW_ID = " + sRcRowId + " AND RC.RESERVE_TYPE_CODE = A.CODE_ID AND RC.POLCVG_LOSS_ROW_ID=CVG.CVG_LOSS_ROW_ID AND PCVG.POLCVG_ROW_ID=CVG.POLCVG_ROW_ID AND PCVG.COVERAGE_TYPE_CODE =B.CODE_ID"
                                          + " AND CVG.LOSS_CODE= C.CODE_ID AND PCVG.POLICY_UNIT_ROW_ID=POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_ID= POLICY.POLICY_ID";

                            objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, strPolDetails);
                            if (objReader.Read())
                            {
                                iPolicyID = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                                sPolicyName = Conversion.ConvertObjToStr(objReader.GetValue(1));
                                sPolicyNumber = Conversion.ConvertObjToStr(objReader.GetValue(2));
                                iPolicyStatusCode = Conversion.ConvertObjToInt(objReader.GetValue(3), m_iClientId);
                                sPolictEffDate = Conversion.ConvertObjToStr(objReader.GetValue(4));
                                sPolicySymbol = Conversion.ConvertObjToStr(objReader.GetValue(5));
                                iPolicyInsurerEID = Conversion.ConvertObjToInt(objReader.GetValue(6), m_iClientId);
                                sReserveTypeCode = Conversion.ConvertObjToStr(objReader.GetValue(7));
                                sReserveTypeDesc = Conversion.ConvertObjToStr(objReader.GetValue(8));
                                iReserveBalance = Conversion.ConvertObjToInt(objReader.GetValue(9), m_iClientId);
                                sCoverageCode = Conversion.ConvertObjToStr(objReader.GetValue(10));
                                scoverageDesc = Conversion.ConvertObjToStr(objReader.GetValue(11));
                                sLossTypeCd = Conversion.ConvertObjToStr(objReader.GetValue(12));
                                sLossTypeDesc = Conversion.ConvertObjToStr(objReader.GetValue(13));
                                iPolicyXUnitId = Conversion.ConvertObjToInt(objReader.GetValue(14), m_iClientId);
                                iCoverageXUnitId = Conversion.ConvertObjToInt(objReader.GetValue(15), m_iClientId);
                                sUnitType = Conversion.ConvertObjToStr(objReader.GetValue(16));
                            }
                            objReader.Close();
                        }
                        if (!sRcRowId.Equals(0))            //Reserve Id Table - RESERVE_CURRENT
                        {
                            AddTagValue(objFinancialNode, "OtherIdentifier", "RcRowId", "OtherId", sRcRowId.ToString(), nsmgr);
                        }

                        if (objFinancialNode.SelectNodes("//ReserveTypeCd", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//ReserveTypeCd", nsmgr).Item(0).InnerText = sReserveTypeCode;
                        }

                        if (objFinancialNode.SelectNodes("//ReserveTypeDesc", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//ReserveTypeDesc", nsmgr).Item(0).InnerText = sReserveTypeDesc;
                        }

                        if (objFinancialNode.SelectNodes("//ReserveAmt", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//ReserveAmt", nsmgr).Item(0).InnerText = iReserveBalance.ToString();
                        }

                        //if (!objCvgXLoss.PolCvgRowId.Equals(0)) //Coverage id related to reserve Table - COVERAGE_X_LOSS
                        if (!iCoverageXUnitId.Equals(0))
                        {
                            AddTagValue(objFinancialNode, "OtherIdentifier", "CoverageXUnitId", "OtherId", iCoverageXUnitId.ToString(), nsmgr);
                        }

                        if (objFinancialNode.SelectNodes("//CoverageCd", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//CoverageCd", nsmgr).Item(0).InnerText =sCoverageCode;
                        }

                        if (objFinancialNode.SelectNodes("//CoverageDesc", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//CoverageDesc", nsmgr).Item(0).InnerText = scoverageDesc;
                        }

                        //        //Start Loss Type
                         sLOBCd = objClaim.LineOfBusCode.ToString();

                         if (sLOBCd == "241")
                         {
                             if (objFinancialNode.SelectNodes("//TypeLossCauseCd", nsmgr).Count > 0)
                             {
                                 objFinancialNode.SelectNodes("//TypeLossCauseCd", nsmgr).Item(0).InnerText = sLossTypeCd;
                             }
                             if (objFinancialNode.SelectNodes("//TypeLossCauseCdDesc", nsmgr).Count > 0)
                             {
                                 objFinancialNode.SelectNodes("//TypeLossCauseCdDesc", nsmgr).Item(0).InnerText = sLossTypeDesc;
                             }

                             //if (objFinancialNode.SelectNodes("//LossCauseCdDesc", nsmgr).Count > 0)
                             //{
                             //    objFinancialNode.SelectNodes("//LossCauseCdDesc", nsmgr).Item(0).InnerText = objClaim.LossDescription;
                             //}
                         }
                         else
                         {
                             XmlNode node = objFinancialNode.SelectSingleNode("//Loss");
                             node.ParentNode.RemoveChild(node);                            
                         }
                        //End Loss Type

                        //   OR

                        //Start Disablity Type
                         sLOBCd = objClaim.LineOfBusCode.ToString();

                         if (sLOBCd == "243")
                         {
                             if (objFinancialNode.SelectNodes("//DisabilityCat", nsmgr).Count > 0)
                             {
                                 objFinancialNode.SelectNodes("//DisabilityCat", nsmgr).Item(0).InnerText = sLossTypeCd;
                             }

                             if (objFinancialNode.SelectNodes("//DisabilityCd", nsmgr).Count > 0)
                             {
                                 objFinancialNode.SelectNodes("//DisabilityCd", nsmgr).Item(0).InnerText = sLossTypeDesc;
                             }

                             //if (objFinancialNode.SelectNodes("//DisabilityDesc", nsmgr).Count > 0)
                             //{
                             //    objFinancialNode.SelectNodes("//DisabilityDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(objCvgXLoss.LossCode);
                             //}
                         }
                         else
                         {                             
                             XmlNode node = objFinancialNode.SelectSingleNode("//Disability");
                             node.ParentNode.RemoveChild(node);
                         }
                        //End Disablity Type

                        if (!iPolicyXUnitId.Equals(0)) //Coverage id related to reserve Table - POLICY_X_UNIT
                        {
                            AddTagValue(objFinancialNode, "OtherIdentifier", "PolicyXUnitId", "OtherId", iPolicyXUnitId.ToString(), nsmgr);
                        }

                        if (objFinancialNode.SelectNodes("//UnitType", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//UnitType", nsmgr).Item(0).InnerText = sUnitType;
                        }

                        switch (sUnitType)
                        {
                            case "V":
                                //Vehicle objVehicle = null;
                                //objVehicle = (Vehicle)objDMF.GetDataModelObject("Vehicle", false);
                                //objVehicle.MoveTo(objPolicyXUnit.UnitId);
                                 string sVehDetails = "SELECT VIN,A.SHORT_CODE,A.CODE_DESC FROM VEHICLE,CODES_TEXT A WHERE UNIT_ID =" + iPolicyXUnitId.ToString() + "  AND A.CODE_ID= VEHICLE.UNIT_TYPE_CODE";
                                 objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sVehDetails);
                                    if (objReader.Read())
                                    {
                                         sVIN = Conversion.ConvertObjToStr(objReader.GetValue(0));
                                         sVehShortCode = Conversion.ConvertObjToStr(objReader.GetValue(1));
                                         sVehShortDesc = Conversion.ConvertObjToStr(objReader.GetValue(2));
                                    }
                                    objReader.Close();
                                if (objFinancialNode.SelectNodes("//Vin", nsmgr).Count > 0)
                                {
                                    objFinancialNode.SelectNodes("//Vin", nsmgr).Item(0).InnerText = sVIN;
                                }
                                if (objFinancialNode.SelectNodes("//UnitTypeCode", nsmgr).Count > 0)
                                {
                                    objFinancialNode.SelectNodes("//UnitTypeCode", nsmgr).Item(0).InnerText = sVehShortCode;
                                }
                                if (objFinancialNode.SelectNodes("//UnitTypeCodeDesc", nsmgr).Count > 0)
                                {
                                    objFinancialNode.SelectNodes("//UnitTypeCodeDesc", nsmgr).Item(0).InnerText = sVehShortDesc;
                                }

                               // objVehicle = null;
                                break;

                            case "P":
                                //PropertyUnit objProperty = null;
                                //objProperty = (PropertyUnit)objDMF.GetDataModelObject("PropertyUnit", false);
                                //objProperty.MoveTo(objPolicyXUnit.UnitId);
                                 string sPropDetails = "SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID =" + iPolicyXUnitId.ToString();
                                 objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sPropDetails);
                                    if (objReader.Read())
                                    {
                                         sPIN = Conversion.ConvertObjToStr(objReader.GetValue(0));                                        
                                    }
                                    objReader.Close();

                                if (objFinancialNode.SelectNodes("//Pin", nsmgr).Count > 0)
                                {
                                    objFinancialNode.SelectNodes("//Pin", nsmgr).Item(0).InnerText = sPIN;
                                }

                                //objProperty = null;
                                break;

                            case "S":
                                //SiteUnit objSite = null;
                                //objSite = (SiteUnit)objDMF.GetDataModelObject("SiteUnit", false);
                                //objSite.MoveTo(objPolicyXUnit.UnitId);
                                 string sSiteDetails = "SELECT SITE_NUMBER,NAME,SIC FROM SITE_UNIT WHERE SITE_ID =" + iPolicyXUnitId.ToString();
                                 objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSiteDetails);
                                    if (objReader.Read())
                                    {
                                        sSiteNumber = Conversion.ConvertObjToStr(objReader.GetValue(0));
                                        sSiteName = Conversion.ConvertObjToStr(objReader.GetValue(1));
                                        sSiteSic = Conversion.ConvertObjToStr(objReader.GetValue(2));
                                    }
                                    objReader.Close(); 
                                if (objFinancialNode.SelectNodes("//SiteNumber", nsmgr).Count > 0)
                                {
                                    objFinancialNode.SelectNodes("//SiteNumber", nsmgr).Item(0).InnerText = sSiteNumber;
                                }
                                if (objFinancialNode.SelectNodes("//Name", nsmgr).Count > 0)
                                {
                                    objFinancialNode.SelectNodes("//Name", nsmgr).Item(0).InnerText = sSiteName;
                                }
                                if (objFinancialNode.SelectNodes("//SIC", nsmgr).Count > 0)
                                {
                                    objFinancialNode.SelectNodes("//SIC", nsmgr).Item(0).InnerText = sSiteSic;
                                }

                                //objSite = null;
                                break;
                        }
                       
                        if (!iPolicyID.Equals(0))                         //Policy id related to reserve Table - POLICY
                        {
                            AddTagValue(objFinancialNode, "OtherIdentifier", "PolicyId", "OtherId", iPolicyID.ToString(), nsmgr);
                        }

                        if (objFinancialNode.SelectNodes("//PolicyName", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//PolicyName", nsmgr).Item(0).InnerText = sPolicyName;
                        }
                        if (objFinancialNode.SelectNodes("//PolicyNumber", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//PolicyNumber", nsmgr).Item(0).InnerText = sPolicyNumber;
                        }
                        if (objFinancialNode.SelectNodes("//PolicyStatusCd", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//PolicyStatusCd", nsmgr).Item(0).InnerText = objCache.GetShortCode(iPolicyStatusCode);
                        }
                        if (objFinancialNode.SelectNodes("//PolicyStatusDesc", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//PolicyStatusDesc", nsmgr).Item(0).InnerText = objCache.GetCodeDesc(iPolicyStatusCode);
                        }
                        if (objFinancialNode.SelectNodes("//PolicyEffDt", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//PolicyEffDt", nsmgr).Item(0).InnerText = sPolictEffDate;
                        }
                        if (objFinancialNode.SelectNodes("//PolicySymbol", nsmgr).Count > 0)
                        {
                            objFinancialNode.SelectNodes("//PolicySymbol", nsmgr).Item(0).InnerText = sPolicySymbol;
                        }

                        objClaimant = (Claimant)objDMF.GetDataModelObject("Claimant", false);
                        objClaimant.MoveTo(iClaimantId);

                        if (objFinancialNode.SelectNodes("//InsurerId", nsmgr).Count > 0)
                        {
                            //objEntity.MoveTo(objClaimant.InsurerEid);
                            objEntity.MoveTo(iPolicyInsurerEID);
                            string name = "";
                            if (!string.IsNullOrEmpty(objEntity.FirstName))
                                name = objEntity.LastName + ", " + objEntity.FirstName;
                            else
                                name = objEntity.LastName;
                            objFinancialNode.SelectNodes("//InsurerId", nsmgr).Item(0).InnerText = name;
                        }

                        objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                        objEntity.MoveTo(iClaimantEid);

                        if (!iClaimantId.Equals(0))
                        {
                            AddTagValue(objFinancialNode, "OtherIdentifier", "ClaimantId", "OtherId", iClaimantId.ToString(), nsmgr);
                        }

                        if (objFinancialNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr).Count > 0)                        
                        {
                            objFinancialNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr).Item(0).InnerText = objEntity.LastName;
                        }
                        if (objFinancialNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr).Count > 0)                        
                        {
                            objFinancialNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr).Item(0).InnerText = objEntity.FirstName;
                        }
                        if (objFinancialNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr).Count > 0)                        
                        {
                            objFinancialNode.SelectNodes("//claimantInfo/GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr).Item(0).InnerText = objEntity.MiddleName;
                        }
                       
                        objReserveNode.AppendChild(objFinancialNode);                        
                        objReserveXml.SelectSingleNode("//ACORD/ClaimsSvcRq").AppendChild(objReserveNode);
                    }
                    iOldResGroupId = sResGroupId;
                }
                
                objReaderLssRes.Close();
                objDMF.Dispose();

                objReserveXml.SelectSingleNode("/ACORD/ClaimsSvcRq").RemoveChild(objReserveXml.SelectNodes("/ACORD/ClaimsSvcRq/ClaimsNotificationAddRq").Item(0));

                if (Convert.ToInt32(iClaimCount) > 0)
                    p_sXml = objReserveXml.OuterXml;
                else
                    p_sXml = "";			// if there is no Reserve to export then don't call web service
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }

            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                }

                if (objEntity != null)
                {
                    objEntity.Dispose();
                }

                if (objClaim != null)
                {
                    objClaim.Dispose();
                }

                if (objClaimant != null)
                {
                    objClaimant.Dispose();
                }

                if (objCvgXLoss != null)
                {
                    objCvgXLoss.Dispose();
                }

                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                }

                if (objPolicyXCvgType != null)
                {
                    objPolicyXCvgType.Dispose();
                }

                if (objPolicyXUnit != null)
                {
                    objPolicyXUnit.Dispose();
                }

                if (objReserveNode != null)
                {
                    objReserveNode = null;
                }
                if (objFinancialNode != null)
                {
                    objFinancialNode = null;
                }

                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }

                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                        objReader.Close();
                    objReader = null;
                }

                if (objReaderLssRes != null)
                {
                    if (!objReaderLssRes.IsClosed)
                        objReaderLssRes.Close();
                    objReaderLssRes = null;
                }

                if (objConn != null)
                {
                    if (objConn.State == ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn = null;
                }                
                nsmgr = null;
                objReserveXml = null;
            }
            return true;
        }
        //End Mits 35472
        #endregion
		#region Private Functions
		/// Name		: AddTagValue
		/// Author		: Manoj Agrawal
		/// Date Created: 08/23/2006
		/// ************************************************************
		/// <summary>		
		/// AddTagValue() function works for following type of structure. It will put sValue under &lt;OtherId&gt; tag if we pass "OtherIdentifier" in sParent and "csc:SupervisorUserId" in sType and "OtherId" in sTag.
		///	 &lt;OtherIdentifier&gt;
		///		&lt;OtherIdTypeCd&gt;csc:SupervisorUserId&lt;/OtherIdTypeCd&gt;
		///		&lt;OtherId&gt;SupervisorName&lt;/OtherId&gt; 
		///	 &lt;/OtherIdentifier&gt;
		/// </summary>
		private void AddTagValue(XmlNode objNode, string sParent, string sType, string sTag, string sValue, XmlNamespaceManager nsmgr)
		{
			try
			{
				for (int i = 0; i < objNode.SelectNodes("//" + sParent, nsmgr).Count; i++)
				{
					if(objNode.SelectNodes("//" + sParent, nsmgr).Item(i).HasChildNodes == true)
					{
						for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
						{
							if(objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText.Equals(sType))
							{
								goto setvalue;
							}
						}

						continue;

					setvalue:
						for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
						{
							if(objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).Name.Equals(sTag))
							{
								objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText = sValue;
								return;
							}
						}
					}
				}
			}
			//catch (RMAppException p_objException)
			//{
			//	throw p_objException;
			//}
			catch (Exception p_objException)
			{
				//throw new RMAppException(Globalization.GetString("LSSExportProcess.Export.Error"), p_objException);
				throw p_objException;
			}
		}


		/// Name		: RemoveTag
		/// Author		: Manoj Agrawal
		/// Date Created: 08/23/2006
		/// ************************************************************
		/// <summary>		
		/// RemoveTag() function works for following type of structure. It will remove &lt;OtherIdTypeCd&gt; and &lt;OtherId&gt; tags if we pass "OtherIdentifier" in sParent and "csc:SupervisorUserId" in sTag.
		///	 &lt;OtherIdentifier&gt;
		///		&lt;OtherIdTypeCd&gt;csc:SupervisorUserId&lt;/OtherIdTypeCd&gt;
		///		&lt;OtherId&gt;SupervisorName&lt;/OtherId&gt; 
		///	 &lt;/OtherIdentifier&gt;
		/// </summary>		
		private void RemoveTag(XmlNode objNode, string sParent, string sTag, XmlNamespaceManager nsmgr)
		{
			int x = -1;

			try
			{
				for (int i = 0; i < objNode.SelectNodes("//" + sParent, nsmgr).Count; i++)
				{
					if(objNode.SelectNodes("//" + sParent, nsmgr).Item(i).HasChildNodes == true)
					{
						for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
						{
							if(objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText.Equals(sTag))
							{
								x = i;
								goto removenode;
							}
						}
					}
				}

				return;

			removenode:
				objNode.SelectNodes("//" + sParent, nsmgr).Item(x).ParentNode.RemoveChild(objNode.SelectNodes("//" + sParent, nsmgr).Item(x));
			}
			//catch (RMAppException p_objException)
			//{
			//	throw p_objException;
			//}
			catch (Exception p_objException)
			{
				//throw new RMAppException(Globalization.GetString("LSSExportProcess.Export.Error"), p_objException);
				throw p_objException;
			}
		}
		#endregion
	}
}
