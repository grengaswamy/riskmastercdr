using System;
using System.Xml;
namespace Riskmaster.Application.LSSInterface
{
	/**************************************************************
	 * $File		: IProcess.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 22/09/2005
	 * $Author		: Parag Sarin
	 * $Comment		: This file contains interfaces for Claim & Payment process
	 * $Source		:  	
	 ************************************************************
	 * Amendment History
	 * ************************************************************
	 * Date Amended		*	Author		*	Amendment   
	 * 07/31/2006			Manoj			Changed the name/structure to include other interfaces
	**********************************************************/

	/// <summary>
	/// Export Process Interface
	/// </summary>
	public interface IExportProcess: IProcess
	{	
		//Manoj - added new interfaces
		bool ClaimExport(ref string p_sXml, ref string sExpClaimNumbers, string sCurrentTime);
		bool AdminExport(ref string p_sXml, ref string sExpAdmins, string sCurrentTime);
		bool CodeExport(ref string p_sXml, ref string sExpCodes, string sCurrentTime);
		bool PaymentStatusExport(ref string p_sXml, ref string sExpPayments, string sCurrentTime);
        //Sumit - Added to export Payee data from RMX to LSS for MCIC -12/10/2008
        bool PayeeExport(ref string p_sXml, ref string sExpAdmins, string sCurrentTime);
        //Sumit - Added to export Check data from RMX to LSS for MCIC -12/29/2008
        bool CheckExport(ref string p_sXml, ref string sExpAdmins, string sCurrentTime);
        bool ReserveExport(ref string p_sXml, ref string sExpClaimNumbers, string sCurrentTime);//Added by sharishkumar for Mits 35472
	}

	/// <summary>
	/// Export Process Factory Interface
	/// </summary>
	public interface IExportProcessFactory: IProcess
	{
		IExportProcess CreateExportProcess();
	}


	/// <summary>
	/// Import Process Interface
	/// </summary>
	public interface IImportProcess: IProcess
	{
		XmlDocument Execute(XmlDocument p_objXmlIn);
        
	}

	/// <summary>
	/// Import Process Factory Interface
	/// </summary>
	public interface IImportProcessFactory
	{
		IImportProcess CreateImportProcess();
	}

    public interface IProcess
    {
        string RiskmasterConnectionString { get; set; }
    }//interface

}
