using System;
using System.Text;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.Collections;
using System.Collections.Specialized;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using System.IO;
using Riskmaster.Settings;
using System.Configuration;


namespace Riskmaster.Application.LSSInterface
{
	/**************************************************************
	 * $File		: LSSImportProcess.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 07/12/2006
	 * $Author		: Raj Goel/Manoj Agrawal
	 * $Comment		: LSSImportProcess class has functions for importing data from LSS to RMX
	 * $Source		:  	
	 ************************************************************
	 * Amendment History
	 * ************************************************************
	 * Date Amended		*	Author		*	Amendment
	 * 08/28/2006			Manoj			Changed existing Matter import for accepting multiple records.
	 *										Added new Code, Payment, Admin, Document, Diary imports.
	**************************************************************/
	/// <summary>
	/// LSSImportProcess class has functions for importing data from LSS to RMX
	/// </summary>
	internal class LSSImportProcess : IImportProcess
	{

		#region Variables Declaration
        /// <summary>
        /// Gets and sets the Riskmaster Database Connection String
        /// </summary>
        public string RiskmasterConnectionString
        {
            get;
            set;
        } // property RiskmasterConnectionString

		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Password
		/// </summary>
		private string m_sPassword="";
		/// <summary>
		/// DSN Name
		/// </summary>
		private string m_sDsnName="";
        private bool m_blnLoggingEnabled = false;
        private int m_intAccountId = 0;
        private int m_iClientId = 0;

        public bool IsLoggingEnabled
        {
            get{return m_blnLoggingEnabled;}
            set{m_blnLoggingEnabled = value;}
        } // property IsLoggingEnabled

        public int AccountId
        {
            get{return m_intAccountId;}
            set{m_intAccountId = value;}
        } // property AccountId

        UserLogin UserLoginInstance
        {
            get;
            set;
        }//property UserLoginInstance

        public int DistributionType
        {
            get;
            set;
        } // property DistributionType


        private int m_iDefaultDistributionType = 0;
        /// <summary>
        /// Writer to aid in HTML creation for LSS , MCM integration
        /// </summary>
        StreamWriter writer = null;
				//start:mits 28131,28128
        private double dExchangeRateCl2Bs;
        private double dClaimAmt;
		//rupal:end mits 28131,28128
        //Start: rsushilaggar Payee/Multi Data Exchange MITS 20073/20036 05/21/2010
        /// <summary>
        /// Set LSS Payment On Hold
        /// </summary>
        public bool bSetLSSPaymentOnHold {get;set;}
        /// <summary>
        /// Allow Add Update LSS Entity 
        /// </summary>
        public bool bAllowAddUpdateLSSEntity {get;set;}
        
        /// <summary>
        /// Allow Add Update LSS Entity 
        /// </summary>
        public bool bPutLssCollectionOnHold { get; set; }
        //END: rsushilaggar Payee/Multi Data Exchange
        //Added by bsharma33 for MITS 26860
        private const int RMB_FUNDS_TRANSACT = 9650;
        private const int RMO_ALLOW_PAYMENT_CLOSED_CLM = 37;
		//End changes by bsharma33 for MITS 26860
		#endregion

		#region Constructor
        /// <summary>
        /// Overloaded Class constructor
        /// </summary>
        /// 
        /// <param name="p_sUserName">string containing the user name to log into the Riskmaster security database</param>
        /// <param name="p_sPassword">string containing the password to log into the Riskmaster security database</param>
        /// <param name="p_sDsnName">string containing the DSN Name selection</param>
        public LSSImportProcess(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
		{
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;

            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();

            //Set the connection string for the Riskmaster Database
            this.RiskmasterConnectionString = CommonFunctions.GetRiskmasterConnectionString(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
            this.UserLoginInstance = CommonFunctions.GetUserLoginInstance(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);

            //Check if Logging has been enabled
            //gagnihotri R5 LSS changes
            //string strLoggingEnabled = RMConfigurator.Value("LSSInterface/Logging");
            //System.Configuration.AppSettingsReader objAppSettings = new AppSettingsReader();
            string strLoggingEnabled = string.Empty;            
            //strLoggingEnabled  = Conversion.ConvertObjToStr(objAppSettings.GetValue("Logging", strLoggingEnabled.GetType()));
            strLoggingEnabled = RMConfigurationManager.GetNameValueSectionSettings("LSSInterface",this.RiskmasterConnectionString, m_iClientId)["Logging"];

			if(strLoggingEnabled.ToUpper().Equals("YES"))
			{
                this.IsLoggingEnabled = true;
			} // if

            //Account Id is taken from riskmaster.config, it should be setup there one time.
            //gagnihotri R5 LSS changes
            //this.AccountId = Conversion.ConvertStrToInteger(RMConfigurator.Value("LSSInterface/AccountId"));	
            //this.AccountId = Conversion.ConvertObjToInt(objAppSettings.GetValue("AccountId", this.AccountId.GetType()));	
            this.AccountId = int.Parse(RMConfigurationManager.GetNameValueSectionSettings("LSSInterface", this.RiskmasterConnectionString, m_iClientId)["AccountId"]);
            this.DistributionType = int.Parse(RMConfigurationManager.GetNameValueSectionSettings("LSSInterface", this.RiskmasterConnectionString, m_iClientId)["DistributionType"]); 

            //rsushilaggar MITS 20938 06/02/2010
            InitCheckOptions();

            if (this.DistributionType == 1)
                this.DistributionType = m_iDefaultDistributionType;
		}
		
		#endregion

		#region Public Execute Method
		/// Name		: Execute
		/// Author		: Manoj Agrawal
		/// Date Created: 08/29/2006
		/// <summary>		
		/// This method decides which type of import to call and then call appropriate method. This method is called from Web Service.
		/// </summary>
		public XmlDocument Execute(XmlDocument p_objXmlIn)
		{
			XmlNamespaceManager nsmgr = null;
			XmlDocument p_objXmlOut = null;
			DbConnection objConn = null;

			try
			{
				//This is used to set the user, which is being used for import, in SYS_PARMS. This info is used to prevent circular updates.
				objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
				objConn.Open();
				objConn.ExecuteNonQuery("UPDATE SYS_PARMS SET RMX_LSS_USER = '" + m_sUserName + "'");
				objConn.Close();
				//objConn=null;

                //Added by sharishkumar for Mits 35472
                SysParms objSysSettings = null;
                objSysSettings = new SysParms(this.RiskmasterConnectionString, m_iClientId);
                //End Mits 35472
				if(!p_objXmlIn.OuterXml.Equals(""))
				{
					nsmgr = new XmlNamespaceManager(p_objXmlIn.NameTable); 
					nsmgr.AddNamespace("csc", p_objXmlIn.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

					if(p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/LitigationInfo/ItemIdInfo/OtherIdentifier/OtherIdTypeCd") != null)
					{
						if(p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/LitigationInfo/ItemIdInfo/OtherIdentifier/OtherIdTypeCd").InnerText.Equals("csc:MatterID"))
							p_objXmlOut = MatterImport(p_objXmlIn);
					}
					else if(p_objXmlIn.SelectSingleNode("/ACORD/ExtensionsSvcRq/csc:InternalCode", nsmgr) != null)
					{
						p_objXmlOut = CodeImport(p_objXmlIn);
					}
					else if(p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/ClaimsPayment") != null)
					{
                        //Added by sharishkumar for Mits 35472
                        if(objSysSettings.SysSettings.MultiCovgPerClm==-1)
                            p_objXmlOut = CcPaymentImport(p_objXmlIn);
                        else
						    p_objXmlOut = PaymentImport(p_objXmlIn);
                        //End Mits 35472
					}
					else if(p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/ClaimsParty") != null)
					{
						p_objXmlOut = AdminImport(p_objXmlIn);
					}
					else if(p_objXmlIn.SelectSingleNode("/ACORD/ExtensionsSvcRq/csc:Diary", nsmgr) != null)
					{
						p_objXmlOut = DiaryImport(p_objXmlIn);
					}
					else if(p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/ClaimsSubsequentRptSubmitRq/FileAttachmentInfo", nsmgr) != null)
					{
						p_objXmlOut = DocumentImport(p_objXmlIn);
					}
                    //Sumit (04/14/2009)-MITS# 15473 - Send Expense Reserve Type and Amount to LSS
                    else if (p_objXmlIn.SelectSingleNode("/ACORD/ClaimsSvcRq/LossRunInqRq/LossRunControls/ClaimsFinancialSummary/LossSummaryCd", nsmgr) != null)
                    {
                        p_objXmlOut = SendReserveBalance(p_objXmlIn);
					}
				}
			}
				//catch(Exception p_objException)
				//{
				//	CommonFunctions.logImportError(p_objException.Message);
				//}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LSSImportProcess.Execute.Error",m_iClientId), p_objEx);
			}
			finally
			{
				nsmgr = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                }
			}

			return p_objXmlOut;
		}
		#endregion
        #region Private Reserves Export Method
        /// Name		: SendReserveBalance
        /// Author		: Sumit, paggarwal2
        /// MITS        : 15473 
        /// Date Created: 04/14/2009
        /// <summary>		
        /// This method is used to export Expense Reserve Types and Reserve balance amount to LSS
        /// </summary>
        private XmlDocument SendReserveBalance(XmlDocument objReservesXml)
        {
            LocalCache objCache = null;
            XmlDocument objResponse = null;
            XmlNamespaceManager nsmgr = null;
            StringCollection arrTransType = null;
            ArrayList arrReserveType = null;
            ArrayList arrReserveBalance = null;
            int iWCLOBCode = 243;
            int iDILOBCode = 844;
            int iGCLOBCode = 241;
            int iReserveTracking = 0;
            int iTemp = 0;
            int iClaimId = 0;
            int iLOBCode = 0;
            int iClaimantEid = 0;
            string sMainRqUID = "";
            string sRqUID = "";
            string sClaimNumber = "";           
            string sPayTypeCd = "";               
            bool bResponse = false;		//this is set to false when error 
            bool bError = false;		//this is set to true when error
            //skhare7 r8 
            int iClaimCurrCode = 0;
            int iReqId = 0;
            int iEventId = 0;
            //paggarwal2: LSS Enhancement for MCIC: 5/12/2009
            bool isNodeDeleted = false;
            //paggarwal2: LSS Enhancement for MCIC: 5/12/2009
            int invoiceCount = 0;
            SysParms objSysSettings=null;

            try
            {
                //logging2 - enter initial records in loggin tables
                if (this.IsLoggingEnabled)
                {
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
                }

                CommonFunctions.iLogDXRequest(iReqId, "EXPR", "TORM", "LSS", "STRT", objReservesXml.OuterXml, this.RiskmasterConnectionString);
                CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objCache = new LocalCache(this.RiskmasterConnectionString,m_iClientId);

                nsmgr = new XmlNamespaceManager(objReservesXml.NameTable);
                nsmgr.AddNamespace("csc", objReservesXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                if (objReservesXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
                    sMainRqUID = objReservesXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;

                //loop in case multiple invoices have been sent
                foreach (XmlNode objXmlNode in objReservesXml.SelectNodes("//LossRunInqRq"))
                {
                    //paggarwal2: LSS Enhancement for MCIC: 5/12/2009
                    invoiceCount++;
                    sRqUID = "";
                    sClaimNumber = "";
                    iClaimId = 0;
                    sPayTypeCd = "";
                    arrTransType = new StringCollection();
                    bResponse = false;
                    bError = false;

                    if (objXmlNode.SelectSingleNode("RqUID") != null)
                    {
                        sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
                    }
                    else
                    {
                        sRqUID = sMainRqUID;
                    }

                    #region EmptyTagsValidation
                    if (objXmlNode.SelectSingleNode("//csc:InsurerId", nsmgr) != null)
                    {
                        sClaimNumber = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("//csc:InsurerId", nsmgr).InnerText);
                    }
                    
                    if (sClaimNumber.Trim().Equals(""))
                    {
                        AddError(ref objResponse, "ExpenseReserveResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Claim Number}");
                        bResponse = false;
                        bError = true;
                    }

                    foreach (XmlNode objTransXmlNode in objXmlNode.SelectNodes("LossRunControls/ClaimsFinancialSummary", nsmgr))
                    {
                        sPayTypeCd = "";

                        if (objTransXmlNode.SelectSingleNode("LossSummaryCd", nsmgr) != null)
                            sPayTypeCd = CommonFunctions.AddAmparsand(objTransXmlNode.SelectSingleNode("LossSummaryCd", nsmgr).InnerText);

                        if (!sPayTypeCd.Equals(""))
                        {
                            arrTransType.Add(sPayTypeCd);
                        }
                    }

                    
                    if (arrTransType.Count <= 0)
                    {
                        AddError(ref objResponse, "ExpenseReserveResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Payment detail}");
                        bResponse = false;
                        bError = true;
                    }
                    #endregion
                    #region Fetching corresponding information of LSS request from RMX
                    //everything is ok and there is no error till this point. so go for another validation
                    if (bError == false)		
                    {//skahre7 r8 Multicurrency
                        //check if claim exists
                        iClaimId = 0;
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                        {
                            if (objReaderTemp.Read())
                            {
                                iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                iLOBCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);
                                //skhare7 r8 Multicurrency
                                iClaimCurrCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(2), m_iClientId);

                            }
                        }

                        if (iClaimId == 0)
                        {
                            AddError(ref objResponse, "ExpenseReserveResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
                            bResponse = false;
                            bError = true;
                        }

                        //get claimant_eid for WC/DI claims because they are required for the payment
                        //to be displayed in the payment history page MITS 11090
                        if ((iLOBCode == iWCLOBCode) || (iLOBCode == iDILOBCode))
                        {
                            string sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + iClaimId.ToString();
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                            {
                                if (objReaderTemp.Read())
                                {
                                    iClaimantEid = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                }
                            }
                        }
                        //gagnihotri MITS 12437 10/30/2008
                        else if (iLOBCode == iGCLOBCode)
                        {
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT RESERVE_TRACKING FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iGCLOBCode))
                            {
                                if (objReaderTemp != null)
                                {
                                    if (objReaderTemp.Read())
                                    {
                                        iReserveTracking = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                    }
                                }
                            }
                            //Checking for Detail Level Tracking
                            if (iReserveTracking == 1 || iReserveTracking == 2)
                            {
                                //As per the KLC, payment should always be made to the primary claimant on the claim
                                string sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + iClaimId.ToString() + " AND PRIMARY_CLMNT_FLAG <> 0";
                                using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                                {
                                    if (objReaderTemp.Read())
                                    {
                                        iClaimantEid = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                    }
                                }
                            }
                        }
                    }
                    //everything is ok and there is no error till this point. so go for another validation
                    if (bError == false)		
                    {
                        //check if Trans type are valid
                        for (int i = 0; i < arrTransType.Count; i++)
                        {
                            iTemp = 0;
                            iTemp = objCache.GetCodeId(arrTransType[i], "TRANS_TYPES");

                            if (iTemp <= 0)
                            {
                                AddError(ref objResponse, "ExpenseReserveResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Payment Type: " + arrTransType[i] + "}");
                                bResponse = false;
                                bError = true;
                                break;
                            }
                        }
                    }
                    #endregion
                    #region Construct the response XML
                    //if everything is ok and there is no error till this point
                    //then we can create payment if bError == false
                    if (bError == false)
                    {
                        AddError(ref objResponse, "ExpenseReserveResponse", sRqUID, "Success", "0", "Success");
                        bResponse = true;
                        bError = false;
                    }

                    //everything is ok and there is no error till this point.
                    if (bError == false)
                    {
                        int iReserveType = 0;
                        int j = 0;
                        arrReserveType = new ArrayList();
                        arrReserveBalance = new ArrayList();

                        //in following loop we will put amount based on Reserve types. 
                        //because Insufficient Reserves would be calculated based on Reserve types.
                        //note: in arrTransType we keep short code of Trans Type. 
                        //note: in arrReserveType we keep code id of Reserve Type
                        for (int i = 0; i < arrTransType.Count; i++)
                        {
                            arrReserveType.Add(objCache.GetRelatedCodeId(objCache.GetCodeId(arrTransType[i], "TRANS_TYPES")));
                        }

                        for (int i = 0; i < arrReserveType.Count; i++)
                        {
                            iReserveType = Common.Conversion.ConvertObjToInt(arrReserveType[i], m_iClientId);
                            arrReserveBalance.Add(GetReserveBalance(iClaimId, iClaimantEid, 0, iReserveType, iLOBCode, 0));
                        }

                        //paggarwal2: LSS Enhancement for MCIC: 5/12/2009 -start
                        //logging6 - begin response
                        CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);
                        if (!isNodeDeleted)
                        {
                            objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(
                                objResponse.SelectNodes("/ACORD/ClaimsSvcRs/LossRunInqRs").Item(0));
                            isNodeDeleted = true;
                        }
                        XmlNode nodeLossRunInqRs = objResponse.SelectNodes("//LossRunInqRs").Item(invoiceCount - 1);
                        XmlNode nodeClaimsFinancialSummary = nodeLossRunInqRs.SelectSingleNode("LossRunControls/ClaimsFinancialSummary");
                        XmlNode nodeLossRunControls = nodeLossRunInqRs.SelectSingleNode("LossRunControls");
                        XmlNode cloneNode = null;
                        if (nodeLossRunInqRs.SelectSingleNode("ItemIdInfo") != null &&
                            nodeLossRunInqRs.SelectSingleNode("ItemIdInfo").ChildNodes != null
                            && nodeLossRunInqRs.SelectSingleNode("ItemIdInfo").ChildNodes[0] != null
                            && nodeLossRunInqRs.SelectSingleNode("ItemIdInfo").ChildNodes[0].Name == "csc:InsurerId")
                        {
                            nodeLossRunInqRs.SelectSingleNode("ItemIdInfo").ChildNodes[0].InnerText = sClaimNumber;
                        }
                        for (int nodeCount = 0; nodeCount < objXmlNode.SelectNodes("LossRunControls/ClaimsFinancialSummary").Count; nodeCount++)
                        {
                            cloneNode = nodeClaimsFinancialSummary.Clone();
                            AddTagValue(cloneNode, "LossSummaryCd", "csc:TransactionTypeCd", "OtherId", arrTransType[j].ToString(), nsmgr);
                            AddTagValue(cloneNode, "LossSummaryCd", "csc:ReserveTypeCd", "OtherId",
                                objCache.GetCodeDesc(Conversion.ConvertObjToInt(arrReserveType[j], m_iClientId)), nsmgr);
                            if (cloneNode.SelectNodes("//FinancialAmount", nsmgr).Count > 0)
                            {
                                //skhare7 r8 Multicurrency
                                objSysSettings = new SysParms(this.RiskmasterConnectionString, m_iClientId);
                              if(iClaimCurrCode!=0 && (objSysSettings.SysSettings.UseMultiCurrency==-1))
                                {
                                    double dExchangeRateB2C = Common.CommonFunctions.ExchangeRateSrc2Dest(objSysSettings.SysSettings.BaseCurrencyType, iClaimCurrCode, this.RiskmasterConnectionString, m_iClientId);

                                arrReserveBalance[j] = (Common.Conversion.ConvertObjToDouble(arrReserveBalance[j], m_iClientId) * dExchangeRateB2C);
                                }
                              cloneNode.SelectNodes("//FinancialAmount", nsmgr).Item(0).InnerText = arrReserveBalance[j].ToString();

                            }
                            //skhare7 r8 Multicurrency

                            nodeLossRunControls.AppendChild(cloneNode);
                            j++;
                        }
                        nodeLossRunInqRs.SelectNodes("LossRunControls").Item(0).RemoveChild(nodeLossRunInqRs.SelectNodes("LossRunControls/ClaimsFinancialSummary").Item(0));
                        //paggarwal2: LSS Enhancement for MCIC: 5/12/2009- end

                        if (objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
                            objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

                        foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
                        {
                            objOrg.InnerText = m_sDsnName;
                        }

                        //logging7 - end response
                        CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);
                        CommonFunctions.iLogDXRequest(iReqId, "EXPR", "TORM", "LSS", "CMPD", objReservesXml.OuterXml, this.RiskmasterConnectionString);
                        CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                        CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
                    }
                    else
                    {
                        CommonFunctions.iLogDXRequest(iReqId, "EXPR", "TORM", "LSS", "FAIL", objReservesXml.OuterXml, this.RiskmasterConnectionString);
                        CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                        CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
                    }
                    #endregion
                }
            }
            catch (Exception p_objException)
            {
                objResponse = null;
                AddError(ref objResponse, "ExpenseReserveResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
                bResponse = false;
                bError = true;

                //logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
            }
            finally
            {
                if (!isNodeDeleted)
                {
                    //rsushilaggar MITS 25167 Date 08/04/2011
                    if (objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs") != null && objResponse.SelectNodes("/ACORD/ClaimsSvcRs/LossRunInqRs") != null && objResponse.SelectNodes("/ACORD/ClaimsSvcRs/LossRunInqRs").Count > 0)
                    {
                        objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(
                                    objResponse.SelectNodes("/ACORD/ClaimsSvcRs/LossRunInqRs").Item(0));
                    }
                    isNodeDeleted = true;
                }
                nsmgr = null;
                arrTransType = null;
                arrReserveBalance = null;
                arrReserveType = null;

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
            return objResponse;
        }
        #endregion

		#region Private Matter Import Method
		/// Name		: MatterImport
		/// Author		: Raj Goel
		/// Date Created: 08/10/2006
		///************************************************************
		///* Amendment History
		///* ************************************************************
		///* Date Amended		*	Author		*	Amendment   
		///* 09/04/2006				Manoj			Updated for multiple Matter import
		///**************************************************************/
		/// <summary>		
		/// This method is used to import Matter from LSS
		/// </summary>
		private XmlDocument MatterImport(XmlDocument objMatterXml)
		{
			DataModelFactory objDMF=null;
			Entity objEntity = null;
            EntityXRole objEntityRole = null;           //Rijul RMA 7914 start
			ClaimXLitigation objClaimLit = null;
			LocalCache objCache = null;
			DbConnection objConn=null;
			//DbReader objReaderTemp = null;        //gagnihotri MITS 12697 07/17/2008

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

			string sMainRqUID = "";

			string sRqUID = "";
			int iClaimId = 0;
			string sClaimNumber = "";
			int iMatterID = 0;
			int iRowId = 0;
			string sPartyType = "";
			int iAttorneyEid = 0;
			int iFirmEid = 0;
			int iJudgeEid = 0;
			int iLitigationRowId = 0;
            // Sumit - 02/17/2009 Added for LSS Entity Id - MCIC Phase 3
            int iRMXEntityId = 0;
            // Sumit - 02/17/2009 Added for LSS Entity Id - MCIC Phase 3
            bool bRMXEntityExist = false;
			string sTemp = "";

			bool bResponse = false;		//Temp variable - this is not used
			bool bError = false;		//this is set to true when error

			//logging1
			int iReqId = 0;
			int iEventId = 0;			
			bool bNewLit = false;

            //gagnihotri MITS 12697 07/17/2008
            int iEntityTableId = 0;
            string sSQL = "";
			try
			{
				//logging2 - enter initial records in loggin tables				
				if(this.IsLoggingEnabled)
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
				}
				CommonFunctions.iLogDXRequest(iReqId, "MATR", "TORM", "LSS", "STRT", objMatterXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				//objCache = objDMF.Context.LocalCache;
				objCache=new LocalCache(this.RiskmasterConnectionString,m_iClientId);

				nsmgr = new XmlNamespaceManager(objMatterXml.NameTable); 
				nsmgr.AddNamespace("csc",objMatterXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				if(objMatterXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
					sMainRqUID = objMatterXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;

				foreach (XmlNode objXmlNode in objMatterXml.SelectNodes("//ClaimsSubsequentRptSubmitRq", nsmgr))
				{
					sRqUID = "";
					sClaimNumber = "";
					iClaimId = 0;
					iMatterID = 0;
					iRowId = 0;

					sPartyType = "";
					sTemp = "";

					bResponse = false;
					bError = false;
					bNewLit = false;
                    // Sumit - 02/17/2009 Added for LSS Entity Id - MCIC Phase 3
                    iRMXEntityId = 0;
                    // Sumit - 02/17/2009 Added for LSS Entity Id - MCIC Phase 3
                    bRMXEntityExist = false;

                    // Sumit - 02/17/2009 Added for LSS Entity Id - MCIC Phase 3
                    iRMXEntityId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:RMXEntityID", "OtherId"));
                    if (iRMXEntityId != 0) bRMXEntityExist = true;

					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;

					sClaimNumber = CommonFunctions.GetValue(objXmlNode,"LitigationInfo/ItemIdInfo/csc:InsurerId", nsmgr);
					if(!sClaimNumber.Equals(""))
					{
                        //gagnihotri MITS 12697 07/17/2008
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                        {
                            if (objReaderTemp.Read())
                            {
                                iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            }
                            //objReaderTemp.Close();
                            //objReaderTemp = null;
                        }
					}

					iMatterID = Common.Conversion.ConvertStrToInteger(CommonFunctions.GetValue(objXmlNode,"LitigationInfo/ItemIdInfo/OtherIdentifier/OtherId", nsmgr));
					
					//Validations for mandatory Claim & MatterID
					if (sClaimNumber.Equals("")) 
					{
						AddError(ref objResponse, "MatterResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Claim Number}");
						bResponse = false;	
						bError = true;
					}
					else if (iClaimId <= 0) 
					{
						AddError(ref objResponse, "MatterResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
						bResponse = false;	
						bError = true;
					}
					else if (iMatterID <= 0) 
					{ 
						AddError(ref objResponse, "MatterResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Matter ID}"); 
						bResponse = false;		
						bError = true;
					}

					if(bError == false)
					{
                        //gagnihotri MITS 12953 10/6/2008 Altered query to check for Litigation record created by LSS Interface only
						//iRowId = this.GetRowIdFromOtherField("LITIGATION_ROW_ID", "LITIGATION_SUPP", "LSS_MATTER_ID", iMatterID.ToString());
                        sSQL = "SELECT LS.LITIGATION_ROW_ID FROM LITIGATION_SUPP LS INNER JOIN CLAIM_X_LITIGATION CXL ON CXL.LITIGATION_ROW_ID = LS.LITIGATION_ROW_ID WHERE LS.LSS_MATTER_ID =" + iMatterID.ToString() + " AND CXL.ADDED_BY_USER = 'LSSINF'";
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                        {
                            if (objReaderTemp.Read())
                            {
                                iRowId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            }
                        }

						objClaimLit = (ClaimXLitigation)objDMF.GetDataModelObject("ClaimXLitigation", false);

						//logging3 - begin update
                        CommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);
						
						if (iRowId > 0) {objClaimLit.MoveTo(iRowId);}
						//else objClaimLit.AddedByUser = "LSSINF";
						else bNewLit = true;

                        objClaimLit.ClaimId = iClaimId;
						//Litigation Data
						objClaimLit.Supplementals["LSS_MATTER_ID"].Value = iMatterID;
						
						objClaimLit.SuitDate = Conversion.ToDbDate(Conversion.ToDate(CommonFunctions.GetValue(objXmlNode,"LitigationInfo/EventInfo/EventDt", "csc:SuitDate", nsmgr)));
						objClaimLit.CourtDate = Conversion.ToDbDate(Conversion.ToDate(CommonFunctions.GetValue(objXmlNode,"LitigationInfo/EventInfo/EventDt", "TrialHearing", nsmgr)));

						//Manoj - structure has been changed as per email from Chris on 08/25/2006
						objClaimLit.VenueStateId = objCache.GetStateRowID(CommonFunctions.GetValue(objXmlNode,"LitigationInfo/Addr/StateProvCd", nsmgr));
						objClaimLit.County = CommonFunctions.GetValue(objXmlNode,"LitigationInfo/Addr/County", nsmgr);
						objClaimLit.DocketNumber = CommonFunctions.GetValue(objXmlNode,"LitigationInfo/DocketNum", nsmgr);

						iAttorneyEid = 0;
						iFirmEid = 0;
						iJudgeEid = 0;

                        //Accoring LSS, there will be one and only one csc:FirmOffice record. Its Entity_id is Attorney Entity's ParentEid
                        XmlNode objFirmOffice = objXmlNode.SelectSingleNode("./ClaimsParty[ClaimsPartyInfo/ClaimsPartyRoleCd='csc:FirmOffice']");
                        if (objFirmOffice != null)
                        {
                            iRowId = 0;
                            if (bRMXEntityExist)
                            {
                                sSQL = "SELECT ENTITY_ID FROM ENTITY WHERE ENTITY_ID = " + iRMXEntityId;
                                using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                                {
                                    if (objReaderTemp.Read())
                                    {
                                        iRowId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                        //iFirmEid = iRowId;
                                    }
                                    else
                                    {
                                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L114", "Data push failed:{Payee: " + iRMXEntityId + " not found.}");
                                        bResponse = false;
                                        bError = true;
                                    }
                                }
                            }

                            if (bError == false)
                            {
                                //Debabrata Biswas Dated 10/13/2009.  Multi Data Exhange MITS# 20036
                                //Add/Update RMX Entities pushed from LSS only if AllowRMXEntityUpdate is set yes.
                                if (bAllowAddUpdateLSSEntity)
                                {
                                    objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);

                                    //Look for an existing Entity based on ENTITY_SUPP.LSS_ADMIN_ID
                                    sTemp = CommonFunctions.GetValue(objFirmOffice, "ItemIdInfo/OtherIdentifier/OtherId", "csc:FirmOfficeId", nsmgr);
                                    //gagnihotri MITS 12697 07/17/2008
                                    //iRowId = this.GetRowIdFromOtherField("ENTITY_ID", "ENTITY_SUPP", "LSS_ADMIN_ID", sTemp);
                                    iEntityTableId = objCache.GetTableId("ATTORNEY_FIRMS");
                                    if (!bRMXEntityExist)
                                    {
                                        sSQL = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + sTemp;// +" and E.ENTITY_TABLE_ID = " + iEntityTableId;
                                        iRowId = 0;
                                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                                        {
                                            if (objReaderTemp.Read())
                                            {
                                                iRowId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                            }
                                        }
                                    }
                                    if (iRowId > 0) objEntity.MoveTo(iRowId);
                                    //else objEntity.AddedByUser = "LSSINF";

                                    //gagnihotri MITS 12697 07/17/2008
                                    //objEntity.EntityTableId = objCache.GetTableId("ATTORNEY_FIRMS");
                                    //Rijul RMA 7914 start
                                    //objEntity.EntityTableId = iEntityTableId;
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                        objEntity.EntityTableId = 0;
                                    else
                                        objEntity.EntityTableId = iEntityTableId;
                                    //Rijul RMA 7914 end
                                    objEntity.Supplementals["LSS_ADMIN_ID"].Value = sTemp;
                                    objEntity.LastName = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/NameInfo/CommlName/CommercialName", nsmgr);
                                    objEntity.Addr1 = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Addr/Addr1", nsmgr);
                                    objEntity.Addr2 = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Addr/Addr2", nsmgr);
                                    objEntity.City = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Addr/City", nsmgr);
                                    objEntity.StateId = objCache.GetStateRowID(CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Addr/StateProvCd", nsmgr));
                                    objEntity.ZipCode = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Addr/PostalCode", nsmgr);
                                    objEntity.CountryCode = objCache.GetCodeId(CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Addr/CountryCd", nsmgr), "COUNTRY");
                                    //rsushilaggar MITS 23030 Date 10/10/2010
                                    objEntity.TaxId = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/NameInfo/TaxIdentity/TaxId", nsmgr);
                                    objEntity.Phone1 = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Communications/PhoneInfo/PhoneNumber", "Office", nsmgr);
                                    objEntity.Phone2 = ReadTagValue(objFirmOffice, "PhoneInfo", "TollFreeNumber", "PhoneNumber");
                                    objEntity.FaxNumber = ReadTagValue(objFirmOffice, "PhoneInfo", "Fax", "PhoneNumber");
                                    objEntity.EmailAddress = CommonFunctions.GetValue(objFirmOffice, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);

                                    //Rijul RMA 7914 start
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                    {
                                        if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                                        {
                                            objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                                            objEntityRole.EntityTableId = iEntityTableId;
                                            objEntityRole.AddedByUser = "LSSINF";
                                            objEntity.EntityXRoleList.Add(objEntityRole);
                                        }
                                    }
                                    //Rijul RMA 7914 end
                                    objEntity.Save();
                                    iFirmEid = objEntity.EntityId;
                                    objEntity.Dispose();
                                    objEntity = null;

                                    //set the added by field value LSSINF if new record was added.
                                    if (iRowId <= 0)
                                    //Rijul RMA 7914 start
                                    {
                                        //if (objEntity.EntityTableId)
                                        //    SetField("ENTITY_X_ROLES", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iFirmEid.ToString());
                                        //else
                                        SetField("ENTITY", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iFirmEid.ToString());
                                    }
                                    //Rijul RMA 7914 end
                                }
                            }//bError
                        } //if (objFirmOffice != null)


                        if (bError == false)
                        {
                            //Debabrata Biswas Dated 10/13/2009. Multi Data Exhange MITS# 20036
                            //Add/Update RMX Entities pushed from LSS only if AllowRMXEntityUpdate is set yes.
                            if (bAllowAddUpdateLSSEntity)
                            {
                                foreach (XmlNode objParty in objXmlNode.SelectNodes("./ClaimsParty[ClaimsPartyInfo/ClaimsPartyRoleCd != 'csc:FirmOffice']"))
                                {
                                    sPartyType = CommonFunctions.GetValue(objParty, "ClaimsPartyInfo/ClaimsPartyRoleCd", nsmgr);
                                    switch (sPartyType)
                                    {
                                        //Manoj - structure has been changed as per email from Chris on 08/25/2006
                                        //case "csc:Venue":
                                        //	objClaimLit.VenueStateId = objCache.GetStateRowID(CommonFunctions.GetValue(objParty,"GeneralPartyInfo/Addr/StateProvCd", nsmgr));
                                        //	objClaimLit.County = CommonFunctions.GetValue(objParty,"GeneralPartyInfo/Addr/County", nsmgr);
                                        //	break;

                                        case "csc:Attorney":	//Attorney Information
                                            objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);

                                            //Look for an existing Entity based on ENTITY_SUPP.LSS_ADMIN_ID
                                            sTemp = CommonFunctions.GetValue(objParty, "ItemIdInfo/OtherIdentifier/OtherId", "csc:AttorneyId", nsmgr);
                                            //gagnihotri MITS 12697 07/17/2008 Start
                                            //iRowId = this.GetRowIdFromOtherField("ENTITY_ID", "ENTITY_SUPP", "LSS_ADMIN_ID", sTemp);
                                            iEntityTableId = objCache.GetTableId("ATTORNEYS");
                                            sSQL = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + sTemp ;//+ " and E.ENTITY_TABLE_ID = " + iEntityTableId;
                                            iRowId = 0;
                                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                                            {
                                                if (objReaderTemp.Read())
                                                {
                                                    iRowId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                                }
                                            }
                                            if (iRowId > 0) objEntity.MoveTo(iRowId);
                                            //else objEntity.AddedByUser = "LSSINF";
                                            //Rijul RMA 7914 start
                                            if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                                objEntity.EntityTableId = 0;
                                            else
                                                objEntity.EntityTableId = iEntityTableId;
                                            //Rijul RMA 7914 end
                                            //objEntity.EntityTableId = objCache.GetTableId("ATTORNEYS");
                                            //gagnihotri MITS 12697 07/17/2008 End
                                            objEntity.Supplementals["LSS_ADMIN_ID"].Value = sTemp;
                                            objEntity.LastName = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr);
                                            objEntity.FirstName = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr);
                                            objEntity.MiddleName = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr);
                                            objEntity.Phone1 = ReadTagValue(objParty, "PhoneInfo", "Office", "PhoneNumber");
                                            objEntity.Phone2 = ReadTagValue(objParty, "PhoneInfo", "Cell", "PhoneNumber");
                                            objEntity.FaxNumber = ReadTagValue(objParty, "PhoneInfo", "Fax", "PhoneNumber");
                                            objEntity.EmailAddress = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);
                                            objEntity.ParentEid = iFirmEid;

                                            //Rijul RMA 7914 start
                                            if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                            {
                                                if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                                                {
                                                    objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                                                    objEntityRole.EntityTableId = iEntityTableId;
                                                    objEntityRole.AddedByUser = "LSSINF";
                                                    objEntity.EntityXRoleList.Add(objEntityRole);
                                                }
                                            }
                                            //Rijul RMA 7914 end
                                            objEntity.Save();
                                            iAttorneyEid = objEntity.EntityId;
                                            objEntity.Dispose();
                                            objEntity = null;

                                            //set the added by field value LSSINF if new record was added.
                                            if (iRowId <= 0)
                                                SetField("ENTITY", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iAttorneyEid.ToString());

                                            break;
                                        case "csc:Judge":	//Judge Data
                                            objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);

                                            //Look for an existing Entity based on ENTITY_SUPP.LSS_ADMIN_ID
                                            sTemp = CommonFunctions.GetValue(objParty, "ItemIdInfo/OtherIdentifier/OtherId", "csc:JudgeId", nsmgr);
                                            //gagnihotri MITS 12697 07/17/2008 Start
                                            //iRowId = this.GetRowIdFromOtherField("ENTITY_ID", "ENTITY_SUPP", "LSS_ADMIN_ID", sTemp);
                                            iEntityTableId = objCache.GetTableId("JUDGES");
                                            sSQL = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + sTemp;// + " and E.ENTITY_TABLE_ID = " + iEntityTableId;
                                            iRowId = 0;
                                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                                            {
                                                if (objReaderTemp.Read())
                                                {
                                                    iRowId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                                }
                                            }
                                            if (iRowId > 0) objEntity.MoveTo(iRowId);
                                            //else objEntity.AddedByUser = "LSSINF";
                                            //Rijul RMA 7914 start
                                            //objEntity.EntityTableId = iEntityTableId;
                                            if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                                objEntity.EntityTableId = 0;
                                            else
                                                objEntity.EntityTableId = iEntityTableId;
                                            //Rijul RMA 7914 end
                                            //objEntity.EntityTableId = objCache.GetTableId("JUDGES");;
                                            //gagnihotri MITS 12697 07/17/2008 End
                                            objEntity.Supplementals["LSS_ADMIN_ID"].Value = sTemp;
                                            objEntity.FirstName = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr);
                                            objEntity.MiddleName = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr);
                                            objEntity.LastName = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr);
                                            objEntity.Addr1 = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Addr/Addr1", nsmgr);
                                            objEntity.Addr2 = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Addr/Addr2", nsmgr);
                                            objEntity.City = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Addr/csc:CityLong", nsmgr);
                                            objEntity.StateId = objCache.GetStateRowID(CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Addr/StateProvCd", nsmgr));
                                            objEntity.ZipCode = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Addr/PostalCode", nsmgr);
                                            objEntity.CountryCode = objCache.GetCodeId(CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Addr/CountryCd", nsmgr), "COUNTRY");
                                            objEntity.Phone1 = ReadTagValue(objParty, "PhoneInfo", "Office", "PhoneNumber");
                                            objEntity.Phone2 = ReadTagValue(objParty, "PhoneInfo", "Cell", "PhoneNumber");
                                            objEntity.FaxNumber = ReadTagValue(objParty, "PhoneInfo", "Fax", "PhoneNumber");
                                            objEntity.EmailAddress = CommonFunctions.GetValue(objParty, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);

                                            //Rijul RMA 7914 start
                                            if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                            {
                                                if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                                                {
                                                    objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                                                    objEntityRole.EntityTableId = iEntityTableId;
                                                    objEntityRole.AddedByUser = "LSSINF";
                                                    objEntity.EntityXRoleList.Add(objEntityRole);
                                                }
                                            }
                                            //Rijul RMA 7914 end
                                            objEntity.Save();
                                            iJudgeEid = objEntity.EntityId;
                                            objEntity.Dispose();
                                            objEntity = null;

                                            //set the added by field value LSSINF if new record was added.
                                            if (iRowId <= 0)
                                                SetField("ENTITY", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iJudgeEid.ToString());

                                            break;
                                    }	//End of Switch Case
                                }		//End of For Each Loop
                            }           

						//Save the Litigation Information
						objClaimLit.Save();
						iLitigationRowId = objClaimLit.LitigationRowId;
						objClaimLit.Dispose();
						objClaimLit = null;

						//set the added by field value LSSINF if new record was added.
						if (bNewLit == true)
							SetField("CLAIM_X_LITIGATION", "ADDED_BY_USER", "LSSINF", "LITIGATION_ROW_ID", iLitigationRowId.ToString());

						if(iAttorneyEid != 0)
						{
							objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
							objConn.Open();
							objConn.ExecuteNonQuery("UPDATE CLAIM_X_LITIGATION SET CO_ATTORNEY_EID = " + iAttorneyEid + " WHERE LITIGATION_ROW_ID = " + iLitigationRowId);
							objConn.Close();
                            //objConn=null;
                        }
                        //gagnihotri MITS 13418 10/7/2008 Start
                        //Attorney firm will only be populated when we have attorney attached to the litigation record
                        else
                        {
                            if (iFirmEid != 0)
                            {
                                objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                                sSQL = "SELECT CO_ATTORNEY_EID FROM CLAIM_X_LITIGATION WHERE LITIGATION_ROW_ID = " + iLitigationRowId;
                                iRowId = 0;
                                using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                                {
                                    if (objReaderTemp.Read())
                                    {
                                        iRowId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                    }
                                }
                                if (iRowId > 0) objEntity.MoveTo(iRowId);
                                iEntityTableId = objCache.GetTableId("ATTORNEYS");
                                    //Rijul RMA 7914 start
                                    //objEntity.EntityTableId = iEntityTableId;
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                        objEntity.EntityTableId = 0;
                                    else
                                        objEntity.EntityTableId = iEntityTableId;
                                    //Rijul RMA 7914 end
                                    objEntity.ParentEid = iFirmEid;

                                    //Rijul RMA 7914 start
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                    {
                                        if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                                        {
                                            objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                                            objEntityRole.EntityTableId = iEntityTableId;
                                            objEntityRole.AddedByUser = "LSSINF";
                                            objEntity.EntityXRoleList.Add(objEntityRole);
                                        }
                                    }
                                    //Rijul RMA 7914 end
                                objEntity.Save();
                                iAttorneyEid = objEntity.EntityId;
                                objEntity.Dispose();
                                objEntity = null;

                                //set the added by field value LSSINF if new record was added.
                                if (iRowId <= 0)
                                {
                                    SetField("ENTITY", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iAttorneyEid.ToString());
                                    objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                                    objConn.Open();
                                    objConn.ExecuteNonQuery("UPDATE CLAIM_X_LITIGATION SET CO_ATTORNEY_EID = " + iAttorneyEid + " WHERE LITIGATION_ROW_ID = " + iLitigationRowId);
                                    objConn.Close();
                                }
                            }
                            //gagnihotri MITS 13418 10/7/2008 End
                        }


						if(iJudgeEid != 0)
						{
							objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
							objConn.Open();
							objConn.ExecuteNonQuery("UPDATE CLAIM_X_LITIGATION SET JUDGE_EID = " + iJudgeEid + " WHERE LITIGATION_ROW_ID = " + iLitigationRowId);
							objConn.Close();
							//objConn=null;
						}

						//logging4 - end update
                        CommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "MatterResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;
					}
					}
				}		//for each
			}			//try
			catch(Exception p_objException)
			{
				AddError(ref objResponse, "MatterResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
				bResponse = false;		
				bError = true;

				//logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
			}
			finally
			{
				objEntity = null;
				objClaimLit = null;
				nsmgr = null;
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                //gagnihotri MITS 12697 07/17/2008
                //if (objReaderTemp != null)
                //{
                //    objReaderTemp.Dispose();
                //}
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
			}

			//logging6 - begin response
            CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
				CommonFunctions.iLogDXRequest(iReqId, "MATR", "TORM", "LSS", "CMPD", objMatterXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
				CommonFunctions.iLogDXRequest(iReqId, "MATR", "TORM", "LSS", "FAIL", objMatterXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}

		#endregion

		#region Private Code Import Method
		/// Name		: CodeImport
		/// Author		: Manoj Agrawal
		/// Date Created: 08/28/2006
		/// <summary>		
		/// This method is used to import Codes from LSS
		/// </summary>
		private XmlDocument CodeImport(XmlDocument objCodeXml)
		{
			DataModelFactory objDMF=null;
			Code objCode = null;
			CodeTextItem objCodeTextItem = null;
			LocalCache objCache = null;
			DbReader objReaderTemp = null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

			string sMainRqUID = "";

			int iCodeId = 0;
			string sRqUID = "";
			string sCRUD = "";
			string sOldCode = "";
			string sNewCode = "";
			string sCodeDesc = "";
			string sCodeType = "";
			string sTableName = "";
            string sAddedbyUser = string.Empty;
			string sTemp = "";

			bool bResponse = false;	//Temp variable - this is not used
			bool bError = false;
			
			//logging1
			int iReqId = 0;
			int iEventId = 0;

			try
			{
				//logging2 - enter initial records in loggin tables				
				if(this.IsLoggingEnabled)
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
				}
				CommonFunctions.iLogDXRequest(iReqId, "INCD", "TORM", "LSS", "STRT", objCodeXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				//objCache = objDMF.Context.LocalCache;
				objCache=new LocalCache(this.RiskmasterConnectionString,m_iClientId);

				nsmgr = new XmlNamespaceManager(objCodeXml.NameTable); 
				nsmgr.AddNamespace("csc", objCodeXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				if(objCodeXml.SelectSingleNode("/ACORD/ExtensionsSvcRq/RqUID") != null)
					sMainRqUID = objCodeXml.SelectSingleNode("/ACORD/ExtensionsSvcRq/RqUID").InnerText;

				foreach (XmlNode objXmlNode in objCodeXml.SelectNodes("//csc:InternalCode", nsmgr))
				{
					iCodeId = 0;
					sRqUID = "";
					sCRUD = "";
					sOldCode = "";
					sNewCode = "";
					sCodeDesc = "";
					sCodeType = "";
					sTableName = "";
					bResponse = false;
					bError = false;

					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;

					if(objXmlNode.SelectSingleNode("csc:CRUD", nsmgr) != null)
						sCRUD = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("csc:CRUD", nsmgr).InnerText);

					if(objXmlNode.SelectSingleNode("csc:Code", nsmgr) != null)
						sOldCode = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("csc:Code", nsmgr).InnerText);

					if(objXmlNode.SelectSingleNode("csc:NewCode", nsmgr) != null)
						sNewCode = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("csc:NewCode", nsmgr).InnerText);

					if(objXmlNode.SelectSingleNode("csc:CodeDescription", nsmgr) != null)
						sCodeDesc = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("csc:CodeDescription", nsmgr).InnerText);
					
					if(objXmlNode.SelectSingleNode("csc:CodeType", nsmgr) != null)
						sCodeType = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("csc:CodeType", nsmgr).InnerText);


					//TODO: check what all Code type will come and what are the corresponding table name for them
					//As per the latest e-mail from Chris on 09/07/2006 we will send only Claim Type and Nature of Injury
					switch(sCodeType)
					{
						case "LINEOFBUSINESS":
							sTableName = "CLAIM_TYPE";
							break;

						case "NATUREOFINJURY":
							sTableName = "INJURY_TYPE";
							break;

							//						case "PAYMENTTYPE":
							//							sTableName = "TRANS_TYPES";
							//							break;

						default:
							sTableName = "";
							break;
					}


					if(sCRUD.Equals("U"))
					{
						if (sOldCode.Equals(""))	//Validations for old code
						{
							AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Code}");
							bResponse = false;		
							bError = true;
						}
						else if (sNewCode.Equals("")) 	//Validations for new code
						{
							AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {New Code}");
							bResponse = false;		
							bError = true;
						}
						else if (sCodeDesc.Equals(""))		//Validations for code desc
						{
							AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Code Desc}");
							bResponse = false;		
							bError = true;
						}
						else if (sTableName.Equals("")) //Validations for code type. only 3 types are expected.
						{
							AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Code type: " + sCodeType + "}");
							bResponse = false;		
							bError = true;
						}
					}
					else if(sCRUD.Equals("D"))
					{
						if (sOldCode.Equals(""))	//Validations for old code
						{
							AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Code}");
							bResponse = false;		
							bError = true;
						}
						else if (sTableName.Equals("")) //Validations for code type. only 3 types are expected.
						{
							AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Code type: " + sCodeType + "}");
							bResponse = false;		
							bError = true;
						}
					}


					if(bError == false)		//everything is ok and there is no error till this point
					{
						iCodeId = 0;
						objReaderTemp=DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CODE_ID, ADDED_BY_USER FROM CODES WHERE SHORT_CODE = '" + sOldCode.Replace("'", "''") + "' AND TABLE_ID = " + objCache.GetTableId(sTableName));
						if(objReaderTemp.Read())
						{
                            iCodeId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            sAddedbyUser = Conversion.ConvertObjToStr(objReaderTemp.GetValue(1));
						}
						objReaderTemp.Close();
						//objReaderTemp = null;

						if(sCRUD.Equals("D"))
						{
							if(iCodeId <= 0)
							{
								AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L112", "Data push failed: Delete Failed: Code not found {" + sOldCode + "}");
								bResponse = false;		
								bError = true;
							}
						}
					}
					
	
					if(bError == false)		//everything is ok and there is no error till this point
					{
						objCode = (Code)objDMF.GetDataModelObject("Code", false);

						//logging3 - begin update
                        CommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

						if(iCodeId > 0)
						{
							//Manoj: code already exists so update that

							objCode.MoveTo(iCodeId);

							if(sCRUD.Equals("D"))
							{
								objCode.DeletedFlag = true;

								sTemp = objCode.CodeTextList.Count.ToString();		//this line is just to load CodeTextList in objCode object
							}
							else
							{
								objCode.ShortCode = sNewCode;
                                //rsushilaggar
                                objCode.UpdatedByUser = this.UserLoginInstance.LoginName;
                                objCode.AddedByUser = sAddedbyUser;

								objCodeTextItem = (CodeTextItem)objDMF.GetDataModelObject("CodeTextItem", false);
								objCodeTextItem.CodeId = iCodeId;
								objCodeTextItem.ShortCode = sNewCode;
								objCodeTextItem.CodeDesc = sCodeDesc;
								objCodeTextItem.LanguageCode = 1033;

								objCode.CodeTextList.Add(objCodeTextItem);
							}
							objCode.Save();
						}
						else
						{
							//Manoj: code does not exist. create new code.

							objCode.ShortCode = sNewCode;
							objCode.TableId = objCache.GetTableId(sTableName);							

                            //rsushilaggar
                            objCode.AddedByUser = this.UserLoginInstance.LoginName;
                            objCode.UpdatedByUser = this.UserLoginInstance.LoginName;
							objCodeTextItem = objCode.CodeTextList.AddNew();
							objCode.Save();

							objCode.CodeTextList.Remove(objCodeTextItem);

							objCodeTextItem = null;

							objCodeTextItem = (CodeTextItem)objDMF.GetDataModelObject("CodeTextItem", false);
							objCodeTextItem.CodeId = objCode.CodeId;
							objCodeTextItem.ShortCode = sNewCode;
							objCodeTextItem.CodeDesc = sCodeDesc;
							objCodeTextItem.LanguageCode = 1033;

							objCode.CodeTextList.Add(objCodeTextItem);
							objCode.Save();

							objCodeTextItem.Dispose();
							objCodeTextItem = null;
						}

						objCode.Dispose();
						objCode = null;

						//logging4 - end update
                        CommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "CodeResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;
					}	//if(bError == false)
				}		//foreach
			}			//try
			catch(Exception p_objException)
			{
				AddError(ref objResponse, "CodeResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
				bResponse = false;		
				bError = true;

				//logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
			}
			finally
			{
				objCode = null;
				objCodeTextItem = null;
				nsmgr = null;

				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
				
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
			}

			//logging6 - begin response
            CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ExtensionsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ExtensionsSvcRs/SPExtensionsSyncRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ExtensionsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ExtensionsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
				CommonFunctions.iLogDXRequest(iReqId, "INCD", "TORM", "LSS", "CMPD", objCodeXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
				CommonFunctions.iLogDXRequest(iReqId, "INCD", "TORM", "LSS", "FAIL", objCodeXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}
		
		#endregion

		#region Private Payment Import Method
		/// Name		: PaymentImport
		/// Author		: Manoj Agrawal
		/// Date Created: 08/29/2006
		/// <summary>		
		/// This method is used to import Payment from LSS
		/// </summary>
		private XmlDocument PaymentImport(XmlDocument objPaymentXml)
		{
			DataModelFactory objDMF=null;
			Funds objFunds = null;
			FundsTransSplit objFundsTransSplit = null;
			Entity objEntity = null;
            EntityXRole objEntityRole = null; //Rijul RMA 7914
			LocalCache objCache = null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

            int iWCLOBCode = 243;
            int iDILOBCode = 844;
            //gagnihotri MITS 12437 10/30/2008
            int iGCLOBCode = 241;
            int iVALOBCode = 242;           //gagnihotri MITS 14376 02/12/2009
            int iReserveTracking = 0;

			string sMainRqUID = "";

			//Funds record
			int iTransId = 0;
			string sRqUID = "";
			int iFirmOfficeId = 0;
			int iEntityId = 0;
			int iInvoiceId = 0;
            // Start Sumit - 1/12/2009 Added for LSS Invoice History Id - MCIC Phase 3 Payee Data Exchange: MITS #20073
            int iInvoiceHistoryId = 0; 
            string sLSSApproverUserId = string.Empty ; 
            int iRMXEntityId = 0;
            bool bRMXEntityExist = false;
            string sRMXEntityType = string.Empty;
            // End Sumit - 1/12/2009 Added for LSS Invoice History Id - MCIC Phase 3 Payee Data Exchange: MITS #20073
			int iMatterId = 0;
			string sPaymentSequence = "";
			string sClaimNumber = "";
			int iClaimId = 0;
            int iLOBCode = 0;
            int iClaimStatusCode=0;
            int iClaimantEid = 0;
			double dInvoiceAmt = 0;
			double dFinalInvoiceAmt = 0;
			string sFirmInvoiceNum = "";
			string sInvoiceDt = "";
			string sFinalDate = "";
			string sMinTaskDt = "";
			string sMaxTaskDt = "";

			//Funds Trans record	- this can be multiple records for one Payment
			string sPayTypeCd = "";
			double dAmt = 0;
			StringCollection arrTransType = null;
			ArrayList arrTransAmt = null;

			ArrayList arrReserveType = null;
			ArrayList arrReserveAmt = null;

			XmlNode objPartyNode = null;
			string sPartyType = "";
			string sFirstName = "";
			string sLastName = "";
			string sAddr1 = "";
			string sAddr2 = "";
			string sCity = "";
			int iStateId = 0;
			string sZipCode = "";
			int iCountryCode = 0;
			string sTaxId = "";
			string sPhone1 = "";
			string sPhone2 = "";
			string sFax = "";
			string sEmail = "";

			string sTemp = "";
			double dTemp = 0;
			int iTemp = 0;

			bool bResponse = false;		//this is set to false when error - this is not used
			bool bError = false;		//this is set to true when error

			//logging1
			int iReqId = 0;
			int iEventId = 0;		
			bool bNewEnt = false;

            //gagnihotri MITS 12697 07/17/2008
            int iEntityTableId = 0;
            string sSQLTemp = "";
            //skhare7 r8 Multicurrency
            SysParms objSysSetting = null;
            int iClaimCurrCode = 0;
            double dExcgB2C = 0.0;    //skhare7 MITS 27671
            FundsXPayee objFundsXPayee = null; 
			try
			{
                objSysSetting = new SysParms(this.RiskmasterConnectionString, m_iClientId);
				//logging2 - enter initial records in loggin tables
             
                    
                 

				if(this.IsLoggingEnabled)
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
				}
				CommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "LSS", "STRT", objPaymentXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				//objCache = objDMF.Context.LocalCache;
				objCache=new LocalCache(this.RiskmasterConnectionString,m_iClientId);

				nsmgr = new XmlNamespaceManager(objPaymentXml.NameTable); 
				nsmgr.AddNamespace("csc", objPaymentXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				if(objPaymentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
					sMainRqUID = objPaymentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;
                
				//following for loop will iterate through all the payments coming in XML
				foreach (XmlNode objXmlNode in objPaymentXml.SelectNodes("//ClaimsSubsequentRptSubmitRq"))
				{
					objFunds = null;
					objFundsTransSplit = null;
					objEntity = null;

					iTransId = 0;
					sRqUID = "";
					iFirmOfficeId = 0;
					iEntityId = 0;
					iInvoiceId = 0;
                    // Start Sumit - 1/12/2009 Added for LSS Invoice History Id - MCIC Phase 3 Payee Data Exchange: MITS #20073
                    iInvoiceHistoryId = 0; 
                    sLSSApproverUserId = "";
                    iRMXEntityId = 0;
                    bRMXEntityExist = false;
                    sRMXEntityType = string.Empty;
                    // End Sumit - 1/12/2009 Added for LSS Invoice History Id - MCIC Phase 3 Payee Data Exchange: MITS #20073
					iMatterId = 0;
					sPaymentSequence = "";
					sClaimNumber = "";
					iClaimId = 0;
					dInvoiceAmt = 0;
					dFinalInvoiceAmt = 0;
					sFirmInvoiceNum = "";
					sInvoiceDt = "";
					sFinalDate = "";
					sMinTaskDt = "";
					sMaxTaskDt = "";

					//Funds Trans record	- this can be multiple records for one Payment
					sPayTypeCd = "";
					dAmt = 0;
					arrTransType = new StringCollection();
					arrTransAmt = new ArrayList();

					bResponse = false;
					bError = false;

					objPartyNode = null;
					sPartyType = "";
					sFirstName = "";
					sLastName = "";
					sAddr1 = "";
					sAddr2 = "";
					sCity = "";
					iStateId = 0;
					sZipCode = "";
					iCountryCode = 0;
					sTaxId = "";
					sPhone1 = "";
					sPhone2 = "";
					sFax = "";
					sEmail = "";
					
					sTemp = "";
					dTemp = 0;
					iTemp = 0;
					bNewEnt = false;
                    bool allowPaymentToClosedClaims = false;//Added by bsharma33 for MITS 26860
					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;


					iFirmOfficeId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:FirmOfficeId", "OtherId"));

					iInvoiceId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:InvoiceId", "OtherId"));
					
                    // Sumit - 1/12/2009 Added for LSS Invoice History Id - MCIC Phase 3 Payee Data Exchange: MITS #20073
                    iInvoiceHistoryId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:InvoiceHistoryId", "OtherId"));

                    // Sumit - 1/12/2009 Added for LSS Invoice UserId - MCIC Phase 3 Payee Data Exchange: MITS #20073
                    sLSSApproverUserId = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:ApproverUserId", "OtherId");

                    // Sumit - 1/12/2009 Added for LSS RMX Entity Type - MCIC Phase 3 Payee Data Exchange: MITS #20073
                    sRMXEntityType = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:RMXEntityType", "OtherId");

                    // Sumit - 02/17/2009 Added for LSS Entity Id - MCIC Phase 3 Payee Data Exchange: MITS #20073
                    iRMXEntityId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:RMXEntityID", "OtherId"));
                    if (iRMXEntityId != 0) bRMXEntityExist = true;

					iMatterId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:MatterId", "OtherId"));
					
					sPaymentSequence = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:PaymentSequence", "OtherId");

					if(objXmlNode.SelectSingleNode("//csc:InsurerId", nsmgr) != null)
						sClaimNumber = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("//csc:InsurerId", nsmgr).InnerText);


                    //skhare7 R8 claim

                   
                    //skhare7 r8 Multicurrency
                    using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                    {
                        if (objReaderTemp.Read())
                        {
                            iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), 0);
                            iLOBCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), 0);
                            iClaimCurrCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(2), 0);
                             
                        }
                    }

					if(objXmlNode.SelectSingleNode("//csc:InvoiceAmt/Amt", nsmgr) != null)
						dInvoiceAmt = Common.Conversion.ConvertStrToDouble(objXmlNode.SelectSingleNode("//csc:InvoiceAmt/Amt", nsmgr).InnerText);
                    if (objXmlNode.SelectSingleNode("//csc:FinalInvoiceAmt/Amt", nsmgr) != null)
                        dFinalInvoiceAmt = Common.Conversion.ConvertStrToDouble(objXmlNode.SelectSingleNode("//csc:FinalInvoiceAmt/Amt", nsmgr).InnerText);
                    objSysSetting = new SysParms(this.RiskmasterConnectionString, m_iClientId);
                    if (iClaimCurrCode != 0 && (objSysSetting.SysSettings.UseMultiCurrency == -1))
                    {

                        double dExchangeRateC2B = Common.CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, this.RiskmasterConnectionString, m_iClientId);
                        dInvoiceAmt = dExchangeRateC2B * dInvoiceAmt;
                    
                       dFinalInvoiceAmt = dFinalInvoiceAmt * dExchangeRateC2B;
                        //skhare7 MITS 27671
                       dExcgB2C = Common.CommonFunctions.ExchangeRateSrc2Dest(Common.CommonFunctions.GetBaseCurrencyCode(this.RiskmasterConnectionString), iClaimCurrCode, this.RiskmasterConnectionString, m_iClientId);
                    }
                    //else
                    //{
                    //    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Firm Office Id}");
                    //    bResponse = false;
                    //    bError = true;
                    
                    //}
                      
				

					if(objXmlNode.SelectSingleNode("//csc:FirmInvoiceNumber", nsmgr) != null)
						sFirmInvoiceNum = objXmlNode.SelectSingleNode("//csc:FirmInvoiceNumber", nsmgr).InnerText;

					sInvoiceDt = ReadTagValue(objXmlNode, "EventInfo", "csc:InvoiceDate", "EventDt");

					sFinalDate = ReadTagValue(objXmlNode, "EventInfo", "csc:FinalDate", "EventDt");
					
					sMinTaskDt = ReadTagValue(objXmlNode, "EventInfo", "csc:MinTaskDate", "EventDt");
					
					sMaxTaskDt = ReadTagValue(objXmlNode, "EventInfo", "csc:MaxTaskDate", "EventDt");

					foreach(XmlNode objTransXmlNode in objXmlNode.SelectNodes("//csc:ClaimsPaymentDetail", nsmgr))
					{
						sPayTypeCd = "";
						dAmt = 0;

						if(objTransXmlNode.SelectSingleNode("csc:PaymentTypeCd", nsmgr) != null)
							sPayTypeCd = CommonFunctions.AddAmparsand(objTransXmlNode.SelectSingleNode("csc:PaymentTypeCd", nsmgr).InnerText);

						if(objTransXmlNode.SelectSingleNode("csc:PaymentTypeAmt/Amt", nsmgr) != null)
                        {
                            dAmt = Common.Conversion.ConvertStrToDouble(objTransXmlNode.SelectSingleNode("csc:PaymentTypeAmt/Amt", nsmgr).InnerText);
                           	//rkotak:mits 28131,28128
						    dClaimAmt=dAmt;
                            if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                            {
                                dExchangeRateCl2Bs = Common.CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, this.RiskmasterConnectionString, m_iClientId);//skhare7 r8 Multicurrency
                              
                                dAmt = dExchangeRateCl2Bs * dAmt;
                            }
                          
							if(!sPayTypeCd.Equals("") && dAmt != 0)
							{
								arrTransType.Add(sPayTypeCd);
								arrTransAmt.Add(dAmt);
								dTemp = dTemp + dAmt;
							}
						}
					}

					if(iFirmOfficeId == 0)
					{
						AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Firm Office Id}");
						bResponse = false;		
						bError = true;
					}
					else if(sClaimNumber.Equals(""))
					{
						AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Claim Number}");
						bResponse = false;		
						bError = true;
					}
					else if(dFinalInvoiceAmt == 0)
					{
						AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Final Invoice Amt: zero}");
						bResponse = false;		
						bError = true;
					}
					else if(arrTransType.Count <= 0)
					{
						AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Payment detail}");
						bResponse = false;		
						bError = true;
					}
                    //gagnihotri MITS 14241 02/17/2009
					//else if(dFinalInvoiceAmt != dTemp)
                    else if (((dFinalInvoiceAmt - dTemp) > 0.009) && dFinalInvoiceAmt > 0)
					{
						AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Final Invoice amount is not equal to sum of Payment details amount}");
						bResponse = false;		
						bError = true;
					}


					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//check if Trans type are valid
						for(int i = 0; i < arrTransType.Count; i++)
						{
							iTemp = 0;
							iTemp = objCache.GetCodeId(arrTransType[i], "TRANS_TYPES");
                            
							if(iTemp <= 0)
							{
								AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Payment Type: " + arrTransType[i] + "}");
								bResponse = false;		
								bError = true;
								break;
							}
						}
					}


					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//check if claim exists
						iClaimId = 0;
                        //Changed by bsharma33 for MITS 26860
						//using(DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE,CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                        {
						    if(objReaderTemp.Read())
						    {
                                iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                iLOBCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);
                                //Added by bsharma33 for MITS 26860
                                iClaimStatusCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(2), m_iClientId);
                                if (objCache.GetRelatedShortCode(iClaimStatusCode) == "C")
                                {
                                    if (!this.UserLoginInstance.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_PAYMENT_CLOSED_CLM))
                                    {
                                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Payment: Not allowed on closed claims.");
                                        bResponse = false;
                                        bError = true;
                                    }
                                    else
                                    {
                                        allowPaymentToClosedClaims = true;
                                    }
                                }
                                //End changes by bsharma33 for MITS 26860

						    }
						}

						if(iClaimId == 0)
						{
							AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
							bResponse = false;		
							bError = true;
						}

                        //get claimant_eid for WC/DI claims because they are required for the payment
                        //to be displayed in the payment history page MITS 11090
                        if ((iLOBCode == iWCLOBCode) || (iLOBCode == iDILOBCode))
                        {
                            string sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + iClaimId.ToString();
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                            {
                                if (objReaderTemp.Read())
                                {
                                    iClaimantEid = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                }
                            }
                        }
                        //gagnihotri MITS 12437 10/30/2008
                        else if ((iLOBCode == iGCLOBCode) || (iLOBCode == iVALOBCode))
                        {
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT RESERVE_TRACKING FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + iLOBCode))
                            {
                                if (objReaderTemp != null)
                                {
                                    if (objReaderTemp.Read())
                                    {
                                        iReserveTracking = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                    }
                                }
                            }
                            //Checking for Detail Level Tracking
                            if (iReserveTracking == 1 || iReserveTracking == 2)
                            {
                                //As per the KLC, payment should always be made to the primary claimant on the claim
                                string sSQL = "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + iClaimId.ToString() + " AND PRIMARY_CLMNT_FLAG <> 0";
                                using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                                {
                                    if (objReaderTemp.Read())
                                    {
                                        iClaimantEid = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                    }
                                }
                            }
                        }
					}

                    //Start: Added by Nitin goel, MITS 32722, 10/03/2013
                    //Check for Duplicate Invoice, Criteria: Invoice Id, Invoice History Id and RMX Entity Id/LSS Firm Office Id
                    if (bError == false)
                    {
                        
                            StringBuilder sSQLDupInvCheck = new StringBuilder();
                            int iCountDupInvFound = 0;
                            if (bRMXEntityExist)
                            {
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" SELECT COUNT(*) FROM FUNDS F INNER JOIN FUNDS_SUPP FS ON F.TRANS_ID=FS.TRANS_ID ");
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" INNER JOIN ENTITY E ON F.PAYEE_EID = E.ENTITY_ID ");
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" WHERE (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND UPPER(F.ADDED_BY_USER) ='LSSINF' AND FS.LSS_INVOICE_ID=").Append(iInvoiceId);
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" AND FS.LSS_HISTORY_ID=").Append(iInvoiceHistoryId);
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" AND E.ENTITY_ID=").Append(iRMXEntityId);
                            }
                            else
                            {
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" SELECT COUNT(*) FROM FUNDS F INNER JOIN FUNDS_SUPP FS ON F.TRANS_ID=FS.TRANS_ID ");
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" INNER JOIN ENTITY E ON F.PAYEE_EID= E.ENTITY_ID INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID=ES.ENTITY_ID ");
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" WHERE (F.VOID_FLAG IS NULL OR F.VOID_FLAG=0) AND UPPER(F.ADDED_BY_USER) ='LSSINF' AND FS.LSS_INVOICE_ID=").Append(iInvoiceId);
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" AND FS.LSS_HISTORY_ID=").Append(iInvoiceHistoryId);
                                sSQLDupInvCheck = sSQLDupInvCheck.Append(" AND ES.LSS_ADMIN_ID=").Append(iFirmOfficeId);
                            }
                            iCountDupInvFound = DbFactory.ExecuteAsType<int>(this.RiskmasterConnectionString, Convert.ToString(sSQLDupInvCheck));
                             if (iCountDupInvFound>0)
                             {
                                 AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Duplicate Invoice Record}");
                                    bResponse = false;
                                    bError = true;
                             }
                             sSQLDupInvCheck = null;
                    }  
                    //End:Added by Nitin goel, MITS 32722, 10/03/2013

					//check for insufficient reserve
					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						double dAmount = 0;
						double dblBalance = 0;
						int iUnitId = 0;
						int iReserveType = 0;
						int iLob = 0;
						int iTransactionId = 0;

						int j = 0;

						arrReserveType = new ArrayList();
						arrReserveAmt = new ArrayList();

						//in following loop we will put amount based on Reserve types. because Insufficient Reserves would be calculated based on Reserve types.
						//note: in arrTransType we keep short code of Trans Type. 
						//note: in arrReserveType we keep code id of Reserve Type
						for (int i = 0; i < arrTransType.Count; i++)
						{
							for (j = 0; j < arrReserveType.Count; j++)
							{
								if(Common.Conversion.ConvertObjToInt(arrReserveType[j], 0) == objCache.GetRelatedCodeId(objCache.GetCodeId(arrTransType[i], "TRANS_TYPES")))		
									//if we already have that reserve type in list then add amount otherwise create new element in array and put amount
								{
									arrReserveAmt[j] = Common.Conversion.ConvertObjToDouble(arrReserveAmt[j], 0) + Common.Conversion.ConvertObjToDouble(arrTransAmt[i], 0);
									break;
								}
							}

							if(j == arrReserveType.Count)			//this means we have not found Reserve type in arrReserveType list
							{
								arrReserveType.Add(objCache.GetRelatedCodeId(objCache.GetCodeId(arrTransType[i], "TRANS_TYPES")));
								arrReserveAmt.Add(arrTransAmt[i]);
							}
						}					

                        if (allowPaymentToClosedClaims == false) //Added by bsharma33 for MITS 26860, if allowPaymentToClosedClaims = true no need to check funds reserve, will be handled automatically in Funds.cs save() function.
                        {
                            for (int i = 0; i < arrReserveType.Count; i++)
                            {
                                dAmount = Common.Conversion.ConvertObjToDouble(arrReserveAmt[i], m_iClientId);
                                iUnitId = 0;
                                iReserveType = Common.Conversion.ConvertObjToInt(arrReserveType[i], m_iClientId);
                                //Debabrata Biswas 01/11/2010 MITS# 18477
                                //Comented resetting the LOB Code to 0
                                //Get the actual LOB Code.
                                //iLob = 0;
                                iLob = iLOBCode;
                                iTransactionId = 0;

                                dblBalance = GetReserveBalance(iClaimId, iClaimantEid, iUnitId, iReserveType, iLob, iTransactionId);
                                if (((dAmount - dblBalance) > 0.009) && dFinalInvoiceAmt > 0)//rsushilaggar 06/02/2010 MITS 20938
                                {
                                    //reserves are insufficient. raise error.
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L109", "Payment: Reserve not met.");
                                    bResponse = false;
                                    bError = true;
                                    break;
                                }
                            }
                        }

						arrReserveType = null;
						arrReserveAmt = null;
					}

                    //Start: rsushilaggar Collections from LSS 06/02/2010 MITS  20938
                    if (dFinalInvoiceAmt < 0 && GetReserevTotalPaid(iClaimId, iClaimantEid, 0, dFinalInvoiceAmt))
                    {
                        //Collection amount is greater than Paid Total. raise error.
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Payment: Inadequate Reserve Balance in RISKMASTER X");
                        bResponse = false;
                        bError = true;
                        break;
                    }
                    //end: rsushilaggar
					//check if payee exists. if yes, update that. if no, create new.
					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//check if payee exists
						iEntityId = 0;
                        //gagnihotri MITS 12697 07/17/2008
                        //using (DbReader objReaderTemp = DbFactory.GetDbReader(m_sDSN, "SELECT ENTITY_ID FROM ENTITY_SUPP WHERE LSS_ADMIN_ID = " + iFirmOfficeId))
                        //iEntityTableId = objCache.GetTableId("ATTORNEY_FIRMS");Sumit
                        //sSQLTemp = "SELECT ES.ENTITY_ID FROM ENTITY AS E INNER JOIN ENTITY_SUPP AS ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + iFirmOfficeId + " and E.ENTITY_TABLE_ID = " + iEntityTableId;Sumit

                        //Sumit - 02/18/2009 Payee Data Exchange: MITS #20073
                        //Modified the entire flow in MCIC Phase 3.
                        //LSS will send the entity ID.If the node is present then RMX will check if that ID is present in RMX DB.If ID is not found then error is returned back.
                        //If node is not present then the existing flow will be used which checks for firm office ID.

                        if (bRMXEntityExist)
                        {
                            sSQLTemp = "SELECT ENTITY_ID FROM ENTITY WHERE ENTITY_ID = " + iRMXEntityId;
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQLTemp))
                            {
                                if (objReaderTemp.Read())
                                {
                                    iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                }
                                else
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L114", "Data push failed:{Payee: " + iRMXEntityId + " not found.}");
                                    bResponse = false;
                                    bError = true;
                                }
                            }
                        }//start rsushilaggar MITS 25581 Date 08/11/2011
                        else
                        {
                            sSQLTemp = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + iFirmOfficeId;// +" and E.ENTITY_TABLE_ID = " + objCache.GetTableId(sRMXEntityType);
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQLTemp))
                            {
                                if (objReaderTemp.Read())
                                {
                                    iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                }
                            }

                        }
                        //End rsushilaggar

                        if (bError == false)
                        {
                            //Debabrata Biswas Dated 10/13/2009. Multi Data Exhange MITS# 20036
                            //Add/Update RMX Entities pushed from LSS only if AllowRMXEntityUpdate is set yes.
                            if (bAllowAddUpdateLSSEntity)
                            {
                                if (objXmlNode.SelectSingleNode("//ClaimsSubsequentRptSubmitRq/ClaimsParty") != null)
                                {
                                    objPartyNode = objXmlNode.SelectSingleNode("//ClaimsSubsequentRptSubmitRq/ClaimsParty");
                                    sPartyType = CommonFunctions.GetValue(objPartyNode, "//ClaimsPartyInfo/ClaimsPartyRoleCd", nsmgr);
                                }

                                if (sPartyType.Equals("csc:FirmOffice"))
                                {
                                    //sFirstName = "";		//no first name given in XML
                                    sLastName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/NameInfo/CommlName/CommercialName", nsmgr);
                                    sAddr1 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr1", nsmgr);
                                    sAddr2 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr2", nsmgr);
                                    sCity = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/City", nsmgr);
                                    iStateId = objCache.GetStateRowID(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/StateProvCd", nsmgr));
                                    sZipCode = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/PostalCode", nsmgr);
                                    iCountryCode = objCache.GetCodeId(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/CountryCd", nsmgr), "COUNTRY");
                                    sTaxId = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/NameInfo/TaxIdentity/TaxId", nsmgr);
                                    sPhone1 = ReadTagValue(objPartyNode, "PhoneInfo", "Office", "PhoneNumber");
                                    sPhone2 = ReadTagValue(objPartyNode, "PhoneInfo", "TollFreeNumber", "PhoneNumber");
                                    sFax = ReadTagValue(objPartyNode, "PhoneInfo", "Fax", "PhoneNumber");
                                    sEmail = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);


                                    //create/update entity with provided Firm information
                                    objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                                    iEntityTableId = objCache.GetTableId(sRMXEntityType);
                                    if (!bRMXEntityExist)
                                    {
                                        sSQLTemp = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + iFirmOfficeId;// +" and E.ENTITY_TABLE_ID = " + iEntityTableId;
                                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQLTemp))
                                        {
                                            if (objReaderTemp.Read())
                                            {
                                                iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                            }
                                        }
                                    }

                                    if (iEntityId > 0) objEntity.MoveTo(iEntityId);
                                    //else objEntity.AddedByUser = "LSSINF";
                                    else bNewEnt = true;

                                    //gagnihotri MITS 12697 07/17/2008
                                    //objEntity.EntityTableId = objCache.GetTableId("ATTORNEY_FIRMS");
                                    //Rijul RMA 7914 start
                                    //objEntity.EntityTableId = iEntityTableId;
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                        objEntity.EntityTableId = 0;
                                    else
                                        objEntity.EntityTableId = iEntityTableId;
                                    //Rijul RMA 7914 end
                                    //objEntity.FirstName = sFirstName;		//no first name
                                    objEntity.LastName = sLastName;
                                    objEntity.Addr1 = sAddr1;
                                    objEntity.Addr2 = sAddr2;
                                    objEntity.City = sCity;
                                    objEntity.StateId = iStateId;
                                    objEntity.ZipCode = sZipCode;
                                    objEntity.CountryCode = iCountryCode;
                                    objEntity.TaxId = sTaxId;
                                    objEntity.Phone1 = sPhone1;
                                    objEntity.Phone2 = sPhone2;
                                    objEntity.FaxNumber = sFax;
                                    objEntity.EmailAddress = sEmail;
                                    objEntity.Supplementals["LSS_ADMIN_ID"].Value = iFirmOfficeId;
                                    //rsushilaggar : Set the entity approval status "APPROVED" by default for all the new entities that are coming from LSS. 
                                    objEntity.EntityApprovalStatusCode = objCache.GetCodeId("A", "ENTITY_APPRV_REJ");
                                    //Rijul RMA 7914 start
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                    {
                                        if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                                        {
                                            objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                                            objEntityRole.EntityTableId = iEntityTableId;
                                            objEntityRole.AddedByUser = "LSSINF";
                                            objEntity.EntityXRoleList.Add(objEntityRole);
                                        }
                                    }
                                    //Rijul RMA 7914 end

                                    objEntity.Save();

                                    //assign newly created Entity id to iEntityId
                                    if (iEntityId == 0)
                                        iEntityId = objEntity.EntityId;

                                    objEntity.Dispose();
                                    objEntity = null;

                                    //set the added by field value LSSINF if new record was added.
                                    if (bNewEnt == true)
                                        SetField("ENTITY", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iEntityId.ToString());
                                }


                                if (!bRMXEntityExist && iEntityId == 0)
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Payee not found.}");
                                    bResponse = false;
                                    bError = true;
                                }
                            }//rsushilaggar MITS 22472 DATE 09/28/2010
                            else
                            {
                                if (!bRMXEntityExist && iEntityId == 0)
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Payee not found.}");
                                    bResponse = false;
                                    bError = true;
                                }
                            }
						}
					}


					//if everything is ok and there is no error till this point
					//then we can create payment if bError == false
					if(bError == false)		
					{
						iTransId = 0;

						objFunds = (Funds)objDMF.GetDataModelObject("Funds", false);

						//logging3 - begin update
                        CommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

						objFunds.ClaimId = iClaimId;
						objFunds.ClaimNumber = sClaimNumber;

                        //Set claimant_Eid for WC/DI claim
                        if (iClaimantEid > 0)
                        {
                            objFunds.ClaimantEid = iClaimantEid;
                        }

                        objFunds.AccountId = this.AccountId;
                        objFunds.DstrbnType = this.DistributionType;

						objFunds.Amount = dFinalInvoiceAmt;
						objFunds.TransDate = sFinalDate;
						objFunds.DateOfCheck = sFinalDate;		//as per Jeff Blanchard we should update this with Trans Date, this is the way we are doing in RM. when check will be printed then this date will be overwritten.
                        //Start: rsushilaggar Collections from LSS 06/02/2010 MITS 20938
                        if (dFinalInvoiceAmt < 0)
                        {
                            objFunds.CollectionFlag = true;
                            objFunds.PaymentFlag = false;
                            objFunds.Amount = objFunds.Amount * (-1);
                        }
                        else
                        {
                            objFunds.PaymentFlag = true;
                            objFunds.CollectionFlag = false;
                        }
                        //skhare7 MITS 27671
                        if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                        {
                            objFunds.BaseToClaimCurRate = dExcgB2C;
                            objFunds.ClaimCurrencyType = iClaimCurrCode;
							//rkotak:start, mits 28131,28128
                            objFunds.ClaimCurrencyAmount=dClaimAmt;
                            objFunds.PmtCurrencyType = iClaimCurrCode;
                            objFunds.PmtToBaseCurRate = dExchangeRateCl2Bs;
                            objFunds.PmtToClaimCurRate = 1.0;
                            objFunds.BaseToPmtCurRate = dExcgB2C;
                            objFunds.PmtCurrencyAmount =dClaimAmt;
							//rkotak:end
                        }
						//end: rsushilaggar

                        // Sumit - 02/06/2009 Modified as per the config entry in RiskmasterExtensibility.config
                        //Start: rsushilaggar: MITS 20036/20938 05/21/2010

                        if ((bSetLSSPaymentOnHold && (dFinalInvoiceAmt >= 0)) || (bPutLssCollectionOnHold && (dFinalInvoiceAmt < 0)))
                        {
                            objFunds.StatusCode = objCache.GetCodeId("H", "CHECK_STATUS");	//released status
                        }
                        else
                        {
                            objFunds.StatusCode = objCache.GetCodeId("R", "CHECK_STATUS");	//released status
                        }
                        //}
                        //End: rsushilaggar: 
						objFunds.Supplementals["LSS_INVOICE_ID"].Value = iInvoiceId;
                        // Start Sumit - 1/12/2009 Added for LSS Invoice UserId - MCIC Phase 3 Payee Data Exchange: MITS #20073
                        objFunds.Supplementals["LSS_HISTORY_ID"].Value = iInvoiceHistoryId; 
                        objFunds.Supplementals["LSS_USER_ID"].Value = sLSSApproverUserId;  
                        // End Sumit - 1/12/2009 Added for LSS Invoice UserId - MCIC Phase 3 Payee Data Exchange: MITS #20073
						objFunds.PayeeTypeCode = objCache.GetCodeId("O", "PAYEE_TYPE");		//Other payee

						objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
						objEntity.MoveTo(iEntityId);

						objFunds.PayeeEid = iEntityId;
						objFunds.FirstName = objEntity.FirstName;
						objFunds.LastName = objEntity.LastName;
						objFunds.Addr1 = objEntity.Addr1;
						objFunds.Addr2 = objEntity.Addr2;
						objFunds.City = objEntity.City;
						objFunds.StateId = objEntity.StateId;
						objFunds.CountryCode = objEntity.CountryCode;
						objFunds.ZipCode = objEntity.ZipCode;
                       

						//objFunds.AddedByUser = "LSSINF";
                       
						objEntity.Dispose();
						objEntity = null;
						
						//	= iMatterId;			//TODO: no where to store this
						//	= dInvoiceAmt;			//TODO: should we store this in Invoice table or in FundsTransSplit

						for (int i = 0; i < arrTransType.Count; i++)
						{
							objFundsTransSplit = (FundsTransSplit)objDMF.GetDataModelObject("FundsTransSplit", false);

                            objFundsTransSplit.InvoiceAmount = dInvoiceAmt;
							objFundsTransSplit.InvoiceNumber = sFirmInvoiceNum;
							objFundsTransSplit.InvoiceDate = sInvoiceDt;
							objFundsTransSplit.FromDate = sMinTaskDt;
							objFundsTransSplit.ToDate = sMaxTaskDt;
							objFundsTransSplit.TransTypeCode = objCache.GetCodeId(arrTransType[i], "TRANS_TYPES");
							objFundsTransSplit.ReserveTypeCode = objCache.GetRelatedCodeId(objCache.GetCodeId(arrTransType[i], "TRANS_TYPES"));
                            objFundsTransSplit.Amount = Common.Conversion.ConvertObjToDouble(arrTransAmt[i], m_iClientId);
                           //rkotak:mits 28131,28128
						    if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                            {
                                objFundsTransSplit.ClaimCurrencyAmount = objFundsTransSplit.Amount * dExcgB2C;
                                objFundsTransSplit.PmtCurrencyAmount = objFundsTransSplit.Amount * dExcgB2C;
                            }
							//rkotak:mits 28131,28128
							//objFundsTransSplit.AddedByUser = "LSSINF";
                            if (dFinalInvoiceAmt < 0)
                                objFundsTransSplit.Amount = objFundsTransSplit.Amount * (-1);

							objFunds.TransSplitList.Add(objFundsTransSplit);

							objFundsTransSplit.Dispose();
							objFundsTransSplit = null;
						}
                        objFunds.PayToTheOrderOf = objFunds.FirstName + " " + objFunds.LastName;
                        //Added by Amitosh for mits 28006
                        objFundsXPayee = (FundsXPayee)objDMF.GetDataModelObject("FundsXPayee", false);
                        objFundsXPayee.PayeeEid = iEntityId;
                        objFundsXPayee.Payee1099Flag = true;
                        objFundsXPayee.PayeeTypeCode = objFunds.PayeeTypeCode;
                        objFunds.FundsXPayeeList.Add(objFundsXPayee);
                        //End Amitosh


                        //rsushilaggar MITS 32694 
                        objFunds.FiringScriptFlag = 2;

						objFunds.Save();		//Reservers are also updated here

						iTransId = objFunds.TransId;
                       
						objFunds.Dispose();
						objFunds = null;

						//set the added by field value LSSINF if new record was added.
						SetField("FUNDS", "ADDED_BY_USER", "LSSINF", "TRANS_ID", iTransId.ToString());
						SetField("FUNDS_TRANS_SPLIT", "ADDED_BY_USER", "LSSINF", "TRANS_ID", iTransId.ToString());

						//logging4 - end update
                        CommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "PaymentResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;

					}	//if(bError == false)
				}		//foreach
			}			//try
			catch(Exception p_objException)
			{
				AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
				bResponse = false;		
				bError = true;

				//logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
			}
			finally
			{
				objEntity = null;
				objFundsTransSplit = null;
				objFunds = null;
				nsmgr = null;
				arrTransType = null;
				arrTransAmt = null;

				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}

                if (objFundsXPayee != null)
                {
                    objFundsXPayee.Dispose();
                    objFundsXPayee = null;
                }
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
                objSysSetting = null;
			}

			//logging6 - begin response
            CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
				CommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "LSS", "CMPD", objPaymentXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
				CommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "LSS", "FAIL", objPaymentXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}
        //Added by sharishkumar for Mits 35472
        private XmlDocument CcPaymentImport(XmlDocument objPaymentXml)
        {
            DataModelFactory objDMF = null;
            Funds objFunds = null;
            FundsTransSplit objFundsTransSplit = null;
            Entity objEntity = null;
            EntityXRole objEntityRole = null; //Rijul RMA 7914
            LocalCache objCache = null;
            ReserveCurrent objReserveCurr = null;

            XmlDocument objResponse = null;
            XmlNamespaceManager nsmgr = null;


            string sMainRqUID = string.Empty;

            //Funds record
            int iTransId = 0;
            string sRqUID = string.Empty;
            int iFirmOfficeId = 0;
            int iEntityId = 0;
            int iInvoiceId = 0;
            
            int iInvoiceHistoryId = 0;
            string sLSSApproverUserId = string.Empty;
            int iRMXEntityId = 0;
            bool bRMXEntityExist = false;
            string sRMXEntityType = string.Empty;
            
            int iMatterId = 0;
            string sPaymentSequence = string.Empty;
            string sClaimNumber = string.Empty;
            int iClaimId = 0;
            int iLOBCode = 0;
            int iClaimStatusCode = 0;
            int iClaimantEid = 0;
            double dInvoiceAmt = 0;
            double dFinalInvoiceAmt = 0;
            string sFirmInvoiceNum = string.Empty;
            string sInvoiceDt = string.Empty;
            string sFinalDate = string.Empty;
            string sMinTaskDt = string.Empty;
            string sMaxTaskDt = string.Empty;
            string sResTypecode = string.Empty;
            //Funds Trans record	- this can be multiple records for one Payment
            string sPayTypeCd = string.Empty;
            double dAmt = 0;
            StringCollection arrTransType = null;
            ArrayList arrTransAmt = null;


            XmlNode objPartyNode = null;
            string sPartyType = string.Empty;
            string sLastName = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sCity = string.Empty;
            int iStateId = 0;
            string sZipCode = string.Empty;
            int iCountryCode = 0;
            string sTaxId = string.Empty;
            string sPhone1 = string.Empty;
            string sPhone2 = string.Empty;
            string sFax = string.Empty;
            string sEmail = string.Empty;
            int sRcRowId = 0;            
            double dTemp = 0;
            int iTemp = 0;
            bool bResponse = false;		//this is set to false when error - this is not used
            bool bError = false;		//this is set to true when error

            //logging1
            int iReqId = 0;
            int iEventId = 0;
            bool bNewEnt = false;
           
            int iEntityTableId = 0;
            string sSQLTemp = string.Empty;
            
            SysParms objSysSetting = null;
            int iClaimCurrCode = 0;
            double dExcgB2C = 0.0;
            FundsXPayee objFundsXPayee = null;
            try
            {
                objSysSetting = new SysParms(this.RiskmasterConnectionString, m_iClientId);
                //logging2 - enter initial records in loggin tables

                if (this.IsLoggingEnabled)
                {
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
                }
                CommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "LSS", "STRT", objPaymentXml.OuterXml, this.RiskmasterConnectionString);
                CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);                
                objCache = new LocalCache(this.RiskmasterConnectionString, m_iClientId);

                nsmgr = new XmlNamespaceManager(objPaymentXml.NameTable);
                nsmgr.AddNamespace("csc", objPaymentXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                if (objPaymentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
                    sMainRqUID = objPaymentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;

                //following for loop will iterate through all the payments coming in XML
                foreach (XmlNode objXmlNode in objPaymentXml.SelectNodes("//ClaimsSubsequentRptSubmitRq"))
                {
                    objFunds = null;
                    objFundsTransSplit = null;
                    objEntity = null;

                    iTransId = 0;
                    sRqUID = string.Empty;
                    iFirmOfficeId = 0;
                    iEntityId = 0;
                    iInvoiceId = 0;
                    
                    iInvoiceHistoryId = 0;
                    sLSSApproverUserId = string.Empty;
                    iRMXEntityId = 0;
                    bRMXEntityExist = false;
                    sRMXEntityType = string.Empty;
                    
                    iMatterId = 0;
                    sPaymentSequence = string.Empty;
                    sClaimNumber = string.Empty;
                    iClaimId = 0;
                    dInvoiceAmt = 0;
                    dFinalInvoiceAmt = 0;
                    sFirmInvoiceNum = string.Empty;
                    sInvoiceDt = string.Empty;
                    sFinalDate = string.Empty;
                    sMinTaskDt = string.Empty;
                    sMaxTaskDt = string.Empty;

                    //Funds Trans record	- this can be multiple records for one Payment
                    sPayTypeCd = string.Empty;
                    dAmt = 0;
                    arrTransType = new StringCollection();
                    arrTransAmt = new ArrayList();

                    bResponse = false;
                    bError = false;

                    objPartyNode = null;
                    sPartyType = string.Empty;
                    sLastName = string.Empty;
                    sAddr1 = string.Empty;
                    sAddr2 = string.Empty;
                    sCity = string.Empty;
                    iStateId = 0;
                    sZipCode = string.Empty;
                    iCountryCode = 0;
                    sTaxId = string.Empty;
                    sPhone1 = string.Empty;
                    sPhone2 = string.Empty;
                    sFax = string.Empty;
                    sEmail = string.Empty;
                    sRcRowId = 0;                    
                    dTemp = 0;
                    iTemp = 0;
                    sResTypecode = string.Empty;
                    bNewEnt = false;
                    bool allowPaymentToClosedClaims = false;
                    if (objXmlNode.SelectSingleNode("RqUID") != null)
                        sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
                    else
                        sRqUID = sMainRqUID;

                    iFirmOfficeId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:FirmOfficeId", "OtherId"));

                    iInvoiceId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:InvoiceId", "OtherId"));
                   
                    iInvoiceHistoryId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:InvoiceHistoryId", "OtherId"));

                    sLSSApproverUserId = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:ApproverUserId", "OtherId");

                    sRMXEntityType = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:RMXEntityType", "OtherId");

                    iRMXEntityId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:RMXEntityID", "OtherId"));
                    if (iRMXEntityId != 0) bRMXEntityExist = true;

                    iMatterId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objXmlNode, "OtherIdentifier", "csc:MatterId", "OtherId"));

                    sPaymentSequence = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:PaymentSequence", "OtherId");

                    if (objXmlNode.SelectSingleNode("//csc:InsurerId", nsmgr) != null)
                        sClaimNumber = CommonFunctions.AddAmparsand(objXmlNode.SelectSingleNode("//csc:InsurerId", nsmgr).InnerText);

                    using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE,CLAIM_CURR_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                    {
                        if (objReaderTemp.Read())
                        {
                            iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), 0);
                            iLOBCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), 0);
                            iClaimCurrCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(2), 0);
                        }
                    }

                    if (objXmlNode.SelectSingleNode("//csc:InvoiceAmt/Amt", nsmgr) != null)
                        dInvoiceAmt = Common.Conversion.ConvertStrToDouble(objXmlNode.SelectSingleNode("//csc:InvoiceAmt/Amt", nsmgr).InnerText);
                    if (objXmlNode.SelectSingleNode("//csc:FinalInvoiceAmt/Amt", nsmgr) != null)
                        dFinalInvoiceAmt = Common.Conversion.ConvertStrToDouble(objXmlNode.SelectSingleNode("//csc:FinalInvoiceAmt/Amt", nsmgr).InnerText);
                    objSysSetting = new SysParms(this.RiskmasterConnectionString, m_iClientId);
                    if (iClaimCurrCode != 0 && (objSysSetting.SysSettings.UseMultiCurrency == -1))
                    {

                        double dExchangeRateC2B = Common.CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, this.RiskmasterConnectionString, m_iClientId);
                        dInvoiceAmt = dExchangeRateC2B * dInvoiceAmt;

                        dFinalInvoiceAmt = dFinalInvoiceAmt * dExchangeRateC2B;

                        dExcgB2C = Common.CommonFunctions.ExchangeRateSrc2Dest(Common.CommonFunctions.GetBaseCurrencyCode(this.RiskmasterConnectionString), iClaimCurrCode, this.RiskmasterConnectionString, m_iClientId);
                    }

                    if (objXmlNode.SelectSingleNode("//csc:FirmInvoiceNumber", nsmgr) != null)
                        sFirmInvoiceNum = objXmlNode.SelectSingleNode("//csc:FirmInvoiceNumber", nsmgr).InnerText;

                    sInvoiceDt = ReadTagValue(objXmlNode, "EventInfo", "csc:InvoiceDate", "EventDt");

                    sFinalDate = ReadTagValue(objXmlNode, "EventInfo", "csc:FinalDate", "EventDt");

                    sMinTaskDt = ReadTagValue(objXmlNode, "EventInfo", "csc:MinTaskDate", "EventDt");

                    sMaxTaskDt = ReadTagValue(objXmlNode, "EventInfo", "csc:MaxTaskDate", "EventDt");

                    if (objXmlNode.SelectSingleNode("//FinancialKey/csc:ReserveKeyRowId", nsmgr) != null)
                        sRcRowId = Common.Conversion.ConvertStrToInteger(objXmlNode.SelectSingleNode("//FinancialKey/csc:ReserveKeyRowId", nsmgr).InnerText);

                    foreach (XmlNode objTransXmlNode in objXmlNode.SelectNodes("//csc:ClaimsPaymentDetail", nsmgr))
                    {
                        sPayTypeCd = "";
                        dAmt = 0;

                        if (objTransXmlNode.SelectSingleNode("csc:PaymentTypeCd", nsmgr) != null)
                            sPayTypeCd = CommonFunctions.AddAmparsand(objTransXmlNode.SelectSingleNode("csc:PaymentTypeCd", nsmgr).InnerText);

                        if (objTransXmlNode.SelectSingleNode("csc:PaymentTypeAmt/Amt", nsmgr) != null)
                        {
                            dAmt = Common.Conversion.ConvertStrToDouble(objTransXmlNode.SelectSingleNode("csc:PaymentTypeAmt/Amt", nsmgr).InnerText);                            
                            dClaimAmt = dAmt;
                            if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                            {
                                dExchangeRateCl2Bs = Common.CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, objSysSetting.SysSettings.BaseCurrencyType, this.RiskmasterConnectionString, 0);//skhare7 r8 Multicurrency

                                dAmt = dExchangeRateCl2Bs * dAmt;
                            }

                            if (!sPayTypeCd.Equals("") && dAmt != 0)
                            {
                                arrTransType.Add(sPayTypeCd);
                                arrTransAmt.Add(dAmt);
                                dTemp = dTemp + dAmt;
                            }
                        }
                    }

                    if (iFirmOfficeId == 0)
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Firm Office Id}");
                        bResponse = false;
                        bError = true;
                    }
                    else if (sRcRowId.Equals(0))                    
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V104", "Data push failed: Required Field Missing: {Reserve Id}");
                        bResponse = false;
                        bError = true;
                    }               
                    else if (sClaimNumber.Equals(""))
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Claim Number}");
                        bResponse = false;
                        bError = true;
                    }
                    else if (dFinalInvoiceAmt == 0)
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Final Invoice Amt: zero}");
                        bResponse = false;
                        bError = true;
                    }
                    else if (arrTransType.Count <= 0)
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Payment detail}");
                        bResponse = false;
                        bError = true;
                    }
                    else if (((dFinalInvoiceAmt - dTemp) > 0.009) && dFinalInvoiceAmt > 0)
                    {
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Final Invoice amount is not equal to sum of Payment details amount}");
                        bResponse = false;
                        bError = true;
                    }

                    if (bError == false)		//everything is ok and there is no error till this point. so go for another validation
                    {
                        //check if Trans type are valid
                        for (int i = 0; i < arrTransType.Count; i++)
                        {
                            iTemp = 0;
                            iTemp = objCache.GetCodeId(arrTransType[i], "TRANS_TYPES");

                            if (iTemp <= 0)
                            {
                                AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Payment Type: " + arrTransType[i] + "}");
                                bResponse = false;
                                bError = true;
                                break;
                            }                            
                            if(bError==false)
                            {
                                objReserveCurr = (ReserveCurrent)objDMF.GetDataModelObject("ReserveCurrent", false);
                                objReserveCurr.MoveTo(sRcRowId);
                                sResTypecode = objCache.GetRelatedShortCode(iTemp);                               
                               
                                if (objReserveCurr.ReserveTypeCode != objCache.GetRelatedCodeId(iTemp))
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Reserve Type sent from LSS not matched to the rmA reserve type");
                                    bResponse = false;
                                    bError = true;
                                    break;
                                }                               
                            }                            
                        }
                    }

                    if (bError == false)		//everything is ok and there is no error till this point. so go for another validation
                    {
                        //check if claim exists
                        iClaimId = 0;                                              
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, "SELECT CLAIM_ID, LINE_OF_BUS_CODE,CLAIM_STATUS_CODE FROM CLAIM WHERE CLAIM_NUMBER = '" + sClaimNumber.Replace("'", "''") + "'"))
                        {
                            if (objReaderTemp.Read())
                            {
                                iClaimId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                iLOBCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(1), m_iClientId);

                                iClaimStatusCode = Conversion.ConvertObjToInt(objReaderTemp.GetValue(2), m_iClientId);
                                if (objCache.GetRelatedShortCode(iClaimStatusCode) == "C")
                                {
                                    if (!this.UserLoginInstance.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_PAYMENT_CLOSED_CLM))
                                    {
                                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Payment: Not allowed on closed claims.");
                                        bResponse = false;
                                        bError = true;
                                    }
                                    else
                                    {
                                        allowPaymentToClosedClaims = true;
                                    }
                                }                              
                            }
                        }

                        if (iClaimId == 0)
                        {
                            AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
                            bResponse = false;
                            bError = true;
                        }                       
                    }

                    if ((dFinalInvoiceAmt > objReserveCurr.BalanceAmount) && dFinalInvoiceAmt > 0)
                    {                        
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "V109", "Payment: Inadequate Reserve Balance in RISKMASTER");
                        bResponse = false;
                        bError = true;
                        break;
                    }

                    //check for insufficient reserve
                    if (bError == false)		//everything is ok and there is no error till this point. so go for another validation
                    {
                        string sSQL = "SELECT CLAIMANT_EID FROM RESERVE_CURRENT WHERE RC_ROW_ID = " + sRcRowId.ToString();
                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL))
                        {
                            if (objReaderTemp.Read())
                            {
                                iClaimantEid = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            }
                        }                      

                        if (allowPaymentToClosedClaims == false) 
                        {
                            if (((dFinalInvoiceAmt - objReserveCurr.BalanceAmount) > 0.009) && dFinalInvoiceAmt > 0)
                                {
                                    //reserves are insufficient. raise error.
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L109", "Payment: Reserve not met.");
                                    bResponse = false;
                                    bError = true;                              
                                }                           
                        }                   
                    }

                    if (dFinalInvoiceAmt < 0 && ((dFinalInvoiceAmt * (-1)) > objReserveCurr.PaidTotal))
                    {
                        //Collection amount is greater than Paid Total. raise error.
                        AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Payment: Collection amount is greater than Paid Total");
                        bResponse = false;
                        bError = true;
                        break;
                    }          

                    //check if payee exists. if yes, update that. if no, create new.
                    if (bError == false)		//everything is ok and there is no error till this point. so go for another validation
                    {
                        //check if payee exists
                        iEntityId = 0;                       

                        if (bRMXEntityExist)
                        {
                            sSQLTemp = "SELECT ENTITY_ID FROM ENTITY WHERE ENTITY_ID = " + iRMXEntityId;
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQLTemp))
                            {
                                if (objReaderTemp.Read())
                                {
                                    iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                }
                                else
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L114", "Data push failed:{Payee: " + iRMXEntityId + " not found.}");
                                    bResponse = false;
                                    bError = true;
                                }
                            }
                        }
                        else
                        {
                            sSQLTemp = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + iFirmOfficeId;// +" and E.ENTITY_TABLE_ID = " + objCache.GetTableId(sRMXEntityType);
                            using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQLTemp))
                            {
                                if (objReaderTemp.Read())
                                {
                                    iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                }
                            }

                        }                        

                        if (bError == false)
                        {                            
                            //Add/Update RMX Entities pushed from LSS only if AllowRMXEntityUpdate is set yes.
                            if (bAllowAddUpdateLSSEntity)
                            {
                                if (objXmlNode.SelectSingleNode("//ClaimsSubsequentRptSubmitRq/ClaimsParty") != null)
                                {
                                    objPartyNode = objXmlNode.SelectSingleNode("//ClaimsSubsequentRptSubmitRq/ClaimsParty");
                                    sPartyType = CommonFunctions.GetValue(objPartyNode, "//ClaimsPartyInfo/ClaimsPartyRoleCd", nsmgr);
                                }

                                if (sPartyType.Equals("csc:FirmOffice"))
                                {
                                    //sFirstName = "";		//no first name given in XML
                                    sLastName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/NameInfo/CommlName/CommercialName", nsmgr);
                                    sAddr1 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr1", nsmgr);
                                    sAddr2 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr2", nsmgr);
                                    sCity = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/City", nsmgr);
                                    iStateId = objCache.GetStateRowID(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/StateProvCd", nsmgr));
                                    sZipCode = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/PostalCode", nsmgr);
                                    iCountryCode = objCache.GetCodeId(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/CountryCd", nsmgr), "COUNTRY");
                                    sTaxId = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/NameInfo/TaxIdentity/TaxId", nsmgr);
                                    sPhone1 = ReadTagValue(objPartyNode, "PhoneInfo", "Office", "PhoneNumber");
                                    sPhone2 = ReadTagValue(objPartyNode, "PhoneInfo", "TollFreeNumber", "PhoneNumber");
                                    sFax = ReadTagValue(objPartyNode, "PhoneInfo", "Fax", "PhoneNumber");
                                    sEmail = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);

                                    //create/update entity with provided Firm information
                                    objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                                    iEntityTableId = objCache.GetTableId(sRMXEntityType);
                                    if (!bRMXEntityExist)
                                    {
                                        sSQLTemp = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + iFirmOfficeId;// +" and E.ENTITY_TABLE_ID = " + iEntityTableId;
                                        using (DbReader objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQLTemp))
                                        {
                                            if (objReaderTemp.Read())
                                            {
                                                iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                            }
                                        }
                                    }

                                    if (iEntityId > 0) objEntity.MoveTo(iEntityId);
                                    //else objEntity.AddedByUser = "LSSINF";
                                    else bNewEnt = true;

                                    //Rijul RMA 7914 start
                                    //objEntity.EntityTableId = iEntityTableId;
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                        objEntity.EntityTableId = 0;
                                    else
                                        objEntity.EntityTableId = iEntityTableId;
                                    //Rijul RMA 7914 end
                                    //objEntity.FirstName = sFirstName;		//no first name
                                    objEntity.LastName = sLastName;
                                    objEntity.Addr1 = sAddr1;
                                    objEntity.Addr2 = sAddr2;
                                    objEntity.City = sCity;
                                    objEntity.StateId = iStateId;
                                    objEntity.ZipCode = sZipCode;
                                    objEntity.CountryCode = iCountryCode;
                                    objEntity.TaxId = sTaxId;
                                    objEntity.Phone1 = sPhone1;
                                    objEntity.Phone2 = sPhone2;
                                    objEntity.FaxNumber = sFax;
                                    objEntity.EmailAddress = sEmail;
                                    objEntity.Supplementals["LSS_ADMIN_ID"].Value = iFirmOfficeId;
                                    objEntity.EntityApprovalStatusCode = objCache.GetCodeId("A", "ENTITY_APPRV_REJ");

                                    //Rijul RMA 7914 start
                                    if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                    {
                                        if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                                        {
                                            objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                                            objEntityRole.EntityTableId = iEntityTableId;
                                            objEntityRole.AddedByUser = "LSSINF";
                                            objEntity.EntityXRoleList.Add(objEntityRole);
                                        }
                                    }
                                    //Rijul RMA 7914 end
                                    objEntity.Save();

                                    //assign newly created Entity id to iEntityId
                                    if (iEntityId == 0)
                                        iEntityId = objEntity.EntityId;

                                    objEntity.Dispose();
                                    objEntity = null;

                                    //set the added by field value LSSINF if new record was added.
                                    if (bNewEnt == true)
                                        SetField("ENTITY", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iEntityId.ToString());
                                }


                                if (!bRMXEntityExist && iEntityId == 0)
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Payee not found.}");
                                    bResponse = false;
                                    bError = true;
                                }
                            }
                            else
                            {
                                if (!bRMXEntityExist && iEntityId == 0)
                                {
                                    AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Payee not found.}");
                                    bResponse = false;
                                    bError = true;
                                }
                            }
                        }
                    }


                    //if everything is ok and there is no error till this point
                    //then we can create payment if bError == false
                    if (bError == false)
                    {
                        iTransId = 0;

                        objFunds = (Funds)objDMF.GetDataModelObject("Funds", false);

                        //logging3 - begin update
                        CommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

                        objFunds.ClaimId = iClaimId;
                        objFunds.ClaimNumber = sClaimNumber;

                        iClaimantEid = objReserveCurr.ClaimantEid;
                        //Set claimant_Eid for WC/DI claim
                        if (iClaimantEid > 0)
                        {
                            objFunds.ClaimantEid = iClaimantEid;
                        }

                        objFunds.AccountId = this.AccountId;

                        objFunds.Amount = dFinalInvoiceAmt;
                        objFunds.TransDate = sFinalDate;
                        objFunds.DateOfCheck = sFinalDate;		
                       
                        if (dFinalInvoiceAmt < 0)
                        {
                            objFunds.CollectionFlag = true;
                            objFunds.PaymentFlag = false;
                            objFunds.Amount = objFunds.Amount * (-1);
                        }
                        else
                        {
                            objFunds.PaymentFlag = true;
                            objFunds.CollectionFlag = false;
                        }
                       
                        if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                        {
                            objFunds.BaseToClaimCurRate = dExcgB2C;
                            objFunds.ClaimCurrencyType = iClaimCurrCode;
                            
                            objFunds.ClaimCurrencyAmount = dClaimAmt;
                            objFunds.PmtCurrencyType = iClaimCurrCode;
                            objFunds.PmtToBaseCurRate = dExchangeRateCl2Bs;
                            objFunds.PmtToClaimCurRate = 1.0;
                            objFunds.BaseToPmtCurRate = dExcgB2C;
                            objFunds.PmtCurrencyAmount = dClaimAmt;
                            
                        }
                        


                        if ((bSetLSSPaymentOnHold && (dFinalInvoiceAmt >= 0)) || (bPutLssCollectionOnHold && (dFinalInvoiceAmt < 0)))
                        {
                            objFunds.StatusCode = objCache.GetCodeId("H", "CHECK_STATUS");	//released status
                        }
                        else
                        {
                            objFunds.StatusCode = objCache.GetCodeId("R", "CHECK_STATUS");	//released status
                        }
                       
                        objFunds.Supplementals["LSS_INVOICE_ID"].Value = iInvoiceId;
                        
                        objFunds.Supplementals["LSS_HISTORY_ID"].Value = iInvoiceHistoryId;
                        objFunds.Supplementals["LSS_USER_ID"].Value = sLSSApproverUserId;
                       
                        objFunds.PayeeTypeCode = objCache.GetCodeId("O", "PAYEE_TYPE");		//Other payee

                        objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);
                        objEntity.MoveTo(iEntityId);

                        objFunds.PayeeEid = iEntityId;
                        objFunds.FirstName = objEntity.FirstName;
                        objFunds.LastName = objEntity.LastName;
                        objFunds.Addr1 = objEntity.Addr1;
                        objFunds.Addr2 = objEntity.Addr2;
                        objFunds.City = objEntity.City;
                        objFunds.StateId = objEntity.StateId;
                        objFunds.CountryCode = objEntity.CountryCode;
                        objFunds.ZipCode = objEntity.ZipCode;


                        //objFunds.AddedByUser = "LSSINF";

                        objEntity.Dispose();
                        objEntity = null;

                        //	= iMatterId;			//TODO: no where to store this
                        //	= dInvoiceAmt;			//TODO: should we store this in Invoice table or in FundsTransSplit

                        for (int i = 0; i < arrTransType.Count; i++)
                        {
                            objFundsTransSplit = (FundsTransSplit)objDMF.GetDataModelObject("FundsTransSplit", false);

                            objFundsTransSplit.InvoiceAmount = dInvoiceAmt;
                            objFundsTransSplit.InvoiceNumber = sFirmInvoiceNum;
                            objFundsTransSplit.RCRowId = sRcRowId;
                            objFundsTransSplit.CoverageId = objReserveCurr.CoverageId;
                            objFundsTransSplit.InvoiceDate = sInvoiceDt;
                            objFundsTransSplit.FromDate = sMinTaskDt;
                            objFundsTransSplit.ToDate = sMaxTaskDt;
                            objFundsTransSplit.TransTypeCode = objCache.GetCodeId(arrTransType[i], "TRANS_TYPES");
                            objFundsTransSplit.ReserveTypeCode = objReserveCurr.ReserveTypeCode;
                            objFundsTransSplit.Amount = Common.Conversion.ConvertObjToDouble(arrTransAmt[i], m_iClientId);
                           
                            if (objSysSetting.SysSettings.UseMultiCurrency == -1)
                            {
                                objFundsTransSplit.ClaimCurrencyAmount = objFundsTransSplit.Amount * dExcgB2C;
                                objFundsTransSplit.PmtCurrencyAmount = objFundsTransSplit.Amount * dExcgB2C;
                            }
                            
                            //objFundsTransSplit.AddedByUser = "LSSINF";
                            if (dFinalInvoiceAmt < 0)
                                objFundsTransSplit.Amount = objFundsTransSplit.Amount * (-1);

                            objFunds.TransSplitList.Add(objFundsTransSplit);

                            objFundsTransSplit.Dispose();
                            objFundsTransSplit = null;
                        }
                        objFunds.PayToTheOrderOf = objFunds.FirstName + " " + objFunds.LastName;
                        
                        objFundsXPayee = (FundsXPayee)objDMF.GetDataModelObject("FundsXPayee", false);
                        objFundsXPayee.PayeeEid = iEntityId;
                        objFundsXPayee.Payee1099Flag = true;
                        objFundsXPayee.PayeeTypeCode = objFunds.PayeeTypeCode;
                        objFunds.FundsXPayeeList.Add(objFundsXPayee);
                        


                      
                        objFunds.FiringScriptFlag = 2;

                        objFunds.Save();		//Reservers are also updated here

                        iTransId = objFunds.TransId;

                        objFunds.Dispose();
                        objFunds = null;

                        //set the added by field value LSSINF if new record was added.
                        SetField("FUNDS", "ADDED_BY_USER", "LSSINF", "TRANS_ID", iTransId.ToString());
                        SetField("FUNDS_TRANS_SPLIT", "ADDED_BY_USER", "LSSINF", "TRANS_ID", iTransId.ToString());

                        //logging4 - end update
                        CommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

                        AddError(ref objResponse, "PaymentResponse", sRqUID, "Success", "0", "Success");
                        bResponse = true;
                        bError = false;

                    }	//if(bError == false)
                }		//foreach
            }			//try
            catch (Exception p_objException)
            {
                AddError(ref objResponse, "PaymentResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
                bResponse = false;
                bError = true;

                //logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
            }
            finally
            {
                objEntity = null;
                objFundsTransSplit = null;
                objFunds = null;
                nsmgr = null;
                arrTransType = null;
                arrTransAmt = null;

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }

                if (objFundsXPayee != null)
                {
                    objFundsXPayee.Dispose();
                    objFundsXPayee = null;
                }
                if (objDMF != null)
                {
                    objDMF.Dispose();
                    objDMF = null;
                }
                objSysSetting = null;
            }

            //logging6 - begin response
            CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

            objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
            if (objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
                objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

            foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
            {
                objOrg.InnerText = m_sDsnName;
            }

            //logging7 - end response
            CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

            //logging8 - final entry
            if (bError == false)
            {
                CommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "LSS", "CMPD", objPaymentXml.OuterXml, this.RiskmasterConnectionString);
                CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
            }
            else
            {
                CommonFunctions.iLogDXRequest(iReqId, "PAYM", "TORM", "LSS", "FAIL", objPaymentXml.OuterXml, this.RiskmasterConnectionString);
                CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
            }

            return objResponse;
        }
        //End Mits 35472
		
		#endregion

		#region Private Admin Import Method
		/// Name		: AdminImport
		/// Author		: Manoj Agrawal
		/// Date Created: 09/01/2006
		/// <summary>		
		/// This method is used to import Admin data from LSS
		/// </summary>
		private XmlDocument AdminImport(XmlDocument objAdminXml)
		{
			DataModelFactory objDMF=null;
			Entity objEntity = null;
            EntityXRole objEntityRole = null; //Rijul RMA 7914
			LocalCache objCache = null;
			DbReader objReaderTemp = null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

			string sMainRqUID = "";

			//Admin (Entity) record
			string sRqUID = "";
			int iEntityId = 0;
			int iAdminId = 0;
			int iEntityParentId = 0;
			int iFirmOfficeId = 0;

			XmlNode objPartyNode = null;
			string sPartyType = "";
			int iEntityTableId = 0;

			string sFirstName = "";
			string sMiddleName = "";
			string sLastName = "";
			string sTaxId = "";
			string sAddr1 = "";
			string sAddr2 = "";
			string sCity = "";
			int iStateId = 0;
			string sZipCode = "";
			int iCountryCode = 0;
			string sPhone1 = "";
			string sPhone2 = "";
			string sFax = "";
			string sEmail = "";

			string sTemp = "";
			double dTemp = 0;
			int iTemp = 0;

			bool bResponse = false;		//this is set to false when error - this is not used
			bool bError = false;		//this is set to true when error

			//logging1
			int iReqId = 0;
			int iEventId = 0;
			bool bNewEnt = false;
            //gagnihotri MITS 12697 07/17/2008
            int iEntityParentTableId = 0;
            string sSQL = "";

            try
            {
                //logging2 - enter initial records in loggin tables				
                if (this.IsLoggingEnabled)
                {
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
                }
                CommonFunctions.iLogDXRequest(iReqId, "ADMN", "TORM", "LSS", "STRT", objAdminXml.OuterXml, this.RiskmasterConnectionString);
                CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
                //objCache = objDMF.Context.LocalCache;
                objCache = new LocalCache(this.RiskmasterConnectionString,m_iClientId);

                nsmgr = new XmlNamespaceManager(objAdminXml.NameTable);
                nsmgr.AddNamespace("csc", objAdminXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

                if (objAdminXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
                    sMainRqUID = objAdminXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;

                //following for loop will iterate through all the Admins coming in XML
                foreach (XmlNode objXmlNode in objAdminXml.SelectNodes("//ClaimsSubsequentRptSubmitRq"))
                {
                    objEntity = null;

                    sRqUID = "";
                    iEntityId = 0;
                    iAdminId = 0;
                    iEntityParentId = 0;
                    iFirmOfficeId = 0;

                    objPartyNode = null;
                    sPartyType = "";
                    iEntityTableId = 0;
                    sFirstName = "";
                    sMiddleName = "";
                    sLastName = "";
                    sTaxId = "";
                    sAddr1 = "";
                    sAddr2 = "";
                    sCity = "";
                    iStateId = 0;
                    sZipCode = "";
                    iCountryCode = 0;
                    sPhone1 = "";
                    sPhone2 = "";
                    sFax = "";
                    sEmail = "";

                    sTemp = "";
                    dTemp = 0;
                    iTemp = 0;

                    bResponse = false;
                    bError = false;
                    bNewEnt = false;

                    if (objXmlNode.SelectSingleNode("RqUID") != null)
                        sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
                    else
                        sRqUID = sMainRqUID;


                    if (bError == false)		//everything is ok and there is no error till this point. so go for another validation
                    {
                        if (objXmlNode.SelectSingleNode("//ClaimsSubsequentRptSubmitRq/ClaimsParty") != null)
                        {
                            objPartyNode = objXmlNode.SelectSingleNode("//ClaimsSubsequentRptSubmitRq/ClaimsParty");
                            sPartyType = CommonFunctions.GetValue(objPartyNode, "//ClaimsPartyInfo/ClaimsPartyRoleCd", nsmgr);
                        }

                        if (sPartyType.Equals("csc:FirmOffice"))
                        {
                            iAdminId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objPartyNode, "OtherIdentifier", "csc:FirmOfficeId", "OtherId"));

                            iEntityTableId = objCache.GetTableId("ATTORNEY_FIRMS");

                            //<NameInfo>
                            sFirstName = "";
                            sMiddleName = "";
                            sLastName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/NameInfo/CommlName/CommercialName", nsmgr);
                            sTaxId = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/NameInfo/TaxIdentity/TaxId", nsmgr);

                            //<Addr>
                            sAddr1 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr1", nsmgr);
                            sAddr2 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr2", nsmgr);
                            sCity = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/City", nsmgr);
                            iStateId = objCache.GetStateRowID(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/StateProvCd", nsmgr));
                            sZipCode = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/PostalCode", nsmgr);
                            iCountryCode = objCache.GetCodeId(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/CountryCd", nsmgr), "COUNTRY");

                            //<Communication>
                            sPhone1 = ReadTagValue(objPartyNode, "PhoneInfo", "Office", "PhoneNumber");
                            sPhone2 = ReadTagValue(objPartyNode, "PhoneInfo", "TollFreeNumber", "PhoneNumber");
                            sFax = ReadTagValue(objPartyNode, "PhoneInfo", "Fax", "PhoneNumber");
                            sEmail = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);
                        }
                        else if (sPartyType.Equals("csc:Judge"))
                        {
                            iAdminId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objPartyNode, "OtherIdentifier", "csc:JudgeId", "OtherId"));

                            iEntityTableId = objCache.GetTableId("JUDGES");

                            //<NameInfo>
                            sFirstName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr);
                            sMiddleName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr);
                            sLastName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr);
                            sTaxId = "";

                            //<Addr>
                            sAddr1 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr1", nsmgr);
                            sAddr2 = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/Addr2", nsmgr);
                            sCity = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/csc:CityLong", nsmgr);
                            iStateId = objCache.GetStateRowID(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/StateProvCd", nsmgr));
                            sZipCode = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/PostalCode", nsmgr);
                            iCountryCode = objCache.GetCodeId(CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Addr/CountryCd", nsmgr), "COUNTRY");

                            //<Communication>
                            sPhone1 = ReadTagValue(objPartyNode, "PhoneInfo", "Office", "PhoneNumber");
                            sPhone2 = ReadTagValue(objPartyNode, "PhoneInfo", "Cell", "PhoneNumber");
                            sFax = ReadTagValue(objPartyNode, "PhoneInfo", "Fax", "PhoneNumber");
                            sEmail = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);
                        }
                        else if (sPartyType.Equals("csc:Attorney"))
                        {
                            iAdminId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objPartyNode, "OtherIdentifier", "csc:AttorneyId", "OtherId"));
                            iFirmOfficeId = Common.Conversion.ConvertStrToInteger(ReadTagValue(objPartyNode, "OtherIdentifier", "csc:FirmOfficeId", "OtherId"));

                            iEntityTableId = objCache.GetTableId("ATTORNEYS");

                            //<NameInfo>
                            sFirstName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:GivenNameLong", nsmgr);
                            sMiddleName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:OtherGivenNameLong", nsmgr);
                            sLastName = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/csc:NameInfoLong/csc:PersonNameLong/csc:SurnameLong", nsmgr);
                            sTaxId = "";

                            //<Addr>		-- this info is not coming in Attorney XML
                            sAddr1 = "";
                            sAddr2 = "";
                            sCity = "";
                            iStateId = 0;
                            sZipCode = "";
                            iCountryCode = 0;

                            //<Communication>
                            sPhone1 = ReadTagValue(objPartyNode, "PhoneInfo", "Office", "PhoneNumber");
                            sPhone2 = ReadTagValue(objPartyNode, "PhoneInfo", "Cell", "PhoneNumber");
                            sFax = ReadTagValue(objPartyNode, "PhoneInfo", "Fax", "PhoneNumber");
                            sEmail = CommonFunctions.GetValue(objPartyNode, "GeneralPartyInfo/Communications/EmailInfo/EmailAddr", nsmgr);
                        }
                        else
                        {
                            AddError(ref objResponse, "AdminResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Admin Type: " + sPartyType + "}");
                            bResponse = false;
                            bError = true;
                        }
                    }


                    if (bError == false)
                    {
                        if (iAdminId == 0)
                        {
                            AddError(ref objResponse, "AdminResponse", sRqUID, "FAILED", "L108", "Data push failed: Missing Admin Data: {Admin Id}");
                            bResponse = false;
                            bError = true;
                        }
                        else
                        {
                            //check if Admin exists
                            iEntityId = 0;
                            //gagnihotri MITS 12697 07/17/2008
                            //objReaderTemp=DbFactory.GetDbReader(m_sDSN, "SELECT ENTITY_ID FROM ENTITY_SUPP WHERE LSS_ADMIN_ID = " + iAdminId);
                            sSQL = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + iAdminId;// +" and E.ENTITY_TABLE_ID = " + iEntityTableId;
                            objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL);
                            if (objReaderTemp.Read())
                            {
                                iEntityId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                            }
                            objReaderTemp.Close();
                            objReaderTemp = null;


                            //check if Attorney's parent exists. For other entitys there is no parent coming in XML
                            if (iFirmOfficeId != 0)
                            {
                                iEntityParentId = 0;
                                //gagnihotri MITS 12697 07/17/2008 Start
                                //objReaderTemp=DbFactory.GetDbReader(m_sDSN, "SELECT ENTITY_ID FROM ENTITY_SUPP WHERE LSS_ADMIN_ID = " + iFirmOfficeId);
                                iEntityParentTableId = objCache.GetTableId("ATTORNEY_FIRMS");
                                sSQL = "SELECT ES.ENTITY_ID FROM ENTITY E INNER JOIN ENTITY_SUPP ES ON E.ENTITY_ID = ES.ENTITY_ID WHERE ES.LSS_ADMIN_ID = " + iFirmOfficeId;// +" and E.ENTITY_TABLE_ID = " + iEntityParentTableId;
                                objReaderTemp = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL);
                                //gagnihotri MITS 12697 07/17/2008 End
                                if (objReaderTemp.Read())
                                {
                                    iEntityParentId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
                                }
                                objReaderTemp.Close();
                                objReaderTemp = null;
                            }
                        }
                    }


                    if (sPartyType.Equals("csc:Attorney") && iEntityParentId == 0)
                    {
                        AddError(ref objResponse, "AdminResponse", sRqUID, "FAILED", "L108", "Data push failed: Missing Admin Data: {Firm Office Id}");
                        bResponse = false;
                        bError = true;
                    }


                    if (bError == false)		//everything is ok and there is no error till this point. so go for another validation
                    {
                        //Debabrata Biswas Dated 10/13/2009. Multi Data Exhange MITS# 20036
                        //Add/Update RMX Entities pushed from LSS only if AllowRMXEntityUpdate is set yes.
                        if (bAllowAddUpdateLSSEntity)
                        {
                            objEntity = (Entity)objDMF.GetDataModelObject("Entity", false);


                            //logging3 - begin update
                            CommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

                            //check if entity exists. if yes, update that. if no, create new.
                            if (iEntityId > 0) objEntity.MoveTo(iEntityId);
                            //else objEntity.AddedByUser = "LSSINF";
                            else bNewEnt = true;

                            //Rijul RMA 7914 start
                            //objEntity.EntityTableId = iEntityTableId;
                            if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                                objEntity.EntityTableId = 0;
                            else
                                objEntity.EntityTableId = iEntityTableId;
                            //Rijul RMA 7914 end
                            objEntity.FirstName = sFirstName;
                            objEntity.MiddleName = sMiddleName;
                            objEntity.LastName = sLastName;
                            objEntity.Addr1 = sAddr1;
                            objEntity.Addr2 = sAddr2;
                            objEntity.City = sCity;
                            objEntity.StateId = iStateId;
                            objEntity.ZipCode = sZipCode;
                            objEntity.CountryCode = iCountryCode;
                            objEntity.TaxId = sTaxId;
                            objEntity.Phone1 = sPhone1;
                            objEntity.Phone2 = sPhone2;
                            objEntity.FaxNumber = sFax;
                            objEntity.EmailAddress = sEmail;
                            objEntity.ParentEid = iEntityParentId;
                            objEntity.Supplementals["LSS_ADMIN_ID"].Value = iAdminId;

                            //Rijul RMA 7914 start
                            if (objEntity.Context.InternalSettings.SysSettings.UseEntityRole)
                            {
                                if (objEntity.IsEntityRoleExists(objEntity.EntityXRoleList, iEntityTableId) < 0)
                                {
                                    objEntityRole = (EntityXRole)objDMF.GetDataModelObject("EntityXRole", false);
                                    objEntityRole.EntityTableId = iEntityTableId;
                                    objEntityRole.AddedByUser = "LSSINF";
                                    objEntity.EntityXRoleList.Add(objEntityRole);
                                }
                            }
                            //Rijul RMA 7914 end
                            objEntity.Save();

                            iEntityId = objEntity.EntityId;

                            objEntity.Dispose();
                            objEntity = null;

                            //set the added by field value LSSINF if new record was added.
                            if (bNewEnt == true)
                                SetField("ENTITY", "ADDED_BY_USER", "LSSINF", "ENTITY_ID", iEntityId.ToString());
                        }
                        //logging4 - end update
                        CommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

                        AddError(ref objResponse, "AdminResponse", sRqUID, "Success", "0", "Success");
                        bResponse = true;
                        bError = false;
                    }
                }		//foreach
            }			//try
            catch (Exception p_objException)
            {
                AddError(ref objResponse, "AdminResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
                bResponse = false;
                bError = true;

                //logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
            }
			finally
			{
                if (objEntity != null)
                {
                    objEntity.Dispose();
                }
                nsmgr = null;				

				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
				
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
			}

			//logging6 - begin response
            CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
				CommonFunctions.iLogDXRequest(iReqId, "ADMN", "TORM", "LSS", "CMPD", objAdminXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
				CommonFunctions.iLogDXRequest(iReqId, "ADMN", "TORM", "LSS", "FAIL", objAdminXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}
		
		#endregion

		#region Private Diary Import Method
		/// Name		: DiaryImport
		/// Author		: Manoj Agrawal
		/// Date Created: 09/01/2006
		/// <summary>		
		/// This method is used to import Diary data from LSS
		/// </summary>
		private XmlDocument DiaryImport(XmlDocument objDiaryXml)
		{
			DataModelFactory objDMF=null;
			WpaDiaryEntry objDiary = null;
			WpaDiaryAct objAct = null;
			LocalCache objCache = null;
			DbReader objReaderTemp = null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

			string sMainRqUID = "";

			string sRqUID = "";

			string sCategory = "";
			string sWorkActivity = "";
			string sTaskDescription = "";
			string sUserId = "";
			int iUserId = 0;
			string sInvoiceId = "";
			string sMatterId = "";
			string sClaimNumber = "";
			int iClaimNumber = 0;
			string sRegarding = "";

			string sTemp = "";
			double dTemp = 0;
			int iTemp = 0;

			bool bResponse = false;		//this is set to false when error - this is not used
			bool bError = false;		//this is set to true when error

			//logging1
			int iReqId = 0;
			int iEventId = 0;			

			try
			{
				//logging2 - enter initial records in loggin tables				
				if(this.IsLoggingEnabled)
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
				}
				CommonFunctions.iLogDXRequest(iReqId, "DIRY", "TORM", "LSS", "STRT", objDiaryXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				//objCache = objDMF.Context.LocalCache;
				objCache=new LocalCache(this.RiskmasterConnectionString,m_iClientId);

				nsmgr = new XmlNamespaceManager(objDiaryXml.NameTable);
				nsmgr.AddNamespace("csc", objDiaryXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				if(objDiaryXml.SelectSingleNode("/ACORD/ExtensionsSvcRq/RqUID") != null)
					sMainRqUID = objDiaryXml.SelectSingleNode("/ACORD/ExtensionsSvcRq/RqUID").InnerText;
                
				//following for loop will iterate through all the Diaries coming in XML
				foreach (XmlNode objXmlNode in objDiaryXml.SelectNodes("//csc:Diary", nsmgr))
				{
					objDiary = null;
					objAct = null;

					sRqUID = "";
					sCategory = "";
					sWorkActivity = "";
					sTaskDescription = "";
					sUserId = "";
					iUserId = 0;
					sInvoiceId = "";
					sMatterId = "";
					sClaimNumber = "";
					iClaimNumber = 0;
					sRegarding = "";

					sTemp = "";
					dTemp = 0;
					iTemp = 0;

					bResponse = false;
					bError = false;

					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;


					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						sCategory = CommonFunctions.GetValue(objXmlNode, "//csc:Category", nsmgr);

						sWorkActivity = CommonFunctions.GetValue(objXmlNode, "//csc:WorkActivity", nsmgr);
						sTaskDescription = CommonFunctions.GetValue(objXmlNode, "//csc:TaskDescription", nsmgr);

						sUserId = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:UserId", "OtherId");
						sInvoiceId = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:InvoiceId", "OtherId");
						sMatterId = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:MatterId", "OtherId");
						sClaimNumber = CommonFunctions.GetValue(objXmlNode, "ItemIdInfo/csc:InsurerId", nsmgr);
					}


					if(bError == false)
					{
						if(!sCategory.Equals("csc:Budget") && !sCategory.Equals("csc:Document") && !sCategory.Equals("csc:Payment"))
						{
							AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Diary Category: " + sCategory + "}");
							bResponse = false;		
							bError = true;
						}
					}


					if(bError == false)
					{
						if(sUserId.Equals(""))
						{
							AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L108", "Data push failed: Missing Admin Data: {User Id}");
							bResponse = false;		
							bError = true;
						}
						else if(sWorkActivity.Equals(""))
						{
							AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Work Activity}");
							bResponse = false;		
							bError = true;
						}
						else if(sTaskDescription.Equals(""))
						{
							AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Task Description}");
							bResponse = false;		
							bError = true;
						}
						else if(sClaimNumber.Equals(""))
						{
							AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Claim Number}");
							bResponse = false;		
							bError = true;
						}
					}


					if(bError == false)
					{
						objReaderTemp=DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), "SELECT USER_ID FROM USER_DETAILS_TABLE WHERE LOGIN_NAME = '" + sUserId + "' AND DSNID = (SELECT DSNID FROM DATA_SOURCE_TABLE WHERE DSN = '" + m_sDsnName + "')");//rkaur27 : Cloud Change
						if(objReaderTemp.Read())
						{
                            iUserId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
						}
						objReaderTemp.Close();
						//objReaderTemp = null;


						iClaimNumber = GetRowIdFromOtherField("CLAIM_ID", "CLAIM", "CLAIM_NUMBER", "'" + sClaimNumber + "'");


						if(iUserId == 0)
						{
							AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L108", "Data push failed: Missing Admin Data: {User Id: " + sUserId + "}");
							bResponse = false;		
							bError = true;
						}
						else if(iClaimNumber == 0)
						{
							AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
							bResponse = false;		
							bError = true;
						}
					}


					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//logging3 - begin update
                        CommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

						sTemp = Conversion.GetDateTime(DateTime.Now.ToString());
						objDiary = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);
						objDiary.EntryName = sWorkActivity;
						objDiary.EntryNotes = sTaskDescription;
						objDiary.CreateDate = sTemp;
						objDiary.CompleteTime = sTemp;
						objDiary.CompleteDate = sTemp.Substring(0, 8);
						objDiary.Priority = 1;		//1 = optional
						objDiary.StatusOpen = true;
						objDiary.AssigningUser = m_sUserName;		//with which user has login to import data
						objDiary.AssignedUser = sUserId;

						sRegarding = "Claim Number = " + sClaimNumber;
						if(!sInvoiceId.Equals(""))
							sRegarding = sRegarding + ", Invoice Id = " + sInvoiceId;
						if(!sMatterId.Equals(""))
							sRegarding = sRegarding + ", Matter Id = " + sMatterId;
						objDiary.Regarding = sRegarding;
                        //Added by Nitin for R5-R4 merging on 23-02-2009 for Mits 12930
                        //gagnihotri 09/11/2008 Uncommented and modified for MITS 12930. Changes Start
                        objDiary.IsAttached = true;
                        objDiary.AttachTable = "CLAIM";
                        objDiary.AttachRecordid = CommonFunctions.GetClaimId(sClaimNumber, this.RiskmasterConnectionString, m_iClientId);
                        //merging the changes of MITS 11622
                        //abisht MITS 11622
                        //When a new diary entry gets entered into WPA_DIARY_ENTRY a value for ASSIGNED_GROUP 
                        //column should be NA (Not available) instead of null.
                        //Null values cause a diary lag on Oracle db with a large numbers of records.
                        objDiary.AssignedGroup = "NA";
                        //gagnihotri Changes End
                        //Nitin for R5-R4 merging on 23-02-2009 for Mits 12930  Ends

						objAct = (WpaDiaryAct)objDMF.GetDataModelObject("WpaDiaryAct", false);
						objAct.ActCode = 0;
						objAct.ActText = sWorkActivity;
						objDiary.WpaDiaryActList.Add(objAct);
        
						objDiary.Save();

						objAct.Dispose();
						//objAct = null;

						objDiary.Dispose();
						//objDiary = null;

						//logging4 - end update
                        CommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "DiaryResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;
					}
				}		//foreach
			}			//try
			catch(Exception p_objException)
			{
				AddError(ref objResponse, "DiaryResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
				bResponse = false;		
				bError = true;

				//logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
			}
			finally
			{
                if (objAct != null)
                {
                    objAct.Dispose();
                }
                if (objDiary != null)
                {
                    objDiary.Dispose();
                }
				nsmgr = null;

				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
				
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
			}

			//logging6 - begin response
            CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ExtensionsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ExtensionsSvcRs/SPExtensionsSyncRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ExtensionsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ExtensionsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
				CommonFunctions.iLogDXRequest(iReqId, "DIRY", "TORM", "LSS", "CMPD", objDiaryXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
				CommonFunctions.iLogDXRequest(iReqId, "DIRY", "TORM", "LSS", "FAIL", objDiaryXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}
		
		#endregion

		#region Private Document Import Method
		/// Name		: DocumentImport
		/// Author		: Manoj Agrawal
		/// Date Created: 09/04/2006
		/// <summary>		
		/// This method is used to import Document data from LSS
		/// </summary>
		private XmlDocument DocumentImport(XmlDocument objDocumentXml)
		{
			DataModelFactory objDMF=null;

			LocalCache objCache = null;
			DbReader objReaderTemp = null;
			DbConnection objConn=null;

			XmlDocument objResponse = null;
			XmlNamespaceManager nsmgr = null;

			string sMainRqUID = "";

			string sRqUID = "";

			int iNewDocumentId = 0;
			int iExistingDocumentId = 0;
			string sDocManId = "";
			string sEnvId = "";
			string sClaimNumber = "";
			int iClaimNumber = 0;
			string sAttachmentDesc = "";		//subject
            string sAttachmentFilename = "";	//not the file name, just name given to document
            string sWebsiteURL = "";
			string sLastUpdateDtm = "";			//contains date in YYYYMMDD format
			string sCRUD = "";					//to find out if document is to create (C) or delete (D)
			string sKeyWord = "";
            string sDocTitle = "";

			string sTemp = "";
            double dTemp = 0;
			int iTemp = 0;

			bool bResponse = false;		//this is set to false when error - this is not used
			bool bError = false;		//this is set to true when error

			//logging1
			int iReqId = 0;
			int iEventId = 0;			

            //rsolanki2 : start updates for mits 19200
            Boolean bSuccess = false;
            string sTempAcrosoftUserId = m_sUserName;
            string sTempAcrosoftPassword = m_sPassword;
            //rsolanki2 : end updates for mits 19200

            //Raman Bhatia -- MCM Updates 09/07/2008
            //We need to move LSS document links to MCM server...
            Acrosoft objAcrosoft = null;
            DbReader objReader = null;
            SysSettings objSettings = null;
            int iReturnId = 0;
            string sSessionId = "";
            string sTempPath = "";
            bool bUseAcrosoftInterface = false;
            string sRMXEventNumber = "";
            string sRMXClaimNumber = "";
            string sAppExcpXml = "";
            //gagnihotri MITS 12996 03/05/2009
            //Handling the filenames, as Acrosoft do not allow more than 60 char
            int iFilenameLength = 0;
            int iPos = 0;
            string sExt = "";
			try
			{
				//logging2 - enter initial records in loggin tables				
				if(this.IsLoggingEnabled)
				{
                    iReqId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST", m_iClientId);
                    iEventId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DX_REQUEST_EVENT", m_iClientId);
				}
				CommonFunctions.iLogDXRequest(iReqId, "DOCM", "TORM", "LSS", "STRT", objDocumentXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "STRT", "", Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "STRT", this.RiskmasterConnectionString, m_iClientId);

                objDMF = new DataModelFactory(this.UserLoginInstance, m_iClientId);
				//objCache = objDMF.Context.LocalCache;
				objCache=new LocalCache(this.RiskmasterConnectionString,m_iClientId);

				nsmgr = new XmlNamespaceManager(objDocumentXml.NameTable);
				nsmgr.AddNamespace("csc", objDocumentXml.SelectSingleNode("/ACORD").GetNamespaceOfPrefix("csc"));

				if(objDocumentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID") != null)
					sMainRqUID = objDocumentXml.SelectSingleNode("/ACORD/ClaimsSvcRq/RqUID").InnerText;
                
				//Raman Bhatia -- MCM Updates 09/07/2008
                //We need to move LSS document links to MCM server...

                objSettings = new SysSettings(this.RiskmasterConnectionString, m_iClientId);
                if (objSettings.UseAcrosoftInterface)
                {
                   bUseAcrosoftInterface = true;
                   sTempPath = RMConfigurator.TempPath;

                   //rsolanki2 :  start updates for MCM mits 19200                                 

                   if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                   {
                       if (AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName] != null
                           && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName].RmxUser))
                       {
                           sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName].AcrosoftUserId;
                           sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_sUserName].AcrosoftPassword;
                       }
                       else
                       {
                           sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                           sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                       }
                   }
                    //rsolanki2 :  end updates for MCM mits 19200                                 
                }
				//following for loop will iterate through all the Documents coming in XML
				foreach (XmlNode objXmlNode in objDocumentXml.SelectNodes("//ClaimsSubsequentRptSubmitRq", nsmgr))
				{

					iNewDocumentId = 0;
					iExistingDocumentId = 0;
					sRqUID = "";
					sDocManId = "";
					sEnvId = "";
					sClaimNumber = "";
					iClaimNumber = 0;
					sRMXClaimNumber = "0";
                    sRMXEventNumber = "0";
					sAttachmentDesc = "";		//subject
                    sAttachmentFilename = "";	//not the file name, just name given to document
					sWebsiteURL = "";
					sLastUpdateDtm = "";			//contains date in YYYYMMDD format
					sCRUD = "";					//to find out if document is to create (C) or delete (D)
					sKeyWord = "";

					sTemp = "";
					dTemp = 0;
					iTemp = 0;
                    //gagnihotri MITS 12996 03/05/2009
                    //Handling the filenames, as Acrosoft do not allow more than 60 char
                    iFilenameLength = 0;
                    iPos = 0;
                    sExt = "";

					bResponse = false;
					bError = false;

					if(objXmlNode.SelectSingleNode("RqUID") != null)
						sRqUID = objXmlNode.SelectSingleNode("RqUID").InnerText;
					else
						sRqUID = sMainRqUID;


					if(bError == false)		//everything is ok and there is no error till this point. so go for another validation
					{
						//to find out if document is to create (C) or delete (D)
						sCRUD = CommonFunctions.GetValue(objXmlNode, "//csc:CRUD", nsmgr);
					}


					if(bError == false)
					{
						if(!sCRUD.Equals("C") && !sCRUD.Equals("D"))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {CRUD: " + sCRUD + "}");
							bResponse = false;		
							bError = true;
						}
					}


					if(bError == false)
					{
						sDocManId = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:DocManId", "OtherId");
						sEnvId = ReadTagValue(objXmlNode, "OtherIdentifier", "csc:EnvId", "OtherId");

						//mandatory
						sClaimNumber = CommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/ItemIdInfo/csc:InsurerId", nsmgr);

						//subject
						sAttachmentDesc = CommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/AttachmentDesc", nsmgr);

						//title. mandatory. not the file name, just name given to document.
						sAttachmentFilename = CommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/AttachmentFilename", nsmgr);
						
						//link to LSS document. mandatory
						sWebsiteURL = CommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/WebsiteURL", nsmgr);

						//contains date in YYYYMMDD format
						sLastUpdateDtm = CommonFunctions.GetValue(objXmlNode, "FileAttachmentInfo/csc:LastUpdateDtm", nsmgr);
						if(!sLastUpdateDtm.Equals(""))
							sLastUpdateDtm = sLastUpdateDtm + "000000";
						else
							sLastUpdateDtm = Conversion.GetDateTime(DateTime.Now.ToString());
					
						if(sDocManId.Equals(""))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {DocManId}");
							bResponse = false;		
							bError = true;
						}
						else if(sEnvId.Equals(""))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {EnvId}");
							bResponse = false;		
							bError = true;
						}
						else if(sAttachmentFilename.Equals(""))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Attachment File name}");
							bResponse = false;		
							bError = true;
						}
						else if(sWebsiteURL.Equals(""))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {WebsiteURL}");
							bResponse = false;		
							bError = true;
						}
						else if(sClaimNumber.Equals(""))
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L104", "Data push failed: Required Field Missing: {Claim Number}");
							bResponse = false;		
							bError = true;
						}
					}

                    //gagnihotri MITS 12996 03/05/2009
                    //Handling the filenames, as Acrosoft do not allow more than 60 char
                    if (bError == false)
                    {
                        iFilenameLength = sAttachmentFilename.Length;
                        if (iFilenameLength > 55)   //.html will be added to it later
                        {
                            iPos = sAttachmentFilename.LastIndexOf('.');
                            sExt = sAttachmentFilename.Substring(iPos);
                            sAttachmentFilename = sAttachmentFilename.Substring(0, (55 - sExt.Length)).Trim();  //leaving the margin for .docx
                            sAttachmentFilename = sAttachmentFilename + sExt;
                        }                        
                    }
					if(bError == false)
					{
						//document is attached to claim
						iClaimNumber = GetRowIdFromOtherField("CLAIM_ID", "CLAIM", "CLAIM_NUMBER", "'" + sClaimNumber + "'");

						if(iClaimNumber == 0)
						{
							AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L111", "Data push failed: Invalid value: {Claim Number: " + sClaimNumber + "}");
							bResponse = false;		
							bError = true;
						}
					}


					if(bError == false)		//everything is ok and there is no error till this point. create document in RMX.
					{

						//logging3 - begin update
                        CommonFunctions.iLogDXEventHist(iEventId, "BUPD", this.RiskmasterConnectionString, m_iClientId);

						sKeyWord = "DocManId=" + sDocManId + ";sEnvId=" + sEnvId;

						if(sCRUD.Equals("D"))
						{
							objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
							objConn.Open();
							objConn.ExecuteNonQuery("UPDATE DOCUMENT SET FOLDER_ID = -1 WHERE KEYWORDS = '" + sKeyWord + "'");
                            //gagnihotri MITS 13543 Document deleted in LSS should be deleted from RMX
                            //Should not be shown in attachments anymore
                            objConn.ExecuteNonQuery("DELETE FROM DOCUMENT_ATTACH WHERE DOCUMENT_ID IN (SELECT DOCUMENT_ID FROM DOCUMENT WHERE KEYWORDS = '" + sKeyWord + "')");
							objConn.Close();

                            //Raman Bhatia -- MCM Updates 09/07/2008
                            //We need to delete LSS document from MCM server...

                            if (bUseAcrosoftInterface)
                            {

                                //Raman Bhatia 07/03/2008
                                //LSS documents are not working with MCM.. they need to be uploaded to MCM too

                                objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                                objConn.Open();
                                string sSQL = "Select event_number , claim_number from claim where claim_id = " + iClaimNumber.ToString();
                                objReader = objConn.ExecuteReader(sSQL);
                                if (objReader.Read())
                                {
                                    sRMXEventNumber = Conversion.ConvertObjToStr(objReader.GetValue("event_number"));
                                    sRMXClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("claim_number"));
                                }
                                objConn.Close();

                                //Authenticating Acrosoft credentials

                                objAcrosoft = new Acrosoft(m_iClientId);  //dvatsa-cloud                              
                                //rsolanki2 :  start updates for MCM mits 19200                                 
                               
                                iReturnId = objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml);
                                //iReturnId = objAcrosoft.Authenticate(m_sUserName, m_sPassword, out sSessionId, out sAppExcpXml);

                                //rsolanki2 :  end updates for MCM mits 19200 

                                if (iReturnId == 0)
                                {
                                    //Set Search Data
                                    objAcrosoft.GetGlobalSearchCriteria = "<ASNDXSearchInput><ClaimNbr>" + sRMXClaimNumber +
                                        "</ClaimNbr><EventNbr>" + sRMXEventNumber + "</EventNbr></ASNDXSearchInput>";

                                    //Deleting old html
                                    //objAcrosoft.DeleteFile(m_sUserName, m_sPassword, (sAttachmentFilename + ".html"), string.Empty, 
                                    //    AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, out sAppExcpXml);

                                    //rsolanki2 :  start updates for MCM mits 19200 

                                    objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword, (sAttachmentFilename + ".html"), string.Empty,
                                    AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, out sAppExcpXml);
                                    //objAcrosoft.DeleteFile(m_sUserName, m_sPassword, (sAttachmentFilename + ".html"), string.Empty,
                                    //    AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, out sAppExcpXml);
                                    //rsolanki2 :  end updates for MCM mits 19200 
					
                                }
                            }

							//objConn=null;
						}
						else if(sCRUD.Equals("C"))
						{
							//either document already exists and we need to update that or create new one. decide this based on ManId and EnvId.

							objReaderTemp=DbFactory.GetDbReader(this.RiskmasterConnectionString,"SELECT DOCUMENT_ID FROM DOCUMENT WHERE KEYWORDS = '" + sKeyWord + "'");
							if (objReaderTemp.Read())
							{
                                iExistingDocumentId = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), m_iClientId);
							}
							objReaderTemp.Close();
							//objReaderTemp=null;

                            //Raman Bhatia -- MCM Updates 09/07/2008
                            //We need to move LSS document links to MCM server...

                            if(bUseAcrosoftInterface)
                            {
                               //Raman Bhatia: Generating the dummy HTML containing the link to LSS document
                               //for MCM consumption
                                //gagnihotri 03/05/2009 Bug: Filenames were different while storing and reading
                                //sDocTitle = sAttachmentFilename.PadRight(32, ' ').Substring(0, 32).Trim().Replace("'", "''");
                                sDocTitle = sAttachmentFilename;
                                GenerateHTMLForLSS(sTempPath, sWebsiteURL , sDocTitle);
                                objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud

                                objConn = DbFactory.GetDbConnection(this.RiskmasterConnectionString);
                                objConn.Open();
                                string sSQL = "Select event_number , claim_number from claim where claim_id = " + iClaimNumber.ToString();
                                objReader = objConn.ExecuteReader(sSQL);
                                if (objReader.Read())
                                {
                                    sRMXEventNumber = Conversion.ConvertObjToStr(objReader.GetValue("event_number"));
                                    sRMXClaimNumber = Conversion.ConvertObjToStr(objReader.GetValue("claim_number"));
                                }
                                objConn.Close();

                            }
							if(iExistingDocumentId == 0)
							{
								//document does not exist so create new one
                                iNewDocumentId = Utilities.GetNextUID(this.RiskmasterConnectionString, "DOCUMENT", m_iClientId);

								objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
								objConn.Open();
								//create the document entry in DOCUMENT table and attach info (to claim) in DOCUMENT_ATTACH table
								
								//default values
								/*ARCHIVE_LEVEL = 0
								DOCUMENT_CATEGORY = 0
								DOCUMENT_CLASS = 0
								DOCUMENT_TYPE = 0
								SECURITY_LEVEL = 0
								DOCUMENT_EXPDTTM = 0
								DOCUMENT_ACTIVE = 0*/

								objConn.ExecuteNonQuery("INSERT INTO DOCUMENT (DOCUMENT_ID, CREATE_DATE, DOCUMENT_NAME, DOCUMENT_SUBJECT, USER_ID, DOC_INTERNAL_TYPE, DOCUMENT_FILENAME, DOCUMENT_FILEPATH, FOLDER_ID, KEYWORDS, ARCHIVE_LEVEL, DOCUMENT_CATEGORY, DOCUMENT_CLASS, DOCUMENT_TYPE, SECURITY_LEVEL, DOCUMENT_EXPDTTM, DOCUMENT_ACTIVE) VALUES (" + iNewDocumentId + ", '" + sLastUpdateDtm + "', '" + sAttachmentFilename.PadRight(32, ' ').Substring(0, 32).Trim().Replace("'", "''") + "', '" + sAttachmentDesc.PadRight(50, ' ').Substring(0, 50).Trim().Replace("'", "''") + "', '" + m_sUserName + "', 3, '" + sAttachmentFilename.PadRight(255, ' ').Substring(0, 255).Trim().Replace("'", "''") + "', '" + sWebsiteURL.Replace("'", "''") + "', 0, '" + sKeyWord + "', 0, 0, 0, 0, 0, 0, 0)");
								objConn.ExecuteNonQuery("INSERT INTO DOCUMENT_ATTACH (TABLE_NAME, RECORD_ID, DOCUMENT_ID) VALUES ('CLAIM', " + iClaimNumber + ", " + iNewDocumentId + ")");
								objConn.Close();
								//objConn=null;
                                if (bUseAcrosoftInterface)
                                {

                                    //Raman Bhatia 07/03/2008
                                    //LSS documents are not working with MCM.. they need to be uploaded to MCM too
                                    
                                    //Authenticating Acrosoft credentials

                                    //rsolanki2 :  start updates for MCM mits 19200 
                                   
                                    iReturnId = objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml);

                                    if (iReturnId == 0)
                                    {
                                        //creating attachment folder
                                        objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            AcrosoftSection.AcrosoftAttachmentsTypeKey, sRMXEventNumber, sRMXClaimNumber,
                                            AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                                        //Posting HTML to Acrosoft..Raman Bhatia
                                        iReturnId = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            (sTempPath + "\\" + sAttachmentFilename + ".html"), sAttachmentFilename, "", sRMXEventNumber, sRMXClaimNumber,
                                            AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, string.Empty, string.Empty, out sAppExcpXml);

                                    }
                                   
                                    //rsolanki2 :  end updates for MCM mits 19200 					

                                    //iReturnId = objAcrosoft.Authenticate(m_sUserName , m_sPassword , out sSessionId , out sAppExcpXml);
                                    
                                    //if(iReturnId == 0)
                                    //{
                                    //    //creating attachment folder
                                    //    objAcrosoft.CreateAttachmentFolder(m_sUserName, m_sPassword, 
                                    //        AcrosoftSection.AcrosoftAttachmentsTypeKey, sRMXEventNumber, sRMXClaimNumber, 
                                    //        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                                    //    //Posting HTML to Acrosoft..Raman Bhatia
                                    //    iReturnId = objAcrosoft.StoreObjectBuffer(m_sUserName, m_sPassword, (sTempPath + "\\" + sAttachmentFilename + ".html") , sAttachmentFilename, "", sRMXEventNumber, sRMXClaimNumber, 
                                    //        AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, string.Empty, string.Empty, out sAppExcpXml); 
                                        
                                    //}
                                    
                                }
							}
							else
							{
								//document already exists so update that
								objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
								objConn.Open();

								//update the document entry in DOCUMENT table and attach info (to claim) in DOCUMENT_ATTACH table
								objConn.ExecuteNonQuery("UPDATE DOCUMENT SET DOCUMENT_NAME = '" + sAttachmentFilename.PadRight(32, ' ').Substring(0, 32).Trim().Replace("'", "''") + "', DOCUMENT_SUBJECT = '" + sAttachmentDesc.PadRight(50, ' ').Substring(0, 50).Trim().Replace("'", "''") + "', DOCUMENT_FILENAME = '" + sAttachmentFilename.PadRight(255, ' ').Substring(0, 255).Trim().Replace("'", "''") + "', DOCUMENT_FILEPATH = '" + sWebsiteURL.Replace("'", "''") + "' WHERE DOCUMENT_ID = " + iExistingDocumentId);
								objConn.ExecuteNonQuery("UPDATE DOCUMENT_ATTACH SET RECORD_ID = " + iClaimNumber + " WHERE DOCUMENT_ID = " + iExistingDocumentId);
								objConn.Close();
                                if (bUseAcrosoftInterface)
                                {

                                    //Raman Bhatia 07/03/2008
                                    //LSS documents are not working with MCM.. they need to be uploaded to MCM too
                                    
                                    //Authenticating Acrosoft credentials

                                    //rsolanki2 :  start updates for MCM mits 19200 
                                    iReturnId = objAcrosoft.Authenticate(sTempAcrosoftUserId, sTempAcrosoftPassword, out sSessionId, out sAppExcpXml);
                                    if (iReturnId == 0)
                                    {
                                        //Set Search Data
                                        objAcrosoft.GetGlobalSearchCriteria = "<ASNDXSearchInput><ClaimNbr>" + sRMXClaimNumber + 
                                            "</ClaimNbr><EventNbr>" + sRMXEventNumber + "</EventNbr></ASNDXSearchInput>";

                                        //Deleting old html
                                        objAcrosoft.DeleteFile(sTempAcrosoftUserId, sTempAcrosoftPassword, 
                                            (sAttachmentFilename + ".html"), "", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", out sAppExcpXml);

                                        //creating attachment folder
                                        objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            AcrosoftSection.AcrosoftAttachmentsTypeKey, sRMXEventNumber, sRMXClaimNumber,
                                            AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                                        //Posting HTML to Acrosoft..Raman Bhatia
                                        iReturnId = objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword,
                                            (sTempPath + "\\" + sAttachmentFilename + ".html"), sAttachmentFilename, "", sRMXEventNumber, 
                                            sRMXClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", "", out sAppExcpXml);
                                    }
                                    
                                    //rsolanki2 :  end updates for MCM mits 19200 

                                    //iReturnId = objAcrosoft.Authenticate(m_sUserName, m_sPassword, out sSessionId, out sAppExcpXml);
                                    //if (iReturnId == 0)
                                    //{
                                    //    //Set Search Data
                                    //    objAcrosoft.GetGlobalSearchCriteria = "<ASNDXSearchInput><ClaimNbr>" + sRMXClaimNumber + "</ClaimNbr><EventNbr>" + sRMXEventNumber + "</EventNbr></ASNDXSearchInput>";
                                        
                                    //    //Deleting old html
                                    //    objAcrosoft.DeleteFile(m_sUserName, m_sPassword, (sAttachmentFilename + ".html"), "", AcrosoftSection.AcrosoftAttachmentsTypeKey, "", out sAppExcpXml);

                                    //    //creating attachment folder
                                    //    objAcrosoft.CreateAttachmentFolder(m_sUserName, m_sPassword, AcrosoftSection.AcrosoftAttachmentsTypeKey, sRMXEventNumber, sRMXClaimNumber, 
                                    //        AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);

                                    //    //Posting HTML to Acrosoft..Raman Bhatia
                                    //    iReturnId = objAcrosoft.StoreObjectBuffer(m_sUserName, m_sPassword, (sTempPath + "\\" + sAttachmentFilename + ".html"), sAttachmentFilename, "", sRMXEventNumber, sRMXClaimNumber, AcrosoftSection.AcrosoftAttachmentsTypeKey, "", "", "", out sAppExcpXml); 


                                    //}

                                }
								//objConn=null;
							}
						}

						//logging4 - end update
                        CommonFunctions.iLogDXEventHist(iEventId, "EUPD", this.RiskmasterConnectionString, m_iClientId);

						AddError(ref objResponse, "DocumentResponse", sRqUID, "Success", "0", "Success");
						bResponse = true;		
						bError = false;
					}
				}		//foreach
			}			//try
			catch(Exception p_objException)
			{
				AddError(ref objResponse, "DocumentResponse", sRqUID, "FAILED", "L110", "Data push failed: Unknown Error: " + p_objException.Message);
				bResponse = false;		
				bError = true;

				//logging5 - log exception
                CommonFunctions.iLogDXEventException(iEventId, p_objException.Message, p_objException.Source, p_objException.StackTrace, this.RiskmasterConnectionString, m_iClientId);
			}
			finally
			{
				nsmgr = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objReaderTemp != null)
                {
                    objReaderTemp.Dispose();
                }
				if (objCache!=null)
				{
					objCache.Dispose();
					objCache = null;
				}
				
				if (objDMF!=null)
				{
					objDMF.Dispose();
					objDMF=null;
				}
                if (objSettings != null)
                {
                    objSettings = null;
                }
                if (objAcrosoft != null)
                {
                    objAcrosoft = null;
                }
			}

			//logging6 - begin response
            CommonFunctions.iLogDXEventHist(iEventId, "BRES", this.RiskmasterConnectionString, m_iClientId);

			objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs").RemoveChild(objResponse.SelectNodes("/ACORD/ClaimsSvcRs/ClaimsSubsequentRptSubmitRs").Item(0));
			if(objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID") != null)
				objResponse.SelectSingleNode("/ACORD/ClaimsSvcRs/RqUID").InnerText = sMainRqUID;

			foreach (XmlNode objOrg in objResponse.GetElementsByTagName("Org"))
			{
				objOrg.InnerText = m_sDsnName;
			}

			//logging7 - end response
            CommonFunctions.iLogDXEventHist(iEventId, "ERES", this.RiskmasterConnectionString, m_iClientId);

			//logging8 - final entry
			if(bError == false)
			{
				CommonFunctions.iLogDXRequest(iReqId, "DOCM", "TORM", "LSS", "CMPD", objDocumentXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "CMPD", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "CMPD", this.RiskmasterConnectionString, m_iClientId);
			}
			else
			{
				CommonFunctions.iLogDXRequest(iReqId, "DOCM", "TORM", "LSS", "FAIL", objDocumentXml.OuterXml, this.RiskmasterConnectionString);
				CommonFunctions.iLogDXRequestEvent(iEventId, iReqId, "FAIL", objResponse.OuterXml, Conversion.GetDateTime(DateTime.Now.ToString()), Conversion.GetDateTime(DateTime.Now.ToString()), this.RiskmasterConnectionString);
                CommonFunctions.iLogDXEventHist(iEventId, "FAIL", this.RiskmasterConnectionString, m_iClientId);
			}

			return objResponse;
		}
		
		#endregion

		#region Private Common Methods

		private void AddError(ref XmlDocument p_objResponse, string sResponseType, string sRqUID, string sStatusCode, string sErrorCode, string sStatusDesc)
		{
			bool bExist = false;
			XmlNode objErrorNode = null;
			string sErrorNode = "";
			string sErrorNodeParent = "";
            string sResponseXMLPath = "";

			if (p_objResponse == null)
			{
				p_objResponse = new XmlDocument();
                //gagnihotri R5 Changes
                //p_objResponse.Load(RMConfigurator.Value("LSSInterface/" + sResponseType));
                switch (sResponseType)
                {
                    case "MatterResponse":
                        sResponseXMLPath = "LSSInterface/MatterResponse.xml";
                        break;

                    case "CodeResponse":
                        sResponseXMLPath = "LSSInterface/CodeResponse.xml";
                        break;

                    case "PaymentResponse":
                        sResponseXMLPath = "LSSInterface/PaymentResponse.xml";
                        break;

                    case "AdminResponse":
                        sResponseXMLPath = "LSSInterface/AdminResponse.xml";
                        break;

                    case "DiaryResponse":
                        sResponseXMLPath = "LSSInterface/DiaryResponse.xml";
                        break;

                    case "DocumentResponse":
                        sResponseXMLPath = "LSSInterface/DocumentResponse.xml";
                        break;
                    case "ExpenseReserveResponse":
                        sResponseXMLPath = "LSSInterface/ExpenseReserveResponse.xml";//rsushuilaggar MITS 25167 Date 08/04/2011
                        break;

                    default:
                        sResponseXMLPath = "";
                        break;
                }
                p_objResponse.Load(Path.Combine(RMConfigurator.AppFilesPath, sResponseXMLPath));
				if(p_objResponse.GetElementsByTagName("ServerDt").Count > 0)
					p_objResponse.GetElementsByTagName("ServerDt").Item(0).InnerText = Conversion.GetDateTime(DateTime.Now.ToString());
			}

			switch(sResponseType)
			{
				case "MatterResponse":
					sErrorNode = "ClaimsSubsequentRptSubmitRs";
					sErrorNodeParent = "ClaimsSvcRs";
					break;

				case "CodeResponse":
					sErrorNode = "SPExtensionsSyncRs";
					sErrorNodeParent = "ExtensionsSvcRs";
					break;

				case "PaymentResponse":
					sErrorNode = "ClaimsSubsequentRptSubmitRs";
					sErrorNodeParent = "ClaimsSvcRs";
					break;

				case "AdminResponse":
					sErrorNode = "ClaimsSubsequentRptSubmitRs";
					sErrorNodeParent = "ClaimsSvcRs";
					break;

				case "DiaryResponse":
					sErrorNode = "SPExtensionsSyncRs";
					sErrorNodeParent = "ExtensionsSvcRs";
					break;

				case "DocumentResponse":
					sErrorNode = "ClaimsSubsequentRptSubmitRs";
					sErrorNodeParent = "ClaimsSvcRs";
					break;
                //Sumit (04/15/2009)-MITS# 15473 
                case "ExpenseReserveResponse":
                    sErrorNode = "LossRunInqRs";
                    sErrorNodeParent = "ClaimsSvcRs";
                    break;
				default:
					sErrorNode = "";
					sErrorNodeParent = "";
					break;
			}

			foreach (XmlNode objXmlNode in p_objResponse.GetElementsByTagName("RqUID"))
			{
				if(objXmlNode.InnerText.Equals(sRqUID))
				{
					bExist = true;		//if error has been put for same RqUID then don't put again
					break;
				}
			}

			if(bExist == false && !sErrorNode.Equals(""))
			{
                //paggarwal2: LSS Enhancement for MCIC: 5/12/2009
                if (p_objResponse.GetElementsByTagName(sErrorNode).Item(0) != null)
                {
				objErrorNode = p_objResponse.GetElementsByTagName(sErrorNode).Item(0).Clone();

				if(objErrorNode.SelectNodes("//RqUID").Count > 0)
					objErrorNode.SelectNodes("//RqUID").Item(0).InnerText = sRqUID;

				if(objErrorNode.SelectNodes("//TransactionResponseDt").Count > 0)
					objErrorNode.SelectNodes("//TransactionResponseDt").Item(0).InnerText = Conversion.GetDateTime(DateTime.Now.ToString());

				if(objErrorNode.SelectNodes("//MsgStatusCd").Count > 0)
					objErrorNode.SelectNodes("//MsgStatusCd").Item(0).InnerText = sStatusCode;

				if(objErrorNode.SelectNodes("//MsgErrorCd").Count > 0)
					objErrorNode.SelectNodes("//MsgErrorCd").Item(0).InnerText = sErrorCode;

				if(objErrorNode.SelectNodes("//MsgStatusDesc").Count > 0)
					objErrorNode.SelectNodes("//MsgStatusDesc").Item(0).InnerText = sStatusDesc;

				p_objResponse.SelectSingleNode("/ACORD/" + sErrorNodeParent).AppendChild(objErrorNode);

				objErrorNode = null;
			}
			}
		}

		
		private int GetRowIdFromOtherField(string sSelectField, string sTableName, string sFieldName, string sFieldValue)
		{
			DbConnection oCn = null;
			DbReader objDbReader = null;
			int iRowId =0;
			try
			{
				oCn = Db.DbFactory.GetDbConnection(this.RiskmasterConnectionString);
				oCn.Open(); 
				
				//objDbReader = oCn.ExecuteReader("SELECT " + sSelectField + " FROM " + sTableName + " WHERE " + sFieldName + " = '" + sFieldValue + "'");
				objDbReader = oCn.ExecuteReader("SELECT " + sSelectField + " FROM " + sTableName + " WHERE " + sFieldName + " = " + sFieldValue);
				if(!objDbReader.Read())
				{	
					objDbReader.Close();
					//objDbReader = null;
					return 0;					
				}
				iRowId = objDbReader.GetInt(0);
				return iRowId;
			}
			catch(Exception p_oException)
			{
				throw new Exception(p_oException.Message, p_oException);
			}
			finally
			{
				if(oCn != null)
				{
					oCn.Close();
					oCn.Dispose();
				}
				if(objDbReader != null)
				{
					if(!objDbReader.IsClosed)
						objDbReader.Close();
					objDbReader = null;
				}
			}
		}
	

		/// Name		: ReadTagValue
		/// Author		: Manoj Agrawal
		/// Date Created: 08/29/2006
		/// ************************************************************
		/// <summary>		
		/// ReadTagValue() function works for following type of structure. It will read sValue from &lt;OtherId&gt; tag if we pass "OtherIdentifier" in sParent and "csc:SupervisorUserId" in sType and "OtherId" in sTag.
		///	 &lt;OtherIdentifier&gt;
		///		&lt;OtherIdTypeCd&gt;csc:SupervisorUserId&lt;/OtherIdTypeCd&gt;
		///		&lt;OtherId&gt;SupervisorName&lt;/OtherId&gt; 
		///	 &lt;/OtherIdentifier&gt;
		/// </summary>
		private string ReadTagValue(XmlNode objNode, string sParent, string sType, string sTag)
		{
			string sValue = "";

			try
			{
				for (int i = 0; i < objNode.SelectNodes("//" + sParent).Count; i++)
				{
					if(objNode.SelectNodes("//" + sParent).Item(i).HasChildNodes == true)
					{
						for (int j = 0; j < objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Count; j++)
						{
							if(objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Item(j).InnerText.Equals(sType))
							{
								goto readvalue;
							}
						}

						continue;

					readvalue:
						for (int j = 0; j < objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Count; j++)
						{
							if(objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Item(j).Name.Equals(sTag))
							{
								sValue = objNode.SelectNodes("//" + sParent).Item(i).ChildNodes.Item(j).InnerText;
								return CommonFunctions.AddAmparsand(sValue);
							}
						}
					}	//if
				}		//for

				return CommonFunctions.AddAmparsand(sValue);
			}			//try
			catch(Exception objException)
			{
				return CommonFunctions.AddAmparsand(sValue);
			}
		}


		private double GetReserveBalance( int p_iClaimId , int p_iClaimantEid , int p_iUnitId , int p_iReserveTypeCode , int p_iLob , int p_iTransId )
		{
			DbReader objReader = null ;
			double dblReserveBalance = 0.0 ;
			string sSQL = "" ;
			string sSQLPayment = "" ;
			string sSQLCollect = "" ;
			double dblReserveAmount = 0.0 ;
			double dblTemp = 0.0 ;
			double dblPayment = 0.0 ;
			double dblCollection = 0.0 ;

			try
			{
				if( p_iClaimId != 0 && p_iReserveTypeCode != 0 )
				{
					sSQL = " SELECT SUM(RESERVE_AMOUNT), SUM(COLLECTION_TOTAL), SUM(PAID_TOTAL) FROM RESERVE_CURRENT"
						+	" WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString() ;

					if( p_iClaimantEid != 0 )
						sSQL += " AND CLAIMANT_EID = " + p_iClaimantEid.ToString() ;
					else
					{
						if( p_iUnitId != 0 )
							sSQL += " AND UNIT_ID = " + p_iUnitId.ToString() ;
					}

					objReader = DbFactory.GetDbReader( this.RiskmasterConnectionString , sSQL );			

					if( objReader != null )
					{
						if( objReader.Read() )
						{
							dblReserveAmount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( 0 ) ));
							dblTemp = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( 2 ) ));
						}
					}
					if( objReader != null )
						objReader.Close();

					sSQL = "SELECT SUM(FTS.AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS,CODES WHERE "
						+	" FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
						+	" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString()
						+	" AND FUNDS.UNIT_ID = " + p_iUnitId.ToString() 
						+	" AND FUNDS.TRANS_ID = FTS.TRANS_ID"
						+	" AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
						+	" AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
						+	" AND FUNDS.VOID_FLAG = 0" ;

					if( p_iTransId != 0 )
						sSQL += " AND FUNDS.TRANS_ID <> " + p_iTransId.ToString() ;

					sSQLPayment = sSQL + " AND FUNDS.PAYMENT_FLAG <> 0" ;
					sSQLCollect = sSQL + " AND FUNDS.PAYMENT_FLAG = 0" ;
				
					objReader = DbFactory.GetDbReader( this.RiskmasterConnectionString , sSQLPayment );			

					if( objReader != null )
					{
						if( objReader.Read() )
						{
							dblPayment = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( 0 ) ));						
						}
					}
					if( objReader != null )
						objReader.Close();

					objReader = DbFactory.GetDbReader( this.RiskmasterConnectionString , sSQLCollect );			

					if( objReader != null )
					{
						if( objReader.Read() )
						{
							dblCollection = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( 0 ) ));						
						}
					}
					if( objReader != null )
						objReader.Close();
					
					dblReserveBalance = CalReserveBalance( dblReserveAmount , dblPayment , dblCollection , p_iLob , 0 ) ;
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("LSSImportProcess.GetReserveBalance.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}				
			}
			return( dblReserveBalance );
		}


		private double CalReserveBalance( double p_dblReserveAmount , double p_dblPaid , double p_dblCollection , int p_iLob , int p_iStateCode )
		{
			LocalCache objLocalCache = null ;
			ColLobSettings objColLobSettings = null ;

			string sShortCode = "" ;
			double dblPaidCollectionDiff = 0.0 ;
			double dblReserveBalance = 0.0 ;

			try
			{
				objLocalCache = new LocalCache( this.RiskmasterConnectionString,m_iClientId);
                objColLobSettings = new ColLobSettings(this.RiskmasterConnectionString, m_iClientId);
			
				sShortCode = objLocalCache.GetShortCode( objLocalCache.GetRelatedCodeId( p_iStateCode ) );
                if (p_iLob != 0)
                {
                    if (objColLobSettings[p_iLob].CollInRsvBal)
                    {
                        dblPaidCollectionDiff = p_dblPaid - p_dblCollection;
                        if (dblPaidCollectionDiff < 0.0)
                        {
                            dblPaidCollectionDiff = 0.0;
                        }
                        dblReserveBalance = p_dblReserveAmount - dblPaidCollectionDiff;
                    }
                    else
                    {
                        dblReserveBalance = p_dblReserveAmount - p_dblPaid;
                    }
                }
                if (sShortCode == "C" && dblReserveBalance < 0.0)
                {
                    dblReserveBalance = 0.0;
                }
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("LSSImportProcess.CalReserveBalance.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
				//				objColLobSettings = null ;
			}
			return( dblReserveBalance );
		}

		private void SetField(string sTable, string sSetField, string sSetValue, string sWhereField, string sWhereValue)
		{
			DbConnection objConn=null;

			try
			{
				objConn=DbFactory.GetDbConnection(this.RiskmasterConnectionString);
				objConn.Open();
				objConn.ExecuteNonQuery("UPDATE " + sTable + " SET " + sSetField + " = '" + sSetValue.Replace("'", "''") + "' WHERE " + sWhereField + " = " + sWhereValue.Replace("'", "''"));
				objConn.Close();
				objConn=null;
			}
			catch(Exception objEx)
			{			
			}
			finally
			{
				if(objConn != null)
				{
					objConn.Close();
					objConn=null;
				}
			}
		}
        /// Name		: GenerateHTMLForLSS
        /// Author		: Raman Bhatia
        /// Date Created: 07/03/2008
        /// ************************************************************
        /// <summary>		
        /// GenerateHTMLForLSS() function generates dummy HTML for opening LSS documents in MCM
        /// </summary>
        private bool GenerateHTMLForLSS(string filepath , string url , string filename)
        {
            FileStream outputfile = null;
            string sCompleteFilePath = "";
            try
            {
                sCompleteFilePath = filepath + "\\" + filename + ".html";
                if (File.Exists(sCompleteFilePath))
                {
                    File.Delete(sCompleteFilePath);
                }
                outputfile = new FileStream(sCompleteFilePath, FileMode.OpenOrCreate, FileAccess.Write);

                writer = new StreamWriter(outputfile);

                 DoWrite("<HTML>");
                 DoWrite("<script>");
                 DoWrite("function autoChange()");
                 DoWrite("{");
                 DoWrite("var timeID = setTimeout(location.href='" + url + "', 5000)");
                 DoWrite("}");
                 DoWrite("</script>");
                 DoWrite("<BODY onLoad='autoChange()'>");
                 DoWrite("Loading LSS document..");
                 DoWrite("Your browser will automatically jump there.<BR>");
                 DoWrite("If it does not then please click <A HREF='" + url + "'>Open</A> to view this document</H4>");
                 DoWrite("</BODY>");
                 DoWrite("</HTML>");

                writer.Close();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void DoWrite(String line)
        {
            writer.WriteLine(line);
            writer.Flush();
        }
        //Sumit (04 /14/2009)-MITS# 15473
        private void AddTagValue(XmlNode objNode, string sParent, string sType, string sTag, string sValue, XmlNamespaceManager nsmgr)
        {
            try
            {
                for (int i = 0; i < objNode.SelectNodes("//" + sParent, nsmgr).Count; i++)
                {
                    if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).HasChildNodes == true)
                    {
                        for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
                        {
                            if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText.Equals(sType))
                            {
                                goto setvalue;
                            }
                        }

                        continue;

                    setvalue:
                        for (int j = 0; j < objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Count; j++)
                        {
                            if (objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).Name.Equals(sTag))
                            {
                                objNode.SelectNodes("//" + sParent, nsmgr).Item(i).ChildNodes.Item(j).InnerText = sValue;
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw p_objException;
            }
        }

        /// Name		: InitCheckOptions
		/// Author		: Rahul Aggarwal
		/// Date Created: 05/21/2010	
        /// MITS 20073/20036/20938
		/// <summary>
		/// Reads Check related options
		/// </summary>
		private void InitCheckOptions()
		{
			DbReader objRead=null;
			StringBuilder sbSQL=null;
			try
			{
				sbSQL=new StringBuilder();
                sbSQL.Append("SELECT UPDATE_LSS_ENTITY,LSS_PAYMENT_HOLD,LSS_COL_ON_HOLD, DEF_DSTRBN_TYPE_CODE FROM CHECK_OPTIONS");//smahajan6: Safeway: Payment Supervisory Approval
				objRead=DbFactory.GetDbReader(this.RiskmasterConnectionString,sbSQL.ToString());
				if (objRead.Read())
				{
					bSetLSSPaymentOnHold=Conversion.ConvertObjToBool(
						objRead.GetValue("LSS_PAYMENT_HOLD"), 0);
					bAllowAddUpdateLSSEntity=Conversion.ConvertObjToBool(
						objRead.GetValue("UPDATE_LSS_ENTITY"), 0);
                    bPutLssCollectionOnHold = Conversion.ConvertObjToBool(
                        objRead.GetValue("LSS_COL_ON_HOLD"), 0);
                    m_iDefaultDistributionType = objRead.GetInt32("DEF_DSTRBN_TYPE_CODE");
				}
				else
				{
					bAllowAddUpdateLSSEntity=false;
					bSetLSSPaymentOnHold=false;
                    bPutLssCollectionOnHold = false;
                   
				}
				CloseReader(ref objRead);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("SupervisorApproval.InitCheckOptions.Error", m_iClientId), p_objException);
			}
			finally
			{
				CloseReader(ref objRead);
				sbSQL=null;
			}
		}

        /// <summary>
        /// Rahul Aggarwal
		/// Closes a passed DbReader
        /// Date Created: 05/21/2010	
        /// MITS 20073/20036/20938
		/// </summary>
		/// <param name="p_objReader">DbReader</param>
		private void CloseReader(ref DbReader p_objReader)
		{
			if (p_objReader!=null)
			{
				if (!p_objReader.IsClosed)
				{
					p_objReader.Close();
				}
				p_objReader=null;
			}
		}

        /// <summary>
        /// Author - rsushilaggar
        /// calculate the total collection and total paid for the claim
        /// 06/02/2010 MITS 20938 
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <param name="p_iClaimantEid"></param>
        /// <param name="p_iUnitId"></param>
        /// <param name="dLSSAmount"></param>
        /// <returns></returns>
        private bool GetReserevTotalPaid(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, double dLSSAmount)
        { 
            DbReader objReader = null ;
			string sSQL = "" ;
			double dblReserveAmount = 0.0 ;
			double dblPaidTotal = 0.0 ;
			double dblCollectionTotal = 0.0 ;
            bool bIsAmountExceed = false;
            dLSSAmount = dLSSAmount * (-1);
            try
            {
                if (p_iClaimId != 0 )
                {
                    sSQL = " SELECT SUM(RESERVE_AMOUNT), SUM(COLLECTION_TOTAL), SUM(PAID_TOTAL) FROM RESERVE_CURRENT"
                        + " WHERE CLAIM_ID = " + p_iClaimId.ToString() ;

                    if (p_iClaimantEid != 0)
                        sSQL += " AND CLAIMANT_EID = " + p_iClaimantEid.ToString();
                    else
                    {
                        if (p_iUnitId != 0)
                            sSQL += " AND UNIT_ID = " + p_iUnitId.ToString();
                    }

                    objReader = DbFactory.GetDbReader(this.RiskmasterConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblReserveAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                            dblPaidTotal = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(2)));
                            dblCollectionTotal = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(1)));
                           
                            if (dLSSAmount > (dblPaidTotal - dblCollectionTotal))
                                bIsAmountExceed = true;
                        }
                    }
                    if (objReader != null)
                        objReader.Close();
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LSSImportProcess.GetReserveBalance.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return bIsAmountExceed;

        }
		#endregion
	}
}
