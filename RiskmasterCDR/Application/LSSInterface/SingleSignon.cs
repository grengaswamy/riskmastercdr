﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Collections.Specialized;
using System.Web.Security;
using Riskmaster.Security.Authentication;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Application.LSSInterface
{
    /// <summary>
    /// This class will read the LSS config.
    /// </summary>
    public class SingleSignon
    {
       #region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
        ///Private variable to store LSS Webservice Username
        ///</summary>
        private string m_sLSS_ws_username = "";
        /// <summary>
        /// Private variable to store LSS Webservice Password
        ///</summary>
        private string m_sLSS_ws_Password = "";
        /// <summary>
        /// Private Variable to store LSS Login URL
        /// </summary>
        private string m_sLSS_URL = "";
        /// <summary>
        /// Private variable to initialize Authentication mode MITS 26834
        /// </summary>
        string m_sAuthenticationMode = string.Empty;

        private int m_iClientId=0;

		#endregion

		#region Constructor
		/// <summary>
        ///Author  :   Shameem Arshad
        ///Dated   :   11/24/2008
		///	Constructor, initializes the variables to the default value
		/// </summary>
        /// <param name="p_sUserName">loginName</param>
        /// <param name="p_sPassword">Pass</param>
        public SingleSignon(string p_sUserName, string p_sPassword, string p_sSecurityConnString,int p_iClientId)
		{
            NameValueCollection nvColl = null;
            XmlDocument xLSSConfigFileDoc = null;
            string sLSSConfigFileName = string.Empty;
            string sLSSLoginURL = string.Empty;
            string strSecurityConnString = string.Empty;
            string sTypeName = string.Empty;
            string sAuthenticationMode = string.Empty; // MITS 26834
            m_iClientId = p_iClientId;
            // Debabrata Biswas
            // MITS 26834
            // Get the Membership provider used in RMX
            const string strRMADProvider = "RiskmasterADMembershipProvider";
            const string strRMLDAPProvider = "RiskmasterLDAPMembershipProvider";
            const string strRMDBProvider = "RiskmasterMembershipProvider";
            try
            {
                //START Debabrat Biswas: MITS 26834
                //The implemetation for launching LSS and leting it know which authentication mode RMX has enabled.

                CustomMembershipProvider membProvider = MembershipProviderFactory.CreateMembershipProvider(p_sSecurityConnString, m_iClientId);
                sTypeName = membProvider.GetType().Name;

                switch (sTypeName)
                {
                    case strRMADProvider:
                        sAuthenticationMode = "ACTIVEDIRECTORYON";
                        break;
                    case strRMLDAPProvider:
                        sAuthenticationMode = "LDAPON";
                        break;
                    case strRMDBProvider:
                        sAuthenticationMode = "ACTIVEDIRECTORYOFF";
                        break;
                    default:
                        sAuthenticationMode = "ACTIVEDIRECTORYOFF";
                        break;
                }
               
                //END Debabrata Biswas
                nvColl = RMConfigurationManager.GetNameValueSectionSettings("LSSInterface", string.Empty , m_iClientId);
                if (nvColl != null)
                {
                    sLSSConfigFileName = nvColl["LSSExportServiceConfig"];
                    sLSSLoginURL = nvColl["LSSLoginURL"];
                }

                if (sLSSConfigFileName != "")
                {
                    sLSSConfigFileName = AppDomain.CurrentDomain.BaseDirectory + "\\bin\\" + sLSSConfigFileName;

                    xLSSConfigFileDoc = new XmlDocument();
                    xLSSConfigFileDoc.Load(sLSSConfigFileName);

                    this.m_sUserName = RMCryptography.EncryptString(p_sUserName); //Debabrata Biswas MITS 26834, sending the logged in userID encrypted
                    this.m_sPassword = RMCryptography.EncryptString(p_sPassword); //Debabrata Biswas MITS 26834, sending the logged in userpass encrypted
                    this.m_sLSS_URL = sLSSLoginURL;
                    this.m_sAuthenticationMode = sAuthenticationMode;//Debabrata Biswas MITS 26834, Sending RMX authentication Mode

                    //Fetch LSS webservice username and password  
                    this.m_sLSS_ws_username = xLSSConfigFileDoc.SelectSingleNode("//LSSInterface/add[@key='LSSUser']").Attributes.GetNamedItem("value").InnerText;
                    this.m_sLSS_ws_Password = xLSSConfigFileDoc.SelectSingleNode("//LSSInterface/add[@key='LSSPwd']").Attributes.GetNamedItem("value").InnerText;
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("SingleSignon.SingleSignonXML.Error", m_iClientId), ex);
            }
		}
		#endregion
        

		#region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXML"></param>
        /// <returns></returns>
        public XmlDocument SingleSignonXML(XmlDocument p_objXML)
        {
            XmlElement objNode = null;
            
            try
            {
                objNode = (XmlElement)p_objXML.SelectSingleNode("//LSSUrl");
                objNode.InnerText = m_sLSS_URL;

                // RMX User Name
                objNode = (XmlElement)p_objXML.SelectSingleNode("//loginName");
                objNode.InnerText = m_sUserName;
                
                // RMX Password
                objNode = (XmlElement)p_objXML.SelectSingleNode("//Pass");
                objNode.InnerText = m_sPassword;
                               
                // LSS Webservice Config User Name
                objNode = (XmlElement)p_objXML.SelectSingleNode("//wsUser");
                objNode.InnerText = m_sLSS_ws_username;
                
                // LSS Webservice Config Password
                objNode = (XmlElement)p_objXML.SelectSingleNode("//wsPass");
                objNode.InnerText = m_sLSS_ws_Password;

                // RMX Authentication Mode Webservice Config Password MITS 26834
                objNode = (XmlElement)p_objXML.SelectSingleNode("//authMode");
                objNode.InnerText = m_sAuthenticationMode;
             }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SingleSignon.SingleSignonXML.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }

            return p_objXML;
        }

        #endregion
    }
}
