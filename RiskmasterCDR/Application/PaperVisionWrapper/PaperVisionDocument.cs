﻿
using System;
using System.Collections.Generic;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Xml;
using System.IO;

namespace Riskmaster.Application.PaperVisionWrapper
{
    ///************************************************************** 
    ///* $File		: PaperVisionDocument.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 25-May-2009 
    ///* $Author	: Animesh Sahai
    ///* $Comment	: 
    ///* $Source	: 
    /// Encapsulates Document information and provides methods to retrieve
    ///	and update these details.
    /// </summary>
    public class PaperVisionDocument
    {
        #region Constants
        /// <summary>
        /// FolderId of Recycle Bin for all users
        /// </summary>
        private const long RECYCLE_BIN_ID = -1;

        /// <summary>
        /// Xml tag name for data
        /// </summary>
        private const string DATA_XMLTAGNAME = "data";

        /// <summary>
        /// Xml tag name for Document
        /// </summary>
        private const string DOCUMENT_XMLTAGNAME = "Document";

        /// <summary>
        /// Xml tag name for DocumentId
        /// </summary>
        private const string DOCUMENTID_XMLTAGNAME = "DocumentId";

        /// <summary>
        /// Xml tag name for PaperVisionDocId
        /// </summary>
        private const string PVDOCUMENTID_XMLTAGNAME = "PaperVisionDocId";

        /// <summary>
        /// Xml tag name for CreateDate
        /// </summary>
        private const string CREATEDATE_XMLTAGNAME = "CreateDate";

        /// <summary>
        /// Xml tag name for Category
        /// </summary>
        private const string CATEGORY_XMLTAGNAME = "Category";

        /// <summary>
        /// Xml tag name for Category Code
        /// </summary>
        private const string CATEGORY_CODE_XMLTAGNAME = "Category-cid";

        /// <summary>
        /// Xml tag name for Name
        /// </summary>
        private const string NAME_XMLTAGNAME = "Name";

        /// <summary>
        /// Xml tag name for Class
        /// </summary>
        private const string CLASS_XMLTAGNAME = "Class";

        /// <summary>
        /// Xml tag name for Class Code
        /// </summary>
        private const string CLASS_CODE_XMLTAGNAME = "Class-cid";

        /// <summary>
        /// Xml tag name for Subject
        /// </summary>
        private const string SUBJECT_XMLTAGNAME = "Subject";

        /// <summary>
        /// Xml tag name for Type
        /// </summary>
        private const string TYPE_XMLTAGNAME = "Type";

        /// <summary>
        /// Xml tag name for Type Code
        /// </summary>
        private const string TYPE_CODE_XMLTAGNAME = "Type-cid";

        /// <summary>
        /// Xml tag name for Notes
        /// </summary>
        private const string NOTES_XMLTAGNAME = "Notes";

        /// <summary>
        /// Xml tag name for UserLoginName
        /// </summary>
        private const string USERLOGINNAME_XMLTAGNAME = "UserLoginName";

        /// <summary>
        /// Xml tag name for FileName
        /// </summary>
        private const string FILENAME_XMLTAGNAME = "FileName";

        /// <summary>
        /// Xml tag name for PaperVision Flag
        /// </summary>
        private const string ISPVFLAG_XMLTAGNAME = "PaperVisionFlag";

        /// <summary>
        /// Xml tag name for Keywords
        /// </summary>
        private const string KEYWORDS_XMLTAGNAME = "Keywords";

        /// <summary>
        /// Xml tag name for AttachTable
        /// </summary>
        private const string ATTACHTABLE_XMLTAGNAME = "AttachTable";

        /// <summary>
        /// Xml tag name for AttachRecordId
        /// </summary>
        private const string ATTACHRECORDID_XMLTAGNAME = "AttachRecordId";
        /// <summary>
        /// Xml tag name for AttachRecordId
        /// </summary>
        private const string PAPERVISIONURL_XMLTAGNAME = "PapervisionUrl";

        #endregion

        #region Member Variables

        /// <summary>		
        /// DocumentId
        /// </summary>
        private long m_lDocumentId;

        /// <summary>		
        /// File Name
        /// </summary>
        private string m_sFileName;

        /// <summary>		
        /// File Path
        /// </summary>
        private int m_IAtPaperVision;

        /// <summary>		
        /// Document Name
        /// </summary>
        private string m_sTitle;

        /// <summary>		
        /// Keywords
        /// </summary>
        private string m_sKeywords;

        /// <summary>		
        /// Notes
        /// </summary>
        private string m_sNotes;

        /// <summary>		
        /// Create Date
        /// </summary>
        private string m_sCreateDate;

        /// <summary>		
        /// The table to which document is attached.
        /// </summary>
        private string m_sAttachTable;

        /// <summary>		
        /// The record id to which document is attached.
        /// </summary>
        private long m_lAttachRecordId;

        /// <summary>		
        /// User Login Name
        /// </summary>
        private string m_sUserLoginName;

        /// <summary>		
        /// Subject
        /// </summary>
        private string m_sSubject;

        /// <summary>		
        /// Type
        /// </summary>
        private long m_lType;

        /// <summary>		
        /// Class
        /// </summary>
        private long m_lClass;

        /// <summary>		
        /// Category
        /// </summary>
        private long m_lCategory;

        /// <summary>		
        /// Database Connection String
        /// </summary>
        private string m_sConnectionString;

        /// <summary>		
        /// Database Connection String
        /// </summary>
        private long m_lPVDocumentID;

        /// <summary>		
        /// Indicates whether properties for Document object are loaded.
        /// </summary>
        private bool m_bDocumentPropertiesLoaded;

        private int m_iClientId = 0;

        #endregion

        #region Properties

        /* All string properties are set to string.Empty if a
		 * null value is assigned to them. This ensures a more robust
		 * component free of NullValueExceptions generated inadvertantly
		 * if the client sets a string property to null. 
		 * */

        /// <summary>
        /// DocumentId
        /// </summary>
        public long DocumentId
        {
            get
            {
                return m_lDocumentId;
            }
            set
            {
                m_lDocumentId = value;
            }
        }

        /// <summary>
        /// File Name
        /// </summary>
        public string FileName
        {
            get
            {
                return m_sFileName;
            }
            set
            {
                if (value != null)
                {
                    m_sFileName = value;
                }
                else
                {
                    m_sFileName = string.Empty;
                }
            }
        }

        /// <summary>
        /// File Path
        /// </summary>
        public int IsAtPaperVision
        {
            get
            {
                return m_IAtPaperVision;
            }
            set
            {
                if (value != null)
                {
                    m_IAtPaperVision = value;
                }
            }
        }

        /// <summary>
        /// Document Name
        /// </summary>
        public string Title
        {
            get
            {
                return m_sTitle;
            }
            set
            {
                if (value != null)
                {
                    m_sTitle = value;
                    if (m_sTitle.Length > 32)
                        m_sTitle = m_sTitle.Substring(0, 32);
                }
                else
                {
                    m_sTitle = string.Empty;
                }
            }
        }

        /// <summary>
        /// Keywords
        /// </summary>
        public string Keywords
        {
            get
            {
                return m_sKeywords;
            }
            set
            {
                if (value != null)
                {
                    m_sKeywords = value;
                    if (m_sKeywords.Length > 200)
                        m_sKeywords = m_sKeywords.Substring(0, 200);
                }
                else
                {
                    m_sKeywords = string.Empty;
                }
            }
        }

        /// <summary>
        /// Notes
        /// </summary>
        public string Notes
        {
            get
            {
                return m_sNotes;
            }
            set
            {
                if (value != null)
                {
                    m_sNotes = value;
                    if (m_sNotes.Length > 200)
                        m_sNotes = m_sNotes.Substring(0, 200);
                }
                else
                {
                    m_sNotes = string.Empty;
                }
            }
        }

        /// <summary>
        /// Create Date
        /// </summary>
        public string CreateDate
        {
            get
            {
                return m_sCreateDate;
            }
            set
            {
                if (value != null)
                {
                    m_sCreateDate = value;
                }
                else
                {
                    m_sCreateDate = string.Empty;
                }
            }
        }

        /// <summary>
        /// The table to which document is attached
        /// </summary>
        public string AttachTable
        {
            get
            {
                return m_sAttachTable;
            }
            set
            {
                if (value != null)
                {
                    m_sAttachTable = value;
                }
                else
                {
                    m_sAttachTable = string.Empty;
                }
            }
        }

        /// <summary>
        /// Record Id to which document is attached
        /// </summary>
        public long AttachRecordId
        {
            get
            {
                return m_lAttachRecordId;
            }
            set
            {
                m_lAttachRecordId = value;
            }
        }

        /// <summary>
        /// PaperVision Record Id of the document.
        /// </summary>
        public long PVDocumentId
        {
            get
            {
                return m_lPVDocumentID;
            }
            set
            {
                m_lPVDocumentID = value;
            }
        }


        /// <summary>
        /// User Login Name
        /// </summary>
        public string UserLoginName
        {
            get
            {
                return m_sUserLoginName;
            }
            set
            {
                if (value != null)
                {
                    m_sUserLoginName = value;
                }
                else
                {
                    m_sUserLoginName = string.Empty;
                }
            }
        }

        /// <summary>
        /// Subject
        /// </summary>
        public string Subject
        {
            get
            {
                return m_sSubject;
            }
            set
            {
                if (value != null)
                {
                    m_sSubject = value;
                    if (m_sSubject.Length > 50)
                        m_sSubject = m_sSubject.Substring(0, 50);
                }
                else
                {
                    m_sSubject = string.Empty;
                }
            }
        }

        /// <summary>
        /// Type
        /// </summary>
        public long Type
        {
            get
            {
                return m_lType;
            }
            set
            {
                m_lType = value;
            }
        }

        /// <summary>
        /// Class
        /// </summary>
        public long Class
        {
            get
            {
                return m_lClass;
            }
            set
            {
                m_lClass = value;
            }
        }

        /// <summary>
        /// Category
        /// </summary>
        public long Category
        {
            get
            {
                return m_lCategory;
            }
            set
            {
                m_lCategory = value;
            }
        }
        /// <summary>
        /// Database Connection String
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return m_sConnectionString;
            }
            set
            {
                if (value != null)
                {
                    m_sConnectionString = value;
                }
                else
                {
                    m_sConnectionString = string.Empty;
                }
            }
        }

        #endregion

        #region Constructor

        /// Name			: Document
        /// Author			: Animesh Sahai
        /// Date Created	: 25-May-2009
        /// <summary>
        /// Constructor
        /// </summary>
        public PaperVisionDocument(int p_iClientId)
        {
            this.m_sFileName = string.Empty;
            this.m_sTitle = string.Empty;
            this.m_sKeywords = string.Empty;
            this.m_sNotes = string.Empty;
            this.m_lDocumentId = 0;
            this.m_sCreateDate = string.Empty;
            this.m_sAttachTable = string.Empty;
            this.m_lAttachRecordId = 0;
            this.m_sUserLoginName = string.Empty;
            this.m_sSubject = string.Empty;
            this.m_lType = 0;
            this.m_lClass = 0;
            this.m_lCategory = 0;
            this.m_sConnectionString = string.Empty;
            this.m_IAtPaperVision = 0;
            this.m_lPVDocumentID = 0;
            this.m_bDocumentPropertiesLoaded = false;
            m_iClientId = p_iClientId;
        }

        #endregion

        #region Public Functions

        /// Name			: Load
        /// Author			: Animesh Sahai
        /// Date Created	: 25-May-2009
        /// <summary>
        /// Loads the properties of the Document from the database.
        /// Uses DocumentId, UserLoginName and ConnectionString 
        /// property values.
        /// </summary>
        /// <returns>0 - Success</returns>
        public int Load()
        {
            int iReturnValue = 0;
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            try
            {
                RaiseInitializationException();
                this.m_bDocumentPropertiesLoaded = false;
                sSQL = "SELECT DOCUMENT_NAME,DOCUMENT_FILENAME, "
                    + " KEYWORDS,CREATE_DATE,NOTES,USER_ID, DOCUMENT_CLASS, "
                    + " DOCUMENT_CATEGORY, DOCUMENT_SUBJECT, DOCUMENT_TYPE,ISAT_PV,PV_DOCID "
                    + "	FROM DOCUMENT "
                    + " WHERE DOCUMENT_ID=" + this.DocumentId.ToString();
                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                if (objDbReader.Read())
                {
                    this.Title = objDbReader.GetString("DOCUMENT_NAME");
                    this.Keywords = objDbReader.GetString("KEYWORDS");
                    this.CreateDate = objDbReader.GetString("CREATE_DATE");
                    this.FileName = objDbReader.GetString("DOCUMENT_FILENAME");
                    this.Notes = objDbReader.GetString("NOTES");
                    this.UserLoginName = objDbReader.GetString("USER_ID");
                    this.Class = Convert.ToInt64(objDbReader.GetValue("DOCUMENT_CLASS"));
                    this.Type = Convert.ToInt64(objDbReader.GetValue("DOCUMENT_TYPE"));
                    this.Subject = objDbReader.GetString("DOCUMENT_SUBJECT");
                    this.Category = Convert.ToInt64(objDbReader.GetValue("DOCUMENT_CATEGORY"));
                    this.IsAtPaperVision = Conversion.ConvertObjToInt(objDbReader.GetValue("ISAT_PV"), m_iClientId);
                    this.PVDocumentId = Conversion.ConvertObjToInt64(objDbReader.GetValue("PV_DOCID"), m_iClientId);
                    this.m_bDocumentPropertiesLoaded = true;
                }

                objDbReader.Close();
                objDbReader = null;
                if (IsAtPaperVision == -1)
                {
                    sSQL = " SELECT RECORD_ID, TABLE_NAME "
                        + " FROM DOCUMENT_ATTACH "
                        + " WHERE DOCUMENT_ID=" + this.DocumentId.ToString();
                    objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                    if (objDbReader.Read())
                    {
                        this.AttachTable = objDbReader.GetString("TABLE_NAME");
                        this.AttachRecordId = Convert.ToInt64(objDbReader.GetValue("RECORD_ID"));
                    }
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.Exception",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
            }
            return iReturnValue;
        }


        /// Name			: Update
        /// Author			: Animesh Sahai
        /// Date Created	: 27-May-2009
        /// <summary>
        /// Updates the properties of the Document record in the database.
        /// Creates a new document record in the database, 
        /// if it does not exist
        /// </summary>
        /// <returns>0 - Success</returns>
        public int Update()
        {
            int iReturnValue = 0;
            DbConnection objDbConnection = null;
            DbTransaction objDbTransaction = null;
            DbWriter objDbWriter = null;
            long lNewDocumentId = 0;
            try
            {
                RaiseInitializationException();
                objDbConnection = DbFactory.GetDbConnection(this.ConnectionString);
                objDbConnection.Open();
                objDbTransaction = objDbConnection.BeginTransaction();
                objDbWriter = DbFactory.GetDbWriter(objDbConnection);
                objDbWriter.Tables.Add("DOCUMENT");
                objDbWriter.Fields.Add("DOCUMENT_NAME", this.Title);
                objDbWriter.Fields.Add("KEYWORDS", this.Keywords);
                objDbWriter.Fields.Add("DOCUMENT_FILENAME", this.FileName);
                objDbWriter.Fields.Add("DOCUMENT_FILEPATH", "");
                objDbWriter.Fields.Add("NOTES", this.Notes);
                objDbWriter.Fields.Add("FOLDER_ID", 0);
                objDbWriter.Fields.Add("USER_ID", this.UserLoginName);
                objDbWriter.Fields.Add("DOCUMENT_CATEGORY", this.Category);
                objDbWriter.Fields.Add("DOCUMENT_CLASS", this.Class);
                objDbWriter.Fields.Add("DOCUMENT_SUBJECT", this.Subject);
                objDbWriter.Fields.Add("DOCUMENT_TYPE", this.Type);
                objDbWriter.Fields.Add("MCM_TRANSFER_STATUS", 0);
                objDbWriter.Fields.Add("ISAT_PV", this.IsAtPaperVision);
                objDbWriter.Fields.Add("PV_DOCID", this.PVDocumentId);
                if (this.DocumentId != 0)
                {
                    // Update existing record				
                    objDbWriter.Where.Add(" DOCUMENT_ID=" + this.DocumentId.ToString());
                }
                else
                {
                    // Insert new record
                    lNewDocumentId = Utilities.GetNextUID(this.ConnectionString,
                                                            "DOCUMENT", m_iClientId);

                    objDbWriter.Fields.Add("DOCUMENT_ID", lNewDocumentId);

                    //Animesh Insered //MITS 19654
                    if (this.CreateDate.ToString().Equals(""))
                    {
                        objDbWriter.Fields.Add("CREATE_DATE", DateTime.Now.ToString("yyyyMMddhhmmss"));
                    }
                    else
                    {
                        //Animesh Modified MITS 20256 Date 09/04/2010
                        //objDbWriter.Fields.Add("CREATE_DATE", Conversion.GetDate(this.CreateDate)); 
                        objDbWriter.Fields.Add("CREATE_DATE", Conversion.GetDateTime(this.CreateDate));
                        //Animesh Modification ends
                    }
                    //Animesh Insertion ends //MITS 19654
                    objDbWriter.Fields.Add("ARCHIVE_LEVEL", 0);
                    objDbWriter.Fields.Add("SECURITY_LEVEL", 0);
                    objDbWriter.Fields.Add("DOC_INTERNAL_TYPE", 2);
                    objDbWriter.Fields.Add("DOCUMENT_CREAT_APP", DBNull.Value);
                    objDbWriter.Fields.Add("DOCUMENT_EXPDTTM", 0);
                    objDbWriter.Fields.Add("DOCUMENT_ACTIVE", 0);
                }
                objDbWriter.Execute(objDbTransaction);
                objDbWriter.Reset(true);
                if (this.AttachTable.Length > 0)
                {
                    objDbWriter.Tables.Add("DOCUMENT_ATTACH");
                    objDbWriter.Fields.Add("TABLE_NAME", this.AttachTable);
                    objDbWriter.Fields.Add("RECORD_ID", (long)this.AttachRecordId);
                    if (lNewDocumentId != 0)
                    {
                        // Insert new record
                        objDbWriter.Fields.Add("DOCUMENT_ID", lNewDocumentId);
                    }
                    else
                    {
                        // Update existing record
                        objDbWriter.Where.Add(" DOCUMENT_ID=" + this.DocumentId.ToString());
                    }
                    objDbWriter.Execute(objDbTransaction);
                }
                if (lNewDocumentId != 0)
                {
                    this.DocumentId = lNewDocumentId;
                }
                objDbTransaction.Commit();

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }

                throw p_objRMAppException;
            }
            catch (Exception p_objException)
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Rollback();
                }
                throw new RMAppException(Globalization.GetString("Document.Update.Exception",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbTransaction != null)
                {
                    objDbTransaction.Dispose();
                    objDbTransaction = null;
                }

                objDbWriter = null;

                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }
            }
            return iReturnValue;
        }

        /// Name			: RaiseInitializationException
        /// Author			: Animesh Sahai
        /// Date Created	: 28-May-2009
        /// <summary>
        /// Raises InitializationException if required properties
        /// for Document object are not set.
        /// </summary>
        private void RaiseInitializationException()
        {
            if (this.ConnectionString.Length == 0)
            {
                throw new InitializationException(Globalization.GetString("Document.ConnectionStringNotSet",m_iClientId));
            }

        }

        /// Name			: CreateNewDocument
        /// Author			: Animesh Sahai
        /// Date Created	: 28-May-2009
        /// <summary>
        /// Create new Document record in the database.
        /// </summary>
        /// <param name="p_sDocumentXml">
        ///		Document details in xml format.
        ///	</param>
        /// <returns>0 - Success</returns>			
        public int CreateNewDocument(string p_sDocumentXml, MemoryStream objDocumentFile, string strUser, string strPassword, PVWrapper objPVWrapper)
        {
            int iReturnValue = 0;
            long lPVDocID;
            bool blnSuccess;
            LocalCache objCache;
            DbReader objDbReader = null;
            string sSQL = string.Empty;
            string strClaimNumber = string.Empty;
            string strEntityID = string.Empty;
            string strEventDate = string.Empty;
            //Animesh Inserted MITS 18055
            string strEntityName = string.Empty;
            //Animesh Insertion ends
            //Add by kuladeep for MITS:25212 Start 06/27/2011
            int iPos = 0;
            int iDocCount = 0;
            string sExtension = string.Empty;
            string sFNOnly = string.Empty;
            bool bIsSuccess = false;
            //Add by kuladeep for MITS:25212 End 06/27/2011
            try
            {
                RaiseInitializationException();
                objCache = new LocalCache(this.ConnectionString,m_iClientId);
                this.Load(p_sDocumentXml);

                //Animesh Modufied the SQL Query MITS 18055
                //sSQL = "SELECT CLAIM.CLAIM_NUMBER,(SELECT SHORT_CODE FROM CODES "
                //    + " WHERE CODE_ID = CLAIM.SERVICE_CODE) AS SERV_SH_CODE, "
                //    + " EVENT.DATE_OF_EVENT FROM CLAIM,EVENT WHERE CLAIM.EVENT_ID=EVENT.EVENT_ID AND "
                //    + " CLAIM.CLAIM_ID = " + this.AttachRecordId.ToString();
                sSQL = "SELECT CLAIM.CLAIM_NUMBER,CLAIM.SERVICE_CODE,(SELECT SHORT_CODE FROM CODES "
                    + " WHERE CODE_ID = CLAIM.SERVICE_CODE) AS SERV_SH_CODE, "
                    + " EVENT.DATE_OF_EVENT FROM CLAIM,EVENT WHERE CLAIM.EVENT_ID=EVENT.EVENT_ID AND "
                    + " CLAIM.CLAIM_ID = " + this.AttachRecordId.ToString();
                objDbReader = DbFactory.GetDbReader(this.ConnectionString, sSQL);
                if (objDbReader.Read())
                {
                    strClaimNumber = Conversion.ConvertObjToStr(objDbReader.GetValue("CLAIM_NUMBER"));
                    strEntityID = Conversion.ConvertObjToStr(objDbReader.GetValue("SERV_SH_CODE"));
                    //Animesh Inserted MITS 18055
                    strEntityName = objCache.GetCodeDesc((Conversion.ConvertObjToInt(objDbReader.GetValue("SERVICE_CODE"), m_iClientId)));
                    //Animesh Insertion Ends
                    strEventDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDbReader.GetValue("DATE_OF_EVENT")), "MM/dd/yyyy");
                }
                objDbReader.Close();
                objDbReader = null;

                //Add by kuladeep for MITS:25212 Start 06/27/2011
                if (string.IsNullOrEmpty(this.FileName) == false)
                {
                    iPos = this.FileName.IndexOf(".");
                    if (iPos > -1)
                    {
                        sExtension = this.FileName.Substring(iPos + 1);
                        sFNOnly = this.FileName.Substring(0, iPos);
                    }
                    //Just Need to check duplicate file from DB.
                    sSQL = string.Empty;
                    sSQL = "SELECT COUNT(DOCUMENT_ATTACH.DOCUMENT_ID) TOTAL_ROW FROM DOCUMENT_ATTACH,DOCUMENT "
                         + "WHERE DOCUMENT_ATTACH.DOCUMENT_ID=DOCUMENT.DOCUMENT_ID AND UPPER(DOCUMENT_FILENAME)='" + this.FileName.ToUpper() + "' AND RECORD_ID=" + this.AttachRecordId.ToString();       
                    iDocCount = Conversion.CastToType<int>(DbFactory.ExecuteScalar(this.ConnectionString, sSQL).ToString(), out bIsSuccess);
                    if (iDocCount > 0)
                    {
                        this.FileName = sFNOnly + Conversion.ToDbDateTime(DateTime.Now) + ((sExtension == "") ? "" : "." + sExtension);
                    }
                }
                //Add by kuladeep for MITS:25212 End 06/27/2011

                //Once the class is loaded with the document details upload the document on the
                //papervision server so as to get the PaperVision Record Id
                byte[] objByte = objDocumentFile.ToArray();
                //Animesh Modified MITS 18055
                //blnSuccess = objPVWrapper.AddDocToPaperVision(this.CreateDate, objCache.GetShortCode(Conversion.ConvertObjToInt(this.Type)), strClaimNumber,strEntityID,strEventDate,objByte, this.Title, strUser, strPassword, out lPVDocID);, m_iClientId
                //blnSuccess = objPVWrapper.AddDocToPaperVision(this.CreateDate, objCache.GetShortCode(Conversion.ConvertObjToInt(this.Type)), strClaimNumber, strEntityID, strEventDate, objByte, this.Title, strUser, strPassword, strEntityName,out lPVDocID); //Animesh Modified to store the original filename on backend on the papervision server, m_iClientId
                blnSuccess = objPVWrapper.AddDocToPaperVision(this.CreateDate, objCache.GetShortCode(Conversion.ConvertObjToInt(this.Type, m_iClientId)), strClaimNumber, strEntityID, strEventDate, objByte, this.Title, this.FileName, strUser, strPassword, strEntityName, out lPVDocID,m_iClientId);
                if (blnSuccess)
                {
                    this.PVDocumentId = lPVDocID;
                    this.IsAtPaperVision = -1;
                    iReturnValue = this.Update();
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("PaperVisionManager.AddDocument.Exception",m_iClientId));
                    return 0;
                }
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("Document.CreateNewDocument.XmlException",m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.CreateNewDocument.Exception",m_iClientId), p_objException);
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
            }

            return iReturnValue;
        }

        /// Name			: Load
        /// Author			: Animesh Sahai 
        /// Date Created	: 29-May-2009
        /// <summary>
        /// Loads the properties of the Document object 
        /// using the input Xml parameter
        /// </summary>
        /// <param name="p_sDocumentXml"> 
        ///		Document properties to be loaded in Xml Format
        ///	</param>
        /// <returns>0 - Success</returns>
        public int Load(string p_sDocumentXml)
        {
            int iReturnValue = 0;

            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlNode objDocumentNode = null;
            XmlNode objXmlNode = null;

            try
            {
                RaiseInitializationException();

                objXmlDocument = new XmlDocument();
                objXmlDocument.LoadXml(p_sDocumentXml);

                objRootElement = objXmlDocument.DocumentElement;
                objDocumentNode = objRootElement.FirstChild;

                if (objDocumentNode.SelectSingleNode(DOCUMENTID_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(DOCUMENTID_XMLTAGNAME);
                    this.DocumentId = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                //if (objDocumentNode.SelectSingleNode(FOLDERID_XMLTAGNAME) != null)
                //{
                //    objXmlNode = objDocumentNode.SelectSingleNode(FOLDERID_XMLTAGNAME);
                //    this.FolderId = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                //    objXmlNode = null;
                //}
                if (objDocumentNode.SelectSingleNode(PVDOCUMENTID_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(PVDOCUMENTID_XMLTAGNAME);
                    this.PVDocumentId = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }


                if (objDocumentNode.SelectSingleNode(CREATEDATE_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(CREATEDATE_XMLTAGNAME);
                    this.CreateDate = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(CATEGORY_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(CATEGORY_XMLTAGNAME);
                    this.Category = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(NAME_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(NAME_XMLTAGNAME);
                    this.Title = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(CLASS_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(CLASS_XMLTAGNAME);
                    this.Class = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(SUBJECT_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(SUBJECT_XMLTAGNAME);
                    this.Subject = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(TYPE_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(TYPE_XMLTAGNAME);
                    this.Type = Common.Conversion.ConvertStrToLong(objXmlNode.InnerText);
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(NOTES_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(NOTES_XMLTAGNAME);
                    this.Notes = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(USERLOGINNAME_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(USERLOGINNAME_XMLTAGNAME);
                    this.UserLoginName = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(FILENAME_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(FILENAME_XMLTAGNAME);
                    this.FileName = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                //if (objDocumentNode.SelectSingleNode(FILEPATH_XMLTAGNAME) != null)
                //{
                //    objXmlNode = objDocumentNode.SelectSingleNode(FILEPATH_XMLTAGNAME);
                //    this.FilePath = objXmlNode.InnerText;
                //    objXmlNode = null;
                //}

                if (objDocumentNode.SelectSingleNode(KEYWORDS_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(KEYWORDS_XMLTAGNAME);
                    this.Keywords = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(ATTACHTABLE_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(ATTACHTABLE_XMLTAGNAME);
                    this.AttachTable = objXmlNode.InnerText;
                    objXmlNode = null;
                }

                if (objDocumentNode.SelectSingleNode(ATTACHRECORDID_XMLTAGNAME) != null)
                {
                    objXmlNode = objDocumentNode.SelectSingleNode(ATTACHRECORDID_XMLTAGNAME);
                    this.AttachRecordId = Conversion.ConvertObjToInt64(objXmlNode.InnerText, m_iClientId);
                    objXmlNode = null;
                }

                this.m_bDocumentPropertiesLoaded = true;
            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.XmlException",m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.Load.Exception",m_iClientId), p_objException);
            }
            finally
            {
                objXmlNode = null;
                objDocumentNode = null;
                objRootElement = null;
                objXmlDocument = null;
            }
            return iReturnValue;
        }

        /// Name			: GetDocumentXml
        /// Author			: Animesh Sahai
        /// Date Created	: 4-June-2009
        /// <summary>
        /// Get document details in Xml format from the property values.
        /// </summary>
        /// <param name="p_sDocumentXml">
        ///		Out Parameter. Returns Document details in Xml format.
        ///	</param>
        /// <returns>0 - Success</returns>		
        public int GetDocumentXml(out string p_sDocumentXml)
        {
            int iReturnValue = 0;
            LocalCache objCache = null;
            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objDocumentElement = null;
            XmlElement objXmlElement = null;

            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;

            try
            {
                RaiseInitializationException();

                objXmlDocument = new XmlDocument();
                objRootElement = objXmlDocument.CreateElement(DATA_XMLTAGNAME);

                objXmlDocument.AppendChild(objRootElement);

                objDocumentElement = objXmlDocument.CreateElement(DOCUMENT_XMLTAGNAME);
                objRootElement.AppendChild(objDocumentElement);

                if (this.m_bDocumentPropertiesLoaded == true)
                {
                    objCache = new LocalCache(this.ConnectionString,m_iClientId);

                    objXmlElement = objXmlDocument.CreateElement(DOCUMENTID_XMLTAGNAME);
                    objXmlElement.InnerText = this.DocumentId.ToString();
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    //objXmlElement = objXmlDocument.CreateElement(FOLDERID_XMLTAGNAME);
                    //objXmlElement.InnerText = this.FolderId.ToString();
                    //objDocumentElement.AppendChild(objXmlElement);
                    //objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CREATEDATE_XMLTAGNAME);
                    objXmlElement.InnerText = this.CreateDate;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CATEGORY_XMLTAGNAME);
                    if (this.Category == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objCache.GetCodeInfo((int)this.Category, ref sShortCode, ref sCodeDesc);
                        objXmlElement.InnerText = sShortCode + " - " + sCodeDesc;
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CATEGORY_CODE_XMLTAGNAME);
                    if (this.Category == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.Category.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(NAME_XMLTAGNAME);
                    objXmlElement.InnerText = this.Title;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CLASS_XMLTAGNAME);
                    if (this.Class == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objCache.GetCodeInfo((int)this.Class, ref sShortCode, ref sCodeDesc);
                        objXmlElement.InnerText = sShortCode + " - " + sCodeDesc;
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(CLASS_CODE_XMLTAGNAME);
                    if (this.Class == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.Class.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(SUBJECT_XMLTAGNAME);
                    objXmlElement.InnerText = this.Subject;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(TYPE_XMLTAGNAME);
                    if (this.Type == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objCache.GetCodeInfo((int)this.Type, ref sShortCode, ref sCodeDesc);
                        objXmlElement.InnerText = sShortCode + " - " + sCodeDesc;
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(TYPE_CODE_XMLTAGNAME);
                    if (this.Type == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.Type.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(NOTES_XMLTAGNAME);
                    objXmlElement.InnerText = this.Notes;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(USERLOGINNAME_XMLTAGNAME);
                    objXmlElement.InnerText = this.UserLoginName;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(FILENAME_XMLTAGNAME);
                    objXmlElement.InnerText = this.FileName;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    //objXmlElement = objXmlDocument.CreateElement(FILEPATH_XMLTAGNAME);
                    //objXmlElement.InnerText = this.FilePath;
                    //objDocumentElement.AppendChild(objXmlElement);
                    //objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(KEYWORDS_XMLTAGNAME);
                    objXmlElement.InnerText = this.Keywords;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(ATTACHTABLE_XMLTAGNAME);
                    objXmlElement.InnerText = this.AttachTable;
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;

                    ////Manoj - LSS Interface
                    //objXmlElement = objXmlDocument.CreateElement(DOCINTERNALTYPE_XMLTAGNAME);
                    //if (this.DocInternalType == 0)
                    //{
                    //    objXmlElement.InnerText = string.Empty;
                    //}
                    //else
                    //{
                    //    objXmlElement.InnerText = this.DocInternalType.ToString();
                    //}
                    //objDocumentElement.AppendChild(objXmlElement);
                    //objXmlElement = null;

                    objXmlElement = objXmlDocument.CreateElement(ATTACHRECORDID_XMLTAGNAME);
                    if (this.AttachRecordId == 0)
                    {
                        objXmlElement.InnerText = string.Empty;
                    }
                    else
                    {
                        objXmlElement.InnerText = this.AttachRecordId.ToString();
                    }
                    objDocumentElement.AppendChild(objXmlElement);

                    objXmlElement = objXmlDocument.CreateElement(ISPVFLAG_XMLTAGNAME);
                    objXmlElement.InnerText = this.IsAtPaperVision.ToString();
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;
                    objXmlElement = objXmlDocument.CreateElement(PVDOCUMENTID_XMLTAGNAME);
                    objXmlElement.InnerText = this.PVDocumentId.ToString();
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;
                    objXmlElement = objXmlDocument.CreateElement(PAPERVISIONURL_XMLTAGNAME);
                    objXmlElement.InnerText = "";
                    objDocumentElement.AppendChild(objXmlElement);
                    objXmlElement = null;
                }

                p_sDocumentXml = objXmlDocument.OuterXml;

            }
            catch (InitializationException p_objInitializationException)
            {
                throw p_objInitializationException;
            }
            catch (RMAppException p_objRMAppException)
            {
                throw p_objRMAppException;
            }
            catch (XmlException p_objXmlException)
            {
                throw new RMAppException(Globalization.GetString("Document.GetDocumentXml.XmlException",m_iClientId), p_objXmlException);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Document.GetDocumentXml.Exception",m_iClientId), p_objException);
            }
            finally
            {
                objXmlElement = null;
                objRootElement = null;
                objDocumentElement = null;
                objXmlDocument = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
            }

            return iReturnValue;
        }



        #endregion



    }
}
