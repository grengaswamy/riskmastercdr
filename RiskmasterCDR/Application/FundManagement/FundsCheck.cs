﻿
using System;
using System.Xml ;
using System.Text;
using Riskmaster.DataModel ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;
using Riskmaster.Settings ;
using System.Collections ;
using System.Data;
using Riskmaster.Application.VSSInterface;
using Riskmaster.Security;
namespace Riskmaster.Application.FundManagement
{
	/// <summary>
	/// FundsCheck class used for void checks and marking checks as cleared
	/// </summary>
	public class FundsCheck
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
        /// <summary>
        /// public variable to store permission info (being set from Businessadaptor layer)
        /// </summary>
        public bool m_userLoginIsAllowedEx = true; //rahul - mits 10138 
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable for UserId 
		/// </summary>
		private int m_iUserId = 0 ;
		/// <summary>
		/// Private variable for UserGroupId
		/// </summary>
		private int m_iUserGroupId = 0 ;
		/// <summary>
		/// Private variable to store the instance of Funds object
		/// </summary>
		private Funds m_objFunds = null ; 	
		/// <summary>
		/// Private variable to store the database type
		/// </summary>
		private string m_sDBType = null ;

        private int m_iClientId = 0;

        //mcapps2 MITS 30236 Start
        public class OffsetStruc
        {
            public int iClaimId;
            public int iUnitId;
            public int iClaimantEId;
            public int iReserveType;
            public int iRCRowId;
            public double dAmount;
            public int iPolCvgId;
            public int iCvgLossId;
        };
        //mcapps2 MITS 30236 End

        //Start: rsushilaggar Void payment Reason 05/21/2010 MITS 19970
        public string sVoidReason { get; set; }
        public string sVoidReason_HTMLComments { get; set; }
        //End rsushilaggar
        
		#endregion
        //anandpraka2:Start changes for MITS 25133
        //private const int RMO_ALLOW_VOID_CLOSED_CLAIM = 10250;
        private const int RMO_ALLOW_VOID_CLOSED_CLAIM = 42;
        //anandpraka2:End changes for MITS 25133
        private const int RMB_FUNDS_TRANSACT = 9650; 
        private Boolean bIsSucess = false;
		#region Constructors
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_iUserId">User Id</param>
		/// <param name="p_iUserGroupId">User Group Id</param>	
		public FundsCheck(string p_sDsnName , string p_sUserName , string p_sPassword , int p_iUserId , int p_iUserGroupId, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_iUserGroupId = p_iUserGroupId ;
            m_iClientId = p_iClientId;
			this.Initialize();
		}
		#endregion

		#region Load Checks
		
		/// <summary>
		/// LoadChecks4Void returns the information related to the checks which have to be voided.
		/// </summary>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_bUseCollections">Collections Flag</param>
		/// <param name="p_bNonPrintedOnly">NonPrinted Only Flag</param>
		/// <param name="p_iCompanyEID">Company EID</param>
		/// <param name="p_iNotAttached">Not Attached Flag</param>
		/// 
		/// <returns>XmlDocument</returns>
        public XmlDocument LoadChecks4Void(string p_sFromDate, string p_sToDate, bool p_bUseCollections, bool p_bNonPrintedOnly, int p_iCompanyEID, int p_iNotAttached, bool bExcludePrintedPayments,int p_iAccountID)
		{
			XmlDocument objXmlDocument = null;
			
			try
			{
				objXmlDocument = new XmlDocument();
                objXmlDocument = LoadChecks(0, p_sFromDate, p_sToDate, p_bUseCollections, p_bNonPrintedOnly, p_iCompanyEID, p_iNotAttached, bExcludePrintedPayments, p_iAccountID);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FundCheck.LoadChecks.Error",m_iClientId) , p_objEx );				
			}
				
			return(objXmlDocument);
		
		}

		/// <summary>
		/// LoadChecks4MarkCleared returns the information related to the checks which have to be marked cleared.
		/// </summary>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_bUseCollections">Collections Flag</param>
		/// <param name="p_bNonPrintedOnly">NonPrinted Only Flag</param>
		/// <param name="p_iCompanyEID">Company EID</param>
		/// <param name="p_iNotAttached">Not Attached Flag</param>
		/// 
		/// <returns>XmlDocument</returns>
        public XmlDocument LoadChecks4MarkCleared(string p_sFromDate, string p_sToDate, bool p_bUseCollections, bool p_bNonPrintedOnly, int p_iCompanyEID, int p_iNotAttached, bool bExcludePrintedPayments,int p_iAccountID)
		{
			XmlDocument objXmlDocument = null;
			
			try
			{
				objXmlDocument = new XmlDocument();
                objXmlDocument = LoadChecks(1, p_sFromDate, p_sToDate, p_bUseCollections, p_bNonPrintedOnly, p_iCompanyEID, p_iNotAttached, bExcludePrintedPayments, p_iAccountID);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FundCheck.LoadChecks.Error",m_iClientId) , p_objEx );				
			}
			
			return(objXmlDocument);
			
		}
        /// <summary>
        /// LoadChecks4Release returns the information related to the checks which have to be Release.
        /// </summary>
        /// <param name="p_sFromDate">From Date</param>
        /// <param name="p_sToDate">To Date</param>
        /// <param name="p_bUseCollections">Collections Flag</param>
        /// <param name="p_bNonPrintedOnly">NonPrinted Only Flag</param>
        /// <param name="p_iCompanyEID">Company EID</param>
        /// <param name="p_iNotAttached">Not Attached Flag</param>
        /// 
        /// <returns>XmlDocument</returns>
        public XmlDocument LoadChecks4Release(string p_sFromDate, string p_sToDate, bool p_bUseCollections, bool p_bNonPrintedOnly, int p_iCompanyEID, int p_iNotAttached, bool bExcludePrintedPayments, int p_iAccountID, int p_iBatchNumber)
        {
            XmlDocument objXmlDocument = null;

            try
            {
                bExcludePrintedPayments = false;
                objXmlDocument = new XmlDocument();
                objXmlDocument = LoadChecks4Release(0, p_sFromDate, p_sToDate, p_bUseCollections, p_bNonPrintedOnly, p_iCompanyEID, p_iNotAttached, bExcludePrintedPayments, p_iAccountID, p_iBatchNumber);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.LoadChecks4Release.Error", m_iClientId), p_objEx);
            }

            return (objXmlDocument);
        }
        /// <summary>
        /// LoadChecks4Release returns the information related to the checks which have to release.
        /// </summary>
        /// <param name="p_iMarkCleared">Cleared Flag</param>
        /// <param name="p_sFromDate">From Date</param>
        /// <param name="p_sToDate">To Date</param>
        /// <param name="p_bUseCollections">Collections Flag</param>
        /// <param name="p_bNonPrintedOnly">NonPrinted Only Flag</param>
        /// <param name="p_iCompanyEID">Company EID</param>
        /// <param name="p_iNotAttached">Not Attached Flag</param>
        /// 
        /// <returns>XmlDocument</returns>
        private XmlDocument LoadChecks4Release(int p_iMarkCleared, string p_sFromDate, string p_sToDate, bool p_bUseCollections, bool p_bNonPrintedOnly, int p_iCompanyEID, int p_iNotAttached, bool bExcludePrintedPayments, int p_iAccountId, int p_iBatchNumber)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objParentElement = null;
            XmlElement objChildElement = null;
            DbReader objReader = null;
            DbReader objReader2 = null; // MITS 34393
            string sSQL = "";
            string sRollupCountSQL = string.Empty; // MITS 34393
            int iCheckStatusTableID = 0;
            int iPrintedCodeID = 0;
            int iRollupId = 0; // MITS 34393
            int iThisRollupId = 0; // MITS 34393
            int iRollupCount = 0; // MITS 34393

            //tanwar2 - mits 30910 - start
          //  int iApplyDedToPayment = 0;
            //tanwar2 - mits 30910 - end

            try
            {
                objXmlDocument = new XmlDocument();
                iCheckStatusTableID = GetTableId("CHECK_STATUS");
                iPrintedCodeID = GetCodeIDWithShort(iCheckStatusTableID.ToString(), "P");

                sSQL = "SELECT TRANS_NUMBER, DATE_OF_CHECK, CTL_NUMBER, LAST_NAME, FIRST_NAME, FUNDS.CLAIM_NUMBER, AMOUNT, TRANS_ID,PAYMENT_FLAG, ACCOUNT_NAME, FUNDS.CLAIM_ID ,FUNDS.ROLLUP_ID , FUNDS.BATCH_NUMBER "; // MITS 34393
                sSQL = sSQL + " FROM FUNDS, ACCOUNT";
                sSQL = sSQL + " WHERE VOID_FLAG = 0 AND FUNDS.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID ";
                sSQL = sSQL + " AND CLEARED_FLAG = 0 ";
                sSQL = sSQL + " And  STATUS_CODE = " + iPrintedCodeID.ToString();
                sSQL = sSQL + " AND DATE_OF_CHECK BETWEEN '" + Conversion.GetDate(p_sFromDate) + "' AND '" + Conversion.GetDate(p_sToDate) + "'";

                if (p_iBatchNumber != 0)
                {
                    sSQL = sSQL + " And  BATCH_NUMBER = " + p_iBatchNumber;
                }
                if (p_iAccountId != 0)
                {
                    sSQL = sSQL + " And  FUNDS.ACCOUNT_ID = " + p_iAccountId;
                }


                sSQL = sSQL + "AND ( EFT_FLAG = 0 OR  EFT_FLAG IS NULL) "; //Added by Amitosh because EFT Printed checks cannot be released again.

                //rupal:start, MITS 26023
                //a check having dormancy status as unclaimed or esheat can not be set to 'released' again
                sSQL = sSQL + "AND ( DORMANCY_STATUS = 0 ) "; 
                //rupal:end
                //Start:Added by Nikhil.Reset check functionality
                //start - Commented by Nikhil.Condition no longer required.
             //   sSQL = sSQL + " AND TRANS_NUMBER<>-1 ";
                //End - Commented by Nikhil.Condition no longer required.
                //End:Added by Nikhil.Reset check functionality

                //Start - Added by Nikhil.JIRA# RMA-6276 
                //sSQL = sSQL + " AND PAYMENT_FLAG = -1 ";
                //End - Added by Nikhil.JIRA# RMA-6276 
                sSQL = sSQL + " AND PAYMENT_FLAG IS NOT NULL AND PAYMENT_FLAG <>0 "; //Ash: RMA-6276
                
                                
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                objParentElement = objXmlDocument.CreateElement("Checks");
                objXmlDocument.AppendChild(objParentElement);
                //tanwar2 - mits 30910 - start
              //  objParentElement.SetAttribute("ApplyDedToPayment", iApplyDedToPayment.ToString());
                //tanwar2 - mits 30910 - end

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iThisRollupId = Conversion.ConvertObjToInt(objReader.GetValue("ROLLUP_ID"), m_iClientId); // MITS 34393
                        if (iThisRollupId != 0) // MITS 34393
                        {
                            sRollupCountSQL = "SELECT COUNT(ROLLUP_ID) R_COUNT FROM FUNDS WHERE ROLLUP_ID = " + iThisRollupId; // MITS 34393
                            objReader2 = DbFactory.GetDbReader(m_sConnectionString, sRollupCountSQL); // MITS 34393
                            if (objReader2 != null) // MITS 34393
                            {
                                while (objReader2.Read()) // MITS 34393
                                {
                                    iRollupCount = Conversion.ConvertObjToInt(objReader2.GetValue("R_COUNT"), m_iClientId); // MITS 34393
                                    if (iRollupCount == 1) // MITS 34393
                                    {
                                        iThisRollupId = 0; // MITS 34393
                                    }
                                }
                            }
                            objReader2.Close(); // MITS 34393
                        }
                        objChildElement = objXmlDocument.CreateElement("Check");
                        objChildElement.SetAttribute("TransId", objReader.GetValue("TRANS_ID").ToString());
                        objChildElement.SetAttribute("TransNumber", objReader.GetValue("TRANS_NUMBER").ToString());
                        objChildElement.SetAttribute("CheckDate", Conversion.GetDBDateFormat(objReader.GetValue("DATE_OF_CHECK").ToString(), "d"));
                        objChildElement.SetAttribute("CtlNumber", objReader.GetValue("CTL_NUMBER").ToString());
                        objChildElement.SetAttribute("LastName", objReader.GetValue("LAST_NAME").ToString());
                        objChildElement.SetAttribute("FirstName", objReader.GetValue("FIRST_NAME").ToString());
                        objChildElement.SetAttribute("ClaimNumber", objReader.GetValue("CLAIM_NUMBER").ToString());
                        objChildElement.SetAttribute("Amount", objReader.GetValue("AMOUNT").ToString());
                        objChildElement.SetAttribute("PaymentFlag", objReader.GetValue("PAYMENT_FLAG").ToString());
                        objChildElement.SetAttribute("AccountName", objReader.GetValue("ACCOUNT_NAME").ToString());
                        objChildElement.SetAttribute("ClaimId", objReader.GetValue("CLAIM_ID").ToString());
                        //objChildElement.SetAttribute("RollUp_ID", objReader.GetValue("ROLLUP_ID").ToString()); // MITS 34393
                        objChildElement.SetAttribute("RollUp_ID", iThisRollupId.ToString()); // MITS 34393
                        objChildElement.SetAttribute("BATCH_NUMBER", objReader.GetValue("BATCH_NUMBER").ToString());
                        objParentElement.AppendChild(objChildElement);
                    }
                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.LoadChecks4Release.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReader2 != null) // MITS 34393
                {
                    objReader2.Close(); // MITS 34393
                    objReader2.Dispose(); // MITS 34393
                }
            }

            return (objXmlDocument);
        }



		#endregion

		#region Get Company Names
		/// <summary>
		/// GetCompanyNames returns the lastname and entity_id information for all the companies.
		/// </summary>
		/// <returns>Xml Document</returns>
		public XmlDocument GetCompanyNames()
		{
			DbReader objReader = null ;

			string sSQL = "" ;
			XmlDocument objXmlDocument = null;
			XmlElement objParentElement = null;
			XmlElement objChildElement = null;
			
			try
			{
				objXmlDocument = new XmlDocument();
				sSQL = " SELECT ENTITY_ID, LAST_NAME FROM ENTITY WHERE ENTITY_TABLE_ID = 1006" ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				
				objParentElement = objXmlDocument.CreateElement("Companies");
				objXmlDocument.AppendChild(objParentElement);

				if ( objReader != null )
				{
					while (objReader.Read())	
					{
						objChildElement = objXmlDocument.CreateElement("Company");
						objChildElement.InnerText = objReader.GetValue("LAST_NAME").ToString();
						objChildElement.SetAttribute("CompanyEid" , objReader.GetValue("ENTITY_ID").ToString());
						objParentElement.AppendChild(objChildElement);
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FundCheck.GetCompanyNames.Error",m_iClientId) , p_objEx );				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				objParentElement = null;
				objChildElement = null;
			}
			return( objXmlDocument );
				
		} 
	
		#endregion

        //Ankit Start : Financial Enhancements - Void Code Reason change
        public XmlDocument GetVoidReasonCode()
        {
            DbReader objReader = null;

            System.Text.StringBuilder sbSQL = null;
            XmlDocument objXmlDocument = null;
            XmlElement objParentElement = null;
            XmlElement objChildElement = null;
            bool isBaseLangCode = false;
            UserLogin objUserLogin = new UserLogin(m_iClientId);
            int iUserLangCode = objUserLogin.objUser.NlsCode;
            int iBaseLangCode = Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);

            try
            {
                if (int.Equals(iUserLangCode, iBaseLangCode) || int.Equals(iUserLangCode, 0))
                    isBaseLangCode = true;

                sbSQL = new System.Text.StringBuilder();
                objXmlDocument = new XmlDocument();
                
                sbSQL.Append(" SELECT CT.CODE_ID, C.SHORT_CODE, CT.CODE_DESC, ");
                if (isBaseLangCode)
                    sbSQL.Append(" '' USER_CODE_DESC ");
                else
                    sbSQL.Append(" UCT.CODE_DESC USER_CODE_DESC ");
                sbSQL.Append(" FROM CODES C ");
                sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT CT On C.CODE_ID = CT.CODE_ID AND CT.LANGUAGE_CODE = " + iBaseLangCode + " ");
                if (!isBaseLangCode)
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT UCT On C.CODE_ID = UCT.CODE_ID AND UCT.LANGUAGE_CODE = " + iUserLangCode + " ");
                sbSQL.Append(" INNER JOIN GLOSSARY G ON G.TABLE_ID = C.TABLE_ID ");
                sbSQL.Append(" WHERE UPPER(G.SYSTEM_TABLE_NAME) = 'VOID_CODE_REASON' ");
                sbSQL.Append(" And C.DELETED_FLAG <> -1 ");
                sbSQL.Append(" ORDER BY CT.CODE_ID, CT.CODE_DESC ");

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());

                objParentElement = objXmlDocument.CreateElement("VoidReasonCodesVal");
                objXmlDocument.AppendChild(objParentElement);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objChildElement = objXmlDocument.CreateElement("VoidReasonCode");
                        if (string.IsNullOrEmpty(Convert.ToString(objReader.GetValue("USER_CODE_DESC"))))
                            objChildElement.InnerText = objReader.GetValue("CODE_DESC").ToString();
                        else
                            objChildElement.InnerText = objReader.GetValue("USER_CODE_DESC").ToString();
                        objChildElement.SetAttribute("CodeID", objReader.GetValue("CODE_ID").ToString());
                        objParentElement.AppendChild(objChildElement);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.GetVoidReasonCode.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                sbSQL = null;
                objParentElement = null;
                objChildElement = null;
            }
            return (objXmlDocument);
        } 
        //Ankit End

        #region Get Account/Sub-Account info in XMLDocument
        /// <summary>
        /// GetAccountNames returns all Account/Sub-Account info in XMLDocument.
        /// </summary>
        /// <returns>Xml Document</returns>
        public XmlDocument GetAccountNames(ArrayList arrAcctList, bool b_UseFundsSubAcc)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objParentElement = null;
            XmlElement objChildElement = null;
            string sAccountCollection = string.Empty;
            string sAccount = string.Empty;
            string sAccountEid = string.Empty;

            try
            {
                objXmlDocument = new XmlDocument();

                if (b_UseFundsSubAcc)
                {
                    sAccountCollection = "SubBankAccounts";
                    sAccount = "SubBankAccount";
                    sAccountEid = "SubBankAccountId";
                }
                else
                {
                    sAccountCollection = "BankAccounts";
                    sAccount = "BankAccount";
                    sAccountEid = "BankAccountId";
                }
                objParentElement = objXmlDocument.CreateElement(sAccountCollection);
                objXmlDocument.AppendChild(objParentElement);

                foreach (FundManager.AccountDetail item in arrAcctList)
                   {
                        objChildElement = objXmlDocument.CreateElement(sAccount);
                        objChildElement.InnerText = item.AccountName;
                        if (b_UseFundsSubAcc)
                        {
                            objChildElement.SetAttribute(sAccountEid, item.SubRowId.ToString());

                        }
                        else
                        {
                            objChildElement.SetAttribute(sAccountEid, item.AccountId.ToString());
                        }
                        objParentElement.AppendChild(objChildElement);
                    }
                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.GetCompanyNames.Error", m_iClientId), p_objEx);
            }
            finally
            {
                
                objParentElement = null;
                objChildElement = null;
            }
            return (objXmlDocument);

        }

        #endregion
		#region Void Checks 
		/// <summary>
		/// VoidChecks voids the checks 
		/// </summary>
		/// <param name="p_sTransIds">TransIds in comma seperated format</param>
		/// <returns>true/false</returns>
		public bool VoidChecks(string p_sTransIds,Riskmaster.Security.UserLogin p_userLogin)
		{
			string [] sTransIdsArray = null;
			int iCounter = 0;
			bool bReturnValue = false;
			int iRollupId = 0;
			int iTransId;
			int iRollupCount = 0 ;
			string sSQL = "";
            string sSortedTransIds = string.Empty;
			DbReader objReader = null;
			DbConnection objConn = null;


			try
			{
                //start: Added by Nitin Goel, MITS 30910, To void payments in descending order of transaction Id, Need to test
                
              
                sSQL = String.Concat("SELECT TRANS_ID FROM FUNDS WHERE FUNDS.TRANS_ID IN (" , p_sTransIds.Trim() , ") ORDER BY DTTM_RCD_LAST_UPD DESC ");
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        if (objReader["TRANS_ID"] != null && objReader["TRANS_ID"] != DBNull.Value)
                        {
                            sSortedTransIds = sSortedTransIds + Convert.ToString(objReader["TRANS_ID"]) + ",";
                        }
                    }
                }
                sSortedTransIds = sSortedTransIds.TrimEnd(',');
                sTransIdsArray = sSortedTransIds.Split(new char[] { ',' });      
                //end:Added by Nitin Goel, MITS 30910, To void payments in descending order of transaction Id, Need to test				          
				iCounter = sTransIdsArray.GetLength(0);
			    string sClaimNumber="";
				for (int iCount=0; iCount < iCounter ; iCount++)
				{
					sSQL = "SELECT ROLLUP_ID,CLAIM_ID FROM FUNDS WHERE FUNDS.TRANS_ID = " + sTransIdsArray[iCount] + " AND VOID_FLAG = 0";
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					
					if( objReader != null )
					{
						if( objReader.Read() )
						{
                            //if (IsClaimClosed(Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId),ref sClaimNumber))//gdass2 MITS 25133
                            //{
                            //    if (!p_userLogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_ALLOW_VOID_CLOSED_CLAIM))
                            //    {
                            //        objReader.Close();
                            //        throw new RMAppException("You are not allowed to VOID a transaction on a CLOSED claim ("+sClaimNumber+")");
                            //    }
                            //}

                            iRollupId = Conversion.ConvertObjToInt(objReader.GetValue("ROLLUP_ID"), m_iClientId);
							objReader.Close();
							if(iRollupId!=0)
							{
								sSQL = "SELECT ROLLUP_ID, COUNT(ROLLUP_ID)  COUNT_ROLLUP FROM FUNDS WHERE ROLLUP_ID = " + iRollupId + " GROUP BY ROLLUP_ID";
								
								objConn = DbFactory.GetDbConnection(m_sConnectionString);
								objConn.Open();
								m_sDBType = objConn.DatabaseType.ToString().ToUpper();
								objConn.Close();
								
								if (m_sDBType != Constants.DB_ACCESS)
								{
									sSQL = sSQL.Replace("AS" , "");
								}
								objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
								
								iRollupCount = 0 ;
								if( objReader != null )
								{
									if( objReader.Read() )
									{
										iRollupCount = Conversion.ConvertObjToInt(objReader.GetValue("COUNT_ROLLUP"), m_iClientId);
									}
								}

								objReader.Close();

								if(iRollupCount>1)
								{
                                    //Start:added by nitin goel, NI ChangesTo void payments in descending order of transaction Id,
									
                                   //Changed by Nikhil.Code review changes
                                    
                                    sSQL = String.Concat("SELECT TRANS_ID FROM FUNDS WHERE ROLLUP_ID = ", iRollupId , " ORDER BY DTTM_RCD_LAST_UPD DESC ");
                                    //End:added by nitin goel
                                    //RUPAL:END
									objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
									
									if( objReader != null )
									{
										while( objReader.Read() )
										{
											iTransId = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
											VoidOneTransaction(iTransId);
										}
									}
								}
								else
								{
									VoidOneTransaction(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
								}
							}
							else
							{
								VoidOneTransaction(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
							}
						}
					}
				}
				bReturnValue = true;

			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FundCheck.VoidChecks.Error",m_iClientId) , p_objEx );
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				if( objConn != null )
				{
					objConn.Close();
					objConn.Dispose();
				}
			}

			return (bReturnValue);
		}

		#endregion
        private bool IsClaimClosed(int p_iClaimId,ref string p_sClaimNumber)
        {
            Claim objClaim;
            LocalCache objLocalCache = null;

            bool bIsClaimClosed = false;
            int iStatusCode = 0;

            try
            {
                if (p_iClaimId == 0)
                    p_iClaimId = m_objFunds.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    if (p_iClaimId > 0)
                        objClaim.MoveTo(p_iClaimId);
                    else
                        throw new RMAppException(Globalization.GetString("FundsForm.IsClaimOpen.InvalidClaim", m_iClientId));

                    p_sClaimNumber=objClaim.ClaimNumber;
                    iStatusCode = objClaim.ClaimStatusCode;
                    objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);

                    if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(iStatusCode)).ToUpper() == "C")
                        bIsClaimClosed = true;
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsForm.IsClaimClosed.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return (bIsClaimClosed);
        }
		#region Mark Checks Cleared
		/// <summary>
		/// MarkChecksCleared marks the checks as cleared.
		/// </summary>
		/// <param name="p_sTransIds">TransIds in comma seperated format</param>
		/// <returns>true/false</returns>
		public bool MarkChecksCleared(string p_sTransIds)
		{
			string [] sTransIdsArray = null;
			int iCounter = 0;
			bool bReturnValue = false;
            int iRollupId = 0;
            int iTransId;
            int iRollupCount = 0;
            string sSQL = "";

            DbReader objReader = null;
            DbConnection objConn = null;
			try
			{
				sTransIdsArray = p_sTransIds.Split(new char[] {','});
				iCounter = sTransIdsArray.GetLength(0);

				if( m_objDataModelFactory == null )
					this.Initialize();
				
				for (int iCount=0; iCount < iCounter ; iCount++)
				{
                    //gagnihotri MITS 12329 07/10/2008 Start
                    sSQL = "SELECT ROLLUP_ID FROM FUNDS WHERE FUNDS.TRANS_ID = " + sTransIdsArray[iCount] + " AND CLEARED_FLAG = 0";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iRollupId = Conversion.ConvertObjToInt(objReader.GetValue("ROLLUP_ID"), m_iClientId);
                            objReader.Close();
                            if (iRollupId != 0)
                            {
                                sSQL = "SELECT ROLLUP_ID, COUNT(ROLLUP_ID)  COUNT_ROLLUP FROM FUNDS WHERE ROLLUP_ID = " + iRollupId + " GROUP BY ROLLUP_ID";

                                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                                objConn.Open();
                                m_sDBType = objConn.DatabaseType.ToString().ToUpper();
                                objConn.Close();

                                if (m_sDBType != Constants.DB_ACCESS)
                                {
                                    sSQL = sSQL.Replace("AS", "");
                                }
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                iRollupCount = 0;
                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        iRollupCount = Conversion.ConvertObjToInt(objReader.GetValue("COUNT_ROLLUP"), m_iClientId);
                                    }
                                }

                                objReader.Close();

                                if (iRollupCount > 1)
                                {
                                    
                                    sSQL = "SELECT TRANS_ID FROM FUNDS WHERE ROLLUP_ID = " + iRollupId;
                                    
                                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            iTransId = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
                                            m_objFunds.MoveTo(iTransId);
                                            if (m_objFunds.ClearedFlag == false)
                                            {
                                                m_objFunds.ClearedFlag = true;
                                                //pmittal5 Mits 14354 02/03/09 - Set Void Date when Payment is Cleared
                                                m_objFunds.VoidDate = Conversion.ToDbDate(System.DateTime.Now);
                                                m_objFunds.Save();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    m_objFunds.MoveTo(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
                                    if (m_objFunds.ClearedFlag == false)
                                    {
                                        m_objFunds.ClearedFlag = true;
                                        //pmittal5 Mits 14354 02/03/09 - Set Void Date when Payment is Cleared
                                        m_objFunds.VoidDate = Conversion.ToDbDate(System.DateTime.Now);
                                        m_objFunds.Save();
                                    }
                                }
                            }
                            else
                            {
                                m_objFunds.MoveTo(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
                                if (m_objFunds.ClearedFlag == false)
                                {
                                    m_objFunds.ClearedFlag = true;
                                    //pmittal5 Mits 14354 02/03/09 - Set Void Date when Payment is Cleared
                                    m_objFunds.VoidDate = Conversion.ToDbDate(System.DateTime.Now);
                                    m_objFunds.Save();
                                }
                            }
                        }
                    }				
                    //gagnihotri MITS 12329 07/10/2008 End
				}
				bReturnValue = true;


			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FundCheck.MarkChecksCleared.Error",m_iClientId) , p_objEx );
			}
			return(bReturnValue);
			
		}

		#endregion

		#region Private Functions
		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
				m_objFunds = ( Funds ) m_objDataModelFactory.GetDataModelObject( "Funds" , false );
                //rsushilaggar MITS 22035 Date 10/05/2010
                m_objFunds.FiringScriptFlag = 2;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FundManager.Initialize.ErrorInit", m_iClientId), p_objEx);				
			}			
		}
		~FundsCheck()
		{
			if( m_objDataModelFactory != null )
				this.Dispose();
		}
		public void Dispose()
		{
			try
			{
				m_objDataModelFactory.UnInitialize();
                m_objDataModelFactory.Dispose();
                m_objFunds.Dispose(); 
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}

        /// <summary>
        /// bOffsetPayment Offsets One transaction.
        /// </summary>
        /// <param name="p_objFunds">Funds Object</param>
        /// <returns>true/false</returns>
        private bool bOffsetPayment(Funds p_objFunds)
        {
            bool bReturn = true;
            int RMO_OFFSET = 47;
            int RMB_FUNDS_TRANSACT = 9650;
            UserLogin objUserlogin = null;
            ColLobSettings objColLobSettings = null;
            LobSettings objLobSettings = null;
            string sSQL = string.Empty;
            StringBuilder sbSQL = null;
            Claim objClaim = null;
            ReserveCurrentList objResCurrList = null;
            bool bfound = false;
            int iClaimId = p_objFunds.ClaimId;
            int iClaimantEId = p_objFunds.ClaimantEid;
            int iUnitId = p_objFunds.UnitId;
            ArrayList lstReserves = new ArrayList();
            OffsetStruc objResItem = new OffsetStruc();
            LocalCache objLocalCache = null;
            int iStatusCode = 0;
            double dblBalance = 0;

            objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
            //objLocalCache = new LocalCache(m_sConnectionString);
            //objCCacheFunctions = new CCacheFunctions(m_sConnectionString);
            objLobSettings = objColLobSettings[m_objFunds.LineOfBusCode];

            objUserlogin = new UserLogin(m_sUserName, m_sDsnName,m_iClientId);

            if (objUserlogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_OFFSET))
            {
                string sReason = "Offset Reversal";


                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_objFunds.ClaimId);
                objResCurrList = objClaim.ReserveCurrentList;

                iStatusCode = objClaim.ClaimStatusCode;
                objLocalCache = new LocalCache(m_sConnectionString,m_iClientId);

                if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(iStatusCode)).ToUpper() == "C")
                {
                    p_objFunds.OffSetFlag = false;
                    p_objFunds.Save();
                }
                else
                {
                    foreach (FundsTransSplit objSplit in p_objFunds.TransSplitList)
                    {
                        foreach (OffsetStruc objResList in lstReserves)
                        {
                            if ((objResList.iRCRowId == objSplit.RCRowId) || ((objResList.iRCRowId == 0) && (objResList.iClaimId == iClaimId) && (objResList.iClaimantEId == iClaimantEId) && (objResList.iUnitId == iUnitId) && (objResList.iReserveType == objSplit.ReserveTypeCode)))
                            {
                                objResList.dAmount = (objResList.dAmount + objSplit.Amount);
                                bfound = true;
                                break;
                            }
                        }
                        if (!bfound)
                        {
                            objResItem.iRCRowId = objSplit.RCRowId;
                            objResItem.iClaimId = iClaimId;
                            objResItem.iClaimantEId = iClaimantEId;
                            objResItem.iUnitId = iUnitId;
                            objResItem.iReserveType = objSplit.ReserveTypeCode;
                            int iDiasabilityCat = 0;
                            int iCvgLossId = 0;
                            int iLosstypeCode = 0;
                            if (objSplit.RCRowId > 0)
                            {
                                iLosstypeCode = CommonFunctions.GetLossType(m_sConnectionString, objSplit.RCRowId, ref iDiasabilityCat, ref iCvgLossId, m_iClientId);
                            }
                            objResItem.iCvgLossId = iLosstypeCode;
                            objResItem.iPolCvgId = 0;
                            objResItem.dAmount = objSplit.Amount;
                            lstReserves.Add(objResItem);
                        }
                        else
                        {
                            bfound = false;
                        }

                    }

                    try
                    {
                        foreach (OffsetStruc objOffSetReserve in lstReserves)
                        {
                            dblBalance = GetReserveBalance(m_objFunds.ClaimId, m_objFunds.ClaimantEid, m_objFunds.UnitId, objOffSetReserve.iReserveType, m_objFunds.LineOfBusCode, m_objFunds.TransId, objOffSetReserve.iPolCvgId, objOffSetReserve.iCvgLossId);
                            if (dblBalance - objOffSetReserve.dAmount > 0.009 && objColLobSettings[m_objFunds.LineOfBusCode].NoNegResAdjFlag == -1)
                            {
                                if (objLobSettings.InsufFundsFlag && objColLobSettings[m_objFunds.LineOfBusCode].NoNegResAdjFlag == -1)
                                {
                                    continue;
                                }
                                throw new RMAppException(Globalization.GetString("FundCheck.VoidChecks.Error", m_iClientId));
                                bReturn = false;
                            }
                            else
                            {
                                foreach (ReserveCurrent objThisReserve in objResCurrList)
                                {
                                    if (objOffSetReserve.iRCRowId != 0)
                                    {
                                        if (objOffSetReserve.iRCRowId == objThisReserve.RcRowId)
                                        {
                                            objThisReserve.ReserveAmount = objThisReserve.ReserveAmount - objOffSetReserve.dAmount;
                                            objThisReserve.sUpdateType = "Reserves";
                                            objThisReserve.Reason = sReason;
                                            objThisReserve.Save();
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (m_objFunds.ClaimId == objThisReserve.ClaimId && m_objFunds.ClaimantEid == objThisReserve.ClaimantEid && m_objFunds.UnitId == objThisReserve.UnitId && objOffSetReserve.iReserveType == objThisReserve.ReserveTypeCode)
                                        {
                                            objThisReserve.ReserveAmount = objThisReserve.ReserveAmount - objOffSetReserve.dAmount;
                                            objThisReserve.sUpdateType = "Reserves";
                                            objThisReserve.Reason = sReason;
                                            objThisReserve.Save();
                                            break;
                                        }
                                    }
                                }
                            }
                            
                        }
                        return bReturn;
                    }
                    catch (RMAppException e)
                    {
                        throw e;
                    }
                    catch( Exception p_objEx )
			        {
                        throw new RMAppException(Globalization.GetString("FundCheck.VoidChecks.Error", m_iClientId), p_objEx);
			        }
		
                    finally
                    {
                        objUserlogin = null;
                        sbSQL = null;
                        objColLobSettings = null;
                        objLobSettings = null;
                        objClaim = null;
                        objResCurrList = null;
                        objResItem = null;
                        objResCurrList = null;
                         
                    }
                }
                return bReturn;
            }

            return bReturn;
        }

        /// <summary>
        /// Get Reserve Balance
        /// </summary>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iClaimantEid">ClaimantId</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_iTransId">TransId</param>
        /// <param name="p_iPolCvgID">Policy Coverage Row ID</param>
        /// <returns>Reserve Balance</returns>
        public double GetReserveBalance(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, int p_iReserveTypeCode, int p_iLob, int p_iTransId, int p_iPolCvgID, int p_iCvgLossId)
        {
            DbReader objReader = null;
            double dblReserveBalance = 0.0;
            string sSQL = string.Empty;
            string sSQLPayment = string.Empty;
            string sSQLCollect = string.Empty;
            double dblReserveAmount = 0.0;
            double dblTemp = 0.0;
            double dblPayment = 0.0;
            double dblCollection = 0.0;

            try
            {
                if (p_iClaimId != 0 && p_iReserveTypeCode != 0)
                {
                    sSQL = "SELECT SUM(RESERVE_AMOUNT), SUM(COLLECTION_TOTAL), SUM(PAID_TOTAL) FROM RESERVE_CURRENT"
                        + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();

                    if (p_iClaimantEid != 0)
                        sSQL += " AND CLAIMANT_EID = " + p_iClaimantEid.ToString();
                    else
                    {
                        if (p_iUnitId != 0)
                            sSQL += " AND UNIT_ID = " + p_iUnitId.ToString();
                    }
                    sSQL += " AND POLCVG_LOSS_ROW_ID = " + p_iCvgLossId.ToString();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblReserveAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(0)));
                            dblTemp = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(2)));
                        }
                    }
                    if (objReader != null)
                    {
                        objReader.Close();
                    }
                    sSQL = "SELECT SUM(FTS.AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS,CODES";
                    if (p_iCvgLossId > 0)
                    {
                        sSQL += ",RESERVE_CURRENT RC";
                    }
                    sSQL += " WHERE "
                         + " FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
                        + " AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString()
                        + " AND FUNDS.UNIT_ID = " + p_iUnitId.ToString()
                        + " AND FUNDS.TRANS_ID = FTS.TRANS_ID"
                        + " AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
                        + " AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
                        + " AND FUNDS.VOID_FLAG = 0";
                    if (p_iCvgLossId > 0)
                    {
                        sSQL += " AND FTS.RC_ROW_ID=RC.RC_ROW_ID "
                            + " AND RC.POLCVG_LOSS_ROW_ID = " + p_iCvgLossId.ToString();
                    }


                    if (p_iTransId != 0)
                        sSQL += " AND FUNDS.TRANS_ID <> " + p_iTransId.ToString();

                    sSQLPayment = sSQL + " AND FUNDS.PAYMENT_FLAG <> 0";
                    sSQLCollect = sSQL + " AND FUNDS.PAYMENT_FLAG = 0";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLPayment);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblPayment = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLCollect);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblCollection = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    dblReserveBalance = CalReserveBalance(dblReserveAmount, dblPayment, dblCollection, p_iLob, 0);
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsForm.GetReserveBalance.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (dblReserveBalance);
        }
        /// <summary>
        /// Calculate Reserve Balance
        /// </summary>
        /// <param name="p_dblReserveAmount">Reserve Amount</param>
        /// <param name="p_dblPaid">Paid Amount</param>
        /// <param name="p_dblCollection">Collection Amount</param>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_iStateCode">State Code</param>
        /// <returns>Reserve Balance</returns>
        private double CalReserveBalance(double p_dblReserveAmount, double p_dblPaid, double p_dblCollection, int p_iLob, int p_iStateCode)
        {
            LocalCache objLocalCache = null;
            ColLobSettings objColLobSettings = null;

            string sShortCode = string.Empty;
            double dblPaidCollectionDiff = 0.0;
            double dblReserveBalance = 0.0;

            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                sShortCode = objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(p_iStateCode));
                if (p_iLob != 0)
                {
                    if (objColLobSettings[p_iLob].CollInRsvBal)
                    {
                        dblPaidCollectionDiff = p_dblPaid - p_dblCollection;
                        if (dblPaidCollectionDiff < 0.0)
                            dblPaidCollectionDiff = 0.0;
                        dblReserveBalance = p_dblReserveAmount - dblPaidCollectionDiff;
                    }
                    else
                        dblReserveBalance = p_dblReserveAmount - p_dblPaid;
                }
                else
                    dblReserveBalance = p_dblReserveAmount - p_dblPaid;

                if (sShortCode == "C" && dblReserveBalance < 0.0)
                    dblReserveBalance = 0.0;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsForm.CalReserveBalance.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objColLobSettings = null;
            }
            return (dblReserveBalance);
        }
		/// <summary>
		/// VoidOneTransaction voids 1 transaction based on Transaction Id.
		/// </summary>
		/// <param name="p_iTransId">Transaction Id</param>
		/// <returns>true/false</returns>
		private bool VoidOneTransaction (int p_iTransId)
		{
			try
			{
                //rsushilaggar MITS 29058 Date 20/08/2012
				//if( m_objDataModelFactory == null )
					this.Initialize();
							
				m_objFunds.MoveTo( p_iTransId );

				if (m_objFunds.VoidFlag == false)
				{
                    //mcapps2 MITS 30236 Start
                    if ((m_objFunds.OffSetFlag) && (m_objFunds.PaymentFlag == true) && (m_objFunds.Amount > 0)) //aaggarwal29: MITS 36830 , The given if block needs to be executed for offset payments.
                    {
                        if (!bOffsetPayment(m_objFunds))
                        {
                            return false;
                        }
                        else
                        {
                            m_objFunds.OffSetFlag = false;
                        }
                    }
                    //mcapps2 MITS 30236 End
					m_objFunds.VoidFlag = true;
                    //Start:Added by rsushilaggar for Void Reason-05/21/2010 MITS - 19970
                    m_objFunds.VoidReason = sVoidReason;
                    m_objFunds.VoidReason_HTMLComments = sVoidReason_HTMLComments;
                    //End rsushilaggar
					m_objFunds.VoidDate = Conversion.ToDbDate( System.DateTime.Now );
                    m_objFunds.FiringScriptFlag = 2;
                    //rsolanki2 : mits 22138 : script were not getting called when checks are voided from here. 
					m_objFunds.Save();
                    //Start:Export payment void data to VSS
                    Claim objClaim = null;
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(m_objFunds.ClaimId);
                    //if (objClaim.VssClaimInd)
                    //Use utility Check instead of Claim Check 
                    if (objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                    {
                        VssExportAsynCall objVss = new VssExportAsynCall(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objVss.AsynchVssReserveExport(m_objFunds.ClaimId, m_objFunds.ClaimantEid, 0, p_iTransId, 0, "","","","", "PaymentVoid");
                        objVss = null;
                    }
                    objClaim.Dispose();
                    //End:Export payment void data to VSS
					return true;
				}
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FundCheck.VoidChecks.Error", m_iClientId), p_objEx);
			}
			finally
			{
			}
			return false;

		}

		/// <summary>
		/// LoadChecks returns the information related to the checks.
		/// </summary>
		/// <param name="p_iMarkCleared">Cleared Flag </param>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_bUseCollections">Collections Flag</param>
		/// <param name="p_bNonPrintedOnly">NonPrinted Only Flag</param>
		/// <param name="p_iCompanyEID">Company EID</param>
		/// <param name="p_iNotAttached">Not Attached Flag</param>
		/// 
		/// <returns>XmlDocument</returns>
        private XmlDocument LoadChecks(int p_iMarkCleared, string p_sFromDate, string p_sToDate, bool p_bUseCollections, bool p_bNonPrintedOnly, int p_iCompanyEID, int p_iNotAttached, bool bExcludePrintedPayments,int p_iAccountId) 
		{
			XmlDocument objXmlDocument = null;
			XmlElement objParentElement = null;
			XmlElement objChildElement = null;
			DbReader objReader = null;
			string sSQL = "";
			int iCheckStatusTableID = 0;
			int iPrintedCodeID = 0;
            int iDeductibleCategory = default(int);
            long iTransNumber = default(long);

			try
			{
				objXmlDocument = new XmlDocument();
				iCheckStatusTableID = GetTableId("CHECK_STATUS");
				iPrintedCodeID = GetCodeIDWithShort(iCheckStatusTableID.ToString() ,"P");

                sSQL = "SELECT TRANS_NUMBER,DEDUCTIBLE_CATEGORY, DATE_OF_CHECK, CTL_NUMBER, LAST_NAME, FIRST_NAME, FUNDS.CLAIM_NUMBER, AMOUNT, TRANS_ID,PAYMENT_FLAG, ACCOUNT_NAME, FUNDS.CLAIM_ID ";
				if (p_iNotAttached == 1)
				{
					sSQL = sSQL + "FROM FUNDS, ACCOUNT";
				}
				else
				{
					sSQL = sSQL + "FROM FUNDS, ACCOUNT, CLAIM, EVENT";
				}
			    sSQL = sSQL + " WHERE VOID_FLAG = 0 AND FUNDS.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID ";
                if (p_iAccountId != -1 && p_iAccountId != 0)
                {
                    sSQL = sSQL + " AND FUNDS.ACCOUNT_ID = " + p_iAccountId.ToString();
                }
				if (p_iNotAttached == 1)
				{
					sSQL = sSQL + " AND FUNDS.CLAIM_ID = 0 ";
				}
				else
				{
                    sSQL = sSQL + " AND FUNDS.CLAIM_ID = CLAIM.CLAIM_ID AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND FUNDS.CLAIM_ID <> 0";
				}    
				if (p_iCompanyEID != 0 && p_iNotAttached == 0)
				{
					sSQL = sSQL + " AND EVENT.DEPT_EID IN (SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE COMPANY_EID = " + p_iCompanyEID.ToString() + ") ";
				}
				if ((p_bNonPrintedOnly) || (bExcludePrintedPayments))
				{
					sSQL = sSQL + " AND STATUS_CODE <> " + iPrintedCodeID.ToString();
				}

                //Added by amitosh for EFT Payments.Adding check for EFT Payments as EFT Payments cnnot be voided.
                //sSQL = sSQL + " AND EFT_FLAG = 0 ";
                //Added by ashutosh MITS 26517
                sSQL = sSQL + "  AND ( EFT_FLAG = 0 or  Eft_flag is null) ";

                //rupal:start, MITS 26023
                //CHECKS WITH DORMANCY STATUS NONZERO ARE EITHER MARKED AS UNCLAIMED OR ESCHEAT, DEFAULT DORMANCY STATUS FOR ALL THE CHECKS WOULD BE ZERO
                sSQL = sSQL + "  AND ( DORMANCY_STATUS = 0) ";
                //rupal:end, MITS 26023

                /* Arnab: MITS-10751 - As cleared transactions can not be voided so commenting lines
                 * And adding another AND condition to the SQL query
                // Vaibhav 10/09/2006 : Cleared transactions can be voided.
                if( p_iMarkCleared == 1 )
                    sSQL = sSQL + " AND CLEARED_FLAG = 0";
                */
                // sSQL = sSQL + " AND CLEARED_FLAG = 0"; commented by ashutosh
                //Ashutosh K Pandey.
                //To fetch those record who are Marked as clear 
                // -2 is send when we are looking for the checks which are alreday cleared
                if (p_iMarkCleared == -2)
                {
                    sSQL = sSQL + " AND CLEARED_FLAG <> 0 ";
                }
                else
                {
                    sSQL = sSQL + " AND CLEARED_FLAG = 0";
                }
                //MITS-10751 End

				if (p_bUseCollections == false) 
				{
					sSQL = sSQL + " AND PAYMENT_FLAG <> 0";
				}
				switch (p_iMarkCleared)
				{
					case 1: if (p_bUseCollections == false)
							{
								sSQL = sSQL + " AND STATUS_CODE = " + iPrintedCodeID.ToString();
							}
							else
							{
								sSQL = sSQL + " AND (PAYMENT_FLAG=0 OR STATUS_CODE = " + iPrintedCodeID.ToString() + ")";
							}
							break;
					
					default: break;
				}

                sSQL = sSQL + " AND DATE_OF_CHECK BETWEEN '" + Conversion.GetDate(p_sFromDate) + "' AND '" + Conversion.GetDate(p_sToDate) + "'";

                //Start:Added by Nitin Goel, For NI PCRs Changes.
                // sSQL = sSQL + " AND TRANS_NUMBER<>-1 " ; // aaggarwal29:, RMA-14580 Moved this to Reader iteration to incorporate Manual Deductible changes
                //End: Added by Nitin goel, for NI PCRs Changes.

                // mits 10138 Start :Rahul - if even permissions not provided, user can still void checks for closed claims
                 //private const int ALLOW_VOIDS_ON_CLOSED_CLAIMS = 9692;
                 //private const int RMB_FUNDS_TRANSACT = 9650;
                if (!m_userLoginIsAllowedEx)
                {
                    sSQL = sSQL + "AND CLAIM.CLAIM_ID NOT IN (SELECT CLAIM.CLAIM_ID FROM CLAIM WHERE CLAIM_STATUS_CODE IN ";
                    sSQL = sSQL + "(SELECT C.CODE_ID FROM CODES C INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID and C.Related_Code_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID IN (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'STATUS') AND UPPER(SHORT_CODE) ='C' )";
                    sSQL = sSQL + "WHERE TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE UPPER(SYSTEM_TABLE_NAME) = 'CLAIM_STATUS')))";
                }
                sSQL = sSQL + " ORDER BY TRANS_NUMBER,DATE_OF_CHECK,CTL_NUMBER";

                // mits 10138 end :Rahul 


				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				
				objParentElement = objXmlDocument.CreateElement("Checks");
				objXmlDocument.AppendChild(objParentElement);

				if ( objReader != null )
				{
					while (objReader.Read())	
					{

                        iDeductibleCategory = objReader.GetInt16("DEDUCTIBLE_CATEGORY");
                        iTransNumber = objReader.GetInt64("TRANS_NUMBER");

                        if (iTransNumber == -1 && iDeductibleCategory != 1)// aaggarwal29:, added the if clause for RMA-14580
                            continue;

						objChildElement = objXmlDocument.CreateElement("Check");
                        objChildElement.SetAttribute("TransId",objReader.GetValue("TRANS_ID").ToString());
						objChildElement.SetAttribute("TransNumber" , objReader.GetValue("TRANS_NUMBER").ToString());
						objChildElement.SetAttribute("CheckDate" , Conversion.GetDBDateFormat(objReader.GetValue("DATE_OF_CHECK").ToString(),"d"));
						objChildElement.SetAttribute("CtlNumber" , objReader.GetValue("CTL_NUMBER").ToString());
						objChildElement.SetAttribute("LastName" , objReader.GetValue("LAST_NAME").ToString());
						objChildElement.SetAttribute("FirstName" , objReader.GetValue("FIRST_NAME").ToString());
						objChildElement.SetAttribute("ClaimNumber" , objReader.GetValue("CLAIM_NUMBER").ToString());
						objChildElement.SetAttribute("Amount" , objReader.GetValue("AMOUNT").ToString());
						objChildElement.SetAttribute("PaymentFlag" , objReader.GetValue("PAYMENT_FLAG").ToString());
						objChildElement.SetAttribute("AccountName" , objReader.GetValue("ACCOUNT_NAME").ToString());
						objChildElement.SetAttribute("ClaimId" , objReader.GetValue("CLAIM_ID").ToString());
						objParentElement.AppendChild(objChildElement);
					}
				}
				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FundCheck.LoadChecks.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}

			return(objXmlDocument);
		}
		
        /// <summary>
        /// Start: rsushilaggar Void payment Reason 05/21/2010 MITS 19970
        /// Check if the void reason setting is enabled.
        /// </summary>
        /// <returns>int</returns>
        public int GetVoidReasonFlag()
        {
            int iVoidReasonFlag = 0;
            DbReader objReader = null;
            try
            {

                objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT ENABLE_VOID_REASON FROM CHECK_OPTIONS");
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iVoidReasonFlag = Conversion.ConvertObjToInt(objReader.GetValue("ENABLE_VOID_REASON"), m_iClientId);
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.LoadChecks.Error", m_iClientId), p_objEx);
            }
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
            return iVoidReasonFlag;
        }

		/// <summary>
		/// GetTableId returns the TABLE_ID for a particular SYSTEM_TABLE_NAME in the GLOSSARY table.
		/// </summary>
		/// <param name="p_sTableName">SYSTEM_TABLE_NAME for which the TABLE_ID is required</param>
		/// <returns>Table Id</returns>
		private int GetTableId(string p_sTableName )
		{
			DbReader objReader = null ;

			int iTableId = 0 ;
			string sSQL = "" ;
			
			try
			{
				if( p_sTableName == "" )
					return(0) ;

				sSQL = " SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + p_sTableName + "'" ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if ( objReader != null )
					if( objReader.Read() )				
						iTableId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TABLE_ID" ), m_iClientId );					
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FundManager.GetTableId.ErrorTableID", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( iTableId );
				
		} 

		/// <summary>
		/// GetCodeId returns the CODE_ID for a particular TABLE_ID and a particular short_code.
		/// </summary>
		/// <param name="p_sTableId">TABLE_ID for which the CODE_ID is required</param>
		/// <param name="p_sShortCode">SHORT_CODE</param>
		/// <returns>Code Id</returns>
		private int GetCodeIDWithShort(string p_sTableId , string p_sShortCode)
		{
			DbReader objReader = null ;

			int iCodeId = 0;
			string sSQL = "" ;
			
			try
			{
				if( p_sShortCode == "" )
					return(0) ;

				sSQL = "SELECT CODE_ID FROM CODES WHERE SHORT_CODE = '" + p_sShortCode + "' AND TABLE_ID=" + p_sTableId;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if ( objReader != null )
					if( objReader.Read() )				
						iCodeId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "CODE_ID" ), m_iClientId );					
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FundManager.GetTableId.ErrorTableID", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( iCodeId );
				
		}

        /// <summary>
        /// LoadChecks4MarkUnCleared returns the information related to the checks which have to be UnCleared/Reset to Print State.
        /// </summary>
        /// <param name="p_sFromDate">From Date</param>
        /// <param name="p_sToDate">To Date</param>
        /// <param name="p_bUseCollections">Collections Flag</param>
        /// <param name="p_bNonPrintedOnly">NonPrinted Only Flag</param>
        /// <param name="p_iCompanyEID">Company EID</param>
        /// <param name="p_iNotAttached">Not Attached Flag</param>
        /// 
        /// <returns>XmlDocument</returns>

        public XmlDocument LoadChecks4MarkUnCleared(string p_sFromDate, string p_sToDate, bool p_bUseCollections, bool p_bNonPrintedOnly, int p_iCompanyEID, int p_iNotAttached, bool bExcludePrintedPayments, int p_iAccountID)
        {
            XmlDocument objXmlDocument = null;

            try
            {
                objXmlDocument = new XmlDocument();
                //-2 will be pass to load those check which are alreday in clered State
                objXmlDocument = LoadChecks(-2, p_sFromDate, p_sToDate, p_bUseCollections, p_bNonPrintedOnly, p_iCompanyEID, p_iNotAttached, bExcludePrintedPayments, p_iAccountID);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.LoadChecks4MarkUnCleared.Error", m_iClientId), p_objEx);
            }
          

            return (objXmlDocument);
        }
        /// <summary>
        /// MarkChecksUnCleared marks the checks as Uncleared.
        /// </summary>
        /// <param name="p_sTransIds">TransIds in comma seperated format</param>
        /// <returns>true/false</returns>
        public bool MarkChecksUnCleared(string p_sTransIds)
        {
            string[] sTransIdsArray = null;
            int iCounter = 0;
            bool bReturnValue = false;
            int iRollupId = 0;
            int iTransId;
            int iRollupCount = 0;
            string sSQL = "";

            DbReader objReader = null;
            DbConnection objConn = null;
            try
            {
                sTransIdsArray = p_sTransIds.Split(new char[] { ',' });
                iCounter = sTransIdsArray.GetLength(0);

                if (m_objDataModelFactory == null)
                    this.Initialize();

                for (int iCount = 0; iCount < iCounter; iCount++)
                {
                    //gagnihotri MITS 12329 07/10/2008 Start
                    sSQL = "SELECT ROLLUP_ID FROM FUNDS WHERE FUNDS.TRANS_ID = " + sTransIdsArray[iCount] + " AND CLEARED_FLAG = -1";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iRollupId = Conversion.ConvertObjToInt(objReader.GetValue("ROLLUP_ID"), m_iClientId);
                            objReader.Close();
                            if (iRollupId != 0)
                            {
                                sSQL = "SELECT ROLLUP_ID, COUNT(ROLLUP_ID)  COUNT_ROLLUP FROM FUNDS WHERE ROLLUP_ID = " + iRollupId + " GROUP BY ROLLUP_ID";

                                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                                objConn.Open();
                                m_sDBType = objConn.DatabaseType.ToString().ToUpper();
                                objConn.Close();

                                if (m_sDBType != Constants.DB_ACCESS)
                                {
                                    sSQL = sSQL.Replace("AS", "");
                                }
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                iRollupCount = 0;
                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        iRollupCount = Conversion.ConvertObjToInt(objReader.GetValue("COUNT_ROLLUP"), m_iClientId);
                                    }
                                }

                                objReader.Close();

                                if (iRollupCount > 1)
                                {
                                    
                                    sSQL = "SELECT TRANS_ID FROM FUNDS WHERE ROLLUP_ID = " + iRollupId;
                                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            iTransId = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
                                            m_objFunds.MoveTo(iTransId);
                                            if (m_objFunds.ClearedFlag == true)
                                            {
                                                m_objFunds.ClearedFlag = false;
                                                m_objFunds.Save();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    m_objFunds.MoveTo(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
                                    if (m_objFunds.ClearedFlag == true)
                                    {
                                        m_objFunds.ClearedFlag = false;
                                        m_objFunds.Save();
                                    }
                                }
                            }
                            else
                            {
                                m_objFunds.MoveTo(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
                                if (m_objFunds.ClearedFlag == true)
                                {
                                    m_objFunds.ClearedFlag = false;
                                    m_objFunds.Save();
                                }
                            }
                        }
                    }

                }
                bReturnValue = true;


            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.MarkChecksUnCleared.Error", m_iClientId), p_objEx);
            }
            return (bReturnValue);

        }
        /// <summary>
        /// ReleaseChecks 
        /// </summary>
        /// <param name="p_sTransIds">TransIds in comma seperated format</param>
        /// <returns>true/false</returns>
        public bool ReleaseChecks(string p_sTransIds, Riskmaster.Security.UserLogin p_userLogin)
        {
            string[] sTransIdsArray = null;
            int iCounter = 0;
            bool bReturnValue = false;
            int iRollupId = 0;
            int iTransId;
            int iRollupCount = 0;
            string sSQL = "";

            DbReader objReader = null;
            DbConnection objConn = null;


            try
            {
                sTransIdsArray = p_sTransIds.Split(new char[] { ',' });
                iCounter = sTransIdsArray.GetLength(0);

                for (int iCount = 0; iCount < iCounter; iCount++)
                {


                    // process only those check which are  alreday in print status 
                    //and they are not void nor cleared

                    sSQL = "SELECT ROLLUP_ID,CLAIM_ID FROM FUNDS WHERE FUNDS.TRANS_ID = " + sTransIdsArray[iCount] + " AND VOID_FLAG = 0 AND CLEARED_FLAG=0";
                    sSQL = sSQL + " And Status_Code=" + GetCodeIDWithShort(GetTableId("CHECK_STATUS").ToString(), "P");
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {

                            iRollupId = Conversion.ConvertObjToInt(objReader.GetValue("ROLLUP_ID"), m_iClientId);
                            objReader.Close();
                            if (iRollupId != 0)
                            {
                                sSQL = "SELECT ROLLUP_ID, COUNT(ROLLUP_ID)  COUNT_ROLLUP FROM FUNDS WHERE ROLLUP_ID = " + iRollupId + " GROUP BY ROLLUP_ID";

                                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                                objConn.Open();
                                m_sDBType = objConn.DatabaseType.ToString().ToUpper();
                                objConn.Close();

                                if (m_sDBType != Constants.DB_ACCESS)
                                {
                                    sSQL = sSQL.Replace("AS", "");
                                }
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                iRollupCount = 0;
                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        iRollupCount = Conversion.ConvertObjToInt(objReader.GetValue("COUNT_ROLLUP"), m_iClientId);
                                    }
                                }

                                objReader.Close();

                                if (iRollupCount > 1)
                                {
                                    
                                    sSQL = "SELECT TRANS_ID FROM FUNDS WHERE ROLLUP_ID = " + iRollupId;
                                   
                                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            iTransId = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
                                            ReleaseOneTransaction(iTransId);

                                        }
                                    }
                                }
                                else
                                {

                                    ReleaseOneTransaction(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
                                }
                            }
                            else
                            {
                                ReleaseOneTransaction(Conversion.ConvertStrToInteger(sTransIdsArray[iCount]));
                            }
                        }
                    }
                }
                bReturnValue = true;

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.ReleaseChecks.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
            }

            return (bReturnValue);
        }
        private bool ReleaseOneTransaction(int p_iTransId)
        {
            int iCheckStatusTableID = 0;
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                m_objFunds.MoveTo(p_iTransId);

                iCheckStatusTableID = GetTableId("CHECK_STATUS");
                if (m_objFunds.StatusCode == GetCodeIDWithShort(iCheckStatusTableID.ToString(), "P"))
                {
                    //Added by Nikhil.Void deductible Recovery on reset check.
                    m_objFunds.IsCheckReset = true;
                    //Added by Nikhil.Void deductible Recovery on reset check.
                    m_objFunds.TransNumber = 0;
                    m_objFunds.BatchNumber = 0;
                    m_objFunds.StatusCode = GetCodeIDWithShort(iCheckStatusTableID.ToString(), "R");
                    m_objFunds.PrecheckFlag = false;
                    m_objFunds.RollupId = 0;
                    m_objFunds.Save();
                    return true;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundCheck.ReleaseChecks.Error", m_iClientId), p_objEx);
            }
            finally
            {
            }
            return false;

        }
		#endregion
	}
}
