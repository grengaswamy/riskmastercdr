﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                            *
 **********************************************************************************************
 * 05/23/2014 | 34270           | aahuja21   | Added check memo in payment history gridview
 * 12/17/2013 | 34082           | pgupta93   | Add New fields for MultiCurrency
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Xml;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Collections;
using System.Data;
using System.Text;
using Riskmaster.Application.BRS;
using Riskmaster.Security;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;
using Riskmaster.Application.VSSInterface;
using System.Collections.Generic; //33447

namespace Riskmaster.Application.FundManagement
{
    /// <summary>
    /// Fund Manager Class used for Funds operations.
    /// </summary>
    ///* Amendment  -->
    ///  1.Date		: 7 Feb 06 
    ///    Desc		: Replacing RMO_ACCESS use, and changing params of permission
    ///				  violation exception so it can be correctly called. 
    ///    Author	: Sumit
    /// <summary>	

    public class FundManager : IDisposable
    {
        //Nikhil Garg		Dated: 27-Jan-2006
        //Permission Codes for checking for View Payment History Permission.
        #region security codes
        private const int RMB_CLAIMWC_RESERVES = 5400;
        private const int RMB_CLAIMGC_RESERVES = 2250;
        private const int RMB_CLAIMVA_RESERVES = 8550;
        private const int RMB_CLAIMVA_UNIT_RESERVES = 8250; //TR:2306 28Feb2006 Nitesh 
        private const int RMB_CLAIMDI_RESERVES = 62400;
        private const int RMO_PAYMENT_HISTORY = 150;
        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        private const int RMB_CLAIMPC_RESERVES = 43000;
        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314
        #endregion

        #region Constants
        private const string LANGUAGE_CODE = "CODES_TEXT.LANGUAGE_CODE = 1033";
        private const string CODE_FLDTYPE = "code";
        private const string CODEDETAIL_FLDTYPE = "codewithdetail";
        private const string COMBOBOX_FLDTYPE = "combobox";
        private const string EIDLOOKUP_FLDTYPE = "eidlookup";
        private const string ATT_CODEID = "codeid";

        //Start -  Added by Nikhil. SMS Settings for deductible procesing
        private const int DED_PROC_SHOW = 9698;
        //End - Added by Nikhil. SMS Settings for deductible procesing

        #endregion

        #region Variable Declaration
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store DSNID
        /// </summary>
        private int m_iDSNID = 0;
        /// <summary>
        /// Private variable to store Manager ID of user.
        /// </summary>
        private int m_iManagerID = 0;
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;
        /// <summary>
        /// Private variable to store the instance of Funds object
        /// </summary>
        private Funds m_objFunds = null;
        /// <summary>
        /// Private variable to store the instance of Accounts object
        /// </summary>
        private Account m_objAccounts = null; // rahul mits 10339 - 18 - sept 2007  
        /// <summary>
        /// Private variable to store the TandE flag
        /// </summary>
        private bool m_bIsTandE = false;
        /// <summary>
        /// Private variable to store the instance of Confirmation List
        /// </summary>
        private ArrayList m_arrlstConfirmations = null;
        /// <summary>
        /// Private variable to store the instance of Errors List
        /// </summary>
        private ArrayList m_arrlstErrors = null;
        /// <summary>
        /// Private variable to store the instance of Warning List
        /// </summary>
        private ArrayList m_arrlstWarnings = null;
        /// <summary>
        /// Private variable for OverrideFundsAccountLink Flag
        /// </summary>
        private bool m_bOverrideFundsAccountLink = false;
        /// <summary>
        /// Private variable for FundsAccountLink Flag
        /// </summary>
        private bool m_bFundsAccountLink = false;
        /// <summary>
        /// Private variable for InsufAmntXml string
        /// </summary>
        private string m_sInsufAmntXml = "";
        /// <summary>
        /// Private variable for PayeeType string
        /// </summary>
        private string m_sPayeeType = "";
        /// <summary>
        /// Private variable for PostDate Flag
        /// </summary>
        private bool m_bPostDate = false;
        /// <summary>
        /// Private variable for ExportToFile Flag
        /// </summary>
        private bool m_bExportToFile = false;
        /// <summary>
        /// Private variable for Consolidate Flag
        /// </summary>
        private bool m_bConsolidate = false;
        /// <summary>
        /// Private variable for Max Auto Checks 
        /// </summary>
        private int m_iMaxAutoChecks = 50;
        /// <summary>
        /// Private variable for PlaceSupHoldPayLimits Flag
        /// </summary>
        private bool m_bPlaceSupHoldPayLimits = false;
        /// <summary>
        /// Private variable for PlaceSupHoldPayDetailLimits Flag
        /// </summary>
        private bool m_bPlaceSupHoldPayDetailLimits = false;
        /// <summary>
        /// Private variable for PlaceSupHoldNewPayee Flag
        /// </summary>
        private bool m_bPlaceSupHoldNewPayee = false;
        /// <summary>
        /// Private variable for PlaceSupHoldTransactionType Flag
        /// </summary>
        private bool m_bPlaceSupHoldTransactionType = false;
        /// <summary>
        /// Private variable for UseSpecificSupervisor Flag
        /// </summary>
        private bool m_bUseSpecificSupervisor = false;
        /// <summary>
        /// Private variable for SupChainHold Flag
        /// </summary>
        private bool m_bSupChainHold = false;
        /// <summary>
        /// Private variable for UserId 
        /// </summary>
        private int m_iUserId = 0;
        /// <summary>
        /// Private variable for UserGroupId
        /// </summary>
        private int m_iUserGroupId = 0;
        /// <summary>
        /// Private variable for DataChanged 
        /// </summary>
        private bool m_bDataChanged = false;
        /// <summary>
        /// Private variable for AllowPaymentsClosedClaims  
        /// </summary>
        private bool m_bAllowPaymentsClosedClaims = false;
        /// <summary>
        /// Private variable for UseQueuedPayments  
        /// </summary>
        private bool m_bUseQueuedPayments = false;

        /// <summary>
        /// Private variable for DeletedSplitIds
        /// </summary>
        private ArrayList m_arrlstDeletedSplitIds = null;
        /// <summary>
        /// Private Security DSN.
        /// </summary>
        private string m_securityConnectionString = "";
        /// <summary>
        /// Private Setting whether to send diary when status is On Hold.
        /// </summary>
        private int iHoldSendDiary = 0;
        private string sHoldText = "";

        /// <summary>
        /// Auto-close functionality of claims
        /// </summary>
        private bool m_AutoCloseTransTypeUsed = false;

        /// <summary>
        /// local temp variable for Auto-Close
        /// </summary>
        private ArrayList m_ArrCodeIdTransType = new ArrayList();

        /// <summary>
        /// local variable for claim status
        /// </summary>
        private int m_iStatusCode = 0;

        /// <summary>
        /// local variable to determine whether to display Insufficient Reserves POP-UP
        /// Added by Mihika Defect# 2122 1-Mar-2006
        /// </summary>
        private bool m_bShowInsuffResPopUp = false;
        //kchoudhary2, 12th Dec, MITS 8431 starts

        /// <summary>		
        /// This variable will generate the report in PDF Format.
        /// </summary>
        private PrintPDF m_objPrintPDF = null;
        /// <summary>		
        /// This is a structure variable. 
        /// It will contain the data along with the information like its type, etc that needs to be
        /// displayed in the report.		
        /// </summary>
        private ReportData m_structReportData;
        /// <summary>		
        /// An arraylist contains the start position of columns for displaying data in the report.		
        /// </summary>
        private ArrayList m_arrlstTabAt = null;
        /// <summary>		
        /// An arraylist contains the data needs to be displayed in report.
        /// </summary>
        private ArrayList m_arrlstReportData = null;
        //kchoudhary2, 12th Dec, MITS 8431 ends

        //Ash - cloud, config settings moved to DB
        private int m_iClientId = 0;
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

        //smahajan6: Safeway: Payment Supervisory Approval : Start
        /// <summary>
        /// Notify Immediate Supervisor or not
        /// </summary>
        private bool m_bPmtNotfyImmSup = false;
        //smahajan6: Safeway: Payment Supervisory Approval : End
        private static int iClaimCurrCode = 0;//Deb Multi Currency 
        /// <summary>
        /// Language code of the user logged in
        /// </summary>
        private int m_iLangCode = 0; //Aman MITS 31744
        #endregion
        //Aman MITS 31744
        #region Properties

        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }

        #endregion
        #region Constructors
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iUserId">User Id</param>
        /// <param name="p_iUserGroupId">User Group Id</param>		
        public FundManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iUserId = p_iUserId;
            m_iUserGroupId = p_iUserGroupId;
            m_iClientId = p_iClientId;
            this.Initialize();
        }
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iUserId">User Id</param>
        /// <param name="p_iUserGroupId">User Group Id</param>	
        /// <param name="p_iClaimId">Claim Id</param>
        public FundManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClaimId, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iUserId = p_iUserId;
            m_iUserGroupId = p_iUserGroupId;
            m_iClientId = p_iClientId;
            this.Initialize();
            this.InitializeFromClaimId(p_iClaimId, 0, 0);
        }

        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iUserId">User Id</param>
        /// <param name="p_iManagerId">Manager Id</param>
        /// <param name="p_sSecurityConnectionString">Security Connection string</param>
        /// <param name="p_iUserGroupId">User Group Id</param>
        public FundManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, string p_sSecurityConnectionString, int p_iDSNID, int p_iManagerID, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iUserId = p_iUserId;
            m_iUserGroupId = p_iUserGroupId;
            m_securityConnectionString = p_sSecurityConnectionString;
            m_iDSNID = p_iDSNID;
            m_iManagerID = p_iManagerID;
            m_iClientId = p_iClientId;
            this.Initialize();
        }
        /// <summary>
        /// Destructor
        /// </summary>
        ~FundManager()
        {
            if (m_objDataModelFactory != null)
                this.Dispose();
        }
        #endregion

        #region Experience
        /// <summary>
        /// Get the experience data
        /// </summary>
        /// <param name="p_bCollection">Collection Flag</param>
        /// <param name="p_iEID">ID</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetExperience(bool p_bIsCollection, int p_iEID)
        {
            Experience structExperience;
            XmlElement objRootNode = null;

            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                objRootNode = null;
                this.StartDocument(ref objRootNode, "Experience");

                if (p_bIsCollection)
                {
                    objRootNode.SetAttribute("TransType", "Collection");
                    structExperience = this.GetPayorExperience(p_iEID);
                }
                else
                {
                    objRootNode.SetAttribute("TransType", "Payment");
                    structExperience = this.GetPayeeExperience(p_iEID);

                }

                this.CreateAndSetElement(objRootNode, "NumberOfTrans", structExperience.NumberOfTransactions.ToString());
                this.CreateAndSetElement(objRootNode, "Amount", string.Format("{0:C}", structExperience.Amount));
                this.CreateAndSetElement(objRootNode, "AvgAmount", string.Format("{0:C}", structExperience.AvgAmount));
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetExperience.ErrorExperience", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
            }
            return (m_objDocument);
        }
        /// <summary>
        /// Get the payee experience
        /// </summary>
        /// <param name="p_iPayeeEID">PayeeEID</param>
        /// <returns>Experience structure</returns>
        private Experience GetPayeeExperience(int p_iPayeeEID)
        {
            Experience structExperience;
            //DbReader objReader = null ;   mini performance issue mits 32156
            string sSQL;

            try
            {
                structExperience = new Experience();

                if (p_iPayeeEID == 0)
                    p_iPayeeEID = m_objFunds.PayeeEid;

                if (p_iPayeeEID != 0)
                {
                    sSQL = "SELECT COUNT(*), SUM(AMOUNT) FROM FUNDS WHERE PAYEE_EID = "
                        + p_iPayeeEID + " AND VOID_FLAG = 0 AND PAYMENT_FLAG<>0";
                    //objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL ); mini performance issue
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                structExperience.NumberOfTransactions = Common.Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                                structExperience.Amount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(1)));
                            }
                        }
                    }
                }

                if (structExperience.NumberOfTransactions > 0)
                    structExperience.AvgAmount = structExperience.Amount / structExperience.NumberOfTransactions;
                else
                    structExperience.AvgAmount = structExperience.Amount;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPayeeExperience.ErrorPayeeExperience", m_iClientId), p_objEx);
            }
            //mini performance issue mits 32156
            //finally
            //{
            //    if( objReader != null )
            //    {
            //        objReader.Close();
            //        objReader.Dispose();
            //    }
            //}
            //mini performance issue mits 32156
            return (structExperience);
        }

        /// <summary>
        /// Get the payer experience
        /// </summary>
        /// <param name="p_iPayorEID">PayorEID</param>
        /// <returns>Experience data</returns>
        private Experience GetPayorExperience(int p_iPayorEID)
        {
            Experience structExperience;
            //DbReader objReader = null ; mini performance issue mits 32156
            string sSQL;

            try
            {
                structExperience = new Experience();

                if (p_iPayorEID == 0)
                    p_iPayorEID = m_objFunds.PayeeEid;

                if (p_iPayorEID != 0)
                {
                    sSQL = " SELECT COUNT(*), SUM(AMOUNT) FROM FUNDS WHERE PAYEE_EID = "
                         + p_iPayorEID + " AND VOID_FLAG = 0 AND COLLECTION_FLAG<> 0 ";

                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        //objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL ); mini performance issue
                        //mini perofrmance mits mits 32156
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                structExperience.NumberOfTransactions = Common.Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                                structExperience.Amount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(1)));
                            }
                        }
                    }
                }

                if (structExperience.NumberOfTransactions > 0)
                    structExperience.AvgAmount = structExperience.Amount / structExperience.NumberOfTransactions;
                else
                    structExperience.AvgAmount = structExperience.Amount;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPayorExperience.ErrorPayorExperience", m_iClientId), p_objEx);
            }
            // mini performance issue mits 32156
            //finally
            //{
            //    if( objReader != null )
            //    {
            //        objReader.Close();
            //        objReader.Dispose();
            //    }
            //} 
            //mini performance issue mits 32156
            return (structExperience);
        }

        /// <summary>
        /// Structure For Experience data
        /// </summary>
        private struct Experience
        {
            /// <summary>
            /// Private variable for Number Of Transactions.
            /// </summary>
            private int iNumberOfTransactions;
            /// <summary>
            /// Private variable for Amount.
            /// </summary>
            private double dblAmount;
            /// <summary>
            /// Private variable for Avg Amount.
            /// </summary>
            private double dblAvgAmount;
            /// <summary>
            /// Read/Write property for NumberOfTransactions.
            /// </summary>
            internal int NumberOfTransactions
            {
                get
                {
                    return (iNumberOfTransactions);
                }
                set
                {
                    iNumberOfTransactions = value;
                }
            }
            /// <summary>
            /// Read/Write property for Amount.
            /// </summary>
            internal double Amount
            {
                get
                {
                    return (dblAmount);
                }
                set
                {
                    dblAmount = value;
                }
            }
            /// <summary>
            /// Read/Write property for AvgAmount.
            /// </summary>
            internal double AvgAmount
            {
                get
                {
                    return (dblAvgAmount);
                }
                set
                {
                    dblAvgAmount = value;
                }
            }
        }
        #endregion

        #region Payment Collection Methods :  GetPaymentCollection , DeletePaymentCollection
        /// <summary>
        /// GetPaymentCollection returns the Readonly status and Ctl number Only.
        /// Actual data will be either in Session XML or it will be a blank PaymentCollection.
        /// </summary>
        /// <param name="p_iTransID">TransId</param>
        /// <param name="p_iClaimId">ClaimId</param>		
        /// <param name="p_iClaimantId">ClaimantId</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_iSplitId">SplitId</param>
        /// <param name="p_bAllowPaymentsClosedClaims">AllowPaymentsClosedClaims Flag</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetPaymentCollection(int p_iTransID, int p_iClaimId, int p_iClaimantId, int p_iUnitId, int p_iSplitId, bool p_bAllowPaymentsClosedClaims)
        {
            XmlElement objRootNode = null;

            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (p_iTransID != 0)
                    m_objFunds.MoveTo(p_iTransID);
                else
                    if (p_iClaimId != 0)
                        this.InitializeFromClaimId(p_iClaimId, p_iClaimantId, p_iUnitId);

                m_bAllowPaymentsClosedClaims = p_bAllowPaymentsClosedClaims;

                objRootNode = null;
                this.StartDocument(ref objRootNode, "PaymentCollection");

                this.CreateAndSetElement(objRootNode, "IsReadOnly", (this.IsReadOnly()).ToString());
                this.CreateAndSetElement(objRootNode, "CtlNumber", m_objFunds.CtlNumber);
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPaymentCollection.ErrorPaymentCollection", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
            }
            return (m_objDocument);
        }

        /// <summary>
        ///Delete Payment Collection function does not delete the object from the session XML. 
        ///Targeted object has already deleted at Presentation layer. 
        ///The Input XML does not contain the deleted node. 
        ///This function just deletes the entry from m_objFunds object and then calls 
        ///GetXMLForTransaction function to return the display XML. 
        /// </summary>
        /// <param name="p_sSplitXML">
        /// XML contains Split information for each payment collection. 		
        /// </param>
        /// <param name="p_sTransactionDataXML">Transaction Data XML</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument DeletePaymentCollection(string p_sSplitXML, string p_sTransactionDataXML)
        {
            XmlDocument objSplitDoc = null;
            XmlDocument objTransactionDataDoc = null;

            int iTransId = 0;
            int iClaimId = 0;
            int iClaimantId = 0;
            int iUnitId = 0;

            try
            {
                objSplitDoc = new XmlDocument();
                objSplitDoc.LoadXml(p_sSplitXML);

                objTransactionDataDoc = new XmlDocument();
                objTransactionDataDoc.LoadXml(p_sTransactionDataXML);

                iTransId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "TransId"));
                iClaimId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "ClaimId"));
                iClaimantId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "ClaimantId"));
                iUnitId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "UnitId"));
                m_bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(this.GetValue(objTransactionDataDoc, "AllowPaymentsClosedClaims"));

                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (iTransId != 0)
                    m_objFunds.MoveTo(iTransId);
                else
                    this.InitializeFromClaimId(iClaimId, iClaimantId, iUnitId);

                this.UpdateFundsObjectForTransactionData(objTransactionDataDoc);

                this.UpdateFundsObjectForPaymentCollectionData(objSplitDoc);

                this.GetXMLForTransaction();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.DeletePaymentCollection.ErrorDelPayColl", m_iClientId), p_objEx);
            }
            finally
            {
                objSplitDoc = null;
                objTransactionDataDoc = null;
            }
            return (m_objDocument);
        }


        #endregion

        #region Navigate Transaction Methods : Prev , Next , First , Last , MoveTo , New
        /// <summary>
        /// Get Previous Transaction 
        /// </summary>
        /// <param name="p_iTransId">TransID</param>
        /// <param name="p_bAllowPaymentsClosedClaims">AllowPaymentsClosedClaims Flag</param>
        /// <returns>XmlDocument</returns>				
        public XmlDocument GetPreviousTransaction(int p_iTransId, bool p_bAllowPaymentsClosedClaims)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (p_iTransId > 0)
                {
                    m_objFunds.MoveTo(p_iTransId);
                    m_objFunds.MovePrevious();
                }
                else
                    m_objFunds.MoveFirst();

                m_bAllowPaymentsClosedClaims = p_bAllowPaymentsClosedClaims;

                // Get the XML for m_objFunds.
                this.GetXMLForTransaction();

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPreviousTransaction.ErrorPrev", m_iClientId), p_objEx);
            }
            return (m_objDocument);

        }

        /// <summary>
        /// Get Next Transaction 
        /// </summary>
        /// <param name="p_iTransId">TransID</param>
        /// <param name="p_bAllowPaymentsClosedClaims">AllowPaymentsClosedClaims Flag</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetNextTransaction(int p_iTransId, bool p_bAllowPaymentsClosedClaims)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (p_iTransId > 0)
                {
                    m_objFunds.MoveTo(p_iTransId);
                    m_objFunds.MoveNext();
                }
                else
                    m_objFunds.MoveLast();

                m_bAllowPaymentsClosedClaims = p_bAllowPaymentsClosedClaims;

                // Get the XML for m_objFunds.
                this.GetXMLForTransaction();

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetNextTransaction.ErrorNext", m_iClientId), p_objEx);
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get First Transaction
        /// </summary>
        /// <param name="p_bAllowPaymentsClosedClaims">AllowPaymentsClosedClaims Flag</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetFirstTransaction(bool p_bAllowPaymentsClosedClaims)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                m_objFunds.MoveFirst();

                m_bAllowPaymentsClosedClaims = p_bAllowPaymentsClosedClaims;

                // Get the XML for m_objFunds.
                this.GetXMLForTransaction();

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetFirstTransaction.ErrorFirst", m_iClientId), p_objEx);
            }
            return (m_objDocument);

        }

        /// <summary>
        /// Get Last Transaction
        /// </summary>
        /// <param name="p_bAllowPaymentsClosedClaims">AllowPaymentsClosedClaims Flag</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetLastTransaction(bool p_bAllowPaymentsClosedClaims)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                m_objFunds.MoveLast();

                m_bAllowPaymentsClosedClaims = p_bAllowPaymentsClosedClaims;

                // Get the XML for m_objFunds.
                this.GetXMLForTransaction();

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetLastTransaction.ErrorLast", m_iClientId), p_objEx);
            }
            return (m_objDocument);

        }

        /// <summary>
        /// Get Transaction Or Move To Transaction
        /// </summary>
        /// <param name="p_iTransId">TransID</param>
        /// <param name="p_bAllowPaymentsClosedClaims">AllowPaymentsClosedClaims Flag</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetTransaction(int p_iTransId, bool p_bAllowPaymentsClosedClaims)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                m_objFunds.MoveTo(p_iTransId);

                m_bAllowPaymentsClosedClaims = p_bAllowPaymentsClosedClaims;

                // Get the XML for m_objFunds.
                this.GetXMLForTransaction();

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetTransaction.ErrorGet", m_iClientId), p_objEx);
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get New Transaction for a claim
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_iClaimantEid">ClaimantEid</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_bIsCollection">IsCollection Flag</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetNewTransaction(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, bool p_bIsCollection)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                //MITS 12028 Call the custom script Init function
                m_objFunds.InitializeScriptData();

                this.InitializeFromClaimId(p_iClaimId, p_iClaimantEid, p_iUnitId, p_bIsCollection);

                if (p_bIsCollection)
                {
                    m_objFunds.CollectionFlag = true;
                    m_objFunds.PaymentFlag = false;
                }

                // Get the XML for m_objFunds.
                this.GetXMLForTransaction();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetNewTransaction.ErrorNew", m_iClientId), p_objEx);
            }
            return (m_objDocument);
        }

        /// <summary>
        /// Get New Transaction
        /// </summary>
        /// <param name="p_bIsCollection">IsCollection Flag</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument GetNewTransaction(bool p_bIsCollection)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (p_bIsCollection)
                {
                    m_objFunds.CollectionFlag = true;
                    m_objFunds.PaymentFlag = false;
                }

                // Get the XML for m_objFunds.
                this.GetXMLForTransaction();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetNewTransaction.ErrorNew", m_iClientId), p_objEx);
            }
            return (m_objDocument);
        }

        #endregion

        #region XML Manipulation Methods
        /// <summary>
        /// Get Value of control from XML element
        /// </summary>
        /// <param name="p_sFieldName">Field Name</param>
        /// <param name="p_objXMLNode">XML Node</param>
        /// <returns>Value string</returns>
        private string GetValueOf(string p_sFieldName, XmlElement p_objXMLNode)
        {
            XmlElement objTargetNode = null;
            string sValue = "";

            try
            {
                objTargetNode = (XmlElement)p_objXMLNode.SelectSingleNode("//control[@name='" + p_sFieldName + "']");
                if (objTargetNode != null)
                {
                    if (objTargetNode.GetAttribute("type") == "radio")
                    {
                        objTargetNode = (XmlElement)p_objXMLNode.SelectSingleNode("//control[@name='" + p_sFieldName + "' && @checked]");
                    }
                    if (objTargetNode != null)
                    {
                        sValue = this.GetValue(objTargetNode);
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetValueOf.ErrorValueOf", m_iClientId), p_objEx);
            }
            finally
            {
                objTargetNode = null;
            }
            return (sValue);
        }

        /// <summary>
        /// Get Node Value
        /// </summary>
        /// <param name="p_objXMLNode">XML Node</param>
        /// <returns>Value string</returns>
        private string GetValue(XmlElement p_objXMLNode)
        {
            string sType = "";
            string sName = "";
            string sValue = "";
            try
            {
                sType = p_objXMLNode.GetAttribute("type");
                sName = p_objXMLNode.GetAttribute("name");
                if (sType == CODE_FLDTYPE || sType == CODEDETAIL_FLDTYPE || sType == COMBOBOX_FLDTYPE || sType == EIDLOOKUP_FLDTYPE)
                {
                    sValue = p_objXMLNode.GetAttribute(ATT_CODEID);
                }
                else
                {
                    sValue = p_objXMLNode.InnerText;
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetValue.ErrorValue", m_iClientId), p_objEx);
            }
            return (sValue);
        }

        /// <summary>
        /// Get Value of the Node from XML document.
        /// </summary>
        /// <param name="p_objDocument">XML document</param>
        /// <param name="p_sNodeName">XML Node</param>
        /// <returns>Value string</returns>
        private string GetValue(XmlDocument p_objDocument, string p_sNodeName)
        {
            XmlElement objNode = null;
            string sValue = "";

            try
            {
                objNode = (XmlElement)p_objDocument.SelectSingleNode("//" + p_sNodeName);

                if (objNode != null)
                    sValue = objNode.InnerText;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetValue.ErrorValue", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }

            return (sValue);

        }

        /// <summary>
        /// Create a new Document from a Node
        /// </summary>
        /// <param name="p_objXMLNode">XML Node</param>
        /// <returns>XML Document</returns>
        private XmlDocument NodeToDocument(XmlElement p_objXMLNode)
        {
            XmlDocument objDocument = null;
            try
            {
                objDocument = new XmlDocument();
                objDocument.LoadXml(p_objXMLNode.OuterXml);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.NodeToDocument.ErrorNodeToDoc", m_iClientId), p_objEx);
            }
            return (objDocument);
        }

        /// <summary>
        /// Initialize the document
        /// </summary>
        /// <param name="p_objRootNode">Root Node</param>
        /// <param name="p_sRootNodeName">Root Node Name</param>
        private void StartDocument(ref XmlElement p_objRootNode, string p_sRootNodeName)
        {
            try
            {
                m_objDocument = new XmlDocument();
                p_objRootNode = m_objDocument.CreateElement(p_sRootNodeName);
                m_objDocument.AppendChild(p_objRootNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.StartDocument.ErrorDocInit", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Node Name</param>		
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            try
            {
                objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateElement.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objChildNode = null;
            }

        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_objChildNode">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateElement.Error", m_iClientId), p_objEx);
            }

        }

        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="sText">Text</param>		
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateAndSetElement.Error", m_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateAndSetElement.Error", m_iClientId), p_objEx);
            }
        }

        #endregion

        #region Update Funds Object For Payment Collection Data
        /// <summary>
        /// Update the Funds Object using SplitXML document.
        /// </summary>
        /// <param name="p_objSplitXML">
        /// XML Document contains Split information for each payment collection. 		
        /// </param>
        private void UpdateFundsObjectForPaymentCollectionData(XmlDocument p_objSplitXML)
        {
            XmlElement objFundsTransSplitXMLNode = null;
            FundsTransSplit objFundsTransSplit = null;

            int iCountFundsTransSplitObjects = 0;
            int iSplitRowId = 0;
            int iIndex = 0;
            string sSrc = "";

            try
            {
                // Logic to ensure that Any FundsTransSplit object NOT IN  the Session 
                // Cache gets DELETED in the memory copy of our Funds OBJECT.				
                foreach (FundsTransSplit objTempFundsTransSplit in m_objFunds.TransSplitList)
                {
                    objFundsTransSplitXMLNode = this.GetFundsTransSplitObjectBySplitRowId(p_objSplitXML, objTempFundsTransSplit.SplitRowId);
                    if (objFundsTransSplitXMLNode == null)
                        m_arrlstDeletedSplitIds.Add(objTempFundsTransSplit.SplitRowId);
                }

                iCountFundsTransSplitObjects = this.CountFundsTransSplitObjects(p_objSplitXML);

                //	Logic to ensure that Any FundsTransSplit object known to the Session Cache gets represented 
                //	in the in the memory copy of our Funds OBJECT.

                for (iIndex = 0; iIndex < iCountFundsTransSplitObjects; iIndex++)
                {
                    objFundsTransSplit = null;

                    objFundsTransSplitXMLNode = this.GetFundsTransSplitObjectByIndex(p_objSplitXML, iIndex);

                    sSrc = objFundsTransSplitXMLNode.GetAttribute("src");
                    if (objFundsTransSplitXMLNode.GetAttribute("dirty") == "true")
                        m_bDataChanged = true;
                    else
                        m_bDataChanged = false;

                    //	FundsTransSplit object came from the Database. Use existing
                    if (sSrc == "db")
                    {
                        iSplitRowId = Common.Conversion.ConvertStrToInteger(this.GetValueOf("splitrowid", this.NodeToDocument(objFundsTransSplitXMLNode).DocumentElement));
                        objFundsTransSplit = m_objFunds.TransSplitList[iSplitRowId];
                        if (objFundsTransSplit != null)
                            this.FillFundsTransSplitObject(ref objFundsTransSplit, this.NodeToDocument(objFundsTransSplitXMLNode));
                    }
                    // FundsTransSplit object is not in database. Create New
                    else
                    {
                        objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
                        this.FillFundsTransSplitObject(ref objFundsTransSplit, this.NodeToDocument(objFundsTransSplitXMLNode));
                        m_objFunds.TransSplitList.Add(objFundsTransSplit);
                    }

                    //MITS 12028 Set FundsTransSplit object's flag for custom script
                    objFundsTransSplit.FiringScriptFlag = m_objFunds.FiringScriptFlag;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.UpdateFundsObjectForPaymentCollectionData.ErrorUpdate", m_iClientId), p_objEx);
            }
            finally
            {
                objFundsTransSplitXMLNode = null;
                objFundsTransSplit = null;
            }
        }

        /// <summary>
        /// Update Funds.FundsTransSplitList.FundsTransSplit Object using Split node.
        /// </summary>
        /// <param name="p_objFundsTransSplit">FundsTransSplit object</param>
        /// <param name="p_objSplitXML">
        /// XML Document contains Split information for a payment collection. 		
        /// </param>
        private void FillFundsTransSplitObject(ref FundsTransSplit p_objFundsTransSplit, XmlDocument p_objXMLDoc)
        {
            XmlElement objElement = null;

            bool bIsBRS = false;

            string sTransId = "";
            string sTransTypeCode = "";
            string sAmount = "";
            string sGlAccountCode = "";
            string sFromDate = "";
            string sToDate = "";
            string sInvoicedBy = "";
            string sInvoiceNumber = "";
            string sInvoiceAmount = "";
            string sInvoiceDate = "";
            string sPoNumber = "";
            //MITS:34082 MultiCurrency START
            string sSplitCurrCode = "";
            string sSplitCurrAmount = "";
            string sSplitExchangeRate = "";
            //MITS:34082 MultiCurrency END

            try
            {
                objElement = (XmlElement)p_objXMLDoc.DocumentElement;
                if (objElement.GetAttribute("src") == "brs")
                {
                    if (this.IsBRSInstalled())
                    {
                        this.FillFundsTransSplitObjectForBRS(ref p_objFundsTransSplit, p_objXMLDoc);
                        bIsBRS = true;
                    }
                    else
                        p_objFundsTransSplit.Crc = 1234567890;
                }

                if (!bIsBRS)
                {
                    // Split Row id will not set here....
                    //modified the Xpath of various nodes..

                    sTransId = this.GetValueOf("transid", objElement);
                    sTransTypeCode = this.GetValueOf("transtypecode", objElement);

                    //sTransTypeCode = objElement.SelectSingleNode("//transtypecode").Attributes.ToString(); 
                    sAmount = this.GetValueOf("split_amount", objElement);
                    sGlAccountCode = this.GetValueOf("glaccountcode", objElement);
                    sFromDate = this.GetValueOf("fromdate", objElement);
                    sToDate = this.GetValueOf("todate", objElement);
                    sInvoicedBy = this.GetValueOf("invoicedby", objElement);
                    sInvoiceNumber = this.GetValueOf("invoicenumber", objElement);
                    sInvoiceAmount = this.GetValueOf("invoiceamount", objElement);
                    sInvoiceDate = this.GetValueOf("invoicedate", objElement);
                    sPoNumber = this.GetValueOf("ponumber", objElement);
                    //MITS:34082 MultiCurrency START
                    sSplitCurrCode = this.GetValueOf("SplitCurrCode", objElement);
                    sSplitCurrAmount = this.GetValueOf("SplitCurrAmount", objElement);
                    sSplitExchangeRate = this.GetValueOf("SplitToPaymentCurrRate", objElement);
                    //MITS:34082 MultiCurrency END

                    p_objFundsTransSplit.TransId = Common.Conversion.ConvertStrToInteger(sTransId);
                    p_objFundsTransSplit.TransTypeCode = Common.Conversion.ConvertStrToInteger(sTransTypeCode);
                    p_objFundsTransSplit.ReserveTypeCode = this.GetReserveTypeCode(p_objFundsTransSplit.TransTypeCode);
                    p_objFundsTransSplit.Amount = Common.Conversion.ConvertStrToDouble(sAmount);
                    p_objFundsTransSplit.GlAccountCode = Common.Conversion.ConvertStrToInteger(sGlAccountCode);
                    p_objFundsTransSplit.FromDate = sFromDate;
                    p_objFundsTransSplit.ToDate = sToDate;
                    p_objFundsTransSplit.InvoicedBy = sInvoicedBy;
                    p_objFundsTransSplit.InvoiceNumber = sInvoiceNumber;
                    p_objFundsTransSplit.InvoiceAmount = Common.Conversion.ConvertStrToDouble(sInvoiceAmount);
                    p_objFundsTransSplit.InvoiceDate = sInvoiceDate;
                    p_objFundsTransSplit.PoNumber = sPoNumber;
                    //MITS:34082 MultiCurrency START
                    double dExchRate = Common.Conversion.ConvertStrToDouble(sSplitExchangeRate);
                    p_objFundsTransSplit.SplitCurrCode = Common.Conversion.ConvertStrToInteger(sSplitCurrCode);
                    double dExcRateSplitToClaim = CommonFunctions.ExchangeRateSrc2Dest(p_objFundsTransSplit.SplitCurrCode, iClaimCurrCode, p_objFundsTransSplit.Context.DbConn.ConnectionString, m_iClientId);
                    double dExcRateSplitToBase = CommonFunctions.ExchangeRateSrc2Dest(p_objFundsTransSplit.SplitCurrCode, p_objFundsTransSplit.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsTransSplit.Context.DbConn.ConnectionString, m_iClientId);
                    double dExcRateBaseToSplit = CommonFunctions.ExchangeRateSrc2Dest(p_objFundsTransSplit.Context.InternalSettings.SysSettings.BaseCurrencyType, p_objFundsTransSplit.SplitCurrCode, p_objFundsTransSplit.Context.DbConn.ConnectionString, m_iClientId);
                    p_objFundsTransSplit.SplitToBaseCurrRate = dExcRateSplitToBase;
                    p_objFundsTransSplit.SplitToPaymentCurrRate = dExchRate;
                    p_objFundsTransSplit.SplitCurrAmount = Common.Conversion.ConvertStrToDouble(sSplitCurrAmount);
                    p_objFundsTransSplit.BaseToSplitCurrRate = p_objFundsTransSplit.SplitCurrAmount * dExcRateBaseToSplit;
                    p_objFundsTransSplit.SplitCurrencyInvAmount = Common.Conversion.ConvertStrToDouble(sInvoiceAmount);
                    //MITS:34082 MultiCurrency END
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.FillFundsTransSplitObject.ErrorUpdate", m_iClientId), p_objEx);
            }
            finally
            {
                objElement = null;
            }
        }

        /// <summary>
        /// Update Funds.FundsTransSplitList.FundsTransSplit( BRS ) Object using Split node.
        /// </summary>
        /// <param name="p_objFundsTransSplit">FundsTransSplit object</param>
        /// <param name="p_objSplitXML">
        /// XML Document contains Split information for a payment collection. 		
        /// </param>
        //		private void FillFundsTransSplitObjectForBRS( FundsTransSplit p_objFundsTransSplit , XmlDocument p_objXMLDoc )
        //		{			
        //			FundsTransSplit objFundsTransSplit = null ;
        //
        //			bool bForceAddToSplitList = false ;
        //
        //			try
        //			{
        //				if( this.IsBRSInstalled() )
        //				{
        //					try
        //					{
        //						m_arrlstDeletedSplitIds.Add( p_objFundsTransSplit.SplitRowId );					
        //						bForceAddToSplitList = true ;
        //					}
        //					catch
        //					{
        //						bForceAddToSplitList = false ;
        //					}
        //					//**************TODO BRS object need to be created ***********************************//
        //					// *************************************************************************//			
        //					//BRSLIB.CManager objBrsLib = new BRSLIB.CManager();
        //					//BRSItemType objBrsItem = objBrsLib.fillBRSItem( p_objXMLDoc , objBrsItem );
        //					//objFundsTransSplit = objBrsItem.objSplit ;
        //					//**************************************************************************//
        //					objFundsTransSplit.ReserveTypeCode = this.GetReserveTypeCode( objFundsTransSplit.TransTypeCode );
        //					objFundsTransSplit.Crc = 1234567890 ;
        //
        //					if( bForceAddToSplitList )
        //						m_objFunds.TransSplitList.Add( objFundsTransSplit );
        //				}
        //			}
        //			catch( DataModelException p_objEx )
        //			{
        //				throw p_objEx ;
        //			}
        //			catch( RMAppException p_objEx )
        //			{
        //				throw p_objEx ;
        //			}
        //			catch( Exception p_objEx )
        //			{
        //				throw new RMAppException(Globalization.GetString("FundManager.FillFundsTransSplitObjectForBRS.ErrorUpdateBRS") , p_objEx );				
        //			}
        //			finally
        //			{				
        //				objFundsTransSplit = null ;
        //			}
        //		}
        //		
        private void FillFundsTransSplitObjectForBRS(ref FundsTransSplit p_objFundsTransSplit, XmlDocument p_objXMLDoc)
        {

            bool bForceAddToSplitList = false;

            try
            {
                if (this.IsBRSInstalled())
                {
                    try
                    {
                        if (p_objFundsTransSplit.SplitRowId == 0)
                        {
                            bForceAddToSplitList = false;
                        }
                        else
                        {
                            m_arrlstDeletedSplitIds.Add(p_objFundsTransSplit.SplitRowId);
                            bForceAddToSplitList = true;
                        }
                    }
                    catch
                    {
                        bForceAddToSplitList = false;
                    }

                    //**************TODO BRS object need to be created ***********************************//
                    // *************************************************************************//			
                    //BRSLIB.CManager objBrsLib = new BRSLIB.CManager();
                    //BRSItemType objBrsItem = objBrsLib.fillBRSItem( p_objXMLDoc , objBrsItem );
                    //objFundsTransSplit = objBrsItem.objSplit ;
                    //**************************************************************************//
                    /*
                     * BRS implementation-Tanuj
                     * Existing(Funds) Datamodel factory object can be used to instantiate BRS Manager/BRS.->TO DO for Tanuj 
                     * */
                    Manager objBRSMgr = null;
                    BillItem objBillItem = null;
                    try
                    {

                        objBRSMgr = new Manager(m_sDsnName, m_sUserName, m_sPassword, ref this.m_objDataModelFactory, m_iClientId);
                        objBillItem = new BillItem(objBRSMgr.Common, m_iClientId);
                        objBRSMgr.FillBRSItem(ref p_objXMLDoc, ref objBillItem);
                        p_objFundsTransSplit = objBillItem.objSplit;
                        p_objFundsTransSplit.TransId = Conversion.ConvertObjToInt(p_objXMLDoc.SelectSingleNode("//control[@name='transid']").InnerText, m_iClientId);

                    }
                    catch (Exception p_objErr)
                    {
                        throw p_objErr;
                    }
                    finally
                    {
                        /*Alert!!
                         * Dont call dispose of BRS manager here as it will dispose off the Datamodel factory object as well.
                         * 
                         * */
                        //Followiing methods need to be called.Tanuj
                        //objBRSMgr.Dispose();
                        //objBillItem.Dispose();
                        //objBRSMgr=null;
                        //objBillItem=null;

                    }
                    p_objFundsTransSplit.ReserveTypeCode = this.GetReserveTypeCode(p_objFundsTransSplit.TransTypeCode);
                    p_objFundsTransSplit.Crc = 1234567890;



                    if (bForceAddToSplitList)
                        m_objFunds.TransSplitList.Add(p_objFundsTransSplit);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.FillFundsTransSplitObjectForBRS.ErrorUpdateBRS", m_iClientId), p_objEx);
            }
            finally
            {

            }
        }

        /// <summary>
        /// Count Total Split Node in Split XML Document.
        /// </summary>
        /// <param name="p_objSplitXML">
        /// XML Document contains Split information for each payment collection. 		
        /// </param>
        /// <returns>Number of Split Object</returns>
        private int CountFundsTransSplitObjects(XmlDocument p_objSplitXML)
        {
            XmlElement objRootNode = null;
            int iChildNodes = 0;

            try
            {
                objRootNode = (XmlElement)p_objSplitXML.SelectSingleNode("//billitems");
                if (objRootNode != null)
                    iChildNodes = objRootNode.ChildNodes.Count;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CountFundsTransSplitObjects.ErrorCount", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
            }
            return (iChildNodes);
        }

        /// <summary>
        /// Get FundsTransSplit Node of a particular index from Split XML Document.
        /// </summary>
        /// <param name="p_objSplitXML">
        /// XML Document contains Split information for each payment collection. 		
        /// </param>
        /// <param name="p_iIndex">Index</param>
        /// <returns>Split Node</returns>
        private XmlElement GetFundsTransSplitObjectByIndex(XmlDocument p_objSplitXML, int p_iIndex)
        {
            XmlElement objXMLNode = null;
            XmlNodeList objXMLNodeList = null;
            try
            {
                objXMLNodeList = p_objSplitXML.SelectNodes("/billitems/form");
                objXMLNode = (XmlElement)objXMLNodeList[p_iIndex];
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetFundsTransSplitObjectByIndex.ErrorByIndex", m_iClientId), p_objEx);
            }
            finally
            {
                objXMLNodeList = null;
            }
            return (objXMLNode);
        }

        /// <summary>
        /// Get FundsTransSplit Node of a particular SplitID from Split XML Document.
        /// </summary>
        /// <param name="p_objSplitXML">
        /// XML Document contains Split information for each payment collection. 		
        /// </param>
        /// <param name="p_iSplitRowId">Split Id</param>
        /// <returns>Split Node</returns>
        private XmlElement GetFundsTransSplitObjectBySplitRowId(XmlDocument p_objSplitXML, int p_iSplitRowId)
        {
            XmlElement objXMLNode = null;

            try
            {
                objXMLNode = (XmlElement)p_objSplitXML.SelectSingleNode("//form/group/control[@name='splitrowid' and .='" + p_iSplitRowId.ToString() + "']");
                if (objXMLNode != null)
                    objXMLNode = (XmlElement)objXMLNode.ParentNode.ParentNode.CloneNode(true);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetFundsTransSplitObjectBySplitRowId.ErrorSplitID", m_iClientId), p_objEx);
            }
            return (objXMLNode);
        }


        #endregion

        #region Update Funds Object For Transaction Data
        /// <summary>
        /// Update the Funds Object using Transaction Data XML Document.
        /// </summary>
        /// <param name="p_objTransactionDataXML">Transaction Data XML Document</param>
        private void UpdateFundsObjectForTransactionData(XmlDocument p_objTransactionDataXML)
        {
            SysSettings objSysSetting = null;

            bool bHasVoidPermission = false;
            bool bVoidFlag = false;
            bool bHasCheckClearedPermission = false;
            bool bClearedFlag = false;
            //pmahli BRS 11/7/2007 - Start 
            bool bHasCheckResubmitPermission = false;
            bool bResubmitFlag = false;
            //pmahli BRS 11/7/2007 - End
            bool bCorvOnly = false;
            bool bCollectionFlag = false;
            bool bEnclosureFlag = false;
            bool bAutoCheckFlag = false;
            bool bSettlementFlag = false;
            int iSubAccountId = 0;
            int iAccountId = 0;
            int iClaimId = 0;
            int iClaimantEID = 0;
            int iStatusCode = 0;
            int iPayeeEid = 0;
            int iClaimantUnitId = 0;
            string sDetail = "";
            string sCtlNumber = "";
            string sTransDate = "";
            string sPayeeTypeCode = "";
            string sLastName = "";
            string sFirstName = "";
            string sTaxId = "";
            string sAddr1 = "";
            string sAddr2 = "";
            string sAddr3 = "";
            string sAddr4 = "";
            string sCity = "";
            string sStateId = "";
            string sZipCode = "";
            string sCheckMemo = "";
            string sCheckNumber = "";
            string sDateOfCheck = "";
            string sNotes = "";
            //MITS-19970 02/03/2009 Shameem Void Reason
            string sVoidReason = string.Empty;
            //skhare7 R8 enhancement Combined Payment
            bool bCombinedPayFlag = false;
            //skhare7 R8 enhancement Combined Payment End
            try
            {
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

                bHasVoidPermission = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "HasVoidPermission"));
                bVoidFlag = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "CheckVoid"));
                bHasCheckClearedPermission = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "HasCheckClearedPermission"));
                bClearedFlag = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "CheckCleared"));
                //pmahli BRS 11/7/2007
                //getting value of resubmit checkbox
                bHasCheckResubmitPermission = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "HasCheckResubmitPermission"));
                bResubmitFlag = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "CheckResubmit"));
                //pmahli BRS 11/7/2007
                bCorvOnly = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "CorvOnly"));
                bCollectionFlag = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "CollectionFlag"));
                bEnclosureFlag = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "Enclosure"));
                bAutoCheckFlag = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "AutoCheck"));
                bSettlementFlag = Conversion.ConvertStrToBool(this.GetValue(p_objTransactionDataXML, "Settlement"));

                iAccountId = Common.Conversion.ConvertStrToInteger(this.GetValue(p_objTransactionDataXML, "AccountId"));
                if (m_objDataModelFactory.Context.InternalSettings.SysSettings.UseFundsSubAcc)
                {
                    iSubAccountId = iAccountId;
                    BankAccSub objBankAccSub = (BankAccSub)m_objDataModelFactory.GetDataModelObject("BankAccSub", false);
                    objBankAccSub.MoveTo(iSubAccountId);
                    iAccountId = objBankAccSub.AccountId;
                }

                iClaimId = Common.Conversion.ConvertStrToInteger(this.GetValue(p_objTransactionDataXML, "ClaimId"));
                iClaimantEID = Common.Conversion.ConvertStrToInteger(this.GetValue(p_objTransactionDataXML, "Claimant"));
                if (this.GetValue(p_objTransactionDataXML, "ClaimantUnitId").ToString() != "")
                {
                    iClaimantUnitId = Common.Conversion.ConvertStrToInteger((this.GetValue(p_objTransactionDataXML, "ClaimantUnitId")).Substring(1));
                    sDetail = this.GetValue(p_objTransactionDataXML, "ClaimantUnitId").Substring(0, 1);
                }

                //iStatusCode = Common.Conversion.ConvertStrToInteger( this.GetValue( p_objTransactionDataXML , "StateCid" ) );
                iStatusCode = Common.Conversion.ConvertStrToInteger(this.GetValue(p_objTransactionDataXML, "StatusCode"));
                iPayeeEid = Common.Conversion.ConvertStrToInteger(this.GetValue(p_objTransactionDataXML, "PayeeId"));

                //sDetail = this.GetValue( p_objTransactionDataXML , "Detail" );
                sCtlNumber = this.GetValue(p_objTransactionDataXML, "ControlNumber");
                sTransDate = this.GetValue(p_objTransactionDataXML, "TransDate");
                sPayeeTypeCode = this.GetValue(p_objTransactionDataXML, "PayeeType");
                sLastName = this.GetValue(p_objTransactionDataXML, "LastName");
                sFirstName = this.GetValue(p_objTransactionDataXML, "FirstName");
                sTaxId = this.GetValue(p_objTransactionDataXML, "TaxId");
                sAddr1 = this.GetValue(p_objTransactionDataXML, "Addr1");
                sAddr2 = this.GetValue(p_objTransactionDataXML, "Addr2");
                sAddr3 = this.GetValue(p_objTransactionDataXML, "Addr3");
                sAddr4 = this.GetValue(p_objTransactionDataXML, "Addr4");
                sCity = this.GetValue(p_objTransactionDataXML, "City");
                sStateId = this.GetValue(p_objTransactionDataXML, "StateCid");
                sZipCode = this.GetValue(p_objTransactionDataXML, "Zip");
                sCheckMemo = this.GetValue(p_objTransactionDataXML, "CheckMemo");
                sCheckNumber = this.GetValue(p_objTransactionDataXML, "CheckNumber");
                sDateOfCheck = this.GetValue(p_objTransactionDataXML, "CheckDate");
                sNotes = this.GetValue(p_objTransactionDataXML, "Notes");
                //MITS-19970 02/03/2009 Shameem Void Reason
                sVoidReason = this.GetValue(p_objTransactionDataXML, "VoidReason");

                //pmittal5 Mits 14250 01/20/09 - Make Transaction Date equal to Check Date on Printing
                if (iStatusCode == 1054)
                {
                    DateTime dTransDate, dDateOfCheck;
                    dTransDate = DateTime.Parse(sTransDate);
                    dDateOfCheck = DateTime.Parse(sDateOfCheck);
                    if (dTransDate > DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy")) && dTransDate > dDateOfCheck)
                        sTransDate = sDateOfCheck;
                }
                //End - pmittal5

                if (bHasVoidPermission)
                {
                    if (bVoidFlag && (!m_objFunds.VoidFlag))
                        m_objFunds.VoidDate = Conversion.ToDbDate(System.DateTime.Now);
                    m_objFunds.VoidFlag = bVoidFlag;
                }

                if (bHasCheckClearedPermission)
                    m_objFunds.ClearedFlag = bClearedFlag;

                //pmahli BRS 11/7/2007 - Start
                m_objFunds.ResubmitFlag = bResubmitFlag;
                //pmahli BRS 11/7/2007 - End
                //Manoj - for allowing to change the LSS payment status
                if (iStatusCode != 0)
                    m_objFunds.StatusCode = iStatusCode;

                if (!bCorvOnly)
                {
                    if (m_objFunds.TransId == 0)
                    {
                        if (bCollectionFlag)
                        {
                            m_objFunds.CollectionFlag = true;
                            m_objFunds.PaymentFlag = false;
                            m_objFunds.StatusCode = 1054; // Printed status code
                        }
                    }
                    if (sCtlNumber != "")
                        m_objFunds.CtlNumber = sCtlNumber;

                    if (sTransDate != "")
                        m_objFunds.TransDate = sTransDate;

                    //Modified code to set correct AccountId and SubAccountId in the database
                    if (objSysSetting.UseFundsSubAcc)
                    {
                        m_objFunds.SubAccountId = iSubAccountId;
                        m_objFunds.AccountId = iAccountId;
                    }
                    else
                        //m_objFunds.AccountId = iSubAccountId;
                        m_objFunds.AccountId = iAccountId;

                    m_objFunds.ClaimId = iClaimId;

                    if (this.ReserveTracking() != 0 || m_objFunds.LineOfBusCode == 243 || m_objFunds.LineOfBusCode == 844)
                    {
                        if (m_objFunds.LineOfBusCode == 242)
                        {

                            if (sDetail != "")
                            {
                                if (sDetail.Substring(0, 1).ToUpper() == "U")
                                {
                                    m_objFunds.UnitId = iClaimantUnitId;
                                    m_objFunds.ClaimantEid = 0;
                                }
                                else
                                {
                                    m_objFunds.ClaimantEid = iClaimantUnitId;
                                    m_objFunds.UnitId = 0;
                                }
                            }
                            else
                                m_objFunds.ClaimantEid = iClaimantUnitId;
                        }
                        else
                            m_objFunds.ClaimantEid = iClaimantUnitId;
                    }

                    m_objFunds.PayeeEid = iPayeeEid;

                    if (Common.Utilities.IsNumeric(sPayeeTypeCode))
                        m_objFunds.PayeeTypeCode = Conversion.ConvertStrToInteger(sPayeeTypeCode);
                    else
                        if ((sPayeeTypeCode != null && sPayeeTypeCode.Trim() != "") && (sPayeeTypeCode.Substring(0, 1) == "e" || sPayeeTypeCode.Substring(0, 1) == "o"))
                        {
                            m_objFunds.PayeeTypeCode = Conversion.ConvertStrToInteger(sPayeeTypeCode.Substring(1));
                            m_sPayeeType = sPayeeTypeCode.Substring(0, 1);
                        }
                    m_objFunds.LastName = sLastName;
                    m_objFunds.FirstName = sFirstName;

                    if (sTaxId != "")
                        m_objFunds.PayeeEntity.TaxId = sTaxId;

                    m_objFunds.Addr1 = sAddr1;
                    m_objFunds.Addr2 = sAddr2;
                    m_objFunds.Addr3 = sAddr3;
                    m_objFunds.Addr4 = sAddr4;
                    m_objFunds.City = sCity;

                    if (sStateId != "" && Common.Utilities.IsNumeric(sStateId))
                        m_objFunds.StateId = Common.Conversion.ConvertStrToInteger(sStateId);
                    else
                        m_objFunds.StateId = 0;

                    m_objFunds.ZipCode = sZipCode;
                    m_objFunds.EnclosureFlag = bEnclosureFlag;
                    m_objFunds.AutoCheckFlag = bAutoCheckFlag;
                    m_objFunds.SettlementFlag = bSettlementFlag;
                    m_objFunds.CheckMemo = sCheckMemo;

                    if (sCheckNumber != "" && Common.Conversion.IsNumeric(sCheckNumber))
                        m_objFunds.TransNumber = Conversion.ConvertStrToLong(sCheckNumber);
                    else
                        m_objFunds.TransNumber = 0;

                    if (iStatusCode != 0)
                        m_objFunds.StatusCode = iStatusCode;
                    else
                        if (m_objFunds.CollectionFlag)
                            m_objFunds.StatusCode = 479; //Released

                    m_objFunds.DateOfCheck = sDateOfCheck;
                    m_objFunds.Notes = sNotes;
                    //MITS-19970 02/03/2009 Shameem Void Reason
                    m_objFunds.VoidReason = sVoidReason;
                }

                //MITS 10420 Always allow user to save changes made to supplemental fields
                XmlDocument objSuppXMlDoc = new XmlDocument();
                objSuppXMlDoc.LoadXml(p_objTransactionDataXML.SelectSingleNode("//Supplementals").OuterXml);
                m_objFunds.Supplementals.PopulateObject(objSuppXMlDoc);
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.UpdateFundsObjectForTransactionData.ErrorUpdate", m_iClientId), p_objEx);
            }
            finally
            {
                objSysSetting = null;
            }
        }

        #endregion

        #region Out XML for m_objFunds

        /// <summary>
        /// Build the XML For funds object.
        /// </summary>
        private void GetXMLForTransaction()
        {
            UnitXClaimList objUnitXClaimList = null;
            ClaimantList objClaimantList = null;
            SysSettings objSysSetting = null;
            LocalCache objLocalCache = null;
            ArrayList arrlstAccountDetail = null;
            ArrayList arrlstPayeeType = null;

            XmlDocument objXml = null;//Umesh
            XmlElement objRootNode = null;
            XmlElement objTransSplitListNode = null;
            XmlElement objTransSplitNode = null;
            XmlElement objFundsNode = null;
            XmlElement objConfirmationsNode = null;
            XmlElement objErrorsNode = null;
            XmlElement objWarningsNode = null;
            XmlElement objAccountsNode = null;
            XmlElement objAccountNode = null;
            XmlElement objUnitsNode = null;
            XmlElement objUnitNode = null;
            XmlElement objClaimantsNode = null;
            XmlElement objClaimantNode = null;
            XmlElement objPayeeTypesNode = null;
            XmlElement objPayeeTypeNode = null;
            XmlElement objSupplementalNode = null; //Anurag
            XmlElement objSuppDefElement = null; //Anurag
            XmlElement objClaim_PrimaryClaimantNode = null;
            DbReader objRdr = null;//Umesh

            PayeeType structPayeeType;
            AccountDetail structAccountDetail;

            string sShortCode = "";
            string sDesc = "";
            int iIndex = 0;
            bool bAccountSelected = false;
            double dFundsTransSplitTotalAmount = 0;
            int iCounter = 1;


            try
            {
                #region Root Node
                // Add Root Node tag "Transaction" 
                this.StartDocument(ref objRootNode, "Transaction");
                #endregion

                #region TransSplit Node
                // Add "TransSplitList" node tag
                if (m_objFunds.TransSplitList.Count - m_arrlstDeletedSplitIds.Count > 0)
                {
                    CreateElement(objRootNode, "TransSplitList", ref objTransSplitListNode);
                    //adding code to set Count attribute to get total number of FundsTransSplit..modified by Raman Bhatia
                    objTransSplitListNode.SetAttribute("Count", m_objFunds.TransSplitList.Count.ToString());

                    // For each FundsTransSplit, add a new node tag "TransSplit".
                    //					foreach( FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList )
                    //					{
                    //						if( this.IsSplitItemDeleted( objFundsTransSplit.SplitRowId ) )
                    //							continue;
                    //						CreateElement( objTransSplitListNode , "TransSplit" , ref objTransSplitNode );
                    //						// Added code to find the total amounts for all FundTransSplit transactions..modified by Raman Bhatia				
                    //						dFundsTransSplitTotalAmount = dFundsTransSplitTotalAmount + objFundsTransSplit.Amount;
                    //                     
                    //						this.TransSplitXML( objTransSplitNode , objFundsTransSplit );
                    //					}
                    foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                    {

                        if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            continue;

                        // Added code to find the total amounts for all FundTransSplit transactions..modified by Raman Bhatia				

                        if (objFundsTransSplit.Crc != 1234567890)
                        {
                            CreateElement(objTransSplitListNode, "TransSplit", ref objTransSplitNode);
                            /*
                             * Addded "SplitType" to distinguish between various types of splits.
                             * i.e BRS splits, Regulat splits, T&E splits. Tanuj
                             * 
                             * */
                            objTransSplitNode.SetAttribute("SplitType", "RegularSplit");
                            this.TransSplitXML(objTransSplitNode, objFundsTransSplit);

                        }
                        else
                        {
                            if (objFundsTransSplit.Crc == 1234567890 && this.IsBRSInstalled())
                            {
                                CreateElement(objTransSplitListNode, "TransSplit", ref objTransSplitNode);
                                objTransSplitNode.SetAttribute("SplitType", "BRSSplit");
                                this.BRSTransSplitXML(objTransSplitNode, objFundsTransSplit);
                            }
                            else
                            {
                                continue;
                            }
                        }
                        dFundsTransSplitTotalAmount = dFundsTransSplitTotalAmount + objFundsTransSplit.Amount;
                    }
                    //adding code to set FundsTransSplitTotalAmount attribute to find the total amounts for all FundTransSplit transactions..modified by Raman Bhatia
                    objTransSplitListNode.SetAttribute("FundsTransSplitTotalAmount", dFundsTransSplitTotalAmount.ToString());
                }
                #endregion

                #region Funds Node
                // Add "Funds" node 
                CreateElement(objRootNode, "Funds", ref objFundsNode);
                this.FundsXML(objFundsNode);
                #endregion

                #region Confirmations Node
                // Add "Confirmations" node
                //if errors are reported then no need to send confirmations node.. code changed by raman bhatia
                if ((m_arrlstErrors.Count == 0) && (m_arrlstConfirmations.Count > 0))
                {
                    CreateElement(objRootNode, "Confirmations", ref objConfirmationsNode);
                    objConfirmationsNode.SetAttribute("Count", m_arrlstConfirmations.Count.ToString());
                    for (iIndex = 0; iIndex < m_arrlstConfirmations.Count; iIndex++)
                        this.CreateAndSetElement(objConfirmationsNode, "Confirmation", (string)m_arrlstConfirmations[iIndex]);
                }
                #endregion

                #region Errors Node
                // Add "Errors" node
                if (m_arrlstErrors.Count > 0)
                {
                    CreateElement(objRootNode, "Errors", ref objErrorsNode);
                    objErrorsNode.SetAttribute("Count", m_arrlstErrors.Count.ToString());
                    for (iIndex = 0; iIndex < m_arrlstErrors.Count; iIndex++)
                        this.CreateAndSetElement(objErrorsNode, "Error", (string)m_arrlstErrors[iIndex]);

                }
                #endregion

                #region Warnings Node
                // Add "Warnings" node
                //if confirmations or errors are reported then no need to send warnings node.. code changed by raman bhatia
                //if( m_arrlstWarnings.Count > 0 )
                if ((m_arrlstErrors.Count == 0) && (m_arrlstConfirmations.Count == 0) && (m_arrlstWarnings.Count > 0))
                {
                    CreateElement(objRootNode, "Warnings", ref objWarningsNode);
                    objWarningsNode.SetAttribute("Count", m_arrlstWarnings.Count.ToString());
                    for (iIndex = 0; iIndex < m_arrlstWarnings.Count; iIndex++)
                        this.CreateAndSetElement(objWarningsNode, "Warning", (string)m_arrlstWarnings[iIndex]);
                }
                #endregion

                #region Accounts Node
                // Add "Accounts" Node
                bool bAccountFound = false;
                arrlstAccountDetail = this.GetAccounts(true);
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
                if (arrlstAccountDetail.Count > 0)
                {
                    CreateElement(objRootNode, "Accounts", ref objAccountsNode);
                    objAccountsNode.SetAttribute("Count", arrlstAccountDetail.Count.ToString());
                    for (iIndex = 0; iIndex < arrlstAccountDetail.Count; iIndex++)
                    {
                        bAccountSelected = false;
                        structAccountDetail = (AccountDetail)arrlstAccountDetail[iIndex];
                        CreateElement(objAccountsNode, "Account", ref objAccountNode);
                        objAccountNode.SetAttribute("AccountName", structAccountDetail.AccountName);
                        objAccountNode.SetAttribute("AccountId", structAccountDetail.AccountId.ToString());
                        objAccountNode.SetAttribute("SubRowId", structAccountDetail.SubRowId.ToString());

                        if (objSysSetting.UseFundsSubAcc)
                        {
                            if (m_objFunds.SubAccountId == structAccountDetail.SubRowId)
                                bAccountSelected = true;
                        }
                        else
                        {
                            //raman has changed the logic of AccountId
                            //if( m_objFunds.AccountId == structAccountDetail.SubRowId )
                            if (m_objFunds.AccountId == structAccountDetail.AccountId)
                                bAccountSelected = true;
                        }

                        if (bAccountSelected)
                        {
                            objAccountNode.SetAttribute("Selected", true.ToString());
                            bAccountFound = true;
                        }

                    }
                }

                //MITS 9432 Add asigned bank account to the list if the account is
                // no longer effective, i.e. old bank account.
                if ((!bAccountFound) && (m_objFunds.TransId > 0))
                {
                    if (arrlstAccountDetail.Count == 0)
                    {
                        CreateElement(objRootNode, "Accounts", ref objAccountsNode);
                    }

                    if (addAssignedAccount(objSysSetting.UseFundsSubAcc, ref objAccountsNode))
                    {
                        objAccountsNode.SetAttribute("Count", (arrlstAccountDetail.Count + 1).ToString());
                    }
                }
                #endregion

                #region Units Node
                // Add "Units" Node
                objUnitXClaimList = this.GetUnits();
                if (objUnitXClaimList.Count > 0)
                {
                    CreateElement(objRootNode, "Units", ref objUnitsNode);
                    objUnitsNode.SetAttribute("Count", objUnitXClaimList.Count.ToString());
                    foreach (UnitXClaim objUnitXClaim in objUnitXClaimList)
                    {
                        CreateElement(objUnitsNode, "Unit", ref objUnitNode);
                        objUnitNode.SetAttribute("Id", objUnitXClaim.UnitId.ToString());
                        objUnitNode.SetAttribute("VehicleMake", objUnitXClaim.VehicleMake);
                        objUnitNode.SetAttribute("VehicleModel", objUnitXClaim.Vehicle.VehicleModel);
                        if (m_objFunds.UnitId == objUnitXClaim.UnitId)
                            objUnitNode.SetAttribute("Selected", true.ToString());
                    }

                }
                #endregion

                #region Claimants Node
                // Add "Claimants" Node
                objClaimantList = this.GetClaimants();
                if (objClaimantList.Count > 0)
                {
                    CreateElement(objRootNode, "Claimants", ref objClaimantsNode);
                    objClaimantsNode.SetAttribute("Count", objClaimantList.Count.ToString());
                    using (objLocalCache = new LocalCache(m_sConnectionString, m_iClientId))
                    {
                        foreach (Claimant objClaimant in objClaimantList)
                        {
                            objLocalCache.GetStateInfo(objClaimant.ClaimantEntity.StateId, ref sShortCode, ref sDesc);

                            CreateElement(objClaimantsNode, "Claimant", ref objClaimantNode);
                            objClaimantNode.SetAttribute("EId", objClaimant.ClaimantEid.ToString());
                            objClaimantNode.SetAttribute("LastName", objClaimant.ClaimantEntity.LastName);
                            objClaimantNode.SetAttribute("FirstName", objClaimant.ClaimantEntity.FirstName);
                            //added code to fetch TaxId,Address1,Address2,Address3,Address4 City,StateId,ShortCode,Desc,ZipCode,ClaimantSNo,Type for each claimant...Raman Bhatia
                            objClaimantNode.SetAttribute("TaxId", objClaimant.ClaimantEntity.TaxId);
                            objClaimantNode.SetAttribute("Address1", objClaimant.ClaimantEntity.Addr1);
                            objClaimantNode.SetAttribute("Address2", objClaimant.ClaimantEntity.Addr2);
                            objClaimantNode.SetAttribute("Address3", objClaimant.ClaimantEntity.Addr3);
                            objClaimantNode.SetAttribute("Address4", objClaimant.ClaimantEntity.Addr4);
                            objClaimantNode.SetAttribute("City", objClaimant.ClaimantEntity.City);
                            objClaimantNode.SetAttribute("StateId", objClaimant.ClaimantEntity.StateId.ToString());
                            objClaimantNode.SetAttribute("ShortCode", sShortCode.ToString());
                            objClaimantNode.SetAttribute("Desc", sDesc.ToString());
                            objClaimantNode.SetAttribute("ZipCode", objClaimant.ClaimantEntity.ZipCode);
                            objClaimantNode.SetAttribute("ClaimantSNo", iCounter++.ToString());
                            objClaimantNode.SetAttribute("Type", objLocalCache.GetTableName(objClaimant.ClaimantEntity.EntityTableId));
                            if (m_objFunds.ClaimantEid == objClaimant.ClaimantEid)
                                objClaimantNode.SetAttribute("Selected", true.ToString());
                        }
                    }
                }
                // MITS- 8658, Diary Changes
                string sClaim_PrimaryClaimant = string.Empty;
                if (m_objFunds.ClaimId > 0)
                {
                    Claim objClaim = null;
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(m_objFunds.ClaimId);
                    sClaim_PrimaryClaimant = objClaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                }
                else
                    sClaim_PrimaryClaimant = "";
                CreateAndSetElement(objRootNode, "Claim_PrimaryClaimant", sClaim_PrimaryClaimant);
                #endregion

                #region PayeeType Node
                // Add "PayeeType" Node 								
                arrlstPayeeType = this.GetPayeeTypes();
                if (arrlstPayeeType.Count > 0)
                {
                    //objSysSetting was being used without creation of the instance
                    objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

                    CreateElement(objRootNode, "PayeeTypes", ref objPayeeTypesNode);
                    objPayeeTypesNode.SetAttribute("Count", arrlstPayeeType.Count.ToString());
                    objPayeeTypesNode.SetAttribute("ShortCode", sShortCode);

                    using (objLocalCache = new LocalCache(m_sConnectionString, m_iClientId))
                    {
                        objLocalCache.GetCodeInfo(m_objFunds.PayeeTypeCode, ref sShortCode, ref sDesc);
                    }
                    if (sShortCode == "O")
                        objPayeeTypesNode.SetAttribute("Count", this.GetEntityGlossaryType(m_objFunds.PayeeEid).ToString());

                    for (iIndex = 0; iIndex < arrlstPayeeType.Count; iIndex++)
                    {
                        structPayeeType = (PayeeType)arrlstPayeeType[iIndex];

                        CreateElement(objPayeeTypesNode, "PayeeType", ref objPayeeTypeNode);
                        objPayeeTypeNode.SetAttribute("CodeId", structPayeeType.CodeId.ToString());
                        objPayeeTypeNode.SetAttribute("ShortCode", structPayeeType.ShortCode);
                        objPayeeTypeNode.SetAttribute("CodeDesc", structPayeeType.CodeDesc);
                    }
                }
                #endregion

                //Anurag: Date:30-Aug-2005 Adding support for supplementals as a Tab on Funds screen
                #region Supplementals

                CreateElement(objRootNode, "SuppDef", ref objSuppDefElement);
                CreateElement(objRootNode, "Supplementals", ref objSupplementalNode);
                //If m_objFunds has supplementals then send back a view section suitable to present them.
                //Adding Supplementals View Xml & Data XML
                if (m_objFunds.GetType().GetProperty("Supplementals") != null)
                {
                    XmlDocument objSuppXMLDoc = new XmlDocument();

                    Supplementals objSupp = (m_objFunds.GetProperty("Supplementals", new object[] { }) as Supplementals);
                    if (objSupp != null)
                    {
                        objSuppXMLDoc.LoadXml(objSupp.SerializeObject());
                        objSupplementalNode.InnerXml = objSuppXMLDoc.DocumentElement.FirstChild.InnerXml;
                        objSupplementalNode.SetAttribute("tablename", objSuppXMLDoc.DocumentElement.FirstChild.Attributes["tablename"].Value);
                        objSuppDefElement.InnerXml = objSupp.ViewXml.FirstChild.InnerXml;
                        foreach (XmlNode objNde in objSuppDefElement.GetElementsByTagName("control"))
                        {
                            if (((XmlElement)objNde).HasAttribute("ref"))
                            {
                                ((XmlElement)objNde).SetAttribute("ref", ((XmlElement)objNde).GetAttribute("ref").Replace("/Instance/*", "/Instance/"));
                            }
                        }
                    }
                }
                #endregion


                this.CreateAndSetElement(objRootNode, "IsReadOnly", this.IsReadOnly().ToString());
                this.CreateAndSetElement(objRootNode, "IsClaimFrozen", this.IsClaimFrozen().ToString());

                //Manoj - RMX-LSS i/f PCR#3
                this.CreateAndSetElement(objRootNode, "IsLSSPayment", this.IsLSSPayment().ToString());

                this.CreateAndSetElement(objRootNode, "IsClaimOpen", this.IsClaimOpen().ToString());
                this.CreateAndSetElement(objRootNode, "ReserveTracking", this.ReserveTracking().ToString());
                //raman has changed the output from string format to xml
                //this.CreateAndSetElement( objRootNode , "InsufAmntXml" , m_sInsufAmntXml );
                m_sInsufAmntXml = m_sInsufAmntXml.Replace('~', '<');
                m_sInsufAmntXml = m_sInsufAmntXml.Replace('|', '>');

                XmlElement objXmlElem = null;
                objXmlElem = objRootNode.OwnerDocument.CreateElement("InsufAmntXml");
                objXmlElem.InnerXml = m_sInsufAmntXml;
                objRootNode.AppendChild(objXmlElem);

                this.CreateAndSetElement(objRootNode, "UseSubAccounts", objSysSetting.UseFundsSubAcc.ToString());
                this.CreateAndSetElement(objRootNode, "DataChanged", m_bDataChanged.ToString());
                /// TR#2165 Checks for OFAC configuration				
                if (objSysSetting.P2Login == "" || objSysSetting.P2Pass == "" || objSysSetting.P2Url == "")
                    this.CreateAndSetElement(objRootNode, "OFAC", "False");
                else
                //  01/05/2007 REM Umesh
                {

                    objXml = new XmlDocument();
                    string strContent = RMSessionManager.GetCustomSettings(m_iClientId);//rkaur27
                    if (!string.IsNullOrEmpty(strContent))
                    {
                        objXml.LoadXml(strContent);

                        XmlNode objSearch = objXml.SelectSingleNode("//Funds/OFAC[@title='OFAC Check:']");
                        if (objSearch != null)
                        {
                            if (objSearch.InnerText == "-1")
                            {
                                this.CreateAndSetElement(objRootNode, "OFAC", "True");
                            }
                            else
                            {
                                this.CreateAndSetElement(objRootNode, "OFAC", "False");
                            }
                        }
                    }
                    else
                    {
                        this.CreateAndSetElement(objRootNode, "OFAC", "True");
                    }//else
                }// REM Umesh

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetXMLForTransaction.ErrorOutXML", m_iClientId), p_objEx);
            }
            finally
            {
                objUnitXClaimList = null;
                objClaimantList = null;
                objSysSetting = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                arrlstAccountDetail = null;
                arrlstPayeeType = null;

                objRootNode = null;
                objTransSplitListNode = null;
                objTransSplitNode = null;
                objFundsNode = null;
                objConfirmationsNode = null;
                objErrorsNode = null;
                objWarningsNode = null;
                objAccountsNode = null;
                objAccountNode = null;
                objUnitsNode = null;
                objUnitNode = null;
                objClaimantsNode = null;
                objClaimantNode = null;
                objPayeeTypesNode = null;
                objPayeeTypeNode = null;
                if (objRdr != null)
                    objRdr.Close();
                objXml = null;
            }
        }

        /// <summary>
        /// Build the XML for Funds Direct Properties.
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        private void FundsXML(XmlElement p_objParentNode)
        {
            LocalCache objLocalCache = null;
            XmlElement objStateNode = null;
            XmlElement objStatusNode = null;
            XmlElement objPayeeNode = null;
            XmlElement objPayeeEidNode = null;

            Claim objClaim = null;
            Event objEvent = null;
            int iDeptEID = 0;

            string sStateShortCode = "";
            string sStateDesc = "";
            string sStatusShortCode = "";
            string sStatusDesc = "";
            string sReleasedShortCode = "";
            string sReleasedDesc = "";
            string sPayeeTypeShortCode = "";
            string sPayeeTypeDesc = "";
            int iPrintedStatusCode = 0;

            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                objLocalCache.GetStateInfo(m_objFunds.StateId, ref sStateShortCode, ref sStateDesc);
                objLocalCache.GetCodeInfo(m_objFunds.StatusCode, ref sStatusShortCode, ref sStatusDesc);
                objLocalCache.GetCodeInfo(m_objFunds.PayeeTypeCode, ref sPayeeTypeShortCode, ref sPayeeTypeDesc);
                objLocalCache.GetCodeInfo(479, ref sReleasedShortCode, ref sReleasedDesc);
                iPrintedStatusCode = objLocalCache.GetCodeId("P", "CHECK_STATUS");


                this.CreateAndSetElement(p_objParentNode, "ClaimId", m_objFunds.ClaimId.ToString());
                this.CreateAndSetElement(p_objParentNode, "ClaimNumber", m_objFunds.ClaimNumber);
                this.CreateAndSetElement(p_objParentNode, "ReserveTracking", this.ReserveTracking().ToString());
                this.CreateAndSetElement(p_objParentNode, "TransDate", Conversion.GetDBDateFormat(m_objFunds.TransDate, "d"));
                XmlElement objElem = (XmlElement)p_objParentNode.SelectSingleNode("//TransDate");
                objElem.SetAttribute("currentdate", Conversion.GetDBDateFormat(Conversion.GetDate(DateTime.Now.Date.ToString()), "MM/dd/yyyy"));
                this.CreateAndSetElement(p_objParentNode, "CtlNumber", m_objFunds.CtlNumber);
                this.CreateAndSetElement(p_objParentNode, "VoidFlag", m_objFunds.VoidFlag.ToString());
                this.CreateAndSetElement(p_objParentNode, "ClearedFlag", m_objFunds.ClearedFlag.ToString());
                //pmahli BRS 11/7/2007 - Start 
                //setting value of Resubmit Flag from funds object
                this.CreateAndSetElement(p_objParentNode, "ResubmitFlag", m_objFunds.ResubmitFlag.ToString());
                //pmahli BRS 11/7/2007 - End
                //adding code to fetch Collection Flag, TransId, ZipCode ,LOB Code ,AccountId, SubAccountId -- modified by Raman Bhatia
                this.CreateAndSetElement(p_objParentNode, "IsCollection", m_objFunds.CollectionFlag.ToString());
                this.CreateAndSetElement(p_objParentNode, "TransId", m_objFunds.TransId.ToString());
                this.CreateAndSetElement(p_objParentNode, "ZipCode", m_objFunds.ZipCode.ToString());
                this.CreateAndSetElement(p_objParentNode, "LineOfBusinessCode", m_objFunds.LineOfBusCode.ToString());
                this.CreateAndSetElement(p_objParentNode, "AccountId", m_objFunds.AccountId.ToString());
                this.CreateAndSetElement(p_objParentNode, "SubAccountId", m_objFunds.SubAccountId.ToString());
                this.CreateAndSetElement(p_objParentNode, "PayeeTypeCode", m_objFunds.PayeeTypeCode.ToString(), ref objPayeeNode);
                objPayeeNode.SetAttribute("ShortCode", sPayeeTypeShortCode);
                objPayeeNode.SetAttribute("Desc", sPayeeTypeDesc);

                this.CreateAndSetElement(p_objParentNode, "LastName", m_objFunds.LastName);
                this.CreateAndSetElement(p_objParentNode, "FirstName", m_objFunds.FirstName);
                this.CreateAndSetElement(p_objParentNode, "PrecheckFlag", m_objFunds.PrecheckFlag.ToString());
                this.CreateAndSetElement(p_objParentNode, "PayeeEid", m_objFunds.PayeeEid.ToString(), ref objPayeeEidNode);
                objPayeeEidNode.SetAttribute("EntityGlossaryType", this.GetEntityGlossaryType(m_objFunds.PayeeEid).ToString());

                this.CreateAndSetElement(p_objParentNode, "PayeeEntityTaxId", m_objFunds.PayeeEntity.TaxId);
                this.CreateAndSetElement(p_objParentNode, "Addr1", m_objFunds.Addr1);
                this.CreateAndSetElement(p_objParentNode, "Addr2", m_objFunds.Addr2);
                this.CreateAndSetElement(p_objParentNode, "Addr3", m_objFunds.Addr3);
                this.CreateAndSetElement(p_objParentNode, "Addr4", m_objFunds.Addr4);
                this.CreateAndSetElement(p_objParentNode, "City", m_objFunds.City);

                // If new transaction, then set the check date to the present date. 
                if (m_objFunds.TransId != 0)
                    this.CreateAndSetElement(p_objParentNode, "DateOfCheck", Conversion.GetDBDateFormat(m_objFunds.DateOfCheck, "d"));
                else
                    this.CreateAndSetElement(p_objParentNode, "DateOfCheck", Conversion.GetDBDateFormat(Conversion.GetDate(DateTime.Now.Date.ToString()), "MM/dd/yyyy"));

                this.CreateAndSetElement(p_objParentNode, "TransNumber", m_objFunds.TransNumber.ToString());
                this.CreateAndSetElement(p_objParentNode, "CheckMemo", m_objFunds.CheckMemo);
                this.CreateAndSetElement(p_objParentNode, "ClaimantEid", m_objFunds.ClaimantEid.ToString());
                this.CreateAndSetElement(p_objParentNode, "EnclosureFlag", m_objFunds.EnclosureFlag.ToString());
                this.CreateAndSetElement(p_objParentNode, "AutoCheckFlag", m_objFunds.AutoCheckFlag.ToString());
                this.CreateAndSetElement(p_objParentNode, "SettlementFlag", m_objFunds.SettlementFlag.ToString());

                //raman has changed the format of codedata in the out XML
                //this.CreateAndSetElement( p_objParentNode , "StateId" , m_objFunds.StateId.ToString() , ref objStateNode );
                //objStateNode.SetAttribute( "ShortCode" , sStateShortCode );
                //objStateNode.SetAttribute( "Desc" , sStateDesc );									

                this.CreateAndSetElement(p_objParentNode, "StateId", (sStateShortCode + ' ' + sStateDesc), ref objStateNode);
                objStateNode.SetAttribute("codeid", m_objFunds.StateId.ToString());

                this.CreateAndSetElement(p_objParentNode, "StatusCode", m_objFunds.StatusCode.ToString(), ref objStatusNode);
                objStatusNode.SetAttribute("ShortCode", sStatusShortCode);
                objStatusNode.SetAttribute("Desc", sStatusDesc);
                objStatusNode.SetAttribute("ReleasedShortCode", sReleasedShortCode);
                objStatusNode.SetAttribute("ReleasedDesc", sReleasedDesc);
                objStatusNode.SetAttribute("PrintedStatusCode", iPrintedStatusCode.ToString());

                this.CreateAndSetElement(p_objParentNode, "UnitId", m_objFunds.UnitId.ToString());
                this.CreateAndSetElement(p_objParentNode, "Vehicle", this.GetVehicle(m_objFunds.UnitId));
                this.CreateAndSetElement(p_objParentNode, "ClaimantLastFirstName", this.GetLastFirstName(m_objFunds.ClaimantEid));
                this.CreateAndSetElement(p_objParentNode, "IsTandE", m_bIsTandE.ToString());
                this.CreateAndSetElement(p_objParentNode, "Notes", m_objFunds.Notes);
                this.CreateAndSetElement(p_objParentNode, "BatchNumber", m_objFunds.BatchNumber.ToString());

                // Vaibhav has added the IsBRSInstalled node in out XML.
                this.CreateAndSetElement(p_objParentNode, "IsBRSInstalled", this.IsBRSInstalled().ToString());

                // Raman has added the UseAcrosoftInterface node in out XML.
                this.CreateAndSetElement(p_objParentNode, "UseAcrosoftInterface", m_objDataModelFactory.Context.InternalSettings.SysSettings.UseAcrosoftInterface.ToString());

                // Vaibhav has added the SubBabkAccount node to out Xml.
                this.CreateAndSetElement(p_objParentNode, "UseFundsSubAcc", m_objDataModelFactory.Context.InternalSettings.SysSettings.UseFundsSubAcc.ToString());

                //MITS-19970 Added by Shameem for Void Reason-01/27/2009
                this.CreateAndSetElement(p_objParentNode, "VoidReason", m_objFunds.VoidReason);

                //raman has added the DeptEid node in out XML.
                //this.CreateAndSetElement(p_objParentNode, "DeptEid", m_objFunds.SecDeptEid.ToString());
                // ABhateja, 07.27.2007 -START-
                // MITS 10087,SecDeptEid no more exposed from datamodel.
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(m_objFunds.ClaimId);
                objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                if (objClaim.EventId > 0)
                    objEvent.MoveTo(objClaim.EventId);

                iDeptEID = objEvent.DeptEid;
                this.CreateAndSetElement(p_objParentNode, "DeptEid", iDeptEID.ToString());
                // ABhateja, 07.27.2007 -END-

                //raman has added code to fetch auto close id's related to bug 1308
                string sSQL = "SELECT TRANS_TYPE_CODE FROM SYS_TRANS_CLOSE WHERE LINE_OF_BUS_CODE=" + m_objFunds.LineOfBusCode;
                string sAutoCloseIds = "";
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        sAutoCloseIds = sAutoCloseIds + objReader.GetValue("TRANS_TYPE_CODE").ToString() + ",";
                    }
                    objReader.Close();
                }
                if (sAutoCloseIds.EndsWith(",") == true)
                {
                    sAutoCloseIds = sAutoCloseIds.Substring(0, (sAutoCloseIds.Length - 1));
                }
                this.CreateAndSetElement(p_objParentNode, "AutoCloseIds", sAutoCloseIds);

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.FundsXML.ErrorFunds", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objStateNode = null;
                objStatusNode = null;
                objPayeeNode = null;
                objPayeeEidNode = null;
            }
        }
        /// <summary>
        /// Build the Trans Split XML for a Funds Trans Split Object.
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_objFundsTransSplit">Funds Trans Split Object</param>		
        private void TransSplitXML(XmlElement p_objParentNode, FundsTransSplit p_objFundsTransSplit)
        {
            LocalCache objLocalCache = null;
            XmlElement objTransTypeNode = null;
            XmlElement objGlAccountNode = null;
            XmlElement objReserveTypeNode = null;

            string sTransTypeShortCode = "";
            string sTransTypeDesc = "";
            string sReserveTypeShortCode = "";
            string sReserveTypeDesc = "";
            string sGlAccountShortCode = "";
            string sGlAccountDesc = "";
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objLocalCache.GetCodeInfo(p_objFundsTransSplit.TransTypeCode, ref sTransTypeShortCode, ref sTransTypeDesc);
                objLocalCache.GetCodeInfo(p_objFundsTransSplit.ReserveTypeCode, ref sReserveTypeShortCode, ref sReserveTypeDesc);
                objLocalCache.GetCodeInfo(p_objFundsTransSplit.GlAccountCode, ref sGlAccountShortCode, ref sGlAccountDesc);

                this.CreateAndSetElement(p_objParentNode, "Crc", p_objFundsTransSplit.Crc.ToString());
                this.CreateAndSetElement(p_objParentNode, "TransId", p_objFundsTransSplit.TransId.ToString());
                this.CreateAndSetElement(p_objParentNode, "SplitRowId", p_objFundsTransSplit.SplitRowId.ToString());

                //raman has changed the format of codedata in the out XML
                /*
                this.CreateAndSetElement( p_objParentNode , "TransTypeCode" , p_objFundsTransSplit.TransTypeCode.ToString() , ref objTransTypeNode );
                objTransTypeNode.SetAttribute( "ShortCode" , sTransTypeShortCode );
                objTransTypeNode.SetAttribute( "Desc" , sTransTypeDesc );
                */

                this.CreateAndSetElement(p_objParentNode, "TransTypeCode", (sTransTypeShortCode + ' ' + sTransTypeDesc), ref objTransTypeNode);
                objTransTypeNode.SetAttribute("codeid", p_objFundsTransSplit.TransTypeCode.ToString());

                this.CreateAndSetElement(p_objParentNode, "Amount", p_objFundsTransSplit.Amount.ToString());

                /*
                this.CreateAndSetElement( p_objParentNode , "GlAccountCode" , p_objFundsTransSplit.GlAccountCode.ToString() , ref objGlAccountNode );
                objGlAccountNode.SetAttribute( "ShortCode" , sGlAccountShortCode );
                objGlAccountNode.SetAttribute( "Desc" , sGlAccountDesc );
                */

                this.CreateAndSetElement(p_objParentNode, "GlAccountCode", (sGlAccountShortCode + ' ' + sGlAccountDesc), ref objGlAccountNode);
                objGlAccountNode.SetAttribute("codeid", p_objFundsTransSplit.GlAccountCode.ToString());

                this.CreateAndSetElement(p_objParentNode, "FromDate", Conversion.GetDBDateFormat(p_objFundsTransSplit.FromDate, "d"));
                this.CreateAndSetElement(p_objParentNode, "ToDate", Conversion.GetDBDateFormat(p_objFundsTransSplit.ToDate, "d"));
                this.CreateAndSetElement(p_objParentNode, "InvoicedBy", p_objFundsTransSplit.InvoicedBy);
                this.CreateAndSetElement(p_objParentNode, "InvoiceNumber", p_objFundsTransSplit.InvoiceNumber);
                this.CreateAndSetElement(p_objParentNode, "InvoiceAmount", p_objFundsTransSplit.InvoiceAmount.ToString());
                this.CreateAndSetElement(p_objParentNode, "InvoiceDate", Conversion.GetDBDateFormat(p_objFundsTransSplit.InvoiceDate, "d"));
                this.CreateAndSetElement(p_objParentNode, "PoNumber", p_objFundsTransSplit.PoNumber);
                this.CreateAndSetElement(p_objParentNode, "DataChanged", p_objFundsTransSplit.DataChanged.ToString());

                /*
                this.CreateAndSetElement( p_objParentNode , "ReserveTypeCode" , p_objFundsTransSplit.ReserveTypeCode.ToString() , ref objReserveTypeNode );
                objReserveTypeNode.SetAttribute( "ShortCode" , sReserveTypeShortCode );
                objReserveTypeNode.SetAttribute( "Desc" , sReserveTypeDesc );				
                */

                this.CreateAndSetElement(p_objParentNode, "ReserveTypeCode", (sReserveTypeShortCode + ' ' + sReserveTypeDesc), ref objReserveTypeNode);
                objReserveTypeNode.SetAttribute("codeid", p_objFundsTransSplit.ReserveTypeCode.ToString());

                //raman has added code to fetch reserve balance
                double dReserveBalance = GetReserveBalance(m_objFunds.ClaimId, m_objFunds.ClaimantEid, m_objFunds.UnitId, p_objFundsTransSplit.ReserveTypeCode, m_objFunds.LineOfBusCode, m_objFunds.TransId);
                this.CreateAndSetElement(p_objParentNode, "ReserveBalance", dReserveBalance.ToString());
                this.CreateAndSetElement(p_objParentNode, "NewRecord", p_objFundsTransSplit.IsNew.ToString());
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.TransSplitXML.ErrorTransSplit", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objTransTypeNode = null;
                objGlAccountNode = null;
                objReserveTypeNode = null;
            }
        }

        /// <summary>
        /// Get the Accounts List.
        /// </summary>
        /// <param name="p_bSubAccount">Sub Account Flag</param>
        /// <param name="p_iParentAccount">Parent Account Id.</param>
        /// <returns>Account Array List</returns>
        public ArrayList GetAccounts(bool p_bSubAccount, int p_iParentAccount)
        {
            Claim objClaim = null;
            Event objEvent = null;
            Policy objPolicy = null;
            // Start Naresh Changes in funds for Enhanced Policy
            PolicyEnh objPolicyEnh = null;
            // End Naresh Changes in funds for Enhanced Policy
            DisabilityPlan objDisabilityPlan = null;
            SysSettings objSysSetting = null;
            DbReader objReader = null;
            DbReader objReaderCount = null;
            ArrayList arrlstAccountDetail = null;
            AccountDetail structAccountDetail;
            object objAccountId = null;


            string sSQL = "";
            bool bUseFundsSubAccounts = false;
            bool bFundsAccountLink = false;
            bool bFoundNextPolicy = false;
            int iPolicyId = 0;
            int iPolicyBankId = 0;
            int iPolicySubAccId = 0;
            int iPlanId = 0;
            int iPlanBankId = 0;
            int iPlanSubAccRowId = 0;
            int iDeptEID = 0;
            int iThisLob = 0;
            //int iOwnerEID = 0 ;
            //int iTableId = 0;
            //int iCount = 0 ;
            int iOrderCondition = 0;
            int iServiceCode = 0;
            bool bExit = false;
            string sEffTriger = "";
            string sTriggerFromDate = "";
            string sTriggerToDate = "";
            string sOrderBy = string.Empty;
            double dClaimTotal = 0;//Geeta 05/17/07 : Modified to fix 9097
            double dOccTotal = 0;
            int iLastPolicyID = 0;
            int iCoverageTypeCode = 0;
            LocalCache objLocalCache = null;
            //Start-Mridul Bansal. 01/12/10. MITS#18229.
            ColLobSettings objLOBSettings = null;
            //End-Mridul Bansal. 01/12/10. MITS#18229.
            int iPolLOBCode = 0;
            try
            {
                arrlstAccountDetail = new ArrayList();
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                bUseFundsSubAccounts = objSysSetting.UseFundsSubAcc;

                if (m_bOverrideFundsAccountLink)
                    bFundsAccountLink = m_bFundsAccountLink;
                else
                    bFundsAccountLink = objSysSetting.FundsAccountLink;

                // vaibhav: functionality added for list sorted on priority.

                #region Set the Order By.

                if (p_bSubAccount && bUseFundsSubAccounts)
                    sOrderBy = " ORDER BY SUB_ACC_NAME";
                else
                    sOrderBy = " ORDER BY ACCOUNT_NAME";

                try
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT ORDER_BANK_ACC FROM SYS_PARMS");
                    if (objReader != null && objReader.Read())
                    {
                        iOrderCondition = Common.Conversion.ConvertObjToInt(objReader.GetValue("ORDER_BANK_ACC"), m_iClientId);
                        if (p_bSubAccount && bUseFundsSubAccounts)
                        {
                            switch (iOrderCondition)
                            {
                                case 0:
                                    sOrderBy = "";
                                    break;
                                case 1:
                                    sOrderBy = " ORDER BY BANK_ACC_SUB.PRIORITY  DESC, SUB_ACC_NAME ASC";
                                    break;
                                case 2:
                                    sOrderBy = " ORDER BY SUB_ACC_NAME";
                                    break;
                                case 3:
                                    sOrderBy = " ORDER BY SUB_ACC_NUMBER, SUB_ACC_NAME";
                                    break;
                            }
                        }
                        else
                        {
                            switch (iOrderCondition)
                            {
                                case 0:
                                    sOrderBy = "";
                                    break;
                                case 1:
                                    sOrderBy = " ORDER BY PRIORITY DESC, ACCOUNT_NAME ASC";
                                    break;
                                case 2:
                                    sOrderBy = " ORDER BY ACCOUNT_NAME";
                                    break;
                                case 3:
                                    sOrderBy = " ORDER BY ACCOUNT_NUMBER, ACCOUNT_NAME";
                                    break;
                            }
                        }
                    }
                }
                finally
                {
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
                #endregion
                if (objSysSetting.MultiCovgPerClm == -1)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    if (m_objFunds.ClaimId > 0)
                        objClaim.MoveTo(m_objFunds.ClaimId);

                    objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                    if (objClaim.EventId > 0)
                        objEvent.MoveTo(objClaim.EventId);

                    iDeptEID = objEvent.DeptEid;
                    iServiceCode = objClaim.ServiceCode;
                    if (iDeptEID == 0)
                    {
                        if (iServiceCode != 0)
                        {
                            if (p_bSubAccount && bUseFundsSubAccounts)
                            {
                                sSQL = "SELECT SUB_ACC_NAME,SUB_ROW_ID,BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, BANK_ACC_SUB.TRIGGER_FROM_DATE, "
                                    + " BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT WHERE " + iServiceCode
                                    + " IN ( SELECT SERVICE_CODE FROM ACC_SERVICE_CODE WHERE "
                                    + " BANK_ACC_SUB.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID "
                                    + " AND BANK_ACC_SUB.SUB_ROW_ID = ACC_SERVICE_CODE.SUB_ACC_ID) "
                                    + " OR BANK_ACC_SUB.SUB_ROW_ID NOT IN (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL)";
                            }
                            else
                            {
                                sSQL = "SELECT ACCOUNT_NAME,ACCOUNT_ID,EFF_TRIGGER, TRIGGER_FROM_DATE, "
                                    + " TRIGGER_TO_DATE,IS_EFT_ACCOUNT FROM ACCOUNT WHERE " + iServiceCode
                                    + " IN (SELECT SERVICE_CODE FROM ACC_SERVICE_CODE "
                                    + " WHERE ACCOUNT.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID) "
                                    + " OR ACCOUNT.ACCOUNT_ID NOT IN (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE)";
                            }
                        }
                        else
                        {
                            if (p_bSubAccount && bUseFundsSubAccounts)
                            {
                                sSQL = "SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, "
                                    + " BANK_ACC_SUB.TRIGGER_FROM_DATE, BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT"
                                    + " WHERE BANK_ACC_SUB.SUB_ROW_ID NOT IN (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL) AND BANK_ACC_SUB.ACCOUNT_ID =ACCOUNT.ACCOUNT_ID";
                            }
                            else
                            {
                                sSQL = "SELECT ACCOUNT_NAME,ACCOUNT_ID,EFF_TRIGGER, TRIGGER_FROM_DATE, "
                                    + " TRIGGER_TO_DATE,IS_EFT_ACCOUNT FROM ACCOUNT"
                                    + " WHERE ACCOUNT.ACCOUNT_ID NOT IN (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE)";
                            }
                        }
                    }
                    else
                    {
                        iThisLob = objClaim.LineOfBusCode;
                        if (iServiceCode != 0)
                        {
                            if (p_bSubAccount && bUseFundsSubAccounts)
                            {
                                sSQL = "SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, BANK_ACC_SUB.TRIGGER_FROM_DATE, "
                                    + " BANK_ACC_SUB.TRIGGER_TO_DATE, BANK_ACC_SUB.SUB_ACC_NUMBER, BANK_ACC_SUB.OWNER_EID, BANK_ACC_SUB.PRIORITY,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT "
                                    + " WHERE (BANK_ACC_SUB.OWNER_LOB = " + iThisLob + " OR BANK_ACC_SUB.OWNER_LOB = 0 OR BANK_ACC_SUB.OWNER_LOB IS NULL) "
                                    + " AND (" + iServiceCode + " IN (SELECT SERVICE_CODE FROM ACC_SERVICE_CODE "
                                    + " WHERE BANK_ACC_SUB.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID AND  "
                                    + " BANK_ACC_SUB.SUB_ROW_ID = ACC_SERVICE_CODE.SUB_ACC_ID ) "
                                    + " OR BANK_ACC_SUB.SUB_ROW_ID NOT IN "
                                    + " (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL)) AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID";
                            }
                            else
                            {
                                sSQL = "SELECT ACCOUNT_NAME,ACCOUNT_ID, EFF_TRIGGER, TRIGGER_FROM_DATE, "
                                    + " TRIGGER_TO_DATE, ACCOUNT_NUMBER, OWNER_EID, PRIORITY,IS_EFT_ACCOUNT FROM ACCOUNT "//Amitosh for EFT Payments
                                    + " WHERE (OWNER_LOB = " + iThisLob + " OR OWNER_LOB = 0 OR "
                                    + " OWNER_LOB IS NULL) AND (" + iServiceCode
                                    + " IN (SELECT SERVICE_CODE FROM ACC_SERVICE_CODE "
                                    + " WHERE ACCOUNT.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID) "
                                    + " OR ACCOUNT.ACCOUNT_ID NOT IN "
                                    + " (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE))";
                            }
                        }
                        else
                        {
                            if (p_bSubAccount && bUseFundsSubAccounts)
                            {
                                sSQL = "SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, "
                                    + " BANK_ACC_SUB.TRIGGER_FROM_DATE, BANK_ACC_SUB.TRIGGER_TO_DATE, BANK_ACC_SUB.OWNER_EID,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT WHERE "
                                    + " (BANK_ACC_SUB.OWNER_LOB = " + iThisLob + " OR BANK_ACC_SUB.OWNER_LOB = 0 OR BANK_ACC_SUB.OWNER_LOB IS NULL) "
                                    + " AND BANK_ACC_SUB.SUB_ROW_ID NOT IN "
                                    + " (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL) AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID";
                            }
                            else
                            {
                                sSQL = "SELECT ACCOUNT_NAME,ACCOUNT_ID, EFF_TRIGGER, "
                                    + " TRIGGER_FROM_DATE, TRIGGER_TO_DATE, OWNER_EID,IS_EFT_ACCOUNT FROM ACCOUNT WHERE (OWNER_LOB = "
                                    + iThisLob + " OR OWNER_LOB = 0 OR OWNER_LOB IS NULL) "
                                    + " AND ACCOUNT.ACCOUNT_ID NOT IN "
                                    + " (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE)";
                            }
                        }

                        if (p_bSubAccount && bUseFundsSubAccounts)
                        {
                            sSQL += " AND (BANK_ACC_SUB.OWNER_EID = 0 OR BANK_ACC_SUB.OWNER_EID IS NULL OR (" + iDeptEID
                                + " IN (SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (ORG_HIERARCHY.CLIENT_EID = BANK_ACC_SUB.OWNER_EID "
                                + " OR COMPANY_EID = BANK_ACC_SUB.OWNER_EID OR OPERATION_EID = BANK_ACC_SUB.OWNER_EID "
                                + " OR REGION_EID = BANK_ACC_SUB.OWNER_EID OR DIVISION_EID = BANK_ACC_SUB.OWNER_EID "
                                + " OR LOCATION_EID = BANK_ACC_SUB.OWNER_EID OR FACILITY_EID = BANK_ACC_SUB.OWNER_EID "
                                + " OR DEPARTMENT_EID = BANK_ACC_SUB.OWNER_EID) AND DEPARTMENT_EID = " + iDeptEID + ")))";
                        }
                        else
                        {
                            sSQL += " AND (OWNER_EID = 0 OR OWNER_EID IS NULL OR (" + iDeptEID
                                + " IN (SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (ORG_HIERARCHY.CLIENT_EID = ACCOUNT.OWNER_EID "
                                + " OR COMPANY_EID = ACCOUNT.OWNER_EID OR OPERATION_EID = ACCOUNT.OWNER_EID "
                                + " OR REGION_EID = ACCOUNT.OWNER_EID OR DIVISION_EID = ACCOUNT.OWNER_EID "
                                + " OR LOCATION_EID = ACCOUNT.OWNER_EID OR FACILITY_EID = ACCOUNT.OWNER_EID "
                                + " OR DEPARTMENT_EID = ACCOUNT.OWNER_EID) AND DEPARTMENT_EID = " + iDeptEID + ")))";
                        }

                        //mona: Making Policy Lob on Bank Account as multicode and non mandatory:Start
                        iPolLOBCode = objClaim.PolicyLOBCode;

                        if (p_bSubAccount && bUseFundsSubAccounts)
                        {
                            // sSQL += " AND BANK_ACC_SUB.ACCOUNT_ID IN (SELECT ACCOUNT_ID FROM ACCOUNT) AND BANK_ACC_SUB.POLICY_LOB_CODE = " + objClaim.PolicyLOBCode;//Filter out orphan sub accounts MITS 16720

                            sSQL += " AND ((BANK_ACC_SUB.SUB_ROW_ID NOT IN (SELECT DISTINCT SUB_ACC_ID FROM ACC_X_POL_LOB WHERE SUB_ACC_ID IS NOT NULL))";
                            if (iPolLOBCode != 0)
                            {
                                sSQL += " OR (" + iPolLOBCode + " IN (SELECT POLICY_LOB FROM ACC_X_POL_LOB "
                                + " WHERE BANK_ACC_SUB.ACCOUNT_ID = ACC_X_POL_LOB.ACCOUNT_ID AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID AND  "
                                + " BANK_ACC_SUB.SUB_ROW_ID = ACC_X_POL_LOB.SUB_ACC_ID ))";
                            }
                            sSQL += ")";
                        }
                        else
                        {
                            //sSQL += " AND POLICY_LOB_CODE = " + objClaim.PolicyLOBCode;
                            sSQL += " AND (( ACCOUNT.ACCOUNT_ID NOT IN (SELECT DISTINCT ACCOUNT_ID FROM ACC_X_POL_LOB))";
                            if (iPolLOBCode != 0)
                            {
                                sSQL += " OR (" + iPolLOBCode + " IN (SELECT POLICY_LOB FROM ACC_X_POL_LOB WHERE ACCOUNT.ACCOUNT_ID = ACC_X_POL_LOB.ACCOUNT_ID))";
                            }
                            sSQL += ")";
                        }
                        //mona: Making Policy Lob on Bank Account as multicode and non mandatory:End
                    }
                }
                else
                {
                    if (bFundsAccountLink && m_objFunds.ClaimId > 0)
                    {
                        #region Getting Policy data.
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(m_objFunds.ClaimId);
                        //Start-Mridul Bansal. 01/12/10. MITS#18229.
                        // Start Naresh Changes in funds for Enhanced Policy
                        //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
                        objLOBSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                        if (objLOBSettings[objClaim.LineOfBusCode].UseEnhPolFlag == -1)
                        //End-Mridul Bansal. 01/12/10. MITS#18229.
                        {
                            iPolicyId = objClaim.PrimaryPolicyIdEnh;
                        }
                        else
                        {
                            iPolicyId = objClaim.PrimaryPolicyId;
                        }
                        // End Naresh Changes in funds for Enhanced Policy

                        if (iPolicyId != 0)
                        {
                            //Geeta 05/17/07 : Modified to fix 9097
                            sSQL = "SELECT SUM(AMOUNT) FROM FUNDS WHERE CLAIM_ID = " + m_objFunds.ClaimId + " AND PAYMENT_FLAG = -1 AND VOID_FLAG <> -1";
                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objReader != null && objReader.Read())
                            {
                                dClaimTotal = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                            }

                            objReader.Close();

                            sSQL = "SELECT SUM(AMOUNT) FROM FUNDS WHERE CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIM, EVENT WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM_ID = " + m_objFunds.ClaimId + ")" + " AND PAYMENT_FLAG = -1 AND VOID_FLAG <> -1";
                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objReader != null && objReader.Read())
                            {
                                dOccTotal = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                            }

                            objReader.Close();

                            iCoverageTypeCode = objLocalCache.GetRelatedCodeId(objClaim.ClaimTypeCode);
                            iLastPolicyID = iPolicyId;
                            bFoundNextPolicy = false;
                            System.Collections.Generic.List<int> colExhaustedPolicies = new System.Collections.Generic.List<int>();
                            while (bFoundNextPolicy != true)
                            {
                                sSQL = "SELECT NEXT_POLICY_ID, TOTAL_PAYMENTS, POLICY_LIMIT, CLAIM_LIMIT, OCCURRENCE_LIMIT, COVERAGE_TYPE_CODE FROM POLICY_X_CVG_TYPE WHERE POLICY_ID = " + iPolicyId + " AND COVERAGE_TYPE_CODE = " + iCoverageTypeCode;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objReader != null && objReader.Read())
                                {
                                    if (!(colExhaustedPolicies.Contains(objReader.GetInt("NEXT_POLICY_ID"))) &&
                                        (Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_PAYMENTS"), m_iClientId) >= Conversion.ConvertObjToDouble(objReader.GetValue("POLICY_LIMIT"), m_iClientId) || dClaimTotal >= Conversion.ConvertObjToDouble(objReader.GetValue("CLAIM_LIMIT"), m_iClientId) || dOccTotal >= Conversion.ConvertObjToDouble(objReader.GetValue("OCCURRENCE_LIMIT"), m_iClientId)))
                                    {
                                        iLastPolicyID = iPolicyId;
                                        colExhaustedPolicies.Add(iLastPolicyID);
                                        iPolicyId = objReader.GetInt("NEXT_POLICY_ID");
                                        bFoundNextPolicy = false;
                                    }
                                    else
                                    {
                                        bFoundNextPolicy = true;
                                    }
                                }
                                else
                                {
                                    iPolicyId = iLastPolicyID;
                                    bFoundNextPolicy = true;
                                }
                            }

                            objReader.Close();
                            //Start-Mridul Bansal. 01/12/10. MITS#18229
                            // Start Naresh Changes in funds for Enhanced Policy
                            //if (objClaim.Context.InternalSettings.SysSettings.UseEnhPolFlag == -1)
                            if (objLOBSettings[objClaim.LineOfBusCode].UseEnhPolFlag == -1)
                            //End-Mridul Bansal. 01/12/10. MITS#18229
                            {
                                objPolicyEnh = (PolicyEnh)m_objDataModelFactory.GetDataModelObject("PolicyEnh", false);

                                objPolicyEnh.MoveTo(iPolicyId);

                                iPolicyBankId = objPolicyEnh.BankAccId;
                                iPolicySubAccId = objPolicyEnh.SubAccRowId;
                            }
                            else
                            {
                                objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                                objPolicy.MoveTo(iPolicyId);

                                iPolicyBankId = objPolicy.BankAccId;
                                iPolicySubAccId = objPolicy.SubAccRowId;
                            }
                            // End Naresh Changes in funds for Enhanced Policy
                        }
                        else
                        {
                            iPlanId = objClaim.PlanId;
                            if (iPlanId > 0)
                            {
                                objDisabilityPlan = (DisabilityPlan)m_objDataModelFactory.GetDataModelObject("DisabilityPlan", false);
                                objDisabilityPlan.MoveTo(iPlanId);

                                iPlanBankId = objDisabilityPlan.BankAccId;
                                iPlanSubAccRowId = objDisabilityPlan.SubAccRowId;
                            }
                        }
                        #endregion

                        if (iPolicyBankId > 0)
                        {
                            #region Policy Bank Id
                            if (p_bSubAccount && bUseFundsSubAccounts)
                            {
                                sSQL = " SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER,BANK_ACC_SUB.TRIGGER_FROM_DATE,"
                                    + " BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT WHERE BANK_ACC_SUB.ACCOUNT_ID = " + iPolicyBankId
                                    + " AND SUB_ROW_ID = " + iPolicySubAccId + " AND BANK_ACC_SUB.ACCOUNT_ID =ACCOUNT.ACCOUNT_ID";
                            }
                            else
                            {
                                sSQL = " SELECT ACCOUNT.ACCOUNT_NAME, ACCOUNT.ACCOUNT_ID,EFF_TRIGGER, TRIGGER_FROM_DATE,"
                                    + " TRIGGER_TO_DATE,IS_EFT_ACCOUNT FROM ACCOUNT WHERE ACCOUNT.ACCOUNT_ID = " + iPolicyBankId;
                            }
                            #endregion
                        }
                        else
                        {
                            if (iPlanBankId > 0)
                            {
                                #region Plan Bank Id
                                if (p_bSubAccount && bUseFundsSubAccounts)
                                {
                                    sSQL = " SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER,BANK_ACC_SUB.TRIGGER_FROM_DATE,"
                                        + " BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT WHERE BANK_ACC_SUB.ACCOUNT_ID = " + iPlanBankId
                                        + " AND SUB_ROW_ID = " + iPlanSubAccRowId + " AND BANK_ACC_SUB.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID";
                                }
                                else
                                {
                                    sSQL = " SELECT ACCOUNT.ACCOUNT_NAME, ACCOUNT.ACCOUNT_ID,EFF_TRIGGER, "
                                        + " TRIGGER_FROM_DATE, TRIGGER_TO_DATE,IS_EFT_ACCOUNT FROM ACCOUNT WHERE ACCOUNT.ACCOUNT_ID = "
                                        + iPlanBankId;
                                }
                                #endregion
                            }
                            else
                            {
                                objEvent = objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                                if (objClaim.EventId > 0)
                                    objEvent.MoveTo(objClaim.EventId);

                                iDeptEID = objEvent.DeptEid;
                                iServiceCode = objClaim.ServiceCode;
                                if (iDeptEID == 0)
                                {
                                    #region If Department Id is zero .
                                    if (iServiceCode != 0)
                                    {
                                        if (p_bSubAccount && bUseFundsSubAccounts)
                                        {
                                            // Mihika 18-Jan-2006 Defect# 1339 Changed the query to return account_id as third field
                                            sSQL = "SELECT SUB_ACC_NAME,SUB_ROW_ID,BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, BANK_ACC_SUB.TRIGGER_FROM_DATE, "
                                                + " BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT WHERE " + iServiceCode
                                                + " IN ( SELECT SERVICE_CODE FROM ACC_SERVICE_CODE WHERE "
                                                + " BANK_ACC_SUB.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID AND BANK_ACC_SUB.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID"
                                                + " AND BANK_ACC_SUB.SUB_ROW_ID = ACC_SERVICE_CODE.SUB_ACC_ID) "
                                                + " OR BANK_ACC_SUB.SUB_ROW_ID NOT IN (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL)";
                                        }
                                        else
                                        {
                                            sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID,EFF_TRIGGER, TRIGGER_FROM_DATE, "
                                                + " TRIGGER_TO_DATE,IS_EFT_ACCOUNT FROM ACCOUNT WHERE " + iServiceCode
                                                + " IN (SELECT SERVICE_CODE FROM ACC_SERVICE_CODE "
                                                + " WHERE ACCOUNT.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID) "
                                                + " OR ACCOUNT.ACCOUNT_ID NOT IN (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE)";
                                        }
                                    }
                                    else
                                    {
                                        if (p_bSubAccount && bUseFundsSubAccounts)
                                        {
                                            sSQL = " SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, "
                                                + " BANK_ACC_SUB.TRIGGER_FROM_DATE, BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT"
                                                + " WHERE BANK_ACC_SUB.SUB_ROW_ID NOT IN (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL) AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID";
                                        }
                                        else
                                        {
                                            sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID,EFF_TRIGGER, TRIGGER_FROM_DATE, "
                                                + " TRIGGER_TO_DATE,IS_EFT_ACCOUNT FROM ACCOUNT"
                                                + " WHERE ACCOUNT.ACCOUNT_ID NOT IN (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE)";
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region If Department Id is not zero.
                                    iThisLob = objClaim.LineOfBusCode;
                                    if (iServiceCode != 0)
                                    {
                                        // Mihika 18-Jan-2006 Defect# 1339 Changed the query to maintain the field order as in other cases
                                        if (p_bSubAccount && bUseFundsSubAccounts)
                                        {
                                            sSQL = " SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, BANK_ACC_SUB.TRIGGER_FROM_DATE, "
                                                + " BANK_ACC_SUB.TRIGGER_TO_DATE, BANK_ACC_SUB.SUB_ACC_NUMBER, BANK_ACC_SUB.OWNER_EID, BANK_ACC_SUB.PRIORITY,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT "
                                                + " WHERE (BANK_ACC_SUB.OWNER_LOB = " + iThisLob + " OR BANK_ACC_SUB.OWNER_LOB = 0 OR BANK_ACC_SUB.OWNER_LOB IS NULL) "
                                                + " AND (" + iServiceCode + " IN (SELECT SERVICE_CODE FROM ACC_SERVICE_CODE "
                                                + " WHERE BANK_ACC_SUB.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID AND  BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID AND"
                                                + " BANK_ACC_SUB.SUB_ROW_ID = ACC_SERVICE_CODE.SUB_ACC_ID) "
                                                + " OR BANK_ACC_SUB.SUB_ROW_ID NOT IN "
                                                + " (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL)) AND BANK_ACC_SUB.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID";
                                        }
                                        else
                                        {
                                            sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID, EFF_TRIGGER, TRIGGER_FROM_DATE, "
                                                + " TRIGGER_TO_DATE, ACCOUNT_NUMBER, OWNER_EID, PRIORITY,IS_EFT_ACCOUNT FROM ACCOUNT "
                                                + " WHERE (OWNER_LOB = " + iThisLob + " OR OWNER_LOB = 0 OR "
                                                + " OWNER_LOB IS NULL) AND (" + iServiceCode
                                                + " IN (SELECT SERVICE_CODE FROM ACC_SERVICE_CODE "
                                                + " WHERE ACCOUNT.ACCOUNT_ID = ACC_SERVICE_CODE.ACCOUNT_ID) "
                                                + " OR ACCOUNT.ACCOUNT_ID NOT IN "
                                                + " (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE))";
                                        }
                                    }
                                    else
                                    {
                                        if (p_bSubAccount && bUseFundsSubAccounts)
                                        {
                                            sSQL = " SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID, BANK_ACC_SUB.EFF_TRIGGER, "
                                                + " BANK_ACC_SUB.TRIGGER_FROM_DATE, BANK_ACC_SUB.TRIGGER_TO_DATE, BANK_ACC_SUB.OWNER_EID,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT WHERE "
                                                + " (BANK_ACC_SUB.OWNER_LOB = " + iThisLob + " OR BANK_ACC_SUB.OWNER_LOB = 0 OR BANK_ACC_SUB.OWNER_LOB IS NULL) "
                                                + " AND BANK_ACC_SUB.SUB_ROW_ID NOT IN "
                                                + " (SELECT SUB_ACC_ID FROM ACC_SERVICE_CODE WHERE SUB_ACC_ID IS NOT NULL) AND BANK_ACC_SUB.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID";
                                        }
                                        else
                                        {
                                            sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID, EFF_TRIGGER, "
                                                + " TRIGGER_FROM_DATE, TRIGGER_TO_DATE, OWNER_EID,IS_EFT_ACCOUNT FROM ACCOUNT WHERE (OWNER_LOB = "
                                                + iThisLob + " OR OWNER_LOB = 0 OR OWNER_LOB IS NULL) "
                                                + " AND ACCOUNT.ACCOUNT_ID NOT IN "
                                                + " (SELECT ACCOUNT_ID FROM ACC_SERVICE_CODE)";
                                        }
                                    }
                                    #endregion

                                    #region Owner Id Logic
                                    if (p_bSubAccount && bUseFundsSubAccounts)
                                    {
                                        sSQL += " AND (BANK_ACC_SUB.OWNER_EID = 0 OR BANK_ACC_SUB.OWNER_EID IS NULL OR (" + iDeptEID
                                            + " IN (SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (ORG_HIERARCHY.CLIENT_EID = BANK_ACC_SUB.OWNER_EID "
                                            + " OR COMPANY_EID = BANK_ACC_SUB.OWNER_EID OR OPERATION_EID = BANK_ACC_SUB.OWNER_EID "
                                            + " OR REGION_EID = BANK_ACC_SUB.OWNER_EID OR DIVISION_EID = BANK_ACC_SUB.OWNER_EID "
                                            + " OR LOCATION_EID = BANK_ACC_SUB.OWNER_EID OR FACILITY_EID = BANK_ACC_SUB.OWNER_EID "
                                            + " OR DEPARTMENT_EID = BANK_ACC_SUB.OWNER_EID) AND DEPARTMENT_EID = " + iDeptEID + ")))";
                                    }
                                    else
                                    {
                                        sSQL += " AND (OWNER_EID = 0 OR OWNER_EID IS NULL OR (" + iDeptEID
                                            + " IN (SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (ORG_HIERARCHY.CLIENT_EID = ACCOUNT.OWNER_EID "
                                            + " OR COMPANY_EID = ACCOUNT.OWNER_EID OR OPERATION_EID = ACCOUNT.OWNER_EID "
                                            + " OR REGION_EID = ACCOUNT.OWNER_EID OR DIVISION_EID = ACCOUNT.OWNER_EID "
                                            + " OR LOCATION_EID = ACCOUNT.OWNER_EID OR FACILITY_EID = ACCOUNT.OWNER_EID "
                                            + " OR DEPARTMENT_EID = ACCOUNT.OWNER_EID) AND DEPARTMENT_EID = " + iDeptEID + ")))";
                                    }

                                    if (p_bSubAccount && bUseFundsSubAccounts)
                                    {
                                        sSQL += " AND BANK_ACC_SUB.ACCOUNT_ID IN (SELECT ACCOUNT_ID FROM ACCOUNT)";//Filter out orphan sub accounts MITS 16720
                                    }

                                    #endregion

                                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL + sOrderBy);
                                    if (objReader != null)
                                    {
                                        while (objReader.Read())
                                        {
                                            #region Add the record to the list .
                                            sEffTriger = objReader.GetString("EFF_TRIGGER");
                                            sTriggerFromDate = objReader.GetString("TRIGGER_FROM_DATE");
                                            sTriggerToDate = objReader.GetString("TRIGGER_TO_DATE");
                                            if (this.InEffDateRange(m_objFunds.ClaimId, sEffTriger, sTriggerFromDate, sTriggerToDate))
                                            {
                                                structAccountDetail = new AccountDetail();
                                                // Mihika 18-Jan-2006 Defect# 1339 Changed the indexes according to the query field ordering.
                                                structAccountDetail.AccountName = objReader.GetString(0);
                                                //raman has modified the indexes of objReader depending on SubAccounts Flag
                                                structAccountDetail.IsEFtAccount = Conversion.ConvertObjToBool(objReader.GetValue("IS_EFT_ACCOUNT"), m_iClientId);//Added by Amitosh for eFT
                                                if (p_bSubAccount && bUseFundsSubAccounts)
                                                {
                                                    structAccountDetail.SubRowId = objReader.GetInt(1);
                                                    objAccountId = objReader.GetInt(2);
                                                }
                                                else
                                                {
                                                    structAccountDetail.SubRowId = 0;
                                                    objAccountId = objReader.GetInt(1);
                                                }

                                                try
                                                {
                                                    structAccountDetail.AccountId = Common.Conversion.ConvertObjToInt(objAccountId, m_iClientId);
                                                }
                                                catch
                                                {
                                                    structAccountDetail.AccountId = 0;
                                                }
                                                arrlstAccountDetail.Add(structAccountDetail);
                                            }
                                            #endregion

                                        }// reader while 
                                    }// reader if
                                    bExit = true;
                                }// id DeptEID != 0 					
                            }// if planbankID
                        }// policy bank id 
                    }
                    else
                    {
                        if (p_iParentAccount > 0)
                        {
                            if (p_bSubAccount && bUseFundsSubAccounts)
                            {
                                sSQL = " SELECT SUB_ACC_NAME,SUB_ROW_ID,BANK_ACC_SUB.ACCOUNT_ID,BANK_ACC_SUB.EFF_TRIGGER, BANK_ACC_SUB.TRIGGER_FROM_DATE, "
                                    + " BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT WHERE BANK_ACC_SUB.ACCOUNT_ID = " + p_iParentAccount + " AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID";
                            }
                            else
                            {
                                sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID,EFF_TRIGGER, TRIGGER_FROM_DATE, TRIGGER_TO_DATE,IS_EFT_ACCOUNT "   //Ritesh: IS_EFT_ACCOUNT added for MITS 26910
                                    + " FROM ACCOUNT WHERE ACCOUNT_ID =" + p_iParentAccount;
                            }
                        }
                        else
                        {
                            if (p_bSubAccount && bUseFundsSubAccounts)
                            {
                                sSQL = " SELECT SUB_ACC_NAME,SUB_ROW_ID, BANK_ACC_SUB.ACCOUNT_ID,BANK_ACC_SUB.EFF_TRIGGER,BANK_ACC_SUB.TRIGGER_FROM_DATE, "
                                    + " BANK_ACC_SUB.TRIGGER_TO_DATE,ACCOUNT.IS_EFT_ACCOUNT FROM BANK_ACC_SUB,ACCOUNT ";
                            }
                            else
                            {
                                sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID,EFF_TRIGGER,TRIGGER_FROM_DATE,TRIGGER_TO_DATE,IS_EFT_ACCOUNT "//Amitosh for EFT Payment
                                    + " FROM ACCOUNT ";
                            }
                        }

                        if (p_bSubAccount && bUseFundsSubAccounts)
                        {
                            int iWhereFound = 0;
                            iWhereFound = sSQL.IndexOf("WHERE");
                            if (iWhereFound == -1)
                            {
                                sSQL += " WHERE BANK_ACC_SUB.ACCOUNT_ID IN (SELECT ACCOUNT_ID FROM ACCOUNT) AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID ";  //Filter out orphan sub accounts MITS 16720
                            }
                            else
                            {
                                sSQL += " AND BANK_ACC_SUB.ACCOUNT_ID IN (SELECT ACCOUNT_ID FROM ACCOUNT) AND BANK_ACC_SUB.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID ";  //Filter out orphan sub accounts MITS 16720
                            }
                        }
                    }
                }
                if (!bExit)
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL + sOrderBy);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sEffTriger = objReader.GetString("EFF_TRIGGER");
                            sTriggerFromDate = objReader.GetString("TRIGGER_FROM_DATE");
                            sTriggerToDate = objReader.GetString("TRIGGER_TO_DATE");
                            if (this.InEffDateRange(m_objFunds.ClaimId, sEffTriger, sTriggerFromDate, sTriggerToDate))
                            {
                                structAccountDetail = new AccountDetail();
                                structAccountDetail.AccountName = objReader.GetString(0);
                                //structAccountDetail.SubRowId = Common.Conversion.ConvertObjToInt( objReader.GetValue( 1 ), m_iClientId );
                                //objAccountId = objReader.GetValue(2);
                                structAccountDetail.IsEFtAccount = Conversion.ConvertObjToBool(objReader.GetValue("IS_EFT_ACCOUNT"), m_iClientId);//Added by Amitosh for eFT
                                if (p_bSubAccount && bUseFundsSubAccounts)
                                {
                                    structAccountDetail.SubRowId = objReader.GetInt(1);
                                    objAccountId = objReader.GetInt(2);
                                }
                                else
                                {
                                    structAccountDetail.SubRowId = 0;
                                    objAccountId = objReader.GetInt(1);
                                }

                                try
                                {
                                    structAccountDetail.AccountId = Common.Conversion.ConvertObjToInt(objAccountId, m_iClientId);
                                }
                                catch
                                {
                                    structAccountDetail.AccountId = 0;
                                }

                                arrlstAccountDetail.Add(structAccountDetail);
                            }
                        }
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetAccounts.ErrorAccount", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
                objEvent = null;
                objPolicy = null;
                // Start Naresh Changes in funds for Enhanced Policy
                objPolicyEnh = null;
                // End Naresh Changes in funds for Enhanced Policy
                objDisabilityPlan = null;
                objSysSetting = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderCount != null)
                {
                    objReaderCount.Close();
                    objReaderCount.Dispose();
                }
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                //Start-Mridul Bansal. 01/12/10. MITS#18229
                objLOBSettings = null;
                //End-Mridul Bansal. 01/12/10. MITS#18229
            }
            return (arrlstAccountDetail);
        }

        /// <summary>
        /// Get the Accounts List.
        /// </summary>
        /// <param name="p_bSubAccount">Sub Account Flag</param>
        /// <returns>Account Array List</returns>
        public ArrayList GetAccounts(bool p_bSubAccount)
        {
            return (this.GetAccounts(p_bSubAccount, 0));
        }
        /// <summary>
        /// Get the AccountsEx List.
        /// </summary>
        /// <param name="p_bFundsAccountLink">Funds Account Link Flag</param>
        /// <param name="p_bSubAccount">Sub Account Flag</param>
        /// <param name="p_iParentAccount">Parent Account Id.</param>		
        /// <returns>Account List</returns>
        public ArrayList GetAccountsEx(bool p_bFundsAccountLink, bool p_bSubAccount, int p_iParentAccount)
        {
            ArrayList arrlstAccountDetail = null;
            try
            {
                m_bFundsAccountLink = p_bFundsAccountLink;
                m_bOverrideFundsAccountLink = true;
                arrlstAccountDetail = this.GetAccounts(p_bSubAccount, p_iParentAccount);
                m_bOverrideFundsAccountLink = false;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetAccountsEx.ErrorGetAccount", m_iClientId), p_objEx);
            }
            return (arrlstAccountDetail);
        }
        /// <summary>
        /// Check Date is in effective date range.
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_sEffTriger">EffTrigger</param>
        /// <param name="p_sTriggerFromDate">From Date</param>
        /// <param name="p_sTriggerToDate">To Date</param>
        /// <returns></returns>
        private bool InEffDateRange(int p_iClaimId, string p_sEffTriger, string p_sTriggerFromDate, string p_sTriggerToDate)
        {
            DbReader objReader = null;

            bool bInEffDateRange = false;
            string sDateOfClaim = "";
            string sDateOfEvent = "";
            string sCurrentDate = "";
            string sFromDate = "";
            string sToDate = "";
            string sSQL = "";

            try
            {
                sFromDate = Conversion.GetDBDateFormat(p_sTriggerFromDate, "d");
                sToDate = Conversion.GetDBDateFormat(p_sTriggerToDate, "d");

                if (p_sEffTriger == "")
                    bInEffDateRange = true;
                else
                {
                    switch (p_sEffTriger.ToUpper())
                    {
                        case "CLAIM DATE":
                            if (p_iClaimId > 0)
                            {
                                sSQL = " SELECT DATE_OF_CLAIM FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;

                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        sDateOfClaim = objReader.GetString("DATE_OF_CLAIM");
                                    }
                                    else
                                    {
                                        bInEffDateRange = true;
                                        break;
                                    }
                                }

                                if (objReader != null)
                                    objReader.Close();

                                if (sDateOfClaim != "")
                                {
                                    if (Utilities.IsDate(sFromDate) && Utilities.IsDate(sToDate))
                                    {
                                        if (Conversion.ConvertStrToLong(sDateOfClaim) <= Conversion.ConvertStrToLong(p_sTriggerToDate) && Conversion.ConvertStrToLong(sDateOfClaim) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                            bInEffDateRange = true;
                                        else
                                            bInEffDateRange = false;
                                    }
                                    else
                                    {
                                        if (Utilities.IsDate(sToDate))
                                        {
                                            if (Conversion.ConvertStrToLong(sDateOfClaim) <= Conversion.ConvertStrToLong(p_sTriggerToDate))
                                                bInEffDateRange = true;
                                            else
                                                bInEffDateRange = false;
                                        }
                                        else
                                        {
                                            if (Utilities.IsDate(sFromDate))
                                            {
                                                if (Conversion.ConvertStrToLong(sDateOfClaim) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                                    bInEffDateRange = true;
                                                else
                                                    bInEffDateRange = false;
                                            }
                                            else
                                            {
                                                bInEffDateRange = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    bInEffDateRange = true;
                                }
                            }
                            else
                            {
                                bInEffDateRange = true;
                            }
                            break;

                        case "EVENT DATE":
                            if (p_iClaimId > 0)
                            {
                                sSQL = " SELECT DATE_OF_EVENT FROM EVENT, CLAIM WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID "
                                    + " AND CLAIM.CLAIM_ID = " + p_iClaimId;

                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        sDateOfEvent = objReader.GetString("DATE_OF_EVENT");
                                    }
                                    else
                                    {
                                        bInEffDateRange = true;
                                        break;
                                    }
                                }

                                if (objReader != null)
                                    objReader.Close();

                                if (sDateOfEvent != "")
                                {
                                    if (Utilities.IsDate(sFromDate) && Utilities.IsDate(sToDate))
                                    {
                                        if (Conversion.ConvertStrToLong(sDateOfEvent) <= Conversion.ConvertStrToLong(p_sTriggerToDate) && Conversion.ConvertStrToLong(sDateOfEvent) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                            bInEffDateRange = true;
                                        else
                                            bInEffDateRange = false;
                                    }
                                    else
                                    {
                                        if (Utilities.IsDate(sToDate))
                                        {
                                            if (Conversion.ConvertStrToLong(sDateOfEvent) <= Conversion.ConvertStrToLong(p_sTriggerToDate))
                                                bInEffDateRange = true;
                                            else
                                                bInEffDateRange = false;
                                        }
                                        else
                                        {
                                            if (Utilities.IsDate(sFromDate))
                                            {
                                                if (Conversion.ConvertStrToLong(sDateOfEvent) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                                    bInEffDateRange = true;
                                                else
                                                    bInEffDateRange = false;
                                            }
                                            else
                                            {
                                                bInEffDateRange = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    bInEffDateRange = true;
                                }
                            }
                            else
                            {
                                bInEffDateRange = true;
                            }
                            break;

                        case "CURRENT SYSTEM DATE":
                            sCurrentDate = Conversion.GetDate(System.DateTime.Today.Date.ToString("d"));
                            if (sCurrentDate != "")
                            {
                                if (Utilities.IsDate(sFromDate) && Utilities.IsDate(sToDate))
                                {
                                    if (Conversion.ConvertStrToLong(sCurrentDate) <= Conversion.ConvertStrToLong(p_sTriggerToDate) && Conversion.ConvertStrToLong(sCurrentDate) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                        bInEffDateRange = true;
                                    else
                                        bInEffDateRange = false;
                                }
                                else
                                {
                                    if (Utilities.IsDate(sToDate))
                                    {
                                        if (Conversion.ConvertStrToLong(sCurrentDate) <= Conversion.ConvertStrToLong(p_sTriggerToDate))
                                            bInEffDateRange = true;
                                        else
                                            bInEffDateRange = false;
                                    }
                                    else
                                    {
                                        if (Utilities.IsDate(sFromDate))
                                        {
                                            if (Conversion.ConvertStrToLong(sCurrentDate) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                                bInEffDateRange = true;
                                            else
                                                bInEffDateRange = false;
                                        }
                                        else
                                        {
                                            bInEffDateRange = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                bInEffDateRange = true;
                            }
                            break;
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.InEffDateRange.ErrorDateRange", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (bInEffDateRange);
        }

        /// <summary>
        /// Get Payee Type
        /// </summary>
        private ArrayList GetPayeeTypes()
        {
            DbReader objReader = null;
            ArrayList arrlstPayeeType = null;
            PayeeType structPayeeType;

            int iTableId = 0;
            string sSQL = "";

            try
            {
                arrlstPayeeType = new ArrayList();
                iTableId = this.GetTableId("PAYEE_TYPE");

                sSQL = " SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT "
                    + " WHERE CODES.CODE_ID=CODES_TEXT.CODE_ID AND CODES.TABLE_ID=" + iTableId
                    + " AND " + LANGUAGE_CODE;

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        structPayeeType = new PayeeType();
                        structPayeeType.CodeId = Common.Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), m_iClientId);
                        structPayeeType.ShortCode = objReader.GetString("SHORT_CODE");
                        structPayeeType.CodeDesc = objReader.GetString("CODE_DESC");
                        arrlstPayeeType.Add(structPayeeType);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPayeeTypes.ErrorPayee", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (arrlstPayeeType);
        }

        /// <summary>
        /// Get Claimants
        /// </summary>
        /// <param name="p_iClaimId">Claim ID</param>
        /// <returns>ClaimantList</returns>
        private ClaimantList GetClaimants(int p_iClaimId)
        {
            Claim objClaim = null;
            ClaimantList objClaimantList = null;
            try
            {
                if (p_iClaimId == 0)
                    p_iClaimId = m_objFunds.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);
                    objClaimantList = objClaim.ClaimantList;
                }
                // Return empty list, if this is for new Transaction.
                else
                    objClaimantList = (ClaimantList)m_objDataModelFactory.GetDataModelObject("ClaimantList", false);
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetClaimants.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
            }
            return (objClaimantList);
        }

        /// <summary>
        /// Get Claimants
        /// </summary>
        /// <returns>ClaimantList</returns>
        private ClaimantList GetClaimants()
        {
            return (this.GetClaimants(0));
        }

        /// <summary>
        /// GetUnits
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>UnitXClaimList</returns>
        private UnitXClaimList GetUnits(int p_iClaimId)
        {
            Claim objClaim = null;
            UnitXClaimList objUnitXClaimList = null;
            try
            {
                if (p_iClaimId == 0)
                    p_iClaimId = m_objFunds.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);
                    objUnitXClaimList = objClaim.UnitList;
                }
                // Return empty list, if this is for new Transaction.
                else
                    objUnitXClaimList = (UnitXClaimList)m_objDataModelFactory.GetDataModelObject("UnitXClaimList", false);

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetUnits.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
            }
            return (objUnitXClaimList);
        }

        public XmlDocument GetInsuffReserve(SessionManager p_objSessionManager)
        {
            XmlDocument objInsuffXml = null;
            try
            {
                string sSessionRawData = Utilities.BinaryDeserialize(p_objSessionManager.GetBinaryItem("InsufAmntXml")).ToString();

                objInsuffXml = new XmlDocument();
                objInsuffXml.LoadXml(sSessionRawData);


            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetInsuffReserve.Error", m_iClientId), p_objEx);
            }
            finally
            {
            }
            return objInsuffXml;

        }

        private XmlDocument InsuffResAmntXml(double p_dblBalance, double p_dblPayment, double p_dblReserve, int p_iReserveTypeCode, FundsAuto p_FundsAuto)
        {
            XmlElement objelement = null;
            XmlDocument objXMLDoc = null;

            double dblAddReserve = 0.0;
            double dblSetReserve = 0.0;
            string sXmlForInsResrvAmnt = "";
            string sDesc = "";
            string sShortCode = "";

            try
            {
                dblAddReserve = p_dblPayment - p_dblBalance;
                if (dblAddReserve < 0.0)
                    dblAddReserve = -dblAddReserve;

                dblSetReserve = p_dblBalance - (p_dblPayment + p_dblReserve);
                if (dblSetReserve < 0.0)
                    dblSetReserve = -dblSetReserve;


                sXmlForInsResrvAmnt = "~insufres scr=\"1\" userid=\"" + p_FundsAuto.Context.RMUser.UserId.ToString() + "\" groupid=\"" + p_FundsAuto.Context.RMUser.GroupId.ToString() + "\" claimid=\""
                    + p_FundsAuto.ClaimId.ToString() + "\"|"
                    + "~unit|" + p_FundsAuto.UnitId.ToString() + "~/unit|"
                    + "~claimant|" + p_FundsAuto.ClaimantEid.ToString() + "~/claimant|"
                    + "~balance|" + Math.Round(p_dblBalance, 2, MidpointRounding.AwayFromZero) + "~/balance|"
                    + "~reserve|" + Math.Round(p_dblReserve, 2, MidpointRounding.AwayFromZero) + "~/reserve|"
                    + "~payment|" + Math.Round(p_dblPayment, 2, MidpointRounding.AwayFromZero) + "~/payment|"
                    + "~setreserve|" + Math.Round(dblSetReserve, 2, MidpointRounding.AwayFromZero) + "~/setreserve|"
                    + "~addreserve|" + Math.Round(dblAddReserve, 2, MidpointRounding.AwayFromZero) + "~/addreserve|"
                    + "~newamount|0~/newamount|";

                p_FundsAuto.Context.LocalCache.GetCodeInfo(p_iReserveTypeCode, ref sShortCode, ref sDesc);

                sXmlForInsResrvAmnt += "~rcode id=\"" + p_iReserveTypeCode.ToString()
                    + "\"|" + sDesc + "~/rcode|" + "~/insufres|";

                sXmlForInsResrvAmnt = sXmlForInsResrvAmnt.Replace('~', '<');
                sXmlForInsResrvAmnt = sXmlForInsResrvAmnt.Replace('|', '>');

                objXMLDoc.LoadXml(sXmlForInsResrvAmnt);

                return objXMLDoc;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetXmlForInsResrvAmnt.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objelement = null;
            }
        }


        /// <summary>
        /// GetUnits
        /// </summary>
        /// <returns>UnitXClaimList</returns>
        private UnitXClaimList GetUnits()
        {
            return (this.GetUnits(0));
        }

        #endregion

        #region Save Transaction
        //Not Used Now: Raman Bhatia 07/29/2009
        /*
		/// <summary>
		/// Save Transaction.
		/// </summary>
		/// <param name="p_sSplitXML">
		/// XML contains Split information for each payment collection. 		
		/// </param>
		/// <param name="p_sTransactionDataXML">Transaction Data XML</param>
		/// <returns>XmlDocument</returns>
		public XmlDocument SaveTransaction( string p_sSplitXML , string p_sTransactionDataXML, BusinessAdaptorBase busAdaptor)
		{
			XmlDocument objSplitDoc = null;
			XmlDocument objTransactionDataDoc = null ;
			
			int iTransId = 0;
			int iClaimId = 0 ;
			int iClaimantId = 0 ;
			int iUnitId = 0 ;

			int iConfirmedCount = 0 ;
			int iIndex = 0 ;
			string sConfirmation = "" ;

			CCacheFunctions objCCacheFunctions = null ;

			try
			{
				objSplitDoc = new XmlDocument();
				objSplitDoc.LoadXml( p_sSplitXML );

				objTransactionDataDoc = new XmlDocument();
				objTransactionDataDoc.LoadXml( p_sTransactionDataXML );

				iTransId = Common.Conversion.ConvertStrToInteger( this.GetValue( objTransactionDataDoc , "TransId" ) );
				iClaimId = Common.Conversion.ConvertStrToInteger( this.GetValue( objTransactionDataDoc , "ClaimId" ) );
				iClaimantId = Common.Conversion.ConvertStrToInteger( this.GetValue( objTransactionDataDoc , "ClaimantId" ) );
				iUnitId = Common.Conversion.ConvertStrToInteger( this.GetValue( objTransactionDataDoc , "UnitId" ) );
				iConfirmedCount = Common.Conversion.ConvertStrToInteger( this.GetValue( objTransactionDataDoc , "ConfirmedCount" ) );						
				m_bAllowPaymentsClosedClaims = Common.Conversion.ConvertStrToBool( this.GetValue( objTransactionDataDoc , "AllowPaymentsClosedClaims" ) );
				
				// Mihika Defect# 2122 In case, 'Continue' is clicked on he Insufficient Reserves screen, 
				// the screen is not displayed again.
				if (this.GetValue(objTransactionDataDoc, "Reason") == "AUTO-ADJUST")
					m_bShowInsuffResPopUp = false;
				else
					m_bShowInsuffResPopUp = true;
	
				if( m_objDataModelFactory == null )
					this.Initialize() ;				

				if( iTransId != 0 )
					m_objFunds.MoveTo( iTransId );
				else
					this.InitializeFromClaimId( iClaimId , iClaimantId , iUnitId );
				
				this.UpdateFundsObjectForTransactionData( objTransactionDataDoc );
			
				this.UpdateFundsObjectForPaymentCollectionData( objSplitDoc );

				// Pull any comments that were entered in from session   JP 2/2/2007
				if (iTransId != 0)
				{
					string sKey = "funds|" + iTransId.ToString();
					string sComments=null,sHTMLComments=null;

					if (busAdaptor.cachedObjectExists(sKey + "|Comments"))
					{
						sComments = (string) busAdaptor.getCachedObject(sKey + "|Comments");
						busAdaptor.removeCachedObject(sKey + "|Comments");
					}
					if (busAdaptor.cachedObjectExists(sKey + "|HTMLComments"))
					{
						sHTMLComments = (string) busAdaptor.getCachedObject(sKey + "|HTMLComments");
						busAdaptor.removeCachedObject(sKey + "|HTMLComments");
					}
					if (sComments != null)
						m_objFunds.Comments = sComments;

					if (sHTMLComments != null)
						m_objFunds.HTMLComments = sHTMLComments;
				}
				// *END* Pull any comments that were entered in from session   JP 2/2/2007

				m_bDataChanged = false ;

				//-- Auto-Close
				if (objSplitDoc.GetElementsByTagName("control") != null)
				{
					XmlNodeList elemList = objSplitDoc.GetElementsByTagName("control");
					for (int i=0; i < elemList.Count; i++)
					{
						if(elemList[i].Attributes.GetNamedItem("name").Value == "transtypecode")
						{
							m_ArrCodeIdTransType.Add(elemList[i].Attributes.GetNamedItem("codeid").Value);
						}
					}
				}

				if( !this.ValidateData() )
				{
					if( m_arrlstErrors.Count > 0 )
					{
						m_bDataChanged = true ;
					}
					else
					{
						if( m_arrlstConfirmations.Count > 0 )
						{
							if( iConfirmedCount == m_arrlstConfirmations.Count )							
							{
								//fix for changing check status to hold in case of new payee and appropriate utility settings
								if((m_objFunds.PayeeEid == 0) && m_bPlaceSupHoldNewPayee == true) 
								{
									objCCacheFunctions = new CCacheFunctions( m_sConnectionString );	
									
									if(m_bUseQueuedPayments == true)
									{
										m_objFunds.StatusCode = objCCacheFunctions.GetCodeIDWithShort( "Q" , "CHECK_STATUS" );
										sHoldText = "A new entity has been created";
										m_arrlstWarnings.Add( "Payment has been queued and requires supervisory approval because: " + sHoldText );
									}
									else
									{
										DbReader objRead = null;
										objRead = DbFactory.GetDbReader(m_sConnectionString,"SELECT SH_DIARY FROM CHECK_OPTIONS");
										if(objRead!=null)
										{
											while(objRead.Read())
											{
												iHoldSendDiary = Conversion.ConvertObjToInt(objRead.GetValue("SH_DIARY"), m_iClientId);
											}
										}
										m_objFunds.StatusCode = objCCacheFunctions.GetCodeIDWithShort( "H" , "CHECK_STATUS" );
										sHoldText = "A new entity has been created";
										m_arrlstWarnings.Add( "A hold requiring supervisory approval has been placed on this payment because: " + sHoldText );
									}
								}
								Save();

                                //smahajan6: Safeway: Payment Supervisory Approval : Start - Commented Diary ,now Diary created from Funds Datamodel class
                                //if(iHoldSendDiary==-1)
                                //{  
                                //    CreateHoldDiary("Check On Hold",sHoldText,"FUNDS",m_objFunds.TransId,"Payments: "+m_objFunds.CtlNumber,m_objFunds.TransId);
                                //}
                                //smahajan6: Safeway: Payment Supervisory Approval : End

								//arraylist for confirmations node was not getting cleared on saving...raman bhatia
								m_arrlstConfirmations.Clear();
							}
							else
							{
								for( iIndex = 0 ; iIndex < m_arrlstConfirmations.Count ; iIndex++ )
								{
									sConfirmation = ( string ) m_arrlstConfirmations[iIndex] ;
									if( sConfirmation.IndexOf( "roll_up*" ) != -1 )
										m_objFunds.VoidFlag = false ;
									else
									{	
										m_bDataChanged = true ;
									}									
								}
							}
						}
						else
						{
							if( m_arrlstWarnings.Count > 0 )
								Save();

                            //smahajan6: Safeway: Payment Supervisory Approval : Start - Commented Diary ,now Diary created from Funds Datamodel class
                            //if(iHoldSendDiary==-1)
                            //{
                            //    CreateHoldDiary("Check On Hold",sHoldText,"FUNDS",m_objFunds.TransId,"Payments: "+m_objFunds.CtlNumber,m_objFunds.TransId);
                            //}
                            //smahajan6: Safeway: Payment Supervisory Approval : End
						}					
					}
				}
				else
				{
					Save();
                    //smahajan6: Safeway: Payment Supervisory Approval : Start - Commented Diary ,now Diary created from Funds Datamodel class
                    //if(iHoldSendDiary==-1)
                    //{
                    //    CreateHoldDiary("Check On Hold",sHoldText,"FUNDS",m_objFunds.TransId,"Payments: "+m_objFunds.CtlNumber,m_objFunds.TransId);
                    //}
                    //smahajan6: Safeway: Payment Supervisory Approval : End
				}
				//TR# 1402,1403,1404,1405 Pankaj 01/14/06 Check Claim Type Change Option settings
				this.CheckClaimTypeChangeOption(); 
				this.GetXMLForTransaction();
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("FundManager.SaveTransaction.ErrorSaveTrans") , p_objEx );
			}
			finally
			{
				objSplitDoc = null;
				objTransactionDataDoc = null ;				
			}
			return( m_objDocument);
		}
        */
        /// <summary>
        /// Save the Funds Object.
        /// </summary>
        private void Save()
        {
            try
            {
                CreateFundsInvoice();
                // MITS 12788
                //if( m_objFunds.RollupId != 0 && m_objFunds.VoidFlag )
                if (m_objFunds.VoidFlag)
                {
                    if (m_objFunds.RollupId != 0)
                    {
                        SaveVoidedRollup();
                    }
                    // MITS 12788
                    else
                    {
                        m_objFunds.MoveTo(m_objFunds.TransId);
                        m_objFunds.VoidDate = Conversion.ToDbDate(System.DateTime.Now);
                        m_objFunds.VoidFlag = true;
                        m_objFunds.Save();

                        //Start:Export payment void data to VSS
                        Claim objClaim = null;
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        objClaim.MoveTo(m_objFunds.ClaimId);
                        //if (objClaim.VssClaimInd)
                        //Use Utility Check Instead of Claim Check
                        if (objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                        {
                            VssExportAsynCall objVss = new VssExportAsynCall(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                            objVss.AsynchVssReserveExport(m_objFunds.ClaimId, m_objFunds.ClaimantEid, 0, m_objFunds.TransId, 0, "", "", "", "", "PaymentVoid");
                            objVss = null;
                        }
                        objClaim.Dispose();
                        //End:Export payment void data to VSS
                    }
                }
                else
                {
                    this.RemoveDeletedSplitsItems();

                    //-- Auto-close functionality
                    if (m_AutoCloseTransTypeUsed)
                    {
                        if (IsClaimOpen(m_objFunds.ClaimId, 0))
                        {
                            // LocalCache oCache = new LocalCache(m_sConnectionString);
                            int iCodeId = m_objDataModelFactory.Context.LocalCache.GetCodeId("C", "CLAIM_STATUS");

                            Claim oClaim = null;
                            oClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                            oClaim.MoveTo(m_objFunds.ClaimId);

                            oClaim.ClaimStatusCode = iCodeId;
                            oClaim.DttmClosed = Conversion.ToDbDate(System.DateTime.Now);
                            oClaim.Save();
                        }
                    }
                    // Start Naresh MITS 8579 12/21/2006
                    try
                    {
                        m_objFunds.Save();
                    }

                    catch (Exception e)
                    {
                        if (e.InnerException != null && (e.InnerException.Message == "DataObject.Save.ValidationFailure" ||
                            m_objFunds.Context.ScriptValidationErrors.Count > 0 ||
                            m_objFunds.Context.ScriptWarningErrors.Count > 0))
                        {
                            foreach (System.Collections.DictionaryEntry objError in m_objFunds.Context.ScriptValidationErrors)
                            {
                                if (m_arrlstErrors.Count == 0)
                                {
                                    m_arrlstErrors.Add("Validation Script Error");
                                    m_arrlstErrors.Add(objError.Value.ToString());
                                }
                                else
                                {
                                    m_arrlstErrors.Add(objError.Value.ToString());
                                }
                            }
                            foreach (System.Collections.DictionaryEntry objError in m_objFunds.Context.ScriptWarningErrors)
                            {
                                if (m_arrlstErrors.Count == 0)
                                {
                                    m_arrlstErrors.Add("Warning Script Error");
                                    m_arrlstErrors.Add(objError.Value.ToString());
                                }
                                else
                                {
                                    m_arrlstErrors.Add(objError.Value.ToString());
                                }
                            }
                        }
                        else
                            throw e;
                    }

                    // End Naresh MITS 8579 12/21/2006


                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.Save.ErrorSave", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Save the Funds Object.
        /// </summary>
        /// <param name="p_objFunds">Funds Object</param>
        public void Save(Funds p_objFunds)
        {
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                m_objFunds = p_objFunds;
                Save();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.Save.ErrorSave", m_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Check the UseResFilter Setting in Utility
        /// </summary>
        /// <param name="objXmlDoc">XmlDocument Object</param>
        public XmlDocument CheckUtility(XmlDocument objXmlDoc)// stara Mits 16667 15/05/09
        {

            XmlNode objXmlNode = null;

            try
            {
                if (m_objDataModelFactory.Context.InternalSettings.SysSettings.UseResFilter)
                {
                    objXmlNode = objXmlDoc.SelectSingleNode("//UseResFilter");
                    if (objXmlNode != null)
                        objXmlNode.InnerText = "True";
                }

                objXmlNode = objXmlDoc.SelectSingleNode("//UseCarrierClaims");


                if (objXmlNode != null)
                {
                    if (m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                    {
                        objXmlNode.InnerText = "True";
                    }
                }
                return objXmlDoc;

            }


            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckUtility.Error", m_iClientId), p_objEx);
            }
        }


        /// <summary>
        /// Get the list of policy attached to claim.
        /// </summary>
        /// <param name="objXmlDoc">XmlDocument Object</param>
        public XmlDocument GetPolicyList(XmlDocument objXmlDoc)// stara Mits 16667 15/05/09
        {
            XmlElement xmlOption = null;
            XmlCDataSection objCData = null;
            int iTransId = 0;
            int iClaimId = 0;
            bool bConverted = false;
            DbReader objReader = null;
            string sSql = string.Empty;
            Policy objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
            LocalCache objLocalCache = null;
            XmlElement objPolicyListElement = null;
            int iPolicyId = 0;
            XmlAttribute xmlOptionAttrib = null;
            int iPolicyInput = 0;
            int iCovRowId = 0;
            int iCovTypeCode = 0;
            int iResTypeCode = 0;
            string sCovDesc = string.Empty;
            string sRestTypeCodeDesc = string.Empty;
            //rupal:start , for first & final payment
            bool bIsFirstFinal = false;
            //rupal:end, for first & final payment
            XmlElement oUnitListEle = null;
            //rupal:startpolicy system interface
            int iPolUnitRowId = 0;
            //Mona: Policy Interface: Implementing Loss Code
            int iRcRowId = 0;
            int iLossCode = 0;
            string sLossCodeDec = string.Empty;
            string sDisabilityCodeDesc = string.Empty;
            int iDisabilityCode = 0;
            int iLobCode = 0;
            int iCvgLossId = 0;
            try
            {
                //rupal:start , for first & final payment
                if (objXmlDoc.SelectSingleNode("//IsFirstFinal") != null)
                    bIsFirstFinal = Conversion.CastToType<bool>(objXmlDoc.SelectSingleNode("//IsFirstFinal").InnerText, out bConverted);
                //rupal:end, for first & final payment
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objPolicyListElement = (XmlElement)objXmlDoc.SelectSingleNode("//Policy");
                if (objXmlDoc.SelectSingleNode("//ClaimId") != null)
                    iClaimId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//ClaimId").InnerText, out bConverted);
                if (objXmlDoc.SelectSingleNode("//PolicyId") != null)
                    iPolicyInput = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//PolicyId").InnerText, out bConverted);
                //rupal:start,policy system interface
                objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(iPolicyInput);
                //rupal:end
                //sSql = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + iClaimId;
                if (objXmlDoc.SelectSingleNode("//LobCode") != null)
                    iLobCode = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//LobCode").InnerText, out bConverted);
                if (objXmlDoc.SelectSingleNode("//PolCvgID") != null)
                {
                    iCovRowId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//PolCvgID").InnerText, out bConverted);
                    //rupal:start,policy system interface
                    //sSql = "SELECT COVERAGE_TYPE_CODE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID =" + iCovRowId.ToString();
                    sSql = "SELECT COVERAGE_TYPE_CODE,POLICY_UNIT_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID =" + iCovRowId.ToString();
                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    //iCovTypeCode = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out bConverted);
                    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    {
                        if (oReader.Read())
                        {
                            iCovTypeCode = Conversion.CastToType<Int32>(oReader.GetValue(0).ToString(), out bConverted);
                            iPolUnitRowId = Conversion.CastToType<Int32>(oReader.GetValue(1).ToString(), out bConverted);
                        }
                    }
                    if (objPolicy.PolicySystemId > 0)
                    {
                        sCovDesc = GetCoverageText(iPolUnitRowId, iCovRowId, iCovTypeCode);
                    }
                    else
                    {
                        sCovDesc = objLocalCache.GetShortCode(iCovTypeCode) + " " + objLocalCache.GetCodeDesc(iCovTypeCode);
                    }
                    //rupal:end, policy system interface
                    xmlOption = objXmlDoc.CreateElement("CoverageTypeCode");
                    objCData = objXmlDoc.CreateCDataSection(sCovDesc);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    xmlOptionAttrib.Value = "COVERAGE_TYPE";
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    xmlOptionAttrib.Value = iCovTypeCode.ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objPolicyListElement.AppendChild(xmlOption);
                }
                //Mona: Policy Interface: Implementing Loss Code:Start
                if (objXmlDoc.SelectSingleNode("//RCRowID") != null)
                    iRcRowId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//RCRowID").InnerText, out bConverted);

                if (iRcRowId > 0)
                    sSql = "SELECT LOSS_CODE,COVERAGE_X_LOSS.POLCVG_ROW_ID,DISABILITY_CAT,CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS, RESERVE_CURRENT WHERE COVERAGE_X_LOSS.CVG_LOSS_ROW_ID=RESERVE_CURRENT.POLCVG_LOSS_ROW_ID AND RC_ROW_ID  =" + iRcRowId.ToString();
                else
                {
                    if (objXmlDoc.SelectSingleNode("//CvgLossID") != null)
                    {
                        iCvgLossId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//CvgLossID").InnerText, out bConverted);
                        sSql = "SELECT LOSS_CODE,POLCVG_ROW_ID,DISABILITY_CAT,CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE  CVG_LOSS_ROW_ID  =" + iCvgLossId.ToString();
                    }
                }

                if (iRcRowId > 0 || iCvgLossId > 0)
                {
                    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    {
                        if (oReader.Read())
                        {
                            iLossCode = Conversion.CastToType<Int32>(oReader.GetValue(0).ToString(), out bConverted);
                            iCovRowId = Conversion.CastToType<Int32>(oReader.GetValue(1).ToString(), out bConverted);
                            iDisabilityCode = Conversion.CastToType<Int32>(oReader.GetValue(2).ToString(), out bConverted);

                        }
                    }

                    sDisabilityCodeDesc = objLocalCache.GetShortCode(iDisabilityCode) + " " + objLocalCache.GetCodeDesc(iDisabilityCode);



                    xmlOption = objXmlDoc.CreateElement("DisabilityCatCode");


                    objCData = objXmlDoc.CreateCDataSection(sDisabilityCodeDesc);
                    xmlOption.AppendChild(objCData);

                    xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    xmlOptionAttrib.Value = objLocalCache.GetTableName(objLocalCache.GetCodeTableId(iDisabilityCode));
                    xmlOption.Attributes.Append(xmlOptionAttrib);

                    xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    xmlOptionAttrib.Value = iDisabilityCode.ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);

                    objPolicyListElement.AppendChild(xmlOption);





                    sLossCodeDec = objLocalCache.GetShortCode(iLossCode) + " " + objLocalCache.GetCodeDesc(iLossCode);


                    if (iLobCode != 243)
                    {

                        xmlOption = objXmlDoc.CreateElement("LossTypeCode");
                    }
                    else
                    {
                        xmlOption = objXmlDoc.CreateElement("DisabilityTypeCode");

                    }
                    objCData = objXmlDoc.CreateCDataSection(sLossCodeDec);
                    xmlOption.AppendChild(objCData);



                    xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    xmlOptionAttrib.Value = objLocalCache.GetTableName(objLocalCache.GetCodeTableId(iLossCode));
                    xmlOption.Attributes.Append(xmlOptionAttrib);

                    xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    xmlOptionAttrib.Value = iLossCode.ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);

                    objPolicyListElement.AppendChild(xmlOption);

                    //}
                    //else
                    //{
                    //     using (DbReader oReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    //     {
                    //         if (oReader.Read())
                    //         {
                    //             iCovTypeCode = Conversion.CastToType<Int32>(oReader.GetValue(0).ToString(), out bConverted);
                    //             iPolUnitRowId = Conversion.CastToType<Int32>(oReader.GetValue(1).ToString(), out bConverted);
                    //         }
                    //     }
                    //     if (objPolicy.PolicySystemId > 0)
                    //     {
                    //          sCovDesc = GetCoverageText(iPolUnitRowId, iCovRowId, iCovTypeCode);
                    //     }
                    //     else
                    //     {
                    //           sCovDesc = objLocalCache.GetShortCode(iCovTypeCode) + " " + objLocalCache.GetCodeDesc(iCovTypeCode);
                    //     }
                    //     //rupal:end, policy system interface
                    //}

                    //Mona: Policy Interface: Implementing Loss Code:End
                    //sCovDesc = objLocalCache.GetCodeDesc(iCovTypeCode);
                    //xmlOption = objXmlDoc.CreateElement("CoverageTypeCode");
                    //objCData = objXmlDoc.CreateCDataSection(sCovDesc);
                    //xmlOption.AppendChild(objCData);
                    //xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    //xmlOptionAttrib.Value = "COVERAGE_TYPE";
                    //xmlOption.Attributes.Append(xmlOptionAttrib);
                    //xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    //xmlOptionAttrib.Value = iCovTypeCode.ToString();
                    //xmlOption.Attributes.Append(xmlOptionAttrib);
                    //objPolicyListElement.AppendChild(xmlOption);
                }

                if (objXmlDoc.SelectSingleNode("//ResTypeCodeId") != null)
                {
                    iResTypeCode = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//ResTypeCodeId").InnerText, out bConverted);

                    sRestTypeCodeDesc = objLocalCache.GetCodeDesc(iResTypeCode);
                    xmlOption = objXmlDoc.CreateElement("ReserveTypeCode");
                    objCData = objXmlDoc.CreateCDataSection(sRestTypeCodeDesc);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    xmlOptionAttrib.Value = "RESERVE_TYPE";
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    xmlOptionAttrib.Value = iResTypeCode.ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objPolicyListElement.AppendChild(xmlOption);
                }
                objPolicyListElement = (XmlElement)objXmlDoc.SelectSingleNode("//PolicyList");
                if (iClaimId <= 0)
                {
                    object objClaimID = null;
                    if (objXmlDoc.SelectSingleNode("//TransId") != null)
                    {
                        iTransId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//TransId").InnerText, out bConverted);
                        sSql = "SELECT CLAIM_ID FROM FUNDS WHERE TRANS_ID =" + iTransId.ToString();
                        //objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                        //rupal: code optimized
                        //iClaimId = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out bConverted);
                    }
                    else
                    {
                        sSql = "SELECT CLAIM_ID FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_TRANS_ID IN ( SELECT AUTO_TRANS_ID FROM FUNDS_AUTO_SPLIT WHERE AUTO_SPLIT_ID =" + objXmlDoc.SelectSingleNode("//AutoSplitId").InnerText + ")";
                        //rupal: code optimized
                        //iClaimId = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out bConverted);
                    }
                    objClaimID = DbFactory.ExecuteScalar(m_sConnectionString, sSql);
                    if (objClaimID != null)
                        iClaimId = Conversion.CastToType<Int32>(objClaimID.ToString(), out bConverted);
                }
                //sSql = "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID =" + iClaimId.ToString();

                //rupal:start for first & final payment

                //following line commented by rupal
                //sSql = "SELECT DISTINCT(POLICY_ID) FROM RESERVE_CURRENT WHERE CLAIM_ID =" + iClaimId.ToString();

                //if carrier claim setting is on, we can make payments w/o having existing reserve,
                //policy may not exist in reserve_current table if reserve is not already created
                //we want to populate policy even if reserves are not created in case of first & final payment 
                //so we will populate policy from from CLAIM_X_POLICY 
                if (objPolicy.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0 && bIsFirstFinal == true)
                {
                    sSql = "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID =" + iClaimId.ToString();
                }
                else
                {
                    //Raman: Implementing Unit in R8

                    //sSql = "SELECT DISTINCT(POLICY_X_CVG_TYPE.POLICY_ID) FROM RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + iClaimId.ToString();
                    sSql = "SELECT DISTINCT(POLICY_X_UNIT.POLICY_ID) FROM RESERVE_CURRENT,POLICY_X_CVG_TYPE,POLICY_X_UNIT,COVERAGE_X_LOSS WHERE  RESERVE_CURRENT.POLCVG_LOSS_ROW_ID= COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND  COVERAGE_X_LOSS.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + iClaimId.ToString();
                }
                //rupal:end for first & final payment
                bool bFirstRecord = true;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iPolicyId = Conversion.CastToType<Int32>(objReader.GetValue("POLICY_ID").ToString(), out bConverted);
                        objPolicy.MoveTo(iPolicyId);
                        xmlOption = objXmlDoc.CreateElement("option");
                        objCData = objXmlDoc.CreateCDataSection(objPolicy.PolicyName);
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXmlDoc.CreateAttribute("value");
                        xmlOptionAttrib.Value = iPolicyId.ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        if (iPolicyId == iPolicyInput)
                        {
                            xmlOptionAttrib = objXmlDoc.CreateAttribute("selected");
                            xmlOptionAttrib.Value = "true";
                            xmlOption.Attributes.Append(xmlOptionAttrib);

                            oUnitListEle = objXmlDoc.CreateElement("UnitList");
                            oUnitListEle.SetAttribute("PolicyID", iPolicyId.ToString());

                            if (!bIsFirstFinal)
                            {
                                //GetUnitList(iPolicyId, iClaimId, iCovRowId);//rupal:mits 27459
                                GetUnitList(iPolicyId, iClaimId, iCovRowId, ref oUnitListEle, objPolicy.PolicySystemId);
                            }
                            else
                            {
                                //rupal:start,mits 27459
                                //GetUnitList(iPolicyId, ref oUnitListEle);
                                GetUnitList(iPolicyId, ref oUnitListEle, objPolicy.PolicySystemId, iClaimId);
                                //rupal:end
                            }
                            objPolicyListElement.ParentNode.AppendChild(oUnitListEle);
                        }
                        if (bFirstRecord && iPolicyInput == 0)
                        {
                            if (bIsFirstFinal)
                            {

                                xmlOptionAttrib = objXmlDoc.CreateAttribute("selected");
                                xmlOptionAttrib.Value = "true";
                                xmlOption.Attributes.Append(xmlOptionAttrib);

                                oUnitListEle = objXmlDoc.CreateElement("UnitList");
                                oUnitListEle.SetAttribute("PolicyID", iPolicyId.ToString());
                                //rupal:start,mits 27459
                                //GetUnitList(iPolicyId, ref oUnitListEle);
                                GetUnitList(iPolicyId, ref oUnitListEle, objPolicy.PolicySystemId, iClaimId);
                                //rupal:end
                                objPolicyListElement.ParentNode.AppendChild(oUnitListEle);

                                bFirstRecord = false;
                            }
                            else
                            {
                                xmlOptionAttrib = objXmlDoc.CreateAttribute("selected");
                                xmlOptionAttrib.Value = "true";
                                xmlOption.Attributes.Append(xmlOptionAttrib);

                                oUnitListEle = objXmlDoc.CreateElement("UnitList");
                                oUnitListEle.SetAttribute("PolicyID", iPolicyId.ToString());
                                //rupal:start, mits 27459
                                //GetUnitList(iPolicyId, iClaimId, iCovRowId, ref oUnitListEle);
                                GetUnitList(iPolicyId, iClaimId, iCovRowId, ref oUnitListEle, objPolicy.PolicySystemId);
                                //rupal:end
                                objPolicyListElement.ParentNode.AppendChild(oUnitListEle);

                                bFirstRecord = false;
                            }


                        }

                        objPolicyListElement.AppendChild(xmlOption);
                    }
                }

                return objXmlDoc;
            }


            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckUtility.Error", m_iClientId), p_objEx);
            }
            finally
            {
                xmlOption = null;
                objCData = null;
                objPolicy = null;
                objPolicyListElement = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }
        public XmlDocument GetPolicyListNew(XmlDocument objXmlDoc)// stara Mits 16667 15/05/09
        {
            XmlElement xmlOption = null;
            XmlCDataSection objCData = null;
            int iTransId = 0;
            int iClaimId = 0;
            bool bConverted = false;
            DbReader objReader = null;
            string sSql = string.Empty;
            Policy objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
            LocalCache objLocalCache = null;
            XmlElement objPolicyListElement = null;
            int iPolicyId = 0;
            XmlAttribute xmlOptionAttrib = null;
            int iPolicyInput = 0;
            int iCovRowId = 0;
            //Mona: Policy Interface: Implementing Loss Code
            int iCovLossId = 0;
            int iLossCode = 0;
            string sLossCodeDec = string.Empty;
            //Mona
            int iCovTypeCode = 0;
            int iResTypeCode = 0;
            string sCovDesc = string.Empty;
            string sRestTypeCodeDesc = string.Empty;
            //rupal:start , for first & final payment
            bool bIsFirstFinal = false;
            //rupal:end, for first & final payment
            XmlElement oUnitListEle = null;
            //rupal:startpolicy system interface
            int iPolUnitRowId = 0;
            try
            {
                //rupal:start , for first & final payment
                if (objXmlDoc.SelectSingleNode("//IsFirstFinal") != null)
                    bIsFirstFinal = Conversion.CastToType<bool>(objXmlDoc.SelectSingleNode("//IsFirstFinal").InnerText, out bConverted);
                //rupal:end, for first & final payment
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objPolicyListElement = (XmlElement)objXmlDoc.SelectSingleNode("//Policy");
                if (objXmlDoc.SelectSingleNode("//ClaimId") != null)
                    iClaimId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//ClaimId").InnerText, out bConverted);
                if (objXmlDoc.SelectSingleNode("//PolicyId") != null)
                    iPolicyInput = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//PolicyId").InnerText, out bConverted);
                //rupal:start,policy system interface
                objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(iPolicyInput);
                //rupal:end

                //Mona: Policy Interface: Implementing Loss Code:Start
                if (objXmlDoc.SelectSingleNode("//CvgLossID") != null)
                {

                    iCovLossId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//CvgLossID").InnerText, out bConverted);
                    sSql = "SELECT LOSS_CODE,POLCVG_ROW_ID FROM COVERAGE_X_LOSS WHERE CVG_LOSS_ROW_ID  =" + iCovLossId.ToString();
                    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    {
                        if (oReader.Read())
                        {
                            iLossCode = Conversion.CastToType<Int32>(oReader.GetValue(0).ToString(), out bConverted);
                            iCovRowId = Conversion.CastToType<Int32>(oReader.GetValue(1).ToString(), out bConverted);
                        }
                    }

                    sLossCodeDec = objLocalCache.GetCodeDesc(iLossCode);

                    xmlOption = objXmlDoc.CreateElement("LossTypeCode");
                    objCData = objXmlDoc.CreateCDataSection(sLossCodeDec);
                    xmlOption.AppendChild(objCData);

                    xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    xmlOptionAttrib.Value = objLocalCache.GetTableName(objLocalCache.GetCodeTableId(iLossCode));
                    xmlOption.Attributes.Append(xmlOptionAttrib);

                    xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    xmlOptionAttrib.Value = iLossCode.ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);

                    objPolicyListElement.AppendChild(xmlOption);
                }
                //Mona: Policy Interface: Implementing Loss Code:End

                if (iCovRowId != 0)
                {
                    // iCovRowId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//PolCvgID").InnerText, out bConverted);
                    //rupal:start,policy system interface
                    //sSql = "SELECT COVERAGE_TYPE_CODE FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID =" + iCovRowId.ToString();
                    sSql = "SELECT COVERAGE_TYPE_CODE,POLICY_UNIT_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLCVG_ROW_ID =" + iCovRowId.ToString();
                    //objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    //iCovTypeCode = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out bConverted);
                    using (DbReader oReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    {
                        if (oReader.Read())
                        {
                            iCovTypeCode = Conversion.CastToType<Int32>(oReader.GetValue(0).ToString(), out bConverted);
                            iPolUnitRowId = Conversion.CastToType<Int32>(oReader.GetValue(1).ToString(), out bConverted);
                        }
                    }
                    if (objPolicy.PolicySystemId > 0)
                    {
                        sCovDesc = GetCoverageText(iPolUnitRowId, iCovRowId, iCovTypeCode);
                    }
                    else
                    {
                        sCovDesc = objLocalCache.GetCodeDesc(iCovTypeCode);
                    }
                    //rupal:end, policy system interface
                    xmlOption = objXmlDoc.CreateElement("CoverageTypeCode");
                    objCData = objXmlDoc.CreateCDataSection(sCovDesc);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    xmlOptionAttrib.Value = "COVERAGE_TYPE";
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    xmlOptionAttrib.Value = iCovTypeCode.ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objPolicyListElement.AppendChild(xmlOption);
                }

                if (objXmlDoc.SelectSingleNode("//ResTypeCodeId") != null)
                {
                    iResTypeCode = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//ResTypeCodeId").InnerText, out bConverted);

                    sRestTypeCodeDesc = objLocalCache.GetCodeDesc(iResTypeCode);
                    xmlOption = objXmlDoc.CreateElement("ReserveTypeCode");
                    objCData = objXmlDoc.CreateCDataSection(sRestTypeCodeDesc);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("tablename");
                    xmlOptionAttrib.Value = "RESERVE_TYPE";
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    xmlOptionAttrib = objXmlDoc.CreateAttribute("codeid");
                    xmlOptionAttrib.Value = iResTypeCode.ToString();
                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objPolicyListElement.AppendChild(xmlOption);
                }
                objPolicyListElement = (XmlElement)objXmlDoc.SelectSingleNode("//PolicyList");
                if (iClaimId <= 0)
                {
                    object objClaimID = null;
                    if (objXmlDoc.SelectSingleNode("//TransId") != null)
                    {
                        iTransId = Conversion.CastToType<Int32>(objXmlDoc.SelectSingleNode("//TransId").InnerText, out bConverted);
                        sSql = "SELECT CLAIM_ID FROM FUNDS WHERE TRANS_ID =" + iTransId.ToString();
                        //objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                        //rupal: code optimized
                        //iClaimId = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out bConverted);
                    }
                    else
                    {
                        sSql = "SELECT CLAIM_ID FROM FUNDS_AUTO WHERE FUNDS_AUTO.AUTO_TRANS_ID IN ( SELECT AUTO_TRANS_ID FROM FUNDS_AUTO_SPLIT WHERE AUTO_SPLIT_ID =" + objXmlDoc.SelectSingleNode("//AutoSplitId").InnerText + ")";
                        //rupal: code optimized
                        //iClaimId = Conversion.CastToType<Int32>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out bConverted);
                    }
                    objClaimID = DbFactory.ExecuteScalar(m_sConnectionString, sSql);
                    if (objClaimID != null)
                        iClaimId = Conversion.CastToType<Int32>(objClaimID.ToString(), out bConverted);
                }
                if (objPolicy.Context.InternalSettings.SysSettings.MultiCovgPerClm != 0 && bIsFirstFinal == true)
                {
                    sSql = "SELECT POLICY_ID FROM CLAIM_X_POLICY WHERE CLAIM_ID =" + iClaimId.ToString();
                }
                else
                {
                    //Raman: Implementing Unit in R8

                    //sSql = "SELECT DISTINCT(POLICY_X_CVG_TYPE.POLICY_ID) FROM RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + iClaimId.ToString();
                    sSql = "SELECT DISTINCT(POLICY_X_UNIT.POLICY_ID) FROM RESERVE_CURRENT,POLICY_X_CVG_TYPE,POLICY_X_UNIT WHERE RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND RESERVE_CURRENT.CLAIM_ID =" + iClaimId.ToString();
                }
                //rupal:end for first & final payment
                bool bFirstRecord = true;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iPolicyId = Conversion.CastToType<Int32>(objReader.GetValue("POLICY_ID").ToString(), out bConverted);
                        objPolicy.MoveTo(iPolicyId);
                        xmlOption = objXmlDoc.CreateElement("option");
                        objCData = objXmlDoc.CreateCDataSection(objPolicy.PolicyName);
                        xmlOption.AppendChild(objCData);
                        xmlOptionAttrib = objXmlDoc.CreateAttribute("value");
                        xmlOptionAttrib.Value = iPolicyId.ToString();
                        xmlOption.Attributes.Append(xmlOptionAttrib);
                        if (iPolicyId == iPolicyInput)
                        {
                            xmlOptionAttrib = objXmlDoc.CreateAttribute("selected");
                            xmlOptionAttrib.Value = "true";
                            xmlOption.Attributes.Append(xmlOptionAttrib);

                            oUnitListEle = objXmlDoc.CreateElement("UnitList");
                            oUnitListEle.SetAttribute("PolicyID", iPolicyId.ToString());

                            if (!bIsFirstFinal)
                            {
                                //GetUnitList(iPolicyId, iClaimId, iCovRowId);//rupal:mits 27459
                                GetUnitList(iPolicyId, iClaimId, iCovRowId, ref oUnitListEle, objPolicy.PolicySystemId);
                            }
                            else
                            {
                                //rupal:start,mits 27459
                                //GetUnitList(iPolicyId, ref oUnitListEle);
                                GetUnitList(iPolicyId, ref oUnitListEle, objPolicy.PolicySystemId, iClaimId);
                                //rupal:end
                            }
                            objPolicyListElement.ParentNode.AppendChild(oUnitListEle);
                        }
                        if (bFirstRecord && iPolicyInput == 0)
                        {
                            if (bIsFirstFinal)
                            {

                                xmlOptionAttrib = objXmlDoc.CreateAttribute("selected");
                                xmlOptionAttrib.Value = "true";
                                xmlOption.Attributes.Append(xmlOptionAttrib);

                                oUnitListEle = objXmlDoc.CreateElement("UnitList");
                                oUnitListEle.SetAttribute("PolicyID", iPolicyId.ToString());
                                //rupal:start,mits 27459
                                //GetUnitList(iPolicyId, ref oUnitListEle);
                                GetUnitList(iPolicyId, ref oUnitListEle, objPolicy.PolicySystemId, iClaimId);
                                //rupal:end
                                objPolicyListElement.ParentNode.AppendChild(oUnitListEle);

                                bFirstRecord = false;
                            }
                            else
                            {
                                xmlOptionAttrib = objXmlDoc.CreateAttribute("selected");
                                xmlOptionAttrib.Value = "true";
                                xmlOption.Attributes.Append(xmlOptionAttrib);

                                oUnitListEle = objXmlDoc.CreateElement("UnitList");
                                oUnitListEle.SetAttribute("PolicyID", iPolicyId.ToString());
                                //rupal:start, mits 27459
                                //GetUnitList(iPolicyId, iClaimId, iCovRowId, ref oUnitListEle);
                                GetUnitList(iPolicyId, iClaimId, iCovRowId, ref oUnitListEle, objPolicy.PolicySystemId);
                                //rupal:end
                                objPolicyListElement.ParentNode.AppendChild(oUnitListEle);

                                bFirstRecord = false;
                            }


                        }

                        objPolicyListElement.AppendChild(xmlOption);
                    }
                }

                return objXmlDoc;
            }


            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPolicyListNew.Error", m_iClientId), p_objEx);
            }
            finally
            {
                xmlOption = null;
                objCData = null;
                objPolicy = null;
                objPolicyListElement = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }
        //rupal:start,policy system interface
        private string GetCoverageText(int iPolicyUnitRowId, int iPolicyCvgRowId, int iCoverageTypeCode)
        {
            string sCoverageText = string.Empty;
            StringBuilder sSQL = null;
            try
            {
                sSQL = new StringBuilder();
                sSQL.Append(" SELECT DISTINCT");

                sSQL.Append("  COVERAGE_TEXT");
                sSQL.Append(" FROM ");
                sSQL.Append(" ( ");
                sSQL.Append(" SELECT CODE_DESC, COVERAGE_TEXT, C.COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO,B.CNT FROM ");
                sSQL.Append(" ( ");
                sSQL.Append(" SELECT COVERAGE_TYPE_CODE,COUNT(*) CNT FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE=" + iCoverageTypeCode + " GROUP BY COVERAGE_TYPE_CODE");
                sSQL.Append(" )B,");
                sSQL.Append(" (");
                sSQL.Append(" SELECT POLICY_X_CVG_TYPE.POLCVG_ROW_ID, CODE_DESC, COVERAGE_TEXT, SHORT_CODE,COVERAGE_TYPE_CODE ,CVG_SEQUENCE_NO, TRANS_SEQ_NO, COVERAGE_KEY");       //Ankit Start : Worked on MITS - 34297
                sSQL.Append(" FROM POLICY_X_CVG_TYPE,CODES_TEXT");
                sSQL.Append(" WHERE");
                sSQL.Append(" POLICY_UNIT_ROW_ID =" + iPolicyUnitRowId + " AND POLCVG_ROW_ID=" + iPolicyCvgRowId);
                sSQL.Append(" AND CODES_TEXT.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE	");
                sSQL.Append(" ) C WHERE B.COVERAGE_TYPE_CODE = C.COVERAGE_TYPE_CODE");
                sSQL.Append(" )A ");
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                {
                    if (objReader.Read())
                    {
                        sCoverageText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                    }
                }
                return sCoverageText;

            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("ReserveFunds.GetReservesBOB.GenericError", m_iClientId), objException);
            }
            finally
            {
                sSQL = null;
            }
        }
        //rupal:end

        /// <summary>
        /// Get the list of units attached to Policy 
        /// </summary>
        /// <param name="objXmlDoc">XmlDocument Object</param>
        //public void GetUnitList(long lPolicyId, ref XmlElement oEle)//rupal:mits 27459
        public void GetUnitList(long lPolicyId, ref XmlElement oEle, int iPolicySystemId, int iClaimId)
        {

            long lPolicyUnitRowId = 0;
            string sUnit = "";
            XmlElement oChildEle = null;
            XmlCDataSection objCData = null;
            StringBuilder sbSQL;
            string sDBType = string.Empty;
            SysSettings objSysSetting;
            try
            {
                //rupal:start, policy system interface changes,
                //formatted the query and appended joins in the query for site unit and other unit
                sbSQL = new StringBuilder();
                //for MITS 31997 start
                if (m_objDataModelFactory == null)
                    this.Initialize();
                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                //for MITS 31997 end

                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

                if (objSysSetting.MultiCovgPerClm == -1)
                {
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL = sbSQL.Append("SELECT DISTINCT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, ISNULL(VEHICLE.VEH_DESC,VEHICLE.VIN) Unit");
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL = sbSQL.Append("SELECT DISTINCT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, nvl(VEHICLE.VEH_DESC,VEHICLE.VIN) Unit");
                    }
                }
                else
                {
                    sbSQL = sbSQL.Append("SELECT DISTINCT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, VEHICLE.VIN Unit");
                }
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                if (iPolicySystemId > 0)
                {
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN UNIT_X_CLAIM ON UNIT_X_CLAIM.UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='V' AND POLICY_X_UNIT.POLICY_ID = {0} AND UNIT_X_CLAIM.CLAIM_ID={1} ", lPolicyId.ToString(), iClaimId.ToString()));
                }
                sbSQL = sbSQL.Append(" INNER JOIN VEHICLE ON VEHICLE.UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString());

                sbSQL = sbSQL.Append(" UNION");
                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, PROPERTY_UNIT.PIN Unit"
                    );
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                if (iPolicySystemId > 0)
                {
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_PROPERTYLOSS ON CLAIM_X_PROPERTYLOSS.PROPERTY_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID={0} AND CLAIM_X_PROPERTYLOSS.CLAIM_ID={1} ", lPolicyId.ToString(), iClaimId.ToString()));
                }
                sbSQL = sbSQL.Append(" INNER JOIN PROPERTY_UNIT ON PROPERTY_UNIT.PROPERTY_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString());


                // as of now site and other unit will be available for policy system interface only                        
                if (iPolicySystemId > 0)
                {
                    sbSQL = sbSQL.Append(" UNION");

                    //SITE UNIT - S
                    sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, SITE_UNIT.NAME Unit");
                    sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_SITELOSS ON CLAIM_X_SITELOSS.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_SITELOSS.CLAIM_ID={1} ", lPolicyId.ToString(), iClaimId.ToString()));
                    sbSQL = sbSQL.Append(" INNER JOIN SITE_UNIT ON SITE_UNIT.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString());

                    //OTHER UNIT - SU
                    sbSQL = sbSQL.Append(" UNION");
                    // changes for MITS 31997 begin
                    if (sDBType == Constants.DB_SQLSRVR)
                        //dbisht6 start for mits 35655
                        sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) Unit");
                    //sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, (ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'')) Unit");
                    //dbisht6 end
                    else if (sDBType == Constants.DB_ORACLE)
                        // sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, (nvl(ENTITY.FIRST_NAME,'') || ' ' || nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,'')) Unit"); 
                        //dbisht6 start of mits 35655
                        sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,POINT_UNIT_DATA.STAT_UNIT_NUMBER || ' - ' || nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,''))))) Unit");
                    //dbisht6 end
                    // changes for MITS 31997 end
                    sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_OTHERUNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='SU' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_OTHERUNIT.CLAIM_ID={1} ", lPolicyId.ToString(), iClaimId.ToString()));
                    sbSQL = sbSQL.Append(" INNER JOIN OTHER_UNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=OTHER_UNIT.OTHER_UNIT_ID");
                    sbSQL = sbSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID=OTHER_UNIT.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");
                    //dbisht6 start mits of 35655
                    sbSQL = sbSQL.Append(" INNER JOIN POINT_UNIT_DATA ON	OTHER_UNIT.OTHER_UNIT_ID = POINT_UNIT_DATA.UNIT_ID and POINT_UNIT_DATA.UNIT_TYPE = OTHER_UNIT.UNIT_TYPE ");
                    //dbisht6 end

                }

                /*
                //rupal:start, mits 27459
                if (iPolicySystemId > 0 && iClaimId > 0)
                {
                    sSql = string.Format("SELECT v.VIN UNIT , p.POLICY_UNIT_ROW_ID FROM VEHICLE v , POLICY_X_UNIT p, UNIT_X_CLAIM U WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND p.POLICY_ID = {0} AND U.UNIT_ID = P.UNIT_ID AND U.CLAIM_ID = {1} ) UNION SELECT u.PIN UNIT , p.POLICY_UNIT_ROW_ID  FROM PROPERTY_UNIT u , POLICY_X_UNIT p, CLAIM_X_PROPERTYLOSS CPL WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND p.POLICY_ID = {0} AND CPL.PROPERTY_ID=P.UNIT_ID AND CPL.CLAIM_ID={1}) UNION SELECT wc.EMPLOYEE_DETAILS UNIT , p.POLICY_UNIT_ROW_ID FROM WC_POLICY_EMPLOYEE wc , POLICY_X_UNIT p WHERE (p.UNIT_TYPE = 'E' AND wc.ROW_ID = p.UNIT_ID AND  p.POLICY_ID = {0})", lPolicyId.ToString(), iClaimId.ToString());
                }
                else
                {
                    sSql = string.Format("SELECT v.VIN UNIT , p.POLICY_UNIT_ROW_ID FROM VEHICLE v , POLICY_X_UNIT p WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND p.POLICY_ID = {0} ) UNION SELECT u.PIN UNIT , p.POLICY_UNIT_ROW_ID  FROM PROPERTY_UNIT u , POLICY_X_UNIT p WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND p.POLICY_ID = {0}) UNION SELECT wc.EMPLOYEE_DETAILS UNIT , p.POLICY_UNIT_ROW_ID FROM WC_POLICY_EMPLOYEE wc , POLICY_X_UNIT p WHERE (p.UNIT_TYPE = 'E' AND wc.ROW_ID = p.UNIT_ID AND  p.POLICY_ID = {0}) ", lPolicyId.ToString());
                }
                //rupal:end*/
                //rupal:end, policy system interface changes
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            lPolicyUnitRowId = Conversion.ConvertStrToLong(objReader.GetValue("POLICY_UNIT_ROW_ID").ToString());
                            sUnit = objReader.GetString("Unit");
                            oChildEle = oEle.OwnerDocument.CreateElement("option");
                            objCData = oEle.OwnerDocument.CreateCDataSection(sUnit);
                            oChildEle.AppendChild(objCData);
                            oChildEle.SetAttribute("value", lPolicyUnitRowId.ToString());
                            oEle.AppendChild(oChildEle);
                        }
                    }
                }
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckUtility.Error", m_iClientId), p_objEx);
            }
            finally
            {
                sbSQL = null;
                objSysSetting = null;
            }

        }

        /// <summary>
        /// Get the list of units attached to Policy for a Claim.
        /// </summary>
        /// <param name="objXmlDoc">XmlDocument Object</param>
        //public void GetUnitList(long lPolicyId , long lClaimId , long lUnitCvgRowIdInput , ref XmlElement oEle) //RUPAL:MITS 27459
        public void GetUnitList(long lPolicyId, long lClaimId, long lUnitCvgRowIdInput, ref XmlElement oEle, int iPolicySystemId)
        {
            long lPolicyUnitRowId = 0;
            string sUnit = "";
            XmlElement oChildEle = null;
            long lUnitCvgRowId = 0;
            XmlCDataSection objCData = null;
            StringBuilder sbSQL;
            string sDBType = string.Empty;
            SysSettings objSysSettings = null;
            try
            {

                //rupal:start, policy system interface changes,
                //formatted the query and appended joins in the query for site unit and other unit
                sbSQL = new StringBuilder();

                if (m_objDataModelFactory == null)
                    this.Initialize();
                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud

                if (objSysSettings.MultiCovgPerClm == -1)
                {

                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, ISNULL(VEHICLE.VEH_DESC, VEHICLE.VIN) Unit");
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, nvl(VEHICLE.VEH_DESC, VEHICLE.VIN) Unit");
                    }

                }
                else
                {
                    sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, VEHICLE.VIN Unit");
                }
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                if (iPolicySystemId > 0)
                {
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN UNIT_X_CLAIM ON UNIT_X_CLAIM.UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='V' AND POLICY_X_UNIT.POLICY_ID = {0} AND UNIT_X_CLAIM.CLAIM_ID={1} ", lPolicyId.ToString(), lClaimId.ToString()));
                }
                sbSQL = sbSQL.Append(" INNER JOIN VEHICLE ON VEHICLE.UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID");
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + lClaimId.ToString());


                sbSQL = sbSQL.Append(" UNION");
                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, PROPERTY_UNIT.PIN Unit"
                    );
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                if (iPolicySystemId > 0)
                {
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_PROPERTYLOSS ON CLAIM_X_PROPERTYLOSS.PROPERTY_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID={0} AND CLAIM_X_PROPERTYLOSS.CLAIM_ID={1} ", lPolicyId.ToString(), lClaimId.ToString()));
                }
                sbSQL = sbSQL.Append(" INNER JOIN PROPERTY_UNIT ON PROPERTY_UNIT.PROPERTY_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID");
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + lClaimId.ToString());

                // as of now site and other unit will be available for policy system interface only                        
                //tanwar2 - Policy From Staging - Site unit should be available
                if (iPolicySystemId > 0)
                //if (iPolicySystemId > 0)
                {
                    sbSQL = sbSQL.Append(" UNION");

                    //SITE UNIT - S
                    sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, SITE_UNIT.NAME Unit");
                    sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_SITELOSS ON CLAIM_X_SITELOSS.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_SITELOSS.CLAIM_ID={1} ", lPolicyId.ToString(), lClaimId.ToString()));
                    sbSQL = sbSQL.Append(" INNER JOIN SITE_UNIT ON SITE_UNIT.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString());
                    sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID");
                    sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + lClaimId.ToString());

                    //OTHER UNIT - SU
                    sbSQL = sbSQL.Append(" UNION ");
                    //changes for MITS 31997 start
                    if (sDBType == Constants.DB_SQLSRVR)
                        //dbisht6 start for mits 35655
                        sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) Unit ");
                    //sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, (ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,'')) Unit");
                    //dbisht6 end
                    else if (sDBType == Constants.DB_ORACLE)
                        sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID PolUnitRowId,POINT_UNIT_DATA.STAT_UNIT_NUMBER || ' - ' || nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((nvl(ENTITY.FIRST_NAME,'') || ' '|| nvl(ENTITY.MIDDLE_NAME,'') ||' '|| nvl(ENTITY.LAST_NAME,''))))) Unit");
                    //changes for MITS 31997 end
                    sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                    sbSQL = sbSQL.Append(string.Format(" INNER JOIN CLAIM_X_OTHERUNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='SU' AND POLICY_X_UNIT.POLICY_ID = {0} AND CLAIM_X_OTHERUNIT.CLAIM_ID={1} ", lPolicyId.ToString(), lClaimId.ToString()));
                    sbSQL = sbSQL.Append(" INNER JOIN OTHER_UNIT ON CLAIM_X_OTHERUNIT.OTHER_UNIT_ID=OTHER_UNIT.OTHER_UNIT_ID");
                    sbSQL = sbSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID=OTHER_UNIT.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");
                    sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID");
                    sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND RESERVE_CURRENT.CLAIM_ID = " + lClaimId.ToString());
                    //dbisht6 start mits of 35655
                    sbSQL = sbSQL.Append(" INNER JOIN POINT_UNIT_DATA ON	OTHER_UNIT.OTHER_UNIT_ID = POINT_UNIT_DATA.UNIT_ID and POINT_UNIT_DATA.UNIT_TYPE = OTHER_UNIT.UNIT_TYPE ");
                    //dbisht6 end

                }
                /*
                //rupal:start, mits 27549
                if (iPolicySystemId > 0)
                {
                    sSql = string.Format("SELECT DISTINCT v.VIN UNIT , p.POLICY_UNIT_ROW_ID FROM VEHICLE v , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE, UNIT_X_CLAIM U  WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID = {0} AND RESERVE_CURRENT.CLAIM_ID = {1} AND U.UNIT_ID=P.UNIT_ID AND U.CLAIM_ID = {1}) UNION SELECT DISTINCT u.PIN UNIT , p.POLICY_UNIT_ROW_ID FROM PROPERTY_UNIT u , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE,CLAIM_X_PROPERTYLOSS CPL WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID ={0} AND RESERVE_CURRENT.CLAIM_ID = {1} AND CPL.PROPERTY_ID=P.UNIT_ID AND CPL.CLAIM_ID= {1} ) UNION SELECT DISTINCT wc.EMPLOYEE_DETAILS UNIT , p.POLICY_UNIT_ROW_ID FROM WC_POLICY_EMPLOYEE wc , POLICY_X_UNIT p ,   RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE  (p.UNIT_TYPE = 'E' AND wc.ROW_ID = p.UNIT_ID   AND RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND   POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND  p.POLICY_ID ={0} AND RESERVE_CURRENT.CLAIM_ID = {1}) ", lPolicyId.ToString(), lClaimId.ToString());
                }
                else
                {
                    sSql = string.Format("SELECT DISTINCT v.VIN UNIT , p.POLICY_UNIT_ROW_ID FROM VEHICLE v , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID = {0} AND RESERVE_CURRENT.CLAIM_ID = {1}) UNION SELECT DISTINCT u.PIN UNIT , p.POLICY_UNIT_ROW_ID FROM PROPERTY_UNIT u , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID ={0} AND RESERVE_CURRENT.CLAIM_ID = {1}) UNION SELECT DISTINCT wc.EMPLOYEE_DETAILS UNIT , p.POLICY_UNIT_ROW_ID FROM WC_POLICY_EMPLOYEE wc , POLICY_X_UNIT p ,   RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE  (p.UNIT_TYPE = 'E' AND wc.ROW_ID = p.UNIT_ID   AND RESERVE_CURRENT.POLCVG_ROW_ID = POLICY_X_CVG_TYPE.POLCVG_ROW_ID AND   POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND  p.POLICY_ID ={0} AND RESERVE_CURRENT.CLAIM_ID = {1}) ", lPolicyId.ToString(), lClaimId.ToString());
                }
                 */
                //rupal:end
                //rupal:start, policy system interface changes,
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            lPolicyUnitRowId = Conversion.ConvertStrToLong(objReader.GetValue("POLICY_UNIT_ROW_ID").ToString());
                            sUnit = objReader.GetString("Unit");
                            oChildEle = oEle.OwnerDocument.CreateElement("option");
                            objCData = oEle.OwnerDocument.CreateCDataSection(sUnit);
                            oChildEle.AppendChild(objCData);
                            //oChildEle.InnerText = sUnit;
                            oChildEle.SetAttribute("value", lPolicyUnitRowId.ToString());
                            string sSql2 = String.Format("SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID = {0}", lPolicyUnitRowId.ToString());
                            using (DbReader objReader2 = DbFactory.GetDbReader(m_sConnectionString, sSql2))
                            {
                                if (objReader2 != null)
                                {
                                    while (objReader2.Read())
                                    {
                                        lUnitCvgRowId = Conversion.ConvertStrToLong(objReader2.GetValue("POLCVG_ROW_ID").ToString());
                                        if (lUnitCvgRowIdInput == lUnitCvgRowId)
                                        {
                                            oChildEle.SetAttribute("selected", "true");
                                            continue;
                                        }
                                    }
                                }
                            }


                            oEle.AppendChild(oChildEle);
                        }
                    }
                }
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckUtility.Error", m_iClientId), p_objEx);
            }
            finally
            {
                sbSQL = null;
                objCData = null;
                oChildEle = null;
                objSysSettings = null;
            }

        }

        /// <summary>
        /// Get the list of units attached to Policy for a Claim.
        /// </summary>
        /// <param name="objXmlDoc">XmlDocument Object</param>
        public string GetSelectedUnit(long lPolicyId, long lUnitCvgRowId, out long lPolicyUnitRowId)
        {
            lPolicyUnitRowId = 0;
            string sUnit = "";
            StringBuilder sbSQL;
            try
            {

                //rupal:start, policy system interface changes,
                //formatted the query and appended joins in the query for site unit and other unit
                sbSQL = new StringBuilder();
                sbSQL = sbSQL.Append("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, VEHICLE.VIN Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                sbSQL = sbSQL.Append(" INNER JOIN VEHICLE ON VEHICLE.UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());


                sbSQL = sbSQL.Append(" UNION");
                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, PROPERTY_UNIT.PIN Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                sbSQL = sbSQL.Append(" INNER JOIN PROPERTY_UNIT ON PROPERTY_UNIT.PROPERTY_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());

                // as of now site and other unit will be available for policy system interface only                        

                sbSQL = sbSQL.Append(" UNION");

                //SITE UNIT - S
                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, SITE_UNIT.NAME Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                sbSQL = sbSQL.Append(" INNER JOIN SITE_UNIT ON SITE_UNIT.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());

                //OTHER UNIT - SU
                sbSQL = sbSQL.Append(" UNION");
                sbSQL = sbSQL.Append(" SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID POLICY_UNIT_ROW_ID, ENTITY.LAST_NAME Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                sbSQL = sbSQL.Append(" INNER JOIN OTHER_UNIT ON OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='SU' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID=OTHER_UNIT.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());


                /*/
                //rupal:start, mits 27459
                //sSql = string.Format("SELECT v.VIN UNIT , p.POLICY_UNIT_ROW_ID FROM VEHICLE v , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID = {0} ) UNION SELECT DISTINCT u.PIN UNIT , p.POLICY_UNIT_ROW_ID FROM PROPERTY_UNIT u , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID ={0}) ", lPolicyId.ToString(), lUnitCvgRowId.ToString());
                sSql = string.Format("SELECT v.VIN UNIT FROM VEHICLE v , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND p.POLICY_UNIT_ROW_ID={2} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID = {0} ) UNION SELECT DISTINCT u.PIN UNIT FROM PROPERTY_UNIT u , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND p.POLICY_UNIT_ROW_ID = {2} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID ={0}) UNION SELECT DISTINCT wc.EMPLOYEE_DETAILS UNIT FROM WC_POLICY_EMPLOYEE wc , POLICY_X_UNIT p ,     RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE      (p.UNIT_TYPE = 'E' AND wc.ROW_ID = p.UNIT_ID  AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND p.POLICY_UNIT_ROW_ID = {2} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID ={0}) ", lPolicyId.ToString(), lUnitCvgRowId.ToString(), lPolicyUnitRowId.ToString());
                //rupal:end
                 */
                //RUPAL:END, POLICY SYSTEM INTERFACE CHANGES
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            lPolicyUnitRowId = Conversion.ConvertStrToLong(objReader.GetValue("POLICY_UNIT_ROW_ID").ToString());
                            sUnit = objReader.GetString("Unit");

                        }
                    }
                }
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckUtility.Error", m_iClientId), p_objEx);
            }
            finally
            {
                sbSQL = null;
            }
            return sUnit;

        }



        /// <summary>
        /// Determines whether any split item is a BRS item and hence should cause
        /// the creation of a top-level invoice object.  Then create the object if necessary.
        /// </summary>
        private void CreateFundsInvoice()
        {
            Object objJunk = null;

            try
            {
                foreach (FundsTransSplit objTempFundsTransSplit in m_objFunds.TransSplitList)
                {
                    if (this.IsSplitItemDeleted(objTempFundsTransSplit.SplitRowId))
                        continue;
                    if (objTempFundsTransSplit.Crc == 1234567890)
                    {
                        objJunk = m_objFunds.BrsInvoice;
                        break;
                    }
                }

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateFundsInvoice.ErrorInvoice", m_iClientId), p_objEx);
            }
            finally
            {
                objJunk = null;
            }
        }

        /// <summary>
        /// Save Voided Rollup.
        /// </summary>
        private void SaveVoidedRollup()
        {
            ArrayList arrlstTransIds = null;
            DbReader objReader = null;
            string sSQL = "";
            int iIndex = 0;

            try
            {
                arrlstTransIds = new ArrayList();

                sSQL = "SELECT TRANS_ID FROM FUNDS WHERE ROLLUP_ID = " + m_objFunds.RollupId.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        arrlstTransIds.Add(Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId));
                    }
                }

                for (iIndex = 0; iIndex < arrlstTransIds.Count; iIndex++)
                {
                    m_objFunds.MoveTo((int)arrlstTransIds[iIndex]);
                    m_objFunds.VoidDate = Conversion.ToDbDate(System.DateTime.Now);
                    m_objFunds.VoidFlag = true;
                    m_objFunds.Save();
                    //Start:Export payment void data to VSS
                    Claim objClaim = null;
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(m_objFunds.ClaimId);
                    //if (objClaim.VssClaimInd)
                    //Use utility Check Instead of Claim Check
                    if (objClaim.Context.InternalSettings.SysSettings.EnableVSS.Equals(-1))
                    {
                        VssExportAsynCall objVss = new VssExportAsynCall(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                        objVss.AsynchVssReserveExport(m_objFunds.ClaimId, m_objFunds.ClaimantEid, 0, m_objFunds.TransId, 0, "", "", "", "", "PaymentVoid");
                        objVss = null;
                    }
                    objClaim.Dispose();
                    //End:Export payment void data to VSS
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.SaveVoidedRollup.ErrorRollUp", m_iClientId), p_objEx);
            }
            finally
            {
                arrlstTransIds = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        #endregion

        # region Validate Data
        /// <summary>
        /// Validate the Data.
        /// </summary>
        /// <param name="p_objFunds">Funds Object</param>
        /// <returns>Valid Flag</returns>
        public bool ValidateData(Funds p_objFunds)
        {
            bool bValidateData = false;
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                m_objFunds = p_objFunds;
                bValidateData = this.ValidateData();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ValidateData.ErrorValidate", m_iClientId), p_objEx);
            }
            return (bValidateData);
        }
        private ArrayList GetLOBPerClaimLmtReserves()
        {
            ArrayList arrlstCodes = null;
            DbReader objReader = null;
            LOBPerClaimLmtReserves structPerClaimLmtReserves;


            string sSQL = "";

            try
            {
                arrlstCodes = new ArrayList();
                sSQL = " SELECT LINE_OF_BUS_CODE,RES_TYPE_CODE, MAX(MAX_AMOUNT) ,CLAIM_TYPE_CODE FROM CL_RES_PAY_LMTS WHERE USER_ID = " + UserId.ToString();
                if (m_iUserGroupId > 0)
                {
                    sSQL += "  OR GROUP_ID = " + m_iUserGroupId.ToString();
                }
                sSQL += " GROUP BY LINE_OF_BUS_CODE,RES_TYPE_CODE,CLAIM_TYPE_CODE";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        structPerClaimLmtReserves = new LOBPerClaimLmtReserves();
                        structPerClaimLmtReserves.LineOfBusiness = Common.Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                        structPerClaimLmtReserves.ReserveTypeCode = Common.Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);
                        structPerClaimLmtReserves.MaxAmount = Common.Conversion.ConvertObjToDouble(objReader.GetValue(2), m_iClientId);
                        structPerClaimLmtReserves.ClaimTypeCode = Common.Conversion.ConvertObjToInt(objReader.GetValue(3), m_iClientId);
                        arrlstCodes.Add(structPerClaimLmtReserves);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetLOBPerClaimLmtReserves.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (arrlstCodes);
        }
        private bool GetMaxPerClaimReserveAmount(int p_iLob, int p_iReserveCode, int p_iClaimType, ref double p_dblAmount, ref int p_iReserve)
        {
            try
            {

                ArrayList objList = null;
                LOBPerClaimLmtReserves objTempLOB;
                objList = GetLOBPerClaimLmtReserves();
                int iCode = 0;
                for (int i = 0; i < objList.Count; i++)
                {
                    objTempLOB = (LOBPerClaimLmtReserves)objList[i];
                    iCode = objTempLOB.ReserveTypeCode;
                    if (iCode != 0)
                    {
                        if (objTempLOB.LineOfBusiness == p_iLob && objTempLOB.ReserveTypeCode == p_iReserveCode
                            && objTempLOB.ClaimTypeCode == p_iClaimType)
                        {
                            p_dblAmount = objTempLOB.MaxAmount;
                            p_iReserve = objTempLOB.ReserveTypeCode;
                            return true;
                        }
                        if (objTempLOB.LineOfBusiness == p_iLob && objTempLOB.ReserveTypeCode == p_iReserveCode
                            && objTempLOB.ClaimTypeCode == 0)
                        {
                            p_dblAmount = objTempLOB.MaxAmount;
                            p_iReserve = objTempLOB.ReserveTypeCode;
                            return true;
                        }
                    }
                    else
                    {

                        if (objTempLOB.LineOfBusiness == p_iLob && objTempLOB.ClaimTypeCode == p_iClaimType)
                        {
                            p_dblAmount = objTempLOB.MaxAmount;
                            p_iReserve = objTempLOB.ReserveTypeCode;
                            return true;
                        }
                        if (objTempLOB.LineOfBusiness == p_iLob && objTempLOB.ClaimTypeCode == 0)
                        {
                            p_dblAmount = objTempLOB.MaxAmount;
                            p_iReserve = objTempLOB.ReserveTypeCode;
                            return true;
                        }
                    }
                }


            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetMaxPerClaimReserveAmount.Error", m_iClientId), p_objEx);
            }
            return false;
        }
        private double GetPaidTotal(int p_iClaimId, int p_iCode, int p_iClaimantEid, int p_iUnitId)
        {
            //start  7810
            /*DbReader objReader = null;
            string sSQL = "";
            double dPaidTotal = 0;
            sSQL = " SELECT SUM(PAID_TOTAL) FROM RESERVE_CURRENT WHERE CLAIM_ID= " + p_iClaimId.ToString();
            if (p_iCode != 0)
            {
                sSQL += "  AND  RESERVE_TYPE_CODE = " + p_iCode.ToString();
            }
            if (p_iClaimantEid != 0)
            {
                sSQL += "  AND  CLAIMANT_EID = " + p_iClaimantEid.ToString();
            }
            if (p_iUnitId != 0)
            {
                sSQL += "  AND  UNIT_ID = " + p_iUnitId.ToString();
            }
            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    dPaidTotal = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                }
            }
            if (objReader != null)
                objReader.Close();
            objReader = null;
            return dPaidTotal;*/
           
            DbReader objReader = null;
            string sSQL = string.Empty;
            double dPaidTotal = 0;
            string sDBType = string.Empty;
            sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
            sSQL = "SELECT SUM(FUNDS_TRANS_SPLIT.AMOUNT) FROM FUNDS, FUNDS_TRANS_SPLIT WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ";
            sSQL += "AND FUNDS.CLAIM_ID = " + p_iClaimId.ToString() + " AND FUNDS.VOID_FLAG = 0 ";//rsushilaggar MITS 28278 Date 05/11/2012
            if (m_objFunds.TransId > 0)
            {
                sSQL += "AND FUNDS.TRANS_ID != " + m_objFunds.TransId.ToString();
            }
            //sSQL = "SELECT SUM(PAID_TOTAL) FROM RESERVE_CURRENT WHERE CLAIM_ID= " + p_iClaimId.ToString();
            if (p_iCode != 0)
            {
                sSQL += "  AND  FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE = " + p_iCode.ToString();
                //sSQL += "  AND  RESERVE_TYPE_CODE = " + p_iCode.ToString();
            }
            if (p_iClaimantEid != 0)
            {
                sSQL += "  AND  FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString();
                //sSQL += "  AND  CLAIMANT_EID = " + p_iClaimantEid.ToString();
            }
            if (p_iUnitId != 0)
            {
                sSQL += "  AND  FUNDS.UNIT_ID = " + p_iUnitId.ToString();
                //sSQL += "  AND  UNIT_ID = " + p_iUnitId.ToString();
            }
            //rupal
            if (sDBType == Constants.DB_SQLSRVR)
            {
                sSQL += "  AND  FUNDS_TRANS_SPLIT.POLCVG_ROW_ID in( SELECT TOP 1 POLCVG_ROW_ID FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID= " + m_objFunds.TransId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iCode.ToString() + ")";
                //rupal
            }
            else if (sDBType == Constants.DB_ORACLE)
            {
                sSQL += "  AND  FUNDS_TRANS_SPLIT.POLCVG_ROW_ID in( SELECT POLCVG_ROW_ID FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID= " + m_objFunds.TransId.ToString() + " AND  rownum=1 and  RESERVE_TYPE_CODE = " + p_iCode.ToString() + ")";
            }

            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
            if (objReader != null)
            {
                if (objReader.Read())
                {
                    dPaidTotal = Conversion.ConvertObjToDouble(objReader.GetValue(0), m_iClientId);
                }
            }
            if (objReader != null)
                objReader.Close();
            objReader = null;
            return dPaidTotal;
            //End  7810
        }
        private int GetClaimType(int p_iClaimId)
        {
            Claim objClaim = null;
            int iClaimTypeCode = 0;
            try
            {
                if (p_iClaimId == 0)
                    p_iClaimId = m_objFunds.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);
                    iClaimTypeCode = objClaim.ClaimTypeCode;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetClaimType.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
            }
            return (iClaimTypeCode);
        }
        //kchoudhary2, 12th Dec, MITS 8431 starts
        /// <summary>
        /// This method would print the copyright information on the report.		
        /// </summary>		
        private void PrintCopyright()
        {
            string sDate = String.Empty;
            string sTime = String.Empty;
            DateTime dttm = DateTime.Now;
            try
            {
                m_objPrintPDF.PrintCopyright();
                sDate = dttm.ToString("d");
                sTime = dttm.ToString("HH:mm:ss");

                m_objPrintPDF.SetFontUnderline(true);
                m_objPrintPDF.SetFontBold(true);
                m_objPrintPDF.SetFontSize(8);

                SetTabIndex(2);

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Note:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();
                m_objPrintPDF.SetFontUnderline(false);

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "     Proprietary & Confidential Data", true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();


                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "     Current as of " + sDate + " " + sTime, true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();


                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "     Transaction Details (TM)", true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();


                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, String.Format("     {0}", m_MessageSettings["DEF_RMCOPYRIGHT"].ToString()), true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();


                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, String.Format("     {0}", m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString()), true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();


                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "     Copies May be Used Only by Authorized Licensed Users.", true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.PrintCopyright.ErrorPrint", m_iClientId), p_objException);
            }
            finally
            {
                m_arrlstTabAt = null;
                m_arrlstReportData = null;
            }
        }
        /// <summary>
        /// This method would take in the Transaction Id for which the Report needs to be generated.
        /// Then the data would be fetched for that Transaction id 
        /// and PDF report would be generated.
        /// The PDF report would be rendered back to a memory stream.
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>Memory stream containing the report data</returns>
        public MemoryStream CreateTransReport(int itransID)
        {
            MemoryStream objPDFMemoryStream = null;
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (itransID > 0)
                {
                    m_objFunds.MoveTo(itransID);
                    //Rahul - mits 10339 - 18 sept 2007 
                    m_objAccounts.MoveTo(m_objFunds.AccountId);
                }
                else
                    m_objFunds.MoveLast();
                TransactionReport();
                PrintCopyright();
                objPDFMemoryStream = m_objPrintPDF.RenderToStream();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateTransReport.ErrorGenerate", m_iClientId), p_objException);
            }
            finally
            {
                m_objPrintPDF = null;
                m_arrlstTabAt = null;
                m_arrlstReportData = null;
            }
            return objPDFMemoryStream;
        }
        /// <summary>
        /// Print the transaction details
        /// </summary>
        /// <returns>Valid Flag</returns>
        internal void TransactionReport()
        {
            LocalCache objLocalCache = null;

            try
            {
                m_objPrintPDF = new PrintPDF(m_iClientId);
                m_objPrintPDF.SetFont("Arial");
                m_objPrintPDF.SetLineSpacing(250);
                m_objPrintPDF.SetFontSize(10);
                //Deb
                m_objPrintPDF.SetConnectionString(m_sConnectionString);
                //Deb
                m_arrlstTabAt = new ArrayList();
                m_arrlstReportData = new ArrayList();
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                m_objPrintPDF.PrintCenter("Transaction Details", true, 10, true);
                m_objPrintPDF.PrintCenter("", true, 10, false);
                m_objPrintPDF.PrintCenter("", true, 10, false);

                SetTabIndex(8);

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Control Number:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.CtlNumber, false, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Type:", true, false);
                if (m_objFunds.PaymentFlag)
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Payment", false, false);
                }
                else if (m_objFunds.CollectionFlag)
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Collection", false, false);
                }
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Void:", true, false);
                if (m_objFunds.VoidFlag)
                {
                    //Ankit Start : Worked on MITS - 33629
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, Conversion.GetDBDateFormat(m_objFunds.VoidDate, "d"), false, false);
                    //Ankit End
                }
                else
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "No", false, false);
                }
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Cleared:", true, false);
                if (m_objFunds.ClearedFlag)
                {
                    //Ankit Start : Worked on MITS - 33629
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, Conversion.GetDBDateFormat(m_objFunds.VoidDate, "d"), false, false);
                    //Ankit End
                }
                else
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "No", false, false);
                }
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                SetTabIndex(2);

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Claim Number:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.ClaimNumber, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Transaction Date:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, Conversion.GetDBDateFormat(m_objFunds.TransDate, "d"), false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Bank Account:", true, false);
                //GeneratePrintString(GlobalTransDetConstant.CHARSTRING,objLocalCache.GetCodeDesc(m_objFunds.AccountId),false,false);
                //rahul mits 10339 - 19 sept 2007  AccountDetail
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objAccounts.AccountName, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                if (m_objFunds.PaymentFlag)
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Payee Type:", true, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetShortCode(m_objFunds.PayeeTypeCode) + " - " + objLocalCache.GetCodeDesc(m_objFunds.PayeeTypeCode), false, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();
                }
                else if (m_objFunds.CollectionFlag)
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Payor Type:", true, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetShortCode(m_objFunds.PayeeTypeCode) + " - " + objLocalCache.GetCodeDesc(m_objFunds.PayeeTypeCode), false, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();
                }

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Last Name:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.LastName, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "First Name:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.FirstName, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Tax ID:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.PayeeEntity.TaxId, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Address:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.Addr1 + " " + m_objFunds.Addr2 + " " + m_objFunds.Addr3 + " " + m_objFunds.Addr4, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "City:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.City, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "State:", true, false);

                string sStateAbb = string.Empty;
                string sStateName = string.Empty;
                objLocalCache.GetStateInfo(m_objFunds.StateId, ref sStateAbb, ref sStateName);
                if (!string.IsNullOrEmpty(sStateAbb))
                    sStateName = sStateAbb + " " + sStateName;
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, sStateName, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Zip:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.ZipCode, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                SetTabIndex(6);

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Enclosure:", true, false);
                if (m_objFunds.EnclosureFlag)
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Yes", false, false);
                else
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "No", false, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Auto Check:", true, false);
                if (m_objFunds.AutoCheckFlag)
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Yes", false, false);
                else
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "No", false, false);
                //Ashish Ahuja 12282
                //GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Include Claimant In Payee:", true, false);
                //if (m_objFunds.SettlementFlag)
                //    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Yes", false, false);
                //else
                //    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "No", false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                SetTabIndex(2);

                if (m_objFunds.PaymentFlag)
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Check Status:", true, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetCodeDesc(m_objFunds.StatusCode), false, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();

                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Check Date:", true, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, Conversion.GetDBDateFormat(m_objFunds.DateOfCheck, "d"), false, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();

                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Check Number:", true, false);
                    if (m_objFunds.TransNumber != 0)
                    {
                        GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.TransNumber.ToString(), false, false);
                    }
                    else
                    {
                        GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", false, false);
                    }
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();

                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Check Memo:", true, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.CheckMemo, false, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();
                }
                else if (m_objFunds.CollectionFlag)
                {
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Date:", true, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, Conversion.GetDBDateFormat(m_objFunds.DateOfCheck, "d"), false, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();

                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Deposit Number:", true, false);
                    if (m_objFunds.TransNumber != 0)
                    {
                        GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.TransNumber.ToString(), false, false);
                    }
                    else
                    {
                        GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", false, false);
                    }
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();
                }

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Transaction Notes:", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, m_objFunds.Notes, false, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Transaction Detail:", true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                m_objPrintPDF.PrintCenter("", true, 10, false);
                m_objPrintPDF.PrintCenter("", true, 10, false);

                SetTabIndex(4);

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Transaction Type", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Reserve Type", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Amount", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "From/To", true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

                m_objPrintPDF.PrintCenter("", true, 10, false);

                SetTabIndex(4);
                m_objPrintPDF.SetFontBold(false);


                double dAmount = 0;
                //Start - Added by Nikhil on 07/30/14 to include Deductible recovery split for first party payment
                int iCovRowId = 0;
                int iDedType = 0;
                bool success;
                int iDedRecTransId = 0;
                Funds objRecCollection = null;
                SysSettings objSysSetting = null;
                string sAmount = string.Empty;
                string sSql = string.Empty;

                //End - Added by Nikhil on 07/30/14 to include Deductible recovery split for first party payment

                foreach (FundsTransSplit objtransSplit in m_objFunds.TransSplitList)
                {
                    m_objPrintPDF.SetFontSize(10);
                    SetTabIndex(4);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetCodeDesc(objtransSplit.TransTypeCode), false, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetCodeDesc(objtransSplit.ReserveTypeCode), false, false);
                    GeneratePrintString(GlobalTransDetConstant.MONEY, objtransSplit.Amount.ToString(), false, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, Conversion.GetDBDateFormat(objtransSplit.FromDate, "d") + "-" + Conversion.GetDBDateFormat(objtransSplit.ToDate, "d"), false, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();

                    SetTabIndex(5);
                    m_objPrintPDF.SetFontSize(8);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "GL Account:", false, false);
                    if (objtransSplit.GlAccountCode != 0)
                    {
                        // rahul - mits 10339 - 18 sept 2007 
                        GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetShortCode(objtransSplit.GlAccountCode) + " - " + objLocalCache.GetCodeDesc(objtransSplit.GlAccountCode), false, false);
                    }
                    else
                    {
                        GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", false, false);
                    }
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Invoiced By:", false, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objtransSplit.InvoicedBy, false, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", true, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();

                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Invoiced Number:", false, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objtransSplit.InvoiceNumber, false, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Invoice Amount:", false, false);
                    GeneratePrintString(GlobalTransDetConstant.MONEY, objtransSplit.InvoiceAmount.ToString(), false, false);
                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", true, false);
                    m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                    m_arrlstReportData.Clear();

                    m_objPrintPDF.PrintCenter("", true, 10, false);
                    //tanwar2 - Negative split changed to collection - start
                    //dAmount += objtransSplit.Amount;
                    if (objtransSplit.Context.LocalCache.GetRelatedCodeId(objtransSplit.ReserveTypeCode) ==
                            objtransSplit.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE"))
                    {
                        dAmount -= objtransSplit.Amount;
                    }
                    else
                    {
                        dAmount += objtransSplit.Amount;
                    }
                    //tanwar2 - end

                    //Added by Nikhil
                    iCovRowId = objtransSplit.CoverageId;
                }
                //Added by Nikhil on 07/30/14 to include Deductible recovery split for first party payment
                objRecCollection = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                //Start: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014

                // sbhatnagar21 - RMA JIRA - RMA-9265
                if (m_objFunds.LineOfBusCode > 0 && objRecCollection.Context.InternalSettings.ColLobSettings[m_objFunds.LineOfBusCode] != null && objRecCollection.Context.InternalSettings.ColLobSettings[m_objFunds.LineOfBusCode].ApplyDedToPaymentsFlag == -1)
                //End: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014
                {
                    sSql = string.Format("SELECT DED_TYPE_CODE FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} AND POLCVG_ROW_ID={1}", m_objFunds.ClaimId, iCovRowId);
                    // iDedType = this.Context.DbConnLookup.ExecuteInt(string.Format("SELECT DED_TYPE_CODE FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} AND POLCVG_ROW_ID={1}", m, p_iPolCvgRowId));
                    //Start: Changed the following code by Sumit Agarwal on 11/25/2014 for NI as SQL may return NULL records
                    //iDedType = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out success);
                    //if (success && iDedType == m_objFunds.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"))
                    iDedType = DbFactory.ExecuteAsType<int>(m_sConnectionString, sSql);
                    if (iDedType == m_objFunds.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"))
                    //End: Changed the following code by Sumit Agarwal on 11/25/2014 for NI as SQL may return NULL records
                    {
                        sSql = string.Format("SELECT DED_COL_TRANS_ID FROM FUNDS_X_DED_COL_MAP WHERE DED_PAY_TRANS_ID={0}", m_objFunds.TransId);
                        //Start: Changed the following code by Sumit Agarwal on 11/25/2014 for NI as SQL may return NULL records
                        //iDedRecTransId = Conversion.CastToType<int>(DbFactory.ExecuteScalar(m_sConnectionString, sSql).ToString(), out success);
                        //if (success && iDedRecTransId > 0)
                        iDedRecTransId = DbFactory.ExecuteAsType<int>(m_sConnectionString, sSql);
                        if (iDedRecTransId > 0)
                        //End: Changed the following code by Sumit Agarwal on 11/25/2014 for NI as SQL may return NULL records
                        {

                            // objRecCollection= (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                            objRecCollection.MoveTo(iDedRecTransId);

                            foreach (FundsTransSplit objtransSplit in objRecCollection.TransSplitList)
                            {
                                m_objPrintPDF.SetFontSize(10);
                                SetTabIndex(4);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetCodeDesc(objtransSplit.TransTypeCode), false, false);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetCodeDesc(objtransSplit.ReserveTypeCode), false, false);
                                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);
                                //Start: Changed the following code by Sumit Agarwal as if the currencey symbol is added to the amount, later on ConvertStrtoDouble set this amount to 0: 10/29/2014
                                //sAmount = CommonFunctions.ConvertByCurrencyCode(objSysSetting.BaseCurrencyType, -1 * objtransSplit.Amount, m_sConnectionString);
                                sAmount = (-1 * objtransSplit.Amount).ToString();
                                //End: Changed the following code by Sumit Agarwal as if the currencey symbol is added to the amount, later on ConvertStrtoDouble set this amount to 0: 10/29/2014
                                GeneratePrintString(GlobalTransDetConstant.MONEY, sAmount, false, false);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, Conversion.GetDBDateFormat(objtransSplit.FromDate, "d") + "-" + Conversion.GetDBDateFormat(objtransSplit.ToDate, "d"), false, false);
                                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                                m_arrlstReportData.Clear();

                                SetTabIndex(5);
                                m_objPrintPDF.SetFontSize(8);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "GL Account:", false, false);
                                if (objtransSplit.GlAccountCode != 0)
                                {
                                    // rahul - mits 10339 - 18 sept 2007 
                                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objLocalCache.GetShortCode(objtransSplit.GlAccountCode) + " - " + objLocalCache.GetCodeDesc(objtransSplit.GlAccountCode), false, false);
                                }
                                else
                                {
                                    GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", false, false);
                                }
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Invoiced By:", false, false);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objtransSplit.InvoicedBy, false, false);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", true, false);
                                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                                m_arrlstReportData.Clear();

                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Invoiced Number:", false, false);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, objtransSplit.InvoiceNumber, false, false);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Invoice Amount:", false, false);
                                GeneratePrintString(GlobalTransDetConstant.MONEY, objtransSplit.InvoiceAmount.ToString(), false, false);
                                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", true, false);
                                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                                m_arrlstReportData.Clear();

                                m_objPrintPDF.PrintCenter("", true, 10, false);

                                //tanwar2 - Negative split changed to collection - start
                                //dAmount += objtransSplit.Amount;
                                if (objtransSplit.Context.LocalCache.GetRelatedCodeId(objtransSplit.ReserveTypeCode) ==
                                        objtransSplit.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE"))
                                {
                                    dAmount -= objtransSplit.Amount;
                                }
                                else
                                {
                                    dAmount += objtransSplit.Amount;
                                }
                                //tanwar2 - end
                            }



                        }

                    }



                }
                //Added by Nikhil on 07/30/14 to include Deductible recovery split for first party payment


                m_objPrintPDF.SetFontSize(10);
                SetTabIndex(4);

                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "Total:", true, false);
                GeneratePrintString(GlobalTransDetConstant.MONEY, dAmount.ToString().Trim(), true, false);
                GeneratePrintString(GlobalTransDetConstant.CHARSTRING, "", true, false);
                m_objPrintPDF.PrintString(m_arrlstTabAt, m_arrlstReportData);
                m_arrlstReportData.Clear();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.TransactionReport.ErrorGenerate", m_iClientId), p_objException);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }
        /// <summary>
        /// This method would build the structure
        /// containing data and the related information like data type, alignment, style (bold).
        /// This structure would be added to the arraylist that would be used for printing a string on the report.
        /// </summary>	
        /// <param name="p_iDatatype">Data Type</param>		
        /// <param name="p_sData">Data String</param>		
        /// <param name="p_bSetBold">Bold (True/False)</param>		
        /// <param name="p_bRightAlign">Right Align (True/False)</param>		
        private void GeneratePrintString(int p_iDatatype, string p_sData, bool p_bSetBold, bool p_bRightAlign)
        {
            try
            {
                m_structReportData.DataType = p_iDatatype;
                m_structReportData.Data = p_sData;
                m_structReportData.SetBold = p_bSetBold;
                m_structReportData.RightAlign = p_bRightAlign;
                m_arrlstReportData.Add(m_structReportData);
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GeneratePrintString.ErrorGen", m_iClientId), p_objException);
            }
        }
        /// <summary>
        /// This method would set the starting position for each column to be printed on report.
        /// </summary>		
        /// <param name="p_iNoofColumns">Number of Columns</param>
        private void SetTabIndex(int p_iNoofColumns)
        {
            try
            {
                switch (p_iNoofColumns)
                {
                    case 1:
                        m_arrlstTabAt.Clear();
                        m_arrlstTabAt.Add(350);
                        break;
                    case 2:
                        m_arrlstTabAt.Clear();
                        m_arrlstTabAt.Add(150);
                        m_arrlstTabAt.Add(2050);
                        break;
                    case 3:
                        m_arrlstTabAt.Clear();
                        m_arrlstTabAt.Add(150);
                        m_arrlstTabAt.Add(5150);
                        m_arrlstTabAt.Add(8150);
                        break;
                    case 4:
                        m_arrlstTabAt.Clear();
                        m_arrlstTabAt.Add(150);
                        m_arrlstTabAt.Add(3300);
                        m_arrlstTabAt.Add(6450);
                        m_arrlstTabAt.Add(9600);
                        break;
                    case 5:
                        m_arrlstTabAt.Clear();
                        m_arrlstTabAt.Add(150);
                        m_arrlstTabAt.Add(1550);
                        m_arrlstTabAt.Add(5150);
                        m_arrlstTabAt.Add(6450);
                        m_arrlstTabAt.Add(8150);
                        break;
                    case 6:
                        m_arrlstTabAt.Clear();
                        m_arrlstTabAt.Add(150);
                        m_arrlstTabAt.Add(1390);
                        m_arrlstTabAt.Add(3725);
                        m_arrlstTabAt.Add(5025);
                        m_arrlstTabAt.Add(7000);
                        m_arrlstTabAt.Add(9600);
                        break;
                    case 8:
                        m_arrlstTabAt.Clear();
                        m_arrlstTabAt.Add(150);
                        m_arrlstTabAt.Add(2050);//1550
                        m_arrlstTabAt.Add(4000);
                        m_arrlstTabAt.Add(4600);
                        m_arrlstTabAt.Add(5900);
                        m_arrlstTabAt.Add(6490);
                        m_arrlstTabAt.Add(7800);
                        m_arrlstTabAt.Add(8630);
                        break;
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.SetTabIndex.ErrorSet", m_iClientId), p_objException);
            }
        }
        //kchoudhary2, 12th Dec, MITS 8431 ends -- 

        /// <summary>
        /// Validate the Data.
        /// </summary>
        /// <returns>Valid Flag</returns>
        private bool ValidateData()
        {
            DbReader objReader = null;
            LocalCache objLocalCache = null;
            CCacheFunctions objCCacheFunctions = null;
            ColLobSettings objColLobSettings = null;
            LobSettings objLobSettings = null;
            ArrayList arrlstReserveAmount = null;
            ArrayList arrlstTransId = null;
            SysSettings objSysSettings = null;

            ReserveAmount structReserveAmount;
            string strError = "";
            //TR2546: Raman Bhatia - We already have a global variable for sHoldText which is being used to create diaries
            //local variable is causing the problems..lets stick to one global variable for sHoldText
            //string sHoldText = "" ;
            string sShortCode = "";
            string sDesc = "";
            string sSQL = "";
            string sFirstName = "";
            string sTaxId = "";

            bool bValidData = true;
            bool bCreateHold = false;
            bool bClientCreateHold; //Added: Mohit for Bug No. 001094
            bool bFound = false;

            //Shruti for EMI changes
            int iStateId = 0;
            int iIndex = 0;
            int iPayeeEid = 0;
            int iAdjId; //Added: Mohit for Bug No. 001094
            int iReserveTypeCode = 0;
            double dblBalance = 0.0;
            double dblTransTotal = 0.0;
            double dblAmount = 0.0;
            double dblTempAmount = 0.0;

            try
            {
                arrlstReserveAmount = new ArrayList();
                arrlstTransId = new ArrayList();

                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);

                ClearErrors();

                if (m_objFunds.TransId != 0 && m_objFunds.ClearedFlag && (!m_objFunds.VoidFlag))
                    return (bValidData);

                ReadCheckOptions();

                switch (m_objFunds.LineOfBusCode)
                {
                    case 241:
                        if (m_objFunds.ClaimId != 0)
                        {
                            if (objColLobSettings[241].ReserveTracking == 1)
                            {
                                if (m_objFunds.ClaimantEid == 0)
                                {
                                    strError = "A claimant must be selected for GC payments. Detail Level Tracking is enabled.";
                                    m_arrlstErrors.Add(strError);
                                    return (false);
                                }
                            }
                        }
                        break;
                    case 242:
                        if (m_objFunds.ClaimId != 0)
                        {
                            if (objColLobSettings[242].ReserveTracking == 1)
                            {
                                // Defect no. 1879 The error should come in case no unit or claimant is selected
                                if (m_objFunds.ClaimantEid == 0 && m_objFunds.UnitId == 0)
                                {
                                    strError = "A claimant or vehicle/unit must be selected for VA payments. Detail Level Tracking is enabled.";
                                    m_arrlstErrors.Add(strError);
                                    return (false);
                                }
                            }
                        }
                        break;
                    case 243:
                        //commenting check that only claimant can be selected for WC payment..bug no 1420
                        /*
                        if( m_objFunds.ClaimId != 0 )
                        {
                            if( m_objFunds.ClaimantEid == 0 )
                            {
                                strError = "A claimant must be selected for WC payments." ;
                                m_arrlstErrors.Add( strError );
                                return( false );									
                            }
						
                        }
                        */
                        break;
                    case 844:
                        //commenting check that only claimant can be selected for WC payment..bug no 1420
                        /*

                        if( m_objFunds.ClaimId != 0 )
                        {
                            if( m_objFunds.ClaimantEid == 0 )
                            {
                                strError = "A claimant must be selected for DI payments." ;
                                m_arrlstErrors.Add( strError );
                                return( false );									
                            }
						
                        }
                        */
                        break;
                }// end switch

                //Check for permissions to save without claim attached,
                // Vaibhav has commented the code as per the fucntionality of RMWorld.
                //				if( m_objFunds.ClaimId == 0 )
                //				{
                //					strError = "You must select a claim for this payment before saving." ;
                //					m_arrlstErrors.Add( strError );
                //					return( false );
                //				}

                foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                {
                    if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                        continue;
                    dblAmount += objFundsTransSplit.Amount;
                }

                m_objFunds.Amount = dblAmount;
                if (m_objFunds.PaymentFlag)
                {
                    objLobSettings = objColLobSettings[m_objFunds.LineOfBusCode];
                    if (objLobSettings != null && objLobSettings.PayLmtFlag)
                    {
                        if (GetMaxPayAmount(m_objFunds.LineOfBusCode, ref dblTempAmount))
                        {
                            if (dblAmount > dblTempAmount)
                            {
                                if (m_bPlaceSupHoldPayLimits)
                                {
                                    bCreateHold = true;
                                    //sHoldText = " The amount of this payment exceeds your limit." ;//zalam 04/21/2008 Mits:-11576
                                    sHoldText = "The amount of this payment exceeded the user's limit and requires manager's approval.";
                                }
                                else
                                {
                                    strError = "You do not have permission to save a payment of this amount.";
                                    m_arrlstErrors.Add(strError);
                                    return (false);
                                }
                            }
                        }
                    }
                    //Begin:Added: Mohit for bug no. 001094
                    bClientCreateHold = false;

                    if (!WithinClientLimit(m_objFunds.Amount, m_objFunds.LineOfBusCode, m_objFunds.ClaimId))
                    {
                        bClientCreateHold = true;
                        if (m_bUseSpecificSupervisor && m_objFunds.TransId == 0)
                        {
                            iAdjId = GetAdjusterEID(m_objFunds.ClaimId);

                            if (iAdjId > 0) //We have a current adjuster who has a RM user id assigned.  We go after this users limits and work our way up the supervisory chain
                            {
                                //***********Start:Added Condition by Mohit Yadav for MITS #14252 ****************************
                                if (iAdjId != m_iUserId)
                                    m_objFunds.ApproverId = GetApprovalID(iAdjId, m_objFunds.Amount, m_objFunds.LineOfBusCode);
                                else
                                    m_objFunds.ApproverId = GetApprovalID(GetManagerID(iAdjId), m_objFunds.Amount, m_objFunds.LineOfBusCode);//We might end-up having GetManagerID(iAdjId) as 0
                                //***********End:Added Condition by Mohit Yadav for MITS #14252 ******************************

                                /***********Start:Commented by Mohit Yadav for MITS #14252 ***********************************
                                m_objFunds.ApproverId = GetApprovalID(iAdjId, m_objFunds.Amount, m_objFunds.LineOfBusCode);
                                ************End:Commented by Mohit Yadav for MITS #14252 *************************************/
                            }
                            else //We do not have a current adjuster with a RM user Id Assigned.  We will go after the limits for the user who is logged on and work our way up there supervisory chain
                                m_objFunds.ApproverId = GetApprovalID(m_iManagerID, m_objFunds.Amount, m_objFunds.LineOfBusCode);
                        }
                        else
                        {
                            m_objFunds.ApproverId = GetApprovalID(m_iManagerID, m_objFunds.Amount, m_objFunds.LineOfBusCode);
                        }
                    }
                    else
                    {
                        // || condition added by Shivendu for MITS 11121
                        if ((m_bUseSpecificSupervisor || m_bUseQueuedPayments || (m_objFunds.StatusCode == objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS"))) && m_objFunds.TransId == 0)
                        {
                            iAdjId = GetAdjusterEID(m_objFunds.ClaimId);

                            if (iAdjId > 0) //We have a current adjuster who has a RM user id assigned.  We go after this users limits and work our way up the supervisory chain
                            {
                                //***********Start:Added Condition by Mohit Yadav for MITS #14252 ****************************
                                if (iAdjId != m_iUserId)
                                    m_objFunds.ApproverId = GetApprovalID(iAdjId, m_objFunds.Amount, m_objFunds.LineOfBusCode);
                                else
                                    m_objFunds.ApproverId = GetApprovalID(GetManagerID(iAdjId), m_objFunds.Amount, m_objFunds.LineOfBusCode);//We might end-up having GetManagerID(iAdjId) as 0
                                //***********End:Added Condition by Mohit Yadav for MITS #14252 ******************************

                                /***********Start:Commented by Mohit Yadav for MITS #14252 ***********************************
                                m_objFunds.ApproverId = GetApprovalID(iAdjId, m_objFunds.Amount, m_objFunds.LineOfBusCode);
                                ************End:Commented by Mohit Yadav for MITS #14252 *************************************/
                            }
                            else //We do not have a current adjuster with a RM user Id Assigned.  We will go after the limits for the user who is logged on and work our way up there supervisory chain
                                m_objFunds.ApproverId = GetApprovalID(m_iManagerID, m_objFunds.Amount, m_objFunds.LineOfBusCode);

                            bClientCreateHold = false;
                        }
                        else if (m_objFunds.TransId == 0)
                        {
                            m_objFunds.ApproverId = GetApprovalID(m_iManagerID, m_objFunds.Amount, m_objFunds.LineOfBusCode);
                        }
                    }

                    if (bClientCreateHold)
                    {
                        sHoldText += " The amount of this payment exceeds the Client Payment limit.";
                        bCreateHold = true;
                    }

                    //End:Added: Mohit for bug no. 001094
                    if ((objLobSettings != null && objLobSettings.PayDetLmtFlag) || (objLobSettings != null && objLobSettings.InsufFundsFlag))
                    {
                        foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                        {
                            if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                continue;
                            bFound = false;
                            for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                            {
                                structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];
                                if (structReserveAmount.ReserveTypeCode == objFundsTransSplit.ReserveTypeCode)
                                {
                                    //Raman Bhatia..modifying amount below was not updating the arraylist
                                    //structReserveAmount.Amount += objFundsTransSplit.Amount ;
                                    double dTempAmount = structReserveAmount.Amount + objFundsTransSplit.Amount;

                                    arrlstReserveAmount.RemoveAt(iIndex);
                                    structReserveAmount = new ReserveAmount();
                                    structReserveAmount.ReserveTypeCode = objFundsTransSplit.ReserveTypeCode;
                                    structReserveAmount.Amount = dTempAmount;
                                    arrlstReserveAmount.Insert(iIndex, structReserveAmount);
                                    bFound = true;
                                }
                            }
                            if (!bFound)
                            {
                                structReserveAmount = new ReserveAmount();
                                structReserveAmount.ReserveTypeCode = objFundsTransSplit.ReserveTypeCode;
                                structReserveAmount.Amount = objFundsTransSplit.Amount;
                                arrlstReserveAmount.Add(structReserveAmount);
                            }
                            dblTransTotal += objFundsTransSplit.Amount;
                        }
                    }
                    else
                    {
                        foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                        {
                            if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                continue;
                            dblTransTotal += objFundsTransSplit.Amount;
                        }
                    }

                    if ((objLobSettings != null && objLobSettings.PayDetLmtFlag) && arrlstReserveAmount.Count > 0)
                    {
                        string sAllDesc = "";
                        int iWarningCounter = 0;
                        for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                        {
                            structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];
                            if (GetMaxPayDetailAmount(m_objFunds.LineOfBusCode, structReserveAmount.ReserveTypeCode, ref dblTempAmount))
                            {
                                if (structReserveAmount.Amount > dblTempAmount)
                                {
                                    objLocalCache.GetCodeInfo(structReserveAmount.ReserveTypeCode, ref sShortCode, ref sDesc);
                                    if (m_bPlaceSupHoldPayDetailLimits)
                                    {
                                        iWarningCounter++;
                                        if (iWarningCounter > 1)
                                            sAllDesc += ", " + sDesc;
                                        else
                                            sAllDesc += sDesc;
                                        bCreateHold = true;
                                        //sHoldText += " The amount assigned to the reserve type: " + sDesc + " exceeds the amount authorized for that reserve type." ;
                                        //break;
                                    }
                                    else
                                    {
                                        strError = "You do not have permission to save this payment.  The amount assigned to the reserve type: " + sDesc + " exceeds the amount authorized for that reserve type.";
                                        m_arrlstErrors.Add(strError);
                                        return (false);
                                    }
                                }
                            }
                        }
                        if (iWarningCounter > 1)
                        {
                            int iIndexOf = sAllDesc.LastIndexOf(",");
                            sAllDesc = sAllDesc.Remove(iIndexOf, 1);
                            sAllDesc = sAllDesc.Insert(iIndexOf, " and ");
                            sHoldText += " The amounts assigned to the reserve type: " + sAllDesc + " exceed the amounts authorized for these reserve type.";
                        }
                        else
                            sHoldText += " The amount assigned to the reserve type: " + sAllDesc + " exceeds the amount authorized for that reserve type.";
                    }
                    //Start by Shivendu for MITS 11121
                    if (m_objFunds.StatusCode == objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS"))
                    {
                        bCreateHold = true;
                        m_objFunds.VoidFlag = true;
                        sHoldText += "Check Status selected is Queued.";
                        //Added to check if the user has an approver
                        if (m_objFunds.ApproverId == 0)
                        {
                            m_arrlstErrors.Add("Cannot find an approver for this payment.");
                            return false;
                        }

                    }
                    //End by Shivendu for MITS 11121

                    //If the payment is on hold, "Use Current Adjuster Supervisory Chain" is on,
                    //and no approver ID is found, log an error
                    if (m_bUseSpecificSupervisor && bCreateHold && m_objFunds.ApproverId == 0)
                    {
                        m_arrlstErrors.Add("Cannot find an approver for this payment.");
                        return false;
                    }

                    //Anurag 02/02/2006 - Defect#001755: MITS 4541 'A','M' - System should give warning
                    //to user if payment is being made against closed claim
                    if (this.IsClaimClosed())
                    {
                        m_arrlstConfirmations.Add("claim_closed*");
                        bValidData = false;
                    }

                    // If a tax id is provided, we will search with it along with the name, else we will
                    // Just use last and first name.
                    if (m_objFunds.PayeeEid == 0)
                    {

                        sSQL = " SELECT ENTITY_ID FROM ENTITY WHERE LAST_NAME ='" + m_objFunds.LastName.Trim().Replace("'", "''") + "'";
                        sFirstName = m_objFunds.FirstName.Trim().Replace("'", "''");

                        if (sFirstName == "")
                            sSQL += " AND (FIRST_NAME = '' OR FIRST_NAME IS NULL)";
                        else
                            sSQL += " AND FIRST_NAME = '" + sFirstName + "'";

                        sTaxId = m_objFunds.PayeeEntity.TaxId.Trim();

                        iIndex = sTaxId.IndexOf("-");
                        while (iIndex != -1)
                        {
                            sTaxId = sTaxId.Substring(0, iIndex) + sTaxId.Substring(iIndex + 1);
                            iIndex = sTaxId.IndexOf("-");
                        }

                        // Mihika 16-Jan-2006 Changed the way the SSN number is formatted as the previous method was not working.
                        sTaxId = sTaxId.Trim();
                        if (sTaxId != "")
                            sSQL += " AND TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "')";

                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                iPayeeEid = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), m_iClientId);
                                m_objFunds.PayeeEid = iPayeeEid;

                                m_objFunds.LastName = m_objFunds.PayeeEntity.LastName;
                                m_objFunds.FirstName = m_objFunds.PayeeEntity.FirstName;
                                m_objFunds.Addr1 = m_objFunds.PayeeEntity.Addr1;
                                m_objFunds.Addr2 = m_objFunds.PayeeEntity.Addr2;
                                m_objFunds.Addr3 = m_objFunds.PayeeEntity.Addr3;
                                m_objFunds.Addr4 = m_objFunds.PayeeEntity.Addr4;
                                m_objFunds.City = m_objFunds.PayeeEntity.City;
                                m_objFunds.StateId = m_objFunds.PayeeEntity.StateId;
                                m_objFunds.ZipCode = m_objFunds.PayeeEntity.ZipCode;
                            }
                            objReader.Close();
                        }

                        if (m_objFunds.PayeeEid == 0)
                        {
                            m_objFunds.PayeeEntity.LastName = m_objFunds.LastName;
                            m_objFunds.PayeeEntity.FirstName = m_objFunds.FirstName;
                            m_objFunds.PayeeEntity.Addr1 = m_objFunds.Addr1;
                            m_objFunds.PayeeEntity.Addr2 = m_objFunds.Addr2;
                            m_objFunds.PayeeEntity.Addr3 = m_objFunds.Addr3;
                            m_objFunds.PayeeEntity.Addr4 = m_objFunds.Addr4;
                            m_objFunds.PayeeEntity.City = m_objFunds.City;
                            m_objFunds.PayeeEntity.StateId = m_objFunds.StateId;
                            m_objFunds.PayeeEntity.ZipCode = m_objFunds.ZipCode;
                            //Shruti for 10381
                            m_objFunds.PayeeEntity.EntityTableId = objLocalCache.GetTableId("OTHER_PEOPLE");
                            //Shruti for 10381 ends
                            m_arrlstConfirmations.Add("new_payee*");
                            bValidData = false;
                        }
                    }

                    if (m_objFunds.TransId == 0)
                    {
                        if (CheckForDupPayments(dblTransTotal))
                        {
                            bValidData = false;
                        }
                    }

                    if (m_objFunds.TransId != 0)
                    {
                        if (objLocalCache.GetShortCode(m_objFunds.StatusCode).ToUpper() == "P")
                        {
                            if (m_objFunds.RollupId != 0 && m_objFunds.VoidFlag)
                            {
                                sSQL = " SELECT TRANS_ID FROM FUNDS WHERE ROLLUP_ID = " + m_objFunds.RollupId;

                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objReader != null)
                                {
                                    while (objReader.Read())
                                    {
                                        //Raman Bhatia MITS 9078 We certainly do not want current record to be displayed in rollup confirmations .. modified on 03/22/2007
                                        if (Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId) != m_objFunds.TransId)
                                        {
                                            arrlstTransId.Add(Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId));
                                        }
                                    }
                                    objReader.Close();
                                }
                                //MITS 10950 If this is the only payment in the rollup check, don't popup message box
                                if (arrlstTransId.Count > 0)
                                {
                                    m_arrlstConfirmations.Add("*roll_up*" + GenerateTransSplitHTML(arrlstTransId));
                                    bValidData = false;
                                }
                            }
                        }
                    }

                    if (this.IsClaimOpen())
                    {
                        if ((objLobSettings != null && objLobSettings.InsufFundsFlag) && (!m_objFunds.VoidFlag))
                        {
                            if (arrlstReserveAmount.Count > 0)
                            {
                                for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                                {
                                    structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];
                                    dblBalance = GetReserveBalance(m_objFunds.ClaimId, m_objFunds.ClaimantEid, m_objFunds.UnitId, structReserveAmount.ReserveTypeCode, m_objFunds.LineOfBusCode, m_objFunds.TransId);
                                    //TR#1302,1303,1304 01/18/06: Pankaj-Message box in case of "Do not allow Negative Reserve Adjustment" checkbox in Utilities->LOB Params
                                    if (structReserveAmount.Amount - dblBalance > 0.009 && objColLobSettings[m_objFunds.LineOfBusCode].NoNegResAdjFlag == -1)
                                    {
                                        dblTempAmount = GetCurrentReserveAmount(m_objFunds.ClaimId, m_objFunds.ClaimantEid, m_objFunds.UnitId, structReserveAmount.ReserveTypeCode);
                                        m_arrlstErrors.Add("There are insufficient reserves for this payment.");
                                        m_sInsufAmntXml = GetXmlForInsResrvAmnt(dblBalance, structReserveAmount.Amount, dblTempAmount, structReserveAmount.ReserveTypeCode);
                                        bValidData = false;
                                    }
                                    // Mihika Defect# 2122 Added a check for the 'm_bShowInsuffResPopUp'
                                    else if (structReserveAmount.Amount - dblBalance > 0.009 && m_bShowInsuffResPopUp)
                                    {
                                        dblTempAmount = GetCurrentReserveAmount(m_objFunds.ClaimId, m_objFunds.ClaimantEid, m_objFunds.UnitId, structReserveAmount.ReserveTypeCode);
                                        m_arrlstErrors.Add("Insufficient Reserves Available");
                                        m_sInsufAmntXml = GetXmlForInsResrvAmnt(dblBalance, structReserveAmount.Amount, dblTempAmount, structReserveAmount.ReserveTypeCode);
                                        bValidData = false;
                                    }
                                }
                            }
                        }
                    }

                    //Shruti for EMI changes 
                    objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
                    sSQL = " SELECT FILING_STATE_ID FROM CLAIM WHERE CLAIM_ID = " + m_objFunds.ClaimId;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null && objReader.Read())
                    {
                        iStateId = objReader.GetInt32("FILING_STATE_ID");
                    }
                    objReader.Close();

                    if (iStateId == 11 && objSysSettings.UseFLMaxRate == -1) //state = florida
                    {
                        if (!ValidatePaymentAmount())
                        {
                            return false;
                        }
                    }

                    //Shruti for EMI changes ends
                    //Begin:Modified: Mohit for Bug No. 001487
                    if (m_bPlaceSupHoldTransactionType)
                    {
                        foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                        {
                            if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                continue;

                            if (CheckForSupHoldTransTypes(objFundsTransSplit.TransTypeCode))
                            {
                                bCreateHold = true;
                                sHoldText += " A Transaction Type that requires supervisory approval has been chosen.";
                                break;
                            }
                        }
                    }
                    //End:Modified: Mohit for Bug No. 001487

                    if (bCreateHold)
                    {
                        //12/28/2005 Raman Bhatia ..Incase of Queued Payment Flag we have to set status as queued
                        // || Condition added by Shivendu for MITS 11121. Status should be queued if manual queuing is done irrespective of Queued Payment Flag
                        if (m_bUseQueuedPayments == true || (m_objFunds.StatusCode == objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS")))
                        {
                            m_objFunds.StatusCode = objCCacheFunctions.GetCodeIDWithShort("Q", "CHECK_STATUS");
                            m_objFunds.VoidFlag = true;
                            m_arrlstWarnings.Add("Payment has been queued and requires supervisory approval because: " + sHoldText);
                        }
                        else
                        {
                            DbReader objRead = null;
                            objRead = DbFactory.GetDbReader(m_sConnectionString, "SELECT SH_DIARY FROM CHECK_OPTIONS");
                            if (objRead != null)
                            {
                                while (objRead.Read())
                                {
                                    iHoldSendDiary = Conversion.ConvertObjToInt(objRead.GetValue("SH_DIARY"), m_iClientId);
                                }
                            }
                            m_objFunds.StatusCode = objCCacheFunctions.GetCodeIDWithShort("H", "CHECK_STATUS");
                            m_arrlstWarnings.Add("A hold requiring supervisory approval has been placed on this payment because: " + sHoldText);
                        }
                    }
                }
                dblAmount = 0;
                dblBalance = 0;
                if ((objLobSettings != null && objLobSettings.PerClaimLmtFlag) && arrlstReserveAmount.Count > 0)
                {
                    for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                    {
                        structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];

                        if (GetMaxPerClaimReserveAmount(m_objFunds.LineOfBusCode, structReserveAmount.ReserveTypeCode, GetClaimType(m_objFunds.ClaimId), ref dblTempAmount, ref iReserveTypeCode))
                        {
                            dblBalance = GetPaidTotal(m_objFunds.ClaimId, structReserveAmount.ReserveTypeCode, m_objFunds.ClaimantEid, m_objFunds.UnitId);
                            dblAmount = dblAmount + structReserveAmount.Amount;
                            if (((dblAmount + dblBalance) > dblTempAmount) && (objCCacheFunctions.GetShortCode(m_objFunds.StatusCode) != "RD")
                                && (objCCacheFunctions.GetShortCode(m_objFunds.StatusCode) != "Q")
                                )
                            {
                                if (iReserveTypeCode != 0)
                                {
                                    string sCode = "";
                                    string sCodeDesc = "";
                                    strError = "You do not have authority to pay more than %%1 per claim for reserve type %%2.";
                                    strError = strError.Replace("%%1", string.Format("{0:C}", dblTempAmount));
                                    objCCacheFunctions.GetCodeInfo(structReserveAmount.ReserveTypeCode, ref sCode, ref sCodeDesc);
                                    strError = strError.Replace("%%2", sCodeDesc);
                                    m_arrlstErrors.Add(strError);
                                    return (false);
                                }
                                else
                                {
                                    strError = "You do not have authority to pay more than %%1 per claim.";
                                    strError = strError.Replace("%%1", string.Format("{0:C}", dblTempAmount));
                                    m_arrlstErrors.Add(strError);
                                    return (false);
                                }
                            }
                        }
                    }
                }
                //-- Auto-close functionality
                m_AutoCloseTransTypeUsed = false;
                if (m_ArrCodeIdTransType.Count != 0)
                {
                    DataSet objDs = null;
                    try
                    {
                        objDs = DbFactory.GetDataSet(m_sConnectionString, "SELECT TRANS_TYPE_CODE FROM SYS_TRANS_CLOSE WHERE LINE_OF_BUS_CODE=" + m_objFunds.LineOfBusCode, m_iClientId);

                        if (objDs.Tables[0] != null)
                        {
                            foreach (DataRow dr in objDs.Tables[0].Rows)
                            {
                                if (m_ArrCodeIdTransType.Contains(Conversion.ConvertObjToStr(dr["TRANS_TYPE_CODE"])))
                                {
                                    // prompt the message
                                    m_AutoCloseTransTypeUsed = true;
                                    break;
                                }
                            }
                        }
                    }
                    finally
                    {
                        if (objDs != null)
                            objDs.Dispose();
                    }
                }

                if (m_objFunds.TransId == 0 && m_objFunds.CtlNumber != "")
                {
                    if (GetFundsID(m_objFunds.CtlNumber) != 0)
                    {
                        strError = "The control number: " + m_objFunds.CtlNumber + " you entered is already in use. Please enter a new control number and save again.";
                        m_arrlstErrors.Add(strError);
                        return (false);
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ValidateData.ErrorValidate", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objCCacheFunctions = null;
                objColLobSettings = null;
                objLobSettings = null;
                arrlstReserveAmount = null;
                arrlstTransId = null;
            }
            return (bValidData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private bool ValidatePaymentAmount()
        {
            LocalCache objLocalCache = null;
            DbReader objReader = null;

            string sSql = "";
            string sTransYear = "";
            string sMaxYearText = "";

            int iPayeeType = 0;
            int iTransCode = 0;
            int iYear = 0;
            double dCompRate = 0;
            double dMaxCompRate = 0;
            double dPaymentAmount = 0;
            double dWeeklyRate = 0;

            bool bFlagPTD = false;
            bool bFlagPTDPS = false;
            bool bFlagTrans = false;
            bool bCompRateFromFL = false;
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                iPayeeType = m_objFunds.PayeeTypeCode;

                if (iPayeeType == objLocalCache.GetCodeId("C", "PAYEE_TYPE"))
                {
                    sSql = "SELECT COMP_RATE FROM CLAIM WHERE CLAIM_NUMBER = '" + m_objFunds.ClaimNumber + "'";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader != null && objReader.Read())
                    {
                        dCompRate = objReader.GetDouble("COMP_RATE");
                    }

                    foreach (FundsTransSplit objtransSplit in m_objFunds.TransSplitList)
                    {
                        iTransCode = objtransSplit.TransTypeCode;
                        if (iTransCode == objLocalCache.GetCodeId("TPD", "TRANS_TYPES") ||
                            iTransCode == objLocalCache.GetCodeId("TTD", "TRANS_TYPES") ||
                            iTransCode == objLocalCache.GetCodeId("TR", "TRANS_TYPES") ||
                            iTransCode == objLocalCache.GetCodeId("PPD", "TRANS_TYPES") ||
                            iTransCode == objLocalCache.GetCodeId("PTD", "TRANS_TYPES") ||
                            iTransCode == objLocalCache.GetCodeId("SI", "TRANS_TYPES") ||
                            iTransCode == objLocalCache.GetCodeId("DB", "TRANS_TYPES"))
                        {
                            if (iTransCode == objLocalCache.GetCodeId("PTD", "TRANS_TYPES"))
                            {
                                bFlagPTD = true;
                            }
                            dPaymentAmount = dPaymentAmount + objtransSplit.Amount;
                            bFlagTrans = true;
                        }
                    }

                    if (bFlagPTD)
                    {
                        foreach (FundsTransSplit objtransSplit in m_objFunds.TransSplitList)
                        {
                            if (objtransSplit.TransTypeCode == objLocalCache.GetCodeId("PS", "TRANS_TYPES"))
                            {
                                dPaymentAmount = dPaymentAmount + objtransSplit.Amount;
                                bFlagPTDPS = true;
                            }
                        }
                    }

                    if (bFlagPTDPS) //in case of PTD and PS
                    {
                        sSql = "SELECT WEEKLY_RATE FROM PERSON_INVOLVED WHERE PI_EID = " + m_objFunds.ClaimantEid;
                        sSql = sSql + " AND EVENT_ID = (SELECT EVENT_ID FROM CLAIM WHERE CLAIM_ID = " + m_objFunds.ClaimId + ")";

                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                        if (objReader != null && objReader.Read())
                        {
                            dWeeklyRate = objReader.GetDouble("WEEKLY_RATE");
                        }
                        objReader.Close();

                        sTransYear = m_objFunds.TransDate.Substring(0, 4);
                        sSql = "SELECT MAX_RATE_AMT, MAX_YEAR_TEXT FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT  = '" + sTransYear + "'";
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                        if (objReader != null && objReader.Read())
                        {
                            dMaxCompRate = objReader.GetDouble("MAX_RATE_AMT");
                            iYear = Conversion.ConvertObjToInt(objReader.GetValue("MAX_YEAR_TEXT"), m_iClientId);
                        }
                        objReader.Close();

                        dCompRate = Math.Round(dWeeklyRate * 0.6667, 2, MidpointRounding.AwayFromZero);

                        if (dCompRate > dMaxCompRate)
                        {
                            dCompRate = dMaxCompRate;
                            bCompRateFromFL = true;
                        }

                        if (dCompRate < dPaymentAmount)
                        {
                            if (bCompRateFromFL)
                            {
                                m_arrlstConfirmations.Add("CompRateFromFL_" + dCompRate.ToString() + "_" + iYear.ToString());
                                return false;
                            }
                            else
                            {
                                m_arrlstConfirmations.Add("CompRate_" + dCompRate.ToString());
                                return false;
                            }
                        }
                    }
                    else //case with no combination of PTD and PS
                    {
                        if (bFlagTrans)
                        {
                            if (dCompRate < dPaymentAmount)
                            {
                                sSql = "SELECT DATE_OF_EVENT FROM EVENT , CLAIM  WHERE EVENT.EVENT_ID = CLAIM.EVENT_ID AND CLAIM.CLAIM_NUMBER = '" + m_objFunds.ClaimNumber + "'";
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                                if (objReader != null && objReader.Read())
                                {
                                    sMaxYearText = objReader.GetString("DATE_OF_EVENT").Substring(0, 4);
                                }
                                objReader.Close();

                                sSql = "SELECT MAX_RATE_AMT, MAX_YEAR_TEXT FROM WC_MAX_RATE_CALC WHERE MAX_YEAR_TEXT  = " + sMaxYearText;
                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                                if (objReader != null && objReader.Read())
                                {
                                    dMaxCompRate = objReader.GetDouble("MAX_RATE_AMT");
                                    iYear = Conversion.ConvertStrToInteger(objReader.GetString("MAX_YEAR_TEXT"));
                                }
                                objReader.Close();

                                if (dCompRate >= dMaxCompRate)
                                {
                                    dCompRate = dMaxCompRate;
                                    bCompRateFromFL = true;
                                }

                                if (bCompRateFromFL)
                                {
                                    m_arrlstConfirmations.Add("CompRateFromFL_" + iYear.ToString() + "_" + dCompRate.ToString());
                                    return false;
                                }
                                else
                                {
                                    m_arrlstConfirmations.Add("CompRate_" + dCompRate.ToString());
                                    return false;
                                }
                            }
                        }
                    }

                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ValidatePaymentAmount.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
            return true;
        }
        /// <summary>
        /// Check For Sup Hold Trans Types
        /// </summary>
        /// <param name="p_iTransTypeCode">Trans Type Code</param>
        /// <returns>Sup Hold Trans Type Flag</returns>
        private bool CheckForSupHoldTransTypes(int p_iTransTypeCode)
        {
            DbReader objReader = null;

            bool bCheckForSupHoldTransTypes = false;
            string sSQL = "";

            try
            {
                sSQL = "SELECT TRANS_TYPE_CODE FROM SUP_HOLD_TRANS WHERE TRANS_TYPE_CODE=" + p_iTransTypeCode.ToString();

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bCheckForSupHoldTransTypes = true;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckForSupHoldTransTypes.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (bCheckForSupHoldTransTypes);
        }

        /// <summary>
        /// Get Funds Id
        /// </summary>
        /// <param name="p_sCtlNumber">Control Number</param>
        /// <returns>Fund ID</returns>
        private int GetFundsID(string p_sCtlNumber)
        {
            DbReader objReader = null;

            int iFundId = 0;
            string sSQL = "";

            try
            {
                sSQL = " SELECT TRANS_ID FROM FUNDS WHERE CTL_NUMBER = '" + p_sCtlNumber.Trim() + "'";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iFundId = Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetFundsID.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (iFundId);
        }

        /// <summary>
        /// Get Xml For Insufficient Reserve Amount
        /// </summary>
        /// <param name="p_dblBalance">Balance Amount</param>
        /// <param name="p_dblPayment">Payment Amount</param>
        /// <param name="p_dblReserve">Reserve Amount</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <returns>Xml For Insufficient Resrve Amoun</returns>
        private string GetXmlForInsResrvAmnt(double p_dblBalance, double p_dblPayment, double p_dblReserve, int p_iReserveTypeCode)
        {
            LocalCache objLocalCache = null;

            double dblAddReserve = 0.0;
            double dblSetReserve = 0.0;
            string sXmlForInsResrvAmnt = "";
            string sDesc = "";
            string sShortCode = "";

            try
            {
                //Raman Bhatia..Add Reserve were not populating correctly
                //dblAddReserve = p_dblPayment - p_dblReserve ;
                dblAddReserve = p_dblPayment - p_dblBalance;
                if (dblAddReserve < 0.0)
                    dblAddReserve = -dblAddReserve;

                dblSetReserve = p_dblBalance - (p_dblPayment + p_dblReserve);
                if (dblSetReserve < 0.0)
                    dblSetReserve = -dblSetReserve;


                sXmlForInsResrvAmnt = "~insufres scr=\"1\" userid=\"" + m_iUserId + "\" groupid=\"" + m_iUserGroupId + "\" claimid=\""
                    + m_objFunds.ClaimId.ToString() + "\"|"
                    + "~unit|" + m_objFunds.UnitId.ToString() + "~/unit|"
                    + "~claimant|" + m_objFunds.ClaimantEid.ToString() + "~/claimant|"
                    + "~balance|" + Math.Round(p_dblBalance, 2, MidpointRounding.AwayFromZero) + "~/balance|"
                    + "~reserve|" + Math.Round(p_dblReserve, 2, MidpointRounding.AwayFromZero) + "~/reserve|"
                    + "~payment|" + Math.Round(p_dblPayment, 2, MidpointRounding.AwayFromZero) + "~/payment|"
                    + "~setreserve|" + Math.Round(dblSetReserve, 2, MidpointRounding.AwayFromZero) + "~/setreserve|"
                    + "~addreserve|" + Math.Round(dblAddReserve, 2, MidpointRounding.AwayFromZero) + "~/addreserve|"
                    + "~newamount|0~/newamount|";

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objLocalCache.GetCodeInfo(p_iReserveTypeCode, ref sShortCode, ref sDesc);

                sXmlForInsResrvAmnt += "~rcode id=\"" + p_iReserveTypeCode.ToString()
                    + "\"|" + sDesc + "~/rcode|" + "~/insufres|";
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetXmlForInsResrvAmnt.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return (sXmlForInsResrvAmnt);
        }

        /// <summary>
        /// Get Current Reserve Amount
        /// </summary>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iClaimantEid">ClaimantId</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <returns>Current Reserve Amount</returns>
        public double GetCurrentReserveAmount(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, int p_iReserveTypeCode)
        {
            DbReader objReader = null;

            double dblCurrentReserveAmount = 0.0;
            string sSQL = "";

            try
            {
                sSQL = " SELECT SUM(RESERVE_AMOUNT) FROM RESERVE_CURRENT WHERE CLAIM_ID = " + p_iClaimId.ToString()
                    + " AND CLAIMANT_EID = " + p_iClaimantEid.ToString() + " AND UNIT_ID = "
                    + p_iUnitId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        dblCurrentReserveAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetCurrentReserveAmount.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (dblCurrentReserveAmount);
        }
        //asharma326 JIRA 870 Starts
        private bool CheckPerRsvSettings(int p_iPerRsvFlag, int p_iReserveTypeCode, int p_iLob)
        {
            List<string> lstResTypeCode = null;
            string sSQL = string.Empty;
            //string sConnectionString = m_sConnectionString;
            try
            {
                lstResTypeCode = new List<string>();
                sSQL = string.Format("SELECT RES_TYPE_CODE FROM SYS_RES_COLL_IN_RSV_INCR_BAL WHERE LINE_OF_BUS_CODE = {0}" +
                                   "AND COLL_IN_RSV_BAL = {1}", p_iLob, p_iPerRsvFlag);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        lstResTypeCode.Add(Convert.ToString(objReader.GetValue("RES_TYPE_CODE")));
                    }
                }

                if (!lstResTypeCode.Contains(Convert.ToString(p_iReserveTypeCode)))
                {
                    return false;
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            finally
            {
                lstResTypeCode = null;
            }

            return true;
        }
        /// <summary>
        /// Calculate Reserve Balance
        /// </summary>
        /// <param name="p_dblReserveAmount">Reserve Amount</param>
        /// <param name="p_dblPaid">Paid Amount</param>
        /// <param name="p_dblCollection">Collection Amount</param>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_iStateCode">State Code</param>
        /// <returns>Reserve Balance</returns>
        private double CalReserveBalance(double p_dblReserveAmount, double p_dblPaid, double p_dblCollection, int p_iLob, int p_iStateCode, int p_iReserveTypeCode)
        {
            LocalCache objLocalCache = null;
            ColLobSettings objColLobSettings = null;

            string sShortCode = "";
            double dblPaidCollectionDiff = 0.0;
            double dblReserveBalance = 0.0;
            //MITS 30191 Raman Bhatia: Implementation of recovery reserve
            bool bIsRecoveryReserve = false;
            var bCollInPerRsvBal = false;
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                sShortCode = objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(p_iStateCode));

                //MITS 30191 Raman Bhatia: Implementation of recovery reserve
                if (p_iReserveTypeCode != 0 && String.Compare(objLocalCache.GetRelatedShortCode(p_iReserveTypeCode), "R") == 0)
                {
                    //This is a recovery reserve.. Balance would be calculated differently
                    //for calculation details please refer to rmA13.1 financial equivalency design document

                    bIsRecoveryReserve = true;
                }
                //asharma326 JIRA 870
                if (objColLobSettings[p_iLob].PerRsvCollInRsvBal && !bIsRecoveryReserve)
                {
                    bCollInPerRsvBal = this.CheckPerRsvSettings(-1, p_iReserveTypeCode, p_iLob);
                }

                //MITS 30191 Raman Bhatia: Implementation of recovery reserve
                if (bIsRecoveryReserve)
                {
                    dblReserveBalance = p_dblReserveAmount - p_dblCollection;  //Paid Total would always be 0 for recovery reserves
                }
                else
                {
                    if (p_iLob != 0)
                    {
                        if (objColLobSettings[p_iLob].CollInRsvBal || bCollInPerRsvBal)
                        {
                            dblPaidCollectionDiff = p_dblPaid - p_dblCollection;
                            //if (dblPaidCollectionDiff < 0.0)  //mcapps2 we are not going to set negative amounts to zero any longer
                            //{
                            //    dblPaidCollectionDiff = 0.0;
                            //}
                            dblReserveBalance = p_dblReserveAmount - dblPaidCollectionDiff;
                        }
                        else
                        {
                            dblReserveBalance = p_dblReserveAmount - p_dblPaid;
                        }
                    }
                    else
                    {
                        dblReserveBalance = p_dblReserveAmount - p_dblPaid;
                    }

                    //if (sShortCode == "C" && dblReserveBalance < 0.0) //mcapps2 we are not going to set negative amounts to zero any longer
                    //{
                    //    dblReserveBalance = 0.0;
                    //}
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CalReserveBalance.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objColLobSettings = null;
            }
            return (dblReserveBalance);
        }

        /// <summary>
        /// Get Reserve Balance
        /// </summary>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iClaimantEid">ClaimantId</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <param name="p_iLob">Lob Code</param>
        /// <returns>Reserve Balance</returns>
        public double GetReserveBalance(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, int p_iReserveTypeCode, int p_iLob)
        {
            DbReader objReader = null;
            double dblReserveBalance = 0.0;
            string sSQL = "";
            string sSQLPayment = "";
            string sSQLCollect = "";
            double dblReserveAmount = 0.0;
            double dblTemp = 0.0;
            double dblPayment = 0.0;
            double dblCollection = 0.0;

            try
            {

                if (p_iClaimId != 0 && p_iReserveTypeCode != 0)
                {
                    //Deb Multi Currency 
                    if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                    {
                        sSQL = " SELECT SUM(RESERVE_AMOUNT), SUM(COLLECTION_TOTAL), SUM(PAID_TOTAL), CLAIM_CURR_CODE FROM RESERVE_CURRENT"
                            + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();
                    }
                    else
                    {
                        sSQL = " SELECT SUM(CLAIM_CURRENCY_RESERVE_AMOUNT), SUM(CLAIM_CURR_COLLECTION_TOTAL), SUM(CLAIM_CURRENCY_PAID_TOTAL), CLAIM_CURR_CODE FROM RESERVE_CURRENT"
                                + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();
                    }
                    if (p_iClaimantEid != 0)
                        sSQL += " AND CLAIMANT_EID = " + p_iClaimantEid.ToString();
                    else
                    {
                        if (p_iUnitId != 0)
                            sSQL += " AND UNIT_ID = " + p_iUnitId.ToString();
                    }
                    sSQL += " GROUP BY CLAIM_CURR_CODE ";
                    //Deb Multi Currency 
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblReserveAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                            dblTemp = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(2)));
                            iClaimCurrCode = Conversion.ConvertObjToInt(objReader.GetValue(3), m_iClientId);//Deb Multi Currency 
                        }
                    }
                    if (objReader != null)
                        objReader.Close();
                    //Deb Multi Currency 
                    if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                    {
                        sSQL = "SELECT SUM(FTS.AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS,CODES WHERE "
                            + " FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
                            + " AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString()
                            + " AND FUNDS.UNIT_ID = " + p_iUnitId.ToString()
                            + " AND FUNDS.TRANS_ID = FTS.TRANS_ID"
                            + " AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
                            + " AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
                            + " AND FUNDS.VOID_FLAG = 0";
                    }
                    else
                    {
                        sSQL = "SELECT SUM(FTS.CLAIM_CURRENCY_AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS,CODES WHERE "
                           + " FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
                           + " AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString()
                           + " AND FUNDS.UNIT_ID = " + p_iUnitId.ToString()
                           + " AND FUNDS.TRANS_ID = FTS.TRANS_ID"
                           + " AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
                           + " AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
                           + " AND FUNDS.VOID_FLAG = 0";
                    }
                    //Deb Multi Currency 
                    sSQLPayment = sSQL + " AND FUNDS.PAYMENT_FLAG <> 0";
                    sSQLCollect = sSQL + " AND FUNDS.PAYMENT_FLAG = 0";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLPayment);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblPayment = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLCollect);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblCollection = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    dblReserveBalance = CalReserveBalance(dblReserveAmount, dblPayment, dblCollection, p_iLob, m_iStatusCode, p_iReserveTypeCode);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetReserveBalance.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (dblReserveBalance);
        }


        /// <summary>
        /// Get Reserve Balance
        /// </summary>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iClaimantEid">ClaimantId</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_iTransId">TransId</param>
        /// <returns>Reserve Balance</returns>
        public double GetReserveBalance(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, int p_iReserveTypeCode, int p_iLob, int p_iTransId)
        {
            DbReader objReader = null;
            double dblReserveBalance = 0.0;
            string sSQL = "";
            string sSQLPayment = "";
            string sSQLCollect = "";
            double dblReserveAmount = 0.0;
            double dblTemp = 0.0;
            double dblPayment = 0.0;
            double dblCollection = 0.0;

            try
            {
                if (p_iClaimId != 0 && p_iReserveTypeCode != 0)
                {
                    //Deb Multi Currency 
                    if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                    {
                        sSQL = " SELECT SUM(RESERVE_AMOUNT), SUM(COLLECTION_TOTAL), SUM(PAID_TOTAL), CLAIM_CURR_CODE FROM RESERVE_CURRENT"
                            + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();
                    }
                    else
                    {
                        sSQL = " SELECT SUM(CLAIM_CURRENCY_RESERVE_AMOUNT), SUM(CLAIM_CURR_COLLECTION_TOTAL), SUM(CLAIM_CURRENCY_PAID_TOTAL), CLAIM_CURR_CODE FROM RESERVE_CURRENT"
                                + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();
                    }
                    if (p_iClaimantEid != 0)
                        sSQL += " AND CLAIMANT_EID = " + p_iClaimantEid.ToString();
                    else
                    {
                        if (p_iUnitId != 0)
                            sSQL += " AND UNIT_ID = " + p_iUnitId.ToString();
                    }
                    sSQL += " GROUP BY CLAIM_CURR_CODE ";
                    //Deb Multi Currency 
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblReserveAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                            dblTemp = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(2)));
                            iClaimCurrCode = Conversion.ConvertObjToInt(objReader.GetValue(3), m_iClientId);//Deb Multi Currency 
                        }
                    }
                    if (objReader != null)
                        objReader.Close();
                    //Deb Multi Currency 
                    if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                    {
                        sSQL = "SELECT SUM(FTS.AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS,CODES WHERE "
                         + " FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
                         + " AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString()
                         + " AND FUNDS.UNIT_ID = " + p_iUnitId.ToString()
                         + " AND FUNDS.TRANS_ID = FTS.TRANS_ID"
                         + " AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
                         + " AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
                         + " AND FUNDS.VOID_FLAG = 0";
                    }
                    else
                    {
                        sSQL = "SELECT SUM(FTS.CLAIM_CURRENCY_AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS,CODES WHERE "
                         + " FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
                         + " AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid.ToString()
                         + " AND FUNDS.UNIT_ID = " + p_iUnitId.ToString()
                         + " AND FUNDS.TRANS_ID = FTS.TRANS_ID"
                         + " AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
                         + " AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
                         + " AND FUNDS.VOID_FLAG = 0";
                    }
                    //Deb Multi Currency 
                    if (p_iTransId != 0)
                        sSQL += " AND FUNDS.TRANS_ID <> " + p_iTransId.ToString();

                    sSQLPayment = sSQL + " AND FUNDS.PAYMENT_FLAG <> 0";
                    sSQLCollect = sSQL + " AND FUNDS.PAYMENT_FLAG = 0";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLPayment);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblPayment = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLCollect);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblCollection = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    dblReserveBalance = CalReserveBalance(dblReserveAmount, dblPayment, dblCollection, p_iLob, m_iStatusCode, p_iReserveTypeCode);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetReserveBalance.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (dblReserveBalance);
        }

        //skhare7 R8 supervisory approval Get the status of Reserve type
        /// <summary>
        ///GetReserveStatusDesc
        /// </summary>
        /// <param name="p_iReserveTypeCode">Reserve status Code</param>
        ///  /// <param name="p_iClaimId">ClaimId</param>

        private int GetReserveStatusDesc(int p_iClaimId, int p_iReserveTypeCode, int p_iClaimantEid, int p_iUnitId)
        {

            //skhare7 R8 Supervisory approval
            // LocalCache objLocalCache = null;
            DbReader objReader = null;
            string sReserveStatusCode = string.Empty;
            int iReserveStatusCode = 0;
            string sSQL = string.Empty;
            //skhare7 R8 Supervisory approval End

            try
            {
                if (p_iClaimId != 0 && p_iReserveTypeCode != 0)
                {
                    sSQL = " SELECT RES_STATUS_CODE FROM RESERVE_CURRENT"
                        + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();

                    if (p_iClaimantEid != 0)
                        sSQL += " AND CLAIMANT_EID = " + p_iClaimantEid.ToString();
                    else
                    {
                        if (p_iUnitId != 0)
                            sSQL += " AND UNIT_ID = " + p_iUnitId.ToString();
                    }

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iReserveStatusCode = Common.Conversion.ConvertStrToInteger(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));


                        }
                    }


                }
                return iReserveStatusCode;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {

                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }

        }

        /// By Deb Jena
        /// <summary>
        ///GetReserveStatusDescForCarrierClaims
        /// </summary>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iReserveTypeCode">Reserve status Code</param>
        /// <param name="p_iClaimantEid">ClaimantEID</param>
        /// <param name="p_iUnitId">UnitID</param>
        /// <param name="p_iPolicyId">PolicyID</param>
        /// <param name="p_iCovTypeCode">CoverageType</param>
        /// //rupal
        //private int GetReserveStatusDescForCarrierClaims(int p_iClaimId, int p_iReserveTypeCode, int p_iClaimantEid, int p_iUnitId, int p_iPolicyId, int p_iCovTypeCode,int p_iPolicyUnitRowId)
        private int GetReserveStatusDescForCarrierClaims(int p_iClaimId, int p_iReserveTypeCode, int p_iClaimantEid, int p_iUnitId, int p_iPolicyId, int p_iCovTypeCode, int p_iPolicyUnitRowId, string p_sCovgSeqNum, string p_sTransSeqNum, string p_sCoverageKey, int p_iLossCode)        //Ankit Start : Worked on MITS - 34297 //asharma326 JIRA 305 add int p_iLossCode
        {

            DbReader objReader = null;
            string sReserveStatusCode = string.Empty;
            int iReserveStatusCode = 0;
            string sSQL = string.Empty;
            bool bIsSuccess = false;
            Policy objPolicy = null; //rupal:policy system interface

            try
            {
                //rupal:start,policy system interface                
                objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(p_iPolicyId);
                //rupal:end

                if (p_iClaimId != 0 && p_iReserveTypeCode != 0)
                {
                    sSQL = "SELECT RES_STATUS_CODE FROM RESERVE_CURRENT RC INNER JOIN COVERAGE_X_LOSS CXL ON RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID"
                        + " WHERE RC.CLAIM_ID = " + Convert.ToString(p_iClaimId) + " AND RC.RESERVE_TYPE_CODE = " + Convert.ToString(p_iReserveTypeCode);
                    if (p_iClaimantEid != 0)
                        sSQL += " AND RC.CLAIMANT_EID = " + Convert.ToString(p_iClaimantEid);
                    else
                    {
                        if (p_iUnitId != 0)
                            sSQL += " AND RC.UNIT_ID = " + Convert.ToString(p_iUnitId);
                    }
                    if (p_iCovTypeCode != 0)
                    {
                        //rupal:r8 unit implementation
                        //sSQL += " AND POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLICY_X_UNIT.POLICY_ID =" + Convert.ToString(p_iPolicyId) + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID AND COVERAGE_TYPE_CODE =" + Convert.ToString(p_iCovTypeCode) + ")";
                        //rupal:start,policy system interface
                        if (objPolicy.PolicySystemId > 0)
                        {
                            //start:RMA-11452 rkaur27
                            // sSQL += " AND CXL.POLCVG_ROW_ID = (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode + " AND (CVG_SEQUENCE_NO IS NULL OR CVG_SEQUENCE_NO ='" + p_sCovgSeqNum + "') AND (TRANS_SEQ_NO IS NULL OR TRANS_SEQ_NO='" + p_sTransSeqNum + "')";
                            sSQL += " AND CXL.POLCVG_ROW_ID = (SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode;
                            if (!string.IsNullOrEmpty(p_sCovgSeqNum))
                                sSQL = string.Concat(sSQL, " AND CVG_SEQUENCE_NO = '" + p_sCovgSeqNum + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)
                            if (!string.IsNullOrEmpty(p_sTransSeqNum))
                                sSQL = string.Concat(sSQL, " AND TRANS_SEQ_NO = '" + p_sTransSeqNum + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)
                            if (!string.IsNullOrEmpty(p_sCoverageKey))
                                sSQL = string.Concat(sSQL, " AND COVERAGE_KEY = '" + p_sCoverageKey + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)

                            sSQL = sSQL + " )";
                            //Ankit Start : Worked on MITS - 34297
                            //if (string.IsNullOrEmpty(p_sCoverageKey))
                            //    sSQL = string.Concat(sSQL, " AND COVERAGE_KEY IS NULL)");
                            //else
                            //    sSQL = string.Concat(sSQL, " AND COVERAGE_KEY='", p_sCoverageKey, "')");
                            //Ankit End
                            //end:RMA-11452
                        }
                        else
                            sSQL += " AND CXL.POLCVG_ROW_ID = (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode + ")";

                        //RUPAL:END
                        sSQL += " AND CXL.LOSS_CODE= " + p_iLossCode;//Asharma326 JIRA 305
                    }
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iReserveStatusCode = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("RES_STATUS_CODE")), out bIsSuccess);
                        }
                    }
                }
                return iReserveStatusCode;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objPolicy = null;
            }
        }

        /// <summary>
        /// Get Reserve Balance
        /// </summary>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iClaimantEid">ClaimantId</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_iTransId">TransId</param>
        /// <param name="p_iTransId">p_iPolicyId</param>
        /// <param name="p_iTransId">p_iCovTypeCode</param>
        /// <returns>Reserve Balance</returns>
        //private double GetReserveBalance(int p_iClaimId, int p_iClaimantId, int p_iUnitId, int p_iReserveTypeCode, int iLOB, int p_iTransID, int p_iPolicyId, int p_iCovTypeCode) //commented and changed by rupal:for r8 unit implementation
        //public double GetReserveBalance(int p_iClaimId, int p_iClaimantId, int p_iUnitId, int p_iReserveTypeCode, int iLOB, int p_iTransID, int p_iPolicyId, int p_iCovTypeCode, int p_iPolicyUnitRowId)
        //rupal:for policy system interface
        public double GetReserveBalance(int p_iClaimId, int p_iClaimantId, int p_iUnitId, int p_iReserveTypeCode, int iLOB, int p_iTransID, int p_iPolicyId, int p_iCovTypeCode, int p_iPolicyUnitRowId, string p_sCovgSeqNum, int p_iLossCode, string p_sTransSeqNum, string p_sCoverageKey)       //Ankit Start : Worked on MITS - 34297
        {
            SysSettings objSysSettings = null;
            DbReader objReader = null;
            double dblReserveBalance = 0.0;
            string sSQL = string.Empty;
            string sSQLPayment = string.Empty;
            string sSQLCollect = string.Empty;
            double dblReserveAmount = 0.0;
            double dblTemp = 0.0;
            double dblPayment = 0.0;
            double dblCollection = 0.0;
            int iPolCvgRowID = 0;
            int iRcRowId = 0;
            Policy objPolicy;//rupal:policy system interface
            try
            {
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
                objPolicy = (Policy)m_objDataModelFactory.GetDataModelObject("Policy", false);
                objPolicy.MoveTo(p_iPolicyId);
                
                if (p_iClaimId != 0 && p_iReserveTypeCode != 0)
                {
                    //Deb Multi Currency
                    if (objSysSettings.UseMultiCurrency == 0)
                    {
                        sSQL = " SELECT SUM(RESERVE_AMOUNT), SUM(COLLECTION_TOTAL), SUM(PAID_TOTAL), CLAIM_CURR_CODE,RC_ROW_ID FROM RESERVE_CURRENT"
                           + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();
                    }
                    else
                    {
                        sSQL = " SELECT SUM(CLAIM_CURRENCY_RESERVE_AMOUNT), SUM(CLAIM_CURR_COLLECTION_TOTAL), SUM(CLAIM_CURRENCY_PAID_TOTAL), CLAIM_CURR_CODE, RC_ROW_ID FROM RESERVE_CURRENT"
                            + " WHERE CLAIM_ID = " + p_iClaimId.ToString() + " AND RESERVE_TYPE_CODE = " + p_iReserveTypeCode.ToString();
                    }
                    //Deb Multi Currency
                    if (p_iClaimantId != 0)
                        sSQL += " AND CLAIMANT_EID = " + p_iClaimantId.ToString();
                    else
                    {
                        if (p_iUnitId != 0)
                            sSQL += " AND UNIT_ID = " + p_iUnitId.ToString();
                    }
                    //if (p_iPolicyId != 0)
                    //{
                    //    sSQL += " AND POLICY_ID = " + p_iPolicyId.ToString();
                    //}

                    if (p_iCovTypeCode != 0)
                    {
                        //Raman : Implementing Unit in R8
                        //sSQL += " AND POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_ID =" + p_iPolicyId.ToString() + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode.ToString() + ")";
                        //rupal:strat, r8 unit implementation
                        //sSQL += " AND POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLICY_X_UNIT.POLICY_ID =" + p_iPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode.ToString() + ")";
                        //RUPAL:policy system interafce 
                        if (p_iLossCode > 0)
                        {
                            sSQL = sSQL + " AND POLCVG_LOSS_ROW_ID = ( SELECT CVG_LOSS_ROW_ID FROM COVERAGE_X_LOSS WHERE  ";

                            if (objPolicy.PolicySystemId > 0)
                            {
                                //start:RMA-11452 rkaur27
                                //sSQL += "  POLCVG_ROW_ID = (SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode + " AND (ISNULL(CVG_SEQUENCE_NO,'') ='" + p_sCovgSeqNum + "') AND (ISNULL(TRANS_SEQ_NO,'') ='" + p_sTransSeqNum + "') AND (ISNULL(COVERAGE_KEY,'') ='" + p_sCoverageKey + "'))";
                                sSQL += "  POLCVG_ROW_ID = (SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode;
                                if (!string.IsNullOrEmpty(p_sCovgSeqNum))
                                    sSQL = string.Concat(sSQL, " AND CVG_SEQUENCE_NO = '" + p_sCovgSeqNum + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)
                                if (!string.IsNullOrEmpty(p_sTransSeqNum))
                                    sSQL = string.Concat(sSQL, " AND TRANS_SEQ_NO = '" + p_sTransSeqNum + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)
                                if (!string.IsNullOrEmpty(p_sCoverageKey))
                                    sSQL = string.Concat(sSQL, " AND COVERAGE_KEY = '" + p_sCoverageKey + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)

                                sSQL = sSQL + ")";
                                  
                                //Ankit Start : Worked on MITS - 34297
                                //if (string.IsNullOrEmpty(p_sCoverageKey))
                                //    sSQL = string.Concat(sSQL, " AND COVERAGE_KEY IS NULL)");
                                //else
                                //    sSQL = string.Concat(sSQL, " AND COVERAGE_KEY='", p_sCoverageKey, "')");
                                //Ankit End

                                //END:RMA-11452
                            }
                            else
                            {
                                sSQL += "  POLCVG_ROW_ID = (SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode + ")";
                            }
                            //rupal:end

                            sSQL = sSQL + " AND LOSS_CODE = " + p_iLossCode + " )";
                        }
                    }

                    sSQL += " GROUP BY CLAIM_CURR_CODE,RC_ROW_ID ";//Deb Multi Currency 

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblReserveAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                            dblTemp = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(2)));
                            iClaimCurrCode = Conversion.ConvertObjToInt(objReader.GetValue(3), m_iClientId);//Deb Multi Currency 
                            iRcRowId = Conversion.ConvertObjToInt(objReader.GetValue("RC_ROW_ID"), m_iClientId);
                        }
                    }
                    if (objReader != null)
                        objReader.Close();
                    // MITS 37470,JIRA 5704: For Carrier claims, if there is no RCRowID,safely assuming it is first n final, return from here.
                    if (!objSysSettings.MultiCovgPerClm.Equals(default(int)) && iRcRowId.Equals(default(int)))
                    {
                        return 0;
                    }
                    //RUPAL:R8 UNIT IMPLEMENTATION
                    //rupal:policy system interafce
                    if (objPolicy.PolicySystemId > 0)
                    {
                        //start:RMA-11452 rkaur27
                        //sSQL = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode + " AND CVG_SEQUENCE_NO='" + p_sCovgSeqNum + "' AND TRANS_SEQ_NO='" + p_sTransSeqNum + "'";
                         sSQL = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode;
                         if (!string.IsNullOrEmpty(p_sCovgSeqNum))
                             sSQL = string.Concat(sSQL, " AND CVG_SEQUENCE_NO = '" + p_sCovgSeqNum + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)
                         if (!string.IsNullOrEmpty(p_sTransSeqNum))
                             sSQL = string.Concat(sSQL, " AND TRANS_SEQ_NO = '" + p_sTransSeqNum + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)
                         if (!string.IsNullOrEmpty(p_sCoverageKey))
                             sSQL = string.Concat(sSQL, " AND COVERAGE_KEY = '" + p_sCoverageKey + "'");//rkotak, rma-11746, single quotes in the query are missing, ripple effects of (rma-11452)
                        //Ankit Start : Worked on MITS - 34297
                        //if (string.IsNullOrEmpty(p_sCoverageKey))
                        //    sSQL = string.Concat(sSQL, " AND COVERAGE_KEY IS NULL");
                        //else
                        //    sSQL = string.Concat(sSQL, " AND COVERAGE_KEY='", p_sCoverageKey, "'");
                        //Ankit End
                        //END:RMA-11452
                    }
                    else
                    {
                        sSQL = "SELECT POLCVG_ROW_ID from POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID=" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode;
                    }
                    //rupal:end
                    //sSQL = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLICY_X_UNIT.POLICY_ID =" + p_iPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID AND COVERAGE_TYPE_CODE =" + p_iCovTypeCode.ToString();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iPolCvgRowID = Common.Conversion.ConvertObjToInt(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)), m_iClientId);
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    //Deb Multi Currency
                    if (objSysSettings.UseMultiCurrency == 0)
                    {
                        sSQL = "SELECT SUM(FTS.AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS WHERE "
                            //+ " FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
                            //+ " AND FUNDS.CLAIMANT_EID = " + p_iClaimantId.ToString()
                            //+ " AND FUNDS.UNIT_ID = " + p_iUnitId.ToString()
                            + " FTS.RC_ROW_Id=" + iRcRowId
                            + " AND FUNDS.TRANS_ID = FTS.TRANS_ID "
                            //+ " AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
                            //+ " AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
                            + " AND FUNDS.VOID_FLAG = 0";
                    }
                    else
                    {
                        sSQL = "SELECT SUM(FTS.CLAIM_CURRENCY_AMOUNT) FROM FUNDS,FUNDS_TRANS_SPLIT FTS WHERE "
                            //+ " FUNDS.CLAIM_ID = " + p_iClaimId.ToString()
                            //+ " AND FUNDS.CLAIMANT_EID = " + p_iClaimantId.ToString()
                            //+ " AND FUNDS.UNIT_ID = " + p_iUnitId.ToString()
                            + " FTS.RC_ROW_Id=" + iRcRowId
                            + " AND FUNDS.TRANS_ID = FTS.TRANS_ID"
                            //+ " AND CODES.CODE_ID = FTS.TRANS_TYPE_CODE"
                            //+ " AND CODES.RELATED_CODE_ID = " + p_iReserveTypeCode.ToString()
                            + " AND FUNDS.VOID_FLAG = 0";
                    }
                    //Deb Multi Currency
                    //if (objSysSettings.MultiCovgPerClm != 0)
                    //{
                    //    sSQL += " AND FTS.POLCVG_ROW_ID = " + iPolCvgRowID.ToString();
                    //}

                    if (p_iTransID != 0)
                        sSQL += " AND FUNDS.TRANS_ID <> " + p_iTransID.ToString();

                    sSQLPayment = sSQL + " AND FUNDS.PAYMENT_FLAG <> 0";
                    sSQLCollect = sSQL + " AND FUNDS.PAYMENT_FLAG = 0";
                    //if (iRcRowId > 0)
                    //{
                    //    sSQLPayment = sSQLPayment + " AND FTS.RC_ROW_ID=" + iRcRowId;
                    //    sSQLCollect = sSQLCollect + " AND FTS.RC_ROW_ID = " + iRcRowId;
                    //}
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLPayment);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblPayment = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLCollect);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            dblCollection = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                    }
                    if (objReader != null)
                        objReader.Close();

                    dblReserveBalance = CalReserveBalance(dblReserveAmount, dblPayment, dblCollection, iLOB, m_iStatusCode, p_iReserveTypeCode);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetReserveBalance.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (dblReserveBalance);
        }

        /// <summary>
        /// Get Trans Split HTML
        /// </summary>
        /// <param name="p_arrlstTransId">TransId List</param>
        /// <returns>Trans Split HTML string</returns>
        private string GenerateTransSplitHTML(ArrayList p_arrlstTransId)
        {
            //DbReader objReader = null ;
            LocalCache objLocalCache = null;

            int iIndex = 0;
            int iTransId = 0;
            int iLastTransId = 0;
            bool bFirst = true;
            bool bVoid = false;
            string sHTML = "";
            string sTransDate = "";
            string sFirstName = "";
            string sName = "";
            string sFromDate = "";
            string sToDate = "";
            string sTransTypeShortCode = "";
            string sTransTypeDesc = "";
            string sAmount = "";
            string sSQL = "";

            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                for (iIndex = 0; iIndex < p_arrlstTransId.Count; iIndex++)
                {
                    iTransId = (int)p_arrlstTransId[iIndex];
                    if (iTransId != iLastTransId)
                    {
                        iLastTransId = iTransId;

                        sSQL = " SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.VOID_FLAG,FUNDS.TRANS_DATE,"
                            + " FUNDS.AMOUNT, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, "
                            + " FUNDS_TRANS_SPLIT.FROM_DATE, FUNDS_TRANS_SPLIT.TO_DATE,"
                            + " FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE, FUNDS_TRANS_SPLIT.AMOUNT,FUNDS.CLAIM_NUMBER"
                            + " FROM FUNDS,FUNDS_TRANS_SPLIT"
                            + " WHERE FUNDS.TRANS_ID = " + iTransId.ToString()
                            + " AND FUNDS_TRANS_SPLIT.TRANS_ID = " + iTransId.ToString();

                        bFirst = true;
                        //objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                        //MITS 14306 : Umesh
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {

                            if (objReader != null)
                            {
                                while (objReader.Read())
                                {
                                    sHTML += "*";
                                    if (bFirst)
                                    {
                                        bFirst = false;
                                        sTransDate = Conversion.GetDBDateFormat(objReader.GetString("TRANS_DATE"), "d");
                                        sHTML += sTransDate + "|";

                                        sHTML += objReader.GetString("CTL_NUMBER") + "|";

                                        sFirstName = objReader.GetString("FIRST_NAME");
                                        if (sFirstName != "")
                                            sFirstName = sFirstName + " ";
                                        sName = sFirstName + objReader.GetString("LAST_NAME");

                                        sHTML += sName + "|";

                                        bVoid = objReader.GetBoolean("VOID_FLAG");
                                        if (bVoid)
                                            sHTML += "Yes" + "|";
                                        else
                                            sHTML += "No" + "|";

                                        sFromDate = Conversion.GetDBDateFormat(objReader.GetString("FROM_DATE"), "d");
                                        sToDate = Conversion.GetDBDateFormat(objReader.GetString("TO_DATE"), "d");
                                        sHTML += sFromDate + " - " + sToDate + "|";

                                        objLocalCache.GetCodeInfo(Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId), ref sTransTypeShortCode, ref sTransTypeDesc);

                                        sHTML += sTransTypeDesc + "|";

                                        sAmount = string.Format("{0:C}", Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(10))));
                                        sHTML += sAmount + "|";

                                        sAmount = string.Format("{0:C}", Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(4))));
                                        sHTML += sAmount + "|";

                                        sHTML += objReader.GetString("CLAIM_NUMBER") + "|";
                                    }
                                    else
                                    {
                                        sHTML += "\"|";
                                        sHTML += "\"|";
                                        sHTML += "\"|";
                                        sHTML += "\"|";

                                        sFromDate = Conversion.GetDBDateFormat(objReader.GetString("FROM_DATE"), "d");
                                        sToDate = Conversion.GetDBDateFormat(objReader.GetString("TO_DATE"), "d");
                                        sHTML += sFromDate + " - " + sToDate + "|";

                                        objLocalCache.GetCodeInfo(Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId), ref sTransTypeShortCode, ref sTransTypeDesc);

                                        sHTML += sTransTypeDesc + "|";

                                        sAmount = string.Format("{0:C}", Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(10))));
                                        sHTML += sAmount + "|";

                                        sHTML += "\"|";
                                        sHTML += "\"|";
                                    }
                                }//end while
                            }//end if
                        }//MITS 14306 : End

                    }// end if
                }//end for
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GenerateTransSplitHTML.ErrorGenHTML", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                //if( objReader != null )
                //{
                //    objReader.Close();
                //    objReader.Dispose();
                //}
            }
            return (sHTML);
        }

        /// <summary>
        /// Check For Duplicate Payments
        /// </summary>
        /// <param name="p_dblTransTotal">Trans Total Amount</param>
        /// <returns>Duplicate Payment Flag</returns>
        private bool CheckForDupPayments(double p_dblTransTotal)
        {
            DbReader objReader = null;
            Entity objEntity = null;
            ArrayList arrlstTransID = null;

            bool bEntityFound = false;
            bool bCheckForDupPayments = false;
            bool bCheckDupPay = false;
            bool bFlag = false;
            bool bExit = false;
            bool bExitAgain = false;
            string sTaxId = "";
            string sSQL = "";
            string sDatePast = "";
            string sDateFuture = "";
            string sFirstName = "";
            string sLastName = "";
            //int iIndex = 0 ;
            int iPayCriteria = 0;
            int iNumDays = 0;
            int iTransTypeCode = 0;
            int iClaimId = 0;
            int iClaimantEID = 0;

            double dblAmount = 0.0;
            DateTime datDate;
            DateTime datDatePast;
            DateTime datDateFuture;

            try
            {
                arrlstTransID = new ArrayList();

                sSQL = "SELECT DUPLICATE_FLAG,DUP_PAY_CRITERIA,DUP_NUM_DAYS FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        bCheckDupPay = objReader.GetBoolean("DUPLICATE_FLAG");
                        iPayCriteria = Common.Conversion.ConvertObjToInt(objReader.GetValue("DUP_PAY_CRITERIA"), m_iClientId);
                        iNumDays = Common.Conversion.ConvertObjToInt(objReader.GetValue("DUP_NUM_DAYS"), m_iClientId);
                    }
                    objReader.Close();
                }
                if (bCheckDupPay)
                {
                    sSQL = " SELECT FUNDS.TRANS_ID,FUNDS.TRANS_DATE FROM FUNDS, FUNDS_TRANS_SPLIT "
                        + " WHERE FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ";
                    if (iPayCriteria != 7)
                        sSQL += " AND FUNDS.CLAIM_ID = " + m_objFunds.ClaimId;

                    if (iPayCriteria != 10 && iPayCriteria != 11)
                        sSQL += " AND FUNDS.PAYEE_EID = " + m_objFunds.PayeeEid;

                    #region Switch on PayCriteria
                    switch (iPayCriteria)
                    {
                        case 1:
                            if (m_objFunds.TransDate.Trim() == "")
                            {
                                bCheckForDupPayments = false;
                                return (false);
                            }
                            datDate = Convert.ToDateTime(Conversion.GetDBDateFormat(m_objFunds.TransDate, "d"));
                            datDatePast = datDate.AddDays((double)-iNumDays);
                            datDateFuture = datDate.AddDays((double)iNumDays);

                            sDatePast = Conversion.ToDbDate(datDatePast);
                            sDateFuture = Conversion.ToDbDate(datDateFuture);

                            sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                                + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";

                            break;
                        case 2:
                            sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_dblTransTotal.ToString() + ") < .005)";
                            break;
                        case 3:
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                sDatePast = objFundsTransSplit.FromDate;
                                sDateFuture = objFundsTransSplit.ToDate;

                                if (sDatePast != "" && sDateFuture != "")
                                {
                                    if (!bFlag)
                                    {
                                        sSQL += " AND (";
                                        bFlag = true;
                                    }
                                    else
                                    {
                                        sSQL += " OR";
                                    }
                                    sSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                    sSQL += " AND TO_DATE >= '" + sDatePast + "')";

                                    sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                    sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                                }
                            }
                            if (bFlag)
                                sSQL += ")";
                            break;
                        case 4:
                            bFlag = false;
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                dblAmount = objFundsTransSplit.Amount;
                                iTransTypeCode = objFundsTransSplit.TransTypeCode;
                                if (!bFlag)
                                {
                                    sSQL += " AND (";
                                    bFlag = true;
                                }
                                else
                                {
                                    sSQL += " OR";
                                }
                                sSQL += " (TRANS_TYPE_CODE = " + iTransTypeCode.ToString();
                                sSQL += " AND (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005))";
                            }
                            if (bFlag)
                                sSQL += ")";
                            break;
                        case 5:
                            datDate = Convert.ToDateTime(Conversion.GetDBDateFormat(m_objFunds.TransDate, "d"));
                            datDatePast = datDate.AddDays((double)-iNumDays);
                            datDateFuture = datDate.AddDays((double)iNumDays);

                            sDatePast = Conversion.ToDbDate(datDatePast);
                            sDateFuture = Conversion.ToDbDate(datDateFuture);

                            sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                                + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";
                            sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_dblTransTotal.ToString() + ") < .005)";
                            break;
                        case 6:
                            datDate = Convert.ToDateTime(Conversion.GetDBDateFormat(m_objFunds.TransDate, "d"));
                            datDatePast = datDate.AddDays((double)-iNumDays);
                            datDateFuture = datDate.AddDays((double)iNumDays);

                            sDatePast = Conversion.ToDbDate(datDatePast);
                            sDateFuture = Conversion.ToDbDate(datDateFuture);

                            sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                                + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";

                            bFlag = false;
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                dblAmount = objFundsTransSplit.Amount;
                                iTransTypeCode = objFundsTransSplit.TransTypeCode;
                                if (!bFlag)
                                {
                                    sSQL += " AND (";
                                    bFlag = true;
                                }
                                else
                                {
                                    sSQL += " OR";
                                }
                                sSQL += " (TRANS_TYPE_CODE = " + iTransTypeCode.ToString();
                                sSQL += " AND (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005))";
                            }
                            if (bFlag)
                                sSQL += ")";
                            break;
                        case 7:
                            iClaimId = m_objFunds.ClaimId;
                            if (iClaimId == 0)
                            {
                                bExit = true;
                                break;
                            }
                            iClaimantEID = m_objFunds.ClaimantEid;
                            if (iClaimantEID == 0)
                            {
                                objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID =" + iClaimId.ToString() + " AND PRIMARY_CLMNT_FLAG <> 0");
                                if (objReader != null)
                                    if (objReader.Read())
                                        iClaimantEID = Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIMANT_EID"), m_iClientId);
                                if (objReader != null)
                                    objReader.Close();

                                if (iClaimantEID == 0)
                                {
                                    objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID =" + iClaimId.ToString());
                                    if (objReader != null)
                                        if (objReader.Read())
                                            iClaimantEID = Common.Conversion.ConvertObjToInt(objReader.GetValue("CLAIMANT_EID"), m_iClientId);
                                    if (objReader != null)
                                        objReader.Close();
                                }
                            }

                            if (iClaimantEID > 0)
                            {
                                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                                try
                                {
                                    objEntity.MoveTo(iClaimantEID);
                                    bEntityFound = true;
                                }
                                catch
                                {
                                    bEntityFound = false;
                                }
                                if (bEntityFound)
                                {
                                    sTaxId = objEntity.TaxId;
                                    sTaxId = sTaxId.Replace("-", "");
                                    sTaxId = sTaxId.Trim();
                                }
                                else
                                    sTaxId = "";

                                if (sTaxId != "")
                                    sSQL += " AND FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT,ENTITY WHERE CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "' ))";
                                else
                                    sSQL += " AND FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM CLAIMANT WHERE CLAIMANT_EID = " + iClaimantEID.ToString() + ")";
                            }
                            else
                                sSQL += " AND FUNDS.CLAIM_ID = " + iClaimId.ToString();

                            bFlag = false;
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                sDatePast = objFundsTransSplit.FromDate;
                                sDateFuture = objFundsTransSplit.ToDate;

                                if (sDatePast != "" && sDateFuture != "")
                                {
                                    if (!bFlag)
                                    {
                                        sSQL += " AND (";
                                        bFlag = true;
                                    }
                                    else
                                    {
                                        sSQL += " OR";
                                    }
                                    sSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                    sSQL += " AND TO_DATE >= '" + sDatePast + "')";

                                    sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                    sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                                }
                            }
                            if (bFlag)
                                sSQL += ")";
                            break;
                        case 8:
                            datDate = Convert.ToDateTime(Conversion.GetDBDateFormat(m_objFunds.TransDate, "d"));
                            datDatePast = datDate.AddDays((double)-iNumDays);
                            datDateFuture = datDate.AddDays((double)iNumDays);

                            sDatePast = Conversion.ToDbDate(datDatePast);
                            sDateFuture = Conversion.ToDbDate(datDateFuture);

                            sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                                + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";
                            sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_dblTransTotal.ToString() + ") < .005)";
                            // Item One
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsTransSplit.InvoiceNumber + "'";
                                break;
                            }
                            sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_dblTransTotal.ToString() + ") < .005)";
                            break;

                        case 9:
                            // Item one 
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsTransSplit.InvoiceNumber + "'";
                                break;
                            }
                            sSQL += " AND FUNDS.PAYEE_EID = " + m_objFunds.PayeeEid.ToString();
                            break;
                        case 10:
                            sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_dblTransTotal.ToString() + ") < .005)";
                            bFlag = false;
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                dblAmount = objFundsTransSplit.Amount;
                                iTransTypeCode = objFundsTransSplit.TransTypeCode;
                                if (!bFlag)
                                {
                                    sSQL += " AND (";
                                    bFlag = true;
                                }
                                else
                                {
                                    sSQL += " OR";
                                }
                                sSQL += " (TRANS_TYPE_CODE = " + iTransTypeCode.ToString();
                                sSQL += " AND (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005))";
                            }
                            if (bFlag)
                                sSQL += ")";

                            sTaxId = m_objFunds.PayeeEntity.TaxId;
                            sTaxId = sTaxId.Replace("-", "");
                            sTaxId = sTaxId.Trim();

                            if (sTaxId != "")
                                sSQL += " AND FUNDS.CLAIM_ID IN (SELECT CLAIM_ID FROM FUNDS, ENTITY WHERE FUNDS.PAYEE_EID = ENTITY.ENTITY_ID AND ENTITY.TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "' ))";

                            bFlag = false;
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                sDatePast = objFundsTransSplit.FromDate;
                                sDateFuture = objFundsTransSplit.ToDate;

                                if (sDatePast != "" && sDateFuture != "")
                                {
                                    if (!bFlag)
                                    {
                                        sSQL += " AND (";
                                        bFlag = true;
                                    }
                                    else
                                    {
                                        sSQL += " OR";
                                    }
                                    sSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                    sSQL += " AND TO_DATE >= '" + sDatePast + "')";

                                    sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                    sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                                }
                            }
                            if (bFlag)
                                sSQL += ")";

                            // Item one ....
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                if (objFundsTransSplit.InvoiceNumber != "")
                                    sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsTransSplit.InvoiceNumber + "'";
                                break;
                            }
                            break;
                        // Mihika 16-Jan-2006 Defect no. 1502
                        // Added the case for the criteria - 'claims with same (payee name or SSN) and (overlapping date range or invoice number)'
                        case 11:
                            //We Get the payee tax id
                            objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                            try
                            {
                                objEntity.MoveTo(m_objFunds.PayeeEid);
                                bEntityFound = true;
                            }
                            catch
                            {
                                bEntityFound = false;
                            }

                            if (bEntityFound)
                            {
                                sTaxId = objEntity.TaxId;
                                sTaxId = sTaxId.Replace("-", "");
                                sTaxId = sTaxId.Trim();
                            }
                            else
                                sTaxId = "";

                            sLastName = m_objFunds.LastName.Replace("'", "''");
                            sFirstName = m_objFunds.FirstName.Replace("'", "''");

                            sSQL += " AND (((FIRST_NAME = '" + sFirstName + "') OR (FIRST_NAME IS NULL)) AND ((LAST_NAME = '" + sLastName + "') OR (LAST_NAME IS NULL))";
                            sSQL += " OR (FUNDS.PAYEE_EID IN (SELECT PAYEE_EID FROM FUNDS, ENTITY WHERE FUNDS.PAYEE_EID = ENTITY.ENTITY_ID AND FUNDS.CLAIM_ID = " + m_objFunds.ClaimId;

                            if (sTaxId != "")
                                sSQL += " AND ENTITY.TAX_ID IN ('" + sTaxId + "','" + Int32.Parse(sTaxId).ToString("00-0000000") + "','" + Int32.Parse(sTaxId).ToString("000-00-0000") + "' ))))";
                            else
                                sSQL += " AND ENTITY.TAX_ID IN ('" + sTaxId + "' ))))";

                            bFlag = false;
                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;

                                sDatePast = objFundsTransSplit.FromDate;
                                sDateFuture = objFundsTransSplit.ToDate;

                                if (sDatePast != "" && sDateFuture != "")
                                {
                                    if (!bFlag)
                                    {
                                        sSQL += " AND ((";
                                        bFlag = true;
                                    }
                                    else
                                    {
                                        sSQL += " OR ";
                                    }
                                    sSQL += " ((FROM_DATE <= '" + sDatePast + "'";
                                    sSQL += " AND TO_DATE >= '" + sDatePast + "')";

                                    sSQL += " OR (FROM_DATE <= '" + sDateFuture + "'";
                                    sSQL += " AND TO_DATE >= '" + sDateFuture + "'))";
                                }
                            }
                            if (bFlag)
                            {
                                // Item one ....
                                foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                                {
                                    if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                        continue;

                                    if (objFundsTransSplit.InvoiceNumber != "")
                                        sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsTransSplit.InvoiceNumber + "'))";
                                    else
                                        sSQL += "))";
                                    break;
                                }
                            }
                            else
                            {
                                foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                                {
                                    if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                        continue;

                                    if (objFundsTransSplit.InvoiceNumber != "")
                                        //TR2604 - Funds screen was crashing due to incorrect SQL
                                        //sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsTransSplit.InvoiceNumber + "')" ;
                                        sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsTransSplit.InvoiceNumber + "'";
                                    break;
                                }
                            }
                            break;
                        case 12:
                            bFlag = false;
                            datDate = Convert.ToDateTime(Conversion.GetDBDateFormat(m_objFunds.TransDate, "d"));
                            datDatePast = datDate.AddDays((double)-iNumDays);
                            datDateFuture = datDate.AddDays((double)iNumDays);

                            sDatePast = Conversion.ToDbDate(datDatePast);
                            sDateFuture = Conversion.ToDbDate(datDateFuture);

                            sSQL += " AND FUNDS.TRANS_DATE >= '" + sDatePast
                                + "' AND FUNDS.TRANS_DATE <= '" + sDateFuture + "'";
                            sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_dblTransTotal.ToString() + ") < .005)";

                            foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                            {
                                if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                                    continue;
                                dblAmount = objFundsTransSplit.Amount;

                                if (!bFlag)
                                {
                                    sSQL += " AND FUNDS_TRANS_SPLIT.INVOICE_NUMBER = '" + objFundsTransSplit.InvoiceNumber + "'";
                                    sSQL += " AND (((ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005)";
                                    bFlag = true;
                                }
                                else
                                {
                                    sSQL += " OR (ABS(FUNDS_TRANS_SPLIT.AMOUNT - " + dblAmount.ToString() + ") < .005)";
                                }
                            }

                            if (bFlag)
                                sSQL += "))";

                            sSQL += " AND (ABS(FUNDS.AMOUNT - " + p_dblTransTotal.ToString() + ") < .005)";
                            break;

                    }// end switch
                    #endregion

                    if (!bExit)
                    {
                        if (m_objFunds.PaymentFlag)
                            sSQL += " AND FUNDS.PAYMENT_FLAG <> 0 ";
                        else
                            sSQL += " AND FUNDS.PAYMENT_FLAG = 0 ";

                        sSQL += " ORDER BY FUNDS.TRANS_DATE DESC,FUNDS.TRANS_ID";

                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                        if (objReader != null)
                        {
                            if (objReader.Read())
                                bExitAgain = false;
                            else
                                bExitAgain = true;
                        }

                        if (!bExitAgain)
                        {
                            arrlstTransID.Add(Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId));
                            while (objReader.Read())
                                arrlstTransID.Add(Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId));
                            if (objReader != null)
                                objReader.Close();

                            m_arrlstConfirmations.Add("Duplicate Payment " + GenerateTransSplitHTML(arrlstTransID));
                            bCheckForDupPayments = true;
                        }
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckForDupPayments.ErrorDupPay", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objEntity = null;
                arrlstTransID = null;
            }
            return (bCheckForDupPayments);
        }

        /// <summary>
        /// Get Max Pay Detail Amount
        /// </summary>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>
        /// <param name="p_dblAmount">Amount</param>
        /// <returns>Max Pay Detail Amount Flag</returns>
        public bool GetMaxPayDetailAmount(int p_iLob, int p_iReserveTypeCode, ref double p_dblAmount)
        {
            DbReader objReader = null;

            bool bGetMaxPayDetailAmount = false;
            string sSQL = "";
            try
            {
                p_dblAmount = 0;

                sSQL = " SELECT LINE_OF_BUS_CODE, RESERVE_TYPE_CODE,MAX(MAX_AMOUNT) FROM FUNDS_DET_LIMITS "
                    + " WHERE (USER_ID = " + m_iUserId;

                if (m_iUserGroupId > 0)
                    sSQL += " OR GROUP_ID = " + m_iUserGroupId;
                sSQL += ") AND LINE_OF_BUS_CODE=" + p_iLob + " AND RESERVE_TYPE_CODE=" + p_iReserveTypeCode;
                sSQL += " GROUP BY LINE_OF_BUS_CODE,RESERVE_TYPE_CODE";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        p_dblAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(2)));
                        bGetMaxPayDetailAmount = true;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetMaxPayDetailAmount.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (bGetMaxPayDetailAmount);
        }

        /// <summary>
        /// Get Max Pay Amount
        /// </summary>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_dblAmount">Amount</param>
        /// <returns>Max Pay Amount Flag</returns>
        public bool GetMaxPayAmount(int p_iLob, ref double p_dblAmount)
        {
            DbReader objReader = null;

            bool bGetMaxPayAmount = false;
            string sSQL = "";

            try
            {
                p_dblAmount = 0;

                sSQL = " SELECT MAX(MAX_AMOUNT) FROM FUNDS_LIMITS "
                    + " WHERE (USER_ID = " + m_iUserId;

                if (m_iUserGroupId > 0)
                    sSQL += " OR GROUP_ID = " + m_iUserGroupId;
                sSQL += ") AND LINE_OF_BUS_CODE=" + p_iLob;
                sSQL += " GROUP BY LINE_OF_BUS_CODE";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        p_dblAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        bGetMaxPayAmount = true;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetMaxPayAmount.ErrorMaxPay", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (bGetMaxPayAmount);
        }

        #region WithinClientLimit
        //Begin: Added: Mohit Yadav for Bug No. 001094		
        /// <summary>
        /// Get Max Pay Amount from Client Limits
        /// </summary>
        /// <param name="p_dNewAmount">Amount</param>
        /// <param name="p_iLob">Lob Code</param>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>Client Limits Flag</returns>
        private bool WithinClientLimit(double p_dNewAmount, int p_iLob, int p_iClaimId)
        {
            DbReader objReader = null;

            bool bWithinClientLimit = true;
            string sSQL = "";

            try
            {
                sSQL = "SELECT * FROM CLIENT_LIMITS WHERE LOB_CODE = " + p_iLob + " AND PAYMENT_FLAG <> 0 AND" +
                    " CLIENT_EID = (SELECT DEPT_EID FROM EVENT,CLAIM WHERE EVENT.EVENT_ID = CLAIM.EVENT_ID" +
                    " AND CLAIM.CLAIM_ID = " + p_iClaimId + ")";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if (p_dNewAmount > Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue("MAX_AMOUNT"))))
                            bWithinClientLimit = false;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.WithinClientLimit.ErrorClientLimits", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (bWithinClientLimit);
        }

        #endregion
        /// <summary>
        /// Read Check Options
        /// </summary>
        private void ReadCheckOptions()
        {
            DbReader objReader = null;
            string sSQL = "";
            bool bSetToDefault = true;

            try
            {
                sSQL = "SELECT * FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_bPostDate = objReader.GetBoolean("ALLOW_POST_DATE");
                        m_bExportToFile = objReader.GetBoolean("CHECK_TO_FILE");
                        m_bConsolidate = objReader.GetBoolean("ROLLUP_CHECKS");
                        m_iMaxAutoChecks = Common.Conversion.ConvertObjToInt(objReader.GetValue("MAX_AUTO_CHECKS"), m_iClientId);
                        m_bPlaceSupHoldPayLimits = objReader.GetBoolean("SH_PAYMENT");
                        m_bPlaceSupHoldPayDetailLimits = objReader.GetBoolean("SH_PAY_DET");
                        m_bPlaceSupHoldNewPayee = objReader.GetBoolean("SH_NEW_PAYEE");
                        m_bPlaceSupHoldTransactionType = objReader.GetBoolean("SH_TRANS_TYPES");
                        m_bUseQueuedPayments = objReader.GetBoolean("QUEUED_PAYMENTS");
                        //Begin: Added: Mohit Yadav for Bug No. 001094
                        m_bSupChainHold = objReader.GetBoolean("USER_SPECIFIC_FLAG");
                        m_bUseSpecificSupervisor = objReader.GetBoolean("USER_SPECIFIC_FLAG");
                        //End: Added: Mohit Yadav for Bug No. 001094
                        m_bPmtNotfyImmSup = objReader.GetBoolean("PMT_NOTFY_IMM_SUP");//smahajan6: Safeway: Payment Supervisory Approval
                        bSetToDefault = false;
                    }
                }
                if (bSetToDefault)
                {
                    m_bPostDate = false;
                    m_bExportToFile = false;
                    m_bConsolidate = false;
                    m_iMaxAutoChecks = 50;
                    m_bPlaceSupHoldPayLimits = false;
                    m_bPlaceSupHoldPayDetailLimits = false;
                    m_bPlaceSupHoldNewPayee = false;
                    m_bPlaceSupHoldTransactionType = false;
                    m_bUseQueuedPayments = false;
                    //Begin: Added: Mohit Yadav for Bug No. 001094
                    m_bSupChainHold = false;
                    m_bUseSpecificSupervisor = false;
                    //End: Added: Mohit Yadav for Bug No. 001094
                    m_bPmtNotfyImmSup = false;//smahajan6: Safeway: Payment Supervisory Approval
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ReadCheckOptions.ErrorCheckOpt", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }
        public struct LOBPerClaimLmtReserves
        {
            /// <summary>
            /// Private variable for MaxAmount.
            /// </summary>
            private double dMaxAmount;
            /// <summary>
            /// Private variable for LineOfBusiness.
            /// </summary>
            private int iLineOfBusiness;
            /// <summary>
            /// Private variable for ReserveTypeCode.
            /// </summary>
            private int iReserveTypeCode;
            /// <summary>
            /// Private variable for ClaimTypeCode.
            /// </summary>
            private int iClaimTypeCode;
            /// <summary>
            /// Read/Write property for LineOfBusiness.
            /// </summary>
            public int LineOfBusiness
            {
                get
                {
                    return (iLineOfBusiness);
                }
                set
                {
                    iLineOfBusiness = value;
                }
            }
            public int ClaimTypeCode
            {
                get
                {
                    return (iClaimTypeCode);
                }
                set
                {
                    iClaimTypeCode = value;
                }
            }
            public int ReserveTypeCode
            {
                get
                {
                    return (iReserveTypeCode);
                }
                set
                {
                    iReserveTypeCode = value;
                }
            }
            public double MaxAmount
            {
                get
                {
                    return (dMaxAmount);
                }
                set
                {
                    dMaxAmount = value;
                }
            }

        }
        /// <summary>
        /// Reserve Amount Detail Structure
        /// </summary>
        private struct ReserveAmount
        {
            /// <summary>
            /// Private variable for Reserve Type Code.
            /// </summary>
            private int iReserveTypeCode;
            /// <summary>
            /// Private variable for Amount.
            /// </summary>
            private double dblAmount;
            /// <summary>
            /// Read/Write property for ReserveTypeCode.
            /// </summary>
            internal int ReserveTypeCode
            {
                get
                {
                    return (iReserveTypeCode);
                }
                set
                {
                    iReserveTypeCode = value;
                }
            }
            /// <summary>
            /// Read/Write property for Amount.
            /// </summary>
            internal double Amount
            {
                get
                {
                    return (dblAmount);
                }
                set
                {
                    dblAmount = value;
                }
            }
        }

        /// <summary>
        /// Clear Error, Warning, Confirmation List
        /// </summary>
        private void ClearErrors()
        {
            try
            {
                m_arrlstErrors = new ArrayList();
                m_arrlstWarnings = new ArrayList();
                m_arrlstConfirmations = new ArrayList();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ClearErrors.ErrorClear", m_iClientId), p_objEx);
            }
        }

        #endregion

        #region Add/Edit Split
        /// <summary>
        /// Get the XML for Funds Object after Add/Edit Split.
        /// </summary>
        /// <param name="p_sSplitXML">
        /// XML contains Split information for each payment collection. 		
        /// </param>
        /// <param name="p_sTransactionDataXML">Transaction Data XML</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument AddEditSplitTransaction(string p_sSplitXML, string p_sTransactionDataXML)
        {
            XmlDocument objSplitDoc = null;
            XmlDocument objTransactionDataDoc = null;

            int iTransId = 0;
            int iClaimId = 0;
            int iClaimantId = 0;
            int iUnitId = 0;

            try
            {
                objSplitDoc = new XmlDocument();
                objSplitDoc.LoadXml(p_sSplitXML);

                objTransactionDataDoc = new XmlDocument();
                objTransactionDataDoc.LoadXml(p_sTransactionDataXML);

                iTransId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "TransId"));
                iClaimId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "ClaimId"));
                iClaimantId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "ClaimantId"));
                iUnitId = Common.Conversion.ConvertStrToInteger(this.GetValue(objTransactionDataDoc, "UnitId"));
                m_bAllowPaymentsClosedClaims = Conversion.ConvertStrToBool(this.GetValue(objTransactionDataDoc, "AllowPaymentsClosedClaims"));

                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (iTransId != 0)
                    m_objFunds.MoveTo(iTransId);
                else
                    this.InitializeFromClaimId(iClaimId, iClaimantId, iUnitId);

                this.UpdateFundsObjectForTransactionData(objTransactionDataDoc);

                this.UpdateFundsObjectForPaymentCollectionData(objSplitDoc);

                m_bDataChanged = true;

                this.GetXMLForTransaction();
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.AddEditSplitTransaction.ErrorAddSplit", m_iClientId), p_objEx);
            }
            finally
            {
                objSplitDoc = null;
                objTransactionDataDoc = null;
            }
            return (m_objDocument);
        }
        /// <summary>
        /// MITS:34082 MultiCurrency: Added function for get ExchangeRate and it's Effective date
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <returns></returns>
        public XmlDocument GetExchRateAndEffDate(XmlDocument p_objXmlIn)
        {
            XmlElement objRootNode = null;
            XmlElement objTempNode = null;
            int iReserveCurrencyType = Convert.ToInt16(p_objXmlIn.SelectSingleNode("//CurrencyTypeForClaim").InnerText);
            string sPaymentcurrencyType = p_objXmlIn.SelectSingleNode("//pmtcurrencytype").InnerText;
            int iPaymentCurrTypeCode = CommonFunctions.iGetReserveCurrencyCode(sPaymentcurrencyType, m_sConnectionString, m_iClientId);
            try
            {

                if (iReserveCurrencyType > 0)
                {
                    string ExchangeRateEffDate = CommonFunctions.sGetCurrencyRateEffectDate(iReserveCurrencyType, iPaymentCurrTypeCode, m_sConnectionString);
                    double dExchnagerateSplitToPayment = CommonFunctions.ExchangeRateSrc2Dest(iReserveCurrencyType, iPaymentCurrTypeCode, m_sConnectionString, m_iClientId);
                    double dExchnagerateBaseToPayment = CommonFunctions.ExchangeRateSrc2Dest(m_objFunds.Context.InternalSettings.SysSettings.BaseCurrencyType, iPaymentCurrTypeCode, m_sConnectionString, m_iClientId);
                    objRootNode = null;
                    this.StartDocument(ref objRootNode, "SpliInformation");
                    this.CreateAndSetElement(objRootNode, "ExchangeRateDate", ExchangeRateEffDate);
                    objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ExchangeRateDate");
                    this.CreateAndSetElement(objRootNode, "ExchangeRate", dExchnagerateSplitToPayment.ToString());
                    this.CreateAndSetElement(objRootNode, "SplitToPaymentCurrRate", dExchnagerateSplitToPayment.ToString());

                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetExchRateAndEffDate.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
                objTempNode = null;
            }
            return (m_objDocument);

        }

        #endregion Add/Edit Split

        #region Sending Documents via EMail
        /// <summary>
        /// Sending the email to multiple users
        /// </summary>
        /// <param name="p_sUserName">User name</param>
        /// <param name="p_sConnStringName">Security Connection String</param>
        /// <param name="p_sEmailIds">EMail Ids of all the recievers</param>
        /// <param name="p_sSubject">Subject of the mail</param>
        /// <param name="p_sMessage">Message of the mail</param>

        /// <returns>True if successful else false.</returns>
        public bool EmailDocuments(string p_sUserName, string p_sConnStringName, string p_sEmailIds,
            string p_sSubject, string p_sMessage)
        {
            string[] arrsEmailIds = null;
            string sDelimiter = ",";
            string sEmail = "";
            bool bRetVal = false;
            Mailer objMail = null;

            try
            {
                if (p_sUserName != "")
                {
                    sEmail = GetEmailId(p_sConnStringName, p_sUserName);
                    if (sEmail == "")
                    {
                        throw new InvalidValueException(Globalization.GetString("FundManager.Email.InvalidSenderID", m_iClientId));
                    }
                }
                else
                {
                    throw new InvalidValueException(Globalization.GetString("FundManager.Email.InvalidUserName", m_iClientId));
                }

                if (p_sEmailIds.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("FundManager.Email.InvalidIDs", m_iClientId));
                }

                if (sEmail != "")
                {
                    if (p_sEmailIds.Length > 0)
                    {
                        arrsEmailIds = p_sEmailIds.Split(sDelimiter.ToCharArray());

                        foreach (string sEmailId in arrsEmailIds)
                        {
                            objMail = new Mailer(m_iClientId);

                            objMail.To = sEmailId;
                            objMail.From = sEmail;
                            objMail.Subject = p_sSubject;
                            objMail.Body = p_sMessage;


                            if (sEmailId != "")
                            {
                                objMail.SendMail();
                            }


                            bRetVal = true;
                        }
                    }
                }
            }
            catch (XmlOperationException p_objException)
            {
                bRetVal = false;
                throw p_objException;
            }

            catch (DataModelException p_objException)
            {
                bRetVal = false;
                throw p_objException;
            }

            catch (RMAppException p_objException)
            {
                bRetVal = false;
                throw p_objException;
            }

            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.EmailDocuments.SendError", m_iClientId), p_objException);
            }
            finally
            {
                objMail = null;
            }
            return (bRetVal);
        }

        #endregion


        #region Payment History
        //Ankit MITS 29909-Method signature modified-Start changes
        public XmlDocument GetPaymentHistory(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, bool p_bAscending, UserLogin p_objUserLogin, int p_iEntityId, int p_iPageNumber, string p_sFilterValue)
        //Ankit-MITS 29909-End changes
        {
            StringBuilder sbSQL = null;
            // akaushik5 Commented of Performance Improvement Starts
            // StringBuilder sentSQL = null;  // averma62 MITS 26999
            // akaushik5 Commented of Performance Improvement Ends
            //  LocalCache objLocalCache = null ; //mini perofrmance  mits 32156
            XmlElement objRootNode = null;
            XmlElement objRowNode = null;
            XmlElement objTempNode = null;
            //Nadim for 16254(REM Customization)
            XmlDocument objXmlAutoCheck = null;
            string sAutoCheckbtn = string.Empty;
            //Nadim for 16254(REM Customization)

            // akaushik5 Commented of Performance Improvement Starts
            // const string  DITTO = "\"" ;
            // akaushik5 Commented of Performance Improvement Ends
            int iTransId = 0;
            // akaushik5 Commented of Performance Improvement Starts
            // int iLastTransId = 0 ;
            // akaushik5 Commented of Performance Improvement Ends
            //int iTransNumber = 0 ;
            long iTransNumber = 0;    //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database, So changing its type to Long
            int iCheckStatusCodeId = 0;
            double dblAmount = 0.0;
            double dblNetTotal = 0.0;
            double dblSplitAmount = 0.0;
            double dblIAmount = 0.0;
            double dblTotalVoid = 0.0;
            double dblTotalCollect = 0.0;
            double dblTotalPay = 0.0;
            double dblTotalVoidPay = 0.0;
            double dblTotalVoidCollect = 0.0;
            bool bPayment = false;
            bool bVoid = false;
            bool bStopPay = false;  //swati agarwal for MITS # 33431
            bool bCleared = false;
            bool bAscending = false;
            //nadim for 15388
            string sTitle = string.Empty;
            //LobSettings objLobSettings = null; //mini perofrmance mits 32156
            string sClaimTitle = string.Empty;
            string sClaimant = string.Empty;
            string sClaimnumberTitle = string.Empty;
            string sDepartment = string.Empty;
            int iCaptionlevel = 0;
            int iLobCode = 0;
            //nadim for 15388
            string sSortCol = string.Empty;
            string sCtlNumber = string.Empty;
            string sOffset = string.Empty;
            //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
            string sCheckTotal = string.Empty;
            double dblCheckTotal = 0.0;
            //END - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
            string sTransDate = string.Empty;
            string sAmount = string.Empty;
            string sTransNumber = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sInvoiceNumber = string.Empty;
            string sCodeDesc = string.Empty;
            string sDBType = string.Empty;
            string sName = string.Empty;
            string sCheckStatusCode = string.Empty;
            string sCheckStatusDesc = string.Empty;
            string sCheckStatus = string.Empty;
            string sCheckDate = string.Empty;
            string sVoid = string.Empty;
            string sStopPay = string.Empty; //swati agarwal for MITS # 33431
            string sCleared = string.Empty;
            string sPayment = string.Empty;
            string sAddedByUser = string.Empty;
            // Added by Amitosh for mits 23603 (05/09/2011)
            string sPayee = string.Empty;
            //Ashish Ahuja Mits 34270
            string sCheckMemo = string.Empty;
            Hashtable objHtTransIds = null;

            //rupal:start, for first & final payment
            bool bIsFirstFinal = false;
            string sIsFirstFinal = string.Empty;
            //rupal:end, for first & final payment
            //Added by Amitosh for mits 23603(05/09/2011)

            //Ankit Start : Financials Enhancements - Manual Check / Void, Cleared Date Changes
            bool bIsManualCheck = false;
            string sIsManualCheck = string.Empty;
            string sVoidDate = string.Empty;
            string sStopPayDate = string.Empty; //swati agarwal for MITS # 33431
            //Ankit End

            string sClaimantLastName = string.Empty;
            string sClaimantFirstName = string.Empty;
            //end Amitosh
            //skhare7 R8 enhancement Combined Pay
            bool bIsCombinedPay = false;
            string sIsCombinedPay = string.Empty;
            // bool bFlag=false;

            //skhare7 r8 end
            //Ankit 29909 Start variable definitions
            StringBuilder sbMainSQL = null;  //holds value for query used to fetch the actual records
            StringBuilder sbSQLCount = null;   // holds value for query used to fetch total record count
            // akaushik5 Commented of Performance Improvement Starts
            // StringBuilder sbSortValue = null;
            // akaushik5 Commented of Performance Improvement Ends
            long lStartAt = 0;
            long uppercount = 0;
            long lTotalRecCount = 0; //max number of records                
            //DataSet objDataSet = null;
            string sFilterColumn = string.Empty;
            // akaushik5 Commented of Performance Improvement Starts
            // string sSortColumn = string.Empty;
            // akaushik5 Commented of Performance Improvement Ends
            string sFilterValue = string.Empty;
            string sFilterOperator = string.Empty;
            int iStartIndex = 0;
            int iEndIndex = 0;
            int iNumOfFilters = 0;
            int iCounter = 0;
            int iTempIndex = 0;
            string sTempFilterString = string.Empty;
            Regex reDateExpr = null;
            Boolean bIsCorrectFormat = false;
            //Ankit 29909- End variable definitions          
            string sPolicyName = string.Empty;
            string sPolcyCvgType = string.Empty;
            string sUnitType = string.Empty;
            string sLossType = string.Empty;
            string sPayeesName = string.Empty;  //averma62 MITS 26999
            int iNoOfPayees = 0;
            int iNumOfRecordsPerPage = 0;//Ankit MITS 29834
            //JIRA 7810 Start Snehal
            string sHoldReason = string.Empty;
            LocalCache objCache = null;
            //JIRA 7810 End
            // akaushik5 Commented of Performance Improvement Starts
            // int iTotalItemCount = 0;//Ankit MITS 29834
            // akaushik5 Commented of Performance Improvement Ends
            string sReserveType = string.Empty;
            try
            {
                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                //mini perofrmance mits
                //   iNumOfRecordsPerPage = GetRecordsPerPage(); //Ankit MITS 29834
                if (m_objDataModelFactory == null)
                    this.Initialize();
                if (m_objDataModelFactory.Context.InternalSettings.SysSettings.PayHistLimit > 10)
                    iNumOfRecordsPerPage = m_objDataModelFactory.Context.InternalSettings.SysSettings.PayHistLimit;
                else
                    iNumOfRecordsPerPage = 10;


                //mini perofrmance mits
                //nadim for 15388-Set Title
                //skhare7 r8 enhancement
                if (p_iClaimId != 0)
                {
                    //using (DataModelFactory objDataModelFacFortitle = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword))
                    //   {
                    //mini perofrmance mits
                    using (Claim objClaimForTitle = (DataModel.Claim)m_objDataModelFactory.GetDataModelObject("Claim", false))
                    {
                        objClaimForTitle.MoveTo(p_iClaimId);
                        sClaimnumberTitle = objClaimForTitle.ClaimNumber;
                        iLobCode = objClaimForTitle.LineOfBusCode;
                        //mini perofrmance mits
                        //  objLobSettings = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[objClaimForTitle.LineOfBusCode];                      
                        iCaptionlevel = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[objClaimForTitle.LineOfBusCode].CaptionLevel;
                        sDepartment = objClaimForTitle.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaimForTitle.Parent as Event).DeptEid, iCaptionlevel, 0, ref iCaptionlevel);
                        sDepartment = sDepartment.Substring(0, Math.Min(sDepartment.Length, 25));
                        //Deb MITS 31370
                        if (p_iClaimantEid != 0 && objClaimForTitle.ClaimantList[p_iClaimantEid] != null && m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                        {
                            sClaimant = objClaimForTitle.ClaimantList[p_iClaimantEid].ClaimantEntity.GetLastFirstName();
                            sClaimantFirstName = objClaimForTitle.PrimaryClaimant.ClaimantEntity.FirstName.Trim();
                            sClaimantLastName = objClaimForTitle.PrimaryClaimant.ClaimantEntity.LastName.Trim();
                        }
                        else
                        {
                            if (objClaimForTitle.PrimaryClaimant != null)
                            {
                                sClaimant = objClaimForTitle.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                                //Added by Amitosh for mits 23603  (05/09/2011)
                                sClaimantFirstName = objClaimForTitle.PrimaryClaimant.ClaimantEntity.FirstName.Trim();
                                sClaimantLastName = objClaimForTitle.PrimaryClaimant.ClaimantEntity.LastName.Trim();
                                //end Amitosh
                            }
                            else
                            {
                                sClaimant = objClaimForTitle.PrimaryPiEmployee.PiEntity.LastName.Trim() + ',' + objClaimForTitle.PrimaryPiEmployee.PiEntity.FirstName.Trim();
                                //Added by Amitosh for mits 23603  (05/09/2011)  
                                sClaimantFirstName = objClaimForTitle.PrimaryPiEmployee.PiEntity.FirstName.Trim();
                                sClaimantLastName = objClaimForTitle.PrimaryPiEmployee.PiEntity.LastName.Trim();
                                //end Amitosh
                            }
                        }
                        //Deb MITS 31370
                        if (sClaimant.EndsWith(","))
                        {
                            sClaimant = sClaimant.Remove(sClaimant.LastIndexOf(','));
                        }
                    }
                    //  }
                    //using (LocalCache objLocalCacheForTitle = new LocalCache(m_sConnectionString))
                    //    {
                    sClaimTitle = m_objDataModelFactory.Context.LocalCache.GetCodeDesc(iLobCode);
                    //}
                    sTitle = sClaimTitle + " [" + sClaimnumberTitle + "*" + sDepartment + "*" + sClaimant + "]";
                    //nadim for 15388 
                }//skhare7 R8 Enhancement
                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                //mini perofrmance mits
                //objLocalCache = new LocalCache(m_sConnectionString);

                //Nadim for 16254(REM Customization)
                objXmlAutoCheck = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);//rkaur27
                if (!string.IsNullOrEmpty(strContent))
                {
                    objXmlAutoCheck.LoadXml(strContent);
                    sAutoCheckbtn = objXmlAutoCheck.SelectSingleNode("//RMAdminSettings/SpecialSettings/Show_AutoCheck").InnerText;
                }

                //Nadim for 16254(REM Customization)
                //Nikhil Garg		Dated: 27-Jan-2006
                //Checking for View Payment History Permission
                // akaushik5 Commented for Performance Improvement Starts
                // string sSQL="SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId.ToString();
                // int iLOB=0;
                // akaushik5 Commented for Performance Improvement Ends
                string sLOB = string.Empty;
                int iBaseSecurityCode = 0;
                // akaushik5 Changed for Performance Improvement Starts
                // using (DbReader objLOBReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                // {
                //    if (objLOBReader != null)
                //    {
                //       if (objLOBReader.Read())
                //       {
                //            iLOB = Conversion.ConvertObjToInt(objLOBReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                //            sLOB = objLocalCache.GetShortCode(iLOB);
                if (iLobCode != default(int))
                {
                    //mini perofrmance mits
                    sLOB = m_objDataModelFactory.Context.LocalCache.GetShortCode(iLobCode);
                    // akaushik5 Changed for Performance Improvement Ends
                    switch (sLOB)
                    {
                        case "GC":
                            iBaseSecurityCode = RMB_CLAIMGC_RESERVES;
                            break;
                        case "WC":
                            iBaseSecurityCode = RMB_CLAIMWC_RESERVES;
                            break;
                        case "VA":
                            //TR:2306 28Feb2006 Nitesh Starts
                            if (p_iUnitId != 0)
                            { // detail level - coming from unit screen
                                iBaseSecurityCode = RMB_CLAIMVA_UNIT_RESERVES;
                            }
                            else
                            { // claim level
                                iBaseSecurityCode = RMB_CLAIMVA_RESERVES;
                            }
                            //TR:2306 28Feb2006 Nitesh Ends
                            break;
                        case "DI":
                            iBaseSecurityCode = RMB_CLAIMDI_RESERVES;
                            break;
                        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
                        case "PC":
                            iBaseSecurityCode = RMB_CLAIMPC_RESERVES;
                            break;
                        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314
                    }
                    if (!p_objUserLogin.IsAllowedEx(iBaseSecurityCode, RMO_PAYMENT_HISTORY))
                        //throw new PermissionViolationException(RMPermissions.RMO_PAYMENT_HISTORY, iBaseSecurityCode); //dvatsa - error not filtered and thrown properly
                        throw new RMAppException(CommonFunctions.FilterBusinessMessage(Globalization.GetString("Permission.NoPaymentHistory", m_iClientId))); //dvatsa

                }
                // akaushik5 Commented for Performance Improvement Starts
                //    }
                // }
                // akaushik5 Commented for Performance Improvement Ends
                sbSQL = new StringBuilder();

                //changed by Amitosh for mits 23603(05/09/2011)

                //rupal:start, for first & final payment
                //added FUNDS_TRANS_SPLIT.IS_FIRST_FINAL in select clause -rupal
                //mini perofrmance mits
                //tanwar2 - uncommented
                SysSettings objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);
                //if (objSysSetting.MultiCovgPerClm == -1)
                //skhare7 R8 enhancement 

                //ddhiman : commented : MITS 27009
                //sTitle = string.Empty;                 //if (objSysSetting.MultiCovgPerClm == -1)
                //End ddhiman

                //skhare7 R8 enhancement 
                //    if (p_iEntityId!=0)

                //Ankit MITS 29909-Start Changes
                sbMainSQL = new StringBuilder();
                sbSQLCount = new StringBuilder();
                //stoppayflag added by swati for MITS # 33431
                //if(p_sSortCol.ToUpper().Equals("PAYMENTFLAG") || p_sSortCol.ToUpper().Equals("CLEAREDFLAG") || p_sSortCol.ToUpper().Equals("VOIDFLAG") || p_sSortCol.ToUpper().Equals("OFFSET")) // Mits 35247 added check for offset
                if (p_sSortCol.ToUpper().Equals("PAYMENTFLAG") || p_sSortCol.ToUpper().Equals("CLEAREDFLAG") || p_sSortCol.ToUpper().Equals("VOIDFLAG") || p_sSortCol.ToUpper().Equals("OFFSET") || p_sSortCol.ToUpper().Equals("STOPPAYFLAG")) // Mits 35247 added check for offset
                    //change end by swati
                    p_bAscending = !p_bAscending; //MITS 32340 because these are integers but their description rae in opposite order
                bAscending = p_bAscending;
                //rsharma220 MITS 33794 Start
                string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
                int iLangCode = p_objUserLogin.objUser.NlsCode;
                //Ankit-End changes
                //mini perofrmance mits
                int iMultiCovgPerClm = m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm;
                if (iMultiCovgPerClm == -1)
                //if (m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                //mini perofrmance mits
                {
                    //Ankit-Start changes for MITS 29909 Custom Paging                    
                    sbMainSQL.Append("SELECT * FROM (");
                    sbSQL.Append(" SELECT ROW_NUMBER() OVER(ORDER BY ");
                    if (p_sSortCol.CompareTo(string.Empty) == 0)
                        sbSQL.Append("FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID");
                    else
                        sbSQL.Append(ModifySortColumn(p_sSortCol, sDBType));
                    if (!bAscending)
                        sbSQL.Append(" DESC");
                    //mits Prashant  35247 STARTS
                    if (p_sSortCol == "Offset")
                    {
                        if (!bAscending)
                            sbSQL.Append(",FUNDS.VOID_DATE DESC");
                        else
                            sbSQL.Append(",FUNDS.VOID_DATE");
                    }
                    //mits Prashant  35247 ends
                    //stop_pay_flag added by Swati agarwal for MITS # 33431
                    //sbSQL.Append(" ) as rno,FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.CLAIM_ID,FUNDS.CLAIM_NUMBER, ");
                    sbSQL.Append(" ) as rno,FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.STOP_PAY_FLAG, FUNDS.CLAIM_ID,FUNDS.CLAIM_NUMBER, ");
                    //end changes by swati
                    //Ankit-End changes for MITS 29909 Custom Paging                    
                    // akaushik5 Changed for MITS 37343 Starts
                    //sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.SETTLEMENT_FLAG,FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    // akaushik5 Changed for MITS 37343 Ends
                    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,LOSSCODE.CODE_DESC LOSS_CODE,LOSSCODE.SHORT_CODE SHORT_CODE, FUNDS_TRANS_SPLIT.INVOICE_AMOUNT | FIAMT,");
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, A.CODE_DESC, A.SHORT_CODE TRANSSHORTCODE, FUNDS_TRANS_SPLIT.FROM_DATE, POL.CODE_DESC COVERAGETYPE,POL.SHORT_CODE SHORTCOVERAGETYPE,FUNDS.CHECK_MEMO,"); //smahajan22 - mits 35587
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, A.CODE_DESC, A.SHORT_CODE TRANSSHORTCODE, FUNDS_TRANS_SPLIT.FROM_DATE, POL.CODE_DESC COVERAGETYPE,POL.SHORT_CODE SHORTCOVERAGETYPE,POLICY_X_CVG_TYPE.COVERAGE_TEXT COVERAGETEXT,FUNDS.CHECK_MEMO,"); //smahajan22 - mits 35587
                    //sbSQL.Append(" POLICY.POLICY_NAME, FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG,FUNDS.MANUAL_CHECK, ");
                    //rupal:start,on behalf of smahajan22-mits 35587
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL.Append(" ISNULL(POLICY.POLICY_SYSTEM_ID,0) POLICY_SYSTEM_ID, POLICY.POLICY_NAME, FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG,FUNDS.MANUAL_CHECK, ");//rupal:on behalf of smahajan22-mits 35587
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        sbSQL.Append(" NVL(POLICY.POLICY_SYSTEM_ID,0) POLICY_SYSTEM_ID, POLICY.POLICY_NAME, FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG,FUNDS.MANUAL_CHECK, ");//rupal:on behalf of smahajan22-mits 35587
                    }
                    //rupal:end on behalf of smahajan22-mits 35587
                    //stop_pay_date added by swati MITS # 33431
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,FUNDS.VOID_DATE ,POLICY_X_UNIT.UNIT_ID,FUNDS.OFFSET_FLAG,");
                    //start - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    // sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,FUNDS.VOID_DATE ,FUNDS.STOP_PAY_DATE ,POLICY_X_UNIT.UNIT_ID,FUNDS.OFFSET_FLAG,");
                    
                    //JIRA 7810 start  fix RMA-13278
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,FUNDS.VOID_DATE ,FUNDS.STOP_PAY_DATE ,POLICY_X_UNIT.UNIT_ID,FUNDS.OFFSET_FLAG,FUNDS.CHECK_TOTAL,ENTITY2.ENTITY_APPROVAL_STATUS, FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, RESERVE_TYPE.SHORT_CODE RES_TYPE_SHORT_CODE, RESERVE_TYPE.CODE_DESC RES_TYPE_CODE_DESC,  URESERVE_TYPE.CODE_DESC URES_TYPE_CODE_DESC,"); //asharma326 added " ENTITY2.ENTITY_APPROVAL_STATUS " to check attached payee current status jira 7810
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,FUNDS.VOID_DATE ,FUNDS.STOP_PAY_DATE ,POLICY_X_UNIT.UNIT_ID,FUNDS.OFFSET_FLAG,FUNDS.CHECK_TOTAL, ");
                 
                    sbSQL.Append(" CASE");
                    sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                    sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS IN(" + objCache.GetCodeId("R", "ENTITY_APPRV_REJ") + ")) THEN ");
                    sbSQL.Append(objCache.GetCodeId("R", "ENTITY_APPRV_REJ"));

                    sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                    sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS IN(" + objCache.GetCodeId("P", "ENTITY_APPRV_REJ") + ",0)) THEN ");
                    sbSQL.Append(objCache.GetCodeId("P", "ENTITY_APPRV_REJ"));

                    sbSQL.Append(" ELSE ");
                    sbSQL.Append(objCache.GetCodeId("A", "ENTITY_APPRV_REJ"));

                    sbSQL.Append(" END ");

                    sbSQL.Append(" ENTITY_APPROVAL_STATUS , ");
                    
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE, RESERVE_TYPE.SHORT_CODE RES_TYPE_SHORT_CODE, RESERVE_TYPE.CODE_DESC RES_TYPE_CODE_DESC,  URESERVE_TYPE.CODE_DESC URES_TYPE_CODE_DESC,");
                    //JIRA 7810 start  fix RMA-13278
                   
                    //end - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    //change end here by swati
                    sbSQL.Append(" ULOSSCODE.CODE_DESC U_LOSS_CODE,UPOL.CODE_DESC UCOVERAGETYPE,UA.CODE_DESC U_CODE_DESC");
                    //RUpal:start,policy interface change
                    //changes for MITS 31997 start
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        //dbisht6 start for mits 35655
                        sbSQL.Append(" , CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT ISNULL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT POINT_UNIT_DATA.STAT_UNIT_NUMBER + ' - ' + ISNULL(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM((ISNULL(ENTITY.FIRST_NAME,'') + ' '+ ISNULL(ENTITY.MIDDLE_NAME,'') +' '+ ISNULL(ENTITY.LAST_NAME,''))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT_TYPE");

                        //dbisht6 end
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {
                        //payment history Oracle query fixed
                        //dbisht6 start for mits 35655 //mkaran2 - modified for MITS 35725
                        sbSQL.Append(", CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT NVL(VEH_DESC, VIN) FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'S' THEN (SELECT NAME FROM SITE_UNIT WHERE SITE_ID=POLICY_X_UNIT.UNIT_ID) WHEN 'SU' THEN (SELECT CONCAT(POINT_UNIT_DATA.STAT_UNIT_NUMBER,CONCAT(' - ',nvl(ENTITY.REFERENCE_NUMBER,LTRIM(RTRIM( CONCAT(nvl(ENTITY.FIRST_NAME,'') ,CONCAT( ' ' ,CONCAT( nvl(ENTITY.MIDDLE_NAME,'') ,CONCAT(' ', nvl(ENTITY.LAST_NAME,'')))))))))) FROM ENTITY,OTHER_UNIT,POINT_UNIT_DATA WHERE OTHER_UNIT.OTHER_UNIT_ID=POINT_UNIT_DATA.UNIT_ID AND OTHER_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND OTHER_UNIT.ENTITY_ID=ENTITY.ENTITY_ID AND OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID) END UNIT_TYPE");

                        //dbisht6 end
                    }
                    //changes for MITS 31997 end
                    //rupal:end

                    sbSQL.Append(" ,B.CODE_DESC | CHKSTATUSTEXT ");
                    //sbSQL.Append(" FROM FUNDS_TRANS_SPLIT,CODES_TEXT A,FUNDS,CODES_TEXT B,RESERVE_CURRENT,COVERAGE_X_LOSS  ");
                    //sbSQL.Append(" ,POLICY_X_CVG_TYPE,CODES_TEXT POL,POLICY, POLICY_X_UNIT,CODES_TEXT LOSSCODE ");
                    //sbSQL.Append(" WHERE  FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                    //sbSQL.Append(" AND FUNDS_TRANS_SPLIT.RC_ROW_ID= RESERVE_CURRENT.RC_ROW_ID");
                    //sbSQL.Append(" AND COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID");
                    //sbSQL.Append(" AND POLICY_X_CVG_TYPE.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID ");
                    //sbSQL.Append("   AND LOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE  ");
                    //sbSQL.Append("   AND POL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE ");
                    //sbSQL.Append("  AND   POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                    //sbSQL.Append(" AND POLICY.POLICY_ID =POLICY_X_UNIT.POLICY_ID");
                   
                    //JIRA 7810 Start Snehal
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL.Append(" ,ISNULL((SELECT STUFF((SELECT ', ' + CT.CODE_DESC  FROM HOLD_REASON");
                        sbSQL.Append(" INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                        sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE =" + p_objUserLogin.objUser.NlsCode);//RMA-14373 
                        sbSQL.Append(" WHERE ON_HOLD_ROW_ID = T.ON_HOLD_ROW_ID AND HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                        sbSQL.Append(" FROM HOLD_REASON  T WHERE HOLD_STATUS_CODE =  " + objCache.GetCodeId("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID =  " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID   ),'') AS HOLD_REASON");
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {

                        sbSQL.Append(" ,nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID)");
                        sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                        sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE = " + p_objUserLogin.objUser.NlsCode + " WHERE HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "CHECK_STATUS").ToString());//RMA-14373 
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLD_REASON");

                    }


                    //JIRA 7810 End
                    sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT ");
                    sbSQL.Append(" Inner Join CODES_TEXT A On A.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE And A.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" LEFT OUTER Join CODES_TEXT UA On UA.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE And UA.LANGUAGE_CODE = ");
                    sbSQL.Append(iLangCode);
                    sbSQL.Append(" Inner Join FUNDS On FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                    sbSQL.Append(" Inner Join CODES_TEXT B On b.CODE_ID = FUNDS.STATUS_CODE ");
                    sbSQL.Append(" INNER JOIN ENTITY ENTITY2 ON ENTITY2.ENTITY_ID = FUNDS.PAYEE_EID  "); //asharma326 added to check attached payee current status jira 7810
                    sbSQL.Append(" Inner Join RESERVE_CURRENT On FUNDS_TRANS_SPLIT.RC_ROW_ID= RESERVE_CURRENT.RC_ROW_ID ");
                    sbSQL.Append(" Inner Join COVERAGE_X_LOSS On COVERAGE_X_LOSS.CVG_LOSS_ROW_ID= RESERVE_CURRENT.POLCVG_LOSS_ROW_ID ");
                    sbSQL.Append(" Inner Join POLICY_X_CVG_TYPE On POLICY_X_CVG_TYPE.POLCVG_ROW_ID = COVERAGE_X_LOSS.POLCVG_ROW_ID ");
                    sbSQL.Append(" Inner Join CODES_TEXT POL On POL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE AND POL.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" LEFT OUTER Join CODES_TEXT UPOL On UPOL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE AND UPOL.LANGUAGE_CODE = ");
                    sbSQL.Append(iLangCode);
                    sbSQL.Append(" Inner Join POLICY_X_UNIT ON POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                    sbSQL.Append(" Inner Join POLICY On POLICY.POLICY_ID = POLICY_X_UNIT.POLICY_ID ");
                    sbSQL.Append(" Inner Join CODES_TEXT LOSSCODE On LOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE AND LOSSCODE.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" LEFT OUTER Join CODES_TEXT ULOSSCODE On ULOSSCODE.CODE_ID=COVERAGE_X_LOSS.LOSS_CODE AND ULOSSCODE.LANGUAGE_CODE = ");
                    sbSQL.Append(iLangCode);
                    sbSQL.Append(" Inner Join CODES_TEXT RESERVE_TYPE On RESERVE_TYPE.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE AND RESERVE_TYPE.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" LEFT OUTER Join CODES_TEXT URESERVE_TYPE On URESERVE_TYPE.CODE_ID=RESERVE_CURRENT.RESERVE_TYPE_CODE AND URESERVE_TYPE.LANGUAGE_CODE = ");
                    sbSQL.Append(iLangCode);
                    sbSQL.Append(" WHERE");

                }
                else
                {
                    //Ankit-Start changes for MITS 29909 Custom Paging
                    sbMainSQL.Append("SELECT * FROM (");
                    sbSQL.Append(" SELECT ROW_NUMBER() OVER(ORDER BY ");
                    if (p_sSortCol.CompareTo(string.Empty) == 0)
                        sbSQL.Append("FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID");
                    else
                        sbSQL.Append(ModifySortColumn(p_sSortCol, sDBType));

                    if (!bAscending)
                        sbSQL.Append(" DESC");
                    //mits Prashant  35247 STARTS
                    if (p_sSortCol == "Offset")
                    {
                        if (!bAscending)
                            sbSQL.Append(",FUNDS.VOID_DATE DESC");
                        else
                            sbSQL.Append(",FUNDS.VOID_DATE");
                    }
                    //mits Prashant  35247 ends
                    //rsharma220 MITS 33794
                    //stop_pay_flag added by swati agarwal MITS # 3431
                    //sbSQL.Append(" ) as rno, A.CODE_ID, FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.CLAIM_ID,FUNDS.CLAIM_NUMBER, ");
                    sbSQL.Append(" ) as rno, A.CODE_ID, FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.STOP_PAY_FLAG, FUNDS.CLAIM_ID,FUNDS.CLAIM_NUMBER, ");
                    //change end here by swati
                    // akaushik5 Changed for MITS 37343 Starts
                    //sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.SETTLEMENT_FLAG,FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    // akaushik5 Changed for MITS 37343 Ends
                    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,FUNDS_TRANS_SPLIT.INVOICE_AMOUNT | FIAMT,");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, A.CODE_DESC, A.SHORT_CODE TRANSSHORTCODE, FUNDS_TRANS_SPLIT.FROM_DATE, ");
                    //start -Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG,FUNDS.MANUAL_CHECK,FUNDS.OFFSET_FLAG, ");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG,FUNDS.MANUAL_CHECK,FUNDS.OFFSET_FLAG,FUNDS.CHECK_TOTAL, ");
                    //end - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    //stop_py_date added by swati for MITS #33431
                    //sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,B.CODE_DESC | CHKSTATUSTEXT,FUNDS.VOID_DATE,FUNDS.CHECK_MEMO ");
                    //JIRA 7810 start  fix RMA-13278
                    // sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,B.CODE_DESC | CHKSTATUSTEXT,FUNDS.VOID_DATE,FUNDS.CHECK_MEMO,FUNDS.STOP_PAY_DATE,ENTITY2.ENTITY_APPROVAL_STATUS, FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE , RESERVE_TYPE.SHORT_CODE RES_TYPE_SHORT_CODE, RESERVE_TYPE.CODE_DESC RES_TYPE_CODE_DESC,  URESERVE_TYPE.CODE_DESC URES_TYPE_CODE_DESC");//addded "ENTITY2.ENTITY_APPROVAL_STATUS" for jira 7810
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,B.CODE_DESC | CHKSTATUSTEXT,FUNDS.VOID_DATE,FUNDS.CHECK_MEMO,FUNDS.STOP_PAY_DATE, ");
                  
                    sbSQL.Append(" CASE");
                    sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                    sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS IN(" + objCache.GetCodeId("R", "ENTITY_APPRV_REJ") + ")) THEN ");
                    sbSQL.Append(objCache.GetCodeId("R", "ENTITY_APPRV_REJ"));

                    sbSQL.Append("   WHEN EXISTS (SELECT ENTITY.ENTITY_APPROVAL_STATUS FROM FUNDS_X_PAYEE ,ENTITY WHERE PAYEE_EID=ENTITY_ID");
                    sbSQL.Append(" AND FUNDS_X_PAYEE.FUNDS_TRANS_ID=FUNDS.TRANS_ID AND ENTITY.ENTITY_APPROVAL_STATUS IN(" + objCache.GetCodeId("P", "ENTITY_APPRV_REJ") + ",0)) THEN ");
                    sbSQL.Append(objCache.GetCodeId("P", "ENTITY_APPRV_REJ"));

                    sbSQL.Append(" ELSE ");
                    sbSQL.Append(objCache.GetCodeId("A", "ENTITY_APPRV_REJ"));

                    sbSQL.Append(" END ");

                    sbSQL.Append("  ENTITY_APPROVAL_STATUS , ");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE , RESERVE_TYPE.SHORT_CODE RES_TYPE_SHORT_CODE, RESERVE_TYPE.CODE_DESC RES_TYPE_CODE_DESC,  URESERVE_TYPE.CODE_DESC URES_TYPE_CODE_DESC");
                    //JIRA 7810 END  fix RMA-13278
                    //JIRA 7810 Start Snehal
                    if (sDBType == Constants.DB_SQLSRVR)
                    {
                        sbSQL.Append(" ,ISNULL((SELECT STUFF((SELECT ', ' + CT.CODE_DESC  FROM HOLD_REASON");
                        sbSQL.Append(" INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                        sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE =" + p_objUserLogin.objUser.NlsCode);//RMA-14373 
                        sbSQL.Append(" WHERE ON_HOLD_ROW_ID = T.ON_HOLD_ROW_ID AND HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') SHORT_CODE_DESC");
                        sbSQL.Append(" FROM HOLD_REASON  T WHERE HOLD_STATUS_CODE =  " + objCache.GetCodeId("H", "CHECK_STATUS").ToString());
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID =  " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID   ),'') AS HOLD_REASON");
                    }
                    else if (sDBType == Constants.DB_ORACLE)
                    {

                        sbSQL.Append(" ,nvl( (SELECT LISTAGG(CT.CODE_DESC, ',') WITHIN GROUP (ORDER BY ON_HOLD_ROW_ID)");
                        sbSQL.Append(" FROM HOLD_REASON INNER JOIN CODES  C ON HOLD_REASON.HOLD_REASON_CODE = C.CODE_ID");
                        sbSQL.Append(" INNER JOIN CODES_TEXT CT ON C.CODE_ID = CT.CODE_ID AND C.TABLE_ID= " + objCache.GetTableId("HOLD_REASON_CODE") + " AND CT.LANGUAGE_CODE = " + p_objUserLogin.objUser.NlsCode + " WHERE HOLD_STATUS_CODE = " + objCache.GetCodeId("H", "CHECK_STATUS").ToString());//RMA-14373 
                        sbSQL.Append(" AND ON_HOLD_TABLE_ID = " + objCache.GetTableId("FUNDS"));
                        sbSQL.Append(" AND ON_HOLD_ROW_ID = FUNDS.TRANS_ID GROUP BY ON_HOLD_ROW_ID) ,'') AS HOLD_REASON");

                    }
                    //JIRA 7810 End

                    //change end by swati
                    sbSQL.Append(" FROM  CODES_TEXT B , CODES_TEXT A,FUNDS INNER JOIN ENTITY ENTITY2 ON ENTITY2.ENTITY_ID = FUNDS.PAYEE_EID, FUNDS_TRANS_SPLIT ");//addded "INNER JOIN ENTITY ENTITY2 ON ENTITY2.ENTITY_ID = FUNDS.PAYEE_EID" for jira 7810
					sbSQL.Append(" Inner Join CODES_TEXT RESERVE_TYPE On RESERVE_TYPE.CODE_ID=FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE AND RESERVE_TYPE.LANGUAGE_CODE = ");
                    sbSQL.Append(sBaseLangCode);
                    sbSQL.Append(" LEFT OUTER Join CODES_TEXT URESERVE_TYPE On URESERVE_TYPE.CODE_ID=FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE AND URESERVE_TYPE.LANGUAGE_CODE = ");
                    sbSQL.Append(iLangCode);
                    sbSQL.Append(" WHERE  FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                }
                //Ankit-MITS 29909-Start Changes:Done for custom filtering. Since custom paging is implemented, 
                //we will not be using the filtering supplied by telerik grid.
                #region looping through number of AND conditions
                //Code to determine if there is a single filter or multiple filters               
                if (p_sFilterValue.CompareTo(string.Empty) != 0)
                {
                    sTempFilterString = p_sFilterValue;
                    iTempIndex = p_sFilterValue.IndexOf("AND");
                    if (iTempIndex == -1) //in case there is only a single filter
                    {
                        iNumOfFilters = 1;
                    }
                    else
                    {
                        do
                        {
                            iTempIndex = sTempFilterString.IndexOf("AND");
                            if (iTempIndex == -1)
                                break;
                            iTempIndex = iTempIndex + 3;
                            iNumOfFilters = iNumOfFilters + 1;
                            sTempFilterString = sTempFilterString.Substring(iTempIndex, sTempFilterString.Length - iTempIndex);
                        } while (iTempIndex < p_sFilterValue.Length);
                        iNumOfFilters = iNumOfFilters + 1;
                    }

                    sTempFilterString = p_sFilterValue;
                    while (iCounter < iNumOfFilters)
                    {
                        if (p_sFilterValue.CompareTo(string.Empty) != 0)
                        {
                            sTempFilterString = sTempFilterString.Substring(iStartIndex);
                            //Fetch table column name
                            iStartIndex = sTempFilterString.IndexOf("[");
                            iEndIndex = sTempFilterString.IndexOf("]");
                            sFilterColumn = (sTempFilterString.Substring(iStartIndex + 1, (iEndIndex - iStartIndex) - 1)).Trim();
                            //Fetch comparison operator
                            iStartIndex = iEndIndex + 2;
                            iEndIndex = sTempFilterString.IndexOf("'");
                            sFilterOperator = (sTempFilterString.Substring(iStartIndex, (iEndIndex - iStartIndex))).Trim();
                            //Fetch comparison value
                            iStartIndex = sTempFilterString.IndexOf("'");
                            if ((iNumOfFilters == 1) || (iCounter == (iNumOfFilters - 1)))
                                iEndIndex = (sTempFilterString.LastIndexOf("')")) - 1;
                            else
                            {
                                iEndIndex = (sTempFilterString.IndexOf("') AND")) - 1;
                            }

                            sFilterValue = (sTempFilterString.Substring(iStartIndex + 1, iEndIndex - iStartIndex)).Trim();
							//rkulavil :RMA-12789/MITS 38521 starts
                            //sFilterValue = sFilterValue.Replace("'", "''");
							//rkulavil :RMA-12789/MITS 38521 ends
                            iStartIndex = iEndIndex + 3;
                        }

                        switch (sFilterColumn)
                        {
						//rkulavil :RMA-12789/MITS 38521 starts
                            case "CtlNumber": 
                                                if (sDBType == Constants.DB_SQLSRVR)
                                                    sFilterColumn = "FUNDS.CTL_NUMBER";
                                                else
                                                {
                                                    sFilterColumn = " UPPER(LTRIM(RTRIM(FUNDS.CTL_NUMBER)))";
                                                    sFilterValue = sFilterValue.ToUpper();
                                                }
                                                sFilterValue = "'" + sFilterValue + "'";
                                                break;
                                            //sFilterColumn = "FUNDS.CTL_NUMBER";
                                            //                sFilterValue = "'" + sFilterValue + "'";                                                
                                            //break;
							//rkulavil :RMA-12789/MITS 38521 ends
                            case "TransNumber": sFilterColumn = "FUNDS.TRANS_NUMBER";
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            case "TransDate": sFilterColumn = "FUNDS.TRANS_DATE";
							//rkulavil :RMA-12789/MITS 38521 starts
                                              //sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                                string[] tdate = sFilterValue.Split(' ');
                                                if (tdate.Length > 1)
                                                {
                                                    sFilterOperator = ">=";
                                                    sFilterValue = string.Format("'{0}{1}{2}'", Conversion.GetDate(tdate[0]), "' AND FUNDS.TRANS_DATE <='", Conversion.GetDate(tdate[1])); ;
                                                }
                                                else
                                                {
                                                    sFilterValue = string.Format("'{0}'",Conversion.GetDate(tdate[0]));
                                                }
                            //rkulavil :RMA-12789/MITS 38521 ends
                                break;
                            case "PaymentFlag": //sFilterColumn = "FUNDS.PAYMENT_FLAG";                                                                                                     
                                //if (sFilterValue.ToLower().CompareTo("payment") == 0)
                                //    {
                                //        sFilterOperator = "<>";
                                //        sFilterValue = "0";
                                //    }
                                //    else
                                //        sFilterValue = "2"; //random int value                                                       
                                // mkaran2 - MITS 32620 - Start
                                if (sFilterValue.ToLower().Contains("payment"))
                                {
                                    sFilterColumn = "FUNDS.PAYMENT_FLAG";
                                    sFilterOperator = "<>";
                                    sFilterValue = "0";
                                }
                                else if (sFilterValue.ToLower().Contains("collection"))
                                {
                                    sFilterColumn = "FUNDS.COLLECTION_FLAG";
                                    sFilterOperator = "<>";
                                    sFilterValue = "0";
                                }
                                else  //random int value
                                {
                                    sFilterColumn = "FUNDS.PAYMENT_FLAG";
                                    sFilterOperator = "=";
                                    sFilterValue = "2";
                                }
                                break;
                            // mkaran2 - MITS 32620 - End

                            case "ClearedFlag": sFilterColumn = "FUNDS.CLEARED_FLAG";
                                reDateExpr = new Regex("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]");
                                bIsCorrectFormat = reDateExpr.IsMatch(sFilterValue);
                                if (sFilterValue.ToLower().CompareTo("no") == 0)
                                {
                                    sFilterValue = "0";
                                }
                                else if ((sFilterValue.Length == 10) && (bIsCorrectFormat))
                                {
                                    sFilterColumn = "FUNDS.VOID_DATE";
													//rkulavil :RMA-12789/MITS 38521 starts
                                                    //sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                                    sFilterValue = String.Format("'{0}'{1}", Conversion.GetDate(sFilterValue),"AND FUNDS.CLEARED_FLAG=-1");
													//rkulavil :RMA-12789/MITS 38521 ends
                                }
                                else
                                    sFilterValue = "2";
                                break;
                            case "VoidFlag": sFilterColumn = "FUNDS.VOID_FLAG";
                                reDateExpr = new Regex("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]");
                                bIsCorrectFormat = reDateExpr.IsMatch(sFilterValue);
                                if (sFilterValue.ToLower().CompareTo("no") == 0)
                                {
                                    sFilterValue = "0";
                                }
                                else if ((sFilterValue.Length == 10) && (bIsCorrectFormat))
                                {
                                    sFilterColumn = "FUNDS.VOID_DATE";
													//rkulavil :RMA-12789/MITS 38521 starts
                                                    //sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                                    sFilterValue = String.Format("'{0}'{1}", Conversion.GetDate(sFilterValue), "AND FUNDS.VOID_FLAG=-1");
													//rkulavil :RMA-12789/MITS 38521 ends
                                }
                                else
                                    sFilterValue = "2";
                                break;
                            //Added by Swati Agarwal for WWIG MITS # 33431
                            case "StopPayFlag": sFilterColumn = "FUNDS.STOP_PAY_FLAG";
                                reDateExpr = new Regex("[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]");
                                bIsCorrectFormat = reDateExpr.IsMatch(sFilterValue);
                                if (sFilterValue.ToLower().CompareTo("no") == 0)
                                {
									//rkulavil :RMA-12789/MITS 38521 starts
                                    //sFilterValue = "0";
                                    sFilterValue = "0 OR FUNDS.STOP_PAY_FLAG IS NULL)";
                                    sFilterColumn = string.Format("({0}", sFilterColumn);
									//rkulavil :RMA-12789/MITS 38521 ends
                                }
                                else if ((sFilterValue.Length == 10) && (bIsCorrectFormat))
                                {
                                    sFilterColumn = "FUNDS.STOP_PAY_DATE";
													//rkulavil :RMA-12789/MITS 38521 starts
                                                    //sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                                    sFilterValue = Conversion.GetDate(sFilterValue);
													//rkulavil :RMA-12789/MITS 38521 ends
                                }
                                else
                                    sFilterValue = "2";
                                break;
                            //end by Swati Agarwal
                            //Prashant MITS 35247 starts
                            case "ClearVoidFlagedFlag": sFilterColumn = "FUNDS.VOID_FLAG";
                                if (sFilterValue.ToLower().CompareTo("no") == 0)
                                {
                                    sFilterValue = "0";
                                }
                                else if ((sFilterValue.Length == 8))
                                {
                                    sFilterColumn = "FUNDS.VOID_DATE";
                                    sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                }
                                else
                                    sFilterValue = "2";
                                break;
                            //case "Offset": sFilterColumn = "FUNDS.OFFSET_FLAG";
                            //                        if (sFilterValue.ToLower().CompareTo("yes") == 0)
                            //                        {
                            //                            sFilterOperator = "<>";
                            //                            sFilterValue = "0";
                            //                        }
                            //                        else if (sFilterValue.ToLower().CompareTo("no") == 0)
                            //                        {
                            //                            sFilterValue = "0";
                            //                            if (sbSQL.ToString().EndsWith("WHERE") == false)
                            //                                sbSQL.Append(" AND FUNDS.VOID_FLAG = 0");
                            //                            else
                            //                                sbSQL.Append(" FUNDS.VOID_FLAG = 0");
                            //                        }
                            //                        else if (sFilterValue.ToLower().CompareTo("na") == 0)
                            //                        {
                            //                            sFilterValue = "0";
                            //                            if (sbSQL.ToString().EndsWith("WHERE") == false)
                            //                                sbSQL.Append(" AND FUNDS.VOID_FLAG = -1");
                            //                            else
                            //                                sbSQL.Append(" FUNDS.VOID_FLAG = -1");
                            //                        }
                            //                        else
                            //                            sFilterValue = "2";
                            //                        break;
                            //Prashant MITS 35247 ends 
                            case "CheckStatus": sFilterColumn = "FUNDS.STATUS_CODE";
                                //mini perofrmance mits
                                int iCheckStatusCode = m_objDataModelFactory.Context.LocalCache.GetCheckStatus(sFilterValue.ToUpper());
                                sFilterValue = iCheckStatusCode.ToString();
                                break;
                            case "Name": if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = " ISNULL(FUNDS.FIRST_NAME + ' ' + FUNDS.LAST_NAME,FUNDS.LAST_NAME) ";
                                else
                                {
                                    //avipinsrivas start : Worked for JIRA - 11604
                                    //sFilterColumn = " upper(nvl(CONCAT(FIRST_NAME,LAST_NAME),LAST_NAME)) ";
                                    //sFilterColumn = " UPPER(TRIM(FIRST_NAME || ' ' || LAST_NAME)) ";
                                    sFilterColumn = " UPPER(TRIM(CONCAT(CONCAT(FIRST_NAME,' '),LAST_NAME))) ";
									//rkulavil :RMA-12789/MITS 38521 starts
                                    //sFilterValue = CommonFunctions.HTMLCustomEncode(sFilterValue).ToUpper();
                                    sFilterValue = sFilterValue.ToUpper();
									//rkulavil :RMA-12789/MITS 38521 ends
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            //Ashish Ahuja Mits 34270 added case for check memo
                            case "CheckMemo": //sFilterColumn = " FUNDS.CHECK_MEMO";
                                //sFilterValue = "'" + sFilterValue + "'";
                                if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = "FUNDS.CHECK_MEMO";
                                else
                                {
                                    sFilterColumn = " upper(ltrim(rtrim(FUNDS.CHECK_MEMO)))";
                                    sFilterValue = sFilterValue.ToUpper();
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            case "Amount": if (sFilterValue.CompareTo(string.Empty) == 0)
                                    sFilterColumn = "FUNDS.AMOUNT";
                                else
                                {
                                    if (sDBType == Constants.DB_ORACLE)
                                        sFilterColumn = "ROUND(FUNDS.AMOUNT,2) ";
                                    else
                                        sFilterColumn = "CONVERT(DECIMAL(37,2),FUNDS.AMOUNT)";
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                //avipinsrivas start : Worked for JIRA - 11949
                                //sFilterValue = sFilterValue.Replace("$", "");
                                //sFilterValue = sFilterValue.Replace("£", "");
                                sFilterValue = GetFilteredAmount(sFilterValue);
                                //avipinsrivas end
                                break;
                            case "FromDate": sFilterColumn = "FUNDS_TRANS_SPLIT.FROM_DATE";
                                sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                break;
                            case "ToDate": sFilterColumn = "FUNDS_TRANS_SPLIT.TO_DATE";
                                sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                break;
                            case "InvoiceNumber": sFilterColumn = "FUNDS_TRANS_SPLIT.INVOICE_NUMBER";
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            case "CodeDesc": if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = "(A.SHORT_CODE + ' ' + A.CODE_DESC)";
                                else
                                {
                                    //sFilterColumn = " upper(A.SHORT_CODE + ' ' + A.CODE_DESC) ";
                                    sFilterColumn = " upper(ltrim(rtrim(CONCAT(CONCAT(A.SHORT_CODE, ' '),A.CODE_DESC)))) "; //mkaran2 - MITS 35712
                                    sFilterValue = sFilterValue.ToUpper();
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            case "SplitAmount": if (sFilterValue.CompareTo(string.Empty) == 0)
                                    sFilterColumn = "FUNDS_TRANS_SPLIT.AMOUNT";
                                else
                                {
                                    if (sDBType == Constants.DB_ORACLE)
                                        sFilterColumn = "ROUND(FUNDS_TRANS_SPLIT.AMOUNT,2) ";
                                    else
                                        sFilterColumn = "CONVERT(DECIMAL(37,2),FUNDS_TRANS_SPLIT.AMOUNT)";
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                //avipinsrivas start : Worked for JIRA - 11949
                                //sFilterValue = sFilterValue.Replace("$", "");
                                //sFilterValue = sFilterValue.Replace("£", "");
                                sFilterValue = GetFilteredAmount(sFilterValue);
                                //avipinsrivas end
                                break;
                            case "User":
                                //sFilterColumn = "FUNDS_TRANS_SPLIT.ADDED_BY_USER";
                                //sFilterValue = "'" + sFilterValue + "'";
                                //                          break;
                                //mkaran2 - MITS 35712 - Start
                                if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = "FUNDS_TRANS_SPLIT.ADDED_BY_USER";
                                else
                                {
                                    sFilterColumn = " upper(ltrim(rtrim(FUNDS_TRANS_SPLIT.ADDED_BY_USER)))";
                                    sFilterValue = sFilterValue.ToUpper();
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            //mkaran2 - MITS 35712 - End
                            case "CheckDate": sFilterColumn = "FUNDS.DATE_OF_CHECK";
                                sFilterValue = Conversion.GetDBDateFormat(sFilterValue, "yyyyMMdd");
                                break;
                            case "CombinedPay": sFilterColumn = "FUNDS.COMBINED_PAY_FLAG";
                                if (sFilterValue.ToLower().CompareTo("yes") == 0)
                                {
                                    sFilterOperator = "<>";
                                    sFilterValue = "0";
                                }
                                else if (sFilterValue.ToLower().CompareTo("no") == 0)
                                    sFilterValue = "0";
                                else
                                    sFilterValue = "2";
                                break;
                            case "IsFirstFinal": sFilterColumn = "FUNDS_TRANS_SPLIT.IS_FIRST_FINAL";
                                if (sFilterValue.ToLower().CompareTo("yes") == 0)
                                {
                                    sFilterOperator = "<>";
                                    sFilterValue = "0";
                                }
                                else if (sFilterValue.ToLower().CompareTo("no") == 0)
                                    sFilterValue = "0";
                                else
                                    sFilterValue = "2";
                                break;
                                break;
                            case "PolicyName": if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = "POLICY.POLICY_NAME";
                                else
                                {
                                    sFilterColumn = " upper(POLICY.POLICY_NAME)";
                                    sFilterValue = sFilterValue.ToUpper();
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            case "CoverageType": if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = "(POL.SHORT_CODE + ' ' + POL.CODE_DESC)";
                                else
                                {
                                    //sFilterColumn = " upper(POL.SHORT_CODE + ' ' + POL.CODE_DESC)";
                                    sFilterColumn = " upper(ltrim(rtrim(CONCAT(CONCAT(POL.SHORT_CODE, ' '), POL.CODE_DESC))))  "; //mkaran2 - MITS 35712
                                    sFilterValue = sFilterValue.ToUpper();
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            case "Unit": sFilterColumn = "POLICY_X_UNIT.UNIT_ID";
                                break;
                            case "LossType": if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = "(LOSSCODE.SHORT_CODE + ' ' + LOSSCODE.CODE_DESC)";
                                else
                                {
                                    //sFilterColumn = " upper(LOSSCODE.SHORT_CODE + ' ' + LOSSCODE.CODE_DESC)";
                                    sFilterColumn = " upper(ltrim(rtrim(CONCAT(CONCAT(LOSSCODE.SHORT_CODE, ' '), LOSSCODE.CODE_DESC))))  "; //mkaran2 - MITS 35712
                                    sFilterValue = sFilterValue.ToUpper();
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            case "ManualCheck": sFilterColumn = "FUNDS.MANUAL_CHECK";
                                if (sFilterValue.ToLower().CompareTo("yes") == 0)
                                {
                                    sFilterOperator = "<>";
                                    sFilterValue = "0";
                                }
                                else if (sFilterValue.ToLower().CompareTo("no") == 0)
                                    sFilterValue = "0";
                                else
                                    sFilterValue = "2"; //random int value
                                break;
                            // mkaran2 - MITS 35712 -- Start
                            case "Offset": sFilterColumn = "FUNDS.OFFSET_FLAG";
                                if (sFilterValue.ToLower().CompareTo("na") == 0)
                                {
                                    sFilterColumn = "(FUNDS.VOID_FLAG=-1 OR FUNDS.AMOUNT < 0 OR FUNDS.COLLECTION_FLAG <> 0)";
                                    sFilterOperator = "";
                                    sFilterValue = "";
                                }
                                else if (sFilterValue.ToLower().CompareTo("no") == 0)
                                {
                                    sFilterColumn = "( FUNDS.VOID_FLAG <> -1 AND FUNDS.AMOUNT >= 0 AND FUNDS.COLLECTION_FLAG = 0 AND (FUNDS.OFFSET_FLAG = 0 OR FUNDS.OFFSET_FLAG IS NULL))";
                                    sFilterOperator = "";
                                    sFilterValue = "";
                                }
                                else if (sFilterValue.ToLower().CompareTo("yes") == 0)
                                {
                                    sFilterValue = "-1";
                                }
                                else
                                    sFilterValue = "2";

                                break;
                            // mkaran2 - MITS 35712 -- End
                            case "InvoiceAmount": if (sFilterValue.CompareTo(string.Empty) == 0)
                                    sFilterColumn = "FUNDS_TRANS_SPLIT.INVOICE_AMOUNT";
                                else
                                {
                                    if (sDBType == Constants.DB_ORACLE)
                                        sFilterColumn = "ROUND(FUNDS_TRANS_SPLIT.INVOICE_AMOUNT,2) ";
                                    else
                                        sFilterColumn = "CONVERT(DECIMAL(37,2),FUNDS_TRANS_SPLIT.INVOICE_AMOUNT)";
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                //avipinsrivas start : Worked for JIRA - 11949
                                //sFilterValue = sFilterValue.Replace("$", "");
                                //sFilterValue = sFilterValue.Replace("£", "");
                                sFilterValue = GetFilteredAmount(sFilterValue);
                                //avipinsrivas end
                                break;
                            //Start - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                            case "CheckTotal": if (sFilterValue.CompareTo(string.Empty) == 0)
                                    sFilterColumn = "FUNDS.CHECK_TOTAL";
                                else
                                {
                                    if (sDBType == Constants.DB_ORACLE)
                                        sFilterColumn = "ROUND(FUNDS.CHECK_TOTAL,2) ";
                                    else
                                        sFilterColumn = "CONVERT(DECIMAL(37,2),FUNDS.CHECK_TOTAL)";
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                //avipinsrivas start : Worked for JIRA - 11949
                                //sFilterValue = sFilterValue.Replace("$", "");
                                //sFilterValue = sFilterValue.Replace("£", "");
                                sFilterValue = GetFilteredAmount(sFilterValue);
                                //avipinsrivas end
                                break;
                            //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                            case "ReserveType":
                                if (sDBType == Constants.DB_SQLSRVR)
                                    sFilterColumn = "(RESERVE_TYPE.SHORT_CODE + ' ' + RESERVE_TYPE.CODE_DESC )";
                                else
                                {
                                    //sFilterColumn = " upper(LOSSCODE.SHORT_CODE + ' ' + LOSSCODE.CODE_DESC)";
                                    sFilterColumn = " upper(ltrim(rtrim(CONCAT(CONCAT(RESERVE_TYPE.SHORT_CODE, ' '), RESERVE_TYPE.CODE_DESC))))  "; //mkaran2 - MITS 35712
                                    sFilterValue = sFilterValue.ToUpper();
                                }
                                sFilterValue = "'" + sFilterValue + "'";
                                break;
                            default: break;
                        }
                        switch (sFilterOperator)
                        {
                            case "&gt;": sFilterOperator = ">"; break;
                            case "&lt;": sFilterOperator = "<"; break;
                            case "&lt;&gt;": sFilterOperator = "<>"; break;
                            case "&gt;=": sFilterOperator = ">="; break;
                            case "&lt;=": sFilterOperator = "<="; break;
                            case "LIKE": if (sFilterValue.IndexOf("'") == -1)
                                    sFilterValue = "'" + sFilterValue + "'"; break;
							//rkulavil :RMA-12789/MITS 38521 starts
                            case "NOT LIKE":

                                if (sFilterValue.IndexOf("'") == -1)
                                {
                                    sFilterValue = sFilterValue + "' OR " + sFilterColumn + " IS NULL )"; 

                                }
                                else
                                {
                                    sFilterValue = sFilterValue + " OR " + sFilterColumn + " IS NULL )"; 
                                }
                                sFilterColumn = "(" + sFilterColumn;
                                break;
							//rkulavil :RMA-12789/MITS 38521 ends
                            default: break;
                        }

                        if (sDBType == Constants.DB_ORACLE)
                        {
                            if ((sFilterValue.CompareTo("") == 0) || sFilterValue.CompareTo("''") == 0)
                            {
                                if (sFilterOperator.CompareTo("<>") == 0)
                                    sFilterOperator = " IS NOT NULL ";
                                else if (sFilterOperator.CompareTo("=") == 0)
                                    sFilterOperator = " IS NULL ";
                                sFilterValue = "";
                            }
                        }

                        if (p_sFilterValue.CompareTo(string.Empty) != 0)
                        {
                            if (sbSQL.ToString().EndsWith("WHERE") == false)
                            { sbSQL.Append(" AND "); }

                            sbSQL.Append(" ");//nkaranam2 - 29004
							//rkulavil :RMA-12789/MITS 38521 starts
                            //sbSQL.Append(sFilterColumn);
                            //if (string.Compare(sFilterOperator,"<>") == 0 ? sbSQL.Append("("+sFilterColumn) : sbSQL.Append(sFilterColumn));
                            if (string.Compare(sFilterOperator, "<>") == 0)
                            {
                                sbSQL.Append("(" + sFilterColumn);
                            }
                            else
                            {
                                sbSQL.Append(sFilterColumn);
                            }
							//rkulavil :RMA-12789/MITS 38521 ends
                            sbSQL.Append(" ");
                            sbSQL.Append(sFilterOperator);
                            sbSQL.Append(" ");
							//rkulavil :RMA-12789/MITS 38521 starts
                            //sbSQL.Append(sFilterValue);
                            //if (string.Equals(sFilterOperator,"<>") ?sbSQL.Append(sFilterValue + " OR " + sFilterColumn + " IS NULL )"):sbSQL.Append(sFilterValue));
                            if (string.Compare(sFilterOperator, "<>") == 0)
                            {
                                sbSQL.Append(sFilterValue + " OR " + sFilterColumn + " IS NULL )");
                            }
                            else
                            {
                                sbSQL.Append(sFilterValue);
                            }
							//rkulavil :RMA-12789/MITS 38521 ends
                        }
                        iCounter = iCounter + 1;
                    }
                }
                #endregion looping through number of AND conditions
                //Ankit-MITS 29909 End Changes             
                if (p_iEntityId != 0)
                {

                    if (sbSQL.ToString().EndsWith("WHERE") == false)
                        sbSQL.Append("  AND FUNDS.PAYEE_EID = " + p_iEntityId);
                    else
                        sbSQL.Append(" FUNDS.PAYEE_EID = " + p_iEntityId);
                }
                else
                {
                    if (sbSQL.ToString().EndsWith("WHERE") == false)
                        sbSQL.Append(" AND FUNDS.CLAIM_ID = " + p_iClaimId);
                    else
                        sbSQL.Append(" FUNDS.CLAIM_ID = " + p_iClaimId);
                }
                if (p_iClaimantEid > 0)
                {
                    if (sbSQL.ToString().EndsWith("WHERE") == false)
                    {
                        if (objSysSetting.MultiCovgPerClm == -1)
                            sbSQL.Append(" AND RESERVE_CURRENT.CLAIMANT_EID = " + p_iClaimantEid);
                        else
                        {
                            sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid);
                        }
                    }
                    else
                    {
                        if (objSysSetting.MultiCovgPerClm == -1)
                            sbSQL.Append("  RESERVE_CURRENT.CLAIMANT_EID = " + p_iClaimantEid);
                        else
                        {
                            sbSQL.Append("  FUNDS.CLAIMANT_EID = " + p_iClaimantEid);
                        }
                    }
                    //if (sbSQL.ToString().EndsWith("WHERE") == false)
                    //    sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid);
                    //else
                    //    sbSQL.Append(" FUNDS.CLAIMANT_EID = " + p_iClaimantEid);

                }
                else
                {
                    if (p_iUnitId > 0)
                        if (sbSQL.ToString().EndsWith("WHERE") == false)
                            sbSQL.Append(" AND FUNDS.UNIT_ID = " + p_iUnitId);
                        else
                            sbSQL.Append(" FUNDS.UNIT_ID = " + p_iUnitId);
                }

                //MITS 29909 Start Changes-Ankit	
                //Aman MITS 31281
                //rsharma220 MITS 33794
                if (iMultiCovgPerClm == -1)
                {
                    if (sDBType == Constants.DB_ORACLE)
                        sbSQL.Append(" ) mytable ");
                    else
                        sbSQL.Append(" ) as mytable ");
                }
                else
                {
                    if (sDBType == Constants.DB_ORACLE)
                        sbSQL.Append(" AND A.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE AND b.CODE_ID = FUNDS.STATUS_CODE AND A.LANGUAGE_CODE = 1033 ) mytable ");
                    else
                        sbSQL.Append(" AND A.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE AND b.CODE_ID = FUNDS.STATUS_CODE AND A.LANGUAGE_CODE = 1033 ) as mytable ");
                }

                //Aman MITS 31281                
                // akaushik5 Changed for Performance Improvement Starts
                // sbSQLCount.Append("SELECT COUNT(*) as rwcount FROM (");
                sbSQLCount.Append("SELECT COUNT(rno) as rwcount FROM (");
                // akaushik5 Changed for Performance Improvement Ends
                sbSQLCount.Append(sbSQL);
                //MITS 29909:End changes

                if (sDBType == Constants.DB_ACCESS)
                    sbSQLCount = sbSQLCount.Replace("|", " AS ");
                else
                    sbSQLCount = sbSQLCount.Replace("|", " ");
                //Ankit MITS 29909-Start changes

                // akaushik5 Changed for Performance Improvement Starts
                // objDataSet = DbFactory.GetDataSet(m_sConnectionString, sbSQLCount.ToString());
                // if (objDataSet.Tables.Count > 0)
                // {
                //      lTotalRecCount = Conversion.ConvertObjToInt(objDataSet.Tables[0], m_iClientId.Rows[0]["rwcount"]);
                // }
                bool success;
                lTotalRecCount = Conversion.CastToType<long>(DbFactory.ExecuteScalar(m_sConnectionString, sbSQLCount.ToString()).ToString(), out success);
                // akaushik5 Changed for Performance Improvement Ends
                #region Paging Logic
                int p_lPageCount = (int)(lTotalRecCount / iNumOfRecordsPerPage); //total number of pages
                float fCalPages = (float)((float)lTotalRecCount / (float)iNumOfRecordsPerPage);

                if ((float)(p_lPageCount) < (float)(fCalPages))
                {
                    p_lPageCount = p_lPageCount + 1;
                }
                if (p_lPageCount == 0)
                {
                    p_lPageCount = 1;
                }
                if (p_iPageNumber == 0)
                    p_iPageNumber = 1;
                if (p_iPageNumber == 1)
                {
                    lStartAt = 1;
                    uppercount = lTotalRecCount;
                }
                else
                {
                    lStartAt = ((p_iPageNumber - 1) * iNumOfRecordsPerPage) + 1;
                }
                if (p_lPageCount > 1)
                {
                    if (p_iPageNumber == p_lPageCount)
                        uppercount = lTotalRecCount;
                    else
                        uppercount = (lStartAt + iNumOfRecordsPerPage) - 1;
                }
                else
                    uppercount = lTotalRecCount;
                //modify query to fetch records between start and uppercount
                #endregion

                // akaushik5 Commented for MITS 31151 Starts
                //sbSQL.Append(" WHERE rno BETWEEN ");
                //sbSQL.Append(lStartAt);
                //sbSQL.Append(" AND ");
                //sbSQL.Append(uppercount);
                // akaushik5 Commented for MITS 31151 Ends

                // akaushik5 Commented of Performance Improvement Starts
                // sbSortValue = new StringBuilder();
                // akaushik5 Commented of Performance Improvement Ends
                //mkaran2- MITS 32340 Start changes
                if (p_sSortCol == "")
                {
                    // akaushik5 Changed for MITS 36343 Starts
                    //sSortCol = "FUNDS.TRANS_DATE";                                        
                    sSortCol = "FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID";
                    // akaushik5 Changed for MITS 36343 Ends
                    bAscending = false;
                }
                else
                {
                    sSortCol = p_sSortCol;
                    //sSortCol = ModifySortColumn(sSortCol, sDBType);
                    //mkaran2- MITS 32340 Start changes                    
                    // akaushik5 Changed for MITS 37344 Starts
                    //if(sSortCol == "Name")
                    //{
                    //    if (sDBType == Constants.DB_ORACLE)
                    //        sSortCol = "Name_Oracle";
                    //    else
                    //        sSortCol = "Name_SQL";
                    //}
                    //else
                    //    sSortCol = ModifySortColumn(sSortCol, sDBType);
                    switch (sSortCol)
                    {
                        case "Name":
                            sSortCol = sDBType.Equals(Constants.DB_ORACLE) ? "Name_Oracle" : "Name_SQL";
                            break;
                        case "CodeDesc":
                            sSortCol = sDBType.Equals(Constants.DB_ORACLE) ? "CodeDesc_Oracle" : "CodeDesc_SQL";
                            break;
                        default:
                            sSortCol = ModifySortColumn(sSortCol, sDBType);
                            break;
                    }
                    // akaushik5 Changed for MITS 37344 Ends
                    //mkaran2- MITS 32340 Start changes
                    bAscending = p_bAscending;
                }
                //mkaran2- MITS 32340 End changes                
                sbMainSQL.Append(sbSQL);
                if (sDBType == Constants.DB_ACCESS)
                    sbMainSQL = sbMainSQL.Replace("|", " AS ");
                else
                    sbMainSQL = sbMainSQL.Replace("|", " ");

                // Initialize a new XML document and create Payment History node
                this.StartDocument(ref objRootNode, "PaymentHistory");
                //ddhiman : commented MITS 27009
                //this.CreateAndSetElement(objRootNode, "Title", sTitle);
                //End ddhiman
                this.CreateAndSetElement(objRootNode, "ShowAutoCheckbtn", sAutoCheckbtn);//Nadim for 16254(REM Customization)
                //rupal: for first & final payment                
                //mini perofrmance mits
                this.CreateAndSetElement(objRootNode, "CarrierClaims", iMultiCovgPerClm.ToString());
                //this.CreateAndSetElement(objRootNode, "CarrierClaims", m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());
                //mini performance 
                objHtTransIds = new Hashtable();

                //Manish for multi currency MITS 27209
                string culture = CommonFunctions.GetCulture(p_iClaimId, CommonFunctions.NavFormType.None, m_sConnectionString);
                // akaushik5 Added for MITS 31151 Starts
                int rowNo = default(int);
                bool rowSuccess;
                // akaushik5 Added for MITS 31151 Ends

                bool bIsOffsetAllowed = IsOffsetPermission();
                //Start Changes for MITS 29909
                //Aman MITS 31281
                //Deb : Uncommented  ML Changes is here dont comment this
                // akaushik5 Commented for Performance Improvement Starts
                //rsharma220 MITS 33794
                // string sBaseLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0];
                //int iLangCode = p_objUserLogin.objUser.NlsCode;
                StringBuilder sbMain = new StringBuilder();
                if (iMultiCovgPerClm != -1)
                {
                    sbMain.Append(sbMainSQL.Replace("1033", iLangCode.ToString()) + " UNION " + sbMainSQL.Replace("1033", sBaseLangCode) + @" AND mytable.CODE_ID NOT IN (SELECT CODES.CODE_ID FROM CODES INNER JOIN CODES_TEXT
                 ON CODES.CODE_ID = CODES_TEXT.CODE_ID WHERE CODES.TABLE_ID = (SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'TRANS_TYPES') AND CODES_TEXT.LANGUAGE_CODE = " + iLangCode.ToString() + ")");
                }
                // akaushik5 Commented for Performance Improvement Ends
                //Aman MITS 31281
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbMainSQL.ToString()))
                //End Changes for MITS 29909
                {
                    while (objReader.Read())
                    {
                        // akaushik5 Added for MITS 31151 Starts
                        rowNo = Conversion.CastToType<int>(objReader.GetValue("rno").ToString(), out rowSuccess);
                        if (rowSuccess && rowNo >= lStartAt && rowNo <= uppercount)
                        {
                            // akaushik5 Added for MITS 31151 Ends
                            iTransId = Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
                            //Added by Amitosh for mits 23603  (05/09/2011)
                            this.CreateElement(objRootNode, "row", ref objRowNode);
                            //end Amitosh
                            //mbahl3 Mits 28614
                            //if (iTransId != iLastTransId)
                            //{
                            //mbahl3 mits: 28614
                            // akaushik5 Commented for Performance Improvement Starts    
                            // iLastTransId = iTransId ;
                            // akaushik5 Commented for Performance Improvement Ends
                            //Start averma62 MITS 26999
                            string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + iTransId;
                            if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                            {
                                sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                                sEntSQL = sEntSQL.Replace("+", "||");

                            }
                            // akaushik5 Added for MITS 37343 Starts
                            sName = string.Empty;
                            // akaushik5 Added for MITS 37343 Ends
                            using (DbReader objeReader = DbFactory.GetDbReader(m_sConnectionString, sEntSQL))
                            {
                                while (objeReader.Read())
                                {
                                    if (sPayeesName == string.Empty)
                                    {
                                        sPayeesName = objeReader.GetString("PAYEES_NAME");
                                    }
                                    else
                                    {
                                        sPayeesName = sPayeesName + "<br> " + objeReader.GetString("PAYEES_NAME");
                                    }

                                    // akaushik5 Added for MITS 37343 Starts
                                    sName = string.IsNullOrEmpty(sName) ? objeReader.GetString("PAYEES_NAME") : string.Format("{0} and {1}", sName, objeReader.GetString("PAYEES_NAME"));
                                    // akaushik5 Added for MITS 37343 Ends
                                    iNoOfPayees++;

                                }
                            }
                            //End averma62 MITS 26999

                            bVoid = Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId);
                            sVoid = bVoid ? "Yes" : "No";

                            //Added by Swati Agarwal for MITS # 33431
                            bool bstopPaySuccess;    //Added by Swati
                            bStopPay = Conversion.CastToType<bool>(objReader.GetValue("STOP_PAY_FLAG").ToString(), out bstopPaySuccess);
                            sStopPay = bStopPay ? "Yes" : "No";
                            //change end here by swati

                            bCleared = Common.Conversion.ConvertObjToBool(objReader.GetValue("CLEARED_FLAG"), m_iClientId);
                            sCleared = bCleared ? "Yes" : "No";

                            bPayment = Common.Conversion.ConvertObjToBool(objReader.GetValue("PAYMENT_FLAG"), m_iClientId);
                            sPayment = bPayment ? "Payment" : "Collection";

                            //rupal:start, for first & final payment
                            bIsFirstFinal = Common.Conversion.ConvertObjToBool(objReader.GetValue("IS_FIRST_FINAL"), m_iClientId);
                            sIsFirstFinal = bIsFirstFinal ? "Yes" : "No";
                            //rupal:end, for first & final payment

                            //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database
                            iTransNumber = Common.Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_NUMBER"), m_iClientId);
                            sTransNumber = iTransNumber.ToString();

                            sCtlNumber = objReader.GetString("CTL_NUMBER");
                            sOffset = objReader.GetBoolean("OFFSET_FLAG").ToString();
                            //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                            dblCheckTotal = Common.Conversion.ConvertObjToDouble(objReader.GetValue("CHECK_TOTAL"), m_iClientId);

                            sCheckTotal = CommonFunctions.ConvertByCurrencyCode(objSysSetting.BaseCurrencyType, dblCheckTotal, m_sConnectionString, m_iClientId);
                            //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                            sCheckDate = Conversion.GetDBDateFormat(objReader.GetString("DATE_OF_CHECK"), "d");
                            sTransDate = Conversion.GetDBDateFormat(objReader.GetString("TRANS_DATE"), "d");
                            sAddedByUser = objReader.GetString("ADDED_BY_USER");

                            // akaushik5 Added for MITS 37343 Starts
                            if (string.IsNullOrEmpty(sName))
                            // akaushik5 Added for MITS 37343 Ends
                            {
                                sName = objReader.GetString("FIRST_NAME");
                                if (sName != "")
                                    sName += " " + objReader.GetString("LAST_NAME");
                                else
                                    sName = objReader.GetString("LAST_NAME");
                            }
                            //Manish Jain Multicurrency MITS 27209
                            dblAmount = Common.Conversion.ConvertObjToDouble(objReader.GetValue("FAMT"), m_iClientId);
                            if (!bPayment)
                                dblAmount = -dblAmount;
                            //Manish for multi currency MITS 27209
                            sAmount = CommonFunctions.ConvertByCurrencyCode(m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, dblAmount, m_sConnectionString, m_iClientId);


                            iCheckStatusCodeId = Common.Conversion.ConvertObjToInt(objReader.GetValue("STATUS_CODE"), m_iClientId);
                            //mini perofrmance mits
                            m_objDataModelFactory.Context.LocalCache.GetCodeInfo(iCheckStatusCodeId, ref sCheckStatusCode, ref sCheckStatusDesc);
                            sCheckStatus = sCheckStatusDesc;
                            //JIRA 7810 Start Snehal                           
                            if ((Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_APPROVAL_STATUS"), m_iClientId).Equals(m_objDataModelFactory.Context.LocalCache.GetCodeId("A", "ENTITY_APPRV_REJ")))
                                  && (!string.IsNullOrEmpty(Common.Conversion.ConvertObjToStr(objReader.GetValue("HOLD_REASON"))))
                                  && (Common.Conversion.ConvertObjToStr(objReader.GetValue("HOLD_REASON")).Contains(m_objDataModelFactory.Context.LocalCache.GetCodeDesc(m_objDataModelFactory.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")))))
                            {

                                if ((Common.Conversion.ConvertObjToStr(objReader.GetValue("HOLD_REASON")).Trim().Equals(m_objDataModelFactory.Context.LocalCache.GetCodeDesc(m_objDataModelFactory.Context.LocalCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT"))))
                                  && (iCheckStatusCodeId != objCache.GetCodeId("R", "CHECK_STATUS")))
                                {
                                    sHoldReason = objCache.GetCodeDesc(objCache.GetCodeId("PAYE_APRV_PYM_UNAPRV", "HOLD_REASON_PARENT"));
                                }
                                else
                                {
                                    sHoldReason = (Common.Conversion.ConvertObjToStr(objReader.GetValue("HOLD_REASON")).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',').Replace(", ,", ",");

                                }
                                // sHoldReason = (Common.Conversion.ConvertObjToStr(objReader.GetValue("HOLD_REASON")).Replace(objCache.GetCodeDesc(objCache.GetCodeId("PAYEE_NOT_APPRV", "HOLD_REASON_PARENT")), "")).Trim().TrimEnd(',').TrimStart(',');
                            }
                            else
                            {
                                sHoldReason = Common.Conversion.ConvertObjToStr(objReader.GetValue("HOLD_REASON"));
                            }
                            //JIRA 7810 End
                            //Added by Amitosh for mits 23603  (05/09/2011)

                            sPayee = objReader.GetString("LAST_NAME") + ", " + objReader.GetString("FIRST_NAME");
                            // akaushik5 Commented for MITS 37343 Starts
                            //if (objReader.GetInt16("SETTLEMENT_FLAG") == -1 && !string.IsNullOrEmpty(sClaimant) && !sPayee.Equals(sClaimant))
                            //{
                            //    if (!string.IsNullOrEmpty(sClaimantFirstName))
                            //    {
                            //        sName = sName + " and " + sClaimantFirstName + " " + sClaimantLastName;
                            //    }
                            //    else
                            //    {
                            //        sName = sName + " and " + sClaimantLastName;
                            //    }

                            //}
                            // akaushik5 Commented for MITS 37343 Ends
                            //mini performance mits
                            //else
                            //{
                            //    sName = sName;
                            //}
                            //mini performance mits
                            //skhare7:start, R8 combined Pay
                            bIsCombinedPay = Common.Conversion.ConvertObjToBool(objReader.GetValue("COMBINED_PAY_FLAG"), m_iClientId);
                            sIsCombinedPay = bIsCombinedPay ? "Yes" : "No";
                            //RUPAL:start, ON BEHALF OF SMAHAJAN22-MITS 35587
                            int iPolicySystemId = 0;
                            //bool bReturn=false;
                            //rupal:end ON BEHALF OF SMAHAJAN22-MITS 35587
                            //mini perofrmance mits
                            if (iMultiCovgPerClm == -1)
                            //if (m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                            //mini performance mits
                            { //skhare7 r8 
                                sPolicyName = objReader.GetString("POLICY_NAME");
                                //RUPAL:start.ON BEHALF OF SMAHAJAN22-MITS 35587
                                iPolicySystemId = objReader.GetInt("POLICY_SYSTEM_ID");
                                if (iPolicySystemId > 0)
                                {
                                    sPolcyCvgType = objReader.GetString("COVERAGETEXT");//smahajan22 - mits 35587
                                }
                                else
                                {
                                    sPolcyCvgType = objReader.GetString("SHORTCOVERAGETYPE") + " " + (string.IsNullOrEmpty(objReader.GetString("UCOVERAGETYPE")) ? objReader.GetString("COVERAGETYPE") : objReader.GetString("UCOVERAGETYPE"));//smahajan22 - mits 35587
                                }
                                //RUPAL:end - ON BEHALF OF SMAHAJAN22-MITS 35587
                                sLossType = objReader.GetString("SHORT_CODE") + " " + (string.IsNullOrEmpty(objReader.GetString("U_LOSS_CODE")) ? objReader.GetString("LOSS_CODE") : objReader.GetString("U_LOSS_CODE"));
                                sUnitType = objReader.GetString("UNIT_TYPE");
                            }
                            //skhare7:start, R8 combined Pay End
                            ////end Amitosh
                            //Ashish Ahuja Mits 34270
                            sCheckMemo = Common.Conversion.ConvertObjToStr(objReader.GetValue("CHECK_MEMO"));
                            //Ankit Start : Financials Enhancements - Manual Check / Void, Cleared Date Changes
                            bIsManualCheck = Common.Conversion.ConvertObjToBool(objReader.GetValue("MANUAL_CHECK"), m_iClientId);
                            sIsManualCheck = bIsManualCheck ? "Yes" : "No";

                            sVoidDate = Conversion.GetDBDateFormat(objReader.GetString("VOID_DATE"), "d");
                            //Added by swati agarwal for MITS # 33431
                            sStopPayDate = Conversion.GetDBDateFormat(objReader.GetString("STOP_PAY_DATE"), "d");
                            //end here by swati
                            //Ankit End

                            //ddhiman : MITS 27009
                            if (string.IsNullOrEmpty(sTitle) && p_iEntityId != 0)
                            {
                                sTitle = sName;
                            }
                            //End ddhiman
                            //mini perofrmance mits
                            if (iMultiCovgPerClm == -1)
                            //if (m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                            //mini perofrmance mits
                            { //skhare7 r8 
                                sPolicyName = objReader.GetString("POLICY_NAME");
                                //sPolcyCvgType = objReader.GetString("SHORTCOVERAGETYPE") + " " + (string.IsNullOrEmpty(objReader.GetString("UCOVERAGETYPE")) ? objReader.GetString("COVERAGETYPE") : objReader.GetString("UCOVERAGETYPE"));//smahajan22 - mits 35587
                                //sPolcyCvgType = objReader.GetString("COVERAGETEXT");//smahajan22 - mits 35587

                                //RUPAL:start.ON BEHALF OF SMAHAJAN22-MITS 35587
                                iPolicySystemId = objReader.GetInt("POLICY_SYSTEM_ID");
                                if (iPolicySystemId > 0)
                                {
                                    sPolcyCvgType = objReader.GetString("COVERAGETEXT");//smahajan22 - mits 35587
                                }
                                else
                                {
                                    sPolcyCvgType = objReader.GetString("SHORTCOVERAGETYPE") + " " + (string.IsNullOrEmpty(objReader.GetString("UCOVERAGETYPE")) ? objReader.GetString("COVERAGETYPE") : objReader.GetString("UCOVERAGETYPE"));//smahajan22 - mits 35587
                                }
                                //RUPAL:end - ON BEHALF OF SMAHAJAN22-MITS 35587

                                sUnitType = objReader.GetString("UNIT_TYPE");
                                sLossType = objReader.GetString("SHORT_CODE") + " " + (string.IsNullOrEmpty(objReader.GetString("U_LOSS_CODE")) ? objReader.GetString("LOSS_CODE") : objReader.GetString("U_LOSS_CODE"));
                            }
                            // Code for calculation of total only once for a TransID - Mihika
                            if (!objHtTransIds.Contains(objReader.GetValue("TRANS_ID")))
                            {
                                if (bVoid)
                                {
                                    if (bPayment)
                                        dblTotalVoidPay += dblAmount;
                                    else
                                        dblTotalVoidCollect += dblAmount;
                                }
                                else
                                {
                                    if (bPayment)
                                        dblTotalPay += dblAmount;
                                    else
                                        dblTotalCollect += dblAmount;
                                }

                                objHtTransIds.Add(objReader.GetValue("TRANS_ID"), objReader.GetValue("TRANS_ID"));
                            }
                            // End of code -Mihika
                            sFromDate = Conversion.GetDBDateFormat(objReader.GetString("FROM_DATE"), "d");
                            sToDate = Conversion.GetDBDateFormat(objReader.GetString("TO_DATE"), "d");

                            sInvoiceNumber = objReader.GetString("INVOICE_NUMBER");
                            if (iMultiCovgPerClm == -1)
                            {
                                sCodeDesc = objReader.GetString("TRANSSHORTCODE") + " " + (string.IsNullOrEmpty(objReader.GetString("U_CODE_DESC")) ? objReader.GetString("CODE_DESC") : objReader.GetString("U_CODE_DESC"));
                            }
                            else
                                sCodeDesc = objReader.GetString("TRANSSHORTCODE") + " " + objReader.GetString("CODE_DESC");
                            //rsharma220 MITS 33794 End
                            //Manish Jain Multicurrency MITS 27209
                            ////dblSplitAmount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr( objReader.GetValue( "FTSAMT" ) ));
                            dblSplitAmount = Common.Conversion.ConvertObjToDouble(objReader.GetValue("FTSAMT"), m_iClientId);//Deb Changes 

                            if (!bPayment)
                                dblSplitAmount = -dblSplitAmount;
                            dblIAmount = Common.Conversion.ConvertObjToDouble(objReader.GetValue("FIAMT"), m_iClientId);
                            if (!bPayment)
                                dblIAmount = -dblIAmount;

                            this.CreateAndSetElement(objRowNode, "Name", sName);
                            //Moved at start of while loop
                            this.CreateAndSetElement(objRowNode, "CtlNumber", sCtlNumber, ref objTempNode);
                            //objCache.GetCodeInfo(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId), ref sReserveTypeCode, ref sReserveTypeCodeDesc, iLangCode);
                            //this.CreateAndSetElement(objRowNode, "ReserveType", sReserveTypeCode+" "+sReserveTypeCodeDesc);
                            sReserveType = objReader.GetString("RES_TYPE_SHORT_CODE") + " " + (string.IsNullOrEmpty(objReader.GetString("URES_TYPE_CODE_DESC")) ? objReader.GetString("RES_TYPE_CODE_DESC") : objReader.GetString("URES_TYPE_CODE_DESC"));
                            this.CreateAndSetElement(objRowNode, "ReserveType", sReserveType);
                            objTempNode.SetAttribute("TransId", iTransId.ToString());
                            if (!bIsOffsetAllowed)
                            {
                                sOffset = "SMS";
                            }
                            this.CreateAndSetElement(objRowNode, "Offset", sOffset, ref objTempNode);
                            objTempNode.SetAttribute("TransId", iTransId.ToString());
                            this.CreateAndSetElement(objRowNode, "TransNumber", sTransNumber);
                            this.CreateAndSetElement(objRowNode, "TransDate", sTransDate);
                            this.CreateAndSetElement(objRowNode, "PaymentFlag", sPayment);
                            //rupal:start, for first & final payme
                            //Start averma62 MITS 26999
                            if (string.IsNullOrEmpty(sPayeesName))
                            {
                                if (!string.IsNullOrEmpty(sClaimantFirstName))
                                {
                                    sPayeesName = sClaimantFirstName + " " + sClaimantLastName;
                                }
                                else
                                {
                                    sPayeesName = sClaimantLastName;
                                }
                            }
                            this.CreateAndSetElement(objRowNode, "Payees_Name", sPayeesName.Trim());
                            this.CreateAndSetElement(objRowNode, "NoOfPayees", iNoOfPayees.ToString());
                            sPayeesName = string.Empty;
                            iNoOfPayees = 0;
                            //End averma62 MITS 26999
                            this.CreateAndSetElement(objRowNode, "IsFirstFinal", sIsFirstFinal);

                            //rupal:end, for first & final payment
                            this.CreateAndSetElement(objRowNode, "ClearedFlag", sCleared);
                            this.CreateAndSetElement(objRowNode, "VoidFlag", sVoid);
                            //Added by swati agarwal for MITS # 33431
                            this.CreateAndSetElement(objRowNode, "StopPayFlag", sStopPay);
                            //end here by Swati
                            this.CreateAndSetElement(objRowNode, "CheckStatus", sCheckStatus);
                            this.CreateAndSetElement(objRowNode, "HoldReason", sHoldReason);//JIRA 7810  Snehal
                           
                            this.CreateAndSetElement(objRowNode, "Amount", sAmount);
                            //Nikhil
                            this.CreateAndSetElement(objRowNode, "CheckTotal", sCheckTotal);
                            //Manish Jain Multicurrency MITS 27209
                            this.CreateAndSetElement(objRowNode, "DAmount", Convert.ToString(dblAmount));
                            this.CreateAndSetElement(objRowNode, "FromDate", sFromDate);
                            this.CreateAndSetElement(objRowNode, "ToDate", sToDate);
                            this.CreateAndSetElement(objRowNode, "InvoiceNumber", sInvoiceNumber);
                            this.CreateAndSetElement(objRowNode, "CodeDesc", sCodeDesc);
                            //this.CreateAndSetElement( objRowNode , "SplitAmount" , string.Format( "{0:C}" , dblSplitAmount ) );
                            //Manish Jain Multicurrency MITS 27209
                            //mini perofrmance mits
                            this.CreateAndSetElement(objRowNode, "SplitAmount", CommonFunctions.ConvertByCurrencyCode(m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, dblSplitAmount, m_sConnectionString, m_iClientId));
                            this.CreateAndSetElement(objRowNode, "InvoiceAmount", CommonFunctions.ConvertByCurrencyCode(m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, dblIAmount, m_sConnectionString, m_iClientId));
                            this.CreateAndSetElement(objRowNode, "DSplitAmount", Convert.ToString(dblSplitAmount));
                            this.CreateAndSetElement(objRowNode, "DIAmount", Convert.ToString(dblIAmount));
                            this.CreateAndSetElement(objRowNode, "User", sAddedByUser);
                            this.CreateAndSetElement(objRowNode, "CheckDate", sCheckDate);
                            //Manish Jain Multicurrency MITS 27209
                            this.CreateAndSetElement(objRowNode, "BaseCurrency", culture);
                            //nadim for 15388
                            //this.CreateAndSetElement(objRowNode, "Title", sTitle);
                            //nadim for 153888
                            //skhare7:start, for R8 combined Pay

                            this.CreateAndSetElement(objRowNode, "CombinedPay", sIsCombinedPay);
                            //skhare7 r8 combined Payment
                            //skhare7 r8
                            //mini perofrmance mits 
                            if (iMultiCovgPerClm == -1)
                            //if (m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
                            //mini performance mits
                            {
                                this.CreateAndSetElement(objRowNode, "PolicyName", sPolicyName);
                                this.CreateAndSetElement(objRowNode, "CoverageType", sPolcyCvgType);
                                this.CreateAndSetElement(objRowNode, "Unit", sUnitType);
                                this.CreateAndSetElement(objRowNode, "LossType", sLossType);
                            }

                            //skhare7:start, for R8 combined Pay End
                            //Ankit Start : Financials Enhancements - Manual Check / Void, Cleared Date Changes
                            this.CreateAndSetElement(objRowNode, "ManualCheck", sIsManualCheck);
                            //Ashish Ahuja Mits 34270
                            this.CreateAndSetElement(objRowNode, "CheckMemo", sCheckMemo);
                            this.CreateAndSetElement(objRowNode, "VoidDate", sVoidDate);
                            //Added by swati agarwal for MITS # 33431
                            this.CreateAndSetElement(objRowNode, "StopPayDate", sStopPayDate);
                            //end here by Swati
                            //Ankit End
                            //Ankit Start : MITS 31315
                            this.CreateAndSetElement(objRowNode, "IsCleared", bCleared.ToString());
                            this.CreateAndSetElement(objRowNode, "IsVoided", bVoid.ToString());
                            //Added by swati agarwal for MITS # 33431
                            this.CreateAndSetElement(objRowNode, "IsStopPayed", bStopPay.ToString());
                            //end here by Swati
                            //Ankit End
                        }
                        // akaushik5 Added for MITS 31151 Starts
                        if (rowSuccess && (rowNo < lStartAt || rowNo > uppercount))
                        {
                            if (!objHtTransIds.Contains(objReader.GetValue("TRANS_ID")))
                            {
                                bool voidSuccess;
                                bool stopPaySuccess;    //Added by Swati
                                bool paymentSuccess;
                                bool amountSuccess;

                                bVoid = Conversion.CastToType<bool>(objReader.GetValue("VOID_FLAG").ToString(), out voidSuccess);
                                //Added by swati agarwal for MITS # 33431
                                bStopPay = Conversion.CastToType<bool>(objReader.GetValue("STOP_PAY_FLAG").ToString(), out stopPaySuccess);
                                //end here by Swati
                                bPayment = Conversion.CastToType<bool>(objReader.GetValue("PAYMENT_FLAG").ToString(), out paymentSuccess);
                                dblAmount = Conversion.CastToType<double>(objReader.GetValue("FAMT").ToString(), out amountSuccess);

                                if (amountSuccess)
                                {
                                    //or condition added by Swati MITS # 33431
                                    //if (voidSuccess && bVoid)
                                    if ((voidSuccess && bVoid) || (stopPaySuccess && bStopPay))
                                    {
                                        if (paymentSuccess && bPayment)
                                        {
                                            dblTotalVoidPay += dblAmount;
                                        }
                                        else
                                        {
                                            dblTotalVoidCollect -= dblAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (paymentSuccess && bPayment)
                                        {
                                            dblTotalPay += dblAmount;
                                        }
                                        else
                                        {
                                            dblTotalCollect -= dblAmount;
                                        }
                                    }
                                }

                                objHtTransIds.Add(objReader.GetValue("TRANS_ID"), objReader.GetValue("TRANS_ID"));
                            }
                        }
                        // akaushik5 Added for MITS 31151 Ends
                    }
                }

                dblNetTotal = dblTotalPay + dblTotalCollect;
                dblTotalVoid = dblTotalVoidPay + dblTotalVoidCollect;
                //Total collection amount needs to be displayed as positive MITS 8823
                dblTotalCollect = -dblTotalCollect;
                this.CreateAndSetElement(objRootNode, "TotalAll", string.Format("{0:C}", dblNetTotal));
                this.CreateAndSetElement(objRootNode, "TotalCollect", string.Format("{0:C}", dblTotalCollect));
                this.CreateAndSetElement(objRootNode, "TotalPay", string.Format("{0:C}", dblTotalPay));
                this.CreateAndSetElement(objRootNode, "TotalVoid", string.Format("{0:C}", dblTotalVoid));
                this.CreateAndSetElement(objRootNode, "PaymentsLessVoids", string.Format("{0:C}", dblTotalPay - dblTotalVoid));

                this.CreateAndSetElement(objRootNode, "SortCol", sSortCol);
                this.CreateAndSetElement(objRootNode, "Ascending", bAscending.ToString());

                //ddhiman : MITS 27009
                this.CreateAndSetElement(objRootNode, "Title", sTitle);
                //End ddhiman
                //Ankit:MITS 29834:Start changes
                this.CreateAndSetElement(objRootNode, "PageSize", iNumOfRecordsPerPage.ToString());
                this.CreateAndSetElement(objRootNode, "VirtualItemCount", lTotalRecCount.ToString());
                //Ankit:MITS 29834:End changes

                //Deb MITS 30185
                string sUserPrefsXML = string.Empty;
                XmlDocument objUserPrefXML = new XmlDocument();
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnectionString, "Select PREF_XML from USER_PREF_XML where USER_ID=" + UserId.ToString()))
                {
                    if (objRdr.Read())
                    {
                        sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                    }
                }
                sUserPrefsXML = sUserPrefsXML.Trim();
                //objUserPrefXML = new XmlDocument(); mini commented for performance testing (it is declared twice)
                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }
                CreateUserPreferXML(objUserPrefXML);
                this.CreateAndSetElement(objRootNode, "Config", objUserPrefXML.SelectSingleNode("setting/PayHisConfig").OuterXml);
                objUserPrefXML = null;
                //Deb MITS 30185
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPaymentHistory.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //mini performance mits 
                sbMainSQL = null;
                sbSQLCount = null;
                //mini performance mits 
                sbSQL = null;
                objRootNode = null;
                objRowNode = null;
                // akaushik5 Commented of Performance Improvement Starts
                // sentSQL = null; //averma62
                // akaushik5 Commented of Performance Improvement Ends

                objHtTransIds = null;
                //mini perofrmance mits
                //if (objLocalCache != null)
                //    objLocalCache.Dispose();
                //nadim for 15388

                //if (objLobSettings != null)
                //{
                //    objLobSettings = null;
                //}
                //mini perofrmance mits
                //nadim for 15388

            }
            return (m_objDocument);
        }
        private bool IsOffsetPermission()   //mcapps2 MITS 30236 Start
        {
            UserLogin objUserlogin = null;
            int RMO_OFFSET = 47;
            int RMB_FUNDS_TRANSACT = 9650;

            try
            {

                objUserlogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);//rkaur27

                if (objUserlogin.IsAllowedEx(RMB_FUNDS_TRANSACT, RMO_OFFSET))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            finally
            {
                objUserlogin = null;
            }
        }  //mcapps2 MITS 30236 End
        #endregion
        /// <summary>
        /// //Deb MITS 30185
        /// </summary>
        /// <param name="p_objUserPrefXML"></param>
        private void CreateUserPreferXML(XmlDocument p_objUserPrefXML)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting");
                    objTempNode = p_objUserPrefXML.CreateElement("PayHisConfig");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Control");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Control");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Check");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Check");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Type");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Type");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Cleared");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Cleared");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Void");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Void");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Status");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Status");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //JIRA 7810 Start Snehal
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/HoldReason");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("HoldReason");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ReserveType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ReserveType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //JIRA 7810 End
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Payee");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Payee");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckAmount");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckAmount");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/FromDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("FromDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ToDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ToDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Invoice");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Invoice");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransactionType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransactionType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/SplitAmount");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SplitAmount");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/InvoiceAmount");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("InvoiceAmount");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/User");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("User");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CombinedPay");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CombinedPay");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                //Deb
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/IsFirstFinal");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("IsFirstFinal");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/PolicyName");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PolicyName");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CoverageType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CoverageType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Unit");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Unit");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/LossType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("LossType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ManualCheck");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ManualCheck");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //Ashish Ahuja Mits 34270 start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckMemo");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckMemo");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //Ashish Ahuja Mits 34270 end
                //dvatsa- JIRA 11858(start)
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckTotal");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckTotal");
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //dvatsa - JIRA 11858(end)
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ControlHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ControlHeader");
                    objTempNode.SetAttribute("value", "Control #");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckHeader");
                    objTempNode.SetAttribute("value", "Check #");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransDateHeader");
                    objTempNode.SetAttribute("value", "Trans Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TypeHeader");
                    objTempNode.SetAttribute("value", "Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ClearedHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClearedHeader");
                    objTempNode.SetAttribute("value", "Cleared");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/VoidHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("VoidHeader");
                    objTempNode.SetAttribute("value", "Void");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/StatusHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("StatusHeader");
                    objTempNode.SetAttribute("value", "Status");
                    objParentNode.AppendChild(objTempNode);
                }

                //JIRA 7810 Start Snehal
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/HoldReasonHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("HoldReasonHeader");
                    objTempNode.SetAttribute("value", "Hold Reason");
                    objParentNode.AppendChild(objTempNode);
                }
                //JIRA 7810 End
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/PayeeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PayeeHeader");
                    objTempNode.SetAttribute("value", "Payee");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckAmountHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckAmountHeader");
                    objTempNode.SetAttribute("value", "Check Amount");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/FromDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("FromDateHeader");
                    objTempNode.SetAttribute("value", "From Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ToDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ToDateHeader");
                    objTempNode.SetAttribute("value", "To Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/InvoiceHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("InvoiceHeader");
                    objTempNode.SetAttribute("value", "Invoice #");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransactionTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransactionTypeHeader");
                    objTempNode.SetAttribute("value", "Transaction Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/SplitAmountHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SplitAmountHeader");
                    objTempNode.SetAttribute("value", "Split Amount");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/InvoiceAmountHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("InvoiceAmountHeader");
                    objTempNode.SetAttribute("value", "Invoice Amount");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/UserHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserHeader");
                    objTempNode.SetAttribute("value", "User");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckDateHeader");
                    objTempNode.SetAttribute("value", "Check Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CombinedPayHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CombinedPayHeader");
                    objTempNode.SetAttribute("value", "Combined Pay");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/IsFirstFinalHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("IsFirstFinalHeader");
                    objTempNode.SetAttribute("value", "IsFirstFinal");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/PolicyNameHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PolicyNameHeader");
                    objTempNode.SetAttribute("value", "Policy Name");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CoverageTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CoverageTypeHeader");
                    objTempNode.SetAttribute("value", "Coverage Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/UnitHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UnitHeader");
                    objTempNode.SetAttribute("value", "Unit");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/LossTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("LossTypeHeader");
                    objTempNode.SetAttribute("value", "Loss Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ManualCheckHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ManualCheckHeader");
                    objTempNode.SetAttribute("value", "Manual Check");
                    objParentNode.AppendChild(objTempNode);
                }
                //Ashish Ahuja Mits 34270 start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckMemoHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckMemoHeader");
                    objTempNode.SetAttribute("value", "Check Memo");
                    objParentNode.AppendChild(objTempNode);
                }
                //Ashish Ahuja Mits 34270 end
                //dvatsa- JIRA 11858(start)
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckTotalHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckTotalHeader");
                    objTempNode.SetAttribute("value", "Check Total");
                    objParentNode.AppendChild(objTempNode);
                }
                //dvatsa- JIRA 11858(end)
				objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ReserveTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ReserveTypeHeader");
                    objTempNode.SetAttribute("value", "Reserve Type");
                    objParentNode.AppendChild(objTempNode);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("WPA.CreateUserPreferXML.Err", m_iClientId), p_objException);
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }
        //Deb MITS 30185
        #region Auto Check List
        //added two new parameters to the function to support sorting in auto checks.
        public XmlDocument GetAutoCheckList(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, bool p_bAscending, int p_iEntityId)
        {
            XmlElement objRootNode = null;
            XmlElement objRowNode = null;
            XmlElement objTempNode = null;
            DbReader objReader = null;

            string sSQL = "";
            int iAutoBatchID = 0;
            string sPrintDate = "";
            string sControlNumber = "";
            string sName = "";
            double dAmount = 0;
            string sAmount = "";
            double dTotal = 0;
            string sClaimNumber = "";
            int iCount = 0;

            //abisht MITS 10458
            string sSortCol = string.Empty;
            bool bAscending = false;
            string sWhere=string.Empty;
            try
            {
                // m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword );	
                // m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
                if (m_objDataModelFactory == null)
                    this.Initialize();
                //skhare7 R8 combined Payment
                if (Conversion.ConvertStrToBool(m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString()))
                {
                    if (p_iEntityId > 0)
                    {
                        sSQL = "SELECT FUNDS_AUTO.AUTO_BATCH_ID, FUNDS_AUTO.PRINT_DATE, FUNDS_AUTO.CTL_NUMBER, FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, FUNDS_AUTOAMOUNT, FUNDS_AUTO.CLAIM_NUMBER" +
                         " FROM FUNDS_AUTO";
                        sWhere = "WHERE FUNDS_AUTO.PAYEE_EID = " + p_iEntityId.ToString();

                       
                    }
                    else
                    {
                        sSQL = "SELECT FUNDS_AUTO.AUTO_BATCH_ID, FUNDS_AUTO.PRINT_DATE,FUNDS_AUTO.CTL_NUMBER, FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, FUNDS_AUTO.AMOUNT, FUNDS_AUTO.CLAIM_NUMBER" +
                             " FROM FUNDS_AUTO";

                        sWhere = "WHERE FUNDS_AUTO.CLAIM_ID = " + p_iClaimId.ToString();

                       

                    }

                    if (p_iClaimantEid > 0)
                    {
                        sSQL = sSQL + ",RESERVE_CURRENT,FUNDS_AUTO_SPLIT  ";
                        sWhere = sWhere + " AND RESERVE_CURRENT.CLAIMANT_EID = " + p_iClaimantEid.ToString() + " AND RESERVE_CURRENT.RC_ROW_ID=FUNDS_AUTO_SPLIT.RC_ROW_ID AND FUNDS_AUTO_SPLIT.AUTO_TRANS_ID=FUNDS_AUTO.AUTO_TRANS_ID";

                    }

                    sSQL = sSQL + sWhere;

                    //if (p_iClaimantEid > 0)
                    //    sSQL = sSQL + " AND CLAIMANT_EID = " + p_iClaimantEid.ToString();

                    //if (p_iUnitId > 0)
                    //    sSQL = sSQL + " AND UNIT_ID = " + p_iUnitId.ToString();

                }
                else
                {
                    if (p_iEntityId > 0)
                {
                    sSQL = "SELECT AUTO_BATCH_ID, PRINT_DATE, CTL_NUMBER, FIRST_NAME, LAST_NAME, AMOUNT, CLAIM_NUMBER" +
                     " FROM FUNDS_AUTO WHERE PAYEE_EID = " + p_iEntityId.ToString();
                }
                else
                {
                    sSQL = "SELECT AUTO_BATCH_ID, PRINT_DATE, CTL_NUMBER, FIRST_NAME, LAST_NAME, AMOUNT, CLAIM_NUMBER" +
                         " FROM FUNDS_AUTO WHERE CLAIM_ID = " + p_iClaimId.ToString();
                }

                if (p_iClaimantEid > 0)
                    sSQL = sSQL + " AND CLAIMANT_EID = " + p_iClaimantEid.ToString();

                if (p_iUnitId > 0)
                    sSQL = sSQL + " AND UNIT_ID = " + p_iUnitId.ToString();
                }

                //abisht MITS 10458
                if (p_sSortCol == "")
                {
                    sSQL = sSQL + " ORDER BY PRINT_DATE DESC ";
                    sSortCol = "PRINT_DATE";
                    bAscending = false;
                }
                else
                {
                    sSortCol = p_sSortCol;
                    bAscending = p_bAscending;
                    if (bAscending)
                        sSQL = sSQL + " ORDER BY " + sSortCol + " ASC";
                    else
                        sSQL = sSQL + " ORDER BY " + sSortCol + " DESC";
                }
                // Initialize a new XML document and create Payment History node
                this.StartDocument(ref objRootNode, "AutoCheckList");
                objRootNode.SetAttribute("ClaimId", "", p_iClaimId.ToString());
                objRootNode.SetAttribute("ClaimantEid", "", p_iClaimantEid.ToString());
                objRootNode.SetAttribute("UnitId", "", p_iUnitId.ToString());

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iAutoBatchID = Common.Conversion.ConvertObjToInt(objReader.GetValue("AUTO_BATCH_ID"), m_iClientId);
                        sControlNumber = objReader.GetString("CTL_NUMBER");
                        sClaimNumber = objReader.GetString("CLAIM_NUMBER");
                        sPrintDate = Conversion.GetDBDateFormat(objReader.GetString("PRINT_DATE"), "d");
                        sName = objReader.GetString("FIRST_NAME");
                        if (sName != "")
                            sName += " " + objReader.GetString("LAST_NAME");
                        else
                            sName = objReader.GetString("LAST_NAME");

                        dAmount = objReader.GetDouble("AMOUNT");
                        //Deb
                        //sAmount = string.Format( "{0:C}" , dAmount ) ;
                        sAmount = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), dAmount, m_sConnectionString, m_iClientId);
                        dTotal = dTotal + dAmount;

                        this.CreateElement(objRootNode, "row", ref objRowNode);
                        this.CreateAndSetElement(objRowNode, "PrintDate", sPrintDate, ref objTempNode);
                        objTempNode.SetAttribute("AutoBatchId", iAutoBatchID.ToString());
                        this.CreateAndSetElement(objRowNode, "ControlNumber", sControlNumber);
                        this.CreateAndSetElement(objRowNode, "Name", sName);
                        this.CreateAndSetElement(objRowNode, "ClaimNumber", sClaimNumber);
                        this.CreateAndSetElement(objRowNode, "Amount", sAmount);

                        iCount = iCount + 1;
                        if (iCount > 1000)
                            break;
                    }
                }
                //Deb
                //this.CreateAndSetElement( objRootNode , "Total" , string.Format( "{0:C}" , dTotal ) );
                this.CreateAndSetElement(objRootNode, "Total", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), dTotal, m_sConnectionString, m_iClientId));
                objRootNode.SetAttribute("AutoBatchId", "", iAutoBatchID.ToString());
                //abisht MITS 10458
                this.CreateAndSetElement(objRootNode, "SortCol", sSortCol);
                this.CreateAndSetElement(objRootNode, "Ascending", bAscending.ToString());
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetAutoCheckList.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
                objRowNode = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return (m_objDocument);
        }
        #endregion

        #region Get Reserves Code Information
        /// <summary>
        /// Get reserves code information for a particular transaction type
        /// </summary>
        /// <param name="p_iTransTypeCode">Transaction Type Code</param>
        /// <param name="p_iClaimId">ClaimId</param>
        /// <returns>Reserve information</returns>	

        public XmlDocument GetReservesCodeInfo(int p_iTransTypeCode, int p_iClaimId)
        {
            int iReserveTypeCode = 0;
            int iLOB = 0;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            XmlElement objRootNode = null;
            XmlElement objTempNode = null;
            Claim objClaim = null;
            LocalCache objLocalCache = null;
            try
            {

                if (p_iTransTypeCode != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                    if (p_iClaimId != 0)
                    {
                        objClaim.MoveTo(p_iClaimId);
                        iLOB = objClaim.LineOfBusCode;
                    }
                    iReserveTypeCode = GetReserveTypeCode(p_iTransTypeCode, iLOB, p_iClaimId);
                    objLocalCache.GetCodeInfo(iReserveTypeCode, ref sShortCode, ref sCodeDesc);
                }
                objRootNode = null;
                this.StartDocument(ref objRootNode, "ReservesInformation");

                this.CreateAndSetElement(objRootNode, "ReserveType", sShortCode + ' ' + sCodeDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ReserveType");
                objTempNode.SetAttribute("codeid", iReserveTypeCode.ToString());
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPaymentCollection.ErrorPaymentCollection", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
                objTempNode = null;
                if (objClaim != null)
                    objClaim.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return (m_objDocument);
        }

        #endregion


        #region Get Reserves Information
        /// <summary>
        /// Get reserves information for a particular transaction type
        /// </summary>
        /// <param name="p_iTransTypeCode">Transaction Type Code</param>
        /// <param name="p_iReserveTypeCode">Reserve Type Code</param>//added by rkaur7 - MITS 17241
        /// <param name="p_iClaimId">ClaimId</param>
        /// <param name="p_iClaimantId">ClaimantId</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <returns>Reserve information</returns>		
        public XmlDocument GetReservesInfo(int p_iTransTypeCode, int p_iReserveTypeCode, int p_iClaimId, int p_iClaimantId, int p_iUnitId, int p_iTransID, int p_iPolicyId, int p_iCovTypeCode, int p_iPolicyUnitRowId, int p_iPmtCurrCode, string p_sCovgSeqNum, int iLossCode, string p_sTransSeqNum, string p_sCoveragekey)      //Ankit Start : Worked on MITS - 34297
        {
            int iReserveTypeCode = 0;
            double dReserveBalance = 0;
            string sShortCode = "";
            string sCodeDesc = "";
            int iLOB = 0;
            int iPolicyId = p_iPolicyId;
            int iCovTypeCode = p_iCovTypeCode;
            XmlElement objRootNode = null;
            XmlElement objTempNode = null;
            LocalCache objLocalCache = null;
            Claim objClaim = null;
            iReserveTypeCode = p_iReserveTypeCode;
            //skhare7 R8 supervisory approval
            int sReserveStatusCode = 0;
            string sSQL = string.Empty;
            string sReserveShortCode = string.Empty;
            string sReserveCodeDesc = string.Empty;
            //skhare7 R8 supervisory approval end
            bool bCarrierClaims = false;
            int iClaimCurrTypeCode = 0;//skhare7 MITS 29039
            int iPolicySystemId = Int32.MinValue;
            int iTableId = Int32.MinValue;
            int iIncludeClmType = Int32.MinValue;
            int iPolSysCd = Int32.MinValue;
            int iClaimTypeCd = Int32.MinValue;
            ColLobSettings objColLobSettings = null;
            //rsharma220 MITS 37198
            int iReserveParentCd = Int32.MinValue;
            string sReserveParentShortCd = string.Empty, sReserveParentDesc = string.Empty;
            //start - Added by Nikhil.
            bool bOverrideDedFlagVisible = false;
            //End - Added by Nikhil.
            string sSelectedDedTypeCode = string.Empty;
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                bCarrierClaims = Conversion.ConvertStrToBool(this.m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());//Deb
                if (p_iTransTypeCode != 0)
                {
                    if (p_iTransID != 0)
                    {
                        if (m_objDataModelFactory == null)
                            this.Initialize();

                        m_objFunds.MoveTo(p_iTransID);
                        iLOB = m_objFunds.LineOfBusCode;
                        if (p_iClaimantId == 0)
                            p_iClaimantId = m_objFunds.ClaimantEid;
                        if (p_iUnitId == 0)
                            p_iUnitId = m_objFunds.UnitId;
                    }
                    else
                    {
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        if (p_iClaimId != 0)
                        {
                            objClaim.MoveTo(p_iClaimId);
                            iLOB = objClaim.LineOfBusCode;
                            m_iStatusCode = objClaim.ClaimStatusCode;
                            iClaimTypeCd = objClaim.ClaimTypeCode;
                        }
                    }
                    //aaggarwal29: get reserve code for external policy start
                    // if external policy, and include claim_type flag is on, get reserve type code directly based on transaction code selected
                    if (bCarrierClaims)
                    {
                        sSQL = "SELECT POLICY.POLICY_SYSTEM_ID, POLICY_SYSTEM_CODE FROM POLICY, POLICY_X_WEB WHERE POLICY_ID = " + p_iPolicyId.ToString() + " AND POLICY_X_WEB.POLICY_SYSTEM_ID = POLICY.POLICY_SYSTEM_ID ";
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iPolicySystemId = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_SYSTEM_ID"), m_iClientId);
                                iPolSysCd = Conversion.ConvertObjToInt(objReader.GetValue("POLICY_SYSTEM_CODE"), m_iClientId);
                            }
                        }

                        iTableId = objLocalCache.GetTableId("RESERVE_TYPE");
                        sSQL = "SELECT INCLUDE_CLAIM_TYPE FROM PS_MAP_TABLES WHERE RMX_TABLE_ID = " + iTableId + "AND POLICY_SYSTEM_TYPE_ID =" + iPolSysCd;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                iIncludeClmType = Conversion.ConvertObjToInt(objReader.GetValue("INCLUDE_CLAIM_TYPE"), m_iClientId);
                            }
                        }

                        if (iPolicySystemId > 0 && iIncludeClmType == 1 && objColLobSettings[iLOB].ResByClmType == false)
                        {
                            iReserveTypeCode = objLocalCache.GetRelatedCodeId(p_iTransTypeCode);
                            //verify if the mapping exists in table also..
                            sSQL = "SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID = " + iPolicySystemId + " AND RMX_TABLE_ID =" + iTableId + " AND CLAIM_TYPE_CODE = " + iClaimTypeCd + "AND RMX_CODE_ID = " + iReserveTypeCode;
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                            {
                                if (objReader.Read())
                                {
                                    iReserveTypeCode = Conversion.ConvertObjToInt(objReader.GetValue("RMX_CODE_ID"), m_iClientId);
                                }
                                else
                                {
                                    iReserveTypeCode = 0;
                                }
                            }

                            // 
                        }
                        // mbahl3 mits 34622
                        else
                        {
                            iReserveTypeCode = GetReserveTypeCode(p_iTransTypeCode, iLOB, p_iClaimId);
                        }
                        // mbahl3 mits 34622
                    }
                    else
                    {
                        iReserveTypeCode = GetReserveTypeCode(p_iTransTypeCode, iLOB, p_iClaimId);
                    }
                    //aaggarwal29: get reserve code for external policy end
                }
                if (iReserveTypeCode != 0)
                {

                    //skhare7 R8 supervisory approval
                    //Deb
                    if (bCarrierClaims)
                    {
                        //rupal:policy system interface
                        //sReserveStatusCode = GetReserveStatusDescForCarrierClaims(p_iClaimId, iReserveTypeCode, p_iClaimantId, p_iUnitId, p_iPolicyId, p_iCovTypeCode, p_iPolicyUnitRowId);
                        sReserveStatusCode = GetReserveStatusDescForCarrierClaims(p_iClaimId, iReserveTypeCode, p_iClaimantId, p_iUnitId, p_iPolicyId, p_iCovTypeCode, p_iPolicyUnitRowId, p_sCovgSeqNum, p_sTransSeqNum, p_sCoveragekey, iLossCode);       //Ankit Start : Worked on MITS - 34297 //asharma326 add iLossCode JIRA 305
                    }
                    else
                        sReserveStatusCode = GetReserveStatusDesc(p_iClaimId, iReserveTypeCode, p_iClaimantId, p_iUnitId);
                    //Deb
                    if (sReserveStatusCode != 0)
                    {

                        objLocalCache.GetCodeInfo(sReserveStatusCode, ref sReserveShortCode, ref sReserveCodeDesc, this.LanguageCode); //Aman MITS 31744
                        iReserveParentCd = objLocalCache.GetRelatedCodeId(sReserveStatusCode); //rsharma220 MITS 37198
                        objLocalCache.GetCodeInfo(iReserveParentCd, ref sReserveParentShortCd, ref sReserveParentDesc, this.LanguageCode); //rsharma220 MITS 37198 
                    }
                    //skhare7 R8 supervisory approval end
                    //added by rkaur7 - MITS 17241
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    if (p_iClaimId != 0)
                    {
                        objClaim.MoveTo(p_iClaimId);
                        iLOB = objClaim.LineOfBusCode;
                        m_iStatusCode = objClaim.ClaimStatusCode;
                        //Ankit Start : MITS 31425 
                        iClaimCurrCode = objClaim.CurrencyType;
                        //Ankit End
                    }
                    //end by rkaur7 skhare7 Defect removed
                    //objLocalCache = new LocalCache(m_sConnectionString);
                    objLocalCache.GetCodeInfo(iReserveTypeCode, ref sShortCode, ref sCodeDesc, this.LanguageCode); //Aman MITS 31744
                    //Raman Bhatia 12/19/2005..Reserve Balance calculation should use ClaimantEid and UnitId if detail level tracking is set to claimant level
                    //or in case of a WC or DI claim
                    if (this.ReserveTracking(iLOB) != 0 || iLOB == 844)
                    {
                        //rupal:start, BOB Change & first & final payment
                        if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                        {
                            dReserveBalance = GetReserveBalance(p_iClaimId, p_iClaimantId, p_iUnitId, iReserveTypeCode, iLOB, p_iTransID);
                        }
                        else
                        {
                            //rupal:policy system interface
                            dReserveBalance = GetReserveBalance(p_iClaimId, p_iClaimantId, p_iUnitId, iReserveTypeCode, iLOB, p_iTransID, p_iPolicyId, p_iCovTypeCode, p_iPolicyUnitRowId, p_sCovgSeqNum, iLossCode, p_sTransSeqNum, p_sCoveragekey);       //Ankit Start : Worked on MITS - 34297
                        }
                        //rupal:end, BOB change & first & final payment

                    }
                    else if (iLOB == 243)
                    {
                        if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                            dReserveBalance = GetReserveBalance(p_iClaimId, p_iClaimantId, p_iUnitId, iReserveTypeCode, iLOB, p_iTransID);
                        else
                        {
                            //rupal:policy system interface
                            dReserveBalance = GetReserveBalance(p_iClaimId, p_iClaimantId, p_iUnitId, iReserveTypeCode, iLOB, p_iTransID, p_iPolicyId, p_iCovTypeCode, p_iPolicyUnitRowId, p_sCovgSeqNum, iLossCode, p_sTransSeqNum, p_sCoveragekey);     //Ankit Start : Worked on MITS - 34297
                        }
                    }
                    else
                    {
                        if (objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                            dReserveBalance = GetReserveBalance(p_iClaimId, 0, 0, iReserveTypeCode, iLOB, p_iTransID);
                        else
                        {
                            //rupal:policy system interface
                            dReserveBalance = GetReserveBalance(p_iClaimId, p_iClaimantId, p_iUnitId, iReserveTypeCode, iLOB, p_iTransID, p_iPolicyId, p_iCovTypeCode, p_iPolicyUnitRowId, p_sCovgSeqNum, iLossCode, p_sTransSeqNum, p_sCoveragekey);       //Ankit Start : Worked on MITS - 34297
                        }
                    }
                }

                objRootNode = null;
                this.StartDocument(ref objRootNode, "ReservesInformation");
                //Deb Multi Currency
                if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (iClaimCurrCode > 0)
                    {
                        this.CreateAndSetElement(objRootNode, "ClaimCurrReserveBal", CommonFunctions.ConvertByCurrencyCode(iClaimCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                    }
                    else
                    {
                        this.CreateAndSetElement(objRootNode, "ClaimCurrReserveBal", CommonFunctions.ConvertByCurrencyCode(p_iPmtCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                    }
                    if (iClaimCurrCode > 0 && iClaimCurrCode != p_iPmtCurrCode)
                    {
                        //Convert Claim Currency Amount to Base Currency Amount then to Pmt Currency Amount
                        //To Maintain consistency in data conversion
                        double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, this.m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                        double dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(this.m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, p_iPmtCurrCode, m_sConnectionString, m_iClientId);
                        dReserveBalance = dReserveBalance * dExhRateClaimToBase;
                        dReserveBalance = dReserveBalance * dExhRateBaseToPmt;
                    }
                }
                else
                {
                    //For the timing being commented,may be used in future
                    //this.CreateAndSetElement(objRootNode, "ClaimCurrReserveBal", CommonFunctions.ConvertByCurrencyCode(this.m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, dReserveBalance, m_sConnectionString));
                    p_iPmtCurrCode = this.m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType;
                }
                //Deb Multi Currency

                this.CreateAndSetElement(objRootNode, "ReserveType", sShortCode + ' ' + sCodeDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ReserveType");
                objTempNode.SetAttribute("codeid", iReserveTypeCode.ToString());
                //skhare7 R8 supervisory approval
                this.CreateAndSetElement(objRootNode, "ReserveTypeStatus", sReserveShortCode + ' ' + sReserveCodeDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ReserveTypeStatus");
                objTempNode.SetAttribute("shortcode", sReserveShortCode.ToString());
                objTempNode.SetAttribute("codedesc", sReserveCodeDesc.ToString());
                //skhare7 R8 supervisory approval End
                //rsharma220 MITS 37198
                this.CreateAndSetElement(objRootNode, "ReserveParentCode", sReserveParentShortCd + ' ' + sReserveParentDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ReserveParentCode");
                objTempNode.SetAttribute("shortcode", sReserveParentShortCd.ToString());
                objTempNode.SetAttribute("codedesc", sReserveParentDesc.ToString());

                if (dReserveBalance.ToString().IndexOf(".") == -1)
                {
                    //this.CreateAndSetElement(objRootNode, "ReserveBalance", ("$" + dReserveBalance.ToString() + ".00"));
                    this.CreateAndSetElement(objRootNode, "ReserveBalance", CommonFunctions.ConvertByCurrencyCode(p_iPmtCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                }
                else
                {
                    //this.CreateAndSetElement(objRootNode, "ReserveBalance", ("$" + dReserveBalance.ToString()));
                    this.CreateAndSetElement(objRootNode, "ReserveBalance", CommonFunctions.ConvertByCurrencyCode(p_iPmtCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                }
                //rupal:start, r8 unit implementation
                //Deb Not required here
                //objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);//Deb Not required here
                //bCarrierClaims = Conversion.ConvertStrToBool(objClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());
                //Deb Not required here
                if (bCarrierClaims)
                {
                    this.CreateAndSetElement(objRootNode, "CarrierClaims", bCarrierClaims.ToString());
                }
                //Added by Nikhil for NI.

                bool bIsEligibleForDedProcessing = false;
                bIsEligibleForDedProcessing = IsEligibleForDeductibleProcessing(iReserveTypeCode, p_iClaimId, p_iCovTypeCode, Convert.ToInt64(p_iPolicyUnitRowId), p_sCovgSeqNum, p_sTransSeqNum, p_sCoveragekey, out sSelectedDedTypeCode);
                this.CreateAndSetElement(objRootNode, "IsEligibleForDedProcessing", bIsEligibleForDedProcessing.ToString());
                //END - Added by Nikhil for NI

                //start - Added by Nikhil
                bool bIsDedProcAllowed = false;//ddhiman 10/14/2014, SMS Settings for deductible Processing

                if (iLOB > 0)
                {
                    bIsDedProcAllowed = objClaim.Context.RMUser.IsAllowedEx(DED_PROC_SHOW);//ddhiman 10/14/2014, SMS Settings for deductible Procesing
                    if (m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1 && m_objDataModelFactory.Context.InternalSettings.SysSettings.UsePolicyInterface && bIsDedProcAllowed)//ddhiman 10/14/2014, SMS Settings for deductible Procesing
                    {
                        bOverrideDedFlagVisible = true;
                    }
                    else
                    {
                        bOverrideDedFlagVisible = false;
                    }
                }

                //end - Added by Nikhil
                this.CreateAndSetElement(objRootNode, "OverrideDedFlagVisible", bOverrideDedFlagVisible ? "-1" : "0");
                //rupal:end
                this.CreateAndSetElement(objRootNode, "NotDetDedCodeID", objLocalCache.GetCodeId("ND", "DEDUCTIBLE_TYPE").ToString());
                this.CreateAndSetElement(objRootNode, "SelectedDedVal", sSelectedDedTypeCode);
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPaymentCollection.ErrorPaymentCollection", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
                objTempNode = null;
                if (objClaim != null)
                    objClaim.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (objColLobSettings != null)
                    objColLobSettings = null;
            }
            return (m_objDocument);
        }

        public XmlDocument GetManualDeductibleReservesInfo(int p_iTransTypeCode, int p_iReserveTypeCode, int p_iClaimId, int p_iClaimantId, int p_iUnitId, int p_iTransID, int p_iPolicyId, int p_iCovTypeCode, int p_iPolicyUnitRowId, int p_iPmtCurrCode, string p_sCovgSeqNum, int iLossCode, string p_sTransSeqNum, string p_sCoveragekey)      //Ankit Start : Worked on MITS - 34297
        {
            int iReserveTypeCode = 0;
            double dReserveBalance = 0;
            string sShortCode = "";
            string sCodeDesc = "";
            int iLOB = 0;
            int iPolicyId = p_iPolicyId;
            int iCovTypeCode = p_iCovTypeCode;
            XmlElement objRootNode = null;
            XmlElement objTempNode = null;
            LocalCache objLocalCache = null;
            Claim objClaim = null;
            iReserveTypeCode = p_iReserveTypeCode;
            //skhare7 R8 supervisory approval
            int sReserveStatusCode = 0;
            string sSQL = string.Empty;
            string sReserveShortCode = string.Empty;
            string sReserveCodeDesc = string.Empty;
            //skhare7 R8 supervisory approval end
            bool bCarrierClaims = false;
            int iClaimCurrTypeCode = 0;//skhare7 MITS 29039
            int iPolicySystemId = Int32.MinValue;
            int iTableId = Int32.MinValue;
            int iIncludeClmType = Int32.MinValue;
            int iPolSysCd = Int32.MinValue;
            int iClaimTypeCd = Int32.MinValue;
            int iDedTransCodeId = Int32.MinValue;
            ColLobSettings objColLobSettings = null;
            //rsharma220 MITS 37198
            int iReserveParentCd = Int32.MinValue;
            string sReserveParentShortCd = string.Empty, sReserveParentDesc = string.Empty;
            //start - Added by Nikhil.
            bool bOverrideDedFlagVisible = false;
            //End - Added by Nikhil.
            string sSelectedDedTypeCode = string.Empty;
            StringBuilder sbSQL;
            string sDedTranCodeDesc = "", p_sPolCvgId = "", sDedTranShortCode = "";
            int i_DedTypeCode = 0, i_PolicyId = 0, i_ClaimantEid = 0, i_CovGroupId = 0;
            bool b_ExcludeExpFlag;
            double d_SirDedAmt = 0;
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                bCarrierClaims = Conversion.ConvertStrToBool(this.m_objDataModelFactory.Context.InternalSettings.SysSettings.MultiCovgPerClm.ToString());//Deb
                if (p_iTransTypeCode != 0)
                {
                    if (p_iTransID != 0)
                    {
                        if (m_objDataModelFactory == null)
                            this.Initialize();

                        m_objFunds.MoveTo(p_iTransID);
                        iLOB = m_objFunds.LineOfBusCode;
                        if (p_iClaimantId == 0)
                            p_iClaimantId = m_objFunds.ClaimantEid;
                        if (p_iUnitId == 0)
                            p_iUnitId = m_objFunds.UnitId;
                    }
                    else
                    {

                    }



                }

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimId != 0)
                {
                    objClaim.MoveTo(p_iClaimId);
                    iLOB = objClaim.LineOfBusCode;
                    m_iStatusCode = objClaim.ClaimStatusCode;
                    iClaimTypeCd = objClaim.ClaimTypeCode;
                }
                // Write a query based on new mapping table.
                // This query will get the Deductible reserve and transaction based on the input reserve type and claim LOB- GC or WC
                // SELECT DED_RES_CODE, DED_TRANS_ID FROM LOB_MAPPING WHERE LOB= p_sClaimLOB AND PAYMENT_RESERVE = p_sResTypeCode
                // sDedResCodeId = DED_RES_CODE, sDedTransCodeId= DED_TRANS_ID

                // SELECT RC_ROW_ID FROM RESERVE_CURRENT WHERE RESERVE_TYPE_CODE= iDedResCodeId AND CLAIM_ID = p_sClaimID AND CLAIMANT_EID=  AND POLCVG_LOSS_ROW_ID = AND POLCVG_ROW_ID = p_sPolCvgId
                // iDedRcRowId=RC_ROW_ID
                //sDedResCodeDesc = objLocalCache.GetCodeDesc(iDedResCodeId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                if (objColLobSettings[iLOB] != null)
                {
                    iReserveTypeCode = objColLobSettings[iLOB].DedRecReserveType;
                    iDedTransCodeId = objColLobSettings[iLOB].DedRecTransType;
                }
                sDedTranCodeDesc = objLocalCache.GetCodeDesc(iDedTransCodeId);
                objLocalCache.GetCodeInfo(iDedTransCodeId, ref sDedTranShortCode, ref sDedTranCodeDesc, this.LanguageCode);
                sbSQL = new StringBuilder();
                if (iReserveTypeCode != 0)
                {

                    sReserveStatusCode = GetReserveStatusDescForCarrierClaims(p_iClaimId, iReserveTypeCode, p_iClaimantId, p_iUnitId, p_iPolicyId, p_iCovTypeCode, p_iPolicyUnitRowId, p_sCovgSeqNum, p_sTransSeqNum, p_sCoveragekey, iLossCode);       //Ankit Start : Worked on MITS - 34297 //asharma326 add iLossCode JIRA 305
                    if (sReserveStatusCode != 0)
                    {

                        objLocalCache.GetCodeInfo(sReserveStatusCode, ref sReserveShortCode, ref sReserveCodeDesc, this.LanguageCode); //Aman MITS 31744
                        iReserveParentCd = objLocalCache.GetRelatedCodeId(sReserveStatusCode); //rsharma220 MITS 37198
                        objLocalCache.GetCodeInfo(iReserveParentCd, ref sReserveParentShortCd, ref sReserveParentDesc, this.LanguageCode); //rsharma220 MITS 37198 
                    }

                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    if (p_iClaimId != 0)
                    {
                        objClaim.MoveTo(p_iClaimId);
                        iLOB = objClaim.LineOfBusCode;
                        m_iStatusCode = objClaim.ClaimStatusCode;
                        //Ankit Start : MITS 31425 
                        iClaimCurrCode = objClaim.CurrencyType;
                        //Ankit End
                    }

                    objLocalCache.GetCodeInfo(iReserveTypeCode, ref sShortCode, ref sCodeDesc, this.LanguageCode); //Aman MITS 31744
                    this.m_objFunds.ClaimId = p_iClaimId;

                    object iPolCvgRowId = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_TYPE WHERE POLICY_UNIT_ROW_ID =" + p_iPolicyUnitRowId + " AND COVERAGE_TYPE_CODE=" + iCovTypeCode);


                    dReserveBalance = this.GetDeductibleInfo(Conversion.ConvertObjToInt(iPolCvgRowId, m_iClientId), p_iClaimId, out d_SirDedAmt, out i_DedTypeCode, out b_ExcludeExpFlag, out i_PolicyId, out i_ClaimantEid, out i_CovGroupId, false, false);
                }

                objRootNode = null;
                this.StartDocument(ref objRootNode, "ReservesInformation");

                if (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.UseMultiCurrency != 0)
                {
                    if (iClaimCurrCode > 0)
                    {
                        this.CreateAndSetElement(objRootNode, "ClaimCurrReserveBal", CommonFunctions.ConvertByCurrencyCode(iClaimCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                    }
                    else
                    {
                        this.CreateAndSetElement(objRootNode, "ClaimCurrReserveBal", CommonFunctions.ConvertByCurrencyCode(p_iPmtCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                    }
                    if (iClaimCurrCode > 0 && iClaimCurrCode != p_iPmtCurrCode)
                    {
                        double dExhRateClaimToBase = CommonFunctions.ExchangeRateSrc2Dest(iClaimCurrCode, this.m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, m_sConnectionString, m_iClientId);
                        double dExhRateBaseToPmt = CommonFunctions.ExchangeRateSrc2Dest(this.m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType, p_iPmtCurrCode, m_sConnectionString, m_iClientId);
                        dReserveBalance = dReserveBalance * dExhRateClaimToBase;
                        dReserveBalance = dReserveBalance * dExhRateBaseToPmt;
                    }
                }
                else
                {

                    p_iPmtCurrCode = this.m_objDataModelFactory.Context.InternalSettings.SysSettings.BaseCurrencyType;
                }


                this.CreateAndSetElement(objRootNode, "ReserveType", sShortCode + ' ' + sCodeDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ReserveType");
                objTempNode.SetAttribute("codeid", iReserveTypeCode.ToString());

                this.CreateAndSetElement(objRootNode, "DedTranType", sDedTranShortCode + " " + sDedTranCodeDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//DedTranType");
                objTempNode.SetAttribute("codeid", iDedTransCodeId.ToString());

                this.CreateAndSetElement(objRootNode, "ReserveTypeStatus", sReserveShortCode + ' ' + sReserveCodeDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ReserveTypeStatus");
                objTempNode.SetAttribute("shortcode", sReserveShortCode.ToString());
                objTempNode.SetAttribute("codedesc", sReserveCodeDesc.ToString());

                this.CreateAndSetElement(objRootNode, "ReserveParentCode", sReserveParentShortCd + ' ' + sReserveParentDesc);
                objTempNode = (XmlElement)objRootNode.SelectSingleNode("//ReserveParentCode");
                objTempNode.SetAttribute("shortcode", sReserveParentShortCd.ToString());
                objTempNode.SetAttribute("codedesc", sReserveParentDesc.ToString());

                if (dReserveBalance.ToString().IndexOf(".") == -1)
                {
                    //this.CreateAndSetElement(objRootNode, "ReserveBalance", ("$" + dReserveBalance.ToString() + ".00"));
                    this.CreateAndSetElement(objRootNode, "ReserveBalance", CommonFunctions.ConvertByCurrencyCode(p_iPmtCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                }
                else
                {
                    //this.CreateAndSetElement(objRootNode, "ReserveBalance", ("$" + dReserveBalance.ToString()));
                    this.CreateAndSetElement(objRootNode, "ReserveBalance", CommonFunctions.ConvertByCurrencyCode(p_iPmtCurrCode, dReserveBalance, m_sConnectionString, m_iClientId));
                }

                if (bCarrierClaims)
                {
                    this.CreateAndSetElement(objRootNode, "CarrierClaims", bCarrierClaims.ToString());
                }


                bool bIsEligibleForDedProcessing = false;


                bIsEligibleForDedProcessing = IsEligibleForDeductibleProcessing(iReserveTypeCode, p_iClaimId, p_iCovTypeCode, Convert.ToInt64(p_iPolicyUnitRowId), p_sCovgSeqNum, p_sTransSeqNum, p_sCoveragekey, out sSelectedDedTypeCode);
                this.CreateAndSetElement(objRootNode, "IsEligibleForDedProcessing", bIsEligibleForDedProcessing.ToString());

                bool bIsDedProcAllowed = false;

                if (iLOB > 0)
                {
                    bIsDedProcAllowed = objClaim.Context.RMUser.IsAllowedEx(DED_PROC_SHOW);//ddhiman 10/14/2014, SMS Settings for deductible Procesing
                    if (m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1 && m_objDataModelFactory.Context.InternalSettings.SysSettings.UsePolicyInterface && bIsDedProcAllowed)//ddhiman 10/14/2014, SMS Settings for deductible Procesing
                    {
                        bOverrideDedFlagVisible = true;
                    }
                    else
                    {
                        bOverrideDedFlagVisible = false;
                    }
                }
                this.CreateAndSetElement(objRootNode, "OverrideDedFlagVisible", bOverrideDedFlagVisible ? "-1" : "0");
                this.CreateAndSetElement(objRootNode, "NotDetDedCodeID", objLocalCache.GetCodeId("ND", "DEDUCTIBLE_TYPE").ToString());
                this.CreateAndSetElement(objRootNode, "SelectedDedVal", sSelectedDedTypeCode);
                this.CreateAndSetElement(objRootNode, "DedRemainingAmt", dReserveBalance.ToString());
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPaymentCollection.ErrorPaymentCollection", m_iClientId), p_objEx);
            }
            finally
            {
                objRootNode = null;
                objTempNode = null;
                if (objClaim != null)
                    objClaim.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (objColLobSettings != null)
                    objColLobSettings = null;
            }
            return (m_objDocument);
        }
        #endregion

        /// <summary>
        /// Gets the output file name for Payment Information
        /// </summary>
        /// <returns>string containing the output filename</returns>
        internal static string GetPaymentFilePath()
        {
            string sFileName = string.Empty;
            string strDirectoryPath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "Payments");

            DirectoryInfo objReqFolder = new DirectoryInfo(strDirectoryPath);

            if (!objReqFolder.Exists)
                objReqFolder.Create();

            sFileName = String.Format("{0}\\{1}", strDirectoryPath, Path.GetFileName(Path.GetTempFileName()).Replace(".tmp", ".pdf"));

            return sFileName;
        } // method: GetPaymentFilePath


        /// <summary>
        /// GetTableId returns the TABLE_ID for a particular SYSTEM_TABLE_NAME in the GLOSSARY table.
        /// </summary>
        /// <param name="p_sTableName">SYSTEM_TABLE_NAME for which the TABLE_ID is required</param>
        /// <returns>Table Id</returns>
        private int GetTableId(string p_sTableName)
        {
            DbReader objReader = null;

            int iTableId = 0;
            string sSQL = "";

            try
            {
                if (p_sTableName == "")
                    return (0);

                sSQL = " SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + p_sTableName + "'";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                    if (objReader.Read())
                        iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("TABLE_ID"), m_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetTableId.ErrorTableID", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (iTableId);

        }
        /// <summary>
        /// Get Entity Glossary Type
        /// </summary>
        /// <param name="p_iEntityId">Entity Id</param>
        /// <returns>Glossary Type</returns>
        private int GetEntityGlossaryType(int p_iEntityId)
        {
            DbReader objReader = null;
            int iEntityGlossaryType = 0;
            string sSQL = "";

            try
            {
                if (p_iEntityId > 0)
                {
                    sSQL = " SELECT GLOSSARY.GLOSSARY_TYPE_CODE FROM ENTITY, GLOSSARY WHERE ENTITY.ENTITY_ID="
                        + p_iEntityId + " AND ENTITY.ENTITY_TABLE_ID=GLOSSARY.TABLE_ID ";

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            iEntityGlossaryType = Common.Conversion.ConvertObjToInt(objReader.GetValue("GLOSSARY_TYPE_CODE"), m_iClientId);
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetEntityGlossaryType.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (iEntityGlossaryType);
        }

        /// <summary>
        /// Get Owner Table Id
        /// </summary>
        /// <param name="p_iOwnerEID">Owner Id</param>
        /// <returns>Owner Table Id</returns>
        private int GetOwnerTableId(int p_iOwnerEID)
        {
            Entity objEntity = null;
            bool bEntityFound = false;
            int iTableId = 0;

            try
            {
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                try
                {
                    objEntity.MoveTo(p_iOwnerEID);
                    bEntityFound = true;
                }
                catch
                {
                    bEntityFound = false;
                }
                if (bEntityFound)
                    iTableId = objEntity.EntityTableId;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetOwnerTableId.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objEntity = null;
            }
            return (iTableId);
        }
        /// <summary>
        /// Get Reserve Tracking
        /// </summary>
        /// <returns>Reserve Tracking</returns>
        private int ReserveTracking()
        {
            ColLobSettings objColLobSettings = null;
            int iReserveTracking = 0;

            try
            {
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                if (m_objFunds.LineOfBusCode != 0)
                    iReserveTracking = objColLobSettings[m_objFunds.LineOfBusCode].ReserveTracking;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ReserveTracking.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objColLobSettings = null;
            }
            return (iReserveTracking);
        }
        /// <summary>
        /// Get Reserve Tracking
        /// </summary>
        /// <returns>Reserve Tracking</returns>
        private int ReserveTracking(int p_iLOB)
        {
            ColLobSettings objColLobSettings = null;
            int iReserveTracking = 0;

            try
            {
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                if (p_iLOB != 0)
                    iReserveTracking = objColLobSettings[p_iLOB].ReserveTracking;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ReserveTracking.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objColLobSettings = null;
            }
            return (iReserveTracking);
        }

        /// <summary>
        /// Initialize objects
        /// </summary>
        private void Initialize()
        {
            try
            {
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);//sonali
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
                //rsushilaggar MITS 22933   Date 11/09/2010
                m_securityConnectionString = RMConfigurationSettings.GetSecurityDSN(m_iClientId);//sonali

                m_objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
                m_objFunds.FiringScriptFlag = 2;
                //Rahul mits 10339 - 17 sept 2007
                m_objAccounts = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);
                m_arrlstConfirmations = new ArrayList();
                m_arrlstErrors = new ArrayList();
                m_arrlstWarnings = new ArrayList();
                m_arrlstDeletedSplitIds = new ArrayList();
                m_bIsTandE = m_objDataModelFactory.Context.InternalSettings.SysSettings.UseTAndE;
                //  this.CheckTandE();
                //mini perofrmance mits

                // Ash - cloud, config settings moved to DB
                m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId);

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.Initialize.ErrorInit", m_iClientId), p_objEx);	//sonali			
            }
        }
        /// <summary>
        /// Initialize From ClaimId
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_iClaimantEid">ClaimantEid</param>
        /// <param name="p_iUnitId">UnitId</param>
        /// <param name="p_bIsCollection">IsCollection</param>
        private void InitializeFromClaimId(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, bool p_bIsCollection)
        {
            Claim objClaim = null;

            try
            {
                m_objFunds.ClaimId = p_iClaimId;
                if (p_bIsCollection)
                    m_objFunds.CollectionFlag = true;
                else
                    m_objFunds.PaymentFlag = true;

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (p_iClaimId > 0)
                    objClaim.MoveTo(p_iClaimId);
                /*
                 * Vaibhav has commented the code, for saving the Payment without Claim.
                else
                    throw new RMAppException(Globalization.GetString("FundManager.InitializeFromClaimId.InvalidClaim") );
                */

                //Raman Bhatia 12/20/2005..ClaimantId should be set only in case of detail level tracking or WC or DI
                if (this.ReserveTracking(objClaim.LineOfBusCode) != 0 || m_objFunds.LineOfBusCode == 243 || m_objFunds.LineOfBusCode == 844)
                {
                    m_objFunds.ClaimantEid = p_iClaimantEid;
                    m_objFunds.UnitId = p_iUnitId;
                }
                m_objFunds.ClaimNumber = objClaim.ClaimNumber;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.InitializeFromClaimId.ErrorInit", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
            }

        }

        /// <summary>
        /// Initialize From ClaimId
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_iClaimantEid">ClaimantEid</param>
        /// <param name="p_iUnitId">UnitId</param>		
        private void InitializeFromClaimId(int p_iClaimId, int p_iClaimantEid, int p_iUnitId)
        {
            InitializeFromClaimId(p_iClaimId, p_iClaimantEid, p_iUnitId, false);
        }
        /// <summary>
        /// Check Time and Expense
        /// </summary>
        private void CheckTandE()
        {
            //DbReader objReader = null ; mini performance issue  
            string sSQL = "";

            try
            {
                sSQL = "SELECT USETANDE FROM SYS_PARMS";

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            m_bIsTandE = objReader.GetBoolean("USETANDE");
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckTandE.Error", m_iClientId), p_objEx);
            }
            //mini performance mits
            //finally
            //{
            //    if( objReader != null )
            //    {
            //        objReader.Close();
            //        objReader.Dispose();
            //    }
            //}
            //mini performance mits

        }

        /// <summary>
        /// IsReadOnly
        /// </summary>
        /// <returns>Read Only Flag</returns>
        private bool IsReadOnly()
        {
            LocalCache objLocalCache = null;
            bool bIsReadOnly = false;

            try
            {
                if (m_objFunds.TransId != 0)
                {
                    objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                    if (objLocalCache.GetShortCode(m_objFunds.StatusCode).ToUpper() == "P")
                    {
                        bIsReadOnly = true;
                    }
                    else
                    {
                        if (!this.IsClaimOpen() && (!m_bAllowPaymentsClosedClaims))
                        {
                            bIsReadOnly = true;
                        }
                        else
                        {
                            if (this.IsClaimFrozen())
                                bIsReadOnly = true;
                        }
                    }
                    //Parijat: Mits 10752
                    if (m_objFunds.PaymentFlag != true && m_objFunds.TransNumber > 0)
                    {
                        bIsReadOnly = true;
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.IsReadOnly.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return (bIsReadOnly);

        }

        /// <summary>
        /// Is Claim Open
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_iStatusCode">Status Code</param>
        /// <returns>Claim Open Flag</returns>
        public bool IsClaimOpen(int p_iClaimId, int p_iStatusCode)
        {
            Claim objClaim;
            LocalCache objLocalCache = null;

            bool bIsClaimOpen = false;
            int iStatusCode = 0;

            try
            {
                if (p_iClaimId == 0)
                    p_iClaimId = m_objFunds.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    if (p_iClaimId > 0)
                        objClaim.MoveTo(p_iClaimId);
                    else
                        throw new RMAppException(Globalization.GetString("FundManager.IsClaimOpen.InvalidClaim", m_iClientId));

                    iStatusCode = objClaim.ClaimStatusCode;
                    objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                    if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(iStatusCode)).ToUpper() == "O")
                        bIsClaimOpen = true;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.IsClaimOpen.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return (bIsClaimOpen);
        }

        /// <summary>
        /// Is LSS Payment
        /// </summary>
        public bool IsLSSPayment()
        {
            bool bIsLSSPayment = false;

            try
            {
                if (m_objFunds.AddedByUser.Equals("LSSINF"))
                    bIsLSSPayment = true;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateAndSetElement.Error", m_iClientId), p_objEx);
            }
            finally
            {
            }
            return (bIsLSSPayment);
        }

        /// <summary>
        /// Is Claim Open
        /// </summary>
        /// <returns>Claim Open Flag</returns>
        private bool IsClaimOpen()
        {
            return (IsClaimOpen(0, 0));
        }
        /// <summary>
        /// Is Claim Frozen
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>Claim Frozen Flag</returns>
        private bool IsClaimFrozen(int p_iClaimId)
        {
            Claim objClaim = null;
            bool bIsClaimFrozen = false;

            try
            {
                if (p_iClaimId == 0)
                    p_iClaimId = m_objFunds.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(p_iClaimId);

                    bIsClaimFrozen = objClaim.PaymntFrozenFlag;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.IsClaimFrozen.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
            }
            return (bIsClaimFrozen);
        }

        /// <summary>
        /// Is Claim Closed
        /// </summary>
        /// <returns>Claim Closed Flag</returns>
        private bool IsClaimClosed()
        {
            return (this.IsClaimClosed(0));
        }

        /// <summary>
        /// IsClaimClosed
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <returns>Claim Closed Flag</returns>
        private bool IsClaimClosed(int p_iClaimId)
        {
            Claim objClaim;
            LocalCache objLocalCache = null;

            bool bIsClaimClosed = false;
            int iStatusCode = 0;

            try
            {
                if (p_iClaimId == 0)
                    p_iClaimId = m_objFunds.ClaimId;

                if (p_iClaimId != 0)
                {
                    objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                    if (p_iClaimId > 0)
                        objClaim.MoveTo(p_iClaimId);
                    else
                        throw new RMAppException(Globalization.GetString("FundManager.IsClaimOpen.InvalidClaim", m_iClientId));

                    iStatusCode = objClaim.ClaimStatusCode;
                    objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                    if (objLocalCache.GetShortCode(objLocalCache.GetRelatedCodeId(iStatusCode)).ToUpper() == "C")
                        bIsClaimClosed = true;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.IsClaimClosed.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return (bIsClaimClosed);
        }

        /// <summary>
        /// Is Claim Frozen
        /// </summary>
        /// <returns>Claim Frozen Flag</returns>
        private bool IsClaimFrozen()
        {
            return (this.IsClaimFrozen(0));
        }

        /// <summary>
        /// Get Reserve Type Code
        /// </summary>
        /// <param name="p_iTransTypeCode">Trans Type Code</param>
        /// <param name="p_iLOB">Lob Code</param>
        /// <returns>Reserve Type Code</returns>
        private int GetReserveTypeCode(int p_iTransTypeCode, int p_iLOB, int p_iClaimId)
        {
            LocalCache objLocalCache = null;
            ColLobSettings objColLobSettings = null;

            LobSettings.CColReserves objColReserves = null;
            LobSettings.CReserve objReserve = null;

            int iRelatedCodeId = 0;
            int iReserveTypeCode = 0;
            //int iReserveType = 0 ;

            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);

                if (p_iTransTypeCode != 0)
                {
                    if (p_iLOB == 0)
                        p_iLOB = m_objFunds.LineOfBusCode;

                    iRelatedCodeId = objLocalCache.GetRelatedCodeId(p_iTransTypeCode);
                    try
                    {
                        objColReserves = objColLobSettings[p_iLOB].Reserves;

                        Claim objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        //raman..fix to fetch reservesinfo in case of absence of TransId
                        if (m_objFunds.ClaimId > 0)
                            objClaim.MoveTo(m_objFunds.ClaimId);
                        else if (p_iClaimId != 0)
                            objClaim.MoveTo(p_iClaimId);

                        //Anurag - Defect#1713 MITS#6383
                        //If the option for setting reserves per claim type is used, the transaction types are not 
                        //properly filtered when making a payment. The list of available trans types is not 
                        //matching with reserve types available for that claim. They should filter out
                        // on the basis of Claim Type
                        if (objColLobSettings[p_iLOB].ResByClmType == true)
                            objReserve = (LobSettings.CReserve)objColReserves[iRelatedCodeId.ToString() + objClaim.ClaimTypeCode.ToString()];
                        else
                            objReserve = (LobSettings.CReserve)objColReserves[iRelatedCodeId.ToString() + "0"];

                        if (objReserve == null)
                            iReserveTypeCode = 0;
                        else
                            iReserveTypeCode = objReserve.ReserveTypeCode;

                        objClaim.Dispose();
                        objClaim = null;
                    }
                    catch
                    {
                        iReserveTypeCode = 0;
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetReserveTypeCode.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                objColLobSettings = null;
                objColReserves = null;
                objReserve = null;
            }
            return (iReserveTypeCode);
        }

        /// <summary>
        /// Get Reserve Type Code
        /// </summary>
        /// <param name="p_iTransTypeCode">Trans Type Code</param>
        /// <returns>Reserve Type Code</returns>
        private int GetReserveTypeCode(int p_iTransTypeCode)
        {
            return (this.GetReserveTypeCode(p_iTransTypeCode, 0, 0));
        }
        /// <summary>
        /// Get Reserve Type Code
        /// </summary>
        /// <param name="p_iTransTypeCode">Trans Type Code</param>
        /// <returns>Reserve Type Code</returns>
        private int GetReserveTypeCode(int p_iTransTypeCode, int p_iLOB)
        {
            return (this.GetReserveTypeCode(p_iTransTypeCode, p_iLOB, 0));
        }
        /// <summary>
        /// Get Vehicle Description 
        /// </summary>
        /// <param name="p_iUnitId">Unit Id</param>
        /// <returns>Vehicle Description string</returns>
        private string GetVehicle(int p_iUnitId)
        {
            Vehicle objVehicle = null;
            string sVehicle = "";
            try
            {
                if (p_iUnitId != 0)
                {
                    objVehicle = (Vehicle)m_objDataModelFactory.GetDataModelObject("Vehicle", false);
                    objVehicle.MoveTo(p_iUnitId);
                    sVehicle = objVehicle.VehicleMake + " " + objVehicle.VehicleModel;
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetVehicle.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objVehicle = null;
            }
            return (sVehicle);
        }

        /// <summary>
        /// Get Last First Name
        /// </summary>
        /// <param name="p_iEntityId">Entity Id</param>
        /// <returns>Last First Name string </returns>
        private string GetLastFirstName(int p_iEntityId)
        {
            Entity objEntity = null;
            string sLastFirstName = "";
            bool bEntityFound = false;
            try
            {
                if (p_iEntityId != 0)
                {
                    objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                    try
                    {
                        objEntity.MoveTo(p_iEntityId);
                        bEntityFound = true;
                    }
                    catch
                    {
                        bEntityFound = false;
                    }
                    if (bEntityFound)
                        sLastFirstName = objEntity.GetLastFirstName();
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetLastFirstName.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objEntity = null;
            }
            return (sLastFirstName);
        }

        /// <summary>
        /// Un Initialize the data model factory object. 
        /// </summary>
        public void Dispose()
        {
            try
            {
                // m_objDataModelFactory.UnInitialize();
                m_objDataModelFactory.Dispose();
                m_objDataModelFactory = null;
                m_objFunds = null;
                m_arrlstConfirmations = null;
                m_arrlstErrors = null;
                m_arrlstWarnings = null;
                m_arrlstDeletedSplitIds = null;
            }
            catch
            {
                // Avoid raising any exception here.				
            }
        }

        /// <summary>
        /// Check is split item deleted.
        /// </summary>
        /// <param name="p_sSplitId">SplitId</param>
        private bool IsSplitItemDeleted(int p_iSplitId)
        {
            bool bIsSplitItemDeleted = false;
            try
            {
                bIsSplitItemDeleted = m_arrlstDeletedSplitIds.Contains(p_iSplitId);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.IsSplitItemDeleted.Deleted", m_iClientId), p_objEx);
            }
            return (bIsSplitItemDeleted);
        }

        /// <summary>
        /// Remove the deleted splits.
        /// </summary>
        private void RemoveDeletedSplitsItems()
        {
            try
            {
                foreach (int iSplitId in m_arrlstDeletedSplitIds)
                {
                    m_objFunds.TransSplitList.Delete(iSplitId);
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.RemoveDeletedSplitsItems.ErrorDelete", m_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Read/Write property for UserGroupId.
        /// </summary>
        public int UserGroupId
        {
            get
            {
                return (m_iUserGroupId);
            }
            set
            {
                m_iUserGroupId = value;
            }
        }

        /// <summary>
        /// Read/Write property for UserId.
        /// </summary>
        public int UserId
        {
            get
            {
                return (m_iUserId);
            }
            set
            {
                m_iUserId = value;
            }
        }

        /// <summary>
        /// Read/Write property for LoginName.
        /// </summary>
        public string LoginName
        {
            get
            {
                return (m_sUserName);
            }
            set
            {
                m_sUserName = value;
            }
        }

        /// <summary>
        /// Read property for ErrorCount.
        /// </summary>
        public int ErrorCount
        {
            get
            {
                if (m_arrlstErrors == null)
                    return (0);
                else
                    return (m_arrlstErrors.Count);
            }
        }
        /// <summary>
        /// Read property for ConfirmationsCount.
        /// </summary>
        public int ConfirmationsCount
        {
            get
            {
                if (m_arrlstConfirmations == null)
                    return (0);
                else
                    return (m_arrlstConfirmations.Count);
            }
        }
        /// <summary>
        /// Read property for WarningsCount.
        /// </summary>
        public int WarningsCount
        {
            get
            {
                if (m_arrlstWarnings == null)
                    return (0);
                else
                    return (m_arrlstWarnings.Count);
            }
        }

        /// <summary>
        /// Read property for InsufAmntXml.
        /// </summary>
        public string InsufAmntXml
        {
            get
            {
                return (m_sInsufAmntXml);
            }
        }
        /// <summary>
        /// Get The Error string 
        /// </summary>
        /// <param name="p_iIndex">Index</param>
        /// <returns>string</returns>
        public string Error(int p_iIndex)
        {
            string sError = "";
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();
                if (p_iIndex < 0 || p_iIndex > m_arrlstErrors.Count)
                    throw new IndexOutOfRangeException(Globalization.GetString("FundManager.Error.IndexOutofRange", m_iClientId));
                sError = (string)m_arrlstErrors[p_iIndex];
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.Error.ErrorMsg", m_iClientId), p_objEx);
            }
            return (sError);
        }

        /// <summary>
        /// Get The Confirmation string 
        /// </summary>
        /// <param name="p_iIndex">Index</param>
        /// <returns>string</returns>
        public string Confirmation(int p_iIndex)
        {
            string sConfirmation = "";
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();
                if (p_iIndex < 0 || p_iIndex > m_arrlstConfirmations.Count)
                    throw new IndexOutOfRangeException(Globalization.GetString("FundManager.Confirmation.IndexOutofRange", m_iClientId));
                sConfirmation = (string)m_arrlstConfirmations[p_iIndex];
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.Confirmation.ErrorMsg", m_iClientId), p_objEx);
            }
            return (sConfirmation);
        }

        /// <summary>
        /// Get The Warning string 
        /// </summary>
        /// <param name="p_iIndex">Index</param>
        /// <returns>string</returns>
        public string Warning(int p_iIndex)
        {
            string sWarning = "";
            try
            {
                if (m_objDataModelFactory == null)
                    this.Initialize();

                if (p_iIndex < 0 || p_iIndex > m_arrlstWarnings.Count)
                    throw new IndexOutOfRangeException(Globalization.GetString("FundManager.Warning.IndexOutofRange", m_iClientId));
                sWarning = (string)m_arrlstWarnings[p_iIndex];
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.Warning.ErrorMsg", m_iClientId), p_objEx);
            }
            return (sWarning);
        }

        /// <summary>
        /// IsBrsInstalled
        /// </summary>
        /// <returns>BrsInstalled Flag</returns>
        private bool IsBRSInstalled()
        {
            // akaushik5 Changed for MITS 37242 Starts
            //return(SysSettings.IsBRSInstalled());
            return (this.m_objDataModelFactory.Context.InternalSettings.SysSettings.IsBRSInstalled());
            // akaushik5 Changed for MITS 37242 Ends
        }


        /// <summary>
        /// Gets the e-mail id
        /// </summary>
        /// <param name="p_sConnectionString">Security Connection String</param>
        /// <param name="p_sLogInName">Log-in name of the user</param>
        /// <returns>E-mail id of the user</returns>
        private string GetEmailId(string p_sConnectionString, string p_sLogInName)
        {
            string sRetVal = "";
            string sSQL = "";
            DataSet objDS = null;

            try
            {
                sSQL = "SELECT USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME='" + p_sLogInName + "'";
                objDS = DbFactory.GetDataSet(p_sConnectionString, sSQL, m_iClientId);//rkaur27
                sRetVal = Conversion.ConvertObjToStr(objDS.Tables[0].Rows[0]["EMAIL_ADDR"]);

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetEmailId.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objDS != null) objDS.Dispose();
            }
            return (sRetVal);
        }
        //		private bool IsBRSInstalled()
        //		{			
        //			bool bIsBRSInstalled = false ;
        //			Type objType = null ;
        //			Assembly objAssembly = null ;
        //			try
        //			{
        //				// ***********************************************************//
        //				// TODO Following Line Need to be modified later on.		  //
        //				// ***********************************************************//
        //				objAssembly = Assembly.Load( @"C:\WINNT\system32\BRSLib.dll" );  
        //				foreach( Type objTemp in objAssembly.GetTypes() )
        //				{
        //					if( objTemp.Name == "CManager" )      // Need to change BRSLib.CManager
        //					{
        //						objType = objTemp ;
        //						break;
        //					}
        //				}
        //
        //				if( objType != null )
        //				{
        //					Object objTemp = Activator.CreateInstance( objType );
        //					if( objTemp != null )				
        //						bIsBRSInstalled = true ;
        //				}
        //			}
        //			catch
        //			{
        //				bIsBRSInstalled = false ;
        //			}
        //			return( bIsBRSInstalled );
        //		}

        /// <summary>
        /// Get codes List 
        /// </summary>
        /// <param name="p_sTableName">Table Name </param>
        /// <returns>Array List of CodesDetail structure </returns>
        private ArrayList GetCodes(string p_sTableName)
        {
            ArrayList arrlstCodes = null;
            DbReader objReader = null;
            CodeDetail structCodeDetail;

            int iTableId = 0;
            string sSQL = "";

            try
            {
                arrlstCodes = new ArrayList();
                iTableId = this.GetTableId(p_sTableName);
                sSQL = " SELECT CODES.CODE_ID, CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, "
                    + " CODES_TEXT WHERE CODES.CODE_ID=CODES_TEXT.CODE_ID AND " + LANGUAGE_CODE
                    + " AND CODES.TABLE_ID=" + iTableId;

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        structCodeDetail = new CodeDetail();
                        structCodeDetail.CodeId = Common.Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), m_iClientId);
                        structCodeDetail.ShortCode = objReader.GetString("SHORT_CODE");
                        structCodeDetail.CodeDesc = objReader.GetString("CODE_DESC");
                        arrlstCodes.Add(structCodeDetail);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetCodes.GetCodes", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (arrlstCodes);
        }
        /// <summary>
        /// Code Details structure 
        /// </summary>
        private struct CodeDetail
        {
            /// <summary>
            /// Private variable for CodeId.
            /// </summary>
            private int iCodeId;
            /// <summary>
            /// Private variable for ShortCode.
            /// </summary>
            private string sShortCode;
            /// <summary>
            /// Private variable for CodeDesc.
            /// </summary>
            private string sCodeDesc;
            /// <summary>
            /// Read/Write property for CodeId.
            /// </summary>
            internal int CodeId
            {
                get
                {
                    return (iCodeId);
                }
                set
                {
                    iCodeId = value;
                }
            }
            /// <summary>
            /// Read/Write property for ShortCode.
            /// </summary>
            internal string ShortCode
            {
                get
                {
                    return (sShortCode);
                }
                set
                {
                    sShortCode = value;
                }
            }
            /// <summary>
            /// Read/Write property for CodeDesc.
            /// </summary>
            internal string CodeDesc
            {
                get
                {
                    return (sCodeDesc);
                }
                set
                {
                    sCodeDesc = value;
                }
            }
        }

        /// <summary>
        /// Account Detail Structure 
        /// </summary>
        public struct AccountDetail
        {
            /// <summary>
            /// Private variable for Account Name.
            /// </summary>
            private string sAccountName;
            /// <summary>
            /// Private variable for SubRowId.
            /// </summary>
            private int iSubRowId;
            /// <summary>
            /// Private variable for AccountId.
            /// </summary>
            private int iAccountId;
            /// <summary>
            /// Read/Write property for AccountName.
            /// </summary>
            public string AccountName
            {
                get
                {
                    return (sAccountName);
                }
                set
                {
                    sAccountName = value;
                }
            }
            /// <summary>
            /// Read/Write property for SubRowId.
            /// </summary>
            public int SubRowId
            {
                get
                {
                    return (iSubRowId);
                }
                set
                {
                    iSubRowId = value;
                }
            }
            /// <summary>
            /// Read/Write property for AccountId.
            /// </summary>
            public int AccountId
            {
                get
                {
                    return (iAccountId);
                }
                set
                {
                    iAccountId = value;
                }
            }
            //Added by Amitosh for EFt Payments
            private bool bEftAcc;
            public bool IsEFtAccount
            {
                get
                {
                    return (bEftAcc);
                }
                set
                {
                    bEftAcc = value;
                }
            }//End Amitosh
        }

        /// <summary>
        /// PayeeType Detail Structure 
        /// </summary>
        private struct PayeeType
        {
            /// <summary>
            /// Private variable for CodeId.
            /// </summary>
            private int iCodeId;
            /// <summary>
            /// Private variable for ShortCode.
            /// </summary>
            private string sShortCode;
            /// <summary>
            /// Private variable for CodeDesc.
            /// </summary>
            private string sCodeDesc;
            /// <summary>
            /// Read/Write property for CodeId.
            /// </summary>
            internal int CodeId
            {
                get
                {
                    return (iCodeId);
                }
                set
                {
                    iCodeId = value;
                }
            }
            /// <summary>
            /// Read/Write property for ShortCode.
            /// </summary>
            internal string ShortCode
            {
                get
                {
                    return (sShortCode);
                }
                set
                {
                    sShortCode = value;
                }
            }
            /// <summary>
            /// Read/Write property for CodeDesc.
            /// </summary>
            internal string CodeDesc
            {
                get
                {
                    return (sCodeDesc);
                }
                set
                {
                    sCodeDesc = value;
                }
            }
        }
        /// <summary>
        /// Build the Trans Split XML for a Funds Trans Split Object.
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_objFundsTransSplit">Funds Trans Split Object</param>		
        private void BRSTransSplitXML(XmlElement p_objParentNode, FundsTransSplit p_objFundsTransSplit)
        {
            XmlDocument objBrsEmptySplit = null;
            Manager objMgr = null;
            BillItem objBillItem = null;
            int iTemp = 0;
            BRSRequestMapper objBrsMapper = null;
            XmlElement objFormNode = null;
            XmlDocument objEmptyBrsStandardInsDoc = null;
            XmlDocument objCurrentInsDoc = null;
            LocalCache objLocalCache = null;
            string sReserveTypeShortCode = "", sReserveTypeDesc = "";
            bool bContractExists = false;
            int iFeeSchedule = 0;
            int iFeeSchedule2 = 0;
            try
            {
                objMgr = new Manager(m_sDsnName, m_sUserName, m_sPassword, ref this.m_objDataModelFactory, m_iClientId);
                objBillItem = new BillItem(objMgr.Common, m_iClientId);
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                objBrsEmptySplit = new XmlDocument();
                objBrsEmptySplit.Load(RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, @"BRS\instance.xml"));
                objEmptyBrsStandardInsDoc = (XmlDocument)objBrsEmptySplit.Clone();
                iTemp++;

                objMgr.Common.SetValueOf("step", Conversion.ConvertObjToStr((int)BRS.Common.eSteps.Preload), ref objBrsEmptySplit);

                objBillItem.objSplit = p_objFundsTransSplit;

                objMgr.FillBRSDOM(ref objBrsEmptySplit, ref objBillItem);

                objMgr.Common.SetValueOf("idx", iTemp.ToString(), ref objBrsEmptySplit);
                objMgr.Common.SetValueOf("splitrowid", Conversion.ConvertObjToStr(p_objFundsTransSplit.SplitRowId), ref objBrsEmptySplit);
                objMgr.Common.SetValueOf("transid", Conversion.ConvertObjToStr(p_objFundsTransSplit.TransId), ref objBrsEmptySplit);

                objLocalCache.GetCodeInfo(p_objFundsTransSplit.ReserveTypeCode, ref sReserveTypeShortCode, ref sReserveTypeDesc);
                objMgr.Common.SetValueOf("reservetypecode", sReserveTypeShortCode + " " + sReserveTypeDesc, ref objBrsEmptySplit);


                Funds objFunds = (Funds)p_objFundsTransSplit.Parent;
                bContractExists = objMgr.FindContract(objFunds.PayeeEid, p_objFundsTransSplit.FromDate);
                if (bContractExists)
                {
                    if (objMgr.ProviderContracts != null)
                    {
                        iFeeSchedule = objMgr.ProviderContracts.FeeTableId;
                        iFeeSchedule2 = objMgr.ProviderContracts.FeeTableId2nd;
                    }//end if(m_objProviderContracts != null)
                }
                else
                {
                    //if editing an existing transsplit and no contract, grab the fee schedule from it
                    if (objFunds.TransId != 0)
                    {

                        iFeeSchedule = p_objFundsTransSplit.BrsInvoiceDetail.TableCode;


                    }
                }
                objMgr.Common.SetValueOf("feesched", iFeeSchedule.ToString(), ref objBrsEmptySplit);
                objMgr.Common.SetValueOf("feesched2", iFeeSchedule2.ToString(), ref objBrsEmptySplit);
                objMgr.Common.SetValueOf("contractexists", Conversion.ConvertBoolToInt(bContractExists, m_iClientId).ToString(), ref objBrsEmptySplit);
                objFormNode = (XmlElement)objBrsEmptySplit.SelectSingleNode("//form");
                objFormNode.SetAttribute("idx", iTemp.ToString());
                objCurrentInsDoc = this.NodeToDocument(objFormNode);
                objEmptyBrsStandardInsDoc = this.NodeToDocument((XmlElement)objEmptyBrsStandardInsDoc.SelectSingleNode("//form"));

                objBrsMapper = new BRSRequestMapper(ref objCurrentInsDoc, ref objEmptyBrsStandardInsDoc, m_iClientId);
                objBrsMapper.MapRequestInstanceToStandardBrsInstance();

                p_objParentNode.InnerXml = objEmptyBrsStandardInsDoc.OuterXml;
            }
            catch (Exception p_objErr)
            {
                throw p_objErr;
            }
            finally
            {
                objEmptyBrsStandardInsDoc = null;
                /*Alert!!
                 * Dont call dispose of BRS manager here as it will dispose off the Datamodel factory object as well.
                 * 
                 * */
                //objMgr=null;
                // objBillItem=null;
                objBrsMapper = null;
                objFormNode = null;
                objCurrentInsDoc = null;
                objBrsEmptySplit = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }
        /// <summary>
        /// Get Split Trans Types
        /// </summary>
        /// <returns>Array List of CodesDetail structure</returns>
        private ArrayList GetSplitTransTypes()
        {
            ArrayList arrlstCodes = null;
            DbReader objReader = null;
            Claim objClaim = null;
            CodeDetail structCodeDetail;

            int iTableId = 0;
            int iClaimType = 0;
            string sSQL = "";
            bool bResByClmType = false;
            bool bClaimExist = false;

            try
            {
                iTableId = this.GetTableId("TRANS_TYPES");
                arrlstCodes = new ArrayList();

                if (m_objFunds.LineOfBusCode != 0)
                {
                    sSQL = "SELECT RES_BY_CLM_TYPE FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE = " + m_objFunds.LineOfBusCode;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                        if (objReader.Read())
                            bResByClmType = objReader.GetBoolean("RES_BY_CLM_TYPE");

                    objReader.Close();

                    if (bResByClmType)
                    {
                        objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                        try
                        {
                            objClaim.MoveTo(m_objFunds.ClaimId);
                            bClaimExist = true;
                        }
                        catch
                        {
                            bClaimExist = false;
                        }
                        if (bClaimExist)
                            iClaimType = objClaim.ClaimTypeCode;
                    }
                    if (bResByClmType)
                    {
                        sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,TRIGGER_DATE_FIELD, "
                            + " EFF_START_DATE,EFF_END_DATE,ORG_GROUP_EID,LINE_OF_BUS_CODE FROM CODES, "
                            + " CODES_TEXT WHERE CODES.TABLE_ID = " + iTableId
                            + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID IN "
                            + " (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE = "
                            + m_objFunds.LineOfBusCode + " AND CLAIM_TYPE_CODE = " + iClaimType
                            + " ) AND CODES.DELETED_FLAG = 0";

                    }
                    else
                    {
                        sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,TRIGGER_DATE_FIELD,	"
                            + " EFF_START_DATE,EFF_END_DATE,ORG_GROUP_EID,LINE_OF_BUS_CODE FROM CODES, "
                            + " CODES_TEXT WHERE CODES.TABLE_ID = " + iTableId
                            + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID IN "
                            + " (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE = "
                            + m_objFunds.LineOfBusCode + ") AND CODES.DELETED_FLAG = 0 ORDER BY CODES.SHORT_CODE";

                    }
                }
                else
                {
                    sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,TRIGGER_DATE_FIELD, "
                        + " EFF_START_DATE,EFF_END_DATE,ORG_GROUP_EID,LINE_OF_BUS_CODE FROM CODES, "
                        + " CODES_TEXT WHERE CODES.TABLE_ID = " + iTableId
                        + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG = 0 "
                        + " ORDER BY CODES.SHORT_CODE";

                }

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        structCodeDetail = new CodeDetail();
                        structCodeDetail.CodeId = Common.Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), m_iClientId);
                        structCodeDetail.ShortCode = objReader.GetString("SHORT_CODE");
                        structCodeDetail.CodeDesc = objReader.GetString("CODE_DESC");
                        arrlstCodes.Add(structCodeDetail);
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetSplitTransTypes.GetSplitCodes", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (arrlstCodes);
        }

        #region Create Diary when Transaction goes on Hold
        private void CreateHoldDiary(string sEntryName, string sEntryNotes, string sAttachTable, long lAttachRecordID, string sRegarding, int iTransId)
        {
            string sSQL = string.Empty;
            long lAmount = 0;
            int iClaimId = 0;
            int iAdjID = 0;
            int iRecipiantID = 0;
            int iLOBClaim = 0;
            int iManagerID = 0;
            string sManagerLogin = string.Empty;
            StringBuilder sbSQL = null;
            bool bUseSpecificSupervisor = false;
            DbConnection objConn = null;
            DbReader objrd = null;
            try
            {
                sSQL = "SELECT AMOUNT, CLAIM_ID FROM FUNDS WHERE TRANS_ID = " + iTransId;
                objrd = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objrd != null)
                {
                    while (objrd.Read())
                    {
                        lAmount = Conversion.ConvertObjToInt64(objrd.GetValue("AMOUNT"), m_iClientId);
                        iClaimId = Conversion.ConvertObjToInt(objrd.GetValue("CLAIM_ID"), m_iClientId);
                    }
                }
                iLOBClaim = GetLOBForClaim(iClaimId);
                objrd = DbFactory.GetDbReader(m_sConnectionString, "SELECT USER_SPECIFIC_FLAG FROM CHECK_OPTIONS");
                if (objrd != null)
                {
                    while (objrd.Read())
                    {
                        bUseSpecificSupervisor = Conversion.ConvertObjToBool(objrd.GetValue("USER_SPECIFIC_FLAG"), m_iClientId);
                    }
                }
                if (bUseSpecificSupervisor)
                {
                    iAdjID = GetAdjusterEID(iClaimId);
                    if (iAdjID > 0)
                        iRecipiantID = GetApprovalID(iAdjID, lAmount, iLOBClaim);
                    else
                        iRecipiantID = GetApprovalID(m_iManagerID, lAmount, iLOBClaim);
                }
                else
                {
                    //Mjain8 11/27/06 MITS 8449
                    iRecipiantID = GetManagerID(m_iUserId);
                }
                sSQL = "SELECT USER_ID,LOGIN_NAME FROM USER_DETAILS_TABLE WHERE DSNID=" + m_iDSNID;
                sSQL = sSQL + " AND USER_ID = " + iRecipiantID;
                objrd = DbFactory.GetDbReader(m_securityConnectionString, sSQL);
                if (objrd != null)
                {
                    while (objrd.Read())
                    {
                        sManagerLogin = objrd.GetString("LOGIN_NAME");
                        iManagerID = Conversion.ConvertObjToInt(objrd.GetValue("USER_ID"), m_iClientId);
                    }
                }
                else
                    iManagerID = 0;
                if (iManagerID == 0)
                    return;
                //Indu - Adding att_parent_code for Diary enhancement - Mits 33843
                string scSQL = string.Empty;
                DbReader objRead = null;
                int attachedParent = 0;
                scSQL = "SELECT CLAIM_ID FROM FUNDS WHERE TRANS_ID =" + lAttachRecordID;
                objRead = DbFactory.GetDbReader(m_sConnectionString, scSQL);
                if (objRead != null)
                {
                    while (objRead.Read())
                    {
                        attachedParent = Conversion.ConvertObjToInt(objRead.GetInt32("CLAIM_ID"), m_iClientId);
                    }
                    objRead.Close();
                    objRead.Dispose();
                }
                sbSQL = new StringBuilder();
                sbSQL.Append("INSERT INTO WPA_DIARY_ENTRY (ENTRY_ID,ENTRY_NAME,ENTRY_NOTES,CREATE_DATE,PRIORITY,");
                sbSQL.Append(" STATUS_OPEN,AUTO_CONFIRM,ASSIGNED_USER, ASSIGNING_USER, ASSIGNED_GROUP,IS_ATTACHED,ATTACH_TABLE,");
                sbSQL.Append(" ATT_FORM_CODE,ATTACH_RECORDID,REGARDING,ATT_SEC_REC_ID,COMPLETE_TIME,COMPLETE_DATE,");
                sbSQL.Append(" DIARY_VOID, DIARY_DELETED,NOTIFY_FLAG,ROUTE_FLAG,ESTIMATE_TIME,AUTO_ID,ATT_PARENT_CODE) ");
                sbSQL.Append(" VALUES(" + Utilities.GetNextUID(m_sConnectionString, "WPA_DIARY_ENTRY", m_iClientId) + ",'" + sEntryName + "','");
                sbSQL.Append(sEntryNotes + "','" + Conversion.ToDbDateTime(System.DateTime.Now) + "',1,1,0,'" + sManagerLogin + "','");
                sbSQL.Append(m_sUserName + "','',1,'" + sAttachTable + "',3192,'" + lAttachRecordID + "','" + sRegarding);
                //Mjain changed due date to current date MITS 8539
                sbSQL.Append("',0,'235959','" + Conversion.ToDbDate(System.DateTime.Today) + "',0,0,0,0,0,0," + attachedParent + ")");
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sbSQL.ToString());
                objConn.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CreateHoldDiary.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn = null;
                }
            }
        }
        #endregion

        #region Function to Get Appover ID, Adjuster ID, LOB of Claim, Manager ID.
        private int GetLOBForClaim(int p_iClaimID)
        {
            int iLOBForClaim = 0;
            DbReader objRead = null;
            StringBuilder sbSQL = null;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID = " + p_iClaimID.ToString());
                objRead = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objRead.Read())
                {
                    iLOBForClaim = Conversion.ConvertObjToInt(objRead.GetValue(0), m_iClientId);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetLOBForClaim.Error", m_iClientId), p_objException);
            }
            finally
            {
                sbSQL = null;
            }
            return iLOBForClaim;
        }

        /// <summary>
        /// Get the Adjuster Id
        /// </summary>
        /// <param name="iClaimId">Claim Id</param>
        /// <returns>Adjuster Id</returns>
        private int GetAdjusterEID(int iClaimId)
        {
            string sSQL = string.Empty;
            int iCurrentAdjusterID = 0;
            int iRMUserID = 0;
            DbReader objReader = null;
            try
            {
                sSQL = "SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER WHERE CLAIM_ID = " + iClaimId + " AND CURRENT_ADJ_FLAG <> 0";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iCurrentAdjusterID = Conversion.ConvertObjToInt(objReader.GetValue("ADJUSTER_EID"), m_iClientId);
                    }
                }
                if (iCurrentAdjusterID > 0)
                {
                    sSQL = "SELECT RM_USER_ID FROM ENTITY WHERE ENTITY_ID = " + iCurrentAdjusterID;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            iRMUserID = Conversion.ConvertObjToInt(objReader.GetValue("RM_USER_ID"), m_iClientId);
                        }
                    }
                }
                iCurrentAdjusterID = iRMUserID;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetAdjusterEID.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return (iCurrentAdjusterID);
        }

        /// Name		: GetSupervisorApprovalID
        /// Author		: Saurav Mahajan
        /// Date Created: 11/12/2008
        /// <summary>
        /// Get Supervisor Approval ID
        /// </summary>
        /// <param name="iTransId"></param>
        /// <returns>Approval ID</returns>
        public int GetSupervisorApprovalID(int iTransId)
        {
            int iApproverID = 0;
            long lAmount = 0;
            int iLOBCode = 0;
            long lMaxAmount = 0;
            string sSQL = string.Empty;
            LobSettings objLobSettings = null;
            ColLobSettings objColLobSettings = null;
            Funds objFunds = null;
            int iGroupID = 0;
            ArrayList arrLimitExceed = null;//7810
            try
            {
                ReadCheckOptions();
                arrLimitExceed = new ArrayList();//7810
                objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                objFunds.MoveTo(iTransId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objLobSettings = objColLobSettings[objFunds.LineOfBusCode];
                lAmount = Conversion.ConvertObjToInt64(objFunds.Amount, m_iClientId);
                iLOBCode = objFunds.LineOfBusCode;
                m_objFunds = objFunds;//rsushilaggar MITS 26801 Date 05/22/2012
                arrLimitExceed = ValidateLimits(iTransId);//7810
                //rbhatia4 MITS 26333 : Oct 18 2011
                //Approval process needs to consider group id too

                //sSQL = "SELECT MAX_AMOUNT FROM FUNDS_LIMITS WHERE USER_ID = " + m_iUserId + " AND LINE_OF_BUS_CODE = " + iLOBCode;
                //rsushilaggar MITS 26801 Date 05/22/2012
               // sSQL = "SELECT MAX_AMOUNT FROM FUNDS_LIMITS WHERE ((USER_ID = " + m_iUserId + " AND GROUP_ID = 0) OR (GROUP_ID = " + m_iUserGroupId + " AND USER_ID = 0)) AND LINE_OF_BUS_CODE = " + iLOBCode;
                /*(using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("MAX_AMOUNT"), m_iClientId);
                        if (lMaxAmount >= lAmount)
                        {
                            if (objFunds.LineOfBusCode != 0)
                            {
                                if (objLobSettings.PayDetLmtFlag)
                                    //iApproverID = GetApprovalIDAgainstPayDetail(m_iUserId, lAmount, iLOBCode);
                                    iApproverID = GetApprovalIDAgainstExceedPay(m_iUserId, lAmount, iLOBCode);
                                else
                                    iApproverID = m_iUserId;
                            }
                        }
                    }
                    /*else
                    {
                        sSQL = "SELECT PAYMENT_MAX FROM LOB_SUP_APPROVAL WHERE USER_ID = " + m_iUserId + " AND ( LOB_CODE = -1 OR LOB_CODE = " + iLOBCode + ")";
                        using (DbReader objRead = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (!objRead.Read())
                            {
                                if (objFunds.LineOfBusCode != 0)
                                {
                                    if (objLobSettings.PayDetLmtFlag)
                                        //iApproverID = GetApprovalIDAgainstPayDetail(m_iUserId, lAmount, iLOBCode);
                                        iApproverID = GetApprovalIDAgainstExceedPay(m_iUserId, lAmount, iLOBCode);
                                    else
                                        iApproverID = m_iUserId;
                                }
                            }
                        }
                    }
                    objReader.Close();
                }*/
                //start 7810
                for (int j = 0; j < arrLimitExceed.Count; j++)
                {
                    if (arrLimitExceed[j].ToString() == "ExcPayAuth")
                    {
                        if (objFunds.LineOfBusCode != 0)
                        {
                            if (objLobSettings.PayLmtFlag)
                                //  iApproverID = GetApprovalIDAgainstPayDetail(m_iUserId, lAmount, iLOBCode);
                                iApproverID = GetApprovalIDAgainstExceedPay(m_iUserId, lAmount, iLOBCode);
                            else
                                iApproverID = m_iUserId;
                        }
                    }
                    else if (arrLimitExceed[j].ToString() == "ExcPayDetailAuth")
                    {
                        if (objFunds.LineOfBusCode != 0)
                        {
                            if (objLobSettings.PayDetLmtFlag)
                              //  iApproverID = GetApprovalIDAgainstPayDetail(m_iUserId, lAmount, iLOBCode);
                                iApproverID = GetApprovalIDAgainstExceedPay(m_iUserId, lAmount, iLOBCode);
                            else
                                iApproverID = m_iUserId;
                        }
                    }
                    else if (arrLimitExceed[j].ToString() == "ExcPayClmLmt")
                    {
                        if (objFunds.LineOfBusCode != 0)
                        {
                            if (objLobSettings.PerClaimLmtFlag)
                               // iApproverID = GetApprovalIDAgainstPayDetail(m_iUserId, lAmount, iLOBCode);
                                iApproverID = GetApprovalIDAgainstExceedPay(m_iUserId, lAmount, iLOBCode);
                            else
                                iApproverID = m_iUserId;
                        }
                    }
                }
                //End 7810
                sSQL = "SELECT PAYMENT_MAX FROM LOB_SUP_APPROVAL WHERE USER_ID = " + m_iUserId + " AND LOB_CODE = " + iLOBCode;
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("PAYMENT_MAX"), m_iClientId);
                        if (lMaxAmount >= lAmount)
                        {
                            if (objLobSettings.PayDetLmtFlag || objLobSettings.PayLmtFlag || objLobSettings.PerClaimLmtFlag)
                                //iApproverID = GetApprovalIDAgainstPayDetail(m_iUserId, lAmount, iLOBCode);
                                iApproverID = GetApprovalIDAgainstExceedPay(m_iUserId, lAmount, iLOBCode);
                            else
                                iApproverID = m_iUserId;
                        }
                    }
                }

                if (arrLimitExceed.Count != 0)
                {

                    sSQL = "SELECT PAYMENT_MAX FROM LOB_SUP_APPROVAL WHERE USER_ID = " + m_iUserId + " AND ( LOB_CODE = -1 OR LOB_CODE = " + iLOBCode + ")";
                    using (DbReader objRead = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (!objRead.Read())
                        {
                            if (objFunds.LineOfBusCode != 0)
                            {
                                if (objLobSettings.PayDetLmtFlag || objLobSettings.PayLmtFlag || objLobSettings.PerClaimLmtFlag)
                                    //iApproverID = GetApprovalIDAgainstPayDetail(m_iUserId, lAmount, iLOBCode);
                                    iApproverID = GetApprovalIDAgainstExceedPay(m_iUserId, lAmount, iLOBCode);
                                else
                                    iApproverID = m_iUserId;
                            }
                        }
                    }
                }
                else
                    iApproverID = m_iUserId;

                //Get Top level user
                sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1 AND USER_ID = " + m_iUserId;
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iApproverID = m_iUserId;
                    }
                }

                if (iApproverID != m_iUserId)
                {
                    iApproverID = GetApprovalID(m_iManagerID, lAmount, iLOBCode);

                }
                return iApproverID;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetSupervisorApprovalID.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objColLobSettings = null;
                objLobSettings = null;
                objFunds = null;
            }
        }

        /// <summary>
        /// Get the Approver Id
        /// </summary>
        /// <param name="iManagerID">Manager Id</param>
        /// <param name="lAmount">Amount</param>
        /// <param name="iLOBCode">lob code</param>
        /// <returns>Approver Id</returns>
        private int GetApprovalID(int iManagerID, double lAmount, int iLOBCode)
        {
            string sSQL = string.Empty;
            int iThisManagerID = 0;
            long lMaxAmount = 0;
            int iApproverID = 0;

            ColLobSettings objColLobSettings = null;	//Umesh
            LobSettings objLobSettings = null;         //Umesh
            try
            {
                //Umesh
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objLobSettings = objColLobSettings[m_objFunds.LineOfBusCode];
                iApproverID = iManagerID;
                iThisManagerID = iManagerID;

                //If there is no manager assigned
                if (iThisManagerID == 0)
                {
                    sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOBCode;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            //smahajan6: Safeway: Payment Supervisory Approval : Start
                            if (m_bPmtNotfyImmSup)
                            {
                                if (m_iUserId != Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId))
                                {
                                    iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                    return iApproverID;
                                }
                            }
                            else
                            {
                                lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("PAYMENT_MAX"), m_iClientId);
                                if ((lMaxAmount > lAmount))
                                {
                                    iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                    return iApproverID;
                                }
                            }

                            //Commeted Code - Payment Max should be checked
                            //lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("RESERVE_MAX"));
                            //if ((lMaxAmount > lAmount))
                            //{
                            //    iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                            //    return iApproverID;
                            //}

                            //smahajan6: Safeway: Payment Supervisory Approval : End
                        }
                    }

                    //Get Top level user
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                    {
                        if (objReader.Read())
                        {
                            iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                        }
                    }
                    return iApproverID;
                }

                if (!m_bPmtNotfyImmSup)//smahajan6: Safeway: Payment Supervisory Approval - Check Exit loop if for Notify Immediate Supervisor Chain
                {
                    sSQL = "SELECT * FROM FUNDS_LIMITS WHERE USER_ID = " + iThisManagerID + " AND LINE_OF_BUS_CODE = " + iLOBCode;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("MAX_AMOUNT"), m_iClientId);
                            if (lMaxAmount >= lAmount)
                            {  //MIS 11065    :Umesh
                                if (m_objFunds.LineOfBusCode != 0)
                                {
                                    if (objLobSettings.PayDetLmtFlag)
                                        iApproverID = GetApprovalIDAgainstPayDetail(iApproverID, lAmount, iLOBCode);
                                }


                                return iApproverID;
                            }
                        }
                        else
                        {
                            //MITS 11065    :Umesh
                            if (m_objFunds.LineOfBusCode != 0)
                            {
                                if (objLobSettings.PayDetLmtFlag)
                                    iApproverID = GetApprovalIDAgainstPayDetail(iApproverID, lAmount, iLOBCode);
                            }
                            return iApproverID;
                        }
                    }

                    while (lMaxAmount < lAmount)
                    {
                        iApproverID = iThisManagerID;
                        iThisManagerID = GetManagerID(iThisManagerID);

                        //If get the same manager id, just exit the endless loop
                        if (iApproverID == iThisManagerID)
                        {
                            //MITS 11065    :Umesh
                            if (m_objFunds.LineOfBusCode != 0)
                            {
                                if (objLobSettings.PayDetLmtFlag)
                                    iApproverID = GetApprovalIDAgainstPayDetail(iApproverID, lAmount, iLOBCode);
                            }
                            return iApproverID;
                        }

                        if (iThisManagerID == 0)
                        {
                            sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + iLOBCode;
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                            {
                                while (objReader.Read())
                                {
                                    //smahajan6: Safeway: Payment Supervisory Approval : Start - Fixed base issue , Payment Max Limt should be checked
                                    lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("PAYMENT_MAX"), m_iClientId);
                                    //lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("RESERVE_MAX"));
                                    //smahajan6: Safeway: Payment Supervisory Approval : End
                                    if ((lMaxAmount > lAmount))
                                    {
                                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                        return iApproverID;
                                    }
                                }
                            }
                            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                            {
                                if (objReader.Read())
                                {
                                    iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                }
                            }
                            return iApproverID;
                        }
                        sSQL = "SELECT * FROM FUNDS_LIMITS WHERE USER_ID = " + iThisManagerID + " AND LINE_OF_BUS_CODE = " + iLOBCode;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            if (objReader.Read())
                            {
                                lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("MAX_AMOUNT"), m_iClientId);
                            }
                            else
                            {
                                break;
                            }
                        }
                        iApproverID = iThisManagerID;
                    }
                    //MITS 11065    :Umesh
                    if (m_objFunds.LineOfBusCode != 0)
                    {
                        if (objLobSettings.PayDetLmtFlag)
                            iApproverID = GetApprovalIDAgainstPayDetail(iApproverID, lAmount, iLOBCode);
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetApprovalID.Error", m_iClientId), p_objException);
            }

            return iApproverID;
        }
        /// <summary>
        /// Get the Approver Id against pay detail limit
        /// </summary>
        /// <param name="p_iAppoverID">Approver ID against payment limit</param>
        /// <param name="lAmount">Amount</param>
        /// <param name="p_iLOBCode">lob code</param>
        /// <returns>Approver Id against pay detail limit</returns>
        private int GetApprovalIDAgainstPayDetail(int p_iAppoverID, double lAmount, int p_iLOBCode)
        {

            string sSQL = string.Empty;
            int iThisManagerID = 0;
            int iApproverID = 0;
            long lMaxAmount = 0;
            bool bValidApprover = true;

            try
            {

                iThisManagerID = p_iAppoverID;
                bValidApprover = ValidApprover(iThisManagerID, p_iLOBCode);
                if (bValidApprover)
                    return p_iAppoverID;
                while (!bValidApprover)
                {
                    iThisManagerID = GetManagerID(iThisManagerID);

                    if (iThisManagerID == 0)
                    {
                        sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + p_iLOBCode;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            while (objReader.Read())
                            {
                                //smahajan6: Safeway: Payment Supervisory Approval : Start
                                if (m_bPmtNotfyImmSup)
                                {
                                    if (m_iUserId != Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId))
                                    {
                                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                        return iApproverID;
                                    }
                                }
                                else
                                {
                                    lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("PAYMENT_MAX"), m_iClientId);
                                    if ((lMaxAmount > lAmount))
                                    {
                                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                        return iApproverID;
                                    }
                                }

                                //Commeted Code - Payment Max should be checked
                                //lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("RESERVE_MAX"));
                                //if ((lMaxAmount > lAmount))
                                //{
                                //    iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                //    return iApproverID;
                                //}

                                ///smahajan6: Safeway: Payment Supervisory Approval : End
                            }
                        }
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                        {
                            if (objReader.Read())
                            {
                                iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                            }
                        }
                        return iApproverID;
                    }

                    //smahajan6: Safeway: Payment Supervisory Approval : Start
                    if (m_bPmtNotfyImmSup)
                    {
                        iApproverID = iThisManagerID;
                        return iApproverID;
                    }
                    //smahajan6: Safeway: Payment Supervisory Approval : End
                    bValidApprover = ValidApprover(iThisManagerID, p_iLOBCode);
                    if (bValidApprover)
                        iApproverID = iThisManagerID;


                }

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetApprovalIDAgainstPayDetail.Error", m_iClientId), p_objException);
            }

            return iApproverID;
        }

        /// <summary>
        /// To varify valid approver
        /// </summary>
        /// <param name="p_iAppoverID">Approver ID </param>
        /// <param name="p_iLOBCode">lob code</param>
        /// <returns>true/false</returns>

        private bool ValidApprover(int p_iAppoverID, int p_iLOBCode)
        {
            string sSQL = string.Empty;
            int iUserGroupId = 0;
            double dMaxAmount = 0.0;
            bool bValidApprover = true;


            try
            {


                foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                {
                    sSQL = "SELECT GROUP_PERMISSIONS.FUNC_ID,USER_MEMBERSHIP.GROUP_ID FROM USER_MEMBERSHIP, GROUP_PERMISSIONS " +
                           " WHERE USER_MEMBERSHIP.USER_ID =" + p_iAppoverID +
                           " AND USER_MEMBERSHIP.GROUP_ID = GROUP_PERMISSIONS.GROUP_ID";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                iUserGroupId = Conversion.ConvertObjToInt(objReader.GetValue("GROUP_ID"), m_iClientId);
                            }

                        }
                    }

                    sSQL = " SELECT LINE_OF_BUS_CODE, RESERVE_TYPE_CODE,MAX_AMOUNT FROM FUNDS_DET_LIMITS "
                    + " WHERE (USER_ID = " + p_iAppoverID;

                    if (iUserGroupId > 0)
                        sSQL += " OR GROUP_ID = " + iUserGroupId;
                    sSQL += ") AND LINE_OF_BUS_CODE=" + p_iLOBCode + " AND RESERVE_TYPE_CODE=" + objFundsTransSplit.ReserveTypeCode;

                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        if (objReader.Read())
                        {
                            dMaxAmount = Conversion.ConvertObjToDouble(objReader.GetValue("MAX_AMOUNT"), m_iClientId);
                            if (dMaxAmount < objFundsTransSplit.Amount)
                            {
                                bValidApprover = false;
                            }
                        }

                    }

                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.ValidApprover.Error", m_iClientId), p_objException);
            }

            return bValidApprover;

        }

        /// <summary>
        /// Get the Manager Id
        /// </summary>
        /// <param name="iRMId">User Id</param>
        /// <returns>Manager Id</returns>
        private int GetManagerID(int iRMId)
        {
            int iManagerID = 0;
            string sSQL = string.Empty;
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.MANAGER_ID ";
                sSQL = sSQL + " FROM USER_DETAILS_TABLE,USER_TABLE";
                sSQL = sSQL + " WHERE USER_DETAILS_TABLE.DSNID = " + m_iDSNID;
                sSQL = sSQL + " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";
                sSQL = sSQL + " AND USER_TABLE.USER_ID = " + iRMId;
                objReader = DbFactory.GetDbReader(m_securityConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iManagerID = Conversion.ConvertObjToInt(objReader.GetValue("MANAGER_ID"), m_iClientId);
                    }
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetManagerID.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return iManagerID;
        }


        /// <summary>
        /// TR# 1402,1403,1404,1405 Pankaj 01/14/06. 
        /// This method implements the Claim Type Change Option if any. 
        /// </summary>
        private void CheckClaimTypeChangeOption()
        {
            int iCurrClaimType = 0;
            int iFirstNewClaimType = -1;
            DbReader objRdr = null;
            DbConnection objConn = null;
            try
            {
                objRdr = DbFactory.GetDbReader(m_sConnectionString, "SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM_ID = " + m_objFunds.ClaimId);
                if (objRdr.Read())
                    iCurrClaimType = objRdr.GetInt32("CLAIM_TYPE_CODE");
                objRdr.Dispose();

                objRdr = DbFactory.GetDbReader(m_sConnectionString,
                    "SELECT * FROM CL_CHG_OPTIONS WHERE LOB = " + m_objFunds.LineOfBusCode + " AND SOURCE_CLAIM_TYPE = " + iCurrClaimType);
                if (!objRdr.Read())
                    return;
                objRdr.Dispose();

                foreach (FundsTransSplit objSplit in m_objFunds.TransSplitList)
                {
                    if (iFirstNewClaimType == -1)
                    {
                        objRdr = DbFactory.GetDbReader(m_sConnectionString,
                            "SELECT DEST_CLAIM_TYPE FROM CL_CHG_OPTIONS WHERE  LOB = " + m_objFunds.LineOfBusCode +
                            " AND SOURCE_CLAIM_TYPE = " + iCurrClaimType + " AND TRIG_TRANS_TYPE = " +
                            objSplit.TransTypeCode);
                        if (objRdr.Read())
                            iFirstNewClaimType = objRdr.GetInt32("DEST_CLAIM_TYPE");
                        objRdr.Dispose();
                    }
                }
                if (iFirstNewClaimType == -1)
                    return;

                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery("UPDATE CLAIM SET CLAIM_TYPE_CODE = " + iFirstNewClaimType + " WHERE CLAIM_ID = " + m_objFunds.ClaimId);
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.CheckClaimTypeChangeOption.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        /// <summary>
        /// Add the assigned bank account to the list if it's not in the list. It
        /// happens when the assigned bank account is no longer valid or effective.
        /// </summary>
        /// <param name="bUseFundsSubAccounts">"Use Sub Bank Accounts" option</param>
        /// <param name="objAccounts">Bank accounts</param>
        private bool addAssignedAccount(bool bUseFundsSubAccounts, ref XmlElement objAccounts)
        {
            XmlElement objAccount = null;
            string sSQL = string.Empty;
            DbReader oReader = null;
            bool bReturn = false;
            try
            {
                if (bUseFundsSubAccounts)
                    sSQL = "SELECT SUB_ACC_NAME FROM BANK_ACC_SUB WHERE SUB_ROW_ID = " + m_objFunds.SubAccountId;
                else
                    sSQL = "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + m_objFunds.AccountId;

                oReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (oReader.Read())
                {
                    CreateElement(objAccounts, "Account", ref objAccount);
                    objAccount.SetAttribute("AccountName", oReader.GetString(0));
                    objAccount.SetAttribute("AccountId", m_objFunds.AccountId.ToString());
                    objAccount.SetAttribute("SubRowId", m_objFunds.SubAccountId.ToString());
                    objAccount.SetAttribute("Selected", true.ToString());
                    bReturn = true;
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("AddAssignedAccount.Error", m_iClientId), ex);
            }
            finally
            {
                if (oReader != null)
                    oReader.Close();
                oReader = null;
            }

            return bReturn;
        }

        public ArrayList GetAccounts_forReSetCheck(bool p_bSubAccount)
        {
            return (this.GetAccounts_forReSetCheck(p_bSubAccount, 0));
        }

        public ArrayList GetAccounts_forReSetCheck(bool p_bSubAccount, int p_iParentAccount)
        {
            Claim objClaim = null;
            Event objEvent = null;
            Policy objPolicy = null;
            // Start Naresh Changes in funds for Enhanced Policy
            PolicyEnh objPolicyEnh = null;
            // End Naresh Changes in funds for Enhanced Policy
            DisabilityPlan objDisabilityPlan = null;
            SysSettings objSysSetting = null;
            DbReader objReader = null;
            DbReader objReaderCount = null;
            ArrayList arrlstAccountDetail = null;
            AccountDetail structAccountDetail;
            object objAccountId = null;

            string sSQL = "";
            bool bUseFundsSubAccounts = false;
            bool bFundsAccountLink = false;
            bool bFoundNextPolicy = false;
            int iPolicyId = 0;
            int iPolicyBankId = 0;
            int iPolicySubAccId = 0;
            int iPlanId = 0;
            int iPlanBankId = 0;
            int iPlanSubAccRowId = 0;
            int iDeptEID = 0;
            int iThisLob = 0;
            //int iOwnerEID = 0 ;
            //int iTableId = 0;
            //int iCount = 0 ;
            int iOrderCondition = 0;
            int iServiceCode = 0;
            bool bExit = false;
            string sEffTriger = "";
            string sTriggerFromDate = "";
            string sTriggerToDate = "";
            string sOrderBy = string.Empty;
            double dClaimTotal = 0;//Geeta 05/17/07 : Modified to fix 9097
            double dOccTotal = 0;
            int iLastPolicyID = 0;
            int iCoverageTypeCode = 0;
            LocalCache objLocalCache = null;
            //Start-Mridul Bansal. 01/12/10. MITS#18229.
            ColLobSettings objLOBSettings = null;
            //End-Mridul Bansal. 01/12/10. MITS#18229.

            try
            {
                arrlstAccountDetail = new ArrayList();
                objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                bUseFundsSubAccounts = objSysSetting.UseFundsSubAcc;

                if (m_bOverrideFundsAccountLink)
                    bFundsAccountLink = m_bFundsAccountLink;
                else
                    bFundsAccountLink = objSysSetting.FundsAccountLink;

                // vaibhav: functionality added for list sorted on priority.

                #region Set the Order By.

                if (p_bSubAccount && bUseFundsSubAccounts)
                    sOrderBy = " ORDER BY SUB_ACC_NAME";
                else
                    sOrderBy = " ORDER BY ACCOUNT_NAME";

                try
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT ORDER_BANK_ACC FROM SYS_PARMS");
                    if (objReader != null && objReader.Read())
                    {
                        iOrderCondition = Common.Conversion.ConvertObjToInt(objReader.GetValue("ORDER_BANK_ACC"), m_iClientId);
                        if (p_bSubAccount && bUseFundsSubAccounts)
                        {
                            switch (iOrderCondition)
                            {
                                case 0:
                                    sOrderBy = "";
                                    break;
                                case 1:
                                    sOrderBy = " ORDER BY BANK_ACC_SUB.PRIORITY  DESC, SUB_ACC_NAME ASC";
                                    break;
                                case 2:
                                    sOrderBy = " ORDER BY SUB_ACC_NAME";
                                    break;
                                case 3:
                                    sOrderBy = " ORDER BY SUB_ACC_NUMBER, SUB_ACC_NAME";
                                    break;
                            }
                        }
                        else
                        {
                            switch (iOrderCondition)
                            {
                                case 0:
                                    sOrderBy = "";
                                    break;
                                case 1:
                                    sOrderBy = " ORDER BY PRIORITY DESC, ACCOUNT_NAME ASC";
                                    break;
                                case 2:
                                    sOrderBy = " ORDER BY ACCOUNT_NAME";
                                    break;
                                case 3:
                                    sOrderBy = " ORDER BY ACCOUNT_NUMBER, ACCOUNT_NAME";
                                    break;
                            }
                        }
                    }
                }
                finally
                {
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
                }
                #endregion


                //JIRA:438 START: ajohari2
                sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID,EFF_TRIGGER,TRIGGER_FROM_DATE,TRIGGER_TO_DATE,IS_EFT_ACCOUNT "//Amitosh for EFT Payment
                                 + " FROM ACCOUNT ";

                //WHERE ISNULL(IS_EFT_ACCOunt,'') != '-1' ";
                //MITS 26889 Start apandey21
                //if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                //{
                //    sSQL = sSQL.Replace("ISNULL", "nvl");
                //}
                //MITS 26889 End
                //JIRA:438 End: 

                if (!bExit)
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL + sOrderBy);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {

                            structAccountDetail = new AccountDetail();
                            structAccountDetail.AccountName = objReader.GetString(0);
                            structAccountDetail.SubRowId = 0;
                            objAccountId = objReader.GetInt(1);

                            try
                            {
                                structAccountDetail.AccountId = Common.Conversion.ConvertObjToInt(objAccountId, m_iClientId);
                            }
                            catch
                            {
                                structAccountDetail.AccountId = 0;
                            }

                            arrlstAccountDetail.Add(structAccountDetail);

                        }
                    }
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetAccounts.ErrorAccount", m_iClientId), p_objEx);
            }
            finally
            {
                objClaim = null;
                objEvent = null;
                objPolicy = null;
                // Start Naresh Changes in funds for Enhanced Policy
                objPolicyEnh = null;
                // End Naresh Changes in funds for Enhanced Policy
                objDisabilityPlan = null;
                objSysSetting = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderCount != null)
                {
                    objReaderCount.Close();
                    objReaderCount.Dispose();
                }
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                //Start-Mridul Bansal. 01/12/10. MITS#18229
                objLOBSettings = null;
                //End-Mridul Bansal. 01/12/10. MITS#18229
            }
            return (arrlstAccountDetail);
        }


        /// <summary>
        /// Author: Animesh Sahai//
        /// Date: 3/12/2010
        /// Method to calculate the bill review fee amount base on the number of line items //skhare7 RMSC Merge 28397
        /// </summary>
        /// <param name="iIemCount"></param>
        /// <returns></returns>
        // private double BillReviewCalculation(int iItemCount)
        // {
        //     string sSQL = string.Empty;
        //     DbReader objReader = null;
        //     int iClaimID = 0;
        //     int iTransID = 0;
        //     int iHospLine = 0;
        //     int iMedLine = 0;
        //     double dblHospAmt = 0;
        //     double dblMedAmt = 0;
        //     string strBillType=string.Empty ;
        //     double dblTotalAmount = 0;
        //     string strOrgLevel=string.Empty;
        //     int iBillOrgLevel=0;
        //     try
        //     {
        //         iClaimID = m_objFunds.ClaimId;
        //         iTransID = m_objFunds.TransId;
        //         iBillOrgLevel =  m_objFunds.Context.InternalSettings.SysSettings.OrgBillLevel;
        //         strOrgLevel=GetOrgHierarchyName(iBillOrgLevel);   
        //         sSQL = "SELECT HOSPITAL_LINES,HOSPITAL_AMT,MEDICAL_LINES,MEDICAL_AMT" 
        //              + " FROM ENTITY WHERE ENTITY_ID =" ;
        //         if (strOrgLevel.Equals("DEPARTMENT")) 
        //         {
        //             sSQL = sSQL + "(SELECT DEPT_EID FROM EVENT,CLAIM WHERE  CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM_ID = " + iClaimID + ")";
        //         }
        //         else
        //         {
        //             sSQL = sSQL + " (SELECT " + strOrgLevel + ".ENTITY_ID"
        //                    + " FROM ENTITY DEPARTMENT"
        //                    + " INNER JOIN ORG_HIERARCHY ON (ORG_HIERARCHY.DEPARTMENT_EID=DEPARTMENT.ENTITY_ID)"
        //                    + " INNER JOIN ENTITY " + strOrgLevel + " ON (" + strOrgLevel + ".ENTITY_ID=ORG_HIERARCHY." + strOrgLevel + "_EID)"
        //                    + " WHERE (DEPARTMENT.ENTITY_ID ="
        //                    + " (SELECT DEPT_EID FROM EVENT,CLAIM WHERE  CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM_ID = " + iClaimID + "))"
        //                    + " AND(" + strOrgLevel + ".ENTITY_TABLE_ID =" + iBillOrgLevel + "))"; 
        //         }
        //         objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //         if (objReader != null)
        //         {
        //             if (objReader.Read())
        //             {
        //                 iHospLine = Conversion.ConvertObjToInt(objReader.GetValue("HOSPITAL_LINES"), m_iClientId);
        //                 dblHospAmt = Conversion.ConvertObjToDouble(objReader.GetValue("HOSPITAL_AMT"), m_iClientId);   
        //                 iMedLine = Conversion.ConvertObjToInt(objReader.GetValue("MEDICAL_LINES"), m_iClientId);
        //                 dblMedAmt = Conversion.ConvertObjToDouble(objReader.GetValue("MEDICAL_AMT"), m_iClientId);
        //             }
        //             objReader.Close();  
        //         }

        //         strBillType = m_objFunds.Context.LocalCache.GetShortCode(m_objFunds.BillTypeCode);
        //         if (strBillType.Equals("H"))
        //         {
        //             if (iItemCount < iHospLine)
        //                 iItemCount = iHospLine;
        //             dblTotalAmount = dblHospAmt * iItemCount;
        //         }
        //         else if (strBillType.Equals("M"))
        //         {
        //             if (iItemCount < iMedLine)
        //                 iItemCount = iMedLine;
        //             dblTotalAmount = dblMedAmt * iItemCount;
        //         }
        //         else
        //             dblTotalAmount = 0;
        //         return dblTotalAmount;
        //     }
        //     catch (Exception p_objException)
        //     {
        //         throw new RMAppException(Globalization.GetString("FundManager.BillReviewCalculation.Error"), p_objException);
        //     }
        //     finally
        //     {
        //         if (objReader != null)
        //         {
        //             objReader.Close();
        //             objReader.Dispose();  
        //         }
        //     }
        // }

        /// <summary>
        /// Method to return the Org Hierarchy level name based on the specified Org Hierarchy code.
        /// </summary>
        /// <param name="iOrgLevel">Org Hierarchy level code</param>
        /// <returns>Org hierarchy Level name</returns>
        // protected string GetOrgHierarchyName(int iOrgLevel)
        // {
        //     string strReturn = string.Empty;
        //     switch (iOrgLevel)
        //     {
        //         case 1005:
        //             strReturn = "CLIENT";
        //             break;
        //         case 1006:
        //             strReturn = "COMPANY";
        //             break;
        //         case 1007:
        //             strReturn = "OPERATION";
        //             break;
        //         case 1008:
        //             strReturn = "REGION";
        //             break;
        //         case 1009:
        //             strReturn = "DIVISION";
        //             break;
        //         case 1010:
        //             strReturn = "LOCATION";
        //             break;
        //         case 1011:
        //             strReturn = "FACILITY";
        //             break;
        //         case 1012:
        //             strReturn = "DEPARTMENT";
        //             break;
        //         default:
        //             strReturn = "OPERATION";
        //             break;
        //     }
        //     return strReturn;
        // }


        // private void CreateBillReviewPayment()
        // {
        //     int iLineItems = 0;
        //     double dblBillReviewFees = 0;
        //     Funds objFundsBillRev = null;
        //     FundsTransSplit objFundsBillRevTransSplit = null;
        //     DbReader objReader = null;
        //     string sSQL = string.Empty; 
        //     try
        //     {
        //         foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
        //         {
        //             if (objFundsTransSplit.Crc == 1234567890)
        //                 iLineItems += 1; 
        //         }
        //         if (iLineItems > 0)
        //         {
        //             dblBillReviewFees = BillReviewCalculation(iLineItems);
        //             if (dblBillReviewFees != 0)
        //             {
        //                 sSQL = "SELECT LAST_NAME,FIRST_NAME,ADDR1,ADDR2,CITY,STATE_ID,ZIP_CODE,TAX_ID,ENTITY_ID "
        //                      + " FROM ENTITY WHERE ABBREVIATION = 'RMSC'"
        //                      + " AND ENTITY_TABLE_ID = 1005";
        //                 objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
        //                 if (objReader != null)
        //                 {
        //                     if (objReader.Read())
        //                     {
        //                         objFundsBillRev = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
        //                         objFundsBillRev.AccountId = m_objFunds.AccountId;
        //                         objFundsBillRev.Amount = dblBillReviewFees;
        //                         objFundsBillRev.ClaimId = m_objFunds.ClaimId;
        //                         objFundsBillRev.ClaimantEid = m_objFunds.ClaimantEid;
        //                         objFundsBillRev.ClaimNumber = m_objFunds.ClaimNumber;
        //                         objFundsBillRev.LastName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
        //                         objFundsBillRev.FirstName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
        //                         objFundsBillRev.PayeeEid = Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), m_iClientId);
        //                         objFundsBillRev.PayeeTypeCode = m_objFunds.Context.LocalCache.GetCodeId("O", "PAYEE_TYPE");
        //                         objFundsBillRev.StatusCode = m_objFunds.Context.LocalCache.GetCodeId("Q", "CHECK_STATUS");
        //                         objFundsBillRev.TransDate = m_objFunds.TransDate;
        //                         objFundsBillRev.PaymentFlag = true;
        //                         objFundsBillRev.DateOfCheck = m_objFunds.DateOfCheck;
        //                         objFundsBillRev.UnitId = m_objFunds.UnitId;
        //                         objFundsBillRev.VoidFlag = true;
        //                         objFundsBillRev.Addr1 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR1"));
        //                         objFundsBillRev.Addr2 = Conversion.ConvertObjToStr(objReader.GetValue("ADDR2"));
        //                         objFundsBillRev.StateId = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), m_iClientId);
        //                         objFundsBillRev.ZipCode = Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE"));
        //                         objFundsBillRev.City = Conversion.ConvertObjToStr(objReader.GetValue("CITY"));

        //                         objFundsBillRevTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);
        //                         objFundsBillRevTransSplit.TransTypeCode = m_objFunds.Context.LocalCache.GetCodeId("BRE","TRANS_TYPES");
        //                         objFundsBillRevTransSplit.ReserveTypeCode = m_objFunds.Context.LocalCache.GetCodeId("E", "RESERVE_TYPE");
        //                         objFundsBillRevTransSplit.Amount = dblBillReviewFees;

        //                         objFundsBillRev.TransSplitList.Add(objFundsBillRevTransSplit);
        //                         objFundsBillRev.Save();  
        //                     }
        //                     objReader.Close();  
        //                 }
        //             }
        //         }
        //     }
        //     catch (Exception p_objException)
        //     {
        //         throw new RMAppException(Globalization.GetString("FundManager.BillReviewCreation.Error"), p_objException);
        //     }
        //     finally
        //     {
        //         if (objReader != null)
        //         {
        //             objReader.Close();
        //             objReader.Dispose();  
        //         }
        //         if (objFundsBillRevTransSplit != null)
        //         {
        //             objFundsBillRevTransSplit.Dispose();
        //             objFundsBillRevTransSplit = null; 
        //         }
        //         if (objFundsBillRev != null)
        //         {
        //             objFundsBillRev.Dispose();
        //             objFundsBillRev = null;  
        //         }

        //     }

        // }

        #endregion
        //Added by Amitosh for mobile adjuster 
        public int getClaimId(string sClaimNumber)
        {
            int iClaimID = 0;
            string sSQL = null;

            try
            {
                sSQL = "SELECT CLAIM_ID FROM CLAIM WHERE CLAIM_NUMBER='" + sClaimNumber + "'";
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iClaimID = objReader.GetInt("CLAIM_ID");
                    }
                }

                return iClaimID;

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
        }

        //public XmlDocument GetPaymentHistoryForMobileAdjuster(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, bool p_bAscending, UserLogin p_objUserLogin)
        //{
        //    StringBuilder sbSQL = null;
        //    LocalCache objLocalCache = null;
        //    XmlElement objRootNode = null;
        //    XmlElement objRowNode = null;
        //    XmlElement objTempNode = null;
        //    //Nadim for 16254(REM Customization)
        //    XmlDocument objXmlAutoCheck = null;
        //    string sAutoCheckbtn = string.Empty;
        //    //Nadim for 16254(REM Customization)

        //    const string DITTO = "\"";

        //    int iTransId = 0;
        //    int iLastTransId = 0;
        //    //int iTransNumber = 0 ;
        //    long iTransNumber = 0;    //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database, So changing its type to Long
        //    int iCheckStatusCodeId = 0;
        //    double dblAmount = 0.0;
        //    double dblNetTotal = 0.0;
        //    double dblSplitAmount = 0.0;
        //    double dblTotalVoid = 0.0;
        //    double dblTotalCollect = 0.0;
        //    double dblTotalPay = 0.0;
        //    double dblTotalVoidPay = 0.0;
        //    double dblTotalVoidCollect = 0.0;
        //    bool bPayment = false;
        //    bool bVoid = false;
        //    bool bCleared = false;
        //    bool bAscending = false;
        //    //nadim for 15388
        //    string sTitle = string.Empty;
        //    LobSettings objLobSettings = null;
        //    string sClaimTitle = string.Empty;
        //    string sClaimant = string.Empty;
        //    string sClaimnumberTitle = string.Empty;
        //    string sDepartment = string.Empty;
        //    int iCaptionlevel = 0;
        //    int iLobCode = 0;
        //    //nadim for 15388
        //    string sSortCol = string.Empty;
        //    string sCtlNumber = string.Empty;
        //    string sTransDate = string.Empty;
        //    string sAmount = string.Empty;
        //    string sTransNumber = string.Empty;
        //    string sFromDate = string.Empty;
        //    string sToDate = string.Empty;
        //    string sInvoiceNumber = string.Empty;
        //    string sCodeDesc = string.Empty;
        //    string sDBType = string.Empty;
        //    string sName = string.Empty;
        //    string sCheckStatusCode = string.Empty;
        //    string sCheckStatusDesc = string.Empty;
        //    string sCheckStatus = string.Empty;
        //    string sCheckDate = string.Empty;
        //    string sVoid = string.Empty;
        //    string sCleared = string.Empty;
        //    string sPayment = string.Empty;
        //    string sAddedByUser = string.Empty;
        //    string sAddress = null;
        //    string sCity = null;

        //    Hashtable objHtTransIds = null;

        //    try
        //    {
        //        // m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword );	
        //        // m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
        //        if (m_objDataModelFactory == null)
        //            this.Initialize();
        //        //nadim for 15388-Set Title
        //        using (DataModelFactory objDataModelFacFortitle = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword))
        //        {
        //            using (Claim objClaimForTitle = (DataModel.Claim)objDataModelFacFortitle.GetDataModelObject("Claim", false))
        //            {
        //                objClaimForTitle.MoveTo(p_iClaimId);
        //                sClaimnumberTitle = objClaimForTitle.ClaimNumber;
        //                iLobCode = objClaimForTitle.LineOfBusCode;
        //                objLobSettings = objDataModelFacFortitle.Context.InternalSettings.ColLobSettings[objClaimForTitle.LineOfBusCode];
        //                iCaptionlevel = objLobSettings.CaptionLevel;
        //                sDepartment = objClaimForTitle.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaimForTitle.Parent as Event).DeptEid, iCaptionlevel, 0, ref iCaptionlevel);
        //                sDepartment = sDepartment.Substring(0, Math.Min(sDepartment.Length, 25));

        //                if (objClaimForTitle.PrimaryClaimant != null)
        //                {
        //                    sClaimant = objClaimForTitle.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
        //                }
        //                else
        //                {
        //                    sClaimant = objClaimForTitle.PrimaryPiEmployee.PiEntity.LastName.Trim() + ',' + objClaimForTitle.PrimaryPiEmployee.PiEntity.FirstName.Trim();
        //                }
        //                if (sClaimant.EndsWith(","))
        //                {
        //                    sClaimant = sClaimant.Remove(sClaimant.LastIndexOf(','));
        //                }
        //            }
        //        }
        //        using (LocalCache objLocalCacheForTitle = new LocalCache(m_sConnectionString))
        //        {
        //            sClaimTitle = objLocalCacheForTitle.GetCodeDesc(iLobCode);
        //        }
        //        sTitle = sClaimTitle + " [" + sClaimnumberTitle + "*" + sDepartment + "*" + sClaimant + "]";
        //        //nadim for 15388 
        //        sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
        //        objLocalCache = new LocalCache(m_sConnectionString);

        //        //Nadim for 16254(REM Customization)
        //        objXmlAutoCheck = new XmlDocument();
        //        string strContent = RMSessionManager.GetCustomContent();
        //        if (!string.IsNullOrEmpty(strContent))
        //        {
        //            objXmlAutoCheck.LoadXml(strContent);
        //            sAutoCheckbtn = objXmlAutoCheck.SelectSingleNode("//RMAdminSettings/SpecialSettings/Show_AutoCheck").InnerText;
        //        }

        //        //Nadim for 16254(REM Customization)
        //        //Nikhil Garg		Dated: 27-Jan-2006
        //        //Checking for View Payment History Permission
        //        string sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId.ToString();
        //        int iLOB = 0;
        //        string sLOB = string.Empty;
        //        int iBaseSecurityCode = 0;
        //        using (DbReader objLOBReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
        //        {
        //            if (objLOBReader != null)
        //            {
        //                if (objLOBReader.Read())
        //                {
        //                    iLOB = Conversion.ConvertObjToInt(objLOBReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
        //                    sLOB = objLocalCache.GetShortCode(iLOB);
        //                    switch (sLOB)
        //                    {
        //                        case "GC":
        //                            iBaseSecurityCode = RMB_CLAIMGC_RESERVES;
        //                            break;
        //                        case "WC":
        //                            iBaseSecurityCode = RMB_CLAIMWC_RESERVES;
        //                            break;
        //                        case "VA":
        //                            //TR:2306 28Feb2006 Nitesh Starts
        //                            if (p_iUnitId != 0)
        //                            { // detail level - coming from unit screen
        //                                iBaseSecurityCode = RMB_CLAIMVA_UNIT_RESERVES;
        //                            }
        //                            else
        //                            { // claim level
        //                                iBaseSecurityCode = RMB_CLAIMVA_RESERVES;
        //                            }
        //                            //TR:2306 28Feb2006 Nitesh Ends
        //                            break;
        //                        case "DI":
        //                            iBaseSecurityCode = RMB_CLAIMDI_RESERVES;
        //                            break;
        //                        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
        //                        case "PC":
        //                            iBaseSecurityCode = RMB_CLAIMPC_RESERVES;
        //                            break;
        //                        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314
        //                    }
        //                    if (!p_objUserLogin.IsAllowedEx(iBaseSecurityCode, RMO_PAYMENT_HISTORY))
        //                        throw new PermissionViolationException(RMPermissions.RMO_PAYMENT_HISTORY, iBaseSecurityCode);
        //                }
        //            }
        //        }
        //        sbSQL = new StringBuilder();

        //        sbSQL.Append(" SELECT FUNDS.ADDR1,FUNDS.ADDR2,FUNDS.CITY,FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, ");
        //        sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
        //        sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,");
        //        sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE, ");
        //        sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, ");
        //        sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE");
        //        sbSQL.Append(" FROM FUNDS, FUNDS_TRANS_SPLIT,CODES_TEXT ");
        //        sbSQL.Append(" WHERE FUNDS.CLAIM_ID = " + p_iClaimId);
        //        sbSQL.Append(" AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");

        //        if (p_iClaimantEid > 0)
        //            sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid);
        //        else
        //        {
        //            if (p_iUnitId > 0)
        //                sbSQL.Append(" AND FUNDS.UNIT_ID = " + p_iUnitId);
        //        }

        //        sbSQL.Append(" AND CODES_TEXT.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE ");

        //        if (p_sSortCol == "")
        //        {
        //           // sbSQL.Append(" ORDER BY FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID ");
        //                //sSortCol = "FUNDS.TRANS_DATE";
        //            sbSQL.Append(" ORDER BY FUNDS.DATE_OF_CHECK DESC, FUNDS.TRANS_ID ");
        //            sSortCol = "FUNDS.DATE_OF_CHECK";
        //            bAscending = false;
        //        }
        //        else
        //        {
        //            sSortCol = p_sSortCol;
        //            bAscending = p_bAscending;
        //            if (bAscending)
        //                sbSQL.Append(" ORDER BY " + sSortCol + " ASC, FUNDS.TRANS_ID");
        //            else
        //                sbSQL.Append(" ORDER BY " + sSortCol + " DESC, FUNDS.TRANS_ID");
        //        }

        //        if (sDBType == Constants.DB_ACCESS)
        //            sbSQL = sbSQL.Replace("|", " AS ");
        //        else
        //            sbSQL = sbSQL.Replace("|", " ");

        //        // Initialize a new XML document and create Payment History node
        //        this.StartDocument(ref objRootNode, "PaymentHistory");
        //        this.CreateAndSetElement(objRootNode, "Title", sTitle);
        //        this.CreateAndSetElement(objRootNode, "ShowAutoCheckbtn", sAutoCheckbtn);//Nadim for 16254(REM Customization)
        //        objHtTransIds = new Hashtable();

        //        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
        //        {
        //            while (objReader.Read())
        //            {
        //                // skip duplicates caused by inner join with splits table
        //                iTransId = Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
        //                //if (iTransId != iLastTransId)
        //                //{
        //                    iLastTransId = iTransId;

        //                    bVoid = Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"));
        //                    sVoid = bVoid ? "Yes" : "No";

        //                    bCleared = Common.Conversion.ConvertObjToBool(objReader.GetValue("CLEARED_FLAG"));
        //                    sCleared = bCleared ? "Yes" : "No";

        //                    bPayment = Common.Conversion.ConvertObjToBool(objReader.GetValue("PAYMENT_FLAG"));
        //                    sPayment = bPayment ? "Payment" : "Collection";

        //                    //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database
        //                    //iTransNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TRANS_NUMBER" ), m_iClientId );
        //                    iTransNumber = Common.Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_NUMBER"));
        //                    sTransNumber = iTransNumber.ToString();

        //                    sCtlNumber = objReader.GetString("CTL_NUMBER");
        //                    sCheckDate = Conversion.GetDBDateFormat(objReader.GetString("DATE_OF_CHECK"), "d");
        //                    sTransDate = Conversion.GetDBDateFormat(objReader.GetString("TRANS_DATE"), "d");
        //                    sAddedByUser = objReader.GetString("ADDED_BY_USER");


        //                    sAddress = objReader.GetString("ADDR1") + objReader.GetString("ADDR2");
        //                    sCity = objReader.GetString("CITY");

        //                    sName = objReader.GetString("FIRST_NAME");
        //                    if (sName != "")
        //                        sName += " " + objReader.GetString("LAST_NAME");
        //                    else
        //                        sName = objReader.GetString("LAST_NAME");

        //                    dblAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue("FAMT")));
        //                    if (!bPayment)
        //                        dblAmount = -dblAmount;
        //                    sAmount = string.Format("{0:C}", dblAmount);

        //                    iCheckStatusCodeId = Common.Conversion.ConvertObjToInt(objReader.GetValue("STATUS_CODE"), m_iClientId);
        //                    objLocalCache.GetCodeInfo(iCheckStatusCodeId, ref sCheckStatusCode, ref sCheckStatusDesc);
        //                    sCheckStatus = sCheckStatusDesc;
        //                //}
        //                //else
        //                //{
        //                //    sVoid = DITTO;
        //                //    sCleared = DITTO;
        //                //    sPayment = DITTO;
        //                //    sTransNumber = DITTO;
        //                //    sCtlNumber = DITTO;
        //                //    sCheckDate = DITTO;
        //                //    sFromDate = DITTO;
        //                //    sToDate = DITTO;
        //                //    sTransDate = DITTO;
        //                //    sAddedByUser = objReader.GetString("ADDED_BY_USER");
        //                //    sName = DITTO;
        //                //    sAmount = DITTO;
        //                //    sCheckStatus = DITTO;
        //                //}

        //                // Code for calculation of total only once for a TransID - Mihika
        //                if (!objHtTransIds.Contains(objReader.GetValue("TRANS_ID")))
        //                {
        //                    if (bVoid)
        //                    {
        //                        if (bPayment)
        //                            dblTotalVoidPay += dblAmount;
        //                        else
        //                            dblTotalVoidCollect += dblAmount;
        //                    }
        //                    else
        //                    {
        //                        if (bPayment)
        //                            dblTotalPay += dblAmount;
        //                        else
        //                            dblTotalCollect += dblAmount;
        //                    }

        //                    objHtTransIds.Add(objReader.GetValue("TRANS_ID"), objReader.GetValue("TRANS_ID"));
        //                }
        //                // End of code -Mihika
        //                sFromDate = Conversion.GetDBDateFormat(objReader.GetString("FROM_DATE"), "d");
        //                sToDate = Conversion.GetDBDateFormat(objReader.GetString("TO_DATE"), "d");

        //                sInvoiceNumber = objReader.GetString("INVOICE_NUMBER");
        //                sCodeDesc = objReader.GetString("CODE_DESC");
        //                dblSplitAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue("FTSAMT")));

        //                if (!bPayment)
        //                    dblSplitAmount = -dblSplitAmount;

        //                this.CreateElement(objRootNode, "row", ref objRowNode);
        //             //   this.CreateAndSetElement(objRowNode, "CtlNumber", sCtlNumber, ref objTempNode);
        //            //    objTempNode.SetAttribute("TransId", iTransId.ToString());
        //                //this.CreateAndSetElement(objRowNode, "TransNumber", sTransNumber);
        //              //  this.CreateAndSetElement(objRowNode, "TransDate", sTransDate);
        //              //  this.CreateAndSetElement(objRowNode, "PaymentFlag", sPayment);
        //                //this.CreateAndSetElement(objRowNode, "ClearedFlag", sCleared);
        //                //this.CreateAndSetElement(objRowNode, "VoidFlag", sVoid);
        //               // this.CreateAndSetElement(objRowNode, "CheckStatus", sCheckStatus);
        //                this.CreateAndSetElement(objRowNode, "Name", sName);
        //                this.CreateAndSetElement(objRowNode, "Address", sAddress);
        //                this.CreateAndSetElement(objRowNode, "City", sCity);
        //                this.CreateAndSetElement(objRowNode, "Amount", sAmount);
        //               // this.CreateAndSetElement(objRowNode, "FromDate", sFromDate);
        //               // this.CreateAndSetElement(objRowNode, "ToDate", sToDate);
        //        //        this.CreateAndSetElement(objRowNode, "InvoiceNumber", sInvoiceNumber);
        //              //  this.CreateAndSetElement(objRowNode, "CodeDesc", sCodeDesc);
        //           //   this.CreateAndSetElement(objRowNode, "SplitAmount", string.Format("{0:C}", dblSplitAmount));
        //            //    this.CreateAndSetElement(objRowNode, "User", sAddedByUser);
        //                this.CreateAndSetElement(objRowNode, "CheckDate", sCheckDate);
        //                //nadim for 15388
        //                //this.CreateAndSetElement(objRowNode, "Title", sTitle);
        //                //nadim for 153888
        //            }
        //        }

        //        dblNetTotal = dblTotalPay + dblTotalCollect;
        //        dblTotalVoid = dblTotalVoidPay + dblTotalVoidCollect;
        //        //Total collection amount needs to be displayed as positive MITS 8823
        //        dblTotalCollect = -dblTotalCollect;
        //        this.CreateAndSetElement(objRootNode, "TotalAll", string.Format("{0:C}", dblNetTotal));
        //        this.CreateAndSetElement(objRootNode, "TotalCollect", string.Format("{0:C}", dblTotalCollect));
        //        this.CreateAndSetElement(objRootNode, "TotalPay", string.Format("{0:C}", dblTotalPay));
        //        this.CreateAndSetElement(objRootNode, "TotalVoid", string.Format("{0:C}", dblTotalVoid));
        //        this.CreateAndSetElement(objRootNode, "PaymentsLessVoids", string.Format("{0:C}", dblTotalPay - dblTotalVoid));

        //        this.CreateAndSetElement(objRootNode, "SortCol", sSortCol);
        //        this.CreateAndSetElement(objRootNode, "Ascending", bAscending.ToString());
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("FundManager.GetPaymentHistory.Error"), p_objEx);
        //    }
        //    finally
        //    {
        //        sbSQL = null;
        //        objRootNode = null;
        //        objRowNode = null;

        //        objHtTransIds = null;
        //        if (objLocalCache != null)
        //            objLocalCache.Dispose();
        //        //nadim for 15388

        //        if (objLobSettings != null)
        //        {
        //            objLobSettings = null;
        //        }
        //        //nadim for 15388

        //    }
        //    return (m_objDocument);
        //}
        //end Amitosh

        public XmlDocument GetPaymentHistoryForMobileAdjuster(int p_iClaimId, int p_iClaimantEid, int p_iUnitId, string p_sSortCol, bool p_bAscending, UserLogin p_objUserLogin, int p_iEntityId, string caller)
        {
            StringBuilder sbSQL = null;
            StringBuilder sentSQL = null;  // averma62 MITS 26999
            LocalCache objLocalCache = null;
            XmlElement objRootNode = null;
            XmlElement objRowNode = null;
            XmlElement objTempNode = null;
            //Nadim for 16254(REM Customization)
            XmlDocument objXmlAutoCheck = null;
            string sAutoCheckbtn = string.Empty;
            //Nadim for 16254(REM Customization)

            //const string DITTO = "\"";

            int iTransId = 0;
            int iLastTransId = 0;
            //int iTransNumber = 0 ;
            long iTransNumber = 0;    //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database, So changing its type to Long
            int iCheckStatusCodeId = 0;
            double dblAmount = 0.0;
            double dblNetTotal = 0.0;
            double dblSplitAmount = 0.0;
            double dblIAmount = 0.0;
            double dblTotalVoid = 0.0;
            double dblTotalCollect = 0.0;
            double dblTotalPay = 0.0;
            double dblTotalVoidPay = 0.0;
            double dblTotalVoidCollect = 0.0;
            bool bPayment = false;
            bool bVoid = false;
            bool bCleared = false;
            bool bAscending = false;
            //nadim for 15388
            string sTitle = string.Empty;
            LobSettings objLobSettings = null;
            string sClaimTitle = string.Empty;
            string sClaimant = string.Empty;
            string sClaimnumberTitle = string.Empty;
            string sDepartment = string.Empty;
            int iCaptionlevel = 0;
            int iLobCode = 0;
            //nadim for 15388
            string sSortCol = string.Empty;
            string sCtlNumber = string.Empty;
            string sTransDate = string.Empty;
            string sAmount = string.Empty;
            string sTransNumber = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sInvoiceNumber = string.Empty;
            string sCodeDesc = string.Empty;
            string sPaymentCode = string.Empty; //mits 35930
            string sPaymentDesc = string.Empty; //mits 35930
            string sDBType = string.Empty;
            string sName = string.Empty;
            string sCheckStatusCode = string.Empty;
            string sCheckStatusDesc = string.Empty;
            string sCheckStatus = string.Empty;
            string sCheckDate = string.Empty;
            string sVoid = string.Empty;
            string sCleared = string.Empty;
            string sPayment = string.Empty;
            string sAddedByUser = string.Empty;
            // Added by Amitosh for mits 23603 (05/09/2011)
            string sPayee = string.Empty;
            Hashtable objHtTransIds = null;

            //rupal:start, for first & final payment
            bool bIsFirstFinal = false;
            string sIsFirstFinal = string.Empty;
            //rupal:end, for first & final payment
            //Added by Amitosh for mits 23603(05/09/2011)

            string sClaimantLastName = string.Empty;
            string sClaimantFirstName = string.Empty;
            //end Amitosh
            //skhare7 R8 enhancement Combined Pay
            bool bIsCombinedPay = false;
            string sIsCombinedPay = string.Empty;
            // bool bFlag=false;

            //skhare7 r8 end
            string sPolicyName = string.Empty;
            string sPolcyCvgType = string.Empty;
            string sUnitType = string.Empty;
            string sPayeesName = string.Empty;  //averma62 MITS 26999
            int iNoOfPayees = 0;
            string sAddress = string.Empty;
            string sCity = string.Empty;
            try
            {
                // m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword );	
                // m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
                if (m_objDataModelFactory == null)
                    this.Initialize();
                //nadim for 15388-Set Title
                //skhare7 r8 enhancement
                if (p_iClaimId != 0)
                {
                    using (DataModelFactory objDataModelFacFortitle = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId))//rkaur27
                    {
                        using (Claim objClaimForTitle = (DataModel.Claim)objDataModelFacFortitle.GetDataModelObject("Claim", false))
                        {
                            objClaimForTitle.MoveTo(p_iClaimId);
                            sClaimnumberTitle = objClaimForTitle.ClaimNumber;
                            iLobCode = objClaimForTitle.LineOfBusCode;
                            objLobSettings = objDataModelFacFortitle.Context.InternalSettings.ColLobSettings[objClaimForTitle.LineOfBusCode];
                            iCaptionlevel = objLobSettings.CaptionLevel;
                            sDepartment = objClaimForTitle.Context.InternalSettings.CacheFunctions.GetOrgParent((objClaimForTitle.Parent as Event).DeptEid, iCaptionlevel, 0, ref iCaptionlevel);
                            sDepartment = sDepartment.Substring(0, Math.Min(sDepartment.Length, 25));

                            if (objClaimForTitle.PrimaryClaimant != null)
                            {
                                sClaimant = objClaimForTitle.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                                //Added by Amitosh for mits 23603  (05/09/2011)
                                sClaimantFirstName = objClaimForTitle.PrimaryClaimant.ClaimantEntity.FirstName.Trim();
                                sClaimantLastName = objClaimForTitle.PrimaryClaimant.ClaimantEntity.LastName.Trim();
                                //end Amitosh
                            }
                            else
                            {
                                sClaimant = objClaimForTitle.PrimaryPiEmployee.PiEntity.LastName.Trim() + ',' + objClaimForTitle.PrimaryPiEmployee.PiEntity.FirstName.Trim();
                                //Added by Amitosh for mits 23603  (05/09/2011)  
                                sClaimantFirstName = objClaimForTitle.PrimaryPiEmployee.PiEntity.FirstName.Trim();
                                sClaimantLastName = objClaimForTitle.PrimaryPiEmployee.PiEntity.LastName.Trim();
                                //end Amitosh
                            }
                            if (sClaimant.EndsWith(","))
                            {
                                sClaimant = sClaimant.Remove(sClaimant.LastIndexOf(','));
                            }
                        }
                    }
                    using (LocalCache objLocalCacheForTitle = new LocalCache(m_sConnectionString, m_iClientId))
                    {
                        sClaimTitle = objLocalCacheForTitle.GetCodeDesc(iLobCode);
                    }
                    sTitle = sClaimTitle + " [" + sClaimnumberTitle + "*" + sDepartment + "*" + sClaimant + "]";
                    //nadim for 15388 
                }//skhare7 R8 Enhancement
                sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

                //Nadim for 16254(REM Customization)
                objXmlAutoCheck = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                if (!string.IsNullOrEmpty(strContent))
                {
                    objXmlAutoCheck.LoadXml(strContent);
                    sAutoCheckbtn = objXmlAutoCheck.SelectSingleNode("//RMAdminSettings/SpecialSettings/Show_AutoCheck").InnerText;
                }

                //Nadim for 16254(REM Customization)
                //Nikhil Garg		Dated: 27-Jan-2006
                //Checking for View Payment History Permission
                string sSQL = "SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + p_iClaimId.ToString();
                int iLOB = 0;
                string sLOB = string.Empty;
                int iBaseSecurityCode = 0;
                //mini perofrmance mits
                //using (DbReader objLOBReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))



                //  {
                //    if (objLOBReader != null)
                //    {
                //        if (objLOBReader.Read())
                //        {
                //            iLOB = Conversion.ConvertObjToInt(objLOBReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                //            sLOB = objLocalCache.GetShortCode(iLOB);

                //mini perofrmance mits
                if (iLobCode != default(int))
                {
                    sLOB = objLocalCache.GetShortCode(iLobCode);
                    switch (sLOB)
                    {
                        case "GC":
                            iBaseSecurityCode = RMB_CLAIMGC_RESERVES;
                            break;
                        case "WC":
                            iBaseSecurityCode = RMB_CLAIMWC_RESERVES;
                            break;
                        case "VA":
                            //TR:2306 28Feb2006 Nitesh Starts
                            if (p_iUnitId != 0)
                            { // detail level - coming from unit screen
                                iBaseSecurityCode = RMB_CLAIMVA_UNIT_RESERVES;
                            }
                            else
                            { // claim level
                                iBaseSecurityCode = RMB_CLAIMVA_RESERVES;
                            }
                            //TR:2306 28Feb2006 Nitesh Ends
                            break;
                        case "DI":
                            iBaseSecurityCode = RMB_CLAIMDI_RESERVES;
                            break;
                        //Start:Added by Nitin Goel:Correct SMS setting for Property Claim,04/02/2010,MITS#19314
                        case "PC":
                            iBaseSecurityCode = RMB_CLAIMPC_RESERVES;
                            break;
                        //End:Nitin Goel,04/02/2010:Correct SMS setting for Property Claim,MITS#19314
                    }
                    if (!p_objUserLogin.IsAllowedEx(iBaseSecurityCode, RMO_PAYMENT_HISTORY))
                        throw new PermissionViolationException(RMPermissions.RMO_PAYMENT_HISTORY, iBaseSecurityCode);
                    //perofrmance mini }
                    // perofrmance mini}
                }
                sbSQL = new StringBuilder();

                //sbSQL.Append( " SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, " );
                //sbSQL.Append( " FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, " );
                //sbSQL.Append( " FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT," );
                //sbSQL.Append( " FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE, " );
                //sbSQL.Append( " FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER, " );
                //sbSQL.Append( " FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE" );
                //sbSQL.Append( " FROM FUNDS, FUNDS_TRANS_SPLIT,CODES_TEXT " );
                //sbSQL.Append( " WHERE FUNDS.CLAIM_ID = " + p_iClaimId );
                //sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID " );

                //changed by Amitosh for mits 23603(05/09/2011)

                //rupal:start, for first & final payment
                //added FUNDS_TRANS_SPLIT.IS_FIRST_FINAL in select clause -rupal
                SysSettings objSysSetting = new SysSettings(m_sConnectionString, m_iClientId);//Ash - cloud
                //if (objSysSetting.MultiCovgPerClm == -1)
                //skhare7 R8 enhancement 
                //if (p_iEntityId!=0)
                //{
                //    sTitle = string.Empty;

                //    sbSQL.Append(" SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.CLAIM_ID,FUNDS.CLAIM_NUMBER, ");
                //    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.SETTLEMENT_FLAG,FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                //    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,");
                //    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE, ");
                //    sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG, ");
                //    sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL");
                //    sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT,CODES_TEXT,FUNDS  ");
                //    sbSQL.Append(" WHERE  FUNDS.PAYEE_EID = " + p_iEntityId );
                //    sbSQL.Append(" AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                //}
                //else
                //{
                //    sbSQL.Append(" SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_ID,FUNDS.CLAIM_ID, FUNDS.TRANS_DATE,CLAIM.CLAIM_NUMBER CLAIMNO, FUNDS.VOID_FLAG, ");
                //    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.SETTLEMENT_FLAG,FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                //    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,");
                //    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC, FUNDS_TRANS_SPLIT.FROM_DATE, ");
                //    sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG, ");
                //    sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL");
                //    sbSQL.Append(" FROM FUNDS,CLAIM, FUNDS_TRANS_SPLIT,CODES_TEXT ");
                //    sbSQL.Append("WHERE FUNDS.CLAIM_ID = " + p_iClaimId);
                //    sbSQL.Append(" AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                //    sbSQL.Append(" AND FUNDS.CLAIM_ID = CLAIM.CLAIM_ID ");
                //}

                //ddhiman : commented : MITS 27009
                //sTitle = string.Empty;                 //if (objSysSetting.MultiCovgPerClm == -1)
                //End ddhiman

                //skhare7 R8 enhancement 
                //    if (p_iEntityId!=0)

                if (objSysSetting.MultiCovgPerClm == -1)
                {
                    sbSQL.Append(" SELECT FUNDS.ADDR1,FUNDS.ADDR2,FUNDS.ADDR3,FUNDS.ADDR4,FUNDS.CITY,FUNDS.ZIP_CODE, FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.CLAIM_ID,FUNDS.CLAIM_NUMBER, ");
                    // akaushik5 Changed for MITS 37343 Starts
                    //sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.SETTLEMENT_FLAG,FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    // akaushik5 Changed for MITS 37343 Ends
                    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE, FUNDS_TRANS_SPLIT.FROM_DATE, POL.CODE_DESC COVERAGETYPE,");
                    sbSQL.Append(" POLICY.POLICY_NAME, FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG, ");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL,POLICY_X_UNIT.UNIT_ID");
                    sbSQL.Append(" , CASE  POLICY_X_UNIT.UNIT_TYPE  WHEN 'P' THEN (SELECT PIN FROM PROPERTY_UNIT WHERE PROPERTY_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'V' THEN (SELECT VIN FROM VEHICLE WHERE UNIT_ID = POLICY_X_UNIT.UNIT_ID)  WHEN 'E' THEN (SELECT EMPLOYEE_DETAILS FROM WC_POLICY_EMPLOYEE) END UNIT_TYPE");
                    sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT,CODES_TEXT,FUNDS  ");
                    sbSQL.Append(" ,POLICY_X_CVG_TYPE,CODES_TEXT POL,POLICY, POLICY_X_UNIT");

                    sbSQL.Append(" WHERE  FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                    sbSQL.Append(" AND  FUNDS_TRANS_SPLIT.POLCVG_ROW_ID=POLICY_X_CVG_TYPE.POLCVG_ROW_ID ");
                    sbSQL.Append("   AND POL.CODE_ID=POLICY_X_CVG_TYPE.COVERAGE_TYPE_CODE ");
                    sbSQL.Append("  AND   POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID ");
                    sbSQL.Append(" AND POLICY.POLICY_ID =POLICY_X_UNIT.POLICY_ID");

                }
                else
                {
                    sbSQL.Append(" SELECT FUNDS.ADDR1,FUNDS.ADDR2,FUNDS.ADDR3,FUNDS.ADDR4,FUNDS.CITY,FUNDS.ZIP_CODE, FUNDS.CTL_NUMBER, FUNDS.TRANS_ID, FUNDS.TRANS_DATE, FUNDS.VOID_FLAG, FUNDS.CLAIM_ID,FUNDS.CLAIM_NUMBER, ");
                    // akaushik5 Changed for MITS 37343 Starts
                    //sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.SETTLEMENT_FLAG,FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    sbSQL.Append(" FUNDS.TRANS_NUMBER, FUNDS.CLEARED_FLAG, FUNDS.AMOUNT | FAMT, FUNDS.FIRST_NAME, ");
                    // akaushik5 Changed for MITS 37343 Ends
                    sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PAYMENT_FLAG,FUNDS_TRANS_SPLIT.AMOUNT | FTSAMT,");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, CODES_TEXT.CODE_DESC,CODES_TEXT.SHORT_CODE, FUNDS_TRANS_SPLIT.FROM_DATE, ");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.TO_DATE, FUNDS_TRANS_SPLIT.INVOICE_NUMBER,FUNDS.COMBINED_PAY_FLAG, ");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.ADDED_BY_USER,FUNDS.DATE_OF_CHECK, FUNDS.STATUS_CODE,FUNDS_TRANS_SPLIT.IS_FIRST_FINAL");
                    sbSQL.Append(" FROM  FUNDS_TRANS_SPLIT,CODES_TEXT,FUNDS  ");
                    sbSQL.Append(" WHERE  FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                }
                if (p_iEntityId != 0)
                {

                    sbSQL.Append("  AND FUNDS.PAYEE_EID = " + p_iEntityId);
                }
                else
                {

                    sbSQL.Append(" AND FUNDS.CLAIM_ID = " + p_iClaimId);


                }
                if (p_iClaimantEid > 0)
                    sbSQL.Append(" AND FUNDS.CLAIMANT_EID = " + p_iClaimantEid);
                else
                {
                    if (p_iUnitId > 0)
                        sbSQL.Append(" AND FUNDS.UNIT_ID = " + p_iUnitId);
                }

                sbSQL.Append(" AND CODES_TEXT.CODE_ID = FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE ");

                if (p_sSortCol == "")
                {
                    sbSQL.Append(" ORDER BY FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID ");
                    sSortCol = "FUNDS.TRANS_DATE";
                    bAscending = false;
                }
                else
                {
                    sSortCol = p_sSortCol;
                    bAscending = p_bAscending;
                    if (bAscending)
                        sbSQL.Append(" ORDER BY " + sSortCol + " ASC, FUNDS.TRANS_ID");
                    else
                        sbSQL.Append(" ORDER BY " + sSortCol + " DESC, FUNDS.TRANS_ID");
                }

                if (sDBType == Constants.DB_ACCESS)
                    sbSQL = sbSQL.Replace("|", " AS ");
                else
                    sbSQL = sbSQL.Replace("|", " ");

                // Initialize a new XML document and create Payment History node
                this.StartDocument(ref objRootNode, "PaymentHistory");
                //ddhiman : commented MITS 27009
                //rsharma220 mobility window changes //mits 35930
                if (caller == "MobilityAdjuster")
                    this.CreateAndSetElement(objRootNode, "Title", sTitle);
                //End ddhiman
                if (caller != "MobilityAdjuster")
                {
                    this.CreateAndSetElement(objRootNode, "ShowAutoCheckbtn", sAutoCheckbtn);//Nadim for 16254(REM Customization)
                    //rupal: for first & final payment                
                    this.CreateAndSetElement(objRootNode, "CarrierClaims", objSysSetting.MultiCovgPerClm.ToString());
                }
                objHtTransIds = new Hashtable();

                //Manish for multi currency MITS 27209
                string culture = CommonFunctions.GetCulture(p_iClaimId, CommonFunctions.NavFormType.None, m_sConnectionString);


                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    while (objReader.Read())
                    {
                        // skip duplicates caused by inner join with splits table
                        iTransId = Common.Conversion.ConvertObjToInt(objReader.GetValue("TRANS_ID"), m_iClientId);
                        //Added by Amitosh for mits 23603  (05/09/2011)
                        this.CreateElement(objRootNode, "row", ref objRowNode);
                        //end Amitosh
                        // if (iTransId != iLastTransId)
                        // {
                        iLastTransId = iTransId;
                        //Start averma62 MITS 26999
                        string sEntSQL = "SELECT ISNULL(ENTITY.FIRST_NAME,'') + ' ' + ENTITY.LAST_NAME AS PAYEES_NAME from ENTITY INNER JOIN FUNDS_X_PAYEE ON  ENTITY.ENTITY_ID =FUNDS_X_PAYEE.PAYEE_EID WHERE FUNDS_X_PAYEE.FUNDS_TRANS_ID=" + iTransId;
                        if (m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() == Constants.DB_ORACLE)
                        {
                            sEntSQL = sEntSQL.Replace("ISNULL", "nvl");
                            sEntSQL = sEntSQL.Replace("+", "||");

                        }
                        // akaushik5 Added for MITS 37343 Starts
                        sName = string.Empty;
                        // akaushik5 Added for MITS 37343 Ends
                        using (DbReader objeReader = DbFactory.GetDbReader(m_sConnectionString, sEntSQL))
                        {
                            while (objeReader.Read())
                            {
                                if (sPayeesName == string.Empty)
                                {
                                    sPayeesName = "<Payee>" + objeReader.GetString("PAYEES_NAME") + "</Payee>";
                                }
                                else
                                {
                                    sPayeesName = sPayeesName + "<Payee>" + objeReader.GetString("PAYEES_NAME") + "</Payee>";
                                }

                                // akaushik5 Added for MITS 37343 Starts
                                sName = string.IsNullOrEmpty(sName) ? objeReader.GetString("PAYEES_NAME") : string.Format("{0} and {1}", sName, objeReader.GetString("PAYEES_NAME"));
                                // akaushik5 Added for MITS 37343 Ends
                                iNoOfPayees++;

                            }
                        }
                        //End averma62 MITS 26999

                        bVoid = Common.Conversion.ConvertObjToBool(objReader.GetValue("VOID_FLAG"), m_iClientId);
                        sVoid = bVoid ? "Yes" : "No";

                        bCleared = Common.Conversion.ConvertObjToBool(objReader.GetValue("CLEARED_FLAG"), m_iClientId);
                        sCleared = bCleared ? "Yes" : "No";

                        bPayment = Common.Conversion.ConvertObjToBool(objReader.GetValue("PAYMENT_FLAG"), m_iClientId);
                        sPayment = bPayment ? "Payment" : "Collection";

                        //rupal:start, for first & final payment
                        bIsFirstFinal = Common.Conversion.ConvertObjToBool(objReader.GetValue("IS_FIRST_FINAL"), m_iClientId);
                        sIsFirstFinal = bIsFirstFinal ? "Yes" : "No";
                        //rupal:end, for first & final payment

                        //pmittal5 Mits 14500 02/23/09 - TRANS_NUMBER has 64 bits length in Database
                        //iTransNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TRANS_NUMBER" ), m_iClientId );
                        iTransNumber = Common.Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_NUMBER"), m_iClientId);
                        sTransNumber = iTransNumber.ToString();

                        sCtlNumber = objReader.GetString("CTL_NUMBER");
                        sCheckDate = Conversion.GetDBDateFormat(objReader.GetString("DATE_OF_CHECK"), "d");
                        sTransDate = Conversion.GetDBDateFormat(objReader.GetString("TRANS_DATE"), "d");
                        sAddedByUser = objReader.GetString("ADDED_BY_USER");
                        sAddress = objReader.GetString("ADDR1") + " " + objReader.GetString("ADDR2") + " " + objReader.GetString("ADDR3") + " " + objReader.GetString("ADDR4");
                        sCity = objReader.GetString("CITY");

                        sName = objReader.GetString("FIRST_NAME");
                        if (sName != "")
                            sName += " " + objReader.GetString("LAST_NAME");
                        else
                            sName = objReader.GetString("LAST_NAME");

                        //Manish Jain Multicurrency MITS 27209
                        dblAmount = Common.Conversion.ConvertObjToDouble(objReader.GetValue("FAMT"), m_iClientId);
                        //dblAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(objReader.GetValue("FAMT")));
                        if (!bPayment)
                            dblAmount = -dblAmount;
                        //sAmount = string.Format("{0:C}", dblAmount);

                        //Manish for multi currency MITS 27209
                        sAmount = CommonFunctions.ConvertByCurrencyCode(objSysSetting.BaseCurrencyType, dblAmount, m_sConnectionString, m_iClientId);


                        iCheckStatusCodeId = Common.Conversion.ConvertObjToInt(objReader.GetValue("STATUS_CODE"), m_iClientId);
                        objLocalCache.GetCodeInfo(iCheckStatusCodeId, ref sCheckStatusCode, ref sCheckStatusDesc);
                        sCheckStatus = sCheckStatusDesc;

                        //Added by Amitosh for mits 23603  (05/09/2011)

                        sPayee = objReader.GetString("LAST_NAME") + ", " + objReader.GetString("FIRST_NAME");
                        // akaushik5 Commented for MITS 37343 Starts
                        //if (objReader.GetInt16("SETTLEMENT_FLAG") == -1 && !string.IsNullOrEmpty(sClaimant) && !sPayee.Equals(sClaimant))
                        //{
                        //    if (!string.IsNullOrEmpty(sClaimantFirstName))
                        //    {
                        //        sName = sName + " and " + sClaimantFirstName + " " + sClaimantLastName;
                        //    }
                        //    else
                        //    {
                        //        sName = sName + " and " + sClaimantLastName;
                        //    }

                        //}
                        // akaushik5 Commented for MITS 37343 Ends
                        //mini performance mits
                        //else
                        //{
                        //    sName = sName;
                        //}
                        //mini performance mits
                        //skhare7:start, R8 combined Pay
                        bIsCombinedPay = Common.Conversion.ConvertObjToBool(objReader.GetValue("COMBINED_PAY_FLAG"), m_iClientId);
                        sIsCombinedPay = bIsCombinedPay ? "Yes" : "No";
                        if (objSysSetting.MultiCovgPerClm == -1)
                        { //skhare7 r8 
                            sPolicyName = objReader.GetString("POLICY_NAME");
                            sPolcyCvgType = objReader.GetString("COVERAGETYPE");

                            sUnitType = objReader.GetString("UNIT_TYPE");
                        }
                        //skhare7:start, R8 combined Pay End
                        ////end Amitosh

                        //ddhiman : MITS 27009
                        if (string.IsNullOrEmpty(sTitle) && p_iEntityId != 0)
                        {
                            sTitle = sName;
                        }
                        //End ddhiman
                        //}
                        //else
                        //{
                        //    sVoid = DITTO;
                        //    sCleared = DITTO;
                        //    sPayment = DITTO;
                        //    sTransNumber = DITTO;
                        //    sCtlNumber = DITTO;
                        //    sCheckDate = DITTO;
                        //    sFromDate = DITTO;
                        //    sToDate = DITTO;
                        //    sTransDate = Conversion.GetDBDateFormat(objReader.GetString("TRANS_DATE"), "d"); //neha: ditto removed as this is date time column
                        //    sAddedByUser = objReader.GetString("ADDED_BY_USER");
                        //    sName = DITTO;
                        //    sAmount = DITTO;
                        //    sCheckStatus = DITTO;
                        //    //rupal:start, for first & final payment
                        //    bIsFirstFinal = Common.Conversion.ConvertObjToBool(objReader.GetValue("IS_FIRST_FINAL"));
                        //    sIsFirstFinal = bIsFirstFinal ? "Yes" : "No";
                        //    //rupal:end, for first & final payment
                        //    //skhare7 r8 Combined Payment
                        //    bIsCombinedPay = Common.Conversion.ConvertObjToBool(objReader.GetValue("COMBINED_PAY_FLAG"));
                        //    sIsCombinedPay = bIsCombinedPay ? "Yes" : "No";
                        //}
                        if (objSysSetting.MultiCovgPerClm == -1)
                        { //skhare7 r8 
                            sPolicyName = objReader.GetString("POLICY_NAME");
                            sPolcyCvgType = objReader.GetString("COVERAGETYPE");
                            sUnitType = objReader.GetString("UNIT_TYPE");
                        }
                        // Code for calculation of total only once for a TransID - Mihika
                        if (!objHtTransIds.Contains(objReader.GetValue("TRANS_ID")))
                        {
                            if (bVoid)
                            {
                                if (bPayment)
                                    dblTotalVoidPay += dblAmount;
                                else
                                    dblTotalVoidCollect += dblAmount;
                            }
                            else
                            {
                                if (bPayment)
                                    dblTotalPay += dblAmount;
                                else
                                    dblTotalCollect += dblAmount;
                            }

                            objHtTransIds.Add(objReader.GetValue("TRANS_ID"), objReader.GetValue("TRANS_ID"));
                        }
                        // End of code -Mihika
                        sFromDate = Conversion.GetDBDateFormat(objReader.GetString("FROM_DATE"), "d");
                        sToDate = Conversion.GetDBDateFormat(objReader.GetString("TO_DATE"), "d");

                        sInvoiceNumber = objReader.GetString("INVOICE_NUMBER");
                        sCodeDesc = objReader.GetString("SHORT_CODE") + " " + objReader.GetString("CODE_DESC");
                        sPaymentCode = objReader.GetString("SHORT_CODE");
                        sPaymentDesc = objReader.GetString("CODE_DESC");

                        //Manish Jain Multicurrency MITS 27209
                        ////dblSplitAmount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr( objReader.GetValue( "FTSAMT" ) ));
                        dblSplitAmount = Common.Conversion.ConvertObjToDouble(objReader.GetValue("FTSAMT"), m_iClientId);//Deb Changes 

                        if (!bPayment)
                            dblSplitAmount = -dblSplitAmount;

                        //dblIAmount = Common.Conversion.ConvertObjToDouble(objReader.GetValue("FIAMT"), m_iClientId);//Deb Changes 
                        //if (!bPayment)
                        //    dblIAmount = -dblIAmount;
                        this.CreateAndSetElement(objRowNode, "Name", sName);
                        this.CreateAndSetElement(objRowNode, "Address", sAddress);
                        this.CreateAndSetElement(objRowNode, "City", sCity);

                        //Moved at start of while loop
                        //this.CreateElement( objRootNode , "row" , ref objRowNode );
                        if (caller != "MobilityAdjuster")
                        {
                            this.CreateAndSetElement(objRowNode, "CtlNumber", sCtlNumber, ref objTempNode);
                            objTempNode.SetAttribute("TransId", iTransId.ToString());
                        }
                        if (caller == "MobilityAdjuster")
                            this.CreateAndSetElement(objRowNode, "CheckNumber", sTransNumber);

                        //this.CreateAndSetElement(objRowNode, "TransNumber", sTransNumber);
                        //this.CreateAndSetElement(objRowNode, "TransDate", sTransDate);
                        //this.CreateAndSetElement(objRowNode, "PaymentFlag", sPayment);
                        //rupal:start, for first & final payme
                        //Start averma62 MITS 26999
                        if (string.IsNullOrEmpty(sPayeesName))
                        {
                            if (!string.IsNullOrEmpty(sClaimantFirstName))
                            {
                                sPayeesName = "<Payee>" + sClaimantFirstName + " " + sClaimantLastName + "</Payee>";
                            }
                            else
                            {
                                sPayeesName = "<Payee>" + sClaimantLastName + "</Payee>";
                            }
                        }
                        if (caller != "MobilityAdjuster")
                        {
                            this.CreateAndSetElement(objRowNode, "Payees_Name", sPayeesName.Trim());
                            this.CreateAndSetElement(objRowNode, "NoOfPayees", iNoOfPayees.ToString());
                        }
                        sPayeesName = string.Empty;
                        iNoOfPayees = 0;
                        //End averma62 MITS 26999
                        //this.CreateAndSetElement(objRowNode, "IsFirstFinal", sIsFirstFinal);

                        //rupal:end, for first & final payment
                        //this.CreateAndSetElement(objRowNode, "ClearedFlag", sCleared);
                        //this.CreateAndSetElement(objRowNode, "VoidFlag", sVoid);
                        if (sCheckStatus != "void")
                            this.CreateAndSetElement(objRowNode, "CheckStatus", sCheckStatus);
                        else
                            this.CreateAndSetElement(objRowNode, "CheckStatus", "VOID");
                        this.CreateAndSetElement(objRowNode, "Amount", sAmount);
                        //Manish Jain Multicurrency MITS 27209
                        //this.CreateAndSetElement(objRowNode, "DAmount", Convert.ToString(dblAmount));
                        //this.CreateAndSetElement(objRowNode, "FromDate", sFromDate);
                        //this.CreateAndSetElement(objRowNode, "ToDate", sToDate);
                        //this.CreateAndSetElement(objRowNode, "InvoiceNumber", sInvoiceNumber);
                        if (caller == "MobilityAdjuster")
                        {
                            this.CreateAndSetElement(objRowNode, "PaymentTypes", sCodeDesc);
                            this.CreateAndSetElement(objRowNode, "PaymentShortCode", sPaymentCode);
                            this.CreateAndSetElement(objRowNode, "PaymentDescription", sPaymentDesc);
                        }//this.CreateAndSetElement( objRowNode , "SplitAmount" , string.Format( "{0:C}" , dblSplitAmount ) );
                        //Manish Jain Multicurrency MITS 27209
                        if (caller != "MobilityAdjuster")
                            this.CreateAndSetElement(objRowNode, "SplitAmount", CommonFunctions.ConvertByCurrencyCode(objSysSetting.BaseCurrencyType, dblSplitAmount, m_sConnectionString, m_iClientId));
                        this.CreateAndSetElement(objRowNode, "InvoiceAmount", CommonFunctions.ConvertByCurrencyCode(objSysSetting.BaseCurrencyType, dblIAmount, m_sConnectionString, m_iClientId));
                        //this.CreateAndSetElement(objRowNode, "DSplitAmount", Convert.ToString(dblSplitAmount));
                        //this.CreateAndSetElement(objRowNode, "User", sAddedByUser);
                        this.CreateAndSetElement(objRowNode, "CheckDate", sCheckDate);
                        //Manish Jain Multicurrency MITS 27209
                        //this.CreateAndSetElement(objRowNode, "BaseCurrency", culture);
                        //nadim for 15388
                        //this.CreateAndSetElement(objRowNode, "Title", sTitle);
                        //nadim for 153888
                        //skhare7:start, for R8 combined Pay

                        //this.CreateAndSetElement(objRowNode, "CombinedPay", sIsCombinedPay);
                        //skhare7 r8 combined Payment
                        //skhare7 r8 
                        //if (objSysSetting.MultiCovgPerClm == -1)
                        //{
                        //    this.CreateAndSetElement(objRowNode, "PolicyName", sPolicyName);
                        //    this.CreateAndSetElement(objRowNode, "CoverageType", sPolcyCvgType);
                        //    this.CreateAndSetElement(objRowNode, "Unit", sUnitType);
                        //}

                        //skhare7:start, for R8 combined Pay End

                    }
                }

                dblNetTotal = dblTotalPay + dblTotalCollect;
                dblTotalVoid = dblTotalVoidPay + dblTotalVoidCollect;
                //Total collection amount needs to be displayed as positive MITS 8823
                dblTotalCollect = -dblTotalCollect;
                this.CreateAndSetElement(objRootNode, "TotalAll", string.Format("{0:C}", dblNetTotal));
                this.CreateAndSetElement(objRootNode, "TotalCollect", string.Format("{0:C}", dblTotalCollect));
                this.CreateAndSetElement(objRootNode, "TotalPay", string.Format("{0:C}", dblTotalPay));
                this.CreateAndSetElement(objRootNode, "TotalVoid", string.Format("{0:C}", dblTotalVoid));
                this.CreateAndSetElement(objRootNode, "PaymentsLessVoids", string.Format("{0:C}", dblTotalPay - dblTotalVoid));

                this.CreateAndSetElement(objRootNode, "SortCol", sSortCol);
                this.CreateAndSetElement(objRootNode, "Ascending", bAscending.ToString());

                //ddhiman : MITS 27009
                //this.CreateAndSetElement(objRootNode, "Title", sTitle);
                //End ddhiman
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetPaymentHistory.Error", m_iClientId), p_objEx);
            }
            finally
            {
                sbSQL = null;
                objRootNode = null;
                objRowNode = null;
                sentSQL = null; //averma62

                objHtTransIds = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                //nadim for 15388

                if (objLobSettings != null)
                {
                    objLobSettings = null;
                }
                //nadim for 15388

            }
            return (m_objDocument);
        }
        //end Amitosh

        #region Get Number of Records Per Page
        //Ankit:Start changes for 29834-paging
        /// <summary>
        /// Function added to fetch number of records per page for diary list 
        /// </summary>
        /// <returns></returns>
        /// //performance issue
        //public int GetRecordsPerPage()
        //{
        //    StringBuilder sbSQL = null;
        //    int iReturn = 0;
        //    DbReader objReader = null;

        //    try
        //    {
        //        sbSQL = new StringBuilder();
        //        sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
        //        sbSQL.Append(" WHERE PARM_NAME = 'PAY_HISTORY_LIMIT' ");
        //        //objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()); mini perofrmance mits
        //        using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
        //        {
        //            if (objReader.Read())
        //            {
        //                iReturn = Conversion.ConvertObjToInt(objReader.GetValue("STR_PARM_VALUE"), m_iClientId);
        //                if (iReturn < 10)
        //                    iReturn = 10;//Record per Page should greater than equal to 10
        //            }
        //            else
        //            {
        //                //iReturn=0;
        //                iReturn = 10;//Record per Page should greater than equal to 10
        //            }
        //        }
        //        //if (!objReader.IsClosed)
        //        //{
        //        //    objReader.Close();
        //        //}
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    finally
        //    {
        //        sbSQL = null;
        //        //mini perofrmance mits
        //        //if (!objReader.IsClosed)
        //        //{
        //        //    objReader.Close();
        //        //    //Shivendu to dispose the object
        //        //    objReader.Dispose();
        //        //}
        //        //mini perofrmance mits
        //       // objReader = null;
        //    }
        //    return iReturn;
        //}
        //performance issue
        //aanandpraka2:End changes for custom paging
        #endregion
        public string ModifySortColumn(string sColumnName, string sDBType)
        {
            switch (sColumnName)
            {
                //mbahl3 mits 28455
                case "CtlNumber":
                case "FUNDS.CTL_NUMBER":
                    sColumnName = "FUNDS.CTL_NUMBER";
                    break;

                case "TransNumber":
                case "FUNDS.TRANS_NUMBER":
                    sColumnName = "FUNDS.TRANS_NUMBER";
                    break;
                case "TransDate":
                case "FUNDS.TRANS_DATE":
                    sColumnName = "FUNDS.TRANS_DATE";
                    break;
                case "PaymentFlag":
                case "FUNDS.PAYMENT_FLAG":
                    sColumnName = "FUNDS.PAYMENT_FLAG";
                    break;
                case "ClearedFlag":
                case "FUNDS.CLEARED_FLAG":
                    sColumnName = "FUNDS.CLEARED_FLAG";
                    break;
                case "VoidFlag":
                case "FUNDS.VOID_FLAG":
                    sColumnName = "FUNDS.VOID_FLAG";
                    break;
                case "CheckStatus":
                case "B.CODE_DESC":
                    sColumnName = "B.CODE_DESC";
                    break;
                case "Name":
                case " ISNULL(FUNDS.FIRST_NAME + FUNDS.LAST_NAME,FUNDS.LAST_NAME)":
                    //mkaran - MITS 35712 - Start
                    sColumnName = " ISNULL(FUNDS.FIRST_NAME + FUNDS.LAST_NAME,FUNDS.LAST_NAME)";
                    if (sDBType == Constants.DB_ORACLE)
                        sColumnName = "nvl(CONCAT(FIRST_NAME,LAST_NAME),LAST_NAME)";
                    //mkaran - MITS 35712 - End
                    break;
                case "Amount":
                case "FUNDS.AMOUNT":
                    sColumnName = "FUNDS.AMOUNT";
                    break;
                case "FromDate":
                case "FUNDS_TRANS_SPLIT.FROM_DATE":
                    sColumnName = "FUNDS_TRANS_SPLIT.FROM_DATE";
                    break;
                case "ToDate":
                case "FUNDS_TRANS_SPLIT.TO_DATE":

                    sColumnName = "FUNDS_TRANS_SPLIT.TO_DATE";
                    break;
                case "InvoiceNumber":
                case "FUNDS_TRANS_SPLIT.INVOICE_NUMBER":
                    sColumnName = "FUNDS_TRANS_SPLIT.INVOICE_NUMBER";
                    break;
                case "CodeDesc":
                case "A.CODE_DESC":
                    //sColumnName = "A.CODE_DESC"; 
                    //mkaran - MITS 35712 - Start
                    sColumnName = "ISNULL(A.SHORT_CODE, A.CODE_DESC)";
                    if (sDBType == Constants.DB_ORACLE)
                        sColumnName = "nvl(CONCAT(A.SHORT_CODE,A.CODE_DESC),A.CODE_DESC)";
                    //mkaran - MITS 35712 - End
                    break;
                case "SplitAmount":
                case "FUNDS_TRANS_SPLIT.AMOUNT":
                    sColumnName = "FUNDS_TRANS_SPLIT.AMOUNT";
                    break;
                case "User":
                case "FUNDS.ADDED_BY_USER":
                    sColumnName = "FUNDS.ADDED_BY_USER";
                    break;
                case "CheckDate":
                case "FUNDS.DATE_OF_CHECK":
                    sColumnName = "FUNDS.DATE_OF_CHECK";
                    break;
                case "FUNDS.COMBINED_PAY_FLAG":
                case "CombinedPay":
                    sColumnName = "FUNDS.COMBINED_PAY_FLAG";
                    break;
                //Ashish Ahuja - rma- 523
                case "IsFirstFinal":
                case "FUNDS_TRANS_SPLIT.IS_FIRST_FINAL":
                    sColumnName = "FUNDS_TRANS_SPLIT.IS_FIRST_FINAL";
                    break;
                case "PolicyName":
                case "POLICY_NAME":
                    sColumnName = "POLICY_NAME";
                    break;
                case "CoverageType":
                case "POL.CODE_DESC":
                    sColumnName = "POL.CODE_DESC";
                    break;
                case "Unit":
                case "POLICY_X_UNIT.UNIT_ID":
                    sColumnName = "POLICY_X_UNIT.UNIT_ID";
                    break;
                case "LossType":
                case "COVERAGE_X_LOSS.LOSS_CODE":
                    //Prashant MITS 35098 starts
                    // sColumnName = "COVERAGE_X_LOSS.LOSS_CODE";
                    sColumnName = "LOSSCODE.SHORT_CODE";
                    //Prashant MITS 35098 ends
                    break;
                case "ManualCheck":
                case "FUNDS.MANUAL_CHECk":
                    sColumnName = "FUNDS.MANUAL_CHECk";
                    break;
                //Prashant MITS 35247 starts
                case "Offset":
                case "FUNDS.OFFSET_FLAG":
                    sColumnName = "FUNDS.OFFSET_FLAG";
                    break;
                //Prashant MITS 35247 ends
                // psharma206 sorting in invoice amount column 
                case "InvoiceAmount":
                case "FUNDS_TRANS_SPLIT.INVOICE_AMOUNT":
                    sColumnName = "FUNDS_TRANS_SPLIT.INVOICE_AMOUNT";
                    break;
                //Ashish Ahuja Mits 34270 start
                case "CheckMemo":
                case "FUNDS.CHECK_MEMO":
                    sColumnName = "FUNDS.CHECK_MEMO";
                    break;
                //Ashish Ahuja Mits 34270 end
                case "ReserveType":
                case "RESERVE_TYPE.CODE_DESC":
                    sColumnName = "RESERVE_TYPE.CODE_DESC";
                    break;
                default: sColumnName = "FUNDS.TRANS_DATE DESC, FUNDS.TRANS_ID";
                    break;
                //mbahl3 mits 28455
            }
            //mkaran2 - MITS 35712 - commented ; it executes for every column having ISNULL
            //if (sDBType == Constants.DB_ORACLE)
            //{
            //    sColumnName = sColumnName.Replace("mytable.", "");
            //    sColumnName = sColumnName.Replace("ISNULL", "nvl");
            //    if (sColumnName.IndexOf("nvl") != -1)
            //        sColumnName = "nvl(CONCAT(FIRST_NAME,LAST_NAME),LAST_NAME)";
            //}

            //mkaran2 - MITS 35712 - Start
            List<string> lAmountFields = new List<string>();  //issue RMA324 psharma206
            lAmountFields.Add("FUNDS.AMOUNT");
            lAmountFields.Add("FUNDS_TRANS_SPLIT.AMOUNT");
            lAmountFields.Add("FUNDS_TRANS_SPLIT.INVOICE_AMOUNT");
            // akaushik5 Added for MITS 36343 Starts
            lAmountFields.Add("FUNDS.TRANS_ID");
            // akaushik5 Added for MITS 36343 Ends

            if (sDBType == Constants.DB_ORACLE && !lAmountFields.Contains(sColumnName))
            {
                foreach (string s in lAmountFields)
                {
                    sColumnName = sColumnName.Replace(s, "upper(" + s + ")");
                }
            }
            //mkaran2 - MITS 35712 - End
            return sColumnName;
        }
        //START - Added by Nikhil for NI.
        /// <summary>
        /// Check for decuctible processing eligiblity based on Coverage type and reserve type.
        /// </summary>
        /// <param name="p_iReserveTypeCode">Reserve type code</param>
        /// <param name="p_iClaimId">Claim id</param>
        /// <param name="p_iCovTypeCode">coverage type code</param>
        /// <param name="p_iPolicyUnitRowId">policy unit id</param>
        /// <param name="p_sCovgSeqNum">coverage seq number</param>
        /// <param name="p_sTransSeqNum">transaction seq number</param>
        /// <param name="p_sCoveragekey">coverage key</param>
        // <returns>Boolean flag</returns>	
        public bool IsEligibleForDeductibleProcessing(int p_iReserveTypeCode, int p_iClaimId, int p_iCovTypeCode, long p_iPolicyUnitRowId, string p_sCovgSeqNum, string p_sTransSeqNum, string p_sCoveragekey, out string p_sDedTypeCode)
        {
            bool bIsEligibleForDedProcessing = false;
            StringBuilder sbSQL = null;
            bool success = false;
            int iDedTypeCode = 0;
            LocalCache objLocalCache = null;
            p_sDedTypeCode = string.Empty;
            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT CXPD.DED_TYPE_CODE FROM POLICY_X_CVG_TYPE PXCT LEFT JOIN CLAIM_X_POL_DED CXPD ");
                sbSQL.Append(" ON PXCT.POLCVG_ROW_ID =  CXPD.POLCVG_ROW_ID");
                sbSQL.AppendFormat(" WHERE PXCT.POLICY_UNIT_ROW_ID = {0}", p_iPolicyUnitRowId);
                sbSQL.AppendFormat(" AND PXCT.COVERAGE_TYPE_CODE = {0}", p_iCovTypeCode);
                sbSQL.AppendFormat(" AND PXCT.CVG_SEQUENCE_NO = '{0}'", p_sCovgSeqNum);
                sbSQL.AppendFormat(" AND PXCT.TRANS_SEQ_NO = '{0}'", p_sTransSeqNum);
                sbSQL.AppendFormat("  AND CXPD.CLAIM_ID =  {0}", p_iClaimId);

                if (string.IsNullOrEmpty(p_sCoveragekey))
                    sbSQL.Append("  AND PXCT.COVERAGE_KEY IS NULL");
                else
                    sbSQL.AppendFormat("  AND PXCT.COVERAGE_KEY ='{0}'", p_sCoveragekey);
                iDedTypeCode = Conversion.CastToType<int>(m_objDataModelFactory.Context.DbConnLookup.ExecuteInt(sbSQL.ToString()).ToString(), out success);

                p_sDedTypeCode = iDedTypeCode.ToString();

                // npadhy JIRA 12884 If the Deductible Type is None we do not process it.
                if (iDedTypeCode > 0 && iDedTypeCode != objLocalCache.GetCodeId("None", "DEDUCTIBLE_TYPE") && (iDedTypeCode == objLocalCache.GetCodeId("TP", "DEDUCTIBLE_TYPE") || iDedTypeCode == objLocalCache.GetCodeId("SIR", "DEDUCTIBLE_TYPE")) && objLocalCache.GetRelatedCodeId(p_iReserveTypeCode) == objLocalCache.GetCodeId("R", "MASTER_RESERVE"))
                {
                    bIsEligibleForDedProcessing = true;

                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
            return bIsEligibleForDedProcessing;
        }
        //END -  by Nikhil for NI.
        //avipinsrivas start : Worked for JIRA - 11949
        public static string GetFilteredAmount(string sFilterExp)
        {
            //avipinsrivas start : Worked for JIRA - 11949
            string s_Subfilter = string.Empty;
            string s_Exp = string.Empty;
            string s_AmountString = string.Empty;
            if (sFilterExp.IndexOf("[Amount]") > 0 || sFilterExp.IndexOf("[InvoiceAmount]") > 0 || sFilterExp.IndexOf("[SplitAmount]") > 0 || sFilterExp.IndexOf("[CheckTotal]") > 0)
            {
                string sIndexOfString = string.Empty;
                if (sFilterExp.IndexOf("[Amount]") > 0)
                    sIndexOfString = "[Amount]";
                else if (sFilterExp.IndexOf("[InvoiceAmount]") > 0)
                    sIndexOfString = "[InvoiceAmount]";
                else if (sFilterExp.IndexOf("[SplitAmount]") > 0)
                    sIndexOfString = "[SplitAmount]";
                else if (sFilterExp.IndexOf("[CheckTotal]") > 0)
                    sIndexOfString = "[CheckTotal]";

                s_Subfilter = sFilterExp.Substring(sFilterExp.IndexOf(sIndexOfString));
                s_Exp = s_Subfilter.Substring((s_Subfilter.IndexOf("'")) + 1);
                s_AmountString = s_Exp.Substring(0, s_Exp.IndexOf("'"));
                sFilterExp = sFilterExp.Replace(s_AmountString, s_AmountString.Replace("$", "").Replace("£", "").Replace("(", "").Replace(")", ""));
            }
            else
                sFilterExp = sFilterExp.Replace("$", "").Replace("£", "").Replace("(", "").Replace(")", "");
            //avipinsrivas end

            return sFilterExp.Trim();
        }
        //avipinsrivas end

        //start 7810
        public ArrayList ValidateLimits(int iTransId)
        {

            bool bFound = false;
            long lAmount = 0;
            int iLOBCode = 0;
            long lMaxAmount = 0;
            string sSQL = string.Empty;
            LobSettings objLobSettings = null;
            ColLobSettings objColLobSettings = null;
            CCacheFunctions objCCacheFunctions = null;
            Funds objFunds = null;
            int iGroupID = 0;
            int iIndex = 0;
            ArrayList arrlstReserveAmount = null;
            ReserveAmount structReserveAmount;
            double dblTransTotal = 0.0;
            int iReserveTypeCode = 0;
            double dblAmount = 0.0;
            double dblTempAmount = 0.0;
            double dblBalance = 0.0;
            string strError = "";
            ArrayList arrlstLimitDetail = null;
           
            
            try
            {

                ReadCheckOptions();
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                arrlstLimitDetail = new ArrayList();
                objFunds = this.m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                objFunds.MoveTo(iTransId);
                objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
                objLobSettings = objColLobSettings[objFunds.LineOfBusCode];
                lAmount = Conversion.ConvertObjToInt64(objFunds.Amount, m_iClientId);
                iLOBCode = objFunds.LineOfBusCode;
                m_objFunds = objFunds;

                sSQL = "SELECT MAX_AMOUNT FROM FUNDS_LIMITS WHERE ((USER_ID = " + m_iUserId + " AND GROUP_ID = 0) OR (GROUP_ID = " + m_iUserGroupId + " AND USER_ID = 0)) AND LINE_OF_BUS_CODE = " + iLOBCode;
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("MAX_AMOUNT"), m_iClientId);
                        if (lMaxAmount < lAmount)
                        {
                            if (objFunds.LineOfBusCode != 0)
                            {
                                if (objLobSettings.PayLmtFlag)
                                {
                                    arrlstLimitDetail.Add("ExcPayAuth");
                                }
                            }
                            
                        }
                        
                    }
                    objReader.Close();
                }

                sSQL = "SELECT MAX_AMOUNT FROM FUNDS_DET_LIMITS WHERE ((USER_ID = " + m_iUserId + " AND GROUP_ID = 0) OR (GROUP_ID = " + m_iUserGroupId + " AND USER_ID = 0)) AND LINE_OF_BUS_CODE = " + iLOBCode;
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("MAX_AMOUNT"), m_iClientId);
                        if (lMaxAmount < lAmount)
                        {
                            if (objFunds.LineOfBusCode != 0)
                            {
                                if (objLobSettings.PayDetLmtFlag)
                                {
                                    arrlstLimitDetail.Add("ExcPayDetailAuth");
                                }
                            }
                           
                        }
                       
                    }
                    objReader.Close();
                }
                arrlstReserveAmount = new ArrayList();
                if ((objLobSettings != null && objLobSettings.PayDetLmtFlag) || (objLobSettings != null && objLobSettings.InsufFundsFlag))
                {
                    foreach (FundsTransSplit objFundsTransSplit in m_objFunds.TransSplitList)
                    {
                        if (this.IsSplitItemDeleted(objFundsTransSplit.SplitRowId))
                            continue;
                        bFound = false;
                        for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                        {
                            structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];
                            if (structReserveAmount.ReserveTypeCode == objFundsTransSplit.ReserveTypeCode)
                            {
                                //Raman Bhatia..modifying amount below was not updating the arraylist
                                //structReserveAmount.Amount += objFundsTransSplit.Amount ;
                                double dTempAmount = structReserveAmount.Amount + objFundsTransSplit.Amount;

                                arrlstReserveAmount.RemoveAt(iIndex);
                                structReserveAmount = new ReserveAmount();
                                structReserveAmount.ReserveTypeCode = objFundsTransSplit.ReserveTypeCode;
                                structReserveAmount.Amount = dTempAmount;
                                arrlstReserveAmount.Insert(iIndex, structReserveAmount);
                                bFound = true;
                            }
                        }
                        if (!bFound)
                        {
                            structReserveAmount = new ReserveAmount();
                            structReserveAmount.ReserveTypeCode = objFundsTransSplit.ReserveTypeCode;
                            structReserveAmount.Amount = objFundsTransSplit.Amount;
                            arrlstReserveAmount.Add(structReserveAmount);
                        }
                        dblTransTotal += objFundsTransSplit.Amount;
                    }
                }

                if ((objLobSettings != null && objLobSettings.PerClaimLmtFlag) && arrlstReserveAmount.Count > 0)
                {
                    for (iIndex = 0; iIndex < arrlstReserveAmount.Count; iIndex++)
                    {
                        structReserveAmount = (ReserveAmount)arrlstReserveAmount[iIndex];

                        if (GetMaxPerClaimReserveAmount(m_objFunds.LineOfBusCode, structReserveAmount.ReserveTypeCode, GetClaimType(m_objFunds.ClaimId), ref dblTempAmount, ref iReserveTypeCode))
                        {
                            dblBalance = GetPaidTotal(m_objFunds.ClaimId, structReserveAmount.ReserveTypeCode, m_objFunds.ClaimantEid, m_objFunds.UnitId);
                            //start 7810
                            for (int iTempIndex = 0; iTempIndex < arrlstReserveAmount.Count; iTempIndex++)
                            {
                                ReserveAmount structTempReserveAmount;
                                structTempReserveAmount = (ReserveAmount)arrlstReserveAmount[iTempIndex];
                                if (iReserveTypeCode == structTempReserveAmount.ReserveTypeCode || iReserveTypeCode == 0)
                                {
                                    dblAmount = dblAmount + structTempReserveAmount.Amount;
                                }
                            }
                            //End 7810
                            //dblAmount = dblAmount + structReserveAmount.Amount;
                            if (((dblAmount + dblBalance) > dblTempAmount) && (objCCacheFunctions.GetShortCode(m_objFunds.StatusCode) != "RD")
                                && (objCCacheFunctions.GetShortCode(m_objFunds.StatusCode) != "Q")
                                )
                            {
                                if (objLobSettings.PerClaimLmtFlag)
                                {
                                    if (iReserveTypeCode != 0)
                                    {
                                        string sCode = "";
                                        string sCodeDesc = "";
                                        strError = "You do not have authority to pay more than %%1 per claim for reserve type %%2.";
                                        strError = strError.Replace("%%1", string.Format("{0:C}", dblTempAmount));
                                        objCCacheFunctions.GetCodeInfo(structReserveAmount.ReserveTypeCode, ref sCode, ref sCodeDesc);
                                        strError = strError.Replace("%%2", sCodeDesc);
                                        m_objFunds.IsExcPayClmLmt = true;
                                        arrlstLimitDetail.Add("ExcPayClmLmt");
                                        m_arrlstErrors.Add(strError);
                                        //return (false);
                                    }
                                    else
                                    {
                                        strError = "You do not have authority to pay more than %%1 per claim.";
                                        strError = strError.Replace("%%1", string.Format("{0:C}", dblTempAmount));
                                        m_arrlstErrors.Add(strError);
                                        m_objFunds.IsExcPayClmLmt = true;
                                        arrlstLimitDetail.Add("ExcPayClmLmt");
                                        // return (false);
                                    }
                                }
                            }
                        }
                    }
                }
             

            }
            catch (Exception ex)
            {
            }
            return arrlstLimitDetail;
        }
		private int GetApprovalIDAgainstExceedPay(int p_iAppoverID, double lAmount, int p_iLOBCode)
        {

            string sSQL = string.Empty;
            int iThisManagerID = 0;
            int iApproverID = 0;
            long lMaxAmount = 0;
            bool bValidApprover = true;

            try
            {

                iThisManagerID = p_iAppoverID;
               
                    iThisManagerID = GetManagerID(iThisManagerID);

                    if (iThisManagerID == 0)
                    {
                        sSQL = "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = " + p_iLOBCode;
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                        {
                            while (objReader.Read())
                            {
                                //smahajan6: Safeway: Payment Supervisory Approval : Start
                                if (m_bPmtNotfyImmSup)
                                {
                                    if (m_iUserId != Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId))
                                    {
                                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                        return iApproverID;
                                    }
                                }
                                else
                                {
                                    lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("PAYMENT_MAX"), m_iClientId);
                                    if ((lMaxAmount > lAmount))
                                    {
                                        iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                        return iApproverID;
                                    }
                                }

                                //Commeted Code - Payment Max should be checked
                                //lMaxAmount = Conversion.ConvertObjToInt64(objReader.GetValue("RESERVE_MAX"));
                                //if ((lMaxAmount > lAmount))
                                //{
                                //    iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                                //    return iApproverID;
                                //}

                                ///smahajan6: Safeway: Payment Supervisory Approval : End
                            }
                        }
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT * FROM LOB_SUP_APPROVAL WHERE LOB_CODE = -1"))
                        {
                            if (objReader.Read())
                            {
                                iApproverID = Conversion.ConvertObjToInt(objReader.GetValue("USER_ID"), m_iClientId);
                            }
                        }
                        return iApproverID;
                    }

                    //smahajan6: Safeway: Payment Supervisory Approval : Start
                    if (m_bPmtNotfyImmSup)
                    {
                        iApproverID = iThisManagerID;
                        return iApproverID;
                    }
                 
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("FundManager.GetApprovalIDAgainstExceedPay.Error", m_iClientId), p_objException);
            }

            return iApproverID;
        }
        //end 7810
        private int GetDeductiblePerEventFlag(int p_iCovGroupId)
        {
            int iPreventEditDedPerEvent = 0;
            if (p_iCovGroupId > 0)
                iPreventEditDedPerEvent = this.m_objFunds.Context.DbConnLookup.ExecuteInt("SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + p_iCovGroupId);

            return iPreventEditDedPerEvent;
        }
		
		public double GetDeductibleInfo(int p_iPolCvgId,int p_iClaimId, out double p_dSirDedAmt, out int p_iDedTypeCode, out bool p_bExcludeExpenseFlag,
           out int p_iPolicyId, out int p_iClaimantEid, out int p_iCovGroupId, bool p_bUseOriginalDedAmt, bool p_bSkipAggregateCalc)
        {
            p_dSirDedAmt = 0d;
            p_iDedTypeCode = 0;
            p_bExcludeExpenseFlag = false;
            p_iPolicyId = 0;
            p_iClaimantEid = 0;
            p_iCovGroupId = 0;
            bool bSuccess = false;
            int iClmXPolDedId = 0;
            //NI-PCR Changes by Nikhil -  Start
            //int iPreventEditDedPerEvent = 0; 
            //NI-PCR Changes by Nikhil -  End
            ClaimXPolDed objClaimXPolDed = null;

            DbReader objDbReader = null;
            // string sSql = string.Empty;
            StringBuilder sbSQL = null;
            int iDimTypeCode = 0;
            string sClaimDate = string.Empty;
            double dDimPercent = 0d;
            double dRemainingAmt = 0d;
            int iFPTypeCode = 0;
            int iEventId = 0;
            int iExcludeExpense = 0;
            sbSQL = new StringBuilder();
            sbSQL.Append("SELECT DISTINCT CPD.EVENT_ID,CPD.CLM_X_POL_DED_ID, CPD.POLICY_ID, CPD.SIR_DED_AMT, CPD.DED_TYPE_CODE, CPD.EXCLUDE_EXPENSE_FLAG, CPD.CLAIMANT_EID, ");
            sbSQL.Append(" C.DATE_OF_CLAIM, CPD.DIMNSHNG_TYPE_CODE, CPD.COV_EVALUATION_DATE, CPD.DIM_PERCENT_NUM, PXG.COV_GROUP_CODE ");
            sbSQL.Append(" FROM CLAIM_X_POL_DED CPD ");
            sbSQL.Append(" INNER JOIN CLAIM C ON CPD.CLAIM_ID = C.CLAIM_ID ");
            sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXG ON CPD.POLICY_X_INSLINE_GROUP_ID = PXG.POLICY_X_INSLINE_GROUP_ID ");
            sbSQL.Append(" WHERE CPD.POLCVG_ROW_ID=").Append(p_iPolCvgId);
            sbSQL.Append(" AND CPD.CLAIM_ID=").Append(p_iClaimId);
            //using (objDbReader = this.Context.DbConnLookup.ExecuteReader(Convert.ToString(sbSQL)))
            using (objDbReader = DbFactory.ExecuteReader(this.m_objFunds.Context.DbConnLookup.ConnectionString, Convert.ToString(sbSQL)))
            {
                if (objDbReader.Read())
                {
                    p_bExcludeExpenseFlag = Conversion.CastToType<int>(objDbReader["EXCLUDE_EXPENSE_FLAG"].ToString(), out bSuccess) == -1;
                    p_iDedTypeCode = Conversion.CastToType<int>(objDbReader["DED_TYPE_CODE"].ToString(), out bSuccess);
                    p_dSirDedAmt = Conversion.CastToType<double>(objDbReader["SIR_DED_AMT"].ToString(), out bSuccess);
                    p_iPolicyId = Conversion.CastToType<int>(objDbReader["POLICY_ID"].ToString(), out bSuccess);
                    p_iClaimantEid = Conversion.CastToType<int>(objDbReader["CLAIMANT_EID"].ToString(), out bSuccess);
                    p_iCovGroupId = Conversion.CastToType<int>(objDbReader["COV_GROUP_CODE"].ToString(), out bSuccess);
                    iDimTypeCode = Conversion.CastToType<int>(objDbReader["DIMNSHNG_TYPE_CODE"].ToString(), out bSuccess);
                    sClaimDate = objDbReader["DATE_OF_CLAIM"].ToString();
                    dDimPercent = Conversion.CastToType<double>(objDbReader["DIM_PERCENT_NUM"].ToString(), out bSuccess);
                    iClmXPolDedId = Conversion.CastToType<int>(objDbReader["CLM_X_POL_DED_ID"].ToString(), out bSuccess);
                    iEventId = Conversion.CastToType<int>(objDbReader["EVENT_ID"].ToString(), out bSuccess);
                }
            }
            //Start: Added by Nitin Goel, Coverage Group should return zero if deductible type is first party,06/11/2014
            if (p_iDedTypeCode == this.m_objFunds.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE"))
            {
                p_iCovGroupId = 0;
            }
            //end: added by Nitin Goel
            //start:Added By Nitin goel,
            if (p_iCovGroupId > 0 && GetDeductiblePerEventFlag(p_iCovGroupId) == -1)
            {
                sbSQL.Length = 0;
                sbSQL.Append(" SELECT CXD.EXCLUDE_EXPENSE_FLAG FROM CLAIM_X_POL_DED CXD ");
                sbSQL.Append(" INNER JOIN POLICY_X_INSLINE_GROUP PXIG ON CXD.POLICY_X_INSLINE_GROUP_ID = PXIG.POLICY_X_INSLINE_GROUP_ID ");
                sbSQL.Append(" WHERE CXD.EVENT_ID=").Append(iEventId);
                sbSQL.Append(" AND PXIG.COV_GROUP_CODE=").Append(p_iCovGroupId);
                sbSQL.Append(" ORDER BY CXD.EXCLUDE_EXPENSE_FLAG ASC ");
                iExcludeExpense = this.m_objFunds.Context.DbConnLookup.ExecuteInt(sbSQL.ToString());
                if (iExcludeExpense == -1)
                {
                    p_bExcludeExpenseFlag = true;
                }
            }
            //End:added by Nitin goel
            objClaimXPolDed = this.m_objFunds.Context.Factory.GetDataModelObject("ClaimXPolDed", false) as ClaimXPolDed;
            objClaimXPolDed.MoveTo(iClmXPolDedId);

            iFPTypeCode = objClaimXPolDed.Context.LocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");

            if (!p_bUseOriginalDedAmt)
            {
                p_dSirDedAmt = objClaimXPolDed.CalculateReducedDeductibleAmount(p_iClaimId, iDimTypeCode, p_iDedTypeCode, p_dSirDedAmt, dDimPercent, iClmXPolDedId);
            }

            //Different method when coverage group is to be applied
            if (p_iCovGroupId == 0 || objClaimXPolDed.DedTypeCode == 0 || objClaimXPolDed.DedTypeCode == iFPTypeCode)
            {
                dRemainingAmt = objClaimXPolDed.CalculateRemainingAmount(p_dSirDedAmt, p_iClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode);
            }
            else
            {
                //NI-PCR changes by Nikhil for handling deductable check at event level - Start
                //Changes by Nikhil 12/26/2013 -  Start
                // dRemainingAmt = objClaimXPolDed.CalculateRemainingAmount(p_dSirDedAmt, p_iClaimId, p_iCovGroupId, p_bSkipAggregateCalc);
                //   iPreventEditDedPerEvent = DbFactory.ExecuteAsType<int>(this.Context.DbConn.ConnectionString, "SELECT USE_SHARED_DED_FLAG FROM COVERAGE_GROUP WHERE COVERAGE_GROUP_ID= " + p_iCovGroupId);
                if (GetDeductiblePerEventFlag(p_iCovGroupId) != -1)
                    //Changes by Nikhil 12/26/2013 -  End
                    dRemainingAmt = objClaimXPolDed.CalculateRemainingAmount(p_dSirDedAmt, p_iClaimId, objClaimXPolDed.PolcvgRowId, objClaimXPolDed.DedTypeCode, p_iCovGroupId, p_bSkipAggregateCalc);
                else
                    dRemainingAmt = objClaimXPolDed.CalculateRemainingAmount(p_dSirDedAmt, p_iClaimId, p_iCovGroupId, p_bSkipAggregateCalc);
                //NI-PCR changes by Nikhil for handling deductable check at event level - End
            }

            return dRemainingAmt;
        }
    }
}
