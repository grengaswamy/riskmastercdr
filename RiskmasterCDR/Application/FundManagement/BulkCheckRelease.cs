﻿using System;
using System.Xml;
using System.IO;
using System.Data;
using System.Collections;
using Riskmaster.Settings;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.Application.Reserves;   
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Riskmaster.Application.FundManagement
{
    /// <summary>
    /// Author: Animesh Sahai
    /// Date: 22-Jan-2010 
    /// BulkCheckRelease class, designed to process the bulk check related functionality. 
    /// </summary>
    public class BulkCheckRelease
    {
        #region Variable Declaration
        
        private string m_sDSN = string.Empty;
        private string m_sUserName = string.Empty;
        private string m_sUserPass = string.Empty;
        private DataModelFactory m_objDMF = null;
        private int m_iClientId = 0;//rkaur27
        
        #endregion
        
        #region Constructor
		/// <summary>
        /// BulkCheckRelease class constructor
        /// </summary>
        /// <param name="p_sDSN">Connection string DSN in which user is logged in</param>
        /// <param name="p_sUser">userid of the logged in user</param>
        /// <param name="p_sPassword">password of the logged in user</param> 
		public BulkCheckRelease(string p_sDSN , string p_sUser , string p_sPassword, int p_iClientId )
		{
			m_sDSN=p_sDSN;
			m_sUserName = p_sUser ;
			m_sUserPass = p_sPassword ;
			m_objDMF = new DataModelFactory( p_sDSN , p_sUser , p_sPassword, p_iClientId );
            m_iClientId = p_iClientId;//rkaur27
		}
        ~BulkCheckRelease()
		{
			if( m_objDMF != null )
				this.Dispose();
		}

		public void Dispose()
		{
			try
			{
				m_objDMF.UnInitialize();
                m_objDMF.Dispose();				
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}
        #endregion

        #region Methods
        
        /// <summary>
        /// Returns the list of Queued payment based on the specified filter conditions
        /// </summary>
        /// <param name="p_objDocument">Input XML Document</param>
        /// <returns>XML document that contains the list of all queued payment based on search result</returns>
        /// Start ddhiman 12/14/2010 
        public XmlDocument GetPayments(XmlDocument p_objDocument)
        {
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sFromDateStart = string.Empty;
            string sFromDateEnd = string.Empty;
            string sToDateStart = string.Empty;
            string sToDateEnd = string.Empty;
            string sInvoiceFromDate = string.Empty;
            string sInvoiceToDate = string.Empty;
            string sInvoiceNumber = string.Empty;
            int iReserveType = 0;
            int iProviderID = 0;
            int iClientID = 0;
            int iClaimNumber = 0;
            string sTransType = string.Empty;
            string objNodeExpr = string.Empty;
            string objNodeDir = string.Empty;
            string sAmountCond = string.Empty;
            string sAmount = string.Empty;
            string sAccountName = string.Empty;
            string sClaimNum = string.Empty;
            string sDate = string.Empty;
            string sClaimId = string.Empty;
            DataSet objDS = null;
            XmlNode objNode = null;
            StringBuilder sbSQL = null;
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemTemp = null;
            sbSQL = new StringBuilder();
            string strClientIds = string.Empty; 
            int iCurrentTransID=0;
            int iPrevTransID = 0;
            int iEntityID=0;
            string[] arrClient = null;
            DataTable objDT = null;
            DataView objDV = null;
            DataRow objRowTemp = null;
            try
            {
                //Transaction From Date
                objNode = p_objDocument.SelectSingleNode("//control[@name='FromDate']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sFromDate = Conversion.GetDate(objNode.InnerText);
                    }
                }
                //Transation To Date
                objNode = p_objDocument.SelectSingleNode("//control[@name='ToDate']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sToDate = Conversion.GetDate(objNode.InnerText);
                    }
                }
                //Provider Entity ID
                objNode = p_objDocument.SelectSingleNode("//control[@name='ProviderEID']");
                if (objNode != null)
                {
                    iProviderID = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].Value);
                }
                //Client Entity ID
                objNode = p_objDocument.SelectSingleNode("//control[@name='ClientEID']");
                if (objNode != null)
                {
                    iClientID = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].Value);
                }
                //Claim Number
                objNode = p_objDocument.SelectSingleNode("//control[@name='dClaimNumber']");
                if (objNode != null)
                {
                    iClaimNumber = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].Value);
                }
                //Transation Type List
                objNode = p_objDocument.SelectSingleNode("//control[@name='lstTransTypes']");
                if (objNode != null)
                {
                    sTransType = objNode.Attributes["codeid"].Value;
                    sTransType = sTransType.Trim();
                    sTransType = sTransType.Replace(" ", ",");   
                }
                //Amount Condition
                objNode = p_objDocument.SelectSingleNode("//control[@name='ddlAmountCond']");
                if (objNode != null)
                {
                    sAmountCond = p_objDocument.SelectSingleNode("//control[@name='ddlAmountCond']").InnerText;
                }
                //Amount
                objNode = p_objDocument.SelectSingleNode("//control[@name='txtAmount']");
                if (objNode != null)
                {
                    sAmount = p_objDocument.SelectSingleNode("//control[@name='txtAmount']").InnerText;
                }
                //Account Name
                objNode = p_objDocument.SelectSingleNode("//control[@name='AccountName']");
                if (objNode != null)
                {
                    sAccountName = p_objDocument.SelectSingleNode("//control[@name='AccountName']").InnerText;
                }
                //Reserve Type
                objNode = p_objDocument.SelectSingleNode("//control[@name='ReserveTypeID']");
                if (objNode != null)
                {
                    iReserveType = Conversion.ConvertStrToInteger(objNode.Attributes["codeid"].Value);
                }
                //From Date Start
                objNode = p_objDocument.SelectSingleNode("//control[@name='FromDateStart']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sFromDateStart = Conversion.GetDate(objNode.InnerText);
                    }
                }
                //From Date End
                objNode = p_objDocument.SelectSingleNode("//control[@name='FromDateEnd']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sFromDateEnd = Conversion.GetDate(objNode.InnerText);
                    }
                }
                //To Date Start
                objNode = p_objDocument.SelectSingleNode("//control[@name='ToDateStart']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sToDateStart = Conversion.GetDate(objNode.InnerText);
                    }
                }
                //To Date End
                objNode = p_objDocument.SelectSingleNode("//control[@name='ToDateEnd']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sToDateEnd = Conversion.GetDate(objNode.InnerText);
                    }
                }
                //Invoice Number
                objNode = p_objDocument.SelectSingleNode("//control[@name='InvoiceNumber']");
                if (objNode != null)
                {
                    sInvoiceNumber = p_objDocument.SelectSingleNode("//control[@name='InvoiceNumber']").InnerText;
                }
                //Invoice From Date
                objNode = p_objDocument.SelectSingleNode("//control[@name='InvoiceFromDate']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sInvoiceFromDate = Conversion.GetDate(objNode.InnerText);
                    }
                }
                //Invoice To Date
                objNode = p_objDocument.SelectSingleNode("//control[@name='InvoiceToDate']");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            sInvoiceToDate = Conversion.GetDate(objNode.InnerText);
                    }
                }

                //Creating the main query
                sbSQL.Append("SELECT F.TRANS_ID,F.TRANS_DATE,F.LAST_NAME,F.FIRST_NAME,C.CLAIM_NUMBER,");
         if(  m_objDMF.Context.InternalSettings.SysSettings.UseMultiCurrency==0)

                sbSQL.Append(" F.AMOUNT, ");
         else
             sbSQL.Append(" F.CLAIM_CURRENCY_AMOUNT AMOUNT, ");  


                sbSQL.Append("   FTS.TRANS_TYPE_CODE,FTS.SPLIT_ROW_ID,ACC.ACCOUNT_NAME,");
                sbSQL.Append(" E.DEPT_EID, FTS.RESERVE_TYPE_CODE, C.CLAIM_ID, FTS.FROM_DATE, FTS.TO_DATE,");
                sbSQL.Append(" FTS.INVOICE_NUMBER,FTS.INVOICE_DATE");
                //Query From Condition
                sbSQL.Append(" FROM FUNDS F,FUNDS_TRANS_SPLIT FTS,ACCOUNT ACC,CLAIM C,EVENT E");
                //Query Where Conditions 
                sbSQL.Append(" WHERE F.STATUS_CODE = ");
                sbSQL.Append(m_objDMF.Context.LocalCache.GetCodeId("Q", "CHECK_STATUS"));
                sbSQL.Append(" AND F.PAYMENT_FLAG <> 0");
                sbSQL.Append(" AND F.TRANS_ID=FTS.TRANS_ID AND F.ACCOUNT_ID=ACC.ACCOUNT_ID");
                sbSQL.Append(" AND F.CLAIM_ID=C.CLAIM_ID AND C.EVENT_ID =E.EVENT_ID");
                //*******************************

                //Applying the filter conditions 
                //*******************************
                if (!sFromDate.Equals(""))
                {
                    sbSQL.Append(" AND F.TRANS_DATE >= '" + sFromDate + "' ");
                }
                if (!sToDate.Equals(""))
                {
                    sbSQL.Append(" AND F.TRANS_DATE <= '" + sToDate + "' ");
                }
                if (iProviderID != 0)
                {
                    sbSQL.Append(" AND F.PAYEE_EID= " + iProviderID);
                }
                if (iClaimNumber != 0)
                {
                    sbSQL.Append(" AND C.CLAIM_ID= " + iClaimNumber);
                }
                if (sTransType != "")
                {
                    sbSQL.Append(" AND FTS.TRANS_TYPE_CODE IN " + "(" + sTransType + ")");
                }
                if (! String.IsNullOrEmpty(sAmount))
                {
                    sAmount = ((sAmount.Replace("$", "")).Replace(",","").Trim());
                    //MGaba2:MITS 28752 
                    if (m_objDMF.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)

                        sbSQL.Append("  AND F.AMOUNT");
                    else
                        sbSQL.Append("  AND F.CLAIM_CURRENCY_AMOUNT");
                    //MGaba2:MITS 28752

                    if (sAmountCond == "0")
                    {
                        //MGaba2:MITS 28752 
                        //sbSQL.Append(" AND AMOUNT= " + sAmount);
                        sbSQL.Append(" = " + sAmount);
                    }
                    if (sAmountCond == "1")
                    {
                        //MGaba2:MITS 28752 
                        //sbSQL.Append(" AND AMOUNT<= " + sAmount);
                        sbSQL.Append(" <= " + sAmount);
                    }
                    if (sAmountCond == "2")
                    {
                        //MGaba2:MITS 28752 
                        //sbSQL.Append(" AND AMOUNT>= " + sAmount);
                        sbSQL.Append(" >= " + sAmount);
                    }
                }
                if (iReserveType != 0)
                {
                    sbSQL.Append(" AND FTS.RESERVE_TYPE_CODE= " + iReserveType);
                }
                if (!String.IsNullOrEmpty(sAccountName))
                {
                    if(sAccountName.Contains("*"))
                    {
                        sAccountName=sAccountName.Replace("*", "%");
                    }
                    sbSQL.Append(" AND ACC.ACCOUNT_NAME LIKE '" + sAccountName + "'");
                }

                if (!sFromDateStart.Equals(""))
                {
                    sbSQL.Append(" AND FTS.FROM_DATE >= '" + sFromDateStart + "' ");
                }
                if (!sFromDateEnd.Equals(""))
                {
                    sbSQL.Append(" AND FTS.FROM_DATE <= '" + sFromDateEnd + "' ");
                }

                if (!sToDateStart.Equals(""))
                {
                    sbSQL.Append(" AND FTS.TO_DATE >= '" + sToDateStart + "' ");
                }
                if (!sToDateEnd.Equals(""))
                {
                    sbSQL.Append(" AND FTS.TO_DATE <= '" + sToDateEnd + "' ");
                }

                if (!sInvoiceFromDate.Equals(""))
                {
                    sbSQL.Append(" AND FTS.INVOICE_DATE >= '" + sInvoiceFromDate + "' ");
                }
                if (!sInvoiceToDate.Equals(""))
                {
                    sbSQL.Append(" AND FTS.INVOICE_DATE <= '" + sInvoiceToDate + "' ");
                }
                if (!String.IsNullOrEmpty(sInvoiceNumber))
                {
                    if(sInvoiceNumber.Contains("*"))
                    {
                        sInvoiceNumber=sInvoiceNumber.Replace("*", "%");
                    }
                    sbSQL.Append(" AND FTS.INVOICE_NUMBER LIKE '" + sInvoiceNumber + "'"); 
                }
                if (iClientID != 0)
                {
                    strClientIds = GetClientIds(iClientID);
                    if (!strClientIds.Equals(""))
                    {
                        //Code to handle the SQL IN Clause Limitation  
                        arrClient = strClientIds.Split(new char[] { ',' });
                        if (arrClient.Length <= 800)
                        {
                            sbSQL.Append(" AND E.DEPT_EID IN (" + strClientIds + ")");
                        }
                        else
                        {
                            sbSQL.Append(" AND (");
                            int iCounter = arrClient.Length / 800;
                            int jMaxValue = arrClient.Length;  
                            int jCurrValue = 0;
                            bool blnSetEndBckt = false;
                            for (int i = 0; i <= iCounter; i++)
                            {
                                if (jMaxValue > 0)
                                {
                                    if (i != 0)
                                        sbSQL.Append(" OR ");
                                    sbSQL.Append(" E.DEPT_EID IN (");
                                    blnSetEndBckt = true; 
                                }
                                jCurrValue = 800;
                                if (jMaxValue <= jCurrValue)
                                    jCurrValue = jMaxValue;
                                for (int j = 0; j < jCurrValue; j++)
                                {
                                    if (j != 0)
                                        sbSQL.Append(",");
                                    sbSQL.Append(arrClient[(i*800)+j]);   
                                }
                                jMaxValue = jMaxValue - jCurrValue;
                                if (blnSetEndBckt)
                                {
                                    sbSQL.Append(")");
                                    blnSetEndBckt = false;  
                                }
                            }
                            sbSQL.Append(")");
                        }
                    }
                }

                //*******************************
                sbSQL.Append(" ORDER BY FTS.TRANS_ID");
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("Root");
                objDOM.AppendChild(objElemParent);
                if (!String.IsNullOrEmpty(sTransType))
                {
                    objElemParent = objDOM.CreateElement("ListItem");
                    objDOM.FirstChild.AppendChild(objElemParent);
                    //******************************
                    string[] objArray = null;
                    objArray = sTransType.Split(new char[] { ',' });
                    foreach (string strTemp in objArray)
                    {
                        objElemTemp = objDOM.CreateElement("Code");
                        objElemTemp.SetAttribute("id", Conversion.ConvertObjToStr(strTemp));
                        objElemTemp.SetAttribute("description", Conversion.ConvertObjToStr(m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(strTemp, m_iClientId)) + " " + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(strTemp, m_iClientId))));
                        objDOM.SelectSingleNode("//ListItem").AppendChild(objElemTemp);
                    }
                    //******************************
                }
                objElemParent = objDOM.CreateElement("bulkcheckgrid");
                objDOM.FirstChild.AppendChild(objElemParent);
                objDS = DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString, sbSQL.ToString(), m_iClientId);
                objDT = new DataTable();
                objRowTemp = null;
                if (objDS.Tables[0] != null)
                {
                    //Defining Columns
                    objDT.Columns.Add("Key",typeof(string));
                    objDT.Columns.Add("id", typeof(string));
                    objDT.Columns.Add("client",typeof(string));
                    objDT.Columns.Add("transactiondate", typeof(string));
                    objDT.Columns.Add("payeename",typeof(string));
                    objDT.Columns.Add("claimnumber", typeof(string));
                    objDT.Columns.Add("amount",typeof(double));
                    objDT.Columns.Add("transactiontype", typeof(string));
                    objDT.Columns.Add("accountname",typeof(string));
                    objDT.Columns.Add("reservetype", typeof(string));
                    objDT.Columns.Add("fromdate",typeof(string));
                    objDT.Columns.Add("todate", typeof(string));
                    objDT.Columns.Add("invoicenumber",typeof(string));
                    objDT.Columns.Add("invoicedate", typeof(string));
                    //End Defining Columns
                    foreach (DataRow objRow in objDS.Tables[0].Rows)
                    {
                        iCurrentTransID = Conversion.ConvertObjToInt(objRow["TRANS_ID"], m_iClientId);
                        iEntityID = GetOrgParentByDepartID(Conversion.ConvertObjToInt(objRow["DEPT_EID"], m_iClientId), 1007);
                        if (iCurrentTransID != iPrevTransID)
                        {
                            iPrevTransID = iCurrentTransID;                                               
                            //Adding Rows  
                            objRowTemp = objDT.NewRow();
                            objRowTemp["key"]= Conversion.ConvertObjToStr(objRow["TRANS_ID"]);
                            objRowTemp["id"] = Conversion.ConvertObjToStr(objRow["TRANS_ID"]) + ";" + Conversion.ConvertObjToStr(objRow["SPLIT_ROW_ID"]) + ";" + Conversion.ConvertObjToStr(objRow["AMOUNT"]) + ";" + GetCurrentAdjuster(Conversion.ConvertObjToInt(objRow["CLAIM_ID"], m_iClientId));
                            objRowTemp["client"]= GetEntityNameEx(iEntityID);
                            objRowTemp["transactiondate"]= Conversion.ConvertObjToStr(objRow["TRANS_DATE"]);
                            if (Conversion.ConvertObjToStr(objRow["FIRST_NAME"]) == "")
                            {
                                objRowTemp["payeename"] = Conversion.ConvertObjToStr(objRow["LAST_NAME"]);
                            }
                            else
                            {
                                objRowTemp["payeename"] = Conversion.ConvertObjToStr(objRow["LAST_NAME"]) + ", " + Conversion.ConvertObjToStr(objRow["FIRST_NAME"]);
                            }
                            objRowTemp["claimnumber"] = Conversion.ConvertObjToStr(objRow["CLAIM_NUMBER"]);
                            objRowTemp["amount"] = Conversion.ConvertObjToDouble(objRow["AMOUNT"], m_iClientId);
                            objRowTemp["transactiontype"] = m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objRow["TRANS_TYPE_CODE"], m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objRow["TRANS_TYPE_CODE"], m_iClientId));
                            objRowTemp["accountname"]= Conversion.ConvertObjToStr(objRow["ACCOUNT_NAME"]);
                            objRowTemp["reservetype"] = m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objRow["RESERVE_TYPE_CODE"], m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objRow["RESERVE_TYPE_CODE"], m_iClientId));
                            objRowTemp["fromdate"]= Conversion.ConvertObjToStr(objRow["FROM_DATE"]);
                            objRowTemp["todate"]= Conversion.ConvertObjToStr(objRow["TO_DATE"]);
                            objRowTemp["invoicenumber"]= Conversion.ConvertObjToStr(objRow["INVOICE_NUMBER"]);
                            objRowTemp["invoicedate"]= Conversion.ConvertObjToStr(objRow["INVOICE_DATE"]);
                            objDT.Rows.Add(objRowTemp);
                            objRowTemp = null;  
                            //End Adding Rows
                        }
                        else
                        {
                            int iCount = objDT.Rows.Count; 
                            objRowTemp = objDT.Rows[iCount -1];
                                if (Conversion.ConvertObjToInt(objRow["TRANS_TYPE_CODE"], m_iClientId) != 0)
                                    objRowTemp["transactiontype"] = objRowTemp["transactiontype"] + ", " + m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objRow["TRANS_TYPE_CODE"], m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objRow["TRANS_TYPE_CODE"], m_iClientId));
                                if (Conversion.ConvertObjToInt(objRow["RESERVE_TYPE_CODE"], m_iClientId) != 0)
                                    objRowTemp["reservetype"] = objRowTemp["reservetype"] + ", " + m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objRow["RESERVE_TYPE_CODE"], m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objRow["RESERVE_TYPE_CODE"], m_iClientId));
                                if (!Conversion.ConvertObjToStr(objRow["FROM_DATE"]).Equals(""))
                                    objRowTemp["fromdate"] = objRowTemp["fromdate"] + ", " + Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRow["FROM_DATE"]), "d");
                                if (!Conversion.ConvertObjToStr(objRow["TO_DATE"]).Equals(""))
                                    objRowTemp["todate"] = objRowTemp["todate"] + ", " + Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRow["TO_DATE"]), "d");
                                if (!Conversion.ConvertObjToStr(objRow["INVOICE_NUMBER"]).Equals(""))
                                    objRowTemp["invoicenumber"] = objRowTemp["invoicenumber"] + ", " + Conversion.ConvertObjToStr(objRow["INVOICE_NUMBER"]);
                                if (!Conversion.ConvertObjToStr(objRow["INVOICE_DATE"]).Equals(""))
                                    objRowTemp["invoicedate"] = objRowTemp["invoicedate"] + ", " + Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRow["INVOICE_DATE"]), "d");
                                objRowTemp = null; 
                        }
                    }                    
                    //DataView Sorting
                    objDV = new DataView(objDT);
                    objNodeExpr = p_objDocument.SelectSingleNode("//SortExpression").InnerText;
                    //Switch Case
                    switch (objNodeExpr)
                    {
                        case "1":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "client DESC";
                                }
                                else
                                {
                                    objDV.Sort = "client ASC";
                                }
                                break;
                            }
                        case "2":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "transactiondate DESC";
                                }
                                else
                                {
                                    objDV.Sort = "transactiondate ASC";
                                }
                                break;
                            }
                        case "3":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "payeename DESC";
                                }
                                else
                                {
                                    objDV.Sort = "payeename ASC";
                                }
                                break;
                            }
                        case "4":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "claimnumber DESC";
                                }
                                else
                                {
                                    objDV.Sort = "claimnumber ASC";
                                }
                                break;
                            }
                        case "5":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "amount DESC";
                                }
                                else
                                {
                                    objDV.Sort = "amount ASC";
                                }
                                break;
                            }
                        case "6":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "transactiontype DESC";
                                }
                                else
                                {
                                    objDV.Sort = "transactiontype ASC";
                                }
                                break;
                            }
                        case "7":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "accountname DESC";
                                }
                                else
                                {
                                    objDV.Sort = "accountname ASC";
                                }
                                break;
                            }
                        case "8":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "reservetype DESC";
                                }
                                else
                                {
                                    objDV.Sort = "reservetype ASC";
                                }
                                break;
                            }
                        case "9":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "fromdate DESC";
                                }
                                else
                                {
                                    objDV.Sort = "fromdate ASC";
                                }
                                break;
                            }
                        case "10":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "todate DESC";
                                }
                                else
                                {
                                    objDV.Sort = "todate ASC";
                                }
                                break;
                            }
                        case "11":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "invoicenumber DESC";
                                }
                                else
                                {
                                    objDV.Sort = "invoicenumber ASC";
                                }
                                break;
                            }
                        case "12":
                            {
                                objNodeDir = p_objDocument.SelectSingleNode("//SortDirection").InnerText;
                                if (objNodeDir == "Ascending")
                                {
                                    objDV.Sort = "invoicedate DESC";
                                }
                                else
                                {
                                    objDV.Sort = "invoicedate ASC";
                                }
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                    //End Switch Case                                  
                    //End DataView Sorting

                    //Creating XML from DataView
                    foreach (DataRowView objDRV in objDV)
                    {
                        objElemTemp = objDOM.CreateElement("Payment");
                        objElemTemp.SetAttribute("key", Conversion.ConvertObjToStr(objDRV["key"]));
                        objElemTemp.SetAttribute("id", Conversion.ConvertObjToStr(objDRV["id"]));
                        objElemTemp.SetAttribute("client", Conversion.ConvertObjToStr(objDRV["client"]));
                        objElemTemp.SetAttribute("transactiondate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDRV["transactiondate"]), "d"));
                        objElemTemp.SetAttribute("payeename", Conversion.ConvertObjToStr(objDRV["payeename"]));
                        objElemTemp.SetAttribute("claimnumber", Conversion.ConvertObjToStr(objDRV["claimnumber"]));
                        objElemTemp.SetAttribute("amount", String.Format("{0:c}", Conversion.ConvertObjToStr(objDRV["amount"])));
                        objElemTemp.SetAttribute("transactiontype", Conversion.ConvertObjToStr(objDRV["transactiontype"]));
                        objElemTemp.SetAttribute("accountname", Conversion.ConvertObjToStr(objDRV["accountname"]));
                        objElemTemp.SetAttribute("reservetype", Conversion.ConvertObjToStr(objDRV["reservetype"]));
                        objElemTemp.SetAttribute("fromdate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDRV["fromdate"]), "d"));
                        objElemTemp.SetAttribute("todate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDRV["todate"]), "d"));
                        objElemTemp.SetAttribute("invoicenumber", Conversion.ConvertObjToStr(objDRV["invoicenumber"]));
                        objElemTemp.SetAttribute("invoicedate", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objDRV["invoicedate"]), "d"));
                        objDOM.SelectSingleNode("//bulkcheckgrid").AppendChild(objElemTemp);
                    }
                    //End Creating XML from DataView
                }
                return objDOM;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManagement.GetPayments.Error", m_iClientId), p_objEx);
            }
            finally
            {
                
                objNode = null;
                sbSQL = null;
                objElemParent = null;
                objElemTemp = null;
                objNodeExpr = null;
                objNodeDir = null;
                objDT = null;
                objDV = null;
                objRowTemp = null;
                if (objDS != null)
                {
                    objDS.Dispose();
                    objDS = null;
                }
            }
        }
        //End ddhiman 12/14/2010

        /// <summary>
        /// Method to return the Department entity ID's based on the specified input parameter
        /// </summary>
        /// <param name="iEntityid">Entity ID</param>
        /// <returns>All the Department ID's</returns>
        protected string GetClientIds(int iEntityid)
        {
            StringBuilder objEntityID = new StringBuilder();
            DbReader objReader = null;
            string sSQL = string.Empty;
            string strEntityTblName = string.Empty;  
            try
            {
                sSQL = "SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID= " + iEntityid ;
                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        strEntityTblName = m_objDMF.Context.LocalCache.GetTableName(Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId));   
                    }
                    objReader.Close();  
                }
                if (strEntityTblName.Equals("DEPARTMENT"))
                {
                    return iEntityid.ToString();
                }
                else
                {
                    sSQL = "SELECT " + strEntityTblName + ".ENTITY_ID " + strEntityTblName + " ,DEPARTMENT.ENTITY_ID DEPT" +
                            " FROM ENTITY  " + strEntityTblName + " INNER JOIN ORG_HIERARCHY ON ( " + strEntityTblName + ".ENTITY_ID=ORG_HIERARCHY." + strEntityTblName + "_EID)" +
                            " INNER JOIN ENTITY DEPARTMENT ON (ORG_HIERARCHY.DEPARTMENT_EID=DEPARTMENT.ENTITY_ID)" +
                            " WHERE (DEPARTMENT.ENTITY_TABLE_ID=(SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = 'DEPARTMENT')  )" +
                            " AND " + strEntityTblName + ".ENTITY_ID= " + iEntityid;
                    objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            if (!objEntityID.ToString().Equals(""))
                            {
                                objEntityID.Append(",");
                            }
                            objEntityID.Append(Conversion.ConvertObjToStr(objReader.GetValue("DEPT")));
                        }
                        objReader.Close();  
                    }
                    return objEntityID.ToString();  
                }

            }
            catch
            {
                return "";
            }
            finally
            {
                objEntityID = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Method to return the specified table Parent name based on the Department ID
        /// </summary>
        /// <param name="iDeptID">Department Entity ID </param>
        /// <param name="iTableID">Parent Table ID i.e Orh Hierarchy level code</param>
        /// <returns>Department specified level Parent Name </returns>
        protected int GetOrgParentByDepartID(int iDeptID, int iTableID)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            int intReturn = 0;
            try
            {
                sSQL = "SELECT " + GetOrgHierarchyFieldName(iTableID) + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + iDeptID;
                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        intReturn = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    }
                    objReader.Close();  
                }
                return intReturn;
            }
            catch
            {
                return 0;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Method to return the Org Hierarchy level name based on the specified Org Hierarchy code.
        /// </summary>
        /// <param name="iOrgLevel">Org Hierarchy level code</param>
        /// <returns>Org hierarchy Level name</returns>
        protected string GetOrgHierarchyFieldName(int iOrgLevel)
        {
            string strReturn = string.Empty; 
            switch (iOrgLevel)
            {
                case 1005:
                    strReturn = "CLIENT_EID";
                    break;
                case 1006:
                    strReturn = "COMPANY_EID";
                    break;
                case 1007:
                    strReturn = "OPERATION_EID";
                    break;
                case 1008:
                    strReturn = "REGION_EID";
                    break;
                case 1009:
                    strReturn = "DIVISION_EID";
                    break;
                case 1010:
                    strReturn = "LOCATION_EID";
                    break;
                case 1011:
                    strReturn = "FACILITY_EID";
                    break;
                case 1012:
                    strReturn = "DEPARTMENT_EID";
                    break;
                default:
                    strReturn = "CLIENT_EID";
                    break;
            }
            return strReturn; 
        }

        /// <summary>
        /// Method to return the Entity Lastname, First name based on the specified entity ID 
        /// </summary>
        /// <param name="iEntityID">Entity Id as parameter</param>
        /// <returns>Entity name</returns>
        protected string GetEntityNameEx(int iEntityID)
        {
            string strEntityName = string.Empty;
            DbReader objReader = null;
            string sSQL = string.Empty;
            try
            {
                sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + iEntityID;
                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        strEntityName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                        if (!(Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Equals("")))
                        {
                            strEntityName = strEntityName + ", " + Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                        }
                    }
                    else
                    {
                        strEntityName = "";
                    }
                    objReader.Close();
                }
                return strEntityName;
            }
            catch
            {
                return "";
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        protected string GetCurrentAdjuster(int iClaimID)
        {
            string strName = string.Empty;
            DbReader objReader = null;
            string sSQL = string.Empty;
            try
            {
                sSQL = "SELECT LAST_NAME, FIRST_NAME FROM ENTITY WHERE ENTITY_ID = (SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER  WHERE CLAIM_ID = " + iClaimID + " AND CURRENT_ADJ_FLAG <> 0)";
                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        strName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                        if (!(Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")).Equals("")))
                        {
                            strName = strName + ", " + Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                        }
                    }
                    else
                    {
                        strName = "";
                    }
                    objReader.Close();  
                }
                return strName;
            }
            catch
            {
                return "";
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }

        }
        /// <summary>
        /// Method to get the initial Screen values.
        /// Author: Animesh Sahai
        /// Date : 2/6/10
        /// </summary>
        /// <param name="p_objDocument">Input Screen XML</param>
        /// <returns>Screen Controls XML with populated values.</returns>
        public XmlDocument GetInitialScreenData(XmlDocument p_objDocument)
        {
            XmlElement objElement = null;
            XmlElement objChildElement = null;
            int iTableID = 0;
            DataSet objDs = null;
            string sSQL = string.Empty;
            try
            {
                objElement = (XmlElement)p_objDocument.SelectSingleNode("//control[@name='checkstatus']");
                sSQL = "SELECT C.CODE_ID ,C.SHORT_CODE,CT.CODE_DESC FROM CODES C, CODES_TEXT CT WHERE C.CODE_ID = CT.CODE_ID"
                        + " AND C.SHORT_CODE IN ('P','R') AND C.DELETED_FLAG = 0 AND C.CODE_ID <> 0 ";

                if (objElement != null)
                {
                    iTableID = m_objDMF.Context.LocalCache.GetTableId("CHECK_STATUS");
                    sSQL = sSQL + " AND C.TABLE_ID = " + iTableID + " ORDER BY C.SHORT_CODE DESC,CT.CODE_DESC DESC";
                    objDs = DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString, sSQL, m_iClientId);
                    if (objDs.Tables[0] != null)
                    {
                        foreach (DataRow dr in objDs.Tables[0].Rows)
                        {
                            objChildElement = p_objDocument.CreateElement("Types");
                            objChildElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                            objChildElement.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"] + " - " + Conversion.ConvertObjToStr(dr["CODE_DESC"]));
                            if (Conversion.ConvertObjToStr(dr["SHORT_CODE"])=="R")
                            {
                                objChildElement.SetAttribute("selected", "1");
                            }
                            objElement.AppendChild(objChildElement);
                        }
                    }
                }
                return p_objDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManagement.GetInitialScreenData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElement = null;
                objChildElement = null;
                objDs = null;                
            }
        }

        /// <summary>
        /// Method to process the Bulk Check Payments
        /// </summary>
        /// <param name="p_objDocument">XML Document containing the list of selected payments</param>
        /// <returns>XML Document specifying the successful and unsuccessful payments.</returns>
        public XmlDocument ProcessBulkPayments(XmlDocument p_objDocument)
        {
            string strSelectedIds = string.Empty;
            int iStatusCode = 0;
            XmlDocument objDOM = null;
            XmlElement objParentElem=null;
            XmlElement objtempElem=null;
            StringBuilder sbSQL = null;
            XmlNode objNode = null;
            string[] arrTransID = null;
            int iCurrentTransID = 0;
            int iCurrentClaimID = 0;
            bool blnValidateResult;
            DataTable objDtSuccess=null;
            DataTable objDtUnSuccess = null;
            string[] objTempArray = null; //Array designed to handle the values provided in IN Clause as there in limitation in SQL and oracle for IN clause Length
            string sSQL = string.Empty;  
            DbReader objReader = null;
            try
            {
                objDOM = new XmlDocument();  
                blnValidateResult = false;  
                sbSQL = new StringBuilder();
                objDtSuccess = new DataTable();
                objDtUnSuccess = new DataTable();  
                objNode = p_objDocument.SelectSingleNode("//selectedtransid");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            strSelectedIds = Conversion.ConvertObjToStr(objNode.InnerText);
                    }
                }
                objNode = p_objDocument.SelectSingleNode("//checkstatus");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                            iStatusCode = Conversion.ConvertObjToInt(objNode.InnerText, m_iClientId);
                    }
                }
                if (!strSelectedIds.Equals("") && (iStatusCode!=0))
                {

                    //Success Data Table Columns
                    objDtSuccess.Columns.Add("client", typeof(string));
                    objDtSuccess.Columns.Add("transactiondate",typeof(string));
                    objDtSuccess.Columns.Add("payeename", typeof(string));
                    objDtSuccess.Columns.Add("claimnumber", typeof(string));
                    objDtSuccess.Columns.Add("amount", typeof(string));
                    objDtSuccess.Columns.Add("transactiontype", typeof(string));
                    objDtSuccess.Columns.Add("accountname", typeof(string));
                    objDtSuccess.Columns.Add("reservetype", typeof(string));
                    objDtSuccess.Columns.Add("currentadjuster", typeof(string));
                    //Unsuccessful Data Table Columns
                    objDtUnSuccess .Columns.Add("client", typeof(string));
                    objDtUnSuccess.Columns.Add("transactiondate", typeof(string));
                    objDtUnSuccess.Columns.Add("payeename", typeof(string));
                    objDtUnSuccess.Columns.Add("claimnumber", typeof(string));
                    objDtUnSuccess.Columns.Add("amount", typeof(string));
                    objDtUnSuccess.Columns.Add("transactiontype", typeof(string));
                    objDtUnSuccess.Columns.Add("accountname", typeof(string));
                    objDtUnSuccess.Columns.Add("reservetype", typeof(string));
                    objDtUnSuccess.Columns.Add("currentadjuster", typeof(string));
                    
                    arrTransID = strSelectedIds.Split(new char[] {','});

                    foreach (string strTemp in arrTransID)
                    {
                        iCurrentTransID = Conversion.ConvertStrToInteger(strTemp);
                        if (iCurrentTransID > 0)
                        {
                            if (m_objDMF.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                            {
                                sSQL = "SELECT F.TRANS_DATE, F.LAST_NAME, F.FIRST_NAME, C.CLAIM_NUMBER,"
                                    + " F.AMOUNT,FTS.TRANS_TYPE_CODE,ACC.ACCOUNT_NAME, E.DEPT_EID,"
                                    + " FTS.RESERVE_TYPE_CODE, C.CLAIM_ID FROM FUNDS F,FUNDS_TRANS_SPLIT FTS,ACCOUNT ACC,CLAIM C,EVENT E"
                                    + " WHERE F.TRANS_ID = " + iCurrentTransID + " AND F.TRANS_ID=FTS.TRANS_ID AND F.ACCOUNT_ID=ACC.ACCOUNT_ID AND F.CLAIM_ID=C.CLAIM_ID "
                                    + " AND C.EVENT_ID =E.EVENT_ID ORDER BY FTS.TRANS_ID";
                            }
                            else
                                
                            { 
                            sSQL = "SELECT F.TRANS_DATE, F.LAST_NAME, F.FIRST_NAME, C.CLAIM_NUMBER,"
                                    + " F.CLAIM_CURRENCY_AMOUNT AMOUNT,FTS.TRANS_TYPE_CODE,ACC.ACCOUNT_NAME, E.DEPT_EID,"
                                    + " FTS.RESERVE_TYPE_CODE, C.CLAIM_ID FROM FUNDS F,FUNDS_TRANS_SPLIT FTS,ACCOUNT ACC,CLAIM C,EVENT E"
                                    + " WHERE F.TRANS_ID = " + iCurrentTransID + " AND F.TRANS_ID=FTS.TRANS_ID AND F.ACCOUNT_ID=ACC.ACCOUNT_ID AND F.CLAIM_ID=C.CLAIM_ID "
                                    + " AND C.EVENT_ID =E.EVENT_ID ORDER BY FTS.TRANS_ID";
                            
                            
                            }
                            
                            blnValidateResult = ValidatePayment(iCurrentTransID, out iCurrentClaimID);
                            if (blnValidateResult)
                            {
                                this.ProcessPayments(iCurrentTransID, iStatusCode);
                                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                                int iCounter = 0;
                                if (objReader != null)
                                {
                                    DataRow objTempRow = objDtSuccess.NewRow();  

                                    while (objReader.Read())
                                    {
                                        if (iCounter == 0)
                                        {
                                            objTempRow["client"] = GetEntityNameEx(GetOrgParentByDepartID(Conversion.ConvertObjToInt(objReader.GetValue("DEPT_EID"), m_iClientId), 1007));
                                            objTempRow["transactiondate"]= Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("TRANS_DATE")), "d");
                                            objTempRow["payeename"]=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")) + ", " + Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                                            objTempRow["claimnumber"]=Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));
                                            objTempRow["amount"]= Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT"));
                                            objTempRow["transactiontype"]=m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId));
                                            objTempRow["accountname"]=Conversion.ConvertObjToStr(objReader.GetValue("ACCOUNT_NAME"));
                                            objTempRow["reservetype"]= m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId));
                                            objTempRow["currentadjuster"] = GetCurrentAdjuster(Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId));
                                        }
                                        else
                                        {
                                            if (Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId) != 0)
                                                objTempRow["transactiontype"] = objTempRow["transactiontype"] + ", " + m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId));
                                            if (Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId) != 0)
                                                objTempRow["reservetype"] = objTempRow["reservetype"] + ", " + m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId));
                                        }
                                        iCounter += 1; 
                                    }
                                    objDtSuccess.Rows.Add(objTempRow);
                                    objTempRow=null;
                                    objReader.Close();  
                                    objReader.Dispose();  
                                }
                            }
                            else
                            {
                                
                                //populate the UnsuccessFul payments
                                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                                int iCounter = 0;
                                if (objReader != null)
                                {
                                    DataRow objTempRow = objDtUnSuccess.NewRow();  
                                    while (objReader.Read())
                                    {
                                        if (iCounter == 0)
                                        {
                                            objTempRow["client"]=GetEntityNameEx(GetOrgParentByDepartID(Conversion.ConvertObjToInt(objReader.GetValue("DEPT_EID"), m_iClientId), 1007));
                                            objTempRow["transactiondate"]= Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objReader.GetValue("TRANS_DATE")), "d");
                                            objTempRow["payeename"]=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")) + ", " + Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                                            objTempRow["claimnumber"]=Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_NUMBER"));
                                            objTempRow["amount"]= Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT"));
                                            objTempRow["transactiontype"]=m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId));
                                            objTempRow["accountname"]=Conversion.ConvertObjToStr(objReader.GetValue("ACCOUNT_NAME"));
                                            objTempRow["reservetype"] = m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId));
                                            objTempRow["currentadjuster"] = GetCurrentAdjuster(Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId));
                                            //Create a Diary ...
                                            CreateDiaryForAdjuster("Insufficient Reserves", "Insufficient", "CLAIM", Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId),"Payment Ctrl No", 0);
                                        }
                                        else
                                        {
                                            if (Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId) != 0)
                                                objTempRow["transactiontype"] = objTempRow["transactiontype"] + ", " + m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE"), m_iClientId));
                                            if (Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId) != 0)
                                                objTempRow["reservetype"] = objTempRow["reservetype"] + ", " + m_objDMF.Context.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId)) + "-" + m_objDMF.Context.LocalCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_TYPE_CODE"), m_iClientId));
                                        }
                                        iCounter += 1; 
                                    }
                                    objDtUnSuccess.Rows.Add(objTempRow);
                                    objTempRow=null;
                                    objReader.Close();  
                                    objReader.Dispose();  
                                }
                            }
                        }
                    }
                    objParentElem=objDOM.CreateElement("processresult");
                    objDOM.AppendChild(objParentElem);
                    objParentElem=null;
                    objParentElem=objDOM.CreateElement("success");
                    objDOM.ChildNodes[0].AppendChild(objParentElem);     
                    objParentElem=null;
                    objParentElem=objDOM.CreateElement("unsuccess");
                    objDOM.ChildNodes[0].AppendChild(objParentElem);     
                    //populating successful data.
                    foreach(DataRow objRow in objDtSuccess.Rows)
                    {
                        objtempElem = objDOM.CreateElement("Payment");
                        objtempElem.SetAttribute("client",objRow["client"].ToString() );
                        objtempElem.SetAttribute("transactiondate",objRow["transactiondate"].ToString());
                        objtempElem.SetAttribute("payeename",objRow["payeename"].ToString() );
                        objtempElem.SetAttribute("claimnumber",objRow["claimnumber"].ToString() );
                        objtempElem.SetAttribute("amount", String.Format("{0:c}", objRow["amount"].ToString()));
                        objtempElem.SetAttribute("transactiontype",objRow["transactiontype"].ToString());
                        objtempElem.SetAttribute("accountname",objRow["accountname"].ToString());
                        objtempElem.SetAttribute("reservetype",objRow["reservetype"].ToString());
                        objtempElem.SetAttribute("currentadjuster", objRow["currentadjuster"].ToString());
                        objDOM.SelectSingleNode("//processresult//success").AppendChild(objtempElem);
                        objtempElem=null; 
                    }
                    //populating successful data.
                    foreach(DataRow objRow in objDtUnSuccess.Rows)
                    {
                        objtempElem = objDOM.CreateElement("Payment");
                        objtempElem.SetAttribute("client",objRow["client"].ToString());
                        objtempElem.SetAttribute("transactiondate",objRow["transactiondate"].ToString());
                        objtempElem.SetAttribute("payeename",objRow["payeename"].ToString());
                        objtempElem.SetAttribute("claimnumber",objRow["claimnumber"].ToString());
                        objtempElem.SetAttribute("amount", String.Format("{0:c}", objRow["amount"].ToString()));
                        objtempElem.SetAttribute("transactiontype",objRow["transactiontype"].ToString());
                        objtempElem.SetAttribute("accountname",objRow["accountname"].ToString());
                        objtempElem.SetAttribute("reservetype",objRow["reservetype"].ToString());
                        objtempElem.SetAttribute("currentadjuster", objRow["currentadjuster"].ToString());
                        objDOM.SelectSingleNode("//processresult//unsuccess").AppendChild(objtempElem);
                        objtempElem=null; 
                    }   
                }
                return objDOM; 
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManagement.ProcessBulkPayments.Error", m_iClientId), p_objEx);
            }
            finally
            {
                sbSQL = null;
                objDOM = null;
                objNode = null;
                if (objDtSuccess!=null)
                {
                    objDtSuccess.Dispose();
                    objDtSuccess=null; 
                }
                if (objDtUnSuccess!=null)
                {
                    objDtUnSuccess.Dispose();
                    objDtUnSuccess=null; 
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();  
                }
            }
        }

        /// <summary>
        /// Method to validate that whether enough reserves are available to process the payments.
        /// </summary>
        /// <param name="iTransID">Payment transaction id</param>
        /// <param name="p_iClaimID">out parameter payment claim ID</param>
        /// <returns>boolean value</returns>
        protected bool ValidatePayment(int iTransID ,out int p_iClaimID)
        {
            string sSQL = string.Empty;
            int iclaimID = 0;
            int iClaimantID = 0;
            int iUnitID = 0;
            int iReserveType = 0;
            int iLOB = 0;
            int iFundsTransID = 0;
            double dblReserveBalance = 0;
            double  dblTotalPayments = 0;
            bool blnReturn=true;
            DbReader objReader = null;
            FundManager objFundManager = null;
            p_iClaimID = 0;
            //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
            //int iCoverageId = 0;
            int iRCRowId = 0;
            int iCovgTypeCode=0;
            int iPolicyId=0;
            int iPolicyUnitRowId=0;
            string sCovgSeqNum = string.Empty;//rupal:policy system interafce
            int iLossCode = 0;
            string sTransSeqNum = string.Empty;//smishra54: MITS 33996
            string sCoverageKey = string.Empty;     //Ankit Start : Worked on MITS - 34297
            try
            {
                objFundManager= new FundManager(m_sDSN,m_sUserName,m_sUserPass,m_objDMF.Context.RMUser.UserId,m_objDMF.Context.RMUser.GroupId, m_objDMF.Context.ClientId);
                if (m_objDMF.Context.InternalSettings.SysSettings.UseMultiCurrency == 0)
                {
                    //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                    /*sSQL = "SELECT F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID,FTS.POLCVG_ROW_ID ,"
                         + " SUM(FTS.AMOUNT) TOTAL FROM FUNDS F , CLAIM C , FUNDS_TRANS_SPLIT FTS WHERE  F.TRANS_ID = " + iTransID
                         + " AND C.CLAIM_ID  = F.CLAIM_ID AND  C.CLAIM_ID  = F.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID "
                         + " GROUP BY F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID ,FTS.POLCVG_ROW_ID ";*/
                    sSQL = "SELECT F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID,FTS.RC_ROW_ID ,"
                         + " SUM(FTS.AMOUNT) TOTAL FROM FUNDS F , CLAIM C , FUNDS_TRANS_SPLIT FTS WHERE  F.TRANS_ID = " + iTransID
                         + " AND C.CLAIM_ID  = F.CLAIM_ID AND  C.CLAIM_ID  = F.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID "
                         + " GROUP BY F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID ,FTS.RC_ROW_ID ";                   
                }
                else
                {
                    //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                    /*sSQL = "SELECT F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID,FTS.POLCVG_ROW_ID , "
                            + " SUM(FTS.CLAIM_CURRENCY_AMOUNT) TOTAL FROM FUNDS F , CLAIM C , FUNDS_TRANS_SPLIT FTS,COVERAGE_X_LOSS CXL,RESERVE_CURRENT RC WHERE  F.TRANS_ID = " + iTransID
                            + " AND C.CLAIM_ID  = F.CLAIM_ID AND  C.CLAIM_ID  = F.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID "
                            + " GROUP BY F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID,FTS.POLCVG_ROW_ID  "; */
                    sSQL = "SELECT F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID,FTS.RC_ROW_ID , "
                            + " SUM(FTS.CLAIM_CURRENCY_AMOUNT) TOTAL FROM FUNDS F , CLAIM C , FUNDS_TRANS_SPLIT FTS,COVERAGE_X_LOSS CXL,RESERVE_CURRENT RC WHERE  F.TRANS_ID = " + iTransID
                            + " AND C.CLAIM_ID  = F.CLAIM_ID AND  C.CLAIM_ID  = F.CLAIM_ID AND F.TRANS_ID = FTS.TRANS_ID "
                            + " GROUP BY F.CLAIM_ID, F.CLAIMANT_EID,F.UNIT_ID, FTS.RESERVE_TYPE_CODE, C.LINE_OF_BUS_CODE, F.TRANS_ID,FTS.RC_ROW_ID  ";
                
                }
                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iclaimID = objReader.GetInt32("CLAIM_ID");
                        iClaimantID = objReader.GetInt32("CLAIMANT_EID");
                        iUnitID = objReader.GetInt32("UNIT_ID");
                        iReserveType = objReader.GetInt32("RESERVE_TYPE_CODE");
                        iLOB =objReader.GetInt32("LINE_OF_BUS_CODE");
                        iFundsTransID = objReader.GetInt32("TRANS_ID");
                        dblTotalPayments =objReader.GetDouble("TOTAL");
                        if (m_objDMF.Context.InternalSettings.SysSettings.MultiCovgPerClm == 0)
                            dblReserveBalance = objFundManager.GetReserveBalance(iclaimID, iClaimantID, iUnitID, iReserveType, iLOB, iFundsTransID);
                        else
                        {
                            //iCoverageId=(objReader.GetInt("POLCVG_ROW_ID"));
                            iRCRowId = objReader.GetInt("RC_ROW_ID");

                            //if (iCoverageId > 0)
                            if (iRCRowId > 0)
                            {
                                //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID :Start
                                //rupal:policy system interface
                                //sSQL = string.Format("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID,COVERAGE_TYPE_CODE , POLICY_X_UNIT.POLICY_ID FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLCVG_ROW_ID = {0} AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iCoverageId);
                               // sSQL = string.Format("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID,COVERAGE_TYPE_CODE , POLICY_X_UNIT.POLICY_ID,POLICY_X_CVG_TYPE.CVG_SEQUENCE_NO FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLCVG_ROW_ID = {0} AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iCoverageId);
                                sSQL = string.Format("SELECT CXL.LOSS_CODE,POLICY_X_UNIT.POLICY_UNIT_ROW_ID,COVERAGE_TYPE_CODE , POLICY_X_UNIT.POLICY_ID,POLICY_X_CVG_TYPE.CVG_SEQUENCE_NO,POLICY_X_CVG_TYPE.TRANS_SEQ_NO,POLICY_X_CVG_TYPE.COVERAGE_KEY FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT,RESERVE_CURRENT RC,COVERAGE_X_LOSS CXL WHERE RC.RC_ROW_ID = {0} AND RC.POLCVG_LOSS_ROW_ID = CXL.CVG_LOSS_ROW_ID AND POLICY_X_CVG_TYPE.POLCVG_ROW_ID = CXL.POLCVG_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iRCRowId);
                                //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID :End
                                using (DbReader objReaderBob = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL))
                                {
                                    if (objReader.Read())
                                    {
                                        iPolicyId = objReaderBob.GetInt("POLICY_ID");
                                        iCovgTypeCode = objReaderBob.GetInt("COVERAGE_TYPE_CODE");
                                        iPolicyUnitRowId = objReaderBob.GetInt("POLICY_UNIT_ROW_ID");
                                        sCovgSeqNum = Conversion.ConvertObjToStr("CVG_SEQUENCE_NO");//rupal:policy system interface
                                        iLossCode = objReaderBob.GetInt("LOSS_CODE");
                                        sTransSeqNum = Conversion.ConvertObjToStr("TRANS_SEQ_NO");//smishra54: MITS 33996
                                        sCoverageKey = Conversion.ConvertObjToStr("COVERAGE_KEY");      //Ankit Start : Worked on MITS - 34297
                                    }
                                }
                            }
                            //rupal:policy system interface
                            //dblReserveBalance = objFundManager.GetReserveBalance(iclaimID, iClaimantID, iUnitID, iReserveType, iLOB, iFundsTransID,iPolicyId,iCovgTypeCode,iPolicyUnitRowId);
                            dblReserveBalance = objFundManager.GetReserveBalance(iclaimID, iClaimantID, iUnitID, iReserveType, iLOB, iFundsTransID, iPolicyId, iCovgTypeCode, iPolicyUnitRowId, sCovgSeqNum, iLossCode, sTransSeqNum, sCoverageKey);
                        }
                        if ((dblTotalPayments - dblReserveBalance) > 0.009)
                        {
                            p_iClaimID = iclaimID;
                            blnReturn = false;
                            break;
                        }
                    }
                }
                return blnReturn; 
            }
            catch
            {
                return false;
            }
            finally
            {
                if (objFundManager!=null) 
                {
                    objFundManager.Dispose();
                    objFundManager= null;  
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }
        /// <summary>
        /// Method to porcess the payments from Queued status if enough reserves are available
        /// </summary>
        /// <param name="iTransID">Transaction Id of the payment</param>
        /// <param name="iNewStatusCode">New stautus code of the check</param>
        /// <returns>returns boolean value</returns>
        protected bool ProcessPayments(int iTransID, int iNewStatusCode)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iClaimID =0 ;
            int iClaimantID = 0;
            int iUnitID = 0;
            string strDtOfChk = string.Empty;
            int iLOB = 0;
            bool blnDataExists;
            ReserveFunds objReserveFund = null;
            bool blnReturn; 
            try
            {
                blnDataExists = false;
                blnReturn = false;  
                sSQL = "SELECT F.CLAIM_ID, F.CLAIMANT_EID, F.UNIT_ID, C.LINE_OF_BUS_CODE, F.DATE_OF_CHECK, F.STATUS_CODE" +
                       " FROM FUNDS F, CLAIM C WHERE TRANS_ID = " + iTransID + " AND C.CLAIM_ID  = F.CLAIM_ID";
                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iClaimID = Conversion.ConvertObjToInt(objReader.GetValue("CLAIM_ID"), m_iClientId);
                        iClaimantID = Conversion.ConvertObjToInt(objReader.GetValue("CLAIMANT_EID"), m_iClientId);
                        iUnitID = Conversion.ConvertObjToInt(objReader.GetValue("UNIT_ID"), m_iClientId);
                        iLOB = Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                        strDtOfChk = Conversion.ConvertObjToStr(objReader.GetValue("DATE_OF_CHECK"));
                        blnDataExists = true; 
                    }
                }
                objReader.Close();
                if (blnDataExists)
                {
                    objReserveFund = new ReserveFunds(m_objDMF.Context.DbConn.ConnectionString,"",0,0, m_iClientId);//rkaur27              
                    sSQL = "UPDATE FUNDS SET STATUS_CODE = " + iNewStatusCode + " ,VOID_FLAG= 0, "
                          + " VOID_DATE = '' , DTTM_RCD_LAST_UPD = '" + DateTime.Now.ToString("YYYYMMDDHHMMSS") + "',"
                          + " UPDATED_BY_USER='" + m_sUserName + "' WHERE TRANS_ID = " + iTransID;
                    m_objDMF.Context.DbConn.ExecuteNonQuery(sSQL);
                    objReserveFund.UpdateFundsReserveCurrent(iClaimID, iClaimantID, iUnitID, iLOB, strDtOfChk, m_objDMF.Context.RMUser.LoginName);           
                    blnReturn = true; 
                }
                return blnReturn; 
            }
            catch
            {
                return false;  
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objReserveFund != null)
                {
                    objReserveFund = null;
                }
            }

        }
        /// <summary>
        /// Method to create the diary for an adjuster if enough reserves are not available to process a payment
        /// </summary>
        /// <param name="strEntryName"></param>
        /// <param name="strEntryNotes"></param>
        /// <param name="strAttachTable"></param>
        /// <param name="iAttachRecordID"></param>
        /// <param name="strRegarding"></param>
        /// <param name="iAttachSecondRecID"></param>
        protected void CreateDiaryForAdjuster(string strEntryName,string strEntryNotes,string strAttachTable,int iAttachRecordID,string strRegarding, int iAttachSecondRecID)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            int intRMUserID = 0;
            string strLoginName = string.Empty;
            WpaDiaryEntry objWPADiaryEntry = null;
            string strSecurityDSN = string.Empty;
            try
            {
                sSQL = "SELECT RM_USER_ID FROM ENTITY WHERE ENTITY_ID  = (SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER WHERE CLAIM_ID = " + iAttachRecordID + " AND CURRENT_ADJ_FLAG = -1 )";
                objReader = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        intRMUserID = Conversion.ConvertObjToInt(objReader.GetValue("RM_USER_ID"), m_iClientId);   
                    }
                    objReader.Close();
                    objReader.Dispose();  
                }
                if (intRMUserID != 0)
                {
                    strSecurityDSN = Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId);
                    sSQL = "SELECT USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE, USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = " + intRMUserID
                         + " AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID = " + m_objDMF.Context.RMDatabase.DataSourceId;
                    objReader = DbFactory.GetDbReader(strSecurityDSN, sSQL);
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            strLoginName = Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
                        }
                        objReader.Close();
                        objReader.Dispose();
                    }
                    if (!strLoginName.Equals(""))
                    {
                        objWPADiaryEntry = (WpaDiaryEntry)m_objDMF.GetDataModelObject("WpaDiaryEntry", false);
                        objWPADiaryEntry.EntryName = strEntryName;
                        objWPADiaryEntry.EntryNotes = strEntryNotes;
                        objWPADiaryEntry.CreateDate = System.DateTime.Now.ToString("yyyyMMddHHmmss");
                        objWPADiaryEntry.Priority = 1;
                        objWPADiaryEntry.StatusOpen = true;
                        objWPADiaryEntry.AutoConfirm = false;
                        objWPADiaryEntry.AssignedUser = strLoginName;
                        objWPADiaryEntry.AssigningUser = m_objDMF.Context.RMUser.LoginName;
                        objWPADiaryEntry.AssignedGroup = "";
                        objWPADiaryEntry.IsAttached = true;
                        objWPADiaryEntry.AttachTable = strAttachTable;
                        objWPADiaryEntry.AttFormCode = 3192;
                        objWPADiaryEntry.AttachRecordid = iAttachRecordID;
                        objWPADiaryEntry.Regarding = strRegarding;
                        objWPADiaryEntry.AttSecRecId = 0;
                        objWPADiaryEntry.CompleteTime = "235959";
                        objWPADiaryEntry.CompleteDate = System.DateTime.Now.ToString("yyyyMMdd");
                        objWPADiaryEntry.DiaryVoid = false;
                        objWPADiaryEntry.DiaryDeleted = false;
                        objWPADiaryEntry.NotifyFlag = false;
                        objWPADiaryEntry.RouteFlag = false;
                        objWPADiaryEntry.EstimateTime = 0;
                        objWPADiaryEntry.AutoId = 0;
                        //Indu - Adding att_parent_code for Diary enhancement - Mits 33843
                        string scSQL = string.Empty;
                        DbReader objRead = null;
                        scSQL = "SELECT CLAIM_ID FROM FUNDS WHERE TRANS_ID =" + iAttachRecordID;
                        objRead = DbFactory.GetDbReader(m_objDMF.Context.DbConn.ConnectionString, scSQL);
                        if (objRead != null)
                        {
                            while (objRead.Read())
                            {
                                objWPADiaryEntry.AttParentCode = Conversion.ConvertObjToInt(objRead.GetValue("CLAIM_ID"), m_iClientId);//MITS  35419 asharma326 
                            }
                            objRead.Close();
                            objRead.Dispose();  
                        }
                        //End of Mits 28937
                        objWPADiaryEntry.Save();  
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundManagement.CreateDiaryForAdjuster.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();  
                }
                if (objWPADiaryEntry != null)
                {
                    objWPADiaryEntry.Dispose(); 
                }
            }
        }

        #endregion
    }
}
