﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;

using C1.C1PrintDocument;

namespace Riskmaster.Application.FundManagement
{
	/**************************************************************
	 * $File		: PaymentNotification.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 08/08/2005
	 * $Author		: Mihika Agrawal
	 * $Comment		: PaymentNotification class has functions to display and create Payment Notification Report.
	 * $Source		:  	
	**************************************************************/
	/// <summary>
	/// PaymentHistory class.
	/// </summary>
	public class PaymentNotification
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;

		/// <summary>
		/// C1PrintDocumnet object instance.
		/// </summary>
		private C1PrintDocument m_objPrintDoc = null ;

		/// <summary>
		/// CurrentX
		/// </summary>
		private double m_dblCurrentX = 0 ;

		/// <summary>
		/// CurrentY
		/// </summary>
		private double m_dblCurrentY = 0 ;

		/// <summary>
		/// Font object.
		/// </summary>
		private Font m_objFont = null ;

		/// <summary>
		/// Text object.
		/// </summary>
		private RenderText m_objText = null ;

		/// <summary>
		/// PageWidth
		/// </summary>
		private double m_dblPageWidth = 0.0 ;

		/// <summary>
		/// PageHeight
		/// </summary>
		private double m_dblPageHeight = 0.0 ;

		/// <summary>
		/// ClientWidth
		/// </summary>
		private double m_dblClientWidth = 0.0 ;
        /// <summary>
        /// ClientId for multi-tenancy, cloud
        /// </summary>
        private int m_iClientId =0 ;

        //Ash - cloud, config settings moved to DB
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;

		#endregion

		#region Constants Declaration
		/// <summary>
		/// Left alignment
		/// </summary>
		private const string LEFT_ALIGN = "1" ;
		/// <summary>
		/// Right alignment
		/// </summary>
		private const string RIGHT_ALIGN = "2" ;
		/// <summary>
		/// Center alignment
		/// </summary>
		private const string CENTER_ALIGN = "3" ;
		/// <summary>
		/// Number of rows in a report page
		/// </summary>
		private const int NO_OF_ROWS=54;
		#endregion
		
		#region Constructors
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		public PaymentNotification(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString = p_sConnectionString;
            // Ash - cloud, config settings moved to db
            m_iClientId = p_iClientId;
            m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnectionString, m_iClientId);

		}
		#endregion

		#region Property Declarations
		/// <summary>
		/// Read property for PageHeight.
		/// </summary>
		private double PageHeight 
		{
			get
			{
				return( m_dblPageHeight ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double PageWidth 
		{
			get
			{
				return( m_dblPageWidth ) ;
			}			
		}
		/// <summary>
		/// Read property for PageWidth.
		/// </summary>
		private double ClientWidth 
		{
			get
			{
				return( m_dblClientWidth ) ;
			}	
			set
			{
				m_dblClientWidth=value;
			}
		}
		
		/// <summary>
		/// Read/Write property for CurrentX.
		/// </summary>
		private double CurrentX 
		{
			get
			{
				return( m_dblCurrentX ) ;
			}
			set
			{
				m_dblCurrentX = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CurrentY.
		/// </summary>
		private double CurrentY 
		{
			get
			{
				return( m_dblCurrentY ) ;
			}
			set
			{
				m_dblCurrentY = value ;
			}
		}		
		/// <summary>
		/// Gets unique filename
		/// </summary>
		private string TempFile
		{
            get
            {
                return FundManager.GetPaymentFilePath();
            }
		}
		#endregion

		#region Public Methods

		/// Name		: GetCutomPaymentNotification
		/// Author		: Mihika Agrawal
		/// Date Created: 08/08/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the for cutom payment notification depending upon the criteria selected -
		/// due by - week or month/date range
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetCutomPaymentNotification(XmlDocument p_objXmlDoc)
		{
			DbReader objReader = null;
			XmlElement objRootNode = null;
			XmlElement objRowNode = null;
			XmlElement objElement = null;
			StringBuilder sbSqlFunds = new StringBuilder();
			StringBuilder sbSqlAuto = new StringBuilder();

			int iCurrentDay = 0;
			int iTotalCount = 0;
			double dblTotalAmt = 0.0;
			double dblAmount = 0.0;

			string sDueBy = p_objXmlDoc.SelectSingleNode("//DueBy").InnerText;
			string sEndOf = p_objXmlDoc.SelectSingleNode("//EndOf").InnerText;
			string sFromDate = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//FromDate").InnerText);
			string sToDate = Conversion.GetDate(p_objXmlDoc.SelectSingleNode("//ToDate").InnerText);
			string sEndDate = string.Empty;

			if (sDueBy.Trim() == "")
			{
				sDueBy = "WeekMonth";
				sEndOf = "Week";
			}

			try
			{
				//Query for the Funds Table
				sbSqlFunds.Append("SELECT FUNDS.TRANS_DATE | CHECK_DATE, FUNDS.CTL_NUMBER, FUNDS.FIRST_NAME, FUNDS.LAST_NAME, ");
				sbSqlFunds.Append("FUNDS.AMOUNT, FUNDS.CLAIM_NUMBER ");
				sbSqlFunds.Append("FROM FUNDS, CLAIM ");
				sbSqlFunds.Append("WHERE FUNDS.CLAIM_ID = CLAIM.CLAIM_ID AND FUNDS.STATUS_CODE <> " + GetCodeIDWithShort(GetTableId("CHECK_STATUS").ToString() ,"P"));
				sbSqlFunds.Append(" AND FUNDS.PAYMENT_FLAG <> 0 AND FUNDS.VOID_FLAG = 0 ");

				// Query for the Funds_Auto Table.
				sbSqlAuto.Append("SELECT FUNDS_AUTO.PRINT_DATE | CHECK_DATE, FUNDS_AUTO.CTL_NUMBER, FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, ");
				sbSqlAuto.Append("FUNDS_AUTO.AMOUNT, FUNDS_AUTO.CLAIM_NUMBER ");
				sbSqlAuto.Append("FROM FUNDS_AUTO, CLAIM ");
				sbSqlAuto.Append("WHERE FUNDS_AUTO.CLAIM_ID = CLAIM.CLAIM_ID ");

				iCurrentDay = DateTime.Now.DayOfWeek.GetHashCode();
				if (sDueBy == "WeekMonth")
				{
					if (sEndOf == "Week") // If end of week
						sEndDate = Conversion.ToDbDate(DateTime.Today.AddDays(6 - iCurrentDay));
					else if (sEndOf == "Month") // If end of month
					{
						sEndDate = Conversion.ToDbDate(DateTime.Today);
						sEndDate = sEndDate.Substring(0, 6) + "31";
					}

					sbSqlAuto.Append("AND FUNDS_AUTO.PRINT_DATE <= '" + sEndDate + "'");
					sbSqlFunds.Append("AND FUNDS.TRANS_DATE <= '" + sEndDate + "'");
				}
				else if (sDueBy == "DateRange")
				{
					sbSqlAuto.Append("AND FUNDS_AUTO.PRINT_DATE BETWEEN '" + sFromDate + "' AND '" + sToDate + "'");
					sbSqlFunds.Append("AND FUNDS.TRANS_DATE BETWEEN '" + sFromDate + "' AND '" + sToDate + "'");
				}

				sbSqlAuto.Append(" ORDER BY CHECK_DATE");
				sbSqlFunds.Append(" ORDER BY CHECK_DATE");

				sbSqlFunds = sbSqlFunds.Replace("|", " ");
				sbSqlAuto = sbSqlAuto.Replace("|", " ");

				DataTable dt = new DataTable();
				dt.Columns.Add("CHECK_DATE", typeof(DateTime));
				dt.Columns.Add("CTL_NUMBER", typeof(string));
				dt.Columns.Add("FIRST_NAME", typeof(string));
				dt.Columns.Add("LAST_NAME", typeof(string));
				dt.Columns.Add("CLAIM_NUMBER", typeof(string));
				dt.Columns.Add("AMOUNT", typeof(string));

				objReader = DbFactory.GetDbReader(m_sConnectionString, sbSqlAuto.ToString());
				if (objReader != null)
				{
					while(objReader.Read())
					{
						dblAmount = objReader.GetDouble("AMOUNT");
						dblTotalAmt += dblAmount;

						dt.Rows.Add(
							new object[] {Conversion.GetDBDateFormat(objReader.GetString("CHECK_DATE"), "d"),
											 objReader.GetString("CTL_NUMBER"), 
											 objReader.GetString("FIRST_NAME"), 
											 objReader.GetString("LAST_NAME"), 
											 objReader.GetString("CLAIM_NUMBER"), 
											 string.Format("{0:C}", dblAmount)});

						iTotalCount ++;
						if (iTotalCount >= 1000)
							break;
					}
				}
				objReader = null;
				dt.AcceptChanges();

				if (iTotalCount < 1000)
				{
					objReader = DbFactory.GetDbReader(m_sConnectionString, sbSqlFunds.ToString());
					if (objReader != null)
					{
						while(objReader.Read())
						{
							dblAmount = objReader.GetDouble("AMOUNT");
							dblTotalAmt += dblAmount;

							dt.Rows.Add(
								new object[] {Conversion.GetDBDateFormat(objReader.GetString("CHECK_DATE"), "d"),
												 objReader.GetString("CTL_NUMBER"), 
												 objReader.GetString("FIRST_NAME"), 
												 objReader.GetString("LAST_NAME"), 
												 objReader.GetString("CLAIM_NUMBER"), 
												 string.Format("{0:C}", dblAmount)});

							iTotalCount ++;
							if (iTotalCount >= 1000)
								break;
						}
					}
				}

				string strExpr = "";
				string strSort;
    
				strSort = "CHECK_DATE ASC";
				// Use the Select method to find all rows matching the filter.
				DataRow[] foundRows = 
					dt.Select(strExpr, strSort, DataViewRowState.CurrentRows);

				objRootNode = (XmlElement)p_objXmlDoc.SelectSingleNode("//PaymentNotification");

				foreach( DataRow r in foundRows )
				{
					objRowNode = objRootNode.OwnerDocument.CreateElement("Row");
					
					// Node for Check Date
					objElement = objRowNode.OwnerDocument.CreateElement("CheckDate");
					objElement.InnerText = Conversion.GetDBDateFormat(Conversion.GetDate(r["CHECK_DATE"].ToString()), "d");
					objRowNode.AppendChild(objElement);

					// Node for Control Number
					objElement = objRowNode.OwnerDocument.CreateElement("ControlNumber");
					objElement.InnerText = r["CTL_NUMBER"].ToString();
					objRowNode.AppendChild(objElement);

					// Node for Payee
					objElement = objRowNode.OwnerDocument.CreateElement("Payee");
					objElement.InnerText = r["FIRST_NAME"].ToString() + " " + r["LAST_NAME"].ToString();
					objRowNode.AppendChild(objElement);

					// Node for Claim Number	
					objElement = objRowNode.OwnerDocument.CreateElement("ClaimNumber");
					objElement.InnerText = r["CLAIM_NUMBER"].ToString();
					objRowNode.AppendChild(objElement);

					// Node for Amount
					objElement = objRowNode.OwnerDocument.CreateElement("Amount");
					objElement.InnerText = r["AMOUNT"].ToString();
					objRowNode.AppendChild(objElement);

					objRootNode.AppendChild(objRowNode);
				}
		
				objRootNode = (XmlElement)p_objXmlDoc.SelectSingleNode("//TotalAmount");
				objRootNode.InnerText = string.Format("{0:C}", dblTotalAmt);

				objRootNode = (XmlElement)p_objXmlDoc.SelectSingleNode("//TotalNumChecks");
				if (iTotalCount <= 1000)
					objRootNode.InnerText = iTotalCount.ToString();
				else
					objRootNode.InnerText = "First 1000";

			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.Error", m_iClientId), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                
				objRootNode = null;
				objRowNode = null;
				objElement = null;
			}

			return p_objXmlDoc;
		}

		/// Name		: NotifyUserOfFuturePayments
		/// Author		: Raman Bhatia
		/// Date Created: 08/16/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the startup payment notification 
		/// </summary>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument NotifyUserOfFuturePayments(int p_iUserId)
		{
			DbReader objReader = null;
			XmlElement objRootNode = null;
			XmlElement objRowNode = null;
			XmlElement objElement = null;
			XmlDocument objXmlDoc = null;

			int iTotalCount = 0;
			int iCurrentDay = 0;
			int iLOBCount = 0;
			int iNotifyType = 0;
			int iLOB = 0;
			int iprevLOB = 0;
			int iNotifyDay = 0;
			double dblTotalAmt = 0.0;
			double dblAmount = 0.0;
			string []arrDateSQL = null;
			string []arrLOB = null;
			string sSQL = "";
			string sCmpDate = "";
			string sSqlFunds = "";
			string sSqlAuto = "";
			bool bFlag = false;
			bool bFirstRecordRead = false;
			bool bRecordExists = false;


			try
			{
				objXmlDoc = new XmlDocument();
				objRootNode = objXmlDoc.CreateElement("PaymentNotification");
				objXmlDoc.AppendChild(objRootNode);
				
				arrDateSQL = new string[100];
				arrLOB = new string[100];

                sSQL = String.Format("SELECT * FROM PAY_NOTIFY_USERS WHERE USER_ID = {0} ORDER BY LINE_OF_BUS_CODE,NOTIFY_TYPE", p_iUserId);

                using(objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
	            {
                    if (objReader == null)
                        return (objXmlDoc);

                    iCurrentDay = Conversion.ConvertObjToInt(System.DateTime.Now.DayOfWeek, m_iClientId);

                    while (objReader.Read())
                    {
                        bRecordExists = true;

                        if (bFirstRecordRead == false)
                        {
                            iprevLOB = Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                            arrDateSQL[0] = "";
                            arrLOB[0] = iprevLOB.ToString();
                            bFirstRecordRead = true;
                        }


                        iLOB = Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                        if (iLOB != iprevLOB)
                        {
                            iLOBCount = iLOBCount + 1;
                            arrDateSQL[iLOBCount] = "";
                            iprevLOB = iLOB;
                            arrLOB[iLOBCount] = iLOB.ToString();
                        }

                        iNotifyType = Conversion.ConvertObjToInt(objReader.GetValue("NOTIFY_TYPE"), m_iClientId);
                        iNotifyDay = iNotifyType % 10;

                        //Arnab: MITS-8489 - Modified (iNotifyDay == (iCurrentDay - 1) to iNotifyDay == (iCurrentDay - 1)
                        //As it was not showing the day dependancy for Payment Notification
                        //if ((iNotifyDay == 0) || (iNotifyDay == (iCurrentDay - 1)))
                        if ((iNotifyDay == 0) || (iNotifyDay == iCurrentDay))
                        {
                            if (iNotifyType > 30)
                                sCmpDate = String.Format("{0:yyyy}", DateTime.Now) + String.Format("{0:MM}", DateTime.Now) + "31";
                            else if (iNotifyType >= 20)
                                sCmpDate = String.Format("{0:yyyy}", DateTime.Now) + String.Format("{0:MM}", DateTime.Now) + String.Format("{0:dd}", DateTime.Now.AddDays(7 - iCurrentDay));
                            else
                                sCmpDate = String.Format("{0:yyyy}", DateTime.Now) + String.Format("{0:MM}", DateTime.Now) + String.Format("{0:dd}", DateTime.Now);

                            if (Conversion.ConvertStrToInteger(sCmpDate) > Conversion.ConvertStrToInteger(arrDateSQL[iLOBCount]))
                                arrDateSQL[iLOBCount] = sCmpDate;
                        }
                    }    
	            }//using
				
				

				if (bRecordExists==false)
					return (objXmlDoc);
				
				sSqlFunds = "SELECT FUNDS.TRANS_DATE | CHECK_DATE,FUNDS.CTL_NUMBER,FUNDS.FIRST_NAME,FUNDS.LAST_NAME,FUNDS.AMOUNT,FUNDS.CLAIM_NUMBER FROM FUNDS,CLAIM WHERE FUNDS.CLAIM_ID = CLAIM.CLAIM_ID AND FUNDS.STATUS_CODE <> " + GetCodeIDWithShort(GetTableId("CHECK_STATUS").ToString() ,"P");
				sSqlFunds = sSqlFunds + " AND FUNDS.PAYMENT_FLAG <> 0 AND FUNDS.VOID_FLAG = 0 ";
				sSqlAuto = "SELECT FUNDS_AUTO.PRINT_DATE | CHECK_DATE,FUNDS_AUTO.CTL_NUMBER,FUNDS_AUTO.FIRST_NAME,FUNDS_AUTO.LAST_NAME,FUNDS_AUTO.AMOUNT,FUNDS_AUTO.CLAIM_NUMBER FROM FUNDS_AUTO,CLAIM WHERE FUNDS_AUTO.CLAIM_ID = CLAIM.CLAIM_ID ";

				for (int i=0; i <= iLOBCount ; i++)
					if (arrDateSQL[i] != "") bFlag = true;

				if(bFlag==false)
					return(objXmlDoc);
		
				bRecordExists = false;
				for (int i=0; i<=iLOBCount;i++)
				{
					if (i == 0)  
					{
						sSqlAuto = sSqlAuto + " AND (";
						sSqlFunds = sSqlFunds + " AND (";
					}
					else
					{
						if (arrDateSQL[i] != "") 
						{
							if(bRecordExists)
							{
								sSqlAuto = sSqlAuto + " OR ";
								sSqlFunds = sSqlFunds + " OR ";
							}
						}
					}
   
					if(arrDateSQL[i] != "")
					{
						bRecordExists = true;
						sSqlAuto = sSqlAuto + " ((CLAIM.LINE_OF_BUS_CODE = " + arrLOB[i] + ") AND (FUNDS_AUTO.PRINT_DATE <='" + arrDateSQL[i] + "'))";
						sSqlFunds = sSqlFunds + " ((CLAIM.LINE_OF_BUS_CODE = " +  arrLOB[i] + ") AND (FUNDS.TRANS_DATE <='" + arrDateSQL[i] + "'))";
					}
				}

				sSqlAuto = sSqlAuto + ")";
				sSqlFunds = sSqlFunds + ")";
					
				sSqlAuto = sSqlAuto + " ORDER BY CHECK_DATE";
				sSqlFunds = sSqlFunds + " ORDER BY CHECK_DATE";

				sSqlFunds = sSqlFunds.Replace("|", " ");
				sSqlAuto = sSqlAuto.Replace("|", " ");

				DataTable dt = new DataTable();
				dt.Columns.Add("CHECK_DATE", typeof(DateTime));
				dt.Columns.Add("CTL_NUMBER", typeof(string));
				dt.Columns.Add("FIRST_NAME", typeof(string));
				dt.Columns.Add("LAST_NAME", typeof(string));
				dt.Columns.Add("CLAIM_NUMBER", typeof(string));
				dt.Columns.Add("AMOUNT", typeof(string));

				using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSqlAuto))
				{
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            dblAmount = objReader.GetDouble("AMOUNT");
                            dblTotalAmt += dblAmount;

                            dt.Rows.Add(
                                new object[] {Conversion.GetDBDateFormat(objReader.GetString("CHECK_DATE"), "d"),
											 objReader.GetString("CTL_NUMBER"), 
											 objReader.GetString("FIRST_NAME"), 
											 objReader.GetString("LAST_NAME"), 
											 objReader.GetString("CLAIM_NUMBER"), 
											 string.Format("{0:C}", dblAmount)});

                            iTotalCount++;
                            if (iTotalCount >= 1000)
                                break;
                        }
                    }
				} // using

				dt.AcceptChanges();

				if (iTotalCount < 1000)
				{
					using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSqlFunds))
					{
                        if (objReader != null)
                        {
                            while (objReader.Read())
                            {
                                dblAmount = objReader.GetDouble("AMOUNT");
                                dblTotalAmt += dblAmount;

                                dt.Rows.Add(
                                    new object[] {Conversion.GetDBDateFormat(objReader.GetString("CHECK_DATE"), "d"),
												 objReader.GetString("CTL_NUMBER"), 
												 objReader.GetString("FIRST_NAME"), 
												 objReader.GetString("LAST_NAME"), 
												 objReader.GetString("CLAIM_NUMBER"), 
												 string.Format("{0:C}", dblAmount)});

                                iTotalCount++;
                                if (iTotalCount >= 1000)
                                    break;
                            }
                        }
					} // using
				}//if

                string strExpr = string.Empty;
				string strSort;
    
				strSort = "CHECK_DATE ASC";
				// Use the Select method to find all rows matching the filter.
				DataRow[] foundRows = 
					dt.Select(strExpr, strSort, DataViewRowState.CurrentRows);

				objRootNode = (XmlElement)objXmlDoc.SelectSingleNode("//PaymentNotification");

				foreach( DataRow r in foundRows )
				{
					objRowNode = objRootNode.OwnerDocument.CreateElement("Row");
					
					// Node for Check Date
					objElement = objRowNode.OwnerDocument.CreateElement("CheckDate");
					objElement.InnerText = Conversion.GetDBDateFormat(Conversion.GetDate(r["CHECK_DATE"].ToString()), "d");
					objRowNode.AppendChild(objElement);

					// Node for Control Number
					objElement = objRowNode.OwnerDocument.CreateElement("ControlNumber");
					objElement.InnerText = r["CTL_NUMBER"].ToString();
					objRowNode.AppendChild(objElement);

					// Node for Payee
					objElement = objRowNode.OwnerDocument.CreateElement("Payee");
					objElement.InnerText = r["FIRST_NAME"].ToString() + " " + r["LAST_NAME"].ToString();
					objRowNode.AppendChild(objElement);

					// Node for Claim Number	
					objElement = objRowNode.OwnerDocument.CreateElement("ClaimNumber");
					objElement.InnerText = r["CLAIM_NUMBER"].ToString();
					objRowNode.AppendChild(objElement);

					// Node for Amount
					objElement = objRowNode.OwnerDocument.CreateElement("Amount");
					objElement.InnerText = r["AMOUNT"].ToString();
					objRowNode.AppendChild(objElement);

					objRootNode.AppendChild(objRowNode);
				}
		
				objRootNode = objXmlDoc.CreateElement("TotalAmount");
				objXmlDoc.SelectSingleNode("//PaymentNotification").AppendChild(objRootNode);

				objRootNode = objXmlDoc.CreateElement("TotalNumChecks");
				objXmlDoc.SelectSingleNode("//PaymentNotification").AppendChild(objRootNode);
				
				objRootNode = (XmlElement)objXmlDoc.SelectSingleNode("//TotalAmount");
				objRootNode.InnerText = string.Format("{0:C}", dblTotalAmt);

				objRootNode = (XmlElement)objXmlDoc.SelectSingleNode("//TotalNumChecks");
				if (iTotalCount <= 1000)
					objRootNode.InnerText = iTotalCount.ToString();
				else
					objRootNode.InnerText = "First 1000";
			}

			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.NotifyUserOfFuturePayments.Error", m_iClientId), p_objEx);
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objRootNode = null;
				objRowNode = null;
				objElement = null;
			}

			return (objXmlDoc);
		}

		/// Name		: CreateNotificationReport
		/// Author		: Mihika Agrawal
		/// Date Created: 08/12/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Creates report based on passed XML document
		/// </summary>
		/// <param name="p_objXmlDoc">The input XML document</param>
		/// <param name="p_objMemory">Memory object containing report</param>
		/// <returns>Success: 1 or Failure: 0</returns>
		public int CreateNotificationReport(XmlDocument p_objXmlDoc, out MemoryStream p_objMemory)
		{
			double dblCharWidth = 0.0;
			double dblLineHeight = 0.0;
			double dblLeftMargin = 10.0;
			double dblRightMargin = 10.0;
			double dblTopMargin = 10.0;
			double dblPageWidth = 0.0;
			double dblPageHeight = 0.0;
			double dblBottomHeader = 0.0;
			double dblCurrentY = 0.0;
			double dblCurrentX = 0.0;	
			double iCounter = 0.0;
			double[] arrColWidth = null;
			int iReturnValue = 0;

			string sHeader = string.Empty;
			string sFile = string.Empty;
			string sSQL = string.Empty;
			string sNumChecks = string.Empty;
			string[] arrColAlign = null;
			string[] arrCol = null;

			p_objMemory = null;

			try
			{
				arrColAlign = new string[5] ;
				arrColWidth = new double[5] ;	
				arrCol = new string[5];

				sNumChecks = p_objXmlDoc.SelectSingleNode("//TotalNumChecks").InnerText;

				StartDoc(true, 10, 10, 10, 10);				
				SetFontBold(true);
				ClientWidth = 15400;

				dblCharWidth = GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ")/40;
				dblLineHeight = GetTextHeight("W");
				dblLeftMargin = dblCharWidth * 2;
				dblRightMargin = dblCharWidth * 2;
			
				dblTopMargin = dblLineHeight * 5;
				dblPageWidth = PageWidth - dblLeftMargin - dblRightMargin;
				dblPageHeight = PageHeight;
				dblBottomHeader = dblPageHeight - (5 * dblLineHeight);
			

				arrColWidth[0] = dblCharWidth * 20;// Check Date
				arrColWidth[1] = dblCharWidth * 30;// Control Number
				arrColWidth[2] = dblCharWidth * 40;// Payee Name
				arrColWidth[3] = dblCharWidth * 30;// Claim Number
				arrColWidth[4] = dblCharWidth * 20;// Payment Amount

				dblCurrentX = dblLeftMargin;
				dblCurrentY = 900;

				CreateNewPage(m_sConnectionString, true, sNumChecks);
			
				arrColAlign[0] = LEFT_ALIGN ; //Check Date
				arrColAlign[1] = LEFT_ALIGN ; //Control Number
				arrColAlign[2] = LEFT_ALIGN ; // Payee Name
				arrColAlign[3] = LEFT_ALIGN ; // Claim Number
				arrColAlign[4] = RIGHT_ALIGN ; // Payment Amount

				CurrentX = dblLeftMargin;
				CurrentY = dblCurrentY;

				#region Intialize array for heading
                arrCol[0] = Globalization.GetString("PaymentNotification.Heading.CheckDate", m_iClientId);
                arrCol[1] = Globalization.GetString("PaymentNotification.Heading.ControlNumber", m_iClientId);
                arrCol[2] = Globalization.GetString("PaymentNotification.Heading.Payee", m_iClientId);
                arrCol[3] = Globalization.GetString("PaymentNotification.Heading.ClaimNumber", m_iClientId);
                arrCol[4] = Globalization.GetString("PaymentNotification.Heading.Amount", m_iClientId);
				
				#endregion
				//Print header
				PrintHeader(ref dblCurrentY, 5, dblPageWidth, dblLeftMargin, dblLineHeight, arrCol, arrColWidth, arrColAlign);

				SetFontBold(false);					
				dblCurrentY = 1200;
				iCounter = 0;

				XmlNodeList objRows = (XmlNodeList)p_objXmlDoc.SelectNodes("//PaymentNotification/Row");
			
				for (int i = 0; i < objRows.Count; i++)
				{
					CreateRow(objRows.Item(i), dblLeftMargin, ref dblCurrentY, arrColAlign, 
						dblLeftMargin, dblLineHeight, arrColWidth, dblCharWidth);
					iCounter ++;
					if (iCounter == NO_OF_ROWS)
					{
						CreateNewPage(m_sConnectionString, false, sNumChecks);
						iCounter = 0;
						dblCurrentY = 900;
					}
				}

				// Printing the Summary
				if ((iCounter + 3) > NO_OF_ROWS)
				{
					CreateNewPage(m_sConnectionString, false, sNumChecks);
					dblCurrentY = 900;
					iCounter = 0;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight;
				}

				SetFontBold(true);
				CurrentX = dblLeftMargin;
				CurrentY = dblCurrentY;
                PrintTextNoCr(Globalization.GetString("PaymentNotification.Summary.TotalAmount", m_iClientId) + p_objXmlDoc.SelectSingleNode("//PaymentNotification/TotalAmount").InnerText);

				// Printing the Footer
				if ((iCounter + 7) > NO_OF_ROWS)
				{
					CreateNewPage(m_sConnectionString, false, sNumChecks);
					dblCurrentY = 900;
				}
				else
				{
					dblCurrentY = dblCurrentY + dblLineHeight * 2;
				}

				CreateFooter(dblLeftMargin, dblCurrentY, dblLineHeight, 250);
				EndDoc();
				sFile = TempFile;
				Save(sFile);
                p_objMemory = Utilities.ConvertFilestreamToMemorystream(sFile, m_iClientId);

				File.Delete(sFile);
				iReturnValue = 1;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.CreateReport.Error", m_iClientId), p_objException);
			}
			finally
			{
				arrColAlign = null;
				arrColWidth = null;	
				m_objPrintDoc = null;
				m_objFont = null;
				m_objText = null;
			}
			return iReturnValue;
		}
		#endregion

		#region Private Functions
	
		/// <summary>
		/// GetTableId returns the TABLE_ID for a particular SYSTEM_TABLE_NAME in the GLOSSARY table.
		/// </summary>
		/// <param name="p_sTableName">SYSTEM_TABLE_NAME for which the TABLE_ID is required</param>
		/// <returns>Table Id</returns>
		private int GetTableId(string p_sTableName )
		{
			DbReader objReader = null ;

			int iTableId = 0 ;
			string sSQL = "" ;
			
			try
			{
				if( p_sTableName == "" )
					return(0) ;

				sSQL = " SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME='" + p_sTableName + "'" ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if ( objReader != null )
					if( objReader.Read() )				
						iTableId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TABLE_ID" ), m_iClientId );					
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FundManager.GetTableId.ErrorTableID", m_iClientId), p_objEx);
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( iTableId );
		} 

		/// <summary>
		/// GetCodeId returns the CODE_ID for a particular TABLE_ID and a particular short_code.
		/// </summary>
		/// <param name="p_sTableId">TABLE_ID for which the CODE_ID is required</param>
		/// <param name="p_sShortCode">SHORT_CODE</param>
		/// <returns>Code Id</returns>
		private int GetCodeIDWithShort(string p_sTableId , string p_sShortCode)
		{
			DbReader objReader = null ;

			int iCodeId = 0;
			string sSQL = "" ;
			
			try
			{
				if( p_sShortCode == "" )
					return(0) ;

				sSQL = "SELECT CODE_ID FROM CODES WHERE SHORT_CODE = '" + p_sShortCode + "' AND TABLE_ID=" + p_sTableId;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if ( objReader != null )
					if( objReader.Read() )				
						iCodeId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "CODE_ID" ), m_iClientId );					
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("FundManager.GetTableId.ErrorTableID", m_iClientId), p_objEx);
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
			}
			return( iCodeId );
		}

		/// Name		: StartDoc
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_bIsLandscape">Landscape or Portrait</param>
		/// <param name="p_Left">Left</param>
		/// <param name="p_Right">Right</param>
		/// <param name="p_Top">Top</param>
		/// <param name="p_Bottom">Bottom</param>
		private void StartDoc(bool p_bIsLandscape, int p_Left, int p_Right, int p_Top, int p_Bottom)
		{		
			try
			{
				m_objPrintDoc = new C1PrintDocument();
				m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
				// m_objPrintDoc.PageSettings.PrinterSettings.PrinterName = RMConfigurator.Value("DefaultPrinterName").ToString();

				m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape ;
				m_objPrintDoc.PageSettings.Margins.Left = p_Left;
				m_objPrintDoc.PageSettings.Margins.Right = p_Right;
				m_objPrintDoc.PageSettings.Margins.Top = p_Top;
				m_objPrintDoc.PageSettings.Margins.Bottom = p_Bottom;

				m_objPrintDoc.StartDoc();

				// Causes access to default printer     objRect = m_objPrintDoc.PageSettings.Bounds;
				m_dblPageHeight = 11 * 1440;
				m_dblPageWidth = 8.5 * 1440;
				if(p_bIsLandscape)
					m_dblPageHeight += 700 ;
				else
					m_dblPageWidth += 700 ;
			
				m_objFont = new Font("Arial", 7);		
				m_objText = new RenderText(m_objPrintDoc);			
				m_objText.Style.Font = m_objFont ;
				m_objText.Style.WordWrap = false ;
				m_objText.AutoWidth=true;			
			}
			catch( RMAppException p_objEx )
			{								
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.StartDoc.Error", m_iClientId), p_objEx);				
			}				
		}

		/// Name		: SetFont
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Set Font 
		/// </summary>
		/// <param name="p_sFontName">Font Name</param>
		/// <param name="p_dblFontSize">Font Size</param>
		private void SetFont( string p_sFontName , double p_dblFontSize )
		{
			try
			{
				m_objFont = new Font( p_sFontName , (float)p_dblFontSize );
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.SetFont.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: SetFontBold
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// Set font bold
		/// </summary>
		/// <param name="p_bBold">Bool flag</param>
		private void SetFontBold( bool p_bBold )
		{		
			try
			{
				if( p_bBold )
					m_objFont = new Font( m_objFont.Name , m_objFont.Size , FontStyle.Bold );
				else	
					m_objFont = new Font( m_objFont.Name , m_objFont.Size );
				
				m_objText.Style.Font = m_objFont ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.SetFontBold.Error", m_iClientId), p_objEx);
			}
		}

		/// Name		: GetTextWidth
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get text width.
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Width</returns>
		private double GetTextWidth( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundWidth );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.GetTextWidth.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: GetTextHeight
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///***************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Text Height
		/// </summary>
		/// <param name="p_sText">String Text</param>
		/// <returns>Height</returns>
		private double GetTextHeight( string p_sText )
		{
			try
			{
				m_objText.Text = p_sText + "" ;
				return( m_objText.BoundHeight );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.GetTextHeight.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: CreateNewPage
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a new page in a report
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		/// <param name="p_bFirstPage">First page or not</param>
		/// <param name="p_sNumChecks">Number of Checks that need to be printed</param>
		private void CreateNewPage(string p_sDSN, bool p_bFirstPage, string p_sNumChecks)
		{
			SysParms objSysSetting = null;
		
			RenderText m_objText = null;
			try
			{
				if (!p_bFirstPage)
				{
					m_objPrintDoc.NewPage();
				}
				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 11700);

				m_objPrintDoc.RenderDirectRectangle(0, 0, 15600, 500, Color.Black, 1, Color.LightGray);
				m_objPrintDoc.RenderDirectRectangle(0, 11300, 15600, 11900, Color.Black, 1, Color.LightGray);
				m_objText = new RenderText(m_objPrintDoc);	
				m_objText.Text = " ";
				m_objText.AutoWidth=true;
				CurrentX = m_objText.BoundWidth;

                m_objText.Text = p_sNumChecks + " " + Globalization.GetString("PaymentNotification.CreateReport.Caption", m_iClientId);
				m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.Width, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Left);
				m_objText = new RenderText(m_objPrintDoc);

                objSysSetting = new SysParms(p_sDSN, m_iClientId);
				m_objText.Text = objSysSetting.SysSettings.ClientName + "  " + Conversion.GetDBDateFormat(Conversion.ToDbDate(DateTime.Now), "d") + "  ";
				objSysSetting = null;
				CurrentX = (ClientWidth - m_objText.BoundWidth);

				m_objPrintDoc.RenderDirectText(CurrentX, 100, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Right);
				m_objText = new RenderText(m_objPrintDoc);

                m_objText.Text = Globalization.GetString("PaymentNotification.CreateReport.Page", m_iClientId) + " [@@PageNo@@]";
				CurrentX = (PageWidth - m_objText.BoundWidth)/2;
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold), Color.Black, AlignHorzEnum.Center);

                //m_objText.Text = Globalization.GetString("PaymentNotification.CreateReport.ConfidentialData") + " - " + RMConfigurator.Text("message", "id", "DEF_RMCAPTION") + "  ";
                m_objText.Text = String.Format("{0} - {1}  ", Globalization.GetString("PaymentNotification.CreateReport.ConfidentialData", m_iClientId), m_MessageSettings["DEF_RMCAPTION"]);

				CurrentX = (ClientWidth - m_objText.BoundWidth);
				m_objPrintDoc.RenderDirectText(CurrentX, 11600, m_objText.Text, m_objText.BoundWidth, new Font("Arial", 11, FontStyle.Bold|FontStyle.Italic), Color.Black, AlignHorzEnum.Right);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.CreateNewPage.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				objSysSetting = null;
				m_objText = null;
			}
		}

		/// Name		: PrintText
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Render Text on the document.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintText( string p_sPrintText )
		{
			try
			{
				m_objText.Text = p_sPrintText + "\r" ;	
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect( m_dblCurrentX , m_dblCurrentY , m_objText );			
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.PrintText.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: PrintTextNoCr
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// Render Text on the document with no "\r" on the end.
		/// </summary>
		/// <param name="p_sPrintText">String text to print</param>
		private void PrintTextNoCr(string p_sPrintText)
		{
			try
			{
				m_objText.Text = p_sPrintText + "";			
				m_objText.AutoWidth = true;
				m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);						
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.PrintTextNoCr.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: Save
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the C1PrintDocument in PDF format.
		/// </summary>
		/// <param name="p_sPath">Path</param>
		private void Save(string p_sPath)
		{
			try
			{
				m_objPrintDoc.ExportToPDF(p_sPath ,false );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.Save.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: EndDoc
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Call the end event.
		/// </summary>
		private void EndDoc()
		{
			try
			{
				m_objPrintDoc.EndDoc();
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.EndDoc.Error", m_iClientId), p_objEx);				
			}
		}

		/// Name		: PrintHeader
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Prints report header.
		/// </summary>
		/// <param name="p_dblCurrentY">Current Y</param>
		/// <param name="p_iColumnCount">Columns Count</param>
		/// <param name="p_dblPageWidth">Page Width</param>
		/// <param name="p_dblLeftMargin">Left Margin</param>
		/// <param name="p_dblLineHeight">Line Height</param>
		/// <param name="p_arrCol">Columns Text</param>
		/// <param name="p_arrColWidth">Columns Width</param>
		/// <param name="p_arrColAlign">Columns Alignment.</param>
		private void PrintHeader( ref double p_dblCurrentY , int p_iColumnCount , double p_dblPageWidth , 
			double p_dblLeftMargin , double p_dblLineHeight , string[] p_arrCol , 
			double[] p_arrColWidth , string[] p_arrColAlign )
		{
			double dblCurrentX = 0.0 ;
			int iIndex = 0 ;
				
			try
			{				
				dblCurrentX = p_dblLeftMargin ;
				for( iIndex = 0 ; iIndex < p_iColumnCount ; iIndex++ )
				{
					CurrentY = p_dblCurrentY;
					if( p_arrColAlign[iIndex] == LEFT_ALIGN )
					{
						CurrentX = dblCurrentX ;
						PrintText( p_arrCol[iIndex] );
					}
					else
					{
						CurrentX = dblCurrentX + p_arrColWidth[iIndex]
							- GetTextWidth( p_arrCol[iIndex] );
						PrintText( p_arrCol[iIndex] );
					}
					
					dblCurrentX += p_arrColWidth[iIndex] ;
				}
				p_dblCurrentY += p_dblLineHeight * 2 ;

			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.PrintHeader.Error", m_iClientId), p_objException);
			}						
		}

		/// Name		: CreateRow
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates a record in a report
		/// </summary>
		/// <param name="p_objData">DataRow object containing a row data</param>
		/// <param name="p_dblCurrentX">Current X position</param>
		/// <param name="p_dblCurrentY">Current Y position</param>
		/// <param name="p_arrColAlign">Columns alignment</param>
		/// <param name="p_dblLeftMargin">Left margin</param>
		/// <param name="p_dblLineHeight">Line height</param>
		/// <param name="p_arrColWidth">Column width</param>
		/// <param name="p_dblCharWidth">Character width</param>
		/// <param name="p_bLeadingLine">Leading Line Flag</param>
		/// <param name="p_bCalTotal">Calculate Total Flag</param>
		/// <param name="p_dblTotalAll">Over all Total</param>
		/// <param name="p_dblTotalPay">Total Payments</param>
		/// <param name="p_dblTotalCollect">Total Collections</param>
		/// <param name="p_dblTotalVoid">Total Voids</param>
		private void CreateRow(XmlNode p_objData, double p_dblCurrentX,
			ref double p_dblCurrentY, string[] p_arrColAlign, double p_dblLeftMargin, double p_dblLineHeight,
			double[] p_arrColWidth, double p_dblCharWidth)
		{
			string sData = "";
//			LocalCache objLocalCache = null;
			string sCheckStatusCode = string.Empty;
			string sCheckStatusDesc = string.Empty;
			XmlNodeList objNodes = null;

			try
			{
				CurrentX = p_dblCurrentX;
				CurrentY = p_dblCurrentY;
//				objLocalCache = new LocalCache(m_sConnectionString);

				objNodes = p_objData.ChildNodes;
				for (int iIndexJ = 0; iIndexJ < 5; iIndexJ ++)
				{
					sData = objNodes[iIndexJ].InnerText;
					if (sData == "")
						sData = " ";
					while(GetTextWidth(sData) > p_arrColWidth[iIndexJ])
						sData = sData.Substring(0 ,sData.Length - 1);

					// Print using alignment
					if(p_arrColAlign[iIndexJ] == LEFT_ALIGN)
					{
						CurrentX = p_dblCurrentX;
						PrintTextNoCr(" " + sData);
					}
					else
					{
						CurrentX = p_dblCurrentX + p_arrColWidth[iIndexJ] 
							- GetTextWidth(sData) - p_dblCharWidth ;
						PrintTextNoCr(sData);
					}
					p_dblCurrentX += p_arrColWidth[iIndexJ];
				}

				p_dblCurrentX = p_dblLeftMargin;
				p_dblCurrentY += p_dblLineHeight;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.CreateRow.Error", m_iClientId), p_objException);
			}
		}

		/// Name		: CreateFooter
		/// Author		: Mihika Agrawal
		/// Date Created: 08/01/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates Report Footer
		/// </summary>
		/// <param name="p_dCurrentX">X Position</param>
		/// <param name="p_dCurrentY">Y Position</param>
		/// <param name="p_dHeight">Height</param>
		/// <param name="p_dblSpace">Space between 2 words</param>
		private void CreateFooter(double p_dCurrentX, double p_dCurrentY, double p_dHeight, double p_dblSpace)
		{
			try
			{
				m_objText.Style.Font=new Font("Arial", 7f, FontStyle.Underline|FontStyle.Bold);
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_NOTE");
                m_objText.Text = m_MessageSettings["DEF_NOTE"].ToString();
				p_dCurrentY = p_dCurrentY + p_dHeight;
				m_objPrintDoc.RenderDirect(p_dCurrentX ,p_dCurrentY ,m_objText);

				m_objText.Style.Font = new Font("Arial", 7f, FontStyle.Bold);
				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMPROPRIETRY");
                m_objText.Text = m_MessageSettings["DEF_RMPROPRIETARY"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_NAME");
                m_objText.Text = m_MessageSettings["DEF_NAME"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMCOPYRIGHT");
                m_objText.Text = m_MessageSettings["DEF_RMCOPYRIGHT"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);

				p_dCurrentY = p_dCurrentY + p_dHeight;
				//m_objText.Text = RMConfigurator.Text("message", "id", "DEF_RMRIGHTSRESERVED");
                m_objText.Text = m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString();
				m_objPrintDoc.RenderDirect(p_dCurrentX + p_dblSpace, p_dCurrentY, m_objText);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PaymentNotification.CreateFooter.Error", m_iClientId), p_objEx);				
			}
		}
		#endregion
	}
}
