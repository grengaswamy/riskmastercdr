﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Settings;
	//Changed by Gagan for MITS 22238 : End
namespace Riskmaster.Application.FundManagement
{
	/// <summary>
	/// AddPaymentsToCoverages Class
	/// </summary>
	public class AddPaymentsToCoverages
	{
		#region Variables Declaration
		/// <summary>
		/// Connection string for database
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		private string m_sUserPass = "" ;
		private DataModelFactory m_objDMF = null ;
        private int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		/// AddPaymentsToCoverages class constructor
		/// </summary>
		/// <param name="p_sDSN">Connection string to database</param>
		public AddPaymentsToCoverages(string p_sDSN , string p_sUser , string p_sPassword, int p_iClientId=0)
		{
			m_sDSN=p_sDSN;
			m_sUserName = p_sUser ;
			m_sUserPass = p_sPassword ;
            m_iClientId = p_iClientId;
            m_objDMF = new DataModelFactory(p_sDSN, p_sUser, p_sPassword, m_iClientId);
			//bUseEnhancedPolicySystem=true;
			//ReadCurrentSystemValues();
		}
		~AddPaymentsToCoverages()
		{
			if( m_objDMF != null )
				this.Dispose();
		}

		public void Dispose()
		{
			try
			{
				m_objDMF.UnInitialize();
                m_objDMF.Dispose();				
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}

		#endregion

		#region Update Policy Amounts 
	//Changed by Gagan for MITS 22238 : Start
		public XmlDocument GetPolicies(XmlDocument p_objDocument,ref string p_sSQL)
		{
			string sFromDate="";
			string sToDate="";
			string sPolicyNumber="";
            string sPolicyName = "";
			string sEffectiveDate="";
            string sExpirationDate = "";
			string sPolicyId="";
	//Changed by Gagan for MITS 22238 : End
			DataSet objDS=null;
			XmlNode objNode=null;
			StringBuilder sbSQL=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			sbSQL=new StringBuilder();
            bool bIsPolicyTracking = true;
            DbConnection objDbConnection = null;
            DbReader objDbReader = null;
            //int iClaimId = 0;
            int iLOB = 0;
            LocalCache objLocalCache = null;
            ColLobSettings oLobSettings = null;
            string sTransactionID = string.Empty;
            string sTermNumber = String.Empty;
            //rupal:start, r8 enh
            SysSettings oSysSetting = null;
            int iCarrierClaims = 0;
			//rupal:end
            //Changed by bsharma33 for PenTesting MITS 26876 
            Dictionary<string, string> dictParams = new Dictionary<string, string>();
            DbCommand m_objDbCommand=null;
            objDbConnection = DbFactory.GetDbConnection(m_objDMF.Context.DbConn.ConnectionString);
            //End Changes by bsharma33 for PenTesting MITS 26876
            objNode=p_objDocument.SelectSingleNode("//FromDate");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					if(!objNode.InnerText.Trim().Equals(""))
						sFromDate=Conversion.GetDate(objNode.InnerText);
				}
			}
			objNode=p_objDocument.SelectSingleNode("//ToDate");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					if(!objNode.InnerText.Trim().Equals(""))
						sToDate=Conversion.GetDate(objNode.InnerText);
				}
			}
	//Changed by Gagan for MITS 22238 : Start
            objNode = p_objDocument.SelectSingleNode("//PolicyName");
            if (objNode != null)
            {
                if (objNode.InnerText != null)
                {                    
                        sPolicyName = objNode.InnerText;
                }
            }
            objNode = p_objDocument.SelectSingleNode("//PolicyNumber");
            if (objNode != null)
            {
                if (objNode.InnerText != null)
                {                 
                        sPolicyNumber = objNode.InnerText;
                }
            }

            objNode = p_objDocument.SelectSingleNode("//PolicyType");
            if (objNode != null)
            {
                if (objNode.InnerText == "Policy Tracking")
                {
                    bIsPolicyTracking = true;
                    sbSQL.Append(" FROM POLICY");
                    //rupal:start, r8 enh
                    oSysSetting = new SysSettings(m_objDMF.Context.DbConn.ConnectionString, m_iClientId);
                    iCarrierClaims = oSysSetting.MultiCovgPerClm;
                    //rupal:end
                }
                else
                {
                    bIsPolicyTracking = false;


                    //sbSQL.Append(" FROM POLICY_ENH PE INNER JOIN POLICY_X_TERM_ENH PT ON PE.POLICY_ID = PT.POLICY_ID ");
                   // sbSQL.Append(" WHERE PE.POLICY_ID IN (SELECT DISTINCT PRIMARY_POLICY_ID FROM CLAIM WHERE PRIMARY_POLICY_ID <> 0 ");
                   // sbSQL.Append(" ORDER BY PE.POLICY_ID ");
                    //sbSQL.Append(" FROM POLICY_ENH INNER JOIN POLICY_X_TERM_ENH PT ON PE.POLICY_ID = PT.POLICY_ID ");
                //    sbSQL.Append(" INNER JOIN POLICY_X_TRANS_ENH PTR JOIN PT.POLICY_ID = PTR.POLICY_ID ");



                    sbSQL.Append(" FROM POLICY_ENH PE INNER JOIN POLICY_X_TERM_ENH PT ON ");
                    sbSQL.Append(" PE.POLICY_ID = PT.POLICY_ID INNER JOIN POLICY_X_TRANS_ENH PTR ON PE.POLICY_ID = PTR.POLICY_ID ");
//                    sbSQL.Append(" WHERE (PE.POLICY_ID IN (SELECT DISTINCT PRIMARY_POLICY_ID FROM CLAIM WHERE PRIMARY_POLICY_ID <> 0)) ");  
                }
            }


            if (bIsPolicyTracking == true)
            {
                if (!sFromDate.Equals(""))
                {
                    //Changed by bsharma33 for PenTesting MITS 26876
                    //sbSQL.Append("  WHERE EFFECTIVE_DATE >= '" + sFromDate + "'");
                    sbSQL.Append(string.Format("  WHERE EFFECTIVE_DATE >= {0}", "~EFFECTIVE_DATE~"));
                    dictParams.Add("EFFECTIVE_DATE", sFromDate);
                    //End Changes by bsharma33 for PenTesting MITS 26876
                }
                if (!sToDate.Equals(""))
                {
                    if (!sFromDate.Equals(""))
                    {
                        sbSQL.Append(" AND ");
                    }
                    else
                    {
                        sbSQL.Append(" WHERE ");
                    }
                    //Changed by bsharma33 for PenTesting MITS 26876
                    //sbSQL.Append(" EXPIRATION_DATE <=  '" + sToDate + "'");
                    sbSQL.Append(string.Format(" EXPIRATION_DATE <=  {0}","~EXPIRATION_DATE~"));
                    dictParams.Add("EXPIRATION_DATE", sToDate);
                    //End Changes by bsharma33 for PenTesting MITS 26876
                }

                if (sbSQL.ToString().Contains("WHERE"))
                {
                    if (sPolicyName != "")
                    {

                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" AND POLICY_NAME = '" + sPolicyName + "'");
                        sbSQL.Append(string.Format("  AND POLICY_NAME =   {0}", "~POLICY_NAME~"));
                        dictParams.Add("POLICY_NAME", sPolicyName);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                    }
                }
                else
                {
                    if (sPolicyName != "")
                    {
                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" WHERE POLICY_NAME = '" + sPolicyName + "'");
                        sbSQL.Append(string.Format("   WHERE POLICY_NAME =    {0}", "~POLICY_NAME~"));
                        dictParams.Add("POLICY_NAME", sPolicyName);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                    }
                }


                if (sbSQL.ToString().Contains("WHERE"))
                {
                    if (sPolicyNumber != "")
                    {
                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" AND POLICY_NUMBER = '" + sPolicyNumber + "'");
                        sbSQL.Append(string.Format("   AND POLICY_NUMBER =    {0}", "~POLICY_NUMBER~"));
                        dictParams.Add("POLICY_NUMBER", sPolicyNumber);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                    }
                }
                else
                {
                    if (sPolicyNumber != "")
                    {
                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" WHERE POLICY_NUMBER = '" + sPolicyNumber + "'");
                        sbSQL.Append(string.Format("   WHERE POLICY_NUMBER =    {0}", "~POLICY_NUMBER~"));
                        dictParams.Add("POLICY_NUMBER", sPolicyNumber);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                    }
                }

                if (sbSQL.ToString().Contains("WHERE"))
                {
                    //rupal:start, r8 enh
                    if (iCarrierClaims == -1)
                    {
                        //in case of carrier claims, Policy can be found in Claim_X_Policy
                        sbSQL.Append(" AND POLICY_ID IN (SELECT DISTINCT POLICY_ID FROM CLAIM_X_POLICY)");
                    }
                    else
                    {
                        sbSQL.Append(" AND POLICY_ID IN (SELECT PRIMARY_POLICY_ID FROM CLAIM)");
                    }
                    //rupal:end
                }
                else
                {
                     //rupal:start, r8 enh
                    if (iCarrierClaims == -1)
                    {
                        //in case of carrier claims, Policy can be found in Claim_X_Policy
                        sbSQL.Append(" WHERE POLICY_ID IN (SELECT DISTINCT POLICY_ID FROM CLAIM_X_POLICY)");
                    }
                    else
                    {
                        sbSQL.Append(" WHERE POLICY_ID IN (SELECT PRIMARY_POLICY_ID FROM CLAIM WHERE PRIMARY_POLICY_ID <> 0)");
                    }
                }
            }
            else
            {
                if (!sFromDate.Equals(""))
                {
                    //Changed by bsharma33 for PenTesting MITS 26876
                    //sbSQL.Append("  WHERE PTR.EFFECTIVE_DATE >= '" + sFromDate + "'");
                    sbSQL.Append(string.Format("  WHERE PTR.EFFECTIVE_DATE >= {0}", "~PTR_EFFECTIVE_DATE~"));
                    dictParams.Add("PTR_EFFECTIVE_DATE", sFromDate);
                    //End Changes by bsharma33 for PenTesting MITS 26876
                }
                if (!sToDate.Equals(""))
                {
                    if (!sFromDate.Equals(""))
                    {
                        sbSQL.Append(" AND ");
                    }
                    else
                    {
                        sbSQL.Append(" WHERE ");
                    }
                    //Changed by bsharma33 for PenTesting MITS 26876
                    //sbSQL.Append(" PTR.EXPIRATION_DATE <=  '" + sToDate + "'");
                    sbSQL.Append(string.Format(" PTR.EXPIRATION_DATE <=  {0}", "~PTR_EXPIRATION_DATE~"));
                    dictParams.Add("PTR_EXPIRATION_DATE", sToDate);
                    //End Changes by bsharma33 for PenTesting MITS 26876
                }

                if (sbSQL.ToString().Contains("WHERE"))
                {
                    if (sPolicyName != "")
                    {
                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" AND PE.POLICY_NAME = '" + sPolicyName + "'");
                        sbSQL.Append(string.Format("   AND PE.POLICY_NAME =    {0}", "~PE_POLICY_NAME~"));
                        dictParams.Add("PE_POLICY_NAME", sPolicyName);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                        
                    }
                }
                else
                {
                    if (sPolicyName != "")
                    {
                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" WHERE PE.POLICY_NAME = '" + sPolicyName + "'");
                        sbSQL.Append(string.Format("   WHERE PE.POLICY_NAME =    {0}", "~PE_POLICY_NAME~"));
                        dictParams.Add("PE_POLICY_NAME", sPolicyName);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                    }
                }


                if (sbSQL.ToString().Contains("WHERE"))
                {
                    if (sPolicyNumber != "")
                    {
                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" AND PT.POLICY_NUMBER = '" + sPolicyNumber + "'");
                        sbSQL.Append(string.Format("   AND PT.POLICY_NUMBER =    {0}", "~PT_POLICY_NUMBER~"));
                        dictParams.Add("PT_POLICY_NUMBER", sPolicyNumber);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                    }
                }
                else
                {
                    
                    if (sPolicyNumber != "")
                    {
                        //Changed by bsharma33 for PenTesting MITS 26876
                        //sbSQL.Append(" WHERE PT.POLICY_NUMBER = '" + sPolicyNumber + "'");
                        sbSQL.Append(string.Format("   WHERE PT.POLICY_NUMBER =    {0}", "~PT_POLICY_NUMBER~"));
                        dictParams.Add("PT_POLICY_NUMBER", sPolicyNumber);
                        //End Changes by bsharma33 for PenTesting MITS 26876
                    }
                }

                if (sbSQL.ToString().Contains("WHERE"))
                {
                    sbSQL.Append(" AND PE.POLICY_ID IN (SELECT DISTINCT PRIMARY_POLICY_ID FROM CLAIM WHERE PRIMARY_POLICY_ID <> 0 ) ");
                    //sbSQL.Append(" AND PE.POLICY_ID IN (SELECT PRIMARY_POLICY_ID FROM CLAIM)");
                }
                else
                {
                    sbSQL.Append(" WHERE PE.POLICY_ID IN (SELECT DISTINCT PRIMARY_POLICY_ID FROM CLAIM WHERE PRIMARY_POLICY_ID <> 0 )");
                    //sbSQL.Append(" WHERE PE.POLICY_ID IN (SELECT PRIMARY_POLICY_ID FROM CLAIM)");
                }
            }           


			objDOM =new XmlDocument();
			objElemParent = objDOM.CreateElement("Policies");
			objDOM.AppendChild(objElemParent);
            objLocalCache = new LocalCache(m_objDMF.Context.DbConn.ConnectionString,m_iClientId);

            if (bIsPolicyTracking == true)
            {
                //Changed by bsharma33 for PenTesting MITS 26876
                if (objDbConnection.State != ConnectionState.Open)
                {
                    objDbConnection.Open();
                }
                m_objDbCommand = objDbConnection.CreateCommand();
                m_objDbCommand.CommandText = "SELECT POLICY_ID, POLICY_NAME, POLICY_NUMBER, EFFECTIVE_DATE, EXPIRATION_DATE  " + sbSQL.ToString();
                objDS = DbFactory.ExecuteDataSet(m_objDbCommand, dictParams, m_iClientId);

                //objDS = DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString, "SELECT POLICY_ID, POLICY_NAME, POLICY_NUMBER, EFFECTIVE_DATE, EXPIRATION_DATE  " + sbSQL.ToString());
                //End Changes by bsharma33 for PenTesting MITS 26876
            }
            else
            {
                //objDS = DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString, "SELECT DISTINCT PE.POLICY_ID, PE.POLICY_NAME POLICY_NAME, PT.POLICY_NUMBER POLICY_NUMBER, PT.EFFECTIVE_DATE EFFECTIVE_DATE, PT.EXPIRATION_DATE EXPIRATION_DATE " + sbSQL.ToString());
                //sbSQL.Append(" AND PE.POLICY_STATUS_CODE = 212 ");   //Policy is In Effect
                sbSQL.Append(" AND PE.POLICY_STATUS_CODE = ");
                sbSQL.Append(objLocalCache.GetCodeId("I", "POLICY_STATUS"));   //Policy is In Effect
                sbSQL.Append(" GROUP BY PT.TERM_NUMBER,PE.POLICY_ID, PE.POLICY_NAME, PT.POLICY_NUMBER , PT.EFFECTIVE_DATE ,  PT.EXPIRATION_DATE ");
                //Changed by bsharma33 for PenTesting MITS 26876
                if (objDbConnection.State != ConnectionState.Open)
                {
                    objDbConnection.Open();
                }
                m_objDbCommand = objDbConnection.CreateCommand();
                m_objDbCommand.CommandText = "SELECT MAX(PTR.TRANSACTION_ID) TRANSACTION_ID, PT.TERM_NUMBER TERM_NUMBER,PE.POLICY_ID POLICY_ID, PE.POLICY_NAME POLICY_NAME, PT.POLICY_NUMBER POLICY_NUMBER, PT.EFFECTIVE_DATE EFFECTIVE_DATE, PT.EXPIRATION_DATE EXPIRATION_DATE " + sbSQL.ToString();
                objDS = DbFactory.ExecuteDataSet(m_objDbCommand, dictParams, m_iClientId);

                //objDS = DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString, "SELECT MAX(PTR.TRANSACTION_ID) TRANSACTION_ID, PT.TERM_NUMBER TERM_NUMBER,PE.POLICY_ID POLICY_ID, PE.POLICY_NAME POLICY_NAME, PT.POLICY_NUMBER POLICY_NUMBER, PT.EFFECTIVE_DATE EFFECTIVE_DATE, PT.EXPIRATION_DATE EXPIRATION_DATE " + sbSQL.ToString()); 
                //End Changes by bsharma33 for PenTesting MITS 26876
            }

            
            // sbSQL.Append(" ORDER BY PE.POLICY_ID ");
            //Changed by bsharma33 for PenTesting MITS 26876
            if (objDbConnection.State != ConnectionState.Open)
            {
                objDbConnection.Open();
            }
            //End Changes by bsharma33 for PenTesting MITS 26876
            //objLocalCache = new LocalCache(m_objDMF.Context.DbConn.ConnectionString);
            oLobSettings = new ColLobSettings(m_objDMF.Context.DbConn.ConnectionString, m_iClientId);

			if (objDS.Tables[0]!=null)
			{
				if (objDS.Tables[0].Rows.Count <= 500)
				{
					foreach(DataRow objRow in objDS.Tables[0].Rows)
					{
                        sPolicyId = Conversion.ConvertObjToStr(objRow["POLICY_ID"]);              
                        iLOB = 0;
                        sTransactionID = "";
                        sTermNumber = "";

                        //START:RUPAL, R8 ENH
                        //IN case of carrier claims, for all LOB(s) only policy tracking wl be enabled, so we do not need folowing condition 
                        if (iCarrierClaims == 0)
                        {
                            //Checking for whether policy is 
                            sbSQL.Length = 0;
                            sbSQL.Append("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE PRIMARY_POLICY_ID = ");
                            sbSQL.Append(sPolicyId);

                            objDbReader = objDbConnection.ExecuteReader(sbSQL.ToString());
                            if (objDbReader.Read())
                            {
                                //iClaimId = objDbReader.GetInt32("CLAIM_ID");
                                iLOB = objDbReader.GetInt32("LINE_OF_BUS_CODE");

                            }
                            objDbReader.Close();

                            //when Policy tracking is chosen and Policy is Enh Policy
                            if (bIsPolicyTracking == true && oLobSettings[iLOB].UseEnhPolFlag != 0)
                                continue;
                            //when Policy Enh is chosen and Policy is Policy Tracking
                            else if (bIsPolicyTracking == false && oLobSettings[iLOB].UseEnhPolFlag == 0)
                                continue;
                        }
                        //rupal:end
                        sPolicyName = Conversion.ConvertObjToStr(objRow["POLICY_NAME"]);
						sPolicyNumber =Conversion.ConvertObjToStr(objRow["POLICY_NUMBER"]);
						
						sEffectiveDate =Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRow["EFFECTIVE_DATE"]),"d");
                        sExpirationDate = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRow["EXPIRATION_DATE"]), "d");

                        if (bIsPolicyTracking == false)
                        {
                            sTransactionID = Conversion.ConvertObjToStr(objRow["TRANSACTION_ID"]);;
                            sTermNumber = Conversion.ConvertObjToStr(objRow["TERM_NUMBER"]);
                        }

						objElemTemp=objDOM.CreateElement("Policy");
                        if (bIsPolicyTracking == false)
                        {
                            objElemTemp.SetAttribute("id", sPolicyId + "_" + sTermNumber + "_" + sTransactionID);
                        }
                        else
                        {
                            objElemTemp.SetAttribute("id", sPolicyId);
                        }

                            objElemTemp.SetAttribute("effectivedate", sEffectiveDate);
                        objElemTemp.SetAttribute("expirationdate", sExpirationDate);
                        objElemTemp.SetAttribute("visible", "visible");
                        objElemTemp.SetAttribute("policyname", sPolicyName);
                        objElemTemp.SetAttribute("policynumber", sPolicyNumber);
                        //objElemTemp.SetAttribute("transactionid", sTransactionID);
                        //objElemTemp.SetAttribute("termnumber", sTermNumber);                        
						objDOM.FirstChild.AppendChild(objElemTemp);
					}
					p_sSQL="";
				}
				else
				{
					objElemTemp=objDOM.CreateElement("Policy");
					objElemTemp.SetAttribute("id","");
                    objElemTemp.SetAttribute("visible", "hidden");
                    objElemTemp.SetAttribute("effectivedate", "");
                    objElemTemp.SetAttribute("expirationdate", "");
                    //objElemTemp.SetAttribute("transactionid", "");
                    //objElemTemp.SetAttribute("termnumber", "");                        						
                    objElemTemp.SetAttribute("visible", "visible");
                    objElemTemp.SetAttribute("policyname", "<ALL MATCHING POLICIES>");
                    objElemTemp.SetAttribute("policynumber", "<ALL MATCHING POLICIES>");
                    //objElemTemp.SetAttribute("claimnumber", "<ALL MATCHING POLICIES>");
                    objElemTemp.InnerText = "<ALL MATCHING POLICIES>";
					objDOM.FirstChild.AppendChild(objElemTemp);
                    if (bIsPolicyTracking == true)
					    p_sSQL=" SELECT POLICY_ID "+sbSQL.ToString();
                    else
                        p_sSQL = " SELECT PE.POLICY_ID " + sbSQL.ToString(); 
				}
			}
            if (objDbConnection.State== ConnectionState.Open)
                objDbConnection.Close();

            if (objLocalCache != null)
                objLocalCache.Dispose();

			objDS.Dispose();
			return objDOM;
		}
		public XmlDocument ApplyPayments(XmlDocument objDocument)
		{
			string sSQL="";
			DataSet objDS=null;
			int iFlag=0;
			int j=0;
			string[] arrClaims=null;
            string[] arrPolicies=null;
            string[] arrTermNumbers = null;
            string[] arrTransactionIds = null;
            string[] arrStr = null; 
			XmlNode objNode=null;
			XmlDocument objDOM=null;
			XmlElement objElement=null;
			GetPolicies(objDocument,ref sSQL);
			objDOM=new XmlDocument();
			objElement=objDOM.CreateElement("Errors");
			objDOM.AppendChild(objElement);
			string[,] sInformation = new string[600,2];

            //Start MITS 20941 
            //string sTransIDs = "";
            string sSQLPayments = string.Empty;
            string sSQLCollections = string.Empty;
            string sSQLTotalPayments = "";
            string sSQLClaims = "";
            string sSQLDelete = string.Empty;
            string sSQLCoverages = string.Empty;
            
            StringBuilder sb = null;
            //DataSet objDSPayments=null;
            //DataSet objDSCollections=null;
            Hashtable objTotalPayments =null;
            DbConnection objDbConnection = null;
            DbReader objDbReader = null;
            int iPolicyCvgID = 0;
            int iPrevAmount = 0;
            double dblTotalPayments = 0;
            double dblPayments = 0;
            double dblCollections = 0;
            List<int> objlstClaim = null;
            List<int> objCoverages = null;
            int k = 0;
            bool bIsPolicyTracking = false;
            //rupal:start, r8 enh
            SysSettings oSysSetting = new SysSettings(m_objDMF.Context.DbConn.ConnectionString, m_iClientId);
            int iCarrierClaims = oSysSetting.MultiCovgPerClm;
            //rupal:end
            //End MITS 20941 

			Funds objFunds = ( Funds ) m_objDMF.GetDataModelObject( "Funds" , false );

            SysSettings objSettings = new SysSettings(m_objDMF.Context.DbConn.ConnectionString, m_iClientId);
            string sTableName = string.Empty;
            //if (objSettings.UseEnhPolFlag == -1)
           objNode=objDocument.SelectSingleNode("//PolicyType");                
            if(objNode.InnerText == "Policy Management")
            {
                sTableName = "POLICY_X_CVG_ENH";
                bIsPolicyTracking = false;
            }
            else
            {
                sTableName = "POLICY_X_CVG_TYPE";
                bIsPolicyTracking = true;
            }

            objlstClaim = new List<int>();
            objCoverages = new List<int>();
            sb = new StringBuilder();

			if (sSQL.Equals(""))
			{
				objNode=objDocument.SelectSingleNode("//Policies");                
                arrPolicies = objNode.InnerText.Split(" ".ToCharArray());
                arrTermNumbers = new string[arrPolicies.Length];
                arrTransactionIds = new string[arrPolicies.Length];

                //For Enhanced Policy
                if (bIsPolicyTracking == false)
                {
                    for (int i = 0; i < arrPolicies.Length; i++)
                    {
                        arrStr = arrPolicies[i].Split("_".ToCharArray());
                        arrPolicies[i] = arrStr[0];
                        arrTermNumbers[i] = arrStr[1];
                        arrTransactionIds[i] = arrStr[2];
                    }
                }

                objDbConnection = DbFactory.GetDbConnection(m_objDMF.Context.DbConn.ConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();

                //finding claims to which the selected policies are attached
                for (int i = 0; i < arrPolicies.Length; i++)
                {
                    if (!arrPolicies[i].Trim().Equals(""))
                    {
                        //rupal:start, r8 enh
                        //IN CASE OF CARRIER claims, policies are stored in claim_x_policy, also note that thet for carrier claims in r8, only policy tracking wl be available
                        if (iCarrierClaims == -1)
                        {
                            sSQLClaims = "SELECT CLAIM_ID FROM CLAIM_X_POLICY WHERE POLICY_ID = " + arrPolicies[i] + " ORDER BY CLAIM_ID";
                        }
                        else
                        {
                            sSQLClaims = "SELECT CLAIM_ID FROM CLAIM WHERE PRIMARY_POLICY_ID = " + arrPolicies[i] + " ORDER BY CLAIM_ID";
                        }
                        //rupal:end
                        objDbReader = objDbConnection.ExecuteReader(sSQLClaims);
                        while (objDbReader.Read())
                            objlstClaim.Add(objDbReader.GetInt32("CLAIM_ID"));
                        objDbReader.Close();

                        //start:rupal, r8 enh
                        //if carrier claim is on, Policy_Id in POLICY_X_CVG_TYPE table will always  be zero, instead we have Policy_Unit_Row_ID available in this table
                        //, so we will have to find Policy_Unit_Row_ID from Policy_X_Unit table based on Policy_ID and then based on Policy_Unit_Row_ID we can find
                        //Policy_Coverage_Row_ID from POLICY_X_CVG_TYPE table
                        if (iCarrierClaims == -1 && bIsPolicyTracking == true)
                        {
                            sSQLCoverages = "SELECT POLCVG_ROW_ID FROM " + sTableName + " WHERE POLICY_UNIT_ROW_ID IN (SELECT POLICY_UNIT_ROW_ID FROM POLICY_X_UNIT WHERE POLICY_ID = " + arrPolicies[i] + ")";
                        }
                        else if (bIsPolicyTracking == true)
                        {   
                            sSQLCoverages = "SELECT POLCVG_ROW_ID FROM " + sTableName + " WHERE POLICY_ID = " + arrPolicies[i];
                        }
                        else
                        {
                            sSQLCoverages = "SELECT POLCVG_ROW_ID FROM " + sTableName + " WHERE POLICY_ID = " + arrPolicies[i] + " AND TERM_NUMBER = " + arrTermNumbers[i] + " AND TRANSACTION_ID = " + arrTransactionIds[i] ;
                        }
                        //rupal:end
                        objDbReader = objDbConnection.ExecuteReader(sSQLCoverages);
                        while (objDbReader.Read())
                            objCoverages.Add(objDbReader.GetInt32("POLCVG_ROW_ID"));
                        objDbReader.Close();
                        
                    }
                }

                if (objlstClaim.Count > 0)
                {
                    arrClaims = new string[objlstClaim.Count];
                    k = 0;
                    foreach (int objClaimID in objlstClaim)
                    {
                        arrClaims[k] = objClaimID.ToString();
                        k++;
                    }
                }

                // We need to Update the Total Payments to zero and delete all the payments made in Col_X_Funds and Coverage_X_Funds
                // As these are impacting the Policy Limit, Event limit and Claim Limit

                //rupal:start, r8 enh
                //in case of carrier claims, we dont have policy id in POLICY_X_CVG_TYPE table, but we already have Pol_Cvg_Row_IDs available so 
                //we will directly update payments based on Pol_Cvg_Row_IDs
                if (iCarrierClaims == -1)
                {
                    for (int i = 0; i < objCoverages.Count; i++)
                    {
                        if (objCoverages[i] != 0)
                        {
                            sSQLTotalPayments = "UPDATE " + sTableName + " SET TOTAL_PAYMENTS = 0 WHERE POLCVG_ROW_ID = " + objCoverages[i];
                            objDbConnection.ExecuteNonQuery(sSQLTotalPayments);

                            sSQLDelete = "DELETE FROM COVERAGE_X_FUNDS WHERE POLCVG_ROW_ID = " + objCoverages[i];
                            objDbConnection.ExecuteNonQuery(sSQLDelete);

                            sSQLDelete = "DELETE FROM COL_X_FUNDS WHERE POLCVG_ROW_ID = " + objCoverages[i];
                            objDbConnection.ExecuteNonQuery(sSQLDelete);
                        }
                    }
                }
                else
                {
                    // akaushik5 Changed for MITS 37694 Starts
                    //for (int i = 0; i < arrPolicies.Length; i++)
                    //{
                    //    if (!arrPolicies[i].Trim().Equals(""))
                    //    {
                    //        sSQLTotalPayments = "UPDATE " + sTableName + " SET TOTAL_PAYMENTS = 0 WHERE POLICY_ID = " + arrPolicies[i];
                    //        objDbConnection.ExecuteNonQuery(sSQLTotalPayments);

                    //        sSQLDelete = "DELETE FROM COVERAGE_X_FUNDS WHERE POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM " + sTableName + " WHERE POLICY_ID = " + arrPolicies[i] + ")";
                    //        objDbConnection.ExecuteNonQuery(sSQLDelete);

                    //        sSQLDelete = "DELETE FROM COL_X_FUNDS WHERE POLCVG_ROW_ID IN (SELECT POLCVG_ROW_ID FROM " + sTableName + " WHERE POLICY_ID = " + arrPolicies[i] + ")";
                    //        objDbConnection.ExecuteNonQuery(sSQLDelete);
                    //    }
                    //}

                    List<int> lstProcessedPolicyIds = new List<int>();
                    for (int i = 0; i < arrPolicies.Length; i++)
                    {
                        string sPolicyId = arrPolicies[i];
                        List<int> lstPolCvgId = new List<int>();
                        do
                        {
                            if (!string.IsNullOrEmpty(sPolicyId))
                            {
                                List<int> lstNextPolicyId = new List<int>();
                                string sSql = string.Format("SELECT POLCVG_ROW_ID, NEXT_POLICY_ID FROM {0} WHERE POLICY_ID IN ({1})", sTableName, sPolicyId);
                                using (DbReader objRdr = objDbConnection.ExecuteReader(sSql))
                                {
                                    while (objRdr.Read())
                                    {
                                        if (!Convert.ToInt16(objRdr["POLCVG_ROW_ID"]).Equals(default(int)) && !lstPolCvgId.Contains(Convert.ToInt16(objRdr["POLCVG_ROW_ID"])))
                                        {
                                            lstPolCvgId.Add(Convert.ToInt16(objRdr["POLCVG_ROW_ID"]));
                                        }
                                        if (!Convert.ToInt16(objRdr["NEXT_POLICY_ID"]).Equals(default(int)) && !lstNextPolicyId.Contains(Convert.ToInt16(objRdr["NEXT_POLICY_ID"])))
                                        {
                                            lstNextPolicyId.Add(Convert.ToInt16(objRdr["NEXT_POLICY_ID"]));
                                        }
                                    }
                                }

                                if (lstNextPolicyId.Count > 0)
                                {
                                    lstNextPolicyId = (from x in lstNextPolicyId
                                                       where !lstProcessedPolicyIds.Contains(x)
                                                       select x).ToList();
                                    lstNextPolicyId.ForEach(x => { lstProcessedPolicyIds.Add(x); });
                                }
                                sPolicyId = lstNextPolicyId.Count > 0 ? lstNextPolicyId.Select(t => t.ToString()).Aggregate((s1, s2) => string.Format("{0},{1}", s1, s2)) : string.Empty;
                            }
                        }
                        while (!string.IsNullOrEmpty(sPolicyId));

                        if (lstPolCvgId.Count > 0)
                        {
                            string slstPolCvgId = lstPolCvgId.Select(t => t.ToString()).Aggregate((s1, s2) => string.Format("{0},{1}", s1, s2));
                            sSQLTotalPayments = string.Format("UPDATE {0} SET TOTAL_PAYMENTS = 0 WHERE POLCVG_ROW_ID IN ({1})", sTableName, slstPolCvgId);
                            objDbConnection.ExecuteNonQuery(sSQLTotalPayments);

                            sSQLDelete = string.Format("DELETE FROM COVERAGE_X_FUNDS WHERE POLCVG_ROW_ID IN ({0})", slstPolCvgId);
                            objDbConnection.ExecuteNonQuery(sSQLDelete);

                            sSQLDelete = string.Format("DELETE FROM COL_X_FUNDS WHERE POLCVG_ROW_ID IN ({0})", slstPolCvgId);
                            objDbConnection.ExecuteNonQuery(sSQLDelete);
                        }
                    }
                    // akaushik5 Changed for MITS 37694 Ends
                }
                //rupal:end

				//arrClaims=objNode.InnerText.Split(" ".ToCharArray());
	            //Changed by Gagan for MITS 22238 : End
                //MITS 20578 :start
                objTotalPayments = new Hashtable();
                //MITS 20578 :end

				for(int i=0;i<arrClaims.Length;i++)
				{
					if (!arrClaims[i].Trim().Equals(""))
					{
						objDS=DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString,"SELECT CTL_NUMBER,TRANS_ID,CLAIM_NUMBER FROM FUNDS WHERE CLAIM_ID="+arrClaims[i], m_iClientId);
                        //Start MITS 20941 
	                    //Changed by Gagan for MITS 22238 : Start
                        sb.Length = 0;
	                    //Changed by Gagan for MITS 22238 : End

                        //End MITS 20941 
						if (objDS.Tables[0]!=null)
						{
							foreach(DataRow objRow in objDS.Tables[0].Rows)
							{
								iFlag=-1;
								// Vaibhav 26:Feb-2006:
								// update policy amount code is moved to the datamodel.
								objFunds.MoveTo( Conversion.ConvertObjToInt(objRow["TRANS_ID"], m_iClientId));
								objFunds.DataChanged = true;
                                // npadhy Start MITS 20941 Apply Payments to Coverages adds collections more than once
                                objFunds.ApplyPayments2Cvg = true;
                                // npadhy End MITS 20941 Apply Payments to Coverages adds collections more than once
								objFunds.Save();
								if(j<500)
								{
									sInformation[j,0] = Conversion.ConvertObjToStr(objRow["CLAIM_NUMBER"]);
									sInformation[j++,1] = Conversion.ConvertObjToStr(objRow["CTL_NUMBER"]);
								}
                                //Start MITS 20941 
	                            //Changed by Gagan for MITS 22238 : Start
                                if(sb.ToString() != "")
                                    sb.Append(",");
                                sb.Append(Conversion.ConvertObjToStr(objRow["TRANS_ID"]));
	                            //Changed by Gagan for MITS 22238 : End

                                //End MITS 20941 
							}
						}
						objDS.Dispose();
					}

                    //Update total Payments
                    //Start MITS 20941 
	                //Changed by Gagan for MITS 22238 : Start

                    //if(sb.ToString() != "")
                    //{                             
                    //    sSQLPayments = "SELECT POLCVG_ROW_ID , SUM(AMOUNT) AS TOTAL_PAYMENTS FROM COVERAGE_X_FUNDS WHERE TRANS_ID IN (" + sb.ToString() + ") GROUP BY POLCVG_ROW_ID";
                    //    sSQLCollections = "SELECT POLCVG_ROW_ID , SUM(AMOUNT) AS TOTAL_PAYMENTS FROM COL_X_FUNDS WHERE TRANS_ID IN (" + sb.ToString() + ") AND COL_FLAG = -1 AND VOID_FLAG = 0 GROUP BY POLCVG_ROW_ID";
                    ////Changed by Gagan for MITS 22238 : End

                    //    objDSPayments = DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString, sSQLPayments);
                    //    objDSCollections = DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString, sSQLCollections);

                    //    if (objDSPayments.Tables[0] != null)
                    //    {
                    //        foreach (DataRow objRow in objDSPayments.Tables[0].Rows)
                    //        {
                    //            //Add to Total Payments Hashtable
                    //            //MITS 20578 :start
                    //            iPolicyCvgID = Conversion.ConvertObjToInt(objRow["POLCVG_ROW_ID"], m_iClientId);
                    //            iPrevAmount = 0;
                    //            if (objTotalPayments.Contains(iPolicyCvgID))
                    //            {
                    //                iPrevAmount = Conversion.ConvertObjToInt(objTotalPayments[iPolicyCvgID]);, m_iClientId
                    //                objTotalPayments[iPolicyCvgID] = iPrevAmount + Conversion.ConvertObjToDouble(objRow["TOTAL_PAYMENTS"]);                         
                    //            }
                    //            else
                    //            {
                    //                objTotalPayments.Add(iPolicyCvgID, Conversion.ConvertObjToDouble(objRow["TOTAL_PAYMENTS"]));
                    //            }
                    //            //MITS 20578 :end
                    //        }
                    //    }

                    //    //Calculate total Payments for Policy Coverage
                    //    if (objDSCollections.Tables[0] != null)
                    //    {
                    //        foreach (DataRow objRow in objDSCollections.Tables[0].Rows)
                    //        {
                    //            iPolicyCvgID = Conversion.ConvertObjToInt(objRow["POLCVG_ROW_ID"], m_iClientId);

                    //            //If Policy Cvg Id already there in both Collections and Payments then subtract their amounts
                    //            if(objTotalPayments.Contains(iPolicyCvgID))
                    //            {
                    //                dblTotalPayments = Conversion.ConvertObjToDouble(objTotalPayments[iPolicyCvgID]) - Conversion.ConvertObjToDouble(objRow["TOTAL_PAYMENTS"]);
                    //                objTotalPayments[Conversion.ConvertObjToInt(objRow["POLCVG_ROW_ID"], m_iClientId)] = dblTotalPayments;
                    //            }
                    //            else //Otherwise Add The Sum of collections to the total payments for the coverage.
                    //            {
                    //                objTotalPayments.Add(iPolicyCvgID, -(Conversion.ConvertObjToDouble(objRow["TOTAL_PAYMENTS"])));
                    //            }
                    //        }
                    //    }

                    //    objDSPayments.Dispose();
                    //    objDSCollections.Dispose();

                    // }
                 
                }
                //MITS 20578 :start
                objDbConnection = DbFactory.GetDbConnection(m_objDMF.Context.DbConn.ConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();


                //Update Total Payments for Policy Coverage
                //foreach (object objKey in objTotalPayments)
                //{
                    
                //    sSQLTotalPayments = "UPDATE " + sTableName + " SET TOTAL_PAYMENTS = " + (Conversion.ConvertObjToDouble(((DictionaryEntry)objKey).Value)) + " WHERE POLCVG_ROW_ID = " + Conversion.ConvertObjToInt(((DictionaryEntry)objKey).Key);, m_iClientId
                //    objDbConnection.ExecuteNonQuery(sSQLTotalPayments);
                //}
                foreach (int iCoverageRowId in objCoverages)
                {
                    // Get the Payments
                    sSQLPayments = "SELECT SUM(AMOUNT) PAYMENTS FROM COVERAGE_X_FUNDS WHERE POLCVG_ROW_ID = " + iCoverageRowId;
                    objDbReader = objDbConnection.ExecuteReader(sSQLPayments);
                    if (objDbReader.Read())
                        dblPayments = objDbReader.GetDouble("PAYMENTS");
                    objDbReader.Close();
                    // Get the Collections
                    sSQLCollections = "SELECT SUM(AMOUNT) COLLECTIONS FROM COL_X_FUNDS WHERE POLCVG_ROW_ID = " + iCoverageRowId + " AND COL_FLAG = -1 AND VOID_FLAG = 0";
                    objDbReader = objDbConnection.ExecuteReader(sSQLCollections);
                    if (objDbReader.Read())
                        dblCollections = objDbReader.GetDouble("COLLECTIONS");
                    objDbReader.Close();
                    // Update the Total Payment
                    sSQLTotalPayments = "UPDATE " + sTableName + " SET TOTAL_PAYMENTS = " + (dblPayments - dblCollections) + " WHERE POLCVG_ROW_ID = " + iCoverageRowId;                    
                    objDbConnection.ExecuteNonQuery(sSQLTotalPayments);
                }

                objDbConnection.Close();                
                //End MITS 20941 
                //MITS 20578 :end
			}//end if
			else
			{
				int i=0;
				objDS=DbFactory.GetDataSet(m_objDMF.Context.DbConn.ConnectionString,"SELECT CTL_NUMBER,TRANS_ID,CLAIM_NUMBER FROM FUNDS WHERE CLAIM_ID IN ("+sSQL+")",m_iClientId);
				if (objDS.Tables[0]!=null)
				{
					foreach(DataRow objRow in objDS.Tables[0].Rows)
					{
						i++;
						// Vaibhav 26:Feb-2006:
						// update policy amount code is moved to the datamodel.
						objFunds.MoveTo( Conversion.ConvertObjToInt(objRow["TRANS_ID"], m_iClientId) );
						objFunds.DataChanged = true;
						objFunds.Save();
						if(j<500)
						{
							sInformation[j,0] = Conversion.ConvertObjToStr(objRow["CLAIM_NUMBER"]);
							sInformation[j++,1] = Conversion.ConvertObjToStr(objRow["CTL_NUMBER"]);
						}
						iFlag=-1;
					}
					
				}
				objDS.Dispose();
			}
			objElement=(XmlElement)objDOM.FirstChild;
			objElement.SetAttribute("NoPayments",iFlag.ToString());
			XmlElement objTargetElement = objDOM.CreateElement("ProcessedRecordList");
			objElement.AppendChild(objTargetElement);
			for(int i=0;i<j;i++)
			{
				XmlElement objChildElement = null;
				objChildElement = objDOM.CreateElement("ClaimNumber");
				objChildElement.SetAttribute("ControlNumber" , sInformation[i,1]);
				objChildElement.InnerText = sInformation[i,0];
				objTargetElement.AppendChild(objChildElement);
			}
			return objDOM;
		}
		

		#endregion
	}
}
