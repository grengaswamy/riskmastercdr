﻿/**********************************************************************************************
 *   Date     |  jira   | Programmer | Description                                            *
 **********************************************************************************************
 * 09/20/2014 | rma-857 | ngupta36  | Incurred Handling - Collection Allowed or Not Allowed
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Collections;
using Riskmaster.Application.Reserves;
using Riskmaster.Security;

namespace Riskmaster.Application.FundManagement
{
    /// <summary>
    /// The ControlRequest class that will be used to 
    /// implement functionality for control request in Riskmaster.
    /// </summary>
    public class ControlRequest : IDisposable
    {
        private int m_iClientId = 0;//sonali
        #region Constructor

        /// <summary>
        /// This is default constructor.
        /// </summary>
        public ControlRequest()
        {
        }

        /// <summary>
        /// Overloaded constructor, which sets the connection string.
        /// </summary>
        /// <param name="p_sConnectionstring">Database connection string</param>
        public ControlRequest(string p_sConnectionstring,int p_iClientId)//sonali
        {
            m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
        }

        /// <summary>
        /// Overloaded constructor, which sets the DataModelFactory Object.
        /// and the user login object
        /// </summary>
        /// <param name="p_objDmf"> The DataModelFactory Object</param>
        /// <param name="p_objLogin"> The user login Object</param>
        public ControlRequest(DataModelFactory p_objDmf, UserLogin p_objLogin, int p_iClientId)//sonali
        {
            m_objDmf = p_objDmf;
            m_sConnectionString = ((Riskmaster.Db.DbConnection)(((Riskmaster.DataModel.Context)(m_objDmf.Context)).DbConn)).ConnectionString.ToString();
            m_sLoginName = p_objLogin.LoginName;
            m_iClientId = p_iClientId;
            m_objLogin = p_objLogin;
            m_objCache = new LocalCache(m_sConnectionString, m_iClientId);
            m_objSettings = new SysSettings(m_sConnectionString,m_iClientId);//sonali
            m_iCarrierClaims = m_objSettings.MultiCovgPerClm;
            m_IsMulticurrencyOn = m_objSettings.UseMultiCurrency;//skhare7 MITS 28309
        }

        # endregion

        #region Destructor
        ~ControlRequest()
        {
            Dispose();
        }
        public void Dispose()
        {
            if (m_objDmf != null)
                m_objDmf.Dispose();
            m_objCache = null;
            m_objLogin = null;
            m_objSettings = null;
        }
        #endregion

        #region Private Variables
        /// <summary>
        /// The UserLogin Object.
        /// </summary>	
        private UserLogin m_objLogin = null;
        /// <summary>
        /// The DataModelFactory Object.
        /// </summary>		
        private DataModelFactory m_objDmf = null;

        /// <summary>
        /// Represents the DSN id.
        /// </summary>
        private long m_lDSN = 0;

        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;

        private bool m_bFundsBRSSplitsFound = false;

        private bool m_bFundsSplitsFound = false;

        private int m_iCarrierClaims = 0;

        private Funds m_FundsObject = null;

        private LocalCache m_objCache = null;

        private string m_sOption = string.Empty;

        private string m_sLoginName = string.Empty;

        private int iCtrlNumber = 0;

        private SysSettings m_objSettings = null;

        private int iReasonTypeCode = 0;
        private int m_IsMulticurrencyOn = 0;//skhare7 MITS 28309
        #endregion

        

        #region Private Functions

        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(objChildNode);
            objChildNode = null;
        }
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            XmlElement objChildNode = null;
            this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
        }
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
            p_objChildNode.InnerText = p_sText;
        }
        private string FormatCurrecy(string sAmount)
        {
            string sCurrecyAmount = string.Empty;
            if (sAmount.StartsWith("$"))
            {
                sCurrecyAmount = sAmount;
            }
            else
            {
                if (string.IsNullOrEmpty(sAmount))
                    sAmount = "0.00";
                decimal dAmount = decimal.Parse(sAmount);
                sCurrecyAmount = string.Format("{0:C}", dAmount);
            }

            return sCurrecyAmount;
        }

        private string FormatDate(string sDate)
        {
            string sFormattedDate = string.Empty;
            if (sDate.Contains("/"))
            {
                sFormattedDate = sDate;
            }
            else
            {
                if (string.IsNullOrEmpty(sDate))
                {
                    sFormattedDate = "";
                }
                else
                {
                    sFormattedDate = Conversion.GetDBDateFormat(sDate, "d");
                }
            }

            return sFormattedDate;
        }
        /// <summary>
        /// 
        /// </summary>
        private int GetPolicyDetails(int iCoverageId, out int iPolicyID, out long lPolUnitRowId, out int iCoveTypeCode)
        {
            int iResult = 0;
            DbReader objReader = null;
            DbConnection objConn = null;
            string sSql = string.Empty;
            iCoveTypeCode = 0;
            iPolicyID = 0;
            lPolUnitRowId = 0;
            try
            {
                sSql = string.Format("SELECT POLICY_X_UNIT.POLICY_UNIT_ROW_ID,COVERAGE_TYPE_CODE , POLICY_X_UNIT.POLICY_ID FROM POLICY_X_CVG_TYPE , POLICY_X_UNIT WHERE POLCVG_ROW_ID = {0} AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID = POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID", iCoverageId);

                if (iCoverageId != 0)
                {
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader.Read())
                    {
                        lPolUnitRowId = objReader.GetInt32(0);
                        iCoveTypeCode = objReader.GetInt32(1);
                        iPolicyID = objReader.GetInt32(2);

                    }
                    objReader.Close();
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.GetPolicyDetails.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
            return iResult;
        }

        private int GetDepartmentId()
        {
            int iDeptId = 0;
            string sSQL = "SELECT DEPT_EID FROM EVENT, CLAIM WHERE EVENT.EVENT_ID = CLAIM.EVENT_ID" +
                        " AND CLAIM.CLAIM_ID = " + m_FundsObject.ClaimId;
            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader.Read())
                {
                    iDeptId = objReader.GetInt("DEPT_EID");
                }
            }
            return iDeptId;
        }

        private int GetPolCvgRowId(int iRcRowId,out int iClaimantEid,out int iDestReserveTypeCode)
        {
            int iPolCvgRowId = 0;
            iClaimantEid = 0;
            iDestReserveTypeCode = 0;
            string sSQL = "SELECT COVERAGE_X_LOSS.POLCVG_ROW_ID,RESERVE_CURRENT.CLAIMANT_EID,RESERVE_CURRENT.RESERVE_TYPE_CODE FROM RESERVE_CURRENT, COVERAGE_X_LOSS WHERE RESERVE_CURRENT.POLCVG_LOSS_ROW_ID= COVERAGE_X_LOSS.CVG_LOSS_ROW_ID AND RC_ROW_ID =" + iRcRowId;

            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {
                if (objReader.Read())
                {
                    iPolCvgRowId = objReader.GetInt("POLCVG_ROW_ID");
                    iClaimantEid = objReader.GetInt("CLAIMANT_EID");
                    iDestReserveTypeCode = objReader.GetInt("RESERVE_TYPE_CODE");
                }
            }
            return iPolCvgRowId;
        }

       

        private string GetSelectedUnit(long lPolicyId, long lUnitCvgRowId, long lPolicyUnitRowId)
        {
            StringBuilder sbSQL;
            string sUnit = string.Empty;
            try
            {
                //rupal:start, policy system interface changes,
                //sSql = string.Format("SELECT v.VIN UNIT FROM VEHICLE v , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'V' AND v.UNIT_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND p.POLICY_UNIT_ROW_ID={2} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID = {0} ) UNION SELECT DISTINCT u.PIN UNIT FROM PROPERTY_UNIT u , POLICY_X_UNIT p , RESERVE_CURRENT,POLICY_X_CVG_TYPE WHERE (p.UNIT_TYPE = 'P' AND u.PROPERTY_ID = p.UNIT_ID AND RESERVE_CURRENT.POLCVG_ROW_ID = {1} AND p.POLICY_UNIT_ROW_ID = {2} AND POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = P.POLICY_UNIT_ROW_ID  AND p.POLICY_ID ={0}) ", lPolicyId.ToString(), lUnitCvgRowId.ToString(), lPolicyUnitRowId.ToString());

                //formatted the query and appended joins in the query for site unit and other unit
                sbSQL = new StringBuilder();
                sbSQL = sbSQL.Append("SELECT  VEHICLE.VIN Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                sbSQL = sbSQL.Append(" INNER JOIN VEHICLE ON VEHICLE.UNIT_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE = 'V' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());


                sbSQL = sbSQL.Append(" UNION");
                sbSQL = sbSQL.Append(" SELECT  PROPERTY_UNIT.PIN Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT");
                sbSQL = sbSQL.Append(" INNER JOIN PROPERTY_UNIT ON PROPERTY_UNIT.PROPERTY_ID = POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='P' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());

                // as of now site and other unit will be available for policy system interface only                        

                sbSQL = sbSQL.Append(" UNION");

                //SITE UNIT - S
                sbSQL = sbSQL.Append(" SELECT  SITE_UNIT.NAME Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                sbSQL = sbSQL.Append(" INNER JOIN SITE_UNIT ON SITE_UNIT.SITE_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='S' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());

                //OTHER UNIT - SU
                sbSQL = sbSQL.Append(" UNION");
                sbSQL = sbSQL.Append(" SELECT ENTITY.LAST_NAME Unit");
                sbSQL = sbSQL.Append(" FROM POLICY_X_UNIT ");
                sbSQL = sbSQL.Append(" INNER JOIN OTHER_UNIT ON OTHER_UNIT.OTHER_UNIT_ID=POLICY_X_UNIT.UNIT_ID AND POLICY_X_UNIT.UNIT_TYPE='SU' AND POLICY_X_UNIT.POLICY_ID= " + lPolicyId.ToString() + " AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN ENTITY ON ENTITY.ENTITY_ID=OTHER_UNIT.ENTITY_ID AND OTHER_UNIT.UNIT_TYPE='SU'");
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE ON POLICY_X_CVG_TYPE.POLICY_UNIT_ROW_ID = POLICY_X_UNIT.POLICY_UNIT_ROW_ID AND POLICY_X_UNIT.POLICY_UNIT_ROW_ID=" + lPolicyUnitRowId.ToString());
                sbSQL = sbSQL.Append(" INNER JOIN RESERVE_CURRENT ON RESERVE_CURRENT.POLCVG_ROW_ID = " + lUnitCvgRowId.ToString());


                //RUPAL:END, POLICY SYSTEM INTERFACE CHANGES
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            sUnit = objReader.GetString("Unit");

                        }
                    }
                }
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.GetSelectedUnit.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                sbSQL = null;
            }
            return sUnit;

        }

        private XmlDocument GetMessageTemplate()
        {
            //asharma326 add CheckCollMove jira 857
            XmlDocument oTemplate = new XmlDocument();
            if (m_iCarrierClaims == 0)
            {
                oTemplate.LoadXml(@"
                <option>
                    <TransTypeCode></TransTypeCode> 
                    <SplitRowId></SplitRowId> 
                    <ReserveTypeCode></ReserveTypeCode> 
                    <Amount></Amount>
                    <CheckCollMove></CheckCollMove>
                </option>
            ");
            }
            else
            {
                //Asharma326 MITS 32734 ADD Policy_ID//asharma326 add CheckCollMove jira 857
                oTemplate.LoadXml(@"
                <option>
                    <claimantname></claimantname> 
                    <PolicyID></PolicyID>
                    <Policy_ID></Policy_ID>
                    <UnitID></UnitID>
                    <CheckCollMove></CheckCollMove>
                    <CoverageTypeCode></CoverageTypeCode>
                    <LossTypeCode></LossTypeCode>
                    <DisabilityCatCode></DisabilityCatCode>
                    <DisabilityTypeCode></DisabilityTypeCode>
                    <ReserveTypeCode></ReserveTypeCode>
                    <Amount></Amount>
                    <SplitRowId></SplitRowId> 
                </option>
            ");
            }

            return oTemplate;
        }

        private void AppendDestKeyGridData(ref XmlDocument objXMLDOM)
        {
            ReserveFunds objReserves = null;
            XmlDocument objXmldoc = null;
            XmlElement objElement = null;
            StringBuilder sbControlRequest = null;
            //rupal:start, mits 33600
            bool bReturn = false;
            int iLossTypeCode = 0;
            string sShortCode=string.Empty;
            string sCodeDesc = string.Empty;
            //rupal:end, mits 33600
            try
            {
                objReserves = new ReserveFunds(m_sConnectionString, m_objLogin, m_iClientId);//rkaur27
                objXmldoc = objReserves.GetReservesBOB(m_FundsObject.ClaimId);

                CreateElement(objXMLDOM.DocumentElement, "DestFinKeysGrid", ref objElement);
                sbControlRequest = new StringBuilder();

                sbControlRequest.Append("<listhead>");
                sbControlRequest.Append("<claimantname>Claimant Name</claimantname>");
                sbControlRequest.Append("<PolicyID>Policy</PolicyID>");
                sbControlRequest.Append("<UnitID>Unit</UnitID>");
                sbControlRequest.Append("<CoverageTypeCode>Coverage Type</CoverageTypeCode>");
                //rupal:start, mits 33600               
                    sbControlRequest.Append("<LossTypeCode>Loss Type</LossTypeCode>");
                    sbControlRequest.Append("<DisabilityCatCode>Disability Category</DisabilityCatCode>");
                    sbControlRequest.Append("<DisabilityTypeCode>Disability Loss Type</DisabilityTypeCode>");
                //rupal:end
                sbControlRequest.Append("<ReserveTypeCode>Reserve Type</ReserveTypeCode>");
                sbControlRequest.Append("</listhead>");
                //JIRA-857 Starts
                int iAllRsvFlag = 0, iPerRsvFlag=0;
                CollRestriction(ref iAllRsvFlag, ref iPerRsvFlag);  // -1 if prevented collection at LOB level.
                //JIRA-857 Ends
                foreach (XmlNode objXmlNode in objXmldoc.SelectNodes("Reserves/Reserve"))
                {
                    sbControlRequest.Append("<option>");
                    sbControlRequest.Append("<claimantname>" + objXmlNode.Attributes["claimantname"].Value + "</claimantname>");
                    sbControlRequest.Append("<PolicyID>" + objXmlNode.Attributes["policyname"].Value + "</PolicyID>");
                    sbControlRequest.Append("<UnitID>" + objXmlNode.Attributes["unit"].Value + "</UnitID>");
                    sbControlRequest.Append("<CoverageTypeCode>" + objXmlNode.Attributes["coveragename"].Value + "</CoverageTypeCode>");
                    //rupal:start, mits 33600

                    iLossTypeCode = Conversion.CastToType<int>(objXmlNode.Attributes["policylosstypecode"].Value, out bReturn);
                    m_objCache.GetCodeInfo(iLossTypeCode, ref sShortCode, ref sCodeDesc, m_objLogin.objUser.NlsCode);
                    if (m_FundsObject.LineOfBusCode != 243)
                    {
                        sbControlRequest.Append("<LossTypeCode>" + sShortCode + " " + sCodeDesc + "</LossTypeCode>");
                        sbControlRequest.Append("<DisabilityCatCode>NA</DisabilityCatCode>");
                        sbControlRequest.Append("<DisabilityTypeCode>NA</DisabilityTypeCode>");
                    }
                    else
                    {
                        sbControlRequest.Append("<DisabilityCatCode>" + objXmlNode.Attributes["disabilitycatdesc"].Value + "</DisabilityCatCode>");
                        sbControlRequest.Append("<DisabilityTypeCode>" + sShortCode + " " + sCodeDesc + "</DisabilityTypeCode>");
                        sbControlRequest.Append("<LossTypeCode>NA</LossTypeCode>");
                    }
                    //rupal:end, mits 33600
                    sbControlRequest.Append("<ReserveTypeCode>" + objXmlNode.Attributes["reservename"].Value + "</ReserveTypeCode>");
                    sbControlRequest.Append("<polcvgrowid>" + objXmlNode.Attributes["polcvgrowid"].Value + "</polcvgrowid>");
                    sbControlRequest.Append("<rcrowid>" + objXmlNode.Attributes["reserveid"].Value + "</rcrowid>");
                    //JIRA-857 Start 
                    if (m_FundsObject.Context.LocalCache.GetRelatedShortCode(Conversion.ConvertStrToInteger(objXmlNode.Attributes["reservetypecode"].Value)) == "R")
                    {
                        sbControlRequest.Append("<preventcollonres>1</preventcollonres>"); // do not prevent in case of Recovery reserve type.
                    }
                    else
                    {
                        //put logic to see if collection prevented at lob level and set to -1.
                        if (iAllRsvFlag == -1)
                        {
                            sbControlRequest.Append("<preventcollonres>-1</preventcollonres>"); // prevent if non-Recovery reserve type and collection not allowed at lob level.
                        }
                        else
                        {
                            sbControlRequest.Append("<preventcollonres>" + objXmlNode.Attributes["preventcollonres"].Value + "</preventcollonres>");
                        }
                    }
                    //JIRA-857 Ends :  
                    sbControlRequest.Append("</option>");
                }

                //Adding blank option for grid to handle the xml for binding.
                sbControlRequest.Append("<option>");
                sbControlRequest.Append("<claimantname></claimantname>");
                sbControlRequest.Append("<PolicyID></PolicyID>");
                sbControlRequest.Append("<UnitID></UnitID>");
                sbControlRequest.Append("<CoverageTypeCode></CoverageTypeCode>");
                //rupal:start, mits 33600                
                 sbControlRequest.Append("<LossTypeCode></LossTypeCode>");
                 sbControlRequest.Append("<DisabilityCatCode></DisabilityCatCode>");
                 sbControlRequest.Append("<DisabilityTypeCode></DisabilityTypeCode>");

                //rupal:end
                sbControlRequest.Append("<ReserveTypeCode></ReserveTypeCode>");
                sbControlRequest.Append("<polcvgrowid></polcvgrowid>");
                sbControlRequest.Append("<rcrowid>0</rcrowid>");
                sbControlRequest.Append("</option>");

                objElement.InnerXml = Utilities.AddTags(sbControlRequest.ToString());

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.AppendDestKeyGridData.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objReserves = null;
                objXmldoc = null;
                objElement = null;
                sbControlRequest = null;
            }

        }

        private void AppendOtherData(ref XmlDocument objXMLDOM)
        {
            int iorgEid = 0;
            int iclm_entityid = 0;
            int iclaimid = 0;
            int ilineofbusinesscode = 0;
            XmlElement objElement = null;
            int iTransId = 0;
            string sTransDate = string.Empty;
            StringBuilder sbControlRequest = null;
            try
            {
                iorgEid = GetDepartmentId();
                iclm_entityid = m_FundsObject.ClaimantEid;
                iclaimid = m_FundsObject.ClaimId;
                ilineofbusinesscode = m_FundsObject.LineOfBusCode;
                iTransId = m_FundsObject.TransId;
                sTransDate = m_FundsObject.TransDate;

                CreateElement(objXMLDOM.DocumentElement, "ControlRequest", ref objElement);

                sbControlRequest = new StringBuilder();

                sbControlRequest.Append("<orgEid>" + iorgEid + "</orgEid>");
                sbControlRequest.Append("<clm_entityid>" + iclm_entityid + "</clm_entityid>");
                sbControlRequest.Append("<claimid>" + iclaimid + "</claimid>");
                sbControlRequest.Append("<lineofbusinesscode>" + ilineofbusinesscode + "</lineofbusinesscode>");
                sbControlRequest.Append("<transid>" + iTransId + "</transid>");
                sbControlRequest.Append("<transdate>" + sTransDate + "</transdate>");

                objElement.InnerXml = sbControlRequest.ToString();

            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.AppendOtherData.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objElement = null;
                sbControlRequest = null;
            }
        }

        /// <summary Append Funds Split Grid Data
        /// Creates an XML structure of Funds Split Detail Data 
        /// </summary>
        private void AppendFundsSplitGridData(DataObject objFunds,ref XmlDocument objXmlDom)
        {
            
            XmlElement objFundsSplitElement = null;
            XmlElement objListHeadXmlElement = null;
            FundsTransSplit objFundsSplitTemp = null;
            FundsTransSplitList objFundsSplitList = null;
            int iCount = 1;
            m_bFundsBRSSplitsFound = false;
            m_bFundsSplitsFound = false;
            StringBuilder sbListHead = null;
            try
            {
                m_FundsObject = (Funds)objFunds;
                CreateElement(objXmlDom.DocumentElement, "FundsSplits", ref objFundsSplitElement);

                // Create HEADER nodes for Grid.
                CreateElement(objFundsSplitElement, "listhead", ref objListHeadXmlElement);
                //CreateElement(objFundsBRSSplitElement, "listhead", ref objBRSListHeadXmlElement);

                sbListHead = new StringBuilder();

                if (m_iCarrierClaims != 0)
                {
                    sbListHead.Append("<claimantname>Claimant Name</claimantname>");
                    sbListHead.Append("<PolicyID>Policy</PolicyID>");
                    sbListHead.Append("<UnitID>Unit</UnitID>");
                    sbListHead.Append("<CoverageTypeCode>Coverage Type</CoverageTypeCode>");
                    //rupal:start, mits 33600
                    sbListHead.Append("<LossTypeCode>Loss Type</LossTypeCode>");
                    sbListHead.Append("<DisabilityCatCode>Disability Category</DisabilityCatCode>");
                    sbListHead.Append("<DisabilityTypeCode>Disability Loss Type</DisabilityTypeCode>");
                    //rupal:end
                    sbListHead.Append("<ReserveTypeCode>Reserve Type</ReserveTypeCode>");
                    sbListHead.Append("<Amount>Amount</Amount>");
                }
                else
                {
                    
                    sbListHead.Append("<TransTypeCode>Transaction Type</TransTypeCode>");
                    sbListHead.Append("<ReserveTypeCode>Reserve Type</ReserveTypeCode>");
                    sbListHead.Append("<Amount>Amount</Amount>");
                    
                }
                

                objListHeadXmlElement.InnerXml = sbListHead.ToString();
                

                objFundsSplitList = m_FundsObject.TransSplitList;
                foreach (FundsTransSplit objFundsSplit in objFundsSplitList)
                {
                    if (objFundsSplit.ControlReqFlag == false || (objFundsSplit.ControlReqFlag == true && objFundsSplit.FundsCRStatus == m_objCache.GetCodeId("NF", "FUNDS_CR_STATUS_CODE")))
                    {
                        XmlForFundsSplit(objFundsSplitElement, objFundsSplit, iCount, m_FundsObject.TransId);
                
                        ++iCount;
                    }
                }
                
                // Add an Extra FundsSplit Node for new data grid binding. 
                objFundsSplitTemp = (FundsTransSplit)m_FundsObject.Context.Factory.GetDataModelObject("FundsTransSplit", false);
                objFundsSplitTemp.FiringScriptFlag = 2;
                objFundsSplitTemp.InitializeScriptData();

                XmlForFundsSplit(objFundsSplitElement, objFundsSplitTemp, iCount, m_FundsObject.TransId);


            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.AppendFundsSplitGridData.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objFundsSplitElement = null;
                objListHeadXmlElement = null;
                objFundsSplitTemp = null;
                objFundsSplitList = null;
                sbListHead = null;
                //m_objCache = null;
            }
        }
        //bsharma33 : MITS 31523, fetch policy details attached to the reserve.
        private void GetPolicyCoverageDetails(int iReserveCurrentId,out int iCoverageId, out string sPolicyCvgShortCode, out string sPolicyCoverageDescription)
        {
            StringBuilder sbSQL;
            string sUnit = string.Empty;
            bool bResult = false;
            iCoverageId = 0;
            sPolicyCvgShortCode = string.Empty;
            sPolicyCoverageDescription = string.Empty;
            try
            {
                sbSQL = new StringBuilder();
                sbSQL = sbSQL.Append(" SELECT RC.RC_ROW_ID,PCVG.POLCVG_ROW_ID,PCVG.COVERAGE_TYPE_CODE, CT.*  FROM RESERVE_CURRENT RC ");
                sbSQL = sbSQL.Append(" INNER JOIN POLICY_X_CVG_TYPE PCVG ON RC.POLCVG_ROW_ID = PCVG.POLCVG_ROW_ID");
                sbSQL = sbSQL.Append(" INNER JOIN CODES_TEXT CT ON PCVG.COVERAGE_TYPE_CODE = CT.CODE_ID AND CT.LANGUAGE_CODE = 1033 where RC.RC_ROW_ID =  " + iReserveCurrentId.ToString());
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            iCoverageId = objReader.GetInt32("POLCVG_ROW_ID"); //Policy Coverage ID
                            sPolicyCvgShortCode = objReader.GetString("SHORT_CODE"); //Policy Coverage Short code
                            sPolicyCoverageDescription = objReader.GetString("CODE_DESC"); //Policy Coverage Short code
                            

                        }
                    }
                }
            }

            catch (RMAppException p_objException)
            {
                throw p_objException;
            }

            finally
            {
                sbSQL = null;
            }

        
        
        }

        private int GetClaimantEidFromReserve(int iRcrowId)
        {
            int iClaimantEid = 0;
            try
            {
                iClaimantEid =Conversion.ConvertObjToInt( DbFactory.ExecuteScalar(m_FundsObject.Context.DbConn.ConnectionString, "SELECT CLAIMANT_EID FROM RESERVE_CURRENT WHERE RC_ROW_ID=" + iRcrowId),m_iClientId);

            }
            catch (Exception e)
            {
                throw e;
            }
            return iClaimantEid;
        }
        private void XmlForFundsSplit(XmlElement p_objRootElement, FundsTransSplit p_objFundsSplit, int p_iCount,int p_iTransId)
        {
            XmlElement objOptionXmlElement = null;
            string sTransTypeCodeDescription = string.Empty;
            string sReserveTypeCodeDescription = string.Empty;
            string sGlAccountCodeDescription = string.Empty;
            int iCoverageId = 0;
            bool bResult = false;
            Policy objPolicy = null;
            Entity objEntity = null;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            int iCoveTypeCode = 0;
            int iclm_entityid = 0;
            double dAmount = 0.0;//skhare7 MITS 28309
            bool blnSuccess = false;
            int iReserveCurrentId = 0;//bsharma33 : MITS 31523
            int iLossTypeCode = 0;
            int iDisabilityCat = 0; 
            int iCvgLossId = 0;
            //rupal:end
            string sCoverageText = string.Empty;//smahajan22 mits 35587
            StringBuilder sSQL = null; //smahajan22 mits 35587
         
            try
            {
                this.CreateElement(p_objRootElement, "option", ref objOptionXmlElement);
            
                XmlDocument oDoc = new XmlDocument();
                XmlDocument oDocOption = GetMessageTemplate();
                oDoc.LoadXml("<option></option>");
                m_sOption = p_objFundsSplit.SerializeObject(oDoc);
           
                if (!string.IsNullOrEmpty(m_sOption))
                {
                   
                    oDoc.LoadXml(m_sOption);
                    if (!string.IsNullOrEmpty(oDoc.DocumentElement.InnerXml))
                    {
                        //iCoverageId = Conversion.CastToType<Int32>(oDoc.SelectSingleNode("//CoverageId").InnerText, out bResult);
                        iReserveCurrentId = Conversion.CastToType<Int32>(oDoc.SelectSingleNode("//RCRowId").InnerText, out bResult);
                        GetPolicyCoverageDetails(iReserveCurrentId, out iCoverageId, out sShortCode, out sCodeDesc);//bsharma33 : MITS 31523 Fetch coverage info on the basis of attached reserve (RCID)
                        XmlNode objFundsTransSplit = oDoc.SelectSingleNode("//FundsTransSplit");
                        if (m_iCarrierClaims != 0)
                        {
                            if (p_objFundsSplit.SplitRowId != 0)
                            {
                               // iclm_entityid = m_FundsObject.ClaimantEid;
                                iclm_entityid = GetClaimantEidFromReserve(p_objFundsSplit.RCRowId);
                                objEntity = (Entity)(Riskmaster.DataModel.DataObject)m_objDmf.GetDataModelObject("Entity", false);
                                objEntity.MoveTo(iclm_entityid);

                                XmlNode objClaimantName = oDocOption.SelectSingleNode("//claimantname");
                                objClaimantName.InnerText = string.IsNullOrEmpty(objEntity.FirstName) ? objEntity.LastName : objEntity.LastName + "," + objEntity.FirstName;
                            }
                            long lPolicyUnitRowId;
                            int iPolicyID;
                            GetPolicyDetails(iCoverageId, out iPolicyID, out lPolicyUnitRowId, out iCoveTypeCode);
            
                            objPolicy = (Policy)(Riskmaster.DataModel.DataObject)m_objDmf.GetDataModelObject("Policy", false);
                            objPolicy.MoveTo(iPolicyID);

                            XmlNode objPolicyOption = oDocOption.SelectSingleNode("//PolicyID");
                            objPolicyOption.InnerText = objPolicy.PolicyName;

                            //Asharma326 MITS 32734 Add Policy_ID
                            XmlNode objPolicyIDOption = oDocOption.SelectSingleNode("//Policy_ID");
                            objPolicyIDOption.InnerText = objPolicy.PolicyId.ToString();
                            //Asharma326 MITS 32734 Add Policy_ID

                            XmlNode objUnitOption = oDocOption.SelectSingleNode("//UnitID");
                            objUnitOption.InnerText = GetSelectedUnit(objPolicy.PolicyId, iCoverageId, lPolicyUnitRowId);

                            //following line commented and changed:rupal
                            //objCache.GetCodeInfo(iResult, ref sShortCode, ref sCodeDesc);                            
                             
                            m_objCache.GetCodeInfo(iCoveTypeCode, ref sShortCode, ref sCodeDesc);

                            XmlNode objCoverageTypeOption = oDocOption.SelectSingleNode("//CoverageTypeCode");
                            //objCoverageTypeOption.InnerText = sShortCode + " " + sCodeDesc;//smahajan22 mits 35587 start
                            if (objPolicy.PolicySystemId > 0)
                            {
                                //get coverage text
                                sSQL = new StringBuilder();
                                sSQL.Append("SELECT COVERAGE_TEXT FROM POLICY_X_CVG_TYPE WHERE  POLCVG_ROW_ID = " + iCoverageId);
                                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString()))
                                {
                                    if (objReader.Read())
                                    {
                                        sCoverageText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                                    }
                                }
                                objCoverageTypeOption.InnerText = sCoverageText;
                            }
                            else
                            {
                                objCoverageTypeOption.InnerText = sShortCode + " " + sCodeDesc;
                            }
                            // smahajan mits 35587 end
                            XmlNode objResTypeOption = oDocOption.SelectSingleNode("//ReserveTypeCode");
                            XmlNode objResType = oDoc.SelectSingleNode("//ReserveTypeCode");
                            objResTypeOption.InnerText = objResType.InnerText;

                            //rupal:start, mits 33600
                            if (p_objFundsSplit.RCRowId > 0)
                            {
                                iLossTypeCode = CommonFunctions.GetLossType(m_sConnectionString, p_objFundsSplit.RCRowId, ref iDisabilityCat, ref  iCvgLossId, m_iClientId);
                            }
                            m_objCache.GetCodeInfo(iLossTypeCode, ref sShortCode, ref sCodeDesc, m_objLogin.objUser.NlsCode);
                            if (m_FundsObject.LineOfBusCode != 243)
                            {
                                XmlNode objLossTypeOption = oDocOption.SelectSingleNode("//LossTypeCode");                                
                                objLossTypeOption.InnerText = sShortCode + " " + sCodeDesc;
                                XmlNode objDisabilityCat = oDocOption.SelectSingleNode("//DisabilityCatCode");
                                objDisabilityCat.InnerText = "NA";
                                XmlNode objDisType = oDocOption.SelectSingleNode("//DisabilityTypeCode");
                                objDisType.InnerText = "NA";
                            }
                            else
                            {
                                XmlNode objDisabilityCat = oDocOption.SelectSingleNode("//DisabilityCatCode");                                
                                iDisabilityCat = p_objFundsSplit.Context.LocalCache.GetRelatedCodeId(iLossTypeCode);
                                m_objCache.GetCodeInfo(iDisabilityCat, ref sShortCode, ref sCodeDesc, m_objLogin.objUser.NlsCode);
                                objDisabilityCat.InnerText = sShortCode + " " + sCodeDesc;
                                XmlNode objDisType = oDocOption.SelectSingleNode("//DisabilityTypeCode");                                
                                m_objCache.GetCodeInfo(iLossTypeCode, ref sShortCode, ref sCodeDesc, m_objLogin.objUser.NlsCode);
                                objDisType.InnerText = sShortCode + " " + sCodeDesc;
                                XmlNode objLossTypeOption = oDocOption.SelectSingleNode("//LossTypeCode");
                                objLossTypeOption.InnerText = "NA";
                            }
                            //rupal:end


                            if (p_objFundsSplit.SplitRowId != 0)
                            {
                                XmlNode objAmountOption = oDocOption.SelectSingleNode("//Amount");
                                if (m_IsMulticurrencyOn == 0)//skhare7 MITs 28309
                                {
                                    XmlNode objAmount = oDoc.SelectSingleNode("//Amount");
                                    objAmountOption.InnerText = FormatCurrecy(objAmount.InnerText);
                                }
                                else
                                {
                                    XmlNode objAmount = oDoc.SelectSingleNode("//PmtCurrencyAmount");
                                    dAmount = Conversion.CastToType<int>(objAmount.InnerText, out blnSuccess);
                                    objAmountOption.InnerText = Riskmaster.Common.CommonFunctions.ConvertCurrency(p_iTransId, dAmount, CommonFunctions.NavFormType.Funds, m_sConnectionString, m_iClientId);
                                
                                }
                                //skhare7 MITs 28309 End
                            }

                        }
                        else
                        {
                            XmlNode objResTypeOption = oDocOption.SelectSingleNode("//ReserveTypeCode");
                            XmlNode objResType = oDoc.SelectSingleNode("//ReserveTypeCode");
                            objResTypeOption.InnerText = objResType.InnerText;

                            XmlNode objTransTypeOption = oDocOption.SelectSingleNode("//TransTypeCode");
                            XmlNode objTransType = oDoc.SelectSingleNode("//TransTypeCode");
                            objTransTypeOption.InnerText = objTransType.InnerText;

                            if (p_objFundsSplit.SplitRowId != 0)
                            {
                                XmlNode objAmountOption = oDocOption.SelectSingleNode("//Amount");
                                //skhare7 MITs 28309
                                if (m_IsMulticurrencyOn == 0)
                                {
                                    XmlNode objAmount = oDoc.SelectSingleNode("//Amount");
                                    objAmountOption.InnerText = FormatCurrecy(objAmount.InnerText);
                                }
                                else
                                {
                                    XmlNode objAmount = oDoc.SelectSingleNode("//PmtCurrencyAmount");
                                  
                                    dAmount = Conversion.CastToType<int>(objAmount.InnerText, out blnSuccess);
                                    objAmountOption.InnerText = Riskmaster.Common.CommonFunctions.ConvertCurrency(p_iTransId, dAmount, CommonFunctions.NavFormType.Funds, m_sConnectionString, m_iClientId);

                                }
                                //skhare7 MITs 28309 End
                            }
                        }

                        //rma-857 Starts
                        
                        XmlNode xmlReserve = oDocOption.SelectSingleNode("//CheckCollMove");
                        int iAllRsvFlag = 0, iPerRsvFlag = 0;
                        this.CollRestriction(ref iAllRsvFlag, ref iPerRsvFlag);
                        xmlReserve.InnerText = Convert.ToString(iAllRsvFlag);
                        //if (m_FundsObject.CollectionFlag)
                        //{
                        //    xmlReserve.InnerText = "0";
                        //}
                        //else
                        //{
                        //    xmlReserve.InnerText = "-1";  //prevent movement if non-collection.
                        //}

                        //rma-857 Ends

                        XmlNode objSplitRowIdOption = oDocOption.SelectSingleNode("//SplitRowId");
                        XmlNode objSplitRowId = oDoc.SelectSingleNode("//SplitRowId");
                        objSplitRowIdOption.InnerText = objSplitRowId.InnerText;
                        
                        objOptionXmlElement.InnerXml = oDocOption.DocumentElement.InnerXml;
                    }
                }
                
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.XmlForFundsSplit.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objOptionXmlElement = null;
                objPolicy = null;
                objEntity = null;
            }
        }
        
        //JIRA-857 Start
        private void CollRestriction(ref int p_PreventCollectionAllReserve, ref int p_PreventCollectionPerReserve)  //int iResTypeCode)
        {
            ColLobSettings objColLobSettings = null; //set to -1 if collection restricted on m_FundsObject
            objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId); //JIRA-857
            //int iLOB = m_FundsObject.LineOfBusCode;
            if (m_FundsObject.LineOfBusCode > 0)
            {
                p_PreventCollectionAllReserve = objColLobSettings[m_FundsObject.LineOfBusCode].PreventCollectionAllReserve;
                p_PreventCollectionPerReserve = objColLobSettings[m_FundsObject.LineOfBusCode].PreventCollectionPerReserve;
            }
        }
        //JIRA-857 ends
        //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
        //private void CreateChangeFundTransaction(ref Funds objFunds, FundsTransSplit objFundsSplit,int iSplitRowId,int iDestPolCovRowId,int iDestResTypeCode,int iDestTransTypeCode,int iClaimantEid)
        //start - Changed by Nikhil for deductiblie
        //private void CreateChangeFundTransaction(ref Funds objFunds, FundsTransSplit objFundsSplit, int iSplitRowId, int iDestRCRowId, int iDestResTypeCode, int iDestTransTypeCode, int iClaimantEid)
            private void CreateChangeFundTransaction(ref Funds objFunds, FundsTransSplit objFundsSplit, int iSplitRowId, int iDestRCRowId, int iDestResTypeCode, int iDestTransTypeCode, int iClaimantEid,int iDestPolCovRowId)
        //end - Changed by Nikhil for deductiblie
        {
            
           // FundsTransSplit oNewFundSplit = null;
            XmlDocument objXmlDocFunds = null;
            //SysSettings objSysSettings = null;
            //FundsTransSplitList objFundsSplitList = null;
            try
            {
                objXmlDocFunds = new XmlDocument();
                //commented by Amitosh because new funds object is cloned from old object.
                 objFunds = m_FundsObject.CreateFundsClone();
                //objFunds = m_FundsObject;//bsharma33 : MITS 31525
                //objSysSettings = new SysSettings(m_sConnectionString);
                //objXmlDocFunds.InnerXml = m_FundsObject.SerializeObject();
                //objFunds.PopulateObject(objXmlDocFunds);
                objFunds.ClaimantEid = iClaimantEid;
                objFunds.TransId = 0;
                objFunds.CtlNumber = string.Empty;

                //objFundsSplitList = objFunds.TransSplitList;
                //foreach (FundsTransSplit objSplit in objFundsSplitList)
                //{
                //    objSplit.TransId = 0;
                //    objFunds.TransSplitList.Remove(objFundsSplit.SplitRowId);
                //}

                //objFunds.ClaimId = m_FundsObject.ClaimId;
                //objFunds.ClaimNumber = m_FundsObject.ClaimNumber;
                //objFunds.AccountId = m_FundsObject.AccountId;
                //objFunds.SubAccountId = m_FundsObject.SubAccountId;
                //objFunds.TransDate = m_FundsObject.TransDate;
                //objFunds.StatusCode = m_FundsObject.StatusCode;
                //objFunds.DateOfCheck = m_FundsObject.DateOfCheck;
                //objFunds.PaymentFlag = m_FundsObject.PaymentFlag;
                //objFunds.CheckMemo = m_FundsObject.CheckMemo;
                //objFunds.EnclosureFlag = m_FundsObject.EnclosureFlag;
                //objFunds.TransNumber = m_FundsObject.TransNumber;
                objFunds.WithholdingType = m_FundsObject.Context.LocalCache.GetCodeId("BU", "WITHHOLDING_TYPE");
                objFunds.WithholdingChildFlag = false;
                objFunds.WithholdingPMTFlag = false;
                //objFunds.AutoCheckFlag = m_FundsObject.AutoCheckFlag;
                //objFunds.PrecheckFlag = m_FundsObject.PrecheckFlag;
                //objFunds.BatchNumber = m_FundsObject.BatchNumber;
                //objFunds.ClaimantEid = m_FundsObject.ClaimantEid;
                //objFunds.VoucherFlag = m_FundsObject.VoucherFlag;
                //objFunds.PayeeTypeCode = m_FundsObject.PayeeTypeCode;
                //objFunds.PayeeEid = m_FundsObject.PayeeEid;
                //objFunds.FirstName = m_FundsObject.FirstName;
                //objFunds.LastName = m_FundsObject.LastName;
                //objFunds.Addr1 = m_FundsObject.Addr1;
                //objFunds.Addr2 = m_FundsObject.Addr2;
                //objFunds.City = m_FundsObject.City;
                //objFunds.StateId = m_FundsObject.StateId;
                //objFunds.CountryCode = m_FundsObject.CountryCode;
                //objFunds.ZipCode = m_FundsObject.ZipCode;
                //objFunds.ClaimCurrencyType = m_FundsObject.ClaimCurrencyType;
                //objFunds.ClaimCurrencyAmount = m_FundsObject.ClaimCurrencyAmount;
                //objFunds.PmtCurrencyType = m_FundsObject.PmtCurrencyType;
                //objFunds.PmtCurrencyAmount = m_FundsObject.PmtCurrencyAmount;
                //objFunds.PmtToBaseCurRate = m_FundsObject.PmtToBaseCurRate;
                //objFunds.PmtToClaimCurRate = m_FundsObject.PmtToClaimCurRate;

                //oNewFundSplit = objFunds.TransSplitList.AddNew();

                //objXmlDocFunds.InnerXml = objFundsSplit.SerializeObject();
                //oNewFundSplit.PopulateObject(objXmlDocFunds);
				//bsharma33 : MITS 31525, start changes.
               // int i = -1;
                foreach (FundsTransSplit oNewFundSplit in objFunds.TransSplitList)
                {
                    //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                    //oNewFundSplit.CoverageId = iDestPolCovRowId;
                    if (oNewFundSplit.SplitRowId == iSplitRowId)
                    {
                        oNewFundSplit.RCRowId = iDestRCRowId;
                        //oNewFundSplit.SplitRowId = 0;
                        //oNewFundSplit.TransId = 0;
                        oNewFundSplit.DttmRcdAdded = string.Empty;
                        oNewFundSplit.DttmRcdLastUpd = string.Empty;
                        oNewFundSplit.AddedByUser = string.Empty;
                        oNewFundSplit.ParentSplitRowId = iSplitRowId;
                        oNewFundSplit.ControlReqFlag = true;
                        oNewFundSplit.ReserveTypeCode = iDestResTypeCode;
                        oNewFundSplit.TransTypeCode = iDestTransTypeCode;
                        oNewFundSplit.FundsCRStatus = m_objCache.GetCodeId("NF", "FUNDS_CR_STATUS_CODE");
                        //Added by Nikhil for deductiblie
                        oNewFundSplit.CoverageId = iDestPolCovRowId;
                    }
                    else
                    {
                        oNewFundSplit.SplitRowId = 0;
                    }
                    //oNewFundSplit.SplitRowId = i;
                    //oNewFundSplit.ParentSplitRowId = 0;
                    //i--;
                    //bsharma33 : MITS 31525, end changes.
                }
                //objFunds.Amount = oNewFundSplit.Amount;
                objFunds.Amount = 0;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.ProcessControlRequest.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objXmlDocFunds = null;
                //objSysSettings = null;
            }
        }

        private void CreateOffSettingFundsTransaction(ref Funds objFunds, FundsTransSplit objFundsSplit, int iSplitRowId)
        {

            //FundsTransSplit oNewFundSplit = null;
            XmlDocument objXmlDocFunds = null;
            //SysSettings objSysSettings = null;
            //FundsTransSplitList objFundsSplitList = null;
            try
            {
                objXmlDocFunds = new XmlDocument();
                //Commented by Amitosh because new funds object is cloned from previous object.
                objFunds = m_FundsObject.CreateFundsClone();
                //objSysSettings = new SysSettings(m_sConnectionString);
                //objXmlDocFunds.InnerXml = m_FundsObject.SerializeObject();
                //objFunds.PopulateObject(objXmlDocFunds);
                objFunds.TransId = 0;
                objFunds.CtlNumber = string.Empty;
                
                //objFundsSplitList = objFunds.TransSplitList;
                //foreach (FundsTransSplit objSplit in objFundsSplitList)
                //{
                //    objSplit.TransId = 0;
                //    objFunds.TransSplitList.Remove(objFundsSplit.SplitRowId);
                //}

                //objFunds.ClaimId = m_FundsObject.ClaimId;
                //objFunds.ClaimNumber = m_FundsObject.ClaimNumber;
                //objFunds.AccountId = m_FundsObject.AccountId;
                //objFunds.SubAccountId = m_FundsObject.SubAccountId;
                //objFunds.TransDate = m_FundsObject.TransDate;
                //objFunds.StatusCode = m_FundsObject.StatusCode;
                //objFunds.DateOfCheck = m_FundsObject.DateOfCheck;
                //objFunds.PaymentFlag = m_FundsObject.PaymentFlag;
                //objFunds.CheckMemo = m_FundsObject.CheckMemo;
                //objFunds.EnclosureFlag = m_FundsObject.EnclosureFlag;
                //objFunds.TransNumber = m_FundsObject.TransNumber;
                objFunds.WithholdingType = m_FundsObject.Context.LocalCache.GetCodeId("BU", "WITHHOLDING_TYPE");
                objFunds.WithholdingChildFlag = false;
                objFunds.WithholdingPMTFlag = false;
                //objFunds.AutoCheckFlag = m_FundsObject.AutoCheckFlag;
                //objFunds.PrecheckFlag = m_FundsObject.PrecheckFlag;
                //objFunds.BatchNumber = m_FundsObject.BatchNumber;
                //objFunds.ClaimantEid = m_FundsObject.ClaimantEid;
                //objFunds.VoucherFlag = m_FundsObject.VoucherFlag;
                //objFunds.PayeeTypeCode = m_FundsObject.PayeeTypeCode;
                //objFunds.PayeeEid = m_FundsObject.PayeeEid;
                //objFunds.FirstName = m_FundsObject.FirstName;
                //objFunds.LastName = m_FundsObject.LastName;
                //objFunds.Addr1 = m_FundsObject.Addr1;
                //objFunds.Addr2 = m_FundsObject.Addr2;
                //objFunds.City = m_FundsObject.City;
                //objFunds.StateId = m_FundsObject.StateId;
                //objFunds.CountryCode = m_FundsObject.CountryCode;
                //objFunds.ZipCode = m_FundsObject.ZipCode;
                //objFunds.ClaimCurrencyType = m_FundsObject.ClaimCurrencyType;
                //objFunds.ClaimCurrencyAmount = m_FundsObject.ClaimCurrencyAmount;
                //objFunds.PmtCurrencyType = m_FundsObject.PmtCurrencyType;
                //objFunds.PmtCurrencyAmount = m_FundsObject.PmtCurrencyAmount;
                //objFunds.PmtToBaseCurRate = m_FundsObject.PmtToBaseCurRate;
                //objFunds.PmtToClaimCurRate = m_FundsObject.PmtToClaimCurRate;

              //  oNewFundSplit = objFunds.TransSplitList.AddNew();

                //objXmlDocFunds.InnerXml = objFundsSplit.SerializeObject();
                //oNewFundSplit.PopulateObject(objXmlDocFunds);
                foreach (FundsTransSplit oNewFundSplit in objFunds.TransSplitList)
                {
                    oNewFundSplit.Amount = -(oNewFundSplit.Amount);
                    oNewFundSplit.SumAmount = oNewFundSplit.Amount;
                    oNewFundSplit.SplitRowId = 0;
                    oNewFundSplit.TransId = 0;
                    oNewFundSplit.DttmRcdAdded = string.Empty;
                    oNewFundSplit.DttmRcdLastUpd = string.Empty;
                    oNewFundSplit.AddedByUser = string.Empty;
                    oNewFundSplit.ParentSplitRowId = iSplitRowId;
                    oNewFundSplit.ControlReqFlag = true;
                    oNewFundSplit.FundsCRStatus = m_objCache.GetCodeId("OS", "FUNDS_CR_STATUS_CODE");
                }
                //objFunds.Amount = oNewFundSplit.Amount;
                //objFunds.Amount = 0;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.ProcessControlRequest.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objXmlDocFunds = null;
                //objSysSettings = null;
            }
        }

        private void CreateNegativeSplit(Funds FundsObject, FundsTransSplit objFundsSplit,int iSplitRowId)
        {
            FundsTransSplit oNewFundSplit = null;
            XmlDocument objXmlDoc = null;
            //SysSettings objSysSettings = null;
            try
            {
                objXmlDoc = new XmlDocument();
                //objSysSettings = new SysSettings(m_sConnectionString);
                //m_objCache = new LocalCache(m_sConnectionString);
                //Create Negative split
                oNewFundSplit = FundsObject.TransSplitList.AddNew();
                objXmlDoc.InnerXml = objFundsSplit.SerializeObject();
                oNewFundSplit.PopulateObject(objXmlDoc);
                if (m_IsMulticurrencyOn == 0)//skhare7 MITS 28309
                 oNewFundSplit.Amount = -(oNewFundSplit.Amount);
                else
                {
                    oNewFundSplit.PmtCurrencyAmount = -(oNewFundSplit.PmtCurrencyAmount);
                    oNewFundSplit.Amount = -(oNewFundSplit.Amount);
                }
                oNewFundSplit.SplitRowId = 0;
                oNewFundSplit.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
                oNewFundSplit.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                oNewFundSplit.ParentSplitRowId = iSplitRowId;
                oNewFundSplit.ControlReqFlag = true;
                oNewFundSplit.FundsCRStatus = m_objCache.GetCodeId("OS", "FUNDS_CR_STATUS_CODE");
                //FundsObject.TransSplitList.Add(oNewFundSplit);
                //oNewFundSplit.Save();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.ProcessControlRequest.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                //oNewFundSplit = null;
                objXmlDoc = null;
                //objSysSettings = null;
                //m_objCache = null;
            }
        }

        private void CreateChangeSplit(int iDestReserveTypeCodeid, int iDestTransTypeCodeid, FundsTransSplit objFundsSplit,int iSpliRowId)
        {
            FundsTransSplit oNewFundSplit = null;
            XmlDocument objXmlDoc = null;
            //SysSettings objSysSettings = null;
            try
            {
                objXmlDoc = new XmlDocument();
                //objSysSettings = new SysSettings(m_sConnectionString);
                //m_objCache = new LocalCache(m_sConnectionString);
                //Create Change split
                oNewFundSplit = m_FundsObject.TransSplitList.AddNew();
                objXmlDoc.InnerXml = objFundsSplit.SerializeObject();
                oNewFundSplit.PopulateObject(objXmlDoc);
                //Add changed transaction type and reserve types to the newly added split.

                oNewFundSplit.TransTypeCode = iDestTransTypeCodeid;
                oNewFundSplit.ReserveTypeCode = iDestReserveTypeCodeid;
                oNewFundSplit.SplitRowId = 0;
                oNewFundSplit.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
                oNewFundSplit.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                oNewFundSplit.ParentSplitRowId = iSpliRowId;
                oNewFundSplit.ControlReqFlag = true;
                oNewFundSplit.FundsCRStatus = m_objCache.GetCodeId("NF", "FUNDS_CR_STATUS_CODE");
                //FundsObject.TransSplitList.Add(oNewFundSplit);
                //oNewFundSplit.Save();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.ProcessControlRequest.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objXmlDoc = null;
                //objSysSettings = null;
            }
        }

        private void CreateControlRequestHist(FundsTransSplit objFundsSplit, int iSplitRowId)
        {
            DbConnection objCn = null;
            string sSQL = string.Empty;
            int iCtrlReqId = 0;


            try
            {
                objCn = DbFactory.GetDbConnection(m_sConnectionString);
                objCn.Open();
                //m_objCache = new LocalCache(m_sConnectionString);

                if (objFundsSplit.ControlReqFlag && objFundsSplit.ParentSplitRowId == iSplitRowId)
                {
                    if (objFundsSplit.FundsCRStatus == m_objCache.GetCodeId("OS", "FUNDS_CR_STATUS_CODE"))
                    {
                        iCtrlReqId = Utilities.GetNextUID(m_sConnectionString, "CONTROL_REQUEST", m_iClientId);
                        sSQL = "INSERT INTO CONTROL_REQUEST (CONTROL_REQUEST_ID,CONTROL_NUMBER,CLAIM_ID,TABLE_NAME,PARENT_ID," +
                        "DESTINATION_ID,CR_STATUS_CODE ,CR_TYPE_CODE,DTTM_RCD_ADDED,ADDED_BY_USER,CR_REASON_CODE) VALUES(" + iCtrlReqId +
                        "," + iCtrlNumber + "," + m_FundsObject.ClaimId + ",'" + "FUNDS_TRANS_SPLIT" + "'," + iSplitRowId + ","
                        + objFundsSplit.SplitRowId + "," + m_objCache.GetCodeId("C", "CR_STATUS") +
                        "," + m_objCache.GetCodeId("R", "CR_CHANGE_TYPE") + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "','" + m_sLoginName + "'," + iReasonTypeCode + ")";
                        objCn.ExecuteNonQuery(sSQL);
                    }
                    else if (objFundsSplit.FundsCRStatus == m_objCache.GetCodeId("NF", "FUNDS_CR_STATUS_CODE"))
                    {
                        iCtrlReqId = Utilities.GetNextUID(m_sConnectionString, "CONTROL_REQUEST", m_iClientId);
                        sSQL = "INSERT INTO CONTROL_REQUEST (CONTROL_REQUEST_ID,CONTROL_NUMBER,CLAIM_ID,TABLE_NAME,PARENT_ID," +
                        "DESTINATION_ID,CR_STATUS_CODE ,CR_TYPE_CODE,DTTM_RCD_ADDED,ADDED_BY_USER,CR_REASON_CODE) VALUES(" + iCtrlReqId +
                        "," + iCtrlNumber + "," + m_FundsObject.ClaimId + ",'" + "FUNDS_TRANS_SPLIT" + "'," + iSplitRowId + ","
                        + objFundsSplit.SplitRowId + "," + m_objCache.GetCodeId("C", "CR_STATUS") +
                        "," + m_objCache.GetCodeId("CP", "CR_CHANGE_TYPE") + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "','" + m_sLoginName + "'," + iReasonTypeCode + ")";
                        objCn.ExecuteNonQuery(sSQL);
                    }
                }
                else if (objFundsSplit.SplitRowId == iSplitRowId)
                {
                    iCtrlReqId = Utilities.GetNextUID(m_sConnectionString, "CONTROL_REQUEST", m_iClientId);
                    sSQL = "INSERT INTO CONTROL_REQUEST (CONTROL_REQUEST_ID,CONTROL_NUMBER,CLAIM_ID,TABLE_NAME,PARENT_ID," +
                    "DESTINATION_ID,CR_STATUS_CODE ,CR_TYPE_CODE,DTTM_RCD_ADDED,ADDED_BY_USER,CR_REASON_CODE) VALUES(" + iCtrlReqId +
                    "," + iCtrlNumber + "," + m_FundsObject.ClaimId + ",'" + "FUNDS_TRANS_SPLIT" + "'," + iSplitRowId + ","
                    + iSplitRowId + "," + m_objCache.GetCodeId("C", "CR_STATUS") +
                    "," + m_objCache.GetCodeId("U", "CR_CHANGE_TYPE") + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "','" + m_sLoginName + "'," + iReasonTypeCode + ")";
                    objCn.ExecuteNonQuery(sSQL);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.ProcessControlHistory.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                if (objCn != null)
                {
                    objCn.Close();
                    objCn = null;
                }
            }
        }

        private void CreateControlRequestHistCarrier(Funds objFundsOrig, Funds objFundsNew,Funds objFundsOffSetting, int iSplitRowId)
        {
            DbConnection objCn = null;
            string sSQL = string.Empty;
            int iCtrlReqId = 0;
            FundsTransSplitList objFundsSplitList = null;

            try
            {
                objCn = DbFactory.GetDbConnection(m_sConnectionString);
                objCn.Open();

                objFundsSplitList = objFundsOffSetting.TransSplitList;
                foreach (FundsTransSplit objFundsSplit in objFundsSplitList)
                {
                    if (objFundsSplit.ControlReqFlag && objFundsSplit.ParentSplitRowId == iSplitRowId && objFundsSplit.FundsCRStatus == m_objCache.GetCodeId("OS", "FUNDS_CR_STATUS_CODE"))
                    {
                        iCtrlReqId = Utilities.GetNextUID(m_sConnectionString, "CONTROL_REQUEST", m_iClientId);
                        sSQL = "INSERT INTO CONTROL_REQUEST (CONTROL_REQUEST_ID,CONTROL_NUMBER,CLAIM_ID,TABLE_NAME,PARENT_ID," +
                        "DESTINATION_ID,CR_STATUS_CODE ,CR_TYPE_CODE,DTTM_RCD_ADDED,ADDED_BY_USER,CR_REASON_CODE) VALUES(" + iCtrlReqId +
                        "," + iCtrlNumber + "," + m_FundsObject.ClaimId + ",'" + "FUNDS" + "'," + objFundsOrig.TransId + ","
                        + objFundsSplit.TransId + "," + m_objCache.GetCodeId("C", "CR_STATUS") +
                        "," + m_objCache.GetCodeId("R", "CR_CHANGE_TYPE") + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "','" + m_sLoginName + "'," + iReasonTypeCode + ")";
                        objCn.ExecuteNonQuery(sSQL);
                    }
                }
                objFundsSplitList = objFundsNew.TransSplitList;
                foreach (FundsTransSplit objFundsSplit in objFundsSplitList)
                {
                    if (objFundsSplit.ControlReqFlag && objFundsSplit.ParentSplitRowId == iSplitRowId && objFundsSplit.FundsCRStatus == m_objCache.GetCodeId("NF", "FUNDS_CR_STATUS_CODE"))
                    {
                        iCtrlReqId = Utilities.GetNextUID(m_sConnectionString, "CONTROL_REQUEST", m_iClientId);
                        sSQL = "INSERT INTO CONTROL_REQUEST (CONTROL_REQUEST_ID,CONTROL_NUMBER,CLAIM_ID,TABLE_NAME,PARENT_ID," +
                        "DESTINATION_ID,CR_STATUS_CODE ,CR_TYPE_CODE,DTTM_RCD_ADDED,ADDED_BY_USER,CR_REASON_CODE) VALUES(" + iCtrlReqId +
                        "," + iCtrlNumber + "," + m_FundsObject.ClaimId + ",'" + "FUNDS" + "'," + objFundsOrig.TransId + ","
                        + objFundsSplit.TransId + "," + m_objCache.GetCodeId("C", "CR_STATUS") +
                        "," + m_objCache.GetCodeId("CP", "CR_CHANGE_TYPE") + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "','" + m_sLoginName + "'," + iReasonTypeCode + ")";
                        objCn.ExecuteNonQuery(sSQL);
                    }
                }

                iCtrlReqId = Utilities.GetNextUID(m_sConnectionString, "CONTROL_REQUEST", m_iClientId);
                sSQL = "INSERT INTO CONTROL_REQUEST (CONTROL_REQUEST_ID,CONTROL_NUMBER,CLAIM_ID,TABLE_NAME,PARENT_ID," +
                "DESTINATION_ID,CR_STATUS_CODE ,CR_TYPE_CODE,DTTM_RCD_ADDED,ADDED_BY_USER,CR_REASON_CODE) VALUES(" + iCtrlReqId +
                "," + iCtrlNumber + "," + m_FundsObject.ClaimId + ",'" + "FUNDS" + "'," + objFundsOrig.TransId + ","
                + objFundsOrig.TransId + "," + m_objCache.GetCodeId("C", "CR_STATUS") +
                "," + m_objCache.GetCodeId("U", "CR_CHANGE_TYPE") + ",'" + Conversion.ToDbDateTime(DateTime.Now) + "','" + m_sLoginName + "'," + iReasonTypeCode + ")";
                objCn.ExecuteNonQuery(sSQL);
    
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.ProcessControlHistory.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objFundsSplitList = null;
                if (objCn != null)
                {
                    objCn.Close();
                    objCn = null;
                }
            }
        }
        #endregion

        #region Public Functions

        #region Get Search Results For Selected Entity
        /// <summary>
        /// This function retrieves the record for passed in Entity-Id.
        /// </summary>
        /// <param name="p_iEntityId">The Entity Id for which the results have to be returned</param>
        /// <returns>XML DOM containing data corresponding to passed in entity ID.</returns>
        public XmlDocument GetSearchResultsForSelectedFund(int p_iEntityId, bool isRequestProcessed) //mkaran2 - Control Request Functionlity  - MITS 34569
        {
            XmlDocument objXMLDOM = null;
            //XmlDocument objTempDom = null;
            Riskmaster.DataModel.DataObject objFunds = null;
            string sData = string.Empty;
            string sLookUpType = string.Empty;
            string sTemp = string.Empty;
            
            try
            {
                objXMLDOM = new XmlDocument();
                //objTempDom = new XmlDocument();
                //TBD check for p_sType.

                //string sSerializationConfigXml = "<Funds>" +
                //                "	<TransSplitList>" +
                //                "		<FundsTransSplit/>" +
                //                "	</TransSplitList>" +
                //                "</Funds>";

                //objTempDom.LoadXml(sSerializationConfigXml);


                objFunds = (Riskmaster.DataModel.DataObject)m_objDmf.GetDataModelObject("Funds", false);
                objFunds.MoveTo(p_iEntityId);

                //if (objTempDom != null)
                //    sData = objEntity.SerializeObject(objTempDom);
                //else
                //    sData = objEntity.SerializeObject();

                //if (sData.Length > 0)
                //    objXMLDOM.LoadXml(sData);
                //else
                //    throw new RMAppException(Globalization.GetString("SearchXML.GetSearchResultsForSelectedEntity.NoDataErr"));

                //mkaran2 - Control Request Functionlity  - MITS 34569
                //if (((Funds)objFunds).StatusCode != m_objCache.GetCodeId("P", "CHECK_STATUS") || ((Funds)objFunds).VoidFlag) 
                if (!isRequestProcessed && (((Funds)objFunds).StatusCode != m_objCache.GetCodeId("P", "CHECK_STATUS") || ((Funds)objFunds).VoidFlag))
                {
                    throw new RMAppException(Globalization.GetString("ControlRequest.GetSearchResultsForSelectedFund.ValidationError",m_iClientId));//sonali
                }
                objXMLDOM.LoadXml("<Funds/>");
                AppendFundsSplitGridData(objFunds, ref objXMLDOM);
                AppendOtherData(ref objXMLDOM);
                if (m_iCarrierClaims != 0)
                {
                    AppendDestKeyGridData(ref objXMLDOM);
                }
            }
            catch (XmlOperationException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ControlRequest.GetSearchResultsForSelectedFund.GenericError",m_iClientId), p_objException);//sonali
            }
            finally
            {
                if (objFunds != null)
                    objFunds = null;
            }

            return (objXMLDOM);
        }

        #endregion

        public XmlDocument ProcessControlRequest(int iTransId, int iSplitRowId, int iDestReserveTypeCodeId, int iDestTransTypeCodeId, int iDestRCRowId, int p_iReasonTypeCode)
        {
            FundsTransSplitList objFundsSplitList = null;
            XmlDocument objXmlDoc = new XmlDocument();
            int iDestPolCvgRowId = 0;
            int iClaimantEid = 0;
            Funds objNewFund = null;
            Funds objOffSettingFund = null;
            int iAllRsvFlag = 0, iPerRsvFlag = 0;//857
            try
            {
                iReasonTypeCode = p_iReasonTypeCode;
                m_FundsObject = (Funds)m_objDmf.GetDataModelObject("Funds", false);
                m_FundsObject.MoveTo(iTransId);
                objFundsSplitList = m_FundsObject.TransSplitList;
                this.CollRestriction(ref iAllRsvFlag, ref iPerRsvFlag);
                //foreach (FundsTransSplit objFundsSplit in objFundsSplitList)
                //{
                    
                //}
                
                if (m_iCarrierClaims == 0)
                {

                    //m_objCache = new LocalCache(m_sConnectionString);//skhare7 MITS 28309
                    foreach (FundsTransSplit objFundsSplit in objFundsSplitList)
                    {
                        //asharma326 JIRA 857 Starts
                        if (m_FundsObject.CollectionFlag && objFundsSplit.Context.LocalCache.GetRelatedShortCode(iDestReserveTypeCodeId) != "R")
                        {
                            if (iAllRsvFlag.Equals(-1))
                            {
                                throw new RMAppException(Globalization.GetString("ValidationPreventCollectionAllReserve", m_iClientId));
                            }
                            else if (iPerRsvFlag.Equals(-1))
                            {
                                object objResTypeCode = DbFactory.ExecuteScalar(m_sConnectionString, @"SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE 
                                                                                           = " + m_FundsObject.LineOfBusCode + " AND RES_TYPE_CODE = " + iDestReserveTypeCodeId);
                                if (objResTypeCode != null)
                                {
                                    if (objResTypeCode != null)
                                    {
                                        throw new RMAppException(Globalization.GetString("ValidationPreventCollectionPerReserve", m_iClientId).Replace("%%1", (objFundsSplit.Context.LocalCache.GetShortCode(iDestReserveTypeCodeId)) + " " + objFundsSplit.Context.LocalCache.GetCodeDesc(iDestReserveTypeCodeId)));
                                    }
                                }
                            }
                        }
                        //asharma326 JIRA 857 Ends
                        if (m_FundsObject.PaymentFlag && objFundsSplit.Context.LocalCache.GetRelatedShortCode(iDestReserveTypeCodeId) == "R")
                        {

                            throw new RMAppException(Globalization.GetString("ControlRequest.ValidationError.PaymentOnRecoveryReserve",m_iClientId));//sonali
                        }
                        //skhare7 MITS 28309 End
                        if (objFundsSplit.SplitRowId == iSplitRowId)
                        {
                            CreateNegativeSplit(m_FundsObject, objFundsSplit, iSplitRowId);
                            CreateChangeSplit(iDestReserveTypeCodeId, iDestTransTypeCodeId, objFundsSplit, iSplitRowId);
                            objFundsSplit.ControlReqFlag = true;
                            objFundsSplit.FundsCRStatus = m_objCache.GetCodeId("O", "FUNDS_CR_STATUS_CODE");
                            break;
                        }
                    }
                    m_FundsObject.Save();
                    objFundsSplitList = m_FundsObject.TransSplitList;
                    iCtrlNumber = Utilities.GetNextUID(m_sConnectionString, "CONTROL_NUMBER", m_iClientId);
                    foreach (FundsTransSplit objFundsSplit in objFundsSplitList)
                    {
                        CreateControlRequestHist(objFundsSplit, iSplitRowId);
                    }
                }
                else
                {
                    iDestPolCvgRowId = GetPolCvgRowId(iDestRCRowId,out iClaimantEid, out iDestReserveTypeCodeId);
                    objNewFund = (Funds)m_objDmf.GetDataModelObject("Funds", false);
                    objOffSettingFund = (Funds)m_objDmf.GetDataModelObject("Funds", false);
                    foreach (FundsTransSplit objFundsSplit in objFundsSplitList)
                    {
                        //asharma326 JIRA 857 Starts
                        if (m_FundsObject.CollectionFlag && objFundsSplit.Context.LocalCache.GetRelatedShortCode(iDestReserveTypeCodeId) != "R")
                        {
                            if (iAllRsvFlag.Equals(-1))
                            {
                                throw new RMAppException(Globalization.GetString("ValidationPreventCollectionAllReserve", m_iClientId));
                            }
                            else if (iPerRsvFlag.Equals(-1))
                            {
                                object objResTypeCode = DbFactory.ExecuteScalar(m_sConnectionString, @"SELECT RES_TYPE_CODE FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE 
                                                                                           = " + m_FundsObject.LineOfBusCode + " AND RES_TYPE_CODE = " + iDestReserveTypeCodeId);
                                if (objResTypeCode != null)
                                {
                                    if (objResTypeCode != null)
                                    {
                                        throw new RMAppException(Globalization.GetString("ValidationPreventCollectionPerReserve", m_iClientId).Replace("%%1", (objFundsSplit.Context.LocalCache.GetShortCode(iDestReserveTypeCodeId)) + " " + objFundsSplit.Context.LocalCache.GetCodeDesc(iDestReserveTypeCodeId)));
                                    }
                                }
                            }
                        }
                        //asharma326 JIRA 857 Ends
                        //skhare7 MITS 28309
                        if (m_FundsObject.PaymentFlag && objFundsSplit.Context.LocalCache.GetRelatedShortCode(iDestReserveTypeCodeId) == "R")
                        {

                            throw new RMAppException(Globalization.GetString("ControlRequest.ValidationError.PaymentOnRecoveryReserve",m_iClientId));//sonali
                        }
                        //skhare7 MITS 28309 End
                        if (objFundsSplit.SplitRowId == iSplitRowId)
                        {
                            //mkaran2 - Control Request Functionlity  - MITS 34569
                           // CreateOffSettingFundsTransaction(ref objOffSettingFund, objFundsSplit, iSplitRowId);
                            //Mona: Maintaining relation b/w FUNDS_TRANS_SPLIT and RESERVE_CURRENT through RC_ROW_ID 
                            //CreateChangeFundTransaction(ref objNewFund, objFundsSplit, iSplitRowId, iDestPolCvgRowId, iDestReserveTypeCodeId, iDestTransTypeCodeId, iClaimantEid);
                            //start - Changed by Nikhil for deductiblie
                           // CreateChangeFundTransaction(ref objNewFund, objFundsSplit, iSplitRowId, iDestRCRowId, iDestReserveTypeCodeId, iDestTransTypeCodeId, iClaimantEid);
                            CreateChangeFundTransaction(ref objNewFund, objFundsSplit, iSplitRowId, iDestRCRowId, iDestReserveTypeCodeId, iDestTransTypeCodeId, iClaimantEid, iDestPolCvgRowId);
                            //end - Changed by Nikhil for deductiblie
                            //objNewFund.ClaimantEid = iClaimantEid;
                            objFundsSplit.FundsCRStatus = m_objCache.GetCodeId("O", "FUNDS_CR_STATUS_CODE");
                            objFundsSplit.ControlReqFlag = true;
                            //objFundsSplit.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                            //m_FundsObject.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                            break;
                        }
                    }
                    //mkaran2 - Control Request Functionlity : Start  - MITS 34569
                    m_FundsObject.VoidFlag = true;
                    m_FundsObject.VoidDate = Conversion.ToDbDate(System.DateTime.Now);
                    m_FundsObject.VoidReason = "Control Request Processed";
                    m_FundsObject.VoidReason_HTMLComments = "Control Request Processed";
                    m_FundsObject.TransNumber = 0;
                    m_FundsObject.Save();
                    //mkaran2 - Control Request Functionlity : End  - MITS 34569
                    //m_FundsObject.Save();
                    DbTransaction objTrans = this.m_objDmf.Context.TransStart();
                    objOffSettingFund.Save();
                    objNewFund.Save();

                 //   m_FundsObject.Save();
                    this.m_objDmf.Context.TransCommit();
                    iCtrlNumber = Utilities.GetNextUID(m_sConnectionString, "CONTROL_NUMBER", m_iClientId);
                    CreateControlRequestHistCarrier(m_FundsObject, objNewFund,objOffSettingFund, iSplitRowId);
                }
            }
            catch (RMAppException p_objException)
            {
                this.m_objDmf.Context.TransRollback();
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                this.m_objDmf.Context.TransRollback();
                throw new RMAppException(Globalization.GetString("ControlRequest.ProcessControlRequest.Error",m_iClientId), p_objException);//sonali
            }
            finally
            {
                objFundsSplitList = null;
            }
            return objXmlDoc;
        }

        #endregion



        
    }
}
