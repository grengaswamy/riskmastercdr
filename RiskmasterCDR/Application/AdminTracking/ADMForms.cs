using System;
using System.IO; 
using System.Xml; 
using Riskmaster.Db; 
using Riskmaster.Common;
using System.Collections; 
using Riskmaster.DataModel; 
using Riskmaster.ExceptionTypes;  

namespace Riskmaster.Application.AdminTracking
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   27th,Dec 2004
	///Purpose :   This is the main class that implements the functionality
	///				of Admin Tracking Web Forms. Returns XML string for each 
	///				of the public method called.
	/// </summary>
	public class ADMForms
	{
		#region Variable Declaration

		/// <summary>
		/// Connection String Variable
		/// </summary>
		private string m_sConnString="";

		/// <summary>
		/// Private variable for Dsn
		/// </summary>
		private string m_sDsn="";

		/// <summary>
		/// Private variable for User
		/// </summary>
		private string m_sUser="";

		/// <summary>
		/// Private variable for Password
		/// </summary>
		private string m_sPwd="";

		/// <summary>
		/// Parameters collection to hold key/value
		/// </summary>
		private Hashtable m_objParms = null;

		/// <summary>
		/// Private variable for Image Path
		/// </summary>
		private string m_sImagePath="";

		/// <summary>
		/// Private DataModelFactory Object
		/// </summary>
		private DataModelFactory m_objDmf=null;
        private int m_iClientId = 0; //mbahl3 JIRA [RMACLOUD-123]

		/// <summary>
		/// Enumerated type for Navigation Direction
		/// </summary>
		private enum NavDir:int
		{
			NavigationNone = 0,
			NavigationFirst = 1,
			NavigationPrev = 2,
			NavigationNext = 3,
			NavigationLast = 4,
			NavigationGoTo = 99
		}

		#endregion

		#region Constructors

		/// Name		: ADMForms
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor with Dsn,User,Password
		/// </summary>
		/// <param name="p_sDsn">Dsn Name</param>
		/// <param name="p_sUser">User Name</param>
		/// <param name="p_sPwd">Password</param>
        public ADMForms(string p_sDsn, string p_sUser, string p_sPwd, int p_iClientId) //mbahl3 JIRA [RMACLOUD-123]
		{
			m_sDsn = p_sDsn;
			m_sUser = p_sUser;
			m_sPwd = p_sPwd;
            m_iClientId = p_iClientId; //mbahl3 JIRA [RMACLOUD-123]
			m_objParms = new Hashtable();

		}

		#endregion

		#region Public Properties

		/// <summary>
		/// Public property ImagesPath
		/// </summary>
		public string ImagesPath
		{
			get
			{
				return m_sImagePath;
			}
		}

		/// <summary>
		/// Public property Params
		/// </summary>
		public Hashtable Params
		{
			get
			{
				return m_objParms;
			}
			set
			{
				m_objParms = value;
			}
		}

		#endregion

		#region Methods

		/// Name		: InitializeDataModel
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize DataModelFactory and Connection String
		/// </summary>
		private void InitializeDataModel()
		{
			if(m_objDmf==null)
			{
				//Initialize DataModel Factory if not already done
                m_objDmf = new DataModelFactory(m_sDsn, m_sUser, m_sPwd, m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
				m_sConnString = m_objDmf.Context.DbConn.ConnectionString;
                m_sImagePath = CheckImagePath(RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "Images"));
			}			
		}

		/// Name		: IsAttachmentEnabled
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for Attachment Flag
		/// </summary>
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>Boolean flag for Attachments</returns>
		private bool IsAttachmentEnabled(string p_sTableName)
		{
			DbReader objRdr= null;
			int iAttchEnabled=0;
			try
			{
				InitializeDataModel(); 

				objRdr = DbFactory.GetDbReader(m_sConnString,"SELECT ATTACHMENTS_FLAG FROM GLOSSARY " + 
					" WHERE SYSTEM_TABLE_NAME='" + p_sTableName.ToLower() + "'");
				if(objRdr.Read())
				{
					iAttchEnabled = objRdr.GetInt16("ATTACHMENTS_FLAG");
				}

				if (iAttchEnabled>0)
                    return true;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ADMForms.IsAttachmentEnabled.DataErr",m_iClientId) ,
					p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
			return false; 
		}

		/// Name		: RecordExists
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks if Record with a particular Id exists in a Table
		/// </summary>
		/// <param name="p_iID">Record ID</param>
		/// <param name="p_sKeyCol">Column Name</param>
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>Boolean Flag</returns>
		private bool RecordExists(int p_iID, string p_sKeyCol, string p_sTableName)
		{
            DbReader objRdr = null;
			try
			{
				if(p_iID <0 || p_sKeyCol=="" || p_sTableName=="")
					return false;

				objRdr = DbFactory.GetDbReader(m_sConnString,"SELECT " + p_sKeyCol + " FROM " + 
					p_sTableName + " WHERE " + p_sKeyCol + "=" + p_iID);
				
				if(objRdr.Read())
				{
					return true;
				}                
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ADMForms.RecordExists.DataErr",m_iClientId) , p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objRdr!=null)objRdr.Dispose();
			}
			return false;
		}

		/// Name		: GetADMTables
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Table List
		/// </summary>
		/// <returns>Xml String for Table List</returns>
		public XmlDocument GetADMTables()
		{
			string sTableId="";
			string sSysTableName ="";
			string sTableName ="";
			ADMTableList objADMLst = null;
			IEnumerator objEnum = null;
			XmlDocument objXmlDoc =  null;
			Array arrObj =null;
			XmlElement objElm = null;
			XmlElement objTblIdElm = null;
			XmlElement objSysNamedElm = null;
			XmlElement objTblNameElm = null;
			XmlElement objRoot = null;

			try
			{
				objXmlDoc = new XmlDocument(); 
				objRoot = objXmlDoc.CreateElement("AdmTracking");
				InitializeDataModel(); 
				objADMLst = (ADMTableList)m_objDmf.GetDataModelObject("ADMTableList",false);
				//Load Admin Table List
				objADMLst.LoadData();
				objEnum = objADMLst.TableUserNames().GetEnumerator();
				objXmlDoc.AppendChild(objRoot); 
				while(objEnum.MoveNext())
				{
					//For each of the Table in the List
					arrObj = (System.Array) objEnum.Current;
					sTableId = Conversion.ConvertObjToStr(arrObj.GetValue(2))  ;
					sSysTableName = Conversion.ConvertObjToStr(arrObj.GetValue(0));
					sTableName = Conversion.ConvertObjToStr(arrObj.GetValue(1));				
					objElm = objXmlDoc.CreateElement("AdmTrackingTable");
				
					//Add TableId node
					objTblIdElm = objXmlDoc.CreateElement("TableId");				
					objTblIdElm.InnerText = sTableId;
					objElm.AppendChild(objTblIdElm);				
				
					//Add SysTableName node
					objSysNamedElm = objXmlDoc.CreateElement("SysTableName");
					objSysNamedElm.InnerText = sSysTableName;
					objElm.AppendChild(objSysNamedElm);
				
					//Add TableName node
					objTblNameElm = objXmlDoc.CreateElement("TableName");		
					objTblNameElm.InnerText = sTableName;
					objElm.AppendChild(objTblNameElm);

					objXmlDoc.FirstChild.AppendChild(objElm);					
				}
			}
			catch(DataModelException p_objDmX)
			{
				throw p_objDmX;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.GetADMTables.Err", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objADMLst!=null)objADMLst.Dispose();
				objRoot = null;
				objEnum = null;
				arrObj =null;
				objElm = null;
				objTblIdElm = null;
				objSysNamedElm = null;
				objTblNameElm = null;
			}
			//return objXmlDoc.InnerXml ; 
			return objXmlDoc ; 
		}

		/// Name		: GetADMXml
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 02/10/2005		   Changes related to LocalCache and Utilities -Pankaj
		///************************************************************
		/// <summary>
		/// Gets the ADM Table XML
		/// </summary>
		/// <param name="p_objXmlDoc">XML Structure Input Document</param>
		/// <param name="p_objAdmTbl">Admin Table</param>
		/// <returns>Xml containing the admin Table</returns>
		private void GetADMXml(XmlDocument p_objXmlDoc, ADMTable p_objAdmTbl)
		{
			string sName = "";
			string sDate = "";
			string sKeyField = "";
			XmlNodeList objNodLst = null;
			SupplementalField objFld = null;
			Entity objEntity = null;			
			IEnumerator objEnum = null;
			XmlElement objEl = null;
			XmlElement objXmlSubElm = null;
			LocalCache objCache = null;

			try
			{
				foreach(SupplementalField objSup in p_objAdmTbl)
				{
					if (objSup.IsKeyValue) 
					{
						sKeyField = objSup.FieldName;
						break;
					}
				}

				InitializeDataModel(); 

				objNodLst= p_objXmlDoc.GetElementsByTagName("control");

				objCache = new LocalCache(m_sConnString,m_iClientId);

				foreach(XmlElement objElm in objNodLst)
				{
					sName = objElm.GetAttribute("name").ToUpper();
					if(sName.Trim()=="")
						continue;
					
					if(sName.ToUpper().Trim() == "SYS_ADMPID")
					{
						objElm.InnerText  = m_objParms["SYS_ADMPID"].ToString() ;
						objEl = (XmlElement)p_objXmlDoc.SelectSingleNode("form");
						objEl.SetAttribute("recordid",m_objParms["SYS_ADMPID"].ToString());
						objEl = null;
						continue;
					}
					

					objFld = p_objAdmTbl[sName]; 

					switch(objFld.FieldType)
					{
							//Following two cases have same functionality
						case SupplementalFieldTypes.SuppTypeCode:
                            int iCodeId = Conversion.ConvertObjToInt(objFld.Value, m_iClientId);
							objElm.SetAttribute("codeid",iCodeId.ToString());
							if (iCodeId>0)
							{						
								string sShortCode="";
								string sCodeDesc="";
								objCache.GetCodeInfo(iCodeId, ref sShortCode,ref sCodeDesc);
								objElm.InnerText  = sShortCode + " " + sCodeDesc;
							}
							break;
						case SupplementalFieldTypes.SuppTypeState:
                            int iStateId = Conversion.ConvertObjToInt(objFld.Value, m_iClientId);
							objElm.SetAttribute("codeid",iStateId.ToString());
							if (iStateId>0)
							{						
								string sAbbreviation="";
								string sStateName="";
								objCache.GetStateInfo(iStateId, ref sAbbreviation,ref sStateName);
								objElm.InnerText  = sAbbreviation + " " + sStateName;
							}
							break;
						case SupplementalFieldTypes.SuppTypeCurrency:
							objElm.InnerText  = Conversion.ConvertObjToStr(objFld.Value);
							break;
						case SupplementalFieldTypes.SuppTypeDate:
							sDate = Conversion.ConvertObjToStr(objFld.Value);
							if(sDate!="")
							{
								objElm.InnerText  =  Conversion.GetDBDateFormat(sDate,"MM/dd/yyyy");   								
							}
							else
								objElm.InnerText  ="";
							
							break;
						case SupplementalFieldTypes.SuppTypeEntity:
							if(objEntity == null)
							{								
								objEntity = (Entity)m_objDmf.GetDataModelObject("Entity",false); 
							}
							if (Utilities.IsNumeric(Conversion.ConvertObjToStr(objFld.Value)))
								objElm.InnerText   =  objEntity.GetLastFirstName(
                                    Conversion.ConvertObjToInt(objFld.Value, m_iClientId));
							else
								objElm.InnerText  ="";
							objElm.SetAttribute("codeid",Conversion.ConvertObjToStr(objFld.Value));
							break;
							
							//Following cases have the same functionality
						case SupplementalFieldTypes.SuppTypeEventLookup:
						case SupplementalFieldTypes.SuppTypeFreeText:
						case SupplementalFieldTypes.SuppTypeMemo:
						case SupplementalFieldTypes.SuppTypeNumber:
						case SupplementalFieldTypes.SuppTypePrimaryKey:
						case SupplementalFieldTypes.SuppTypeSecondaryKey:
						case SupplementalFieldTypes.SuppTypeText:					
							
							objElm.InnerText= Conversion.ConvertObjToStr( objFld.Value);
							break;
						case SupplementalFieldTypes.SuppTypeTime:
							
							sDate = Conversion.ConvertObjToStr(objFld.Value);
							string ssTime = Conversion.GetTimeAMPM(sDate);
							if(sDate!="")
							{
								objElm.InnerText  =  ssTime; //Conversion.GetDBTimeFormat(sDate,"HHMMSS");								
							}
							else
								objElm.InnerText =""; 
							break;
						case SupplementalFieldTypes.SuppTypeMultiEntity:
							if(objFld.Values!=null)
							{
								objEnum = objFld.Values.GetEnumerator();
								while(objEnum.MoveNext())
								{
									if(objEntity == null)
										objEntity = (Entity)m_objDmf.GetDataModelObject("Entity",false);
									int iEntityID = Conversion.ConvertObjToInt(
                                        ((DictionaryEntry)objEnum.Current).Key.ToString(), m_iClientId); 
									if(iEntityID>0) 
									{
										objXmlSubElm = p_objXmlDoc.CreateElement("Item");
										objEntity.MoveTo(iEntityID);
										if (objEntity.EntityId == iEntityID)
											objXmlSubElm.InnerText = objEntity.Abbreviation  + " - " + 
												objEntity.GetLastFirstName();   
										objXmlSubElm.SetAttribute("value", iEntityID.ToString());
										objElm.AppendChild(objXmlSubElm);
										objXmlSubElm = null;
									}
								}
								objEnum = null;
							}
							break;
						case SupplementalFieldTypes.SuppTypeMultiCode:
						case SupplementalFieldTypes.SuppTypeMultiState:
							objEnum = objFld.Values.GetEnumerator();
							while(objEnum.MoveNext())
							{
                                int iEntityID = Conversion.ConvertObjToInt(((DictionaryEntry)objEnum.Current).Key.ToString(), m_iClientId); 
								if(iEntityID>0) 
								{
									objXmlSubElm = p_objXmlDoc.CreateElement("Item");
									string sShortCode="";
									string sCodeDesc="";									
									if (objFld.FieldType==SupplementalFieldTypes.SuppTypeMultiState)
										objCache.GetStateInfo(iEntityID, ref sShortCode,ref sCodeDesc);
									else
										objCache.GetCodeInfo(iEntityID, ref sShortCode,ref sCodeDesc);
									if(iEntityID>0)
										objXmlSubElm.InnerText  = sShortCode + " " + sCodeDesc;
									objXmlSubElm.SetAttribute("value",iEntityID.ToString());
									objElm.AppendChild(objXmlSubElm);
									objXmlSubElm = null;
								}
							}
							objEnum = null;
							break;
						default:
							objElm.InnerText  = Conversion.ConvertObjToStr(objFld.Value);
							break;
					}			
				}
			}			
			catch(DataModelException p_objDmX)
			{
				throw p_objDmX;
			}
			catch(RMAppException p_objEX)
			{
				throw p_objEX;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.GetADMXml.Err", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objFld != null) objFld.Dispose();
				if( objEntity != null)objEntity.Dispose();
				if(objCache!=null) objCache.Dispose();
				objNodLst = null;
				objEnum = null;		
				objEl = null;
				objXmlSubElm = null;				
			}				
		}

		/// Name		: GetFormDOM
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the DOM structure for the Table
		/// </summary>
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>Xml String corresponding to the DOM</returns>
		public XmlDocument GetFormDOM(string p_sTableName)
		{
			XmlDocument objXmlDoc = null;
			try
			{
				objXmlDoc = new XmlDocument();
				//return GetFormXML(objXmlDoc,p_sTableName).InnerXml;
				return GetFormXML(objXmlDoc,p_sTableName);
			}
			catch(Exception p_objEx)
			{
				throw p_objEx;
			}
			finally
			{
				objXmlDoc = null;
			}
		}

		/// Name		: GetFormWithData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Data along with the Form Structure
		/// </summary>
		/// <param name="p_sInpXml">Input Xml</param>
		/// <param name="p_sTableName">Table Name</param>
		/// <param name="p_iNavDir">Navigation Direction</param>
		/// <param name="p_iCurrPos">Current Position</param>
		/// <returns>Xml String with Data and the Dom Structure</returns>
		public XmlDocument GetFormWithData(string p_sInpXml, string p_sTableName,int p_iNavDir, int p_iCurrPos)
		{
			string sKeyField="";
			XmlDocument objXmlDoc = null;
			ADMTable objAdm = null;
			try
			{
				objXmlDoc = new XmlDocument();
				objXmlDoc.LoadXml(p_sInpXml);
			}
			catch(XmlException p_objEx)
			{
                objXmlDoc = null;
				//If not able to load xml then its Ok. We shall get it from GetFormXML
			}			
			try
			{
				p_sTableName = p_sTableName.ToLower();
		
				if (objXmlDoc ==null)
				{
					objXmlDoc = new XmlDocument();
					objXmlDoc = GetFormXML(objXmlDoc,p_sTableName);
				}

				InitializeDataModel(); 

				objAdm = (ADMTable)m_objDmf.GetDataModelObject("ADMTable",false); 
				objAdm.TableName = p_sTableName; 

				foreach(SupplementalField objSup in objAdm)
				{
					if (objSup.IsKeyValue) 
					{
						sKeyField = objSup.FieldName;
						break;
					}
				}

				switch(p_iNavDir)
				{
					case  1: //Move First
						objAdm.MoveFirst(); 
						break;
					case 2: //Move Previous
						objAdm.MoveTo (p_iCurrPos);
						objAdm.MovePrevious();
						break;
					case 3: //Move Next
						objAdm.MoveTo (p_iCurrPos);
						objAdm.MoveNext();
						break;
					case 4: //Move Last
						objAdm.MoveLast(); 
						break;
					case 99: //Move To

						if (RecordExists(p_iCurrPos,sKeyField ,objAdm.TableName))
							objAdm.MoveTo(p_iCurrPos); 
						else							
							throw new RMAppException(
								Globalization.GetString("ADMForms.GetFormWithData.NoRecord",m_iClientId));  	//dvatsa-cloud						
							
						
						break;
					default:
						objAdm.MoveTo(p_iCurrPos); 
						break;
				}

				m_objParms.Clear();
				m_objParms.Add("SYS_ADMPID",objAdm[sKeyField].Value);
				
				GetADMXml(objXmlDoc,objAdm);  
			}
			catch(DataModelException p_objX)
			{
				throw p_objX;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.GetFormWithData.Err", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objAdm != null)objAdm.Dispose();
			}
			//return objXmlDoc.InnerXml;  
			return objXmlDoc;  
		}

		/// Name		: DeleteRecord
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes a Particular Record from the DataBase
		/// </summary>
		/// <param name="p_sTableName">Table Name</param>
		/// <param name="p_iRecId">Record Id to be deleted</param>
		public void DeleteRecord(string p_sTableName, int p_iRecId)
		{
			ADMTable objAdm = null;
			try
			{
				InitializeDataModel(); 

				objAdm = (ADMTable)m_objDmf.GetDataModelObject("ADMTable",false);			
				objAdm.TableName = p_sTableName;

				//Move and Delete
				objAdm.MoveTo(p_iRecId);
				objAdm.Delete();
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.DeleteRecord.Err", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if(objAdm != null)objAdm.Dispose() ;
			}
		}

		/// Name		: GetADMForm
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the ADM Xml
		/// </summary>		
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>Table Name Xml string</returns>
		public XmlDocument GetADMForm(string p_sTableName)
		{
			XmlDocument objXmlDoc=null;
			try
			{
				objXmlDoc = new XmlDocument(); 
				//return GetFormXML(objXmlDoc,p_sTableName).InnerXml; 
				return GetFormXML(objXmlDoc,p_sTableName); 
			}
			catch(Exception p_objEx)
			{
				throw p_objEx;
			}
			finally
			{
				objXmlDoc = null;
			}
		}

		/// Name		: GetFormXML
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 02/10/2005		   Changes related to LocalCache and Utilities -Pankaj
		///************************************************************
		/// <summary>
		/// Gets the ADM Form Xml String
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input String</param>
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>Xml Document</returns>
		private XmlDocument GetFormXML(XmlDocument p_objXmlDoc, string p_sTableName)
		{
            bool bFocused =false;
			string sDateTime = "";
			ADMTable objAdm = null;
			LocalCache objCache = null;			
			Entity objEntity = null;
			Entity objEnt = null;
			IEnumerator objEnum  = null;
			XmlElement objElm = null;
			XmlElement objXMLSubElem  = null;
			XmlElement objXmlDispCol = null;
			XmlElement objXmlElm  = null;
			XmlElement objGroupElm = null;
			XmlElement objDispCol  = null;
			XmlElement objControlElm = null;
			XmlElement objButton = null;
			XmlElement objSetValue1=null;
			XmlElement objSetValue2=null;
			XmlElement objJavascript=null;


			try
			{
				InitializeDataModel(); 

				objAdm = (ADMTable)m_objDmf.GetDataModelObject("ADMTable",false);
				objAdm.TableName = p_sTableName;

				objElm = p_objXmlDoc.CreateElement("form");
				objElm.SetAttribute("name",p_sTableName);
				objElm.SetAttribute("title",objAdm.TableUserName);
				//objElm.SetAttribute("postpage","adm.aspx?sid=" + (5000000 + objAdm.TableId * 20));


				if(File.Exists(m_sImagePath + p_sTableName + ".jpg"))
					objElm.SetAttribute("image", p_sTableName.ToLower()+ ".jpg");

				objElm.SetAttribute("goto_server","1");
				objElm.SetAttribute("recordid","0");
				p_objXmlDoc.AppendChild(objElm);

				//Create Toolbar
				objElm = p_objXmlDoc.CreateElement("toolbar");
				p_objXmlDoc.FirstChild.AppendChild(objElm);
				//New
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","new");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="New";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);
				
				//Save
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","save");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Save";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);

				//First
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","first");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="First";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);

				//Prev
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","prev");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Prev";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);
			
				//Next
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","next");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Next";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);
			
				//Last
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","last");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Last";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);

				//Delete
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","delete");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="Delete";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Delete";
				objButton.AppendChild(objSetValue2);
				objJavascript=objButton.OwnerDocument.CreateElement("javascript");
				objJavascript.SetAttribute("action","onclick");
				objJavascript.InnerText="return ifDelete()";
				objButton.AppendChild(objJavascript);
				objElm.AppendChild(objButton);
			
				//Attachments
				if(IsAttachmentEnabled(p_sTableName))
				{
					objButton = objElm.OwnerDocument.CreateElement("serverbutton");
					objButton.SetAttribute("type","attach");
					objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
					objSetValue1.SetAttribute("ref","/Instance/ui/action");
					objSetValue1.InnerText="PerformAction";
					objButton.AppendChild(objSetValue1);
					objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
					objSetValue2.SetAttribute("ref","/Instance/Document/test");
					objSetValue2.InnerText="attach";
					objButton.AppendChild(objSetValue2);
					objElm.AppendChild(objButton);
				}

				//Search
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","search");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Search";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);

				//LookUp
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","lookup");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Lookup";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);

				//Summary
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","summary");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Summary";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);

				//MailMerge
				objButton = objElm.OwnerDocument.CreateElement("serverbutton");
				objButton.SetAttribute("type","mailmerge");
				objSetValue1=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue1.SetAttribute("ref","/Instance/ui/action");
				objSetValue1.InnerText="PerformAction";
				objButton.AppendChild(objSetValue1);
				objSetValue2=objButton.OwnerDocument.CreateElement("setvalue");
				objSetValue2.SetAttribute("ref","/Instance/Document/test");
				objSetValue2.InnerText="Mailmerge";
				objButton.AppendChild(objSetValue2);
				objElm.AppendChild(objButton);


				//Create Group
				objElm = p_objXmlDoc.CreateElement("group");
				objElm.SetAttribute("name","admgroup");
				objElm.SetAttribute("title",objAdm.TableUserName); 
				p_objXmlDoc.FirstChild.AppendChild(objElm);

				objGroupElm = (XmlElement)p_objXmlDoc.GetElementsByTagName("group").Item(0);
				objGroupElm.SetAttribute("selected","1");

                objCache = new LocalCache(m_sConnString, m_iClientId);

				foreach(SupplementalField objSup in objAdm)
				{
					objDispCol = objGroupElm.OwnerDocument.CreateElement("displaycolumn");
					objControlElm = objDispCol.OwnerDocument.CreateElement("control");
					objControlElm.SetAttribute("name",objSup.FieldName.ToLower());				
					objControlElm.SetAttribute("ref","//control[@name='"+objSup.FieldName.ToLower()+"']");
					objControlElm.SetAttribute("title",objSup.Caption);
					if(objSup.Required)
						objControlElm.SetAttribute("required","yes");

					if (objSup.Visible && !bFocused)
					{
						objControlElm.SetAttribute("firstfield", "1");
						bFocused=true;
					}

					switch(objSup.FieldType)
					{
						case SupplementalFieldTypes.SuppTypePrimaryKey:
						case SupplementalFieldTypes.SuppTypeSecondaryKey:
							objControlElm.SetAttribute("type","id");
							break;
						case SupplementalFieldTypes.SuppTypeCode:
							objControlElm.SetAttribute("type","code");
							objControlElm.SetAttribute("codetable",objCache.GetTableName(objSup.CodeFileId));
							if (objSup.Value!=null)		
							{
								string sShortCode="";
								string sCodeDesc="";						
								objCache.GetCodeInfo((int)objSup.Value, ref sShortCode,ref sCodeDesc);
								objControlElm.InnerText = sShortCode + " " + sCodeDesc;
								objControlElm.SetAttribute("codeid",Conversion.ConvertObjToStr(objSup.Value));
							}
							break;
						case SupplementalFieldTypes.SuppTypeCurrency:
							objControlElm.SetAttribute("type","currency");
							if (objSup.Value!=null)
								objElm.InnerText = Conversion.ConvertObjToStr(objSup.Value);
							break;
						case SupplementalFieldTypes.SuppTypeDate:
							objControlElm.SetAttribute("type","date");
							sDateTime = Conversion.ConvertObjToStr(objSup.Value);
							if (sDateTime!="")
							{
								objControlElm.InnerText  =  Conversion.GetDBDateFormat(sDateTime,"MM/dd/yyyy");
							}
							break;
						case SupplementalFieldTypes.SuppTypeEntity:
							objControlElm.SetAttribute("type","eidlookup");
							objControlElm.SetAttribute("tableid",objCache.GetTableName(objSup.CodeFileId));
							if(objSup.Value!=null)
							{
								objEnt = (Entity)m_objDmf.GetDataModelObject("Entity",false);
								objControlElm.InnerText = objEnt.GetLastFirstName((int)objSup.Value);
								objControlElm.SetAttribute("codeid",Conversion.ConvertObjToStr( objSup.Value));
							}
							break;  
						case SupplementalFieldTypes.SuppTypeEventLookup:
							objControlElm.SetAttribute("type","text");
							if(objSup.Value!=null)
								objControlElm.InnerText = Conversion.ConvertObjToStr(objSup.Value);
							break;
						case SupplementalFieldTypes.SuppTypeFreeText:
							objControlElm.SetAttribute("type","textml");
							objControlElm.SetAttribute("cols","30");
							objControlElm.SetAttribute("rows","5");
							if(objSup.Value!=null)
								objControlElm.InnerText = Conversion.ConvertObjToStr(objSup.Value);
							break;
						case SupplementalFieldTypes.SuppTypeMemo:
							if(objSup.CodeFileId!=0)
							{
								objControlElm.SetAttribute("type","freecode"); 
								objControlElm.SetAttribute("codetable",objCache.GetTableName(objSup.CodeFileId)); 
							}
							else
							{
								objControlElm.SetAttribute("type","textml");
								objControlElm.SetAttribute("cols","30");
								objControlElm.SetAttribute("rows","5");
							}
							if(objSup.Value!=null)
								objControlElm.InnerText  = Conversion.ConvertObjToStr(objSup.Value);
							break;
						case SupplementalFieldTypes.SuppTypeNumber:
							objControlElm.SetAttribute("type","numeric");
							if(objSup.Value!=null)
								objControlElm.InnerText = Conversion.ConvertObjToStr(objSup.Value);
							break;
						case SupplementalFieldTypes.SuppTypeState:
							objControlElm.SetAttribute("type","code");
							objControlElm.SetAttribute("codetable","states");
							if(objSup.Value!=null)
							{
								objControlElm.SetAttribute("codeid",Conversion.ConvertObjToStr(objSup.Value));
								string sShortCode="";
								string sCodeDesc="";						
								objCache.GetStateInfo((int)objSup.Value, ref sShortCode,ref sCodeDesc);   
								objControlElm.InnerText = sShortCode + " " + sCodeDesc;  
							}
							break;
						case SupplementalFieldTypes.SuppTypeText:
							if (objSup.Format.Trim()=="###-##-####")
								objControlElm.SetAttribute("type","ssn");
							else if(objSup.Format.Trim() == "(###) ###-####   Ext. #####" ||
								objSup.Format.Trim() == "(###) ###-####")
								objControlElm.SetAttribute("type","phone");
							else if(objSup.Format.Trim() == "#####-####")
								objControlElm.SetAttribute("type", "zip");
							else
								objControlElm.SetAttribute("type", "text");

							if(objSup.Value!=null)
								objControlElm.InnerText  = Conversion.ConvertObjToStr(objSup.Value);
							break;
						case SupplementalFieldTypes.SuppTypeTime:
							objControlElm.SetAttribute("type","time");
							sDateTime = Conversion.ConvertObjToStr(objSup.Value);
							if(sDateTime!="")
							{
								objElm.InnerText  =  sDateTime; //Conversion.GetDBTimeFormat(sDateTime,"t");
							}
							break;
						case SupplementalFieldTypes.SuppTypeClaimLookup:
							objControlElm.SetAttribute("type", "claimnumberlookup");
							break;
						case SupplementalFieldTypes.SuppTypeVehicleLookup:
							objControlElm.SetAttribute("type", "vehiclenumberlookup");
							break;
						case SupplementalFieldTypes.SuppTypeMultiCode:
						case SupplementalFieldTypes.SuppTypeMultiState:
							objControlElm.SetAttribute("type", "codelist");
							objControlElm.SetAttribute("codeid", "");
							objControlElm.SetAttribute("codetable", objCache.GetTableName(objSup.CodeFileId));
							if(objSup.Values!=null)
							{
								objEnum = objSup.Values.GetEnumerator();
								while(objEnum.MoveNext())
								{
                                    int iValue = Conversion.ConvertObjToInt(objEnum.Current, m_iClientId);
									if (Utilities.IsNumeric(objEnum.Current.ToString()))
									{
										string sShortCode="";
										string sCodeDesc="";	
										objXMLSubElem = objControlElm.OwnerDocument.CreateElement("Item");
										if(objSup.FieldType == SupplementalFieldTypes.SuppTypeMultiState)	
										{
											if (objSup.FieldType==SupplementalFieldTypes.SuppTypeMultiState)
												objCache.GetStateInfo(iValue, ref sShortCode,ref sCodeDesc);
											else
												objCache.GetCodeInfo(iValue, ref sShortCode,ref sCodeDesc);
										}
										if (iValue>0)
											objXMLSubElem.InnerText = sShortCode + " " + sCodeDesc;
										objXMLSubElem.SetAttribute("value" , iValue.ToString());
										objControlElm.AppendChild(objXMLSubElem); 
									}
								}
								objEnum = null;
							}
							break;
						case SupplementalFieldTypes.SuppTypeMultiEntity:
							objControlElm.SetAttribute("type", "entitylist");
							objControlElm.SetAttribute("tableid", objSup.CodeFileId.ToString());
							if(objSup.Values!=null)
							{
								objEnum = objSup.Values.GetEnumerator();
								while(objEnum.MoveNext())
								{
									if(objEntity == null)
										objEntity = (Entity)m_objDmf.GetDataModelObject("Entity",false);
                                    int iValue = Conversion.ConvertObjToInt(objEnum.Current, m_iClientId);
									if (Utilities.IsNumeric(objEnum.Current.ToString()))
									{
										objXMLSubElem = objControlElm.OwnerDocument.CreateElement("option");
										objEntity.MoveTo(iValue);
										if(objEntity.EntityId == iValue)
											objXMLSubElem.InnerText = objEntity.Abbreviation  + " - " + 
												objEntity.GetLastFirstName();
										objXMLSubElem.SetAttribute("value", iValue.ToString());
										objControlElm.AppendChild(objXMLSubElem); 
									} 
								}
								objEnum = null;
							}
							break;
						}
					objDispCol.AppendChild(objControlElm);
					objGroupElm.AppendChild(objDispCol);
				}
				// -- Ratheen
				objDispCol = objGroupElm.OwnerDocument.CreateElement("displaycolumn");
				objControlElm = objDispCol.OwnerDocument.CreateElement("control");
				objControlElm.SetAttribute("name","sys_admpid");	
				objControlElm.SetAttribute("type","controlgroup");
				objControlElm.SetAttribute("tabindex","");
				
				objDispCol.AppendChild(objControlElm);
				objGroupElm.AppendChild(objDispCol);
				// -- Ratheen
				
				
				
				//' Used by browser internaly to get primary id name
				objXmlElm = p_objXmlDoc.CreateElement("internal");
				objXmlElm.SetAttribute("name", "sys_formidname");
				objXmlElm.SetAttribute("type", "hidden");
				objXmlElm.SetAttribute("value", "sys_admpid");
				p_objXmlDoc.GetElementsByTagName("form")[0].AppendChild(objXmlElm);
				

				//Used by browser internaly to get Parent Id in browser
				objXmlElm = p_objXmlDoc.CreateElement("internal");
				objXmlElm.SetAttribute("name", "sys_formpidname");
				objXmlElm.SetAttribute("type", "hidden");
				objXmlElm.SetAttribute("value", "");
				p_objXmlDoc.GetElementsByTagName("form")[0].AppendChild(objXmlElm);

				//Used by browser as Parent Form name
				objXmlElm = p_objXmlDoc.CreateElement("internal");
				objXmlElm.SetAttribute("name", "sys_formpform");
				objXmlElm.SetAttribute("type", "hidden");
				objXmlElm.SetAttribute("value", "");
				p_objXmlDoc.GetElementsByTagName("form")[0].AppendChild(objXmlElm);

//				//Used by browser when Deleting the records
//				objXmlElm = p_objXmlDoc.CreateElement("internal");
//				objXmlElm.SetAttribute("name", "sys_deletepage");
//				objXmlElm.SetAttribute("type", "hidden");
//				objXmlElm.SetAttribute("value", "adm.aspx");
//				p_objXmlDoc.GetElementsByTagName("form")[0].AppendChild(objXmlElm);

				//List of fields that are not required if record is new, does not apply to ADM
				objXmlElm = p_objXmlDoc.CreateElement("internal");
				objXmlElm.SetAttribute("name", "sys_notreqnew");
				objXmlElm.SetAttribute("type", "hidden");
				objXmlElm.SetAttribute("value", "");
				p_objXmlDoc.GetElementsByTagName("form")[0].AppendChild(objXmlElm);

//				//Security id
//				objXmlElm = p_objXmlDoc.CreateElement("internal");
//				objXmlElm.SetAttribute("name", "sys_sid");
//				objXmlElm.SetAttribute("type", "hidden");
//				int iVal = 5000000 + objAdm.TableId * 20;
//				objXmlElm.SetAttribute("value", iVal.ToString());
//				p_objXmlDoc.GetElementsByTagName("form")[0].AppendChild(objXmlElm);

			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.GetFormXML.Err", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if (objAdm!=null)objAdm.Dispose();				
				if (objEntity!=null)objEntity.Dispose();
				if (objEnt !=null)objEnt.Dispose();
				if (objCache!=null)objCache.Dispose();
				objElm = null;
				objEnum  = null;
				objButton = null;
				objGroupElm = null;
				objDispCol  = null;
				objControlElm  = null;				
				objXMLSubElem  = null;
				objXmlDispCol = null;
				objXmlElm  = null;
			}
			return p_objXmlDoc;
		}

		/// Name		: SaveData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves Data to the Database
		/// </summary>
		/// <param name="p_sInpXml">Input Xml String</param>
		/// <param name="p_sTableName">Table Name</param>
		/// <returns>Saved xml string</returns>
		public XmlDocument SaveData(string p_sInpXml, string p_sTableName) 
		{
			ADMTable objAdm = null;
			XmlElement objElm = null;
			XmlDocument objXmlDoc=null;
			XmlNodeList objNodLst = null;
			SupplementalField objFld = null;
			bool bValidationOK=false;
			bool bRequiredErr=false;
			string sKeyField="";
			XmlDocument objOutput=null;
			
			try
			{
				objXmlDoc = new XmlDocument();
				objXmlDoc.LoadXml(p_sInpXml);
			}
			catch(XmlException p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.SaveData.XmlErr", m_iClientId), p_objEx);//dvatsa-cloud
			}

			try
			{
                InitializeDataModel(); 

				objAdm = (ADMTable)m_objDmf.GetDataModelObject("ADMTable",false);
 
				objAdm.TableName = p_sTableName;

				foreach(SupplementalField objSup in objAdm)
				{
					if (objSup.IsKeyValue) 
					{
						sKeyField = objSup.FieldName;
						break;
					}					
				}

				objElm = (XmlElement)objXmlDoc.SelectSingleNode("//control[@name='" + sKeyField.ToLower() + "']");

				if(objElm == null)
				{
                    throw new RMAppException(Globalization.GetString("ADMForms.SaveData.KeyNotFound", m_iClientId));  //dvatsa-cloud					
				}

				bValidationOK = true;
				objNodLst = objXmlDoc.GetElementsByTagName("control");
				foreach(XmlElement objXmlEl in objNodLst)
				{
					string sKey = objXmlEl.GetAttribute("name");
					foreach(SupplementalField objSup in objAdm)
					{
						if (objSup.FieldName == sKey.ToUpper()) 
						{
							objFld = objSup;
							break;
						}					
					}

					if(objFld!=null)
					{
						if(objFld.Required)
						{
							bRequiredErr=false;                        
							if(objFld.FieldType == SupplementalFieldTypes.SuppTypeCode ||
								objFld.FieldType == SupplementalFieldTypes.SuppTypeState)
							{
                                if (Conversion.ConvertObjToInt(objXmlEl.GetAttribute("codeid"), m_iClientId) == 0)
									//Required Field Violation
									bRequiredErr = true;
							}
							else if (IsMulti(objFld.FieldType))
							{
								if(objXmlEl.ChildNodes.Count == 0)
									bRequiredErr = true;
							}
							else if(objXmlEl.InnerText.Trim()=="")
								bRequiredErr=true;
							if(bRequiredErr)
							{
								bValidationOK = false;
                                throw new RMAppException(Globalization.GetString("ADMForms.SaveData.ReqField", m_iClientId)); //dvatsa-cloud 								
							}
						}
						if( objFld.FieldType == SupplementalFieldTypes.SuppTypeCode ||
							objFld.FieldType == SupplementalFieldTypes.SuppTypeState ||
							objFld.FieldType == SupplementalFieldTypes.SuppTypeEntity)
                            objFld.Value = Conversion.ConvertObjToInt(objXmlEl.GetAttribute("codeid"), m_iClientId);
						else if(IsMulti(objFld.FieldType))
						{
							objFld.Values.ClearAll();
							foreach(XmlElement objE in objXmlEl.ChildNodes)
							{
								objFld.Values.Add( objE.GetAttribute("value"));
							}
						}//changes made to accomodate double values
						else if(objFld.FieldType == SupplementalFieldTypes.SuppTypeCurrency)
						{
							objFld.Value  = Conversion.ConvertStrToDouble(objXmlEl.InnerText);
						}
						else if(objFld.FieldType == SupplementalFieldTypes.SuppTypeNumber)
						{
							objFld.Value  = Conversion.ConvertStrToInteger(objXmlEl.InnerText);
						}
						else if(objFld.FieldType == SupplementalFieldTypes.SuppTypeTime )
						{
							objFld.Value  = Conversion.GetTimeHHMM00(objXmlEl.InnerText);
						}
						else //if(objFld.FieldType != SupplementalFieldTypes.SuppTypePrimaryKey)
							objFld.Value = objXmlEl.InnerText; //-- Ratheen -- Value must persist
					}
					objFld = null;
				}
				if (bValidationOK)
				{
					//Save if things are OK
					IPersistence objIPer = objAdm;
					objIPer.Save();
					m_objParms.Clear();
					m_objParms.Add("SYS_ADMPID",objAdm[sKeyField].Value);
					objOutput = GetFormWithData(objXmlDoc.OuterXml ,p_sTableName ,99,
                        Conversion.ConvertObjToInt(objAdm[sKeyField].Value, m_iClientId)); 
				}
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.SaveData.SaveErr", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				if (objAdm != null)objAdm.Dispose();
				if (objFld != null)objFld.Dispose();
				objElm = null;
				objXmlDoc=null;
				objNodLst = null;
			}
			return objOutput;
		}

		/// Name		: IsMulti
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks for Multi type of Supplemental fields
		/// </summary>
		/// <param name="p_objFldTyp">Supplemental Field Type</param>
		/// <returns>Boolean True or False</returns>
		private bool IsMulti(SupplementalFieldTypes p_objFldTyp)
		{
			switch(p_objFldTyp)
			{
				case SupplementalFieldTypes.SuppTypeMultiCode:
				case SupplementalFieldTypes.SuppTypeMultiState:
				case SupplementalFieldTypes.SuppTypeMultiEntity:
					return true;
				default:
					return false;
			}
		}

		/// Name		: CheckImagePath
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Appends the Image path with /
		/// </summary>
		/// <param name="p_sPath">Path string</param>
		/// <returns>Appended path</returns>
		private string CheckImagePath(string p_sPath)
		{
			if(p_sPath.Trim()!="")
				if ( p_sPath.Substring(p_sPath.Length -1,1)!=@"\")
					p_sPath = p_sPath +  @"\";
			return p_sPath;
		}

		/// Name		: CreateTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Overloaded function CreateTable
		/// </summary>
		/// <param name="p_sSysTableName">System Table Name</param>
		/// <param name="p_sUserTableName">User Table Name</param>
		public void CreateTable(string p_sSysTableName, string p_sUserTableName)
		{
			CreateTable(p_sSysTableName,p_sUserTableName, p_sSysTableName + "_ID" , 7, 0); 
		}

		/// Name		: CreateTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 02/10/2005		   Changes related to LocalCache and Utilities -Pankaj
		///************************************************************
		/// <summary>
		/// Function taken from Utilities bAddTable
		/// Creates table in the Database
		/// </summary>
		/// <param name="p_sSysTableName">System Table Name</param>
		/// <param name="p_sUserTableName">User Table Name</param>
		/// <param name="p_sFieldName">Field Name</param>
		/// <param name="p_iFieldType">Field Type</param>
		/// <param name="p_iFieldSize">Field Size</param>
		public void CreateTable(string p_sSysTableName, string p_sUserTableName, 
			string p_sFieldName, int p_iFieldType,int p_iFieldSize )
		{
			string sSQL="";
			int iTableId = 0;
			int iFieldId = 0;
			DbCommand objCmd = null;
			LocalCache objCache = null;
			try
			{
				//If invalid input raise error
				if ( p_sUserTableName.Trim()=="" || p_sSysTableName.Trim()=="")
                    throw new RMAppException(Globalization.GetString("ADMForms.CreateTable.InvalidInput", m_iClientId));//dvatsa-cloud

				InitializeDataModel(); 
				
				//Make the create table query
				switch(m_objDmf.Context.DbConn.DatabaseType )
				{
					case eDatabaseType.DBMS_IS_ACCESS:
						sSQL = "CREATE TABLE "  + p_sSysTableName.ToUpper().Trim() + " (" + 
							p_sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize) + ")";   
						break;
						//Following have the same sql
					case eDatabaseType.DBMS_IS_INFORMIX:
					case eDatabaseType.DBMS_IS_SQLSRVR:
					case eDatabaseType.DBMS_IS_SYBASE:
					case eDatabaseType.DBMS_IS_ORACLE:
					case eDatabaseType.DBMS_IS_DB2:
						sSQL = "CREATE TABLE " + p_sSysTableName.ToUpper().Trim()  +  " (" + 
							p_sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize) + 
							" NOT NULL)";
						break;
				}
 
				//If we're not already in a transaction, start one.
				if(m_objDmf.Context.DbTrans == null)
				{
					m_objDmf.Context.DbTrans = m_objDmf.Context.DbConn.BeginTransaction (); 
				}
				objCmd = m_objDmf.Context.DbConn.CreateCommand();
				objCmd.Transaction  =  m_objDmf.Context.DbTrans ; 
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();	

				CreatePrimaryKey(objCmd, p_sSysTableName, p_sFieldName);      

				//Initialize LocalCache
				objCache = new LocalCache(m_sConnString,m_iClientId); 

				//iTableId = Utilities.GetNextUID(m_objDmf.Context.DbConn,"GLOSSARY");
				
				//Changed By: Nikhil Garg.     Date: 18-Feb-2005
				//Reason: It was giving an error on passing the connection as there is a transaction attached to 
				//		  this connection and in Utilities it was not assigning this transaction to the command object 
				//		  Hence I passed connectin string and create a new connection	
                iTableId = Utilities.GetNextUID(m_objDmf.Context.DbConn.ConnectionString, "GLOSSARY", m_iClientId);

				//Glossary entry
				sSQL = "INSERT INTO GLOSSARY(TABLE_ID,SYSTEM_TABLE_NAME,GLOSSARY_TYPE_CODE,ATTACHMENTS_FLAG," + 
					"RELATED_TABLE_ID,REQD_REL_TABL_FLAG,DTTM_LAST_UPDATE,DELETED_FLAG) VALUES (" +
					iTableId + ",'" + p_sSysTableName.ToUpper().Trim()  + "'," +  
					objCache.GetCodeId("8", "GLOSSARY_TYPES") + 
					",0,0,0,'" + Conversion.ToDbDateTime(DateTime.Now)  + "',0)";
				
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				sSQL = "INSERT INTO GLOSSARY_TEXT(TABLE_ID,TABLE_NAME,LANGUAGE_CODE) VALUES (" + 
					iTableId + ",'" + p_sUserTableName.ToUpper().Trim() + "',1033)";
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				sSQL = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + Conversion.ToDbDateTime(DateTime.Now) + 
						"' WHERE SYSTEM_TABLE_NAME = '" + p_sSysTableName.ToUpper().Trim() + "'";					
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				//add primary key entry to SUPP_DICTIONARY table
				//iFieldId = Utilities.GetNextUID(m_objDmf.Context.DbConn, "SUPP_DICTIONARY");

				//Changed By: Nikhil Garg.     Date: 18-Feb-2005
				//Reason: It was giving an error on passing the connection as there is a transaction attached to 
				//		  this connection and in Utilities it was not assigning this transaction to the command object 
				//		  Hence I passed connectin string and create a new connection	
                iFieldId = Utilities.GetNextUID(m_objDmf.Context.DbConn.ConnectionString, "SUPP_DICTIONARY", m_iClientId);

				sSQL = "INSERT INTO SUPP_DICTIONARY(FIELD_ID,SUPP_TABLE_NAME,SYS_FIELD_NAME,DELETE_FLAG," +
						"FIELD_TYPE) VALUES ( " + iFieldId + ",'" + p_sSysTableName.ToUpper().Trim() +  
						"','" + p_sFieldName  + "',0,7)" ;
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				//commit
				m_objDmf.Context.DbTrans.Commit();
			}
			catch(RMAppException p_objEx)
			{
				if(m_objDmf.Context.DbTrans != null)
				{
					m_objDmf.Context.DbTrans.Rollback();
				}
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				if(m_objDmf.Context.DbTrans != null)
				{
					m_objDmf.Context.DbTrans.Rollback();
				}
                throw new RMAppException(Globalization.GetString("ADMForms.CreateTable.DataErr", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				objCmd = null;
				if (objCache!= null)objCache.Dispose();
			}
		}

		/// Name		: CreateField
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// 02/10/2005		   Changes related to LocalCache and Utilities -Pankaj
		///************************************************************
		/// <summary>
		/// Creates a Field in the Database Table
		/// </summary>
		/// <param name="p_sSysTableName">System Table Name</param>
		/// <param name="p_sSysFieldName">System Field Name</param>
		/// <param name="p_sUserFieldName">User Field Name</param>
		/// <param name="p_iFieldType">Field Type</param>
		/// <param name="p_iFieldSize">Field Size</param>
		/// <param name="p_iReqd">Required Flag</param>
		/// <param name="p_iIsPatterned">Patterned</param>
		/// <param name="p_iLkp">Lookup Flag</param>
		/// <param name="p_iCodFilId">Code Field Id</param>
		/// <param name="p_iIsReq">Required</param>
		/// <param name="p_iGrpAss">Asso Flag</param>
		public void CreateField(string p_sSysTableName, string p_sSysFieldName, string p_sUserFieldName, 
			int p_iFieldType, int p_iFieldSize, int p_iReqd, int p_iIsPatterned, int p_iLkp, 
			int p_iCodFilId, int p_iIsReq, int p_iGrpAss)
		{
			string sFieldName ="";
			string sSQL = "" ;
			int iFieldSize = 0;
			int iHelpId = 0;
			int iFieldId = 0;
			DbCommand objCmd = null ;
			DbReader objRdr = null;

			try
			{
                //Check for valid input
				if ( p_sSysTableName.Trim()=="" || p_sSysFieldName.Trim()=="" || p_sUserFieldName.Trim()=="")
                    throw new RMAppException(Globalization.GetString("ADMForms.CreateField.InvalidInput", m_iClientId));//dvatsa-cloud

				InitializeDataModel(); 

				//Append type to field name
				sFieldName = ExtendFieldName(p_sSysFieldName ,p_iFieldType);
				switch(p_iFieldType)
				{
					case 0:
						iFieldSize = p_iFieldSize;
						break;
					case 3:
						iFieldSize = 8;
						break;
					case 4:
						iFieldSize = 6;
						break;
					case 10:
					case 12:
						iFieldSize = 25;
						break;
					case 13:
						iFieldSize = 20;
						break;
					default:
						iFieldSize = 0;
						break;
				}

				//Generate the query for Alter table
				switch(m_objDmf.Context.DbConn.DatabaseType )
				{
					case eDatabaseType.DBMS_IS_ACCESS:
						sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD COLUMN " + 
							sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize);   
						break;
					case eDatabaseType.DBMS_IS_INFORMIX:
						sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD ( " + 
							sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize) + ")";   
						break;
					case eDatabaseType.DBMS_IS_SQLSRVR:
						sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " + 
							sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize) +  " NULL";   
						break;
					case eDatabaseType.DBMS_IS_SYBASE:
						sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD " + 
							sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize) +  " NULL";   
						break;
					case eDatabaseType.DBMS_IS_ORACLE:
						sSQL = "ALTER TABLE " + p_sSysTableName.ToUpper().Trim() + " ADD (" + 
							sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize) + " NULL)";   
						break;
					case eDatabaseType.DBMS_IS_DB2:
						sSQL = "CREATE TABLE " + p_sSysTableName.ToUpper().Trim()  +  " ADD " + 
							sFieldName.ToUpper().Trim() + " " + TypeMap(p_iFieldType,p_iFieldSize);
						break;
				}

				//If we're not already in a transaction, start one.
				if(m_objDmf.Context.DbTrans == null)
				{
					m_objDmf.Context.DbTrans = m_objDmf.Context.DbConn.BeginTransaction (); 
				}
				objCmd = m_objDmf.Context.DbConn.CreateCommand();
				objCmd.Transaction  =  m_objDmf.Context.DbTrans ; 
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				//add primary key entry to SUPP_DICTIONARY table
				//iFieldId = Utilities.GetNextUID(m_objDmf.Context.DbConn,"SUPP_DICTIONARY");

				//Changed By: Nikhil Garg.     Date: 18-Feb-2005
				//Reason: It was giving an error on passing the connection as there is a transaction attached to 
				//		  this connection and in Utilities it was not assigning this transaction to the command object 
				//		  Hence I passed connectin string and create a new connection	
                iFieldId = Utilities.GetNextUID(m_objDmf.Context.DbConn.ConnectionString, "SUPP_DICTIONARY", m_iClientId);

				//iHelpId = Utilities.GetNextUID(m_objDmf.Context.DbConn,"HELP_DEF");

				//Changed By: Nikhil Garg.     Date: 18-Feb-2005
				//Reason: It was giving an error on passing the connection as there is a transaction attached to 
				//		  this connection and in Utilities it was not assigning this transaction to the command object 
				//		  Hence I passed connectin string and create a new connection	
                iHelpId = Utilities.GetNextUID(m_objDmf.Context.DbConn.ConnectionString, "HELP_DEF", m_iClientId);

				sSQL = "INSERT INTO SUPP_DICTIONARY(FIELD_ID,SEQ_NUM,SUPP_TABLE_NAME,USER_PROMPT,SYS_FIELD_NAME," + 
					"DELETE_FLAG,FIELD_TYPE,FIELD_SIZE,REQUIRED_FLAG,IS_PATTERNED,LOOKUP_FLAG," + 
					"CODE_FILE_ID,HELP_CONTEXT_ID,GRP_ASSOC_FLAG) " + 
					"VALUES ( " + iFieldId + "," + iFieldId + ",'" + p_sSysTableName.ToUpper().Trim() + "','" + 
					p_sUserFieldName.ToUpper() +   "','" + sFieldName  + "',0," + p_iFieldType + "," + 
					p_iFieldSize + "," + p_iReqd + "," + p_iIsPatterned + "," + 
					p_iLkp + "," + p_iCodFilId + "," + iHelpId + "," + p_iGrpAss + ")" ;
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				//Update help details
				sSQL = "SELECT * FROM REQ_DEF WHERE LABEL_ID = " + iHelpId;
				objRdr = DbFactory.GetDbReader(m_sConnString,sSQL);
				if(!objRdr.Read())
				{
					sSQL = "INSERT INTO REQ_DEF (LABEL_ID,REQUIRED_FLAG) VALUES (" + iHelpId + "," + 
						p_iIsReq + ")";
					objCmd.CommandText = sSQL;
					objCmd.ExecuteNonQuery();
				}
				objRdr.Dispose();

				//Update glossary modification history
				sSQL = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + Conversion.ToDbDateTime(DateTime.Now) + 
					"' WHERE SYSTEM_TABLE_NAME = 'REQ_DEF'" ;					
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();
				
				//commit
				m_objDmf.Context.DbTrans.Commit();				
			}
			catch(RMAppException p_objEx)
			{
				if(m_objDmf.Context.DbTrans != null)
				{
					m_objDmf.Context.DbTrans.Rollback();
				}
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				if(m_objDmf.Context.DbTrans != null)
				{
					m_objDmf.Context.DbTrans.Rollback();
				}
                throw new RMAppException(Globalization.GetString("ADMForms.CreateField.DataErr", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				objCmd = null ;
				if(objRdr!=null)objRdr.Dispose(); 
			}
		}

		/// Name		: DeleteTable
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete the table
		/// </summary>
		/// <param name="p_sSysTableName">System Table Name</param>
		/// <param name="p_sUserTableName">User Table Name</param>
		public void DeleteTable(string p_sSysTableName, string p_sUserTableName)
		{
			string sSQL="";
			DbCommand objCmd = null ;
			try
			{
				//Check for valid Input
				if ( p_sSysTableName.Trim()=="" || p_sUserTableName.Trim()=="")
                    throw new RMAppException(Globalization.GetString("ADMForms.DeleteTable.InvalidInput", m_iClientId));//dvatsa-cloud

				//Initialized DataModelFactory if not already done
				InitializeDataModel();

				//Drop table
				sSQL = "DROP TABLE " + p_sSysTableName.ToUpper().Trim();  

				//If we're not already in a transaction, start one.
				if(m_objDmf.Context.DbTrans == null)
				{
					m_objDmf.Context.DbTrans = m_objDmf.Context.DbConn.BeginTransaction (); 
				}
				objCmd = m_objDmf.Context.DbConn.CreateCommand();
				objCmd.Transaction  =  m_objDmf.Context.DbTrans ; 
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				//delete glossary entry
				sSQL = "DELETE FROM GLOSSARY WHERE GLOSSARY.SYSTEM_TABLE_NAME = '" + 
					p_sSysTableName.ToUpper().Trim()   + "'";
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				sSQL = "DELETE FROM GLOSSARY_TEXT WHERE GLOSSARY_TEXT.TABLE_NAME = '" + 
					p_sUserTableName.ToUpper().Trim() + "'";
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				sSQL = "UPDATE GLOSSARY SET DTTM_LAST_UPDATE = '" + Conversion.ToDbDateTime(DateTime.Now) + 
					"' WHERE SYSTEM_TABLE_NAME = 'GLOSSARY'" ;					
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				//Delete supp_dictionary entry
				sSQL = "DELETE FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + 
					p_sSysTableName.ToUpper().Trim() + "'";
				objCmd.CommandText = sSQL;
				objCmd.ExecuteNonQuery();

				//Commit
				m_objDmf.Context.DbTrans.Commit();
			}
			catch(DataModelException p_objEx)
			{
				if(m_objDmf.Context.DbTrans != null)
				{
					m_objDmf.Context.DbTrans.Rollback();
				}
				throw p_objEx;
			}
			catch(RMAppException p_objEx)
			{
				if(m_objDmf.Context.DbTrans != null)
				{
					m_objDmf.Context.DbTrans.Rollback();
				}
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.DeleteTable.DataErr", m_iClientId), p_objEx);//dvatsa-cloud
			}
			finally
			{
				objCmd = null;
			}
		}

		/// Name		: ExtendFieldName
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds Field Type to the Field Name
		/// </summary>
		/// <param name="p_sFieldName">Field Name</param>
		/// <param name="p_iFieldType">Field Type</param>
		/// <returns>String containing concatenated field name</returns>
		private string ExtendFieldName(string p_sFieldName, int p_iFieldType)
		{
			int iLen = p_sFieldName.Length;
			if(iLen>11)
				iLen =11;
			switch(p_iFieldType)
			{
				case 0:
					return	p_sFieldName.Substring(0,iLen) + "_TEXT"; 
				case 1:
					return	p_sFieldName.Substring(0,iLen) + "_NUM"; 
				case 2:
					return	p_sFieldName.Substring(0,iLen) + "_AMT"; 
				case 3:
					return	p_sFieldName.Substring(0,iLen) + "_DATE"; 
				case 4:
					return	p_sFieldName.Substring(0,iLen) + "_TIME"; 
				case 5:
					return	p_sFieldName.Substring(0,iLen) + "_TXCD"; 
				case 6:
					return	p_sFieldName.Substring(0,iLen) + "_CODE"; 
				case 8:
					return	p_sFieldName.Substring(0,iLen) + "_EID"; 
				case 9:
					return	p_sFieldName.Substring(0,iLen) + "_STATE"; 
				case 10:
				case 12:
				case 13:
					return	p_sFieldName.Substring(0,iLen) + "_TEXT"; 
				case 11:
					return	p_sFieldName.Substring(0,iLen) + "_MEMO"; 
				default:
					return	p_sFieldName.Substring(0,iLen); 
			}
		}

		/// Name		: CreatePrimaryKey
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates Index on Primary Key
		/// </summary>
		/// <param name="p_objCmd">DB Command</param>
		/// <param name="p_sSysTableName">System Table Name</param>
		/// <param name="p_sKeyField">Key Field</param>
		private void CreatePrimaryKey(DbCommand p_objCmd, string p_sSysTableName, string p_sKeyField)
		{
			string sKeyName = "";
			string sSql = "";
			int iLen = 0;
			try
			{
				iLen = p_sSysTableName.Trim().Length;
				if (iLen >16)
					iLen =16;  

				//Primary Key Name
				sKeyName = p_sSysTableName.ToUpper().Trim().Substring(0,iLen) + "_PK";
				sSql = "CREATE UNIQUE INDEX " + sKeyName + " ON " + p_sSysTableName.ToUpper().Trim()   + 
				" (" +  p_sKeyField.ToUpper().Trim()   + ")";
				if(m_objDmf.Context.DbConn.DatabaseType == eDatabaseType.DBMS_IS_ACCESS)
					sSql =  sSql + " WITH PRIMARY";

				p_objCmd.CommandText = sSql;
				p_objCmd.ExecuteNonQuery();   
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ADMForms.CreatePrimaryKey.DataErr", m_iClientId), p_objEx);//dvatsa-cloud
			}
		}

		/// Name		: TypeMap
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Translates RM supplemental type to DB type string
		/// </summary>
		/// <param name="p_iSuppFieldType">Field Type</param>
		/// <param name="p_iSize">Field Size</param>
		/// <returns>Concatenated String</returns>
		private string TypeMap(int p_iSuppFieldType, int p_iSize)
		{
			string sType="";
			switch(p_iSuppFieldType)
			{
					//string, date, time, lookups
				case 0:
				case 3:
				case 4:
				case 10:
				case 12:
				case 13:
				switch(m_objDmf.Context.DbConn.DatabaseType)
				{
					case eDatabaseType.DBMS_IS_ACCESS:
						sType="TEXT";
						break;
					case eDatabaseType.DBMS_IS_INFORMIX:
					case eDatabaseType.DBMS_IS_SQLSRVR:
					case eDatabaseType.DBMS_IS_SYBASE:
					case eDatabaseType.DBMS_IS_DB2:
						sType="VARCHAR";
						break;
					case eDatabaseType.DBMS_IS_ORACLE:
						sType = "VARCHAR2";
						break;
				}
					//Add Size
					sType  = sType + "(" + p_iSize.ToString() + ")";  
				break;
					//number, currency (float)
				case 1:
				case 2:
				switch(m_objDmf.Context.DbConn.DatabaseType)
				{
					case eDatabaseType.DBMS_IS_ACCESS:
						sType="DOUBLE";
						break;
					case eDatabaseType.DBMS_IS_INFORMIX:
					case eDatabaseType.DBMS_IS_SQLSRVR:
					case eDatabaseType.DBMS_IS_SYBASE:
					case eDatabaseType.DBMS_IS_DB2:
						sType="FLOAT";
						break;
					case eDatabaseType.DBMS_IS_ORACLE:
						sType = "NUMBER";
						break;
				}
				break;
					//code, primary key, entity, state, MultiCode, MultiState, MultiEntity
				case 6:
				case 7:
				case 8:
				case 9:
				case 14:
				case 15:
				case 16:
				switch(m_objDmf.Context.DbConn.DatabaseType)
				{
					case eDatabaseType.DBMS_IS_ACCESS:
						sType="LONG";
						break;
					case eDatabaseType.DBMS_IS_SQLSRVR:
					case eDatabaseType.DBMS_IS_SYBASE:
						sType = "INTEGER";
						break;
					case eDatabaseType.DBMS_IS_INFORMIX:
					case eDatabaseType.DBMS_IS_DB2:
						sType="INTEGER";
						break;
					case eDatabaseType.DBMS_IS_ORACLE:
						sType = "NUMBER(10,0)";
						break;
				}
					break;
					//multi-valued and free text
				case 5:
				case 11:
				switch(m_objDmf.Context.DbConn.DatabaseType)
				{
					case eDatabaseType.DBMS_IS_ACCESS:
						sType="MEMO";
						break;
					case eDatabaseType.DBMS_IS_INFORMIX:
					case eDatabaseType.DBMS_IS_SQLSRVR:
					case eDatabaseType.DBMS_IS_SYBASE:
						sType="TEXT";
						break;
					case eDatabaseType.DBMS_IS_DB2:
						sType="CLOB(10M)";
						break;
					case eDatabaseType.DBMS_IS_ORACLE:
						sType = "VARCHAR2(2000)";
						break;
				}
					break;
			}
			return sType;
		}

		/// Name		: Dispose
		/// Author		: Pankaj Chowdhury
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Dispose method for cleaning
		/// </summary>
		public void Dispose()
		{
			if(m_objDmf!=null)m_objDmf.UnInitialize();
		}

		#endregion
	}
}