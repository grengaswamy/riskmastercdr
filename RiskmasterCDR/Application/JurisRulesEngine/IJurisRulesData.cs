﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Riskmaster.Application.JurisRulesEngine
{
  
    /// <summary>
    /// Interface for implementation of Jurisdictional Rules
    /// </summary>
    [Guid("BED9B9CC-C5BB-479d-81CF-C99FD1F9A03A")]
    public interface IJurisRulesData
    {
        /// <summary>
        /// Allows looking up whether or not a particular Jurisdictional 
        /// Rule is valid for execution
        /// </summary>
        /// <param name="strStateName">string indicating the state name</param>
        /// <param name="strStateYear">string indicating the corresponding year</param>
        /// <returns>boolean indicating whether or not the Jurisdictional Rule
        /// is valid to execute</returns>
        int FindJurisRulesRecord(string strStateName, string strStateYear);
    }
}
