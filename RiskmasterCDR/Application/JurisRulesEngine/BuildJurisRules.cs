﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Riskmaster.Application.JurisRulesEngine
{
    /// <summary>
    /// Builds the Jurisdictional Rules
    /// required for the current Jurisdictional States
    /// </summary>
    internal class BuildJurisRules
    {
        /// <summary>
        /// Gets and sets the Datatable for JurisRules Limits
        /// </summary>
        private DataTable JurisRulesLimits { get; set; }
        private const string STATE_COLUMN = "STATE";
        private const string YEAR_COLUMN = "YEAR";
        private const string INCOME_LIMIT_COLUMN = "INCOME_LIMIT";

        /// <summary>
        /// Default class constructor
        /// </summary>
        /// <remarks>creates and initializes the DataTable
        /// to be used to store the various Jurisdictional Rules</remarks>
        public BuildJurisRules()
        {
            DataTable dtJurisRules = new DataTable();
            dtJurisRules.TableName = "JurisRulesMappings";
            DataColumn[] dtPrimaryKeys = new DataColumn[2];
            DataColumn dtColumn;

            dtColumn = new DataColumn();
            dtColumn.ColumnName = "STATE";
            dtColumn.DataType = System.Type.GetType("System.String");

            dtJurisRules.Columns.Add(dtColumn);

            //Add the column to the Primary Key array
            dtPrimaryKeys[0] = dtColumn;

            dtColumn = new DataColumn();
            dtColumn.ColumnName = "YEAR";
            dtColumn.DataType = System.Type.GetType("System.Int32");


            dtJurisRules.Columns.Add(dtColumn);

            //Add the column to the Primary Key array
            dtPrimaryKeys[1] = dtColumn;


            dtColumn = new DataColumn();
            dtColumn.ColumnName = "INCOME_LIMIT";
            dtColumn.DataType = System.Type.GetType("System.Int32");


            dtJurisRules.Columns.Add(dtColumn);

            //Set the primary keys for the Data Table
            dtJurisRules.PrimaryKey = dtPrimaryKeys;

            //Set the property to the newly created DataTable
            this.JurisRulesLimits = dtJurisRules;
        }//class constructor

        /// <summary>
        /// Builds the Jurisdictional Rules for Rhode Island
        /// </summary>
        /// <returns></returns>
        public DataTable BuildRIRules()
        {
            DataTable dtJurisRIRules = JurisRulesLimits.Clone();
            const int RI_LIMIT = 4000;
            const string STATE = "RI";


            for (int intYearCounter = 1999; intYearCounter < DateTime.Now.Year; intYearCounter++)
            {
                //Add the required rows to the Data Table
                DataRow dtRow = dtJurisRIRules.NewRow();

                dtRow[STATE_COLUMN] = STATE;
                dtRow[YEAR_COLUMN] = intYearCounter;
                dtRow[INCOME_LIMIT_COLUMN] = RI_LIMIT;

                dtJurisRIRules.Rows.Add(dtRow);
            }//for

            return dtJurisRIRules;
        }//method: BuildRIRules()

        /// <summary>
        /// Builds the Jurisdictional Rules for Michigan
        /// </summary>
        /// <returns></returns>
        public DataTable BuildMIRules()
        {
            DataTable dtJurisMIRules = JurisRulesLimits.Clone();
            const int MI_LIMIT = 6000;
            const string STATE = "MI";


            for (int intYearCounter = 2005; intYearCounter < DateTime.Now.Year; intYearCounter++)
            {
                //Add the required rows to the Data Table
                DataRow dtRow = dtJurisMIRules.NewRow();

                dtRow[STATE_COLUMN] = STATE;
                dtRow[YEAR_COLUMN] = intYearCounter;
                dtRow[INCOME_LIMIT_COLUMN] = MI_LIMIT;

                dtJurisMIRules.Rows.Add(dtRow);
            }//for

            return dtJurisMIRules;
        }//method: BuildMIRules()

        /// <summary>
        /// Builds the Jurisdictional Rules for Alaska
        /// </summary>
        /// <returns></returns>
        public DataTable BuildAKRules()
        {
            DataTable dtJurisAKRules = JurisRulesLimits.Clone();
            const string STATE = "AK";

            for (int intYearCounter = 2003; intYearCounter < DateTime.Now.Year; intYearCounter++)
            {
                //Add the required rows to the Data Table
                DataRow dtRow = dtJurisAKRules.NewRow();

                dtRow[STATE_COLUMN] = STATE;
                dtRow[YEAR_COLUMN] = intYearCounter;


                switch (intYearCounter)
                {
                    case 2003:
                        dtRow[INCOME_LIMIT_COLUMN] = 2844;
                        break;
                    case 2004:
                        dtRow[INCOME_LIMIT_COLUMN] = 2874;
                        break;
                    case 2005:
                        dtRow[INCOME_LIMIT_COLUMN] = 2874;
                        break;
                }//switch

                dtJurisAKRules.Rows.Add(dtRow);
            }//for

            return dtJurisAKRules;
        }//method: BuildAKRules()

        /// <summary>
        /// Builds the Jurisdictional Rules for Iowa
        /// </summary>
        /// <returns></returns>
        public DataTable BuildIARules()
        {
            DataTable dtJurisIARules = JurisRulesLimits.Clone();
            const string STATE = "IA";

            for (int intYearCounter = 2000; intYearCounter < DateTime.Now.Year; intYearCounter++)
            {
                //Add the required rows to the Data Table
                DataRow dtRow = dtJurisIARules.NewRow();

                dtRow[STATE_COLUMN] = STATE;
                dtRow[YEAR_COLUMN] = intYearCounter;


                switch (intYearCounter)
                {
                    case 2000:
                        dtRow[INCOME_LIMIT_COLUMN] = 4200;
                        break;
                    case 2001:
                        dtRow[INCOME_LIMIT_COLUMN] = 4400;
                        break;
                    case 2002:
                        dtRow[INCOME_LIMIT_COLUMN] = 4400;
                        break;
                    case 2003:
                        dtRow[INCOME_LIMIT_COLUMN] = 4520;
                        break;
                    case 2004:
                        dtRow[INCOME_LIMIT_COLUMN] = 4600;
                        break;
                    case 2005:
                        dtRow[INCOME_LIMIT_COLUMN] = 4800;
                        break;
                }//switch

                dtJurisIARules.Rows.Add(dtRow);
            }//for

            return dtJurisIARules;
        }//method: BuildIARules()

        /// <summary>
        /// Builds the Jurisdictional Rules for Maine
        /// </summary>
        /// <returns></returns>
        public DataTable BuildMERules()
        {
            DataTable dtJurisMERules = JurisRulesLimits.Clone();
            const int ME_LIMIT = 8000;
            const string STATE = "ME";


            for (int intYearCounter = 2003; intYearCounter < DateTime.Now.Year; intYearCounter++)
            {
                //Add the required rows to the Data Table
                DataRow dtRow = dtJurisMERules.NewRow();

                dtRow[STATE_COLUMN] = STATE;
                dtRow[YEAR_COLUMN] = intYearCounter;
                dtRow[INCOME_LIMIT_COLUMN] = ME_LIMIT;

                dtJurisMERules.Rows.Add(dtRow);
            }//for

            return dtJurisMERules;
        }//method: BuildMERules()

        /// <summary>
        /// Builds all of the respective Jurisdictional Rules
        /// </summary>
        /// <returns></returns>
        public DataTable BuildJurisRulesData()
        {
            DataTable dtMergeData = JurisRulesLimits.Copy();

            dtMergeData.Merge(BuildAKRules(), true);

            dtMergeData.Merge(BuildIARules(), true);

            dtMergeData.Merge(BuildMERules(), true);

            dtMergeData.Merge(BuildMIRules(), true);

            dtMergeData.Merge(BuildRIRules(), true);

            return dtMergeData;
        }//method: BuildJurisRulesData()
    }
}
