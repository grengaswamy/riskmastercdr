using System;
using System.Data;
using System.Collections;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Application.SecurityManagement;

namespace Riskmaster.Application.EmailDocuments
{
	/// <summary>
	/// EmailDocuments Class for Sending Email with or without attachments
	/// </summary>
	public class EmailDocuments : IDisposable
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable for UserId 
		/// </summary>
		private int m_iUserId = 0 ;
		/// <summary>
		/// Private variable for UserGroupId
		/// </summary>
		private int m_iUserGroupId = 0 ;

        private int m_iClientId = 0;

		#endregion
		
		#region Constructors
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_iUserId">User Id</param>
		/// <param name="p_iUserGroupId">User Group Id</param>		
		public EmailDocuments( string p_sDsnName , string p_sUserName , string p_sPassword , int p_iUserId , int p_iUserGroupId, int p_iClientId )
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_iUserGroupId = p_iUserGroupId ;
            m_iClientId = p_iClientId;
			this.Initialize();
		}		
		/// <summary>
		/// Destructor
		/// </summary>
		~EmailDocuments()
		{
            Dispose();
		}
		#endregion 


		#region Private Functions
		
		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EmailDocuments.Initialize.ErrorInit", m_iClientId) , p_objEx );				
			}			
		}		
		
		/// <summary>
		/// Split a string based on some string pattern.
		private bool SplitForStringPattern(string p_sString,string p_sPattern,ref ArrayList p_arrlstSplit)
		{
			bool bReturn = false;

			int iIndexOf = 0;

			try
			{
				iIndexOf = p_sString.IndexOf(p_sPattern);

				if(iIndexOf == -1)
				{
					p_arrlstSplit.Add(p_sString);
					bReturn = true;
					return bReturn;
				}
				p_arrlstSplit.Add(p_sString.Substring(0,iIndexOf));

				string sAgain = p_sString.Substring(iIndexOf+p_sPattern.Length,p_sString.Length-(iIndexOf+p_sPattern.Length));
				SplitForStringPattern(sAgain,p_sPattern,ref p_arrlstSplit);
				bReturn = true;
			}
			catch(Exception p_objException)
			{
                throw new DataModelException(Globalization.GetString("DocumentManagementAdaptor.SplitForStringPattern.SplitForStringPatternError", m_iClientId), p_objException);
			}

			finally
			{
			}
			return bReturn;

		}

		/// <summary>
		/// Gets the e-mail id
		/// </summary>
		/// <param name="p_sConnectionString">Security Connection String</param>
		/// <param name="p_sLogInName">Log-in name of the user</param>
		/// <returns>E-mail id of the user</returns>
		private string GetEmailId (string p_sConnectionString,string p_sLogInName)
		{
			string sRetVal = "";
			string sSQL = "";
			DataSet objDS = null;

			try
			{
				sSQL = "SELECT USER_TABLE.EMAIL_ADDR FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME='" + p_sLogInName +"'";
				objDS=DbFactory.GetDataSet(p_sConnectionString, sSQL,m_iClientId);//psharma206
				sRetVal = Conversion.ConvertObjToStr (objDS.Tables[0].Rows[0]["EMAIL_ADDR"]);
			
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FundManager.GetEmailId.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objDS != null) objDS.Dispose();
			}
			return (sRetVal);
		}
		#endregion
		
		#region GetUsers
		/// <summary>
		/// Loads the users details that are allowed to login to a current datasource.
		/// </summary>
		public bool GetUsers(string p_sCurrentDSN, out string p_objXmlDocOut)
		{
			bool bReturnValue = false;

			XmlElement objRootElement = null;
			XmlNode objXmlNode = null;

			int i = 0, j = 0;
			Users objUsers = new Users(m_iClientId);
			DataSources objDataSources = new DataSources(m_iClientId);
            UserLogins objUserLogins = new UserLogins(m_iClientId);
			ArrayList arrlstDataSourcesCol = new ArrayList();
			ArrayList arrlstLoginCol = new ArrayList();
			ArrayList arrlstUserCol = new ArrayList();
			ArrayList p_arrlstPermToLogin = new ArrayList();
			
			XmlDocument objXmlDocOut = null;
			p_objXmlDocOut = string.Empty;
            
			try	
			{

				if(!objDataSources.Load(ref arrlstDataSourcesCol))
					return bReturnValue;

				if(!objUsers.Load(ref arrlstUserCol))
					return bReturnValue;
				
				if(!objUserLogins.Load(ref arrlstLoginCol))
					return bReturnValue;
		    
				if(arrlstLoginCol.Count < 1)
					return bReturnValue;
				
				for(i=0;i<arrlstLoginCol.Count;i++)
				{
					string sUserLoginName = ((UserLogins)arrlstLoginCol[i]).LoginName;
					int iUserLoginID = ((UserLogins)arrlstLoginCol[i]).UserId;
					string sDataSourceName = ((UserLogins)arrlstLoginCol[i]).objRiskmasterDatabase.DataSourceName;
					string sCurrentDataSourceName = p_sCurrentDSN;
					p_arrlstPermToLogin.Add(sDataSourceName + "^~^~^" + iUserLoginID + "^~^~^" + sUserLoginName + "^~^~^" + sCurrentDataSourceName);
				}

				for(i=0;i<p_arrlstPermToLogin.Count;i++)
				{
					ArrayList arrlstTemp = new ArrayList(); 
					SplitForStringPattern(p_arrlstPermToLogin[i].ToString(),"^~^~^",ref arrlstTemp);
					for(j=0;j<arrlstUserCol.Count;j++)
					{
						int iUserID = ((Users)arrlstUserCol[j]).UserId;
						string sFirstName = ((Users)arrlstUserCol[j]).FirstName;
						string sLastName = ((Users)arrlstUserCol[j]).LastName;
						string sEmail = ((Users)arrlstUserCol[j]).Email;
						if(arrlstTemp[1].ToString() == Convert.ToString(iUserID))
							p_arrlstPermToLogin[i] = p_arrlstPermToLogin[i] + "^~^~^" + sFirstName + "^~^~^" + sLastName + "^~^~^" + sEmail;
					}
				}				
			
				objXmlDocOut = new XmlDocument();

				objXmlNode = objXmlDocOut.CreateElement("data");  
				objXmlDocOut.AppendChild(objXmlNode);
			
				for(i=0;i<p_arrlstPermToLogin.Count;i++)
				{					 
					ArrayList arrlstTemp = new ArrayList();
					SplitForStringPattern(p_arrlstPermToLogin[i].ToString(),"^~^~^",ref arrlstTemp);
					if (arrlstTemp[0].ToString() == arrlstTemp[3].ToString())
					{
						objRootElement = objXmlDocOut.CreateElement("user");
						objRootElement.SetAttribute("id",arrlstTemp[1].ToString());
						objRootElement.SetAttribute("username",arrlstTemp[2].ToString());
						objRootElement.SetAttribute("firstname",arrlstTemp[4].ToString());
						objRootElement.SetAttribute("lastname",arrlstTemp[5].ToString());						
						objRootElement.SetAttribute("email",arrlstTemp[6].ToString());
						objXmlNode.AppendChild(objRootElement);
					}
				}		

				bReturnValue = true;

			}
			catch(InitializationException p_objInitializationException)
			{
				throw p_objInitializationException;
			}
			catch(RMAppException p_objRMAppException)
			{
				throw p_objRMAppException;
			}
			catch(XmlException p_objXMLException)
			{
                throw new RMAppException(Globalization.GetString("DocumentManager.GetUsers.XMLException", m_iClientId), p_objXMLException);  
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("DocumentManager.GetUsers.Exception", m_iClientId), p_objException);  
			}
			finally
			{
				objRootElement = null;
			}		

			p_objXmlDocOut = objXmlDocOut.OuterXml;
			return bReturnValue;
		}
		
		#endregion

		#region Sending Documents via EMail
		/// <summary>
		/// Sending the email to multiple users
		/// </summary>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sConnStringName">Security Connection String</param>
		/// <param name="p_sEmailIds">EMail Ids of all the recievers</param>
		/// <param name="p_sSubject">Subject of the mail</param>
		/// <param name="p_sMessage">Message of the mail</param>
				
		/// <returns>True if successful else false.</returns>
		public bool SendEmail (string p_sUserName, string p_sConnStringName, string p_sEmailIdsTo, string p_sEmailIdsCc ,
			string p_sSubject , string p_sMessage)
		{	
			bool bRetVal = false;
			try 
			{
				bRetVal = SendEmail(p_sUserName , p_sConnStringName , p_sEmailIdsTo , p_sEmailIdsCc , p_sSubject , p_sMessage , "");
			}
            catch (RMAppException p_objException)
            {
                bRetVal = false;
                // throw new RMAppException(Globalization.GetString("EmailDocuments.EmailDocuments.SendError"), p_objException); Commented by Charanpreet for MITS 13310
                throw p_objException; // Added by Charanpreet for MITS 13310
            }
			catch (Exception p_objException)
			{
                bRetVal = false;
                throw new RMAppException(Globalization.GetString("EmailDocuments.EmailDocuments.SendError", m_iClientId), p_objException);
			}
			return (bRetVal);
		}	
		
		/// <summary>
		/// Sending the email to multiple users
		/// </summary>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sConnStringName">Security Connection String</param>
		/// <param name="p_sEmailIds">EMail Ids of all the recievers</param>
		/// <param name="p_sSubject">Subject of the mail</param>
		/// <param name="p_sMessage">Message of the mail</param>
		/// <param name="p_sAttachFileName">Attachment of the mail</param>
		
		/// <returns>True if successful else false.</returns>
		public bool SendEmail (string p_sUserName, string p_sConnStringName, string p_sEmailIdsTo, string p_sEmailIdsCc , 
			string p_sSubject , string p_sMessage , string p_sAttachFileName)
		{			
			string sEmail = "";
			bool bRetVal = false;
			Mailer objMail = null;

			try
			{
				if (p_sUserName != "")
				{
					sEmail = GetEmailId(p_sConnStringName,p_sUserName);
					if(sEmail == "")
					{
                        //throw new InvalidValueException(Globalization.GetString("EmailDocuments.Email.InvalidSenderID")); Ammented By Charanpreet for MITS 13310
                        throw new InvalidValueException(Globalization.GetString("EmailDocuments.Email.BlankSenderID", m_iClientId));  //Added By Charanpreet for MITS 13310
                    }
				}
				else
				{
                    throw new InvalidValueException(Globalization.GetString("EmailDocuments.Email.InvalidUserName", m_iClientId));
				}

				if(p_sEmailIdsTo.Length==0)
				{
                    throw new InvalidValueException(Globalization.GetString("EmailDocuments.Email.InvalidIDs", m_iClientId));
				}

				if (sEmail != "")
				{
					objMail = new Mailer(m_iClientId);//sonali
					objMail.To = p_sEmailIdsTo;
					if(p_sEmailIdsCc!="")
						objMail.Cc = p_sEmailIdsCc;
					objMail.From = sEmail;
					objMail.Subject = p_sSubject;
					objMail.Body = p_sMessage;
					if(p_sAttachFileName!="")
						objMail.AddAttachment(p_sAttachFileName);
							
					objMail.SendMail();
					bRetVal = true;
				}
			}
            catch (RMAppException p_objException)
            {
                bRetVal = false;
                // throw new RMAppException(Globalization.GetString("EmailDocuments.EmailDocuments.SendError"), p_objException); Commented by Charanpreet for MITS 13310
                throw p_objException; // Added by Charanpreet for MITS 13310
            }
			catch (Exception p_objException)
			{
                bRetVal = false;
                throw new RMAppException(Globalization.GetString("EmailDocuments.EmailDocuments.SendError", m_iClientId), p_objException);
			}
			finally 
			{
                if( objMail != null )
                    objMail.Dispose();
				objMail = null;
			}
			return (bRetVal);
		}	


        /// <summary>
        /// Mridul 06/25/09 MITS 16745- Chubb Mail merge (acknowledgement letter)
        /// From email address can be passed from calling application
        /// </summary>
        /// <param name="p_sEmailIdsTo"></param>
        /// <param name="p_sSubject"></param>
        /// <param name="p_sMessage"></param>
        /// <param name="p_sAttachFileName"></param>
        /// <param name="p_sEmailIDFrom"></param>
        /// <returns></returns>
        public bool SendEmail(string p_sEmailIdsTo, string p_sSubject, string p_sMessage, string p_sAttachFileName, string p_sEmailIDFrom)
        {
            bool bRetVal = false;
            Mailer objMail = null;

            try
            {
                if (p_sEmailIDFrom == "")
                    throw new InvalidValueException(Globalization.GetString("EmailDocuments.Email.BlankSenderID", m_iClientId));

                if (p_sEmailIdsTo.Length == 0)
                {
                    throw new InvalidValueException(Globalization.GetString("EmailDocuments.Email.InvalidIDs", m_iClientId));
                }

                if (p_sEmailIDFrom != "")
                {
                    objMail = new Mailer(m_iClientId);
                    objMail.To = p_sEmailIdsTo;
                    objMail.From = p_sEmailIDFrom;
                    objMail.Subject = p_sSubject;
                    objMail.Body = p_sMessage;
                    if (p_sAttachFileName != "")
                        objMail.AddAttachment(p_sAttachFileName);

                    objMail.SendMail();
                    bRetVal = true;
                }
            }
            catch (RMAppException p_objException)
            {
                bRetVal = false;
                throw p_objException; // Added by Charanpreet for MITS 13310
            }
            catch (Exception p_objException)
            {
                bRetVal = false;
                throw new RMAppException(Globalization.GetString("EmailDocuments.EmailDocuments.SendError", m_iClientId), p_objException);
            }
            finally
            {
                if (objMail != null)
                    objMail.Dispose();
                objMail = null;
            }
            return (bRetVal);
        }
		#endregion

		/// <summary>
		/// Un Initialize the data model factory object. 
		/// </summary>
		public void Dispose()
		{
			try
			{
                if (m_objDataModelFactory != null)
                    m_objDataModelFactory.Dispose();			
			}
			catch
			{
				// Avoid raising any exception here.				
			}
		}

		
	}
}
