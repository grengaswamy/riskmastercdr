﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA   | Programmer | Description                                            *
 **********************************************************************************************
 * 11/20/2014 | RMA-1346   | nshah28   | changes for ActivityLog
 **********************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Security;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.Data;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;

namespace Riskmaster.Application.ClaimActLog
{
    ///************************************************************** 
    ///* $File				: ClaimActLog.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 29-Nov-2011
    ///* $Author			: Nitin Sachdeva
    ///***************************************************************	

    public class ClaimActLog 
    {
       
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_ConnectionString = string.Empty;
        /// <summary>
        /// Constant for verify the security in SMS
        /// </summary>
        private const int RMB_ClaimActLog_GC = 189;
        private const int RMB_ClaimActLog_WC = 3039;
        private const int RMB_ClaimActLog_VA = 6039;
        private const int RMB_ClaimActLog_DI = 60039;
        private const int RMB_ClaimActLog_PC = 40039;
        internal static UserLogin m_objUserLogin = null;
        private int m_iClientId = 0; //sharishkumar Jira 825
         #region Constructor
        // Default Constructor
        public ClaimActLog()
        {
        }

        public ClaimActLog(string connectionString, int p_iClientId)//sharishkumar Jira 825
        {
            m_ConnectionString = connectionString;
            m_iClientId = p_iClientId;//sharishkumar Jira 825
        }
        public ClaimActLog(UserLogin p_objUserLogin, string connectionString, int p_iClientId)//sharishkumar Jira 825
        {
            //DbConnection objConn;
            m_ConnectionString = connectionString;
            //m_UserId = userId;
          
            //objConn = DbFactory.GetDbConnection(m_ConnectionString);
            //objConn.Open();
            //m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            //objConn.Close();
            m_objUserLogin = p_objUserLogin;
            m_iClientId = p_iClientId;//sharishkumar Jira 825
        }
        #endregion

        /// <summary>
        /// Collection of table column and related grid fields names
        /// </summary>
        private System.Collections.Hashtable m_arrFields = new System.Collections.Hashtable() 
             {
                {"LogId", "LOG_ID"},
                {"Table","TABLE_ID"},
                {"OpType","OPERATION_CODE"},
                {"ActType","ACTIVITY_TYPE_CODE"},
                {"LogText","LOG_TEXT"}
            };

        /// <summary>
        /// To list all the claim activity on claim UI 
        /// </summary>
        /// <param name="p_objXmlDocument">xml document containing form name and claim id</param>
        /// <returns>xml document conating list of claim activity</returns>
        public XmlDocument GetClaimActivityLog(XmlDocument p_objXmlDocument)
        {
            XmlDocument oXmlDoc = null;
            DataSet objDataSet = null;
            string sSql = string.Empty;
            bool bInSuccess = false;
            XmlElement objElem = null;
            LocalCache objCache = null;
            int iClaimId = 0;
            string sFormName = string.Empty;
            string sLangCode = string.Empty;
            try
            {
                iClaimId = Conversion.CastToType<int>(((XmlElement)p_objXmlDocument.SelectSingleNode("//ClaimId")).InnerXml, out bInSuccess);
                sFormName = ((XmlElement)p_objXmlDocument.SelectSingleNode("//FormName")).InnerXml;
                GetSecurityPermissions(sFormName);
                objCache = new LocalCache(m_ConnectionString,m_iClientId);
                sSql = "SELECT ACTIVITY_TYPE_CODE, ADDED_BY_USER, DTTM_RCD_ADDED, LOG_TEXT FROM CLAIM_ACTIVITY_LOG WHERE CLAIM_ID=" + iClaimId + " ORDER BY DTTM_RCD_ADDED DESC";
                objDataSet = DbFactory.GetDataSet(m_ConnectionString, sSql, m_iClientId);//sharishkumar Jira 825
                oXmlDoc = new XmlDocument();
                objElem = oXmlDoc.CreateElement("ClaimActLogList");
                oXmlDoc.AppendChild(objElem);
                objElem = null;
                sLangCode = ((XmlElement)p_objXmlDocument.SelectSingleNode("//LangCode")).InnerXml;
                foreach (DataRow objRow in objDataSet.Tables[0].Rows)
                {
                    objElem = oXmlDoc.CreateElement("ClaimActivity");
                    //objElem.SetAttribute("dttm_rcd_added", Conversion.ToDate(objRow["DTTM_RCD_ADDED"].ToString()).ToString());
                    objElem.SetAttribute("dttm_rcd_added", Conversion.GetUIDate(objRow["DTTM_RCD_ADDED"].ToString(), sLangCode,m_iClientId) + " " + Conversion.GetUITime(objRow["DTTM_RCD_ADDED"].ToString(), sLangCode, m_iClientId));//sharishkumar Jira 825
                    objElem.SetAttribute("added_by_user", objRow["ADDED_BY_USER"].ToString());
                    objElem.SetAttribute("type", objCache.GetCodeDesc(Conversion.CastToType<int>(objRow["ACTIVITY_TYPE_CODE"].ToString(), out bInSuccess)));
                    objElem.SetAttribute("log_text", objRow["LOG_TEXT"].ToString());
                    oXmlDoc.FirstChild.AppendChild(objElem);
                }
                objDataSet.Dispose(); 
                return oXmlDoc;
            }
            catch (RecordNotFoundException p_objException)
            {
                throw p_objException;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimActLog.GetClaimActivityLog.GeneralError", m_iClientId), p_objException);//sharishkumar Jira 825
            }
            finally
            {
                if (objDataSet != null) objDataSet.Dispose();
                if (objCache != null) objCache.Dispose();
                oXmlDoc = null;
                objElem = null;
            }
            
        }

        /// <summary>
        /// Get all the config from the database
        /// </summary>
        /// <param name="p_objXmlDocument">xml document</param>
        /// <returns>xml document containing all the config</returns>
        public XmlDocument GetConfig(XmlDocument p_objXmlDocument)
        {
            string sSql = string.Empty;
            DataSet objDataSet = null;
            XmlElement objElm = null;
            XmlElement objText = null;
            XmlElement objListRow = null;
            XmlNode objNode = null;
            XmlNodeList objNodeList = null;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sShortDesc = string.Empty;
            bool bInSuccess = false;
            try
            {
                objCache = new LocalCache(m_ConnectionString,m_iClientId);
                sSql = "SELECT LOG_ID, TABLE_ID, OPERATION_CODE, ACTIVITY_TYPE_CODE, LOG_TEXT FROM CLAIM_ACT_LOG_DEF ORDER BY LOG_ID ASC";
                objDataSet = DbFactory.GetDataSet(m_ConnectionString, sSql, m_iClientId);//sharishkumar Jira 825
                objNode = p_objXmlDocument.SelectSingleNode("//ClaimActSetupLists");
                objNodeList = objNode.ChildNodes.Item(0).ChildNodes;
                foreach (DataRow objRow in objDataSet.Tables[0].Rows)
                {
                    objListRow = p_objXmlDocument.CreateElement("row");
                    for(int i = 0; i< objNodeList.Count;i++)  
					{
						objElm = (XmlElement)objNodeList[i]; 
						objText = p_objXmlDocument.CreateElement(objNodeList[i].Name);
                        if(objNodeList[i].Attributes["type"] != null)
						{
							if (objNodeList[i].Attributes["type"].Value.ToLower()=="code")
							{
								objCache.GetCodeInfo(Conversion.CastToType<int>(objRow[m_arrFields[objElm.Name].ToString()].ToString(), out bInSuccess),ref sShortCode,ref sShortDesc);
                                objText.InnerText= sShortCode + " " + sShortDesc;
                                objText.SetAttribute("codeid", objRow[m_arrFields[objElm.Name].ToString()].ToString());
							}
							else if (objNodeList[i].Attributes["type"].Value.ToLower() == "table")
                            {
                                objText.InnerText = objCache.GetTableName(Conversion.CastToType<int>(objRow[m_arrFields[objElm.Name].ToString()].ToString(), out bInSuccess));
                            }
                            else
                                objText.InnerText = objRow[m_arrFields[objElm.Name].ToString()].ToString();
						}
                        else
                            objText.InnerText = objRow[m_arrFields[objElm.Name].ToString()].ToString();
						objListRow.AppendChild(objText);
					}
                    objNode.AppendChild((XmlNode)objListRow);
				}
				objDataSet.Dispose();
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimActLog.GetConfig.Err", m_iClientId), p_objEx);//sharishkumar Jira 825
            }
            finally
            {
                if (objDataSet != null)
                {
                    objDataSet.Dispose();
                    objDataSet = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objListRow = null;
                objText = null;
                objNode = null;
                objNodeList = null;
                objElm = null;
            }
        }
        /// <summary>
        /// Add new/edit existing config
        /// </summary>
        /// <param name="p_objXmlDocument">xml document containing the Log Id</param>
        /// <returns>xml document containing the details of the config</returns>
        public XmlDocument GetConfigDetails(XmlDocument p_objXmlDocument)
        {
            string sSql = string.Empty;
            XmlElement objElm = null;
            XmlNode objNode = null;
            LocalCache objCache = null;
            int iLogId = 0;
            DbReader objRdr=null;
            XmlElement objNewElm = null;
            bool bInSuccess = false;
            //rsharma220 MITS 33205: Added Document in Table Names
           // string sTableNames = "'CLAIM','CLAIMANT','FUNDS','FUNDS_TRANS_SPLIT','RESERVE_CURRENT','RESERVE_HISTORY', 'CLAIM_ADJUSTER','CLAIM_STATUS_HIST','CLAIM_X_LITIGATION','SALVAGE','POLICY_X_CVG_ENH','FUNDS_AUTO', 'CLAIM_X_ARB', 'CLAIM_X_PROPERTYLOSS', 'CLAIM_X_SUBRO', 'DEFENDANT', 'PERSON_INVOLVED','CLAIM_X_POLICY', 'UNIT_X_CLAIM', 'POLICY_X_CVG_TYPE', 'POLICY_X_UNIT', 'DOCUMENT'";
            //JIRA RMA-1346 nshah28 start
            string sTableNames = "'CLAIM','CLAIMANT','FUNDS','FUNDS_TRANS_SPLIT','RESERVE_CURRENT','RESERVE_HISTORY', 'CLAIM_ADJUSTER','CLAIM_STATUS_HIST','CLAIM_X_LITIGATION','SALVAGE','POLICY_X_CVG_ENH','FUNDS_AUTO', 'CLAIM_X_ARB', 'CLAIM_X_PROPERTYLOSS', 'CLAIM_X_SUBRO', 'DEFENDANT', 'PERSON_INVOLVED','CLAIM_X_POLICY', 'UNIT_X_CLAIM', 'POLICY_X_CVG_TYPE', 'POLICY_X_UNIT', 'DOCUMENT', 'PMT_APPROVAL_HIST'";
            //JIRA RMA-1346 nshah28 end
            try
            {
                objCache = new LocalCache(m_ConnectionString,m_iClientId);
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='LogId']");
                if (objNode != null)
                    iLogId = Conversion.CastToType<int>(objNode.InnerText, out bInSuccess);
                if (iLogId > 0)
                {
                    sSql = "SELECT LOG_ID, TABLE_ID, OPERATION_CODE, ACTIVITY_TYPE_CODE, LOG_TEXT, LOG_TEXT_HTMLCOMMENTS FROM CLAIM_ACT_LOG_DEF WHERE LOG_ID =" + iLogId;
                    objRdr = DbFactory.GetDbReader(m_ConnectionString, sSql);
                    while (objRdr.Read())
                    {
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Table']");
                        if (objElm != null)
                            objElm.SetAttribute("value", objRdr.GetInt32("TABLE_ID").ToString());
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='OpType']");
                        if (objElm != null)
                        {
                            objElm.SetAttribute("codeid", objRdr.GetInt32("OPERATION_CODE").ToString());
                            objElm.InnerText = objCache.GetCodeDesc(objRdr.GetInt32("OPERATION_CODE"));
                        }
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ActType']");
                        if (objElm != null)
                        {
                            objElm.SetAttribute("codeid", objRdr.GetInt32("ACTIVITY_TYPE_CODE").ToString());
                            objElm.InnerText = objCache.GetCodeDesc(objRdr.GetInt32("ACTIVITY_TYPE_CODE"));
                        }
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LogText']");
                        if (objElm != null)
                            objElm.InnerText = objRdr.GetString("LOG_TEXT");
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LogText_HTMLComments']");
                        if (objElm != null)
                            objElm.InnerText = objRdr.GetString("LOG_TEXT_HTMLCOMMENTS");
                    }
                    objRdr.Close();
                }
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Table']");
                sSql = "SELECT TABLE_NAME, GLOSSARY.TABLE_ID FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND SYSTEM_TABLE_NAME IN (" + sTableNames + ") ORDER BY TABLE_NAME";//mdhamija MITS 26995
                objRdr = DbFactory.GetDbReader(m_ConnectionString, sSql);
                //rsharma220 MITS 33205 Start
                string[] Separator= {" "};
                
                while (objRdr.Read())
                {
                    string TabName = string.Empty;
                    objNewElm = p_objXmlDocument.CreateElement("option");
                    objNewElm.SetAttribute("value", objRdr.GetInt32("TABLE_ID").ToString());

                    if (!string.IsNullOrEmpty(objRdr.GetString("TABLE_NAME")))
                    {
                        string[] arr = objRdr.GetString("TABLE_NAME").Split(Separator, StringSplitOptions.None);
                        foreach(string name in arr)
                        {
                          TabName = TabName +" " + name.Substring(0, 1).ToUpper() + name.Substring(1, name.Length - 1).ToLower();
                        }
                        objNewElm.InnerText= TabName;
                    }
                    else
                    objNewElm.InnerText = string.Empty;
                    //rsharma220 MITS 33205 End
                    objNode.AppendChild(objNewElm);
                }
                objRdr.Dispose();
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimActLog.GetConfigDetails.Err", m_iClientId), p_objEx);//sharishkumar Jira 825
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objNode = null;
                objElm = null;
                objNewElm = null;
            }
        }
        /// <summary>
        /// verify the security permissions in SMS
        /// </summary>
        /// <param name="p_FormName">Form name</param>
        private void GetSecurityPermissions(string p_FormName)
        {
            switch (p_FormName.ToLower())
            {
                case "claimgc":
                    if (!m_objUserLogin.IsAllowedEx(RMB_ClaimActLog_GC))
                        throw new PermissionViolationException(RMPermissions.RMO_ClaimActLog_Access_GC, RMB_ClaimActLog_GC);
                    break;
                case "claimwc":
                    if (!m_objUserLogin.IsAllowedEx(RMB_ClaimActLog_WC))
                        throw new PermissionViolationException(RMPermissions.RMO_ClaimActLog_Access_WC, RMB_ClaimActLog_WC);
                    break;
                case "claimva":
                    if (!m_objUserLogin.IsAllowedEx(RMB_ClaimActLog_VA))
                        throw new PermissionViolationException(RMPermissions.RMO_ClaimActLog_Access_VA, RMB_ClaimActLog_VA);
                    break;
                case "claimdi":
                    if (!m_objUserLogin.IsAllowedEx(RMB_ClaimActLog_DI))
                        throw new PermissionViolationException(RMPermissions.RMO_ClaimActLog_Access_DI, RMB_ClaimActLog_DI);
                    break;
                case "claimpc":
                    if (!m_objUserLogin.IsAllowedEx(RMB_ClaimActLog_PC))
                    {
                        throw new PermissionViolationException(RMPermissions.RMO_ClaimActLog_Access_PC, RMB_ClaimActLog_PC);
                    }
                    break;
                default:
                    break;
            }

        }
        /// <summary>
        /// Save/Edit the config to the database. 
        /// </summary>
        /// <param name="p_objXmlDocument">xml document conatining the details of the config</param>
        /// <returns>xml document</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string sSql = string.Empty;
            XmlElement objElm = null;
            XmlNode objNode = null;
            string sShortCode = string.Empty;
            string sShortDesc = string.Empty;
            int iLogId = 0;
            int iTable = 0;
            int iOpType = 0;
            int iActType = 0;
            string sLogText = string.Empty;
            string sLogtextHtml = string.Empty;
            DbConnection objCon = null;
            DbCommand objCommand = null;
            DbReader objRdr = null;
            bool bInSuccess = false;
            int iDupLogId = 0;             //Added:Yukti,Dt:02/19/2014, MITS 35140
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='LogId']");
                if (objNode != null)
                    iLogId = Conversion.CastToType<int>(objNode.InnerText, out bInSuccess);
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Table']");
                if (objElm != null)
                    iTable = Conversion.CastToType<int>(objElm.GetAttribute("value"), out bInSuccess);
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='OpType']");
                if (objElm != null)
                    iOpType = Conversion.CastToType<int>(objElm.GetAttribute("codeid"), out bInSuccess);
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ActType']");
                if (objElm != null)
                    iActType = Conversion.CastToType<int>(objElm.GetAttribute("codeid"), out bInSuccess);
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='LogText']");
                if (objNode != null)
                    sLogText = objNode.InnerText;
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='LogText_HTMLComments']");
                if (objNode != null)
                    sLogtextHtml = objNode.InnerText;

                objCon = DbFactory.GetDbConnection(m_ConnectionString);
                objCon.Open();
                objCommand = objCon.CreateCommand();
                objCommand.CommandType = CommandType.Text;
                //ST:Yukti,MITS 35140, DT:02/19/2014
                //if (iLogId == 0)
                //{
                    sSql = "SELECT LOG_ID FROM CLAIM_ACT_LOG_DEF WHERE TABLE_ID =" + iTable + " AND OPERATION_CODE =" + iOpType;
                    objRdr = DbFactory.GetDbReader(m_ConnectionString, sSql);
                    while (objRdr.Read())
                    {
                        //iLogId = objRdr.GetInt("LOG_ID"); 
                        iDupLogId = objRdr.GetInt("LOG_ID");                        
                    }
                    objRdr.Dispose();
                    //if (iLogId > 0)
                    //Code changes for MITS:35355 As per QA comments.
                    //if (iDupLogId > 0)
                    if (iDupLogId > 0 && iLogId!=iDupLogId)
                        throw new RMAppException(Globalization.GetString("ClaimActLog.Duplicate.Err",m_iClientId));//sharishkumar Jira 825
                if (iLogId == 0)
                {
                //Ended:YUkti, DT:02/19/2014, MITS 35140
                    iLogId = Utilities.GetNextUID(objCon, "CLAIM_ACT_LOG_DEF", m_iClientId);
                    objCommand.CommandText = "INSERT INTO CLAIM_ACT_LOG_DEF (LOG_ID, TABLE_ID, OPERATION_CODE, ACTIVITY_TYPE_CODE, " +
                    "ADDED_BY_USER, DTTM_RCD_ADDED, UPDATED_BY_USER, DTTM_RCD_UPDATED, LOG_TEXT, LOG_TEXT_HTMLCOMMENTS) VALUES(" +
                    iLogId + "," + iTable + "," + iOpType + "," + iActType + ",'" + m_objUserLogin.LoginName + "'," + Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now) + ",'" + m_objUserLogin.LoginName + "'," + Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now) + ",'" + sLogText.Replace("'", "''") + "', '" + sLogtextHtml.Replace("'", "''") + "')";                    
                }
                else
                {
                    objCommand.CommandText = "UPDATE CLAIM_ACT_LOG_DEF SET TABLE_ID =" + iTable + ", OPERATION_CODE =" + iOpType + ", ACTIVITY_TYPE_CODE =" + iActType +
                    ", UPDATED_BY_USER ='" + m_objUserLogin.LoginName + "', DTTM_RCD_UPDATED =" + Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now) + ", LOG_TEXT ='" + sLogText.Replace("'", "''") + "', LOG_TEXT_HTMLCOMMENTS ='" + sLogtextHtml.Replace("'", "''") + "' WHERE LOG_ID =" + iLogId; 
                }
                objCommand.ExecuteNonQuery();
                objCon.Dispose(); 

                return p_objXmlDocument;
            }
            //St:ygoyal3, MITS 35140, Dt:01/31/2014
            catch (RMAppException rma_objEx)
            {
                throw rma_objEx;
            }
            //Ended:ygoyal3, MITS 35140
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimActLog.Save.Err", m_iClientId), p_objEx);//sharishkumar Jira 825
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                if (objRdr != null)
                {
                    objRdr.Dispose();
                    objRdr = null;
                }
                objCommand = null;
                objNode = null;
                objElm = null;
            }
        }
        /// <summary>
        /// Delete the config from database.
        /// </summary>
        /// <param name="p_objXmlDoc">xml document containing the log id to delete</param>
        /// <returns></returns>
        public XmlDocument Delete(XmlDocument p_objXmlDoc)
        {
            string sSQL = string.Empty;
            XmlElement objNod = null;
            DbConnection objCon = null;
            int iLogId = 0;
            bool bInSuccess = false;
            try
            {
                objNod = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='LogId']");
                iLogId = Conversion.CastToType<int>(objNod.InnerText, out bInSuccess);
                sSQL = "DELETE FROM CLAIM_ACT_LOG_DEF WHERE LOG_ID = " + iLogId ;
                objCon = DbFactory.GetDbConnection(m_ConnectionString);
                objCon.Open();
                objCon.ExecuteNonQuery(sSQL);
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ClaimActLog.Delete.Err", m_iClientId), p_objEx);//sharishkumar Jira 825
            }
            finally
            {
                objNod = null;
                if (objCon != null)
                {
                    objCon.Dispose();
                }
            }
        }
    }
}
