using Riskmaster.Security.Encryption;
using System;
using System.Collections;
using C1.C1Report;
using Riskmaster.Common;
using System.IO;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.Application.RMUtilities;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;

namespace Riskmaster.Application.ExecutiveSummaryReport
{
	/// <summary>	
	/// Author  :  Neelima Dabral
	/// Date    :  05 October 2004
	/// Purpose :  Riskmaster.Application.ExecutiveSummary.PrintPDF class contains the methods 
	///			   that would generate the Executive Summary Report into a PDF Format.
	/// </summary>
	internal class PrintPDF
	{
		/// <summary>		
		/// C1Report object.
		/// </summary>
		private C1Report m_objExecSummReport = null;
		/// <summary>		
		/// Report Layout.
		/// </summary>
		private Layout	 m_objReportLayout=null;
		/// <summary>		
		/// Report Section (Detail)
		/// </summary>
		private Section	 m_objReportDetail=null;
		/// <summary>		
		/// Report Section (Header)
		/// </summary>
		private Section	 m_objReportPageHeader=null;
		/// <summary>		
		/// Report Section (Footer)
		/// </summary>
		private Section	 m_objReportPageFooter=null;
		/// <summary>		
		/// This variable will contain the current number of fields in the report.
		/// </summary>
		private Double	 m_dblNoOfFields=0;
		/// <summary>		
		/// This variable will contain the number of lines that needs to be displayed in a report page.
		/// </summary>
		private int		 m_iTotalNoLinesInPage =40;
		/// <summary>		
		/// This variable will contain the value of current line number on the current page.
		/// </summary>
		private int		 m_iCurrentLine=0;

		/// <summary>
		/// This variable will contain Page count value of the report.
		public int     m_iCurrentPage = 1; //Shradha - For Executive Summary
		/// </summary>
	   
		/// <summary>
		/// This variable will contain ThresHold Limit.
		public int m_iThresHoldLimit; //Shradha - For Executive Summary
		/// </summary>
		/// <summary>
		/// This variable will hold the error
		public string m_sESerror=string.Empty; //Shradha - For Executive Summary
		/// </summary>


		/// <summary>		
		/// This variable will contain the number of lines that needs to be displayed in a report page.
		/// </summary>
		public int m_ExecutiveSummaryReportType = 0; //Shradha
        public int m_ExecutiveSummaryReportClaimId = 0; //Manish
        public string m_ExecutiveSummaryConnectionString = string.Empty; //Manish

		/// <summary>		
		/// This variable will contain the number of lines that needs to be displayed in a report page.
		/// </summary>
		private int m_ExecutiveSummaryModuleID = 0; //Shradha

		/// <summary>		
		/// This variable will contain the number of lines that needs to be displayed in a report page.
		/// </summary>
		private ExecutiveSummary m_objExecutiveSummary = null; //Shradha

		private bool m_bIsCalledFromDesktop=false; //Shradha
		public string m_sTableName = string.Empty; //shradha


		/// <summary>	
		/// <summary>		
		/// This variable will contain X-Coordinate value where the data would be printed on the report.
		/// </summary>
		private int		 m_iCurrentXPos=0;
		/// <summary>		
		/// The structure contains the data that would be printed.
		/// </summary>
		private ReportData  m_structReportData;
		/// <summary>		
		/// This variable will be used to calculate the Y-Coordinate  value where the data would be printed on the report.
		/// </summary>
		private double   m_dblCurrentY=0;
		/// <summary>		
		/// The variable contains value for the line spacing between two lines in a report.
		/// </summary>
		private int		 m_iLineSpace=0;
		/// <summary>		
		/// The variable identifies the first page of the report.
		/// </summary>
		private bool	 m_bFirstPage=false;

		/// <summary>		
		/// The variable has the userlogin object of the logged in user 
		/// </summary>
		public Riskmaster.Security.UserLogin m_UserLogin ;//Shradha
		//public string m_sConnectionString = String.Empty; //Shradha
		public string m_sTMConnectionString = String.Empty; //Shradha

		/// <summary>
		/// MGaba2:MITS 21905 : Width of Second column(6010-2395)
		/// </summary>
        private const double WIDTH_SECOND_COLUMN=3615;

        /// <summary>
        /// Variable for ClientID in multi-tenant environment, cloud
        /// </summary>
        private int m_iClientID = 0;

		/// <summary>
		/// It would initialize the objects needed for printing the PDF Report.
		/// </summary>
        //internal PrintPDF()
        internal PrintPDF(string p_sConnString, int p_iClientID)
		{
			try
			{
				m_objExecSummReport = new C1Report();				
				m_objExecSummReport.Clear();					
				m_objReportLayout = m_objExecSummReport.Layout;
				m_objReportLayout.Orientation = OrientationEnum.Portrait;
				m_objReportLayout.MarginLeft = 620;						
				m_objReportLayout.Width = 11000;										
				m_objReportDetail= m_objExecSummReport.Sections[SectionTypeEnum.Detail];
				m_objReportDetail.Visible=true;
				m_bFirstPage = true;
                m_iClientID = p_iClientID; //ash - cloud
				m_sTMConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource",m_iClientID);  //Shradha - For Executive Summary on 06/30/2011
                //Ash - cloud, get config setting from db
				//int.TryParse(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold").ToString(), out m_iThresHoldLimit); //Shradha - For Executive Summary on 06/30/2011                
                // TODO Change the Connection String for Cloud
                int.TryParse(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold", p_sConnString, m_iClientID).ToString(), out m_iThresHoldLimit); //Shradha - For Executive Summary on 06/30/2011                
                                
				//m_objExecutiveSummary = new ExecutiveSummary(m_sConnectionString);

				//m_objExecutiveSummary = new ExecutiveSummary(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold").ToString());
				//m_objExecutiveSummary = new ExecutiveSummary(this.m_UserLogin.objRiskmasterDatabase.ConnectionString);
				
				//m_objExecutiveSummary = new ExecutiveSummary( this.m_UserLogin.objRiskmasterDatabase.ConnectionString );
				//m_objExecutiveSummary.m_sTMConnectionString = m_sTMConnectionString;

				//rsolanki2: execSummary offline dekstop app enhacement
				m_bIsCalledFromDesktop = (AppDomain.CurrentDomain.FriendlyName.StartsWith("ExecutiveSummaryScheduler",StringComparison.OrdinalIgnoreCase));
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintPDF.ErrorCreate", m_iClientID),p_objException);
			}	
		}

		/// <summary>
		/// It would initialize the objects needed for printing the PDF Report.
		/// </summary>
        //internal PrintPDF(int iExecSummaryReportType, int iExecSummaryModuleID) //shradha
        internal PrintPDF(int iExecSummaryReportType, int iExecSummaryModuleID, string p_sConnString, int p_iClientID)         
		{
			try
			{
				m_objExecSummReport = new C1Report();
				m_objExecSummReport.Clear();
				m_objReportLayout = m_objExecSummReport.Layout;
				m_objReportLayout.Orientation = OrientationEnum.Portrait;
				m_objReportLayout.MarginLeft = 620;
				m_objReportLayout.Width = 11000;// atavaragiri MITS 28213 //
				m_objReportDetail = m_objExecSummReport.Sections[SectionTypeEnum.Detail];
				m_objReportDetail.Visible = true;
				m_bFirstPage = true;
                m_iClientID = p_iClientID; //ash - cloud
				m_sTMConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource",m_iClientID);  //Shradha - For Executive Summary on 06/30/2011
				//Ash - cloud, get config setting from db
                //int.TryParse(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold").ToString(), out m_iThresHoldLimit); //Shradha - For Executive Summary on 06/30/2011
                // TODO Change the Connection String for Cloud
                int.TryParse(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold", p_sConnString, m_iClientID).ToString(), out m_iThresHoldLimit);    // Ash - cloud             
				//m_objExecutiveSummary = new ExecutiveSummary(m_sConnectionString);

				//m_objExecutiveSummary = new ExecutiveSummary(RMConfigurationManager.GetAppSetting("ExecSummaryOfflieThreshold").ToString());
				//m_objExecutiveSummary = new ExecutiveSummary(this.m_UserLogin.objRiskmasterDatabase.ConnectionString);
				//m_objExecutiveSummary.m_UserLogin = objUserLogin;
				//m_objExecutiveSummary = new ExecutiveSummary(m_UserLogin.objRiskmasterDatabase.ConnectionString);
				//m_objExecutiveSummary.m_sTMConnectionString = m_sTMConnectionString;

				m_ExecutiveSummaryReportType = iExecSummaryReportType;
				m_ExecutiveSummaryModuleID = iExecSummaryModuleID;

				//rsolanki2: execSummary offline dekstop app enhacement
				m_bIsCalledFromDesktop = (AppDomain.CurrentDomain.FriendlyName.StartsWith("ExecutiveSummaryScheduler", StringComparison.OrdinalIgnoreCase));            

			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("PrintPDF.PrintPDF.ErrorCreate", m_iClientID), p_objException);
			}
		}
		/// <summary>
		/// This method would set the line spacing for the report.		
		/// </summary>
		/// <param name="p_iLineSpace">Line Space</param>		
		internal void SetLineSpacing(int p_iLineSpace)
		{
			try
			{
				m_iLineSpace = p_iLineSpace;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.SetLineSpacing.ErrorSet", m_iClientID), p_objException);
			}	
		}
		/// <summary>
		/// This method would set the font for the report.		
		/// </summary>
		/// <param name="p_sFont">Font</param>	
		internal void SetFont(string p_sFont)
		{
			try
			{			
				m_objExecSummReport.Font.Name = p_sFont;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.SetFont.ErrorSet", m_iClientID), p_objException);
			}
		}
		/// <summary>
		/// This method would set the font size for the report.		
		/// </summary>
		/// <param name="p_fFontSize">Font size</param>	
		internal void SetFontSize(float p_fFontSize)
		{
			//try
			//{
				m_objExecSummReport.Font.Size=p_fFontSize;
			//}
			//catch(Exception p_objException)
			//{
			//    throw new RMAppException(Globalization.GetString("PrintPDF.SetFontSize.ErrorSet"),p_objException);
			//}
		}
		/// <summary>
		/// This method would set the font style as Bold/Normal for the report.		
		/// </summary>
		/// <param name="p_bFontBold">True/False</param>	
		internal void SetFontBold(bool p_bFontBold)
		{
			//try
			//{
				m_objExecSummReport.Font.Bold=p_bFontBold;
			//}
			//catch(Exception p_objException)
			//{
			//    throw new RMAppException(Globalization.GetString("PrintPDF.SetFontBold.ErrorSet"),p_objException);
			//}
		}
		/// <summary>
		/// This method would set the font style as Italic/Normal for the report.		
		/// </summary>
		/// <param name="p_bFontItalic">True/False</param>	
		internal void SetFontItalic(bool p_bFontItalic)
		{
			//try
			//{
				m_objExecSummReport.Font.Italic=p_bFontItalic;
			//}
			//catch(Exception p_objException)
			//{
			//    throw new RMAppException(Globalization.GetString("PrintPDF.SetFontItalic.ErrorSet"),p_objException);
			//}
		}
		/// <summary>
		/// This method would set the font style as Underline/Normal for the report.		
		/// </summary>
		/// <param name="p_bFontUnderline">True/False</param>	
		internal void SetFontUnderline(bool p_bFontUnderline)
		{
			//try
			//{
				m_objExecSummReport.Font.Underline=p_bFontUnderline;
			//}
			//catch(Exception p_objException)
			//{
			//    throw new RMAppException(Globalization.GetString("PrintPDF.SetFontUnderline.ErrorSet"),p_objException);
			//}
		}
		/// <summary>
		/// This method would call methods to print the header and footer for the report.		
		/// </summary>
		/// <param name="p_sReportTitle">Report Title</param>	
		/// <param name="p_sReportFooter">Report Footer</param>	
		/// <param name="p_sProductName">Product Name</param>	
		/// <param name="p_sCompanyName">Company Name</param>	
		/// <param name="p_sClaimHeader">Claimant Name</param>	
		internal void PrintOutline(string p_sReportTitle,string p_sReportFooter,string p_sProductName,string p_sCompanyName,string p_sClaimHeader)
		{
			string sHeader="";		
			string sFooter2="";
			try
			{
				
				sHeader = p_sReportTitle + " Executive Summary";
				sFooter2="Confidential Data - " + p_sProductName;				
				SetFontSize(10);
				SetFontBold(true);
				PrintPageHeader(sHeader,p_sCompanyName,p_sClaimHeader);
				
				SetFontItalic(true);
				PrintPageFooter(p_sReportFooter,sFooter2);
				SetFontItalic(false);
				SetFontSize(10);				
				SetFontBold(false);
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintOutline.ErrorPrint", m_iClientID), p_objException);
			}			
		}
		/// <summary>
		/// This method would print the header for the report.		
		/// </summary>
		/// <param name="p_sHeader">Report Title</param>			
		/// <param name="p_sCompanyName">Company Name</param>	
		/// <param name="p_sClaimHeader">Claimant Name</param>	
		internal void PrintPageHeader(string p_sHeader,string p_sCompanyName,string p_sClaimHeader)
		{
			Field objField = null;
			double dblColPos=0;
			try
			{
				m_objReportPageHeader = m_objExecSummReport.Sections[SectionTypeEnum.PageHeader];
				m_objReportPageHeader.Visible=true;				
				
				objField = m_objReportPageHeader.Fields.Add("Rect", "", 0,0, m_objReportLayout.Width, 1200);				
				objField.BorderStyle = BorderStyleEnum.Solid;		
                //rsharma220 RMA-1785, MITS 35665 Start
				objField =m_objReportPageHeader.Fields.Add("FldHeader0" ,"Executive Summary",40,40,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				dblColPos = dblColPos +objField.Width;
				objField.CanGrow = true;							
                
				objField =m_objReportPageHeader.Fields.Add("FldHeader1" ,p_sCompanyName,(m_objReportLayout.Width)/2-2500,240,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				dblColPos = dblColPos +objField.Width;
				objField.CanGrow = true;
								
				objField =m_objReportPageHeader.Fields.Add("FldHeader2" ,System.DateTime.Now.ToString("d"),(m_objReportLayout.Width - 1000),40,0,300);				
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;
                //rsharma220 RMA-1785, MITS 35665 End
				objField = m_objReportPageHeader.Fields.Add("FldLine2", "", 0,400, m_objReportLayout.Width, 20);
				objField.LineSlant = LineSlantEnum.NoSlant;
				objField.LineWidth = 10;
				objField.BorderStyle = BorderStyleEnum.Solid;

				if (p_sClaimHeader.Trim() != "")
				{
					objField =m_objReportPageHeader.Fields.Add("FldHeader4" ,p_sClaimHeader,100,500,0,300);
					objField.Width=m_objReportLayout.Width - objField.Left;				
					objField.CanGrow = true;
				}
				SetLineSpacing(900);
				objField =m_objReportPageHeader.Fields.Add("FldHeader3" ,p_sHeader,(m_objReportLayout.Width/2)-2000,m_iLineSpace,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;
				SetLineSpacing(400);		
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintPageHeader.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objField = null;
			}

		}
		/// <summary>
		/// This method would print the footer for the report.		
		/// </summary>
		/// <param name="p_sFooterLeft">Left Footer String</param>			
		/// <param name="p_sFooterRight">Right Footer String</param>			
		internal void PrintPageFooter(string p_sFooterLeft,string p_sFooterRight)
		{
			Field objField = null;
			try
			{
				m_objReportPageFooter = m_objExecSummReport.Sections[SectionTypeEnum.PageFooter];
				m_objReportPageFooter.Visible=true;


				objField = m_objReportPageFooter.Fields.Add("Rect2", "", 0,0, m_objReportLayout.Width, 500);				
				objField.BorderStyle = BorderStyleEnum.Solid;

				objField = m_objReportPageFooter.Fields.Add("FldFooter0", "", 0,0, m_objReportLayout.Width, 20);
				objField.LineSlant = LineSlantEnum.NoSlant;
				objField.LineWidth = 10;
				objField.BorderStyle = BorderStyleEnum.Solid;				

				objField =m_objReportPageFooter.Fields.Add("FldFooter1",p_sFooterLeft,0,50,0,300);
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;
				
				objField = m_objReportPageFooter.Fields.Add("FldFooter2", @"""Page "" & Page ", 4000, 50, 4000, 300);
				objField.Width=m_objReportLayout.Width - objField.Left;	
				objField.CanGrow = true;
				objField.Calculated = true;

				//Changed Rakhi for MITS 13067-START:To change the footer info(Product Name from RISKMASTER to REM) on Executive Summary
				//Changed objField.left from 7800 to 6300 to prevent split of REM-Risk Enterprise Management.
				//objField =m_objReportPageFooter.Fields.Add("FldFooter3",p_sFooterRight,7800,50,0,300);
				if (p_sFooterRight.Length <= 30)
				{
					objField = m_objReportPageFooter.Fields.Add("FldFooter3", p_sFooterRight, 7800, 50, 0, 300);
				}
				else
				{
					objField = m_objReportPageFooter.Fields.Add("FldFooter3", p_sFooterRight, 6300, 50, 0, 300);
				}
				//Changed Rakhi for MITS 13067-END:To change the footer info(Product Name from RISKMASTER to REM) on Executive Summary
				objField.Width=m_objReportLayout.Width - objField.Left;				
				objField.CanGrow = true;

				SetLineSpacing(400);			
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintPageFooter.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objField=null;
			}
		}
		/// <summary>
		/// This method would print the sub-headings in the report.		
		/// </summary>
		/// <param name="p_sHeading">Heading to be printed</param>			
		/// <param name="p_bFontBold">Bold (True/False)</param>
		/// <param name="p_fFontSize">Font size</param>
		/// <param name="p_bFontUnderline">Underline (True/False)</param>
		internal void PrintSubHeading(string p_sHeading,bool p_bFontBold,float p_fFontSize,bool p_bFontUnderline)
		{
			FieldCollection objFieldColl=null;
			try
			{
				objFieldColl = m_objExecSummReport.Fields;

				if ((m_iCurrentLine == m_iTotalNoLinesInPage))
				{
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;					
					m_iCurrentPage += 1; //Shradha - For executive summary 
					objFieldColl.Add("Rect4" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect4" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect4" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect4" + m_dblNoOfFields].Visible  = true;
				}
				SetFontUnderline(p_bFontUnderline);
				SetFontBold(p_bFontBold);
				SetFontSize(p_fFontSize);
				
				objFieldColl.Add("Fld" + m_dblNoOfFields, p_sHeading ,100,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);			
				objFieldColl["Fld" + m_dblNoOfFields].Width = m_objReportLayout.Width -objFieldColl["Fld" + m_dblNoOfFields].Left;				
				objFieldColl["Fld" + m_dblNoOfFields].CanGrow = true;				
				objFieldColl["Fld" + m_dblNoOfFields].Visible = true;	
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;
				objFieldColl["Fld" + m_dblNoOfFields].Align = FieldAlignEnum.JustTop;				
				
				
				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;

				SetFontBold(false);				
				SetFontUnderline(false);
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintSubHeading.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objFieldColl = null;
			}
		}
		/// <summary>
		/// This method would print a string in the center of the report.		
		/// </summary>
		/// <param name="p_sHeading">Heading to be printed</param>			
		/// <param name="p_bFontBold">Bold (True/False)</param>
		/// <param name="p_fFontSize">Font size</param>
		/// <param name="p_bFontUnderline">Underline (True/False)</param>
		internal void PrintCenter(string p_sHeading,bool p_bFontBold,float p_fFontSize,bool p_bFontUnderline)
		{
			FieldCollection objFieldColl=null;
			try
			{
				objFieldColl = m_objExecSummReport.Fields;

				if ((m_iCurrentLine == m_iTotalNoLinesInPage))
				{
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;	
					m_iCurrentPage += 1; //Shradha - For executive summary 
					objFieldColl.Add("Rect5" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect5" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect5" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect5" + m_dblNoOfFields].Visible  = true;
				}
				SetFontUnderline(p_bFontUnderline);
				SetFontBold(p_bFontBold);
				SetFontSize(p_fFontSize);
				if(p_sHeading.Length < 25)
					objFieldColl.Add("Fld" + m_dblNoOfFields, p_sHeading ,(m_objReportLayout.Width/2)-1000,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);			
				else
					objFieldColl.Add("Fld" + m_dblNoOfFields, p_sHeading ,(m_objReportLayout.Width/2)-1500,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);			
				objFieldColl["Fld" + m_dblNoOfFields].Width = m_objReportLayout.Width -objFieldColl["Fld" + m_dblNoOfFields].Left;				
				objFieldColl["Fld" + m_dblNoOfFields].CanGrow = true;				
				objFieldColl["Fld" + m_dblNoOfFields].Visible = true;	
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;
				objFieldColl["Fld" + m_dblNoOfFields].Align = FieldAlignEnum.JustTop;				
				
				
				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;

				SetFontBold(false);				
				SetFontUnderline(false);
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintCenter.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objFieldColl=null;
			}
		}
		/// <summary>
		/// This method would render the PDF report to a specified file.
		/// </summary>
		/// <param name="p_sReportFile">Filename</param>					
		internal void RenderToFile(string p_sReportFile)
		{
			try
			{
				m_objExecSummReport.RenderToFile(p_sReportFile,FileFormatEnum.PDF);
			}			
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.RenderToFile.ErrorPrint", m_iClientID), p_objException);
			}			
		}
		/// <summary>
		/// This method would render the PDF report to memory stream.
		/// </summary>
		/// <returns>Memory Stream</returns>
		internal MemoryStream RenderToStream()
		{
			MemoryStream objPDFMemoryStream=null;
			try
			{
				objPDFMemoryStream=new MemoryStream();
				m_objExecSummReport.RenderToStream(objPDFMemoryStream,FileFormatEnum.PDF);
			}			
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.RenderToStream.ErrorPrint", m_iClientID), p_objException);
			}			
			return objPDFMemoryStream;
		}
		/// <summary>
		/// This method would print double lines in the report.
		/// </summary>		
		internal void PrintDoubleLine()
		{
			FieldCollection objFieldColl=null;
			bool bPageBreak=false;	
			try
			{
				objFieldColl = m_objExecSummReport.Fields;
				if ((m_iCurrentLine == m_iTotalNoLinesInPage) && !(bPageBreak))
				{
					bPageBreak = true;
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;		
					m_iCurrentPage += 1; //Shradha - For executive summary 
					objFieldColl.Add("Rect10" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect10" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect10" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect10" + m_dblNoOfFields].Visible  = true;	
				}
				objFieldColl.Add("Fld" + m_dblNoOfFields, "", 0,m_dblCurrentY * m_iLineSpace, m_objReportLayout.Width, 50);
				objFieldColl["Fld" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;			
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;				

				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintLine.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objFieldColl = null;
			}
		}
		/// <summary>
		/// This method would print single line in the report.
		/// </summary>		
		internal void PrintLine()
		{
			FieldCollection objFieldColl=null;
			bool bPageBreak=false;	
			try
			{
				objFieldColl = m_objExecSummReport.Fields;
				if ((m_iCurrentLine == m_iTotalNoLinesInPage) && !(bPageBreak))
				{
					bPageBreak = true;
					objFieldColl["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;		
					m_iCurrentPage += 1; //Shradha - for Executive Summary 
					objFieldColl.Add("Rect6" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objFieldColl["Rect6" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objFieldColl["Rect6" + m_dblNoOfFields].Section = 0;
					objFieldColl["Rect6" + m_dblNoOfFields].Visible  = true;	
				}
				objFieldColl.Add("Fld" + m_dblNoOfFields, "", 0,m_dblCurrentY * m_iLineSpace, m_objReportLayout.Width, 20);
				objFieldColl["Fld" + m_dblNoOfFields].LineSlant = LineSlantEnum.NoSlant;
				objFieldColl["Fld" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;			
				objFieldColl["Fld" + m_dblNoOfFields].Section = 0;

				++m_dblNoOfFields;
				++m_iCurrentLine;
				++m_dblCurrentY;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintLine.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objFieldColl = null;
			}
		}		
		/// <summary>
		/// This method would print data string in the report.
		/// </summary>
		/// <param name="p_arrlstTabAt">Arraylist specifying the columns starting position</param>
		/// <param name="p_arrlstPrintData">Arraylist containing the data to be printed</param>
		internal void PrintString(ArrayList p_arrlstTabAt,ArrayList p_arrlstPrintData)
		{
			
			FieldCollection  objField=null;						
			string sData ="";
			int   iDataType=0;
			int iCounter=0;
            int iFieldIndex = 0; // MITS 35699: bkuzhanthaim : Performance improvements for TC10
			bool bPageBreak=false;	
			bool bSetBold = false;
			bool bRightAlign=false;
			string sSQL = string.Empty;
			double dColumnWidth = 0.0; //MGaba2:MITS 21905
			string IsBoolVal = ""; // vprasanth: MITS 28878
			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(m_ExecutiveSummaryReportClaimId, CommonFunctions.NavFormType.None,m_ExecutiveSummaryConnectionString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture, false);

				SetFontSize(10);				
				SetFontBold(false);
				objField = m_objExecSummReport.Fields;				
				for(iCounter=0;iCounter < p_arrlstPrintData.Count;++iCounter)
				{					
					m_structReportData =(ReportData) p_arrlstPrintData[iCounter];
					sData = m_structReportData.Data;
					iDataType = m_structReportData.DataType;	
					bSetBold = m_structReportData.SetBold;
					bRightAlign=m_structReportData.RightAlign;
					
					if(bSetBold)
						SetFontBold(true);
					else
						SetFontBold(false);	

					m_iCurrentXPos = Common.Conversion.ConvertStrToInteger(p_arrlstTabAt[iCounter].ToString());
					switch(iDataType)
					{
						case GlobalExecSummConstant.CHARSTRING:																			
						case GlobalExecSummConstant.NUMERIC:
							break;
						case GlobalExecSummConstant.DATESTRING:	
							sData = Common.Conversion.GetDBDateFormat(sData,"d");
							break;						
						case GlobalExecSummConstant.MONEY:								
							sData = String.Format("{0:C}",Common.Conversion.ConvertStrToDouble(sData));
							break;
						case GlobalExecSummConstant.YESNO:
							//MITS 10271 for data type of bit, the value is False
							IsBoolVal = "Yes"; // vprasanth: MITS 28878 - flag used to identfy boolean values fields to print on pdf
							if (sData == "0" || sData == "False")
								sData = "No";
							else
								sData = "Yes";														
							break;
						case GlobalExecSummConstant.TIMESTRING:												
							if (sData.Trim() != "")
							{
								sData = sData.Substring(0,2) + ":" + sData.Substring(2,2);
								DateTime datDataValue=DateTime.MinValue;
								datDataValue = System.DateTime.Parse(sData);
								sData= datDataValue.ToString("T");
							}
							break;
					}
					if ((m_iCurrentLine == m_iTotalNoLinesInPage) && !(bPageBreak))
					{
						bPageBreak = true;
						objField["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
						m_iCurrentLine = 0;											
							m_iCurrentPage += 1; //Shradha - For executive summary 
                           
						objField.Add("Rect2" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
                        iFieldIndex = objField.Count - 1;// MITS 35699: bkuzhanthaim : Performance improvements for TC10
                        if (iFieldIndex == -1) iFieldIndex = 0;
                        objField[iFieldIndex].BorderStyle = BorderStyleEnum.Solid;
                        objField[iFieldIndex].Section = 0;
                        objField[iFieldIndex].Visible = true;	
					}					
					objField.Add("Fld" + m_dblNoOfFields, sData ,m_iCurrentXPos,m_dblCurrentY*m_iLineSpace,0,m_iLineSpace);
                    iFieldIndex = objField.Count - 1;// MITS 35699: bkuzhanthaim : Performance improvements for TC10
                    if (iFieldIndex == -1) iFieldIndex = 0;
					//MITS 26441 : Client requests change to custom print process for Exec Summary/Large Notes 
                    //modified by Raman Bhatia on 10/21/2011	
                    
                    // Shradha - 06/30/2011
					/*
                    if ((!m_bIsCalledFromDesktop) && CheckThresHoldLimit())
						{
                            

                            throw new InvalidCriteriaException();
						}
				   */
					//MGaba2: MITS 21905:Start
				   //field values in the left column overlap the right column if text is too long
					//Column width is in twips.So we need to explicitly assign a constant rather than calculating width dynamically in double
                    if (iCounter != p_arrlstPrintData.Count - 1)
                    {
                        dColumnWidth = Convert.ToDouble(p_arrlstTabAt[iCounter + 1]) - Convert.ToDouble(p_arrlstTabAt[iCounter]);
                    }

                    // vprasanth: MITS 28878 - commented because even the second field value is small it is displaying it on above the actual line
                    //if (dColumnWidth == WIDTH_SECOND_COLUMN)
                    if ((dColumnWidth == WIDTH_SECOND_COLUMN) && IsBoolVal != "Yes")
                    {
                        objField[iFieldIndex].Width = WIDTH_SECOND_COLUMN - 150;
                    }
                    else
                    {
                        objField[iFieldIndex].Width = m_objReportLayout.Width - objField[iFieldIndex].Left;
                    }
					//MGaba2: MITS 21905:End

                    objField[iFieldIndex].CanGrow = true;
                    objField[iFieldIndex].Visible = true;
                    objField[iFieldIndex].Section = 0;
					if (bRightAlign)
                        objField[iFieldIndex].Align = FieldAlignEnum.RightMiddle;
					else
						//objField["Fld" + m_dblNoOfFields].Align = FieldAlignEnum.JustTop;		//atavaragiri MITS 28213:due to this setting,value was being printed with huge spaces between 2 words//			
                        objField[iFieldIndex].Align = FieldAlignEnum.LeftMiddle;
					
					++m_dblNoOfFields;	
				}	
				++m_iCurrentLine;
				++m_dblCurrentY;				
			}
			catch (InvalidCriteriaException p_objException)
			{
                throw new InvalidCriteriaException(Globalization.GetString("PrintPDF.ValidateCriteria.ThresholdExceeded", m_iClientID), p_objException);
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintString.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objField = null;
			}			
		}

		/// <summary>
		/// Shradha Dwivedi
		/// This function Checks the Threshold Limit for Exec Summary
		/// </summary>
		/// <returns>boolean</returns>
		private bool CheckThresHoldLimit()
		{

			bool bLimitExceeded = false;
			if (m_iThresHoldLimit < m_iCurrentPage || m_objExecSummReport.Page > m_iThresHoldLimit) // check for the threshold 
			{
				TaskManager objTaskManager = new TaskManager(m_UserLogin, m_iClientID);//rkaur27
				string sModule = string.Empty;
				string sReportType = string.Empty;
				try
				{



					ScheduleDetails objSchedule; 
					string sModulename = string.Empty;
					//int iReportType;

					int iTypeId = 0;
					objSchedule = new ScheduleDetails();
					objSchedule.ScheduleState = 1;
					objSchedule.ScheduleTypeId = 1;

					string sSql = "SELECT TASK_TYPE_ID  FROM TM_TASK_TYPE  WHERE LOWER(NAME)  = 'executivesummaryscheduler' ";

					using (DbReader objRdr = DbFactory.ExecuteReader(m_sTMConnectionString, sSql))
					{
						if (objRdr.Read())
						{
							Int32.TryParse(objRdr[0].ToString(), out iTypeId);
						}
						else
						{
							throw new Exception("TM Task Id not found");
						}
					}

					objSchedule.TaskTypeId = iTypeId;
					objSchedule.IntervalTypeId = 0;
					objSchedule.Interval = 0;
					objSchedule.TrigDirectory = "";                    
					objSchedule.FinalRunDTTM = "";
					objSchedule.TaskName = "ExecutiveSummary"; //hardcoded for exec summary comes from TM_TASK_TYPE table
					objSchedule.Mon = 0;
					objSchedule.Tue = 0;
					objSchedule.Wed = 0;
					objSchedule.Thu = 0;
					objSchedule.Fri = 0;
					objSchedule.Sat = 0;
					objSchedule.Sun = 0;
					objSchedule.Jan = 0;
					objSchedule.Feb = 0;
					objSchedule.Mar = 0;
					objSchedule.Apr = 0;
					objSchedule.May = 0;
					objSchedule.Jun = 0;
					objSchedule.Jul = 0;
					objSchedule.Aug = 0;
					objSchedule.Sep = 0;
					objSchedule.Oct = 0;
					objSchedule.Nov = 0;
					objSchedule.Dec = 0;                                      

					if (m_ExecutiveSummaryReportType==1) // for claims
					{
						sModule = "cl";
					}
					else if (m_ExecutiveSummaryReportType==0)
					{
						sModule = "ev";
					}
					else if (m_ExecutiveSummaryReportType==2)
					{
						sModule = "at";
					}
									   

					objSchedule.Config = "<Task Name=\"ExecutiveSummary\" cmdline=\"yes\"><Path>ExecutiveSummaryScheduler.exe</Path><Args><arg>-ru"
					   + m_UserLogin.LoginName
					   + "</arg><arg>-rp"
					   + System.Security.SecurityElement.Escape(RMCryptography.EncryptString(m_UserLogin.Password))
					   + "</arg><arg>-ds"
					   + m_UserLogin.objRiskmasterDatabase.DataSourceName
					   + (
								(sModule == "at")
								? "</arg><arg>-tb" + m_sTableName
								: string.Empty
						  )
					   + "</arg><arg>-" + sModule + m_ExecutiveSummaryModuleID
					   + "</arg></Args></Task>";

					#region Duplicacy check

					int iAlreadyScheduledCount = 0;
					Regex regex = new Regex(@"^(?<start>.*){(?<driver>Oracle.*)}(?<end>.*)$", RegexOptions.IgnoreCase);
					if (regex.Match(m_sTMConnectionString).Success)
					{
						//for Oracle db
						sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE TO_CHAR(LOWER(CONFIG)) = TO_CHAR(LOWER('" + objSchedule.Config + "'))";
					}
					else
					{
						//for sql serevr db
						sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE LOWER(CONFIG) = LOWER('" + objSchedule.Config + "')";                        
					}

					using (DbReader objRdr = DbFactory.ExecuteReader(m_sTMConnectionString, sSql))
					{
						if (objRdr.Read())
						{
							Int32.TryParse(objRdr[0].ToString(), out iAlreadyScheduledCount);
						}
					}

					if (iAlreadyScheduledCount > 0)
					{
						// the task is already scheduled. Exiting in which case.
						return true;
					}

					#endregion


					#region Offline Hours check

					string sOfflineHoursStart;
					bool bSuccess = false;
					DateTime dtOfflineHoursStart;
                    // Ash - cloud, get config setting from db
                    //sOfflineHoursStart = RMConfigurationManager.GetAppSetting("ExecSummaryOfflineHours");
                    sOfflineHoursStart = RMConfigurationManager.GetAppSetting("ExecSummaryOfflineHours", this.m_UserLogin.objRiskmasterDatabase.ConnectionString, m_iClientID);
                                      
					
					bSuccess = DateTime.TryParse(sOfflineHoursStart, out dtOfflineHoursStart);
					if (bSuccess)
					{
						//when the Offline Hours are defined
						objSchedule.TimeToRun = Conversion.GetTime(dtOfflineHoursStart.ToString());
						objSchedule.NextRunDTTM = Conversion.ToDbDateTime(dtOfflineHoursStart);
					}
					else
					{
						//when the Offline Hours are Not defined. 25 sec are added to curretn DT and the task is scheduled to it.
						objSchedule.TimeToRun = Conversion.GetTime(DateTime.Now.TimeOfDay.Add(new TimeSpan(25000)).ToString());
						objSchedule.NextRunDTTM = Conversion.ToDbDateTime(DateTime.Now);
					}

					objSchedule.DayOfMonth = 0;

					#endregion
				

					objTaskManager.SaveSettings(objSchedule);

					bLimitExceeded = true;
				}


				catch (Exception e)
				{
					throw new Exception("CheckThresHoldLimit Exception",e.InnerException);
				}

			}
			return bLimitExceeded;
		  }

		/// <summary>
		/// Gets the next UID from the TM_IDs table.
		/// </summary>
		/// <param name="p_sTableName"></param>
		/// <returns></returns>
		private int GetNextUID()
		{
			string sSQL = string.Empty;
			DbConnection objConn = null;
			DbReader objReader = null;
			int iOrigUID = 0;
			int iNextUID = 0;
			int iCollisionRetryCount = 0;
			int iErrRetryCount = 0;
			int iRows = 0;

			const int COLLISION_RETRY_COUNT = 1000;
			const int ERROR_RETRY_COUNT = 5;
			string sQuery = string.Empty;

			try
			{
				do
				{
					sSQL = "SELECT NEXT_ID FROM TM_IDS WHERE TABLE_NAME = 'TM_SCHEDULE'";

					objConn = DbFactory.GetDbConnection(m_sTMConnectionString);
					objConn.Open();

					using (objReader = objConn.ExecuteReader(sSQL))
					{
						if (!objReader.Read())
						{
							objReader.Close();
							objReader = null;
                            throw new RMAppException(Globalization.GetString("Riskmaster.Application.PrintPDF.GetNextUID.NoSuchTable", m_iClientID));
						}

						iNextUID = objReader.GetInt("NEXT_ID");
						objReader.Close();
						objReader = null;

						// Compute next id
						iOrigUID = iNextUID;
						if (iOrigUID != 0)
							iNextUID++; 
						else
							iNextUID = 2;

						// try to reserve id (searched update)
						sSQL = "UPDATE TM_IDS SET NEXT_ID = " + iNextUID + " WHERE TABLE_NAME = 'TM_SCHEDULE'";

						// only add searched clause if no chance of a null originally
						// in row (only if no records ever saved against table)   
						if (iOrigUID != 0)
							sSQL += " AND NEXT_ID = " + iOrigUID;

						// Try update
						try
						{
							iRows = objConn.ExecuteNonQuery(sSQL);
						}
						catch (Exception e)
						{
							iErrRetryCount++;
							if (iErrRetryCount >= 5)
							{
								objConn.Close();
                                throw new RMAppException(Globalization.GetString("Riskmaster.Application.PrintPDF.GetNextUID.ErrorModifyingDB", m_iClientID), e);
							}
						}
						// if success, return
						if (iRows == 1)
						{
							objConn.Close();
							return iNextUID - 1;
						}
						else // collided with another user - try again (up to 1000 times)
							iCollisionRetryCount++;

					}

				} while ((iErrRetryCount < ERROR_RETRY_COUNT) && (iCollisionRetryCount < COLLISION_RETRY_COUNT));

				if (iCollisionRetryCount >= COLLISION_RETRY_COUNT)
				{
					objConn.Close();
					throw new RMAppException
                        (Globalization.GetString("Riskmaster.Application.PrintPDF.GetNextUID.CollisionTimeout", m_iClientID));
				}
				objConn.Close();
				//shouldn't get here under normal conditions.
				return 0;

			}
			catch (RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.PrintPDF.GetNextUID.Error", m_iClientID), p_objEx);
			}

			finally
			{
				if (objReader != null)
				{
					objReader.Close();
					objReader.Dispose();
				}
				if (objConn != null)
				{
					if (objConn.State == System.Data.ConnectionState.Open)
					{
						objConn.Close();
						objConn.Dispose();
					}
				}
			}
		}
		/// <summary>
		/// This method will find where the copyright needs to be printed on the report.
		/// </summary>		
		internal void PrintCopyright()
		{
			FieldCollection  objField=null;		
			try
			{
				objField= m_objExecSummReport.Fields;			
				if (m_iCurrentLine + 8 > m_iTotalNoLinesInPage)
				{
					objField["Fld" + (m_dblNoOfFields-1)].ForcePageBreak=ForcePageBreakEnum.After;
					m_iCurrentLine = 0;											

					objField.Add("Rect9" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objField["Rect9" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objField["Rect9" + m_dblNoOfFields].Section = 0;
					objField["Rect9" + m_dblNoOfFields].Visible  = true;	

					++m_dblNoOfFields;	
					++m_dblCurrentY;
				}	
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintCopyright.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objField = null;
			}
		}
		/// <summary>
		/// This method would print the outline of the report.
		/// </summary>
		internal void PrintRectangle()
		{
			FieldCollection  objField=null;			
			try
			{
				objField = m_objExecSummReport.Fields;			
				if(m_bFirstPage)
				{
					m_bFirstPage = false;
					objField.Add("Rect3" + m_dblNoOfFields, "", 0,m_dblCurrentY*m_iLineSpace, m_objReportLayout.Width,11750);				
					objField["Rect3" + m_dblNoOfFields].BorderStyle = BorderStyleEnum.Solid;
					objField["Rect3" + m_dblNoOfFields].Section = 0;
					objField["Rect3" + m_dblNoOfFields].Visible  = true;				
				}
			}			
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("PrintPDF.PrintRectangle.ErrorPrint", m_iClientID), p_objException);
			}
			finally
			{
				objField = null;

			}
		}						
		
	}		
}
