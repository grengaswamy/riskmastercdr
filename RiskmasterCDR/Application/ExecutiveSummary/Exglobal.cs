using System;

namespace Riskmaster.Application.ExecutiveSummaryReport
{
    /// <summary>	
    /// Author  :  Neelima Dabral
    /// Date    :  05 October 2004	
    ///</summary>


    /// <summary>	
    /// Class contains the constants for identifying the type of data that would be printed on the report.
    ///</summary>
    internal class GlobalExecSummConstant
    {
        internal const int MONEY = 1;
        internal const int CHARSTRING = 2;
        internal const int DATESTRING = 3;
        internal const int NUMERIC = 4;
        internal const int YESNO = 5;
        internal const int TIMESTRING = 7;
    }

    /// <summary>	
    /// This structure would contain what all information regarding the Event needs to be displayed.
    ///</summary>
    internal struct EventType
    {
        private bool m_bInfo;
        private bool m_bDetail;
        private bool m_bDatedText;
        private bool m_bReported;
        private bool m_bFollow;
        private bool m_bOSHA;
        private bool m_bPersons;
        private bool m_bclaim;
        private bool m_bFall;
        private bool m_bMedwatch;
        private bool m_bEquipment;
        private bool m_bMedication;
        private bool m_bCon;
        private bool m_bTest;
        private bool m_bDiary;
        private bool m_bSupp;
        private bool m_bNotes;
        private bool m_bPatients;
        private bool m_bPhysicians;
        private bool m_bDrivers; //Aman Driver Enh
        private bool m_bMedStaff;
        private bool m_bOthPersons;
        private bool m_bQM;
        private bool m_bIntervention;
        private bool m_bEnhancedNotes;
        //pmittal5 MITS 12293 05/22/08  -Employees & Witness involved info
        private bool m_bEmployees;
        private bool m_bWitness;
        //Changed by Gagan for MITs 12824 : Start
        private bool m_bActivityDate;
        private bool m_bDateCreated;
        private bool m_bEnteredBy;
        private bool m_bNoteType;
        private bool m_bUserType;
        private bool m_bNoteText;
        private bool m_bSubject; //zmohammad MITS 31048
        //Changed by Gagan for MITs 12824 : End

        //rupal:start, r8 enh to include injury loss in exe summary,MITS 26359
        private bool m_bEmployeesInjuryLoss;
        private bool m_bPatientInjuryLoss;
        private bool m_bDriverInjuryLoss; //Aman Driver Enh
        private bool m_bPhysiciansInjuryLoss;
        private bool m_bMedStaffInjuryLoss;
        private bool m_bWitnessInjuryLoss;
        private bool m_bOTherPersonInjuryLoss;
        //rupal:end
        //start:rupal, r8 enh to show case mgt info,MITS 26359
        private bool m_bCaseManagementInfo;
        private bool m_bCaseManagerInfo;
        private bool m_bCaseManagerNotes;
        private bool m_bRestrictedWork;
        private bool m_bWorkLoss;
        private bool m_bTreatmentPlan;
        private bool m_Medical;
        private bool m_bAccommodation;
        private bool m_bVocational;
        //asingh263 mits 30541
        private bool m_bHideTaxIdSSN;
        //asingh263 mits 30541
        //end:rupal

        internal bool Info { get { return m_bInfo; } set { m_bInfo = value; } }
        internal bool Detail { get { return m_bDetail; } set { m_bDetail = value; } }
        internal bool DatedText { get { return m_bDatedText; } set { m_bDatedText = value; } }
        internal bool Reported { get { return m_bReported; } set { m_bReported = value; } }
        internal bool Follow { get { return m_bFollow; } set { m_bFollow = value; } }
        internal bool OSHA { get { return m_bOSHA; } set { m_bOSHA = value; } }
        internal bool Persons { get { return m_bPersons; } set { m_bPersons = value; } }
        internal bool Claim { get { return m_bclaim; } set { m_bclaim = value; } }
        internal bool Fall { get { return m_bFall; } set { m_bFall = value; } }
        internal bool Medwatch { get { return m_bMedwatch; } set { m_bMedwatch = value; } }
        internal bool Equipment { get { return m_bEquipment; } set { m_bEquipment = value; } }
        internal bool Medication { get { return m_bMedication; } set { m_bMedication = value; } }
        internal bool Con { get { return m_bCon; } set { m_bCon = value; } }
        internal bool Test { get { return m_bTest; } set { m_bTest = value; } }
        internal bool Diary { get { return m_bDiary; } set { m_bDiary = value; } }
        internal bool Supp { get { return m_bSupp; } set { m_bSupp = value; } }
        internal bool Notes { get { return m_bNotes; } set { m_bNotes = value; } }
        internal bool Patients { get { return m_bPatients; } set { m_bPatients = value; } }
        internal bool Drivers { get { return m_bDrivers;} set { m_bDrivers = value ; } }  //Aman Driver Enh
        internal bool Physicians { get { return m_bPhysicians; } set { m_bPhysicians = value; } }
        internal bool MedStaff { get { return m_bMedStaff; } set { m_bMedStaff = value; } }
        internal bool OthPersons { get { return m_bOthPersons; } set { m_bOthPersons = value; } }
        internal bool QM { get { return m_bQM; } set { m_bQM = value; } }
        internal bool Intervention { get { return m_bIntervention; } set { m_bIntervention = value; } }
        internal bool EnhancedNotes { get { return m_bEnhancedNotes; } set { m_bEnhancedNotes = value; } }
        //pmittal5 MITS 12293 05/22/08
        internal bool Employees { get { return m_bEmployees; } set { m_bEmployees = value; } }
        internal bool Witness { get { return m_bWitness; } set { m_bWitness = value; } }
        //Changed by Gagan for MITs 12824 : Start
        internal bool ActivityDate { get { return m_bActivityDate; } set { m_bActivityDate = value; } }
        internal bool DateCreated { get { return m_bDateCreated; } set { m_bDateCreated = value; } }
        internal bool EnteredBy { get { return m_bEnteredBy; } set { m_bEnteredBy = value; } }
        internal bool NoteType { get { return m_bNoteType; } set { m_bNoteType = value; } }
        internal bool UserType { get { return m_bUserType; } set { m_bUserType = value; } }
        internal bool NoteText { get { return m_bNoteText; } set { m_bNoteText = value; } }
        internal bool Subject { get { return m_bSubject; } set { m_bSubject = value; } } //zmohammad MITS 31048
        //Changed by Gagan for MITs 12824 : End
        //rupal:start, r8 enh to include injury loss,MITS 26359
        internal bool EmployeesInjuryLoss { get { return m_bEmployeesInjuryLoss; } set { m_bEmployeesInjuryLoss = value; } }
        internal bool PatientInjuryLoss { get { return m_bPatientInjuryLoss; } set { m_bPatientInjuryLoss = value; } }
        internal bool DriverInjuryLoss { get { return m_bDriverInjuryLoss; } set { m_bDriverInjuryLoss = value; } }  //Amab Driver Enh
        internal bool PhysiciansInjuryLoss { get { return m_bPhysiciansInjuryLoss; } set { m_bPhysiciansInjuryLoss = value; } }
        internal bool MedStaffInjuryLoss { get { return m_bMedStaffInjuryLoss; } set { m_bMedStaffInjuryLoss = value; } }
        internal bool WitnessInjuryLoss { get { return m_bWitnessInjuryLoss; } set { m_bWitnessInjuryLoss = value; } }
        internal bool OTherPersonInjuryLoss { get { return m_bOTherPersonInjuryLoss; } set { m_bOTherPersonInjuryLoss = value; } }        
        //rupal:end
        //start:rupal, r8 enh to show case mgt info for event,MITS 26359
        internal bool CaseManagementInfo { get { return m_bCaseManagementInfo; } set { m_bCaseManagementInfo = value; } }
        internal bool CaseManagerInfo { get { return m_bCaseManagerInfo; } set { m_bCaseManagerInfo = value; } }
        internal bool CaseManagerNotes { get { return m_bCaseManagerNotes; } set { m_bCaseManagerNotes = value; } }
        internal bool RestrictedWork { get { return m_bRestrictedWork; } set { m_bRestrictedWork = value; } }
        internal bool WorkLoss { get { return m_bWorkLoss; } set { m_bWorkLoss = value; } }
        internal bool TreatmentPlan { get { return m_bTreatmentPlan; } set { m_bTreatmentPlan = value; } }
        internal bool Medical { get { return m_Medical; } set { m_Medical = value; } }
        internal bool Accommodation { get { return m_bAccommodation; } set { m_bAccommodation = value; } }
        internal bool Vocational { get { return m_bVocational; } set { m_bVocational = value; } }
        //end:rupal
        //asingh263 MITS 30541 starts
        internal bool HideTaxIdSSN { get { return m_bHideTaxIdSSN; } set { m_bHideTaxIdSSN = value; } }
        //asingh263 MITS 30541 ends
    }

    /// <summary>	
    /// This structure would contain what all information needs to be displayed for both the Claim/Event report.
    ///</summary>
    internal struct AllReportsType
    {
        private bool m_bSupp;
        private bool m_bNotes;
        private bool m_bEnhancedNotes;
        internal bool Supp { get { return m_bSupp; } set { m_bSupp = value; } }
        internal bool Notes { get { return m_bNotes; } set { m_bNotes = value; } }
        internal bool EnhancedNotes { get { return m_bEnhancedNotes; } set { m_bEnhancedNotes = value; } }
    }
    /// <summary>	
    /// This structure would contain what all information regarding any Claim needs to be displayed.
    ///</summary>
    internal struct AllClaimsType
    {
        private bool m_bInfo;
        //Changed by Gagan for MITS 19120 : Start
        //added by rkaur7
        //private bool m_bpolicy;
        //Changed by Gagan for MITS 19120 : End
        private bool m_bClReserves;
        private bool m_bPayHist;
        private bool m_bFunds;
        private bool m_bTransDetail;
        private bool m_bEvent;
        private bool m_bPersons;
        private bool m_bAdjuster;
        private bool m_bAdjDatedText;
        private bool m_bLit;
        private bool m_bLitAtt;
        private bool m_bLitExpert;
        private bool m_bLitDemandOffer;
        private bool m_bSubro;
        private bool m_bSubroDemandOffer;
        private bool m_bDefend;
        private bool m_bDefATT;
        private bool m_bDefClaimInv;
        private bool m_bDiary;
        private bool m_bSupp;
        private bool m_bNotes;
        private bool m_bFullName;
        private bool m_bEnhancedNotes;
        //Changed by Gagan for MITs 12824 : Start
        private bool m_bActivityDate;
        private bool m_bDateCreated;
        private bool m_bEnteredBy;
        private bool m_bNoteType;
        private bool m_bUserType;
        private bool m_bNoteText;
        private bool m_bSubject; //zmohammad MITS 31048
        //Changed by Gagan for MITs 12824 : End
        //skhare7 R8 Enhancement
        private bool m_bSubDetails;
        private bool m_bArbDetails;
        private bool m_bLiabDetails;
        //asingh263 mits 30541
        private bool m_bACHideTaxIdSSN;
        //asingh263 mits 30541

        //private bool m_bClaimantDemandOffer;    //added by swati MITs # 35363

        internal bool Info { get { return m_bInfo; } set { m_bInfo = value; } }
        //Changed by Gagan for MITS 19120 : Start
        //added by rkaur7
        //internal bool PolicyState { get { return m_bpolicy; } set { m_bpolicy = value; } }
        //Changed by Gagan for MITS 19120 : End
        internal bool ClReserves { get { return m_bClReserves; } set { m_bClReserves = value; } }
        internal bool PayHist { get { return m_bPayHist; } set { m_bPayHist = value; } }
        internal bool Funds { get { return m_bFunds; } set { m_bFunds = value; } }
        internal bool TransDetail { get { return m_bTransDetail; } set { m_bTransDetail = value; } }
        internal bool Event { get { return m_bEvent; } set { m_bEvent = value; } }
        internal bool Persons { get { return m_bPersons; } set { m_bPersons = value; } }
        internal bool Adjuster { get { return m_bAdjuster; } set { m_bAdjuster = value; } }
        internal bool AdjDatedText { get { return m_bAdjDatedText; } set { m_bAdjDatedText = value; } }
        internal bool Lit { get { return m_bLit; } set { m_bLit = value; } }
        internal bool LitAtt { get { return m_bLitAtt; } set { m_bLitAtt = value; } }
        internal bool LitExpert { get { return m_bLitExpert; } set { m_bLitExpert = value; } }

        internal bool LitDemandOffer { get { return m_bLitDemandOffer; } set { m_bLitDemandOffer = value; } }
        internal bool Subro { get { return m_bSubro; } set { m_bSubro = value; } }
        internal bool SubroDemandOffer { get { return m_bSubroDemandOffer; } set { m_bSubroDemandOffer = value; } }

        //added by swati MITS # 35363
        //internal bool ClaimantDemandOffer { get { return m_bClaimantDemandOffer; } set { m_bClaimantDemandOffer = value; } }
        //change end here by swati

        internal bool Defend { get { return m_bDefend; } set { m_bDefend = value; } }
        internal bool DefATT { get { return m_bDefATT; } set { m_bDefATT = value; } }
        internal bool DefClaimInv { get { return m_bDefClaimInv; } set { m_bDefClaimInv = value; } }
        internal bool Diary { get { return m_bDiary; } set { m_bDiary = value; } }
        internal bool Supp { get { return m_bSupp; } set { m_bSupp = value; } }
        internal bool Notes { get { return m_bNotes; } set { m_bNotes = value; } }
        internal bool FullName { get { return m_bFullName; } set { m_bFullName = value; } }
        internal bool EnhancedNotes { get { return m_bEnhancedNotes; } set { m_bEnhancedNotes = value; } }
        //Changed by Gagan for MITs 12824 : Start
        internal bool ActivityDate { get { return m_bActivityDate; } set { m_bActivityDate = value; } }
        internal bool DateCreated { get { return m_bDateCreated; } set { m_bDateCreated = value; } }
        internal bool EnteredBy { get { return m_bEnteredBy; } set { m_bEnteredBy = value; } }
        internal bool NoteType { get { return m_bNoteType; } set { m_bNoteType = value; } }
        internal bool UserType { get { return m_bUserType; } set { m_bUserType = value; } }
        internal bool NoteText { get { return m_bNoteText; } set { m_bNoteText = value; } }
        internal bool Subject { get { return m_bSubject; } set { m_bSubject = value; } } //zmohammad MITS 31048
        //Changed by Gagan for MITs 12824 : End
        //skhare7 R8 enhancements
        internal bool Subrogation { get { return m_bSubDetails; } set { m_bSubDetails = value; } }
        internal bool Liability { get { return m_bLiabDetails; } set { m_bLiabDetails = value; } }
        internal bool Arbitration { get { return m_bArbDetails; } set { m_bArbDetails = value; } }
        //asingh263 MITS 30541 starts
        internal bool ACHideTaxIdSSN { get { return m_bACHideTaxIdSSN; } set { m_bACHideTaxIdSSN = value; } }
        //asingh263 MITS 30541 ends
    }

    /// <summary>	
    /// This structure would contain what all information regarding the Claimant needs to be displayed.
    ///</summary>
    internal struct ClaimantInfoType
    {
        private bool m_bInfo; 
        private bool m_bAtt;
        private bool m_bInvHist;
        private bool m_bReserves;
        private bool m_bPay;
        private bool m_bFunds;
        private bool m_bTrans;
        //skhare7 R8 
        private bool m_bPropLoss;
        private bool m_bUnitLoss;
        private bool m_bPropSalvage;
        private bool m_bUnitSalvage;

        //25743 Starts

       

        //AllClaims
        private bool m_bACInfo;
      
        private bool m_bClReserves;
        private bool m_bPayHist;
        private bool m_bACFunds;
        private bool m_bTransDetail;
        private bool m_bEvent;
        private bool m_bACPersons;
        private bool m_bAdjuster;
        private bool m_bAdjDatedText;
        private bool m_bLit;
        private bool m_bLitAtt;
        private bool m_bLitExpert;
        private bool m_bLitDemandOffer;
        private bool m_bSubro;
        private bool m_bSubroDemandOffer;
        private bool m_bDefend;
        private bool m_bDefATT;
        private bool m_bDefClaimInv;
        private bool m_bACDiary;
        private bool m_bACSupp;
        private bool m_bACNotes;
        private bool m_bFullName;
        private bool m_bACEnhancedNotes;
     
        private bool m_bACActivityDate;
        private bool m_bACDateCreated;
        private bool m_bACEnteredBy;
        private bool m_bACNoteType;
        private bool m_bACUserType;
        private bool m_bACNoteText;
        private bool m_bACSubject; // zmohammad MITS 31048
        private bool m_bSubDetails;
        private bool m_bArbDetails;
        private bool m_bLiabDetails;
        private bool m_bUnitInfo;
       
        //25743 Ends
		
		private bool m_bClaimantDemandOffer;    //added by swati MITs # 35363
        /*Added by gbindra MITS#34104*/
        private bool m_bClaimantEnhanceNotes;
        private bool m_bClaimantEnhanceNotesACDate;
        private bool m_bClaimantEnhanceNotesDtCreated;
        private bool m_bClaimantEnhanceNotesEntered;
        private bool m_bClaimantEnhanceNotesNoteType;
        private bool m_bClaimantEnhanceNotesUserType;
        private bool m_bClaimantEnhanceNotesNoteText;
        private bool m_bClaimantEnhanceNotesSubject;

        internal bool ClaimantEnhancedNotes { get { return m_bClaimantEnhanceNotes; } set { m_bClaimantEnhanceNotes = value; } }
        internal bool ClaimantEnhancedNotesACDate { get { return m_bClaimantEnhanceNotesACDate; } set { m_bClaimantEnhanceNotesACDate = value; } }
        internal bool ClaimantEnhancedNotesDtCreated { get { return m_bClaimantEnhanceNotesDtCreated; } set { m_bClaimantEnhanceNotesDtCreated = value; } }
        internal bool ClaimantEnhancedNotesEntered { get { return m_bClaimantEnhanceNotesEntered; } set { m_bClaimantEnhanceNotesEntered = value; } }
        internal bool ClaimantEnhancedNotesNoteType { get { return m_bClaimantEnhanceNotesNoteType; } set { m_bClaimantEnhanceNotesNoteType = value; } }
        internal bool ClaimantEnhancedNotesUserType { get { return m_bClaimantEnhanceNotesUserType; } set { m_bClaimantEnhanceNotesUserType = value; } }
        internal bool ClaimantEnhancedNotesNoteText { get { return m_bClaimantEnhanceNotesNoteText; } set { m_bClaimantEnhanceNotesNoteText = value; } }
        internal bool ClaimantEnhancedNotesSubject { get { return m_bClaimantEnhanceNotesSubject; } set { m_bClaimantEnhanceNotesSubject = value; } }

        /*Added by gbindra MITS#34104 END*/

        internal bool Info { get { return m_bInfo; } set { m_bInfo = value; } }
        internal bool Att { get { return m_bAtt; } set { m_bAtt = value; } }
        internal bool InvHist { get { return m_bInvHist; } set { m_bInvHist = value; } }
        internal bool Reserves { get { return m_bReserves; } set { m_bReserves = value; } }
        internal bool Pay { get { return m_bPay; } set { m_bPay = value; } }
        internal bool Funds { get { return m_bFunds; } set { m_bFunds = value; } }
        internal bool Trans { get { return m_bTrans; } set { m_bTrans = value; } }
        internal bool PropertyLoss { get { return m_bPropLoss; } set { m_bPropLoss = value; } }
        internal bool UnitLoss { get { return m_bUnitLoss; } set { m_bUnitLoss = value; } }
        internal bool PropertySalvage { get { return m_bUnitSalvage; } set { m_bUnitSalvage = value; } }
        internal bool UnitSalvage { get { return m_bPropSalvage; } set { m_bPropSalvage = value; } }
        
        //25743 Starts
        internal bool UnitInfo { get { return m_bUnitInfo; } set { m_bUnitInfo = value; } }

       

        //AllClaims

        internal bool ACInfo { get { return m_bACInfo; } set { m_bACInfo = value; } }
 
        internal bool ClReserves { get { return m_bClReserves; } set { m_bClReserves = value; } }
        internal bool PayHist { get { return m_bPayHist; } set { m_bPayHist = value; } }
        internal bool ACFunds { get { return m_bACFunds; } set { m_bACFunds = value; } }
        internal bool TransDetail { get { return m_bTransDetail; } set { m_bTransDetail = value; } }
        internal bool Event { get { return m_bEvent; } set { m_bEvent = value; } }
        internal bool ACPersons { get { return m_bACPersons; } set { m_bACPersons = value; } }
        internal bool Adjuster { get { return m_bAdjuster; } set { m_bAdjuster = value; } }
        internal bool AdjDatedText { get { return m_bAdjDatedText; } set { m_bAdjDatedText = value; } }
        internal bool Lit { get { return m_bLit; } set { m_bLit = value; } }
        internal bool LitAtt { get { return m_bLitAtt; } set { m_bLitAtt = value; } }
        internal bool LitExpert { get { return m_bLitExpert; } set { m_bLitExpert = value; } }

        internal bool LitDemandOffer { get { return m_bLitDemandOffer; } set { m_bLitDemandOffer = value; } }
        internal bool Subro { get { return m_bSubro; } set { m_bSubro = value; } }
        internal bool SubroDemandOffer { get { return m_bSubroDemandOffer; } set { m_bSubroDemandOffer = value; } }

        //added by swati MITS # 35363
        internal bool ClaimantDemandOffer { get { return m_bClaimantDemandOffer; } set { m_bClaimantDemandOffer = value; } }
        //change end here by swati

        internal bool Defend { get { return m_bDefend; } set { m_bDefend = value; } }
        internal bool DefATT { get { return m_bDefATT; } set { m_bDefATT = value; } }
        internal bool DefClaimInv { get { return m_bDefClaimInv; } set { m_bDefClaimInv = value; } }
        internal bool ACDiary { get { return m_bACDiary; } set { m_bACDiary = value; } }
        internal bool ACSupp { get { return m_bACSupp; } set { m_bACSupp = value; } }
        internal bool ACNotes { get { return m_bACNotes; } set { m_bACNotes = value; } }
        internal bool FullName { get { return m_bFullName; } set { m_bFullName = value; } }
        internal bool ACEnhancedNotes { get { return m_bACEnhancedNotes; } set { m_bACEnhancedNotes = value; } }
       
        internal bool ACActivityDate { get { return m_bACActivityDate; } set { m_bACActivityDate = value; } }
        internal bool ACDateCreated { get { return m_bACDateCreated; } set { m_bACDateCreated = value; } }
        internal bool ACEnteredBy { get { return m_bACEnteredBy; } set { m_bACEnteredBy = value; } }
        internal bool ACNoteType { get { return m_bACNoteType; } set { m_bACNoteType = value; } }
        internal bool ACUserType { get { return m_bACUserType; } set { m_bACUserType = value; } }
        internal bool ACNoteText { get { return m_bACNoteText; } set { m_bACNoteText = value; } }
        internal bool ACSubject { get { return m_bACSubject; } set { m_bACSubject = value; } } //zmohammad MITS 31048
        internal bool Subrogation { get { return m_bSubDetails; } set { m_bSubDetails = value; } }
        internal bool Liability { get { return m_bLiabDetails; } set { m_bLiabDetails = value; } }
        internal bool Arbitration { get { return m_bArbDetails; } set { m_bArbDetails = value; } }

        //25743 Ends



    }

    /// <summary>	
    /// This structure would contain what all information regarding the Claim (of type Worker's Compensation)
    /// needs to be displayed.
    ///</summary>
    internal struct WCClaimsType
    {
        private bool m_bEmployee;
        private bool m_bEmployment;
        private bool m_bDepend;
        private bool m_bRestrict;
        private bool m_bLost;
        private bool m_bEvent;
        private bool m_bInvHist;
        private bool m_bOSHA;
        private bool m_bJur;
        private bool m_bCaseManagementInfo;
        private bool m_bCaseManagerInfo;
        private bool m_bCaseManagerNotes;
        private bool m_bRestrictedWork;
        private bool m_bWorkLoss;
        private bool m_bTreatmentPlan;
        private bool m_Medical;
        private bool m_bAccommodation;
        private bool m_bVocational;
        //BOB Unit stat enhancements
        private bool m_bUnitStat;

        //25743 Starts

       
        //AllClaims
        private bool m_bACInfo;

        private bool m_bClReserves;
        private bool m_bPayHist;
        private bool m_bACFunds;
        private bool m_bTransDetail;
        private bool m_bACEvent;
        private bool m_bACPersons;
        private bool m_bAdjuster;
        private bool m_bAdjDatedText;
        private bool m_bLit;
        private bool m_bLitAtt;
        private bool m_bLitExpert;
        private bool m_bLitDemandOffer;
        private bool m_bSubro;
        private bool m_bSubroDemandOffer;
        private bool m_bDefend;
        private bool m_bDefATT;
        private bool m_bDefClaimInv;
        private bool m_bACDiary;
        private bool m_bACSupp;
        private bool m_bACNotes;
        private bool m_bFullName;
        private bool m_bACEnhancedNotes;

        private bool m_bACActivityDate;
        private bool m_bACDateCreated;
        private bool m_bACEnteredBy;
        private bool m_bACNoteType;
        private bool m_bACUserType;
        private bool m_bACNoteText;
        private bool m_bACSubject;  //zmohammad MITS 31048

        private bool m_bSubDetails;
        private bool m_bArbDetails;
        private bool m_bLiabDetails;

        //25743 Ends

        internal bool Employee { get { return m_bEmployee; } set { m_bEmployee = value; } }
        internal bool Employment { get { return m_bEmployment; } set { m_bEmployment = value; } }
        internal bool Depend { get { return m_bDepend; } set { m_bDepend = value; } }
        internal bool Restrict { get { return m_bRestrict; } set { m_bRestrict = value; } }
        internal bool Lost { get { return m_bLost; } set { m_bLost = value; } }
        internal bool Event { get { return m_bEvent; } set { m_bEvent = value; } }
        internal bool InvHist { get { return m_bInvHist; } set { m_bInvHist = value; } }
        internal bool OSHA { get { return m_bOSHA; } set { m_bOSHA = value; } }
        internal bool Jur { get { return m_bJur; } set { m_bJur = value; } }

        internal bool CaseManagementInfo { get { return m_bCaseManagementInfo; } set { m_bCaseManagementInfo = value; } }
        internal bool CaseManagerInfo { get { return m_bCaseManagerInfo; } set { m_bCaseManagerInfo = value; } }
        internal bool CaseManagerNotes { get { return m_bCaseManagerNotes; } set { m_bCaseManagerNotes = value; } }
        internal bool RestrictedWork { get { return m_bRestrictedWork; } set { m_bRestrictedWork = value; } }
        internal bool WorkLoss { get { return m_bWorkLoss; } set { m_bWorkLoss = value; } }
        internal bool TreatmentPlan { get { return m_bTreatmentPlan; } set { m_bTreatmentPlan = value; } }
        internal bool Medical { get { return m_Medical; } set { m_Medical = value; } }
        internal bool Accommodation { get { return m_bAccommodation; } set { m_bAccommodation = value; } }
        internal bool Vocational { get { return m_bVocational; } set { m_bVocational = value; } }
        //BOB Unit stat enhancements
        internal bool UnitStat { get { return m_bUnitStat; } set { m_bUnitStat = value; } }

        //25743 Starts

        //AllClaims

        internal bool ACInfo { get { return m_bACInfo; } set { m_bACInfo = value; } }

        internal bool ClReserves { get { return m_bClReserves; } set { m_bClReserves = value; } }
        internal bool PayHist { get { return m_bPayHist; } set { m_bPayHist = value; } }
        internal bool ACFunds { get { return m_bACFunds; } set { m_bACFunds = value; } }
        internal bool TransDetail { get { return m_bTransDetail; } set { m_bTransDetail = value; } }
        internal bool ACEvent { get { return m_bACEvent; } set { m_bACEvent = value; } }
        internal bool ACPersons { get { return m_bACPersons; } set { m_bACPersons = value; } }
        internal bool Adjuster { get { return m_bAdjuster; } set { m_bAdjuster = value; } }
        internal bool AdjDatedText { get { return m_bAdjDatedText; } set { m_bAdjDatedText = value; } }
        internal bool Lit { get { return m_bLit; } set { m_bLit = value; } }
        internal bool LitAtt { get { return m_bLitAtt; } set { m_bLitAtt = value; } }
        internal bool LitExpert { get { return m_bLitExpert; } set { m_bLitExpert = value; } }

        internal bool LitDemandOffer { get { return m_bLitDemandOffer; } set { m_bLitDemandOffer = value; } }
        internal bool Subro { get { return m_bSubro; } set { m_bSubro = value; } }
        internal bool SubroDemandOffer { get { return m_bSubroDemandOffer; } set { m_bSubroDemandOffer = value; } }

        internal bool Defend { get { return m_bDefend; } set { m_bDefend = value; } }
        internal bool DefATT { get { return m_bDefATT; } set { m_bDefATT = value; } }
        internal bool DefClaimInv { get { return m_bDefClaimInv; } set { m_bDefClaimInv = value; } }
        internal bool ACDiary { get { return m_bACDiary; } set { m_bACDiary = value; } }
        internal bool ACSupp { get { return m_bACSupp; } set { m_bACSupp = value; } }
        internal bool ACNotes { get { return m_bACNotes; } set { m_bACNotes = value; } }
        internal bool FullName { get { return m_bFullName; } set { m_bFullName = value; } }
        internal bool ACEnhancedNotes { get { return m_bACEnhancedNotes; } set { m_bACEnhancedNotes = value; } }

        internal bool ACActivityDate { get { return m_bACActivityDate; } set { m_bACActivityDate = value; } }
        internal bool ACDateCreated { get { return m_bACDateCreated; } set { m_bACDateCreated = value; } }
        internal bool ACEnteredBy { get { return m_bACEnteredBy; } set { m_bACEnteredBy = value; } }
        internal bool ACNoteType { get { return m_bACNoteType; } set { m_bACNoteType = value; } }
        internal bool ACUserType { get { return m_bACUserType; } set { m_bACUserType = value; } }
        internal bool ACNoteText { get { return m_bACNoteText; } set { m_bACNoteText = value; } }
        internal bool ACSubject { get { return m_bACSubject; } set { m_bACSubject = value; } }  //zmohammad MITS 31048

        internal bool Subrogation { get { return m_bSubDetails; } set { m_bSubDetails = value; } }
        internal bool Liability { get { return m_bLiabDetails; } set { m_bLiabDetails = value; } }
        internal bool Arbitration { get { return m_bArbDetails; } set { m_bArbDetails = value; } }

        //25743 Ends
    }

    /// <summary>	
    /// This structure would contain what all information regarding the Claim (of type Vehicle Accident)
    /// needs to be displayed.
    ///</summary>
    internal struct VAClaimsType
    {

        internal ClaimantInfoType m_structClaimantInfoType;
        private bool m_bAccident;
        private bool m_bUnit;
        private bool m_bInvHist;
        private bool m_bReserves;
        private bool m_bPay;
        private bool m_bFunds;
        //private bool m_bTrans;
        private bool m_bTransUnitLevel;
        //skhare7 r8 enhancement
        private bool m_bVASalvage;
        private bool m_bUnitInfo;
		private bool m_bClaimantDemandOffer;    //added by swati MITs # 35363
        /*Added by gbindra MITS#34104*/
        private bool m_bClaimantEnhanceNotes;
        private bool m_bClaimantEnhanceNotesACDate;
        private bool m_bClaimantEnhanceNotesDtCreated;
        private bool m_bClaimantEnhanceNotesEntered;
        private bool m_bClaimantEnhanceNotesNoteType;
        private bool m_bClaimantEnhanceNotesUserType;
        private bool m_bClaimantEnhanceNotesNoteText;
        private bool m_bClaimantEnhanceNotesSubject;

        internal bool ClaimantEnhancedNotes { get { return m_bClaimantEnhanceNotes; } set { m_bClaimantEnhanceNotes = value; } }
        internal bool ClaimantEnhancedNotesACDate { get { return m_bClaimantEnhanceNotesACDate; } set { m_bClaimantEnhanceNotesACDate = value; } }
        internal bool ClaimantEnhancedNotesDtCreated { get { return m_bClaimantEnhanceNotesDtCreated; } set { m_bClaimantEnhanceNotesDtCreated = value; } }
        internal bool ClaimantEnhancedNotesEntered { get { return m_bClaimantEnhanceNotesEntered; } set { m_bClaimantEnhanceNotesEntered = value; } }
        internal bool ClaimantEnhancedNotesNoteType { get { return m_bClaimantEnhanceNotesNoteType; } set { m_bClaimantEnhanceNotesNoteType = value; } }
        internal bool ClaimantEnhancedNotesUserType { get { return m_bClaimantEnhanceNotesUserType; } set { m_bClaimantEnhanceNotesUserType = value; } }
        internal bool ClaimantEnhancedNotesNoteText { get { return m_bClaimantEnhanceNotesNoteText; } set { m_bClaimantEnhanceNotesNoteText = value; } }
        internal bool ClaimantEnhancedNotesSubject { get { return m_bClaimantEnhanceNotesSubject; } set { m_bClaimantEnhanceNotesSubject = value; } }

        /*Added by gbindra MITS#34104 END*/

        internal bool Accident { get { return m_bAccident; } set { m_bAccident = value; } }
        internal bool Unit { get { return m_bUnit; } set { m_bUnit = value; } }
        internal bool InvHist { get { return m_bInvHist; } set { m_bInvHist = value; } }
        internal bool Reserves { get { return m_bReserves; } set { m_bReserves = value; } }
        internal bool Pay { get { return m_bPay; } set { m_bPay = value; } }
        internal bool Funds { get { return m_bFunds; } set { m_bFunds = value; } }
        //pmittal5  MITS:12478  06/16/08
        //internal bool Trans{get{return m_bTrans;}set{m_bTrans= value;} }
        internal bool TransUnitLevel { get { return m_bTransUnitLevel; } set { m_bTransUnitLevel = value; } }
        //skhare7 R8 enhancement
        internal bool VASalvage { get { return m_bVASalvage; } set { m_bVASalvage = value; } }

        internal bool UnitInfo { get { return m_bUnitInfo; } set { m_bUnitInfo = value; } }
        //added by swati MITS # 35363
        internal bool ClaimantDemandOffer { get { return m_bClaimantDemandOffer; } set { m_bClaimantDemandOffer = value; } }
        //change end here by swati

    }

    /// <summary>	
    /// This structure would contain what all information regarding the Claim (of type DI)
    /// needs to be displayed.
    ///</summary>
    internal struct DIClaimsType
    {
        private bool m_bEmployeeInfo;
        private bool m_bEmploymentInfo;
        private bool m_bDependentInfo;
        private bool m_bRestrictedDays;
        private bool m_bLostDays;
        private bool m_bEmployeeEventdetails;
        private bool m_bClaimInvolvmentHistory;
        private bool m_bPlanInfo;
        private bool m_bCaseManagementInfo;
        private bool m_bCaseManagerInfo;
        private bool m_bCaseManagerNotes;
        private bool m_bRestrictedWork;
        private bool m_bWorkLoss;
        private bool m_bTreatmentPlan;
        private bool m_Medical;
        private bool m_bAccommodation;
        private bool m_bVocational;
        //Geeta 02/22/07 : Leave Info Flag added FMLA Enhancement
        private bool m_bLeaveInfo;

        internal bool EmployeeInfo { get { return m_bEmployeeInfo; } set { m_bEmployeeInfo = value; } }
        internal bool EmploymentInfo { get { return m_bEmploymentInfo; } set { m_bEmploymentInfo = value; } }
        internal bool DependentInfo { get { return m_bDependentInfo; } set { m_bDependentInfo = value; } }
        internal bool RestrictedDays { get { return m_bRestrictedDays; } set { m_bRestrictedDays = value; } }
        internal bool LostDays { get { return m_bLostDays; } set { m_bLostDays = value; } }
        internal bool EmployeeEventdetails { get { return m_bEmployeeEventdetails; } set { m_bEmployeeEventdetails = value; } }
        internal bool ClaimInvolvmentHistory { get { return m_bClaimInvolvmentHistory; } set { m_bClaimInvolvmentHistory = value; } }
        internal bool PlanInfo { get { return m_bPlanInfo; } set { m_bPlanInfo = value; } }
        internal bool CaseManagementInfo { get { return m_bCaseManagementInfo; } set { m_bCaseManagementInfo = value; } }
        internal bool CaseManagerInfo { get { return m_bCaseManagerInfo; } set { m_bCaseManagerInfo = value; } }
        internal bool CaseManagerNotes { get { return m_bCaseManagerNotes; } set { m_bCaseManagerNotes = value; } }
        internal bool RestrictedWork { get { return m_bRestrictedWork; } set { m_bRestrictedWork = value; } }
        internal bool WorkLoss { get { return m_bWorkLoss; } set { m_bWorkLoss = value; } }
        internal bool TreatmentPlan { get { return m_bTreatmentPlan; } set { m_bTreatmentPlan = value; } }
        internal bool Medical { get { return m_Medical; } set { m_Medical = value; } }
        internal bool Accommodation { get { return m_bAccommodation; } set { m_bAccommodation = value; } }
        internal bool Vocational { get { return m_bVocational; } set { m_bVocational = value; } }
        //Geeta 02/22/07 : Leave Info Flag added FMLA Enhancement
        internal bool LeaveInfo { get { return m_bLeaveInfo; } set { m_bLeaveInfo = value; } }
    }

    /// <summary>	
    /// This structure would contain data to be displayed on the report.
    /// Along with the data other information like data type, font style etc is also stored in this structure.
    ///</summary>
    internal struct ReportData
    {
        private string sReportColData;
        private int iReportColDataType;
        private bool bSetBold;
        private bool bRightAlign;
        internal string Data { get { return sReportColData; } set { sReportColData = value; } }
        internal int DataType { get { return iReportColDataType; } set { iReportColDataType = value; } }
        internal bool SetBold { get { return bSetBold; } set { bSetBold = value; } }
        internal bool RightAlign { get { return bRightAlign; } set { bRightAlign = value; } }
    }

    /// <summary>
    /// This structure will contain the event related details.
    /// </summary>
    internal struct EventDetail
    {
        internal string COUNTY_OF_INJURY;
        internal string EVENT_NUMBER;
        internal string EVENT_DESCRIPTION;
        internal string BRIEF_DESCRIPTION;
        internal string ADDR1;
        internal string ADDR2;
        internal string ADDR3;
        internal string ADDR4;
        internal string CITY;
        internal string ZIP_CODE;
        internal string LOCATION_AREA_DESC;
        internal string DATE_OF_EVENT;
        internal string TIME_OF_EVENT;
        internal string DATE_REPORTED;
        internal string TIME_REPORTED;
        internal string DATE_TO_FOLLOW_UP;
        internal string DATE_PHYS_ADVISED;
        internal string TIME_PHYS_ADVISED;
        internal string PHYS_NOTES;
        internal string DATE_CARRIER_NOTIF;
        internal string COMMENTS;

        internal int EVENT_TYPE_CODE;
        internal int EVENT_STATUS_CODE;
        internal int EVENT_IND_CODE;
        internal int DEPT_EID;
        internal int DEPT_INVOLVED_EID;
        internal int STATE_ID;
        internal int COUNTRY_CODE;
        internal int LOCATION_TYPE_CODE;
        internal int ON_PREMISE_FLAG;
        internal int NO_OF_FATALITIES;
        internal int NO_OF_INJURIES;
        internal int CAUSE_CODE;
        internal int RPTD_BY_EID;
        internal int TREATMENT_GIVEN;
        internal int RELEASE_SIGNED;
        internal int DEPT_HEAD_ADVISED;
        internal int PRIMARY_LOC_CODE;
        internal int ACCOUNT_ID;
        internal int INT_REQ_FLAG;               //pmittal5 MITS 11502 3/24/2008
        internal string DTTM_RCD_LAST_UPD;          //akaur9 Mobile Adjuster
    }

    /// <summary>
    /// This structure will contain claim related details.
    /// </summary>
    internal struct ClaimDetail
    {
        internal string CLAIM_NUMBER;
        internal int CLAIM_STATUS_CODE;
        internal int LINE_OF_BUS_CODE;
        internal int CLAIM_TYPE_CODE;
        internal string COMMENTS;
        internal string DTTM_CLOSED;
        internal string DATE_OF_CLAIM;
        internal string TIME_OF_CLAIM;
        internal string FILE_NUMBER;
        internal int METHOD_CLOSED_CODE;
        internal int PAYMNT_FROZEN_FLAG;
        internal int PRIMARY_POLICY_ID;
        internal int SERVICE_CODE;
        internal int RESTRICTED_CLAIM_FLAG; //Added by Swati Agarwal MITS # 34909 Gap 10
        internal int ACCIDENT_DESC_CODE;
        internal int ACCIDENT_TYPE_CODE;
        internal string DATE_FDDOT_RPT;
        internal string DATE_STDOT_RPT;
        internal int IN_TRAFFIC_FLAG;
        internal int PREVENTABLE_FLAG;
        internal int REPORTABLE_FLAG;
        internal string ST_DOT_RPT_ID;
        internal double EST_COLLECTION;
        internal int DURATION;
        internal int FILING_STATE_ID;
        internal int POLICE_AGENCY_EID;   //pmittal5 MITS 11663 3/18/2008
        internal string DATE_RPTD_TO_RM;
        internal string DTTM_RCD_LAST_UPD;   //akaur9 Mobile Adjuster
        internal string LOSS_DESCRIPTION;   //Aman Mobile Adjuster MITS 33658
        internal int CATASTROPHE_CODE; //averma62
        internal int CATASTROPHE_ROW_ID; //averma62
        internal int INSURED_CLAIM_DEPT_EID; //Added by Nikhil on 09/19/2014
        internal string MAIL_ACCORD_TO;// added by sbhatnagar21 jira RMA-9239
        internal int POLICY_LOB;// added by sbhatnagar21 jira RMA-9239
        internal int JURISDICTION;// added by sbhatnagar21 jira RMA-9239
        internal int LSS_CLAIM_FLAG;// added by sbhatnagar21 jira RMA-9239
	}


    //pmittal5 MITS 11458 2/29/2008 -Start
    /// <summary>
    /// This structure will contain case management information.
    /// </summary>
    internal struct RMParms
    {
        internal int bWorkMon;
        internal int bWorkTue;
        internal int bWorkWed;
        internal int bWorkThu;
        internal int bWorkFri;
        internal int bWorkSat;
        internal int bWorkSun;
        internal int bInclHolidays;
    }

    /// <summary>
    /// This structure will contain Holiday related information.
    /// </summary>
    internal struct Holiday
    {
        internal string sDate;
        internal string sDesc;
    }
    //pmittal5 MITS 11458 2/29/2008 -End

    //Sumit - MITS# 18145- 10/21/2009 - Start
    /// <summary>	
    /// This structure will contain Property Claim related information.
    ///</summary>
    internal struct PCClaimsType
    {

        internal ClaimantInfoType m_structClaimantInfoType;
        //smahajan6 - MITS# 18230 - 12/30/2009 :Start
        private bool m_bPropertyInformation;
        private bool m_bCOPEData;
        private bool m_bOptionalCOPEData;
        private bool m_bSchedule;
		
		private bool m_bClaimantDemandOffer;    //added by swati MITs # 35363

        /*Added by gbindra MITS#34104*/
        private bool m_bClaimantEnhanceNotes;
        private bool m_bClaimantEnhanceNotesACDate;
        private bool m_bClaimantEnhanceNotesDtCreated;
        private bool m_bClaimantEnhanceNotesEntered;
        private bool m_bClaimantEnhanceNotesNoteType;
        private bool m_bClaimantEnhanceNotesUserType;
        private bool m_bClaimantEnhanceNotesNoteText;
        private bool m_bClaimantEnhanceNotesSubject;

        internal bool ClaimantEnhancedNotes { get { return m_bClaimantEnhanceNotes; } set { m_bClaimantEnhanceNotes = value; } }
        internal bool ClaimantEnhancedNotesACDate { get { return m_bClaimantEnhanceNotesACDate; } set { m_bClaimantEnhanceNotesACDate = value; } }
        internal bool ClaimantEnhancedNotesDtCreated { get { return m_bClaimantEnhanceNotesDtCreated; } set { m_bClaimantEnhanceNotesDtCreated = value; } }
        internal bool ClaimantEnhancedNotesEntered { get { return m_bClaimantEnhanceNotesEntered; } set { m_bClaimantEnhanceNotesEntered = value; } }
        internal bool ClaimantEnhancedNotesNoteType { get { return m_bClaimantEnhanceNotesNoteType; } set { m_bClaimantEnhanceNotesNoteType = value; } }
        internal bool ClaimantEnhancedNotesUserType { get { return m_bClaimantEnhanceNotesUserType; } set { m_bClaimantEnhanceNotesUserType = value; } }
        internal bool ClaimantEnhancedNotesNoteText { get { return m_bClaimantEnhanceNotesNoteText; } set { m_bClaimantEnhanceNotesNoteText = value; } }
        internal bool ClaimantEnhancedNotesSubject { get { return m_bClaimantEnhanceNotesSubject; } set { m_bClaimantEnhanceNotesSubject = value; } }

        /*Added by gbindra MITS#34104 END*/
        internal bool PropertyInformation { get { return m_bPropertyInformation; } set { m_bPropertyInformation = value; } }
        internal bool COPEData { get { return m_bCOPEData; } set { m_bCOPEData = value; } }
        internal bool OptionalCOPEData { get { return m_bOptionalCOPEData; } set { m_bOptionalCOPEData = value; } }
        internal bool Schedule { get { return m_bSchedule; } set { m_bSchedule = value; } }
        //smahajan6 - MITS# 18230 - 12/30/2009 :End
        //added by swati MITS # 35363
        internal bool ClaimantDemandOffer { get { return m_bClaimantDemandOffer; } set { m_bClaimantDemandOffer = value; } }
        //change end here by swati
    }
    //Sumit - MITS#18145- 10/21/2009 - End

    /// <summary>	
    /// Sumit - 02/09/2008 - MITS 13153
    /// This structure contains the policy related information to be displayed on the report
    ///</summary>
    internal struct PolicyType
    {
        private bool m_bPolicyInfo;
        private bool m_bPolicyNum;
        private bool m_bEffecDate;
        private bool m_bExpiDate;
        private bool m_bClaimLimit;
        private bool m_bPolicyLimit;
        private bool m_bPercOfAgg;
        private bool m_bInsuredInfo;
        private bool m_bInsured;
        private bool m_bInsuredAddress;
        private bool m_bInsuredDept;
        private bool m_bInsuranceComp;
        //Changed by Gagan for MITS 19120 : Start
        private bool m_bPolicyState;
        //Changed by Gagan for MITS 19120 : End

        internal bool PolicyInfo { get { return m_bPolicyInfo; } set { m_bPolicyInfo = value; } }
        internal bool PolicyNum { get { return m_bPolicyNum; } set { m_bPolicyNum = value; } }
        internal bool EffecDate { get { return m_bEffecDate; } set { m_bEffecDate = value; } }
        internal bool ExpiDate { get { return m_bExpiDate; } set { m_bExpiDate = value; } }
        internal bool ClaimLimit { get { return m_bClaimLimit; } set { m_bClaimLimit = value; } }
        internal bool PolicyLimit { get { return m_bPolicyLimit; } set { m_bPolicyLimit = value; } }
        internal bool PercOfAgg { get { return m_bPercOfAgg; } set { m_bPercOfAgg = value; } }
        internal bool InsuredInfo { get { return m_bInsuredInfo; } set { m_bInsuredInfo = value; } }
        internal bool Insured { get { return m_bInsured; } set { m_bInsured = value; } }
        internal bool InsuredAddress { get { return m_bInsuredAddress; } set { m_bInsuredAddress = value; } }
        internal bool InsuredDept { get { return m_bInsuredDept; } set { m_bInsuredDept = value; } }
        internal bool InsuranceComp { get { return m_bInsuranceComp; } set { m_bInsuranceComp = value; } }
        //Changed by Gagan for MITS 19120 : Start
        internal bool PolicyState { get { return m_bPolicyState; } set { m_bPolicyState = value; } }
        //Changed by Gagan for MITS 19120 : End
    }


    /// <summary>	
    /// Gagan - 04/01/2010 - MITS 19197
    /// This structure contains the policy management related information to be displayed on the report
    ///</summary>
    internal struct PolicyMgmtType
    {
        private bool m_bPolicyInfo;
        private bool m_bPolicyName;
        private bool m_bPolicyNumber;
        private bool m_bPolicyStatus;
        private bool m_bPolicyType;
        private bool m_bPolicyState;
        private bool m_bEffecDate;
        private bool m_bExpiDate;
        private bool m_bTotalBilledPremium;
        private bool m_bPolicyInsurer;

        private bool m_bPolicyInsured;
        private bool m_bPolicyInsuredName;
        private bool m_bPolicyInsuredAddress;

        private bool m_bPolicyCvgs;
        private bool m_bPolicyCvgType;
        private bool m_bPolicyCvgDesc;
        private bool m_bPolicyLimit;
        private bool m_bClaimLimit;
        private bool m_bOccurrenceLimit;
        private bool m_bTotalPayment;

        private bool m_bPolicyExposure;
        private bool m_bExposureType;

        private bool m_bExposureDesc;
        private bool m_bExposureAmount;
        private bool m_bRate;
        private bool m_bBaseRate;


        internal bool PolicyInfo { get { return m_bPolicyInfo; } set { m_bPolicyInfo = value; } }
        internal bool PolicyName { get { return m_bPolicyName; } set { m_bPolicyName = value; } }
        internal bool PolicyNumber { get { return m_bPolicyNumber; } set { m_bPolicyNumber = value; } }
        internal bool PolicyStatus { get { return m_bPolicyStatus; } set { m_bPolicyStatus = value; } }
        internal bool PolicyType { get { return m_bPolicyType; } set { m_bPolicyType = value; } }
        internal bool PolicyState { get { return m_bPolicyState; } set { m_bPolicyState = value; } }
        internal bool EffecDate { get { return m_bEffecDate; } set { m_bEffecDate = value; } }
        internal bool ExpiDate { get { return m_bExpiDate; } set { m_bExpiDate = value; } }
        internal bool TotalBilledPremium { get { return m_bTotalBilledPremium; } set { m_bTotalBilledPremium = value; } }
        internal bool PolicyInsurer { get { return m_bPolicyInsurer; } set { m_bPolicyInsurer = value; } }
        internal bool PolicyInsured { get { return m_bPolicyInsured; } set { m_bPolicyInsured = value; } }
        internal bool PolicyInsuredName { get { return m_bPolicyInsuredName; } set { m_bPolicyInsuredName = value; } }
        internal bool PolicyInsuredAddress { get { return m_bPolicyInsuredAddress; } set { m_bPolicyInsuredAddress = value; } }


        internal bool PolicyCvgs { get { return m_bPolicyCvgs; } set { m_bPolicyCvgs = value; } }
        internal bool PolicyCvgType { get { return m_bPolicyCvgType; } set { m_bPolicyCvgType = value; } }
        internal bool PolicyCvgDesc { get { return m_bPolicyCvgDesc; } set { m_bPolicyCvgDesc = value; } }
        internal bool PolicyLimit { get { return m_bPolicyLimit; } set { m_bPolicyLimit = value; } }
        internal bool ClaimLimit { get { return m_bClaimLimit; } set { m_bClaimLimit = value; } }
        internal bool OccurrenceLimit { get { return m_bOccurrenceLimit; } set { m_bOccurrenceLimit = value; } }
        internal bool TotalPayment { get { return m_bTotalPayment; } set { m_bTotalPayment = value; } }
        internal bool PolicyExposure { get { return m_bPolicyExposure; } set { m_bPolicyExposure = value; } }
        internal bool ExposureType { get { return m_bExposureType; } set { m_bExposureType = value; } }
        internal bool ExposureDesc { get { return m_bExposureDesc; } set { m_bExposureDesc = value; } }
        internal bool ExposureAmount { get { return m_bExposureAmount; } set { m_bExposureAmount = value; } }
        internal bool Rate { get { return m_bRate; } set { m_bRate = value; } }
        internal bool BaseRate { get { return m_bBaseRate; } set { m_bBaseRate = value; } }




    }






















}
