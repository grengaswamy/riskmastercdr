using System;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Db;

namespace Riskmaster.Application.ExecutiveSummaryReport
{
	/// <summary>	
	/// Author  :  Neelima Dabral
	/// Date    :  05 October 2004
	/// Purpose :  Riskmaster.Application.ExecutiveSummary.Declarations class contains the common methods 
	///			   that would be used while generating the scheduled Executive Summary Reports.
	/// </summary>
	internal class Declarations
	{
		/// <summary>
		/// This method would return string value if the passed string is not null or not blank
		/// else it returns blank string.
		/// </summary>
		/// <param name="p_sAttribute">String</param>				
		/// <returns>Not-null string</returns>
		internal static string GetStrValFromXmlAttribute(string p_sAttribute, int p_iClientId)
		{
			string sRetVal="";
			try
			{
				if(p_sAttribute!=null && p_sAttribute!="")
						sRetVal=p_sAttribute; 				
			}
			catch(Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("Declarations.GetStrValFromXmlAttribute.ErrorGet", p_iClientId), p_objException);
			}
			return sRetVal;
		}
		/// <summary>
		/// This method would return a date after adding specified number of days/months/years.
		/// </summary>
		/// <param name="p_addFormat">d/m/yyyy</param>				
		/// <param name="p_iInterval">Interval</param>				
		/// <param name="p_datDate">Date to which specified interval is added</param>				
		/// <returns>Date</returns>
        internal static DateTime DateAdd(string p_addFormat, int p_iInterval, DateTime p_datDate, int p_iClientId)
		{
			DateTime datRetVal=p_datDate;
			try
			{
				switch(p_addFormat.ToLower())
				{
					case "d":
						datRetVal=p_datDate.AddDays(p_iInterval);
						break;
					case "m":
						datRetVal=p_datDate.AddMonths(p_iInterval);
						break;
					case "yyyy":
						datRetVal=p_datDate.AddYears(p_iInterval);
						break;
				}
			}
			catch(Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("Declarations.DateAdd.Error", p_iClientId), p_objException);
			}
			return datRetVal;
		}
	
		/// <summary>
		/// This method would return the difference in second between the current datetime and the datetime passed.
		/// </summary>
		/// <param name="p_datStart">Datetime</param>								
		/// <returns>Number of seconds</returns>
        internal static int DateDiff(DateTime p_datStart, int p_iClientId)
		{
			try
			{
				return ((TimeSpan)(DateTime.Now.Subtract(p_datStart))).Seconds;
			}
			catch(Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("Declarations.DateDiff.Error", p_iClientId), p_objException);
			}
		}
        //----------------------MITS 9205-Start-------------------------
        /// Name		: IsEnhPolEnabled
        /// Author		: Divya
        /// Date Created	: 03/21/2007		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Function will check whether the Billing System is enabled or not.
        /// </summary>
        /// <returns></returns>
        internal static bool IsEnhPolEnabled(string p_sConnectionString, int p_iClientId)
        {
            bool bRetVal = false;
            DbConnection objConn = null;
            DbReader objReader = null;
            string sQuery = string.Empty;
            try
            {
                sQuery = "SELECT USE_ENH_POL_FLAG FROM SYS_PARMS";

                objConn = DbFactory.GetDbConnection(p_sConnectionString);
                objConn.Open();
                objReader = objConn.ExecuteReader(sQuery);
                if (objReader.Read())
                {
                    bRetVal = Conversion.ConvertObjToBool(objReader.GetValue(0), p_iClientId);

                }

            }
            catch (Exception p_objException)
            {
                throw new DataModelException(Globalization.GetString("RMWinSecurity.IsBillingSysEnabled.IsBillingSysEnabledError",p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();

                }
            }
            return bRetVal;
        }

		/// <summary>
		/// This method will return the numeric part from the start of input string.
		/// If the string does not start with a number or does not have numeric
		/// value at all then this method returns 0.
		/// </summary>
		/// <param name="p_sInput">Input string</param>
		/// <returns>integer</returns>
		internal static int Val(string p_sInput, int p_iClientId)
		{
			int iReturnValue = 0;
			int iCnt = 0;
			string sTemp = "";
			try
			{

				if(p_sInput == null || p_sInput == "")
					return 0;

				for(iCnt=0;iCnt<p_sInput.Length;iCnt++)
				{
					if(Conversion.IsNumeric(p_sInput.Substring(iCnt,1)))
						sTemp = sTemp + p_sInput.Substring(iCnt,1);
					else
					{
						//If the first character is non-numeric then -1 is returned.
						if(iCnt == 0)
							sTemp = "0";
						//exit
						break;
					}
				}//end for
				iReturnValue = Conversion.ConvertStrToInteger(sTemp);
				return iReturnValue;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Declarations.Val.Err", p_iClientId), p_objException);
			}			
		}
	
	}
}
