using System;
using System.Collections;
using System.Xml;

using Riskmaster.Application.ReportInterfaces;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Application.ZipUtil;
using Riskmaster.ExceptionTypes;
using System.IO;
using System.Collections.Generic;

namespace Riskmaster.Application.ExecutiveSummaryReport
{
	/// <summary>	
	/// Author  :  Neelima Dabral
	/// Date    :  05 October 2004
	/// Purpose :  Riskmaster.Application.ExecutiveSummary.ExecSummManager class contains the methods 
	///			   that would generate the Executive Summary Reports that have been scheduled.
	/// </summary>
	public class ExecSummManager:IReportEngine
	{
		/// <summary>		
		/// This variable will contain the Database connection string.
		/// </summary>
		private string m_sDsn ="";	
		/// <summary>		
		/// A counter generates the file name for the Executive Summary Report.
		/// </summary>
		private int	   m_iEngineCounter=0;	
		/// <summary>		
		/// Directory path where reports would be saved.
		/// </summary>
		private string m_sOutputDirectory="";
		/// <summary>		
		/// Object that would notify this class about the caller application.
		/// </summary>
		private IWorkerNotifications m_objNotify=null;		
		/// <summary>		
		/// XML Document containing information about what all reports needs to be generated.
		/// </summary>
		private XmlDocument m_objCriteriaXML=null;		
		/// <summary>		
		/// List of the claim ids for which Executive Summary Reports would be generated.
		/// </summary>
		private ArrayList m_arrlstColClaims=null;
		/// <summary>		
		/// List of the Event ids for which Executive Summary Reports would be generated.
		/// </summary>
		private ArrayList m_arrlstColEvents=null;
		/// <summary>		
		/// List of all the ids for which Executive Summary Reports has been generated.
		/// </summary>
		private ArrayList m_arrlstColOutputs= null;
		/// <summary>		
		/// Object of ExecutiveSummary class to generate a specific PDF report.
		/// </summary>
		private ExecutiveSummary m_objExecSummRpt=null;				
		/// <summary>		
		/// Enum variable identifying the report format.
		/// </summary>
		private eReportFormat m_enumReportFormat;

        private int m_iClientId = 0;//rkaur27

		#region Properties
		/// <summary>
		/// Riskmaster.Application.ExecutiveSummary.ExecSummManager.Notify property 
		/// accesses the value of the notify object.
		/// </summary>
		public IWorkerNotifications Notify
		{
			get
			{
				return m_objNotify;
			}
			set
			{
				m_objNotify=value;
			}
		}
		/// <summary>
		/// Riskmaster.Application.ExecutiveSummary.ExecSummManager.OutputDirectory property 
		/// accesses the value of the output directory.
		/// </summary>
		public string OutputDirectory
		{
			set
			{
				m_sOutputDirectory=value;
			}
			get
			{
				return m_sOutputDirectory;
			}
		}
		/// <summary>
		/// Riskmaster.Application.ExecutiveSummary.ExecSummManager.OutputFormat property 
		/// accesses the value of the report format.
		/// </summary>
		public eReportFormat OutputFormat
		{
			set
			{
				m_enumReportFormat=value;
			}
			get
			{
				return m_enumReportFormat;
			}
		}
		/// <summary>
		/// Riskmaster.Application.ExecutiveSummary.ExecSummManager.ReportOutput property 
		/// accesses the value of the report output.
		/// </summary>
		public  ArrayList ReportOutput
		{
			get
			{
				return m_arrlstColOutputs;
			}
		}
		/// <summary>
		/// Riskmaster.Application.ExecutiveSummary.ExecSummManager.CriteriaXmlDom property 
		/// accesses the XML Document fetched for the scheduled Executive Summary reports.
		/// </summary>
		public XmlDocument CriteriaXmlDom
		{
			get
			{
				return m_objCriteriaXML;
			}
			set
			{
				XmlDocument objXmlCriteriaDoc = null;
				string sTable ="";
				string sFrom ="";
				string sJoin="";
				string sBeginDate ="";
				string sEndDate ="";
				int iTemp=0;
				string sDeltaClause="";
				string sStandardClause= " 1=1 ";
				string sSql="";
				DbReader objReader=null;
				string sDataValue="";
				string sExplicitEventClause="";
				string sExplicitClaimClause="";
				try
				{
					objXmlCriteriaDoc = m_objCriteriaXML;
					m_objCriteriaXML=value;
					if (ValidateCriteria())
					{
							
						//Build SQL Statement(s) to bring back rowid's of all records to report on.
						sTable =GetXMLValue("recordtype").ToUpper();
						sFrom = sTable;
						if(sTable == "CLAIM")
						{
							sFrom = sFrom + ",EVENT";
							sJoin =  " CLAIM.EVENT_ID = EVENT.EVENT_ID ";
						}
						switch(GetXMLValue("datemethod"))
						{
							case "explicit" :
								sBeginDate = "'" + GetXMLValue("begindate") + "000000'";
								sEndDate = "'" + (GetXMLValue("enddate") + 1) + "000000'";
								break;
							case "relative" :
								iTemp = Declarations.Val(GetXMLValue("reviewperiodscalar"), m_iClientId);
								sBeginDate = Declarations.DateAdd(GetXMLValue("reviewperiodunit"), -1 * iTemp,DateTime.Today, m_iClientId).ToString();
								sBeginDate = "'" + Common.Conversion.GetDateTime(sBeginDate)+ "'";							
								sEndDate ="'" + Common.Conversion.GetDate(Declarations.DateAdd("d",1,DateTime.Today,m_iClientId).ToString()) + "000000'";							
								break;
						}
						switch(GetXMLValue("deltatrackingmethod"))
						{
							case "none":
								sDeltaClause ="";
								break;
							case "newonly":								
								if(GetXMLValue("filtermethod") == "standard")
									sDeltaClause = " " + sBeginDate + " <=" + sTable + ".DTTM_RCD_ADDED AND " + sTable + ".DTTM_RCD_ADDED <= " + sEndDate + " ";
								else
									sDeltaClause = " " + sBeginDate + " <= DTTM_RCD_ADDED AND DTTM_RCD_ADDED <= " + sEndDate + " ";
								break;
							case "changedonly":
								if(GetXMLValue("filtermethod") == "standard")
								{
									sDeltaClause = " " + sBeginDate + " <= " + sTable + ".DTTM_RCD_LAST_UPD AND " + sTable + ".DTTM_RCD_LAST_UPD <= " + sEndDate + " ";
									sDeltaClause = sDeltaClause + " AND " + sTable + ".DTTM_RCD_LAST_UPD <> " + sTable + ".DTTM_RCD_ADDED ";
								}
								else
								{
									sDeltaClause = " " + sBeginDate + " <=  DTTM_RCD_LAST_UPD AND DTTM_RCD_LAST_UPD <= " + sEndDate + " ";
									sDeltaClause = sDeltaClause + " AND DTTM_RCD_LAST_UPD <> DTTM_RCD_ADDED ";
								}
								break;
							case "neworchanged":
								if(GetXMLValue("filtermethod") == "standard")
								{								
									sDeltaClause = " ((" + sBeginDate + " <= " + sTable + ".DTTM_RCD_ADDED AND " + sTable + ".DTTM_RCD_ADDED <= " + sEndDate + ") ";
									sDeltaClause = sDeltaClause + " OR ";
									sDeltaClause = sDeltaClause + " (" + sBeginDate + " <= " + sTable + ".DTTM_RCD_LAST_UPD AND " + sTable + ".DTTM_RCD_LAST_UPD <= " + sEndDate + ")) ";
								}
								else
								{
									sDeltaClause = " ((" + sBeginDate + " <= DTTM_RCD_ADDED AND DTTM_RCD_ADDED <= " + sEndDate + ") ";
									sDeltaClause = sDeltaClause + " OR ";
									sDeltaClause = sDeltaClause + " (" + sBeginDate + " <= DTTM_RCD_LAST_UPD AND DTTM_RCD_LAST_UPD <= " + sEndDate + ")) ";
								}
								break;
						}
						switch(GetXMLValue("filtermethod"))
						{
							case "standard":
								if (sTable == "CLAIM")
									sStandardClause = " LINE_OF_BUS_CODE =" + GetXMLValue("lob") + " ";
								if (GetXMLValue("explicitdepts") != "")									
									sStandardClause = sStandardClause + ((sStandardClause != "") ? " AND " : "") + " EVENT.DEPT_EID IN ( " + QuotedList(GetXMLValue("selecteddepartments")) + " )";
								sSql = "SELECT " + sTable + "_ID FROM " + sFrom + " WHERE " + sStandardClause;
								sSql = sSql + ((sJoin != "")? ((sStandardClause != "")? " AND " : "") + sJoin : "");
								sSql = sSql + ((sDeltaClause != "") ? (((sStandardClause != "") || (sJoin != "")) ? " AND ": "") + sDeltaClause : "");																	

								objReader = DbFactory.GetDbReader(m_sDsn,sSql);
								while(objReader.Read())
								{
									sDataValue = Common.Conversion.ConvertObjToStr(objReader[0]);
									if(int.Parse(sDataValue) != 0)
									{
										if(sTable == "CLAIM")
											AddUnique(ref m_arrlstColClaims,sDataValue);
										else
											AddUnique(ref m_arrlstColEvents,sDataValue);
									}
								}
								objReader.Close();
								break;
							case "explicit":
								sExplicitEventClause = "EVENT.EVENT_ID IN ( " + QuotedList(GetXMLValue("events")) + " )";
								sExplicitClaimClause = "CLAIM.CLAIM_ID IN ( " + QuotedList(GetXMLValue("claims")) + " )";

								sSql = "SELECT EVENT.EVENT_ID FROM EVENT WHERE " + sExplicitEventClause;							
								sSql = sSql + ((sDeltaClause != "") ? " AND " + sDeltaClause : "");								
								objReader = DbFactory.GetDbReader(m_sDsn,sSql);
								while(objReader.Read())
								{
									sDataValue = Common.Conversion.ConvertObjToStr(objReader[0]);
									if(int.Parse(sDataValue) != 0)																		
										AddUnique(ref m_arrlstColEvents,sDataValue);									
								}
								objReader.Close();
								sSql = "SELECT CLAIM_ID FROM CLAIM WHERE " + sExplicitClaimClause;
								sSql = sSql + ((sDeltaClause != "" )? " AND " + sDeltaClause: "");
								objReader = DbFactory.GetDbReader(m_sDsn,sSql);
								while(objReader.Read())
								{
									sDataValue = Common.Conversion.ConvertObjToStr(objReader[0]);								
									if(int.Parse(sDataValue) != 0)
										AddUnique(ref m_arrlstColClaims,sDataValue);
								}
								objReader.Close();
								break;
						}
					}
					
				}
				catch(InvalidCriteriaException p_objException )
				{
					throw p_objException;				
				}
				catch(RMAppException p_objException)
				{
					throw p_objException;
				}	
				catch(Exception p_objException)
				{
					throw new RMAppException(Globalization.GetString("ExecSummManager.CriteriaXmlDom.Set", m_iClientId),p_objException);
				}			
				finally
				{
					if(objReader != null)
					{
						objReader.Close();
						objReader.Dispose();
					}
					objXmlCriteriaDoc = null;
				}
			}
		}

		#endregion	

		#region Implementation of Interface
		public string CriteriaHtml{get {return "ERR_NO_IMPL";}}
		public bool HtmlCriteriaSupported{get {return false;}}
		public bool XmlCriteriaSupported{get {return true;}}		
		public int OutputsSupported{get {return (int) eReportFormat.Pdf;}}		
		public IWorkerNotifications WorkerNotifications{get{return m_objNotify;}set{m_objNotify = value;}}
		
		/// <summary>
		/// Database Connection String
		/// </summary>
		public string ConnectionString
		{
			get
			{
				return m_sDsn; 
			}
			set
			{
				if(value!=null)	
				{
					m_sDsn = value; 
				}
				else
				{
					m_sDsn = string.Empty;
				}
			}
		}

		public bool CheckAbort()
		{			
			return false;
		}		
		public	void UpdateProgress(string p_sReportStage,int p_iTimeElapsed,int p_iTimeLeft,int p_iPercentComplete)
		{			
		}
		#endregion

		/// <summary>
		/// Riskmaster.Application.ExecutiveSummary.ExecSummManager
		/// would set the database connection string.
		/// </summary>		
		/// <param name="p_sDsn">Connection String</param>		
		public ExecSummManager(string p_sDsn, int p_iClientId)
		{
			try
			{
				this.m_sDsn=p_sDsn;								
				m_objCriteriaXML = new XmlDocument();
				m_arrlstColClaims= new ArrayList();				
				m_arrlstColEvents= new ArrayList();				
				m_arrlstColOutputs= new ArrayList();
                m_iClientId = p_iClientId;//rkaur27
				m_objExecSummRpt = new ExecutiveSummary(p_sDsn, m_iClientId);//rkaur27
				++m_iEngineCounter;
			}				
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ExecSummManager.ExecSummManager.ErrorInitialize", m_iClientId),p_objException);
			}			
		}
		/// <summary>
		/// This method would check that the connection string is not blank.
		/// </summary>		
		public void Init()
		{
			if(this.m_sDsn=="")				
				throw new ConnectionStringInitializationException(Globalization.GetString("ExecSummManager.Init.ErrorInit",m_iClientId));			
		}
		/// <summary>
		/// This method would fetch what all Executive Reports needs to be generated.
		/// It would generate all those reports at the specified path, zip those reports into a file.
		/// The individual reports would then be deleted.
		/// </summary>	
		public void RunReport()
		{
			DateTime datStartdate = DateTime.Now;
			int iTotalCnt=0;
			int iCurrCount=0;
			bool bFinished = false;			
			int iElapsedSeconds=0;
			string sZipFileName="";
			try
			{
				m_arrlstColOutputs.Clear();				
				iTotalCnt = m_arrlstColClaims.Count + m_arrlstColEvents.Count;
				if(iTotalCnt == 0)							
					throw new ExecSummaryReportEngineException(Globalization.GetString("ExecSummManager.RunReport.NoRecordFound", m_iClientId));				

				if (m_sOutputDirectory == "") 
					m_sOutputDirectory =System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
				
				foreach(string sReport in m_arrlstColClaims)
				{
					++iCurrCount;
					if(this.Notify!=null)
					{
						if(this.Notify.CheckAbort()!=0)						
							throw new AbortRequestedException(Globalization.GetString("ExecSummManager.RunReport.AbortRequest",m_iClientId));
					}
					iElapsedSeconds= Declarations.DateDiff(datStartdate, m_iClientId);					
					if(this.Notify!=null)
					{
						this.Notify.UpdateProgress("Generating Report (" + iCurrCount + " of " + iTotalCnt + ")",iElapsedSeconds,(iElapsedSeconds / ((iCurrCount!=0) ? iCurrCount : 1 * (iTotalCnt-iCurrCount))),(iCurrCount / iTotalCnt * 100));
					}
					m_objExecSummRpt.ClaimExecSumm(int.Parse(sReport),m_sOutputDirectory + "Claim" + sReport + "_" + m_iEngineCounter + ".pdf");
					m_arrlstColOutputs.Add(m_sOutputDirectory + "Claim" + sReport + "_" + m_iEngineCounter + ".pdf");						
				}				
				foreach(string sReport in m_arrlstColEvents)
				{
					++iCurrCount;
					if(this.Notify!=null)
					{
						if(this.Notify.CheckAbort()!=0)
							throw new AbortRequestedException(Globalization.GetString("ExecSummManager.RunReport.AbortRequest",m_iClientId));						
					}				
					iElapsedSeconds= Declarations.DateDiff(datStartdate,m_iClientId);					
					if(this.Notify!=null)
					{
						this.Notify.UpdateProgress("Generating Report (" + iCurrCount + " of " + iTotalCnt + ")",iElapsedSeconds,(iElapsedSeconds / ((iCurrCount!=0) ? iCurrCount : 1 * (iTotalCnt-iCurrCount))),(iCurrCount / iTotalCnt * 100));
					}
					m_objExecSummRpt.EventExecSumm(int.Parse(sReport),m_sOutputDirectory + "Event" + sReport + "_" + m_iEngineCounter + ".pdf");
					m_arrlstColOutputs.Add(m_sOutputDirectory + "Event" + sReport + "_" + m_iEngineCounter + ".pdf");					
				}
				
				iCurrCount = 0;
				iCurrCount = m_arrlstColOutputs.Count;
				
				if(iCurrCount >1)
					bFinished = ZipResults();
				else if(iCurrCount ==1)
				{
					sZipFileName=m_sOutputDirectory + "ExecSummary" + DateTime.Now.ToString("yyyyMMHHmmss")+ ".pdf";
					File.Copy(Common.Conversion.ConvertObjToStr(m_arrlstColOutputs[0]),sZipFileName);
					foreach(string sReport in m_arrlstColOutputs)						
						File.Delete(sReport);	
					m_arrlstColOutputs.Clear();
					m_arrlstColOutputs.Add(sZipFileName);
					
					bFinished = true;
				}
				if(!bFinished)
				{
					foreach(string sReport in m_arrlstColOutputs)						
						File.Delete(sReport);						
				}				

			}
			catch(AbortRequestedException p_objException)
			{
				if(!bFinished)
				{
					foreach(string sReport in m_arrlstColOutputs)						
						File.Delete(sReport);						
				}
				throw p_objException;
			}
			catch(ExecSummaryReportEngineException p_objException)
			{
				throw p_objException;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ExecSummManager.RunReport.Gen",m_iClientId),p_objException);
			}
			finally
			{
				m_objExecSummRpt = null;				
				m_arrlstColClaims= null;								
				m_arrlstColEvents= null;	
			}
		}
		/// <summary>
		/// The method zips the individual reports into a file.
		/// This zipped file would be generated at the specific path.
		/// The individual report would be deleted once they are zipped.
		/// </summary>	
		/// <returns>True/False</returns>
		private bool ZipResults()
		{
			bool bZipSucess=false;
			string sZipFileName="";
            ZipUtility objZipUtility = null;
            List<string> arrZippedFiles = new List<string>();
			try
			{

                objZipUtility = new ZipUtility(m_iClientId);
				sZipFileName=m_sOutputDirectory + "ExecSummary" + DateTime.Now.ToString("yyyyMMHHmmss") + ".zip";
                foreach (string sReportFile in m_arrlstColOutputs)
                {
                    arrZippedFiles.Add(sReportFile);
                }//foreach
			
                //Compress all of the files into a single *.zip file
                objZipUtility.CompressFile(arrZippedFiles, sZipFileName);


				foreach(string sReport in m_arrlstColOutputs)						
					File.Delete(sReport);	
				m_arrlstColOutputs.Clear();
				m_arrlstColOutputs.Add(sZipFileName);
				bZipSucess = true;
				
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ExecSummManager.ZipResults.ErrorZip",m_iClientId),p_objException);
			}
			finally
			{
                objZipUtility = null;
			}
			return bZipSucess;
		}
		/// <summary>
		/// The method would generate the array list containing the unique ids for which executive summary
		/// reports would be generated.
		/// </summary>	
		/// <param name="p_arrlstColClaims">Arraylist containing the Event Id/Claim Id</param>
		/// <param name="p_sDataValue">Event Id/Claim Id that needs to be added to the arraylist</param>
		private void AddUnique(ref ArrayList p_arrlstColClaims,string p_sDataValue)
		{
			try
			{
				if (!Common.Utilities.ExistsInArray(p_arrlstColClaims,p_sDataValue))
					p_arrlstColClaims.Add(p_sDataValue);
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ExecSummManager.AddUnique.Set",m_iClientId),p_objException);
			}
		}
		/// <summary>
		/// The method would generate the where clause of the query for fetching the Event Ids/Claim Ids.	
		/// It splits the CSV string and Join them with ','.	
		/// </summary>	
		/// <param name="p_sCSVList">CSV string</param>
		/// <returns>String</returns>
		private string QuotedList(string p_sCSVList)
		{
			string sRetVal="";
			char[] arrDelimiter={','};			
			string[] arrItems;
			try
			{				
				arrItems=p_sCSVList.Split(arrDelimiter);
				for(int iItemCount=0;iItemCount<arrItems.Length;++iItemCount)
				{
					if(iItemCount != arrItems.Length -1)
						sRetVal += arrItems[iItemCount] + ",";
					else
						sRetVal += arrItems[iItemCount];
				}				
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("ExecSummManager.QuotedList.Set", m_iClientId), p_objException);
			}
			return sRetVal;
		}
		
		/// <summary>
		/// The method would validate the XML Document read for the scheduled report.
		/// </summary>	
		/// <returns>True/False</returns>		
		private bool ValidateCriteria()
		{
			int iXMLAttValue=0;
			bool bRetValue=false;
			string sXMLAttValue="";
			string sCompareString="";
			try
			{
				if(GetXMLValue("filtermethod")== "standard" )
				{
                    iXMLAttValue = Declarations.Val(GetXMLValue("explicitdepts"), m_iClientId);
					if( (iXMLAttValue != 0) && (GetXMLValue("selecteddepartments") == ""))
                        throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.DeptNotFound", m_iClientId));
					if( (iXMLAttValue ==0) && (GetXMLValue("selecteddepartments") != ""))
                        throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.SelDeptChkBoxNotChecked", m_iClientId));
				}
				else if(GetXMLValue("filtermethod") == "explicit")
				{
					if((GetXMLValue("events") == "") && (GetXMLValue("claims") == ""))
                        throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.NoEventClaimSpecified", m_iClientId));
				}
				else
                    throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.MissingFilter", m_iClientId));

				switch (GetXMLValue("deltatrackingmethod").ToLower())
				{
					case "none" :
						break;
					case "newonly":
					case "changedonly":
					case "neworchanged":
						if(GetXMLValue("datemethod")  == "explicit")
						{
							if(GetXMLValue("begindate") == "")
                                throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.InvalidBeginDate", m_iClientId));
							if(GetXMLValue("enddate") == "")
                                throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.InvalidEndDate", m_iClientId));
							if(int.Parse(GetXMLValue("enddate")) < int.Parse(GetXMLValue("begindate")))
                                throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.InvalidDateRange", m_iClientId));
						}
						else if(GetXMLValue("datemethod")  == "relative")
						{
                            iXMLAttValue = Declarations.Val(GetXMLValue("reviewperiodscalar"), m_iClientId);
							if(iXMLAttValue == 0)
                                throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.InvalidTimeUnit", m_iClientId));
							sXMLAttValue=GetXMLValue("reviewperiodunit");
							sCompareString="dmyyyy";
							if ((sCompareString.IndexOf(sXMLAttValue) <= -1) || (sXMLAttValue == ""))
                                throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.InvalidDateUnit", m_iClientId));
						}
						else
                            throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.MissingDate", m_iClientId));
						break;
					default:
                        throw new InvalidCriteriaException(Globalization.GetString("ExecSummManager.ValidateCriteria.InvalidMissingDeltaTrackingOption", m_iClientId));
						
				}			
				bRetValue = true;
			}
			catch(InvalidCriteriaException p_objException )
			{
				throw p_objException;				
			}
			catch(Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("ExecSummManager.ValidateCriteria.ErrorValidate", m_iClientId), p_objException);
			}			
			return bRetValue;
		}
		/// <summary>
		/// The method would return the value from the XML for the attribute passed. 
		/// </summary>	
		/// <param name="p_sName">Attribute Name</param>
		/// <returns>Value of the attribute</returns>
		private string GetXMLValue(string p_sName)
		{
			XmlElement objXMLElement=null;	
			string sReturnVal="";
			try
			{				
				objXMLElement = (XmlElement)m_objCriteriaXML.SelectSingleNode("//control[@name='" + p_sName + "']");
				
				switch (objXMLElement.GetAttribute("type").ToLower())
				{
					case "radio" :						
						objXMLElement = (XmlElement) m_objCriteriaXML.SelectSingleNode("//control[@name='" + p_sName + "' and @checked]");
						if(objXMLElement != null)
							sReturnVal = objXMLElement.GetAttribute("value");
						break;
					case "date" :
						sReturnVal = objXMLElement.GetAttribute("value");
						if (Utilities.IsDate(sReturnVal))
							sReturnVal = Common.Conversion.GetDate(sReturnVal);
						else
							sReturnVal = "";
						break;
					case "combobox" :
						sReturnVal = objXMLElement.GetAttribute("codeid");
						break;
					case "multiclaimnumberlookup" :
						foreach(XmlElement objChild in  objXMLElement.ChildNodes)
                            Common.Utilities.AddDelimited(ref sReturnVal, Declarations.GetStrValFromXmlAttribute(objChild.GetAttribute("value"), m_iClientId), null, m_iClientId);
						break;
					case "multieventnumberlookup":
						foreach(XmlElement objChild in objXMLElement.ChildNodes)							
							Common.Utilities.AddDelimited(ref sReturnVal, Declarations.GetStrValFromXmlAttribute(objChild.GetAttribute("value"), m_iClientId),null, m_iClientId);
						break;
			
					case "multiorgh":
						foreach(XmlElement objChild in objXMLElement.ChildNodes)							
							Common.Utilities.AddDelimited(ref sReturnVal, Declarations.GetStrValFromXmlAttribute(objChild.GetAttribute("value"), m_iClientId),null, m_iClientId);
						break;
					case "numeric":
						sReturnVal = Declarations.GetStrValFromXmlAttribute(objXMLElement.GetAttribute("value"), m_iClientId);
						break;
					case "checkbox":
						objXMLElement = null;						
						objXMLElement = (XmlElement)m_objCriteriaXML.SelectSingleNode("//control[@name='" + p_sName + "' and @checked]");
						sReturnVal = (objXMLElement!=null?"1":"");
						break;
				}		
			}
			catch(RMAppException p_objException )
			{
				throw p_objException;
			}
			catch(Exception p_objException )
			{
                throw new RMAppException(Globalization.GetString("ExecSummManager.GetXMLValue.ErrorGet", m_iClientId), p_objException);
			}
			finally
			{
				objXMLElement=null;				
			}
			return sReturnVal;
		}
		
	}
}
