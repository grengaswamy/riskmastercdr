using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.Extensibility
{
	/// <summary>
	/// Summary description for NavTree.
	/// </summary>
	public class CustomNavTree
	{
        private static int m_iClientId = 0;
        /// <summary>
        /// Gets an Extensibility DOM
        /// </summary>
        public static XmlElement GetExtensibilityDOM
        {
            get
            {
                XmlDocument xmlDoc = new XmlDocument();
                return xmlDoc.CreateElement("Extensibility");
            }//get
        } // property GetExtensibilityDOM

		public static bool AddNodes(XmlDocument objTreeDOM)
		{
            XmlElement objExtensibilityDOM = GetExtensibilityDOM;

			if(objExtensibilityDOM == null)
				return false;
			
			XmlNodeList objNodes = objExtensibilityDOM.SelectNodes("//NavTreeMapping[@action='add']");
			
			foreach(XmlElement objElt in objNodes)
                AddNode(objTreeDOM, objElt.FirstChild as XmlElement, objElt.Attributes["siblingid"].Value, (objElt.Attributes["insert"].Value == "before"), m_iClientId);

			return true;
		}
		public static bool ReplaceNodes(XmlDocument objTreeDOM)
		{
            XmlElement objExtensibilityDOM = GetExtensibilityDOM;

			if(objExtensibilityDOM == null)
				return false;
			XmlNodeList objNodes = objExtensibilityDOM.SelectNodes("//NavTreeMapping[@action='replace']");
			
			foreach(XmlElement objElt in objNodes)
			{
                AddNode(objTreeDOM, objElt.FirstChild as XmlElement, objElt.Attributes["baseid"].Value, false, m_iClientId);
                RemoveNode(objTreeDOM, objElt.Attributes["baseid"].Value, m_iClientId);
			}

			return true;
		}

		public static bool RemoveNodes(XmlDocument objTreeDOM)
		{
            XmlElement objExtensibilityDOM = GetExtensibilityDOM;

			if(objExtensibilityDOM == null)
				return false;
			XmlNodeList objNodes = objExtensibilityDOM.SelectNodes("//NavTreeMapping[@action='remove']");

			foreach(XmlElement objElt in objNodes)
                RemoveNode(objTreeDOM, objElt.Attributes["baseid"].Value, m_iClientId);

			return true;
		}

		#region Utility Routines
		private static void RemoveNode(XmlDocument p_objDocument,string p_sNodeName,int p_iClientId)//sharishkumar Rmacloud
		{
			try
			{ 
				XmlNode objNode=p_objDocument.SelectSingleNode("//entry[@id='"+p_sNodeName+"']");
				if (objNode!=null)
					objNode.ParentNode.RemoveChild(objNode);
			}
			catch(Exception e)
			{
                throw new RMAppException(Globalization.GetString("NavTree.RemoveNode.error", p_iClientId), e);  //sharishkumar Rmacloud
			}
		}
		//If bInsertBefore is false, the new node is inserted after the existing sibling...
        private static void AddNode(XmlDocument p_objDocument, XmlElement p_objNewElt, string p_sSiblingNodeName, bool p_bInsertBefore, int p_iClientId)//sharishkumar Rmacloud
		{
			try
			{ 
				XmlNode objNode=p_objDocument.SelectSingleNode("//entry[@id='"+p_sSiblingNodeName+"']");
				if (objNode!=null)
				{
					XmlNode objDupeNode = p_objDocument.ImportNode(p_objNewElt,true);
					
					if(p_bInsertBefore)
						objNode.ParentNode.InsertBefore(objDupeNode,objNode);
					else
						objNode.ParentNode.InsertAfter(objDupeNode,objNode);
				}
			}
            catch (Exception e) { throw new RMAppException(Globalization.GetString("NavTree.AddNode.error", p_iClientId), e); }//sharishkumar Rmacloud
		}
		#endregion

	}
}
