using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Reflection;
namespace Riskmaster.Application.Extensibility
{
    // Used by Base code to handle extensible surfaces in RMX code.
    // Has two main operations:
    // 1.) Check for configured Extension
    // 2.) Instantiate and hand back an appropriate Extension Interface Implementation

    /// <summary>
    /// Implementation Extensions are defined in riskmaster.config as follows:
    /// 
    /// 
	///	<add 	
	///		name="IVerifyCoverage" 
	///		assembly="Riskmaster.Extension.MCICExtensions.dll" 
    ///      class="Riskmaster.Extension.MCICVerifyCoverage" />
    /// </summary>
    public class ExtensionFactory
    {

        // BSB 03.05.2007 Note this will return null if no suitable Extension can be identified.
        // This would not be an error condition per se and callers should have an alternate 
        // plan for implementing or stubbing out the features of the requested Interface.
        public static object Get(string sInterfaceName, object[] arrCtorParams, int p_iClientId)
       {
           RMAdaptersCollection rmAdapters = RMConfigurationManager.GetRMExtensibilityAdaptors();

           //Only get the 1st element in the collection
           //NOTE: The current implementation does not support collections of extensible assemblies
           RMAdapterElement rmAdapter = rmAdapters[sInterfaceName];

           //Raman 07/24/2009 : Adding null check if no suitable extension can be identified
           if (rmAdapter == null)
           {
               return null;
           }
          
            // Load the assembly
           Assembly a = Assembly.LoadFrom(String.Format("{0}\\bin\\{1}", AppDomain.CurrentDomain.BaseDirectory, rmAdapter.Assembly));
            
            if (a == null)// Throw back system error if cannot load assembly.
                throw new RMAppException(Globalization.GetString("Riskmaster.Extensibility.ErrorLoadingAssembly", p_iClientId));

            // Get the type of the class
            Type t = null;

            t = a.GetType(rmAdapter.Class, true, true);
            if (a == null)// Throw back system error if cannot load assembly.
                throw new RMAppException(Globalization.GetString("Riskmaster.Extensibility.ErrorLoadingAssembly", p_iClientId));

            return System.Activator.CreateInstance(t, arrCtorParams);

        }
    }
}
