using System;
using System.IO ;
using System.Xml ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;

namespace Riskmaster.Application.ProgressNotes
{
    /// <summary>
    /// akaur9: changing the access modifiers of the XML functions from internal to public as we are 
    /// using it at mutiple places now:- Application.ExecutiveSummary
    /// </summary>	
	public class Functions
	{
		#region Common XML functions
		
		/// Name		: StartDocument
		/// Author		: Vaibhav Kaushik
		/// Date Created: 4 Oct 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for XML operations
		/// </summary>
		
		public static void StartDocument( ref XmlDocument objXmlDocument , ref XmlElement p_objRootNode , string p_sRootNodeName, int p_iClientId)
		{
			try
			{
				objXmlDocument = new XmlDocument();
				p_objRootNode = objXmlDocument.CreateElement( p_sRootNodeName );
				objXmlDocument.AppendChild( p_objRootNode );				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.StartDocument.InitDocError", p_iClientId), p_objEx);				
			}
		}

		/// Name		: CreateElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 4 Oct 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for XML operations
		/// </summary>

		public static void CreateElement( XmlElement p_objParentNode , string p_sNodeName, int p_iClientId )
		{
			XmlElement objChildNode = null ;
			try
			{
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.CreateElement.NewNodeError", p_iClientId), p_objEx);
			}
			finally
			{
				objChildNode = null ;
			}

		}	
		
		/// Name		: CreateElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 4 Oct 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for XML operations
		/// </summary>
		
		public static void CreateElement( XmlElement p_objParentNode , string p_sNodeName , ref XmlElement p_objChildNode,int p_iClientId )
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.CreateElement.NewNodeError", p_iClientId), p_objEx);
			}

		}
		
		/// Name		: CreateAndSetElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 4 Oct 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for XML operations
		/// </summary>
		

		public static void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText, int p_iClientId )
		{
			try
			{
				XmlElement objChildNode = null ;
                Functions.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode, p_iClientId);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.CreateAndSetElement.NewNodeError", p_iClientId), p_objEx);
			}
		}
		
		/// Name		: CreateAndSetElement
		/// Author		: Vaibhav Kaushik
		/// Date Created: 4 Oct 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// ************************************************************
		/// <summary>		
		/// This method is basically used for XML operations
		/// </summary>

		public static void CreateAndSetElement( XmlElement p_objParentNode , string p_sNodeName , string p_sText , ref XmlElement p_objChildNode , int p_iClientId)
		{
			try
			{
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode, p_iClientId);
				p_objChildNode.InnerText = p_sText ;			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Functions.CreateAndSetElement.NewNodeError", p_iClientId), p_objEx);
			}
		}

		// While taking input from enhance notes data is saved in html, hence & gets converted to &amp; etc

		/// <summary>
		/// In Enhanced notes if u input following text
		/// this is < a test of & and "&" '
		/// html eqv -> this is &lt; a test of &amp; and &quot;&amp;&quot; '
		/// xml eqv ->this is &amp;lt; a test of &amp;amp; and &amp;quot;&amp;amp;&amp;quot; '
		/// </summary>
		/// <param name="text"></param>
		/// <returns>returns simple string</returns>
		internal static string ConvertToTextForPrinting(string text)
		{ 
			//in db we will have strings of like this -> is &lt; a test of &amp; and &quot;&amp;&quot; '
			// to ge text back
			XmlDocument xd = new XmlDocument();
			xd.LoadXml("<EntityChars attr =\""  + text + "\"/>");			
			XmlNodeList xnl =  xd.GetElementsByTagName("EntityChars");
			if (xnl.Count > 0)
			{
				return xnl.Item(0).Attributes[0].InnerText;
			}
			else
				return text;			
		}


        internal static string IsNoteEditable(string sDateCreated, string sTimeCreated, string sTimeLimit, string sEditFlag, string sEnteredByName , string sUserName)
        {
            DateTime DateTimeCreated = Convert.ToDateTime(sDateCreated + " " + sTimeCreated);
            TimeSpan ts = DateTime.Now - DateTimeCreated;
            string sIsNoteEditable = "true";
            if ((sEditFlag == "true" || sEditFlag == "True") ? (sUserName == sEnteredByName) : true)
            {
                if ((Convert.ToDouble(sTimeLimit) != 0) && (ts.TotalHours >= Convert.ToDouble(sTimeLimit)))//Parijat: 0 considered as infinite
                {
                    sIsNoteEditable = "false";
                }
                else
                {
                    sIsNoteEditable = "true";
                }
            }
            else
            {
                sIsNoteEditable = "false";
            }
            return sIsNoteEditable;
        }
		#endregion 

	}
}
