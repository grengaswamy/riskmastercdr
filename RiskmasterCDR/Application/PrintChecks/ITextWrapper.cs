using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

internal interface ITextWrapper
{
    string FontName
    {
        get;
        set;
    }

    float FontSize
    {
        get;
        set;
    }

    string FontFamily
    {
        get;
        set;
    }

    string FontStyle
    {
        get;
        set;
    }

    float GetTextHeight(string strText);

    float GetTextWidth(string strText);
}
