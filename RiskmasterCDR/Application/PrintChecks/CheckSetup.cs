﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Xml ;
using System.Text ;
using System.Collections ;
using System.Collections.Generic;
using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.Settings ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Application.RMUtilities;
using Riskmaster.Security;
using System.Text.RegularExpressions;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Application.PrintChecks
{
    /**************************************************************
     * $File		: CheckSetup.cs
     * $Revision	: 1.0.0.0
     * $Date		: 01/05/2005
     * $Author		: Vaibhav Kaushik
     * $Comment		: Exposes methods for Check Setup events.
     * $Source		:  	
    **************************************************************/
    public class CheckSetup : IDisposable
    {
        private const int RMB_FUNDS_PRINTCHK = 10100;
        private const int RMB_FUNDS_PRINTCHK_CHK_NUMBER = 31;
        // akaushik5 Added for MITS 35846 Starts

        /// <summary>
        /// OrgHierarchy Mapping Class
        /// </summary>
        private class OrgHierarchyMapping
        {
            /// <summary>
            /// Gets or sets the abbr.
            /// </summary>
            /// <value>
            /// The abbr.
            /// </value>
            internal string Abbr { get; set; }

            /// <summary>
            /// Gets or sets the name.
            /// </summary>
            /// <value>
            /// The name.
            /// </value>
            internal string Name { get; set; }

            /// <summary>
            /// Prevents a default instance of the <see cref="OrgHierarchyMapping"/> class from being created.
            /// </summary>
            /// <param name="abbr">The abbr.</param>
            /// <param name="name">The name.</param>
            internal OrgHierarchyMapping(string abbr, string name)
            {
                this.Abbr = abbr;
                this.Name = name;
            }
        }
        // akaushik5 Added for MITS 35846 Ends

        #region Variable Declaration
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store UserId
        /// </summary>
        private int m_iUserId = 0;
        /// <summary>
        /// Private variable to store User GroupId
        /// </summary>
        private int m_iUserGroupId = 0;
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;
        /// <summary>
        /// Private variable to store the instance of Xml Error Node. 
        /// </summary>
        private XmlElement m_objMessagesNode = null;
        /// <summary>
        /// Private variable for ExportType indicator
        /// </summary>
        private int m_iExportToFile = 0;
        /// <summary>
        /// Private variable for Consolidate Flag
        /// </summary>
        private bool m_bConsolidate = false;
        /// <summary>
        /// Private variable for Organization Level 
        /// </summary>
        private int m_iOrgHierarchyLevel = 0;//Sonam


        private int m_iClientId = 0;//ps206

        Dictionary<int, double> dicMaxAmounts = null;

        SysSettings m_objSysSettings = null;
        ColLobSettings m_objColLobSettings = null;

        /// <summary>
        /// Private variable for Post Dated Checks
        /// </summary>
        private bool m_bAllowPostDtd = false;

        #endregion

        #region Structure Declaration
        /// Name		: AccountDetail
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// AccountDetail Structure used for Account Details.
        /// </summary>
        private struct AccountDetail
        {
            /// <summary>
            /// Private variable for Account Name.
            /// </summary>
            private string sAccountName;
            /// <summary>
            /// Private variable for SubRowId.
            /// </summary>
            private int iSubRowId;
            /// <summary>
            /// Private variable for AccountId.
            /// </summary>
            private int iAccountId;

            //JIRA:438 START: ajohari2
            /// <summary>
            /// Private variable for EFTAccount.
            /// </summary>
            private bool bEFTAccount;
            //JIRA:438 End: 


            /// <summary>
            /// Read/Write property for AccountName.
            /// </summary>
            internal string AccountName
            {
                get
                {
                    return (sAccountName);
                }
                set
                {
                    sAccountName = value;
                }
            }
            /// <summary>
            /// Read/Write property for SubRowId.
            /// </summary>
            internal int SubRowId
            {
                get
                {
                    return (iSubRowId);
                }
                set
                {
                    iSubRowId = value;
                }
            }
            /// <summary>
            /// Read/Write property for AccountId.
            /// </summary>
            internal int AccountId
            {
                get
                {
                    return (iAccountId);
                }
                set
                {
                    iAccountId = value;
                }
            }

            //JIRA:438 START: ajohari2
            internal bool EFTAccount
            {
                get
                {
                    return (bEFTAccount);
                }
                set
                {
                    bEFTAccount = value;
                }
            }
            //JIRA:438 End: 
        }


        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        /// <summary>
        /// Struct for Distribution Type. As of now only storing id and description. Later we can store Print options as well.
        /// </summary>
        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        private struct DistributionType
        {
            /// <summary>
            /// Distribution Type Id
            /// </summary>
            internal int DistributionTypeId
            {
                get;
                set;
            }

            /// <summary>
            /// Distribution Type
            /// </summary>
            internal string DistributionTypeDesc
            {
                get;
                set;
            }

        }
        #endregion

        #region Constructors & destructors
        /// Name		: CheckSetup
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_iUserId">User Id</param>
        /// <param name="p_iUserGroupId">User Group Id</param>
        public CheckSetup(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iUserId = p_iUserId;
            m_iUserGroupId = p_iUserGroupId;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            Functions.m_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
            this.ReadCheckOptions();
            m_objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);	 //ps206
            m_objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
        }
        ~CheckSetup()
        {
            Dispose();
        }
        #endregion

        #region CheckBatch Changed
        /// Name		: UpdateStatusForPrintedChecks
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML after Updating the Status For Printed Checks.
        /// </summary>
        /// <param name="p_iBatchNumber">Batch Number</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <returns>Returns the Out put XML document contains the information to refresh the screen contents after 
        /// updating the status for printed checks.</returns>
        public XmlDocument UpdateStatusForPrintedChecks(XmlDocument p_objInputDoc)
        {
            CheckManager objCheckManager = null;
            int iBatchNumber = 0;
            int iAccountId = 0;
            int iNextBatchNumber = 0;
            //int iNextCheckNum = 0 ;
            long lNextCheckNum = 0;   //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
            XmlElement objRootNode = null;
            XmlElement objTempNode = null;
            XmlElement objAccountChanged = null;
            //igupta3 Mits: 28566 Changes Starts
            bool Success = false;
            //JIRA:438 START: ajohari2
            string sSql = string.Empty;
            int iTransID = 0;
            //JIRA:438 End: 
            int sTransId = 0;
            Funds objFunds = null;
            objFunds = (Funds)m_objDataModelFactory.GetDataModelObject("Funds", false);
            string error = String.Empty;
            //igupta3 Mits: 28566 Changes ends

            SysSettings objSettings = null;
            const int iRealTimePolicyUpdate = 0;
            // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            int iDistributionType = 0;
            // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            try
            {

                objCheckManager = new CheckManager(m_objDataModelFactory);

                iBatchNumber = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "BatchNumber", m_iClientId));
                iAccountId = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "AccountId", m_iClientId));
                iTransID = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "TransId", m_iClientId));//JIRA:438 : ajohari2
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                iDistributionType = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "DistributionType", m_iClientId));
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

                //Added by Amitosh for EFT Payment				
                if (Functions.IsEFTBank(iAccountId, m_sConnectionString, m_iClientId))
                {
                    //objCheckManager.updateEFTPayment(p_objInputDoc);
                    //JIRA:438 START: ajohari2
                    bool bEFTPayment = true;
                    if (iTransID != 0)
                    {
                        sSql = "SELECT EFT_FLAG FROM FUNDS WHERE TRANS_ID = " + iTransID;
                    }
                    else
                    {
                        sSql = "SELECT EFT_FLAG FROM FUNDS WHERE ACCOUNT_ID = " + iAccountId + "  AND BATCH_NUMBER = " + iBatchNumber + "  GROUP BY EFT_FLAG";
                    }
                    bEFTPayment = Conversion.ConvertObjToBool(DbFactory.ExecuteScalar(m_sConnectionString, sSql), m_iClientId);

                    if (bEFTPayment)
                    {
                        objCheckManager.updateEFTPayment(p_objInputDoc);
                    }
                    else
                    {
                        objCheckManager.UserAckCheckPrint(p_objInputDoc);
                    }
                    //JIRA:438 End: 
                }
                else
                {
                    objCheckManager.UserAckCheckPrint(p_objInputDoc);
                }

                Functions.StartDocument(ref m_objDocument, ref objRootNode, ref objTempNode, "PrintChecks", m_iClientId);

                // Return the XML If this is Batch Print.
                if (iBatchNumber != 0)
                {
                    // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                    this.GetAccountDetails(iAccountId, iDistributionType, ref iNextBatchNumber, ref lNextCheckNum, false, false);
                    Functions.CreateElement(objRootNode, "AccountChanged", ref objAccountChanged, m_iClientId);
                    Functions.CreateAndSetElement(objAccountChanged, "FirstCheckNum", lNextCheckNum.ToString(), m_iClientId);
                    this.CheckBatchChangedPre(iNextBatchNumber, iAccountId, true, iDistributionType);
                    this.CheckBatchChangedPost(iNextBatchNumber, iAccountId, true, iDistributionType);
                    // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                }
                //igupta3 Mits: 28566 Changes Starts                 
                var errorList = new List<string>();
                foreach (XmlElement objCheckDetail in p_objInputDoc.SelectNodes("//CheckDetail"))
                {
                    sTransId = Common.Conversion.ConvertStrToInteger(objCheckDetail.ChildNodes[0].InnerText);
                    objFunds.MoveTo(sTransId);

                    if (objFunds.AutoCheckFlag)
                    {
                        Success = CreateWorkLossRestrictions(objFunds);
                        if (!Success)
                        {
                            errorList.Add(objFunds.CtlNumber);
                        }
                    }
                }
                string Temperr = "";
                if (errorList.Count > 0)
                {

                    foreach (var item in errorList)
                    {
                        if (errorList.Count != 1)
                        {
                            Temperr = Temperr + "," + item;
                        }
                        else
                        {
                            Temperr = item;
                        }
                    }

                    error = "An error has occurred while creating workloss/restrictions in this time frame , for control number " + Temperr;
                    Log.Write(error, m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "WorklossError", error, m_iClientId);
                }
                //igupta3 Mits: 28566 Changes ends

                if (p_objInputDoc.SelectNodes("//CheckDetail").Count > 0)
                {
                    objSettings = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud
                    //Added by Amitosh for mits 25163
                    if (objSettings.UsePolicyInterface)
                    {
                        if (iBatchNumber > 0)
                            UpdateActivityTrack(iBatchNumber, 0, iAccountId);
                        else
                            UpdateActivityTrack(iBatchNumber, Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "TransId", m_iClientId)), iAccountId);

                        //if (objSettings.PolicySystemUpdate == iRealTimePolicyUpdate)
                        //{
                        //    SchedulePolicyUpdate(iBatchNumber);
                        //}
                    }
                }


            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.UpdateStatusForPrintedChecks.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCheckManager != null)
                {
                    objCheckManager.Dispose();
                }

                objRootNode = null;
            }
            return (m_objDocument);

        }


        //igupta3 Mits: 28566 Changes Starts 
        public bool CreateWorkLossRestrictions(Funds objFunds)
        {
            Claim objClaim = null;
            FundsTransSplit objFundsTransSplit = null;
            int iLOB = 0; //Line of Business
            bool bAutoCrtWrkLossRest = false; //flag to check whether this module is on/off 
            SysSettings oSettings = null;
            string sWrkLossCode = string.Empty;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            PiXWorkLoss objPiXWorkLoss = null;
            PiXRestrict objPiXRestrict = null;
            bool bWorkLossDelete = false;
            bool bRestrictionsDelete = false;
            bool bCastToTypeSuccess = false;
            string sSql = String.Empty;
            DbReader objReaderFunds = null;

            //igupta3
            double dNetAMount = 0;
            string sTranTypeCode = String.Empty;
            int iSplitId = 0;
            bool status = true;


            try
            {
                //added by raman for R6 Auto Create Work Loss/Restrictions 
                //Parameters fetching for Creating Auto work loss and restriction screens and saving the screens
                oSettings = new SysSettings(m_sConnectionString, m_iClientId); //Ash - cloud
                bAutoCrtWrkLossRest = oSettings.AutoCrtWrkLossRest;
                if (!bAutoCrtWrkLossRest)
                    return false;

                //Raman R7 Prf Imp : Feb 2 2011                              
                iLOB = objFunds.LineOfBusCode;

                if (bAutoCrtWrkLossRest && iLOB != 0)
                {
                    //Trans_id and split_row_id is present in WORK_LOSS_MAPPING table, but split_row_is
                    //is in deleted status i.e. the transaction has been deleted;all such records will be 
                    //deleted:smishra25 start
                    string sSQL = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND SPLIT_ROW_ID NOT IN (SELECT SPLIT_ROW_ID FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID = " + objFunds.TransId.ToString() + " )";
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        objPiXWorkLoss = (PiXWorkLoss)m_objDataModelFactory.GetDataModelObject("PiXWorkLoss", false);
                        objPiXRestrict = (PiXRestrict)m_objDataModelFactory.GetDataModelObject("PiXRestrict", false);
                        long lWorkLossRowId = 0;
                        long lRestrictRowId = 0;
                        while (objReader.Read())
                        {
                            lWorkLossRowId = 0;
                            lRestrictRowId = 0;
                            if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lWorkLossRowId = 0;
                                }
                            }
                            if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lRestrictRowId = 0;
                                }
                            }
                            if (lWorkLossRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                    objPiXWorkLoss.Delete();
                                }
                                string sSQLWL = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_X_WL_ROW_ID =" + lWorkLossRowId;
                                objFunds.Context.DbConn.ExecuteScalar(sSQLWL);
                            }
                            else if (lRestrictRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXRestrict.MoveTo((int)lRestrictRowId);
                                    objPiXRestrict.Delete();
                                }
                                string sSQLRes = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_RESTRICT_ROW_ID =" + lRestrictRowId;
                                objFunds.Context.DbConn.ExecuteScalar(sSQLRes);
                            }
                        }
                    }

                    //getting split related data
                    sSql = "SELECT * FROM FUNDS_TRANS_SPLIT WHERE TRANS_ID = " + objFunds.TransId;
                    objReaderFunds = DbFactory.GetDbReader(m_sConnectionString, sSql);

                    while (objReaderFunds.Read())
                    {
                        objFundsTransSplit = (FundsTransSplit)m_objDataModelFactory.GetDataModelObject("FundsTransSplit", false);

                        objFundsTransSplit.TransId = objFunds.TransId;

                        objFundsTransSplit.TransTypeCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("TRANS_TYPE_CODE"), m_iClientId);
                        objFundsTransSplit.ReserveTypeCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("RESERVE_TYPE_CODE"), m_iClientId);
                        objFundsTransSplit.Amount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("AMOUNT"), m_iClientId);
                        objFundsTransSplit.GlAccountCode = Conversion.ConvertObjToInt(objReaderFunds.GetValue("GL_ACCOUNT_CODE"), m_iClientId);
                        objFundsTransSplit.InvoicedBy = Conversion.ConvertObjToStr(objReaderFunds.GetValue("INVOICED_BY"));
                        objFundsTransSplit.InvoiceAmount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("INVOICE_AMOUNT"), m_iClientId);
                        objFundsTransSplit.InvoiceNumber = Conversion.ConvertObjToStr(objReaderFunds.GetValue("INVOICE_NUMBER"));
                        objFundsTransSplit.PoNumber = Conversion.ConvertObjToStr(objReaderFunds.GetValue("PO_NUMBER"));
                        objFundsTransSplit.DttmRcdAdded = Conversion.ConvertObjToStr(objReaderFunds.GetValue("DTTM_RCD_ADDED"));
                        objFundsTransSplit.AddedByUser = Conversion.ConvertObjToStr(objReaderFunds.GetValue("ADDED_BY_USER"));
                        objFundsTransSplit.DttmRcdLastUpd = Conversion.ConvertObjToStr(objReaderFunds.GetValue("DTTM_RCD_LAST_UPD"));
                        objFundsTransSplit.UpdatedByUser = Conversion.ConvertObjToStr(objReaderFunds.GetValue("UPDATED_BY_USER"));
                        objFundsTransSplit.FromDate = Conversion.ConvertObjToStr(objReaderFunds.GetValue("FROM_DATE"));
                        objFundsTransSplit.ToDate = Conversion.ConvertObjToStr(objReaderFunds.GetValue("TO_DATE"));
                        objFundsTransSplit.SumAmount = Conversion.ConvertObjToDouble(objReaderFunds.GetValue("AMOUNT"), m_iClientId);
                        objFundsTransSplit.SuppPaymentFlag = Conversion.ConvertObjToBool(objReaderFunds.GetValue("SUPP_PAYMENT_FLAG"), m_iClientId);
                        objFundsTransSplit.SplitRowId = Conversion.ConvertObjToInt(objReaderFunds.GetValue("SPLIT_ROW_ID"), m_iClientId);

                        //igupta3
                        if (objFundsTransSplit.TransTypeCode != 0)
                        {
                            dNetAMount = objFunds.Amount;
                            iSplitId = objFundsTransSplit.SplitRowId;
                            sTranTypeCode = objFundsTransSplit.TransTypeCode.ToString();
                        }

                    }

                    objReaderFunds.Close();

                    //Trans_id and split_row_id is present in WORK_LOSS_MAPPING table, but split_row_is
                    //is in deleted status i.e. the transaction has been deleted;all such records will be 
                    //deleted:smishra25 end
                    //foreach (FundsTransSplit objTempFundsTransSplit in objFunds.TransSplitList)
                    //{
                    bWorkLossDelete = true;
                    bRestrictionsDelete = true;
                    string sSQL1 = "SELECT WRKLOS_RSTRCT_CODE FROM LT_RST_DAYS_MAP WHERE LINE_OF_BUS_CODE =" + iLOB + " AND TRANS_TYPE_CODE = " + sTranTypeCode;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL1))
                    {
                        while (objReader.Read())//smishra25 MITS 18193,18212: replacing if statement with while;there can be two rows for same trans type
                        {
                            sWrkLossCode = objReader.GetString("WRKLOS_RSTRCT_CODE");
                            sFromDate = objFundsTransSplit.FromDate;
                            sToDate = objFundsTransSplit.ToDate;
                            //Added by Shivendu for MITS 18875
                            if (!string.IsNullOrEmpty(sFromDate) && !string.IsNullOrEmpty(sToDate))
                            {
                                status = saveWorkLossRestrictions(objFunds.ClaimId, objFunds.ClaimantEid, sFromDate, sToDate, sWrkLossCode, objFunds, iSplitId, dNetAMount);
                            }
                            if (sWrkLossCode.ToLower() == "work loss")
                            {
                                bWorkLossDelete = false;
                            }
                            else
                            {
                                bRestrictionsDelete = false;
                            }
                        }
                    }
                    //If this split_row_id is present in WORK_LOSS_MAPPING table,but the mapping has changed
                    //or transaction type has changed, the existing records should get deleted: smishra25 start
                    if (bRestrictionsDelete || bWorkLossDelete)
                    {
                        string sSQL2 = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND SPLIT_ROW_ID = " + iSplitId.ToString();
                        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL2))
                        {

                            long lWorkLossRowId = 0;
                            long lRestrictRowId = 0;
                            while (objReader.Read())
                            {
                                lWorkLossRowId = 0;
                                lRestrictRowId = 0;
                                if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lWorkLossRowId = 0;
                                    }
                                }
                                if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lRestrictRowId = 0;
                                    }
                                }
                                if (bWorkLossDelete)
                                {
                                    if (lWorkLossRowId != 0)
                                    {
                                        string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                        if (string.Compare(sRecordCount, "1") == 0)
                                        {
                                            objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                            objPiXWorkLoss.Delete();
                                        }
                                        string sSQLWL = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_X_WL_ROW_ID =" + lWorkLossRowId;
                                        objFunds.Context.DbConn.ExecuteScalar(sSQLWL);
                                    }
                                }
                                if (bRestrictionsDelete)
                                {
                                    if (lRestrictRowId != 0)
                                    {
                                        string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                        if (string.Compare(sRecordCount, "1") == 0)
                                        {
                                            objPiXRestrict.MoveTo((int)lRestrictRowId);
                                            objPiXRestrict.Delete();
                                        }
                                        string sSQLRes = "DELETE FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND PI_RESTRICT_ROW_ID =" + lRestrictRowId;
                                        objFunds.Context.DbConn.ExecuteScalar(sSQLRes);
                                    }
                                }
                            }
                        }
                    }



                    //If this split_row_id is present in WORK_LOSS_MAPPING table,but the mapping has changed
                    //or transaction type has changed, the existing records should get deleted: smishra25 end
                    //}
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                //if (objClaim != null)
                //{
                //    objClaim.Dispose();
                //    objClaim = null;
                //}
                if (oSettings != null)
                {
                    oSettings = null;
                }
                if (objFunds != null)
                {
                    objFunds.Dispose();
                    objFunds = null;
                }
                if (objPiXWorkLoss != null)
                    objPiXWorkLoss.Dispose();
                if (objPiXRestrict != null)
                    objPiXRestrict.Dispose();
            }
            //end of R6 Auto Create Work Loss/Restrictions(Utilities Checked and Workloss/Restricton Screen auto created
            return status;
        }

        private bool saveWorkLossRestrictions(int p_iClaimId, int p_iClaimantEid, string p_sfromdate, string p_stodate, string p_sWorkLossRestDesc, Funds objFunds, int SplitId, double NetAMount)
        {
            Claim objClaim = null;
            objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
            DateTime fromdate;
            DateTime todate;
            //Start:Added by Nitin Goel, MITS#23187,11/30/2010
            String sLastWorkDay;
            String sReturnToWork;
            DateTime dLastWorkDay;
            DateTime dReturnToWork;
            List<string> lstWLRowid;
            //End:Added by Nitin Goel, MITS#23187,11/30/2010
            int iClaimantEID = 0;
            int iPiRowID = 0;
            int iEventID = 0;
            int iDeptAssignEID = 0;
            PiXWorkLoss objPiXWorkLoss = null;
            PiXRestrict objPiXRestrict = null;
            //Claim objClaim = null;
            int iWorkLoss = 0;
            int iRestDays = 0;
            string sSQL = "";
            string[] sDaysOfWeek = new string[7];
            string sWorkFlags = string.Empty;
            bool bExistingWorkLoss = false;
            bool bExistingRestriction = false;
            bool bCastToTypeSuccess = false;
            long lWorkLossRowId = 0;
            long lRestrictRowId = 0;
            try
            {
                //Start:Added by Nitin Goel, MITS#23187,11/30/2010
                sLastWorkDay = string.Empty;
                sReturnToWork = string.Empty;
                lstWLRowid = new List<string>();
                //End:Added by Nitin Goel, MITS#23187,11/30/2010
                objPiXWorkLoss = (PiXWorkLoss)m_objDataModelFactory.GetDataModelObject("PiXWorkLoss", false);
                objPiXRestrict = (PiXRestrict)m_objDataModelFactory.GetDataModelObject("PiXRestrict", false);
                //Raman R7 Prf Imp : Feb 2 2011
                //objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                if (objClaim.IsNew)
                {
                    objClaim.MoveTo(p_iClaimId);
                }

                //Delete all the records if the payment is void:smishra25 start
                if (objFunds.VoidFlag == true)
                {

                    string sSQLVoid = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString();
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLVoid))
                    {
                        //long lWorkLossRowId = 0;
                        //long lRestrictRowId = 0;
                        while (objReader.Read())
                        {
                            lWorkLossRowId = 0;
                            lRestrictRowId = 0;
                            if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lWorkLossRowId = 0;
                                }

                            }
                            if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                            {
                                bCastToTypeSuccess = false;
                                lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                if (!bCastToTypeSuccess)
                                {
                                    lRestrictRowId = 0;
                                }

                            }
                            if (lWorkLossRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                    objPiXWorkLoss.Delete();
                                }

                            }
                            else if (lRestrictRowId != 0)
                            {
                                string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                if (string.Compare(sRecordCount, "1") == 0)
                                {
                                    objPiXRestrict.MoveTo((int)lRestrictRowId);
                                    objPiXRestrict.Delete();
                                }

                            }
                        }
                    }
                    string sSQLDel = "DELETE FROM WORK_LOSS_MAPPING WHERE  TRANS_ID =" + objFunds.TransId.ToString();
                    objFunds.Context.DbConn.ExecuteScalar(sSQLDel);

                }
                //Delete all the records if the payment is void:smishra25 End
                else
                {

                    PiEmployee objEmployee = objClaim.PrimaryPiEmployee as PiEmployee;

                    if (objEmployee != null)
                        sWorkFlags = objEmployee.WorkSunFlag + ","
                            + objEmployee.WorkMonFlag + ","
                            + objEmployee.WorkTueFlag + ","
                            + objEmployee.WorkWedFlag + ","
                            + objEmployee.WorkThuFlag + ","
                            + objEmployee.WorkFriFlag + ","
                            + objEmployee.WorkSatFlag;
                    sDaysOfWeek = sWorkFlags.Split(",".ToCharArray());
                    // Changed by Amitosh for Mits 22484 (02/16/2011)
                    ////fromdate1 = Riskmaster.Common.Conversion.ToDate(p_sfromdate).AddDays(-1);
                    ////todate1 = Riskmaster.Common.Conversion.ToDate(p_stodate).AddDays(1);
                    fromdate = Riskmaster.Common.Conversion.ToDate(p_sfromdate);
                    todate = Riskmaster.Common.Conversion.ToDate(p_stodate);
                    // end Amitosh
                    //Start:Added by Nitin Goel, MITS#23187,11/30/2010
                    sLastWorkDay = fromdate.AddDays(-1).ToString("yyyyMMdd");
                    sReturnToWork = todate.AddDays(1).ToString("yyyyMMdd");
                    ////calculate worklost days 
                    //iWorkLoss = GetWorkLossDay(dFromDate, dToDate, sDaysOfWeek);                
                    //End:Added by Nitin Goel, MITS#23187,11/30/2010
                    //calculate worklost days 
                    iWorkLoss = GetWorkLossDay(fromdate, todate, sDaysOfWeek);
                    //calculate restricted codes
                    //iRestDays = GetWorkDay(fromdate1, todate1, sDaysOfWeek);
                    iRestDays = GetWorkDay(Riskmaster.Common.Conversion.ToDate(p_sfromdate), Riskmaster.Common.Conversion.ToDate(p_stodate), sDaysOfWeek);

                    objPiXWorkLoss.PiWlRowId = -1;
                    objPiXRestrict.PiRestrictRowId = -1;

                    iClaimantEID = p_iClaimantEid;
                    iEventID = objClaim.EventId;
                    //Check if the split_row_id entry is already present in the WORK_LOSS_MAPPING
                    //table. smishra25: start
                    string sSQLGetExistingID = "SELECT * FROM WORK_LOSS_MAPPING WHERE TRANS_ID =" + objFunds.TransId.ToString() + " AND SPLIT_ROW_ID =" + SplitId.ToString();

                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQLGetExistingID))
                    {
                        //long lWorkLossRowId = 0;
                        //long lRestrictRowId = 0;
                        while (objReader.Read())
                        {
                            lWorkLossRowId = 0;
                            lRestrictRowId = 0;
                            if (p_sWorkLossRestDesc.ToLower() == "work loss")
                            {
                                if (objReader.GetValue("PI_X_WL_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lWorkLossRowId = Conversion.CastToType<long>(objReader.GetValue("PI_X_WL_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lWorkLossRowId = 0;
                                    }

                                }
                                if (lWorkLossRowId != 0)
                                {
                                    bExistingWorkLoss = true;
                                    string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID = " + lWorkLossRowId).ToString();
                                    if (string.Compare(sRecordCount, "1") == 0)
                                    {
                                        objPiXWorkLoss.MoveTo((int)lWorkLossRowId);
                                    }

                                }
                            }
                            else
                            {
                                if (objReader.GetValue("PI_RESTRICT_ROW_ID") != null)
                                {
                                    bCastToTypeSuccess = false;
                                    lRestrictRowId = Conversion.CastToType<long>(objReader.GetValue("PI_RESTRICT_ROW_ID").ToString(), out bCastToTypeSuccess);
                                    if (!bCastToTypeSuccess)
                                    {
                                        lRestrictRowId = 0;
                                    }

                                }

                                if (lRestrictRowId != 0)
                                {
                                    bExistingRestriction = true;
                                    string sRecordCount = objFunds.Context.DbConn.ExecuteScalar("SELECT COUNT(*) FROM PI_X_RESTRICT WHERE PI_RESTRICT_ROW_ID = " + lRestrictRowId).ToString();
                                    if (string.Compare(sRecordCount, "1") == 0)
                                    {
                                        objPiXRestrict.MoveTo((int)lRestrictRowId);
                                    }

                                }
                            }
                        }
                    }
                    //Check if the split_row_id entry is already present in the WORK_LOSS_MAPPING
                    //table. smishra25: End

                    sSQL = "select pi_row_id ,dept_assigned_eid from person_involved where pi_eid =" + iClaimantEID + "and event_id=" + iEventID;
                    using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {

                        while (objReader.Read())
                        {
                            iPiRowID = objReader.GetInt32("pi_row_id");
                            iDeptAssignEID = objReader.GetInt32("dept_assigned_eid");


                            if (p_sWorkLossRestDesc.ToLower() == "work loss")
                            {

                                if (!bExistingWorkLoss)
                                {
                                    // Changed by Amitosh for Mits 22484 (02/16/2011)
                                    //if (ValidateWorkLoss(objEmployee, objPiXWorkLoss, p_sfromdate, p_stodate, objTempFundsTransSplit.SumAmount))
                                    if (ValidateWorkLoss(objEmployee, objPiXWorkLoss, ref sLastWorkDay, ref sReturnToWork, NetAMount, ref lstWLRowid))
                                    {
                                        objPiXWorkLoss.PiRowId = iPiRowID;

                                        // Changed by Amitosh for Mits 22484 (02/16/2011)
                                        //  objPiXWorkLoss.DateLastWorked = p_sfromdate;
                                        //    objPiXWorkLoss.DateReturned = p_stodate;
                                        objPiXWorkLoss.DateLastWorked = sLastWorkDay;
                                        objPiXWorkLoss.DateReturned = sReturnToWork;
                                        dLastWorkDay = Riskmaster.Common.Conversion.ToDate(sLastWorkDay);
                                        dReturnToWork = Riskmaster.Common.Conversion.ToDate(sReturnToWork);
                                        iWorkLoss = GetWorkLossDay(dLastWorkDay, dReturnToWork, sDaysOfWeek);
                                        //End:Added by Nitin Goel, MITS#23187,11/30/2010

                                        objPiXWorkLoss.Duration = iWorkLoss;
                                        objPiXWorkLoss.StateDuration = iWorkLoss;
                                        objPiXWorkLoss.Save();

                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 start

                                        objFunds.Context.DbConn.ExecuteScalar("INSERT INTO WORK_LOSS_MAPPING(TRANS_ID,SPLIT_ROW_ID,PI_X_WL_ROW_ID,PI_RESTRICT_ROW_ID) VALUES(" + objFunds.TransId.ToString() + "," + SplitId.ToString() + "," + objPiXWorkLoss.PiWlRowId.ToString() + ",0)");

                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 End
                                        //Start:Added by Nitin Goel, MITS#23187,11/30/2010
                                        if (lstWLRowid.Count > 0)
                                        {
                                            UpdateExtraWorkLossRecord(lstWLRowid, lWorkLossRowId);
                                        }
                                        //End:Added by Nitin Goel, MITS#23187,11/30/2010
                                    }
                                    else
                                    {
                                        return false;
                                    }

                                }

                            }

                            else
                            {

                                if (!bExistingRestriction)
                                {
                                    if (ValidateRestrictions(objEmployee, objPiXRestrict, p_sfromdate, p_stodate, NetAMount))
                                    {

                                        objPiXRestrict.PiRowId = iPiRowID;
                                        objPiXRestrict.DateFirstRestrct = p_sfromdate;
                                        objPiXRestrict.DateLastRestrct = p_stodate;
                                        objPiXRestrict.Duration = iRestDays;
                                        objPiXRestrict.Save();

                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 start
                                        objFunds.Context.DbConn.ExecuteScalar("INSERT INTO WORK_LOSS_MAPPING(TRANS_ID,SPLIT_ROW_ID,PI_RESTRICT_ROW_ID,PI_X_WL_ROW_ID) VALUES(" + objFunds.TransId.ToString() + "," + SplitId.ToString() + "," + objPiXRestrict.PiRestrictRowId.ToString() + ",0)");
                                        //Insert the record in WORK_LOSS_MAPPING table:smishra25 End
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }

                            }
                        }
                    }
                }
                return true;
            }



            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }

            finally
            {

                if (objPiXWorkLoss != null)
                    objPiXWorkLoss.Dispose();
                if (objPiXRestrict != null)
                    objPiXRestrict.Dispose();
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    //objClaim = null;
                }
            }

        }
        private int GetWorkLossDay(DateTime sLastWorkDate, DateTime sReturnedToWorkDate, string[] sDaysOfWeek)
        {
            DateTime objLastWorkedDate = System.DateTime.Today;
            DateTime objReturnedToWorkDate = System.DateTime.Today;
            TimeSpan objDays;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            int iDays = 0;
            string sParent = string.Empty;

            objLastWorkedDate = sLastWorkDate.AddDays(1);
            objReturnedToWorkDate = sReturnedToWorkDate;
            objDays = sReturnedToWorkDate.Subtract(objLastWorkedDate);
            iDays = objDays.Days;
            iDayOfWeekStart = (int)objLastWorkedDate.DayOfWeek;
            iDayOfWeekEnd = (int)objReturnedToWorkDate.DayOfWeek;

            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)objLastWorkedDate.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }

            return iNewDays;

        }

        private int GetWorkDay(DateTime dDate1, DateTime dDate2, string[] sDaysOfWeek)
        {
            int iDays = 0;
            int iDayOfWeekStart = 0;
            int iDayOfWeekEnd = 0;
            int iNewDays = 0;
            string sParent = string.Empty;
            TimeSpan objDays;

            objDays = dDate2.Subtract(dDate1);
            iDays = objDays.Days + 1;
            iDayOfWeekStart = (int)dDate1.DayOfWeek;
            iDayOfWeekEnd = (int)dDate2.DayOfWeek;
            for (int i = 1; i <= iDays; i++)
            {
                if (sDaysOfWeek[(int)dDate1.AddDays(i - 1).DayOfWeek] == "True")
                    iNewDays = iNewDays + 1;
            }

            return iNewDays;
        }

        public bool ValidateWorkLoss(PiEmployee objEmployee, PiXWorkLoss objPiXWorkLoss, ref string p_sDateLastWorked, ref string p_sDateReturned, double p_SumAmount, ref List<string> lstWLRowid)
        {
            //DateTime dtReturnDate;
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sWarningMessage = string.Empty;
            //Start:Added by nitin goel,12/14/2010,mits 23187
            DateTime dtListLastWorked;
            DateTime dtListReturnDate;
            DateTime dtLastWorked;
            DateTime dtReturnDate;
            bool bValFlag = false;

            if (p_sDateLastWorked != "")
            {
                if (p_sDateLastWorked.CompareTo(sToday) > 0)
                {
                    sWarningMessage = "Since Last Worked Date > Today's Date, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                    //m_arrlstWarnings.Add("Since Last Worked Date > Today's Date, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                    // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                    return false;
                }

                if (p_sDateReturned != "")
                {
                    if (p_sDateLastWorked.CompareTo(p_sDateReturned) > 0)
                    {
                        sWarningMessage = "Since Last Worked Date > Date Returned, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                        //m_arrlstWarnings.Add("Since Last Worked Date > Date Returned, a Work Loss record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                        // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);

                        return false;
                    }
                }
            }

            int iWorkLossId = 0;
            int iWorkLossIdNextTemp = 0;
            int iWorkLossIdNext = 0;
            PiXWorkLossList objPiWorkLossList = objEmployee.PiXWorkLossList;
            dtLastWorked = Conversion.ToDate(p_sDateLastWorked);
            dtReturnDate = Conversion.ToDate(p_sDateReturned);

            if (objEmployee.PiXWorkLossList.Count > 0)
            {
                foreach (PiXWorkLoss objWorkLossforID in objPiWorkLossList)
                {

                    iWorkLossId = objWorkLossforID.PiWlRowId;
                    if (objPiXWorkLoss.PiWlRowId <= 0)
                        break;
                    else if (iWorkLossId < objPiXWorkLoss.PiWlRowId)
                        break;

                }
                objPiWorkLossList = objEmployee.PiXWorkLossList;

                //if (objPiXWorkLoss.PiWlRowId > 0)  commented for MITS 25713: rsushilaggar
                if (iWorkLossId > 0)  // added : rsushilaggar MITS 25713
                {
                    foreach (PiXWorkLoss objWorkLossforID in objPiWorkLossList)
                    {
                        iWorkLossIdNextTemp = objWorkLossforID.PiWlRowId;
                        dtListLastWorked = Conversion.ToDate(objPiWorkLossList[iWorkLossIdNextTemp].DateLastWorked);
                        dtListReturnDate = Conversion.ToDate(objPiWorkLossList[iWorkLossIdNextTemp].DateReturned);

                        if ((dtLastWorked.CompareTo(dtListReturnDate) < 0) && (dtReturnDate.CompareTo(dtListLastWorked) > 0))
                        {
                            if ((dtLastWorked.CompareTo(dtListLastWorked) == 0) && (dtReturnDate.CompareTo(dtListReturnDate) == 0))
                            {
                                bValFlag = true;
                                break;
                            }
                            else if ((dtLastWorked.CompareTo(dtListLastWorked) > 0) && (dtReturnDate.CompareTo(dtListReturnDate) < 0))
                            {
                                bValFlag = true;
                                break;
                            }
                            if (dtLastWorked.CompareTo(dtListLastWorked) > 0)
                            {
                                p_sDateLastWorked = dtListLastWorked.ToString("yyyyMMdd");
                            }
                            if (dtReturnDate.CompareTo(dtListReturnDate) < 0)
                            {
                                p_sDateReturned = dtListReturnDate.ToString("yyyyMMdd");
                            }
                            lstWLRowid.Add(objWorkLossforID.PiWlRowId.ToString());
                        }
                    }
                }

            }

            if (bValFlag == true)
            {
                sWarningMessage = "A Work Loss record already exists with in this time frame.";
                //m_arrlstWarnings.Add("A Work Loss record already exists with in this time frame.");
                return false;

            }
            return true;
        }


        public void UpdateExtraWorkLossRecord(List<string> lstWLRowid, long lWorkLossRowId)
        {
            string[] arrWLRowid = lstWLRowid.ToArray();
            string sWLRowid = string.Join(",", arrWLRowid);
            try
            {
                using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    objConn.Open();
                    objConn.ExecuteNonQuery("DELETE FROM PI_X_WORK_LOSS WHERE PI_WL_ROW_ID IN(" + sWLRowid + ")");
                    objConn.ExecuteNonQuery("UPDATE WORK_LOSS_MAPPING SET PI_X_WL_ROW_ID =" + lWorkLossRowId + " WHERE PI_X_WL_ROW_ID IN(" + sWLRowid + ")");
                }
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
        }

        public bool ValidateRestrictions(PiEmployee objEmployee, PiXRestrict objPiXRestrict, string p_DateFirstRestrct, string p_DateLastRestrct, double p_SumAmount)
        {
            string sToday = Conversion.ToDbDate(System.DateTime.Now);
            string sWarningMessage = string.Empty;
            // Perform data validation
            DateTime dtFirstRestrict;
            DateTime dtLastRestrict;

            if (p_DateFirstRestrct.CompareTo(sToday) > 0)
            {
                sWarningMessage = "Since First Restricted Date > Today's Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                //m_arrlstWarnings.Add("Since First Restricted Date > Today's Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                //Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                return false;
            }

            if (p_DateLastRestrct.Length > 0)
            {
                if (p_DateFirstRestrct.CompareTo(p_DateLastRestrct) > 0)
                {
                    sWarningMessage = "Since First Restricted Date >  Last Restricted Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                    // m_arrlstWarnings.Add("Since First Restricted Date >  Last Restricted Date, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                    // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                    return false;
                }
            }


            int iRestrictId = 0;
            int iRestrictIdNextTemp = 0;
            int iRestrictIdNext = 0;
            PiXRestrictList objPiRestrictList = objEmployee.PiXRestrictList;

            if (objEmployee.PiXRestrictList.Count > 0)
            {
                foreach (PiXRestrict objRestrict in objPiRestrictList)
                {
                    iRestrictId = objRestrict.PiRestrictRowId;
                    if (objPiXRestrict.PiRestrictRowId <= 0)
                        break;
                    else if (iRestrictId < objPiXRestrict.PiRestrictRowId)
                        break;

                }
                objPiRestrictList = objEmployee.PiXRestrictList;
                if (objPiXRestrict.PiRestrictRowId > 0)
                {
                    foreach (PiXRestrict objRestrict in objPiRestrictList)
                    {
                        iRestrictIdNextTemp = objRestrict.PiRestrictRowId;
                        if (iRestrictIdNextTemp > objPiXRestrict.PiRestrictRowId)
                        {
                            iRestrictIdNext = iRestrictIdNextTemp;
                        }
                        if (iRestrictIdNextTemp == objPiXRestrict.PiRestrictRowId)
                        {
                            if (iRestrictIdNext == 0)
                            {
                                iRestrictIdNext = iRestrictIdNextTemp;
                            }
                            break;
                        }

                    }
                }
                if (objPiXRestrict.PiRestrictRowId != iRestrictId)
                {
                    dtFirstRestrict = Conversion.ToDate(p_DateFirstRestrct);
                    dtLastRestrict = Conversion.ToDate(objPiRestrictList[iRestrictId].DateLastRestrct);
                    if (dtFirstRestrict.CompareTo(dtLastRestrict) <= 0)
                    {
                        sWarningMessage = "You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                        //m_arrlstWarnings.Add("You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                        // Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                        return false;
                    }
                }
                if (objPiXRestrict.PiRestrictRowId != iRestrictIdNext)
                {
                    if (objPiXRestrict.PiRestrictRowId > 0)
                    {
                        objPiRestrictList = objEmployee.PiXRestrictList;
                        dtFirstRestrict = Conversion.ToDate(objPiRestrictList[iRestrictIdNext].DateFirstRestrct);
                        dtLastRestrict = Conversion.ToDate(p_DateLastRestrct);
                        if (dtFirstRestrict.CompareTo(dtLastRestrict) <= 0)
                        {
                            sWarningMessage = "You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString();
                            //m_arrlstWarnings.Add("You have entered overlapping Restricted dates, a Restricted days record will not be created/updated for the transaction with amount " + p_SumAmount.ToString());
                            //  Errors.Add("WarningScriptError", sWarningMessage, BusinessAdaptorErrorType.Warning);
                            return false;
                        }
                    }
                }

            }
            return true;
        }

        //igupta3 Mits: 28566 Changes ends

        /// Name		: CheckBatchChangedPost
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Batch Changed event for Post register checks
        /// </summary>
        /// <param name="p_iBatchNumber">Batch Number</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <returns>Returns the Out put XML document contains the information for CheckBatchChangedPost.</returns>
        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        public XmlDocument CheckBatchChangedPost(int p_iBatchNumber, int p_iAccountId, int p_iDistributionType)
        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        {
            return (this.CheckBatchChangedPost(p_iBatchNumber, p_iAccountId, false, p_iDistributionType));
        }

        /// Name		: CheckBatchChangedPost
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Batch Changed event for Post register checks.
        /// </summary>
        /// <param name="p_iBatchNumber">Batch Number</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <param name="p_bDocStarted">If Document Generation Has Started then true</param>
        /// <returns>Returns the Out put XML document contains the information for CheckBatchChangedPost.</returns>
        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        private XmlDocument CheckBatchChangedPost(int p_iBatchNumber, int p_iAccountId, bool p_bDocStarted, int p_iDistributionType)
        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        {
            CCacheFunctions objCCacheFunctions = null;
            DbReader objReader = null;
            XmlElement objRootNode = null;
            StringBuilder sbSQL = null;

            int iChecksCount = 0;
            string sCheckDate = "";
            string sOrderByField = "";

            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                sbSQL = new StringBuilder();

                if (!p_bDocStarted) // If document genearation has not started.
                {
                    Functions.StartDocument(ref m_objDocument, ref objRootNode, ref m_objMessagesNode, "CheckBatchChangedPost", m_iClientId);
                }
                else
                {
                    Functions.CreateElement(m_objDocument.DocumentElement, "CheckBatchChangedPost", ref objRootNode, m_iClientId);
                }

                // Get number of post-check register eligible checks
                sbSQL.Append(" SELECT COUNT(*) FROM FUNDS ");
                sbSQL.Append(" WHERE FUNDS.BATCH_NUMBER = " + p_iBatchNumber);
                sbSQL.Append(" AND FUNDS.STATUS_CODE = ");
                sbSQL.Append(objCCacheFunctions.GetCodeIDWithShort("P", "CHECK_STATUS"));
                sbSQL.Append(" AND FUNDS.ACCOUNT_ID = " + p_iAccountId);
                sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0");
                sbSQL.Append(" AND FUNDS.VOID_FLAG = 0");
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        iChecksCount = Common.Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    }
                    objReader.Close();
                }

                sbSQL.Remove(0, sbSQL.Length);

                if (iChecksCount != 0)
                {
                    Functions.CreateAndSetElement(objRootNode, "NumberOfPayments", iChecksCount.ToString(), m_iClientId);

                    sbSQL.Append(" SELECT DISTINCT TRANS_NUMBER FROM FUNDS ");
                    sbSQL.Append(" WHERE BATCH_NUMBER = " + p_iBatchNumber);
                    sbSQL.Append(" AND STATUS_CODE = ");
                    sbSQL.Append(objCCacheFunctions.GetCodeIDWithShort("P", "CHECK_STATUS"));
                    sbSQL.Append(" AND ACCOUNT_ID = " + p_iAccountId);
                    sbSQL.Append(" AND PAYMENT_FLAG <> 0 AND VOID_FLAG = 0");
                    // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                    sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                    // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                    iChecksCount = 0;
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            iChecksCount++;
                        }
                        objReader.Close();
                    }
                    Functions.CreateAndSetElement(objRootNode, "NumberOfChecks", iChecksCount.ToString(), m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "PrintPost", true.ToString(), m_iClientId);

                    sbSQL.Remove(0, sbSQL.Length);
                    sbSQL.Append(" SELECT ORDER_BY_FIELD,DATE_RUN FROM CHECK_BATCHES WHERE ACCOUNT_ID = ");
                    sbSQL.Append(p_iAccountId + " AND BATCH_NUMBER = " + p_iBatchNumber);

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            sOrderByField = objReader.GetString("ORDER_BY_FIELD");
                            sCheckDate = Conversion.GetDBDateFormat(objReader.GetString("DATE_RUN"), "d");
                        }
                        else
                        {
                            sOrderByField = "";
                            sCheckDate = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckSetup.CheckBatchChangedPost.NotRun", m_iClientId));
                        }
                        objReader.Close();
                    }

                    if (sOrderByField == "ENTITY.ENTITY_TABLE_ID" || sOrderByField == "ADJUSTER_EID")
                        sOrderByField = "";

                    Functions.CreateAndSetElement(objRootNode, "OrderPostCheck", Functions.ReturnSortOrder(sOrderByField, m_iClientId), m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "DatePostCheck", sCheckDate, m_iClientId);
                }
                else
                {
                    sCheckDate = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckSetup.CheckBatchChangedPost.NotRun", m_iClientId));

                    Functions.CreateAndSetElement(objRootNode, "NumberOfPayments", "(None)", m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "NumberOfChecks", "(None)", m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "PrintPost", false.ToString(), m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "OrderPostCheck", Functions.ReturnSortOrder(sOrderByField, m_iClientId), m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "DatePostCheck", sCheckDate, m_iClientId);
                }
                Functions.CreateAndSetElement(objRootNode, "CheckBatch", p_iBatchNumber.ToString(), m_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.CheckBatchChangedPost.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCCacheFunctions = null;
                objRootNode = null;
                if (objReader != null)
                {
                    //objReader.Close();
                    objReader.Dispose();
                }
                sbSQL = null;
            }
            return (m_objDocument);
        }

        /// Name		: CheckBatchChangedPre
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Batch Changed event for Pre register checks
        /// </summary>
        /// <param name="p_iBatchNumber">Batch Number</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <returns>Returns the Out put XML document contains the information for CheckBatchChangedPre.</returns>
        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        public XmlDocument CheckBatchChangedPre(int p_iBatchNumber, int p_iAccountId, int p_iDistributionType)
        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        {
            return (this.CheckBatchChangedPre(p_iBatchNumber, p_iAccountId, false, p_iDistributionType));
        }

        /// Name		: CheckBatchChangedPre_ForReprint
        /// Author		: Ashutosh K Pandey
        /// Date Created: 10/05/2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Batch Changed event for Pre register checks
        /// </summary>
        /// <param name="p_iBatchNumber">Batch Number</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <returns>Returns the Out put XML document contains the information for CheckBatchChanged for Reprint.</returns>
        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        public XmlDocument CheckBatchChangedPre_ForRePrint(int p_iBatchNumber, int p_iAccountId, bool bReCreate, int p_iDistributionType)
        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        {
            return (this.CheckBatchChangedPre_ForRePrint(p_iBatchNumber, p_iAccountId, false, bReCreate, p_iDistributionType));
        }

        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Batch Changed event for Pre register checks.
        /// </summary>
        /// <param name="p_iBatchNumber">Batch Number</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <param name="p_bDocStarted">If Document Generation Has Started then true</param>
        /// <param name="bRecreate">To Set Reprinting of the check</param>
        /// <returns>Returns the Out put XML document contains the information for CheckBatchChanged for Reprint.</returns>
        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        private XmlDocument CheckBatchChangedPre_ForRePrint(int p_iBatchNumber, int p_iAccountId, bool p_bDocStarted, bool bRecreate, int p_iDistributionType)
        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        {
            CCacheFunctions objCCacheFunctions = null;
            DbReader objReader = null;
            DbReader objReaderCount = null;
            XmlElement objRootNode = null;
            StringBuilder sbSQL = null;

            int iChecksCount = 0;
            string sCheckDate = "";
            string sOrderByField = "";

            // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
            double dblCheckTotal = 0.0;
            double dblRoundedTotal = 0.0;
            // npadhy JIRA 6418 Ends We need to show the amount of Cheque on Print Check tab as well

            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                sbSQL = new StringBuilder();

                if (!p_bDocStarted) // If document genearation has not started.
                {
                    Functions.StartDocument(ref m_objDocument, ref objRootNode, ref m_objMessagesNode, "CheckBatchChangedPre", m_iClientId);
                }
                else
                {
                    Functions.CreateElement(m_objDocument.DocumentElement, "CheckBatchChangedPre", ref objRootNode, m_iClientId);
                }

                sbSQL.Append(" SELECT ORDER_BY_FIELD,DATE_RUN FROM CHECK_BATCHES WHERE ACCOUNT_ID = ");
                sbSQL.Append(p_iAccountId + " AND BATCH_NUMBER = " + p_iBatchNumber);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sOrderByField = objReader.GetString("ORDER_BY_FIELD");
                        sCheckDate = Conversion.GetDBDateFormat(objReader.GetString("DATE_RUN"), "d");

                        // Get total for checks to be printed per this batch (precheck register has been run)
                        sbSQL.Remove(0, sbSQL.Length);
                        // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append("SELECT COUNT(*), SUM(AMOUNT) AMOUNT FROM FUNDS");
                        // npadhy JIRA 6418 Ends We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append(" WHERE BATCH_NUMBER = " + p_iBatchNumber + " AND ACCOUNT_ID = ");
                        sbSQL.Append(p_iAccountId + " AND STATUS_CODE = ");
                        sbSQL.Append(objCCacheFunctions.GetCodeIDWithShort("P", "CHECK_STATUS"));
                        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        sbSQL.Append(" AND PRECHECK_FLAG <> 0");

                        objReaderCount = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        if (objReaderCount != null)
                        {
                            if (objReaderCount.Read())
                            {
                                iChecksCount = Common.Conversion.ConvertObjToInt(objReaderCount.GetValue(0), m_iClientId);
                                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                                dblRoundedTotal = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReaderCount.GetValue("AMOUNT")));
                                dblRoundedTotal = Math.Round(dblRoundedTotal, 2, MidpointRounding.AwayFromZero);
                                dblCheckTotal += dblRoundedTotal;
                                // npadhy JIRA 6418 Ends We need to show the amount of Cheque on Print Check tab as well
                            }
                            objReaderCount.Close();
                        }
                        // auto checks also
                        sbSQL.Remove(0, sbSQL.Length);
                        // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append("SELECT COUNT(*), SUM(AMOUNT) AMOUNT FROM FUNDS_AUTO, FUNDS_AUTO_BATCH ");
                        // npadhy JIRA 6418 Ends We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append(" WHERE FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID ");
                        sbSQL.Append(" AND CHECK_BATCH_NUM = " + p_iBatchNumber);
                        sbSQL.Append(" AND ACCOUNT_ID = " + p_iAccountId);
                        sbSQL.Append(" AND PRECHECK_FLAG <> 0");
                        sbSQL.Append(" AND ((FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL) ");
                        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        sbSQL.Append(" OR (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1)) ");

                        objReaderCount = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        if (objReaderCount != null)
                        {
                            if (objReaderCount.Read())
                            {
                                iChecksCount += Common.Conversion.ConvertObjToInt(objReaderCount.GetValue(0), m_iClientId);
                                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                                dblRoundedTotal = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReaderCount.GetValue("AMOUNT")));
                                dblRoundedTotal = Math.Round(dblRoundedTotal, 2, MidpointRounding.AwayFromZero);
                                dblCheckTotal += dblRoundedTotal;
                                // npadhy JIRA 6418 Ends We need to show the amount of Cheque on Print Check tab as well
                            }
                            objReaderCount.Close();
                        }
                    }
                    objReader.Close();
                }
                if (sCheckDate.Trim() == "")
                    sCheckDate = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckSetup.CheckBatchChangedPre.NotRun", m_iClientId));

                Functions.CreateAndSetElement(objRootNode, "CheckBatch", p_iBatchNumber.ToString(), m_iClientId);

                if (iChecksCount > 0)
                    Functions.CreateAndSetElement(objRootNode, "NumberOfChecks", iChecksCount.ToString(), m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "NumberOfChecks", "(None)", m_iClientId);
                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                Functions.CreateAndSetElement(objRootNode, "TotalAmountsPrint", CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblCheckTotal, m_sConnectionString, m_iClientId), m_iClientId);
                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                Functions.CreateAndSetElement(objRootNode, "OrderPreCheck", Functions.ReturnSortOrder(sOrderByField, m_iClientId), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "DatePreCheck", sCheckDate, m_iClientId);

                // npadhy JIRA RMA-11632 - Before Distributon Type if the Print Option was Only printer, then we used to hide the Recreate Checks File link from MDI
                // Now since we have taken this setting from System wide to per distribution type, so we will not hide the link, rather disable Re-Print button and Show Proper Message
                // Retreive the Print Options
                m_iExportToFile = GetPrintOptions(p_iDistributionType);

                if (m_iExportToFile == 0)
                {
                    Functions.CreateAndSetElement(objRootNode, "PrintBatch", 0.ToString(), m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "RecreateError", Globalization.GetString("CheckSetup.CheckBatchChangedPre_ForReprint.Validation", m_iClientId), m_iClientId);
                }
                else
                {
                    Functions.CreateAndSetElement(objRootNode, "PrintBatch", (iChecksCount > 0).ToString(), m_iClientId);
                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.CheckBatchChangedPre_ForReprint.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCCacheFunctions = null;
                objRootNode = null;
                if (objReader != null)
                {
                    //objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderCount != null)
                {
                    //objReaderCount.Close();
                    objReaderCount.Dispose();
                }
                sbSQL = null;
            }
            return (m_objDocument);
        }

        /// Name		: CheckBatchChangedPre
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Batch Changed event for Pre register checks.
        /// </summary>
        /// <param name="p_iBatchNumber">Batch Number</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <param name="p_bDocStarted">If Document Generation Has Started then true</param>
        /// <returns>Returns the Out put XML document contains the information for CheckBatchChangedPre.</returns>
        // npadhy RMA-11632 We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        internal XmlDocument CheckBatchChangedPre(int p_iBatchNumber, int p_iAccountId, bool p_bDocStarted, int p_iDistributionType, bool p_bRecreateCheck = false)
        {
            CCacheFunctions objCCacheFunctions = null;
            DbReader objReader = null;
            DbReader objReaderCount = null;
            XmlElement objRootNode = null;
            StringBuilder sbSQL = null;

            int iChecksCount = 0;
            // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
            double dblCheckTotal = 0.0;
            double dblRoundedTotal = 0.0;
            // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
            string sCheckDate = "";
            string sOrderByField = "";

            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                sbSQL = new StringBuilder();

                if (!p_bDocStarted) // If document genearation has not started.
                {
                    Functions.StartDocument(ref m_objDocument, ref objRootNode, ref m_objMessagesNode, "CheckBatchChangedPre", m_iClientId);
                }
                else
                {
                    Functions.CreateElement(m_objDocument.DocumentElement, "CheckBatchChangedPre", ref objRootNode, m_iClientId);
                }

                sbSQL.Append(" SELECT ORDER_BY_FIELD,DATE_RUN FROM CHECK_BATCHES WHERE ACCOUNT_ID = ");
                sbSQL.Append(p_iAccountId + " AND BATCH_NUMBER = " + p_iBatchNumber);

                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sOrderByField = objReader.GetString("ORDER_BY_FIELD");
                        sCheckDate = Conversion.GetDBDateFormat(objReader.GetString("DATE_RUN"), "d");

                        // Get total for checks to be printed per this batch (precheck register has been run)
                        sbSQL.Remove(0, sbSQL.Length);
                        // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append("SELECT COUNT(*), SUM(AMOUNT) AMOUNT FROM FUNDS");
                        // npadhy JIRA 6418 Ends We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append(" WHERE BATCH_NUMBER = " + p_iBatchNumber + " AND ACCOUNT_ID = ");
                        sbSQL.Append(p_iAccountId + " AND STATUS_CODE = ");
                        // npadhy RMA-11632 - If the check is getting printed for the first time then we need to retrive the checks with status Released
                        if (!p_bRecreateCheck)
                        {
                            sbSQL.Append(objCCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS"));
                        }
                        else
                        {
                            // npadhy RMA-11632 - If the check is getting reprinted then we need to retrive the checks with status Printed
                            sbSQL.Append(objCCacheFunctions.GetCodeIDWithShort("P", "CHECK_STATUS"));
                        }
                        sbSQL.Append(" AND PRECHECK_FLAG <> 0");
                        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

                        objReaderCount = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        if (objReaderCount != null)
                        {
                            if (objReaderCount.Read())
                            {
                                iChecksCount = Common.Conversion.ConvertObjToInt(objReaderCount.GetValue(0), m_iClientId);
                                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                                dblRoundedTotal = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReaderCount.GetValue("AMOUNT")));
                                dblRoundedTotal = Math.Round(dblRoundedTotal, 2, MidpointRounding.AwayFromZero);
                                dblCheckTotal += dblRoundedTotal;
                                // npadhy JIRA 6418 Ends We need to show the amount of Cheque on Print Check tab as well
                            }
                            objReaderCount.Close();
                        }
                        // auto checks also
                        sbSQL.Remove(0, sbSQL.Length);
                        // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append("SELECT COUNT(*), SUM(AMOUNT) AMOUNT FROM FUNDS_AUTO, FUNDS_AUTO_BATCH ");
                        // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                        sbSQL.Append(" WHERE FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID ");
                        sbSQL.Append(" AND CHECK_BATCH_NUM = " + p_iBatchNumber);
                        sbSQL.Append(" AND ACCOUNT_ID = " + p_iAccountId);
                        sbSQL.Append(" AND PRECHECK_FLAG <> 0");
                        sbSQL.Append(" AND ((FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL) ");
                        sbSQL.Append(" OR (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1)) ");
                        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                        sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());

                        objReaderCount = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        if (objReaderCount != null)
                        {
                            if (objReaderCount.Read())
                            {
                                iChecksCount += Common.Conversion.ConvertObjToInt(objReaderCount.GetValue(0), m_iClientId);
                                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                                dblRoundedTotal = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReaderCount.GetValue("AMOUNT")));
                                dblRoundedTotal = Math.Round(dblRoundedTotal, 2, MidpointRounding.AwayFromZero);
                                dblCheckTotal += dblRoundedTotal;
                                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                            }
                            objReaderCount.Close();
                        }
                    }
                    objReader.Close();
                }
                if (sCheckDate.Trim() == "")
                    sCheckDate = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckSetup.CheckBatchChangedPre.NotRun", m_iClientId));

                Functions.CreateAndSetElement(objRootNode, "CheckBatch", p_iBatchNumber.ToString(), m_iClientId);

                if (iChecksCount > 0)
                    Functions.CreateAndSetElement(objRootNode, "NumberOfChecks", iChecksCount.ToString(), m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "NumberOfChecks", "(None)", m_iClientId);
                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                Functions.CreateAndSetElement(objRootNode, "TotalAmountsPrint", CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblCheckTotal, m_sConnectionString, m_iClientId), m_iClientId);
                // npadhy JIRA 6418 Starts We need to show the amount of Cheque on Print Check tab as well
                Functions.CreateAndSetElement(objRootNode, "OrderPreCheck", Functions.ReturnSortOrder(sOrderByField, m_iClientId), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "DatePreCheck", sCheckDate, m_iClientId);

                // npadhy JIRA RMA-11632 If the Check is printed again and Print Option is set "To printer". we do not want user to print again. So we disable the Print Button
                if ((iChecksCount > 0 && !p_bRecreateCheck) || (iChecksCount > 0 && p_bRecreateCheck && m_iExportToFile != 0))
                    Functions.CreateAndSetElement(objRootNode, "PrintBatch", true.ToString(), m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "PrintBatch", false.ToString(), m_iClientId);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.CheckBatchChangedPre.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCCacheFunctions = null;
                objRootNode = null;
                if (objReader != null)
                {
                    //objReader.Close();
                    objReader.Dispose();
                }
                if (objReaderCount != null)
                {
                    //objReaderCount.Close();
                    objReaderCount.Dispose();
                }
                sbSQL = null;
            }
            return (m_objDocument);
        }

        #endregion

        #region CheckSet Changed
        /*
		 * SAMPLE XML 
		 * 
		 <CheckSetChanged>
			<AccountId>1</AccountId> 
			<IncludeAutoPayment>1</IncludeAutoPayment> 
			<UsingSelection>1</UsingSelection>
			<AttachedClaimOnly>0</AttachedClaimOnly> 
			<FromDateFlag>0</FromDateFlag> 
			<FromDate /> 
			<ToDateFlag>0</ToDateFlag> 
			<ToDate>20041231</ToDate>
			<AutoTransIds></AutoTransIds>
			<SelectedChecksIds>123,124</SelectedChecksIds>
			<SelectedAutoChecksIds>125,128</SelectedAutoChecksIds>			
		</CheckSetChanged>
		*/
        /// Name		: CheckSetChanged
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Set Changed event.
        /// </summary>
        /// <param name="p_objInputDoc">Input Xml Doc</param>
        /// <returns>Returns the Out put XML document contains the information for CheckSetChanged.</returns>
        public XmlDocument CheckSetChanged(XmlDocument p_objInputDoc)
        {
            ArrayList arrlstSelectedAutoCheck = null;
            ArrayList arrlstSelectedCheck = null;

            bool bUseFromDate = false;
            bool bUseToDate = false;
            bool bWithAttachedClaimsOnly = false;
            bool bIncludeAutoPays = false;
            bool bUsingSelections = false;
            string sFromDateValue = "";
            string sToDateValue = "";
            //Added by Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string sOrgHierarchy = string.Empty;
            int iAccountId = 0;
            //Added by Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            int iOrgHierarchyLevel = 0;
            //skhare7 R8 enhancement
            bool bIncludeCombinedPays = false;
            //skhare7 R8
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing EFTPayment
            //bool bPrintEFTPayment = false;//JIRA:438 START: ajohari2
            int iDistributionType = 0;
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing EFTPayment
            try
            {
                bUseFromDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "FromDateFlag", m_iClientId));
                bUseToDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "ToDateFlag", m_iClientId));
                bWithAttachedClaimsOnly = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "AttachedClaimOnly", m_iClientId));
                bIncludeAutoPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeAutoPayment", m_iClientId));
                //skhare7 R8 enhancement
                bIncludeCombinedPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeCombinedPayment", m_iClientId));
                //skhare7 R8
                bUsingSelections = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "UsingSelection", m_iClientId));
                sFromDateValue = Functions.GetValue(p_objInputDoc, "FromDate", m_iClientId);
                sToDateValue = Functions.GetValue(p_objInputDoc, "ToDate", m_iClientId);
                iAccountId = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "AccountId", m_iClientId));
                //Added by Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                iOrgHierarchyLevel = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "OrgHierarchyLevel", m_iClientId));
                //Added by Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                sOrgHierarchy = Functions.GetValue(p_objInputDoc, "OrgHierarchy", m_iClientId);
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //bPrintEFTPayment = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "PrintEFTPayment", m_iClientId));
                //JIRA:438 End:
                iDistributionType = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "DistributionType", m_iClientId));
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                if (bUsingSelections)
                {
                    arrlstSelectedAutoCheck = Functions.GetList(p_objInputDoc, "AutoTransIds", ",", m_iClientId);
                    arrlstSelectedCheck = Functions.GetList(p_objInputDoc, "TransIds", ",", m_iClientId);
                }
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel) 
                //return (this.CheckSetChanged(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue,
                // bWithAttachedClaimsOnly, bIncludeAutoPays, iAccountId, false, bUsingSelections, arrlstSelectedCheck, arrlstSelectedAutoCheck));
                //skhare7 R8 enhancement Combined pay
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                return (this.CheckSetChanged(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue,
                    bWithAttachedClaimsOnly, bIncludeAutoPays, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, false, bUsingSelections, arrlstSelectedCheck, arrlstSelectedAutoCheck, bIncludeCombinedPays, iDistributionType));
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 End:
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.CheckSetChanged.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: CheckSetChanged
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Check Set Changed event.
        /// </summary>
        /// <param name="p_bUseFromDate">Use From Date Flag</param>
        /// <param name="p_bUseToDate">Use To Date Flag</param>
        /// <param name="p_sFromDateValue">From Date</param>
        /// <param name="p_sToDateValue">To Date</param>
        /// <param name="p_bWithAttachedClaimsOnly">Attached Claims Only Flag</param>
        /// <param name="p_bIncludeAutoPays">Include Auto Payments Flag</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <param name="p_bDocStarted">If Document Generation Has Started then true</param>
        /// <param name="p_bUsingSelections">Is Using Selection</param>
        /// <param name="p_arrlstSelectedCheck">Selected List of Pre Checks</param>
        /// <param name="p_arrlstSelectedAutoCheck">Selected List of Pre Auto Checks</param>	
        /// <returns>Returns the Out put XML document contains the information for CheckSetChanged.</returns>
        // akaushik5 Changed for MITS 35846 Starts
        //        internal XmlDocument CheckSetChanged( bool p_bUseFromDate, bool p_bUseToDate, string p_sFromDateValue , 
        //            string p_sToDateValue, bool p_bWithAttachedClaimsOnly, bool p_bIncludeAutoPays , 
        //            int p_iAccountId,string p_sOrgHierarchy,int p_iOrgHierarchyLevel, bool p_bDocStarted , bool p_bUsingSelections, ArrayList p_arrlstSelectedCheck, 
        //            ArrayList p_arrlstSelectedAutoCheck,bool p_bIncludeCombinedPay )
        //        {
        //            CCacheFunctions objCCacheFunctions = null;
        //            DbReader objReader = null ;				
        //            XmlElement objRootNode = null ;
        //            Functions objFunctions = null ;
        //            StringBuilder sbSQL = null;

        //            string sFromDate = "" ;
        //            string sToDate = "" ;
        //            string sAlias = "" ;
        //            int iCheckCount = 0 ;
        //            int iThisTransId = 0 ;
        //            int iLastTransID = 0 ;
        //            double dblRoundedAmount = 0.0 ;
        //            double dblTotal = 0.0 ;						 
        //            //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //            string sOrgLevelField=string.Empty ;
        //            string sOrgHierarchyList = string.Empty;
        //            int iTableId = 0;
        //            int iDeptId = 0;
        //            string sOrgLevelFieldValue = string.Empty;
        //            string sOrgLevelFieldList = string.Empty;
        //            string sDeptIds = string.Empty;
        //            //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010

        //            try
        //            {
        //                // changed by nadim for  13685-End
        //                objCCacheFunctions = new CCacheFunctions( m_sConnectionString );
        //                objFunctions = new Functions( m_objDataModelFactory );
        //                sbSQL = new StringBuilder();

        //                if( !p_bDocStarted ) // If document genearation has not started.
        //                {
        //                    Functions.StartDocument( ref m_objDocument , ref objRootNode , ref m_objMessagesNode , "CheckSetChanged" );					
        //                }
        //                else
        //                {					
        //                    Functions.CreateElement( m_objDocument.DocumentElement , "CheckSetChanged" , ref objRootNode );
        //                }				

        //                if ( Functions.m_sDBType == Constants.DB_ACCESS )
        //                    sAlias = " AS " ;
        //                else
        //                    sAlias = " " ;

        //                if( Utilities.IsDate( p_sFromDateValue ) && p_bUseFromDate )
        //                    sFromDate = Conversion.GetDate( p_sFromDateValue );
        //                else
        //                    sFromDate = "" ;					

        //                if( Utilities.IsDate( p_sToDateValue ) && p_bUseToDate )
        //                    sToDate = Conversion.GetDate( p_sToDateValue );
        //                else
        //                    sToDate = "" ;

        //                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                if (p_sOrgHierarchy != "")
        //                {
        //                    sOrgHierarchyList = p_sOrgHierarchy.Replace(" ", ",");
        //                }
        //                switch (p_iOrgHierarchyLevel)
        //                {
        //                    case 1005: sOrgLevelField = "CLIENT_EID";break;
        //                    case 1006: sOrgLevelField = "COMPANY_EID"; break;
        //                    case 1007: sOrgLevelField = "OPERATION_EID"; break;
        //                    case 1008: sOrgLevelField = "REGION_EID"; break;
        //                    case 1009: sOrgLevelField = "DIVISION_EID"; break;
        //                    case 1010: sOrgLevelField = "LOCATION_EID"; break;
        //                    case 1011: sOrgLevelField = "FACILITY_EID"; break;
        //                    case 1012: sOrgLevelField = "DEPARTMENT_EID"; break;
        //                }


        //                if (sOrgHierarchyList != "")
        //                {
        //                    string[] arrOrgList = sOrgHierarchyList.Split(',');

        //                    foreach (string sOrgLevel in arrOrgList)
        //                    {
        //                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + sOrgLevel + ")");
        //                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //                        if (objReader != null)
        //                        {
        //                            while (objReader.Read())
        //                            {
        //                                iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);

        //                            }
        //                            objReader.Close();
        //                        }
        //                        switch (iTableId)
        //                        {
        //                            case 1005: sOrgLevelFieldValue = "CLIENT_EID=" + sOrgLevel + " "; break;
        //                            case 1006: sOrgLevelFieldValue = "COMPANY_EID=" + sOrgLevel + " "; break;
        //                            case 1007: sOrgLevelFieldValue = "OPERATION_EID=" + sOrgLevel + " "; break;
        //                            case 1008: sOrgLevelFieldValue = "REGION_EID=" + sOrgLevel + " "; break;
        //                            case 1009: sOrgLevelFieldValue = "DIVISION_EID=" + sOrgLevel + " "; break;
        //                            case 1010: sOrgLevelFieldValue = "LOCATION_EID=" + sOrgLevel + " "; break;
        //                            case 1011: sOrgLevelFieldValue = "FACILITY_EID=" + sOrgLevel + " "; break;
        //                            case 1012: sOrgLevelFieldValue = "DEPARTMENT_EID=" + sOrgLevel + " "; break;

        //                        }
        //                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + "OR ";                        
        //                        sbSQL.Remove(0, sbSQL.Length);

        //                    }
        //                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0,sOrgLevelFieldList.Length - 3);
        //                    sbSQL.Append("SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (" + sOrgLevelFieldList +")");
        //                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //                    if (objReader != null)
        //                    {
        //                        while (objReader.Read())
        //                        {
        //                            iDeptId = Common.Conversion.ConvertObjToInt(objReader.GetValue("DEPARTMENT_EID"), m_iClientId);
        //                            sDeptIds = iDeptId + "," + sDeptIds;
        //                        }
        //                        objReader.Close();
        //                    }
        //                    sbSQL.Remove(0, sbSQL.Length);
        //                    //rsushilaggar MITS 21453 04-Aug-2010
        //                    if (!string.IsNullOrEmpty(sDeptIds))
        //                        sDeptIds = sDeptIds.Substring(0, sDeptIds.Length - 1);
        //                    else
        //                        sDeptIds = "0";
        //                }
        //                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                #region For Pre Checks
        //                sbSQL.Append( " SELECT FUNDS.TRANS_ID " + sAlias + " FUNDS_TRANS_ID,FUNDS.AMOUNT, " );
        //                //Commented below by debabrata 03/17/2010
        //                //sbSQL.Append( " SELECT FUNDS_TRANS_SPLIT.TRANS_ID " + sAlias + " FUNDS_TRANS_ID, " );
        //                sbSQL.Append( " FUNDS_TRANS_SPLIT.AMOUNT,FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE" );

        //                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                if(sOrgHierarchyList != "")
        //                sbSQL.Append(",(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS.CLAIM_ID)) AS ORG_LEVEL ");
        //                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010

        //// JP 05.15.2006   Removed use of Oracle functions. No longer needed in 9i, 10g.
        ////				if ( Functions.m_sDBType == Constants.DB_ORACLE )
        ////				{
        ////					objFunctions.OracleFunctions(1);
        ////					sbSQL.Append( ",getFrzFlag(FUNDS.CLAIM_ID) " + sAlias + " PAYMNT_FROZEN_FLAG" );
        ////					sbSQL.Append( ",getClaimType(FUNDS.CLAIM_ID)" + sAlias + " CLAIM_TYPE_CODE" );
        ////					sbSQL.Append( ",getLOBCode(FUNDS.CLAIM_ID)" + sAlias + " LINE_OF_BUS_CODE" );
        ////					sbSQL.Append( ",getFrzPayee(FUNDS.PAYEE_EID)" + sAlias + " FREEZE_PAYMENTS" );
        ////				}
        ////				else
        ////				{
        //                    sbSQL.Append( ",(SELECT PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID) " );
        //                    sbSQL.Append( sAlias + " PAYMNT_FROZEN_FLAG" );        
        //                    sbSQL.Append( ",(SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID)" );
        //                    sbSQL.Append( sAlias + " CLAIM_TYPE_CODE" );
        //                    sbSQL.Append( ",(SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID)" );
        //                    sbSQL.Append( sAlias + " LINE_OF_BUS_CODE" );
        //                    sbSQL.Append( ",(SELECT FREEZE_PAYMENTS FROM ENTITY WHERE ENTITY.ENTITY_ID = FUNDS.PAYEE_EID)" );
        //                    sbSQL.Append( sAlias + " FREEZE_PAYMENTS" ) ;        
        ////				}

        //                sbSQL.Append( " FROM FUNDS, FUNDS_TRANS_SPLIT" );
        //                sbSQL.Append( " WHERE STATUS_CODE IN " );
        //                sbSQL.Append("(" + objCCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS") + "," + objCCacheFunctions.GetCodeIDWithShort("I", "CHECK_STATUS") + ")");
        //                sbSQL.Append( " AND ACCOUNT_ID = " + p_iAccountId );

        //                if( sToDate != "" ) 
        //                    sbSQL.Append( " AND TRANS_DATE <= '" + sToDate + "'" );
        //                if( sFromDate != "" )
        //                    sbSQL.Append( " AND TRANS_DATE >= '" + sFromDate + "'" );

        //                sbSQL.Append( " AND PAYMENT_FLAG <> 0" );

        //                if( !p_bIncludeAutoPays )
        //                    sbSQL.Append( " AND AUTO_CHECK_FLAG = 0" );

        //                sbSQL.Append( " AND PRECHECK_FLAG = 0 AND VOID_FLAG = 0" );
        //                //nadim for 13685
        //                if (Functions.AllowZeroPayments())
        //                {
        //                    sbSQL.Append(" AND FUNDS.AMOUNT >= 0");
        //                }
        //                else
        //                {
        //                sbSQL.Append( " AND FUNDS.AMOUNT > 0" );
        //                }
        //                //nadim for 13685
        //                if( p_bWithAttachedClaimsOnly )
        //                    sbSQL.Append( " AND FUNDS.CLAIM_ID <> 0" );
        //                //skhare7 R8 Enhancement Combined Pay
        //                if (!p_bIncludeCombinedPay)
        //                    sbSQL.Append(" AND COMBINED_PAY_FLAG = 0");
        //                //skhare7 End

        //                sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID" );
        //                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                if(sOrgHierarchyList !="")
        //                    sbSQL.Append(" AND CLAIM_ID IN (SELECT CLAIM_ID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND EV.DEPT_EID IN (" + sDeptIds + "))");
        //                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                sbSQL.Append( " ORDER BY FUNDS.TRANS_ID" );

        //                dicMaxAmounts = null;
        //                dicMaxAmounts = GetMaxPrintAmounts(m_iUserId, m_iUserGroupId, m_sConnectionString);

        //                objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
        //                if( objReader != null )
        //                {
        //                    while( objReader.Read() )
        //                    {
        //                        iThisTransId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FUNDS_TRANS_ID" ), m_iClientId );
        //                        if( iThisTransId != iLastTransID )
        //                        {
        //                            if( !this.SkipPayment( objReader , false , "AMOUNT" , p_bUsingSelections , p_arrlstSelectedCheck , p_arrlstSelectedAutoCheck ))
        //                            {
        //                                iCheckCount++ ;
        //                                dblRoundedAmount = Conversion.ConvertStrToDouble( Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
        //                                dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
        //                                dblTotal += dblRoundedAmount ;
        //                            }
        //                            iLastTransID = iThisTransId ;
        //                        }
        //                    }
        //                    objReader.Close();
        //                }
        //                #endregion 

        //                sbSQL.Remove(0,sbSQL.Length);

        //                #region For Auto Checks
        //                if( p_bIncludeAutoPays )
        //                {
        //                    sbSQL.Append( " SELECT FUNDS_AUTO.AUTO_TRANS_ID " + sAlias + " FUNDS_TRANS_ID,FUNDS_AUTO.AMOUNT," );
        //                    sbSQL.Append( " FUNDS_AUTO_SPLIT.AMOUNT,FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE" );

        //// JP 05.15.2006   Removed use of Oracle functions. No longer needed in 9i, 10g.
        ////					if ( Functions.m_sDBType == Constants.DB_ORACLE )
        ////					{
        ////						objFunctions.OracleFunctions(1);
        ////						sbSQL.Append( ",getFrzFlag(FUNDS_AUTO.CLAIM_ID) " + sAlias + " PAYMNT_FROZEN_FLAG" );
        ////						sbSQL.Append( ",getClaimType(FUNDS_AUTO.CLAIM_ID)" + sAlias + " CLAIM_TYPE_CODE" );
        ////						sbSQL.Append( ",getLOBCode(FUNDS_AUTO.CLAIM_ID)" + sAlias + " LINE_OF_BUS_CODE" );
        ////						sbSQL.Append( ",getFrzPayee(FUNDS_AUTO.PAYEE_EID) " + sAlias + " FREEZE_PAYMENTS" );
        ////					}
        ////					else
        ////					{
        //                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                    if (sOrgHierarchyList != "")
        //                        sbSQL.Append(",(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)) AS ORG_LEVEL ");
        //                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                        sbSQL.Append( ",(SELECT PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID) " );
        //                        sbSQL.Append( sAlias + " PAYMNT_FROZEN_FLAG" );
        //                        sbSQL.Append( ",(SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)" );
        //                        sbSQL.Append( sAlias + " CLAIM_TYPE_CODE" );
        //                        sbSQL.Append( ",(SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)" );
        //                        sbSQL.Append( sAlias + " LINE_OF_BUS_CODE" );
        //                        sbSQL.Append( ",(SELECT FREEZE_PAYMENTS FROM ENTITY WHERE ENTITY.ENTITY_ID = FUNDS_AUTO.PAYEE_EID)" );
        //                        sbSQL.Append( sAlias + " FREEZE_PAYMENTS"  );        
        ////					}

        //                    sbSQL.Append( " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT, FUNDS_AUTO_BATCH WHERE " );
        //                    sbSQL.Append( " FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID " );
        //                    sbSQL.Append( " AND ACCOUNT_ID = " + p_iAccountId );
        //                    sbSQL.Append( " AND ((FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1) " );
        //                    sbSQL.Append( " OR (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL))" );

        //                    if( p_bUseToDate && sToDate != "" ) 
        //                        sbSQL.Append( " AND PRINT_DATE <= '" + sToDate + "'" );
        //                    if( p_bUseFromDate && sFromDate != "" )
        //                        sbSQL.Append( " AND PRINT_DATE >= '" + sFromDate + "'" );

        //                    sbSQL.Append( " AND PRECHECK_FLAG = 0" 	);			
        //                    //sbSQL.Append( " AND FUNDS_AUTO.AMOUNT > 0" ); 				 
        //                    sbSQL.Append(" AND FUNDS_AUTO.AMOUNT >= 0"); //pmittal5 Mits 14253 01/16/09 - Include Zero Payments				 
        //                    sbSQL.Append( " AND FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID" );
        //                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                    if (sOrgHierarchyList != "")
        //                        sbSQL.Append(" AND CLAIM_ID IN (SELECT CLAIM_ID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND EV.DEPT_EID IN (" + sDeptIds + "))");
        //                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010

        //                    objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
        //                    iLastTransID = -1 ;
        //                    if( objReader != null )
        //                    {
        //                        while( objReader.Read() )
        //                        {
        //                            iThisTransId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FUNDS_TRANS_ID" ), m_iClientId );
        //                            if( iThisTransId != iLastTransID )
        //                            {
        //                                if( !this.SkipPayment( objReader , true , "AMOUNT" , p_bUsingSelections , p_arrlstSelectedCheck , p_arrlstSelectedAutoCheck ))
        //                                {
        //                                    iCheckCount++ ;
        //                                    dblRoundedAmount = Conversion.ConvertStrToDouble( Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
        //                                    dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
        //                                    dblTotal += dblRoundedAmount ;
        //                                }
        //                                iLastTransID = iThisTransId ;
        //                            }
        //                        }
        //                        objReader.Close();
        //                    }
        //                }
        //                #endregion 

        //                Functions.CreateAndSetElement( objRootNode , "FromDateFlag" , p_bUseFromDate.ToString() );
        //                Functions.CreateAndSetElement( objRootNode , "FromDate" , p_sFromDateValue );
        //                Functions.CreateAndSetElement( objRootNode , "ToDateFlag" , p_bUseToDate.ToString() );
        //                Functions.CreateAndSetElement( objRootNode , "ToDate" , p_sToDateValue );		
        //                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                if (sOrgHierarchyList != "")
        //                {
        //                    string[] arrOrgList = sOrgHierarchyList.Split(',');
        //                    string name = "";
        //                    string abbr = "";
        //                    XmlElement objOrgHierarchyNode = null;
        //                    XmlElement objOptionNode = null;

        //                    Functions.CreateElement(objRootNode, "OrgHierarchy", ref objOrgHierarchyNode);
        //                    foreach (string sOrgLevel in arrOrgList)
        //                    {
        //                        objCCacheFunctions.GetOrgInfo(int.Parse(sOrgLevel), ref abbr, ref name);
        //                        Functions.CreateElement(objOrgHierarchyNode, "option", ref objOptionNode);

        //                        objOptionNode.SetAttribute("value", sOrgLevel);
        //                        //rsushilaggar MITS 21453 DATE 09/27/2010
        //                        string sOrgHierarchy = @"" + abbr + "-" + name;
        //                        sOrgHierarchy = sOrgHierarchy.Replace("&", "&amp;");
        //                        objOptionNode.InnerXml = @""+sOrgHierarchy+"";
        //                    }
        //                    objOrgHierarchyNode = null;
        //                    objOptionNode = null;
        //                }
        //                else
        //                {
        //                    Functions.CreateAndSetElement(objRootNode, "OrgHierarchy", p_sOrgHierarchy.ToString());
        //                }

        //                Functions.CreateAndSetElement(objRootNode, "OrgHierarchyLevel", p_iOrgHierarchyLevel.ToString());
        //                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                Functions.CreateAndSetElement( objRootNode , "IncludeAutoPaymentsFlag" , p_bIncludeAutoPays.ToString() );
        //                //skhare7 R8 enhancement
        //                Functions.CreateAndSetElement(objRootNode, "IncludeCombinedPaymentsFlag", p_bIncludeCombinedPay.ToString());
        //                //skhare7 R8 end
        //                Functions.CreateAndSetElement( objRootNode , "UsingSelection" , p_bUsingSelections.ToString() );

        //                Functions.CreateAndSetElement( objRootNode , "NumberOfPrechecks" , iCheckCount.ToString() );//skhare7 r8 Multicurrency
        //                Functions.CreateAndSetElement( objRootNode , "TotalAmounts" , CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblTotal, m_sConnectionString));//skhare7 r8 
        //                //Functions.CreateAndSetElement( objRootNode , "TotalAmounts" , string.Format( "{0:C}" , dblTotal) );
        //                Functions.CreateAndSetElement( objRootNode , "PrintFlag" , ( iCheckCount > 0 ).ToString() );	

        //            }
        //            catch( RMAppException p_objEx )
        //            {
        //                throw p_objEx ;
        //            }
        //            catch( Exception p_objEx )
        //            {
        //                throw new RMAppException(Globalization.GetString("CheckSetup.CheckSetChanged.Error") , p_objEx );				
        //            }
        //            finally
        //            {
        //                objCCacheFunctions = null;
        //                objFunctions = null;
        //                if( objReader != null )
        //                {
        //                    //objReader.Close();
        //                    objReader.Dispose();
        //                }
        //                objRootNode = null ;
        //                sbSQL = null ;
        //            }
        //            return( m_objDocument );
        //        }

        /// <summary>
        /// Get XML for Check Set Changed event.
        /// </summary>
        /// <param name="p_bUseFromDate">Use From Date Flag</param>
        /// <param name="p_bUseToDate">Use To Date Flag</param>
        /// <param name="p_sFromDateValue">From Date</param>
        /// <param name="p_sToDateValue">To Date</param>
        /// <param name="p_bWithAttachedClaimsOnly">Attached Claims Only Flag</param>
        /// <param name="p_bIncludeAutoPays">Include Auto Payments Flag</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <param name="p_sOrgHierarchy">The P_S org hierarchy.</param>
        /// <param name="p_iOrgHierarchyLevel">The p_i org hierarchy level.</param>
        /// <param name="p_bDocStarted">If Document Generation Has Started then true</param>
        /// <param name="p_bUsingSelections">Is Using Selection</param>
        /// <param name="p_arrlstSelectedCheck">Selected List of Pre Checks</param>
        /// <param name="p_arrlstSelectedAutoCheck">Selected List of Pre Auto Checks</param>
        /// <param name="p_bIncludeCombinedPay">if set to <c>true</c> [P_B include combined pay].</param>
        /// <returns>
        /// Returns the Out put XML document contains the information for CheckSetChanged.
        /// </returns>

        // npadhy RMA-11632  We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        internal XmlDocument CheckSetChanged(bool p_bUseFromDate, bool p_bUseToDate, string p_sFromDateValue,
       string p_sToDateValue, bool p_bWithAttachedClaimsOnly, bool p_bIncludeAutoPays,
       int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel, bool p_bDocStarted, bool p_bUsingSelections, ArrayList p_arrlstSelectedCheck,
       ArrayList p_arrlstSelectedAutoCheck, bool p_bIncludeCombinedPay, int p_iDistributionType, bool p_bRecreateCheck = false)// Removed the EFT and added the Distribution Type
        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        {
            CCacheFunctions objCCacheFunctions = null;
            DbReader objReader = null;
            XmlElement objRootNode = null;
            Functions objFunctions = null;
            StringBuilder sbSQL = null;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sAlias = string.Empty;
            int iCheckCount = 0;
            int iThisTransId = 0;
            int iLastTransID = 0;
            double dblRoundedAmount = 0.0;
            double dblTotal = 0.0;
            string sOrgLevelField = string.Empty;
            string sOrgHierarchyList = string.Empty;
            int iTableId = 0;
            string sOrgLevelFieldValue = string.Empty;
            string sOrgLevelFieldList = string.Empty;
            string sDeptIds = string.Empty;
            //int iEFTDistributionType = 0;
            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                objFunctions = new Functions(m_objDataModelFactory);
                sbSQL = new StringBuilder();

                if (!p_bDocStarted) // If document genearation has not started.
                {
                    Functions.StartDocument(ref m_objDocument, ref objRootNode, ref m_objMessagesNode, "CheckSetChanged", m_iClientId);
                }
                else
                {
                    Functions.CreateElement(m_objDocument.DocumentElement, "CheckSetChanged", ref objRootNode, m_iClientId);
                }

                sAlias = Functions.m_sDBType == Constants.DB_ACCESS ? "AS" : string.Empty;
                sFromDate = Utilities.IsDate(p_sFromDateValue) && p_bUseFromDate ? Conversion.GetDate(p_sFromDateValue) : string.Empty;
                sToDate = Utilities.IsDate(p_sToDateValue) && p_bUseToDate ? Conversion.GetDate(p_sToDateValue) : string.Empty;

                if (!string.IsNullOrEmpty(p_sOrgHierarchy))
                {
                    sOrgHierarchyList = p_sOrgHierarchy.Replace(" ", ",");
                }

                sOrgLevelField = CommonFunctions.GetOrgLevelFieldFromCodeId(p_iOrgHierarchyLevel);

                if (!string.IsNullOrEmpty(sOrgHierarchyList))
                {
                    string[] arrOrgList = sOrgHierarchyList.Split(',');

                    foreach (string sOrgLevel in arrOrgList)
                    {
                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + sOrgLevel + ")");
                        using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                        {
                            if (objReader != null)
                            {
                                while (objReader.Read())
                                {
                                    iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);
                                }
                                objReader.Close();
                            }
                        }

                        sOrgLevelFieldValue = string.Format("ORG_HIERARCHY.{0} = {1} ", CommonFunctions.GetOrgLevelFieldFromCodeId(iTableId), sOrgLevel);
                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + " OR ";
                        sbSQL.Remove(0, sbSQL.Length);

                    }
                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0, sOrgLevelFieldList.Length - 3);
                }
                #region For Pre Checks

                sbSQL.AppendFormat(" SELECT FUNDS.TRANS_ID {0} FUNDS_TRANS_ID,FUNDS.AMOUNT", sAlias);
                sbSQL.Append(", FUNDS_TRANS_SPLIT.AMOUNT, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE");

                if (!string.IsNullOrEmpty(sOrgHierarchyList))
                {
                    sbSQL.AppendFormat(", ORG_HIERARCHY.{0} {1} ORG_LEVEL", sOrgLevelField, sAlias);
                }

                sbSQL.AppendFormat(", CLAIM.PAYMNT_FROZEN_FLAG {0} PAYMNT_FROZEN_FLAG", sAlias);
                sbSQL.AppendFormat(", CLAIM.CLAIM_TYPE_CODE {0} CLAIM_TYPE_CODE", sAlias);
                sbSQL.AppendFormat(", CLAIM.LINE_OF_BUS_CODE {0} LINE_OF_BUS_CODE", sAlias);
                sbSQL.AppendFormat(", ENTITY.FREEZE_PAYMENTS {0} FREEZE_PAYMENTS", sAlias);

                sbSQL.Append(" FROM FUNDS");
                sbSQL.Append("  JOIN FUNDS_TRANS_SPLIT ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");
                sbSQL.Append(" LEFT OUTER JOIN CLAIM ON CLAIM.CLAIM_ID = FUNDS.CLAIM_ID");          //avipinsrivas : Worked for JIRA - 856
                sbSQL.Append(" JOIN ENTITY ON ENTITY.ENTITY_ID = FUNDS.PAYEE_EID");

                if (!string.IsNullOrEmpty(sOrgHierarchyList))
                {
                    sbSQL.Append(" LEFT OUTER JOIN EVENT ON CLAIM.EVENT_ID = EVENT.EVENT_ID");          //avipinsrivas : Worked for JIRA - 856
                    sbSQL.Append(" LEFT OUTER JOIN ORG_HIERARCHY ON ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID");          //avipinsrivas : Worked for JIRA - 856    
                }

                // npadhy RMA-11632 If the check is getting printed for the first time then we need to retrive the checks with status Released and Precheck is 0
                if (!p_bRecreateCheck)
                {
                    sbSQL.Append(" WHERE STATUS_CODE IN ");
                    sbSQL.Append("(" + objCCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS") + "," + objCCacheFunctions.GetCodeIDWithShort("I", "CHECK_STATUS") + ")");
                    sbSQL.Append("  AND FUNDS.PRECHECK_FLAG = 0 ");
                }
                    // If the Checks are getting printed again, the we need to retrieve only the printed checks
                else
                {
                    sbSQL.Append(" WHERE STATUS_CODE = ");
                    sbSQL.Append(objCCacheFunctions.GetCodeIDWithShort("P", "CHECK_STATUS"));
                }
                sbSQL.Append(" AND FUNDS.ACCOUNT_ID = " + p_iAccountId);
                sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0");
                sbSQL.Append("AND FUNDS.VOID_FLAG = 0");
                sbSQL.Append(" AND (CLAIM.PAYMNT_FROZEN_FLAG <> -1 OR CLAIM.PAYMNT_FROZEN_FLAG IS NULL)");
                sbSQL.Append(" AND (ENTITY.FREEZE_PAYMENTS <> -1 OR ENTITY.FREEZE_PAYMENTS IS NULL)");

                if (!string.IsNullOrEmpty(sToDate))
                {
                    sbSQL.AppendFormat(" AND FUNDS.TRANS_DATE <= '{0}'", sToDate);
                }

                if (!string.IsNullOrEmpty(sFromDate))
                {
                    sbSQL.AppendFormat(" AND TRANS_DATE >= '{0}'", sFromDate);
                }

                if (!p_bIncludeAutoPays)
                {
                    sbSQL.Append(" AND AUTO_CHECK_FLAG = 0");
                }

                sbSQL.AppendFormat(" AND FUNDS.AMOUNT {0} 0", Functions.AllowZeroPayments(m_sConnectionString, m_iClientId) ? ">=" : ">");

                if (p_bWithAttachedClaimsOnly)
                {
                    sbSQL.Append(" AND FUNDS.CLAIM_ID <> 0");
                }

                if (!p_bIncludeCombinedPay)
                {
                    sbSQL.Append(" AND COMBINED_PAY_FLAG = 0");
                }
                else
                {
                    if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                        sbSQL.Append(" AND FUNDS.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID");

                    if (!string.IsNullOrEmpty(sToDate))
                    {
                        sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT > '{0}'", sToDate);
                    }
                    if (!string.IsNullOrEmpty(sFromDate))
                    {
                        sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT < '{0}'", sFromDate);
                    }
                    if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                        sbSQL.Append(" )");
                }

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //if (p_bPrintEFTPayment)
                //{
                //    sbSQL.Append(" AND FUNDS.EFT_FLAG = -1");
                //}
                //else
                //{
                	// akaushik5 Changed for MITS 37841 Starts
                    //sbSQL.Append(" AND FUNDS.EFT_FLAG <> -1");
                    //sbSQL.Append(" AND (FUNDS.EFT_FLAG <> -1 OR FUNDS.EFT_FLAG IS NULL)");
                    // akaushik5 Changed for MITS 37841 Ends
                //}
                //JIRA:438 End: 
                sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                if (!string.IsNullOrEmpty(sOrgHierarchyList))
                {
                    sbSQL.AppendFormat("AND ({0})", sOrgLevelFieldList);
                }

                sbSQL.Append(" ORDER BY FUNDS.TRANS_ID");

                dicMaxAmounts = null;
                dicMaxAmounts = GetMaxPrintAmounts(m_iUserId, m_iUserGroupId, m_sConnectionString, m_iClientId);

                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            iThisTransId = Common.Conversion.ConvertObjToInt(objReader.GetValue("FUNDS_TRANS_ID"), m_iClientId);
                            if (iThisTransId != iLastTransID)
                            {
                                if (!this.SkipPayment(objReader, false, "AMOUNT", p_bUsingSelections, p_arrlstSelectedCheck, p_arrlstSelectedAutoCheck))
                                {
                                    iCheckCount++;
                                    dblRoundedAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT")));
                                    dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
                                    dblTotal += dblRoundedAmount;
                                }
                                iLastTransID = iThisTransId;
                            }
                        }
                        objReader.Close();
                    }
                }
                #endregion

                sbSQL.Remove(0, sbSQL.Length);

                #region For Auto Checks
                if (p_bIncludeAutoPays)
                {
                    sbSQL.AppendFormat("SELECT FUNDS_AUTO.AUTO_TRANS_ID {0} FUNDS_TRANS_ID,FUNDS_AUTO.AMOUNT", sAlias);
                    sbSQL.Append(", FUNDS_AUTO_SPLIT.AMOUNT,FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE");

                    if (!string.IsNullOrEmpty(sOrgHierarchyList))
                    {
                        sbSQL.AppendFormat(", ORG_HIERARCHY.{0} {1} ORG_LEVEL", sOrgLevelField, sAlias);
                    }

                    sbSQL.AppendFormat(", CLAIM.PAYMNT_FROZEN_FLAG {0} PAYMNT_FROZEN_FLAG", sAlias);
                    sbSQL.AppendFormat(", CLAIM.CLAIM_TYPE_CODE {0} CLAIM_TYPE_CODE", sAlias);
                    sbSQL.AppendFormat(", CLAIM.LINE_OF_BUS_CODE {0} LINE_OF_BUS_CODE", sAlias);
                    sbSQL.AppendFormat(", ENTITY.FREEZE_PAYMENTS {0} FREEZE_PAYMENTS", sAlias);

                    sbSQL.Append(" FROM FUNDS_AUTO");
                    sbSQL.Append(" JOIN FUNDS_AUTO_SPLIT ON FUNDS_AUTO_SPLIT.AUTO_TRANS_ID = FUNDS_AUTO.AUTO_TRANS_ID");
                    sbSQL.Append(" JOIN FUNDS_AUTO_BATCH ON FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID ");
                    sbSQL.Append(" LEFT OUTER JOIN CLAIM ON CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID");         //avipinsrivas : JIRA 856
                    sbSQL.Append(" JOIN ENTITY ON ENTITY.ENTITY_ID = FUNDS_AUTO.PAYEE_EID");

                    if (!string.IsNullOrEmpty(sOrgHierarchyList))
                    {
                        sbSQL.Append(" LEFT OUTER JOIN EVENT ON CLAIM.EVENT_ID = EVENT.EVENT_ID");          //avipinsrivas : JIRA 856
                        sbSQL.Append(" LEFT OUTER JOIN ORG_HIERARCHY ON ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID");        //avipinsrivas : JIRA 856
                    }

                    sbSQL.AppendFormat(" WHERE FUNDS_AUTO.ACCOUNT_ID = {0}", p_iAccountId);
                    sbSQL.Append(" AND (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1 OR FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL)");
                    sbSQL.Append(" AND PRECHECK_FLAG = 0");
                    sbSQL.Append(" AND FUNDS_AUTO.AMOUNT >= 0");
                    if (!p_bIncludeCombinedPay)
                    {
                        sbSQL.Append(" AND COMBINED_PAY_FLAG = 0");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                            sbSQL.Append(" AND FUNDS_AUTO.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS_AUTO.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID");

                        if (!string.IsNullOrEmpty(sToDate))
                        {
                            sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT > '{0}'", sToDate);
                        }
                        if (!string.IsNullOrEmpty(sFromDate))
                        {
                            sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT < '{0}'", sFromDate);
                        }
                        if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                            sbSQL.Append(" )");
                    }

                    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    //JIRA:438 START: ajohari2
                    //if (p_bPrintEFTPayment)
                    //{
                    //    sbSQL.Append(" AND FUNDS_AUTO.EFT_FLAG = -1");
                    //}
                    //else
                    //{
	                    // akaushik5 Changed for MITS 37841 Starts
	                    //sbSQL.Append(" AND FUNDS.EFT_FLAG <> -1");
	                    //sbSQL.Append(" AND (FUNDS.EFT_FLAG <> -1 OR FUNDS.EFT_FLAG IS NULL)");
	                    // akaushik5 Changed for MITS 37841 Ends
                    //}
                    //JIRA:438 End: 
                    sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    if (p_bUseToDate && !string.IsNullOrEmpty(sToDate))
                    {
                        sbSQL.AppendFormat(" AND PRINT_DATE <= '{0}'", sToDate);
                    }

                    if (p_bUseFromDate && !string.IsNullOrEmpty(sFromDate))
                    {
                        // akaushik5 Changed for MITS 38181 Starts
                        //sbSQL.AppendFormat(" AND PRINT_DATE <= '{0}'", sFromDate);
                        sbSQL.AppendFormat(" AND PRINT_DATE >= '{0}'", sFromDate);
                        // akaushik5 Changed for MITS 38181 Ends
                    }

                    if (!string.IsNullOrEmpty(sOrgHierarchyList))
                    {
                        sbSQL.AppendFormat("AND ({0})", sOrgLevelFieldList);
                    }

                    iLastTransID = -1;
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                    {
                        if (objReader != null)
                        {
                            while (objReader.Read())
                            {
                                iThisTransId = Common.Conversion.ConvertObjToInt(objReader.GetValue("FUNDS_TRANS_ID"), m_iClientId);
                                if (iThisTransId != iLastTransID)
                                {
                                    if (!this.SkipPayment(objReader, true, "AMOUNT", p_bUsingSelections, p_arrlstSelectedCheck, p_arrlstSelectedAutoCheck))
                                    {
                                        iCheckCount++;
                                        dblRoundedAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT")));
                                        dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
                                        dblTotal += dblRoundedAmount;
                                    }
                                    iLastTransID = iThisTransId;
                                }
                            }
                            objReader.Close();
                        }
                    }
                }
                #endregion

                Functions.CreateAndSetElement(objRootNode, "FromDateFlag", p_bUseFromDate.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "FromDate", p_sFromDateValue, m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "ToDateFlag", p_bUseToDate.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "ToDate", p_sToDateValue, m_iClientId);
                if (sOrgHierarchyList != "")
                {
                    string[] arrOrgList = sOrgHierarchyList.Split(',');
                    string name = "";
                    string abbr = "";
                    XmlElement objOrgHierarchyNode = null;
                    XmlElement objOptionNode = null;

                    Functions.CreateElement(objRootNode, "OrgHierarchy", ref objOrgHierarchyNode, m_iClientId);
                    foreach (string sOrgLevel in arrOrgList)
                    {
                        objCCacheFunctions.GetOrgInfo(int.Parse(sOrgLevel), ref abbr, ref name);
                        Functions.CreateElement(objOrgHierarchyNode, "option", ref objOptionNode, m_iClientId);

                        objOptionNode.SetAttribute("value", sOrgLevel);
                        string sOrgHierarchy = @"" + abbr + "-" + name;
                        sOrgHierarchy = sOrgHierarchy.Replace("&", "&amp;");
                        objOptionNode.InnerXml = @"" + sOrgHierarchy + "";
                    }
                    objOrgHierarchyNode = null;
                    objOptionNode = null;
                }
                else
                {
                    Functions.CreateAndSetElement(objRootNode, "OrgHierarchy", p_sOrgHierarchy.ToString(), m_iClientId);
                }

                Functions.CreateAndSetElement(objRootNode, "OrgHierarchyLevel", p_iOrgHierarchyLevel.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "IncludeAutoPaymentsFlag", p_bIncludeAutoPays.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "IncludeCombinedPaymentsFlag", p_bIncludeCombinedPay.ToString(), m_iClientId);
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //Functions.CreateAndSetElement(objRootNode, "PrintEFTPayment", p_bPrintEFTPayment.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "DistributionType", p_iDistributionType.ToString(), m_iClientId);
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                Functions.CreateAndSetElement(objRootNode, "UsingSelection", p_bUsingSelections.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "NumberOfPrechecks", iCheckCount.ToString(), m_iClientId);//skhare7 r8 Multicurrency
                Functions.CreateAndSetElement(objRootNode, "TotalAmounts", CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblTotal, m_sConnectionString, m_iClientId), m_iClientId);//skhare7 r8 

                // npadhy RMA-11632 - Before Distributon Type if the Print Option was Only printer, then we used to hide the Recreate Checks File link from MDI
                // Now since we have taken this setting from System wide to per distribution type, so we will not hide the link, rather disable Re-Print button and Show Proper Message
                // Retreive the Print Options
                m_iExportToFile = GetPrintOptions(p_iDistributionType);
                if (m_iExportToFile == 0 && p_bRecreateCheck)
                {
                    Functions.CreateAndSetElement(objRootNode, "PrintFlag", 0.ToString(), m_iClientId);
                    Functions.CreateAndSetElement(objRootNode, "RecreateError", Globalization.GetString("CheckSetup.CheckBatchChangedPre_ForReprint.Validation", m_iClientId), m_iClientId);
                }
                else
                {
                    Functions.CreateAndSetElement(objRootNode, "PrintFlag", (iCheckCount > 0).ToString(), m_iClientId);
                }

                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                // Overwrite the Value of m_iExportFile
                // As earlier we were inserting the value of FileAndConsolidate OnLoad level. But now the value of m_iExportToFile can change if the distribution type changes.
                // So we are now adding value of FileAndConsolidategoing one parent up. 
                // In case Check Set Changed is called directly, we will not have parents in that case and we do not need this flag there
                if (p_bDocStarted)
                {
                    if (m_iExportToFile == 2 && m_bConsolidate)
                        Functions.CreateAndSetElement((XmlElement)objRootNode.ParentNode, "FileAndConsolidate", true.ToString(), m_iClientId);
                    else
                        Functions.CreateAndSetElement((XmlElement)objRootNode.ParentNode, "FileAndConsolidate", false.ToString(), m_iClientId);
                }
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.CheckSetChanged.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCCacheFunctions = null;
                objFunctions = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                objRootNode = null;
                sbSQL = null;
            }
            return (m_objDocument);
        }
        // akaushik5 Changed for MITS 35846 Ends

        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        /// <summary>
        /// Retrieve the Print settings based on the DistributionType and set the m_iExportToFile. 
        /// </summary>
        /// <param name="iDistributionType"></param>
        /// <returns></returns>
        private int GetPrintOptions(int p_iDistributionType)
        {
            using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
            {
                return (objCache.GetPrintOptions(p_iDistributionType));
            }
        }
        // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

        public static Dictionary<int, Double> GetMaxPrintAmounts(int p_iUserId, int p_iUserGroupId, string m_sConnectionString, int p_iClientId)
        {
            Dictionary<int, Double> dMaxAmounts = new Dictionary<int, Double>();

            string sSQL = "";           
            int iLobCode = 0;//rkotak rma 11875
            try
            {
                //we always want to pick the user limits first. if it does not exists, then check for group limits.  
                ////rkotak rma 11875 starts
                sSQL = string.Format("SELECT MAX_AMOUNT, LINE_OF_BUS_CODE FROM PRT_CHECK_LIMITS WHERE USER_ID = {0} OR GROUP_ID ={1} ORDER BY GROUP_ID ASC", p_iUserId, p_iUserGroupId);
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objReader.Read())
                    {
                        iLobCode = Common.Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE"), p_iClientId);
                        if (!dMaxAmounts.ContainsKey(iLobCode))
                        {
                            dMaxAmounts.Add(iLobCode, Common.Conversion.ConvertObjToDouble(objReader.GetValue("MAX_AMOUNT"), p_iClientId));
                        }
                    }
                }
                //rkotak rma 11875 ends
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.GetAllMaxPrintAmount.Error", p_iClientId), p_objEx);
            }
            finally
            {

            }
            return dMaxAmounts;
        }

        #endregion

        #region Account Changed
        /*
		 * SAMPLE XML
		 * 
		 <AccountChanged>
			<AccountId>1</AccountId> 
			<IncludeAutoPayment>1</IncludeAutoPayment> 
			<AttachedClaimOnly>0</AttachedClaimOnly> 
			<FromDateFlag>0</FromDateFlag> 
			<FromDate /> 
			<ToDateFlag>0</ToDateFlag> 
			<ToDate>20041231</ToDate> 
		</AccountChanged>
		 * 
		 */
        /// Name		: AccountChanged
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Account changed event.
        /// </summary>
        /// <param name="p_objInputDoc">Input Xml Doc</param>
        /// <returns>Returns the Out put XML document contains the information for AccountChanged.</returns>
        public XmlDocument AccountChanged(XmlDocument p_objInputDoc)
        {
            bool bUseFromDate = false;
            bool bUseToDate = false;
            bool bWithAttachedClaimsOnly = false;
            bool bIncludeAutoPays = false;
            string sFromDateValue = "";
            string sToDateValue = "";
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string sOrgHierarchy = string.Empty;
            int iAccountId = 0;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            int iOrgHierarchyLevel = 0;
            //skhare7 R8 enhancement
            bool bIncludeCombinedPays = false;
            //skhare7 End
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //JIRA:438 START: ajohari2
            //bool bPrintEFTPayment = false;
            //JIRA:438 End: 
            int iDistributionType = 0;
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            bool bGenerateBatchNo = false;

            bool bRecreateCheck = false;
            try
            {
                bUseFromDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "FromDateFlag", m_iClientId));
                bUseToDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "ToDateFlag", m_iClientId));
                bWithAttachedClaimsOnly = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "AttachedClaimOnly", m_iClientId));
                bIncludeAutoPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeAutoPayment", m_iClientId));
                //skhare7 R8 enhancement
                bIncludeCombinedPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeCombinedPayment", m_iClientId));
                //skhare7 R8
                sFromDateValue = Functions.GetValue(p_objInputDoc, "FromDate", m_iClientId);
                sToDateValue = Functions.GetValue(p_objInputDoc, "ToDate", m_iClientId);
                iAccountId = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "AccountId", m_iClientId));
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                sOrgHierarchy = Functions.GetValue(p_objInputDoc, "OrgHierarchy", m_iClientId);
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                iOrgHierarchyLevel = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "OrgHierarchyLevel", m_iClientId));

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //bPrintEFTPayment = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "PrintEFTPayment", m_iClientId));
                iDistributionType = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "DistributionType", m_iClientId));
                // npadhy - JIRA 6418 End. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

                // npadhy JIRA 6418 Starts  -Now the batch number will change with respect to the distribution type. Earlier we used to retrieve the Maximum batch number for the account.
                // But the maximum batch no of account might not have a check to print for that Distribution type.
                // So now we will retrive maximum batch no of an account wrt distribution type.
                // From Pre Check Register tab, the Batch needs to be created whereas for Print check we need to retrieve the maxmimum Batch no, hence this flag.
                bGenerateBatchNo = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "GenerateBatchNo", m_iClientId));

                // npadhy RMA-11632 Starts If the Recreate check is on, then retrieve the checks which are printed
                bRecreateCheck = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IsReCreate", m_iClientId));
                // npadhy RMA-11632 Ends If the Recreate check is on, then retrieve the checks which are printed

                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                //return (this.AccountChanged(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue,bWithAttachedClaimsOnly, bIncludeAutoPays, iAccountId, false));

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                return (this.AccountChanged(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue,
                    bWithAttachedClaimsOnly, bIncludeAutoPays, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, false, bIncludeCombinedPays, iDistributionType, bGenerateBatchNo, bRecreateCheck));
                //JIRA:438 End:
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.AccountChanged.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: AccountChanged
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML for Account changed event.
        /// </summary>
        /// <param name="p_bUseFromDate">Use From Date Flag</param>
        /// <param name="p_bUseToDate">Use To Date Flag</param>
        /// <param name="p_sFromDateValue">From Date</param>
        /// <param name="p_sToDateValue">To Date</param>
        /// <param name="p_bWithAttachedClaimsOnly">Attached Claims Only Flag</param>
        /// <param name="p_bIncludeAutoPays">Include Auto Payments Flag</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <param name="p_bDocStarted">If Document Generation Has Started then true</param>
        /// <returns>Returns the Out put XML document contains the information for AccountChanged.</returns>
        // npadhy RMA-11632  We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        private XmlDocument AccountChanged(bool p_bUseFromDate, bool p_bUseToDate, string p_sFromDateValue,
            string p_sToDateValue, bool p_bWithAttachedClaimsOnly, bool p_bIncludeAutoPays, int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel,
            bool p_bDocStarted, bool p_bIncludeCombinedPays, int p_iDistributionType, bool p_bGenerateBatchNo, bool p_bRecreateCheck = false)
        // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        {
            CCacheFunctions objCCacheFunctions = null;
            XmlElement objRootNode = null;
            Account objAccount = null;
            Functions objFunctions = null;
            XmlCDataSection objCData = null;
            //JIRA:438 START: ajohari2
            bool bIsEFTAccount = false;
            bool IsEFTAccountPayment = false;
            //JIRA:438 End: 

            //int iNextCheckNum = 0 ;
            long lNextCheckNum = 0; //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
            int iNextBatchNumber = 0;

            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                objFunctions = new Functions(m_objDataModelFactory);

                if (!p_bDocStarted) // If document genearation has not started.
                {
                    Functions.StartDocument(ref m_objDocument, ref objRootNode, ref m_objMessagesNode, "AccountChanged", m_iClientId);
                }
                else
                {
                    Functions.CreateElement(m_objDocument.DocumentElement, "AccountChanged", ref objRootNode, m_iClientId);
                    //Debabrata Biswas MCIC Batch Printing Filter MITS 20050 Date: 04/22/2010
                    p_iOrgHierarchyLevel = m_iOrgHierarchyLevel;
                }

                objFunctions.LoadListWithStocks(objRootNode, p_iAccountId, m_iClientId);
                objFunctions.IsEFTAccount(objRootNode, p_iAccountId, m_iClientId);//Added by Amitosh For EFT Payment
                // npadhy - Retrive the batch no corresponding to Account no and Distribution Type. Also pass the parameter to distinguish between first time or reprint check
                this.GetAccountDetails(p_iAccountId, p_iDistributionType, ref iNextBatchNumber, ref lNextCheckNum, p_bGenerateBatchNo, p_bIncludeAutoPays, p_bRecreateCheck);

                Functions.CreateAndSetElement(objRootNode, "FirstCheckNum", lNextCheckNum.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "CheckDate", System.DateTime.Now.ToString("d"), m_iClientId);

                // npadhy JIRA 6418 If the Batch number is generated then return one less as the one generated is the next batch number. Whereas if we retrieve the batch no from DB, 
                // then same batch no should be returned
                if (p_bGenerateBatchNo)
                    Functions.CreateAndSetElement(objRootNode, "BatchNumber", (iNextBatchNumber - 1).ToString(), m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "BatchNumber", iNextBatchNumber.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "PayerLevel", objFunctions.GetPayerLevel(p_iAccountId, m_iClientId), m_iClientId);

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //bIsEFTAccount = Functions.IsEFTBank(p_iAccountId, m_sConnectionString, m_iClientId);

                //if (bIsEFTAccount && p_bPrintEFTPayment)
                //{
                //    IsEFTAccountPayment = p_bPrintEFTPayment;
                //}

                //JIRA:438 End:
                // npadhy RMA-11632  We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                //this.CheckSetChanged(p_bUseFromDate, p_bUseToDate, p_sFromDateValue, p_sToDateValue,
                //p_bWithAttachedClaimsOnly, p_bIncludeAutoPays, p_iAccountId, true, false, null, null);
                this.CheckSetChanged(p_bUseFromDate, p_bUseToDate, p_sFromDateValue, p_sToDateValue,
                    p_bWithAttachedClaimsOnly, p_bIncludeAutoPays, p_iAccountId, p_sOrgHierarchy, p_iOrgHierarchyLevel, true, false, null, null, p_bIncludeCombinedPays, p_iDistributionType, p_bRecreateCheck);//JIRA:438 START: ajohari2
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

                // npadhy JIRA 6418 - If the p_bGenerateBatchNo is true, then the Code is being called from Precheck and we need to generate a new Batch no. 
                // But if it is false, then Maximum Batch no of an Account corresponding to Distribution Type shall be retrieved. Earlier we used to find the Batch Number from Account 
                // and return Batch number -1, But that will not work now as Batch number correspinding to different Distribution Type shall be different.
                if (p_bGenerateBatchNo)
                    this.CheckBatchChangedPre(iNextBatchNumber - 1, p_iAccountId, true, p_iDistributionType);
                else
                    // npadhy RMA-11632  We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
                    this.CheckBatchChangedPre(iNextBatchNumber, p_iAccountId, true, p_iDistributionType, p_bRecreateCheck);

                if (p_bGenerateBatchNo)
                    this.CheckBatchChangedPost(iNextBatchNumber - 1, p_iAccountId, true, p_iDistributionType);
                else
                    this.CheckBatchChangedPost(iNextBatchNumber, p_iAccountId, true, p_iDistributionType);
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.AccountChanged.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCCacheFunctions = null;
                objFunctions = null;
                objRootNode = null;
                if (objAccount != null)
                {
                    objAccount.Dispose();
                    objAccount = null;
                }
            }
            return (m_objDocument);
        }

        #endregion



        #region On Load
        /// Name		: OnLoad
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Return the default XML on the load event.
        /// </summary>
        /// <returns>Returns the XML document for OnLoad function, contains initialization information.</returns>
        // npadhy RMA-11632  We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
        public XmlDocument OnLoad(bool p_bRecreateCheck = false)
        {
            CCacheFunctions objCCacheFunctions = null;

            DbReader objReader = null;
            ArrayList arrlstAccountList = null;
            XmlElement objRootNode = null;
            XmlElement objAccountsNode = null;
            XmlElement objAccountNode = null;
            AccountDetail structAccountDetail;

            // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            ArrayList alDistributionType = null;
            DistributionType structDistributionType;
            XmlElement objDistributionTypesNode = null;
            XmlElement objDistributionTypeNode = null;
            // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

            int iIndex = 0;
            int iAccountId = 0;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            int iOrgHierarchyLevel = 0;
            bool bUseFromDate = false;
            bool bUseToDate = true;
            string sFromDateValue = string.Empty;
            string sToDateValue = string.Empty;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string sOrgHierarchy = string.Empty;

            //JIRA:438 START: ajohari2
            //bool bEFTAccount = false;
            //JIRA:438 End: 

            // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            int iDistributionType = 0;
            // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
            try
            {
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);

                Functions.StartDocument(ref m_objDocument, ref objRootNode, ref m_objMessagesNode, "OnLoad", m_iClientId);

                arrlstAccountList = this.GetAccounts();
                for (iIndex = 0; iIndex < arrlstAccountList.Count; iIndex++)
                {
                    if (iIndex == 0)
                        Functions.CreateElement(objRootNode, "Accounts", ref objAccountsNode, m_iClientId);
                    structAccountDetail = (AccountDetail)arrlstAccountList[iIndex];
                    Functions.CreateElement(objAccountsNode, "Account", ref objAccountNode, m_iClientId);

                    objAccountNode.SetAttribute("AccountName", structAccountDetail.AccountName);
                    objAccountNode.SetAttribute("AccountId", structAccountDetail.AccountId.ToString());
                    //JIRA:438 START: ajohari2
                    objAccountNode.SetAttribute("EFTAccount", structAccountDetail.EFTAccount.ToString());
                    //JIRA:438 : ajohari2

                }
                // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
                alDistributionType = this.GetDistributionTypes();
                
                for (iIndex = 0; iIndex < alDistributionType.Count; iIndex++)
                {
                    if (iIndex == 0)
                        Functions.CreateElement(objRootNode, "DistributionTypes", ref objDistributionTypesNode, m_iClientId);
                    structDistributionType = (DistributionType)alDistributionType[iIndex];
                    Functions.CreateElement(objDistributionTypesNode, "DistributionType", ref objDistributionTypeNode, m_iClientId);

                    objDistributionTypeNode.SetAttribute("DistributionTypeId", structDistributionType.DistributionTypeId.ToString());
                    objDistributionTypeNode.SetAttribute("DistributionTypeDesc", structDistributionType.DistributionTypeDesc.ToString());
                }
                // npadhy JIRA 6418 Ends  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks

                if (arrlstAccountList.Count > 0 && alDistributionType.Count > 0)
                {
                    sFromDateValue = System.DateTime.Now.ToString("d");
                    sToDateValue = System.DateTime.Now.ToString("d");
                    structAccountDetail = (AccountDetail)arrlstAccountList[0];
                    iAccountId = structAccountDetail.AccountId;

                    structDistributionType = (DistributionType)alDistributionType[0];
                    //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel) 
                    //this.AccountChanged(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue, false, true, iAccountId, true);  									

                    //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                    //JIRA:438 START: ajohari2
                    //bEFTAccount = structAccountDetail.EFTAccount;
                    iDistributionType = structDistributionType.DistributionTypeId;
                    // npadhy RMA-11632  We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
                    this.AccountChanged(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue, false, true, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, true, false, iDistributionType, false, p_bRecreateCheck);
                    //JIRA:438 End: 
                    //npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                }

                //npadhy - on Change of DistributionType the Following settings can change. So we remove the below code from here and add it inside AccountChanged function
                //if( m_iExportToFile == 2 && m_bConsolidate )
                //    Functions.CreateAndSetElement(objRootNode, "FileAndConsolidate", true.ToString(), m_iClientId);
                //else
                //    Functions.CreateAndSetElement(objRootNode, "FileAndConsolidate", false.ToString(), m_iClientId);

                if (!Functions.IsPrinterSelected(m_sConnectionString, m_iClientId))
                {
                    Functions.CreateAndSetElement(objRootNode, "IsPrinterSelected", "false", m_iClientId);
                }
                else
                {
                    Functions.CreateAndSetElement(objRootNode, "IsPrinterSelected", "true", m_iClientId);
                }
                // akaushik5 Changed for MITS 37242 Starts
                //Functions.CreateAndSetElement(objRootNode, "BRSInstalled", SysSettings.IsBRSInstalled(m_iClientId).ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "BRSInstalled", this.m_objDataModelFactory.Context.InternalSettings.SysSettings.IsBRSInstalled().ToString(), this.m_iClientId);
                // akaushik5 Changed for MITS 37242 Ends
                Functions.CreateAndSetElement(objRootNode, "UseFundsSubAccounts", m_objSysSettings.UseFundsSubAcc.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "NoSubDepCheck", m_objSysSettings.NoSubDepCheck.ToString(), m_iClientId);
                Functions.CreateAndSetElement(objRootNode, "AssignCheckStockToSubAcc", m_objSysSettings.AssignCheckStockToSubAcc.ToString(), m_iClientId);

                if (m_bAllowPostDtd)
                    Functions.CreateAndSetElement(objRootNode, "CheckDateType", "date", m_iClientId);
                else
                    Functions.CreateAndSetElement(objRootNode, "CheckDateType", "readonly", m_iClientId);

                // Vaibhav: Setting flag for check number change permission. Make notice of ( ! ) operator.
                Functions.CreateAndSetElement(objRootNode, "CheckNumberChangePrermission", (!m_objDataModelFactory.Context.RMUser.IsAllowedEx(RMB_FUNDS_PRINTCHK, RMB_FUNDS_PRINTCHK_CHK_NUMBER)).ToString().ToLower(), m_iClientId);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.OnLoad.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCCacheFunctions = null;
                if (objReader != null)
                {
                    //objReader.Close();
                    objReader.Dispose();
                }
                arrlstAccountList = null;
                objRootNode = null;
                objAccountsNode = null;
                objAccountNode = null;
            }
            return (m_objDocument);
        }

        #endregion


        #region Get List Of Check
        /*
		 * SAMPLE XML 
		 * 
		 <GetListOfChecks>
			<AccountId>1</AccountId> 
			<IncludeAutoPayment>1</IncludeAutoPayment> 
			<FromDateFlag>0</FromDateFlag> 
			<FromDate /> 
			<ToDateFlag>0</ToDateFlag> 
			<ToDate>20041231</ToDate> 	
			<ControlNumber></ControlNumber>		
		</GetListOfChecks>
		*/
        /// Name		: GetListOfChecks
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get XML contains list of Checks.
        /// </summary>
        /// <param name="p_objInputDoc">Input Xml Doc</param>
        /// <returns>Returns the Out put XML document contains the list of Checks.</returns>
        public XmlDocument GetListOfChecks(XmlDocument p_objInputDoc)
        {
            bool bUseFromDate = false;
            bool bUseToDate = false;
            bool bIncludeAutoPays = false;
            string sFromDateValue = "";
            string sToDateValue = "";
            string sControlNumber = "";
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string sOrgHierarchy = string.Empty;
            int iAccountId = 0;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            int iOrgHierarchyLevel = 0;
            //skhare7 R8 Enhancement
            bool bIncludeCombinedPays = false;
            //skhare7 R8 End 
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //bool bPrintEFTPayment = false;//JIRA:438 START: ajohari2
            int iDistributionType = 0;
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

            try
            {
                bUseFromDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "FromDateFlag", m_iClientId));
                bUseToDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "ToDateFlag", m_iClientId));
                bIncludeAutoPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeAutoPayment", m_iClientId));
                //skhare7 R8 enhancement
                bIncludeCombinedPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeCombinedPayment", m_iClientId));
                //skhare7 R8
                sFromDateValue = Functions.GetValue(p_objInputDoc, "FromDate", m_iClientId);
                sToDateValue = Functions.GetValue(p_objInputDoc, "ToDate", m_iClientId);
                iAccountId = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "AccountId", m_iClientId));
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                sOrgHierarchy = Functions.GetValue(p_objInputDoc, "OrgHierarchyPre", m_iClientId);
                //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                //JIRA:438 START: ajohari2
                //bPrintEFTPayment = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "PrintEFTPayment", m_iClientId));
                //JIRA:438 End: 
                //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                iDistributionType = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "DistributionType", m_iClientId));
                if (sOrgHierarchy.IndexOf(' ') != -1)
                    sOrgHierarchy = sOrgHierarchy.Replace(' ', ',');

                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                iOrgHierarchyLevel = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "OrgHierarchyLevelPre", m_iClientId));
                sControlNumber = Functions.GetValue(p_objInputDoc, "ControlNumber", m_iClientId);
                string orderBy = Functions.GetValue(p_objInputDoc, "OrderBy", m_iClientId);
                string orderByDirection = Functions.GetValue(p_objInputDoc, "OrderByDirection", m_iClientId);
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel) 
                //return (this.GetListOfChecks(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue,
                //  bIncludeAutoPays, iAccountId, sControlNumber, orderBy, orderByDirection));
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                return (this.GetListOfChecks(bUseFromDate, bUseToDate, sFromDateValue, sToDateValue,
                    bIncludeAutoPays, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, sControlNumber, orderBy, orderByDirection, bIncludeCombinedPays, iDistributionType));
                //JIRA:438 End: 
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.GetListOfChecks.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: GetListOfChecks
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get List of checks for selected criteria.
        /// </summary>
        /// <param name="p_bUseFromDate">Use From Date Flag</param>
        /// <param name="p_bUseToDate">Use To Date Flag</param>
        /// <param name="p_sFromDateValue">From Date</param>
        /// <param name="p_sToDateValue">To Date</param>
        /// <param name="p_sOrgHierarchy">Org Hierarchy</param>
        /// <param name="p_iOrgHierarchyLevel">Org Hierarchy Level</param>
        /// <param name="p_bIncludeAutoPays">Include Auto Payments</param>
        /// <param name="p_iAccountId">Account Id</param>
        /// <param name="p_sControlNumber">ControlNumber</param>
        /// //skhare7 R8 enhancement
        /// /// <param name="p_IncludeCombinedPays">For Combined Payment</param>
        // akaushik5 Changed for MITS 35846 Starts
        //        public XmlDocument GetListOfChecks( bool p_bUseFromDate, bool p_bUseToDate, 
        //            string p_sFromDateValue , string p_sToDateValue, bool p_bIncludeAutoPays ,
        //            int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel, string p_sControlNumber, string orderBy, string orderByDirection,bool p_IncludeCombinedPays)
        //        {
        //            DbReader objReader = null ;
        //            ArrayList arrlstSelectedCheck = null ;
        //            ArrayList arrlstSelectedAutoCheck = null ;		
        //            Functions objFunctions = null ;
        //            XmlElement objChecks = null ;
        //            XmlElement objCheck = null ;	
        //            XmlElement objRootNode = null ;
        //            XmlElement objMessageNode = null ;
        //            CCacheFunctions objCCacheFunctions = null ;
        //            StringBuilder sbSQL = null;

        //            string sFromDate = "" ;
        //            string sToDate = "" ;
        //            string sCtlNumber = "" ;
        //            string sClaimNumber = "" ;
        //            string sFirstName = "" ;
        //            string sLastName = "" ;
        //            string sName = "" ;
        //            string sCheckDate = "" ;
        //            string sSubAccount = "" ;
        //            string sAlias = "" ;
        //            string sAmount = "" ;
        //            int iSubAccountId = 0 ;
        //            int iTransId = 0 ;
        //            int iLastTransId = 0 ;
        //            double dblAmount = 0.0 ;
        //            //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //            int iTableId = 0;
        //            int iDeptId = 0;
        //            int iOrgLevel = 0;
        //            string sOrgLevelFieldValue = string.Empty;
        //            string sOrgLevelFieldList = string.Empty;
        //            string sDeptIds = string.Empty;
        //            string abbr = string.Empty;
        //            string name = string.Empty;
        //            string sOrgLevelField = string.Empty;
        //            string OrgLevel = string.Empty;
        //            //skhare7 R8 Combined Payment
        //            StringBuilder sbComPay = null;
        //            bool bCombinedPay = false;

        //            switch (p_iOrgHierarchyLevel)
        //            {
        //                case 1005: OrgLevel = "Client Code"; break;
        //                case 1006: OrgLevel = "Company Code"; break;
        //                case 1007: OrgLevel = "Operation Code"; break;
        //                case 1008: OrgLevel = "Region Code"; break;
        //                case 1009: OrgLevel = "Division Code"; break;
        //                case 1010: OrgLevel = "Location Code"; break;
        //                case 1011: OrgLevel = "Facility Code"; break;
        //                case 1012: OrgLevel = "Department Code"; break;
        //            }
        //            //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //            //default to TransId

        //            dicMaxAmounts = null;
        //            dicMaxAmounts = GetMaxPrintAmounts(m_iUserId, m_iUserGroupId, m_sConnectionString);

        //            orderBy = (string.IsNullOrEmpty(orderBy) ? "TransId" : orderBy);
        //            string orderByDataType = "text";
        //            switch (orderBy)
        //            {
        //                case "TransId":
        //                case "SortAmount":
        //                case "SortCheckDate":
        //                    orderByDataType = "number";
        //                    break;
        //            }
        //            //default to ascending
        //            orderByDirection = (string.IsNullOrEmpty(orderByDirection) ? "ascending" : orderByDirection);

        //            try
        //            {
        //                arrlstSelectedCheck = new ArrayList();
        //                arrlstSelectedAutoCheck = new ArrayList();
        //                objCCacheFunctions = new CCacheFunctions( m_sConnectionString );
        //                objFunctions = new Functions( m_objDataModelFactory );
        //                sbSQL = new StringBuilder();

        //                Functions.StartDocument( ref m_objDocument , ref objRootNode , ref objMessageNode , "ListOfChecks" );

        //                if ( Functions.m_sDBType == Constants.DB_ACCESS )
        //                    sAlias = " AS " ;
        //                else
        //                    sAlias = " " ;

        //                if( p_bUseFromDate )
        //                {
        //                    if( Utilities.IsDate( p_sFromDateValue ) )
        //                    {
        //                        sFromDate = Conversion.GetDate( p_sFromDateValue );						 
        //                    }
        //                    else
        //                    {
        //                        sFromDate = "" ;
        //                    }
        //                }
        //                if( p_bUseToDate )
        //                {
        //                    if( Utilities.IsDate( p_sToDateValue ) )
        //                    {
        //                        sToDate = Conversion.GetDate( p_sToDateValue );						 
        //                    }
        //                    else
        //                    {
        //                        sToDate = "" ;
        //                    }
        //                }
        //                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                switch (p_iOrgHierarchyLevel)
        //                {
        //                    case 1005: sOrgLevelField = "CLIENT_EID"; break;
        //                    case 1006: sOrgLevelField = "COMPANY_EID"; break;
        //                    case 1007: sOrgLevelField = "OPERATION_EID"; break;
        //                    case 1008: sOrgLevelField = "REGION_EID"; break;
        //                    case 1009: sOrgLevelField = "DIVISION_EID"; break;
        //                    case 1010: sOrgLevelField = "LOCATION_EID"; break;
        //                    case 1011: sOrgLevelField = "FACILITY_EID"; break;
        //                    case 1012: sOrgLevelField = "DEPARTMENT_EID"; break;
        //                }
        //                if (p_sOrgHierarchy != "")
        //                {
        //                    string[] arrOrgList = p_sOrgHierarchy.Split(',');

        //                    foreach (string sOrgLevel in arrOrgList)
        //                    {
        //                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + sOrgLevel + ")");
        //                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //                        if (objReader != null)
        //                        {
        //                            while (objReader.Read())
        //                            {
        //                                iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);

        //                            }
        //                            objReader.Close();
        //                        }
        //                        switch (iTableId)
        //                        {
        //                            case 1005: sOrgLevelFieldValue = "CLIENT_EID=" + sOrgLevel + " "; break;
        //                            case 1006: sOrgLevelFieldValue = "COMPANY_EID=" + sOrgLevel + " "; break;
        //                            case 1007: sOrgLevelFieldValue = "OPERATION_EID=" + sOrgLevel + " "; break;
        //                            case 1008: sOrgLevelFieldValue = "REGION_EID=" + sOrgLevel + " "; break;
        //                            case 1009: sOrgLevelFieldValue = "DIVISION_EID=" + sOrgLevel + " "; break;
        //                            case 1010: sOrgLevelFieldValue = "LOCATION_EID=" + sOrgLevel + " "; break;
        //                            case 1011: sOrgLevelFieldValue = "FACILITY_EID=" + sOrgLevel + " "; break;
        //                            case 1012: sOrgLevelFieldValue = "DEPARTMENT_EID=" + sOrgLevel + " "; break;

        //                        }
        //                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + "OR ";
        //                        sbSQL.Remove(0, sbSQL.Length);

        //                    }
        //                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0, sOrgLevelFieldList.Length - 3);
        //                    sbSQL.Append("SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (" + sOrgLevelFieldList + ")");
        //                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //                    if (objReader != null)
        //                    {
        //                        while (objReader.Read())
        //                        {
        //                            iDeptId = Common.Conversion.ConvertObjToInt(objReader.GetValue("DEPARTMENT_EID"), m_iClientId);
        //                            sDeptIds = iDeptId + "," + sDeptIds;
        //                        }
        //                        objReader.Close();
        //                    }
        //                    sbSQL.Remove(0, sbSQL.Length);
        //                    sDeptIds = sDeptIds.Substring(0, sDeptIds.Length - 1);
        //                }
        //                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                #region For Pre Checks
        //                //skhare7 R8 CombinedPay included
        //                sbSQL.Append(" SELECT TRANS_ID " + sAlias + " FUNDS_TRANS_ID ,TRANS_DATE,CTL_NUMBER,FIRST_NAME, ");
        //                sbSQL.Append(" LAST_NAME,PMT_CURRENCY_CODE,PMT_CURRENCY_AMOUNT  AMOUNT,CLAIM_NUMBER, AMOUNT BASEAMOUNT,PAYEE_EID, ");
        //                sbSQL.Append(" AUTO_CHECK_FLAG,COMBINED_PAY_FLAG,SUB_ACCOUNT_ID");

        //// JP 05.15.2006   Removed use of Oracle functions. No longer needed in 9i, 10g.
        ////				if ( Functions.m_sDBType == Constants.DB_ORACLE )
        ////				{
        ////					objFunctions.OracleFunctions(1);
        ////					sbSQL.Append( ",getFrzFlag(FUNDS.CLAIM_ID) " + sAlias + " PAYMNT_FROZEN_FLAG"  );         
        ////					sbSQL.Append( ",getClaimType(FUNDS.CLAIM_ID)" + sAlias + " CLAIM_TYPE_CODE" );
        ////					sbSQL.Append( ",getLOBCode(FUNDS.CLAIM_ID)" + sAlias + " LINE_OF_BUS_CODE" );
        ////					sbSQL.Append( ",getFrzPayee(FUNDS.PAYEE_EID)" + sAlias + " FREEZE_PAYMENTS" );
        ////				}
        ////				else
        ////				{
        //                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                    //if (p_sOrgHierarchy != "")Debabrata Biswas Mits #14804
        //                    sbSQL.Append(",(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS.CLAIM_ID)) AS ORG_LEVEL ");
        //                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                    sbSQL.Append( ",(SELECT PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID) " );
        //                    sbSQL.Append( sAlias + " PAYMNT_FROZEN_FLAG" );
        //                    sbSQL.Append( ",(SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID)" );
        //                    sbSQL.Append( sAlias + " CLAIM_TYPE_CODE" );
        //                    sbSQL.Append( ",(SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID)"  );
        //                    sbSQL.Append( sAlias + " LINE_OF_BUS_CODE" );
        //                    sbSQL.Append( ",(SELECT FREEZE_PAYMENTS FROM ENTITY WHERE ENTITY.ENTITY_ID = FUNDS.PAYEE_EID)" );
        //                    sbSQL.Append( sAlias + " FREEZE_PAYMENTS" ) ;

        ////				}

        //                sbSQL.Append( " FROM FUNDS WHERE STATUS_CODE IN " );
        //                sbSQL.Append("(" + objCCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS") + "," + objCCacheFunctions.GetCodeIDWithShort("I", "CHECK_STATUS") + ")");

        //                if( sToDate != "" ) 
        //                    sbSQL.Append( " AND TRANS_DATE <= '" + sToDate + "'" );
        //                if( sFromDate != "" )
        //                    sbSQL.Append( " AND TRANS_DATE >= '" + sFromDate + "'" );

        //                sbSQL.Append( " AND ACCOUNT_ID = " + p_iAccountId );
        //                sbSQL.Append( " AND PAYMENT_FLAG <> 0 AND PRECHECK_FLAG = 0 AND VOID_FLAG = 0" );
        //                //nadim 13685
        //                if (m_objSysSettings.UseMultiCurrency != 0)
        //                {
        //                    if (Functions.AllowZeroPayments())
        //                    {
        //                        sbSQL.Append(" AND PMT_CURRENCY_AMOUNT >= 0");
        //                    }
        //                    else
        //                    {
        //                        sbSQL.Append(" AND PMT_CURRENCY_AMOUNT > 0");
        //                    }
        //                }
        //                else
        //                {
        //                    if (Functions.AllowZeroPayments())
        //                    {
        //                        sbSQL.Append(" AND AMOUNT >= 0");
        //                    }
        //                    else
        //                    {
        //                        sbSQL.Append(" AND AMOUNT > 0");
        //                    }


        //                }

        //                //nadim 13685

        //                if( !p_bIncludeAutoPays )
        //                    sbSQL.Append( " AND AUTO_CHECK_FLAG = 0" );
        //                //skhare7 R8 Enhancement 
        //                if (!p_IncludeCombinedPays)
        //                    sbSQL.Append(" AND COMBINED_PAY_FLAG = 0");
        //                else

        //                {
        //                    sbSQL.Append(" AND FUNDS.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID    ");

        //                    if (sToDate != "")
        //                        sbSQL.Append(" AND NEXT_SCH_PRINT > '" + sToDate + "'");
        //                    if (sFromDate != "")
        //                        sbSQL.Append(" AND NEXT_SCH_PRINT < '" + sFromDate + "'");
        //                    if (sToDate != "" || (sFromDate != ""))
        //                        sbSQL.Append(" )");
        //                }


        //                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                if (p_sOrgHierarchy != "")
        //                    sbSQL.Append(" AND CLAIM_ID IN (SELECT CLAIM_ID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND EV.DEPT_EID IN (" + sDeptIds + "))");
        //                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                if( p_sControlNumber != "" )
        //                    sbSQL.Append( " AND CTL_NUMBER LIKE '" + p_sControlNumber + "%' ORDER BY CTL_NUMBER" );

        //                objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
        //                if( objReader != null )
        //                {
        //                    while( objReader.Read() )
        //                    {
        //                        iTransId = Conversion.ConvertObjToInt( objReader.GetValue( "FUNDS_TRANS_ID" ), m_iClientId );
        //                        if( !this.SkipPayment( objReader , false , "AMOUNT" , false , arrlstSelectedCheck , arrlstSelectedAutoCheck ) )
        //                        {
        //                            sCtlNumber = objReader.GetString( "CTL_NUMBER");
        //                            sLastName = objReader.GetString( "FIRST_NAME");
        //                            sFirstName = objReader.GetString( "LAST_NAME");
        //                            if( sFirstName != "" )
        //                                sName = sFirstName + " " + sLastName ;
        //                            else
        //                                sName = sLastName ;

        //                            sClaimNumber = objReader.GetString( "CLAIM_NUMBER");
        //                            string sortCheckDate = objReader.GetString("TRANS_DATE");
        //                            sCheckDate = Conversion.GetDBDateFormat(sortCheckDate, "d");

        //                            bCombinedPay = Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(objReader.GetValue("COMBINED_PAY_FLAG")));

        //                            if (Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(m_objSysSettings.UseMultiCurrency)))
        //                            {
        //                                dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT")));
        //                                sAmount = Riskmaster.Common.CommonFunctions.ConvertCurrency(iTransId, dblAmount, CommonFunctions.NavFormType.Funds, m_sConnectionString);
        //                            }
        //                            else
        //                            {
        //                                dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("BASEAMOUNT")));
        //                                sAmount = string.Format("{0:C}", dblAmount);

        //                            }

        //                            if(m_objSysSettings.UseFundsSubAcc)
        //                            {
        //                                iSubAccountId = Conversion.ConvertObjToInt( objReader.GetValue( "SUB_ACCOUNT_ID" ), m_iClientId );
        //                                sSubAccount = this.SubAccountName( iSubAccountId );						
        //                            }
        //                            else
        //                                sSubAccount = "" ;
        //                            if (objChecks == null)
        //                            {
        //                                Functions.CreateElement(objRootNode, "Checks", ref objChecks);
        //                                objChecks.SetAttribute("OrderBy", orderBy);
        //                                objChecks.SetAttribute("OrderByDirection", orderByDirection);
        //                                objChecks.SetAttribute("OrderByDataType", orderByDataType);
        //                            }

        //                            Functions.CreateElement( objChecks , "Check" , ref objCheck );
        //                            objCheck.SetAttribute("CtlNumber" , sCtlNumber );
        //                            objCheck.SetAttribute("PayeeName" , sName );
        //                            objCheck.SetAttribute("ClaimNumber" , sClaimNumber );
        //                            objCheck.SetAttribute("Amount" , sAmount );
        //                            objCheck.SetAttribute("SortAmount", dblAmount.ToString());
        //                            objCheck.SetAttribute("CheckDate" , sCheckDate );
        //                            objCheck.SetAttribute("SortCheckDate", sortCheckDate);
        //                            objCheck.SetAttribute("TransId" , iTransId.ToString() );
        //                            objCheck.SetAttribute("SubBankAccount" , sSubAccount );
        //                            objCheck.SetAttribute("AutoCheck" , false.ToString() );
        //                            //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                            iOrgLevel = objReader.GetInt("ORG_LEVEL");
        //                            objCCacheFunctions.GetOrgInfo(iOrgLevel, ref abbr, ref name);
        //                            objCheck.SetAttribute("OrgHierarchy", abbr);
        //                                //skhare7 R8 enhancement 
        //                            objCheck.SetAttribute("CombinedPayment", bCombinedPay.ToString());
        //                                //skhare7 R8 End
        //                            if (abbr != "")
        //                                objCheck.SetAttribute("OrgHierarchy", abbr);
        //                            else
        //                                objCheck.SetAttribute("OrgHierarchy", " ");
        //                            //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                        }
        //                    }
        //                    objReader.Close();
        //                }
        //                #endregion 

        //                sbSQL.Remove(0,sbSQL.Length);
        //                if(sbComPay!=null)
        //                sbComPay.Remove(0, sbComPay.Length);

        //                #region For Auto Checks
        //                if( p_bIncludeAutoPays )
        //                {//skhare7 R8 Enhancement Combined Pay included
        //                    sbSQL.Append( " SELECT FUNDS_AUTO.AUTO_TRANS_ID " + sAlias + " FUNDS_TRANS_ID " );
        //                    sbSQL.Append( " ,FUNDS_AUTO.PRINT_DATE,FUNDS_AUTO.CTL_NUMBER," );
        //                    sbSQL.Append(" FUNDS_AUTO.FIRST_NAME,FUNDS_AUTO.LAST_NAME,FUNDS_AUTO.PMT_CURRENCY_CODE,FUNDS_AUTO.PMT_CURRENCY_AMOUNT AMOUNT,FUNDS_AUTO.AMOUNT BASEAMOUNT,");
        //                    sbSQL.Append( " FUNDS_AUTO.CLAIM_NUMBER,FUNDS_AUTO_SPLIT.AMOUNT,FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE," );
        //                    sbSQL.Append(" SUB_ACC_ID,FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG,FUNDS_AUTO.COMBINED_PAY_FLAG,FUNDS_AUTO.PAYEE_EID ");
        //                    //skhare7 R8 end

        //// JP 05.15.2006   Removed use of Oracle functions. No longer needed in 9i, 10g.
        ////					if ( Functions.m_sDBType == Constants.DB_ORACLE )
        ////					{
        ////						objFunctions.OracleFunctions(1);
        ////						sbSQL.Append(",getFrzFlag(FUNDS_AUTO.CLAIM_ID) " + sAlias + "PAYMNT_FROZEN_FLAG");
        ////						sbSQL.Append(",getClaimType(FUNDS_AUTO.CLAIM_ID)" + sAlias + "CLAIM_TYPE_CODE");
        ////						sbSQL.Append(",getLOBCode(FUNDS_AUTO.CLAIM_ID)" + sAlias + "LINE_OF_BUS_CODE");
        ////						sbSQL.Append(",getFrzPayee(FUNDS_AUTO.PAYEE_EID) " + sAlias + "FREEZE_PAYMENTS");
        ////					}
        ////					else
        ////					{
        //                        //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                        //if (p_sOrgHierarchy != "")
        //                        sbSQL.Append(",(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)) AS ORG_LEVEL ");
        //                        //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                        sbSQL.Append(",(SELECT PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)");
        //                        sbSQL.Append(sAlias + " PAYMNT_FROZEN_FLAG");
        //                        sbSQL.Append(",(SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)");
        //                        sbSQL.Append(sAlias + " CLAIM_TYPE_CODE");
        //                        sbSQL.Append(",(SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)");
        //                        sbSQL.Append(sAlias + " LINE_OF_BUS_CODE");
        //                        sbSQL.Append(",(SELECT FREEZE_PAYMENTS FROM ENTITY WHERE ENTITY.ENTITY_ID = FUNDS_AUTO.PAYEE_EID)");
        //                        sbSQL.Append(sAlias + " FREEZE_PAYMENTS");        
        ////					}

        //                    sbSQL.Append(" FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT, FUNDS_AUTO_BATCH ");
        //                    sbSQL.Append(" WHERE ACCOUNT_ID = " + p_iAccountId);
        //                    sbSQL.Append(" AND FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID AND ");
        //                    sbSQL.Append(" ((FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1) ");
        //                    sbSQL.Append(" OR (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL))");

        //                    if(sToDate != "") 
        //                        sbSQL.Append(" AND PRINT_DATE <= '" + sToDate + "'");
        //                    if(sFromDate != "")
        //                        sbSQL.Append(" AND PRINT_DATE >= '" + sFromDate + "'");

        //                    sbSQL.Append(" AND PRECHECK_FLAG = 0");
        //                    //skhare7 R8 
        //                    if (!p_IncludeCombinedPays)
        //                        sbSQL.Append(" AND COMBINED_PAY_FLAG = 0");
        //                    else

        //                {
        //                    sbSQL.Append(" AND FUNDS_AUTO.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS_AUTO.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID    ");

        //                    if (sToDate != "")
        //                        sbSQL.Append(" AND NEXT_SCH_PRINT > '" + sToDate + "'");
        //                    if (sFromDate != "")
        //                        sbSQL.Append(" AND NEXT_SCH_PRINT < '" + sFromDate + "'");
        //                    //if (sToDate != "" || (sFromDate != "")) //asharma326 MITS 34920 
        //                        sbSQL.Append(" )");
        //                }
        //                    if(m_objSysSettings.UseMultiCurrency!=0)
        //                    //sbSQL.Append( " AND FUNDS_AUTO.AMOUNT > 0" );
        //                    sbSQL.Append(" AND FUNDS_AUTO.PMT_CURRENCY_AMOUNT >= 0");   //pmittal5 Mits 14253 01/16/09 - Include Zero Payments
        //                    else
        //                        sbSQL.Append(" AND FUNDS_AUTO.AMOUNT > 0");
        //                    sbSQL.Append( " AND FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID" );
        //                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                    if (p_sOrgHierarchy != "")
        //                        sbSQL.Append(" AND CLAIM_ID IN (SELECT CLAIM_ID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND EV.DEPT_EID IN (" + sDeptIds + "))");
        //                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
        //                    if( p_sControlNumber != "" )
        //                        sbSQL.Append( " AND CTL_NUMBER LIKE '" + p_sControlNumber + "%' ORDER BY CTL_NUMBER" );
        //                    else
        //                        sbSQL.Append( " ORDER BY FUNDS_AUTO.AUTO_TRANS_ID" );			

        //                    objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
        //                    if( objReader != null )
        //                    {
        //                        iLastTransId = -1 ;
        //                        while( objReader.Read() )
        //                        {
        //                            iTransId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FUNDS_TRANS_ID" ), m_iClientId );
        //                            if( iTransId != iLastTransId )
        //                            {
        //                                iLastTransId = iTransId ;
        //                                if( !this.SkipPayment( objReader , true , "AMOUNT" , false , arrlstSelectedCheck , arrlstSelectedAutoCheck ) )
        //                                {
        //                                    sCtlNumber = objReader.GetString( "CTL_NUMBER");
        //                                    sLastName = objReader.GetString( "FIRST_NAME");
        //                                    sFirstName = objReader.GetString( "LAST_NAME");
        //                                    if( sFirstName != "" )
        //                                        sName = sFirstName + " " + sLastName ;
        //                                    else
        //                                        sName = sLastName ;

        //                                    sClaimNumber = objReader.GetString( "CLAIM_NUMBER");
        //                                    string sortCheckDate = objReader.GetString("PRINT_DATE");
        //                                    sCheckDate = Conversion.GetDBDateFormat(sortCheckDate, "d");

        //                                    bCombinedPay = Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(objReader.GetValue("COMBINED_PAY_FLAG")));

        //                                    if (Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(m_objSysSettings.UseMultiCurrency)))
        //                                    {
        //                                        dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT")));
        //                                        sAmount = Riskmaster.Common.CommonFunctions.ConvertCurrency(iTransId, dblAmount, CommonFunctions.NavFormType.Auto, m_sConnectionString);
        //                                    }
        //                                    else
        //                                    {
        //                                        dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("BASEAMOUNT")));
        //                                        sAmount = string.Format("{0:C}", dblAmount);

        //                                    }
        //                                    if (m_objSysSettings.UseFundsSubAcc)
        //                                    {
        //                                        iSubAccountId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "SUB_ACC_ID" ), m_iClientId );
        //                                        sSubAccount = this.SubAccountName( iSubAccountId );						
        //                                    }
        //                                    else
        //                                        sSubAccount = "" ;

        //                                    if( objChecks == null )
        //                                    {
        //                                        Functions.CreateElement(objRootNode, "Checks", ref objChecks);
        //                                        objChecks.SetAttribute("OrderBy", orderBy);
        //                                        objChecks.SetAttribute("OrderByDirection", orderByDirection);
        //                                        objChecks.SetAttribute("OrderByDataType", orderByDataType);
        //                                    }																	

        //                                    Functions.CreateElement( objChecks , "Check" , ref objCheck );
        //                                    objCheck.SetAttribute( "CtlNumber" , sCtlNumber );
        //                                    objCheck.SetAttribute( "PayeeName" , sName );
        //                                    objCheck.SetAttribute( "ClaimNumber" , sClaimNumber );
        //                                    objCheck.SetAttribute( "Amount" , sAmount );
        //                                    objCheck.SetAttribute("SortAmount", dblAmount.ToString());
        //                                    objCheck.SetAttribute( "CheckDate" , sCheckDate );
        //                                    objCheck.SetAttribute("SortCheckDate", sortCheckDate);
        //                                    objCheck.SetAttribute( "TransId" , iTransId.ToString() );
        //                                    objCheck.SetAttribute( "SubBankAccount" , sSubAccount );							
        //                                    objCheck.SetAttribute( "AutoCheck" , true.ToString() );
        //                                    iOrgLevel = objReader.GetInt("ORG_LEVEL");
        //                                    objCCacheFunctions.GetOrgInfo(iOrgLevel, ref abbr, ref name);
        //                                    objCheck.SetAttribute("OrgHierarchy", abbr);
        //                                        //skhare7 R8 enhancement 
        //                                    objCheck.SetAttribute("CombinedPayment", bCombinedPay.ToString());
        //                                        //skhare7 R8 End
        //                                    if (abbr != "")
        //                                        objCheck.SetAttribute("OrgHierarchy", abbr);
        //                                    else
        //                                        objCheck.SetAttribute("OrgHierarchy", " ");
        //                                }
        //                            }
        //                        }
        //                        objReader.Close();
        //                    }
        //                }
        //                #endregion 

        //                //Add by kuladeep for mits:22878
        //                #region For reading Setting from web config

        //                Hashtable htPrintChecks = RMConfigurationManager.GetDictionarySectionSettings("PrintChecks");
        //                if (htPrintChecks != null)
        //                {
        //                    if (htPrintChecks["PrintCheckLimit"] != null)
        //                    {
        //                        Functions.CreateElement(objRootNode, "PrintChecks", ref objChecks);
        //                        objChecks.SetAttribute("PrintCheckLimit", Convert.ToString(htPrintChecks["PrintCheckLimit"]));
        //                    }
        //                }

        //                #endregion 


        //            }
        //            catch( RMAppException p_objEx )
        //            {
        //                throw p_objEx ;
        //            }
        //            catch( Exception p_objEx )
        //            {
        //                throw new RMAppException(Globalization.GetString("CheckSetup.GetListOfChecks.Error") , p_objEx );				
        //            }
        //            finally
        //            {
        //                if( objReader != null )
        //                {
        //                    //objReader.Close();
        //                    objReader.Dispose();
        //                }
        //                if (sbComPay != null)
        //                    sbComPay = null;
        //                arrlstSelectedCheck = null ;
        //                arrlstSelectedAutoCheck = null ;
        //                objFunctions = null;
        //                objChecks = null ;
        //                objCheck = null ;
        //                objCCacheFunctions = null ;
        //                sbSQL = null ;
        //                objRootNode = null ;
        //                objMessageNode = null ;
        //            }
        //            return( m_objDocument );
        //        }

        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        public XmlDocument GetListOfChecks(bool p_bUseFromDate, bool p_bUseToDate,
            string p_sFromDateValue, string p_sToDateValue, bool p_bIncludeAutoPays,
            int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel, string p_sControlNumber, string orderBy, string orderByDirection, bool p_IncludeCombinedPays, int p_iDistributionType) //JIRA:438 : ajohari2
        // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        {
            DbReader objReader = null;
            ArrayList arrlstSelectedCheck = null;
            ArrayList arrlstSelectedAutoCheck = null;
            Functions objFunctions = null;
            XmlElement objChecks = null;
            XmlElement objCheck = null;
            XmlElement objRootNode = null;
            XmlElement objMessageNode = null;
            CCacheFunctions objCCacheFunctions = null;
            StringBuilder sbSQL = null;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sCtlNumber = string.Empty;
            string sClaimNumber = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sName = string.Empty;
            string sCheckDate = string.Empty;
            string sSubAccount = string.Empty;
            string sAlias = string.Empty;
            string sAmount = string.Empty;
            //pkandhari JIRA 6421
            string sPayeeAddress = string.Empty;
            int iSubAccountId = 0;
            int iTransId = 0;
            int iLastTransId = 0;
            double dblAmount = 0.0;
            int iTableId = 0;
            int iOrgLevel = 0;
            string sOrgLevelFieldValue = string.Empty;
            string sOrgLevelFieldList = string.Empty;
            string sDeptIds = string.Empty;
            string abbr = string.Empty;
            string name = string.Empty;
            string sOrgLevelField = string.Empty;
            string OrgLevel = string.Empty;
            bool bCombinedPay = false;
            //pkandhari JIRA 6421
            bool bIsAddressSelect = false;
            System.Collections.Generic.Dictionary<int, OrgHierarchyMapping> objOrgHierarchyMapping = new Dictionary<int, OrgHierarchyMapping>();
            try
            {
                arrlstSelectedCheck = new ArrayList();
                arrlstSelectedAutoCheck = new ArrayList();
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                objFunctions = new Functions(m_objDataModelFactory);
                sbSQL = new StringBuilder();
                Functions.StartDocument(ref m_objDocument, ref objRootNode, ref objMessageNode, "ListOfChecks", m_iClientId);

                sAlias = Functions.m_sDBType == Constants.DB_ACCESS ? "AS" : string.Empty;
                sFromDate = p_bUseFromDate && Utilities.IsDate(p_sFromDateValue) ? Conversion.GetDate(p_sFromDateValue) : string.Empty;
                sToDate = p_bUseToDate && Utilities.IsDate(p_sToDateValue) ? Conversion.GetDate(p_sToDateValue) : string.Empty;

                switch (p_iOrgHierarchyLevel)
                {
                    case 1005: OrgLevel = "Client Code"; break;
                    case 1006: OrgLevel = "Company Code"; break;
                    case 1007: OrgLevel = "Operation Code"; break;
                    case 1008: OrgLevel = "Region Code"; break;
                    case 1009: OrgLevel = "Division Code"; break;
                    case 1010: OrgLevel = "Location Code"; break;
                    case 1011: OrgLevel = "Facility Code"; break;
                    case 1012: OrgLevel = "Department Code"; break;
                }

                dicMaxAmounts = null;
                dicMaxAmounts = GetMaxPrintAmounts(m_iUserId, m_iUserGroupId, m_sConnectionString, m_iClientId);

                orderBy = (string.IsNullOrEmpty(orderBy) ? "TransId" : orderBy);
                string orderByDataType = "text";
                switch (orderBy)
                {
                    case "TransId":
                    case "SortAmount":
                    case "SortCheckDate":
                        orderByDataType = "number";
                        break;
                }

                // default to ascending
                orderByDirection = (string.IsNullOrEmpty(orderByDirection) ? "ascending" : orderByDirection);
                sOrgLevelField = CommonFunctions.GetOrgLevelFieldFromCodeId(p_iOrgHierarchyLevel);

                if (p_sOrgHierarchy != string.Empty)
                {
                    string[] arrOrgList = p_sOrgHierarchy.Split(',');

                    foreach (string sOrgLevel in arrOrgList)
                    {
                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + sOrgLevel + ")");

                        using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                        {
                            if (objReader != null)
                            {
                                while (objReader.Read())
                                {
                                    iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);
                                }
                                objReader.Close();
                            }
                        }

                        sOrgLevelFieldValue = string.Format("ORG_HIERARCHY.{0} = {1} ", CommonFunctions.GetOrgLevelFieldFromCodeId(iTableId), sOrgLevel);
                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + "OR ";
                        sbSQL.Remove(0, sbSQL.Length);
                    }

                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0, sOrgLevelFieldList.Length - 3);
                }

                #region For Pre Checks
                sbSQL.AppendFormat(" SELECT FUNDS.TRANS_ID {0} FUNDS_TRANS_ID, FUNDS.TRANS_DATE, FUNDS.CTL_NUMBER, FUNDS.FIRST_NAME, ", sAlias);
                sbSQL.Append(" FUNDS.LAST_NAME, FUNDS.PMT_CURRENCY_CODE, FUNDS.PMT_CURRENCY_AMOUNT AMOUNT, FUNDS.CLAIM_NUMBER, FUNDS.AMOUNT BASEAMOUNT, FUNDS.PAYEE_EID, ");
                sbSQL.Append(" FUNDS.AUTO_CHECK_FLAG, FUNDS.COMBINED_PAY_FLAG, FUNDS.SUB_ACCOUNT_ID");
                sbSQL.AppendFormat(", ORG_HIERARCHY.{0} {1} ORG_LEVEL", sOrgLevelField, sAlias);
                sbSQL.AppendFormat(", CLAIM.PAYMNT_FROZEN_FLAG {0} PAYMNT_FROZEN_FLAG", sAlias);
                sbSQL.AppendFormat(", CLAIM.CLAIM_TYPE_CODE {0} CLAIM_TYPE_CODE", sAlias);
                sbSQL.AppendFormat(", CLAIM.LINE_OF_BUS_CODE {0} LINE_OF_BUS_CODE", sAlias);
                sbSQL.AppendFormat(", ENTITY.FREEZE_PAYMENTS {0} FREEZE_PAYMENTS", sAlias);

                //sbSQL.Append("  JOIN FUNDS_TRANS_SPLIT ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");              //avipinsrivas : JIRA - 1135, Commented Useless Join

                //pkandhari JIRA 6421               
                //if Setting is On
                bIsAddressSelect = m_objSysSettings.IsPayeeAddressSelect;
                if (bIsAddressSelect)
                {
                    sbSQL.AppendFormat(", FUNDS.ADDR1, FUNDS.ADDR2, FUNDS.ADDR3, FUNDS.ADDR4, FUNDS.CITY, S.STATE_NAME, FUNDS.ZIP_CODE, CT.CODE_DESC {0} COUNTRY ", sAlias);
                }
                sbSQL.Append(" FROM FUNDS");

                if (bIsAddressSelect)
                {
                    sbSQL.Append(" INNER JOIN STATES S ON FUNDS.STATE_ID = S.STATE_ROW_ID ");
                    sbSQL.Append(" INNER JOIN CODES_TEXT CT ON CT.CODE_ID = FUNDS.COUNTRY_CODE ");
                }
                //pkandhari JIRA 6421

                sbSQL.Append(" LEFT OUTER JOIN CLAIM ON CLAIM.CLAIM_ID = FUNDS.CLAIM_ID");                              //avipinsrivas : JIRA - 856
                sbSQL.Append(" JOIN ENTITY ON ENTITY.ENTITY_ID = FUNDS.PAYEE_EID");
                sbSQL.Append(" LEFT OUTER JOIN EVENT ON CLAIM.EVENT_ID = EVENT.EVENT_ID");                              //avipinsrivas : JIRA - 856
                sbSQL.Append(" LEFT OUTER JOIN ORG_HIERARCHY ON ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID");        //avipinsrivas : JIRA - 856

                sbSQL.Append(" WHERE STATUS_CODE IN ");
                sbSQL.Append("(" + objCCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS") + "," + objCCacheFunctions.GetCodeIDWithShort("I", "CHECK_STATUS") + ")");
                sbSQL.Append(" AND FUNDS.ACCOUNT_ID = " + p_iAccountId);
                sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0");
                sbSQL.Append(" AND FUNDS.PRECHECK_FLAG = 0 AND FUNDS.VOID_FLAG = 0");
                sbSQL.Append(" AND (CLAIM.PAYMNT_FROZEN_FLAG <> -1 OR CLAIM.PAYMNT_FROZEN_FLAG IS NULL)");
                sbSQL.Append(" AND (ENTITY.FREEZE_PAYMENTS <> -1 OR ENTITY.FREEZE_PAYMENTS IS NULL)");

                if (!string.IsNullOrEmpty(sToDate))
                {
                    sbSQL.AppendFormat(" AND FUNDS.TRANS_DATE <= '{0}'", sToDate);
                }

                if (!string.IsNullOrEmpty(sFromDate))
                {
                    sbSQL.AppendFormat(" AND TRANS_DATE >= '{0}'", sFromDate);
                }

                if (!p_bIncludeAutoPays)
                {
                    sbSQL.Append(" AND AUTO_CHECK_FLAG = 0");
                }

                if (m_objSysSettings.UseMultiCurrency != 0)
                {
                    sbSQL.AppendFormat(" AND FUNDS.PMT_CURRENCY_AMOUNT {0} 0", Functions.AllowZeroPayments(m_sConnectionString, m_iClientId) ? ">=" : ">");
                }
                else
                {
                    sbSQL.AppendFormat(" AND FUNDS.AMOUNT {0} 0", Functions.AllowZeroPayments(m_sConnectionString, m_iClientId) ? ">=" : ">");
                }

                if (!p_IncludeCombinedPays)
                {
                    sbSQL.Append(" AND COMBINED_PAY_FLAG = 0");
                }
                else
                {
                    // APEYKOV - JIRA RMA-4976 If sFromDate and sToDate are empty prevent exclude all combined payment from query
                    if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                        sbSQL.Append(" AND FUNDS.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID");

                    if (!string.IsNullOrEmpty(sToDate))
                    {
                        sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT > '{0}'", sToDate);
                    }
                    if (!string.IsNullOrEmpty(sFromDate))
                    {
                        sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT < '{0}'", sFromDate);
                    }
                    // APEYKOV - JIRA RMA-4976
                    if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                        sbSQL.Append(" )");
                }

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //if (p_bPrintEFTPayment)
                //{
                //    sbSQL.Append(" AND FUNDS.EFT_FLAG = -1");
                //}
                //else
                //{
                	// akaushik5 Changed for MITS 37841 Starts
                    //sbSQL.Append(" AND FUNDS.EFT_FLAG <> -1");
                    //sbSQL.Append(" AND (FUNDS.EFT_FLAG <> -1 OR FUNDS.EFT_FLAG IS NULL)");
                    // akaushik5 Changed for MITS 37841 Ends
                //}
                //JIRA:438 End:
                sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                if (!string.IsNullOrEmpty(p_sOrgHierarchy))
                {
                    sbSQL.AppendFormat(" AND ({0})", sOrgLevelFieldList);
                }

                if (!string.IsNullOrEmpty(p_sControlNumber))
                {
                    sbSQL.AppendFormat(" AND CTL_NUMBER LIKE '{0}%' ORDER BY CTL_NUMBER", p_sControlNumber);
                }

                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    if (objReader != null)
                    {

                        while (objReader.Read())
                        {
                            iTransId = Conversion.ConvertObjToInt(objReader.GetValue("FUNDS_TRANS_ID"), m_iClientId);
                            if (!this.SkipPayment(objReader, false, "AMOUNT", false, arrlstSelectedCheck, arrlstSelectedAutoCheck))
                            {
                                sCtlNumber = objReader.GetString("CTL_NUMBER");
                                sLastName = objReader.GetString("FIRST_NAME");
                                sFirstName = objReader.GetString("LAST_NAME");
                                if (sFirstName != string.Empty)
                                    sName = sFirstName + " " + sLastName;
                                else
                                    sName = sLastName;

                                //pkandhari JIRA 6421
                                if (bIsAddressSelect)
                                {
                                    var AddressLine1 = objReader.GetString("ADDR1") + " " + objReader.GetString("ADDR2") + " " + objReader.GetString("ADDR3") + " " + objReader.GetString("ADDR4");
                                    if (!string.IsNullOrEmpty(AddressLine1))
                                        AddressLine1 = AddressLine1 + "@^*";
                                    if (!string.IsNullOrEmpty(objReader.GetString("CITY")) && !string.IsNullOrEmpty(objReader.GetString("STATE_NAME")))
                                        sPayeeAddress = AddressLine1
                                            + objReader.GetString("CITY") + ", " + objReader.GetString("STATE_NAME") + " " + objReader.GetString("ZIP_CODE") + " " + objReader.GetString("COUNTRY");
                                    else if (!string.IsNullOrEmpty(objReader.GetString("CITY")))
                                        sPayeeAddress = AddressLine1
                                            + objReader.GetString("CITY") + " " + objReader.GetString("ZIP_CODE") + " " + objReader.GetString("COUNTRY");
                                    else if (!string.IsNullOrEmpty(objReader.GetString("STATE_NAME")))
                                        sPayeeAddress = AddressLine1
                                            + objReader.GetString("STATE_NAME") + " " + objReader.GetString("ZIP_CODE") + " " + objReader.GetString("COUNTRY");
                                    else
                                        sPayeeAddress = AddressLine1
                                            + objReader.GetString("ZIP_CODE") + " " + objReader.GetString("COUNTRY");

                                }
                                sClaimNumber = objReader.GetString("CLAIM_NUMBER");
                                string sortCheckDate = objReader.GetString("TRANS_DATE");
                                sCheckDate = Conversion.GetDBDateFormat(sortCheckDate, "d");

                                bCombinedPay = Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(objReader.GetValue("COMBINED_PAY_FLAG")));

                                if (Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(m_objSysSettings.UseMultiCurrency)))
                                {
                                    dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT")));
                                    sAmount = Riskmaster.Common.CommonFunctions.ConvertCurrency(iTransId, dblAmount, CommonFunctions.NavFormType.Funds, m_sConnectionString, m_iClientId);
                                }
                                else
                                {
                                    dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("BASEAMOUNT")));
                                    sAmount = string.Format("{0:C}", dblAmount);
                                }

                                if (m_objSysSettings.UseFundsSubAcc)
                                {
                                    iSubAccountId = Conversion.ConvertObjToInt(objReader.GetValue("SUB_ACCOUNT_ID"), m_iClientId);
                                    sSubAccount = this.SubAccountName(iSubAccountId);
                                }
                                else
                                {
                                    sSubAccount = string.Empty;
                                }

                                if (objChecks == null)
                                {
                                    Functions.CreateElement(objRootNode, "Checks", ref objChecks, m_iClientId);
                                    objChecks.SetAttribute("OrderBy", orderBy);
                                    objChecks.SetAttribute("OrderByDirection", orderByDirection);
                                    objChecks.SetAttribute("OrderByDataType", orderByDataType);
                                    objChecks.SetAttribute("IsAddressSelect", bIsAddressSelect.ToString());
                                }

                                Functions.CreateElement(objChecks, "Check", ref objCheck, m_iClientId);
                                objCheck.SetAttribute("CtlNumber", sCtlNumber);
                                objCheck.SetAttribute("PayeeName", sName);
                                objCheck.SetAttribute("ClaimNumber", sClaimNumber);
                                objCheck.SetAttribute("Amount", sAmount);
                                objCheck.SetAttribute("SortAmount", dblAmount.ToString());
                                objCheck.SetAttribute("CheckDate", sCheckDate);
                                objCheck.SetAttribute("SortCheckDate", sortCheckDate);
                                objCheck.SetAttribute("TransId", iTransId.ToString());
                                objCheck.SetAttribute("SubBankAccount", sSubAccount);
                                objCheck.SetAttribute("AutoCheck", false.ToString());
                                objCheck.SetAttribute("PayeeAddress", sPayeeAddress);////pkandhari JIRA 6421
                                iOrgLevel = objReader.GetInt("ORG_LEVEL");
                                if (!objOrgHierarchyMapping.ContainsKey(iOrgLevel))
                                {
                                    objCCacheFunctions.GetOrgInfo(iOrgLevel, ref abbr, ref name);
                                    objOrgHierarchyMapping.Add(iOrgLevel, new OrgHierarchyMapping(abbr, name));
                                }
                                else
                                {
                                    abbr = objOrgHierarchyMapping[iOrgLevel].Abbr;
                                }

                                objCheck.SetAttribute("CombinedPayment", bCombinedPay.ToString());

                                if (!string.IsNullOrEmpty(abbr))
                                {
                                    objCheck.SetAttribute("OrgHierarchy", abbr);
                                }
                                else
                                {
                                    objCheck.SetAttribute("OrgHierarchy", " ");
                                }
                            }
                        }
                        objReader.Close();
                    }
                }
                #endregion

                sbSQL.Remove(0, sbSQL.Length);

                #region For Auto Checks
                if (p_bIncludeAutoPays)
                {
                    sbSQL.Append(" SELECT FUNDS_AUTO.AUTO_TRANS_ID " + sAlias + " FUNDS_TRANS_ID ");
                    sbSQL.Append(" ,FUNDS_AUTO.PRINT_DATE, FUNDS_AUTO.CTL_NUMBER");
                    sbSQL.Append(" ,FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, FUNDS_AUTO.PMT_CURRENCY_CODE, FUNDS_AUTO.PMT_CURRENCY_AMOUNT AMOUNT, FUNDS_AUTO.AMOUNT BASEAMOUNT");
                    sbSQL.Append(" ,FUNDS_AUTO.CLAIM_NUMBER,FUNDS_AUTO_SPLIT.AMOUNT,FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE");
                    sbSQL.Append(" ,SUB_ACC_ID, FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG, FUNDS_AUTO.COMBINED_PAY_FLAG, FUNDS_AUTO.PAYEE_EID ");
                    sbSQL.AppendFormat(", ORG_HIERARCHY.{0} {1} ORG_LEVEL", sOrgLevelField, sAlias);
                    sbSQL.AppendFormat(", CLAIM.PAYMNT_FROZEN_FLAG {0} PAYMNT_FROZEN_FLAG", sAlias);
                    sbSQL.AppendFormat(", CLAIM.CLAIM_TYPE_CODE {0} CLAIM_TYPE_CODE", sAlias);
                    sbSQL.AppendFormat(", CLAIM.LINE_OF_BUS_CODE {0} LINE_OF_BUS_CODE", sAlias);
                    sbSQL.AppendFormat(", ENTITY.FREEZE_PAYMENTS {0} FREEZE_PAYMENTS", sAlias);

                    // npadhy JIRA RMA-9366 Starts Address is not coming on select check screen for Auto Checks
                    bIsAddressSelect = m_objSysSettings.IsPayeeAddressSelect;
                    if (bIsAddressSelect)
                    {
                        sbSQL.Append(", FUNDS_AUTO.ADDR1, FUNDS_AUTO.ADDR2, FUNDS_AUTO.ADDR3, FUNDS_AUTO.ADDR4, FUNDS_AUTO.CITY, S.STATE_NAME, FUNDS_AUTO.ZIP_CODE");
                    }
                    // npadhy JIRA RMA-9366 Ends Address is not coming on select check screen for Auto Checks

                    sbSQL.Append(" FROM FUNDS_AUTO");
                    sbSQL.Append(" JOIN FUNDS_AUTO_SPLIT ON FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
                    sbSQL.Append(" JOIN FUNDS_AUTO_BATCH ON FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID");
                    sbSQL.Append(" LEFT OUTER JOIN CLAIM ON CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID");         //avipinsrivas : JIRA 856
                    sbSQL.Append(" JOIN ENTITY ON ENTITY.ENTITY_ID = FUNDS_AUTO.PAYEE_EID");
                    sbSQL.Append(" LEFT OUTER JOIN EVENT ON CLAIM.EVENT_ID = EVENT.EVENT_ID");              //avipinsrivas : JIRA 856
                    sbSQL.Append(" LEFT OUTER JOIN ORG_HIERARCHY ON ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID");        //avipinsrivas : JIRA 856

                    // npadhy JIRA RMA-9366 Starts Address is not coming on select check screen for Auto Checks
                    // As we do not have Country code in Funds Auto we are not using this column here.
                    if (bIsAddressSelect)
                    {
                        sbSQL.Append(" LEFT OUTER JOIN STATES S ON FUNDS_AUTO.STATE_ID = S.STATE_ROW_ID ");
                    }
                    // npadhy JIRA RMA-9366 Ends Address is not coming on select check screen for Auto Checks
                    
                    sbSQL.Append(" WHERE FUNDS_AUTO.ACCOUNT_ID = " + p_iAccountId);
                    sbSQL.Append(" AND (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1 OR FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL)");
                    sbSQL.Append(" AND FUNDS_AUTO.PRECHECK_FLAG = 0");

                    if (!string.IsNullOrEmpty(sToDate))
                    {
                        sbSQL.AppendFormat(" AND PRINT_DATE <= '{0}'", sToDate);
                    }

                    if (!string.IsNullOrEmpty(sFromDate))
                    {
                        sbSQL.AppendFormat(" AND PRINT_DATE >= '{0}'", sFromDate);
                    }

                    if (!p_IncludeCombinedPays)
                    {
                        sbSQL.Append(" AND COMBINED_PAY_FLAG = 0");
                    }
                    else
                    {
                        // APEYKOV - JIRA RMA-4976 If sFromDate and sToDate are empty prevent exclude all combined payment from query
                        if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                            sbSQL.Append(" AND FUNDS_AUTO.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS_AUTO.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID");

                        if (!string.IsNullOrEmpty(sToDate))
                        {
                            sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT > '{0}'", sToDate);
                        }
                        if (!string.IsNullOrEmpty(sFromDate))
                        {
                            sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT < '{0}'", sFromDate);
                        }
                        // APEYKOV - JIRA RMA-4976
                        if (!string.IsNullOrEmpty(sFromDate) || !string.IsNullOrEmpty(sToDate))
                            sbSQL.Append(" )");
                    }

                    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    //JIRA:438 START: ajohari2
                    //if (p_bPrintEFTPayment)
                    //{
                    //    sbSQL.Append(" AND FUNDS_AUTO.EFT_FLAG = -1");
                    //}
                    //else
                    //{
	                    // akaushik5 Changed for MITS 37841 Starts
	                    //sbSQL.Append(" AND FUNDS.EFT_FLAG <> -1");
	                    //sbSQL.Append(" AND (FUNDS.EFT_FLAG <> -1 OR FUNDS.EFT_FLAG IS NULL)");
	                    // akaushik5 Changed for MITS 37841 Ends
                    //}
                    //JIRA:438 End: 
                    sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment

                    if (m_objSysSettings.UseMultiCurrency != 0)
                    {
                        sbSQL.Append(" AND FUNDS_AUTO.PMT_CURRENCY_AMOUNT >= 0");
                    }
                    else
                    {
                        sbSQL.Append(" AND FUNDS_AUTO.AMOUNT > 0");
                    }

                    if (!string.IsNullOrEmpty(p_sOrgHierarchy))
                    {
                        sbSQL.AppendFormat("AND ({0})", sOrgLevelFieldList);
                    }

                    if (!string.IsNullOrEmpty(p_sControlNumber))
                    {
                        sbSQL.AppendFormat(" AND CTL_NUMBER LIKE '{0}%' ORDER BY CTL_NUMBER", p_sControlNumber);
                    }
                    else
                    {
                        sbSQL.Append(" ORDER BY FUNDS_AUTO.AUTO_TRANS_ID");
                    }

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                    if (objReader != null)
                    {
                        iLastTransId = -1;
                        while (objReader.Read())
                        {
                            iTransId = Common.Conversion.ConvertObjToInt(objReader.GetValue("FUNDS_TRANS_ID"), m_iClientId);
                            if (iTransId != iLastTransId)
                            {
                                iLastTransId = iTransId;
                                // akaushik5 Changed for RMA-12770 Starts 
                                //if (!this.SkipPayment(objReader, true, "AMOUNT", false, arrlstSelectedCheck, arrlstSelectedAutoCheck))
                                if (!this.SkipPayment(objReader, true, "BASEAMOUNT", false, arrlstSelectedCheck, arrlstSelectedAutoCheck))
                                // akaushik5 Changed for RMA-12770 Ends
                                {
                                    sCtlNumber = objReader.GetString("CTL_NUMBER");
                                    sLastName = objReader.GetString("FIRST_NAME");
                                    sFirstName = objReader.GetString("LAST_NAME");
                                    if (sFirstName != string.Empty)
                                        sName = sFirstName + " " + sLastName;
                                    else
                                        sName = sLastName;

                                    sClaimNumber = objReader.GetString("CLAIM_NUMBER");
                                    string sortCheckDate = objReader.GetString("PRINT_DATE");
                                    sCheckDate = Conversion.GetDBDateFormat(sortCheckDate, "d");

                                    bCombinedPay = Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(objReader.GetValue("COMBINED_PAY_FLAG")));

                                    if (Conversion.ConvertStrToBool(Conversion.ConvertObjToStr(m_objSysSettings.UseMultiCurrency)))
                                    {
                                        dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT")));
                                        sAmount = Riskmaster.Common.CommonFunctions.ConvertCurrency(iTransId, dblAmount, CommonFunctions.NavFormType.Auto, m_sConnectionString, m_iClientId);
                                    }
                                    else
                                    {
                                        dblAmount = Conversion.ConvertStrToDouble(Conversion.ConvertObjToStr(objReader.GetValue("BASEAMOUNT")));
                                        sAmount = string.Format("{0:C}", dblAmount);

                                    }
                                    if (m_objSysSettings.UseFundsSubAcc)
                                    {
                                        iSubAccountId = Common.Conversion.ConvertObjToInt(objReader.GetValue("SUB_ACC_ID"), m_iClientId);
                                        sSubAccount = this.SubAccountName(iSubAccountId);
                                    }
                                    else
                                        sSubAccount = string.Empty;

                                    // npadhy JIRA RMA-9366 Starts Address is not coming on select check screen for Auto Checks
                                    if (bIsAddressSelect)
                                    {
                                        var AddressLine1 = objReader.GetString("ADDR1") + " " + objReader.GetString("ADDR2") + " " + objReader.GetString("ADDR3") + " " + objReader.GetString("ADDR4");
                                        if (!string.IsNullOrEmpty(AddressLine1))
                                            AddressLine1 = AddressLine1 + "@^*";
                                        if (!string.IsNullOrEmpty(objReader.GetString("CITY")) && !string.IsNullOrEmpty(objReader.GetString("STATE_NAME")))
                                            sPayeeAddress = AddressLine1
                                                + objReader.GetString("CITY") + ", " + objReader.GetString("STATE_NAME") + " " + objReader.GetString("ZIP_CODE");
                                        else if (!string.IsNullOrEmpty(objReader.GetString("CITY")))
                                            sPayeeAddress = AddressLine1
                                                + objReader.GetString("CITY") + " " + objReader.GetString("ZIP_CODE");
                                        else if (!string.IsNullOrEmpty(objReader.GetString("STATE_NAME")))
                                            sPayeeAddress = AddressLine1
                                                + objReader.GetString("STATE_NAME") + " " + objReader.GetString("ZIP_CODE");
                                        else
                                            sPayeeAddress = AddressLine1
                                                + objReader.GetString("ZIP_CODE");
                                    }
                                    // npadhy JIRA RMA-9366 Ends Address is not coming on select check screen for Auto Checks

                                    if (objChecks == null)
                                    {
                                        Functions.CreateElement(objRootNode, "Checks", ref objChecks, m_iClientId);
                                        objChecks.SetAttribute("OrderBy", orderBy);
                                        objChecks.SetAttribute("OrderByDirection", orderByDirection);
                                        objChecks.SetAttribute("OrderByDataType", orderByDataType);
                                    }

                                    Functions.CreateElement(objChecks, "Check", ref objCheck, m_iClientId);
                                    objCheck.SetAttribute("CtlNumber", sCtlNumber);
                                    objCheck.SetAttribute("PayeeName", sName);
                                    objCheck.SetAttribute("ClaimNumber", sClaimNumber);
                                    objCheck.SetAttribute("Amount", sAmount);
                                    objCheck.SetAttribute("SortAmount", dblAmount.ToString());
                                    objCheck.SetAttribute("CheckDate", sCheckDate);
                                    objCheck.SetAttribute("SortCheckDate", sortCheckDate);
                                    objCheck.SetAttribute("TransId", iTransId.ToString());
                                    objCheck.SetAttribute("SubBankAccount", sSubAccount);
                                    objCheck.SetAttribute("AutoCheck", true.ToString());
									// npadhy JIRA RMA-9366 Starts Address is not coming on select check screen for Auto Checks
                                    objCheck.SetAttribute("PayeeAddress", sPayeeAddress);
									// npadhy JIRA RMA-9366 Ends Address is not coming on select check screen for Auto Checks
                                    iOrgLevel = objReader.GetInt("ORG_LEVEL");
                                    objCCacheFunctions.GetOrgInfo(iOrgLevel, ref abbr, ref name);
                                    objCheck.SetAttribute("OrgHierarchy", abbr);
                                    //skhare7 R8 enhancement 
                                    objCheck.SetAttribute("CombinedPayment", bCombinedPay.ToString());
                                    //skhare7 R8 End
                                    if (abbr != string.Empty)
                                        objCheck.SetAttribute("OrgHierarchy", abbr);
                                    else
                                        objCheck.SetAttribute("OrgHierarchy", " ");
                                }
                            }
                        }
                        objReader.Close();
                    }
                }
                #endregion

                //Add by kuladeep for mits:22878
                #region For reading Setting from web config
                Hashtable htPrintChecks = RMConfigurationManager.GetDictionarySectionSettings("PrintChecks", m_sConnectionString, m_iClientId);//ps206
                if (htPrintChecks != null)
                {
                    if (htPrintChecks["PrintCheckLimit"] != null)
                    {
                        Functions.CreateElement(objRootNode, "PrintChecks", ref objChecks, m_iClientId);
                        objChecks.SetAttribute("PrintCheckLimit", Convert.ToString(htPrintChecks["PrintCheckLimit"]));
                    }
                }
                #endregion
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.GetListOfChecks.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }

                arrlstSelectedCheck = null;
                arrlstSelectedAutoCheck = null;
                objFunctions = null;
                objChecks = null;
                objCheck = null;
                objCCacheFunctions = null;
                sbSQL = null;
                objRootNode = null;
                objMessageNode = null;
            }
            return (m_objDocument);
        }
        // akaushik5 Changed for MITS 35846 Ends

        #endregion

        #region Common functions
        /// Name		: ReadCheckOptions
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize Check Options
        /// </summary>
        private void ReadCheckOptions()
        {
            DbReader objReader = null;
            string sSQL = "";
            bool bSetToDefault = true;

            try
            {
                sSQL = "SELECT * FROM CHECK_OPTIONS";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        m_iExportToFile = Common.Conversion.ConvertObjToInt(objReader.GetValue("CHECK_TO_FILE"), m_iClientId);
                        m_bConsolidate = objReader.GetBoolean("ROLLUP_CHECKS");
                        m_bAllowPostDtd = objReader.GetBoolean("ALLOW_POST_DATE");
                        //Sonam Pahariya: MCIC Batch Printing Filter MITS 20050 Date: 04/22/2010
                        m_iOrgHierarchyLevel = Common.Conversion.ConvertObjToInt(objReader.GetValue("PRE_CHK_ORG_LEVEL"), m_iClientId);
                        //Sonam- End
                        bSetToDefault = false;
                    }
                }
                if (bSetToDefault)
                {
                    m_iExportToFile = 0;
                    m_bConsolidate = false;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.ReadCheckOptions.InitcheckOptions", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    //objReader.Close();
                    objReader.Dispose();
                }
            }
        }

        /// Name		: SkipPayment
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Return true, if checks need to skip from payment.
        /// </summary>
        /// <param name="p_objReader">Reader object contains records</param>
        /// <param name="p_bCheckTypeAuto">Is Auto Check</param>
        /// <param name="p_sAmountName">Amount Name used in SQL </param>
        /// <param name="p_bUsingSelections">Is Using Selection</param>
        /// <param name="p_arrlstSelectedCheck">Selected list of Pre Checks</param>
        /// <param name="p_arrlstSelectedAutoCheck">Selected List of Pre Auto Checks</param>
        /// <returns>true/false</returns>
        private bool SkipPayment(DbReader p_objReader, bool p_bCheckTypeAuto, string p_sAmountName,
            bool p_bUsingSelections, ArrayList p_arrlstSelectedCheck, ArrayList p_arrlstSelectedAutoCheck)
        {

            LobSettings objLobSettings = null;
            Functions objFunctions = null;

            double dblAmount = 0.0;
            double dblAmountLimit = 0.0;
            bool bReturnValue = false;
            bool bPayment = false;
            bool bFreeze = false;
            int iTransId = 0;
            int iLob = 0;

            try
            {
                objFunctions = new Functions(m_objDataModelFactory);

                bPayment = p_objReader.GetBoolean("PAYMNT_FROZEN_FLAG");
                bFreeze = p_objReader.GetBoolean("FREEZE_PAYMENTS");
                iTransId = Common.Conversion.ConvertObjToInt(p_objReader.GetValue("FUNDS_TRANS_ID"), m_iClientId);
                iLob = Common.Conversion.ConvertObjToInt(p_objReader.GetValue("LINE_OF_BUS_CODE"), m_iClientId);
                dblAmount = Common.Conversion.ConvertStrToDouble(Common.Conversion.ConvertObjToStr(p_objReader.GetValue(p_sAmountName)));

                if (bPayment || bFreeze)
                {
                    bReturnValue = true;
                }
                else
                {
                    if (p_bUsingSelections)
                    {
                        if (!Functions.CheckInSelectionList(iTransId, p_bCheckTypeAuto, p_arrlstSelectedCheck, p_arrlstSelectedAutoCheck, m_iClientId))
                        {
                            bReturnValue = true; // If selection is used and check is not in selected list then skip true.
                        }
                        else
                        {
                            // check is in selection list. now do validity. If validity falis, skip true.
                            // do validity
                            try
                            {
                                objLobSettings = m_objColLobSettings[iLob];
                            }
                            catch
                            {
                                objLobSettings = null;
                            }
                            if (objLobSettings != null)
                            {
                                if (objLobSettings.PrtChkLmtFlag)
                                {
                                    if (dicMaxAmounts.ContainsKey(iLob)) // True
                                    {
                                        double dMaxValue = dicMaxAmounts[iLob];
                                        if (dblAmount > dMaxValue)
                                        {
                                            bReturnValue = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // do validity
                        try
                        {
                            objLobSettings = m_objColLobSettings[iLob];
                        }
                        catch
                        {
                            objLobSettings = null;
                        }
                        if (objLobSettings != null)
                        {
                            if (objLobSettings.PrtChkLmtFlag)
                            {
                                if (dicMaxAmounts.ContainsKey(iLob)) // True
                                {
                                    double dMaxValue = dicMaxAmounts[iLob];
                                    if (dblAmount > dMaxValue)
                                    {
                                        bReturnValue = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.SkipPayment.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objLobSettings = null;
                objFunctions = null;
            }
            return (bReturnValue);
        }

        /// Name		: SubAccountName
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get SubAccount Name
        /// </summary>
        /// <param name="p_iSubAccountId">Sub Account Id</param>
        /// <returns>SubAccount Name string</returns>
        private string SubAccountName(int p_iSubAccountId)
        {
            BankAccSub objBankAccSub = null;
            string sAcctName = "";
            bool bAccountFound = false;

            try
            {
                objBankAccSub = (BankAccSub)m_objDataModelFactory.GetDataModelObject("BankAccSub", false);
                try
                {
                    objBankAccSub.MoveTo(p_iSubAccountId);
                    bAccountFound = true;
                }
                catch
                {
                    bAccountFound = false;
                }
                if (bAccountFound)
                    sAcctName = objBankAccSub.SubAccName.Trim();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.SubAccountName.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBankAccSub != null)
                {
                    objBankAccSub.Dispose();
                    objBankAccSub = null;
                }
            }
            return (sAcctName);
        }

        /// Name		: GetAccounts
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the Accounts List.
        /// </summary>
        /// <returns>Account Array List</returns>
        private ArrayList GetAccounts()
        {
            DbReader objReader = null;
            ArrayList arrlstAccountDetail = null;
            AccountDetail structAccountDetail;

            string sSQL = "";
            string sEffTriger = "";
            string sTriggerFromDate = "";
            string sTriggerToDate = "";
            //Added by Shivendu for MITS 11526
            string sOrderBy = "";
            //Added by Shivendu for MITS 11526
            int iOrderCondition = 0;

            try
            {
                arrlstAccountDetail = new ArrayList();
                //Start by Shivendu for MITS 11526
                objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT ORDER_BANK_ACC FROM SYS_PARMS");
                if (objReader != null && objReader.Read())
                {
                    iOrderCondition = Common.Conversion.ConvertObjToInt(objReader.GetValue("ORDER_BANK_ACC"), m_iClientId);
                    switch (iOrderCondition)
                    {
                        case 0:
                            sOrderBy = "";
                            break;
                        case 1:
                            sOrderBy = " ORDER BY PRIORITY DESC, ACCOUNT_NAME ASC";
                            break;
                        case 2:
                            sOrderBy = " ORDER BY ACCOUNT_NAME";
                            break;
                        case 3:
                            sOrderBy = " ORDER BY ACCOUNT_NUMBER, ACCOUNT_NAME";
                            break;
                    }
                }
                objReader.Close();
                //End by Shivendu for MITS 11526
                //JIRA:438 START: ajohari2
                sSQL = " SELECT ACCOUNT_NAME,ACCOUNT_ID,IS_EFT_ACCOUNT,EFF_TRIGGER,TRIGGER_FROM_DATE,TRIGGER_TO_DATE "
                    + " FROM ACCOUNT  " + sOrderBy;//Order By Added by Shivendu for MITS 11526
                //JIRA:438 : ajohari2
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        sEffTriger = objReader.GetString("EFF_TRIGGER");
                        sTriggerFromDate = objReader.GetString("TRIGGER_FROM_DATE");
                        sTriggerToDate = objReader.GetString("TRIGGER_TO_DATE");
                        if (this.InEffDateRange(0, sEffTriger, sTriggerFromDate, sTriggerToDate))
                        {
                            structAccountDetail = new AccountDetail();
                            structAccountDetail.AccountName = objReader.GetString(0);
                            structAccountDetail.AccountId = Common.Conversion.ConvertObjToInt(objReader.GetValue(1), m_iClientId);
                            //JIRA:438 START: ajohari2
                            structAccountDetail.EFTAccount = Common.Conversion.ConvertObjToBool(objReader.GetValue(2), m_iClientId);
                            //JIRA:438 : ajohari2
                            arrlstAccountDetail.Add(structAccountDetail);
                        }
                    }
                }
                return (arrlstAccountDetail);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.GetAccounts.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                arrlstAccountDetail = null;
            }
        }

        private ArrayList GetDistributionTypes()
        {
            DbReader objReader = null;
            ArrayList arrlstDistributionType = null;
            DistributionType structDistributionType;
            string sSQL = "";
            int iDistributionTypeId = 0;
            string sDistributionType = string.Empty;
            int iCodeId = 0;
            try
            {
                arrlstDistributionType = new ArrayList();
                // We retrieve only those Codes for which we have saved the settings
                sSQL = " Select C.CODE_ID, CT.CODE_DESC  from CHECK_PRINT_OPTIONS CPO inner join CODES C on CPO.DSTRBN_TYPE_CODE = C.CODE_ID iNNER JOIN CODES_TEXT CT on C.CODE_ID = CT.CODE_ID ORDER BY C.CODE_ID";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        iDistributionTypeId = objReader.GetInt32("CODE_ID");
                        sDistributionType = objReader.GetString("CODE_DESC");
                        structDistributionType = new DistributionType();
                        structDistributionType.DistributionTypeId = iDistributionTypeId;
                        structDistributionType.DistributionTypeDesc = sDistributionType;
                        arrlstDistributionType.Add(structDistributionType);
                    }
                }

                // For Manual and EFT we will not have any setting in Check Print Options. So Add them
                using (LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientId))
                {
                    //structDistributionType = new DistributionType();
                    //iCodeId = objCache.GetCodeId("MAL", "DISTRIBUTION_TYPE");
                    //structDistributionType.DistributionTypeId = iCodeId;
                    //structDistributionType.DistributionTypeDesc = objCache.GetCodeDesc(iCodeId);
                    //arrlstDistributionType.Add(structDistributionType);
                    structDistributionType = new DistributionType();
                    iCodeId = objCache.GetCodeId("EFT", "DISTRIBUTION_TYPE");
                    structDistributionType.DistributionTypeId = iCodeId;
                    structDistributionType.DistributionTypeDesc = objCache.GetCodeDesc(iCodeId);
                    arrlstDistributionType.Add(structDistributionType);
                }
                return (arrlstDistributionType);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.GetAccounts.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                arrlstDistributionType = null;
            }
        }

        /// Name		: InEffDateRange
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Check Date is in effective date range.
        /// </summary>
        /// <param name="p_iClaimId">Claim Id</param>
        /// <param name="p_sEffTriger">EffTrigger</param>
        /// <param name="p_sTriggerFromDate">From Date</param>
        /// <param name="p_sTriggerToDate">To Date</param>
        /// <returns>true, if date is in effective date range.</returns>
        private bool InEffDateRange(int p_iClaimId, string p_sEffTriger, string p_sTriggerFromDate, string p_sTriggerToDate)
        {
            DbReader objReader = null;

            bool bInEffDateRange = false;
            string sDateOfClaim = "";
            string sDateOfEvent = "";
            string sCurrentDate = "";
            string sFromDate = "";
            string sToDate = "";
            string sSQL = "";

            try
            {
                sFromDate = Conversion.GetDBDateFormat(p_sTriggerFromDate, "d");
                sToDate = Conversion.GetDBDateFormat(p_sTriggerToDate, "d");

                if (p_sEffTriger == "")
                    bInEffDateRange = true;
                else
                {
                    switch (p_sEffTriger.ToUpper())
                    {
                        case "CLAIM DATE":
                            if (p_iClaimId > 0)
                            {
                                sSQL = " SELECT DATE_OF_CLAIM FROM CLAIM WHERE CLAIM_ID = " + p_iClaimId;

                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        sDateOfClaim = objReader.GetString("DATE_OF_CLAIM");
                                    }
                                    else
                                    {
                                        bInEffDateRange = true;
                                        break;
                                    }
                                }

                                if (objReader != null)
                                    objReader.Close();

                                if (sDateOfClaim != "")
                                {
                                    if (Utilities.IsDate(sFromDate) && Utilities.IsDate(sToDate))
                                    {
                                        if (Conversion.ConvertStrToLong(sDateOfClaim) <= Conversion.ConvertStrToLong(p_sTriggerToDate) && Conversion.ConvertStrToLong(sDateOfClaim) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                            bInEffDateRange = true;
                                        else
                                            bInEffDateRange = false;
                                    }
                                    else
                                    {
                                        if (Utilities.IsDate(sToDate))
                                        {
                                            if (Conversion.ConvertStrToLong(sDateOfClaim) <= Conversion.ConvertStrToLong(p_sTriggerToDate))
                                                bInEffDateRange = true;
                                            else
                                                bInEffDateRange = false;
                                        }
                                        else
                                        {
                                            if (Utilities.IsDate(sFromDate))
                                            {
                                                if (Conversion.ConvertStrToLong(sDateOfClaim) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                                    bInEffDateRange = true;
                                                else
                                                    bInEffDateRange = false;
                                            }
                                            else
                                            {
                                                bInEffDateRange = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    bInEffDateRange = true;
                                }
                            }
                            else
                            {
                                bInEffDateRange = true;
                            }
                            break;

                        case "EVENT DATE":
                            if (p_iClaimId > 0)
                            {
                                sSQL = " SELECT DATE_OF_EVENT FROM EVENT, CLAIM WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID "
                                    + " AND CLAIM.CLAIM_ID = " + p_iClaimId;

                                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        sDateOfEvent = objReader.GetString("DATE_OF_EVENT");
                                    }
                                    else
                                    {
                                        bInEffDateRange = true;
                                        break;
                                    }
                                }

                                if (objReader != null)
                                    objReader.Close();

                                if (sDateOfEvent != "")
                                {
                                    if (Utilities.IsDate(sFromDate) && Utilities.IsDate(sToDate))
                                    {
                                        if (Conversion.ConvertStrToLong(sDateOfEvent) <= Conversion.ConvertStrToLong(p_sTriggerToDate) && Conversion.ConvertStrToLong(sDateOfEvent) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                            bInEffDateRange = true;
                                        else
                                            bInEffDateRange = false;
                                    }
                                    else
                                    {
                                        if (Utilities.IsDate(sToDate))
                                        {
                                            if (Conversion.ConvertStrToLong(sDateOfEvent) <= Conversion.ConvertStrToLong(p_sTriggerToDate))
                                                bInEffDateRange = true;
                                            else
                                                bInEffDateRange = false;
                                        }
                                        else
                                        {
                                            if (Utilities.IsDate(sFromDate))
                                            {
                                                if (Conversion.ConvertStrToLong(sDateOfEvent) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                                    bInEffDateRange = true;
                                                else
                                                    bInEffDateRange = false;
                                            }
                                            else
                                            {
                                                bInEffDateRange = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    bInEffDateRange = true;
                                }
                            }
                            else
                            {
                                bInEffDateRange = true;
                            }
                            break;

                        case "CURRENT SYSTEM DATE":
                            sCurrentDate = Conversion.GetDate(System.DateTime.Today.Date.ToString("d"));
                            if (sCurrentDate != "")
                            {
                                if (Utilities.IsDate(sFromDate) && Utilities.IsDate(sToDate))
                                {
                                    if (Conversion.ConvertStrToLong(sCurrentDate) <= Conversion.ConvertStrToLong(p_sTriggerToDate) && Conversion.ConvertStrToLong(sCurrentDate) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                        bInEffDateRange = true;
                                    else
                                        bInEffDateRange = false;
                                }
                                else
                                {
                                    if (Utilities.IsDate(sToDate))
                                    {
                                        if (Conversion.ConvertStrToLong(sCurrentDate) <= Conversion.ConvertStrToLong(p_sTriggerToDate))
                                            bInEffDateRange = true;
                                        else
                                            bInEffDateRange = false;
                                    }
                                    else
                                    {
                                        if (Utilities.IsDate(sFromDate))
                                        {
                                            if (Conversion.ConvertStrToLong(sCurrentDate) >= Conversion.ConvertStrToLong(p_sTriggerFromDate))
                                                bInEffDateRange = true;
                                            else
                                                bInEffDateRange = false;
                                        }
                                        else
                                        {
                                            bInEffDateRange = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                bInEffDateRange = true;
                            }
                            break;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.InEffDateRange.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    //objReader.Close();
                    objReader.Dispose();
                }
            }
            return (bInEffDateRange);
        }

        // npadhy RMA-11632  We need to distinguish between the checks which are printed again and the transactions which will be printed for the first time
        // npadhy JIRA 6418 Starts  - Introducing Distribution Type in Funds/Auto Checks and using it for printing checks
        //pmittal5 Mits 14500 02/24/09 - Changed type of p_lNextCheckNum from Int to Long 
        private void GetAccountDetails(int p_iAccountId, int p_iDistributionType, ref int p_iNextBatchNumber, ref long p_lNextCheckNum, bool p_bGenerateBatchNumber, bool p_bIncludeAutoChecks, bool p_bRecreateCheck = false)
        {
            Account objAccount = null;
            Functions objFunctions = null;
            int iTemp = 0;
            CCacheFunctions objCCacheFunctions = null;
            int iTempNextBatchNumber = 0;
            try
            {
                objFunctions = new Functions(m_objDataModelFactory);
                objCCacheFunctions = new CCacheFunctions(m_sConnectionString, m_iClientId);
                // If the Batch Number needs to be retrived for Precheck then the maximum match number should be retrieved. Else for Post and print Check we need to retrieve
                // the corresponding batch generated based on distribution type
                objAccount = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);
                objAccount.MoveTo(p_iAccountId);

                if (objFunctions.MasterBankAcct(p_iAccountId, ref iTemp, ref p_lNextCheckNum, m_iClientId)) //Ash - cloud
                {
                    p_iNextBatchNumber = objAccount.NextBatchNumber;
                }
                else
                {
                    p_iNextBatchNumber = objAccount.NextBatchNumber;
                    p_lNextCheckNum = objAccount.NextCheckNumber;
                }

                if (p_iNextBatchNumber < 2)
                    p_iNextBatchNumber = 2;

                // npadhy 6418 If the Batch number is required then we retrive the Maximum Batch Number for a Distribution Type and Account. 
                if (!p_bGenerateBatchNumber )
                {
                    string sSql = string.Empty;
                    // npadhy RMA-11632  If the Check is printed for the first time, we need to retrieve checks which are printed and PreCheck_Flag is not 0
                    if (!p_bRecreateCheck)
                    {
                        sSql = string.Format("SELECT MAX(BATCH_NUMBER) FROM FUNDS  WHERE ACCOUNT_ID = {0} AND DSTRBN_TYPE_CODE = {1} AND PRECHECK_FLAG <> 0",
    p_iAccountId, p_iDistributionType);
                    }
                        // If the Check is printed again, then we need to retrive the checks which are already printed
                    else
                    {
                        sSql = string.Format("SELECT MAX(BATCH_NUMBER) FROM FUNDS  WHERE ACCOUNT_ID = {0} AND DSTRBN_TYPE_CODE = {1}",
    p_iAccountId, p_iDistributionType);
                    }

                    p_iNextBatchNumber = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSql), m_iClientId);

                    // npadhy 6418 Auto Checks is included, then we retrieve the BATCH number corresponding to FUNDS AUTO as well and return the maximum of the batch number from FUNDS and FUNDS AUTO 
                    if (p_bIncludeAutoChecks)
                    {
                        // npadhy RMA-11632  If the Check is printed for the first time, we need to retrieve checks which are printed and PreCheck_Flag is not 0
                        if (!p_bRecreateCheck)
                        {
                            // Retrieve the Batch Number saved based on Account No and Distribution Type
                            sSql = string.Format("SELECT MAX(CHECK_BATCH_NUM) FROM FUNDS_AUTO FA INNER JOIN FUNDS_AUTO_BATCH FAB ON FA.AUTO_BATCH_ID = FAB.AUTO_BATCH_ID WHERE ACCOUNT_ID = {0} AND DSTRBN_TYPE_CODE = {1} AND PRECHECK_FLAG <> 0", p_iAccountId, p_iDistributionType);
                        }
                        // If the Autocheck need to be printed again, then It would be printed from Funds

                        iTempNextBatchNumber = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSql), m_iClientId);

                        if (iTempNextBatchNumber > p_iNextBatchNumber)
                            p_iNextBatchNumber = iTempNextBatchNumber;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckSetup.GetAccountDetails.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objAccount != null)
                {
                    objAccount.Dispose();
                    objAccount = null;
                }
                objFunctions = null;
            }
        }
        #endregion

        #region Dispose
        private bool _isDisposed = false;
        /// Name		: Dispose
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// UnInitialize the objects. 
        /// </summary>
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                try
                {
                    if (m_objDataModelFactory != null)
                    {
                        m_objDataModelFactory.Dispose();
                        m_objDataModelFactory = null;
                    }
                }
                catch
                {
                    // Avoid raising any exception here.				
                }
                GC.SuppressFinalize(this);
            }
        }
        #endregion

        /// <summary>
        /// This function schedules PolicySystemUpdate exe to update the policy system with the change in funds
        /// </summary>
        /// <author>Amitosh
        /// </author>
        private void SchedulePolicyUpdate(int iCheckBatchId)
        {
            UserLogin objUserLogin = null;
            TaskManager objTaskManager = null;
            string sModule = string.Empty;
            string sReportType = string.Empty;
            string sTMConnectionString = string.Empty;
            int iPolicySystemId = 0;
            int iTimer = 2;
            try
            {
                objUserLogin = new UserLogin(m_sUserName, m_sDsnName, m_iClientId);
                objTaskManager = new TaskManager(objUserLogin, m_iClientId);//rkaur27
                ScheduleDetails objSchedule;
                string sModulename = string.Empty;
                //int iReportType;

                int iTypeId = 0;
                objSchedule = new ScheduleDetails();
                objSchedule.ScheduleState = 1;
                objSchedule.ScheduleTypeId = 1;
                sTMConnectionString = RMConfigurationManager.GetConnectionString("TaskManagerDataSource", m_iClientId);//rkaur27
                string sSql = "SELECT TASK_TYPE_ID  FROM TM_TASK_TYPE  WHERE NAME  = 'Policy System Update' ";

                using (DbReader objRdr = DbFactory.ExecuteReader(sTMConnectionString, sSql))
                {
                    if (objRdr.Read())
                    {
                        iTypeId = Conversion.ConvertObjToInt(objRdr[0], m_iClientId);

                    }
                    else
                    {
                        throw new Exception("TaskNotFound");
                    }
                }

                objSchedule.TaskTypeId = iTypeId;
                objSchedule.IntervalTypeId = 0;
                objSchedule.Interval = 0;
                objSchedule.TrigDirectory = "";
                objSchedule.FinalRunDTTM = "";
                objSchedule.TaskName = "Policy System Update";
                objSchedule.Mon = 0;
                objSchedule.Tue = 0;
                objSchedule.Wed = 0;
                objSchedule.Thu = 0;
                objSchedule.Fri = 0;
                objSchedule.Sat = 0;
                objSchedule.Sun = 0;
                objSchedule.Jan = 0;
                objSchedule.Feb = 0;
                objSchedule.Mar = 0;
                objSchedule.Apr = 0;
                objSchedule.May = 0;
                objSchedule.Jun = 0;
                objSchedule.Jul = 0;
                objSchedule.Aug = 0;
                objSchedule.Sep = 0;
                objSchedule.Oct = 0;
                objSchedule.Nov = 0;
                objSchedule.Dec = 0;
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT POLICY_SYSTEM_ID FROM POLICY_X_WEB WHERE FINANCIAL_UPD_FLAG =-1"))
                {
                    while (objReader.Read())
                    {
                        iPolicySystemId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);

                        objSchedule.Config = "<Task Name=\"Policy System Update\" cmdline=\"yes\"><Path>PolicySystemUpate.exe</Path><Args><arg>"
                          + objUserLogin.objRiskmasterDatabase.DataSourceName
                            + "</arg><arg>"
                            + objUserLogin.LoginName
                            + "</arg><arg>"
                            + iPolicySystemId.ToString()
                            + "</arg><arg>"
                            + iCheckBatchId.ToString()
                            + "</arg><arg>"
                            + "true"
                            + "</arg></Args></Task>";

                        #region Duplicacy check

                        int iAlreadyScheduledCount = 0;
                        Regex regex = new Regex(@"^(?<start>.*){(?<driver>Oracle.*)}(?<end>.*)$", RegexOptions.IgnoreCase);
                        if (regex.Match(sTMConnectionString).Success)
                        {
                            //for Oracle db
                            sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE TO_CHAR(LOWER(CONFIG)) = TO_CHAR(LOWER('" + objSchedule.Config + "'))";
                        }
                        else
                        {
                            //for sql serevr db
                            sSql = "SELECT COUNT(*) FROM TM_SCHEDULE WHERE LOWER(CONFIG) = LOWER('" + objSchedule.Config + "')";
                        }

                        using (DbReader objRdr = DbFactory.ExecuteReader(sTMConnectionString, sSql))
                        {
                            if (objRdr.Read())
                            {
                                Int32.TryParse(objRdr[0].ToString(), out iAlreadyScheduledCount);
                            }
                        }

                        if (iAlreadyScheduledCount > 0)
                        {
                            // the task is already scheduled. Exiting in which case.
                            return;// true;
                        }

                        #endregion
                        objSchedule.TimeToRun = Conversion.GetTime(DateTime.Now.TimeOfDay.Add(new TimeSpan(0, iTimer, 0)).ToString());
                        objSchedule.NextRunDTTM = Conversion.ToDbDateTime(DateTime.Now);
                        objSchedule.DayOfMonth = 0;
                        objTaskManager.SaveSettings(objSchedule);
                        iTimer++;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("TaskScheduleException", e.InnerException);
            }
        }

        /// <summary>
        /// This function will create an entry in ACTIVITY_TRACK
        /// </summary>
        /// <author>Amitosh
        /// </author>
        private void UpdateActivityTrack(int iCheckBatchId, int iTransId, int iAccountId)
        {
            string sSQL = string.Empty;
            int iActivityRowId = 0;
            int iPolicySystemId = 0;
            int iForeignTableId = 0;
            int iClaimId = 0;
            Funds objFunds = null;
            Claim objClaim = null;
            int iActivityType = 0;
            int iForeignTableKey = 0;
            try
            {

                //using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT POLICY_SYSTEM_ID FROM POLICY_X_WEB WHERE FINANCIAL_UPD_FLAG =-1"))
                //{
                //    while (objReader.Read())
                //    {
                //        iPolicySystemId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                //        sSelectSQL = "SELECT ACTIVITY_ROW_ID FROM ACTIVITY_TRACK WHERE CLAIM_ID=0 AND FOREIGN_TABLE_ID =" + objCache.GetTableId("FUNDS") + " AND FOREIGN_TABLE_KEY = "+iTransId+" AND UPLOAD_FLAG = -1 AND CHECK_BATCH_ID= " + iCheckBatchId + " AND POLICY_SYSTEM_ID=" + iPolicySystemId + " AND ACCOUNT_ID="+iAccountId;

                //        iActivityRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSelectSQL), m_iClientId);

                //        if (iActivityRowId == 0)
                //        {
                //            //    iNextUniqueId = Utilities.GetNextUID(Context.DbConn.ConnectionString,"ACTIVITY_TRACK");

                //            //sSql = "INSERT INTO ACTIVITY_TRACK (TRIGGER_TYPE_ROW_ID,CLAIM_ID,FOREIGN_TABLE_ID,FOREIGN_TABLE_KEY,UPLOAD_FLAG,DTTM_RCD_ADDED,ADDED_BY_USER) VALUES" + " (" + iNextUniqueId + "," + iClaimId + "," + iForeignTableId + "," + iForeignTableKey + ",-1," + System.DateTime.Now.ToString("yyyyMMddHHmmss") + "," + Context.RMUser.LoginName + ")";

                //            //Context.DbConn.ExecuteNonQuery(sSql);
                //            CommonFunctions.CreateLogForPolicyInterface(m_sConnectionString, m_sUserName, 0, objCache.GetTableId("FUNDS"), iTransId, iCheckBatchId, iPolicySystemId, objCache.GetCodeId("PP","ACTIVITY_TYPE"),iAccountId,0,0,0);

                //        }
                //    }
                //}

                if ((iTransId > 0) && (iCheckBatchId == 0))
                {
                    objFunds = (Funds)m_objDataModelFactory.Context.Factory.GetDataModelObject("Funds", false);
                    iForeignTableId = m_objDataModelFactory.Context.LocalCache.GetTableId("FUNDS");
                    objFunds.MoveTo(iTransId);
                    // if (((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")) || (((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS") )&& (this as Funds).VoidFlag) || (this as Funds).CollectionFlag)
                    //if (((this as Funds).StatusCode == this.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")) || (this as Funds).VoidFlag)
                    //{
                    iClaimId = objFunds.ClaimId;
                    iForeignTableKey = objFunds.TransId;
                    objClaim = (Claim)m_objDataModelFactory.Context.Factory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(iClaimId);
                    foreach (ClaimXPolicy objClaimXPolicy in objClaim.ClaimPolicyList)
                    {
                        Policy objPolicy = (Policy)m_objDataModelFactory.Context.Factory.GetDataModelObject("Policy", false);
                        objPolicy.MoveTo(objClaimXPolicy.PolicyId);

                        if (objPolicy.PolicySystemId > 0)
                        {

                            if ((objFunds.VoidFlag) && (objFunds.StatusCode == m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS")))
                            {
                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("OA", "ACTIVITY_TYPE");
                            }
                            else
                            {
                                iActivityType = objPolicy.Context.LocalCache.GetCodeId("PP", "ACTIVITY_TYPE");
                            }


                            sSQL = "SELECT ACTIVITY_ROW_ID FROM ACTIVITY_TRACK WHERE CLAIM_ID=" + iClaimId + " AND FOREIGN_TABLE_ID =" + iForeignTableId + " AND FOREIGN_TABLE_KEY = " + iForeignTableKey + " AND UPLOAD_FLAG = -1 AND CHECK_BATCH_ID= 0 AND POLICY_SYSTEM_ID=" + objPolicy.PolicySystemId + " AND ACTIVITY_TYPE = " + iActivityType + " AND ACCOUNT_ID =" + objFunds.AccountId + " AND CHANGE_AMOUNT =0";

                            iActivityRowId = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sConnectionString, sSQL), m_iClientId);



                            CommonFunctions.CreateLogForPolicyInterface(m_sConnectionString, iActivityRowId, m_objDataModelFactory.Context.RMUser.LoginName, iClaimId, iForeignTableId, iForeignTableKey, 0, objPolicy.PolicySystemId, iActivityType, objFunds.AccountId, 0, 0, 0, objFunds.StatusCode, Conversion.ConvertBoolToInt(objFunds.VoidFlag, m_iClientId), Conversion.ConvertBoolToInt(objFunds.CollectionFlag, m_iClientId), false, m_iClientId,0,0,string.Empty); //aaggarwal29 : MITS 35586 changes



                        }
                        objPolicy.Dispose();
                    }
                    objClaim.Dispose();
                }
                else if ((iTransId == 0) && (iCheckBatchId > 0))
                {
                    sSQL = @"UPDATE ACTIVITY_TRACK SET CHECK_STATUS = " + m_objDataModelFactory.Context.LocalCache.GetCodeId("P", "CHECK_STATUS") +
                           " WHERE ACTIVITY_ROW_ID IN ( SELECT ACTIVITY_ROW_ID FROM ACTIVITY_TRACK,FUNDS WHERE  ACTIVITY_TRACK.FOREIGN_TABLE_ID = " + m_objDataModelFactory.Context.LocalCache.GetTableId("FUNDS") +
                           " AND FUNDS.TRANS_ID =  ACTIVITY_TRACK.FOREIGN_TABLE_KEY " + " AND ACTIVITY_TRACK.UPLOAD_FLAG= -1  AND  FUNDS.ACCOUNT_ID  = " + iAccountId + " AND FUNDS.BATCH_NUMBER = " + iCheckBatchId + ")";

                    m_objDataModelFactory.Context.DbConn.ExecuteNonQuery(sSQL);
                }
            }
            finally
            {
                if (objClaim != null)
                {
                    objClaim.Dispose();
                    objClaim = null;
                }
                if (objFunds != null)
                {
                    objFunds.Dispose();
                    objFunds = null;
                }
            }
        }
    }
}
