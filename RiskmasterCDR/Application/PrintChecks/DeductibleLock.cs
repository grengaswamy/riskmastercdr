﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Riskmaster.Common;
using Riskmaster.Db;

namespace Riskmaster.Application.PrintChecks
{
    public class DeductibleLock
    {
        string m_sUserName, m_sConnectionString, m_sTableName, m_sCoverageIDs = string.Empty, m_sCovGroupIDs = string.Empty;

        private DeductibleLock()
        {
            m_sTableName = "DEDUCTIBLE_LOCK";
        }

        public DeductibleLock(string p_sConnectionString, string p_sUserName)
            : this()
        {
            m_sUserName = p_sUserName;
            m_sConnectionString = p_sConnectionString;
        }

        public bool AquireLock(string p_sCoverageIDs, string p_sGroupIDs)
        {
            bool bLockAquired = false;
            string sCurrentDateTime = string.Empty;
            string sSQL = string.Empty;
            int iLockCount = 0;
            try
            {
                //Start: Added by Sumit Agarwal as the values for p_sCoverageIDs and p_sGroupIDs might come blank: 10/29/2014
                if (string.IsNullOrEmpty(p_sCoverageIDs))
                {
                    p_sCoverageIDs = "''";
                }
                if (string.IsNullOrEmpty(p_sGroupIDs))
                {
                    p_sGroupIDs = "''";
                }
                //End: Added by Sumit Agarwal as the values for p_sCoverageIDs and p_sGroupIDs might come blank: 10/29/2014
                sCurrentDateTime = Conversion.ToDbDateTime(DateTime.Now);

                using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    objConn.Open();
                    lock (new object())
                    {
                        sSQL = string.Format("SELECT COUNT(*) FROM {0} WHERE USER_CHECKOUT <>'{3}' AND (CVG_ID IN ({1}) OR COV_GROUP_ID IN ({2}))", m_sTableName, p_sCoverageIDs, p_sGroupIDs, m_sUserName);
                        iLockCount = objConn.ExecuteInt(sSQL);
                        if (iLockCount > 0 )
                        {
                            //Lock already exists
                            bLockAquired = false;
                        }
                        else
                        {
                            foreach (string sCvgId in p_sCoverageIDs.Split(','))
                            {
                                if (sCvgId.Replace("'", "").Trim().Length > 0)
                                {
                                    objConn.ExecuteNonQuery(string.Format("INSERT INTO {0} (CVG_ID, COV_GROUP_ID, USER_CHECKOUT, DTTM_CHECKOUT) VALUES({1},{2},'{3}','{4}')"
                                        , m_sTableName, sCvgId.Replace("'", ""), 0, m_sUserName, sCurrentDateTime));
                                }
                            }
                            foreach (string sGrpId in p_sGroupIDs.Split(','))
                            {
                                if (sGrpId.Replace("'", "").Trim().Length > 0)
                                {
                                    objConn.ExecuteNonQuery(string.Format("INSERT INTO {0} (CVG_ID, COV_GROUP_ID, USER_CHECKOUT, DTTM_CHECKOUT) VALUES({1},{2},'{3}','{4}')"
                                        , m_sTableName, sGrpId.Replace("'", ""), 0, m_sUserName, sCurrentDateTime));
                                }
                            }
                            
                            m_sCoverageIDs = p_sCoverageIDs;
                            m_sCovGroupIDs = p_sGroupIDs;
                            bLockAquired = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return bLockAquired;
        }

        public bool ReleaseLock()
        {
            if (!string.IsNullOrWhiteSpace(m_sCoverageIDs) || !string.IsNullOrWhiteSpace(m_sCovGroupIDs))
            {
                return ReleaseLock(m_sCoverageIDs, m_sCovGroupIDs);
            }
            return false;
        }

        public bool ReleaseLock(string p_sCoverageIDs, string p_sGroupIDs)
        {
            bool bLockReleased = false;
            string sSQL = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(p_sCoverageIDs))
                {
                    p_sCoverageIDs = "''";
                }
                if (string.IsNullOrWhiteSpace(p_sGroupIDs))
                {
                    p_sGroupIDs = "''";
                }
                lock(new object())
                {
                    sSQL = string.Format("DELETE FROM {0} WHERE USER_CHECKOUT='{3}' OR CVG_ID IN ({1}) OR COV_GROUP_ID IN ({2})"
                        , m_sTableName, p_sCoverageIDs, p_sGroupIDs, m_sUserName);

                    using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                    {
                        objConn.Open();         //Added by Sumit Agarwal to open the connection: 10/29/2014
                        objConn.ExecuteNonQuery(sSQL);
                        bLockReleased = true;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return bLockReleased;
        }
    }
}
