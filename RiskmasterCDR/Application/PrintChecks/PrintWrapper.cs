using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;

using C1.C1PrintDocument;
using C1.C1PrintDocument.Export;
using System.Diagnostics;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;



namespace Riskmaster.Application.PrintChecks
{
    /**************************************************************
     * $File		: PrintWrapper.cs
     * $Revision	: 1.0.0.0
     * $Date		: 01/05/2005
     * $Author		: Vaibhav Kaushik
     * $Comment		: PrintWrapper class has basic functions to create Print Document.
     * $Source		:  	
    **************************************************************/
    internal class PrintWrapper
    {
        #region Variable Declarations
        /// <summary>
        /// Private variable to store C1PrintDocumnet object instance.
        /// </summary>
        C1PrintDocument m_objPrintDoc = null;
        /// <summary>
        /// Private variable to store CurrentX
        /// </summary>
        private double m_dblCurrentX = 0;
        /// <summary>
        /// Private variable to store CurrentY
        /// </summary>
        private double m_dblCurrentY = 0;
        /// <summary>
        /// Private variable to store Font object.
        /// </summary>
        private Font m_objFont = null;
        /// <summary>
        /// Private variable to store Text object.
        /// </summary>
        private RenderText m_objText = null;
        /// <summary>
        /// Private variable to store PageWidth
        /// </summary>
        private double m_dblPageWidth = 0.0;
        /// <summary>
        /// Private variable to store PageHeight
        /// </summary>
        private double m_dblPageHeight = 0.0;

        /// <summary>
        /// Private variable to tell whether its a  Clone
        /// </summary>		
        private bool m_bClone = false;

        private int m_iClientId = 0;
        #endregion

        #region Constructor
        /// Name		: PrintWrapper
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Constructor, initializes the variables to the default value
        /// </summary>		
        internal PrintWrapper(int p_iClientId)
        {
            m_objPrintDoc = new C1PrintDocument();
            m_objPrintDoc.DefaultUnit = UnitTypeEnum.Twip;
            m_objPrintDoc.PageSettings.Margins.Top = 0;
            m_objPrintDoc.PageSettings.Margins.Left = 0;
            m_objPrintDoc.PageSettings.Margins.Bottom = 0;
            m_objPrintDoc.PageSettings.Margins.Right = 0;
            m_iClientId = p_iClientId;
        }
        internal void dispose()
        {
            //tanwar2 - mits 30910 - start
            //m_objPrintDoc.KillDoc();
            //m_objPrintDoc.Dispose();
            //m_objText.Dispose();
            //m_objFont.Dispose();
            if (m_objPrintDoc!=null)
            {
                m_objPrintDoc.KillDoc();
                m_objPrintDoc.Dispose();
            }
            if (m_objText!=null)
            {
                m_objText.Dispose();
            }
            if (m_objFont != null)
            {
                m_objFont.Dispose();
            }
            //tanwar2 - mits 30910 - end
		}
		#endregion 

        //temp clone to copy exixting stuff
        public PrintWrapper Clone(bool p_bIsLandscape)
        {
            PrintWrapper temp = new PrintWrapper(m_iClientId);
            temp.CurrentX = this.CurrentX;
            temp.CurrentY = this.CurrentY;
            temp.m_dblPageHeight = this.PageHeight;
            temp.m_dblPageWidth = this.PageWidth;
            temp.m_objFont = this.m_objFont;
            temp.m_bClone = true;
            temp.StartDoc(p_bIsLandscape, m_iClientId);
            return temp;
        }
        internal bool IsClone
        {
            get
            {
                return (m_bClone);
            }
        }


        /// Name		: StartDoc
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the document
        /// </summary>
        /// <param name="p_bIsLandscape">IsLandscape</param>
        internal void StartDoc(bool p_bIsLandscape, int p_iClientId)
        {
            Rectangle objRect;

            try
            {
                m_objPrintDoc.PageSettings.Landscape = p_bIsLandscape;
                //objRect = m_objPrintDoc.PageSettings.Bounds ;			
                //m_dblPageHeight = ( objRect.Height / 100 ) * 1440 ;
                //m_dblPageWidth = ( objRect.Width / 100 ) * 1440  ;
                if (p_bIsLandscape)
                {
                    m_dblPageHeight = 8.5 * 1440;
                    m_dblPageWidth = 11 * 1440;
                    // JP ??? Is this correct?     m_dblPageHeight += 700 ;
                }
                else
                {
                    m_dblPageHeight = 11 * 1440;
                    m_dblPageWidth = 8.5 * 1440;
                    // JP ??? Is this correct?     m_dblPageWidth += 700 ;
                }
                m_objPrintDoc.StartDoc();

                m_objFont = new Font("Arial", 8);
                m_objText = new RenderText(m_objPrintDoc);
                m_objText.Style.Font = m_objFont;
                m_objText.Style.WordWrap = false;
                m_objText.AutoWidth = true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.StartDoc.Error", p_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Read property for PageHeight.
        /// </summary>
        internal double PageHeight
        {
            get
            {
                return (m_dblPageHeight);
            }
        }
        /// <summary>
        /// Read property for PageWidth.
        /// </summary>
        internal double PageWidth
        {
            get
            {
                return (m_dblPageWidth);
            }
        }
        /// <summary>
        /// Read/Write property for CurrentX.
        /// </summary>
        internal double CurrentX
        {
            get
            {
                return (m_dblCurrentX);
            }
            set
            {
                m_dblCurrentX = value;
            }
        }
        /// <summary>
        /// Read/Write property for CurrentY.
        /// </summary>
        internal double CurrentY
        {
            get
            {
                return (m_dblCurrentY);
            }
            set
            {
                m_dblCurrentY = value;
            }
        }

        /// Name		: PrintText
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Render Text on the document.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        internal void PrintText(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "\r";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintText.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: PrintTextNoCr
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Render Text on the document with no "\r" on the end.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        internal void PrintTextNoCr(string p_sPrintText)
        {
            try
            {
                m_objText.Text = p_sPrintText + "";
                m_objText.AutoWidth = true;
                m_objPrintDoc.RenderDirect(m_dblCurrentX, m_dblCurrentY, m_objText);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextNoCr.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: PrintTextEndAt
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Print text such that it ends at given X Co-ordinate.
        /// </summary>
        /// <param name="p_sPrintText">String text to print</param>
        internal void PrintTextEndAt(string p_sPrintText)
        {
            try
            {
                this.CurrentX = m_dblCurrentX - this.GetTextWidth(p_sPrintText);
                this.PrintText(p_sPrintText);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintTextEndAt.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: SetFont
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Set Font 
        /// </summary>
        /// <param name="p_sFontName">Font Name</param>
        /// <param name="p_dblFontSize">Font Size</param>
        internal void SetFont(string p_sFontName, double p_dblFontSize)
        {
            try
            {
                // Vaibhav : As per suggestion from Jim, to make double sure that the MICR sequence on checks,
                // would be printed using Installed font i.e. Covix MICR Unicode.

                if (p_sFontName.Trim() == "MICR")
                    p_sFontName = "Covix MICR Unicode";

                //added:Yukti,Dt:12/12/2013, MITS 33745, Added condition for condition "Monotype Corsiva"
                if (p_sFontName != "Monotype Corsiva")
                {
                    m_objFont = new Font(p_sFontName, (float)p_dblFontSize);
                    m_objText.Style.Font = m_objFont;
                }
                //Ended:Yukti, DT:12/12/2013
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFont.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: SetFontBold
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Set Font Bold
        /// </summary>
        /// <param name="p_bBold">Bool flag</param>
        internal void SetFontBold(bool p_bBold)
        {
            try
            {
                if (p_bBold)
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size, FontStyle.Bold);
                else
                    m_objFont = new Font(m_objFont.Name, m_objFont.Size);

                m_objText.Style.Font = m_objFont;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.SetFontBold.Error", m_iClientId), p_objEx);
            }

        }
        /// Name		: GetTextWidth
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get text width.
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Width</returns>
        internal double GetTextWidth(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundWidth);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextWidth.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: GetTextHeight
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get Text Height
        /// </summary>
        /// <param name="p_sText">String Text</param>
        /// <returns>Height</returns>
        internal double GetTextHeight(string p_sText)
        {
            try
            {
                m_objText.Text = p_sText + "";
                return (m_objText.BoundHeight);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.GetTextHeight.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: Save
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended		*   Amendment											*    Author
        /// 20050317			For securing PDF, Password also taking into account.    Vaibhav Kaushik
        ///************************************************************
        /// <summary>
        /// Save the C1PrintDocument in PDF format.
        /// </summary>
        /// <param name="p_sPath">Path</param>
        /// <param name="p_sPassword">Security Password</param>
        internal void Save(string p_sPath, string p_sPassword)
        {
            PdfExportProvider objExportProvider = null;
            PdfExporter objExporter = null;

            try
            {
                objExportProvider = (PdfExportProvider)ExportProviders.AvailableProviders["PDF"];
                objExporter = (PdfExporter)objExportProvider.NewExporter();
                objExporter.Document = m_objPrintDoc;

                // Set the security options.
                objExporter.OutputFileName = p_sPath;
                objExporter.Security.UserPassword = p_sPassword;
                objExporter.Security.AllowCopyContent = false;
                objExporter.Security.AllowEditAnnotations = false;
                objExporter.Security.AllowEditContent = false;
                objExporter.EmbedTrueTypeFonts = true;
                objExporter.UseCompression = true;
                // TODO 
                //objExporter.DocumentInfo.Company = "CSC" ;

                // This is an back ground process so dialog boxes should not be displayed.
                objExporter.ShowOptions = false;
                objExporter.Export();
                /*Process proc = new Process();
                proc.StartInfo.FileName = @"C:\Parag\Print\pdfprint_cmd\pdfprint_cmd\pdfprint.exe";
                string strArguments = "";
                strArguments += " -printer DEV8000_IP -chgbin  1 -scalex 100 ";
                strArguments += p_sPath;
                Console.WriteLine(strArguments);
                proc.StartInfo.Arguments = @strArguments;
                proc.Start();


                proc.WaitForExit();*/
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objExportProvider = null;
                objExporter = null;
            }

        }

        /// Name		: Save
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended		*   Amendment											*    Author
        ///************************************************************
        /// <summary>
        /// Save the C1PrintDocument in PDF format.
        /// </summary>
        /// <param name="p_sPath">Path</param>
        internal void Save(string p_sPath)
        {
            try
            {
                m_objPrintDoc.ExportToPDF(p_sPath, false);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.Save.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: EndDoc
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Call the end event.
        /// </summary>
        internal void EndDoc()
        {
            try
            {
                m_objPrintDoc.EndDoc();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.EndDoc.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: PrintLine
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Print Line
        /// </summary>
        /// <param name="p_dblFromX">From X</param>
        /// <param name="p_dblFromY">From Y</param>
        /// <param name="p_dblToX">To X</param>
        /// <param name="p_dblToY">To Y</param>
        internal void PrintLine(double p_dblFromX, double p_dblFromY, double p_dblToX, double p_dblToY)
        {
            try
            {
                m_objPrintDoc.RenderDirectLine(p_dblFromX, p_dblFromY, p_dblToX, p_dblToY);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintLine.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: NewPage
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Start a new page.
        /// </summary>
        internal void NewPage()
        {
            try
            {
                m_objPrintDoc.NewPage();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.NewPage.Error", m_iClientId), p_objEx);
            }
        }

        /// Name		: PageNumber
        /// Author		: Zakir Zafar Mohammad
        /// Date Created: 07/27/2015		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get page number.
        /// </summary>
        internal int PageNumber()
        {
            try
            {
                return m_objPrintDoc.PageCount;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PageCount.Error"), p_objEx);
            }
        }

        
        /// Name		: PrintImage
        /// Author		: Vaibhav Kaushik
        /// Date Created: 01/05/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Print the image in center of the page, with its actual size.
        /// </summary>
        /// <param name="p_sPath">Image Path</param>
        /// //skhare7 MITS 23664
        internal void PrintImage(string p_sPath)
        {

            Bitmap objBitMapIamge = null;
            ImageAlignDef objAlignDef = null;

            double dblWidth = 0.0;
            double dblHeight = 0.0;
            double dblTopX = 0.0;
            double dblTopY = 0.0;

            try
            {
                if (p_sPath == string.Empty)
                    return;
                else if (!File.Exists(p_sPath))
                    return;

                objBitMapIamge = new Bitmap(p_sPath, true);
                objAlignDef = new ImageAlignDef(ImageAlignHorzEnum.Center, ImageAlignVertEnum.Center, false, false, true, false, false);


                //
                // Image height is in pixel. By dividing it from resolution, Get the height in Inch.
                // covert inch to twips by multiplying 1440. 
                //

                dblWidth = (objBitMapIamge.Width / objBitMapIamge.HorizontalResolution) * 1440;
                dblHeight = (objBitMapIamge.Height / objBitMapIamge.VerticalResolution) * 1440;
                dblHeight = this.PageHeight;
                //
                // Get the top( X, Y ) by aligining-this.PageWidth  as per page width 7 height. 
                //


                dblTopX = (this.PageWidth - dblWidth) / 2;
                dblTopY = 0.0;

                objBitMapIamge.MakeTransparent();
                m_objPrintDoc.RenderDirectImage(dblTopX, dblTopY, objBitMapIamge, dblWidth, dblHeight, objAlignDef);
                m_objPrintDoc.PageLayer = DocumentPageLayerEnum.Main;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintImage.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objBitMapIamge = null;
                objAlignDef = null;

            }
        }
        //skhare7 MITS 23664 End
        internal void PrintImage(MemoryStream p_objMS)
        {
            PrintImage(p_objMS, false);
        }

        internal void PrintImage(MemoryStream p_objMS, bool bScaleDownToPageSize)
        {
            Image objImage = null;
            ImageAlignDef objAlignDef = null;

            double dblWidth = 0.0;
            double dblHeight = 0.0;
            double dblTopX = 0.0;
            double dblTopY = 0.0;

            try
            {
                p_objMS.Seek(0, SeekOrigin.Begin);
                objImage = Image.FromStream(p_objMS);

                objAlignDef = new ImageAlignDef(ImageAlignHorzEnum.Center, ImageAlignVertEnum.Center, false, false, true, false, false);
                m_objPrintDoc.PageLayer = DocumentPageLayerEnum.Background;

                //
                // Image height is in pixel. By dividing it from resolution, Get the height in Inch.
                // covert inch to twips by multiplying 1440. 
                //

                dblWidth = (objImage.Width / objImage.HorizontalResolution) * 1440;
                dblHeight = (objImage.Height / objImage.VerticalResolution) * 1440;

                //
                // Get the top( X, Y ) by aligining as per page width 7 height. 
                //

                //if (bScaleDownToPageSize && (dblWidth > this.PageWidth) && (dblHeight > this.PageHeight))
                //{
                //    // This workaround is designed to scale down a Visio 2003/2007 EMF based on the
                //    //  CSCCHECK.VSD template file. It should also work for other checks stocks that have
                //    //   deviated from CSCCHECK but they will probably require some coordinate tuning.
                //    objAlignDef = new ImageAlignDef( ImageAlignHorzEnum.Left , ImageAlignVertEnum.Top , true , true , false , false , false );

                //    dblWidth = 8.0463 * 1440.0; // this.PageWidth - .2 * 1440;  // .5 is two .25 inch margins
                //    dblHeight = 10.7315 * 1440.0; // this.PageHeight - .3 * 1440;

                //    dblTopX = 326;
                //    dblTopY = 65;
                //}

                if (bScaleDownToPageSize)
                {
                    objAlignDef = new ImageAlignDef(ImageAlignHorzEnum.Left, ImageAlignVertEnum.Top, true, true, false, false, false);

                    //dblWidth = this.PageWidth - .2 * 1440; // this.PageWidth - .2 * 1440;  // .5 is two .25 inch margins
                    //dblHeight = this.PageHeight - .3 * 1440; // this.PageHeight - .3 * 1440;
                    dblWidth = 8.5 * 1440.0;
                    dblHeight = 10.7315 * 1440.0;
                    dblTopX = 50;
                    dblTopY = 25;
                    //dblTopX =50;
                    //dblTopY = 25;
                }
                else
                {
                    dblTopX = (this.PageWidth - dblWidth) / 2;
                    dblTopY = (this.PageHeight - dblHeight) / 2;
                }

                m_objPrintDoc.RenderDirectImage(dblTopX, dblTopY, objImage, dblWidth, dblHeight, objAlignDef);
                m_objPrintDoc.PageLayer = DocumentPageLayerEnum.Main;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintWrapper.PrintImage.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objImage = null;
                objAlignDef = null;
            }
        }
    }
}
