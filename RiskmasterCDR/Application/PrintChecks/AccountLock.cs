using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;

namespace Riskmaster.Application.PrintChecks
{
	/**************************************************************
	 * $File		: AccountLock.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/05/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: Permits only single user to use PrintChecks functionality from a given bank account at a time. 
	 * $Source		: 	
	**************************************************************/
	internal class AccountLock
	{
		#region Variable Declarations
		/// <summary>
		/// Private variable to store DTTM
		/// </summary>
		private bool m_bHasLock = false ;
		/// <summary>
		/// Private variable to store UserName
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store Connection String
		/// </summary>
		private string m_sConnectionString = "" ;

        /// <summary>
        /// ClientID for multi-tenancy , cloud
        /// </summary>
        private int m_iClientId = 0;

		internal bool HasLock 
		{
			get
			{
				return( m_bHasLock ) ;
			}
			set
			{
				m_bHasLock = value ;
			}
		}
		#endregion

		#region Constructor
		/// Name		: AccountLock
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sConnectionString">Connection String </param>
		/// <param name="p_sUserName">User Name</param>
		internal AccountLock( string p_sConnectionString , string p_sUserName, int p_iClientId )
		{			
			m_sUserName = p_sUserName ;
			m_sConnectionString = p_sConnectionString ;
            m_iClientId = p_iClientId;
		}
		#endregion 
		/// Name		: AcquireLock
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Acquires exclusive rights to print checks from a given bank account.
		/// </summary>
		/// <param name="p_iAccountId">AccountId</param>
		/// <param name="p_sUser">UserName who has the lock</param>
		/// <param name="p_sDTTM">DTTM when lock was acquired</param>
		/// <returns>true, if Account has locked for current user.</returns>				
		internal bool AcquireLock( int p_iAccountId , ref string p_sUser, ref string p_sDTTM )
		{
			DbConnection objConn = null ;
			DbReader objReader = null ;
			
			string sCurrentUser = "" ;
			string sCurrentDTTM = "" ;
			string sSQL = "" ;
			
			try
			{
				sCurrentUser = m_sUserName.Replace( "'" , "''" );
				sCurrentDTTM = System.DateTime.Now.ToString("yyyyMMddHHmmss");			

				objConn = DbFactory.GetDbConnection( m_sConnectionString );			
				objConn.Open();

				// Put in this account id just in case it isn't in there already
				try
				{
					// akaushik5 Added for MITS 35846 Starts
                    bool isAccountExist = false;
                    sSQL = "SELECT ACCOUNT_ID FROM ACCT_LOCK WHERE ACCOUNT_ID = " + p_iAccountId;
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                    {
                        isAccountExist = !object.ReferenceEquals(objReader, null) && objReader.Read();
                    }
                    if (!isAccountExist)
                    {
                        // akaushik5 Added for MITS 35846 Ends
                        sSQL = " INSERT INTO ACCT_LOCK(ACCOUNT_ID) VALUES (" + p_iAccountId + ")";
                        objConn.ExecuteNonQuery(sSQL);
                    }
				}
				catch{}
				
				// Insure that no other thread is reading/updating the Status.
				lock(this)
				{
					sSQL = "SELECT * FROM ACCT_LOCK WHERE ACCOUNT_ID = " + p_iAccountId  ;
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader.Read())
					{
						p_sUser = objReader.GetString( "USER_CHECKOUT" );
						p_sDTTM = objReader.GetString( "DTTM_CHECKOUT" );
					}
					objReader.Close();
					
					// If User Information is not present, get the lock. 
					if( p_sUser.Trim() == "" && p_sDTTM.Trim() == "" )
					{
						sSQL = " UPDATE ACCT_LOCK SET USER_CHECKOUT = '" + sCurrentUser 
							+	"',DTTM_CHECKOUT = '" + sCurrentDTTM + "'" 
							+	" WHERE ACCOUNT_ID = " + p_iAccountId ;

						objConn.ExecuteNonQuery( sSQL );
						m_bHasLock = true ;
						return true ;
					}
					else
					{
						// If Lock is too old, override it.
						if( Conversion.ConvertStrToLong( sCurrentDTTM) - Conversion.ConvertStrToLong( p_sDTTM  ) > 1000 )
						{
							sSQL = " UPDATE ACCT_LOCK SET USER_CHECKOUT = '" + sCurrentUser 
								+	"',DTTM_CHECKOUT = '" + sCurrentDTTM + "'" 
								+	" WHERE ACCOUNT_ID = " + p_iAccountId ;

							objConn.ExecuteNonQuery( sSQL );
							m_bHasLock = true ;
							return true ;
						}
						// Other person has the lock.
						return false ;
					}
				}				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("AccountLock.AcquireLock.Error",m_iClientId) , p_objEx );				
			}	
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
				}
				if( objConn != null )
					objConn.Close();

			}			
		}

		/// Name		: ReleaseLock
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Release Lock
		/// </summary>
		internal void ReleaseLock( int p_iAccountId )
		{
			string sSQL = string.Empty ;
			DbConnection objConn = null ;

			try
			{
				objConn = DbFactory.GetDbConnection( m_sConnectionString );			
				objConn.Open();
				
				sSQL = " UPDATE ACCT_LOCK SET USER_CHECKOUT = '',DTTM_CHECKOUT = '' WHERE ACCOUNT_ID = " + p_iAccountId ;
				objConn.ExecuteNonQuery( sSQL );
							
				objConn.Close(); 
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("AccountLock.ReleaseLock.Error",m_iClientId) , p_objEx );				
			}			
		}

		/// Name		: Dispose
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Dispose.
		/// </summary>
		internal void Dispose()
		{
			
		}
	}
}
