/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-438         | ajohari2   | Underwriters - EFT Payments
 **********************************************************************************************/
using System;
using System.Xml ;
using System.IO ;
using System.Data ;
using System.Text ;
using System.Collections ;
using System.Collections.Generic;
using System.Linq;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.Settings ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Application.PrintEOB ;
using Riskmaster.Security;

namespace Riskmaster.Application.PrintChecks
{
	/**************************************************************
	 * $File		: CheckRegister.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 01/05/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		: Class exposes methods for Pre Register and Post Register Checks. 
	 * $Source		:  	
	**************************************************************/
	public class CheckRegister:IDisposable
	{	
		#region Constants
		/// <summary>
		/// Constant for order by amount
		/// </summary>
		private const string ORDER_BY_AMOUNT = "FUNDS.AMOUNT" ;
		/// <summary>
		/// Constant for order by claim
		/// </summary>
		private const string ORDER_BY_CLAIM = "FUNDS.CLAIM_NUMBER" ;
		/// <summary>
		/// Constant for order by control
		/// </summary>
		private const string ORDER_BY_CONTROL = "FUNDS.CTL_NUMBER" ;
		/// <summary>
		/// Constant for order by name
		/// </summary>
		private const string ORDER_BY_NAME = "FUNDS.LAST_NAME,FUNDS.FIRST_NAME" ;
		/// <summary>
		/// Constant for order by trans date
		/// </summary>
		private const string ORDER_BY_TRANS_DATE = "FUNDS.TRANS_DATE" ;
		/// <summary>
		/// Constant for order by adjuster id
		/// </summary>
		private const string ORDER_BY_ADJUSTER_EID = "ADJUSTER_EID" ;
		/// <summary>
		/// Constant for order by bank account
		/// </summary>
		private const string ORDER_BY_BANK_ACCOUNT_SUB = "BANK_ACC_SUB" ;
        /// <summary>
		/// Constant for order by Org. Level
		/// </summary>
        /// Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date : 03/17/2010
        private const string ORDER_BY_ORG_LEVEL = "ORG_LEVEL";
		#endregion 

		#region Variable Declaration 
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store UserId
		/// </summary>
		private int m_iUserId = 0 ;
		/// <summary>
		/// Private variable to store User GroupId
		/// </summary>
		private int m_iUserGroupId = 0 ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable to store the instance of XmlDocument object
		/// </summary>
		private XmlDocument m_objDocument = null ;
		/// <summary>
		/// Private variable for Print Wrapper object.
		/// </summary>
		private PrintWrapper m_objPrintWrapper = null ;
		/// <summary>
		/// Private variable for PDF Folder Path.
		/// </summary>
		private string m_sPdfSavePath = "" ;

        private int m_iClientId = 0;
        
        Dictionary<int, double> dicMaxAmounts = null;

		SysSettings m_objSysSettings = null ;
		ColLobSettings m_objColLobSettings = null ;
		LobSettings m_objLobSettings = null ;
		LocalCache m_objLocalCache = null ;
		CCacheFunctions m_objCCacheFunctions = null ;
        private static Hashtable m_objConfigSettings;
        //ps206 --conn string 
        //private string m_strCheckRegisterFont = RMConfigurationManager.GetDictionarySectionSettings("PrintChecks")["CheckRegister_Font"].ToString();
        private string m_strCheckRegisterFont = "";        

		#endregion 

		#region Constructors & destructors 
		/// Name		: CheckRegister
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sDsnName">DsnName</param>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_iUserId">User Id</param>
		/// <param name="p_iUserGroupId">User Group Id</param>
		public CheckRegister( string p_sDsnName , string p_sUserName , string p_sPassword , int p_iUserId , int p_iUserGroupId, int p_iClientId )
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
			m_iUserId = p_iUserId ;
			m_iUserGroupId = p_iUserGroupId ;
            m_iClientId = p_iClientId; //Ash - cloud
			m_objDataModelFactory = new DataModel.DataModelFactory( m_sDsnName , m_sUserName , m_sPassword, m_iClientId );	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
			Functions.m_sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString() ;			
            m_objConfigSettings = Functions.LoadOptions(m_sConnectionString, m_iClientId);//Ash -cloud

            m_sPdfSavePath = Functions.GetOptionValue(CheckConstants.PDF_SAVE_PATH, m_objConfigSettings, m_iClientId);								
			if( m_sPdfSavePath.Substring( m_sPdfSavePath.Length -1 ) != @"\" )
				m_sPdfSavePath += @"\" ;

			if( ! Directory.Exists( m_sPdfSavePath ) )
				Directory.CreateDirectory( m_sPdfSavePath );

			m_objSysSettings = new SysSettings( m_sConnectionString, m_iClientId ); //Ash - cloud
            m_objColLobSettings = new ColLobSettings(m_sConnectionString, m_iClientId);
			m_objLobSettings = new LobSettings();
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
            UserLogin objUserLogin = new UserLogin(m_iUserId,m_iClientId);
            m_objLocalCache.LanguageCode = Convert.ToString(objUserLogin.objUser.NlsCode);
            if (objUserLogin != null)
            {
                objUserLogin = null;
            }
			m_objCCacheFunctions = new CCacheFunctions( m_sConnectionString, m_iClientId);

            m_strCheckRegisterFont = RMConfigurationManager.GetDictionarySectionSettings("PrintChecks",m_sConnectionString,m_iClientId)["CheckRegister_Font"].ToString();//ps206

		}
        ~CheckRegister()
        {
            Dispose();
        }        

		#endregion 

		#region Print PreCheck Register
		/*
		 * SAMPLE XML
		 * 
		 * <PreCheckDetail>
				<AccountId>1</AccountId> 
				<OrderBy>BANK_ACC_SUB</OrderBy> 
				<IncludeAutoPayment>1</IncludeAutoPayment> 
				<UsingSelection>1</UsingSelection> 
				<AttachedClaimOnly>0</AttachedClaimOnly> 
				<FromDateFlag>0</FromDateFlag> 
				<FromDate /> 
				<ToDateFlag>1</ToDateFlag> 
				<ToDate>20041231</ToDate> 
				<SelectedChecksIds>123,124</SelectedChecksIds>
				<SelectedAutoChecksIds>234,235</SelectedAutoChecksIds>				
			</PreCheckDetail>
			*/
		/// Name		: PreCheckDetail
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Pre Checks Detail.
		/// </summary>
		/// <param name="p_objInputXml">Input XML Document</param>
		/// <param name="p_bIsInsfficientFund">Insufficient Fund Check flag.</param>
		/// <param name="p_sPdfDocPath">Set the Path of the PDF generated.</param>
		/// <returns>Success/Failure</returns>
        public bool PreCheckDetail(XmlDocument p_objInputXml, ref string p_sPdfDocPath)
        {
            Account objAccount = null;
            DataSet objDSPrintPre = null;
            DataTable objDTPrintPre = null;
            DataSet objDSPrintPreAuto = null;
            DataTable objDTPrintPreAuto = null;
            DbConnection objConn = null;
            ArrayList arrlstSelectedAutoCheck = null;
            ArrayList arrlstSelectedCheck = null;

            //Start Debabrata Biswas Commented lines below and added new arrays Batch Printing filter MITS 19715/20050 Date : 03/17/2010
            //string[,] arrPreCheckRegCol = new string[8,2] ;
            //string[] arrPreCheckRegColAlign = new string[8] ;
            //double[] arrPreCheckRegColWidth = new double[8] ;

            string[,] arrPreCheckRegCol = new string[9, 2];
            string[] arrPreCheckRegColAlign = new string[9];
            double[] arrPreCheckRegColWidth = new double[9];
            //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010

            double dblCharWidth = 0.0;
            double dblLineHeight = 0.0;
            double dblLeftMargin = 0.0;
            double dblRightMargin = 0.0;
            double dblTopMargin = 0.0;
            double dblPageWidth = 0.0;
            double dblPageHeight = 0.0;
            double dblBottomHeader = 0.0;
            double dblReportTotal = 0.0;
            double dblCurrentY = 0.0;
            double dblPageTotal = 0;

            //Geeta 07/28/08 : Mits 12548
            int iFixedLinePerPage = 18;
            int iRealRecordCount = 0;
            int iTransId = 0;
            //int iCheckNum = 0 ;
            long lCheckNum = 0;  //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
            int iDataLineCount = 0;
            int iPageLineCapacity = 0;
            int iRemainder = 0;
            int iIndex = 0;
            int iNumPages = 0;
            int iCumulativeTotal = 0;
            int iPageTotal = 0;
            int iLine = 0;
            int iRowIndexPre = 0;
            int iRowIndexPreAuto = 0;
            int iBatchNum = 0;
            int iOldAdjuster = 0;
            int iClaimId = 0;
            int iSkipPageCount = 0;
            int iOldSubAccount = 0;
            int iAccountId = 0;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            int iOrgHierarchyLevel = 0;
            int iNumAutoChecksToPrint = 0;
            string sFooter = "";
            string sSQL = "";
            string sDate = "";
            string sFromDate = "";
            string sToDate = "";
            string sOrderByField = "";
            string sFromDateValue = "";
            string sToDateValue = "";
            string sErrorText = "";
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string sOrgHierarchy = "";
            bool bReturnValue = false;
            bool bIncludeAutoPays = false;
            bool bWithAttachedClaimsOnly = false;
            bool bUseFromDate = false;
            bool bUseToDate = false;
            bool bUsingSelections = false;
            bool bAccountFound = false;
            bool bSkipRegChecks = false;
            bool bMergeAutoChecks = false;
            bool bAutoTrans = false;
            //skhare7 R8 enhancement
            bool bIncludeCombinedPays = false;
            //end 
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //bool bPrintEFTPayment = false;//JIRA:438 START: ajohari2
            int iDistributionType = 0;
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //Start -  Added by Nikhil.
            Funds objFunds = null;
            //end- added by nikhil
            int iLOB = 0;           //Declare by Sumit Agarwal for NI to get the LOB: 11/07/2014

            try
            {
                bUseFromDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "FromDateFlag", m_iClientId));
                bUseToDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "ToDateFlag", m_iClientId));
                sFromDateValue = Functions.GetValue(p_objInputXml, "FromDate", m_iClientId);
                sToDateValue = Functions.GetValue(p_objInputXml, "ToDate", m_iClientId);
                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                sOrgHierarchy = Functions.GetValue(p_objInputXml, "OrghierarchyPre", m_iClientId);
                iOrgHierarchyLevel = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputXml, "OrghierarchyLevelPre", m_iClientId));
                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                bIncludeAutoPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "IncludeAutoPayment", m_iClientId));
                //skhare7 R8 enhancement
                bIncludeCombinedPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "IncludeCombinedPayment", m_iClientId));
                //skhare7 R8
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //bPrintEFTPayment = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "PrintEFTPayment", m_iClientId));
                //JIRA:438 End: 

                iDistributionType = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputXml, "DistributionType", m_iClientId));
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                iAccountId = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputXml, "AccountId", m_iClientId));
                bWithAttachedClaimsOnly = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "AttachedClaimOnly", m_iClientId));
                bUsingSelections = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "UsingSelection", m_iClientId));
                sOrderByField = Functions.GetValue(p_objInputXml, "OrderBy", m_iClientId);

                sOrderByField = Functions.GetSortOrder(sOrderByField, m_iClientId);

                dicMaxAmounts = null;
                dicMaxAmounts = CheckSetup.GetMaxPrintAmounts(m_iUserId, m_iUserGroupId, m_sConnectionString, m_iClientId);//skhare7 MITS 28010 common fn was used 

                if (bUsingSelections)
                {
                    arrlstSelectedAutoCheck = Functions.GetList(p_objInputXml, "AutoTransIds", ",", m_iClientId);
                    arrlstSelectedCheck = Functions.GetList(p_objInputXml, "TransIds", ",",m_iClientId);
                    iNumAutoChecksToPrint = arrlstSelectedAutoCheck.Count;
                }

                sDate = System.DateTime.Now.ToString("d");
                if (Utilities.IsDate(sFromDateValue) && bUseFromDate)
                    sFromDate = Conversion.GetDate(sFromDateValue);
                else
                    sFromDate = "";

                if (Utilities.IsDate(sToDateValue) && bUseToDate)
                    sToDate = Conversion.GetDate(sToDateValue);
                else
                    sToDate = "";

                #region Insure that atleast one check is there to print ELSE exit.
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                //sSQL = this.CheckPrintSQLPreCheck(sFromDate, sToDate, bIncludeAutoPays, iAccountId, sOrderByField, false, bWithAttachedClaimsOnly);
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                sSQL = this.CheckPrintSQLPreCheck(sFromDate, sToDate, bIncludeAutoPays, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, sOrderByField, false, bWithAttachedClaimsOnly, bIncludeCombinedPays, iDistributionType); //JIRA:438 : ajohari2
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                objDSPrintPre = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                //tanwar2 - mits 30910 - start
				//objDTPrintPre = objDSPrintPre.Tables[0] ;
                //start - Added by Nikhil.
                objFunds = m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                //Start: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014
                //if (objFunds.IsDeductibleApplicable(false))
                if (objDSPrintPre != null)
                {
                    if (objDSPrintPre.Tables[0].Rows.Count > 0)
                    {
                        if (objDSPrintPre.Tables[0].Rows[0]["LINE_OF_BUS_CODE"] != null)
                        {
                            iLOB = Common.Conversion.ConvertStrToInteger(objDSPrintPre.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString());
                        }
                    }
                }
                if (objFunds.Context.InternalSettings.ColLobSettings[iLOB] != null && iLOB > 0)
                {
                    if (objFunds.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1)
                    //End: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014
                    {
                        objDTPrintPre = this.AddDeductibleSplits(objDSPrintPre.Tables[0], false, arrlstSelectedAutoCheck);
                    }
                    else
                    {
                        objDTPrintPre = objDSPrintPre.Tables[0];
                    }
                }
                else
                {
                    objDTPrintPre = objDSPrintPre.Tables[0];
                }
                //end - added by Nikhil
                //tanwar2 - mits 30910 - end
                iRowIndexPre = 0;
                if (bIncludeAutoPays)
                {
                    // If Selection List is not in used
                    // OR 
                    // If Selection list is in used then include at least one Auto Payment.
                    if (!bUsingSelections || (bUsingSelections && iNumAutoChecksToPrint > 0))
                    {
                        //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                        //sSQL = this.CheckPrintSQLPreCheckAuto(sFromDate, sToDate, iAccountId, sOrderByField, false, true);
                        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                        sSQL = this.CheckPrintSQLPreCheckAuto(sFromDate, sToDate, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, sOrderByField, false, true, bIncludeCombinedPays, iDistributionType); //JIRA:438 : ajohari2
                        // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                        objDSPrintPreAuto = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
						//tanwar2 - mits 30910 - start
                        //objDTPrintPreAuto = objDSPrintPreAuto.Tables[0] ;
                        //start - ADDED by Nikhil.
                        //Start: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014
                        //if (objFunds.IsDeductibleApplicable(false))
                        if (objDSPrintPreAuto.Tables[0].Rows.Count > 0)
                        {
                            if (objDSPrintPreAuto.Tables[0].Rows[0]["LINE_OF_BUS_CODE"] != null)
                            {
                                iLOB = Common.Conversion.ConvertStrToInteger(objDSPrintPreAuto.Tables[0].Rows[0]["LINE_OF_BUS_CODE"].ToString());
                            }
                        }
                        if (objFunds.Context.InternalSettings.ColLobSettings[iLOB] != null && iLOB > 0)
                        {
                            if (objFunds.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1)
                            //End: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014
                            {
                                objDTPrintPreAuto = this.AddDeductibleSplits(objDSPrintPreAuto.Tables[0], true, arrlstSelectedAutoCheck);
                            }
                            else
                            {
                                objDTPrintPreAuto = objDSPrintPreAuto.Tables[0];
                            }

                        }
                        else
                        {
                            objDTPrintPreAuto = objDSPrintPreAuto.Tables[0];
                        }
                        //end - ADDED by Nikhil.
                        //tanwar2 - mits 30910 - end
                        iRowIndexPreAuto = 0;
                        if (objDTPrintPreAuto.Rows.Count > 0)
                            bMergeAutoChecks = true;
                    }
                }
                if (objDTPrintPre.Rows.Count <= 0)
                {
                    if (bMergeAutoChecks)
                        bSkipRegChecks = true;
                    else
                    {
                        sErrorText = Globalization.GetString("CheckRegister.PreCheckDetail.NoCheckFound", m_iClientId);
                        throw new RMAppException(sErrorText);
                        //Functions.CreateAndSetElement( objRootNode , "Message" , sErrorText );
                        //return( false );
                    }
                }
                else
                {
                    iClaimId = Common.Conversion.ConvertStrToInteger(objDTPrintPre.Rows[iRowIndexPre]["CLAIM_ID"].ToString());
                }
                #endregion

                #region Count Real Records
                if (!bSkipRegChecks)
                {
                    while (iRowIndexPre < objDTPrintPre.Rows.Count)
                    {
                        while (this.SkipPayment(objDTPrintPre, iRowIndexPre, false, "FUNDS_AMOUNT", bUsingSelections, arrlstSelectedCheck, arrlstSelectedAutoCheck))
                        {
                            iRowIndexPre++;
                            if (iRowIndexPre >= objDTPrintPre.Rows.Count)
                                break;
                        }
                        if (iRowIndexPre < objDTPrintPre.Rows.Count)
                        {
                            iRealRecordCount++;
                            if (iTransId != Common.Conversion.ConvertStrToInteger(objDTPrintPre.Rows[iRowIndexPre]["FUNDS_TRANS_ID"].ToString()))
                                lCheckNum++;

                            iTransId = Common.Conversion.ConvertStrToInteger(objDTPrintPre.Rows[iRowIndexPre]["FUNDS_TRANS_ID"].ToString());

                            iRowIndexPre++;
                        }
                        else
                            break;
                    }
                }

                if (bMergeAutoChecks)
                {
                    while (iRowIndexPreAuto < objDTPrintPreAuto.Rows.Count)
                    {
                        while (this.SkipPayment(objDTPrintPreAuto, iRowIndexPreAuto, true, "FUNDS_AMOUNT", bUsingSelections, arrlstSelectedCheck, arrlstSelectedAutoCheck))
                        {
                            iRowIndexPreAuto++;
                            if (iRowIndexPreAuto >= objDTPrintPreAuto.Rows.Count)
                                break;
                        }
                        if (iRowIndexPreAuto < objDTPrintPreAuto.Rows.Count)
                        {
                            iRealRecordCount++;
                            if (iTransId != Common.Conversion.ConvertStrToInteger(objDTPrintPreAuto.Rows[iRowIndexPreAuto]["FUNDS_TRANS_ID"].ToString()))
                                lCheckNum++;

                            iTransId = Common.Conversion.ConvertStrToInteger(objDTPrintPreAuto.Rows[iRowIndexPreAuto]["FUNDS_TRANS_ID"].ToString());

                            iRowIndexPreAuto++;
                        }
                        else
                            break;
                    }
                }
                #endregion

                #region Column Text Init
                arrPreCheckRegCol[0, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Control", m_iClientId));
                arrPreCheckRegCol[0, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Number", m_iClientId));
                arrPreCheckRegColAlign[0] = CheckConstants.RIGHT_ALIGN;

                arrPreCheckRegCol[1, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Transaction", m_iClientId));
                arrPreCheckRegCol[1, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Date", m_iClientId));
                arrPreCheckRegColAlign[1] = CheckConstants.RIGHT_ALIGN;

                arrPreCheckRegCol[2, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Claim", m_iClientId));
                arrPreCheckRegCol[2, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Number", m_iClientId));
                arrPreCheckRegColAlign[2] = CheckConstants.RIGHT_ALIGN;

                arrPreCheckRegCol[3, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Department", m_iClientId));
                arrPreCheckRegCol[3, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Code", m_iClientId));
                arrPreCheckRegColAlign[3] = CheckConstants.LEFT_ALIGN;

                arrPreCheckRegCol[4, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Payee", m_iClientId));
                arrPreCheckRegCol[4, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Name", m_iClientId));
                arrPreCheckRegColAlign[4] = CheckConstants.LEFT_ALIGN;

                arrPreCheckRegCol[5, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Payment", m_iClientId));
                arrPreCheckRegCol[5, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Type", m_iClientId));
                arrPreCheckRegColAlign[5] = CheckConstants.LEFT_ALIGN;

                arrPreCheckRegCol[6, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Payment", m_iClientId));
                arrPreCheckRegCol[6, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Amount", m_iClientId));
                arrPreCheckRegColAlign[6] = CheckConstants.RIGHT_ALIGN;

                arrPreCheckRegCol[7, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Check", m_iClientId));
                arrPreCheckRegCol[7, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.Total", m_iClientId));
                arrPreCheckRegColAlign[7] = CheckConstants.RIGHT_ALIGN;

                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010

                switch (iOrgHierarchyLevel)
                {
                    case 1005: arrPreCheckRegCol[8, 0] = "Client"; break;
                    case 1006: arrPreCheckRegCol[8, 0] = "Company"; break;
                    case 1007: arrPreCheckRegCol[8, 0] = "Operation"; break;
                    case 1008: arrPreCheckRegCol[8, 0] = "Region"; break;
                    case 1009: arrPreCheckRegCol[8, 0] = "Division"; break;
                    case 1010: arrPreCheckRegCol[8, 0] = "Location"; break;
                    case 1011: arrPreCheckRegCol[8, 0] = "Facility"; break;
                    //case 1012: arrPreCheckRegCol[8, 0] = "Department";  break;//Department code need not to come twice
                }

                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                if (iOrgHierarchyLevel != 1012)
                {
                    arrPreCheckRegCol[8, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.HierarchyCode", m_iClientId));
                    arrPreCheckRegColAlign[8] = CheckConstants.LEFT_ALIGN;
                }
                m_objPrintWrapper = new PrintWrapper(m_iClientId);
                m_objPrintWrapper.StartDoc(true,m_iClientId);

                string sFont = m_strCheckRegisterFont; // Look for optional alternative font for check register. This is not in RISKMASTER.CONFIG by default.
                if (sFont == null)
                    m_objPrintWrapper.SetFont("Arial", 8);
                else
                    m_objPrintWrapper.SetFont(sFont, 8);

                // m_objPrintWrapper.SetFont( "Arial" , 8 );

                dblCharWidth = m_objPrintWrapper.GetTextWidth("0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ") / 40;
                dblLineHeight = m_objPrintWrapper.GetTextHeight("W");
                dblLeftMargin = dblCharWidth * 3;
                dblRightMargin = dblCharWidth * 3;

                dblTopMargin = dblLineHeight * 5;
                dblPageWidth = m_objPrintWrapper.PageWidth - dblLeftMargin - dblRightMargin;
                dblPageHeight = m_objPrintWrapper.PageHeight;
                dblBottomHeader = dblPageHeight - (5 * dblLineHeight);

                // Set up column widths, based on (approximate) character widths
                //arrPreCheckRegColWidth[0] = dblCharWidth * 14  ;// ctl num //zalam 04/10/2008 Mits:-7954
                arrPreCheckRegColWidth[0] = dblCharWidth * 15;// ctl num
                arrPreCheckRegColWidth[1] = dblCharWidth * 10;// trans date
                arrPreCheckRegColWidth[2] = dblCharWidth * 18;// claim num
                arrPreCheckRegColWidth[3] = dblCharWidth * 14;// department code
                arrPreCheckRegColWidth[4] = dblCharWidth * 40;// payee name
                arrPreCheckRegColWidth[5] = dblCharWidth * 17;// payment type
                //Debabrata Biswas Batch Printing Retrofit MITS 19715/20050 Date : 03/17/2010. Changed to 10 from 15 to accomodate org. hierarchy field
                arrPreCheckRegColWidth[6] = dblCharWidth * 10;// payment amt 
                arrPreCheckRegColWidth[7] = dblCharWidth * 15;// check total	
                //Debabrata Biswas Batch Printing Retrofit MITS # 19715/20050 Date : 03/17/2010 Orghierarchy
                arrPreCheckRegColWidth[8] = dblCharWidth * 15;
                #endregion

                #region Count Pages
                //Geeta 07/28/08 : Mits 12548
                //iDataLineCount = iRealRecordCount + 2 ;  // 2 added for "Total for this report : " with blank line
                iDataLineCount = iRealRecordCount;
                iPageLineCapacity = System.Convert.ToInt32(dblPageHeight / dblLineHeight);

                iRemainder = iDataLineCount % (iPageLineCapacity - iFixedLinePerPage) == 0 ? 0 : 1;
                iNumPages = ((iDataLineCount) / (iPageLineCapacity - iFixedLinePerPage)) + iRemainder;
                #endregion

                objAccount = (Account)m_objDataModelFactory.GetDataModelObject("Account", false);
                try
                {
                    objAccount.MoveTo(iAccountId);
                    bAccountFound = true;
                }
                catch
                {
                    bAccountFound = false;
                }
                if (bAccountFound)
                    iBatchNum = objAccount.NextBatchNumber;

                if (iBatchNum == 0)
                    iBatchNum = 1;

                iRowIndexPre = 0;
                iRowIndexPreAuto = 0;
                iTransId = 0;
                for (iIndex = 1; iIndex <= iNumPages; iIndex++)
                {
                    iPageTotal = 0;

                    dblCurrentY = dblTopMargin;

                    // Print the top of the page (header area)
                    ////Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Updated the no. of columns to 9
                    //this.CheckRegPrintHeader(iBatchNum, ref dblCurrentY, sDate, iAccountId, 8, iClaimId,dblPageWidth, dblLeftMargin, dblLineHeight, arrPreCheckRegCol, arrPreCheckRegColWidth,arrPreCheckRegColAlign);

                    this.CheckRegPrintHeader(iBatchNum, ref dblCurrentY, sDate, iAccountId, 9, iClaimId,
                        dblPageWidth, dblLeftMargin, dblLineHeight, arrPreCheckRegCol, arrPreCheckRegColWidth,
                        arrPreCheckRegColAlign);

                    dblPageTotal = 0;
                    for (iLine = 1; iLine <= iPageLineCapacity - iFixedLinePerPage; iLine++)
                    {
                        #region Check for Skip
                        if (!bSkipRegChecks)
                        {
                            if (iRowIndexPre < objDTPrintPre.Rows.Count)
                            {
                                while (this.SkipPayment(objDTPrintPre, iRowIndexPre, false, "FUNDS_AMOUNT",
                                    bUsingSelections, arrlstSelectedCheck, arrlstSelectedAutoCheck))
                                {
                                    iRowIndexPre++;
                                    if (iRowIndexPre >= objDTPrintPre.Rows.Count)
                                        break;
                                }
                            }
                        }

                        if (bMergeAutoChecks)
                        {
                            if (iRowIndexPreAuto < objDTPrintPreAuto.Rows.Count)
                            {
                                while (this.SkipPayment(objDTPrintPreAuto, iRowIndexPreAuto, true,
                                    "FUNDS_AMOUNT", bUsingSelections, arrlstSelectedCheck, arrlstSelectedAutoCheck))
                                {
                                    iRowIndexPreAuto++;
                                    if (iRowIndexPreAuto >= objDTPrintPreAuto.Rows.Count)
                                        break;
                                }
                            }
                        }
                        #endregion

                        if (!PreCheckDetailRegPrtData(objDTPrintPre, ref iRowIndexPre, objDTPrintPreAuto,
                            ref iRowIndexPreAuto, iLine, ref dblCurrentY, ref dblPageTotal, ref dblReportTotal
                            , dblBottomHeader, ref iTransId, ref iPageTotal, sOrderByField, ref iOldSubAccount
                            , ref iOldAdjuster, bSkipRegChecks, bMergeAutoChecks, ref bAutoTrans, dblLeftMargin
                            , dblLineHeight, dblCharWidth, dblPageWidth, arrPreCheckRegColWidth,
                            arrPreCheckRegColAlign))
                            break;
                    }

                    if (iOldAdjuster == -10)
                    {
                        iIndex--;
                        iSkipPageCount++;
                    }
                    if (iOldSubAccount == -10)
                    {
                        iIndex--;
                        iSkipPageCount++;
                    }
                    iCumulativeTotal += iPageTotal;

                    dblCurrentY = dblBottomHeader;
                    sFooter = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetail.PrecheckRegister", m_iClientId));
                    // Print Footer.
                    CheckRegPrintFooter(iIndex, iNumPages, dblPageTotal, dblReportTotal, dblCurrentY,
                        sFooter, iPageTotal, iCumulativeTotal, "", iSkipPageCount, dblCharWidth,
                        dblLineHeight, dblLeftMargin, dblRightMargin, dblPageWidth);

                    if (iIndex != iNumPages)
                    {
                        if (iOldAdjuster == -50)
                        {
                            m_objPrintWrapper.EndDoc();
                            p_sPdfDocPath = m_sPdfSavePath + Functions.GetUniqueFileName(m_iClientId);
                            m_objPrintWrapper.Save(p_sPdfDocPath);
                            break;
                        }
                        else
                        {
                            if (iOldSubAccount == -50)
                            {
                                m_objPrintWrapper.EndDoc();
                                p_sPdfDocPath = m_sPdfSavePath + Functions.GetUniqueFileName(m_iClientId);
                                m_objPrintWrapper.Save(p_sPdfDocPath);
                                break;
                            }
                            else
                                m_objPrintWrapper.NewPage();
                        }
                    }
                    else
                    {
                        m_objPrintWrapper.EndDoc();
                        p_sPdfDocPath = m_sPdfSavePath + Functions.GetUniqueFileName(m_iClientId);
                        m_objPrintWrapper.Save(p_sPdfDocPath);
                    }
                }

                bReturnValue = true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckRegister.PreCheckDetail.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objAccount != null)
                {
                    objAccount.Dispose();
                    objAccount = null;
                }
                if (objDSPrintPre != null)
                {
                    objDSPrintPre.Dispose();
                    objDSPrintPre = null;
                }
                if (objDTPrintPre != null)
                {
                    objDTPrintPre.Dispose();
                    objDTPrintPre = null;
                }
                if (objDSPrintPreAuto != null)
                {
                    objDSPrintPreAuto.Dispose();
                    objDSPrintPreAuto = null;
                }
                if (objDTPrintPreAuto != null)
                {
                    objDTPrintPreAuto.Dispose();
                    objDTPrintPreAuto = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                //Start - added by nIkhil.
                if (objFunds != null)
                {
				    objFunds.Dispose();
                    objFunds = null;
                }
                //end- added by Nikhil

                arrlstSelectedAutoCheck = null;
                arrlstSelectedCheck = null;
            }
            return (bReturnValue);
        }
       
        /// <summary>
        /// tanwar2 - mits 30910 - adds deductible splits in memory (without writing into database)
        /// </summary>
        /// <param name="p_oFundsTable"></param>
        /// <param name="bAuto"></param>
        /// <returns></returns>
        private DataTable AddDeductibleSplits(DataTable p_oFundsTable, bool bAuto, ArrayList p_arrlstSelectedAutoCheck)
        {
            Funds oFunds = null;
            FundsAuto oFundsAuto = null;
            Dictionary<int, List<FundsTransSplit>> dNewSplits = null;
            List<FundsTransSplit> lSplits = null;
            HashSet<string> hSetPolicyUnit = null;

            //tanwar2 - JIRA 6260 - start
            Dictionary<int, double> dicBackup = null;
            //tanwar2 - JIRA 6260 - end

            int iTransId = 0;
            double dAmt = 0d;
            bool bSuccess = false;
            int iSplitRowId = 0;
            double dSplitAmt = 0d;
            int iTransTypeCode = 0;
            int iFPDedTypeCode = 0;
            int iBasicDimTypeCode = 0;
            int iDedTypeCode = 0;
            int iDimTypeCode = 0;
            int iClaimId = 0;
            int iPolicyId = 0;
            int iUnitRowId = 0;
            bool bUseOriginalDeductibleAmt = false;
            int iCount = 0;
            bool bNew = false;
            string sCtlNumber = string.Empty;
            string sSplitRowIdName = string.Empty;
            string sDedRecoveryIdentifierChar = string.Empty;       //Declared by Sumit Agarwal on 10/13/2014 to store the Deductible Recovery Identifier Char

            iFPDedTypeCode = m_objLocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");
            iBasicDimTypeCode = m_objLocalCache.GetCodeId("B", "DIMINISHING_TYPE");
            oFunds = m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
            oFundsAuto = m_objDataModelFactory.GetDataModelObject("FundsAuto", false) as FundsAuto;
            dNewSplits = new Dictionary<int, List<FundsTransSplit>>();
            hSetPolicyUnit = new HashSet<string>();
            for (int i = 0; i < p_oFundsTable.Rows.Count; i++)
            {
                iTransId = 0;
                iDedTypeCode = 0;
                iDimTypeCode = 0;
                iClaimId = 0;
                iPolicyId = 0;
                iUnitRowId = 0;
                bUseOriginalDeductibleAmt = false;
                bNew = false;

                //Check Ded_type_code. If not first party then no negative split will get created.
                if (p_oFundsTable.Columns.Contains("DED_TYPE_CODE"))
                {
                    if (p_oFundsTable.Rows[i]["DED_TYPE_CODE"] != null && p_oFundsTable.Rows[i]["DED_TYPE_CODE"]!=DBNull.Value)
                    {
                        iDedTypeCode = Convert.ToInt32(p_oFundsTable.Rows[i]["DED_TYPE_CODE"]);
                    }
                    if (iDedTypeCode != iFPDedTypeCode)
                    {
                        continue;
                    }
                }
                if (p_oFundsTable.Columns.Contains("DIMNSHNG_TYPE_CODE"))
                {
                    if (p_oFundsTable.Rows[i]["DIMNSHNG_TYPE_CODE"]!=null && p_oFundsTable.Rows[i]["DIMNSHNG_TYPE_CODE"]!=DBNull.Value)
                    {
                        iDimTypeCode = Convert.ToInt32(p_oFundsTable.Rows[i]["DIMNSHNG_TYPE_CODE"]);
                    }
                    if (iDimTypeCode == iBasicDimTypeCode)
                    {
                        iClaimId = Convert.ToInt32(p_oFundsTable.Rows[i]["CLAIM_ID"]);
                        iPolicyId = Convert.ToInt32(p_oFundsTable.Rows[i]["POLICY_ID"]);
                        iUnitRowId = Convert.ToInt32(p_oFundsTable.Rows[i]["POLICY_UNIT_ROW_ID"]);
                        //Changed by Nikhil.Code review changes
                        //if (hSetPolicyUnit.Contains(iClaimId + "|" + iPolicyId + "|" + iUnitRowId))
                        if (hSetPolicyUnit.Contains(String.Concat(iClaimId.ToString() , "|" , iPolicyId.ToString() , "|" ,iUnitRowId.ToString())))
                        {
                            bUseOriginalDeductibleAmt = true;
                        }
                        else
                        {
                            //Changed by Nikhil.Code review changes
                          //  hSetPolicyUnit.Add(iClaimId + "|" + iPolicyId + "|" + iUnitRowId);
                            hSetPolicyUnit.Add(String.Concat(iClaimId.ToString(), "|", iPolicyId.ToString(), "|", iUnitRowId.ToString()));
                            bNew = true;
                        }
                    }
                }
                iTransId = Convert.ToInt32(p_oFundsTable.Rows[i]["FUNDS_TRANS_ID"]);

                if (iTransId > 0 && !dNewSplits.ContainsKey(iTransId))
                {
                    if (bAuto)
                    {
                        if (p_arrlstSelectedAutoCheck != null)
                        {
                            if (!p_arrlstSelectedAutoCheck.Contains(iTransId.ToString()))
                            {
                                continue;
                            }
                        }
                        
                        oFundsAuto.MoveTo(iTransId);
                        //tanwar2 - JIRA 6260 - start
                        //CreateFundsClone will reset the the funds class.
                        //so take backup of remaining amount.
                        //This is done as printing is only virtual here - 
                        //- same coverage which is already printed may get printed again
                        //NOTE: moveto does not re-instantiate the class - so we need not worry about it
                        if (oFunds.dicRemainingAmt!=null)
                        {
                            dicBackup = oFunds.dicRemainingAmt;
                        }
                        //tanwar2 - JIRA 6260 - end
                        oFunds = oFundsAuto.CreateFundsClone();
                        if (dicBackup!=null)
                        {
                            oFunds.dicRemainingAmt = dicBackup;
                        }
                        //tanwar2 - JIRA 6260 - end
                        sSplitRowIdName = "AUTO_SPLIT_ID";
                    }
                    else
                    {
                        oFunds.MoveTo(iTransId);
                        sSplitRowIdName = "SPLIT_ROW_ID";
                    }
                    if (oFunds.IsDeductibleApplicable(true))
                    {
                        //Adding into dictionary as cannot add new row while within the loop defined by the table
                        //jira# RMA-6135.Multicurrency
                        dNewSplits.Add(iTransId, oFunds.CreateDeductibles(oFunds.TransSplitList, oFunds.TransId, out iCount, bUseOriginalDeductibleAmt,false));
                        if (bNew && iCount == 0)
                        {
                            //Changed by Nikhil.Code review changes
                          //  hSetPolicyUnit.Remove(iClaimId + "|" + iPolicyId + "|" + iUnitRowId);
                            hSetPolicyUnit.Remove(String.Concat(iClaimId.ToString(), "|", iPolicyId.ToString(), "|", iUnitRowId.ToString()));
                        }
                    }
                    else if (bNew)
                    {
                        //Changed by Nikhil.Code review changes
                        //hSetPolicyUnit.Remove(iClaimId + "|" + iPolicyId + "|" + iUnitRowId);
                        hSetPolicyUnit.Remove(String.Concat(iClaimId.ToString(), "|", iPolicyId.ToString(), "|", iUnitRowId.ToString()));
                    }
                }
            }

            //using variant so that both key and value can be used effectively whenever required
            foreach (var item in dNewSplits)
            {
                lSplits = item.Value;
                foreach (FundsTransSplit oSplit in lSplits)
                {
                    DataRow[] rows = p_oFundsTable.Select("FUNDS_TRANS_ID=" + item.Key);
                    DataRow newRow = p_oFundsTable.NewRow();
                    for (int i = 0; i < p_oFundsTable.Columns.Count; i++)
                    {
                        newRow[i] = rows[0][i];
                    }

                    int iIndex = p_oFundsTable.Rows.IndexOf(rows[rows.Length - 1]);
                  
                    dAmt = Conversion.CastToType<double>(Convert.ToString(rows[0]["FUNDS_AMOUNT"]), out bSuccess);
                    
                    //tanwar2 - Negative split changed to collection - start
                  
                    if (oSplit.Context.LocalCache.GetRelatedCodeId(oSplit.ReserveTypeCode) ==
                            oSplit.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE"))
                    {
                        dAmt -= oSplit.Amount;
                    }
                    else
                    {
                        dAmt += oSplit.Amount;
                    }
                    //tanwar2 - end

                    //take backup
                   
                    //Start - Added By Nikhil on 08/06/2014.Added control number for deductible recovery split.
                    sCtlNumber = Convert.ToString(rows[0]["CTL_NUMBER"]);
                    //end - Added By Nikhil on 08/06/2014.Added control number for deductible recovery split.

                    //update column values
                    newRow[sSplitRowIdName] = -1;
                    //Start- Added by nikhil on 07/30/14 ,show negative amount for recovery on check
               
                    if (oSplit.Context.LocalCache.GetRelatedCodeId(oSplit.ReserveTypeCode) ==
                          oSplit.Context.LocalCache.GetCodeId("R", "MASTER_RESERVE"))
                    {
                        newRow["AMOUNT"] = -1 * oSplit.Amount;
                    }
                    else
                    {
                        newRow["AMOUNT"] = oSplit.Amount;
                    }
                    //end- Added by nikhil on 07/30/14 ,show negative amount for recovery on check

                    newRow["FUNDS_AMOUNT"] = dAmt;
                    newRow["TRANS_TYPE_CODE"] = oSplit.TransTypeCode;
                    //Start - Added By Nikhil on 08/06/2014.Added control number for deductible recovery split.
                    //Start: Changed the following code by Sumit Agarwal: 10/13/2014
                    //rows[0]["CTL_NUMBER"] = sCtlNumber + "_D";
                    sDedRecoveryIdentifierChar = oSplit.Context.DbConnLookup.ExecuteString(string.Format("SELECT DED_RECOVERY_IDENTIFIER_CHAR FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE={0}", Common.Conversion.ConvertStrToInteger(rows[0]["LINE_OF_BUS_CODE"].ToString())));
                    newRow["CTL_NUMBER"] = sCtlNumber + "_" + sDedRecoveryIdentifierChar;
                    //End: Changed the following code by Sumit Agarwal: 10/13/2014
                    //End - Added By Nikhil on 08/06/2014.Added control number for deductible recovery split.
                    //Insert Row in table as a new row
                 //   p_oFundsTable.ImportRow(rows[0]);
                    p_oFundsTable.Rows.InsertAt(newRow, iIndex + 1);

                  
                    //Update Funds amount of all corresponding rows
                    foreach (DataRow row in rows)
                    {
                        row["FUNDS_AMOUNT"] = dAmt;
                    }
                }
            }

            p_oFundsTable.DefaultView.Sort = "CTL_NUMBER ASC";
            return p_oFundsTable.DefaultView.ToTable(); 
        }
        //static Dictionary<int, Double> GetMaxPrintAmounts(int p_iUserId, int p_iUserGroupId, string m_sConnectionString)
        //{
        //    Dictionary<int, Double> dMaxAmounts = new Dictionary<int, Double>();

        //    string sSQL = "";
          
        //    try
        //    {
        //        if (p_iUserGroupId > 0 && p_iUserId == 0)

        //            sSQL = sSQL = " SELECT MAX_AMOUNT, LINE_OF_BUS_CODE FROM PRT_CHECK_LIMITS WHERE GROUP_ID = " + p_iUserGroupId;
                    
        //        else

        //            sSQL = " SELECT MAX_AMOUNT, LINE_OF_BUS_CODE FROM PRT_CHECK_LIMITS WHERE USER_ID = " + p_iUserId;

        //        using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
        //        {
        //            while (objReader.Read())
        //            {
        //                dMaxAmounts.Add(Common.Conversion.ConvertObjToInt(objReader.GetValue("LINE_OF_BUS_CODE")), Common.Conversion.ConvertObjToDouble(objReader.GetValue("MAX_AMOUNT"), m_iClientId));
        //            }
        //        }
        //    }
        //    catch (RMAppException p_objEx)
        //    {
        //        throw p_objEx;
        //    }
        //    catch (Exception p_objEx)
        //    {
        //        throw new RMAppException(Globalization.GetString("CheckRegister.GetAllMaxPrintAmount.Error", m_iClientId), p_objEx);
        //    }
        //    finally
        //    {

        //    }
        //    return dMaxAmounts;
        //}

		/*
		 * SAMPLE XML
		 * 
		 * <SavePreCheckDetail>
				<AccountId>1</AccountId> 
				<OrderBy>BANK_ACC_SUB</OrderBy> 
				<IncludeAutoPayment>1</IncludeAutoPayment> 
				<UsingSelection>1</UsingSelection> 
				<AttachedClaimOnly>0</AttachedClaimOnly> 
				<FromDateFlag>0</FromDateFlag> 
				<FromDate /> 
				<ToDateFlag>1</ToDateFlag> 
				<ToDate>20041231</ToDate> 
				<SelectedChecksIds>123,124</SelectedChecksIds>
				<SelectedAutoChecksIds>234,235</SelectedAutoChecksIds>
			</SavePreCheckDetail>
			*/
		/// Name		: SavePreCheckDetail
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save the Pre Checks Detail.
		/// </summary>
		/// <param name="p_objInputXml">Input XML Document</param>
		/// <param name="p_sOutXML">Set the XML string, contains output information. </param>
		/// <returns>Return the XML document.</returns>
		public XmlDocument SavePreCheckDetail( XmlDocument p_objInputXml )
		{
			CheckSetup objCheckSetup = null ;
			Account objAccount = null ;
			DataSet objDSPrintPre = null ;
			DataTable objDTPrintPre = null ;
			DataSet objDSPrintPreAuto = null ;
			DataTable objDTPrintPreAuto = null ;
			ArrayList arrlstUpdateSQL = null ;
			DbConnection objConn = null ;
			
			ArrayList arrlstSelectedAutoCheck = null ;
			ArrayList arrlstSelectedCheck = null ;

			int iTransId = 0 ;
			int iRowIndexPre = 0 ;
			int iRowIndexPreAuto = 0 ;
			int iBatchNum = 0;
			int iAccountId = 0 ;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            int iOrgHierarchyLevel = 0;
			int iRecTransID = 0 ;
			int iIndex = 0 ;
			string sSQL = "" ;
			string sFromDate = "" ;
			string sToDate = "" ;		
			string sOrderByField = "" ;
			string sFromDateValue = "" ;
			string sToDateValue = "" ;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string sOrgHierarchy = "";
			bool bIncludeAutoPays = false ;
			bool bWithAttachedClaimsOnly = false ;
			bool bUseFromDate = false ;
			bool bUseToDate = false ;
			bool bUsingSelections = false ;				
			bool bAccountFound = false ;								
            //skhare7 R8 enhancement
            bool bIncludeCombinedPays = false;
			//skhare7 end
            // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            //bool bPrintEFTPayment = false;//JIRA:438 START: ajohari2
            int iDistributionType = 0;
            // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
			
			try
			{
                dicMaxAmounts = null;
                dicMaxAmounts = CheckSetup.GetMaxPrintAmounts(m_iUserId, m_iUserGroupId, m_sConnectionString, m_iClientId);//skhare7 28010

                bUseFromDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "FromDateFlag", m_iClientId));
                bUseToDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "ToDateFlag", m_iClientId));
                sFromDateValue = Functions.GetValue(p_objInputXml, "FromDate", m_iClientId);
                sToDateValue = Functions.GetValue(p_objInputXml, "ToDate", m_iClientId);
                bIncludeAutoPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "IncludeAutoPayment", m_iClientId));
                  //skhare7 R8 enhancement
                bIncludeCombinedPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "IncludeCombinedPayment", m_iClientId));
                //skhare7 R8
				iAccountId = Common.Conversion.ConvertStrToInteger( Functions.GetValue( p_objInputXml , "AccountId", m_iClientId ) );
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                sOrgHierarchy = Functions.GetValue(p_objInputXml, "OrgHierarchy", m_iClientId);
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                iOrgHierarchyLevel = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputXml, "OrgHierarchyLevel", m_iClientId));
				bWithAttachedClaimsOnly = Common.Conversion.ConvertStrToBool( Functions.GetValue( p_objInputXml , "AttachedClaimOnly", m_iClientId ) );
				bUsingSelections = Common.Conversion.ConvertStrToBool( Functions.GetValue( p_objInputXml , "UsingSelection" , m_iClientId) );
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //bPrintEFTPayment = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputXml, "PrintEFTPayment", m_iClientId));
                //JIRA:438 End: 
                iDistributionType = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputXml, "DistributionType", m_iClientId));
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
				sOrderByField = Functions.GetValue( p_objInputXml , "OrderBy" , m_iClientId);

                sOrderByField = Functions.GetSortOrder(sOrderByField, m_iClientId);

				if( bUsingSelections )
				{
					arrlstSelectedAutoCheck = Functions.GetList( p_objInputXml , "AutoTransIds" , "," ,m_iClientId);
					arrlstSelectedCheck = Functions.GetList( p_objInputXml , "TransIds" , "," ,m_iClientId);					
				}
			
				if( Utilities.IsDate( sFromDateValue ) && bUseFromDate )
					sFromDate = Conversion.GetDate( sFromDateValue );
				else
					sFromDate = "" ;					
				
				if( Utilities.IsDate( sToDateValue ) && bUseToDate )
					sToDate = Conversion.GetDate( sToDateValue );
				else
					sToDate = "" ;
			
				#region Get the Batch Number
				objAccount = ( Account ) m_objDataModelFactory.GetDataModelObject( "Account" , false );
				try
				{
					objAccount.MoveTo( iAccountId );
					bAccountFound = true ;
				}
				catch
				{
					bAccountFound = false ;
				}
				if( bAccountFound )
					iBatchNum = objAccount.NextBatchNumber ;
			
				if( iBatchNum == 0 )
					iBatchNum = 1 ;
				#endregion 

				objConn = DbFactory.GetDbConnection( m_sConnectionString );

				#region Update Check status,
							
				#region Update PreRegister checks
                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                ////Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                //sSQL = this.CheckPrintSQLPreCheck(sFromDate, sToDate, bIncludeAutoPays, iAccountId, sOrderByField, false, bWithAttachedClaimsOnly);
                sSQL = this.CheckPrintSQLPreCheck(sFromDate, sToDate, bIncludeAutoPays, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, sOrderByField, false, bWithAttachedClaimsOnly, bIncludeCombinedPays, iDistributionType); //JIRA:438 : ajohari2
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
				objDSPrintPre = DbFactory.GetDataSet( m_sConnectionString , sSQL, m_iClientId);
				objDTPrintPre = objDSPrintPre.Tables[0] ;
				iRowIndexPre = 0 ;

				if( objDTPrintPre.Rows.Count > 0 )
				{
					iTransId = 0 ;
					arrlstUpdateSQL = new ArrayList();
					while( iRowIndexPre < objDTPrintPre.Rows.Count )
					{
						iRecTransID = Common.Conversion.ConvertStrToInteger( objDTPrintPre.Rows[iRowIndexPre]["FUNDS_TRANS_ID"].ToString() ) ;
						if( iTransId != iRecTransID )
						{//Parijat: Mits 10687
                            if (!this.SkipPayment(objDTPrintPre, iRowIndexPre, false, "FUNDS_AMOUNT", 
								bUsingSelections , arrlstSelectedCheck , arrlstSelectedAutoCheck ) )
							{
								sSQL = " UPDATE FUNDS SET PRECHECK_FLAG = -1, BATCH_NUMBER = " + iBatchNum 
									+	" WHERE TRANS_ID = " + iRecTransID ;
								arrlstUpdateSQL.Add( sSQL );
							}
						}
						iTransId = iRecTransID ;
						iRowIndexPre++ ;
					}
					
					objConn.Open();
					for( iIndex = 0 ; iIndex < arrlstUpdateSQL.Count ; iIndex++ )
					{
						sSQL = (string) arrlstUpdateSQL[iIndex] ;
						objConn.ExecuteNonQuery( sSQL );
					}
					objConn.Close();
					arrlstUpdateSQL = null ;
				}
				#endregion 

				#region Update PreRegister Auto Checks
				if( bIncludeAutoPays )
				{
                    ////Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                    // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                    sSQL = this.CheckPrintSQLPreCheckAuto(sFromDate, sToDate, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, sOrderByField, true, false, bIncludeCombinedPays, iDistributionType); //JIRA:438 : 
                    // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
					objDSPrintPreAuto = DbFactory.GetDataSet( m_sConnectionString , sSQL, m_iClientId);
					objDTPrintPreAuto = objDSPrintPreAuto.Tables[0] ;
					iRowIndexPreAuto = 0 ;

					iTransId = 0 ;
					arrlstUpdateSQL = new ArrayList();

					while( iRowIndexPreAuto < objDTPrintPreAuto.Rows.Count )
					{
						iRecTransID = Common.Conversion.ConvertStrToInteger( objDTPrintPreAuto.Rows[iRowIndexPreAuto]["FUNDS_TRANS_ID"].ToString() ) ;
						if( iTransId != iRecTransID )
						{//Parijat:Mits 10687
                            if (!this.SkipPayment(objDTPrintPreAuto, iRowIndexPreAuto, true, "FUNDS_AMOUNT", bUsingSelections, arrlstSelectedCheck, arrlstSelectedAutoCheck))
							{
								sSQL = " UPDATE FUNDS_AUTO SET PRECHECK_FLAG = -1, CHECK_BATCH_NUM = " + iBatchNum 
									+	" WHERE AUTO_TRANS_ID = " + iRecTransID ;

								arrlstUpdateSQL.Add( sSQL );
							}
						}
						iTransId = iRecTransID ;
						iRowIndexPreAuto++ ;
					}
										
					objConn.Open();
					for( iIndex = 0 ; iIndex < arrlstUpdateSQL.Count ; iIndex++ )
					{
						sSQL = (string) arrlstUpdateSQL[iIndex] ;
						objConn.ExecuteNonQuery( sSQL );
					}
					objConn.Close();
					arrlstUpdateSQL = null ;
				}
				#endregion 

				#endregion

				#region Record a new 'batch' record
				if( sOrderByField == "" )
					sOrderByField = " " ;

				sSQL = " INSERT INTO CHECK_BATCHES (BATCH_NUMBER,ACCOUNT_ID,ORDER_BY_FIELD,DATE_RUN) VALUES ("
					+	iBatchNum + "," + iAccountId + ",'" + sOrderByField + "','" 
					+	Conversion.ToDbDate(System.DateTime.Now ) + "')" ; 
				
				objConn.Open();
				objConn.ExecuteNonQuery( sSQL );

				sSQL = " UPDATE ACCOUNT SET NEXT_BATCH_NUMBER = " + (iBatchNum + 1) 
					+	" WHERE ACCOUNT_ID = " + iAccountId ;
				objConn.ExecuteNonQuery( sSQL );
				objConn.Close();
				#endregion 

				// Return the Updated Xml.
                objCheckSetup = new CheckSetup(m_sDsnName, m_sUserName, m_sPassword, m_iUserId, m_iUserGroupId, m_iClientId);
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                //objCheckSetup.CheckSetChanged( bUseFromDate , bUseToDate , sFromDateValue ,sToDateValue, bWithAttachedClaimsOnly, bIncludeAutoPays, iAccountId, false,false , null , null ); 

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                objCheckSetup.CheckSetChanged(bUseFromDate, bUseToDate, sFromDateValue,
                    sToDateValue, bWithAttachedClaimsOnly, bIncludeAutoPays, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, false,
                    false, null, null, bIncludeCombinedPays, iDistributionType); //JIRA:438 : ajohari2
                
                m_objDocument = objCheckSetup.CheckBatchChangedPre(iBatchNum, iAccountId, true, iDistributionType);				
				// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.SavePreCheckDetail.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objAccount != null )
				{
					objAccount.Dispose();
					objAccount = null ;
				}
				if( objDSPrintPre != null )
				{
					objDSPrintPre.Dispose();
					objDSPrintPre = null ;
				}
				if( objDTPrintPre != null )
				{
					objDTPrintPre.Dispose();
					objDTPrintPre = null ;
				}
				if( objDSPrintPreAuto != null )
				{
					objDSPrintPreAuto.Dispose();
					objDSPrintPreAuto = null ;
				}
				if( objDTPrintPreAuto != null )
				{
					objDTPrintPreAuto.Dispose();
					objDTPrintPreAuto = null ;
				}
				arrlstUpdateSQL = null ;
				if( objConn != null )
				{
					//objConn.Close();
					objConn.Dispose();					
				}
				arrlstSelectedAutoCheck = null ;
				arrlstSelectedCheck = null ;
                if (objCheckSetup != null)
                    objCheckSetup.Dispose();
			}
			return( m_objDocument );
		}

		/// Name		: SkipPayment
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return true, if checks need to skip from payment.
		/// </summary>
		/// <param name="p_objDTPrint">DataTable contains records</param>
		/// <param name="p_iRowIndex">Current Row Index</param>
		/// <param name="p_bCheckTypeAuto">Is Auto Check</param>
		/// <param name="p_sAmountName">Amount Name used in SQL </param>
		/// <param name="p_bUsingSelections">Is Using Selection</param>
		/// <param name="p_arrlstSelectedCheck">Selected list of Pre Checks</param>
		/// <param name="p_arrlstSelectedAutoCheck">Selected List of Pre Auto Checks</param>
		/// <returns>true/false</returns>
		private bool SkipPayment( DataTable p_objDTPrint , int p_iRowIndex , bool p_bCheckTypeAuto , 
			string p_sAmountName , bool p_bUsingSelections , ArrayList p_arrlstSelectedCheck , 
			ArrayList p_arrlstSelectedAutoCheck )
		{
			Functions objFunctions = null ;

			double dblAmount = 0.0 ;
			double dblAmountLimit = 0.0 ;
			bool bReturnValue = false ;
			int iPayment = 0 ;
			int iFreeze = 0 ;
			int iTransId = 0 ;
			int iLob = 0 ;
					
			try
			{
				objFunctions = new Functions( m_objDataModelFactory );

				iPayment = Common.Conversion.ConvertStrToInteger( p_objDTPrint.Rows[p_iRowIndex]["PAYMNT_FROZEN_FLAG"].ToString()) ;
				iFreeze = Common.Conversion.ConvertStrToInteger( p_objDTPrint.Rows[p_iRowIndex]["FREEZE_PAYMENTS"].ToString()) ;
				iTransId = Common.Conversion.ConvertStrToInteger( p_objDTPrint.Rows[p_iRowIndex]["FUNDS_TRANS_ID"].ToString() );
				iLob = Common.Conversion.ConvertStrToInteger( p_objDTPrint.Rows[p_iRowIndex]["LINE_OF_BUS_CODE"].ToString() );
				dblAmount = Common.Conversion.ConvertStrToDouble( p_objDTPrint.Rows[p_iRowIndex][p_sAmountName].ToString());				

				if( iPayment != 0 || iFreeze != 0 )
				{
					bReturnValue = true ;
				}
				else
				{			
					if( p_bUsingSelections )
					{
						if( ! Functions.CheckInSelectionList( iTransId , p_bCheckTypeAuto , p_arrlstSelectedCheck , p_arrlstSelectedAutoCheck,m_iClientId ) ) 
						{					
							bReturnValue = true ; // If selection is used and check is not in selected list then skip true.
						}
						else
						{
							// check is in selection list. now do validity. If validity falis, skip true.
							// do validity
							
							try
							{
								m_objLobSettings = m_objColLobSettings[iLob] ;
							}
							catch
							{
								m_objLobSettings = null ;
							}
							if( m_objLobSettings != null )
							{
								if( m_objLobSettings.PrtChkLmtFlag )
								{
                                    if (dicMaxAmounts.ContainsKey(iLob)) // True
                                    {
                                        double dMaxValue = dicMaxAmounts[iLob];
                                        if (dblAmount > dMaxValue)
                                        {
                                            bReturnValue = true;
                                        }
                                    }
								}	
							}
						}
					}
					else
					{	
						// do validity
						try
						{
							m_objLobSettings = m_objColLobSettings[iLob] ;
						}
						catch
						{
							m_objLobSettings = null ;
						}
						if( m_objLobSettings != null )
						{
							if( m_objLobSettings.PrtChkLmtFlag )
							{
                                if (dicMaxAmounts.ContainsKey(iLob)) // True
                                {
                                    double dMaxValue = dicMaxAmounts[iLob];
                                    if (dblAmount > dMaxValue)
                                    {
                                        bReturnValue = true;
                                    }
                                }
							}	
						}
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.SkipPayment.Error", m_iClientId), p_objEx);
			}
			finally
			{
                objFunctions = null;
                    
			}
			return( bReturnValue );				
		}
				
		/// Name		: PreCheckDetailRegPrtData
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Pre Checks Details.
		/// </summary>
		/// <param name="p_objDTPrintPre">DataTable Records for Pre Checks</param>
		/// <param name="p_iRowIndexPre">Current Row Index for Pre Checks</param>
		/// <param name="p_objDTPrintPreAuto">DataTable Records for Pre Auto Checks</param>
		/// <param name="p_iRowIndexPreAuto">Current Row Index for Pre Auto Checks</param>
		/// <param name="p_iLine">Line Number</param>
		/// <param name="p_dblCurrentY">Current Y</param>
		/// <param name="p_dblPageTotal">Page Total</param>
		/// <param name="p_dblReportTotal">Report Total</param>
		/// <param name="p_dblBottomHeader">Bottom Header Location</param>
		/// <param name="p_iTransId">Trans Id</param>
		/// <param name="p_iPageTotal">Page Total</param>
		/// <param name="p_sOrderByField">Order By String</param>
		/// <param name="p_iOldSubAccount">Old Sub Account</param>
		/// <param name="p_iOldAdjuster">Old Adjuster</param>
		/// <param name="p_bSkipRegChecks">Skip Reg Checks Flag</param>
		/// <param name="p_bMergeAutoChecks">Merge Auto Checks Flag</param>
		/// <param name="p_bAutoTrans">Is Last printed check was an Auto Check Flag </param>
		/// <param name="p_dblLeftMargin">Left Margin</param>
		/// <param name="p_dblLineHeight">Line Height</param>
		/// <param name="p_dblCharWidth">Char Width</param>
		/// <param name="p_dblPageWidth">Page Width</param>
		/// <param name="p_arrPreCheckRegColWidth">Columns Width</param>
		/// <param name="p_arrPreCheckRegColAlign">Columns Alignment</param>
		/// <returns>Success/Failure</returns>
		private bool PreCheckDetailRegPrtData( DataTable p_objDTPrintPre , ref int p_iRowIndexPre , 
			DataTable p_objDTPrintPreAuto , ref int p_iRowIndexPreAuto , int p_iLine , ref double p_dblCurrentY , 
			ref double p_dblPageTotal , ref double p_dblReportTotal , double p_dblBottomHeader , ref int p_iTransId , 
			ref int p_iPageTotal , string p_sOrderByField , ref int p_iOldSubAccount , ref int p_iOldAdjuster, 
			bool p_bSkipRegChecks , bool p_bMergeAutoChecks, ref bool p_bAutoTrans , double p_dblLeftMargin , 
			double p_dblLineHeight , double p_dblCharWidth , double p_dblPageWidth , double[] p_arrPreCheckRegColWidth , 
			string[] p_arrPreCheckRegColAlign )
		{
			DataTable objDTPrint = null ;
			Entity objEntity = null ;
			BankAccSub objBankAccSub = null ;
			
			int iRowIndex = 0 ;
			int iNewAdjuster = 0 ;
			int iNewSubAccount = 0 ;
			int iIndex = 0 ;
            // akaushik5 Commented for MITS 35846 Starts
            //int iClaimId = 0 ;
            // akaushik5 Commented for MITS 35846 Ends
            int iTransTypeCode = 0;
			string sData = "" ;
			string sSpace = "                                        " ;  // space( 40 );
			string sAdjuster = "" ;
			string sShortCode = "" ;
			string sSubAccountName = "" ;
			string sCodeDesc = "" ;
            //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string abbr = string.Empty;
            string name = string.Empty;
            //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
			double dblAmount = 0.0 ;
			double dblFundsAmount = 0.0 ;
			double dblRoundedAmount = 0.0 ;
			double dblCurrentX = 0.0 ;
			bool bPreCheckEnd = false ;
			bool bNewCheck = true ;
			bool bPreAutoCheckEnd = false ;
			bool bLeadingLine = false ;
			bool bEntityExist = false ;
			bool bReturnValue = false ;
			bool bBankAccSubExist = false ;
            //Added by Nikhil.
            int iLineOfBusCode = 0;
            int iClaimID = 0;
            bool isDeductibleSplit = false;
            //End
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            // akaushik5 Commented for MITS 35846 Starts
            //CCacheFunctions objCCacheFunctions = null;
            // akaushik5 Commented for MITS 35846 Ends
			
			try
			{
				
				#region Select which type of check need to be print, Set ObjDTPrint & iRowIndex

				if( p_bAutoTrans )  // if last one was an auto check, then print Auto check 
				{
					if( p_iRowIndexPreAuto < p_objDTPrintPreAuto.Rows.Count )
					{
						if( p_iTransId == Common.Conversion.ConvertStrToInteger( p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["FUNDS_TRANS_ID"].ToString() ))
						{
							objDTPrint = p_objDTPrintPreAuto ;
							iRowIndex = p_iRowIndexPreAuto ;
							bNewCheck = false ;
						}
						
					}
				}
				else		// else print normal chcek.
				{
					if( !p_bSkipRegChecks )
					{
						if( p_iRowIndexPre < p_objDTPrintPre.Rows.Count )
						{
							if( p_iTransId == Common.Conversion.ConvertStrToInteger( p_objDTPrintPre.Rows[p_iRowIndexPre]["FUNDS_TRANS_ID"].ToString() ))
							{
								objDTPrint = p_objDTPrintPre ;
								iRowIndex = p_iRowIndexPre ;
								bNewCheck = false ;
							}
						
						}
					}
				}

				if( objDTPrint == null )
				{
					// now need to decide who gets to print if ordering is used
					// case 1 sorted
					// case 2::: no sort put all reg payments first, then autos
					if( p_bSkipRegChecks )
					{
						objDTPrint = p_objDTPrintPreAuto ;
						iRowIndex = p_iRowIndexPreAuto ;
					}
					else
					{
						if( p_sOrderByField != "" )
						{
							if( p_bMergeAutoChecks )
							{
								if( p_iRowIndexPreAuto >= p_objDTPrintPreAuto.Rows.Count )
								{
									objDTPrint = p_objDTPrintPre ;
									iRowIndex = p_iRowIndexPre ;
								}
								else
								{
									if( p_iRowIndexPre >= p_objDTPrintPre.Rows.Count )
									{
										objDTPrint = p_objDTPrintPreAuto ;
										iRowIndex = p_iRowIndexPreAuto ;
									}
									else
									{
										if( this.PrintFromAutoRecords( p_objDTPrintPre , p_iRowIndexPre , p_objDTPrintPreAuto , p_iRowIndexPreAuto , p_sOrderByField ) )
										{
											objDTPrint = p_objDTPrintPreAuto ;
											iRowIndex = p_iRowIndexPreAuto ;
										}
										else
										{
											objDTPrint = p_objDTPrintPre ;
											iRowIndex = p_iRowIndexPre ;
										}
									}
								}
							}	// In not include auto payments
							else
							{
								objDTPrint = p_objDTPrintPre ;
								iRowIndex = p_iRowIndexPre ;
							}
						}
						else // 'not sorted
						{
							if( p_iRowIndexPre < p_objDTPrintPre.Rows.Count )
							{
								objDTPrint = p_objDTPrintPre ;
								iRowIndex = p_iRowIndexPre ;
							}
							else
							{
								if( p_bMergeAutoChecks )
								{
									objDTPrint = p_objDTPrintPreAuto ;
									iRowIndex = p_iRowIndexPreAuto ;
								}
								else
								{
									objDTPrint = p_objDTPrintPre ;
									iRowIndex = p_iRowIndexPre ;
								}
							}
						}
					}
				}
				#endregion 

				if( iRowIndex < objDTPrint.Rows.Count )
				{
					dblCurrentX = p_dblLeftMargin ;
					m_objPrintWrapper.CurrentY = p_dblCurrentY ;
				
					#region If Sort by Adjuster 
					if( p_sOrderByField == "ADJUSTER_EID" )
					{
						iNewAdjuster = Common.Conversion.ConvertStrToInteger( objDTPrint.Rows[iRowIndex]["ADJUSTER_EID"].ToString() );
						// If this is the first line to be print on the page.
						if( p_iLine == 1 )
						{
							p_dblCurrentY += p_dblLineHeight * 2 ;
							m_objPrintWrapper.CurrentX = dblCurrentX ;
							m_objPrintWrapper.SetFontBold( true );
							objEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
							try
							{
								objEntity.MoveTo( iNewAdjuster );
								bEntityExist = true ;
							}
							catch
							{
								bEntityExist = false ;
							}
							if( bEntityExist )
							{
								sAdjuster = ( objEntity.FirstName + " " + objEntity.LastName).Trim() ;
								if( sAdjuster != "" )
                                    sSpace += Functions.ReplaceArguments(CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetailRegPrtData.CurrentAdjuster", m_iClientId)), m_iClientId, sAdjuster);
								else
                                    sSpace += CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetailRegPrtData.NoCurrentAdjuster", m_iClientId));							
							}
							else
							{
                                sSpace += CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetailRegPrtData.NoCurrentAdjuster", m_iClientId));							
							}
							objEntity.Dispose();
							m_objPrintWrapper.PrintText( sSpace );
							m_objPrintWrapper.SetFontBold( false );
							m_objPrintWrapper.PrintText( "" );
						}
						else
						{
							if( p_iOldAdjuster != iNewAdjuster )
							{
								p_iOldAdjuster = -10 ;
								return( false );
							}
						}
						p_iOldAdjuster = iNewAdjuster ;
					}			
					#endregion 

					#region If Sort by Sub Bank Account 
					if( p_sOrderByField == "BANK_ACC_SUB" )
					{
						iNewSubAccount = Common.Conversion.ConvertStrToInteger( objDTPrint.Rows[iRowIndex]["SUB_BNK_ACC"].ToString() );
						// If this is the first line to be print on the page.
						if( p_iLine == 1 )
						{
							p_dblCurrentY += p_dblLineHeight * 2 ;
							m_objPrintWrapper.CurrentX = dblCurrentX ;
							m_objPrintWrapper.SetFontBold( true );
							objBankAccSub = ( BankAccSub ) m_objDataModelFactory.GetDataModelObject( "BankAccSub" , false );
							try
							{
								objBankAccSub.MoveTo( iNewSubAccount );
								bBankAccSubExist = true ;
							}
							catch
							{
								bBankAccSubExist = false ;
							}
							if( bBankAccSubExist )
							{
								sSubAccountName = ( objBankAccSub.SubAccName + " " + objBankAccSub.SubAccNumber).Trim() ;
								if( sSubAccountName != "" )
                                    sSpace += Functions.ReplaceArguments(CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetailRegPrtData.CurrentSubAccount", m_iClientId)), m_iClientId, sSubAccountName);
								else
                                    sSpace += CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetailRegPrtData.NoCurrentSubAccount", m_iClientId));							
							}
							else
							{
                                sSpace += CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PreCheckDetailRegPrtData.NoCurrentSubAccount", m_iClientId));							
							}
							m_objPrintWrapper.PrintText( sSpace );
							m_objPrintWrapper.SetFontBold( false );
							m_objPrintWrapper.PrintText( "" );
						}
						else
						{
							if( p_iOldSubAccount != iNewSubAccount )
							{
								p_iOldSubAccount = -10 ;
								return( false );
							}
						}
						p_iOldSubAccount = iNewSubAccount ;
					}
					#endregion 
			
					if( objDTPrint == p_objDTPrintPre ) 
						p_bAutoTrans = false ;
					else
						p_bAutoTrans = true ;
			
					m_objPrintWrapper.CurrentY = p_dblCurrentY ;
					dblCurrentX = p_dblLeftMargin ;

					bLeadingLine = bNewCheck || p_iLine == 1 ;
					p_iTransId = Common.Conversion.ConvertStrToInteger( objDTPrint.Rows[iRowIndex]["FUNDS_TRANS_ID"].ToString() ) ;
                    //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Updated the no. of columns to 9
                    //for( iIndex = 0 ; iIndex < 8 ; iIndex++ )
                    for (iIndex = 0; iIndex < 9; iIndex++)
					{
						#region Print Each Column for current record
						sData = "" ;
						switch( iIndex )
						{
							case 0 :
                                //Added by Nikhil
                                iClaimID = Common.Conversion.ConvertStrToInteger(objDTPrint.Rows[iRowIndex]["CLAIM_ID"].ToString());
                                iTransTypeCode = Common.Conversion.ConvertStrToInteger(objDTPrint.Rows[iRowIndex]["TRANS_TYPE_CODE"].ToString());
                                iLineOfBusCode = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + iClaimID);
                                // npadhy - RMA-6297 Error in precheck of  printing orphan transaction
                                if(iLineOfBusCode > 0)
                                    isDeductibleSplit = iTransTypeCode == m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLineOfBusCode].DedRecTransType;
                
                                //End
                                if (bLeadingLine || isDeductibleSplit)
									sData = objDTPrint.Rows[iRowIndex]["CTL_NUMBER"].ToString() ;
								break;
							case 1 :
								if( bLeadingLine )
								{
									if( p_bAutoTrans )
										sData = Conversion.GetDBDateFormat( objDTPrint.Rows[iRowIndex]["PRINT_DATE"].ToString() , "d" );
									else
										sData = Conversion.GetDBDateFormat( objDTPrint.Rows[iRowIndex]["TRANS_DATE"].ToString() , "d" );								
								}
								break;
							case 2 :
								if( bLeadingLine )
									sData = objDTPrint.Rows[iRowIndex]["CLAIM_NUMBER"].ToString() ;
								break;
							case 3 :
                                if (bLeadingLine)
                                {
                                    // akaushik5 Changed for MITS 35846 Starts
                                    //iClaimId = Common.Conversion.ConvertStrToInteger(objDTPrint[iRowIndex]["CLAIM_ID"].ToString());
                                    //sData = GetOrgAbbrevFromClaimID(iClaimId);
                                    sData = objDTPrint.Rows[iRowIndex]["ORG_ABBR"].ToString();
                                    // akaushik5 Changed for MITS 35846 Ends
                                }
								break ;
							case 4 :
								if( bLeadingLine )
								{
                                //MGaba2:MITS 13063:09/01/2008:Added Trim Function:To avoid space in case first name is blank
                                    sData = (objDTPrint.Rows[iRowIndex]["FIRST_NAME"].ToString() + " " 
										+ objDTPrint.Rows[iRowIndex]["LAST_NAME"].ToString()).Trim() ;
								}
								break;						
							case 5 :
								iTransTypeCode = Common.Conversion.ConvertStrToInteger( objDTPrint.Rows[iRowIndex]["TRANS_TYPE_CODE"].ToString() );
								m_objLocalCache.GetCodeInfo( iTransTypeCode , ref sShortCode , ref sCodeDesc );
								sData = sCodeDesc ;
								break;
							case 6 :
								dblAmount = Common.Conversion.ConvertStrToDouble( objDTPrint.Rows[iRowIndex]["AMOUNT"].ToString() );
                               // double dExhClaimRateToBase = CommonFunctions.ConvertByCurrencyCode(
                                sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblAmount, m_sConnectionString, m_iClientId);//skhare7 r8 
								//sData = string.Format( "{0:C}" , dblAmount );
								break;
							case 7 :
                                //pmittal5 MITS: 13081 09/03/08
								//if( bLeadingLine )   
                                if (bNewCheck)
                                {
                                    dblFundsAmount = Common.Conversion.ConvertStrToDouble(objDTPrint.Rows[iRowIndex]["FUNDS_AMOUNT"].ToString());
                                    sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblFundsAmount, m_sConnectionString, m_iClientId);//skhare7 r8 
                                   // sData = string.Format("{0:C}", dblFundsAmount);
                                }
                                //pmittal5 MITS: 13081 09/03/08
                                else
                                {
                                    if (p_arrPreCheckRegColAlign[iIndex] == CheckConstants.LEFT_ALIGN)
                                        sData = "   " + "\"";
                                    else
                                        sData = "\"" + "   "; 
                                }
                                //End - pmittal5
								break;										
                            //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                            case 8:
                                if (bLeadingLine)
                                {
                                    if (objDTPrint.Columns.Contains("ORG_LEVEL"))
                                    {
                                        // akaushik5 Commented for MITS 35846 Starts
                                        //objCCacheFunctions = new CCacheFunctions(m_sConnectionString);
                                        // akaushik5 Commented for MITS 35846 Ends
                                        sData = objDTPrint.Rows[iRowIndex]["ORG_LEVEL"].ToString();
                                        if (sData != "")
                                        {
                                            // akaushik5 Changed for MITS 35846 Starts
                                            //objCCacheFunctions.GetOrgInfo(int.Parse(sData), ref abbr, ref name);
                                            m_objLocalCache.GetOrgInfo(int.Parse(sData), ref abbr, ref name);
                                            // akaushik5 Changed for MITS 35846 Ends
                                            sData = abbr;
                                        }
                                    }
                                    else
                                    {
                                        sData = "";
                                    }
                                }
                                //JIRA RMA-9157: ajohari2: Start
                                else
                                {
                                    sData = "  ";
                                }
                                //JIRA RMA-9157: ajohari2: End
                                break;
                            //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
						}// end switch

						// Insert 'dittos' of omitted items
						if( sData == "" )
						{
							if( !bLeadingLine )
							{
								if( p_arrPreCheckRegColAlign[iIndex] == CheckConstants.LEFT_ALIGN )
									sData = "   " + "\"" ;
								else
									sData = "\"" + "   " ;
							}
						}

						while( m_objPrintWrapper.GetTextWidth( sData ) > p_arrPreCheckRegColWidth[iIndex] )
							sData = sData.Substring( 0 , sData.Length - 1 );

						// Print using alignment
						if( p_arrPreCheckRegColAlign[iIndex] == CheckConstants.LEFT_ALIGN )
						{
							m_objPrintWrapper.CurrentX = dblCurrentX ;
							m_objPrintWrapper.PrintTextNoCr( " " + sData );
						}
						else
						{
							m_objPrintWrapper.CurrentX = dblCurrentX + p_arrPreCheckRegColWidth[iIndex] 
								- m_objPrintWrapper.GetTextWidth( sData) - p_dblCharWidth ;
							m_objPrintWrapper.PrintTextNoCr( sData );
						}
						dblCurrentX += p_arrPreCheckRegColWidth[iIndex] ;
						#endregion 

					} // End of Line priniting 

					// Strike sums and go to next record

					if( bNewCheck )
					{
						dblRoundedAmount = Common.Conversion.ConvertStrToDouble( objDTPrint.Rows[iRowIndex]["FUNDS_AMOUNT"].ToString() );
                        dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
						p_dblPageTotal += dblRoundedAmount ;
						p_dblReportTotal += dblRoundedAmount ;
						p_iPageTotal += 1 ;
					}
					p_dblCurrentY += p_dblLineHeight ;
					if( p_bAutoTrans )	
						p_iRowIndexPreAuto++;	// Move Next
					else
						p_iRowIndexPre++;
				}

				bPreAutoCheckEnd = true ;
				if( p_bMergeAutoChecks )
					if( p_iRowIndexPreAuto < p_objDTPrintPreAuto.Rows.Count )
						bPreAutoCheckEnd = false ;

				if( p_bSkipRegChecks )
					bPreCheckEnd = true ;
				else
				{
					if( p_iRowIndexPre < p_objDTPrintPre.Rows.Count )
						bPreCheckEnd = false ;
					else
						bPreCheckEnd = true ;
				}


				if( bPreCheckEnd && bPreAutoCheckEnd )
				{
					if( p_dblCurrentY < p_dblBottomHeader - p_dblLineHeight * 2 )
					{
						p_dblCurrentY += p_dblLineHeight ;
						m_objPrintWrapper.CurrentY = p_dblCurrentY ;
						m_objPrintWrapper.CurrentX = p_dblPageWidth/2 + p_dblLeftMargin ;
                        m_objPrintWrapper.PrintText("Total for this report: " + CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, p_dblReportTotal, m_sConnectionString, m_iClientId));//skhare7 r8 
					//	m_objPrintWrapper.PrintText( "Total for this report: " + string.Format( "{0:C}" , p_dblReportTotal ) );
						p_dblCurrentY += p_dblLineHeight ;
					}
					if( p_sOrderByField == "ADJUSTER_EID" )
						p_iOldAdjuster = -50 ;
					if( p_sOrderByField == "BANK_ACC_SUB" )
						p_iOldSubAccount = -50 ;

					bReturnValue = false ;
				}
				else
				{
					if( p_dblCurrentY < p_dblBottomHeader - p_dblLineHeight )
						bReturnValue = true ;
					else
						bReturnValue = false ;
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("CheckRegister.PreCheckDetailRegPrtData.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objDTPrint != null )
				{
					objDTPrint.Dispose();
					objDTPrint = null ;
				}
				if( objEntity != null )
				{
					objEntity.Dispose();
					objEntity = null ;
				}
				if( objBankAccSub != null )
				{
					objBankAccSub.Dispose();
					objBankAccSub = null ;
				}				
                // akaushik5 Commented for MITS 35846 Starts
                //if (objCCacheFunctions != null)
                //{
                   
                //    objCCacheFunctions = null;
                //}
                // akaushik5 Commented for MITS 35846 Ends
            }
			return( bReturnValue );
		}

		/// Name		: PrintFromAutoRecords
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Return true, if Auto check need to print.
		/// </summary>
		/// <param name="p_objDTPrintPre">DataTable Records for Pre Checks</param>
		/// <param name="p_iRowIndexPre">Current Row Index for Pre Checks</param>
		/// <param name="p_objDTPrintPreAuto">DataTable Records for Pre Auto Checks</param>
		/// <param name="p_iRowIndexPreAuto">Current Row Index for Pre Auto Checks</param>
		/// <param name="p_sOrderByField">Sort By String</param>
		/// <returns>true/false</returns>
		private bool PrintFromAutoRecords( DataTable p_objDTPrintPre , int p_iRowIndexPre , DataTable p_objDTPrintPreAuto , 
			int p_iRowIndexPreAuto , string p_sOrderByField )
		{
			bool bReturnValue = false ;

			int iTemp = 0 ;
			int iTempAuto = 0 ;

			double dblTemp = 0.0 ;
			double dblTempAuto = 0.0 ;

			string sTemp = "" ;
			string sTempAuto = "" ;

			try
			{
				switch( p_sOrderByField )
				{
					case ORDER_BY_CLAIM :
						iTempAuto = Common.Conversion.ConvertStrToInteger( p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["CLAIM_NUMBER"].ToString() );					
						iTemp = Common.Conversion.ConvertStrToInteger( p_objDTPrintPre.Rows[p_iRowIndexPre]["CLAIM_NUMBER"].ToString() );
						if( iTempAuto < iTemp )
							bReturnValue = true ;
						break ;
					case ORDER_BY_TRANS_DATE :
						iTempAuto = Common.Conversion.ConvertStrToInteger( p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["PRINT_DATE"].ToString() );					
						iTemp = Common.Conversion.ConvertStrToInteger( p_objDTPrintPre.Rows[p_iRowIndexPre]["TRANS_DATE"].ToString() );
						if( iTempAuto < iTemp )
							bReturnValue = true ;
						break ;					
					case ORDER_BY_NAME :
						sTempAuto = p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["FIRST_NAME"].ToString() 
							+ " " + p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["LAST_NAME"].ToString() ;					
						sTemp = p_objDTPrintPre.Rows[p_iRowIndexPre]["FIRST_NAME"].ToString() 
							+ " " + p_objDTPrintPre.Rows[p_iRowIndexPre]["LAST_NAME"].ToString() ;
						if( string.Compare( sTempAuto , sTemp ) < 0 ) 
							bReturnValue = true ;
						break ;						
					case ORDER_BY_AMOUNT :
						dblTempAuto = Common.Conversion.ConvertStrToDouble( p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["AMOUNT"].ToString() );					
						dblTemp = Common.Conversion.ConvertStrToDouble( p_objDTPrintPre.Rows[p_iRowIndexPre]["AMOUNT"].ToString() );
						if( dblTempAuto < dblTemp )
							bReturnValue = true ;
						break ;
					case ORDER_BY_CONTROL :
						sTempAuto = p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["CTL_NUMBER"].ToString();					
						sTemp = p_objDTPrintPre.Rows[p_iRowIndexPre]["CTL_NUMBER"].ToString() ;
						if( string.Compare( sTempAuto , sTemp ) < 0 ) 
							bReturnValue = true ;
						break ;	
					case ORDER_BY_ADJUSTER_EID :
						iTempAuto = Common.Conversion.ConvertStrToInteger( p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["ADJUSTER_EID"].ToString() );					
						iTemp = Common.Conversion.ConvertStrToInteger( p_objDTPrintPre.Rows[p_iRowIndexPre]["ADJUSTER_EID"].ToString() );
						if( iTempAuto < iTemp )
							bReturnValue = true ;
						break ;
					case ORDER_BY_BANK_ACCOUNT_SUB :
						iTempAuto = Common.Conversion.ConvertStrToInteger( p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["SUB_BNK_ACC"].ToString() );					
						iTemp = Common.Conversion.ConvertStrToInteger( p_objDTPrintPre.Rows[p_iRowIndexPre]["SUB_BNK_ACC"].ToString() );
						if( iTempAuto < iTemp )
							bReturnValue = true ;
						break ;					
                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                    case ORDER_BY_ORG_LEVEL:
                        //Debabrata Biswas MITS#14804
                        if (p_objDTPrintPreAuto.Columns.Contains("ORG_ABBR"))
                        {
                            sTempAuto = p_objDTPrintPreAuto.Rows[p_iRowIndexPreAuto]["ORG_ABBR"].ToString();
                            sTemp = p_objDTPrintPre.Rows[p_iRowIndexPre]["ORG_ABBR"].ToString();
                            if (string.Compare(sTempAuto, sTemp) < 0)
                                bReturnValue = true;
                        }
                        break;
                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("CheckRegister.PrintFromAutoRecords.Error", m_iClientId), p_objEx);				
			}
			return( bReturnValue );
		}

		/// Name		: CheckPrintSQLPreCheck
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Build the SQL for Pre Checks.
		/// </summary>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_bIncludeAutoPays">Include Auto Checks</param>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_sOrderByField">Order By string </param>
		/// <param name="p_bCountOnly">Count Only Flag</param>
		/// <param name="p_bWithAttachedClaimsOnly">Attached Claim Only Flag</param>
		/// <returns>Returns the SQL string for PreChecks</returns>
		// akaushik5 Changed for MITS 35846 Starts
//        private string CheckPrintSQLPreCheck( string p_sFromDate, string p_sToDate, bool p_bIncludeAutoPays , 
//            int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel, string p_sOrderByField, bool p_bCountOnly, bool p_bWithAttachedClaimsOnly,bool p_bIncludeCombPay)
//        {
//            StringBuilder sbSQL = null;	
//            Functions objFunctions = null ;
//            //changed by nadim for 13685
//            //nadim 13685
			
//            string sAlias = "" ;
//            string sTemp = "" ;
//            //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//            string sOrgLevelField = "";
//            int iTableId = 0;
//            int iDeptId = 0;
//            string sOrgLevelFieldValue = "";
//            string sOrgLevelFieldList = "";
//            string sDeptIds = "";
//            DbReader objReader = null;
//            //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010

//            try
//            {
//                //nadim for 13685
//                if ( Functions.m_sDBType == Constants.DB_ACCESS )
//                    sAlias = " AS " ;
//                else
//                    sAlias = " " ;
				
//                objFunctions = new Functions( m_objDataModelFactory );
//                sbSQL = new StringBuilder();

//                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
               
//                switch (p_iOrgHierarchyLevel)
//                {
//                    case 1005: sOrgLevelField = "CLIENT_EID"; break;
//                    case 1006: sOrgLevelField = "COMPANY_EID"; break;
//                    case 1007: sOrgLevelField = "OPERATION_EID"; break;
//                    case 1008: sOrgLevelField = "REGION_EID"; break;
//                    case 1009: sOrgLevelField = "DIVISION_EID"; break;
//                    case 1010: sOrgLevelField = "LOCATION_EID"; break;
//                    case 1011: sOrgLevelField = "FACILITY_EID"; break;
//                    case 1012: sOrgLevelField = "DEPARTMENT_EID"; break;
//                }

//                if (p_sOrgHierarchy != "")
//                {
//                    //PJS MITS 21458 - Here space is the delimiter not ,
//                    string[] arrOrgList = p_sOrgHierarchy.Split(' ');

//                    foreach (string sEntityID in arrOrgList)
//                    {
//                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + sEntityID + ")");
//                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
//                        if (objReader != null)
//                        {
//                            while (objReader.Read())
//                            {
//                                iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);

//                            }
//                            objReader.Close();
//                        }
//                        switch (iTableId)
//                        {
//                            case 1005: sOrgLevelFieldValue = "CLIENT_EID=" + sEntityID + " "; break;
//                            case 1006: sOrgLevelFieldValue = "COMPANY_EID=" + sEntityID + " "; break;
//                            case 1007: sOrgLevelFieldValue = "OPERATION_EID=" + sEntityID + " "; break;
//                            case 1008: sOrgLevelFieldValue = "REGION_EID=" + sEntityID + " "; break;
//                            case 1009: sOrgLevelFieldValue = "DIVISION_EID=" + sEntityID + " "; break;
//                            case 1010: sOrgLevelFieldValue = "LOCATION_EID=" + sEntityID + " "; break;
//                            case 1011: sOrgLevelFieldValue = "FACILITY_EID=" + sEntityID + " "; break;
//                            case 1012: sOrgLevelFieldValue = "DEPARTMENT_EID=" + sEntityID + " "; break;

//                        }
//                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + "OR ";
//                        sbSQL.Remove(0, sbSQL.Length);

//                    }
//                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0, sOrgLevelFieldList.Length - 3);
//                    sbSQL.Append("SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (" + sOrgLevelFieldList + ")");
//                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
//                    if (objReader != null)
//                    {
//                        while (objReader.Read())
//                        {
//                            iDeptId = Common.Conversion.ConvertObjToInt(objReader.GetValue("DEPARTMENT_EID"), m_iClientId);
//                            sDeptIds = iDeptId + "," + sDeptIds;
//                        }
//                        objReader.Close();
//                    }
//                    sbSQL.Remove(0, sbSQL.Length);
//                    sDeptIds = sDeptIds.Substring(0, sDeptIds.Length - 1);
//                }
//                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                if( p_bCountOnly )
//                {
//                    sbSQL.Append( " SELECT FUNDS.TRANS_ID " + sAlias + " FUNDS_TRANS_ID, " );
//                    sbSQL.Append( " FUNDS_TRANS_SPLIT.SPLIT_ROW_ID" );
//                    sbSQL.Append( " ,FUNDS.AMOUNT " + sAlias + " FUNDS_AMOUNT, FUNDS.CLAIM_ID" );
//                }
//                else
//                {
//                    sbSQL.Append( " SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_DATE, FUNDS.CLAIM_NUMBER, ");
//                    sbSQL.Append( " FUNDS.PAYEE_TYPE_CODE," );
//                    sbSQL.Append( " FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.CLAIM_ID," );
//                    sbSQL.Append( " FUNDS.AMOUNT " + sAlias + " FUNDS_AMOUNT," );
//                    sbSQL.Append( " FUNDS.PAYEE_EID," );
//                    sbSQL.Append( " FUNDS.TRANS_ID " + sAlias + " FUNDS_TRANS_ID," );
//                    sbSQL.Append( " FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE," );
//                    sbSQL.Append( " FUNDS_TRANS_SPLIT.AMOUNT, FUNDS.AUTO_CHECK_FLAG " );		 
//                }

//                if( p_sOrderByField == "BANK_ACC_SUB" )
//                    sbSQL.Append( ", FUNDS.ACCOUNT_ID, FUNDS.SUB_ACCOUNT_ID " + sAlias + " SUB_BNK_ACC" );

//// JP 05.15.2006   Removed use of Oracle functions. No longer needed in 9i, 10g.
////				if( Functions.m_sDBType == Constants.DB_ORACLE )
////				{
////					objFunctions.OracleFunctions(1);
////					sbSQL.Append( " ,getFrzFlag(FUNDS.CLAIM_ID) " + sAlias + " PAYMNT_FROZEN_FLAG" );
////					sbSQL.Append( ",getClaimType(FUNDS.CLAIM_ID)" + sAlias + " CLAIM_TYPE_CODE" );
////					sbSQL.Append( ",getLOBCode(FUNDS.CLAIM_ID)" + sAlias + " LINE_OF_BUS_CODE" );
////					sbSQL.Append( " ,getFrzPayee(FUNDS.PAYEE_EID) " + sAlias + " FREEZE_PAYMENTS" );
////				}
////				else
////				{
//                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                if (p_iOrgHierarchyLevel != 1012)//Debabrata Biswas Mits# 14805
//                {
//                    sbSQL.Append(",(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS.CLAIM_ID)) AS ORG_LEVEL ");
//                }
//                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                //Debabrata Biswas Mits# 14805
//                //sbSQL.Append(",(SELECT ABBREVIATION FROM ENTITY WHERE ENTITY_ID=(SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS.CLAIM_ID))) AS ORG_ABBR ");    
//                sbSQL.Append(",(SELECT ABBREVIATION FROM ENTITY WHERE ENTITY_ID=(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS.CLAIM_ID))) AS ORG_ABBR ");    
//                //Debabrata Biswas End
//                    sbSQL.Append( " ,(SELECT PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID) " ); 
//                    sbSQL.Append( sAlias + " PAYMNT_FROZEN_FLAG , " );
//                    sbSQL.Append( " (SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE " );
//                    sbSQL.Append( " CLAIM.CLAIM_ID = FUNDS.CLAIM_ID)" + sAlias + " CLAIM_TYPE_CODE" );
//                    sbSQL.Append( ",(SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS.CLAIM_ID)"  );
//                    sbSQL.Append( sAlias + " LINE_OF_BUS_CODE ," );
//                    sbSQL.Append( " (SELECT FREEZE_PAYMENTS FROM ENTITY WHERE " );
//                    sbSQL.Append( " ENTITY.ENTITY_ID = FUNDS.PAYEE_EID) " + sAlias + " FREEZE_PAYMENTS"  );
				
//                    if( p_sOrderByField == "ADJUSTER_EID" )
//                    {
//                        sbSQL.Append( ",(SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER,CLAIM WHERE " );
//                        sbSQL.Append( " CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID AND " );
//                        sbSQL.Append( " CLAIM_ADJUSTER.CURRENT_ADJ_FLAG <> 0 AND " );
//                        sbSQL.Append( " CLAIM.CLAIM_ID = FUNDS.CLAIM_ID)" + sAlias + " ADJUSTER_EID" );
//                    }				        
////				}

//                sbSQL.Append( " FROM FUNDS, FUNDS_TRANS_SPLIT" );
//                sbSQL.Append( " WHERE FUNDS.STATUS_CODE = " + m_objCCacheFunctions.GetCodeIDWithShort( "R" , "CHECK_STATUS" ) );

//                if( p_sToDate != "" )
//                    sbSQL.Append( " AND FUNDS.TRANS_DATE <= '" + p_sToDate + "'" );

//                if( p_sFromDate != "" )
//                    sbSQL.Append( " AND FUNDS.TRANS_DATE >= '" + p_sFromDate + "'" ) ;

//                sbSQL.Append( " AND FUNDS.ACCOUNT_ID = " + p_iAccountId );
//                sbSQL.Append( " AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID AND FUNDS.PAYMENT_FLAG <> 0" ) ;

//                if( !p_bIncludeAutoPays )
//                    sbSQL.Append( " AND FUNDS.AUTO_CHECK_FLAG = 0" );
//                //skhare7 R8 Combined Payment
//                if (!p_bIncludeCombPay)
//                    sbSQL.Append(" AND FUNDS.COMBINED_PAY_FLAG = 0");
//                else
//                {
//                    sbSQL.Append(" AND FUNDS.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID    ");


                 
//                    if (p_sToDate != "")
//                        sbSQL.Append(" AND NEXT_SCH_PRINT > '" + p_sToDate + "'");//skhare7  MITS 31249
//                    if (p_sFromDate != "")
//                        sbSQL.Append(" AND NEXT_SCH_PRINT < '" + p_sFromDate + "'");
//                    if (p_sToDate != "" || (p_sFromDate != ""))
//                        sbSQL.Append(" )");
//                }

//                sbSQL.Append( " AND FUNDS.PRECHECK_FLAG = 0 AND FUNDS.VOID_FLAG = 0 " );
//                //changed by nadim for 13685
//                if (Functions.AllowZeroPayments())
//                {
//                    sbSQL.Append(" AND FUNDS.AMOUNT >= 0");
//                }
//                else
//                {
//                    sbSQL.Append(" AND FUNDS.AMOUNT > 0");
//                }
//                //nadim for 13685
//                if( p_bWithAttachedClaimsOnly )
//                    sbSQL.Append( " AND FUNDS.CLAIM_ID <> 0" );
//                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                if (p_sOrgHierarchy != "")
//                    sbSQL.Append(" AND CLAIM_ID IN (SELECT CLAIM_ID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND EV.DEPT_EID IN (" + sDeptIds + "))");
//                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                sbSQL.Append( " ORDER BY " );
//                //Added Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                if (p_sOrderByField == "ORG_LEVEL")
//                    p_sOrderByField = "ORG_ABBR";
//                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                if( !p_bCountOnly && p_sOrderByField != "" && p_sOrderByField != "PAYER_LEVEL" && p_sOrderByField != "BANK_ACC_SUB" )
//                    sbSQL.Append( p_sOrderByField + "," );

//                if( p_sOrderByField == "BANK_ACC_SUB" )
//                    sbSQL.Append( "FUNDS.ACCOUNT_ID, FUNDS.SUB_ACCOUNT_ID," );

//                sbSQL.Append( "FUNDS.TRANS_ID,FUNDS_TRANS_SPLIT.SPLIT_ROW_ID" );
				
//                sTemp = sbSQL.ToString();
//            }
//            catch( RMAppException p_objEx )
//            {
//                throw p_objEx ;
//            }
//            catch( Exception p_objEx )
//            {
//                throw new RMAppException(Globalization.GetString("CheckRegister.CheckPrintSQLPreCheck.SqlBuildError", m_iClientId), p_objEx);
//            }		
//            finally
//            {
//                //nadim for 13685
//                objFunctions = null;                    
//                sbSQL = null ;
//            }
//            return( sTemp );
//        }
        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        private string CheckPrintSQLPreCheck(string p_sFromDate, string p_sToDate, bool p_bIncludeAutoPays,
            int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel, string p_sOrderByField, bool p_bCountOnly, bool p_bWithAttachedClaimsOnly, bool p_bIncludeCombPay, int p_iDistributionType)
        // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        {
            StringBuilder sbSQL = null;
            string sAlias = "";
            string sTemp = "";
            string sOrgLevelField = "";
            int iTableId = 0;
            string sOrgLevelFieldValue = "";
            string sOrgLevelFieldList = "";
            DbReader objReader = null;

            try
            {
                sAlias = Functions.m_sDBType == Constants.DB_ACCESS ? "AS" : string.Empty;
                sbSQL = new StringBuilder();
                sOrgLevelField = CommonFunctions.GetOrgLevelFieldFromCodeId(p_iOrgHierarchyLevel);

                if (p_sOrgHierarchy != "")
                {
                    string[] arrOrgList = p_sOrgHierarchy.Split(' ');

                    foreach (string sEntityID in arrOrgList)
                    {
                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + sEntityID + ")");
                        using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                        {
                            if (objReader != null)
                            {
                                while (objReader.Read())
                                {
                                    iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);
                                }
                                objReader.Close();
                            }
                        }

                        sOrgLevelFieldValue = string.Format("ORG_HIERARCHY.{0} = {1} ", CommonFunctions.GetOrgLevelFieldFromCodeId(iTableId), sEntityID);
                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + "OR ";
                        sbSQL.Remove(0, sbSQL.Length);
                    }

                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0, sOrgLevelFieldList.Length - 3);
                }
                if (p_bCountOnly)
                {
                    sbSQL.AppendFormat(" SELECT FUNDS.TRANS_ID {0} FUNDS_TRANS_ID, ", sAlias);
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID");
                    sbSQL.AppendFormat(" ,FUNDS.AMOUNT {0} FUNDS_AMOUNT, FUNDS.CLAIM_ID", sAlias);
                }
                else
                {
                    sbSQL.Append(" SELECT FUNDS.CTL_NUMBER, FUNDS.TRANS_DATE, FUNDS.CLAIM_NUMBER, ");
                    sbSQL.Append(" FUNDS.PAYEE_TYPE_CODE,");
                    sbSQL.Append(" FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.CLAIM_ID,");
                    sbSQL.AppendFormat(" FUNDS.AMOUNT {0} FUNDS_AMOUNT,", sAlias );
                    sbSQL.Append(" FUNDS.PAYEE_EID,");
                    sbSQL.AppendFormat(" FUNDS.TRANS_ID {0} FUNDS_TRANS_ID,", sAlias);
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,");
                    sbSQL.Append(" FUNDS_TRANS_SPLIT.AMOUNT, FUNDS.AUTO_CHECK_FLAG ");
                    //tanwar2 - mits 30910 - start
                    sbSQL.Append(", FUNDS_TRANS_SPLIT.POLCVG_ROW_ID, ");
                    sbSQL.Append(" CXD.POLICY_ID, CXD.DED_TYPE_CODE, CXD.DIMNSHNG_TYPE_CODE, CXD.POLICY_UNIT_ROW_ID ");
                    //tanwar2 - mits 30910 - end
                }

                if (p_sOrderByField == "BANK_ACC_SUB")
                {
                    // zmohammad Added for MITS 38364 Starts
                    //sbSQL.AppendFormat(", FUNDS.ACCOUNT_ID, FUNDS.SUB_ACCOUNT_ID {0} SUB_BNK_ACC", sAlias);
                    sbSQL.AppendFormat(", FUNDS.ACCOUNT_ID, FUNDS.SUB_ACCOUNT_ID {0} SUB_BNK_ACC, BANK_ACC_SUB.SUB_ACC_NAME", sAlias);
                    // zmohammad Added for MITS 38364 Ends
                }

                if (!string.IsNullOrEmpty(sOrgLevelField) && p_iOrgHierarchyLevel != 1012)
                {
                    sbSQL.AppendFormat(", ORG_HIERARCHY.{0} {1} ORG_LEVEL", sOrgLevelField, sAlias);
                }

                if (!string.IsNullOrEmpty(sOrgLevelField))
                {
                    sbSQL.Append(", ORGENTITY.ABBREVIATION AS ORG_ABBR ");
                }
                
                sbSQL.AppendFormat(", CLAIM.PAYMNT_FROZEN_FLAG {0} PAYMNT_FROZEN_FLAG", sAlias);
                sbSQL.AppendFormat(", CLAIM.CLAIM_TYPE_CODE {0} CLAIM_TYPE_CODE", sAlias);
                sbSQL.AppendFormat(", CLAIM.LINE_OF_BUS_CODE {0} LINE_OF_BUS_CODE", sAlias);
                sbSQL.AppendFormat(", ENTITY.FREEZE_PAYMENTS {0} FREEZE_PAYMENTS", sAlias);

                if (p_sOrderByField == "ADJUSTER_EID")
                {
                    sbSQL.AppendFormat(", CLAIM_ADJUSTER.ADJUSTER_EID {0} ADJUSTER_EID", sAlias);
                }

                sbSQL.Append(" FROM FUNDS JOIN FUNDS_TRANS_SPLIT ON FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID ");
                sbSQL.Append(" LEFT OUTER JOIN CLAIM ON CLAIM.CLAIM_ID = FUNDS.CLAIM_ID ");
                sbSQL.Append(" JOIN ENTITY ON ENTITY.ENTITY_ID = FUNDS.PAYEE_EID ");
                sbSQL.Append(" LEFT OUTER JOIN EVENT ON CLAIM.EVENT_ID = EVENT.EVENT_ID");
                sbSQL.Append(" LEFT OUTER JOIN ORG_HIERARCHY ON ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID");
                // zmohammad Added for MITS 38364 Starts
                if (!string.IsNullOrEmpty(p_sOrderByField) && p_sOrderByField == "BANK_ACC_SUB")
                {
                    sbSQL.Append(" LEFT OUTER JOIN BANK_ACC_SUB ON BANK_ACC_SUB.SUB_ROW_ID = FUNDS.SUB_ACCOUNT_ID ");
                }
                // zmohammad Added for MITS 38364 Ends

                if (!string.IsNullOrEmpty(sOrgLevelField))
                {
                    sbSQL.AppendFormat(" LEFT OUTER JOIN ENTITY ORGENTITY ON ORGENTITY.ENTITY_ID = ORG_HIERARCHY.{0}", sOrgLevelField);
                }

                if (p_sOrderByField == "ADJUSTER_EID")
                {
                    sbSQL.Append(" LEFT OUTER JOIN CLAIM_ADJUSTER ON CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID ");
                }
                
                //tanwar2 - mits 30910 - start
                sbSQL.Append(" LEFT OUTER JOIN CLAIM_X_POL_DED CXD ");
                sbSQL.Append(" ON CXD.CLAIM_ID=FUNDS.CLAIM_ID AND CXD.POLCVG_ROW_ID=FUNDS_TRANS_SPLIT.POLCVG_ROW_ID ");
                //tanwar2 - mits 30910 - end
                sbSQL.Append(" WHERE FUNDS.STATUS_CODE = " + m_objCCacheFunctions.GetCodeIDWithShort("R", "CHECK_STATUS"));
                sbSQL.Append(" AND (CLAIM.PAYMNT_FROZEN_FLAG <> -1 OR CLAIM.PAYMNT_FROZEN_FLAG IS NULL)");
                sbSQL.Append(" AND (ENTITY.FREEZE_PAYMENTS <> -1 OR ENTITY.FREEZE_PAYMENTS IS NULL) ");
                sbSQL.Append(" AND FUNDS.PRECHECK_FLAG = 0 AND FUNDS.VOID_FLAG = 0 ");
                sbSQL.AppendFormat(" AND FUNDS.ACCOUNT_ID = {0}", p_iAccountId);
                sbSQL.Append(" AND FUNDS.PAYMENT_FLAG <> 0");

                if (p_sOrderByField == "ADJUSTER_EID")
                {
                    sbSQL.Append(" AND CLAIM_ADJUSTER.CURRENT_ADJ_FLAG <> 0 ");
                }

                if (!string.IsNullOrEmpty(p_sToDate))
                {
                    sbSQL.AppendFormat(" AND FUNDS.TRANS_DATE <= '{0}'", p_sToDate);
                }

                if (!string.IsNullOrEmpty(p_sFromDate))
                {
                    sbSQL.AppendFormat(" AND FUNDS.TRANS_DATE >= '{0}'", p_sFromDate);
                }

                if (!p_bIncludeAutoPays)
                {
                    sbSQL.Append(" AND FUNDS.AUTO_CHECK_FLAG = 0");
                }

                if (!p_bIncludeCombPay)
                {
                    sbSQL.Append(" AND FUNDS.COMBINED_PAY_FLAG = 0");
                }
                else
                {
                    // akaushik5 Added for MITS 36616 Starts
                    if (!string.IsNullOrEmpty(p_sToDate) || !string.IsNullOrEmpty(p_sFromDate))
                    {
                        // akaushik5 Added for MITS 36616 Ends
                        sbSQL.Append(" AND FUNDS.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID ");

                        if (!string.IsNullOrEmpty(p_sToDate))
                        {
                            sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT > '{0}'", p_sToDate);
                        }

                        if (!string.IsNullOrEmpty(p_sFromDate))
                        {
                            sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT < '{0}'", p_sFromDate);
                        }

                        sbSQL.Append(" )");
                    }
                }

                // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //if (p_bPrintEFTPayment)
                //{
                //    sbSQL.Append(" AND FUNDS.EFT_FLAG = -1");
                //}
                //else
                //{
				     // akaushik5 Changed for MITS 37841 Starts
                    //sbSQL.Append(" AND FUNDS.EFT_FLAG <> -1");
                    //sbSQL.Append(" AND (FUNDS.EFT_FLAG <> -1 OR FUNDS.EFT_FLAG IS NULL)");
                    // akaushik5 Changed for MITS 37841 Ends

                //}
                //JIRA:438 End: 

                sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                if (Functions.AllowZeroPayments(m_sConnectionString, m_iClientId))
                {
                    sbSQL.Append(" AND FUNDS.AMOUNT >= 0");
                }
                else
                {
                    sbSQL.Append(" AND FUNDS.AMOUNT > 0");
                }

                if (p_bWithAttachedClaimsOnly)
                {
                    sbSQL.Append(" AND FUNDS.CLAIM_ID <> 0");
                }

                if (!string.IsNullOrEmpty(p_sOrgHierarchy))
                {
                    sbSQL.AppendFormat("AND ({0})", sOrgLevelFieldList);
                }

                sbSQL.Append(" ORDER BY ");
                if (p_sOrderByField == "ORG_LEVEL")
                {
                    p_sOrderByField = "ORG_ABBR";
                }

                if (!p_bCountOnly && p_sOrderByField != "" && p_sOrderByField != "PAYER_LEVEL" && p_sOrderByField != "BANK_ACC_SUB")
                {
                    sbSQL.Append(p_sOrderByField + ",");
                }

                if (p_sOrderByField == "BANK_ACC_SUB")
                {
                    //sbSQL.Append("FUNDS.ACCOUNT_ID, FUNDS.SUB_ACCOUNT_ID,");
                    sbSQL.Append(" BANK_ACC_SUB.SUB_ACC_NAME,");  // zmohammad Added for MITS 38364
                }

                sbSQL.Append("FUNDS.TRANS_ID,FUNDS_TRANS_SPLIT.SPLIT_ROW_ID");

                sTemp = sbSQL.ToString();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckRegister.CheckPrintSQLPreCheck.SqlBuildError", m_iClientId), p_objEx);
            }
            finally
            {
                sbSQL = null;
            }
            return (sTemp);
        }
		// akaushik5 Changed for MITS 35846 Ends

		/// Name		: CheckPrintSQLPreCheckAuto
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Build the SQL for Pre Auto Checks.
		/// </summary>
		/// <param name="p_sFromDate">From Date</param>
		/// <param name="p_sToDate">To Date</param>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_sOrderByField">Order By string </param>
		/// <param name="p_bCountOnly">Count Only Flag</param>
		/// <param name="p_bScreenTaxPayee">Screen Tax Payee Flag</param>
		/// <returns>Returns the SQL string for Auto PreChecks</returns>
        // akaushik5 Changed for MITS 35846 Starts
//        private string CheckPrintSQLPreCheckAuto(string p_sFromDate, string p_sToDate, int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel,
//            string p_sOrderByField, bool p_bCountOnly, bool p_bScreenTaxPayee, bool p_bIncludeCombPay)
//        {
//            Functions objFunctions = null ;
//            StringBuilder sbSQL = null;

//            string sAlias = "" ;
//            string sTemp = "" ;
//            string sSQL = "" ;
//            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//            int iTableId = 0;
//            int iDeptId = 0;
//            string sOrgLevelField = string.Empty;
//            string sOrgLevelFieldValue = string.Empty;
//            string sOrgLevelFieldList = string.Empty ;
//            string sDeptIds = "";
//            DbReader objReader = null;
//            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//            try
//            {
//                if ( Functions.m_sDBType == Constants.DB_ACCESS )
//                    sAlias = " AS " ;
//                else
//                    sAlias = " " ;
				
//                objFunctions = new Functions( m_objDataModelFactory );
//                sbSQL = new StringBuilder();

//                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                
//                switch (p_iOrgHierarchyLevel)
//                {
//                    case 1005: sOrgLevelField = "CLIENT_EID"; break;
//                    case 1006: sOrgLevelField = "COMPANY_EID"; break;
//                    case 1007: sOrgLevelField = "OPERATION_EID"; break;
//                    case 1008: sOrgLevelField = "REGION_EID"; break;
//                    case 1009: sOrgLevelField = "DIVISION_EID"; break;
//                    case 1010: sOrgLevelField = "LOCATION_EID"; break;
//                    case 1011: sOrgLevelField = "FACILITY_EID"; break;
//                    case 1012: sOrgLevelField = "DEPARTMENT_EID"; break;

//                }


//                if (p_sOrgHierarchy != "")
//                {
//                    //PJS MITS 21458 - Here space is the delimiter not ,
//                    string[] arrOrgList = p_sOrgHierarchy.Split(' ');

//                    foreach (string ss in arrOrgList)
//                    {
//                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + ss + ")");
//                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
//                        if (objReader != null)
//                        {
//                            while (objReader.Read())
//                            {
//                                iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);

//                            }
//                            objReader.Close();
//                        }
//                        switch (iTableId)
//                        {
//                            case 1005: sOrgLevelFieldValue = "CLIENT_EID=" + ss + " "; break;
//                            case 1006: sOrgLevelFieldValue = "COMPANY_EID=" + ss + " "; break;
//                            case 1007: sOrgLevelFieldValue = "OPERATION_EID=" + ss + " "; break;
//                            case 1008: sOrgLevelFieldValue = "REGION_EID=" + ss + " "; break;
//                            case 1009: sOrgLevelFieldValue = "DIVISION_EID=" + ss + " "; break;
//                            case 1010: sOrgLevelFieldValue = "LOCATION_EID=" + ss + " "; break;
//                            case 1011: sOrgLevelFieldValue = "FACILITY_EID=" + ss + " "; break;
//                            case 1012: sOrgLevelFieldValue = "DEPARTMENT_EID=" + ss + " "; break;

//                        }
//                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + "OR ";
//                        sbSQL.Remove(0, sbSQL.Length);

//                    }
//                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0, sOrgLevelFieldList.Length - 3);
//                    sbSQL.Append("SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE (" + sOrgLevelFieldList + ")");
//                    objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
//                    if (objReader != null)
//                    {
//                        while (objReader.Read())
//                        {
//                            iDeptId = Common.Conversion.ConvertObjToInt(objReader.GetValue("DEPARTMENT_EID"), m_iClientId);
//                            sDeptIds = iDeptId + "," + sDeptIds;
//                        }
//                        objReader.Close();
//                    }
//                    sbSQL.Remove(0, sbSQL.Length);
//                    sDeptIds = sDeptIds.Substring(0, sDeptIds.Length - 1);
//                }

//                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                if( p_bCountOnly )
//                {
//                    sbSQL.Append( " SELECT FUNDS_AUTO.AUTO_TRANS_ID " + sAlias );
//                    sbSQL.Append( " FUNDS_TRANS_ID, FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID" );
//                    sbSQL.Append( " ,FUNDS_AUTO.AMOUNT " + sAlias + " FUNDS_AMOUNT, FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG" ) ;
//                }
//                else
//                {
//                    sbSQL.Append( "SELECT FUNDS_AUTO.CTL_NUMBER, FUNDS_AUTO.PRINT_DATE, FUNDS_AUTO.CLAIM_NUMBER, " );
//                    sbSQL.Append( " FUNDS_AUTO.PAYEE_TYPE_CODE," );
//                    sbSQL.Append( " FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, FUNDS_AUTO.CLAIM_ID," );
//                    sbSQL.Append( " FUNDS_AUTO.AMOUNT " + sAlias + " FUNDS_AMOUNT," );
//                    sbSQL.Append( " FUNDS_AUTO.PAYEE_EID," );
//                    sbSQL.Append( " FUNDS_AUTO.AUTO_TRANS_ID " + sAlias + " FUNDS_TRANS_ID," );
//                    sbSQL.Append( " FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID, FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE," );
//                    sbSQL.Append( " FUNDS_AUTO_SPLIT.AMOUNT," );
//                    sbSQL.Append( " FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG" );		 
//                }

//                if( p_sOrderByField == "BANK_ACC_SUB" )
//                    sbSQL.Append( ", FUNDS_AUTO.ACCOUNT_ID, FUNDS_AUTO.SUB_ACC_ID " + sAlias + " SUB_BNK_ACC" );

//// JP 05.15.2006   Removed use of Oracle functions. No longer needed in 9i, 10g.
////				if( Functions.m_sDBType == Constants.DB_ORACLE )
////				{
////					objFunctions.OracleFunctions(1);
////					sbSQL.Append( " ,getFrzFlag(FUNDS_AUTO.CLAIM_ID) " + sAlias + " PAYMNT_FROZEN_FLAG" );
////					sbSQL.Append( " ,getClaimType(FUNDS_AUTO.CLAIM_ID)" + sAlias + " CLAIM_TYPE_CODE" );
////					sbSQL.Append( " ,getLOBCode(FUNDS_AUTO.CLAIM_ID)" + sAlias + " LINE_OF_BUS_CODE" );
////					sbSQL.Append( " ,getFrzPayee(FUNDS_AUTO.PAYEE_EID) " + sAlias + " FREEZE_PAYMENTS" );
////				}
////				else
////				{
//                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                    if (p_iOrgHierarchyLevel != 1012)//Debabrata Biswas Mits# 14805
//                    {
//                        sbSQL.Append(",(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)) AS ORG_LEVEL ");
//                    }
//                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                    //Debabrata Biswas Mits# 14805
//                    sbSQL.Append(",(SELECT ABBREVIATION FROM ENTITY WHERE ENTITY_ID=(SELECT " + sOrgLevelField + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID IN (SELECT DEPT_EID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND CL.CLAIM_ID = FUNDS_AUTO.CLAIM_ID))) AS ORG_ABBR ");
//                    //Debabrata Biswas End
//                    sbSQL.Append( " ,(SELECT PAYMNT_FROZEN_FLAG FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID) "  );
//                    sbSQL.Append( sAlias + " PAYMNT_FROZEN_FLAG" );
//                    sbSQL.Append( ",(SELECT CLAIM_TYPE_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)" );
//                    sbSQL.Append( sAlias + " CLAIM_TYPE_CODE" );
//                    sbSQL.Append( ",(SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)"  );
//                    sbSQL.Append( sAlias + " LINE_OF_BUS_CODE" );
//                    sbSQL.Append( " ,(SELECT FREEZE_PAYMENTS FROM ENTITY WHERE ENTITY.ENTITY_ID = FUNDS_AUTO.PAYEE_EID) "  );
//                    sbSQL.Append( sAlias + " FREEZE_PAYMENTS" ) ;          
				
//                    if( p_sOrderByField == "ADJUSTER_EID" )
//                    {
//                        sbSQL.Append( ",(SELECT ADJUSTER_EID FROM CLAIM_ADJUSTER,CLAIM WHERE " );
//                        sbSQL.Append( " CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID " );
//                        sbSQL.Append( " AND CLAIM_ADJUSTER.CURRENT_ADJ_FLAG <> 0 " );
//                        sbSQL.Append( " AND CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID)" +sAlias + " ADJUSTER_EID" );
//                    }				        
////				}

//                sbSQL.Append( " FROM FUNDS_AUTO, FUNDS_AUTO_SPLIT, FUNDS_AUTO_BATCH" );
//                sbSQL.Append( " WHERE FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID " );
//                sbSQL.Append( " AND FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID" );
//                sbSQL.Append( " AND FUNDS_AUTO.ACCOUNT_ID = " + p_iAccountId );
//                sbSQL.Append( " AND ((FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1) " );
//                sbSQL.Append( " OR (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL))" );

//                if( p_sToDate != "" )
//                    sbSQL.Append( " AND FUNDS_AUTO.PRINT_DATE <= '" + p_sToDate + "'" );

//                if( p_sFromDate != "" )
//                    sbSQL.Append( " AND FUNDS_AUTO.PRINT_DATE >= '" + p_sFromDate + "'" ) ;

//                sbSQL.Append( " AND FUNDS_AUTO.PRECHECK_FLAG = 0" );
//                //sbSQL.Append( " AND FUNDS_AUTO.AMOUNT > 0" );
//                sbSQL.Append(" AND FUNDS_AUTO.AMOUNT >= 0");   //pmittal5 Mits 14253 01/16/09 - Include Zero Payments

//                //skhare7 R8 Combined Payment
//                if (!p_bIncludeCombPay)
//                    sbSQL.Append(" AND FUNDS_AUTO.COMBINED_PAY_FLAG = 0");
//                else
//                {
//                    sbSQL.Append(" AND FUNDS_AUTO.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS_AUTO.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID    ");
                 
//                    if (p_sToDate != "")
//                        sbSQL.Append(" AND NEXT_SCH_PRINT > '" + p_sToDate + "'");
//                    if (p_sFromDate != "")
//                        sbSQL.Append(" AND NEXT_SCH_PRINT < '" + p_sFromDate + "'");
//                    if (p_sToDate != "" || (p_sFromDate != ""))
//                        sbSQL.Append(" )");
//                }
//                if( m_objSysSettings.UseFundsSubAcc )
//                {
//                    sbSQL.Append( " AND FUNDS_AUTO.STATUS_CODE <> " );
//                    sbSQL.Append( m_objCCacheFunctions.GetCodeIDWithShort( "I" , "CHECK_STATUS" ) );
//                }
				
//                if( p_bScreenTaxPayee )
//                    sbSQL.Append( " AND FUNDS_AUTO.PAYEE_EID NOT IN (SELECT TAX_EID FROM TAX_MAPPING)" );
//                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                if (p_sOrgHierarchy != "")
//                    sbSQL.Append(" AND CLAIM_ID IN (SELECT CLAIM_ID FROM EVENT EV, CLAIM CL WHERE EV.EVENT_ID = CL.EVENT_ID AND EV.DEPT_EID IN (" + sDeptIds + "))");
//                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                sbSQL.Append( " ORDER BY " );

//                if( !p_bCountOnly && p_sOrderByField != "" )
//                {
//                    switch( p_sOrderByField.Trim() )
//                    {
//                        case ORDER_BY_AMOUNT :
//                            sTemp = "FUNDS_AUTO.AMOUNT" ;
//                            break ;
//                        case ORDER_BY_CLAIM :
//                            sTemp = "FUNDS_AUTO.CLAIM_NUMBER" ;
//                            break;
//                        case ORDER_BY_CONTROL :
//                            sTemp = "FUNDS_AUTO.CTL_NUMBER" ;
//                            break;
//                        case ORDER_BY_NAME :
//                            sTemp = "FUNDS_AUTO.LAST_NAME,FUNDS_AUTO.FIRST_NAME" ;
//                            break;
//                        case ORDER_BY_TRANS_DATE :
//                            sTemp = "FUNDS_AUTO.PRINT_DATE" ;
//                            break;
//                        case ORDER_BY_ADJUSTER_EID :
//                            sTemp = "ADJUSTER_EID" ;
//                            break;
//                        case ORDER_BY_BANK_ACCOUNT_SUB :
//                            sTemp = "FUNDS_AUTO.SUB_ACC_ID" ;
//                            break;
//                        //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                        case ORDER_BY_ORG_LEVEL:
//                            sTemp = "ORG_LEVEL";
//                            break;
//                        //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                    }
//                    //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                    if (p_sOrderByField == "ORG_LEVEL")
//                        sTemp = "ORG_ABBR";
//                    //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
//                    if( sTemp != "" )
//                        sbSQL.Append( sTemp + "," );
//                }					

//                sbSQL.Append( "FUNDS_AUTO.AUTO_TRANS_ID,FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID" );

//                sSQL = sbSQL.ToString();
//            }
//            catch( RMAppException p_objEx )
//            {
//                throw p_objEx ;
//            }
//            catch( Exception p_objEx )
//            {
//                throw new RMAppException(Globalization.GetString("CheckRegister.CheckPrintSQLPreCheckAuto.SqlBuildError", m_iClientId), p_objEx);
//            }		
//            finally
//            {
//                objFunctions = null;
//                sbSQL = null ;
//            }
//            return( sSQL );
//        }
        // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        private string CheckPrintSQLPreCheckAuto(string p_sFromDate, string p_sToDate, int p_iAccountId, string p_sOrgHierarchy, int p_iOrgHierarchyLevel,
            string p_sOrderByField, bool p_bCountOnly, bool p_bScreenTaxPayee, bool p_bIncludeCombPay, int p_iDistributionType)//JIRA:438 START: ajohari2
        // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
        {
            Functions objFunctions = null;
            StringBuilder sbSQL = null;
            string sAlias = string.Empty;
            string sTemp = string.Empty;
            string sSQL = string.Empty;
            int iTableId = 0;
            string sOrgLevelField = string.Empty;
            string sOrgLevelFieldValue = string.Empty;
            string sOrgLevelFieldList = string.Empty;
            DbReader objReader = null;
            try
            {
                sAlias = Functions.m_sDBType == Constants.DB_ACCESS ? "AS" : string.Empty;
                
                objFunctions = new Functions(m_objDataModelFactory);
                sbSQL = new StringBuilder();
                sOrgLevelField = CommonFunctions.GetOrgLevelFieldFromCodeId(p_iOrgHierarchyLevel);

                if (p_sOrgHierarchy != "")
                {
                    string[] arrOrgList = p_sOrgHierarchy.Split(' ');

                    foreach (string sEntityID in arrOrgList)
                    {
                        sbSQL.Append("(SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + sEntityID + ")");
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
                        if (objReader != null)
                        {
                            while (objReader.Read())
                            {
                                iTableId = Common.Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_TABLE_ID"), m_iClientId);
                            }
                            objReader.Close();
                        }

                        sOrgLevelFieldValue = string.Format("ORG_HIERARCHY.{0} = {1} ", CommonFunctions.GetOrgLevelFieldFromCodeId(iTableId), sEntityID);
                        sOrgLevelFieldList = sOrgLevelFieldList + sOrgLevelFieldValue + "OR ";
                        sbSQL.Remove(0, sbSQL.Length);
                    }
                    sOrgLevelFieldList = sOrgLevelFieldList.Substring(0, sOrgLevelFieldList.Length - 3);
                }

                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                if (p_bCountOnly)
                {
                    sbSQL.AppendFormat("SELECT FUNDS_AUTO.AUTO_TRANS_ID {0} FUNDS_TRANS_ID", sAlias);
                    sbSQL.Append(", FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID");
                    sbSQL.AppendFormat(", FUNDS_AUTO.AMOUNT {0} FUNDS_AMOUNT, FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG", sAlias);
                }
                else
                {
                    sbSQL.Append("SELECT FUNDS_AUTO.CTL_NUMBER, FUNDS_AUTO.PRINT_DATE, FUNDS_AUTO.CLAIM_NUMBER");
                    sbSQL.Append(", FUNDS_AUTO.PAYEE_TYPE_CODE");
                    sbSQL.Append(", FUNDS_AUTO.FIRST_NAME, FUNDS_AUTO.LAST_NAME, FUNDS_AUTO.CLAIM_ID");
                    sbSQL.AppendFormat(", FUNDS_AUTO.AMOUNT {0} FUNDS_AMOUNT", sAlias);
                    sbSQL.Append(", FUNDS_AUTO.PAYEE_EID");
                    sbSQL.AppendFormat(", FUNDS_AUTO.AUTO_TRANS_ID {0} FUNDS_TRANS_ID", sAlias);
                    sbSQL.Append(", FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID, FUNDS_AUTO_SPLIT.TRANS_TYPE_CODE");
                    sbSQL.Append(", FUNDS_AUTO_SPLIT.AMOUNT");
                    sbSQL.Append(", FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG");
                    //tanwar2 - mits 30910 - start
                    sbSQL.Append(", FUNDS_AUTO_SPLIT.POLCVG_ROW_ID, ");
                    sbSQL.Append(" CXD.POLICY_ID, CXD.DED_TYPE_CODE, CXD.DIMNSHNG_TYPE_CODE, CXD.POLICY_UNIT_ROW_ID ");
                    //tanwar2 - mits 30910 - end
                }

                if (p_sOrderByField == "BANK_ACC_SUB")
                {
                    sbSQL.AppendFormat(", FUNDS_AUTO.ACCOUNT_ID, FUNDS_AUTO.SUB_ACC_ID {0} SUB_BNK_ACC", sAlias);
                }

                if (!string.IsNullOrEmpty(sOrgLevelField) && p_iOrgHierarchyLevel != 1012)
                {
                    sbSQL.AppendFormat(", ORG_HIERARCHY.{0} {1} ORG_LEVEL", sOrgLevelField, sAlias);
                }

                if (!string.IsNullOrEmpty(sOrgLevelField))
                {
                    sbSQL.Append(", ORGENTITY.ABBREVIATION AS ORG_ABBR ");
                }
                
                sbSQL.AppendFormat(", CLAIM.PAYMNT_FROZEN_FLAG {0} PAYMNT_FROZEN_FLAG", sAlias);
                sbSQL.AppendFormat(", CLAIM.CLAIM_TYPE_CODE {0} CLAIM_TYPE_CODE", sAlias);
                sbSQL.AppendFormat(", CLAIM.LINE_OF_BUS_CODE {0} LINE_OF_BUS_CODE", sAlias);
                sbSQL.AppendFormat(", ENTITY.FREEZE_PAYMENTS {0} FREEZE_PAYMENTS", sAlias);

                if (p_sOrderByField == "ADJUSTER_EID")
                {
                    sbSQL.AppendFormat(", CLAIM_ADJUSTER.ADJUSTER_EID {0} ADJUSTER_EID", sAlias);
                }

                sbSQL.Append(" FROM FUNDS_AUTO");
                sbSQL.Append(" JOIN FUNDS_AUTO_SPLIT ON FUNDS_AUTO.AUTO_TRANS_ID = FUNDS_AUTO_SPLIT.AUTO_TRANS_ID");
                sbSQL.Append(" JOIN FUNDS_AUTO_BATCH ON FUNDS_AUTO_BATCH.AUTO_BATCH_ID = FUNDS_AUTO.AUTO_BATCH_ID");
                //tanwar2 - mits 30910 - start
                sbSQL.Append(" LEFT OUTER JOIN CLAIM_X_POL_DED CXD ");
                sbSQL.Append(" ON CXD.CLAIM_ID=FUNDS_AUTO.CLAIM_ID AND CXD.POLCVG_ROW_ID=FUNDS_AUTO_SPLIT.POLCVG_ROW_ID ");
                //tanwar2 - mits 30910 - end
                sbSQL.Append(" LEFT OUTER JOIN CLAIM ON CLAIM.CLAIM_ID = FUNDS_AUTO.CLAIM_ID ");
                sbSQL.Append(" JOIN ENTITY ON ENTITY.ENTITY_ID = FUNDS_AUTO.PAYEE_EID ");
                sbSQL.Append(" LEFT OUTER JOIN EVENT ON CLAIM.EVENT_ID = EVENT.EVENT_ID");
                sbSQL.Append(" LEFT OUTER JOIN ORG_HIERARCHY ON ORG_HIERARCHY.DEPARTMENT_EID = EVENT.DEPT_EID");

                if (!string.IsNullOrEmpty(sOrgLevelField))
                {
                    sbSQL.AppendFormat(" LEFT OUTER JOIN ENTITY ORGENTITY ON ORGENTITY.ENTITY_ID = ORG_HIERARCHY.{0}", sOrgLevelField);
                }

                if (p_sOrderByField == "ADJUSTER_EID")
                {
                    sbSQL.Append(" LEFT OUTER JOIN CLAIM_ADJUSTER ON CLAIM.CLAIM_ID = CLAIM_ADJUSTER.CLAIM_ID ");
                }

                if (p_bScreenTaxPayee)
                {
                    sbSQL.Append(" LEFT JOIN TAX_MAPPING ON FUNDS_AUTO.PAYEE_EID = TAX_MAPPING.TAX_EID ");
                }

                sbSQL.Append(" WHERE (CLAIM.PAYMNT_FROZEN_FLAG <> -1 OR CLAIM.PAYMNT_FROZEN_FLAG IS NULL)");
                sbSQL.Append(" AND (ENTITY.FREEZE_PAYMENTS <> -1 OR ENTITY.FREEZE_PAYMENTS IS NULL) ");
                sbSQL.AppendFormat(" AND FUNDS_AUTO.ACCOUNT_ID = {0}", p_iAccountId);
                sbSQL.Append(" AND (FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG <> -1 OR FUNDS_AUTO_BATCH.FREEZE_BATCH_FLAG IS NULL)");
                sbSQL.Append(" AND FUNDS_AUTO.PRECHECK_FLAG = 0");
                sbSQL.Append(" AND FUNDS_AUTO.AMOUNT >= 0");

                if (!string.IsNullOrEmpty(p_sToDate))
                {
                    sbSQL.AppendFormat(" AND FUNDS_AUTO.PRINT_DATE <= '{0}'", p_sToDate);
                }

                if (!string.IsNullOrEmpty(p_sFromDate))
                {
                    sbSQL.AppendFormat(" AND FUNDS_AUTO.PRINT_DATE >= '{0}'", p_sFromDate);
                }
                
                if (p_sOrderByField == "ADJUSTER_EID")
                {
                    sbSQL.Append(" AND CLAIM_ADJUSTER.CURRENT_ADJ_FLAG <> 0 ");
                }

                if (!p_bIncludeCombPay)
                {
                    sbSQL.Append(" AND FUNDS_AUTO.COMBINED_PAY_FLAG = 0");
                }
                else
                {
                    sbSQL.Append(" AND FUNDS_AUTO.PAYEE_EID NOT IN(SELECT COMBINED_PAY_EID FROM COMBINED_PAYMENTS WHERE FUNDS_AUTO.PAYEE_EID=COMBINED_PAYMENTS.COMBINED_PAY_EID");

                    if (!string.IsNullOrEmpty(p_sToDate))
                    {
                        sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT > '{0}'", p_sToDate);
                    }
                    if (!string.IsNullOrEmpty(p_sFromDate))
                    {
                        sbSQL.AppendFormat(" AND COMBINED_PAYMENTS.NEXT_SCH_PRINT < '{0}'", p_sFromDate);
                    }

                    sbSQL.Append(" )");
                }

                 // npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:438 START: ajohari2
                //if (p_bPrintEFTPayment)
                //{
                //    sbSQL.Append(" AND FUNDS_AUTO.EFT_FLAG = -1");
                //}
                //else
                //{
					// akaushik5 Changed for MITS 37841 Starts
                    //sbSQL.Append(" AND FUNDS.EFT_FLAG <> -1");
                    //sbSQL.Append(" AND (FUNDS.EFT_FLAG <> -1 OR FUNDS.EFT_FLAG IS NULL)");
                    // akaushik5 Changed for MITS 37841 Ends

                //}
                //JIRA:438 End: 
                sbSQL.AppendFormat(" AND DSTRBN_TYPE_CODE = {0}", p_iDistributionType.ToString());
                // npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                if (m_objSysSettings.UseFundsSubAcc)
                {
                    sbSQL.AppendFormat(" AND FUNDS_AUTO.STATUS_CODE <> {0}", m_objCCacheFunctions.GetCodeIDWithShort("I", "CHECK_STATUS"));
                }

                if (p_bScreenTaxPayee)
                {
                    sbSQL.Append(" AND TAX_MAPPING.TAX_EID IS NULL");
                }

                if (!string.IsNullOrEmpty(p_sOrgHierarchy))
                {
                    sbSQL.AppendFormat(" AND ({0})", sOrgLevelFieldList);
                }

                sbSQL.Append(" ORDER BY ");

                if (!p_bCountOnly && p_sOrderByField != "")
                {
                    switch (p_sOrderByField.Trim())
                    {
                        case ORDER_BY_AMOUNT:
                            sTemp = "FUNDS_AUTO.AMOUNT";
                            break;
                        case ORDER_BY_CLAIM:
                            sTemp = "FUNDS_AUTO.CLAIM_NUMBER";
                            break;
                        case ORDER_BY_CONTROL:
                            sTemp = "FUNDS_AUTO.CTL_NUMBER";
                            break;
                        case ORDER_BY_NAME:
                            sTemp = "FUNDS_AUTO.LAST_NAME,FUNDS_AUTO.FIRST_NAME";
                            break;
                        case ORDER_BY_TRANS_DATE:
                            sTemp = "FUNDS_AUTO.PRINT_DATE";
                            break;
                        case ORDER_BY_ADJUSTER_EID:
                            sTemp = "ADJUSTER_EID";
                            break;
                        case ORDER_BY_BANK_ACCOUNT_SUB:
                            sTemp = "FUNDS_AUTO.SUB_ACC_ID";
                            break;
                        case ORDER_BY_ORG_LEVEL:
                            sTemp = "ORG_LEVEL";
                            break;
                    }
                    
                    if (p_sOrderByField.Equals("ORG_LEVEL"))
                    {
                        sTemp = "ORG_ABBR";
                    }

                    if (!string.IsNullOrEmpty(sTemp))
                    {
                        sbSQL.Append(sTemp + ",");
                    }
                }

                sbSQL.Append("FUNDS_AUTO.AUTO_TRANS_ID, FUNDS_AUTO_SPLIT.AUTO_SPLIT_ID");

                sSQL = sbSQL.ToString();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckRegister.CheckPrintSQLPreCheckAuto.SqlBuildError", m_iClientId), p_objEx);
            }
            finally
            {
                objFunctions = null;
                sbSQL = null;
            }
            return (sSQL);
        }
        // akaushik5 Changed for MITS 35846 Ends
		
		#endregion

		#region PostCheck Register
		/// Name		: PostCheckSubAccount
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Post Check Sub Account.
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_iBatchNum">Batch Number</param>
		/// <param name="p_sDate">Date</param>
		/// <param name="p_sPdfDocPath">Set the Path of the PDF generated.</param>
		/// <returns>Success/Failure Flag</returns>
        // npadhy JIRA 6418 Starts  - Distribution Type in Funds and using it for printing checks
		public bool PostCheckSubAccount( int p_iAccountId , int p_iBatchNum , string p_sDate , int p_iDistributionType, ref string p_sPdfDocPath )
        // npadhy JIRA 6418 Ends  - Distribution Type in Funds and using it for printing checks
		{
			DataSet objDSPrintPost = null ;
			DataTable objDTPrintPost = null ;
			DbReader objReader = null ;
			BankAccSub objBankAccSub = null ;	
			StringBuilder sbSQL = null;
			PrintEOBAR objPrintEOBAR = null ;
			
			string[,] arrPostCheckRegCol = new string[3,2] ;
			string[] arrPostCheckRegColAlign = new string[3] ;
			double[] arrPostCheckRegColWidth = new double[3] ;

			bool bReturnValue = false ;
			double dblCharWidth = 0.0 ;
			double dblLineHeight = 0.0 ;
			double dblLeftMargin = 0.0 ;
			double dblRightMargin = 0.0 ;
			double dblTopMargin = 0.0 ;
			double dblPageWidth = 0.0 ;
			double dblPageHeight = 0.0 ;
			double dblBottomHeader = 0.0 ;
			double dblReportTotal = 0.0 ;
			double dblCurrentY = 0.0 ;
			double dblPageTotal = 0 ;
			double dblCurrentX = 0.0 ;
			double dblCurrentCheckTotal = 0.0 ;
			int iSubAccountId = 0 ;
			int iFixedLinePerPage = 16 ;
			int iRealRecordCount = 0 ;
			int iDataLineCount = 0 ;
			int iPageLineCapacity = 0 ;
			int iRemainder = 0 ;
			int iIndex = 0 ;
			int iIndexPage = 0 ;
			int iNumPages = 0 ;
			int iCumulativeTotal = 0 ;
			int iPageTotal = 0 ;
			int iRowIndex = 0 ;
			int iTotalRows = 0 ;
			int iClaimId = 0 ;
			int iLine = 0 ;
			int iMaxColumns = 0 ;
			string sFooter = "" ;
			string sData = "" ;
			string sSubAccountNumber = "" ;
			string sSubAccountName = "" ;
			string sAlias = "" ;
			string sSQL = "" ;
			bool bBankAccSubExist = false ;			
			RMConfigurator objConfig = new RMConfigurator();
            //Changed by Nikhil. JIRA# RMA- 10258
            int iLOB = 0;
			try
			{
				objPrintEOBAR = new PrintEOBAR( m_sUserName , m_sPassword , m_sDsnName , "" , m_iClientId);//rkaur27
				sbSQL = new StringBuilder();
				
				if ( Functions.m_sDBType == Constants.DB_ACCESS )
					sAlias = " AS " ;
				else
					sAlias = " " ;
                //Start -  changed by Nikhil.Check total to replace fund amount.
                //Start - Changed by Nikhil. JIRA# RMA- 10258
               
			    sbSQL.Append( "SELECT SUM(AMOUNT) " + sAlias + " AMOUNT ,SUB_ACCOUNT_ID" );
                sbSQL.Append(" , SUM(CHECK_TOTAL) " + sAlias + " CHECK_TOTAL");
                //end - Changed by Nikhil. JIRA# RMA- 10258
                //end -  changed by Nikhil.Check total to replace fund amount.
				sbSQL.Append( " FROM FUNDS " );
				sbSQL.Append( " WHERE BATCH_NUMBER = " + p_iBatchNum );
				sbSQL.Append( " AND STATUS_CODE = " + m_objCCacheFunctions.GetCodeIDWithShort( "P" , "CHECK_STATUS" ) );				
				sbSQL.Append( " AND ACCOUNT_ID = " + p_iAccountId + " AND PAYMENT_FLAG <> 0 AND VOID_FLAG = 0" );
				sbSQL.Append( " GROUP BY SUB_ACCOUNT_ID" );						

				objDSPrintPost = DbFactory.GetDataSet( m_sConnectionString , sbSQL.ToString(), m_iClientId);
				objDTPrintPost = objDSPrintPost.Tables[0] ;

				if( objDTPrintPost.Rows.Count <= 0 )
				{
					return( false );
				}

				#region Column Text Init
				arrPostCheckRegCol[0,0] = "" ;
                arrPostCheckRegCol[0, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSubAccount.SubAccountNumber", m_iClientId));
				arrPostCheckRegColAlign[0] = CheckConstants.LEFT_ALIGN ;

				arrPostCheckRegCol[1,0] = "" ;
                arrPostCheckRegCol[1, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSubAccount.SubAccountName", m_iClientId));
				arrPostCheckRegColAlign[1] = CheckConstants.LEFT_ALIGN ;

				arrPostCheckRegCol[2,0] = "" ;
                arrPostCheckRegCol[2, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSubAccount.CheckTotals", m_iClientId));
				arrPostCheckRegColAlign[2] = CheckConstants.RIGHT_ALIGN ;

				m_objPrintWrapper = new PrintWrapper(m_iClientId);
				m_objPrintWrapper.StartDoc( false,m_iClientId);

                string sFont = m_strCheckRegisterFont;  // Look for optional alternative font for check register. This is not in RISKMASTER.CONFIG by default.
				if (sFont == null) 
					m_objPrintWrapper.SetFont( "Arial" , 8 );
				else
					m_objPrintWrapper.SetFont( sFont , 8 );

				// m_objPrintWrapper.SetFont( "Arial" , 8 );
			
				dblCharWidth = m_objPrintWrapper.GetTextWidth( "0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ")/40 ;
				dblLineHeight = m_objPrintWrapper.GetTextHeight( "W" );
				dblLeftMargin = dblCharWidth * 3 ;
				dblRightMargin = dblCharWidth * 3 ;
			
				dblTopMargin = dblLineHeight * 5 ;
				dblPageWidth = m_objPrintWrapper.PageWidth - dblLeftMargin - dblRightMargin ;
				dblPageHeight = m_objPrintWrapper.PageHeight ;
				dblBottomHeader = dblPageHeight - ( 5 * dblLineHeight ) ;

				// Set up column widths, based on (approximate) character widths
				arrPostCheckRegColWidth[0] = dblCharWidth * 35  ;// sub account number
				arrPostCheckRegColWidth[1] = dblCharWidth * 35  ;// sub account name
				arrPostCheckRegColWidth[2] = dblCharWidth * 21  ;// amount

				iMaxColumns = 3 ;
			
				#endregion
			
				#region Count Pages
				iRowIndex = 0 ;
				iTotalRows = objDTPrintPost.Rows.Count ;
				
				iRealRecordCount = iTotalRows ;										
				iDataLineCount = iRealRecordCount + 2 ;  // 2 added for "Total for this report : " with blank line
				iPageLineCapacity = System.Convert.ToInt32( dblPageHeight / dblLineHeight );			

				iRemainder = iDataLineCount % ( iPageLineCapacity - iFixedLinePerPage ) == 0 ? 0 : 1 ;
				iNumPages = ((iDataLineCount) / ( iPageLineCapacity - iFixedLinePerPage )) + iRemainder ;
			
				#endregion 

				iRowIndex = 0 ;

				for( iIndexPage = 1 ; iIndexPage <= iNumPages ; iIndexPage++ )
				{
					iPageTotal = 0 ;

					dblCurrentY = dblTopMargin ;
                    // npadhy JIRA 6418 Starts  - Distribution Type in Funds and using it for printing checks
                    sSQL = objPrintEOBAR.CheckPrintSQLPostCheck(p_iAccountId, "SUB_ACCOUNT_ID", p_iBatchNum, p_iDistributionType);
                    // npadhy JIRA 6418 Ends  - Distribution Type in Funds and using it for printing checks
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader != null )
					{
						if( objReader.Read() )
						{
							iClaimId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "CLAIM_ID" ), m_iClientId );
						}
						else
						{
							iClaimId = 0 ;
						}
						//objReader.Close();
					}
					
					// Print the top of the page (header area)
					CheckRegPrintHeader( p_iBatchNum , ref dblCurrentY , p_sDate , p_iAccountId , iMaxColumns , 
						iClaimId  , dblPageWidth , dblLeftMargin , dblLineHeight , arrPostCheckRegCol , 
						arrPostCheckRegColWidth , arrPostCheckRegColAlign );

					dblPageTotal = 0 ;
					for( iLine = 1 ; iLine <= iPageLineCapacity - iFixedLinePerPage ; iLine++ )
					{
						m_objPrintWrapper.CurrentY = dblCurrentY ;
						dblCurrentX = dblLeftMargin ;


                        //Start - Changed by Nikhil. JIRA# RMA- 10258

                        iLOB = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + iClaimId.ToString());

                        if (iLOB > 0 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB] != null)
                        {
                            dblCurrentCheckTotal = m_objSysSettings.MultiCovgPerClm == -1 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1
                                ? Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["CHECK_TOTAL"].ToString())
                                : Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                        }
                        else
                        {
                            dblCurrentCheckTotal = Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                        }
                        //End - Changed by Nikhil. JIRA# RMA- 10258
						iSubAccountId = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["SUB_ACCOUNT_ID"].ToString() );
						
						objBankAccSub = ( BankAccSub ) m_objDataModelFactory.GetDataModelObject( "BankAccSub" , false );
						try
						{
							objBankAccSub.MoveTo( iSubAccountId );
							bBankAccSubExist = true ;
						}
						catch
						{
							bBankAccSubExist = false ;
						}
						if( bBankAccSubExist )
						{
							sSubAccountName = objBankAccSub.SubAccName.Trim();
							sSubAccountNumber = objBankAccSub.SubAccNumber ;
						}
						else
						{
							sSubAccountName = "N/A" ;
							sSubAccountNumber = "0" ;
						}

						for( iIndex = 0 ; iIndex < iMaxColumns ; iIndex++ )
						{
							#region Print Each Column for current record
							sData = "" ;
							switch( iIndex )
							{
								case 0 :
									sData = sSubAccountNumber ;
									break;
								case 1 :
									sData = sSubAccountName ;
									break;
								case 2 :
								//	sData = string.Format( "{0:C}" , dblCurrentCheckTotal );MITS 27198
                                       sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblCurrentCheckTotal, m_sConnectionString, m_iClientId);//skhare7 r8 
									break;																		
							}// end switch

							while( m_objPrintWrapper.GetTextWidth( sData ) > arrPostCheckRegColWidth[iIndex] )
								sData = sData.Substring( 0 , sData.Length - 1 );

							// Print using alignment
							if( arrPostCheckRegColAlign[iIndex] == CheckConstants.LEFT_ALIGN )
							{
								m_objPrintWrapper.CurrentX = dblCurrentX ;
								m_objPrintWrapper.PrintTextNoCr( " " + sData );
							}
							else
							{
								m_objPrintWrapper.CurrentX = dblCurrentX + arrPostCheckRegColWidth[iIndex] 
									- m_objPrintWrapper.GetTextWidth( sData) - dblCharWidth ;
								m_objPrintWrapper.PrintTextNoCr( sData );
							}
							dblCurrentX += arrPostCheckRegColWidth[iIndex] ;
							#endregion 						
						}						
						iPageTotal++ ;
						dblCurrentY += dblLineHeight ;
						dblPageTotal += dblCurrentCheckTotal ;
						dblReportTotal += dblCurrentCheckTotal ;
						
						iRowIndex++ ;
											
						if( ( iRowIndex >= iTotalRows ) )  // EOF
						{	
							if( dblCurrentY < dblBottomHeader - dblLineHeight * 2 )
							{
								dblCurrentY += dblLineHeight ;
								m_objPrintWrapper.CurrentY = dblCurrentY ;
								m_objPrintWrapper.CurrentX = dblPageWidth/2 + dblLeftMargin ;
								//m_objPrintWrapper.PrintText( "Total for this report: " + string.Format( "{0:C}" , dblReportTotal ) );
                                sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblReportTotal, m_sConnectionString, m_iClientId);//skhare7 r8 
								dblCurrentY += dblLineHeight ;								
							}
							break ;
						}
						else																// if not eof
						{							
							if( !( dblCurrentY < dblBottomHeader - dblLineHeight) )         // id not space there															
								break;
						}											
					}

					iCumulativeTotal += iPageTotal ;

					dblCurrentY = dblBottomHeader ;
                    sFooter = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSubAccount.PostcheckRegister", m_iClientId));
					CheckRegPrintFooter( iIndexPage , iNumPages , dblPageTotal , dblReportTotal , dblCurrentY , 
						sFooter , iPageTotal , iCumulativeTotal , "Sub Account Summary" , 0 , dblCharWidth , 
						dblLineHeight , dblLeftMargin , dblRightMargin , dblPageWidth );
				
					if( iIndexPage != iNumPages )
						m_objPrintWrapper.NewPage();
					else
					{
						m_objPrintWrapper.EndDoc();				
						p_sPdfDocPath = m_sPdfSavePath + Functions.GetUniqueFileName(m_iClientId);
						m_objPrintWrapper.Save( p_sPdfDocPath );
						bReturnValue = true ;
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("CheckRegister.PostCheckSubAccount.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objDSPrintPost != null )
				{
					objDSPrintPost.Dispose();
					objDSPrintPost = null ;
				}
				if( objDTPrintPost != null )
				{
					objDTPrintPost.Dispose();
					objDTPrintPost = null ;
				}
				if( objReader != null )
				{
					//objReader.Close();
					objReader.Dispose();
				}
				if( objBankAccSub != null )
				{
					objBankAccSub.Dispose();
					objBankAccSub = null ;
				}	

				if (objPrintEOBAR != null )
                    objPrintEOBAR.Dispose();
				sbSQL = null ;
			}
			return( bReturnValue );
		}
		
		/// Name		: PostCheckSummary
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Post Check Summary.
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_iBatchNum">Batch Number</param>
		/// <param name="p_sDate">Date</param>
		/// <param name="p_sPdfDocPath">Set the Path of the PDF generated.</param>
		/// <returns>Success/Failure Flag</returns>
		public bool PostCheckSummary( int p_iAccountId , int p_iBatchNum , string p_sDate , ref string p_sPdfDocPath )
		{
			DataSet objDSPrintPost = null ;
			DataTable objDTPrintPost = null ;
			BankAccSub objBankAccSub = null ;
			StringBuilder sbSQL = null;
			
			string[,] arrPostCheckRegCol = new string[5,2] ;
			string[] arrPostCheckRegColAlign = new string[5] ;
			double[] arrPostCheckRegColWidth = new double[5] ;

			double dblCharWidth = 0.0 ;
			double dblLineHeight = 0.0 ;
			double dblLeftMargin = 0.0 ;
			double dblRightMargin = 0.0 ;
			double dblTopMargin = 0.0 ;
			double dblPageWidth = 0.0 ;
			double dblPageHeight = 0.0 ;
			double dblBottomHeader = 0.0 ;
			double dblReportTotal = 0.0 ;
			double dblCurrentY = 0.0 ;
			double dblPageTotal = 0 ;
			double dblRoundedAmount = 0.0 ;
			double dblCurrentX = 0.0 ;
			double dblCurrentCheckTotal = 0.0 ;
			int iSubAccountId = 0 ;
			int iOldPayeeEid = 0 ;
			int iNewPayeeEid = 0 ;
			//int iOldCheckNumber = 0 ;
            long lOldCheckNumber = 0;        //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
			//int iNewCheckNumber = 0 ;
            long lNewCheckNumber = 0;        //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
			int iFixedLinePerPage = 16 ;
			int iRealRecordCount = 0 ;
			int iDataLineCount = 0 ;
			int iPageLineCapacity = 0 ;
			int iRemainder = 0 ;
			int iIndex = 0 ;
			int iIndexPage = 0 ;
			int iNumPages = 0 ;
			int iCumulativeTotal = 0 ;
			int iPageTotal = 0 ;
			int iRowIndex = 0 ;
			int iTotalRows = 0 ;
			int iClaimId = 0 ;
			int iLine = 0 ;
			int iMaxColumns = 0 ;
			string sFooter = "" ;
			string sCheckDate = "" ;
			string sData = "" ;
			string sName = "" ;
			bool bBankAccSubExist = false ;
			bool bReturnValue = false ;
            int iLOB = 0;
			
			try
			{
				sbSQL = new StringBuilder();

				sbSQL.Append( " SELECT TRANS_NUMBER,PAYEE_EID,FIRST_NAME,LAST_NAME,DATE_OF_CHECK,AMOUNT,SUB_ACCOUNT_ID, " );
                //Start -  Added by Nikhil.Check total to replace fund amount.
                sbSQL.Append(" CHECK_TOTAL, ");
                //Start -  Added by Nikhil.Check total to replace fund amount.
                sbSQL.Append(" FUNDS.CLAIM_ID, LINE_OF_BUS_CODE FROM FUNDS LEFT JOIN CLAIM ON CLAIM.CLAIM_ID = FUNDS.CLAIM_ID WHERE ");
				sbSQL.Append( " BATCH_NUMBER = " + p_iBatchNum );
				sbSQL.Append( " AND STATUS_CODE = " + m_objCCacheFunctions.GetCodeIDWithShort( "P" , "CHECK_STATUS" )	);
				sbSQL.Append( " AND ACCOUNT_ID = " + p_iAccountId + " AND PAYMENT_FLAG <> 0 AND VOID_FLAG = 0" );
				sbSQL.Append( " ORDER BY TRANS_NUMBER, PAYEE_EID" );

                objDSPrintPost = DbFactory.GetDataSet(m_sConnectionString, sbSQL.ToString(), m_iClientId);
				objDTPrintPost = objDSPrintPost.Tables[0] ;

				if( objDTPrintPost.Rows.Count <= 0 )
				{
					return( false );
				}

				#region Column Text Init
                arrPostCheckRegCol[0, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Check", m_iClientId));
                arrPostCheckRegCol[0, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Number", m_iClientId));
				arrPostCheckRegColAlign[0] = CheckConstants.RIGHT_ALIGN ;

                arrPostCheckRegCol[1, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Check", m_iClientId));
                arrPostCheckRegCol[1, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Date", m_iClientId));
				arrPostCheckRegColAlign[1] = CheckConstants.LEFT_ALIGN ;

                arrPostCheckRegCol[2, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Payee", m_iClientId));
                arrPostCheckRegCol[2, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Name", m_iClientId));
				arrPostCheckRegColAlign[2] = CheckConstants.LEFT_ALIGN ;

                arrPostCheckRegCol[3, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Check", m_iClientId));
                arrPostCheckRegCol[3, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Total", m_iClientId));
				arrPostCheckRegColAlign[3] = CheckConstants.RIGHT_ALIGN ;

                arrPostCheckRegCol[4, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.SubAccount", m_iClientId));
                arrPostCheckRegCol[4, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.Name", m_iClientId));	
				arrPostCheckRegColAlign[4] = CheckConstants.LEFT_ALIGN ;
									
				m_objPrintWrapper = new PrintWrapper(m_iClientId);
				m_objPrintWrapper.StartDoc( false,m_iClientId );

                string sFont = m_strCheckRegisterFont;  // Look for optional alternative font for check register. This is not in RISKMASTER.CONFIG by default.
				if (sFont == null) 
					m_objPrintWrapper.SetFont( "Arial" , 8 );
				else
					m_objPrintWrapper.SetFont( sFont , 8 );

				// m_objPrintWrapper.SetFont( "Arial" , 8 );
			
				dblCharWidth = m_objPrintWrapper.GetTextWidth( "0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ")/40 ;
				dblLineHeight = m_objPrintWrapper.GetTextHeight( "W" );
				dblLeftMargin = dblCharWidth * 3 ;
				dblRightMargin = dblCharWidth * 3 ;
			
				dblTopMargin = dblLineHeight * 5 ;
				dblPageWidth = m_objPrintWrapper.PageWidth - dblLeftMargin - dblRightMargin ;
				dblPageHeight = m_objPrintWrapper.PageHeight ;
				dblBottomHeader = dblPageHeight - ( 5 * dblLineHeight ) ;

				// Set up column widths, based on (approximate) character widths
				arrPostCheckRegColWidth[0] = dblCharWidth * 14  ;// check num
				arrPostCheckRegColWidth[1] = dblCharWidth * 12  ;// Check date
				arrPostCheckRegColWidth[2] = dblCharWidth * 60  ;// payee name
				arrPostCheckRegColWidth[3] = dblCharWidth * 17  ;// check total
				arrPostCheckRegColWidth[4] = dblCharWidth * 20  ;// sub account name

				if( m_objSysSettings.UseFundsSubAcc )
					iMaxColumns = 5 ;
				else
					iMaxColumns = 4 ;			
				#endregion
			
				#region Count Pages
				iRowIndex = 0 ;
				iTotalRows = objDTPrintPost.Rows.Count ;

				while( iRowIndex < iTotalRows )
				{
                    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
					//iNewCheckNumber = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["TRANS_NUMBER"].ToString() );
                    lNewCheckNumber = Common.Conversion.ConvertStrToLong(objDTPrintPost.Rows[iRowIndex]["TRANS_NUMBER"].ToString());
					iNewPayeeEid = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["PAYEE_EID"].ToString() );

					if( lOldCheckNumber != lNewCheckNumber || iOldPayeeEid != iNewPayeeEid )
					{
						iRealRecordCount++;
						lOldCheckNumber = lNewCheckNumber ;
						iOldPayeeEid = iNewPayeeEid ;
					}							
					iRowIndex++ ;
				}			
			
				iDataLineCount = iRealRecordCount + 2 ;  // 2 added for "Total for this report : " with blank line
				iPageLineCapacity = System.Convert.ToInt32( dblPageHeight / dblLineHeight );			

				iRemainder = iDataLineCount % ( iPageLineCapacity - iFixedLinePerPage ) == 0 ? 0 : 1 ;
				iNumPages = ((iDataLineCount) / ( iPageLineCapacity - iFixedLinePerPage )) + iRemainder ;
			
				#endregion 

				iRowIndex = 0 ;

                //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
                //iOldCheckNumber = Common.Conversion.ConvertStrToInteger(objDTPrintPost.Rows[iRowIndex]["TRANS_NUMBER"].ToString());
                lOldCheckNumber = Common.Conversion.ConvertStrToLong(objDTPrintPost.Rows[iRowIndex]["TRANS_NUMBER"].ToString());
				iOldPayeeEid = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["PAYEE_EID"].ToString() );
				sCheckDate = Common.Conversion.GetDBDateFormat( objDTPrintPost.Rows[iRowIndex]["DATE_OF_CHECK"].ToString() , "d");
                //MGaba2:MITS 13063:09/01/2008:Added Trim Function:To avoid space in case first name is blank
				sName = (objDTPrintPost.Rows[iRowIndex]["FIRST_NAME"].ToString() + " " 
					+ objDTPrintPost.Rows[iRowIndex]["LAST_NAME"].ToString()).Trim();
                //Start -  changed by Nikhil.Check total to replace fund amount.
                //End - Changed by Nikhil. JIRA# RMA- 10258
                iLOB = Common.Conversion.ConvertStrToInteger(objDTPrintPost.Rows[iRowIndex]["LINE_OF_BUS_CODE"].ToString());

                if (iLOB > 0 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB] != null)
                {
                    dblRoundedAmount = m_objSysSettings.MultiCovgPerClm == -1 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1
                        ? Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["CHECK_TOTAL"].ToString())
                        : Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                }
                else
                {
                    dblRoundedAmount = Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                }
                //End - Changed by Nikhil. JIRA# RMA- 10258
                //End -  changed by Nikhil.Check total to replace fund amount.
                dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
				dblCurrentCheckTotal = dblRoundedAmount ;
				iSubAccountId = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["SUB_ACCOUNT_ID"].ToString() );
								
				for( iIndexPage = 1 ; iIndexPage <= iNumPages ; iIndexPage++ )
				{
					iPageTotal = 0 ;

					dblCurrentY = dblTopMargin ;
					iClaimId = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["CLAIM_ID"].ToString() );
					// Print the top of the page (header area)
					CheckRegPrintHeader( p_iBatchNum , ref dblCurrentY , p_sDate , p_iAccountId , iMaxColumns , 
						iClaimId  , dblPageWidth , dblLeftMargin , dblLineHeight , arrPostCheckRegCol , 
						arrPostCheckRegColWidth , arrPostCheckRegColAlign );

					dblPageTotal = 0 ;
					for( iLine = 1 ; iLine <= iPageLineCapacity - iFixedLinePerPage ; iLine++ )
					{
						iRowIndex++ ;					// MoveNext
						if( iRowIndex < iTotalRows )	// NOT EOF
						{
                            //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
							//iNewCheckNumber = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["TRANS_NUMBER"].ToString() );
                            lNewCheckNumber = Common.Conversion.ConvertStrToLong(objDTPrintPost.Rows[iRowIndex]["TRANS_NUMBER"].ToString());
							iNewPayeeEid = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["PAYEE_EID"].ToString() );			
						}
						else
						{
							lNewCheckNumber = 0 ;
							iNewPayeeEid = 0 ;			
						}

						if( lOldCheckNumber != lNewCheckNumber || iOldPayeeEid != iNewPayeeEid )
						{
							m_objPrintWrapper.CurrentY = dblCurrentY ;
							dblCurrentX = dblLeftMargin ;
							for( iIndex = 0 ; iIndex < iMaxColumns ; iIndex++ )
							{
								#region Print Each Column for current record
								sData = "" ;
								switch( iIndex )
								{
									case 0 :
										sData = lOldCheckNumber.ToString();
										break;
									case 1 :
										sData = sCheckDate ;
										break;
									case 2 :
										sData = sName ;
										break;
									case 3 :
										//sData = string.Format( "{0:C}" , dblCurrentCheckTotal );
                                         sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblCurrentCheckTotal, m_sConnectionString,m_iClientId);//skhare7 r8 
										break;
									case 4 :
										objBankAccSub = ( BankAccSub ) m_objDataModelFactory.GetDataModelObject( "BankAccSub" , false );
										try
										{
											objBankAccSub.MoveTo( iSubAccountId );
											bBankAccSubExist = true ;
										}
										catch
										{
											bBankAccSubExist = false ;
										}
										if( bBankAccSubExist )
											sData = objBankAccSub.SubAccName.Trim();
										else
											sData = "N/A" ;
										break;																		
								}// end switch

								while( m_objPrintWrapper.GetTextWidth( sData ) > arrPostCheckRegColWidth[iIndex] )
									sData = sData.Substring( 0 , sData.Length - 1 );

								// Print using alignment
								if( arrPostCheckRegColAlign[iIndex] == CheckConstants.LEFT_ALIGN )
								{
									m_objPrintWrapper.CurrentX = dblCurrentX ;
									m_objPrintWrapper.PrintTextNoCr( " " + sData );
								}
								else
								{
									m_objPrintWrapper.CurrentX = dblCurrentX + arrPostCheckRegColWidth[iIndex] 
										- m_objPrintWrapper.GetTextWidth( sData) - dblCharWidth ;
									m_objPrintWrapper.PrintTextNoCr( sData );
								}
								dblCurrentX += arrPostCheckRegColWidth[iIndex] ;
								#endregion 
							
							}
							iPageTotal++;
							dblCurrentY += dblLineHeight ;
							dblPageTotal += dblCurrentCheckTotal ;
							dblReportTotal += dblCurrentCheckTotal ;

							if( iRowIndex < iTotalRows )
							{
								lOldCheckNumber = lNewCheckNumber ;
								iOldPayeeEid = iNewPayeeEid ;
								sCheckDate = Common.Conversion.GetDBDateFormat( objDTPrintPost.Rows[iRowIndex]["DATE_OF_CHECK"].ToString() , "d");
                                //MGaba2:MITS 13063:09/01/2008:Added Trim Function:To avoid space in case first name is blank
								sName = (objDTPrintPost.Rows[iRowIndex]["FIRST_NAME"].ToString() + " " 
									+ objDTPrintPost.Rows[iRowIndex]["LAST_NAME"].ToString()).Trim() ;
                                //Start -  changed by Nikhil.Check total to replace fund amount.
                                //Start - Changed by Nikhil. JIRA# RMA- 10258
                                iLOB = Common.Conversion.ConvertStrToInteger(objDTPrintPost.Rows[iRowIndex]["LINE_OF_BUS_CODE"].ToString());
                                
                                if (iLOB > 0 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB] != null)
                                {
                                    dblRoundedAmount = m_objSysSettings.MultiCovgPerClm == -1 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1
                                        ? Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["CHECK_TOTAL"].ToString())
                                        : Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                                }
                                else
                                {
                                    dblRoundedAmount = Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                                }
                                //End - Changed by Nikhil. JIRA# RMA- 10258
                                //END -  changed by Nikhil.Check total to replace fund amount.
                                dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
								dblCurrentCheckTotal = dblRoundedAmount ;
								iSubAccountId = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["SUB_ACCOUNT_ID"].ToString() );
							
							}

							if( ( iRowIndex >= iTotalRows ) )  // EOF
							{
								if( dblCurrentY < dblBottomHeader - dblLineHeight * 2 )
								{
									dblCurrentY += dblLineHeight ;
									m_objPrintWrapper.CurrentY = dblCurrentY ;
									m_objPrintWrapper.CurrentX = dblPageWidth/2 + dblLeftMargin ;
								//	m_objPrintWrapper.PrintText( "Total for this report: " + string.Format( "{0:C}" , dblReportTotal ) );
                                    sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblReportTotal, m_sConnectionString, m_iClientId);//skhare7 r8 
									dblCurrentY += dblLineHeight ;								
								}
								break ;
							}
							else																// if not eof
							{							
								if( !( dblCurrentY < dblBottomHeader - dblLineHeight) )         // id not space there															
									break;
							}
						}
						else
						{
                            //Start -  changed by Nikhil.Check total to replace fund amount.
                            //Start - Changed by Nikhil. JIRA# RMA- 10258
                            iLOB = Common.Conversion.ConvertStrToInteger(objDTPrintPost.Rows[iRowIndex]["LINE_OF_BUS_CODE"].ToString());

                            if (iLOB > 0 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB] != null)
                            {
                                // akaushik5 Changed for MITS 38274 Starts
                                //dblRoundedAmount = m_objSysSettings.MultiCovgPerClm == -1 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1
                                //    ? Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["CHECK_TOTAL"].ToString())
                                //    : Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                                if (m_objSysSettings.MultiCovgPerClm == -1 && m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1)
                                {
                                    dblRoundedAmount = Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["CHECK_TOTAL"].ToString());
                                    dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
                                    dblCurrentCheckTotal = dblRoundedAmount;
                                }
                                else
                                {
                                    dblRoundedAmount = Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                                    dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
                                    dblCurrentCheckTotal += dblRoundedAmount;
                                }
                                // akaushik5 Changed for MITS 38274 Ends
                            }
                            else
                            {
                                dblRoundedAmount = Common.Conversion.ConvertStrToDouble(objDTPrintPost.Rows[iRowIndex]["AMOUNT"].ToString());
                                // akaushik5 Added for MITS 38274 Starts
                                dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
                                dblCurrentCheckTotal += dblRoundedAmount;
                                // akaushik5 Added for MITS 38274 Ends
                            }
                            //End - Changed by Nikhil. JIRA# RMA- 10258
                            //END -  changed by Nikhil.Check total to replace fund amount.
                            // akaushik5 Commneted for MITS 38274 Starts
                            //dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
                            // akaushik5 Commneted for MITS 38274 Ends
                            //start - Changed by Nikhil.
							//dblCurrentCheckTotal += dblRoundedAmount ;
                            // akaushik5 Commneted for MITS 38274 Starts
                            //dblCurrentCheckTotal = dblRoundedAmount;
                            // akaushik5 Commneted for MITS 38274 Ends
                            //End -  changed by Nikhil.
							iLine--;
						}
					}

					iCumulativeTotal += iPageTotal ;

					dblCurrentY = dblBottomHeader ;
                    sFooter = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckSummary.PostcheckRegister", m_iClientId));
					CheckRegPrintFooter( iIndexPage , iNumPages , dblPageTotal , dblReportTotal , dblCurrentY , 
						sFooter , iPageTotal , iCumulativeTotal , "Summary Report" , 0 , dblCharWidth , 
						dblLineHeight , dblLeftMargin , dblRightMargin , dblPageWidth );
				
					if( iIndexPage != iNumPages )
						m_objPrintWrapper.NewPage();
					else
					{
						m_objPrintWrapper.EndDoc();	
						p_sPdfDocPath = m_sPdfSavePath + Functions.GetUniqueFileName(m_iClientId);
						m_objPrintWrapper.Save( p_sPdfDocPath );
						bReturnValue = true ;
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.PostCheckSummary.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objDSPrintPost != null )
				{
					objDSPrintPost.Dispose();
					objDSPrintPost = null ;
				}
				if( objDTPrintPost != null )
				{
					objDTPrintPost.Dispose();
					objDTPrintPost = null ;
				}
				if( objBankAccSub != null )
				{
					objBankAccSub.Dispose();
					objBankAccSub = null ;
				}
				sbSQL = null ;
			}
			return( bReturnValue );
		}
		/// Name		: PostCheckDetail
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Post Check Detail.
		/// </summary>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_sOrderByField">Order By String</param>
		/// <param name="p_iBatchNum">Batch Number</param>
		/// <param name="p_sDate">Date</param>
		/// <param name="p_sPdfDocPath">Set the Path of the PDF generated.</param>
		/// <returns>Success/Failure Flag</returns>
        // npadhy JIRA 6418 Starts  - Distribution Type in Funds and using it for printing checks
		public bool PostCheckDetail( int p_iAccountId , string p_sOrderByField , int p_iBatchNum , 
			string p_sDate , int p_iDistributionType, ref string p_sPdfDocPath )
        // npadhy JIRA 6418 Ends  - Distribution Type in Funds and using it for printing checks
		{
			DataSet objDSPrintPost = null ;
			DataTable objDTPrintPost = null ;
			ArrayList arrlstCheckCount = null ;
			PrintEOBAR objPrintEOBAR = null ;
			
			string[,] arrPostCheckRegCol = new string[9,2] ;
			string[] arrPostCheckRegColAlign = new string[9] ;
			double[] arrPostCheckRegColWidth = new double[9] ;
						
			double dblCharWidth = 0.0 ;
			double dblLineHeight = 0.0 ;
			double dblLeftMargin = 0.0 ;
			double dblRightMargin = 0.0 ;
			double dblTopMargin = 0.0 ;
			double dblPageWidth = 0.0 ;
			double dblPageHeight = 0.0 ;
			double dblBottomHeader = 0.0 ;
			double dblReportTotal = 0.0 ;
			double dblCurrentY = 0.0 ;
			double dblPageTotal = 0 ;
			int iFixedLinePerPage = 18 ;
			int iRealRecordCount = 0 ;
			int iTransId = 0 ;
			//int iCheckNum = 0 ;
            long lCheckNum = 0;    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
			int iOldSubAccount = 0 ;
			int iNewSubAccount = 0 ;
			int iCount = 0 ;
			int iDataLineCount = 0 ;
			int iPageLineCapacity = 0 ;
			int iRemainder = 0 ;
			int iIndex = 0 ;
			int iNumPages = 0 ;
			int iMinPages = 0 ;
			int iCumulativeTotal = 0 ;
			int iPageTotal = 0 ;
			int iRowIndex = 0 ;
			int iTotalRows = 0 ;
			int iClaimId = 0 ;
			int iLine = 0 ;
			string sFooter = "" ;
			string sSQL = "" ;
			bool bReturnValue = false ;
			RMConfigurator objConfig = new RMConfigurator();
            //Start - added by nIkhil.
            //Start: Changed the following code by Sumit Agarwal for NI: 11/07/2014
            //Funds objFunds = null;
            int iLOB = 0;
            //End: Changed the following code by Sumit Agarwal for NI: 11/07/2014
            //end - added by Nikhil.
            
			try
			{
				objPrintEOBAR = new PrintEOBAR( m_sUserName , m_sPassword , m_sDsnName , "" , m_iClientId);//rkaur27
				arrlstCheckCount = new ArrayList();

                p_sOrderByField = Functions.GetSortOrder(p_sOrderByField, m_iClientId);
                // npadhy JIRA 6418 Starts  - Distribution Type in Funds and using it for printing checks
                sSQL = objPrintEOBAR.CheckPrintSQLPostCheck(p_iAccountId, p_sOrderByField, p_iBatchNum, p_iDistributionType);
                // npadhy JIRA 6418 Ends  - Distribution Type in Funds and using it for printing checks
				objDSPrintPost = DbFactory.GetDataSet( m_sConnectionString , sSQL, m_iClientId);

                 // objDTPrintPost = objDSPrintPost.Tables[0] ;

                if (objDSPrintPost.Tables[0].Rows.Count <= 0)
                  {
                      return (false);
                  }
			
                   //start - Added by Nikhil.
                //Start: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014
                //objFunds = m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                //if (objFunds.IsDeductibleApplicable(false))
                iLOB = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + objDSPrintPost.Tables[0].Rows[0]["CLAIM_ID"]);
                if (m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB] != null && iLOB > 0)
                {
                    if (m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLOB].ApplyDedToPaymentsFlag == -1)
                    //End: Changed the following code by Sumit Agarwal to set the Deductible properly for NI: 11/07/2014
                    {
                        objDTPrintPost = this.AddDeductibleRecCollection(objDSPrintPost.Tables[0]);
                    }
                    else
                    {
                        objDTPrintPost = objDSPrintPost.Tables[0];
                    }
                    //end - added by Nikhil
                    //Added By Nikhil
                }
                else
                {
                    objDTPrintPost = objDSPrintPost.Tables[0];
                }


                //End -  Added By Nikhil


				#region Column Text Init
				arrPostCheckRegCol[0,0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Control", m_iClientId));
                arrPostCheckRegCol[0, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Number", m_iClientId));
				arrPostCheckRegColAlign[0] = CheckConstants.RIGHT_ALIGN ;

                arrPostCheckRegCol[1, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Check", m_iClientId));
                arrPostCheckRegCol[1, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Number", m_iClientId));
				arrPostCheckRegColAlign[1] = CheckConstants.RIGHT_ALIGN ;

                arrPostCheckRegCol[2, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Check", m_iClientId));
                arrPostCheckRegCol[2, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Date", m_iClientId));
				arrPostCheckRegColAlign[2] = CheckConstants.LEFT_ALIGN ;

                arrPostCheckRegCol[3, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Claim", m_iClientId));
                arrPostCheckRegCol[3, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Number", m_iClientId));
				arrPostCheckRegColAlign[3] = CheckConstants.RIGHT_ALIGN ;

				if( m_objSysSettings.UseFundsSubAcc )
				{
                    arrPostCheckRegCol[4, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.SubAccount", m_iClientId));
                    arrPostCheckRegCol[4, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Name", m_iClientId));				
				}
				else
				{
					arrPostCheckRegCol[4,0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Department", m_iClientId));
                    arrPostCheckRegCol[4, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Code", m_iClientId));
				}
				arrPostCheckRegColAlign[4] = CheckConstants.LEFT_ALIGN ;

                arrPostCheckRegCol[5, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Payee", m_iClientId));
                arrPostCheckRegCol[5, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Name", m_iClientId));
				arrPostCheckRegColAlign[5] = CheckConstants.LEFT_ALIGN ;

                arrPostCheckRegCol[6, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Payment", m_iClientId));
                arrPostCheckRegCol[6, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Type", m_iClientId));
				arrPostCheckRegColAlign[6] = CheckConstants.LEFT_ALIGN ;

                arrPostCheckRegCol[7, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Payment", m_iClientId));
                arrPostCheckRegCol[7, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Amount", m_iClientId));
				arrPostCheckRegColAlign[7] = CheckConstants.RIGHT_ALIGN ;

                arrPostCheckRegCol[8, 0] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Check", m_iClientId));
                arrPostCheckRegCol[8, 1] = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.Total", m_iClientId));
				arrPostCheckRegColAlign[8] = CheckConstants.RIGHT_ALIGN ;
			
				m_objPrintWrapper = new PrintWrapper(m_iClientId);
				m_objPrintWrapper.StartDoc(true, m_iClientId );

                string sFont = m_strCheckRegisterFont; // Look for optional alternative font for check register. This is not in RISKMASTER.CONFIG by default.
				if (sFont == null) 
					m_objPrintWrapper.SetFont( "Arial" , 8 );
				else
					m_objPrintWrapper.SetFont( sFont , 8 );

				// m_objPrintWrapper.SetFont( "Arial" , 8 );
			
				dblCharWidth = m_objPrintWrapper.GetTextWidth( "0123456789/$,.ABCDEFGHIJKLMNOPQRSTUVWXYZ")/40 ;
				dblLineHeight = m_objPrintWrapper.GetTextHeight( "W" );
				dblLeftMargin = dblCharWidth * 2 ;
				dblRightMargin = dblCharWidth * 2 ;
				dblTopMargin = dblLineHeight * 5 ;
				dblPageWidth = m_objPrintWrapper.PageWidth - dblLeftMargin - dblRightMargin ;
				dblPageHeight = m_objPrintWrapper.PageHeight ;
				dblBottomHeader = dblPageHeight - ( 5 * dblLineHeight ) ;

				// Set up column widths, based on (approximate) character widths
				arrPostCheckRegColWidth[0] = dblCharWidth * 18  ;// ctl num
				arrPostCheckRegColWidth[1] = dblCharWidth * 12  ;// check num
				arrPostCheckRegColWidth[2] = dblCharWidth * 10  ;// Check date
				arrPostCheckRegColWidth[3] = dblCharWidth * 23  ;// claim num
				arrPostCheckRegColWidth[4] = dblCharWidth * 10  ;// department code or sub account number
				arrPostCheckRegColWidth[5] = dblCharWidth * 36  ;// payee name
				arrPostCheckRegColWidth[6] = dblCharWidth * 17  ;// payment type
				arrPostCheckRegColWidth[7] = dblCharWidth * 14  ;// payment amt
				arrPostCheckRegColWidth[8] = dblCharWidth * 15  ;// check total
				#endregion										

				#region Count Pages
				iRowIndex = 0 ;
				iTotalRows = objDTPrintPost.Rows.Count ;

				if( p_sOrderByField == "BANK_ACC_SUB" )
				{
					iOldSubAccount = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["SUB_ACCOUNT_ID"].ToString() );
					iCount = 0 ;
				}
				while( iRowIndex < iTotalRows )
				{
					if( p_sOrderByField == "BANK_ACC_SUB" )
					{
						iNewSubAccount = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["SUB_ACCOUNT_ID"].ToString() );			
						if( iOldSubAccount != iNewSubAccount )
						{
							iOldSubAccount = iNewSubAccount ;
							arrlstCheckCount.Add( iCount );
							iCount = 0 ;
						}
					}
					iCount++;
					iRealRecordCount++ ;
					if( iTransId != Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["FUNDS_TRANS_ID"].ToString() ))
						lCheckNum++ ;

					iTransId = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["FUNDS_TRANS_ID"].ToString() );

					iRowIndex++ ;
				}			
			
				if( p_sOrderByField == "BANK_ACC_SUB" )
					arrlstCheckCount.Add( iCount );			

				iDataLineCount = iRealRecordCount; // + 2 ;  // 2 added for "Total for this report : " with blank line
				iPageLineCapacity = System.Convert.ToInt32( dblPageHeight / dblLineHeight );			

				if( p_sOrderByField == "BANK_ACC_SUB" )
				{
					iFixedLinePerPage = iFixedLinePerPage + 2 ;		// For Sub Bank Account Name
					for( iIndex = 0 ; iIndex < arrlstCheckCount.Count ; iIndex++ )
					{
						iRemainder = ((int)arrlstCheckCount[iIndex]) % ( iPageLineCapacity - iFixedLinePerPage ) == 0 ? 0 : 1 ;
						iMinPages = (((int)arrlstCheckCount[iIndex]) / ( iPageLineCapacity - iFixedLinePerPage )) + iRemainder ;					
						iNumPages += iMinPages ;
					}
				}
				else
				{
					iRemainder = iDataLineCount % ( iPageLineCapacity - iFixedLinePerPage ) == 0 ? 0 : 1 ;
					iNumPages = ((iDataLineCount) / ( iPageLineCapacity - iFixedLinePerPage )) + iRemainder ;
				}
				#endregion 
			
				iRowIndex = 0 ;
				iTransId = 0 ;
				for( iIndex = 1 ; iIndex <= iNumPages ; iIndex++ )
				{
					iPageTotal = 0 ;

					dblCurrentY = dblTopMargin ;
					iClaimId = Common.Conversion.ConvertStrToInteger( objDTPrintPost.Rows[iRowIndex]["CLAIM_ID"].ToString() );
					// Print the top of the page (header area)
					CheckRegPrintHeader( p_iBatchNum , ref dblCurrentY , p_sDate , p_iAccountId , 9 , iClaimId  , 
						dblPageWidth , dblLeftMargin , dblLineHeight , arrPostCheckRegCol , arrPostCheckRegColWidth , 
						arrPostCheckRegColAlign );

					dblPageTotal = 0 ;
					for( iLine = 1 ; iLine <= iPageLineCapacity - iFixedLinePerPage ; iLine++ )
						if( ! PostCheckDetailRegPrtData( objDTPrintPost , ref iRowIndex , iLine , ref dblCurrentY , 
							ref dblPageTotal , ref dblReportTotal , dblBottomHeader , ref iTransId , 
							ref iPageTotal , p_sOrderByField , ref iOldSubAccount , dblLeftMargin , dblLineHeight , 
							dblCharWidth , dblPageWidth , arrPostCheckRegColWidth ,arrPostCheckRegColAlign ))
							break ;

					iCumulativeTotal += iPageTotal ;

					dblCurrentY = dblBottomHeader ;
                    sFooter = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetail.PostcheckRegister", m_iClientId));
					CheckRegPrintFooter( iIndex , iNumPages , dblPageTotal , dblReportTotal , dblCurrentY , 
						sFooter , iPageTotal , iCumulativeTotal , "Detail Report" , 0 , dblCharWidth , 
						dblLineHeight , dblLeftMargin , dblRightMargin , dblPageWidth );
				
					if( iIndex != iNumPages )
					{
						if( iOldSubAccount == -50 )
						{
							m_objPrintWrapper.EndDoc();
							p_sPdfDocPath = m_sPdfSavePath + Functions.GetUniqueFileName(m_iClientId);
							m_objPrintWrapper.Save( p_sPdfDocPath );
							bReturnValue = true ;
							break;
						}
						else
						{
							m_objPrintWrapper.NewPage();
						}
					}	
					else
					{
						m_objPrintWrapper.EndDoc();
						p_sPdfDocPath = m_sPdfSavePath + Functions.GetUniqueFileName(m_iClientId);
						m_objPrintWrapper.Save( p_sPdfDocPath );
						bReturnValue = true ;
					}
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.PostCheckDetail.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objDSPrintPost != null )
				{
					objDSPrintPost.Dispose();
					objDSPrintPost = null ;
				}
				if( objDTPrintPost != null )
				{
					objDTPrintPost.Dispose();
					objDTPrintPost = null ;
				}
				arrlstCheckCount = null ;
				if (objPrintEOBAR!=null)
                    objPrintEOBAR.Dispose();
			}
			return( bReturnValue );
		}
				
		/// Name		: PostCheckDetailRegPrtData
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Post Check Detail Data.
		/// </summary>
		/// <param name="p_objDTPrintPost">DataTable Records</param>
		/// <param name="p_iRowIndex">Curent Row Index</param>
		/// <param name="p_iLine">Line Number</param>
		/// <param name="p_dblCurrentY">Current Y</param>
		/// <param name="p_dblPageTotal">Page Total</param>
		/// <param name="p_dblReportTotal">Report Total</param>
		/// <param name="p_dblBottomHeader">Bottom Header Location </param>
		/// <param name="p_iTransId">Trans Id</param>
		/// <param name="p_iPageTotal">Total Pages</param>
		/// <param name="p_sOrderByField">Sort By string</param>
		/// <param name="p_iOldSubAccount">Old Sub Account</param>
		/// <param name="p_dblLeftMargin">Left Margin</param>
		/// <param name="p_dblLineHeight">Line Height</param>
		/// <param name="p_dblCharWidth">Char Width</param>
		/// <param name="p_dblPageWidth">Page Width</param>
		/// <param name="p_arrPostCheckregColWidth">Columns Width</param>
		/// <param name="p_arrPostCheckRegColAlign">Columns Alignment</param>
		/// <returns>Success/Failure Flag</returns>
		private bool PostCheckDetailRegPrtData( DataTable p_objDTPrintPost , ref int p_iRowIndex , int p_iLine , 
			ref double p_dblCurrentY , ref double p_dblPageTotal , ref double p_dblReportTotal , 
			double p_dblBottomHeader , ref int p_iTransId , ref int p_iPageTotal , string p_sOrderByField , 
			ref int p_iOldSubAccount , double p_dblLeftMargin , double p_dblLineHeight , double p_dblCharWidth , 
			double p_dblPageWidth ,double[] p_arrPostCheckregColWidth ,string[] p_arrPostCheckRegColAlign )
		{
			// Print data for 1 check. Assumes the current record is positioned to the FUNDS record to
			// be printed. After printing, advance to the next record. If the EOF is seen, print two
			// lines of report summary.
			//
			// The printing routine calling this routine should call this routine until it returns 0,
			// indicating that the page is completed.
			
			BankAccSub objBankAccSub = null ;
			
			bool bBankAccSubExist = false ;
			bool bLeadingLine = false ;
			bool bNewCheck = false ;
			bool bReturnValue = false ;
			int iIndex = 0 ;
			int iNewSubAccount = 0 ;
			//int iCheckNum = 0 ;
            long lCheckNum = 0;    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
			int iSubAccountId = 0 ;
			int iClaimId = 0 ;
			int iTransTypeCode = 0 ;
			string sSubAccountName = "" ;
			string sTemp = "                                        " ;  // space( 40 );
			string sData = "" ;
			string sCodeDesc = "" ;
			string sShortCode = "" ;
			double dblAmount = 0.0 ;
			double dblFundsAmount = 0.0 ;
			double dblRoundedAmount = 0.0 ;
			double dblCurrentX = 0.0 ;
            //Added by Nikhil.
            int iLineOfBusCode = 0;
            int iClaimID = 0;
            bool isDeductibleSplit = false;
            //End
			try
			{				
				dblCurrentX = p_dblLeftMargin ;
				m_objPrintWrapper.CurrentY = p_dblCurrentY ;
			
				#region If Sort by Sub Bank Account 
				if( p_sOrderByField == "BANK_ACC_SUB" )
				{
					iNewSubAccount = Common.Conversion.ConvertStrToInteger( p_objDTPrintPost.Rows[p_iRowIndex]["SUB_ACCOUNT_ID"].ToString() );
					// If this is the first line to be print on the page.
					if( p_iLine == 1 )
					{
						p_dblCurrentY += p_dblLineHeight * 2 ;
						m_objPrintWrapper.CurrentX = dblCurrentX ;
						m_objPrintWrapper.SetFontBold( true );
						objBankAccSub = ( BankAccSub ) m_objDataModelFactory.GetDataModelObject( "BankAccSub" , false );
						try
						{
							objBankAccSub.MoveTo( iNewSubAccount );
							bBankAccSubExist = true ;
						}
						catch
						{
							bBankAccSubExist = false ;
						}
						if( bBankAccSubExist )
						{
							sSubAccountName = ( objBankAccSub.SubAccName + " " + objBankAccSub.SubAccNumber).Trim() ;
							if( sSubAccountName != "" )
                                sTemp += Functions.ReplaceArguments(CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetailRegPrtData.CurrentSubAccount", m_iClientId)), m_iClientId, sSubAccountName);
							else
                                sTemp += CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetailRegPrtData.NoCurrentSubAccount", m_iClientId));							
						}
						else
						{
                            sTemp += CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.PostCheckDetailRegPrtData.NoCurrentSubAccount", m_iClientId));							
						}
						m_objPrintWrapper.PrintText( sTemp );
						m_objPrintWrapper.SetFontBold( false );
						m_objPrintWrapper.PrintText( "" );
					}
					else
					{
						if( p_iOldSubAccount != iNewSubAccount )
						{
							p_iOldSubAccount = -10 ;
							return( false );
						}
					}
					p_iOldSubAccount = iNewSubAccount ;
				}
				#endregion 
				
				m_objPrintWrapper.CurrentY = p_dblCurrentY ;

				if( p_iRowIndex < p_objDTPrintPost.Rows.Count )
				{
					bNewCheck = p_iTransId != Common.Conversion.ConvertStrToInteger( p_objDTPrintPost.Rows[p_iRowIndex]["FUNDS_TRANS_ID"].ToString() ) ;
					bLeadingLine = bNewCheck || p_iLine == 1 ;
					p_iTransId = Common.Conversion.ConvertStrToInteger( p_objDTPrintPost.Rows[p_iRowIndex]["FUNDS_TRANS_ID"].ToString() ) ;
                    //Added by Nikhil
                    lCheckNum = Common.Conversion.ConvertStrToLong(p_objDTPrintPost.Rows[p_iRowIndex]["TRANS_NUMBER"].ToString());
                    //End
					for( iIndex = 0 ; iIndex < 9 ; iIndex++ )
					{
						#region Print Each Column for current record
						sData = "" ;
						switch( iIndex )
						{
							case 0 :
                                //Changed by Nikhil.
								//if( bLeadingLine )
                                //Added by Nikhil
                                iClaimID = Common.Conversion.ConvertStrToInteger(p_objDTPrintPost.Rows[p_iRowIndex]["CLAIM_ID"].ToString());
                                iTransTypeCode = Common.Conversion.ConvertStrToInteger(p_objDTPrintPost.Rows[p_iRowIndex]["TRANS_TYPE_CODE"].ToString());
                                iLineOfBusCode = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + iClaimID);

                                // npadhy RMACLOUD-2384 Fixed the error while printing the check 
                                // Fixed the Error in postcheck of  printing orphan transaction 
                                if(iLineOfBusCode > 0)
                                    isDeductibleSplit = iTransTypeCode == m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLineOfBusCode].DedRecTransType;

                                //End
                                if (bLeadingLine || isDeductibleSplit)
                                   //End
									sData = p_objDTPrintPost.Rows[p_iRowIndex]["CTL_NUMBER"].ToString() ;
								break;
							case 1 :
								if( bLeadingLine )
								{
                                    //pmittal5 Mits 14500 02/24/09 - Check Number is of 64 bits length
									//iCheckNum = Common.Conversion.ConvertStrToInteger( p_objDTPrintPost.Rows[p_iRowIndex]["TRANS_NUMBER"].ToString() ) ;
                                    lCheckNum = Common.Conversion.ConvertStrToLong(p_objDTPrintPost.Rows[p_iRowIndex]["TRANS_NUMBER"].ToString());
									if( lCheckNum != 0 )
										sData = string.Format( "{0:######}" , lCheckNum );
									else
										sData = " (None)" ;
								}
								break;
							case 2 :
								if( bLeadingLine )
									sData = Conversion.GetDBDateFormat( p_objDTPrintPost.Rows[p_iRowIndex]["DATE_OF_CHECK"].ToString() , "d" );
								break;
							case 3 :
								if( bLeadingLine )
									sData = p_objDTPrintPost.Rows[p_iRowIndex]["CLAIM_NUMBER"].ToString() ;
								break;
							case 4 :
								if( bLeadingLine )
								{
									if( m_objSysSettings.UseFundsSubAcc )
									{
										iSubAccountId = Common.Conversion.ConvertStrToInteger( p_objDTPrintPost.Rows[p_iRowIndex]["SUB_ACCOUNT_ID"].ToString() );
										objBankAccSub = ( BankAccSub ) m_objDataModelFactory.GetDataModelObject( "BankAccSub" , false );
										try
										{
											objBankAccSub.MoveTo( iSubAccountId );
											bBankAccSubExist = true ;
										}
										catch
										{
											bBankAccSubExist = false ;
										}
										if( bBankAccSubExist )
											sData = objBankAccSub.SubAccName.Trim();
										else
											sData = "N/A" ;
									}
									else
									{
										iClaimId = Common.Conversion.ConvertStrToInteger( p_objDTPrintPost.Rows[p_iRowIndex]["CLAIM_ID"].ToString() );
										sData = GetOrgAbbrevFromClaimID( iClaimId );
									}
								}
								break;
							case 5 :
								if( bLeadingLine )
								{
                                    //MGaba2:MITS 13063:09/01/2008:Added Trim Function:To avoid space in case first name is blank
									sData =( p_objDTPrintPost.Rows[p_iRowIndex]["FIRST_NAME"].ToString() + " " 
										+ p_objDTPrintPost.Rows[p_iRowIndex]["LAST_NAME"].ToString()).Trim() ;
								}
								break;
							case 6 :
								iTransTypeCode = Common.Conversion.ConvertStrToInteger( p_objDTPrintPost.Rows[p_iRowIndex]["TRANS_TYPE_CODE"].ToString() );
								m_objLocalCache.GetCodeInfo( iTransTypeCode , ref sShortCode , ref sCodeDesc );
								sData = sCodeDesc ;
								break;
							case 7 :
								dblAmount = Common.Conversion.ConvertStrToDouble( p_objDTPrintPost.Rows[p_iRowIndex]["AMOUNT"].ToString() );
                                //skhare7 r8
                                  sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblAmount, m_sConnectionString,m_iClientId);//skhare7 r8 
								//sData = string.Format( "{0:C}" , dblAmount );
								break;
							case 8 :
								if( bLeadingLine )
								{
									dblFundsAmount = Common.Conversion.ConvertStrToDouble( p_objDTPrintPost.Rows[p_iRowIndex]["FUNDS_AMOUNT"].ToString() );
                                    sData = CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, dblFundsAmount, m_sConnectionString, m_iClientId);//skhare7 r8 
									//sData = string.Format( "{0:C}" , dblFundsAmount );								
								}
								break;										
						}// end switch

						// Insert 'dittos' of omitted items
						if( sData == "" )
						{
							if( !bLeadingLine )
							{
								if( p_arrPostCheckRegColAlign[iIndex] == CheckConstants.LEFT_ALIGN )
									sData = "   " + "\"" ;
								else
									sData = "\"" + "   " ;
							}
						}

                        //changed by Gagan for mits 10425 :  start

                        while (sData.Length > 0 && m_objPrintWrapper.GetTextWidth(sData) > p_arrPostCheckregColWidth[iIndex])
                            sData = sData.Substring(0, sData.Length - 1);

                        //changed by Gagan for mits 10425 :  end
                        

						// Print using alignment
						if( p_arrPostCheckRegColAlign[iIndex] == CheckConstants.LEFT_ALIGN )
						{
							m_objPrintWrapper.CurrentX = dblCurrentX ;
							m_objPrintWrapper.PrintTextNoCr( " " + sData );
						}
						else
						{
							m_objPrintWrapper.CurrentX = dblCurrentX + p_arrPostCheckregColWidth[iIndex] 
								- m_objPrintWrapper.GetTextWidth( sData) - p_dblCharWidth ;
							m_objPrintWrapper.PrintTextNoCr( sData );
						}
						dblCurrentX += p_arrPostCheckregColWidth[iIndex] ;
						#endregion 

					} // End of Line priniting 

					// Strike sums and go to next record

					if( bNewCheck )
					{
						dblRoundedAmount = Common.Conversion.ConvertStrToDouble( p_objDTPrintPost.Rows[p_iRowIndex]["FUNDS_AMOUNT"].ToString() );
                        dblRoundedAmount = Math.Round(dblRoundedAmount, 2, MidpointRounding.AwayFromZero);
						p_dblPageTotal += dblRoundedAmount ;
						p_dblReportTotal += dblRoundedAmount ;
						p_iPageTotal += 1 ;
					}
					p_dblCurrentY += p_dblLineHeight ;
					p_iRowIndex++;	// Move Next
				}

				if( ( p_iRowIndex >= p_objDTPrintPost.Rows.Count) )
				{
					if( p_dblCurrentY < p_dblBottomHeader - p_dblLineHeight * 2 )
					{
						p_dblCurrentY += p_dblLineHeight ;
						m_objPrintWrapper.CurrentY = p_dblCurrentY ;
						m_objPrintWrapper.CurrentX = p_dblPageWidth/2 + p_dblLeftMargin ;
                        m_objPrintWrapper.PrintText("Total for this report: " + CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, p_dblReportTotal, m_sConnectionString, m_iClientId));//skhare7 r8 
						//m_objPrintWrapper.PrintText( "Total for this report: " + string.Format( "{0:C}" , p_dblReportTotal ) );
						p_dblCurrentY += p_dblLineHeight ;
					}
					bReturnValue = false ;
				}
				else
				{
					if( p_dblCurrentY < p_dblBottomHeader - p_dblLineHeight )
						bReturnValue = true ;
					else
						bReturnValue = false ;
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.PostCheckDetailRegPrtData.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBankAccSub != null )
				{
					objBankAccSub.Dispose();
					objBankAccSub = null ;
				}				
			}
			return( bReturnValue );
		}

		/// Name		: GetOrgAbbrevFromClaimID
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get Org Abbreviation.
		/// </summary>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <returns>Org Abbreviation. string </returns>
		private string GetOrgAbbrevFromClaimID( int p_iClaimId )
		{
			DbReader objReaderDept = null ;
			
			string sSQLDept = "" ;
			string sOrgAbbr = "" ;
			string sOrgName = "" ;
			int iDeptEid = 0 ;

			try
			{				
				sSQLDept = " SELECT EVENT.DEPT_EID, EVENT.DATE_OF_EVENT FROM CLAIM,EVENT WHERE CLAIM.CLAIM_ID = "
					+	p_iClaimId + " AND EVENT.EVENT_ID = CLAIM.EVENT_ID" ;
					
				objReaderDept = DbFactory.GetDbReader( m_sConnectionString , sSQLDept );
	
				if( objReaderDept != null )
				{
					if( objReaderDept.Read() )
					{
						iDeptEid = Common.Conversion.ConvertObjToInt( objReaderDept.GetValue( "DEPT_EID" ), m_iClientId );
						m_objLocalCache.GetOrgInfo( iDeptEid , ref sOrgAbbr ,ref sOrgName );
					}
					//objReaderDept.Close();
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.GetOrgAbbrevFromClaimID.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReaderDept != null )
				{
					objReaderDept.Close();
					objReaderDept.Dispose();
				}
			}
			return( sOrgAbbr );
		}				
		#endregion

		#region Report Headers and Footers

		/// Name		: CheckRegPrintFooter
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Report Footer.
		/// </summary>
		/// <param name="p_iPage">Page Number</param>
		/// <param name="p_iNumPages">Number Of Pages</param>
		/// <param name="p_dblPageTotal">Page Total </param>
		/// <param name="p_dblReportTotal">Report Total</param>
		/// <param name="p_dblCurrentY">Current Y</param>
		/// <param name="p_sRegType">Pre/Post Type</param>
		/// <param name="p_iPageTotal">Total Pages</param>
		/// <param name="p_iCumulativeTotal">Cumulative Total</param>
		/// <param name="p_sRegTypeOptionalLine">Footer String</param>
		/// <param name="p_iSkipPageCount">Skip Page Count</param>
		/// <param name="p_dblCharWidth">Char Width</param>
		/// <param name="p_dblLineHeight">Line Width </param>
		/// <param name="p_dblLeftMargin">Left Margin</param>
		/// <param name="p_dblRightMargin">Right Margin</param>
		/// <param name="p_dblPageWidth">Page Width</param>
		private void CheckRegPrintFooter( int p_iPage , int p_iNumPages , double p_dblPageTotal , 
			double p_dblReportTotal , double p_dblCurrentY ,string p_sRegType , double p_iPageTotal , 
			double p_iCumulativeTotal , string p_sRegTypeOptionalLine , int p_iSkipPageCount , 
			double p_dblCharWidth , double p_dblLineHeight , double p_dblLeftMargin , double p_dblRightMargin , 
			double p_dblPageWidth )
		{
			double dblGapWidth = 0.0 ;
			double dblCenterX = 0.0 ;
			double dblTemp = 0.0 ;
			string sTemp = "" ;			

			try
			{
				dblGapWidth = p_dblCharWidth * 6 ;
				dblCenterX = p_dblLeftMargin + p_dblPageWidth/2 ;
				p_dblCurrentY += p_dblLineHeight ;

				m_objPrintWrapper.PrintLine( p_dblLeftMargin , p_dblCurrentY , dblCenterX - dblGapWidth , p_dblCurrentY );
				m_objPrintWrapper.PrintLine( dblCenterX + dblGapWidth , p_dblCurrentY , p_dblLeftMargin + p_dblPageWidth , p_dblCurrentY );

				p_dblCurrentY -= p_dblLineHeight/2 ;
				m_objPrintWrapper.CurrentY = p_dblCurrentY ;

				if( p_iSkipPageCount == 0 )
				{
                    sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.PageXofY", m_iClientId));
                    sTemp = Functions.ReplaceArguments(sTemp, m_iClientId, string.Format("{0:###}", p_iPage), string.Format("{0:###}", p_iNumPages));
					m_objPrintWrapper.CurrentX = dblCenterX - m_objPrintWrapper.GetTextWidth( sTemp )/2 ;
					m_objPrintWrapper.PrintTextNoCr( sTemp );
				}
				else
				{
                    sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.PageX", m_iClientId));
                    sTemp = Functions.ReplaceArguments(sTemp, m_iClientId, string.Format("{0:###}", p_iPage + p_iSkipPageCount));
					m_objPrintWrapper.CurrentX = dblCenterX - m_objPrintWrapper.GetTextWidth( sTemp )/2 ;
					m_objPrintWrapper.PrintText( sTemp );
				}

				p_dblCurrentY += p_dblLineHeight * 1.5 ;

				m_objPrintWrapper.CurrentX = p_dblLeftMargin ;
				m_objPrintWrapper.CurrentY = p_dblCurrentY ;

				if( p_sRegTypeOptionalLine == "Detail Report" )
                    sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.PagePaymentCount", m_iClientId));
				else
				{
					if( p_sRegTypeOptionalLine == "Sub Account Summary" )
                        sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.PageSubAccounts", m_iClientId));
					else
                        sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.PageCheckCount", m_iClientId));
				}

                sTemp = Functions.ReplaceArguments(sTemp, m_iClientId, string.Format("{0:###}", p_iPageTotal));//MITS 27194 skhare7
               // sTemp = Functions.ReplaceArguments(sTemp, CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, p_iPageTotal, m_sConnectionString));//skhare7 r8 
				m_objPrintWrapper.PrintTextNoCr( sTemp );
				dblTemp = m_objPrintWrapper.GetTextWidth( sTemp );
			
				m_objPrintWrapper.CurrentX = p_dblLeftMargin ;
				m_objPrintWrapper.CurrentY = p_dblCurrentY + p_dblLineHeight ;

				if( p_sRegTypeOptionalLine == "Detail Report" )
                    sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.CumulativePaymentCount", m_iClientId));
				else
				{
					if( p_sRegTypeOptionalLine == "Sub Account Summary" )
                        sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.CumulativeSubAccounts", m_iClientId));
					else
                        sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.CumulativeCheckCount", m_iClientId));
				}
               // sTemp = Functions.ReplaceArguments(sTemp, CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, p_iCumulativeTotal, m_sConnectionString));//skhare7 r8 
                sTemp = Functions.ReplaceArguments(sTemp, m_iClientId, string.Format("{0:###}", p_iCumulativeTotal));//MITS 27194 skhare7
				m_objPrintWrapper.PrintTextNoCr( sTemp );			

				if( dblTemp < m_objPrintWrapper.GetTextWidth( sTemp ) )
					dblTemp = m_objPrintWrapper.GetTextWidth( sTemp );

				dblTemp += p_dblLeftMargin + p_dblCharWidth * 6 ;
			
				m_objPrintWrapper.CurrentX = dblTemp ;
				m_objPrintWrapper.CurrentY = p_dblCurrentY ;
                sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.PageTotal", m_iClientId));
                sTemp = Functions.ReplaceArguments(sTemp, m_iClientId, CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, p_dblPageTotal, m_sConnectionString, m_iClientId));//skhare7 r8 
				//sTemp = Functions.ReplaceArguments( sTemp , string.Format( "{0:C}" , p_dblPageTotal ) );
				m_objPrintWrapper.PrintTextNoCr( sTemp );

				m_objPrintWrapper.CurrentX = dblTemp ;
				m_objPrintWrapper.CurrentY = p_dblCurrentY + p_dblLineHeight ;
                sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.CumulativeTotal", m_iClientId));
                sTemp = Functions.ReplaceArguments(sTemp, m_iClientId, CommonFunctions.ConvertByCurrencyCode(m_objSysSettings.BaseCurrencyType, p_dblReportTotal, m_sConnectionString, m_iClientId));//skhare7 r8 
				//sTemp = Functions.ReplaceArguments( sTemp , string.Format( "{0:C}" , p_dblReportTotal ) );
				m_objPrintWrapper.PrintTextNoCr( sTemp );

				if( p_sRegTypeOptionalLine == "Detail Report" )
                    sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.DetailReport", m_iClientId));
				else
				{
					if( p_sRegTypeOptionalLine == "Sub Account Summary" )
                        sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.SubAccountSummary", m_iClientId));
					else
					{
						if( p_sRegTypeOptionalLine == "Summary Report")
                            sTemp = CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintFooter.SummaryReport", m_iClientId));
						else
							sTemp = p_sRegTypeOptionalLine ;
					}
				} 
				
				dblTemp = m_objPrintWrapper.GetTextWidth( p_sRegType );
				if( dblTemp < m_objPrintWrapper.GetTextWidth( sTemp ) )
					dblTemp = m_objPrintWrapper.GetTextWidth( sTemp );

				m_objPrintWrapper.CurrentY = p_dblCurrentY ;
				m_objPrintWrapper.CurrentX = m_objPrintWrapper.PageWidth - p_dblRightMargin- dblTemp ;
				m_objPrintWrapper.PrintTextNoCr( p_sRegType );

				m_objPrintWrapper.CurrentY = p_dblCurrentY + p_dblLineHeight ;
				m_objPrintWrapper.CurrentX = m_objPrintWrapper.PageWidth - p_dblRightMargin - dblTemp ;
				m_objPrintWrapper.PrintTextNoCr( sTemp );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.CheckRegPrintFooter.Error", m_iClientId), p_objEx);				
			}			
		}
		/// Name		: CheckRegPrintHeader
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Print the Report Header
		/// </summary>
		/// <param name="p_iBatchNum">Batch Number</param>
		/// <param name="p_dblCurrentY">Current Y</param>
		/// <param name="p_sDate">Check Date</param>
		/// <param name="p_iAccountId">Account Id</param>
		/// <param name="p_iColumnCount">Column Count</param>
		/// <param name="p_iClaimId">Claim Id</param>
		/// <param name="p_dblPageWidth">Page Width</param>
		/// <param name="p_dblLeftMargin">Left Margin</param>
		/// <param name="p_dblLineHeight">Line Height</param>
		/// <param name="p_arrCheckRegCol">Column Text</param>
		/// <param name="p_arrCheckRegColWidth">Columns Width</param>
		/// <param name="p_arrCheckRegColAlign">Columns Text Alignment Option</param>
		private void CheckRegPrintHeader( int p_iBatchNum , ref double p_dblCurrentY , string p_sDate , 
			int p_iAccountId , int p_iColumnCount , int p_iClaimId , double p_dblPageWidth , 
			double p_dblLeftMargin , double p_dblLineHeight , string[,] p_arrCheckRegCol , 
			double[] p_arrCheckRegColWidth , string[] p_arrCheckRegColAlign )
		{
			// Pre and post-check register print routine.
			// Prints the top header of the check register page.		
			// p_dblCurrentY : Starting Y position on page of header
			
			Functions objFunctions = null ;

			string[] arrLine = new string[3] ;
			double dblCenterX = 0.0 ;
			double dblCurrentX = 0.0 ;
			int iIndex = 0 ;
			int iIndexJ = 0 ;
			
			try
			{
				objFunctions = new Functions( m_objDataModelFactory );
			
				if( m_objSysSettings.ClientName != "" )
					arrLine[0] = m_objSysSettings.ClientName ;
				else
                    arrLine[0] = "(" + CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintHeader.CompanyNameMissing", m_iClientId)) + ")";

                arrLine[1] = Functions.ReplaceArguments(CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintHeader.BankAccount", m_iClientId)), m_iClientId, objFunctions.AccountName(p_iAccountId,m_iClientId));
                arrLine[2] = Functions.ReplaceArguments(CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintHeader.Batch", m_iClientId)), m_iClientId, p_iBatchNum.ToString())
                    + "     " + Functions.ReplaceArguments(CommonFunctions.FilterBusinessMessage(Globalization.GetString("CheckRegister.CheckRegPrintHeader.RunDate", m_iClientId)), m_iClientId, p_sDate);

				dblCenterX = p_dblPageWidth/2 + p_dblLeftMargin ;

				// Print 3 centered lines of page header
				for( iIndex = 0 ; iIndex < 3 ; iIndex++ )
				{
					m_objPrintWrapper.CurrentX = dblCenterX - m_objPrintWrapper.GetTextWidth( arrLine[iIndex] )/2 ;
					m_objPrintWrapper.CurrentY = p_dblCurrentY  ;
					m_objPrintWrapper.PrintTextNoCr( arrLine[iIndex] );
					p_dblCurrentY += p_dblLineHeight ;
				}

				// Space down by 1 line
				p_dblCurrentY += p_dblLineHeight ;
				dblCurrentX = p_dblLeftMargin ;

				// Print the p_iColumnCount column headings; for each, print the 2 lines of column header. Also, print
				// divider lines between each column
				for( iIndex = 0 ; iIndex < p_iColumnCount ; iIndex++ )
				{
					for( iIndexJ = 0 ; iIndexJ < 2 ; iIndexJ++ )
					{
						m_objPrintWrapper.CurrentY = p_dblCurrentY + ( iIndexJ * p_dblLineHeight ) ;
						m_objPrintWrapper.CurrentX = dblCurrentX + ( p_arrCheckRegColWidth[iIndex]/2 ) 
							- ( m_objPrintWrapper.GetTextWidth( p_arrCheckRegCol[iIndex, iIndexJ ] )/2 );
						m_objPrintWrapper.PrintText( p_arrCheckRegCol[iIndex, iIndexJ ] );
					}
					dblCurrentX += p_arrCheckRegColWidth[iIndex] ;
					if( iIndex != p_iColumnCount -1 )
						m_objPrintWrapper.PrintLine( dblCurrentX , p_dblCurrentY , dblCurrentX , p_dblCurrentY + 2 * p_dblLineHeight );
				}
				p_dblCurrentY += p_dblLineHeight * 2 ;
				
				m_objPrintWrapper.PrintLine( p_dblLeftMargin , p_dblCurrentY , p_dblLeftMargin + p_dblPageWidth , p_dblCurrentY );
				p_dblCurrentY += p_dblLineHeight ;

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("CheckRegister.CheckRegPrintHeader.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                objFunctions = null;
			}			
		}

        /// <summary>
        /// Nikhil - Retrieve deductible collection
        /// </summary>
        /// <param name="p_oFundsTable"></param>
       /// <returns></returns>
        private DataTable AddDeductibleRecCollection(DataTable p_oFundsTable)
        {
            Funds oFunds = null;
            FundsAuto oFundsAuto = null;
            Dictionary<int, List<FundsTransSplit>> dNewSplits = null;
            List<FundsTransSplit> lSplits = null;
            HashSet<string> hSetPolicyUnit = null;

            double dAmt = 0d;
            bool bSuccess = false;
            int iSplitRowId = 0;
            int iTransNumber = 0;
            double dSplitAmt = 0d;
            int iTransTypeCode = 0;
            int iFPDedTypeCode = 0;
         
            int iDedTypeCode = 0;
          

            string sCtlNumber = string.Empty;
            string sCtlNumberD = string.Empty;
            int iReserveTypeCode = 0;
            double dRecoveryAmt = 0d;
            int iClaimID = 0;
            int iCvgRowId = 0;
            string sSplitRowIdName = string.Empty;
            DataTable dtNewFunds = new DataTable();
            string sDedRecoveryIdentifierChar = string.Empty;       //Declared by Sumit Agarwal on 10/13/2014 to store the Deductible Recovery Identifier Char

            iFPDedTypeCode = m_objLocalCache.GetCodeId("FP", "DEDUCTIBLE_TYPE");
            oFunds = m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
            oFundsAuto = m_objDataModelFactory.GetDataModelObject("FundsAuto", false) as FundsAuto;
            dNewSplits = new Dictionary<int, List<FundsTransSplit>>();
            hSetPolicyUnit = new HashSet<string>();
            dtNewFunds = p_oFundsTable.Copy();
            for (int i = 0; i < p_oFundsTable.Rows.Count; i++)
            {
                iDedTypeCode = 0;

                sCtlNumber = string.Empty;
                sCtlNumberD = string.Empty;
                StringBuilder sbSQL = null;
                DbReader objDBReader = null;
                iReserveTypeCode = 0;
                dRecoveryAmt = 0d;
                iClaimID = Convert.ToInt32(p_oFundsTable.Rows[i]["CLAIM_ID"]);
                iCvgRowId = Convert.ToInt32(p_oFundsTable.Rows[i]["POLCVG_ROW_ID"]);
                //Check Ded_type_code. If not first party then no negative split will get created.
                iDedTypeCode = m_objDataModelFactory.Context.DbConn.ExecuteInt(string.Format("SELECT DED_TYPE_CODE FROM CLAIM_X_POL_DED WHERE CLAIM_ID={0} AND POLCVG_ROW_ID={1}", iClaimID, iCvgRowId));
                               
                if (iDedTypeCode != iFPDedTypeCode)
                {
                    continue;
                }


                sCtlNumber = Convert.ToString(p_oFundsTable.Rows[i]["CTL_NUMBER"]);

                //Start: Changed the following code by Sumit Agarwal: 10/13/2014
                //sCtlNumberD = sCtlNumber + "_D";
                //sDedRecoveryIdentifierChar = m_objDataModelFactory.Context.DbConnLookup.ExecuteString(string.Format("SELECT DED_RECOVERY_IDENTIFIER_CHAR FROM SYS_PARMS_LOB WHERE LINE_OF_BUS_CODE={0}", Common.Conversion.ConvertStrToInteger(p_oFundsTable.Rows[0]["LINE_OF_BUS_CODE"].ToString())));
                int iLineOfBusCode = m_objDataModelFactory.Context.DbConnLookup.ExecuteInt("SELECT LINE_OF_BUS_CODE FROM CLAIM WHERE CLAIM_ID=" + iClaimID);
                sDedRecoveryIdentifierChar = m_objDataModelFactory.Context.InternalSettings.ColLobSettings[iLineOfBusCode].DedRecoveryIdentifierChar;
                sCtlNumberD = sCtlNumber + "_" + sDedRecoveryIdentifierChar;
                //End: Changed the following code by Sumit Agarwal: 10/13/2014

                sbSQL = new StringBuilder();

                // sbSQL.Append(" SELECT FUNDS.FIRST_NAME, FUNDS.LAST_NAME, FUNDS.DATE_OF_CHECK,");
                sbSQL.Append(" SELECT FUNDS_TRANS_SPLIT.SPLIT_ROW_ID, FUNDS_TRANS_SPLIT.TRANS_TYPE_CODE,FUNDS_TRANS_SPLIT.RESERVE_TYPE_CODE,");
                sbSQL.Append(" FUNDS_TRANS_SPLIT.AMOUNT,");
               // sbSQL.Append(" FUNDS_TRANS_SPLIT.TRANS_ID");
                //  sbSQL.Append(" FUNDS.CTL_NUMBER, FUNDS.CLAIM_NUMBER,");
                  sbSQL.Append(" FUNDS.CLAIM_ID, FUNDS.TRANS_NUMBER");
                //   sbSQL.Append(" FUNDS.AUTO_CHECK_FLAG, FUNDS.PAYEE_TYPE_CODE");
                // sbSQL.Append(", FUNDS.PAYEE_EID");
                sbSQL.Append(" FROM FUNDS,FUNDS_TRANS_SPLIT ");
                sbSQL.Append(" WHERE FUNDS.CTL_NUMBER = '" + sCtlNumberD + "'");
                sbSQL.Append(" AND FUNDS.TRANS_ID = FUNDS_TRANS_SPLIT.TRANS_ID");

                using (objDBReader = DbFactory.ExecuteReader(m_sConnectionString, sbSQL.ToString()))
                {
                    while (objDBReader.Read())
                    {

                        DataRow[] rows = dtNewFunds.Select("FUNDS_TRANS_ID=" + p_oFundsTable.Rows[i]["FUNDS_TRANS_ID"]);

                        dAmt = Conversion.CastToType<double>(Convert.ToString(p_oFundsTable.Rows[i]["FUNDS_AMOUNT"]), out bSuccess);

                        //tanwar2 - Negative split changed to collection - start
                        //dAmt += oSplit.Amount;
                        iReserveTypeCode = Conversion.CastToType<int>(objDBReader["RESERVE_TYPE_CODE"].ToString(), out bSuccess);
                        dRecoveryAmt = Conversion.CastToType<double>(objDBReader["AMOUNT"].ToString(), out bSuccess);
                        if (m_objLocalCache.GetRelatedCodeId(iReserveTypeCode) ==
                                m_objLocalCache.GetCodeId("R", "MASTER_RESERVE"))
                        {
                            dAmt -= dRecoveryAmt;
                        }
                        else
                        {
                            dAmt += dRecoveryAmt;
                        }
                        //tanwar2 - end

                        //take backup
                        iSplitRowId = Conversion.CastToType<int>(Convert.ToString(rows[0]["SPLIT_ROW_ID"]), out bSuccess);
                        dSplitAmt = Conversion.CastToType<double>(Convert.ToString(rows[0]["AMOUNT"]), out bSuccess);
                        iTransTypeCode = Conversion.CastToType<int>(Convert.ToString(rows[0]["TRANS_TYPE_CODE"]), out bSuccess);
                        iTransNumber = Conversion.CastToType<int>(Convert.ToString(rows[0]["TRANS_NUMBER"]), out bSuccess);
                        //update column values
                        rows[0]["SPLIT_ROW_ID"] = -1;

                        //Start- Added by nikhil on 07/30/14 ,show negative amount for recovery on check
                        //   rows[0]["AMOUNT"] = oSplit.Amount;

                        if (m_objLocalCache.GetRelatedCodeId(iReserveTypeCode) ==
                              m_objLocalCache.GetCodeId("R", "MASTER_RESERVE"))
                        {
                            rows[0]["AMOUNT"] = -1 * dRecoveryAmt;
                        }
                        else
                        {
                            rows[0]["AMOUNT"] = dRecoveryAmt;
                        }
                        //end- Added by nikhil on 07/30/14 ,show negative amount for recovery on check

                        rows[0]["FUNDS_AMOUNT"] = dAmt;
                        rows[0]["TRANS_TYPE_CODE"] = Conversion.CastToType<int>(objDBReader["TRANS_TYPE_CODE"].ToString(), out bSuccess);
                        rows[0]["CTL_NUMBER"] = sCtlNumberD;
                        rows[0]["TRANS_NUMBER"] = Conversion.CastToType<int>(objDBReader["TRANS_NUMBER"].ToString(), out bSuccess);
                      //  rows[0]["FUNDS_TRANS_ID"] = Conversion.CastToType<int>(objDBReader["TRANS_ID"].ToString(), out bSuccess);

                        //Insert Row in table as a new row
                        dtNewFunds.ImportRow(rows[0]);

                        //Restore the original row
                        rows[0]["SPLIT_ROW_ID"] = iSplitRowId;
                        rows[0]["AMOUNT"] = dSplitAmt;
                        rows[0]["TRANS_TYPE_CODE"] = iTransTypeCode;
                        rows[0]["CTL_NUMBER"] = sCtlNumber;
                        rows[0]["TRANS_NUMBER"] = iTransNumber;
                        //Update Funds amount of all corresponding rows
                        foreach (DataRow row in rows)
                        {
                            row["FUNDS_AMOUNT"] = dAmt;
                        }
                    }
                }
            }
            dtNewFunds.DefaultView.Sort = "CTL_NUMBER ASC";
            return dtNewFunds.DefaultView.ToTable();


      
        }
		#endregion 

        private bool _isDisposed = false;
		#region Dispose
		/// Name		: Dispose
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// UnInitialize the objects. 
		/// </summary>
		public void Dispose()
		{
            if (!_isDisposed)
            {
                try
                {
                    _isDisposed = true;

                    if (m_objDataModelFactory != null)
                    {
                        m_objDataModelFactory.Dispose();
                        m_objDataModelFactory = null;
                    }
                    if (m_objLocalCache != null)
                    {
                        m_objLocalCache.Dispose();
                        m_objLocalCache = null;
                    }
                    GC.SuppressFinalize(this);
                }
                catch
                {
                    // Avoid raising any exception here.				
                }
            }
		}
		#endregion 
        /// <summary>
        /// This function is to check the null in transaction date.
        /// rsushilaggar MITS 37250 11/18/2014
        /// </summary>
        /// <param name="p_objInputDoc"></param>
        /// <returns></returns>
        public bool CheckTransDateOnPrintedChecks(XmlDocument p_objInputDoc)
        {
            DataSet objDSPrintPre = null;
            DataTable objDTPrintPre = null;
            CheckRegister objCheckregister = null;

            int iAccountId = 0;
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            int iOrgHierarchyLevel = 0;
            int iNumAutoChecksToPrint = 0;
            //string sFooter = "";
            string sSQL = "";
            //string sDate = "";
            string sFromDate = "";
            string sToDate = "";
            string sOrderByField = "";
            string sFromDateValue = "";
            string sToDateValue = "";
            string sErrorText = "";
            //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
            string sOrgHierarchy = "";
            bool bReturnValue = false;
            bool bIncludeAutoPays = false;
            bool bWithAttachedClaimsOnly = false;
            bool bUseFromDate = false;
            bool bUseToDate = false;
            bool bUsingSelections = false;
            bool bAccountFound = false;
            bool bSkipRegChecks = false;
            bool bMergeAutoChecks = false;
            bool bAutoTrans = false;
            //skhare7 R8 enhancement
            bool bIncludeCombinedPays = false;
            //end 
            string sTransIDs = string.Empty;
            // npadhy JIRA 6418 Starts  - Distribution Type in Funds and using it for printing checks
            int iDistributionType = 0;
            // npadhy JIRA 6418 Ends  - Distribution Type in Funds and using it for printing checks
            try
            {
                bUseFromDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "FromDateFlag", m_iClientId));
                bUseToDate = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "ToDateFlag", m_iClientId));
                sFromDateValue = Functions.GetValue(p_objInputDoc, "FromDate", m_iClientId);
                sToDateValue = Functions.GetValue(p_objInputDoc, "ToDate", m_iClientId);
                //Start Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                sOrgHierarchy = Functions.GetValue(p_objInputDoc, "OrghierarchyPre", m_iClientId);
                iOrgHierarchyLevel = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "OrghierarchyLevelPre", m_iClientId));
                //End Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010
                bIncludeAutoPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeAutoPayment", m_iClientId));
                //skhare7 R8 enhancement
                bIncludeCombinedPays = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "IncludeCombinedPayment", m_iClientId));
                //skhare7 R8
                iAccountId = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "AccountId", m_iClientId));
                bWithAttachedClaimsOnly = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "AttachedClaimOnly", m_iClientId));
                bUsingSelections = Common.Conversion.ConvertStrToBool(Functions.GetValue(p_objInputDoc, "UsingSelection", m_iClientId));
                sTransIDs = Functions.GetValue(p_objInputDoc, "TransIds", m_iClientId);
                sOrderByField = Functions.GetValue(p_objInputDoc, "OrderBy", m_iClientId);

                sOrderByField = Functions.GetSortOrder(sOrderByField, m_iClientId);
                // npadhy JIRA 6418 Starts  - Distribution Type in Funds and using it for printing checks
                iDistributionType = Common.Conversion.ConvertStrToInteger(Functions.GetValue(p_objInputDoc, "DistributionType", m_iClientId));
                // npadhy JIRA 6418 Ends  - Distribution Type in Funds and using it for printing checks


                #region Insure that atleast one check is there to print ELSE exit.
                //Debabrata Biswas Batch Printing Filter MITS 19715/20050 Date : 03/17/2010 Added parameters(OrgHierarchy, iOrgHierarchyLevel)
                //sSQL = this.CheckPrintSQLPreCheck(sFromDate, sToDate, bIncludeAutoPays, iAccountId, sOrderByField, false, bWithAttachedClaimsOnly);
                //npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                sSQL = this.CheckPrintSQLPreCheck(sFromDate, sToDate, bIncludeAutoPays, iAccountId, sOrgHierarchy, iOrgHierarchyLevel, sOrderByField, false, bWithAttachedClaimsOnly, bIncludeCombinedPays, iDistributionType);
                //npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and removing out EFTPayment below
                if (bUsingSelections && !string.IsNullOrEmpty(sTransIDs))
                {
                    string sTemp = sSQL.Substring(0, sSQL.IndexOf("ORDER BY"));
                    string sTempOrderby = sSQL.Substring(sSQL.IndexOf("ORDER BY"), sSQL.Length - sSQL.IndexOf("ORDER BY"));

                    sTemp = sTemp + " AND FUNDS.TRANS_ID IN (" + sTransIDs + ") ";
                    sSQL = sTemp + sTempOrderby;
                }
                objDSPrintPre = DbFactory.GetDataSet(m_sConnectionString, sSQL, m_iClientId);
                objDTPrintPre = objDSPrintPre.Tables[0];

                if (objDTPrintPre.Rows.Count > 0)
                {
                    //Start - rsusilaggar Print pre check validation
                    string sCtlNumber = string.Empty;
                    for (int i = 0; i < objDTPrintPre.Rows.Count; i++)
                    {
                        // akaushik5 Changed for MITS 37250 Starts
                        //if (objDTPrintPre.Rows[i]["TRANS_DATE"] == null || objDTPrintPre.Rows[i]["TRANS_DATE"] == "")
                        if (objDTPrintPre.Rows[i]["TRANS_DATE"].Equals(DBNull.Value) || objDTPrintPre.Rows[i]["TRANS_DATE"] == "")
                        // akaushik5 Changed for MITS 37250 Ends
                        {
                            if (string.IsNullOrEmpty(sCtlNumber))
                            {
                                sCtlNumber = objDTPrintPre.Rows[i]["CTL_NUMBER"].ToString();
                            }
                            else
                            {
                                sCtlNumber = sCtlNumber + ",  " + objDTPrintPre.Rows[i]["CTL_NUMBER"];
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(sCtlNumber))
                    {
                        //sErrorText = Globalization.GetString("CheckRegister.PreCheckDetail.NoCheckFound");
                        sErrorText = sCtlNumber + "; These control numbers are having null value in Transaction date. Please set the values in the transaction date first or exclude those from list.";
                        throw new RMAppException(sErrorText);
                    }
                    //End - rsushilaggar

                }
                #endregion

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckRegister.PreCheckDetail.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDSPrintPre != null)
                {
                    objDSPrintPre.Dispose();
                    objDSPrintPre = null;
                }
                if (objDTPrintPre != null)
                {
                    objDTPrintPre.Dispose();
                    objDTPrintPre = null;
                }
            }
            return (bReturnValue);


        }
		
	}
}
