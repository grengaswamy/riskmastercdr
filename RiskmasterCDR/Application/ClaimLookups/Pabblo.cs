﻿
using System;
using System.Text;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.ClaimLookups
{
	///************************************************************** 
	///* $File		: Pabblo.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 08-May-2006
	///* $Author	: Parag Sarin & Anurag Agarwal
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class works as the interface between Pabblo & RM X.
	/// </summary>
	public class Pabblo
	{
		public struct CoverageData
		{
			/// <summary>
			/// Stores the occrlmt value
			/// </summary>
			public Double OccrLmt;

			/// <summary>
			/// Stores the claimlmtcode value
			/// </summary>
			public string ClaimLmtCode;

			/// <summary>
			/// Stores the claimlmt value
			/// </summary>
			public Double ClaimLmt;

			/// <summary>
			/// Stores the deductval value
			/// </summary>
			public Double DeductVal;

			/// <summary>
			/// Stores the deductcode value
			/// </summary>
			public string DeductCode;

			/// <summary>
			/// Stores the minorlobcode value
			/// </summary>
			public string MinorLOBCode;

			/// <summary>
			/// Stores the insertcvgdata value
			/// </summary>
			public bool InsertCvgData;

			/// <summary>
			/// Stores the limitdesc value
			/// </summary>
			public string LimitDesc;
			

		}
		public struct PolicyMaster
		{
			/// <summary>
			/// Stores the policy_num value
			/// </summary>
			public string Policy_Num;

			/// <summary>
			/// Stores the policy_id value
			/// </summary>
			public int Policy_ID;

			/// <summary>
			/// Stores the policy_status value
			/// </summary>
			public string POLICY_STATUS;

			/// <summary>
			/// Stores the pscode value
			/// </summary>
			public int PSCode;

			/// <summary>
			/// Stores the policy_premium value
			/// </summary>
			public double Policy_Premium;

			/// <summary>
			/// Stores the effective_date value
			/// </summary>
			public string Effective_Date;

			/// <summary>
			/// Stores the expire_date value
			/// </summary>
			public string Expire_Date;

			/// <summary>
			/// Stores the change_eff_date value
			/// </summary>
			public string Change_Eff_Date;

			/// <summary>
			/// Stores the lobcode value
			/// </summary>
			public string LOBCode;

			/// <summary>
			/// Stores the cvgtypecode value
			/// </summary>
			public int CvgTypeCode;

			/// <summary>
			/// Stores the iscoverageexist value
			/// </summary>
			public bool IsCoverageExist;

			/// <summary>
			/// Stores the cvginfo value
			/// </summary>
			public CoverageData CvgInfo;

			/// <summary>
			/// Stores the endorsenum value
			/// </summary>
			public int EndorseNum;
			
		}
		public struct CvgDataFldInfo
		{
			/// <summary>
			/// Stores the limittbl value
			/// </summary>
			public string LimitTbl;

			/// <summary>
			/// Stores the limitfld value
			/// </summary>
			public string LimitFld;

			/// <summary>
			/// Stores the deducttable value
			/// </summary>
			public string DeductTable;

			/// <summary>
			/// Stores the deductfld value
			/// </summary>
			public string DeductFld;
		}
		/// <summary>
		/// Stores the policy status code Group id
		/// </summary>
		public const long  conPSGrpID = 1051;
		/// <summary>
		/// Stores the policy data
		/// </summary>
		public PolicyMaster curPolicy;
		/// <summary>
		/// Stores the policy coverage field data
		/// </summary>
		private CvgDataFldInfo curCvgDataFld;
		/// <summary>
		/// Stores the Connection String for DB
		/// </summary>
		private string m_sConnectString="";
		/// <summary>
		/// Stores the Data source name
		/// </summary>
		private string m_sDBName="";
		/// <summary>
		/// Stores the user name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Stores the password
		/// </summary>
		private string m_sPwd="";
		/// <summary>
		/// Stores the bool for checking Is Pabblo CodeMap Exist
		/// </summary>
		private bool m_bIsPSMaped = false;
        private int m_iClientId = 0;

		/// <summary>
		/// Stores the Connection String for DB
		/// </summary>
		public string DSN
		{
			set
			{
				m_sConnectString=value;
			}
		}
		/// <summary>
		/// Stores the Data source name
		/// </summary>
		public string Database
		{
			set
			{
				m_sDBName=value;
			}
		}
		/// <summary>
		/// Stores the user name
		/// </summary>
		public string UserName
		{
			set
			{
				m_sUserName=value;
			}
		}
		/// <summary>
		/// Stores the password
		/// </summary>
		public string Pwd
		{
			set
			{
				m_sPwd=value;
			}
		}

        /// <summary>
        /// Stores the pabblo connection string
        /// </summary>
        private string PABBLO_DSN
        {
            set
            {
                m_sPabbloConnectString = value;
            }
        }

		/// <summary>
		/// Stores the pabblo connection string
		/// </summary>
        private string m_sPabbloConnectString = string.Empty;
        private const string PABBLO_GC = "241";
        private const string PABBLO_VA = "242";
		
		/// Name		: ClearCurPolData
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Clears the policy data in the structure curPolicy
		/// </summary>
		private void ClearCurPolData()
		{
			curPolicy.Policy_Num = "";
			curPolicy.Policy_ID = 0;
			curPolicy.POLICY_STATUS = "";
			curPolicy.PSCode = 0;
			curPolicy.Policy_Premium = 0;
			curPolicy.Effective_Date = "";
			curPolicy.Expire_Date = "";
			curPolicy.Change_Eff_Date = "";
			curPolicy.EndorseNum = 0;
			curPolicy.LOBCode = "";
			curPolicy.CvgTypeCode = 0;
			curPolicy.IsCoverageExist = false;
			curPolicy.CvgInfo.ClaimLmt = 0;
			curPolicy.CvgInfo.ClaimLmtCode = "";
			curPolicy.CvgInfo.DeductCode = "";
			curPolicy.CvgInfo.DeductVal = 0;
			curPolicy.CvgInfo.OccrLmt = 0;
			curPolicy.CvgInfo.MinorLOBCode = "";
			curPolicy.CvgInfo.LimitDesc = ""   ;      
			curPolicy.CvgInfo.InsertCvgData = false;
		}
		/// Name		: Pabblo
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Constructor for the class
		/// </summary>
		/// <param name="p_sConnString">connection string</param>
		public Pabblo(string p_sConnString,int p_iClientId)//sonali
		{
			m_sConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			curCvgDataFld = new CvgDataFldInfo();
			curPolicy = new PolicyMaster();
			ClearCurPolData();
			m_bIsPSMaped = IsPabbloCodeMapExist();
		}
		/// Name		: GetPabbloDBType
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Sets the DSN in m_sPabbloConnectString based on Lob code
		/// </summary>
		/// <param name="p_sLobCode">LOB</param>
		public void GetPabbloDBType(string p_sLobCode)
		{
			switch(p_sLobCode)
			{
				case PABBLO_GC:
					m_sPabbloConnectString=RMConfigurationManager.GetConnectionString("PabbloGC",m_iClientId);//sonali
					break;
				case PABBLO_VA:
                    m_sPabbloConnectString = RMConfigurationManager.GetConnectionString("PabbloVA", m_iClientId);
					break;
				default:
                    m_sPabbloConnectString = RMConfigurationManager.GetConnectionString("PabbloWC", m_iClientId);
					break;

			}

		}
		/// Name		: InsertPolicyData
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Inserts the policy Data into DB
		/// </summary>
		/// <param name="p_lDeptEID">Mapped Dept id</param>
		public void InsertPolicyData(int p_lDeptEID)
		{
			string sSQL=string.Empty;
			Policy objPolicy=null;
			PolicyXCvgType objPolicyXCvgType=null;
			LocalCache objCache=null;
			DataModelFactory objFactory=null;
			bool bIsFound = false;

			try
			{
				if (IsPolicyExist(curPolicy.Policy_Num,curPolicy.Policy_ID))
				{
					if (curPolicy.Policy_ID==0)
					{
						return;
					}
					else
					{
						objFactory=new DataModelFactory(m_sDBName,m_sUserName,m_sPwd,m_iClientId);//sonali
						if(objFactory!=null)
						{
							objPolicy= (Policy)objFactory.GetDataModelObject("Policy",false);
							if (objPolicy!=null)
								objPolicy.MoveTo(curPolicy.Policy_ID);

						}
					
					}
				}
				else
				{
					objFactory=new DataModelFactory(m_sDBName,m_sUserName,m_sPwd,m_iClientId);//sonali
					if(objFactory!=null)
					{
						objPolicy= (Policy)objFactory.GetDataModelObject("Policy",false);
					}
				}
				objPolicy.PolicyNumber=curPolicy.Policy_Num;
				objPolicy.PolicyName=curPolicy.Policy_Num;
				objPolicy.Premium=curPolicy.Policy_Premium;
				if (curPolicy.Effective_Date.Length > 0)
				{
					objPolicy.EffectiveDate=curPolicy.Effective_Date;
				}
				if (curPolicy.Expire_Date.Length > 0)
				{
					objPolicy.ExpirationDate=curPolicy.Expire_Date;
				}
				if (curPolicy.Change_Eff_Date.Length > 0)
				{
					objPolicy.CancelDate=curPolicy.Change_Eff_Date;
				}
				objPolicy.PrimaryPolicyFlg=true;
				objPolicy.PolicyStatusCode=curPolicy.PSCode;
			
				// JP 12/7/2006: Moved into GetPolicyData where it belongs.     objCache=new LocalCache(m_sConnectString);
				// JP 12/7/2006: Moved into GetPolicyData where it belongs.     curPolicy.CvgTypeCode=objCache.GetCodeId(curPolicy.LOBCode,Conversion.ConvertObjToStr(objCache.GetTableId("COVERAGE_TYPE")));
				// JP 12/7/2006: Moved into GetPolicyData where it belongs.     objCache.Dispose();

				if (curPolicy.CvgTypeCode >0  && curPolicy.Policy_ID >0)
				{
					foreach(PolicyXCvgType objTempPolicyXCvgType in objPolicy.PolicyXCvgTypeList)
					{
						if(objTempPolicyXCvgType.CoverageTypeCode == curPolicy.CvgTypeCode)
						{
							InsertCoverageData(objTempPolicyXCvgType);  
							bIsFound = true;
						}
					}
					if(!bIsFound)
					{
						objPolicyXCvgType=(PolicyXCvgType)objFactory.GetDataModelObject("PolicyXCvgType",false);
						InsertCoverageData(objPolicyXCvgType);
						objPolicy.PolicyXCvgTypeList.Add(objPolicyXCvgType);
					}
				}
				else if (curPolicy.CvgTypeCode >0  && curPolicy.Policy_ID == 0) // new policy download
				{
					objPolicyXCvgType=(PolicyXCvgType)objFactory.GetDataModelObject("PolicyXCvgType",false);
					InsertCoverageData(objPolicyXCvgType);
					objPolicy.PolicyXCvgTypeList.Add(objPolicyXCvgType);
				}
			
				objPolicy.Save();

				curPolicy.Policy_ID=objPolicy.PolicyId;
				try
				{
					objFactory.Context.DbConn.ExecuteNonQuery("INSERT INTO POLICY_X_INSURED(POLICY_ID,INSURED_EID) VALUES (" + curPolicy.Policy_ID + "," + p_lDeptEID + ")");
				}
				catch{}

			}
			catch(Exception p_Objex)
			{
				throw new RMAppException(Globalization.GetString("Pabblo.InsertPolicyData.Error",m_iClientId), p_Objex);//sonali				
			}
			finally
			{
				if (objFactory!=null)
					objFactory.Dispose();

                if (objPolicy != null)
                    objPolicy.Dispose();

                if (objPolicyXCvgType != null)
                    objPolicyXCvgType.Dispose();

				if (objCache!=null)
					objCache.Dispose();

			}

		}
		/// Name		: InsertCoverageData
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Fills the coverage Data struct CvgInfo
		/// </summary>
		/// <param name="p_objPolicyXCvgType">Struct to fill data</param>
		private void InsertCoverageData(PolicyXCvgType p_objPolicyXCvgType)
		{
//			if (!curPolicy.CvgInfo.InsertCvgData)
//				return;
			p_objPolicyXCvgType.CoverageTypeCode=curPolicy.CvgTypeCode;
			p_objPolicyXCvgType.PolicyId=curPolicy.Policy_ID;
			if (curCvgDataFld.LimitTbl =="ZERO")
				p_objPolicyXCvgType.PolicyLimit=0;
			else if (curCvgDataFld.LimitTbl !="NA" && curCvgDataFld.LimitTbl !="")
			{
				p_objPolicyXCvgType.PolicyLimit=curPolicy.CvgInfo.ClaimLmt;
				p_objPolicyXCvgType.BrokerName=curPolicy.CvgInfo.LimitDesc;
			}
			if (curCvgDataFld.DeductTable =="ZERO")
				p_objPolicyXCvgType.SelfInsureDeduct=0;
			else if (curCvgDataFld.LimitTbl !="NA" && curCvgDataFld.LimitTbl !="")
			{
				p_objPolicyXCvgType.SelfInsureDeduct=curPolicy.CvgInfo.DeductVal;
			}
		}
		/// Name		: IsPolicyExist
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Checks if Policy Exists or not
		/// </summary>
		/// <param name="p_sPolicyNum">Policy Number</param>
		/// <param name="p_lPolicyId">Policy Id</param>
		/// <returns>Returns true if policy is retreived else false</returns>
		public bool IsPolicyExist(string p_sPolicyNum,int p_lPolicyId)
		{
			string sSQL=string.Empty;
			bool bReturn=false;
			DbReader objRead=null;
			sSQL="Select POLICY_ID, POLICY_NUMBER From POLICY Where POLICY_NUMBER = '" + p_sPolicyNum + "'";
			try
			{
				objRead=DbFactory.GetDbReader(m_sConnectString,sSQL);
				if (objRead.Read())
				{
					curPolicy.Policy_ID=Conversion.ConvertObjToInt(objRead.GetValue(0), m_iClientId);
					bReturn=true;
				}
				else
				{
					bReturn=false;
				}
				objRead.Close();
			}
			catch(Exception p_Objex)
			{
				throw new RMAppException(Globalization.GetString("Pabblo.IsPolicyExist.Error",m_iClientId), p_Objex);		//sonali		
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
					objRead.Dispose();
				}
			}
			return bReturn;
		}
		/// Name		: GetPSMapVal
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Get Policy Status(es) mapped values
		/// </summary>
		/// <param name="p_sCode">Status code</param>
		/// <returns>Returns the RM specific status code</returns>
		public int GetPSMapVal(string p_sCode)
		{
			string sSQL=string.Empty;
			int iCode=0;
			DbReader objRead=null;
			sSQL="SELECT RM_CODE_ID, PBL_CODE FROM PABBLO_CODE_MAP WHERE GROUP_ID = " + conPSGrpID + " AND PBL_CODE = '" + p_sCode + "'";
			try
			{
				objRead=DbFactory.GetDbReader(m_sConnectString,sSQL);
				if (objRead.Read())
				{
					iCode=Conversion.ConvertObjToInt(objRead.GetValue(0), m_iClientId);
				}
				if (objRead!=null)
					objRead.Close();
			}
			catch(Exception p_Objex)
			{
				throw new RMAppException(Globalization.GetString("Pabblo.GetPSMapVal.Error",m_iClientId), p_Objex);		//sonali		
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
					objRead.Dispose();
				}
			}
			return iCode;
		}
		/// Name		: GetPabbloMemNum
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Gets Pabblo Member number
		/// </summary>
		/// <param name="p_iEID">Entity Id</param>
		/// <returns>Returns the Pabblo Member number</returns>
		public string GetPabbloMemNum(int p_iEID)
		{
			string sSQL=string.Empty;
			// int iCode=0;
			string sMemNum = "";
			DbReader objRead=null;
			sSQL="Select Member_Num From Pabblo_Member Where ENTITY_ID =" +p_iEID;
			try
			{
				objRead=DbFactory.GetDbReader(m_sConnectString,sSQL);
				if (objRead.Read())
				{
					// iCode=Conversion.ConvertObjToInt(objRead.GetValue(0), m_iClientId);
					sMemNum = Conversion.ConvertObjToStr(objRead.GetValue(0));
				}
				if (objRead!=null)
					objRead.Close();
			}
			catch(Exception p_Objex)
			{
				throw new RMAppException(Globalization.GetString("Pabblo.GetPabbloMemNum.Error",m_iClientId), p_Objex);		//sonali		
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
					objRead.Dispose();
				}
			}
			return sMemNum;
		}
		/// Name		: IsPabbloCodeMapExist
		/// Author		: Parag Sarin
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Checks if code map table exists / not
		/// </summary>
		/// <param name="p_iEID">Entity Id</param>
		/// <returns>Returns true if code map table exists else false</returns>
		public bool IsPabbloCodeMapExist()
		{
			string sSQL=string.Empty;
			bool bReturn=false;
			DbReader objRead=null;
			sSQL="SELECT COUNT(*) FROM PABBLO_CODE_MAP";
			try
			{
				objRead=DbFactory.GetDbReader(m_sConnectString,sSQL);
				try
				{
					if (objRead.Read())
					{
						bReturn=true;
					}
				}
				catch(Exception p_Objex)
				{
					bReturn=false;
				}
				if (objRead!=null)
				objRead.Close();
			}
			catch(Exception p_Objex)
			{
				throw new RMAppException(Globalization.GetString("Pabblo.IsPabbloCodeMapExist.Error",m_iClientId), p_Objex);				
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
					objRead.Dispose();
				}
			}
			return bReturn;
		}
		
		/// Name		: GetPolicyData
		/// Author		: Anurag Agarwal
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Gets the policy data from Pabblo database for he Member choosen
		/// </summary>
		/// <param name="p_sMemberNum">Member Number</param>
		/// <param name="p_sEventDt">Event Date</param>
		/// <param name="p_sLOBCode">LOB Code</param>
		/// <param name="p_sCvgCode">Coverage Code</param>
		/// <returns>Returns true if policy is retreived else false</returns>
		public bool GetPolicyData(string p_sMemberNum, string p_sEventDt, string p_sLOBCode,string p_sCvgCode, int lClaimTypeCode)
		{

			StringBuilder sSqlTmp = null;
			DbReader objPolMastReader = null;
			DbReader objTempReader = null;
			string sPolNum = "";
			int iEndorseNum = 0;
			int iSequenceNum = 0;
			bool bReturn = false;
			object objTemp = null;
			bool bNewLookupMethod = false;
			LocalCache objCache=null;

			try
			{
				bReturn = false;

				// First, look for an LOB_TRANS table in the RM database. If so, do a newer (post-TASB) method of LOB lookup.
				//  The old way was to assume CLAIM_TYPE short code in RM == Pabblo LOB which is unrealistic but apparently true at TASB.
                //This part of code was written by Jim Partin for KLC 
                // Pswarnkar 
				bNewLookupMethod = false;
				try
				{
					DbReader lobLookup;

					lobLookup = DbFactory.GetDbReader(m_sConnectString,"SELECT LOB FROM LOB_TRANS WHERE RM_Code = " + lClaimTypeCode);
                    if (lobLookup.Read())
                    {
                        p_sCvgCode = lobLookup["LOB"].ToString();

                    }
                    else
                    {//added by pawan for mits 11752 else part since as per the design this table RM_CODe can have either claimtype or LOB based on Pabblo database
                        lobLookup.Close();
                        lobLookup = DbFactory.GetDbReader(m_sConnectString, "SELECT LOB FROM LOB_TRANS WHERE RM_Code = " + p_sLOBCode);
                        if (lobLookup.Read())
                        {
                            p_sCvgCode = lobLookup["LOB"].ToString();

                        }

                    }
					lobLookup.Close();
					bNewLookupMethod = true;
				}
				catch (Exception e)
				{
					bNewLookupMethod = false;
				}

				// Lookup some mappings
				GetPabbloDBType(p_sLOBCode);
				GetPabbloCvgDataFld(p_sCvgCode);
				
				// Get the policy record
				sSqlTmp = new StringBuilder();
				// if (!bNewLookupMethod)
				// {
				// 	sSqlTmp.Append("SELECT Policy_Num, Policy_Status, Policy_Premium,Effective_Date,Expire_Date");
				// 	sSqlTmp.Append(",Endorse_Num,summary_status,Policy_Bound FROM PUB.Policy_Master Where Member_Num = '");
				// 	sSqlTmp.Append(p_sMemberNum + "' AND PUB.Policy_Master.Effective_Date <= '" + Conversion.ToDate(p_sEventDt) + "'");
				// 	sSqlTmp.Append(" AND PUB.Policy_Master.Expire_Date > '"  + Conversion.ToDate(p_sEventDt) + "'");
				// 	sSqlTmp.Append(" AND PUB.Policy_Master.Policy_Status = 'B' AND PUB.Policy_Master.policy_bound = 1");
				// }
				// else
				// {
					// The newer method links in the Pol_Major_Ext table so we can screen policies on pabblo LOB up front.
					sSqlTmp.Append("SELECT PUB.Policy_Master.Policy_Num, PUB.Policy_Master.Policy_Status, PUB.Policy_Master.Policy_Premium,PUB.Policy_Master.Effective_Date,PUB.Policy_Master.Expire_Date");
					sSqlTmp.Append(",PUB.Policy_Master.Endorse_Num,PUB.Policy_Master.summary_status,PUB.Policy_Master.Policy_Bound FROM PUB.Policy_Master,PUB.Pol_Major_Ext Where ");
					sSqlTmp.Append(" PUB.Policy_Master.Policy_Num = PUB.Pol_Major_Ext.Policy_Num AND PUB.Policy_Master.Endorse_Num = PUB.Pol_Major_Ext.Endorse_Num");
					sSqlTmp.Append(" AND Member_Num = '" + p_sMemberNum + "' AND PUB.Policy_Master.Effective_Date <= '" + Conversion.ToDate(p_sEventDt) + "'");
					sSqlTmp.Append(" AND PUB.Policy_Master.Expire_Date > '"  + Conversion.ToDate(p_sEventDt) + "'");
					sSqlTmp.Append(" AND PUB.Policy_Master.Policy_Status = 'B' AND PUB.Policy_Master.policy_bound = 1");
					sSqlTmp.Append(" AND PUB.Pol_Major_Ext.LOB_Code = '" + p_sCvgCode + "'");
				// }

				objPolMastReader = DbFactory.GetDbReader(m_sPabbloConnectString,sSqlTmp.ToString());	
				if(objPolMastReader != null)
				{
					while(objPolMastReader.Read())
					{
						ClearCurPolData();
						
						curPolicy.LOBCode = p_sCvgCode;

						// Figure out coverage type code for RM. Old method (1st) assumes cvg types in RM match the pabblo LOB.
						//   The newer (second) uses claim type and picks its parent coverage type.
						objCache=new LocalCache(m_sConnectString,m_iClientId);
						if (!bNewLookupMethod)
							curPolicy.CvgTypeCode=objCache.GetCodeId(curPolicy.LOBCode,"COVERAGE_TYPE");
						else
							curPolicy.CvgTypeCode=objCache.GetRelatedCodeId(lClaimTypeCode);
						objCache.Dispose();

						// Fetch remainder of policy fields
						sPolNum = objPolMastReader["Policy_Num"].ToString();
						
						iEndorseNum = Conversion.ConvertStrToInteger(objPolMastReader["Endorse_Num"].ToString());
						if(objPolMastReader["Policy_Premium"] != null)
							curPolicy.Policy_Premium = Conversion.ConvertStrToDouble(objPolMastReader["Policy_Premium"].ToString());
						else
							curPolicy.Policy_Premium = 0.0;
						if(objPolMastReader["summary_status"] != null)
							curPolicy.POLICY_STATUS = objPolMastReader["summary_status"].ToString();
						else
							curPolicy.POLICY_STATUS = "";
						if(m_bIsPSMaped)
							curPolicy.PSCode = GetPSMapVal(curPolicy.POLICY_STATUS);

						//JM 12/11/2003 - find the most recent (greatest) sequence number
						sSqlTmp = new StringBuilder();
						sSqlTmp = sSqlTmp.Append("SELECT max(Sequence_Num)  From PUB.Policy_Schedule Where Policy_Number = '" + sPolNum + "'");
						sSqlTmp = sSqlTmp.Append(" AND LOB_Code = '" + p_sCvgCode + "' AND Endorse_Num = " + iEndorseNum);
						sSqlTmp = sSqlTmp.Append(" AND Effective_Date <= '" + Conversion.ToDate(p_sEventDt) + "'");
						sSqlTmp = sSqlTmp.Append(" AND Expire_Date > '" + Conversion.ToDate(p_sEventDt) + "'");
						objTempReader = DbFactory.GetDbReader(m_sPabbloConnectString,sSqlTmp.ToString());
						if(objTempReader != null)
						{
							if(objTempReader.Read())
								iSequenceNum = Conversion.ConvertStrToInteger(objTempReader[0].ToString());
							objTempReader.Close();
						}

						sSqlTmp = new StringBuilder();
						sSqlTmp = sSqlTmp.Append("SELECT Policy_Number, Endorse_Num, LOB_Code, Und_Premium,Effective_Date,");
						sSqlTmp = sSqlTmp.Append("Expire_Date,Limit_Code, Limit_Value, Deductible_Code, Deductible_Value,");
						sSqlTmp = sSqlTmp.Append("Decimal_1, Decimal_2, Minor_LOB_Code FROM PUB.Policy_Schedule ");
						sSqlTmp = sSqlTmp.Append("WHERE Policy_Number = '" + sPolNum + "' AND LOB_Code = '" + p_sCvgCode + "'");
						sSqlTmp = sSqlTmp.Append(" AND Endorse_Num = " + iEndorseNum);
						sSqlTmp = sSqlTmp.Append(" AND Effective_Date <= '" + Conversion.ToDate(p_sEventDt) + "'");
						sSqlTmp = sSqlTmp.Append(" AND Expire_Date > '" + Conversion.ToDate(p_sEventDt) + "'");
						objTempReader = DbFactory.GetDbReader(m_sPabbloConnectString,sSqlTmp.ToString());
						if(objTempReader != null)
						{
							if(objTempReader.Read())
							{
								curPolicy.Policy_Num = sPolNum;
								
								objTemp = objTempReader["Effective_Date"];
								if( objTemp.ToString() != "" && objTemp != null)
									curPolicy.Effective_Date = objTemp.ToString();
								else
									curPolicy.Effective_Date = "";
								
								objTemp = objTempReader["Expire_Date"];
								if( objTemp.ToString() != "" && objTemp != null)
									curPolicy.Expire_Date = objTemp.ToString();
								else
									curPolicy.Expire_Date = "";
								
								objTemp = objTempReader["Limit_Code"];
								if( objTemp.ToString() != "" && objTemp != null)
									curPolicy.CvgInfo.ClaimLmtCode = objTemp.ToString();
								else
									curPolicy.CvgInfo.ClaimLmtCode = "";

								if(curCvgDataFld.LimitTbl == "POLICY_SCHEDULE" && curCvgDataFld.LimitFld == "DECIMAL_1")
									objTemp = objTempReader["Decimal_1"];
								else if(curCvgDataFld.LimitTbl == "POLICY_SCHEDULE" && curCvgDataFld.LimitFld == "LIMIT_VALUE")
									objTemp = objTempReader["Limit_Value"];	
								if( objTemp.ToString() != "" && objTemp != null)
									curPolicy.CvgInfo.ClaimLmt = Conversion.ConvertObjToDouble(objTemp, 0);
								else
									curPolicy.CvgInfo.ClaimLmt = 0;
								
								objTemp = objTempReader["Deductible_Code"];
								if( objTemp.ToString() != "" && objTemp != null)
									curPolicy.CvgInfo.DeductCode = objTemp.ToString();
								else
									curPolicy.CvgInfo.DeductCode = "";

								if(curCvgDataFld.DeductTable == "POLICY_SCHEDULE" && curCvgDataFld.DeductFld == "DECIMAL_2")
									objTemp = objTempReader["Decimal_2"];
								else if(curCvgDataFld.DeductTable == "POLICY_SCHEDULE" && curCvgDataFld.DeductFld == "DEDUCTIBLE_VALUE")
									objTemp = objTempReader["Deductible_Value"];	
								if( objTemp.ToString() != "" && objTemp != null)
									curPolicy.CvgInfo.DeductVal = Conversion.ConvertObjToDouble(objTemp, 0);
								else
									curPolicy.CvgInfo.DeductVal = 0;
								
								objTemp = objTempReader["Minor_LOB_Code"];
								if( objTemp.ToString() != "" && objTemp != null)
									curPolicy.CvgInfo.MinorLOBCode = objTemp.ToString();
								else
									curPolicy.CvgInfo.MinorLOBCode = "";
								bReturn = true;
							}
							objTempReader.Close();
	
							break;
						}	
					}	
					objPolMastReader.Close(); 
				}

				if(bReturn)
					GetCoverageData();
				
			}
			catch(Exception p_Objex)
			{
				throw new RMAppException(Globalization.GetString("Pabblo.GetPolicyData.Error",m_iClientId), p_Objex);
			}
			finally
			{
				curPolicy.CvgInfo.InsertCvgData = false;
				if(objPolMastReader != null)
					objPolMastReader.Dispose();
				if(objTempReader != null)
					objTempReader.Dispose();
				if(objTemp != null)
					objTemp = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
			}
			return bReturn;
		}

		/// Name		: GetPabbloCvgDataFld
		/// Author		: Anurag Agarwal
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Gets the Mapped coverage fields of Pabblo database.
		/// </summary>
		/// <param name="p_sPblLOBCode">LOB code</param>
		public void GetPabbloCvgDataFld(string p_sPblLOBCode)
		{
			string sSQL = "";
			DbReader objReader = null;
			string sTemp = "";
			string[] sPairs;
			string sTable = "";
			string sField = "";

			try
			{
				curCvgDataFld.DeductFld = "";
				curCvgDataFld.DeductTable = "";
				curCvgDataFld.LimitFld = "";
				curCvgDataFld.LimitTbl = "";
				
				if(p_sPblLOBCode.Trim().Length == 0)
					return;
				
				sSQL = "SELECT LIMIT_FLD, DEDUCT_FLD FROM PABBLO_CVGFLD_MAP WHERE PBL_LOB_CODE = '" + p_sPblLOBCode + "'";
				objReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
				if(objReader != null)
				{
					if(objReader.Read())
					{
						//limit
						sTemp = objReader["LIMIT_FLD"].ToString();
						if(sTemp.Length > 0)
						{
							if(sTemp.IndexOf(".") != -1)
							{
								sPairs = sTemp.Split('.');
								sTable = sPairs[0];		//tbl
								sField = sPairs[1];		//fld
							}
							else
								sTable = sTemp;

							curCvgDataFld.LimitTbl = sTable.Trim().ToUpper(); 
							curCvgDataFld.LimitFld = sField.Trim().ToUpper();
						}
						
						//deduct
						sTemp = objReader["DEDUCT_FLD"].ToString();
						if(sTemp.Length > 0)
						{
							if(sTemp.IndexOf(".") != -1)
							{
								sPairs = sTemp.Split('.');
								sTable = sPairs[0];		//tbl
								sField = sPairs[1];		//fld
							}
							else
								sTable = sTemp;

							curCvgDataFld.DeductTable = sTable.Trim().ToUpper(); 
							curCvgDataFld.DeductFld = sField.Trim().ToUpper();
						}
					}
					objReader.Close();
				}

			}
			catch(Exception p_Objex)
			{
                throw new RMAppException(Globalization.GetString("Pabblo.GetPabbloCvgDataFld.Error", m_iClientId), p_Objex);
			}
			finally
			{
				if(objReader != null)
					objReader.Dispose();
			}
		}

		/// Name		: GetCoverageData
		/// Author		: Anurag Agarwal
		/// Date Created: 08-05-2006
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Gets the coverage data
		/// </summary>
		private void GetCoverageData()
		{
			DbReader objReader = null;
			string sSQL = "";
			object objTemp = null;

			try
			{
				curPolicy.CvgInfo.InsertCvgData = true;
	
				switch (curCvgDataFld.LimitTbl)
				{
					case "LIMITS_MASTER":
						//jm add limit_desc to select stmt
						sSQL = "Select Limit_Desc, " + curCvgDataFld.LimitFld + " From pub.Limits_Master";
						sSQL += " Where LOB_Code = '" + curPolicy.LOBCode + "' And Limit_Code = '";
						sSQL += curPolicy.CvgInfo.ClaimLmtCode + "'";
						if(curPolicy.CvgInfo.MinorLOBCode.Trim() != "")
							sSQL += " And Minor_LOB_Code = '" +  curPolicy.CvgInfo.MinorLOBCode + "'";
						objReader = DbFactory.GetDbReader(m_sPabbloConnectString,sSQL);
						if(objReader != null)
						{
							if(objReader.Read())
							{
								objTemp = objReader[curCvgDataFld.LimitFld];
								if( objTemp.ToString() != "" && objTemp != null)
                                    curPolicy.CvgInfo.ClaimLmt = Conversion.ConvertObjToDouble(objTemp, m_iClientId);
								else
									curPolicy.CvgInfo.ClaimLmt = 0;
								if(curPolicy.LOBCode == "BOIL" || curPolicy.LOBCode == "AL" || curPolicy.LOBCode == "AHN")
								{
									objTemp = objReader["Limit_Desc"];
									if( objTemp.ToString() != "" && objTemp != null)
										curPolicy.CvgInfo.LimitDesc = objTemp.ToString();
									else
										curPolicy.CvgInfo.LimitDesc = "0";
								}
							}
							objReader.Close();
						}
						break;
					case "POLICY_SCHEDULE":
						//done previous
						break;
				}

				//Deductible Value
				switch (curCvgDataFld.DeductTable)
				{
					case "DEDUCTIBLE_MASTER":
						sSQL = "Select " + curCvgDataFld.DeductFld + " From pub.Deductible_Master";
						sSQL += " Where LOB_Code = '" + curPolicy.LOBCode + "' And Deductible_Code = '";
						sSQL += curPolicy.CvgInfo.DeductCode + "'";
						if(curPolicy.CvgInfo.MinorLOBCode.Trim() != "")
							sSQL += " And Minor_LOB_Code = '" +  curPolicy.CvgInfo.MinorLOBCode + "'";
						objReader = DbFactory.GetDbReader(m_sPabbloConnectString,sSQL);
						if(objReader != null)
						{
							if(objReader.Read())
							{
								objTemp = objReader[curCvgDataFld.DeductFld];
								if( objTemp.ToString() != "" && objTemp != null)
                                    curPolicy.CvgInfo.DeductVal = Conversion.ConvertObjToDouble(objTemp, m_iClientId);
								else
									curPolicy.CvgInfo.DeductVal = 0;
							}
							objReader.Close();
						}
						break;
					case "POLICY_SCHEDULE":
						//done previous
						break;
				}
			}
			catch (Exception ex)
			{
				throw new RMAppException("Pabblo SQL: " + sSQL, ex);				
			}
			finally
			{
				curPolicy.CvgInfo.InsertCvgData = false;
				if(objReader != null)
					objReader.Dispose();
				if(objTemp != null)
					objTemp = null;
			}
		}

        
	}
}
