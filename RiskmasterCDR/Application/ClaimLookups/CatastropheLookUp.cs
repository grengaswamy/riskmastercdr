﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.ClaimLookups
{
    ///************************************************************** 
    ///* $File		: CatastropheLookUp.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 13 June 2012
    ///* $Author	: Amandeep Kaur
    
    ///**************************************************************
    /// <summary>	
    ///	This class is used to fetch the list of Catastrophe Codes for a Claim.
    /// </summary>
    public class CatastropheLookUp
    {
        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CatastropheLookUp() { }
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
        public CatastropheLookUp(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)//sonali
        {
            m_sDsnName = p_sDsnName;
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_iClientId = p_iClientId;
        }

        #endregion

        #region Variable Declaration
        /// <summary>
        /// Private Variable for ClientId
        /// </summary>
        private int m_iClientId = 0;
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;

        #endregion

        #region "Public Functions"

        /// Name		: GetCatastropheLookUp
        /// Author		: Anshul Verma
        /// Date Created: 13 - June - 2012
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Gets the List of Catastrophe for the Claim.
        /// </summary>
        /// <param name="p_sEventDate">Event Date</param>
        /// <param name="p_sClaimDate">Claim Date</param>
        /// <returns>XML Document containing the catastrophe list</returns>
        public XmlDocument GetCatastropheLookUp(string p_sEventDate,
                                                  int p_iCatCodetId, int p_CatState, string p_sTableName)//dvatsa JIRA RMA-4495
        {
            DbReader objReader = null;			//reader object for searching
            XmlDocument objCatastropheXml = null;		//Xml to return
            XmlElement objCatastrophe = null;
            XmlElement objElem = null;
            StringBuilder sSQL = new StringBuilder();
            LocalCache objCache = null;
            int iTableId = 0;
            string sCountry = string.Empty;
            //int iCountryCode = 0;
            string sShortCode = string.Empty;
            string sCodeDesc = string.Empty;
            int iCatCode = 0;
            string sCatCode = string.Empty;
            string sLossStartDate = string.Empty;
            string sLossEndDate = string.Empty;
            try
            {
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                iTableId = objCache.GetTableId(p_sTableName);
                sSQL.Append("SELECT CATASTROPHE.CATASTROPHE_ROW_ID, CATASTROPHE.LOSS_START_DATE,CATASTROPHE.LOSS_END_DATE,CATASTROPHE.COUNTRY_CODE, CATASTROPHE.CAT_NUMBER," +
                         " CATASTROPHE.CAT_CODE FROM CATASTROPHE, CODES, CODES_TEXT ");
                if (p_CatState > 0)
                    sSQL.Append(" ,CATASTROPHE_STATES,STATES ");
                sSQL.Append(" WHERE LOSS_START_DATE < = " + p_sEventDate + " " +
                       " AND (LOSS_END_DATE > = " + p_sEventDate + " OR LOSS_END_DATE IS NULL) AND CODES.CODE_ID= " + p_iCatCodetId + " AND CODES.TABLE_ID = " + iTableId + " " +
                       " AND CATASTROPHE.CAT_CODE = CODES.CODE_ID AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                if (p_CatState > 0)
                    sSQL.Append(" AND CATASTROPHE_STATES.CATASTROPHE_ROW_ID = CATASTROPHE.CATASTROPHE_ROW_ID  AND CATASTROPHE_STATES.STATE_ID = " + p_CatState + "");//dvatsa JIRA RMA-4495 remove AND CATASTROPHE_STATES.STATE_ID=STATES.STATE_ROW_ID after review 
                sSQL.Append(" GROUP BY CATASTROPHE.CATASTROPHE_ROW_ID, CATASTROPHE.LOSS_START_DATE,CATASTROPHE.LOSS_END_DATE,CATASTROPHE.COUNTRY_CODE, CATASTROPHE.CAT_NUMBER, CATASTROPHE.CAT_CODE ");
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL.ToString());

                objCatastropheXml = new XmlDocument();
                objCatastrophe = objCatastropheXml.CreateElement("CatastropheLookUpList");
                objCatastropheXml.AppendChild(objCatastrophe);
                while (objReader.Read())
                {                    
                    //iCountryCode = Conversion.ConvertObjToInt(objReader.GetValue("COUNTRY_CODE"), m_iClientId); 
                    //if(iCountryCode != 0)
                    //{
                    //    objCache.GetCodeInfo(iCountryCode, ref sShortCode, ref sCodeDesc);
                    //    sCountry = sShortCode + " " + sCodeDesc;
                    //}
                    iCatCode = Conversion.ConvertObjToInt(objReader.GetValue("CAT_CODE"), m_iClientId);
                    if (iCatCode != 0)
                    {
                        objCache.GetCodeInfo(iCatCode, ref sShortCode, ref sCodeDesc);
                        sCatCode = sShortCode + " " + sCodeDesc;
                    }
                    objElem = objCatastropheXml.CreateElement("CatastropheLookUp");
                    objElem.SetAttribute("RowId", Conversion.ConvertObjToStr(objReader.GetValue("CATASTROPHE_ROW_ID")));
                    objElem.SetAttribute("CatCode", Conversion.ConvertObjToStr(objReader.GetValue("CAT_CODE")));
                    objElem.SetAttribute("CatastropheNumber", Conversion.ConvertObjToStr(objReader.GetValue("CAT_NUMBER")));
                    sLossStartDate = Conversion.ConvertObjToStr(objReader.GetValue("LOSS_START_DATE"));
                    if (!string.IsNullOrEmpty(sLossStartDate))
                    {
                        sLossStartDate = Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("LOSS_START_DATE"))).ToShortDateString();
                    }
                    objElem.SetAttribute("LossStartDate", sLossStartDate);

                    sLossEndDate = Conversion.ConvertObjToStr(objReader.GetValue("LOSS_END_DATE"));
                    if (!string.IsNullOrEmpty(sLossEndDate))
                    {
                        sLossEndDate = Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("LOSS_END_DATE"))).ToShortDateString();
                    }                    
                    objElem.SetAttribute("LossEndDate", sLossEndDate);
                    objElem.SetAttribute("Type", sCatCode);
                    //objElem.SetAttribute("Country", sCountry);
                    objCatastrophe.AppendChild(objElem);                   
                }
                objReader.Close();
                objReader = null;

                return objCatastropheXml;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CatastropheLookUp.GetCatastropheLookUp.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (sSQL != null)
                {
                    sSQL = null;
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objCatastropheXml = null;
                objCatastrophe = null;
                objElem = null;
            }
        }

        #endregion
    }
}
