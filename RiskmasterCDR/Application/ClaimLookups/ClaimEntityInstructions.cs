﻿
using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;

namespace Riskmaster.Application.ClaimLookups
{
	///************************************************************** 
	///* $File		: ClaimEntityInstructions.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 28-Aug-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to fetch the instructions for the Claim Entity.
	/// </summary>
	public class ClaimEntityInstructions
	{
		#region "Constructor"
		/// <summary>
		/// Default constructor.
		/// </summary>
		public ClaimEntityInstructions()
		{
		}
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		public ClaimEntityInstructions(string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)//sonali
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
        /// <summary>
        /// Private Variable to store Client Id
        /// </summary>
        private int m_iClientId = 0;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		#endregion

		#region "Public Functions"

		/// Name		: GetClaimEntityInstructions
		/// Author		: Nikhil Kumar Garg
		/// Date Created: 28-Aug-2005
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Gets the instructions for the Claim Entity.
		/// </summary>
		/// <param name="p_iEId">Claim Type Code</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
		/// <returns>XML Document containing the instructions</returns>
		public XmlDocument GetClaimEntityInstructions(int p_iEId )
		{

			DbReader objReader=null;
			XmlDocument objInstructions=null;
			XmlElement objElem=null;
			XmlCDataSection objCData=null;
			string sSQL=string.Empty;
			string sInstructions=string.Empty;
			int iThisParentEID=0;
			string sEIDList=string.Empty;

			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

				sSQL = "SELECT PARENT_EID FROM ENTITY WHERE ENTITY_ID = " + p_iEId;
				objReader=DbFactory.GetDbReader(m_sConnectionString, sSQL);

				sEIDList=p_iEId.ToString();
				
				if (objReader.Read())
				{
					iThisParentEID=Conversion.ConvertObjToInt(objReader.GetValue("PARENT_EID"), m_iClientId);

					sEIDList = sEIDList + "," + iThisParentEID.ToString();
				
					objReader.Close();
					objReader=null;

					while(iThisParentEID > 0)
					{
						sSQL = "SELECT PARENT_EID FROM ENTITY WHERE ENTITY_ID = " + iThisParentEID;
						objReader=DbFactory.GetDbReader(m_sConnectionString, sSQL);
						if (objReader.Read())
							iThisParentEID=Conversion.ConvertObjToInt(objReader.GetValue("PARENT_EID"), m_iClientId);

						if (iThisParentEID!=0) 
							sEIDList = sEIDList + "," + iThisParentEID.ToString();

						objReader.Close();
						objReader=null;
					}
				}
				else
				{
					objReader.Close();
					objReader=null;
				}

				string sTemp=string.Empty;

				sSQL = "SELECT ENT_INSTR_TEXT FROM ENT_INSTRUCTIONS WHERE ENTITY_ID IN (" + sEIDList + ") ORDER BY ENTITY_ID ASC";
				objReader=DbFactory.GetDbReader(m_sConnectionString, sSQL);

				while (objReader.Read())
				{
					sTemp=Conversion.ConvertObjToStr(objReader.GetValue("ENT_INSTR_TEXT"));
					sTemp=sTemp.Replace("\0","");
					if (sTemp.Trim() != string.Empty)
					{
						sTemp = sTemp.Replace("<", "&lt;");
						sTemp = sTemp.Replace(">", "&gt;");
						if ( sInstructions != "" )
							sInstructions = sInstructions + "<br/><br/>";
						sInstructions = sInstructions + sTemp;
					}
				}
				objReader.Close();
				objReader=null;

				if (sInstructions.Trim() != string.Empty)
				{
					sInstructions=sInstructions.Replace("\n","<br/>");
				}
				else
					sInstructions="[No Instructions]";

				objInstructions=new XmlDocument();
				objElem=objInstructions.CreateElement("Instructions");
				objElem.InnerText = sInstructions;
				objInstructions.AppendChild(objElem);

				return objInstructions;

			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimEntityInstructions.GetClaimEntityInstructions.Error",m_iClientId), p_objEx);//sonali
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Dispose();
                }
				if (m_objDataModelFactory != null)
				{
                    m_objDataModelFactory.Dispose();
				}
				objCData=null;
				objElem=null;
				objInstructions=null;
			}
		}
					 
		#endregion
	}
}
