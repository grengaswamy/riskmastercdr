using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;

namespace Riskmaster.Application.ClaimLookups
{
	///************************************************************** 
	///* $File		: LookupClaimPlan.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 25-Aug-2005
	///* $Author	: Nikhil Kr. Garg
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	///	This class is used to fetch the list of plans for a Claim.
	/// </summary>
	public class LookupClaimPlan
	{
		#region "Constructor"
		/// <summary>
		/// Default constructor.
		/// </summary>
		public LookupClaimPlan()
		{
		}
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_iClientId">ClientId for Cloud Environment and 0 for Non Cloud environment</param>
        public LookupClaimPlan(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)//sonali
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
        /// <summary>
        /// Private variable for Client Id
        /// </summary>
        private int m_iClientId = 0;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		#endregion

		#region "Constants"
		private const string SC_PLAN_REVOKED = "R";
		private const string SC_PLAN_IN_EFFECT = "I";
		private const string SC_PLAN_EXPIRED = "E";
		private const string SC_PLAN_CANCELED = "C";
		#endregion

		#region "Public Functions"

		/// Name		: GetLookupClaimPlanList
		/// Author		: Nikhil Kumar Garg
		/// Date Created: 25-Aug-2005
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		///*************************************************************************
		/// <summary>
		/// Gets the List of Plans for the Claim.
		/// </summary>
		/// <param name="p_iClaimTypeCode">Claim Type Code</param>
		/// <param name="p_sEventDate">Event Date</param>
		/// <param name="p_sClaimDate">Claim Date</param>
		/// <param name="p_iDeptEID">Dept EId</param>
		/// <returns>XML Document containing the plan list</returns>
		public XmlDocument GetLookupClaimPlanList(int p_iClaimTypeCode, string p_sEventDate, 
												  string p_sClaimDate,int p_iDeptEID)
		{
			DbReader objReader=null;			//reader object for searching
			XmlDocument objPlanXml=null;		//Xml to return
			XmlElement objPlans=null;
			XmlElement objElem=null;
			string sSQL=string.Empty;
			LocalCache objCache = null;

			//int iPLAN_STATUS=0;
			int iCN_PLAN_REVOKED=0;
			int iCN_PLAN_IN_EFFECT=0;
			int iCN_PLAN_EXPIRED=0;
			int iCN_PLAN_CANCELED=0;
			int iCoverageTypeCode=0;
			string sGetOrgForDept=string.Empty;

			try
			{
				m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

                objCache = new LocalCache(m_sConnectionString, m_iClientId);

				// preload some code/table ids
				//iPLAN_STATUS = objCache.GetTableId("PLAN_STATUS");
				iCN_PLAN_REVOKED = objCache.GetCodeId(SC_PLAN_REVOKED,"PLAN_STATUS");
				iCN_PLAN_IN_EFFECT = objCache.GetCodeId(SC_PLAN_IN_EFFECT, "PLAN_STATUS");
				iCN_PLAN_EXPIRED = objCache.GetCodeId(SC_PLAN_EXPIRED, "PLAN_STATUS");
				iCN_PLAN_CANCELED = objCache.GetCodeId(SC_PLAN_CANCELED, "PLAN_STATUS");
				iCoverageTypeCode = objCache.GetRelatedCodeId(p_iClaimTypeCode);
				sGetOrgForDept = GetOrgListForDept(p_iDeptEID);

				// build select for looking up Plans for this claim
				//TR#1411 01/24/06 Pankaj: added DTTM_RCD_LAST_UPD to the query
				sSQL = "SELECT DISABILITY_PLAN.PLAN_ID,PLAN_NUMBER,PLAN_NAME,EFFECTIVE_DATE,EXPIRATION_DATE,DTTM_RCD_LAST_UPD";
				sSQL = sSQL + " FROM DISABILITY_PLAN, DIS_PLAN_X_INSURED";
				sSQL = sSQL + " WHERE DISABILITY_PLAN.PLAN_ID = DIS_PLAN_X_INSURED.PLAN_ID";
				sSQL = sSQL + "      AND DIS_PLAN_X_INSURED.INSURED_EID IN (" + sGetOrgForDept + ")";
                //pmittal5 Mits 15357 04/22/09 - Only Plans with Status "InEffect" should be displayed
                //sSQL = sSQL + " AND ((DISABILITY_PLAN.PLAN_STATUS_CODE IN (" + iCN_PLAN_IN_EFFECT + "," + iCN_PLAN_EXPIRED + "))";
                //sSQL = sSQL + "      OR (DISABILITY_PLAN.PLAN_STATUS_CODE = " + iCN_PLAN_CANCELED + " ";
                //sSQL = sSQL + "          AND DISABILITY_PLAN.CANCEL_DATE > '" + p_sClaimDate + "')";
                //sSQL = sSQL + "      OR (DISABILITY_PLAN.PLAN_STATUS_CODE = " + iCN_PLAN_CANCELED;
                //sSQL = sSQL + "          AND DISABILITY_PLAN.CANCEL_DATE > '" + p_sEventDate + "')";
                sSQL = sSQL + " AND ((DISABILITY_PLAN.PLAN_STATUS_CODE = " + iCN_PLAN_IN_EFFECT + ")";
				sSQL = sSQL + " AND ((DISABILITY_PLAN.EFFECTIVE_DATE <= '" + p_sEventDate + "'";
				sSQL = sSQL + "       AND DISABILITY_PLAN.EXPIRATION_DATE >= '" + p_sEventDate + "')";
				sSQL = sSQL + "      OR (DISABILITY_PLAN.EFFECTIVE_DATE <= '" + p_sClaimDate + "'";
				sSQL = sSQL + "          AND DISABILITY_PLAN.EXPIRATION_DATE >= '" + p_sClaimDate + "')))";
				sSQL = sSQL + " ORDER BY PLAN_NUMBER ";


				objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

				objPlanXml=new XmlDocument();
				objPlans=objPlanXml.CreateElement("plans");
				objPlanXml.AppendChild(objPlans);
				while (objReader.Read())
				{
                    if (Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"))) <= Conversion.ToDate(p_sEventDate) && Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"))) <= Conversion.ToDate(p_sClaimDate) && Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"))) >= Conversion.ToDate(p_sEventDate) && Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"))) >= Conversion.ToDate(p_sClaimDate))
                    {
                        objElem = objPlanXml.CreateElement("plan");
                        objElem.SetAttribute("planid", Conversion.ConvertObjToStr(objReader.GetValue("PLAN_ID")));
                        objElem.SetAttribute("planname", Conversion.ConvertObjToStr(objReader.GetValue("PLAN_NAME")));
                        objElem.SetAttribute("plannumber", Conversion.ConvertObjToStr(objReader.GetValue("PLAN_NUMBER")));
                        objElem.SetAttribute("effectivedate", Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"))).ToShortDateString());
                        objElem.SetAttribute("expirationdate", Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"))).ToShortDateString());
                        //TR#1411 01/24/06 Pankaj: dttm last update needs to go to non-occ screen.
                        objElem.SetAttribute("dttmrcdlastupd", Conversion.ConvertObjToStr(objReader.GetValue("DTTM_RCD_LAST_UPD")));
                        objPlans.AppendChild(objElem);
                    }
				}
				objReader.Close();
				objReader=null;

				//get class names select options for each plan
				XmlNodeList objPlanNodes=null;
				string sClassNames=string.Empty;
				int iPlanId=0;
				objPlanNodes=objPlans.SelectNodes("plan");
				foreach(XmlNode objNode in objPlanNodes)
				{
					sClassNames=string.Empty;
					iPlanId= Conversion.ConvertStrToInteger(objNode.Attributes["planid"].Value);
					sSQL = "SELECT CLASS_ROW_ID, CLASS_NAME FROM DISABILITY_CLASS WHERE PLAN_ID = " + iPlanId + " ORDER BY CLASS_NAME";
					objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
					while (objReader.Read())
						sClassNames=sClassNames + "|" + Conversion.ConvertObjToStr(objReader.GetValue("CLASS_ROW_ID")) + ";" + 
														Conversion.ConvertObjToStr(objReader.GetValue("CLASS_NAME"));
					objReader.Close();
					objReader=null;	
					//Raman Bhatia : We should be able to select a plan with no classes. Payments would not proceed in any case without the class
                    if (sClassNames!="")
						((XmlElement)objNode).SetAttribute("classnames",sClassNames.Substring(1));
					/*
                    else
						objPlans.RemoveChild(objNode);
                    */
 
				}

				return objPlanXml;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LookupClaimPlan.GetLookupClaimPlanList.Error",m_iClientId), p_objEx);//sonali
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Dispose();
                }
				if (m_objDataModelFactory != null)
				{
                    m_objDataModelFactory.Dispose();
				}
                if (objCache != null)
                {
                    objCache.Dispose();
                }
				objPlanXml=null;
				objPlans=null;
				objElem=null;
			}
		}

		#endregion

		#region "Private Functions"

		private string GetOrgListForDept(int p_iDeptEId)
		{
			//DbReader objReader=null;	
			string sSQL=string.Empty;
			string sGetOrgListForDept=string.Empty;
            sSQL = "SELECT ENTITY.ENTITY_ID, E1.ENTITY_ID, E2.ENTITY_ID, E3.ENTITY_ID, E4.ENTITY_ID, E5.ENTITY_ID, E6.ENTITY_ID, E7.ENTITY_ID " +
                 " FROM ENTITY, ENTITY E1, ENTITY E2, ENTITY E3, ENTITY E4, ENTITY E5, ENTITY E6, ENTITY E7 " +
                 " WHERE ENTITY.ENTITY_ID=" + p_iDeptEId + " AND E1.ENTITY_ID=ENTITY.PARENT_EID AND " +
                 " E2.ENTITY_ID=E1.PARENT_EID AND E3.ENTITY_ID=E2.PARENT_EID AND E4.ENTITY_ID=E3.PARENT_EID " +
                 " AND E5.ENTITY_ID=E4.PARENT_EID AND E6.ENTITY_ID=E5.PARENT_EID AND E7.ENTITY_ID=E6.PARENT_EID";

            using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
            {

                if (objReader.Read())
                    sGetOrgListForDept = p_iDeptEId.ToString() + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(0)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(1)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(2)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(3)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(4)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(5)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(6)) + "," +
                                           Conversion.ConvertObjToStr(objReader.GetValue(7));
            }
            return sGetOrgListForDept;
           
										
		}

		#endregion
	}
}
