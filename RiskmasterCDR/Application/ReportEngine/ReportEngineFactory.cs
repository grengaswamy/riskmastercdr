using System;
using Riskmaster.Application.ReportInterfaces;  
using Riskmaster.Application.OSHALib;
using Riskmaster.Application.ExecutiveSummaryReport;   

namespace Riskmaster.Application.ReportEngine
{
	///************************************************************** 
	///* $File		: ReportEngineFactory.cs 
	///* $Revision	: 1.0
	///* $Date		: 22-Dec-2004 
	///* $Author	: Aditya Babbar
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************
	/// <summary>	
	/// Provides report engine instances. 
	/// </summary>
	public class ReportEngineFactory
	{
		/// Name			: ReportEngineFactory
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Static Constructor.
		/// </summary>		
		static ReportEngineFactory()
		{
			
		}

		/// Name			: GetOSHAReportsEngine
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Returns OshaManager instance used to generate OSHA reports.
		/// </summary>
		/// <param name="p_sConnectionString">
		///		Database Connection String.
		///	</param>
		/// <param name="p_iReportType">
		///		Report Type.
		///	</param>
		/// <returns>OshaManager instance</returns>
		public static IReportEngine GetOSHAReportsEngine(string p_sConnectionString,
															int p_iReportType, int p_iClientId)
		{
			return 
				new OshaManager(p_sConnectionString,
								p_iReportType, p_iClientId); 
		}

		/// Name			: GetExecutiveSummaryReportsEngine
		/// Author			: Aditya Babbar
		/// Date Created	: 22-Dec-2004
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************		
		/// <summary>
		/// Returns OshaManager instance used to generate OSHA reports.
		/// </summary>
		/// <param name="p_sConnectionString">
		///		Database Connection String.
		///	</param>
		/// <returns>OshaManager instance</returns>
		public static IReportEngine GetExecutiveSummaryReportsEngine(string p_sConnectionString, int p_iClientId)
		{
			// TODO - Executive Summary Manager to be modified to provide a new constructor
			return 
				new ExecSummManager(p_sConnectionString, p_iClientId); 
		}

	}

}
