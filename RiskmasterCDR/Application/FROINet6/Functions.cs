﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;

namespace Riskmaster.Application.FROINet6
{
    public class Functions
    {
        public string g_DSN;
        public int g_LoginId;
        public string g_LoginName;

        public string GetFileName(string sFileName)
        {
            string functionReturnValue = null;
            if (string.IsNullOrEmpty(sFileName))
                return functionReturnValue;

            int l = 0;
            //l = Strings.InStrRev(sFileName, "\\");
            l = sFileName.LastIndexOf("\\");
            if (l == 0)
                return functionReturnValue;

            functionReturnValue = sFileName.Substring(l + 1);
            return functionReturnValue;

        }

        public string GetDocPath(string sFileName)
        {
            string functionReturnValue = null;
            if (string.IsNullOrEmpty(sFileName))
                return functionReturnValue;

            int l = 0;
            l = sFileName.LastIndexOf("'\'");
            if (l == 0)
                return functionReturnValue;

            functionReturnValue = sFileName.Substring(l + 1);
            return functionReturnValue;

        }

        public string GetUniqueFileName(string sBasedOn, bool boolCreateFile = true)
	    {
		    string functionReturnValue = null;
		    int l = 0;
		    string s = null;
		    string sDir = null;
		    string sExtension = null;
		    string sName = null;

		    if (!string.IsNullOrEmpty(sBasedOn)) {
			    // Separate file name and path
			    l = sBasedOn.LastIndexOf("\\");
			    if (l > 0) 
                {
                    sDir = sBasedOn.Substring(0, l );
				    sName = sBasedOn.Substring(l + 1);
				    // Remove extension if any
				    l = sName.LastIndexOf(".");
				    if (l > 0) 
                    {
					    sExtension = sName.Substring(l);
					    sName = sName.Substring(0, l);
				    }
			    } 
                else 
                {
				    s = sBasedOn;
			    }
			    if (string.IsNullOrEmpty(sName))
				    sName = "fdf";
			    if (sDir.Substring(sDir.Length - 1) != "\\")
				    sDir = sDir + "\\";
			    if (!FileExist(sDir + sName + sExtension)) 
                {
				    functionReturnValue = sDir + sName + sExtension;
                    s = sDir + sName + sExtension;
                    return functionReturnValue;
			    } else 
                {
				    // Try to get file name nicely
				    l = 1;
				    do 
                    {
					    s = sDir + sName + "-" + l + sExtension;
					    l = l + 1;
				    } 
                    while (FileExist(s) & l < 2000);
				    if (!FileExist(s)) 
                    {
					    functionReturnValue = s;
                        return functionReturnValue;
				    }
			    }
		    }
         
		    return functionReturnValue;
	    }

        public bool FileExist(string sFile)
	    {
		    bool functionReturnValue = false;
		    
		    //File.Open(sFile,FileMode.Open,FileAccess.Read,FileShare.ReadWrite);
            if (File.Exists(sFile))
            {
			    functionReturnValue = true;
		    }
		    return functionReturnValue;
	    }
        
    }
}

