using System;
using System.Data; 


namespace Riskmaster.Application.CodeUtility
{
	/// <summary>
	///Author  :   Navneet Sota & Pankaj Chowdhury
	///Dated   :   26th,Nov 2004
	///Purpose :   A class representing the GLOSSARY table. This is a wrapper around the Glossary table
	///				with the columns of the table being exposed as properties.
	/// </summary>
	internal class Glossary
	{

		#region Variable Declarations	
		
		/// <summary>
		/// Private variable for REQD_REL_TABL_FLAG
		/// </summary>
		private int m_iReqRelTable = 0;

		/// <summary>
		/// Private variable for IND_STAND_TABLE_ID
		/// </summary>
		private int m_iIndTableId = 0;

		/// <summary>
		/// Private variable for REQD_IND_TABL_FLAG
		/// </summary>
		private int m_iReqIndTable = 0;
		
		/// <summary>
		/// Private variable to store TABLE_ID
		/// </summary>
		private int m_iTableId = 0;  
		
		/// <summary>
		/// Private variable for ATTACHMENTS_FLAG
		/// </summary>
		private int m_iIsAttachment =0; 
		
		/// <summary>
		/// Private variable for TREE_DISPLAY_FLAG
		/// </summary>
		private int m_iIsTree = 0; 
		
		/// <summary>
		/// Private variable for RELATED_TABLE_ID
		/// </summary>
		private int m_iRelatedTableId = 0; 
		
		/// <summary>
		/// Private variable for GLOSSARY_TYPE_CODE
		/// </summary>
		private int m_iTypeCode = 0; 

		/// <summary>
		/// Private variable for LINE_OF_BUS_FLAG
		/// </summary>
		private int m_iLineOfBus = 0; 

		/// <summary>
		/// Private variable for SYSTEM_TABLE_NAME
		/// </summary>
		private string m_sTableName = ""; 
		
		/// <summary>
		/// Private variable for RM_USER_ID
		/// </summary>
		private string m_sUser = ""; 

		/// <summary>
		/// Private variable for DTTM_LAST_UPDATE
		/// </summary>
		private string m_sLastUpdate = ""; 

		/// <summary>
		/// Private variable for User Table Name TABLE_NAME
		/// </summary>
		private string m_sUserTableName = ""; 

	    /// <summary>
	    /// Private variable for DELETED_FLAG
	    /// </summary>
		private int m_iIsDeleted = 0; 

		#endregion

		#region Constructor

		/// <summary>
		/// Constructor creates an instance from the data in the datarow
		/// </summary>
		/// <param name="p_objRow">Glossary table row</param>
		internal Glossary(DataRow p_objRow)
		{
            m_iReqRelTable = DbRow.GetIntField( p_objRow,"REQD_REL_TABL_FLAG");
			m_iIndTableId = DbRow.GetIntField( p_objRow,"IND_STAND_TABLE_ID");
			m_iTableId = DbRow.GetIntField( p_objRow,"TABLE_ID");
			m_iIsAttachment  = DbRow.GetIntField( p_objRow,"ATTACHMENTS_FLAG");
			m_iIsTree = DbRow.GetIntField( p_objRow,"TREE_DISPLAY_FLAG");
			m_iRelatedTableId = DbRow.GetIntField( p_objRow,"RELATED_TABLE_ID");
			m_iTypeCode = DbRow.GetIntField( p_objRow,"GLOSSARY_TYPE_CODE");
			m_iLineOfBus = DbRow.GetIntField( p_objRow,"LINE_OF_BUS_FLAG");
			m_sTableName = DbRow.GetStringField( p_objRow,"SYSTEM_TABLE_NAME");
			m_sUser = DbRow.GetStringField( p_objRow,"RM_USER_ID");
			m_sLastUpdate = DbRow.GetStringField( p_objRow,"DTTM_LAST_UPDATE");
			m_sUserTableName = DbRow.GetStringField( p_objRow,"TABLE_NAME");
			m_iIsDeleted =  DbRow.GetIntField( p_objRow,"DELETED_FLAG");
			// Added by Mihika for 'REQD_IND_TABL_FLAG'
			m_iReqIndTable = DbRow.GetIntField(p_objRow, "REQD_IND_TABL_FLAG");
		}
		#endregion

		#region Properties


		/// <summary>
		/// Internal property ReqRelTable
		/// </summary>
		internal int ReqRelTable
		{
			get
			{
				return m_iReqRelTable;
			}	
		}

		/// <summary>
		/// Internal property for IndTableId
		/// </summary>
		internal int IndTableId
		{
			get
			{
				return m_iIndTableId;
			}
		}

		/// <summary>
		/// Internal property for ReqIndTable
		/// </summary>
		internal int ReqIndTable
		{
			get
			{
				return m_iReqIndTable;
			}
		}
		/// <summary>
		/// Internal property for UserTableName
		/// </summary>
		internal string UserTableName
		{
			get
			{
				return m_sUserTableName;
			}
		}

		/// <summary>
		/// Internal property for LineOfBus
		/// </summary>
		internal int LineOfBus
		{
			get
			{
				return m_iLineOfBus;
			}
		}

		/// <summary>
		/// Internal property for TypeCode
		/// </summary>
		internal int TypeCode
		{
			get
			{
				return m_iTypeCode;
			}
		}

		/// <summary>
		/// Internal property for LastUpdate
		/// </summary>
		internal string LastUpdate
		{
			get
			{
				return m_sLastUpdate;
			}
		}

		/// <summary>
		/// Internal Property for RelatedTableId
		/// </summary>
		internal int RelatedTableId
		{
			get
			{
				return m_iRelatedTableId;
			}
		}	

		/// <summary>
		/// Internal property for User
		/// </summary>
		internal string User
		{
			get
			{
				return m_sUser;
			}
		}	

		/// <summary>
		/// Internal Property IsTree
		/// </summary>
		internal int IsTree
		{
			get
			{
				return m_iIsTree;
			}
		}

		/// <summary>
		/// Internal property IsDeleted
		/// </summary>
		internal int IsDeleted
		{
			get
			{
				return m_iIsDeleted;
			}
		}

		/// <summary>
		/// Internal property IsAttachment
		/// </summary>
		internal int IsAttachment
		{
			get
			{
				return m_iIsAttachment;
			}	
		}

		/// <summary>
		/// Internal Property TableName
		/// </summary>
		internal string TableName
		{
			get
			{
				return m_sTableName;
			}
		}

		/// <summary>
		/// Internal Property TableId
		/// </summary>
		internal int TableId
		{
			get
			{
				return m_iTableId;
			}	
		}

		#endregion

	}// End class
} // End namespace
