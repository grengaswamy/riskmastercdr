using System;
using System.Collections; 

namespace Riskmaster.Application.CodeUtility
{
	/// <summary>
	///Author  :   Navneet Sota & Pankaj Chowdhury
	///Dated   :   26th,Nov 2004
	///Purpose :   Code List contains a collection of Code objects [Code Table rows]
	/// </summary>
	internal class CodeList:CollectionBase
	{	

		#region Constructor

		/// <summary>
		/// Default Constructor
		/// </summary>
		internal CodeList(){}

		#endregion

		#region Methods

		/// Name		: Add
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds Code Object to the collection
		/// </summary>
		/// <param name="p_objCod">Code</param>
		internal void Add(Code p_objCod)
		{
			this.List.Add(p_objCod);
		}

		/// Name		: this
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the Code object for that index
		/// </summary>
		internal Code this[int p_iIndex]
		{        
			get
			{
				return (Code)this.List[p_iIndex];
			}        
		}

        /// Name		: Add
        /// Author		: Amandeep Kaur
        /// Date Created	: 11/26/2012		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Adds Code Object to the collection
        /// </summary>
        /// <param name="p_objCod">CodeDesc</param>
        internal void Add(CodeDesc p_objCod)
        {
            this.List.Add(p_objCod);
        }

        /// Name		: this
        /// Author		: Amandeep Kaur
        /// Date Created: 11/26/2012	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Returns the Code object for that index
        /// </summary>
        internal CodeDesc this[int p_iIndex,bool bCodeDesc]
        {
            get
            {
                return (CodeDesc)this.List[p_iIndex];
            }
        }
		#endregion

	}//End Class
}//End Namespace
