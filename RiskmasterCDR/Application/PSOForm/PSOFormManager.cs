using System;
using System.Xml;
using Riskmaster.DataModel;
using Riskmaster.Common;
using Riskmaster.Db;

namespace Riskmaster.Application.PSOForm
{
    public class PSOFormManager : Common, IDisposable
    {
        #region Variable Declaration
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;
        /// <summary>
        /// Private variable to store the instance of LocalCache object
        /// </summary>
        private LocalCache m_objLocalCache = null;
        /// <summary>
        /// Security DB connection string
        /// </summary>
        private string m_sSecurityConnectionString;
        private int m_iClientId;
        #endregion

        public PSOFormManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
            : base(p_sDsnName, p_sUserName, p_sPassword, p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, p_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = m_objDataModelFactory.Context.LocalCache;
        }

        ~PSOFormManager()
        {
            Dispose();
        }
        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }
        /// <summary>
        /// Read Property for LocalCache.
        /// </summary>
        public LocalCache LocalCache
        {
            get
            {
                if (m_objLocalCache == null)
                {
                    m_objLocalCache = m_objDataModelFactory.Context.LocalCache;
                }
                return (m_objLocalCache);
            }
        }
        /// <summary>
        /// Security Database Connection String
        /// </summary>
        public string SecurityConnectionString
        {
            get
            {
                return m_sSecurityConnectionString;
            }
            set
            {
                if (value != null)
                {
                    m_sSecurityConnectionString = value;
                }
                else
                {
                    m_sSecurityConnectionString = string.Empty;
                }
            }
        }
        public string GetCodeIdList(String sShortCodeList, String sCodeTable)
        {

            String[] sTempArr;
            String[] sTempArr1;
            Int32 iCount;
            String sCodeIdList = "";
            String sCodeIdList1 = "";
            try
            {
                sTempArr = sShortCodeList.Replace('%', ' ').Split('#');



                for (iCount = 0; iCount < sTempArr.Length; iCount++)
                {
                    sTempArr1 = sTempArr[iCount].Split(',');
                    for (Int32 iCount1 = 0; iCount1 < sTempArr1.Length; iCount1++)
                    {
                        if (!(sTempArr1[iCount1] == "*" || sTempArr1[iCount1].ToLower() == "else")) 
                        {
                            if (string.Compare(sCodeTable, "STATES", true) == 0)
                            {
                                sTempArr1[iCount1] = Convert.ToString(m_objDataModelFactory.Context.LocalCache.GetStateRowID(sTempArr1[iCount1].Trim()));
                            }
                            else
                            {
                                sTempArr1[iCount1] = Convert.ToString(m_objDataModelFactory.Context.LocalCache.GetCodeId(sTempArr1[iCount1].Trim(), sCodeTable));
                            }
                        }
                        if (iCount1 == 0)
                        {
                            sCodeIdList = sTempArr1[iCount1];
                        }
                        else
                        {
                            sCodeIdList = sCodeIdList + "," + sTempArr1[iCount1]; 
                        }
                    }
                    if (iCount == 0)
                    {
                        sCodeIdList1 = sCodeIdList;
                    }
                    else
                    {
                        sCodeIdList1 = sCodeIdList1 + "%#" + sCodeIdList;
                    }
                }
                return sCodeIdList1;
            }
            catch (Exception e)
            {
                return "abc";
            }
        }

        public void UpdateDocument()
        {
            string sSQL = string.Empty;
            
            try
            {
                sSQL = "UPDATE DOCUMENT SET UPLOAD_STATUS=-1 WHERE DOCUMENT_ID=(SELECT MAX(DOCUMENT_ID) FROM DOCUMENT)";
                using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                {
                    objConn.Open();
                    objConn.ExecuteNonQuery(sSQL);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetEventFuncId()
        {
            string sSQL = string.Empty;
            int iEventFuncId = 0;

            try
            {
                sSQL = "SELECT FUNC_ID FROM FUNCTION_LIST WHERE UPPER(FUNCTION_NAME) = UPPER('Event/Occurrence')";
              
                using (DbReader objReader = DbFactory.GetDbReader(m_sSecurityConnectionString, sSQL))
                {
                    if (objReader.Read())
                    {
                        iEventFuncId = objReader.GetInt("FUNC_ID");
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iEventFuncId;
        }
       

    }
}
