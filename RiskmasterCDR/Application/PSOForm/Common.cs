using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Riskmaster.Db;

namespace Riskmaster.Application.PSOForm
{
    public class CodeList
    {
        private string sShortCode;
        private string sCodeDesc;
        private int iCodeId;

        public string ShortCode 
        { 
            get 
            { 
                return sShortCode; 
            } 
            set 
            { 
                sShortCode = value; 
            } 
        }
        public string CodeDesc 
        { 
            get 
            {
                return sCodeDesc; 
            } 
            set 
            {
                sCodeDesc = value; 
            } 
        }
        public int CodeId 
        { 
            get 
            { 
                return iCodeId; 
            } 
            set 
            { 
                iCodeId = value; 
            } 
        }
    }
    public class Common 
    {
        internal Common(Context p_objContext)
            : base()
        {
            m_objContext = p_objContext;
            //m_bRoundFlag = setRoundFlag();

        }
        internal Common(string p_sDsnName, string p_sUserName, string p_sPassword,int iClientId=0)
            : base()
        {
            m_iClientId = iClientId;
            m_objDataModelFactory = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword,m_iClientId);
            m_objContext = m_objDataModelFactory.Context;
            //m_bRoundFlag = setRoundFlag();

        }

        #region Variable Declaration
        private Context m_objContext = null;
        private DataModelFactory m_objDataModelFactory = null;
        int m_iClientId;
        #endregion

        #region Variable Properties

        public string ConnectionString
        {
            get
            {
                return (m_objContext.DbConn.ConnectionString);
            }
        }

        public DataModelFactory DataModelFactory
        {
            get
            {
                return (m_objContext.Factory);
            }
        }

        public LocalCache LocalCache
        {
            get
            {
                return (m_objContext.LocalCache);
            }
        }

        public CCacheFunctions CCacheFunctions
        {
            get
            {
                return (m_objContext.InternalSettings.CacheFunctions);
            }
        }
        #endregion


        #region Common XML functions
        /// <summary>
        /// Initialize the XML document
        /// </summary>
        /// <param name="objXmlDocument">XML document</param>
        /// <param name="p_objRootNode">Root Node</param>
        /// <param name="p_objMessagesNode">Messages Node</param>
        /// <param name="p_sRootNodeName">Root Node Name</param>
        public void StartDocument(ref XmlDocument objXmlDocument, ref XmlElement p_objRootNode, string p_sRootNodeName)
        {
            try
            {
                objXmlDocument = new XmlDocument();
                p_objRootNode = objXmlDocument.CreateElement(p_sRootNodeName);
                objXmlDocument.AppendChild(p_objRootNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO",m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Node Name</param>		
        public void CreateElement(XmlElement p_objParentNode, string p_sNodeName)
        {
            XmlElement objChildNode = null;
            try
            {
                objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO",m_iClientId), p_objEx);
            }
            finally
            {
                objChildNode = null;
            }

        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        public void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO",m_iClientId), p_objEx);
            }

        }

        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>		
        public void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO",m_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        public void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO",m_iClientId), p_objEx);
            }
        }

        //public void ResetSysExData(XmlDocument SysEx, string sNode, string sValue)
        //{
        //    try { SysEx.DocumentElement.RemoveChild(SysEx.SelectSingleNode("/SysExData/" + sNode)); }
        //    catch { };
        //    CreateSysExData(SysEx, sNode, sValue);
        //}

        //public void CreateSysExData(XmlDocument SysEx, string sNode, string sValue)
        //{
        //    XmlNode objNew;
        //    //Determine if the specific Node already exists in the SysExData of the Instance Document
        //    if (SysEx.SelectSingleNode("/SysExData/" + sNode) == null)
        //    {
        //        //Create a new XML Element for the specified Xml Node
        //        objNew = SysEx.CreateElement(sNode);

        //        //Set the element inner text to an empty string
        //        objNew.AppendChild(SysEx.CreateCDataSection(sValue));
        //        //objNew.InnerText = FormDataAdaptor.CData();

        //        //Add the new XML Element to the existing Instance Data
        //        SysEx.DocumentElement.AppendChild(objNew);
        //    }
        //}

        public void SortXml(ref XmlElement p_SortXml, SortedList p_objPositiveEntries, SortedList p_objNegativeEntries)
        {


            for (int i = 0; i < p_objPositiveEntries.Count; i++)
            {
                p_SortXml.AppendChild((XmlNode)p_objPositiveEntries.GetByIndex(i));
            }

            for (int i = p_objNegativeEntries.Count - 1; i >= 0; i--)
            {
                p_SortXml.AppendChild((XmlNode)p_objNegativeEntries.GetByIndex(i));
            }

        }

        public void GetCodes(string[] arrTableNames, ref Dictionary<string, List<CodeList>> dicCodeList, ref Dictionary<string, List<string>> dicControlList)
        {
            StringBuilder sbSQL = new StringBuilder();
            int iCodeId = 0;
            bool bIsSuccess = false;
            try
            {
                sbSQL.Append("SELECT CODES.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY");
                sbSQL.Append(" WHERE GLOSSARY.SYSTEM_TABLE_NAME='#TABLENAME#'");
                sbSQL.Append(" AND GLOSSARY.TABLE_ID=CODES.TABLE_ID");
                sbSQL.Append(" AND CODES.CODE_ID=CODES_TEXT.CODE_ID");
                sbSQL.Append(" AND CODES_TEXT.LANGUAGE_CODE=1033");
                sbSQL.Append(" AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)");
                sbSQL.Append(" ORDER BY CODES.SHORT_CODE ASC");

                for (int iCount = 0; iCount < arrTableNames.Length; iCount++)
                {
                    List<CodeList> lstCodeList = new List<CodeList>();
                    List<string> lstControlName = new List<string>();
                    string sSQL = Convert.ToString(sbSQL).Replace("#TABLENAME#", arrTableNames[iCount]);

                    using (DbReader objReader = DbFactory.GetDbReader(m_objDataModelFactory.Context.DbConn.ConnectionString, sSQL))
                    {
                        while (objReader.Read())
                        {
                            iCodeId = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("CODE_ID")), out bIsSuccess);
                            CodeList objCodeList = new CodeList();
                            objCodeList.CodeId = iCodeId;
                            objCodeList.ShortCode = m_objDataModelFactory.Context.LocalCache.GetShortCode(iCodeId);
                            objCodeList.CodeDesc = m_objDataModelFactory.Context.LocalCache.GetCodeDesc(iCodeId);
                            lstCodeList.Add(objCodeList);
                            
                        }
                    }
                    dicCodeList.Add(arrTableNames[iCount], lstCodeList);
                    GetControlRefs(ref lstControlName, arrTableNames[iCount]);
                    dicControlList.Add(arrTableNames[iCount], lstControlName);
                }
            }
            catch (Exception e)
            {

            }
        }


        public string GetPropertyValue(Object objDataModel, string sProperty)
        {
            Type myType = objDataModel.GetType();
            string sValue = string.Empty;

            BindingFlags bBindingFlags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty;
            PropertyInfo prop1Info = myType.GetProperty(sProperty, bBindingFlags);
            sValue = Convert.ToString(prop1Info.GetValue(objDataModel, null));

            return sValue;
        }

        

        private void GetControlRefs(ref List<string>  lstControlName, string sTableName)
        {

            switch (sTableName.ToUpper())
            {
                case "YES_NO":
                    lstControlName.Add("psocdIdentifyReporter");
                    lstControlName.Add("cdPatientAge");
                    lstControlName.Add("psocdCLABSI");
                    lstControlName.Add("psocdAssocPneumonia");
                    lstControlName.Add("psocdCAUTI");
                    lstControlName.Add("cdPatientAge_Neo");
                    lstControlName.Add("psocdRestainedObject");
                    break;
                case "YES_NO_JCAHO":
                    lstControlName.Add("psocdEvidenceReport");
                    lstControlName.Add("psocdRescuePatient");
                    lstControlName.Add("cdResultLength");
                    lstControlName.Add("cdIncidentNotified");
                    lstControlName.Add("cdChkDocument");
                    lstControlName.Add("psocdFallObserved");
                    lstControlName.Add("psocdPhusicalInjury");
                    lstControlName.Add("psocdRiskAssessment");
                    lstControlName.Add("psocdDeterminedForFall");
                    lstControlName.Add("psocdMedicationFall");
                    lstControlName.Add("psocdMedicationContributed");
                    lstControlName.Add("psocdHAIPerson");
                    lstControlName.Add("cdPrimipara");
                    lstControlName.Add("psocdLaborinduced");
                    lstControlName.Add("psocdVaginalDeliveryMother");
                    lstControlName.Add("psocdVaginalDeliveryMotherNeonate");
                    lstControlName.Add("psocdRescuePatient_Neo");
                    lstControlName.Add("cdResultLength_Neo");
                    lstControlName.Add("cdIncidentNotified_Neo");
                    lstControlName.Add("cdDeviceReuse");
                    lstControlName.Add("cdDocHistory");
                    lstControlName.Add("cdSkinInspection");
                    lstControlName.Add("cdUlcerRisk");
                    lstControlName.Add("psocdIntervention");
                    lstControlName.Add("psocdUlcerdeviceUsed");
                    lstControlName.Add("psocdSecondaryMorbidity");
                    lstControlName.Add("cdMorbiditySuspected");
                    lstControlName.Add("cdProcEmergency");
                    lstControlName.Add("psocdAnesthesiologist");
                    lstControlName.Add("psocdXRayObtained");
                    lstControlName.Add("psocdRadiopaque");
                    lstControlName.Add("cdHandoverNM");
                    lstControlName.Add("cdHandoverINC");
                    lstControlName.Add("psocdEventFactor");
                    lstControlName.Add("psocdEventFactorNM");
                    lstControlName.Add("cdHITImplicated");
                    lstControlName.Add("cdHITImplicatedNM");
                    lstControlName.Add("psocdNQFEvent");
                    lstControlName.Add("psocdNQFEventNM");
                    break;
            }
        }
        #endregion
    }
}
