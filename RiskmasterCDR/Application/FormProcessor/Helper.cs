using System;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Microsoft.VisualBasic;

namespace Riskmaster.Application.FormProcessor
{
	///************************************************************** 
	///* $File		: Helper.cs
	///* $Revision	: 1.0.0.0 
	///* $Date		: 23-Dec-2004			
	///* $Author	: Tanuj Narula
	///* $Comment	: This class provides helper functions to vb.net scripts.Though Scripting can directly access DBReader object
	///               but to make the objects look structured, this class is introduced.
	///               All the helper functions for scripts can be put into this class.    
	///* $Source	: 
	///**************************************************************
	public class Helper:IDisposable
	{
		#region Variables
		/// <summary>
		/// Connection string for database
		/// </summary>
		string m_sConnectionString="";
		/// <summary>
		/// Reader object.
		/// </summary>
		DbReader m_objReader=null;
        int m_iClientId = 0;
		#endregion
		#region Constructor
		/// <summary>
		/// This is the constructor for Helper class.
		/// </summary>
		/// <param name="p_sConnectionString"></param>
		public Helper(string p_sConnectionString,int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
		}
        #endregion
		#region Properties
		/// <summary>
		/// Property for Connection string.
		/// </summary>
		public string ConnectionString
		{
			get
			{
				return m_sConnectionString;
			}
		}
		#endregion
		#region Functions
		/// Name		: GetDBReader
		/// Author		: Tanuj Narula
		/// Date Created:  23-Dec-2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function will return DBReader object.
		/// </summary>
		/// <param name="p_sSql">SQL query for DBReader.</param>
		/// <returns>Object of DBReader.</returns>
		public DbReader GetDBReader(string p_sSql)
		{
			try
			{
				m_objReader=DbFactory.GetDbReader(m_sConnectionString,p_sSql);
                return m_objReader;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Helper.GetDBReader.Error", m_iClientId), p_objException);
			}
		}
		/// Name		: CloseDBReader
		/// Author		: Tanuj Narula
		/// Date Created:  23-Dec-2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function closes the DBReader object.
		/// </summary>
		public void CloseDBReader()
		{
			try
			{
				if(m_objReader!=null)
				{
					m_objReader.Close();
					m_objReader.Dispose();
					m_objReader=null;
				}
			}
			catch(RMAppException p_objException)
			{
	         throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Helper.CloseDBReader.Error", m_iClientId), p_objException);
			}
			
		}
		/// Name		: DateDiff
		/// Author		: Tanuj Narula
		/// Date Created:  23-Dec-2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This method would give the number of days between the two dates that will be passed as parameters.
		/// It will compare the two dates and subtract the lesser date from the greater one.
		/// </summary>
		/// <param name="p_datDate1">Date</param>
		/// <param name="p_datDate2">Date</param>
		/// <returns>Number of days</returns>
		public  int DateDiff(DateTime p_datDate1,DateTime p_datDate2)
		{
			int iRetVal=0;
			try
			{
				if(p_datDate1.CompareTo(p_datDate2) < 0)
				{
					iRetVal=((TimeSpan)p_datDate2.Subtract(p_datDate1)).Days;
				}
				else
				{
					iRetVal=((TimeSpan)p_datDate1.Subtract(p_datDate2)).Days;
				}
               return iRetVal;
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Helper.DateDiff.Error", m_iClientId), p_objException);
			}
		}
		/// Name		: FromRMDateToDate
		/// Author		: Tanuj Narula
		/// Date Created:  23-Dec-2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function converts RM date type to Dotnet DateTime object.
		/// </summary>
		/// <param name="p_sRMDate">Date string in RM format.</param>
		/// <returns>Dotnet DateTime object.</returns>
		public DateTime FromRMDateToDate(string p_sRMDate)
		{
			try
			{
				return Riskmaster.Common.Conversion.ToDate(p_sRMDate);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Helper.FromRMDateToDate.Error", m_iClientId), p_objException);
			}
		}
		/// Name		: Dispose
		/// Author		: Tanuj Narula
		/// Date Created:  23-Dec-2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// This is a clean up function.
		/// </summary>
        public void Dispose()
		{
		 try
		 {
			 if(m_objReader!=null)
			 {
				 m_objReader.Close();
				 m_objReader.Dispose();
				 m_objReader=null;
			 }
		 }
		 catch(RMAppException p_objException)
		 {
			 throw p_objException;
		 }
		 catch(Exception p_objException)
		 {
             throw new RMAppException(Globalization.GetString("Riskmaster.Application.FormProcessor.GenericDisposeError", m_iClientId), p_objException);
		 }
		}
		public object GetDataString(object p_objParameter)
		{
			if(p_objParameter is string)
			{
				return m_objReader[(string)p_objParameter];
			}
			else
			{
              return m_objReader[(int)p_objParameter-1];
			}
		}
		
		#endregion
	}
}
