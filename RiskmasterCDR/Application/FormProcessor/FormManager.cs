using System;
using Riskmaster.Application.ExtenderLib;
using Riskmaster.ExceptionTypes;
using Riskmaster.Scripting;
using Riskmaster.Db;
using Riskmaster.Settings;
using System.IO;
using System.Collections;
using Riskmaster.Common;
using System.Xml;
using Riskmaster.Application.DocumentManagement ;
using Riskmaster.Application.AcrosoftWebserviceWrapper;
using Riskmaster.Security;
using Riskmaster.DataModel;
using System.Text.RegularExpressions;
using Riskmaster.Application.MediaViewWrapper;
namespace Riskmaster.Application.FormProcessor
{
	///************************************************************** 
	///* $File		: FormManager.cs
	///* $Revision	: 1.0.0.0 
	///* $Date		: 03-Jan-2005			
	///* $Author	: Tanuj Narula
	///* $Comment	: This class will process all the scripts and PDF forms.
	///* $Source	: 
	///**************************************************************
	public class FormManager:IDisposable

	{
		#region Constants
		/// <summary>
		/// Dll name of Extender library.
		/// </summary>
		private const string EXTENDERLIB_DLL="Riskmaster.Application.ExtenderLib.dll";
		/// <summary>
		/// Dll name of FormProcessor library.
		/// </summary>
		private const string FORMPROCESSOR_DLL="Riskmaster.Application.FormProcessor.dll";
		/// <summary>
		/// Namespace of Extender Library.
		/// </summary>
		private const string EXTENDERLIB_NAMESPACE="Riskmaster.Application.ExtenderLib";
		#endregion
		#region Variables
        /// <summary>
		/// Connection string for database.
		/// </summary>
		private string m_sConnectionString="";
		/// <summary>
		/// This will hold Scripting engine instance.
		/// </summary>
		/// <summary>
		/// UserLogin object for database.
		/// </summary>
		UserLogin m_userLogin = null;
		/// <summary>
		/// This will hold Scripting engine instance.
		/// </summary>
		ScriptEngine m_objScriptEngine=null;
		/// <summary>
		/// Container of all the data in PDF forms.
		/// </summary>
		PdfFormData m_objForm=null;
		/// <summary>
		/// Contains helper functions.
		/// </summary>
		Helper m_objHelper=null;
        private int m_iClientId = 0;
		#endregion
		#region Constructor
		public FormManager()
		{
            InitializeAcrosoftSettings();
		}
		/// <summary>
		/// Constructor for FormManager.
		/// </summary>
		/// <param name="p_sConnectionString">Database connection string</param>
        public FormManager(string p_sConnectionString, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sConnectionString=p_sConnectionString;
            InitializeAcrosoftSettings();
			this.InitScriptEngine();
		}
        public FormManager(UserLogin p_userlogin, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_userLogin = p_userlogin;
			m_sConnectionString = m_userLogin.objRiskmasterDatabase.ConnectionString;
            InitializeAcrosoftSettings();
			this.InitScriptEngine();
		}
    
        /// <summary>
        /// Initializes all required Acrosoft Settings
        /// </summary>
        private void InitializeAcrosoftSettings()
        {
            //Initiate all Acrosoft settings
            //TODO: Determine if this instantiation needs to be present in multiple classes
            //TODO: or if it can be instantiated and managed in a central location
            RMConfigurationManager.GetAcrosoftSettings();
        } // method: InitializeAcrosoftSettings

		#endregion
		#region Functions
		/// Name		: InitScriptEngine
		/// Author		: Tanuj Narula
		/// Date Created: 03-Jan-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function initializes the script engine.
		/// </summary>
		private void InitScriptEngine()
		{
			try
			{
               m_objScriptEngine = new ScriptEngine(m_sConnectionString,m_iClientId);//dvatsa-cloud
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FormManager.InitScriptEngine.Error", m_iClientId), p_objException);
			}

		}
		/// Name		: ProcessForm
		/// Author		: Tanuj Narula
		/// Date Created: 03-Jan-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function processes the script and creates as .FDF file
		/// </summary>
		/// <param name="p_sTemplate">PDF file path</param>
		/// <param name="p_sScriptFileName">Script file path</param>
		/// <param name="p_sOutputFileName">.FDF file path</param>
		/// <param name="p_objExt">Object of Extender library</param>
		public virtual void ProcessForm(string p_sTemplate,string p_sScriptFileName,string p_sOutputFileName,FROIExtender p_objExt)
		{
		
			
			string sScript="";
			StreamReader objRead=null;
			//m_objHelper.GetDataString("tanuj");
		
			try
			{
				if(p_sTemplate==null || p_sTemplate=="")
				{
                    throw new RMAppException(Globalization.GetString("Riskmaster.Application.FormProcessor.EmptyFilePathError", m_iClientId));
				}

                Validate.ValidateFileLocation(p_sScriptFileName, m_iClientId);
                Validate.ValidateFileLocation(p_sOutputFileName, m_iClientId);
				if(p_objExt.GetType().Namespace!=EXTENDERLIB_NAMESPACE)
				{
                    throw new RMAppException(Globalization.GetString("FormManager.ProcessForm.ObjectError", m_iClientId));
				}
				this.InitializeGlobalObjects();
				this.ExposeObjectToScripts("m_objForm",m_objForm.GetType().FullName,FORMPROCESSOR_DLL,m_objForm.GetType().Namespace, m_objForm);
				this.ExposeObjectToScripts("p_objExt",p_objExt.GetType().FullName,EXTENDERLIB_DLL,p_objExt.GetType().Namespace,p_objExt);
				this.ExposeObjectToScripts("m_objHelper",m_objHelper.GetType().FullName,FORMPROCESSOR_DLL,m_objHelper.GetType().Namespace,m_objHelper);
				objRead=File.OpenText(p_sScriptFileName);
				sScript=objRead.ReadToEnd();
				//m_objScriptEngine.AddScript("FormProcessorScript",sScript);
				sScript=null;
				m_objScriptEngine.SetGlobal(ScriptEngine.SCR_CACHE_OBJ,new LocalCache(m_sConnectionString,m_iClientId));
				m_objScriptEngine.Compile();
				m_objScriptEngine.StartEngine();
				m_objScriptEngine.RunScriptMethod("FormMain");
				m_objScriptEngine.StopEngine();
				this.PutDataToFDFFile(p_sOutputFileName,p_sTemplate,ref m_objForm);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FormManager.ProcessForm.Error", m_iClientId), p_objException);
			}
			finally
			{
				if(m_objScriptEngine!=null)
				{
					m_objScriptEngine=null;
				}
				if(m_objForm!=null)
				{
					m_objForm.Dispose();
					m_objForm=null;
				}
				if(m_objHelper!=null)
				{
					m_objHelper.Dispose();
					m_objHelper=null;
				}
				sScript=null;
				if(objRead!=null)
				{
					objRead.Close();
					objRead=null;
				}

			}
			
		}
		/// Name		: PutDataToFDFFile
		/// Author		: Tanuj Narula
		/// Date Created: 03-Jan-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function extracts the data out of Form collection and writes it to .FDF file.
		/// </summary>
		/// <param name="p_sFDFFilePath">Path of .FDF file</param>
		/// <param name="p_sTemplate">Path to PDF form</param>
		/// <param name="p_objForm">Form containing all the data</param>
		protected virtual  void PutDataToFDFFile(string p_sFDFFilePath,string p_sTemplate,ref PdfFormData p_objForm)
		{
			FileStream objFs=null;
			StreamWriter objWriter=null;
			try
			{
				objFs=File.OpenWrite(p_sFDFFilePath);
				objWriter=new StreamWriter(objFs);
				objWriter.Write("%FDF-1.2" + "\r\n");
				objWriter.Write("1 0 obj <<" + "\r\n");
				objWriter.Write("/FDF <<" + "\r\n");
				objWriter.Write("/Fields" + "\r\n");
				objWriter.Write("[" + "\r\n");
				//Pull out the data...
				foreach(string sKey in m_objForm.Form.Keys)
				{
					objWriter.Write("<</T (");
					objWriter.Write(sKey);
					objWriter.Write(") /V (");
					objWriter.Write(m_objForm.Form[sKey]);
					objWriter.Write(")>>" + "\r\n");
				}
				objWriter.Write("]" + "\r\n");
				objWriter.Write("/F (");
				objWriter.Write(p_sTemplate);
				objWriter.Write(")"+ "\r\n");
				objWriter.Write(">>" +"\r\n");
				objWriter.Write(">>" +"\r\n");
				objWriter.Write("endobj" +"\r\n");
				objWriter.Write("trailer" +"\r\n");
				objWriter.Write("<</Root 1 0 R>>" +"\r\n");
				objWriter.Write("%%EOF");
				objWriter.Flush();
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FormManager.PutDataToFDFFile.Error", m_iClientId), p_objException);
			}
			finally
			{
				if(objFs!=null)
				{
					objFs.Close();
					objFs=null;
				}
				if(objWriter!=null)
				{
					objWriter=null;
				}
			}
		}
		/// Name		: ExposeObjectToScripts
		/// Author		: Tanuj Narula
		/// Date Created: 03-Jan-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function exposes the objects to the scripts.
		/// </summary>
		/// <param name="p_sName">Name of the object.</param>
		/// <param name="p_sTypeName">Name of the type.</param>
		/// <param name="p_sAssemblyName">Name of the assembly.</param>
		/// <param name="p_sNamespace">Namespace name.</param>
		/// <param name="p_objGlobal">Object to be exposed.</param>
		protected virtual void ExposeObjectToScripts(string p_sName,string p_sTypeName,string p_sAssemblyName,string p_sNamespace,object p_objGlobal)
		{
			try
			{
				if(p_sName!=null && p_sName.Trim()!="" && p_sTypeName!=null && p_sTypeName.Trim()!="" && p_sAssemblyName!=null && p_sAssemblyName.Trim()!="" && p_sNamespace!=null && p_sNamespace.Trim()!="" && p_objGlobal!=null)
				{
					m_objScriptEngine.AddGlobal(p_sName,p_sTypeName,p_sAssemblyName,p_sNamespace);
					m_objScriptEngine.SetGlobal(p_sName,p_objGlobal);
				}
				else
				{
                    throw new RMAppException(Globalization.GetString("FormManager.ExposeObjectToScripts.NullError", m_iClientId));
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FormManager.ExposeObjectToScripts.Error", m_iClientId), p_objException);
			}
			
		}
		/// Name		: InitializeGlobalObjects
		/// Author		: Tanuj Narula
		/// Date Created: 03-Jan-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This function initializes the Form and Helper objects.
		/// </summary>
		protected virtual void InitializeGlobalObjects()
		{
			try
			{
				m_objForm=new PdfFormData();
                m_objHelper = new Helper(m_sConnectionString, m_iClientId);
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("FormManager.IntializeGlobalObjects.Error", m_iClientId), p_objException);
			}
		}
		/// Name		: Dispose
		/// Author		: Tanuj Narula
		/// Date Created: 03-Jan-2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// This is a clean up function.
		/// </summary>
		public void Dispose()
		{
			try
			{
				if(m_objScriptEngine!=null)
				{
					m_objScriptEngine=null;
				}
				if(m_objForm!=null)
				{
					m_objForm.Dispose();
					m_objForm=null;
				}
				if(m_objHelper!=null)
				{
					m_objHelper.Dispose();
					m_objHelper=null;
				}
			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Riskmaster.Application.FormProcessor.GenericDisposeError", m_iClientId), p_objException);
			}
		}
		
		/// Name			: SavePdfForm
		/// Author			: Tanuj Narula
		/// Date Created	: 18-May-2006
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended	*   Amendment   *    Author
		///					*				*
		///					*				*	
		/// ************************************************************
		/// <summary>
		/// This function saves the PDF form and attaches it to the claim.
		/// </summary>
		/// <param name="p_objDocKeyValuePair">Posted PDF data in Key/Value pairs.</param>
		/// <param name="p_sOutputFDFFilePathAndName">File location of the output file.</param>
		public void SavePdfForm( ref XmlDocument p_objDocKeyValuePair , string p_sOutputFDFFilePathAndName)
		{
			XmlNodeList objKeysVals = null;
			string sTemplatePath = "" ;
			string sPaths = "";
            //skhare7:MITS 23407
            string FormFileName = string.Empty;
            SysSettings objSettings = null;
			string sClaimId = "";
			Claim objClaim = null;
            Acrosoft objAcrosoft = null;
            string sSql = string.Empty;
            DbReader objReader = null;
            string sClaimNumber = "";
            string sFdfFileName = string.Empty;
            string sEventNumber = string.Empty;
            //skhare7:MITS 23407 End
            MediaView oMediaView = null;//Deb : MITS 27328
			try
			{
				
				sClaimId = p_objDocKeyValuePair.SelectSingleNode("//ClaimId").InnerText;
				objKeysVals = p_objDocKeyValuePair.SelectNodes("//parameter") ;
				m_objForm = new PdfFormData() ;

				foreach(XmlNode objNode in objKeysVals)
				{
					if(objNode.FirstChild.InnerText =="pg" || objNode.FirstChild.InnerText.Trim().ToLower() =="btnSaveOnly")
					{
						continue ;
					}
					m_objForm.Form.Add(objNode.FirstChild.InnerText , objNode.LastChild.InnerText);
                    //skhare7:MITS 23407
                    
                    if (objNode.FirstChild.InnerText == "FDF_FILENAME")
                    {
                        sFdfFileName = objNode.LastChild.InnerText;                        
                        Regex re = new Regex(@".*/(?<filename>.*)\.pdf$");
                        //sClaimNumber = sFdfFileName.Substring(0, sFdfFileName.IndexOf(re.Match(sFdfFileName).Groups["filename"].Value + ".fdf"));

                        //rsolanki2: removing the ".fdf" as it was causing issues with the fdf file having version # appending there. 
                        sClaimNumber = sFdfFileName.Substring(0, sFdfFileName.IndexOf(re.Match(sFdfFileName).Groups["filename"].Value));

                    }

                    //if (objNode.FirstChild.InnerText == "INSURER_CLAIM_NO" || objNode.FirstChild.InnerText=="CLAIM_NUMBER")
                    //{
                    //    sClaimNumber = objNode.LastChild.InnerText;


                    //}
                    //skhare7:MITS 23407
				}
				
				sPaths = p_objDocKeyValuePair.SelectSingleNode("//name[text() = 'FDF_FILENAME']").NextSibling.InnerText ;
				sTemplatePath = sPaths.Substring( sPaths.IndexOf("||")+2) ;
               	this.PutDataToFDFFile(p_sOutputFDFFilePathAndName ,sTemplatePath ,ref m_objForm);

				//Raman Bhatia..07/27/2005
				//Post the document to Acrosoft if Acrosoft Interface is enabled

                //skhare7:MITS 23407 
				objSettings = new SysSettings(m_sConnectionString, m_iClientId);
				if (objSettings.UseAcrosoftInterface)
				{
                    sSql = "select E.EVENT_NUMBER,C.CLAIM_ID from CLAIM C,EVENT E where C.EVENT_ID=E.EVENT_ID and C.CLAIM_NUMBER='" + sClaimNumber +"'";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader.Read())
                    {
                        sEventNumber = Conversion.ConvertObjToStr(objReader[0]);
                        p_objDocKeyValuePair.SelectSingleNode("//ClaimId").InnerText = Conversion.ConvertObjToStr(objReader[1]);
                    }

                    ////DataModelFactory objDataModelFactory = new DataModelFactory(m_userLogin);
                    ////objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    ////objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));
                    ////sClaimNumber = objClaim.ClaimNumber;
                    ////string sEventNumber = objClaim.EventNumber;
                    //skhare7:MITS 23407 End
					string sAppExcpXml = "";
                    //sgupta243 changes for the MITS 29961  3/10/2012
					//string sDocTitle = "FROI (" + sPaths.Substring( 0 ,  sPaths.IndexOf("||")) + ")";
                    string sDocTitle = sPaths.Substring(0, sPaths.IndexOf("||"));
                    
                    objAcrosoft = new Acrosoft(m_iClientId);//dvatsa-cloud
					
                    //rsolanki2 :  start updates for MCM mits 19200 
                    Boolean bSuccess = false;
                    string sTempAcrosoftUserId = m_userLogin.LoginName;
                    string sTempAcrosoftPassword = m_userLogin.Password;

                    if (Conversion.CastToType<Boolean>(AcrosoftSection.UseCommonAcrosoftUser, out bSuccess))
                    {
                        if (AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName] != null
                            && !string.IsNullOrEmpty(AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].RmxUser))
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftUserId;
                            sTempAcrosoftPassword = AcrosoftSection.AcroUserMapSection.UserMap[m_userLogin.LoginName].AcrosoftPassword;
                        }
                        else
                        {
                            sTempAcrosoftUserId = AcrosoftSection.AcrosoftUsername;
                            sTempAcrosoftPassword = AcrosoftSection.AcrosoftUserPassword;
                        }
                    }
                    //skhare7:MITS 23407
                    FormFileName = sPaths.Substring(0, sPaths.IndexOf("||"));
                    string sFolderKey =  sEventNumber +""+ "-"+"" + sClaimNumber;
                    objAcrosoft.DeletePDF(sTempAcrosoftUserId, sTempAcrosoftPassword, FormFileName,
                        sEventNumber, sClaimNumber, 
                          AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    //skhare7:MITS 23407 End
                     objAcrosoft.CreateAttachmentFolder(sTempAcrosoftUserId, sTempAcrosoftPassword,
                        AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                    AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    //storing document
                    objAcrosoft.StoreObjectBuffer(sTempAcrosoftUserId, sTempAcrosoftPassword, p_sOutputFDFFilePathAndName,
                        sDocTitle, "Report", sEventNumber, sClaimNumber.ToString(),
                        AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, string.Empty, sTempAcrosoftUserId, out sAppExcpXml);

                    //objAcrosoft.CreateAttachmentFolder(m_userLogin.LoginName, m_userLogin.Password, AcrosoftSection.AcrosoftAttachmentsTypeKey, sEventNumber, sClaimNumber,
                    //AcrosoftSection.EventFolderFriendlyName, AcrosoftSection.ClaimFolderFriendlyName, out sAppExcpXml);
                    ////storing document
                    //objAcrosoft.StoreObjectBuffer(m_userLogin.LoginName, m_userLogin.Password, p_sOutputFDFFilePathAndName, sDocTitle, "Report", sEventNumber, sClaimNumber.ToString(),
                    //    AcrosoftSection.AcrosoftAttachmentsTypeKey, string.Empty, string.Empty, m_userLogin.LoginName, out sAppExcpXml);

               
                    //rsolanki2 :  end updates for MCM mits 19200 
					

				}
                //Deb : MITS 27328
                if (objSettings.UseMediaViewInterface)
                {
                    string sTempFileName = string.Empty;
                    DataModelFactory objDataModelFactory = new DataModelFactory(m_userLogin, m_iClientId);
                    objClaim = (Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                    objClaim.MoveTo(Conversion.ConvertStrToInteger(sClaimId));
                    objDataModelFactory.UnInitialize();
                    sClaimNumber = objClaim.ClaimNumber;
                    //sgupta243 changes for the MITS 29961 3/10/2012
                    // string sDocTitle = "FROI (" + sPaths.Substring(0, sPaths.IndexOf("||")) + ")";
                    string sDocTitle = sPaths.Substring(0, sPaths.IndexOf("||"));

                 
                    if (sDocTitle.Length > 20)
                    {
                        sTempFileName = sDocTitle.Substring(0, 20);
                    }
                    oMediaView = new MediaView();
                    Boolean bSuccess = false;
                    string sFileName = sPaths.Substring(0, sPaths.IndexOf("||"));
                    string sPath = p_sOutputFDFFilePathAndName.Substring(0, p_sOutputFDFFilePathAndName.LastIndexOf(@"\"));
                    oMediaView.MMDeleteDocumentByName(sClaimNumber, "Claim", sTempFileName);
                    importList oList = new importList(sClaimNumber, "Claim", sDocTitle);
                    bSuccess = oMediaView.MMUploadDocument(sFileName, sPath, oList);
                    oList = null;
                }
                //Deb: MITS 27328
			}
			catch(Exception p_objErr)
			{
				throw p_objErr;
			}
			finally
			{
				objSettings = null;
				objAcrosoft = null;
                if(objClaim!=null)
				    objClaim.Dispose();
				this.Dispose();
                //Deb : MITS 27328
                if(oMediaView!=null)
                   oMediaView.Dispose();
                //Deb : MITS 27328
			}
		}
		
		#endregion

	}

}
