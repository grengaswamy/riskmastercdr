﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Collections;
using Riskmaster.ActiveScript;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.FormProcessor
{
    /// <summary>
    /// Form processor class.
    /// </summary>
    public class FormProcessor
    {
        /// <summary>
        /// Processes the form.
        /// </summary>
        /// <param name="sPDFLink">The s PDF link.</param>
        /// <param name="p_sScriptFileName">Name of the P_S script file.</param>
        /// <param name="p_sOutputFileName">Name of the P_S output file.</param>
        /// <param name="objExt">The object ext.</param>
        /// <exception cref="Riskmaster.ExceptionTypes.RMAppException"></exception>
        public void ProcessForm(string sPDFLink,string p_sScriptFileName, string p_sOutputFileName, object objExt)
        {
             PdfFormData Form = default(PdfFormData);
             StreamWriter objWrite = default(StreamWriter);
             VBScript script = null;
            try
            {
                if (!File.Exists(p_sScriptFileName))
                {
                    throw new RMAppException(string.Format("Exception: File not found."));
                }

                script = new VBScript();
                Form = new PdfFormData();
                objWrite = new StreamWriter(p_sOutputFileName);
               
                script.AddObject("Form", Form.Form);
                script.AddObject("objExt", objExt);

                script.AddScript(GetScript(p_sScriptFileName));
                script.Open();

                int result = Convert.ToInt32(script.ExecuteMethod("FormMain", new object[] { }));

                objWrite.Write("%FDF-1.2\r\n");
                objWrite.Write("1 0 obj <<\r\n");
                objWrite.Write("/FDF <<\r\n");
                objWrite.Write("/Fields\r\n");
                objWrite.Write("[\r\n");
             
                foreach (string key in Form.Form.Keys)
                {
                    objWrite.Write("<</T ");
                    objWrite.Write("(" + key.ToString().Replace(")", "\\)").Replace("(", "\\(") + ")"); //RMA-1108 Jira issue	
                    objWrite.Write(" /V ");

                    if (Form.Form[key] == null)
                    {
                        objWrite.Write("(" + " " + ")");
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Form.Form[key].ToString()))
                        {
                            objWrite.Write("(" + Form.Form[key].ToString().Replace(")", "\\)").Replace("(", "\\(")  + ")");
                        }
                        else
                        {
                            objWrite.Write("(" + " " + ")");
                        }
                    }
                    objWrite.Write(">>\r\n");
                }
                objWrite.Write("]\r\n");
                objWrite.Write("/F ");

                //Get position of last '/' character in the path pdflink
                //retrieve only path

                objWrite.Write("(" + sPDFLink + ")");
                objWrite.Write("\r\n");
                objWrite.Write(">>\r\n");
                objWrite.Write(">>\r\n");
                objWrite.Write("endobj\r\n");
                objWrite.Write("trailer\r\n");
                objWrite.Write("<</Root 1 0 R>>\r\n");
                objWrite.Write("%%EOF\r\n");
              

                Form.Form.Clear();
                objWrite.Close();
                script.Close();
            }
            catch (Exception p_objEx)
            {
                if (!string.IsNullOrEmpty(p_objEx.InnerException.Message))
                {
                    throw new RMAppException(string.Format("Exception:{0} \n Path: {1}", p_objEx.InnerException.Message, sPDFLink), p_objEx);
                }
                else 
                {
                    throw new RMAppException(string.Format("Exception:{0} \n Path: {1}", p_objEx.Message, sPDFLink), p_objEx);
                }
            }
            finally
            {
                Form = null;
                objWrite = null;
                script = null;
            }
        }

        public void MergeFormHttp(string p_sOutputFileName, string p_sScriptFileName, object pForm, int p_iClientId)
        {
            try
            {
                StreamWriter objWrite = new StreamWriter(p_sOutputFileName);
                PdfFormData Form = new PdfFormData();

                objWrite.Write("%FDF-1.2\r\n");
                objWrite.Write("1 0 obj <<\r\n");
                objWrite.Write("/FDF <<\r\n");
                objWrite.Write("/Fields\r\n");
                objWrite.Write("[\r\n");

                foreach (string key in Form.Form.Keys)
                {
                    objWrite.Write("<</T ");
                    objWrite.Write("("+key.ToString()+")");
                    objWrite.Write(" /V ");

                    if (Form.Form[key] == null)
                    {
                        objWrite.Write("(" + " " + ")");
                    }
                    else
                    {

                        if (!string.IsNullOrEmpty(Form.Form[key].ToString()))
                        {
                            objWrite.Write("(" + Form.Form[key].ToString() + ")");
                        }
                        else
                        {
                            objWrite.Write("(" + " " + ")");
                        }
                    }

                    objWrite.Write(">>\r\n");
                }

                objWrite.Write("]\r\n");
                objWrite.Write("/F ");
                objWrite.Write("(" + p_sScriptFileName + ")");
                objWrite.Write("\r\n");
                objWrite.Write(">>\r\n");
                objWrite.Write(">>\r\n");
                objWrite.Write("endobj\r\n");
                objWrite.Write("trailer\r\n");
                objWrite.Write("<</Root 1 0 R>>\r\n");
                objWrite.Write("%%EOF\r\n");

                objWrite.Close();
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Extender.GetEntity.AppError", p_iClientId), p_objEx);
            }
        }
        public void MergeFormHttpToTempFile(string p_sOutputFileName, string p_sScriptFileName, object pForm)
        {
            try
            {
                using (StreamWriter objWrite = new StreamWriter(p_sOutputFileName))
                {
                    using (PdfFormData Form = new PdfFormData())
                    {

                        objWrite.Write("%FDF-1.2\r\n");
                        objWrite.Write("1 0 obj <<\r\n");
                        objWrite.Write("/FDF <<\r\n");
                        objWrite.Write("/Fields\r\n");
                        objWrite.Write("[\r\n");

                        foreach (string key in Form.Form.Keys)
                        {
                            objWrite.Write("<</T ");
                            objWrite.Write("(" + key.ToString() + ")");
                            objWrite.Write(" /V ");

                            if (Form.Form[key] == null)
                            {
                                objWrite.Write("(" + " " + ")");
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(Form.Form[key].ToString()))
                                {
                                    objWrite.Write("(" + Form.Form[key].ToString() + ")");
                                }
                                else
                                {
                                    objWrite.Write("(" + " " + ")");
                                }
                            }

                            objWrite.Write(">>\r\n");
                        }

                        objWrite.Write("]\r\n");
                        objWrite.Write("/F ");
                        objWrite.Write("(" + p_sScriptFileName + ")");
                        objWrite.Write("\r\n");
                        objWrite.Write(">>\r\n");
                        objWrite.Write(">>\r\n");
                        objWrite.Write("endobj\r\n");
                        objWrite.Write("trailer\r\n");
                        objWrite.Write("<</Root 1 0 R>>\r\n");
                        objWrite.Write("%%EOF\r\n");

                        objWrite.Close();
                    }
                }
            }
            catch(Exception Ex)
            {
            
            }
        }

        public void ProcessFormHttp(string p_sScriptFileName)
        { 
        
        }


        private string GetScript(string scriptname)
        {
            string retval = "";

            using (StreamReader reader = new StreamReader(scriptname))
            {
                retval = reader.ReadToEnd();
                reader.Close();
            }

            return retval;
        }

    }
  
    //public class PdfFormData : IDisposable
    //{
    //    #region variables
    //    /// <summary>
    //    /// This will contain the key and the corresponding data.
    //    /// </summary>
    //    private Hashtable m_htKeyValuePair = null;
    //    #endregion
    //    #region Constructor
    //    /// <summary>
    //    /// This is the default constructor.
    //    /// </summary>
    //    public PdfFormData()
    //    {
    //        this.m_htKeyValuePair = new Hashtable();
    //    }
    //    #endregion
    //    #region Properties
    //    /// <summary>
    //    /// This is the property exposing Hashtable.
    //    /// </summary>
    //    public Hashtable Form
    //    {
    //        get
    //        {
    //            return m_htKeyValuePair;
    //        }
    //        set
    //        {
    //            m_htKeyValuePair = value;
    //        }

    //    }

        
    //    #endregion
    //    #region Functions
    //    /// Name		: Dispose
    //    /// Author		: Tanuj Narula
    //    /// Date Created: 22-Dec-2004			
    //    ///************************************************************
    //    /// Amendment History
    //    ///************************************************************
    //    /// Date Amended   *   Amendment   *    Author
    //    /// <summary>
    //    /// This is a clean up function.
    //    /// </summary>
    //    public void Dispose()
    //    {
    //        if (m_htKeyValuePair != null)
    //        {
    //            m_htKeyValuePair = null;
    //        }
    //    }
    //    #endregion
    //}
}
