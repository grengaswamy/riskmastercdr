using System;
using Riskmaster.ExceptionTypes;
using System.IO;
using Riskmaster.Common;

namespace Riskmaster.Application.FormProcessor
{
	///************************************************************** 
	///* $File		: Validate.cs 
	///* $Revision	: 1.0.0.0 
	///* $Date		: 21-Dec-2004	
	///* $Author	: Tanuj Narula
	///* $Comment	: 
	///* $Source	: 
	///**************************************************************

	internal class Validate
	{
		#region Constructor
		/// <summary>
		/// This is the default constructor.
		/// </summary>
		internal Validate()
			{
			}
		#endregion
		#region Functions
		/// Name		: ValidateFileLocation
		/// Author		: Tanuj Narula
		/// Date Created: 21-Dec-2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		/// <summary>
		/// This function will validate the file path locations.
		/// </summary>
		/// <param name="p_sFileName">File path to be validated</param>
		public static void ValidateFileLocation(string p_sFileName, int p_iClientId)
		{
			try
			{
				if(p_sFileName==null || p_sFileName=="")
				{
                    throw new RMAppException(Globalization.GetString("Riskmaster.Application.FormProcessor.EmptyFilePathError", p_iClientId));
				}
				if(!File.Exists(p_sFileName))
				{
                    throw new RMAppException(Globalization.GetString("Validate.ValidateFileLocation.FileNotFound", p_iClientId)); 
				}

			}
			catch(RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Validate.ValidateFileLocation.Error", p_iClientId), p_objException);
			}
		}
		#endregion
	}
}
