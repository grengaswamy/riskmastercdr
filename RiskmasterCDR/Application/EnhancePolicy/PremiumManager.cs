﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.EnhancePolicy.Billing ;


namespace Riskmaster.Application.EnhancePolicy
{
    /**************************************************************
	 * $File		: PremiumManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/

    public struct PremiumAmounts
    {
        private double dblManualPremium;
        private double dblExpModFactor;
        private double dblExpModFactorAmount;
        private double dblModifiedPremium;
        private ArrayList objDiscountList;
        private ArrayList objDiscountTierList;
        private double dblFinalPremium;
        private double dblAuditedPremium;
        private double dblEstimatedPremium;
        private double dblTransactionalPremium;
        private double dblTotalBilledPremium;
        private double dblChgBilledPremium;
        private double dblExpenseConstant;//pmahli MITS 9275 Added ExpenseConstant
        //Anu Tennyson for VACo Prem. Calculation MITS 18229
        private double dblTotalModifier;
        //Anu Tennyson for VACo Prem. Calculation MITS 18229
        //npadhy RMSC retrofit Starts
        private double dblTax;
        private double dblTransactionTax;
        private int intTaxFlatOrPercent;
        private double dblTaxRate;
        private double dblManualFinalPremium;
        //npadhy RMSC retrofit Ends

        internal double ManualPremium { get { return dblManualPremium; } set { dblManualPremium = value; } }
        internal double ExpModFactor { get { return dblExpModFactor; } set { dblExpModFactor = value; } }
        internal double ExpModFactorAmount { get { return dblExpModFactorAmount; } set { dblExpModFactorAmount = value; } }
        internal double ModifiedPremium { get { return dblModifiedPremium; } set { dblModifiedPremium = value; } }
        internal ArrayList DiscountList { get { return objDiscountList; } set { objDiscountList = value; } }
        internal ArrayList DiscountTierList { get { return objDiscountTierList; } set { objDiscountTierList = value; } }
        internal double FinalPremium { get { return dblFinalPremium; } set { dblFinalPremium = value; } }
        internal double AuditedPremium { get { return dblAuditedPremium; } set { dblAuditedPremium = value; } }
        internal double EstimatedPremium { get { return dblEstimatedPremium; } set { dblEstimatedPremium = value; } }
        internal double TransactionalPremium { get { return dblTransactionalPremium; } set { dblTransactionalPremium = value; } }
        internal double TotalBilledPremium { get { return dblTotalBilledPremium; } set { dblTotalBilledPremium = value; } }
        internal double ChgBilledPremium { get { return dblChgBilledPremium; } set { dblChgBilledPremium = value; } }
        //pmahli MITS 9275 Added ExpenseConstant
        internal double ExpenseConstant { get { return dblExpenseConstant; } set { dblExpenseConstant = value; } }
        //npadhy RMSC retrofit Starts
        internal double Tax { get { return dblTax; } set { dblTax = value; } }
        internal double TransactionTax { get { return dblTransactionTax; } set { dblTransactionTax = value; } }
        internal int TaxFlatOrPercent { get { return intTaxFlatOrPercent; } set { intTaxFlatOrPercent = value; } }
        internal double TaxRate { get { return dblTaxRate ; } set { dblTaxRate = value; } }
        //npadhy RMSC retrofit Ends
        //Anu Tennyson for VACo Prem. Calculation MITS 18229
        internal double TotalModifier { get { return dblTotalModifier; } set { dblTotalModifier = value; } }
        //Anu Tennyson for VACo Prem. Calculation MITS 18229

        internal double ManualFinalPremium { get { return dblManualFinalPremium; } set { dblManualFinalPremium = value; } }
    }

    public struct Discount
    {
        private int iNameId;
        private string sName;
        private string sType;
        private double dblAmountFactor;
        private double dblAmount;
        private int iLevel;
        private int iId;

        internal int NameId { get { return iNameId; } set { iNameId = value; } }
        internal string Name { get { return sName; } set { sName = value; } }
        internal string Type { get { return sType; } set { sType = value; } }
        internal double AmountFactor { get { return dblAmountFactor; } set { dblAmountFactor = value; } }
        internal double Amount { get { return dblAmount; } set { dblAmount = value; } }
        internal int Level { get { return iLevel; } set { iLevel = value; } }
        internal int Id { get { return iId; } set { iId = value; } }
    }

    public struct DiscountTier
    {
        private int iNameId;
        private double dblAmount;
        private string sName;
        private int iLevelId;
        private int iId;

        internal int NameId { get { return iNameId; } set { iNameId = value; } }
        internal int Id { get { return iId; } set { iId = value; } }
        internal double Amount { get { return dblAmount; } set { dblAmount = value; } }
        internal string Name { get { return sName; } set { sName = value; } }
        internal int LevelId { get { return iLevelId; } set { iLevelId = value; } }
    }
    
    // Naresh Connection Leak
    public class PremiumManager : Common,IDisposable
    {       

        #region Constructor

        internal PremiumManager(PolicyEnh p_objPolicyEnh): base(p_objPolicyEnh.Context)
        {
            m_objPolicyEnh = p_objPolicyEnh;
        }

        public PremiumManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
            : base(p_sDsnName, p_sUserName, p_sPassword, p_iClientId)
		{							
		}

        #endregion 

        #region Global Variables
        
        private PolicyEnh m_objPolicyEnh = null;

        private PolicyEnh PolicyEnh
        {
            get
            {
                return (m_objPolicyEnh);
            }
        }

        #endregion 

        #region Build Screen

        public void CreatePremiumCalculationUI(int p_iTransNumber, bool bBuildFlag, bool bEditFlag, bool bCalcFlag, string sTransactionType, EnhancePolicyUI p_structEnhancePolicyUI, XmlDocument p_objSysViewSection, XmlDocument p_objSysEx)
        {
            PremiumAmounts structPremiumAmounts = new PremiumAmounts();
            //Anu Tennyson for VACo Prem. Calculation MITS 18229
            string sLob = string.Empty;
            sLob = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
            //Anu Tennyson for VACo Prem. Calculation MITS 18229
            
            // Load the Rate record. 
            this.LoadRateRecord(p_iTransNumber, bCalcFlag, ref structPremiumAmounts);

            // Add the Premium Calculation Screen XML as a new Group.
            XmlElement objElem = p_objSysViewSection.SelectSingleNode(@"//section[@name='premcalc']") as XmlElement;
            if (objElem == null)
            {
                objElem = p_objSysViewSection.CreateElement("section");
                objElem.SetAttribute("name", "premcalc");
                p_objSysViewSection.DocumentElement.AppendChild(objElem);
            }
            objElem.InnerXml = this.BuildPremCalcualtionScreenXML(p_iTransNumber, sTransactionType, bBuildFlag, bEditFlag, p_structEnhancePolicyUI, ref structPremiumAmounts);

            // Re-Calculate the Premium . 
            if (bCalcFlag)
            {
                // Start Naresh MITS 9270 Discount Refresh Problem
                if (p_iTransNumber == 0)
                {
                    this.LoadDiscountObject(ref structPremiumAmounts);
                }
                // End Naresh MITS 9270 Discount Refresh Problem
                
                //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                  structPremiumAmounts.TotalModifier = structPremiumAmounts.ModifiedPremium;
                //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                  this.CalculatePremium(p_iTransNumber, p_structEnhancePolicyUI.PremiumCalculationUI, ref structPremiumAmounts, p_structEnhancePolicyUI);
            }

            this.EmitsXmlForPremiumAmounts( p_objSysEx.DocumentElement , structPremiumAmounts );
            
        }
        // Start Naresh MITS 9270 Discount Refresh Problem
        private void LoadDiscountObject(ref PremiumAmounts p_structPremiumAmounts)
        {
            foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
            {
                for (int iIndex = 0; iIndex < p_structPremiumAmounts.DiscountList.Count; iIndex++)
                {
                    Discount structDiscount = (Discount)p_structPremiumAmounts.DiscountList[iIndex];
                    if (objPolicyXDcntEnh.DiscountNameId == structDiscount.NameId)
                    {
                        structDiscount.AmountFactor = objPolicyXDcntEnh.DiscountFactor;
                        p_structPremiumAmounts.DiscountList[iIndex] = structDiscount;
                        break;
                    }
                }
            }
        }
        // End Naresh MITS 9270 Discount Refresh Problem
        private void EmitsXmlForPremiumAmounts( XmlElement p_objSysEx , PremiumAmounts p_structPremiumAmounts)
        {
            XmlElement objPremiumAmountsElement = null ;
            XmlElement objDiscountsElement = null;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
            XmlElement objModifiersElement = null;
            XmlElement objModifierElement = null;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
            XmlElement objDiscountElement = null;
            XmlElement objTiersElement = null;
            XmlElement objTierElement = null;
            string sUniqueId = string.Empty;
            Discount structDiscount;
            DiscountTier structDiscountTier;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS 
            string sLob = string.Empty;
            sLob = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
            int iFlatOrPercent = 0;
            string sFlatOrPercent = string.Empty;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS

            objPremiumAmountsElement = p_objSysEx.SelectSingleNode("//" + CALC_PREM_ROOT) as XmlElement;
            if (objPremiumAmountsElement != null)
                objPremiumAmountsElement.InnerXml = "";
            else
                base.CreateElement(p_objSysEx, CALC_PREM_ROOT, ref objPremiumAmountsElement);

            
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_MANUAL_PREMIUM_ID, p_structPremiumAmounts.ManualPremium.ToString() );
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_EXP_MOD_FACTOR_ID, p_structPremiumAmounts.ExpModFactor.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_EXP_MOD_FACTOR_AMOUNT_ID, p_structPremiumAmounts.ExpModFactorAmount.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_MODIFIED_PREMIUM_ID, p_structPremiumAmounts.ModifiedPremium.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_FINAL_PREMIUM_ID, p_structPremiumAmounts.FinalPremium.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_AUDITED_PREMIUM_ID, p_structPremiumAmounts.AuditedPremium.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_ESTIMATED_PREMIUM_ID, p_structPremiumAmounts.EstimatedPremium.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_TRANSACTION_PREMIUM_ID, p_structPremiumAmounts.TransactionalPremium.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_TOTAL_BILLED_PREMIUM_ID, p_structPremiumAmounts.TotalBilledPremium.ToString());

            //rsushilaggar MITS 21558
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_MANUAL_FINAL_PREMIUM_ID, p_structPremiumAmounts.ManualFinalPremium.ToString());

            //Updated 02/21/07
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_TOTAL_BILLED_PREMIUM_ID + "1", p_structPremiumAmounts.TotalBilledPremium.ToString());
            //Divya : Chg in billed Premium :
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_CHG_BILLED_PREMIUM_ID, p_structPremiumAmounts.ChgBilledPremium.ToString());
            //pmahli MITS 9275 Added Expense Constant
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_EXPENSE_CONST_ID, p_structPremiumAmounts.ExpenseConstant.ToString());
            //npadhy RMSC retrofit Starts
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_TAX_RATE_ID, p_structPremiumAmounts.TaxRate.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_TAX_ID, p_structPremiumAmounts.Tax.ToString());
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_HID_TAX_TYPE_ID, Conversion.ConvertObjToStr(p_structPremiumAmounts.TaxFlatOrPercent));
            base.CreateAndSetElement(objPremiumAmountsElement, CALC_PREM_TRANS_TAX_ID, p_structPremiumAmounts.TransactionTax.ToString());
            //npadhy RMSC retrofit Ends
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
            if (sLob != "WC")
            {
                base.CreateElement(objPremiumAmountsElement, CALC_PREM_MODIFIERS, ref objModifiersElement);
                foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                {
                    if (objPolicyXCvgEnh.PolcvgRowId < 0)
                    {
                        sUniqueId = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.CoverageSeq.ToString();
                    }
                    else
                    {
                        sUniqueId = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.PolcvgRowId.ToString();
                    }
                    // If the Deductible and Modifier are zero, we still want to show the Coverage in Prem Calc Screen
                    // because it might be associated with ranges. User might wish to change the existing modifier, and select a range

                    //if (objPolicyXCvgEnh.Deductible == 0.0 && objPolicyXCvgEnh.Modifier == 0.0)
                    //{
                    //    continue;

                    //}
                    iFlatOrPercent = objPolicyXCvgEnh.FlatOrPercent;
                    sFlatOrPercent = base.LocalCache.GetShortCode(iFlatOrPercent);
                    string sModifier = string.Empty;
                    if (sFlatOrPercent == "P")
                    {
                        sModifier = objPolicyXCvgEnh.Modifier.ToString() + "%";
                    }
                    else
                    {
                        sModifier = objPolicyXCvgEnh.Modifier.ToString();
                    }
                    base.CreateElement(objModifiersElement, CALC_PREM_MODIFIER, ref objModifierElement);
                    base.CreateAndSetElement(objModifierElement, CALC_PREM_MODIFIER_AMOUNT_FACTORID + sUniqueId, sModifier);
                    base.CreateAndSetElement(objModifierElement, CALC_PREM_MODIFIER_AMOUNT + sUniqueId, objPolicyXCvgEnh.Amount.ToString());
                }
            }
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
            
            base.CreateElement(objPremiumAmountsElement, CALC_PREM_DISCOUNTS, ref objDiscountsElement);
            for( int iIndex = 0 ; iIndex < p_structPremiumAmounts.DiscountList.Count ; iIndex++ )
            {
                structDiscount = (Discount)p_structPremiumAmounts.DiscountList[iIndex];
                base.CreateElement(objDiscountsElement, CALC_PREM_DISCOUNT , ref objDiscountElement);
                base.CreateAndSetElement(objDiscountElement, CALC_PREM_DISCOUNT_AMOUNT, structDiscount.Amount.ToString());
                base.CreateAndSetElement(objDiscountElement, CALC_PREM_DISCOUNT_AMOUNT_FACTOR, structDiscount.AmountFactor.ToString());                    
                base.CreateAndSetElement(objDiscountElement, CALC_PREM_DISCOUNT_LEVEL, structDiscount.Level.ToString());
                base.CreateAndSetElement(objDiscountElement, CALC_PREM_DISCOUNT_NAME, structDiscount.Name);
                base.CreateAndSetElement(objDiscountElement, CALC_PREM_DISCOUNT_NAME_ID, structDiscount.NameId.ToString());
                base.CreateAndSetElement(objDiscountElement, CALC_PREM_DISCOUNT_TYPE, structDiscount.Type);
                base.CreateAndSetElement(objDiscountElement, CALC_PREM_DISCOUNT_ID, structDiscount.Id.ToString());
            }

            base.CreateElement(objPremiumAmountsElement, CALC_PREM_TIERS, ref objTiersElement);
            for (int iIndex = 0; iIndex < p_structPremiumAmounts.DiscountTierList.Count; iIndex++)
            {
                structDiscountTier = (DiscountTier)p_structPremiumAmounts.DiscountTierList[iIndex];
                base.CreateElement(objTiersElement, CALC_PREM_TIER, ref objTierElement);
                base.CreateAndSetElement(objTierElement, CALC_PREM_TIER_AMOUNTS, structDiscountTier.Amount.ToString());
                base.CreateAndSetElement(objTierElement, CALC_PREM_TIER_LEVEL, structDiscountTier.LevelId.ToString());
                base.CreateAndSetElement(objTierElement, CALC_PREM_TIER_NAME, structDiscountTier.Name);
                base.CreateAndSetElement(objTierElement, CALC_PREM_TIER_NAME_ID, structDiscountTier.NameId.ToString());
                base.CreateAndSetElement(objTierElement, CALC_PREM_TIER_ID, structDiscountTier.Id.ToString());                
            }


        }

        public void SetPremCalculateScreenFlag(ref bool p_bBuildFlag, ref bool p_bCalcFlag, ref bool p_bEditFlag, ref int p_iTransNumber, ref string p_sTransactionType, string p_sPostBackAction)
        {
            //pmahli string p_sPostBackAction added in the function for MITS 9793 where to set bcalc flag we need to knowledge of PostBackAction
            bool bFound = false;
            p_bBuildFlag = false;
            p_bCalcFlag = false;
            //pmahli MITS 9793 -Start 
            //Max transaction id is found out so that while setting calc flag in Audit we can select the latest transaction
            int iMaxTransID = 0;
            foreach (PolicyXRtngEnh objPolicyXRtngenh in PolicyEnh.PolicyXRatingEnhList)
                if (objPolicyXRtngenh.TransactionId > iMaxTransID)
                    iMaxTransID = objPolicyXRtngenh.TransactionId;
            //pmahli MITS 9793 -End 

            if (PolicyEnh.PolicyXTransEnhList.Count == 0 && PolicyEnh.PolicyXTermEnhList.Count == 0)
            {
                p_bBuildFlag = true;
                p_bCalcFlag = true;
                p_iTransNumber = 0;
            }
            else
            {
                // Loop through to find if we have the transaction number in our list.
                foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                    {
                        if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus) == "PR")
                        {
                            p_bCalcFlag = true;
                            if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "NB" || base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "RN")
                            {
                                foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
                                {
                                    if (objPolicyXDcntEnh.TransactionId == p_iTransNumber)
                                    {
                                        p_bBuildFlag = false;
                                        goto SetEditFlag;
                                    }
                                }
                                foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
                                {
                                    if (objPolicyXDtierEnh.TransactionId == p_iTransNumber)
                                    {
                                        p_bBuildFlag = false;
                                        goto SetEditFlag;
                                    }
                                }
                                p_bBuildFlag = true;
                            }
                            else
                            {
                                p_bBuildFlag = false;
                            }
                        }
                        else
                        {
                            p_bBuildFlag = false;
                            p_bCalcFlag = false;
                        }
                        bFound = true;
                        break;
                    }
                }

                if (!bFound)
                {
                    p_iTransNumber = 0;
                    foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId > p_iTransNumber)//pmahli MITS 9715. The Audit Transaction row was not getting selected as the we do an Audit
                        {
                            p_iTransNumber = objPolicyXTransEnh.TransactionId;
                            if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus) == "PR")
                            {
                                p_bCalcFlag = true;
                                if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "NB" || base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "RN")
                                {
                                    foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
                                    {
                                        if (objPolicyXDcntEnh.TransactionId == p_iTransNumber)
                                        {
                                            p_bBuildFlag = false;
                                            goto SetEditFlag;
                                        }
                                    }
                                    foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
                                    {
                                        if (objPolicyXDtierEnh.TransactionId == p_iTransNumber)
                                        {
                                            p_bBuildFlag = false;
                                            goto SetEditFlag;
                                        }
                                    }
                                    p_bBuildFlag = true;
                                }
                                else
                                {
                                    p_bBuildFlag = false;
                                }
                            }
                            else
                            {
                                p_bBuildFlag = false;
                                p_bCalcFlag = false;
                            }
                            bFound = true;
                        }
                    }

                    if (!bFound)
                    {
                        foreach (PolicyXTermEnh objPolicyXTermEnh in PolicyEnh.PolicyXTermEnhList)
                        {
                            if (objPolicyXTermEnh.TransactionId > p_iTransNumber)
                            {
                                p_iTransNumber = objPolicyXTermEnh.TransactionId;
                            }
                        }

                        if (base.LocalCache.GetShortCode(PolicyEnh.PolicyStatusCode) == "Q")
                        {
                            p_bCalcFlag = true;
                            foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
                            {
                                if (objPolicyXDcntEnh.TransactionId == p_iTransNumber && p_iTransNumber != 0 )
                                {
                                    p_bBuildFlag = false;
                                    goto SetEditFlag;
                                }
                            }
                            foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
                            {
                                if (objPolicyXDtierEnh.TransactionId == p_iTransNumber && p_iTransNumber != 0)
                                {
                                    p_bBuildFlag = false;
                                    goto SetEditFlag;
                                }
                            }
                            p_bBuildFlag = true;
                        }
                        else
                        {
                            p_bBuildFlag = false;
                            p_bCalcFlag = false;
                        }
                    }
                }
            }

    SetEditFlag :

            // brand new quote or txn
            if (p_bBuildFlag)
            {
                p_bCalcFlag = true;
                p_bEditFlag = true;
            }
            else
            {
                if (PolicyEnh.PolicyXTransEnhList.Count == 0)
                {
                    // Quote
                    if (base.LocalCache.GetShortCode(PolicyEnh.PolicyStatusCode) == "Q")
                    {
                        p_bEditFlag = true;
                        p_bCalcFlag = true;
                    }
                    // mkaran2- MITS :29433 - Start   
                    else if ((base.LocalCache.GetShortCode(PolicyEnh.PolicyIndicator) == "Q") && (base.LocalCache.GetShortCode(PolicyEnh.PolicyStatusCode) == "CV"))
                    {
                        p_bCalcFlag = true;
                    }   // mkaran2- MITS :29433 - End
                    else
                    {
                        // Converted Quote
                        p_bEditFlag = false;
                        p_bCalcFlag = false;
                    }
                }
                else
                {
                    // Policy
                    foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                        {
                            // Start Naresh Problem Identified while Solving MITS 9270
                            p_sTransactionType = base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                            // End Naresh Problem Identified while Solving MITS 9270
                            if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus) == "PR")
                            {
                                if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "RN" || base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "AU")
                                {
                                    p_bEditFlag = true;
                                    p_bCalcFlag = true;
                                }
                                else
                                {
                                    p_bEditFlag = false;
                                    p_bCalcFlag = true;
                                }
                            }
                            //pmahli MITS 9793 -start
                            //bcalc flag set to true in case of Audit when there is transaction change and maximum transaction id is selected
                            //As in this case we need bcalc flag to be true so that calculateModifiedPremium can be called from UpdateDiscounts function 
                            else if (p_sPostBackAction == EnhancePolicyManager.ACTION_TRANSACTION_CHANGE && base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "AU" && p_iTransNumber == iMaxTransID)
                            {
                                p_bCalcFlag = true;
                            }
                            //pmahli MITS 9793 - End
                            // mkaran2- MITS :29433 - Start               
                            else if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus) == "OK")
                            {
                                if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "NB")
                                {
                                    p_bCalcFlag = true;
                                }
                            }
                            // mkaran2- MITS :29433 - End  
                            else
                            {
                                p_bEditFlag = false;
                                p_bCalcFlag = false;
                            }
                        }
                    }
                }
            }
        }

        private void LoadRateRecord(int p_iTransNumber, bool p_bCalculateFlag, ref PremiumAmounts p_structPremiumAmounts)
        {
            string sTranStatus = "";
            double dChgdBillPremAmt=0.0;
            string sTransType = string.Empty;//pmahli MITS 9793
            foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
            {
                if (objPolicyXRtngEnh.TransactionId == p_iTransNumber)
                {
                    //Mukul(07/04/2007) Added MITS 9793 Change in billed premium corrected
                    foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                    {
                        if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                        {
                            sTranStatus = base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                            sTransType = base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                            dChgdBillPremAmt=objPolicyXTransEnh.ChgBilledPremium;
                            break;
                        }
                    }
                    p_structPremiumAmounts.ManualPremium = objPolicyXRtngEnh.ManualPremium;
                    p_structPremiumAmounts.ExpModFactor = objPolicyXRtngEnh.ExpModFactor;
                    p_structPremiumAmounts.ExpModFactorAmount = objPolicyXRtngEnh.ExpModAmount;
                    p_structPremiumAmounts.ModifiedPremium = objPolicyXRtngEnh.ModifiedPremium;
                    //pmahli MITS 9793 
                    //When trans type is Audit Final Premium may not be equal to est premium
                    if (sTransType == "AU")
                        p_structPremiumAmounts.FinalPremium = objPolicyXRtngEnh.AuditedPremium; 
                    else
                        p_structPremiumAmounts.FinalPremium = objPolicyXRtngEnh.TotalEstPremium;
                    p_structPremiumAmounts.AuditedPremium = objPolicyXRtngEnh.AuditedPremium;
                    p_structPremiumAmounts.EstimatedPremium = objPolicyXRtngEnh.TotalEstPremium;
                    p_structPremiumAmounts.TransactionalPremium = objPolicyXRtngEnh.TransactionPrem;
                    p_structPremiumAmounts.TotalBilledPremium = objPolicyXRtngEnh.TotalBilledPrem;
                    //npadhy RMSC retrofit Starts
                    p_structPremiumAmounts.Tax = objPolicyXRtngEnh.TaxAmount;
                    p_structPremiumAmounts.TaxRate = objPolicyXRtngEnh.TaxRate;
                    p_structPremiumAmounts.TransactionTax = objPolicyXRtngEnh.TranTax;
                    p_structPremiumAmounts.TaxFlatOrPercent = objPolicyXRtngEnh.TaxType;   
                    //npadhy RMSC retrofit Ends
                    //pmahli MITS 9275 4/24/2007 Expense Constant added to Struct
                    p_structPremiumAmounts.ExpenseConstant = objPolicyXRtngEnh.ExpenseConstant;
                    if (sTranStatus == "OK")
                        p_structPremiumAmounts.ChgBilledPremium = dChgdBillPremAmt;
                    else
                    {
                        if (!p_bCalculateFlag)
                            p_structPremiumAmounts.ChgBilledPremium = objPolicyXRtngEnh.TotalBilledPrem;
                    }
                    break;
                }
            }
        }

        private string BuildPremCalcualtionScreenXML(int p_iTransNumber, string p_sTransactionType, bool p_bBuildFlag, bool p_bEditFlag, EnhancePolicyUI p_structEnhancePolicyUI, ref PremiumAmounts p_structPremiumAmounts)
        {
            XmlDocument objPremiumCalculation = null;
            XmlElement objLastControl = null;
            XmlElement objGroup = null;

            DbReader objReader = null;
            DbReader objReaderDis = null;
            string sSQL = string.Empty;
            int iPrevTierLevel = 1;
            bool bTierFound = true;
            bool bFirstChildAppend = true;
            bool bFinalPremiumVisible = false;
            int iUniqueDiscountId = 0;
            int iUniqueDiscountTierId = 0;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229
            string sLob = string.Empty;
            string sUniqueId = string.Empty;
            bool bIsRangeAvailable = false;
            bool bIsModifierEditable = false;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229
            objPremiumCalculation = new XmlDocument();
            p_structPremiumAmounts.DiscountList = new ArrayList();
            p_structPremiumAmounts.DiscountTierList = new ArrayList();
            //Anu Tennyson for VACo Prem. Calculation MITS 18229
            sLob = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
            //Anu Tennyson for VACo Prem. Calculation MITS 18229

            // Create Group.
            this.CreateGroup(objPremiumCalculation, ref objGroup, "premcalc", "Prem. Calc.");

            // Unsaved brand new quote or txn
            if (p_bBuildFlag)
            {
                if ( PolicyEnh.State == 0 || PolicyEnh.PolicyType == 0 || p_structEnhancePolicyUI.PolicyUIData.EffectiveDate == "" || p_structEnhancePolicyUI.PolicyUIData.ExpirationDate == "" || !this.IsWithingTermDates(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate, p_structEnhancePolicyUI.PolicyUIData.ExpirationDate))
                {
                    if (sLob != "WC")
                    {
                       this.CreateDeductibleXml(objGroup, ref p_structPremiumAmounts, p_iTransNumber, p_bEditFlag, p_structEnhancePolicyUI);
                       this.ArrangeDeductibleXml(objPremiumCalculation, objLastControl); 
                    }
                    this.CreateFinalPremiumXML(objGroup, p_structEnhancePolicyUI, false );
                    
                    return objPremiumCalculation.OuterXml;
                    
                }

                this.CreateManualPremiumXML(objGroup, p_structEnhancePolicyUI, ref objLastControl, p_bBuildFlag, ref  p_structPremiumAmounts, p_iTransNumber, sLob, p_bEditFlag, ref  objPremiumCalculation, ref bFinalPremiumVisible);
                # region Start function
                //do
                //{
                //    //Anu Tennyson STARTS for Creating Prem Calc screen for thr Coverages for MITS 18229.
                //    #region Create Coverages
                //    if (sLob != "WC")
                //    {
                //        this.CreateDeductibleBeforeSaveXml(objGroup, ref p_structPremiumAmounts, ref objLastControl, p_iTransNumber, p_bEditFlag, p_structEnhancePolicyUI, iPrevTierLevel);
                //    }
                
                //    #endregion 

                //    //Anu Tennyson ENDS for MITS 18229.
                //    #region Create Discount

                //    sSQL = " SELECT * FROM SYS_POL_SEL_DISC WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS="
                //        + PolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + PolicyEnh.State + ")"
                //        +   " AND PARENT_LEVEL = " + iPrevTierLevel + " AND NOT IN_USE_FLAG = 0 AND IS_RATE_SETUP = 0" ;

                //    objReaderDis = DbFactory.GetDbReader( base.ConnectionString , sSQL );
                //    while( objReaderDis.Read() )
                //    {
                //        bFinalPremiumVisible = true;
                //        bool bDiscountEditable = false;
                        
                //        Discount structDiscount = new Discount();
                //        structDiscount.NameId = objReaderDis.GetInt("DISCOUNT_ROWID");                        

                //        // Get Range Enable/Disable Flags.
                //        bool bRangeEnabled = false ;
                //        bool bRangeVisible = false ;
                //        sSQL = " SELECT COUNT(*) FROM SYS_POL_DISC_RANGE WHERE DISCOUNT_ROWID = " + structDiscount.NameId ;
                //        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //        if (objReader.Read() && objReader.GetInt(0) != 0 )
                //        {
                //            bRangeEnabled = true ;
                //            bRangeVisible = true ;
                //        }
                //        objReader.Close();
                //        //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
                //        sSQL = " SELECT * FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = " 
                //            + structDiscount.NameId + " AND EFFECTIVE_DATE <= '" 
                //            + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate
                //            + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '" 
                //            + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate + "'))";

                //        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //        if (objReader.Read())
                //        {
                //            structDiscount.Amount = 0 ;
                //            structDiscount.AmountFactor = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"));
                //            structDiscount.Level = objReaderDis.GetInt("PARENT_LEVEL");
                //            structDiscount.Id = iUniqueDiscountId++;

                //            structDiscount.Name = objReader.GetString("DISCOUNT_NAME");
                //            structDiscount.Type = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));

                //            // Get the Discount Override flag. 
                //            bDiscountEditable = objReaderDis.GetBoolean("OVERRIDE_FLAG");
                //            bDiscountEditable = bDiscountEditable && !bRangeEnabled;                            

                //            p_structPremiumAmounts.DiscountList.Add(structDiscount);
                //        this.CreateDiscountControl(objGroup, structDiscount, bDiscountEditable, bRangeVisible);
                //    }
                //        objReader.Close();                                                
                //    }
                //    objReaderDis.Close();
                //    #endregion 

                //    #region Create Discount Tier
                //    //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
                //    sSQL = " SELECT * FROM SYS_POL_DISC_TIER WHERE ((LINE_OF_BUSINESS=0) OR (LINE_OF_BUSINESS="
                //        + PolicyEnh.PolicyType + ")) AND ((STATE=0) OR (STATE="
                //        + PolicyEnh.State + "))"
                //        + " AND PARENT_LEVEL = " + iPrevTierLevel + " AND EFFECTIVE_DATE <= '"
                //        + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate
                //        + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '"
                //        + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate + "')) AND NOT IN_USE_FLAG = 0";
                    
                //    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //    if (objReader.Read())
                //    {
                //        bFinalPremiumVisible = true;

                //        DiscountTier structDiscountTier = new DiscountTier();
                //        structDiscountTier.NameId = objReader.GetInt("DISC_TIER_ROWID");
                //        structDiscountTier.Name = objReader.GetString("TIER_NAME");
                //        structDiscountTier.LevelId = iPrevTierLevel;
                //        structDiscountTier.Amount = 0;
                //        structDiscountTier.Id = iUniqueDiscountTierId++; 
                //        p_structPremiumAmounts.DiscountTierList.Add(structDiscountTier);

                //        iPrevTierLevel = structDiscountTier.NameId;
                //        this.CreateDiscountTierControl(objGroup, structDiscountTier);
                //        bTierFound = true;
                //    }
                //    else
                //    {
                //        bTierFound = false;
                //    }
                //    objReader.Close();
                //    #endregion 

                //}
                //while(bTierFound);
                #endregion
                CreateDiscountTier(p_iTransNumber, objGroup, sLob, p_bEditFlag, p_structEnhancePolicyUI, ref  p_structPremiumAmounts, ref objLastControl, 1, ref bFinalPremiumVisible);
                this.CreateFinalPremiumXML(objGroup, p_structEnhancePolicyUI , bFinalPremiumVisible );
            }
            else
            {
                // For saved record.

                //this.CreateManualPremiumXML(objGroup, p_structEnhancePolicyUI ,ref objLastControl);
                this.CreateManualPremiumXML(objGroup, p_structEnhancePolicyUI, ref objLastControl, p_bBuildFlag, ref  p_structPremiumAmounts, p_iTransNumber, sLob, p_bEditFlag, ref  objPremiumCalculation, ref bFinalPremiumVisible);
                //Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS.

                #region Create Coverage
                //// Create Coverages
                //if (sLob != "WC")
                //{
                //    this.CreateDeductibleXml(objGroup, ref p_structPremiumAmounts,  p_iTransNumber, p_bEditFlag, p_structEnhancePolicyUI);

                //}
                //#endregion
                ////Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS.

                //#region Create Discount
                //// Create Discount 
                //foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
                //{
                //    if (objPolicyXDcntEnh.TransactionId == p_iTransNumber)
                //    {
                //        Discount structDiscount = new Discount();
                //        structDiscount.NameId = objPolicyXDcntEnh.DiscountNameId;
                //        structDiscount.Amount = objPolicyXDcntEnh.DiscountAmt;
                //        structDiscount.AmountFactor = objPolicyXDcntEnh.DiscountFactor;
                //        structDiscount.Level = objPolicyXDcntEnh.DiscountLevelId;
                //        structDiscount.Id = objPolicyXDcntEnh.DiscountId;

                //        sSQL = "SELECT DISCOUNT_NAME , FLAT_OR_PERCENT FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID =" + objPolicyXDcntEnh.DiscountNameId;
                //        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //        if (objReader.Read())
                //        {
                //            structDiscount.Name = objReader.GetString("DISCOUNT_NAME");
                //            structDiscount.Type = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));
                //        }
                //        objReader.Close();
                //        p_structPremiumAmounts.DiscountList.Add(structDiscount);

                //        // Get Range Enable/Disable Flags.
                //        bool bRangeEnabled = false ;
                //        bool bRangeVisible = false ;
                //        sSQL = " SELECT COUNT(*) FROM SYS_POL_DISC_RANGE WHERE DISCOUNT_ROWID = " + objPolicyXDcntEnh.DiscountNameId ;
                //        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //        if (objReader.Read() && objReader.GetInt(0) != 0 && p_bEditFlag )
                //        {
                //            bRangeEnabled = true ;
                //            bRangeVisible = true ;
                //        }
                //        objReader.Close(); 

                //        // Get the Discount Override flag. 
                //        bool bDiscountEditable = false;
                //        sSQL = "SELECT OVERRIDE_FLAG FROM SYS_POL_SEL_DISC WHERE DISCOUNT_ROWID = " + objPolicyXDcntEnh.DiscountNameId + " AND IS_RATE_SETUP = 0";
                //        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //        if (objReader.Read())
                //        {
                //            bDiscountEditable = objReader.GetBoolean("OVERRIDE_FLAG");
                //            bDiscountEditable = bDiscountEditable && p_bEditFlag && !bRangeEnabled;
                //        }
                //        objReader.Close();

                //        this.CreateDiscountControl(objGroup, structDiscount, bDiscountEditable, bRangeVisible);                                 
                                                
                //    }
                //}
                //#endregion

                //#region Create Discount Tier
                //// Create Discount Tiers
                //foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
                //{
                //    if (objPolicyXDtierEnh.TransactionId == p_iTransNumber)
                //    {
                //        sSQL = "SELECT TIER_NAME FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + objPolicyXDtierEnh.DiscTierNameId;
                //        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //        if (objReader.Read())
                //        {
                //            DiscountTier structDiscountTier = new DiscountTier();
                //            structDiscountTier.NameId = objPolicyXDtierEnh.DiscTierNameId;
                //            structDiscountTier.Name = objReader.GetString("TIER_NAME");
                //            structDiscountTier.LevelId = objPolicyXDtierEnh.DiscTierLevelId;
                //            structDiscountTier.Amount = objPolicyXDtierEnh.DiscTierAmt;
                //            structDiscountTier.Id = objPolicyXDtierEnh.DiscountTierId;

                //            p_structPremiumAmounts.DiscountTierList.Add(structDiscountTier);

                //            this.CreateDiscountTierControl(objGroup, structDiscountTier);
                            
                //        }
                //        objReader.Close();
                //    }
                //}
                //#endregion

                //#region Shuffles Discounts AND Tiers

                //// Shuffle the Discount and Discount-Tiers as per the Levels.                 
                //while (bTierFound)
                //{
                //    bTierFound = false;
                //    //Anu Tennyson for MITS 18229 Added to rearrange the Coverages after Save. STRATS
                //    foreach (XmlNode objControl in objPremiumCalculation.SelectNodes("//displaycolumn[@" + CALC_PREM_TYPE + "='" + CALC_PREM_TYPE_MODIFIER + "']"))
                //    {
                //        if (objControl.Attributes[CALC_PREM_DEDUCITBLE_LEVEL].Value == iPrevTierLevel.ToString())
                //        {
                //            if (bFirstChildAppend)
                //            {
                //                objLastControl.PrependChild(objControl);
                //                bFirstChildAppend = false;
                //            }
                //            else
                //            {
                //                objLastControl.ParentNode.InsertAfter(objControl, objLastControl);
                //            }
                //            objLastControl = (XmlElement)objControl;
                //        }
                //    }

                //    //Anu Tennyson for MITS 18229 Added to rearrange the Coverages after Save. ENDS
                //    foreach (XmlNode objControl in objPremiumCalculation.SelectNodes("//displaycolumn[@" + CALC_PREM_TYPE + "='" + CALC_PREM_TYPE_DISCOUNT + "']"))
                //    {
                //        if (objControl.Attributes[CALC_PREM_DISCOUNT_LEVEL].Value == iPrevTierLevel.ToString())
                //        {
                //            if (bFirstChildAppend)
                //            {
                //                objLastControl.PrependChild(objControl);
                //                bFirstChildAppend = false;
                //            }
                //            else
                //            {
                //                objLastControl.ParentNode.InsertAfter(objControl, objLastControl);
                //            }
                //            objLastControl = (XmlElement)objControl;
                //        }
                //    }

                //    foreach (XmlNode objControl in objPremiumCalculation.SelectNodes("//displaycolumn[@" + CALC_PREM_TYPE + "='" + CALC_PREM_TYPE_TIER + "']"))
                //    {
                //        if (objControl.Attributes[CALC_PREM_TIER_LEVEL].Value == iPrevTierLevel.ToString())
                //        {
                //            if (bFirstChildAppend)
                //            {
                //                objLastControl.PrependChild(objControl);
                //                bFirstChildAppend = false;
                //            }
                //            else
                //            {
                //                objLastControl.ParentNode.InsertAfter(objControl, objLastControl);
                //            }
                //            iPrevTierLevel = Conversion.ConvertStrToInteger(objControl.Attributes[CALC_PREM_TIER_NAME].Value);
                //            objLastControl = (XmlElement)objControl;
                //            bTierFound = true;
                //            break;
                //        }
                //    }
                //}
                #endregion
                //CreateDiscountTier(p_iTransNumber, objGroup, sLob, p_bEditFlag, p_structEnhancePolicyUI, ref  p_structPremiumAmounts, ref objLastControl, 1, ref bFinalPremiumVisible);
                CreateDiscountTierEdit(p_iTransNumber, objGroup, sLob, p_bEditFlag, p_structEnhancePolicyUI, ref  p_structPremiumAmounts, ref  objLastControl, 1, ref  objPremiumCalculation);

                this.CreateFinalPremiumXML(objGroup, p_structEnhancePolicyUI);

            }
            return (objPremiumCalculation.OuterXml);
        }

        private void CreateDiscountTier(int p_iTransNumber, XmlElement objGroup, string sLob, bool p_bEditFlag, EnhancePolicyUI p_structEnhancePolicyUI, ref PremiumAmounts p_structPremiumAmounts, ref XmlElement objLastControl, int p_iPrevTierLevel, ref bool bFinalPremiumVisible)
        {
            DbReader objReaderDis = null;
            DbReader objReader = null;
            string sSQL = string.Empty;
            //int iPrevTierLevel = 1;
            bool bTierFound = true;
            bool bFirstChildAppend = true;
            //bool bFinalPremiumVisible = false;
            int iUniqueDiscountId = 0;
            int iUniqueDiscountTierId = 0;
            //bool bTierFound = true;
            do
            {
                //Anu Tennyson STARTS for Creating Prem Calc screen for thr Coverages for MITS 18229.
                #region Create Coverages
                if (sLob != "WC")
                {
                    this.CreateDeductibleBeforeSaveXml(objGroup, ref p_structPremiumAmounts, ref objLastControl, p_iTransNumber, p_bEditFlag, p_structEnhancePolicyUI, p_iPrevTierLevel);
                }

                #endregion

                //Anu Tennyson ENDS for MITS 18229.
                #region Create Discount

                sSQL = " SELECT * FROM SYS_POL_SEL_DISC WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS="
                    + PolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + PolicyEnh.State + ")"
                    + " AND PARENT_LEVEL = " + p_iPrevTierLevel + " AND NOT IN_USE_FLAG = 0 AND IS_RATE_SETUP = 0";

                objReaderDis = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                while (objReaderDis.Read())
                {
                    bFinalPremiumVisible = true;
                    bool bDiscountEditable = false;

                    Discount structDiscount = new Discount();
                    structDiscount.NameId = objReaderDis.GetInt("DISCOUNT_ROWID");

                    // Get Range Enable/Disable Flags.
                    bool bRangeEnabled = false;
                    bool bRangeVisible = false;
                    sSQL = " SELECT COUNT(*) FROM SYS_POL_DISC_RANGE WHERE DISCOUNT_ROWID = " + structDiscount.NameId;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read() && objReader.GetInt(0) != 0)
                    {
                        bRangeEnabled = true;
                        bRangeVisible = true;
                    }
                    objReader.Close();
                    //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
                    sSQL = " SELECT * FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = "
                        + structDiscount.NameId + " AND EFFECTIVE_DATE <= '"
                        + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate
                        + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '"
                        + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate + "'))";

                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        structDiscount.Amount = 0;
                        structDiscount.AmountFactor = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), base.ClientId);
                        structDiscount.Level = objReaderDis.GetInt("PARENT_LEVEL");
                        structDiscount.Id = structDiscount.NameId;

                        structDiscount.Name = objReader.GetString("DISCOUNT_NAME");
                        structDiscount.Type = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));

                        // Get the Discount Override flag. 
                        bDiscountEditable = objReaderDis.GetBoolean("OVERRIDE_FLAG");
                        bDiscountEditable = bDiscountEditable && !bRangeEnabled;

                        p_structPremiumAmounts.DiscountList.Add(structDiscount);
                        this.CreateDiscountControl(objGroup, structDiscount, bDiscountEditable, bRangeVisible);
                    }
                    objReader.Close();
                }
                objReaderDis.Close();
                #endregion

                #region Create Discount Tier
                //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
                sSQL = " SELECT * FROM SYS_POL_DISC_TIER WHERE ((LINE_OF_BUSINESS=0) OR (LINE_OF_BUSINESS="
                    + PolicyEnh.PolicyType + ")) AND ((STATE=0) OR (STATE="
                    + PolicyEnh.State + "))"
                    + " AND PARENT_LEVEL = " + p_iPrevTierLevel + " AND EFFECTIVE_DATE <= '"
                    + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate
                    + "' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '"
                    + p_structEnhancePolicyUI.PolicyUIData.EffectiveDate + "')) AND NOT IN_USE_FLAG = 0";

                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    bFinalPremiumVisible = true;

                    DiscountTier structDiscountTier = new DiscountTier();
                    structDiscountTier.NameId = objReader.GetInt("DISC_TIER_ROWID");
                    structDiscountTier.Name = objReader.GetString("TIER_NAME");
                    structDiscountTier.LevelId = p_iPrevTierLevel;
                    structDiscountTier.Amount = 0;
                    structDiscountTier.Id = structDiscountTier.NameId ;
                    p_structPremiumAmounts.DiscountTierList.Add(structDiscountTier);

                    p_iPrevTierLevel = structDiscountTier.NameId;
                    this.CreateDiscountTierControl(objGroup, structDiscountTier);
                    bTierFound = true;
                }
                else
                {
                    bTierFound = false;
                }
                objReader.Close();
                #endregion

            }
            while (bTierFound);
        }


        private void CreateDiscountTierEdit(int p_iTransNumber, XmlElement objGroup, string sLob, bool p_bEditFlag, EnhancePolicyUI p_structEnhancePolicyUI, ref PremiumAmounts p_structPremiumAmounts, ref XmlElement objLastControl, int p_iPrevTierLevel, ref XmlDocument objPremiumCalculation)
        {
            DbReader objReaderDis = null;
            DbReader objReader = null;
            string sSQL = string.Empty;
            bool bTierFound = true;
            bool bFirstChildAppend = true;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS.
            #region Create Coverage
            // Create Coverages
            if (sLob != "WC")
            {
                this.CreateDeductibleXml(objGroup, ref p_structPremiumAmounts, p_iTransNumber, p_bEditFlag, p_structEnhancePolicyUI);

            }
            #endregion
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS.

            #region Create Discount
            // Create Discount 
            foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
            {
                if (objPolicyXDcntEnh.TransactionId == p_iTransNumber)
                {
                    Discount structDiscount = new Discount();
                    structDiscount.NameId = objPolicyXDcntEnh.DiscountNameId;
                    structDiscount.Amount = objPolicyXDcntEnh.DiscountAmt;
                    structDiscount.AmountFactor = objPolicyXDcntEnh.DiscountFactor;
                    structDiscount.Level = objPolicyXDcntEnh.DiscountLevelId;
                    structDiscount.Id = objPolicyXDcntEnh.DiscountId;

                    sSQL = "SELECT DISCOUNT_NAME , FLAT_OR_PERCENT FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID =" + objPolicyXDcntEnh.DiscountNameId;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        structDiscount.Name = objReader.GetString("DISCOUNT_NAME");
                        structDiscount.Type = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));
                    }
                    objReader.Close();
                    p_structPremiumAmounts.DiscountList.Add(structDiscount);

                    // Get Range Enable/Disable Flags.
                    bool bRangeEnabled = false;
                    bool bRangeVisible = false;
                    sSQL = " SELECT COUNT(*) FROM SYS_POL_DISC_RANGE WHERE DISCOUNT_ROWID = " + objPolicyXDcntEnh.DiscountNameId;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read() && objReader.GetInt(0) != 0 && p_bEditFlag)
                    {
                        bRangeEnabled = true;
                        bRangeVisible = true;
                    }
                    objReader.Close();

                    // Get the Discount Override flag. 
                    bool bDiscountEditable = false;
                    sSQL = "SELECT OVERRIDE_FLAG FROM SYS_POL_SEL_DISC WHERE DISCOUNT_ROWID = " + objPolicyXDcntEnh.DiscountNameId + " AND IS_RATE_SETUP = 0";
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        bDiscountEditable = objReader.GetBoolean("OVERRIDE_FLAG");
                        bDiscountEditable = bDiscountEditable && p_bEditFlag && !bRangeEnabled;
                    }
                    objReader.Close();

                    this.CreateDiscountControl(objGroup, structDiscount, bDiscountEditable, bRangeVisible);

                }
            }
            #endregion

            #region Create Discount Tier
            // Create Discount Tiers
            foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
            {
                if (objPolicyXDtierEnh.TransactionId == p_iTransNumber)
                {
                    sSQL = "SELECT TIER_NAME FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + objPolicyXDtierEnh.DiscTierNameId;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        DiscountTier structDiscountTier = new DiscountTier();
                        structDiscountTier.NameId = objPolicyXDtierEnh.DiscTierNameId;
                        structDiscountTier.Name = objReader.GetString("TIER_NAME");
                        structDiscountTier.LevelId = objPolicyXDtierEnh.DiscTierLevelId;
                        structDiscountTier.Amount = objPolicyXDtierEnh.DiscTierAmt;
                        structDiscountTier.Id = objPolicyXDtierEnh.DiscountTierId;

                        p_structPremiumAmounts.DiscountTierList.Add(structDiscountTier);

                        this.CreateDiscountTierControl(objGroup, structDiscountTier);

                    }
                    objReader.Close();
                }
            }
            #endregion

            #region Shuffles Discounts AND Tiers
            for (int index = 0; index < 2; index++)
            {
                p_iPrevTierLevel = index;
                if (index == 0)
                {
                    objLastControl = (XmlElement)objPremiumCalculation.SelectSingleNode("//displaycolumn/control[@name = '" + CALC_PREM_MANUAL_PREMIUM_ID + "']");
                }
                else
                {
                    objLastControl = (XmlElement)objPremiumCalculation.SelectSingleNode("//displaycolumn/control[@name = '" + CALC_PREM_MODIFIED_PREMIUM_ID + "']");
                }

                bTierFound = true;
                // Shuffle the Discount and Discount-Tiers as per the Levels.                 
                while (bTierFound)
                {
                    bTierFound = false;
                    //Anu Tennyson for MITS 18229 Added to rearrange the Coverages after Save. STRATS
                    foreach (XmlNode objControl in objPremiumCalculation.SelectNodes("//displaycolumn[@" + CALC_PREM_TYPE + "='" + CALC_PREM_TYPE_MODIFIER + "']"))
                    {
                        if (objControl.Attributes[CALC_PREM_DEDUCITBLE_LEVEL].Value == p_iPrevTierLevel.ToString())
                        {
                            if (bFirstChildAppend)
                            {
                                objLastControl.PrependChild(objControl);
                                bFirstChildAppend = false;
                            }
                            else
                            {
                                objLastControl.ParentNode.InsertAfter(objControl, objLastControl);
                            }
                            objLastControl = (XmlElement)objControl;
                        }
                    }

                    //Anu Tennyson for MITS 18229 Added to rearrange the Coverages after Save. ENDS
                    foreach (XmlNode objControl in objPremiumCalculation.SelectNodes("//displaycolumn[@" + CALC_PREM_TYPE + "='" + CALC_PREM_TYPE_DISCOUNT + "']"))
                    {
                        if (objControl.Attributes[CALC_PREM_DISCOUNT_LEVEL].Value == p_iPrevTierLevel.ToString())
                        {
                            if (bFirstChildAppend)
                            {
                                objLastControl.PrependChild(objControl);
                                bFirstChildAppend = false;
                            }
                            else
                            {
                                objLastControl.ParentNode.InsertAfter(objControl, objLastControl);
                            }
                            objLastControl = (XmlElement)objControl;
                        }
                    }

                    foreach (XmlNode objControl in objPremiumCalculation.SelectNodes("//displaycolumn[@" + CALC_PREM_TYPE + "='" + CALC_PREM_TYPE_TIER + "']"))
                    {
                        if (objControl.Attributes[CALC_PREM_TIER_LEVEL].Value == p_iPrevTierLevel.ToString())
                        {
                            if (bFirstChildAppend)
                            {
                                objLastControl.PrependChild(objControl);
                                bFirstChildAppend = false;
                            }
                            else
                            {
                                objLastControl.ParentNode.InsertAfter(objControl, objLastControl);
                            }
                            p_iPrevTierLevel = Conversion.ConvertStrToInteger(objControl.Attributes[CALC_PREM_TIER_NAME].Value);
                            objLastControl = (XmlElement)objControl;
                            bTierFound = true;
                            break;
                        }
                    }
                }
            }
            #endregion
        }


        //Anu Tennyson for MITS 18229 STARTS
        /// <summary>
        /// Function to rearrange the XML formed for the coverages in prem. calc screen
        /// </summary>
        /// <param name="objPremiumCalculation">The input XML which is to be rarranged</param>
        /// <param name="objLastControl">The lat control XML element.</param>
        private void ArrangeDeductibleXml(XmlDocument objPremiumCalculation, XmlElement objLastControl)
        {
            string sBasePath = string.Empty;
            int iAddedID = 1;
            int iCount = 0;
            bool bFound = false;
            bool bIsSuccess = false;
            sBasePath = "//displaycolumn[@" + CALC_PREM_DEDUCTIBLE_ADDED + "='";
            while (!bFound)
            {
                bFound = true;
                if (objPremiumCalculation.SelectNodes(sBasePath + iAddedID + "']") != null)
                {
                    foreach (XmlNode objControl in objPremiumCalculation.SelectNodes(sBasePath + iAddedID + "']"))
                    {
                        if (Conversion.CastToType<int>(objControl.Attributes[CALC_PREM_DEDUCTIBLE_ADDED].Value, out bIsSuccess) == iAddedID)
                        {
                            objLastControl.ParentNode.InsertAfter(objControl, objLastControl);
                            iAddedID++;
                            objLastControl = (XmlElement)objControl;
                            bFound = false;
                        }
                        else
                        {
                            bFound = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Function to Calculate the premium according to the Coverage added.
        /// </summary>
        /// <param name="p_structPremiumAmounts">Premium Amount Structure</param>
        /// <param name="p_iTransNumber">Trans Number</param>
        /// <param name="p_structEnhancePolicyUI">Enhance Policy UI</param>
        /// <param name="iParentLevel">Parent Level of the Coverage</param>
        /// <param name="dblLastTierAmount">Last Tier Amount</param>
        private void CalculateDeductiblePremium(ref PremiumAmounts p_structPremiumAmounts, int p_iTransNumber, EnhancePolicyUI p_structEnhancePolicyUI, int iParentLevel, double dblLastTierAmount, string p_sPrevTransEffDate)
        {

            string sFlatOrPercentage = string.Empty;
            string sFixedOrProRata = string.Empty;
            string sLob = string.Empty;
            // npadhy Start MITS 23088 Premium Calculation in case of Reinstate with and without lapse
            string sTransType = string.Empty;
            // npadhy End MITS 23088 Premium Calculation in case of Reinstate with and without lapse
            int iFlatOrPercentage = 0;
            int iFixedOrProRata = 0;
            bool bIsSuccess = false;
            double dblModifier = 0;
            double dblTotalModifier = 0;
            double dblCalcModifier = 0;
            double dblLastTotal = 0;
            double dblProRataFactor = 0;
            StringBuilder sbSql = null;
            DbReader objReader = null;
            int iLevel = 0;
            int iLength = 0;
            int iState = 0;
            int iCount = 0;
            int iUARCount = 0;
            bool bTierPresent = true;
            if (iParentLevel == 0)
            {
                dblLastTotal = p_structPremiumAmounts.ManualPremium;
            }
            else if (iParentLevel == 1)
            {
                dblLastTotal = p_structPremiumAmounts.ModifiedPremium;
            }
            else
            {
                dblLastTotal = dblLastTierAmount;
            }
            p_structPremiumAmounts.TotalModifier = dblLastTotal;
            //if (dblLastTierAmount != 0)
            //{
            //    dblLastTotal = dblLastTierAmount;
            //}
            //else
            //{
            //    dblLastTotal = p_structPremiumAmounts.ModifiedPremium;
            //}
            iLevel = iParentLevel;
            iUARCount = 1;
            sLob = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
            try
            {
                // npadhy Start MITS 23088 Premium Calculation in case of Reinstate with and without lapse
                foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                    {
                        sTransType = base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                        break;
                    }
                }
                // npadhy End MITS 23088 Premium Calculation in case of Reinstate with and without lapse
                while (iCount < PolicyEnh.PolicyXCvgEnhList.Count)
                {
                    if (!bTierPresent)
                    {
                        break;
                    }
                    
                    foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                    {
                        iCount = iCount + 1;
                        if (objPolicyXCvgEnh.TransactionId == p_iTransNumber || objPolicyXCvgEnh.TransactionId == 0)
                        {
                            // If the Deductible and Modifier are zero, we still want to show the Coverage in Prem Calc Screen
                            // because it might be associated with ranges. User might wish to change the existing modifier, and select a range

                            //if (objPolicyXCvgEnh.Deductible == 0.0 && objPolicyXCvgEnh.Modifier == 0.0)
                            //{
                            //    continue;

                            //}

                            if (PolicyEnh.PolicyXExposureEnhList.Count == 0)
                            {
                                break;
                            }

                            string sModifier = objPolicyXCvgEnh.Modifier.ToString();
                            dblModifier = Conversion.CastToType<double>(sModifier, out bIsSuccess);
                            int iCoveragesType = objPolicyXCvgEnh.CoverageTypeCode;
                            iState = PolicyEnh.State;
                            sbSql = new StringBuilder();
                            sbSql.Append("SELECT * FROM SYS_POL_EXP_RATES WHERE SYS_POL_EXP_RATES.LINE_OF_BUSINESS= ");
                            sbSql.Append(PolicyEnh.PolicyType);
                            sbSql.Append(" AND SYS_POL_EXP_RATES.STATE = ");
                            sbSql.Append(iState);
                            sbSql.Append(" AND SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                            sbSql.Append(iCoveragesType);
                            sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                            sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                            sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                            sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                            sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                            sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                            sbSql.Append("'))");
                            objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                            if (objReader.Read())
                            {
                                iFlatOrPercentage = objReader.GetInt("FLAT_OR_PERCENT");
                                sFlatOrPercentage = base.LocalCache.GetShortCode(iFlatOrPercentage);
                                iFixedOrProRata = objReader.GetInt("FIXED_OR_PRORATE");
                                sFixedOrProRata = base.LocalCache.GetShortCode(iFixedOrProRata);
                            }
                            objReader.Close();
                            if (sFixedOrProRata == "P")
                            {
                                TimeSpan tDiff = ((Conversion.ToDate(objPolicyXCvgEnh.ExpirationDate)).Subtract(Conversion.ToDate(objPolicyXCvgEnh.EffectiveDate)));
                                iLength = tDiff.Days + 1;
                                if (iLength == 366)
                                {
                                    iLength = 365;
                                }
                                else if (CompareEffDate(Conversion.ToDate(objPolicyXCvgEnh.EffectiveDate))  &&  CompareExpDate(Conversion.ToDate(objPolicyXCvgEnh.ExpirationDate)) ) // checks if 29th Feb lies between the effective and expiration Date
                                iLength = iLength -1;
                                // npadhy Start MITS 23088 Premium Calculation in case of Reinstate with and without lapse
                                switch (sTransType)
                                {
                                    case "RL":
                                    case "RNL":
                                        tDiff = ((Conversion.ToDate(p_sPrevTransEffDate)).Subtract(Conversion.ToDate(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate)));
                                        iLength += tDiff.Days;
                                        break;
                                }
                                // npadhy End MITS 23088 Premium Calculation in case of Reinstate with and without lapse
                                dblProRataFactor = iLength * 1.0 / 365;

                            }
                            else
                            {
                                dblProRataFactor = 1;
                            }

                            sbSql = new StringBuilder();
                            sbSql.Append("SELECT * FROM SYS_POL_SEL_DISC, SYS_POL_EXP_RATES WHERE (SYS_POL_SEL_DISC.LINE_OF_BUSINESS=0 OR SYS_POL_SEL_DISC.LINE_OF_BUSINESS= ");
                            sbSql.Append(PolicyEnh.PolicyType);
                            sbSql.Append(" )AND (SYS_POL_SEL_DISC.STATE=0 OR SYS_POL_SEL_DISC.STATE = ");
                            sbSql.Append(iState);
                            sbSql.Append(" ) AND NOT IN_USE_FLAG = 0 AND IS_RATE_SETUP = 1");
                            sbSql.Append(" AND SYS_POL_EXP_RATES.EXP_RATE_ROWID=SYS_POL_SEL_DISC.DISCOUNT_ROWID AND ");
                            sbSql.Append("SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                            sbSql.Append(iCoveragesType);
                            sbSql.Append(" AND SYS_POL_SEL_DISC.PARENT_LEVEL = ");
                            sbSql.Append(iParentLevel);
                            sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                            sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                            sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                            sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                            sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                            sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                            sbSql.Append("'))");
                            objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                            
                            if (objReader.Read())
                            {
                                if (sLob == "AL" && objPolicyXCvgEnh.PolicyXCvgUarAddedAL.Count != 0)
                                {
                                    iUARCount = objPolicyXCvgEnh.PolicyXCvgUarAddedAL.Count;
                                }
                                else if (sLob == "PC" && objPolicyXCvgEnh.PolicyXCvgUarAddedPC.Count != 0)
                                {
                                    iUARCount = objPolicyXCvgEnh.PolicyXCvgUarAddedPC.Count;
                                }
                                else
                                {
                                    iUARCount = 1;
                                }
                                if (sFlatOrPercentage == "P")
                                {
                                    if (iLevel == 0)
                                    {
                                        dblCalcModifier = (objPolicyXCvgEnh.Modifier / 100) * p_structPremiumAmounts.ManualPremium * dblProRataFactor * iUARCount;
                                        dblTotalModifier = dblCalcModifier + dblLastTotal;
                                        objPolicyXCvgEnh.Amount = dblCalcModifier;
                                        dblLastTotal = dblTotalModifier;
                                    }
                                    else if (iLevel == 1)
                                    {
                                        dblCalcModifier = (objPolicyXCvgEnh.Modifier / 100) * p_structPremiumAmounts.ModifiedPremium * dblProRataFactor * iUARCount;
                                        dblTotalModifier = dblCalcModifier + dblLastTotal;
                                        objPolicyXCvgEnh.Amount = dblCalcModifier;
                                        dblLastTotal = dblTotalModifier;
                                    }
                                    else
                                    {
                                        dblCalcModifier = (objPolicyXCvgEnh.Modifier / 100) * dblLastTierAmount * dblProRataFactor * iUARCount;
                                        dblTotalModifier = dblCalcModifier + dblLastTotal;
                                        objPolicyXCvgEnh.Amount = dblCalcModifier;
                                        dblLastTotal = dblTotalModifier;
                                    }
                                }
                                else
                                {
                                    if (iLevel == 1)
                                    {
                                        dblCalcModifier = (objPolicyXCvgEnh.Modifier) * dblProRataFactor * iUARCount;
                                        dblTotalModifier = dblCalcModifier + dblLastTotal;
                                        objPolicyXCvgEnh.Amount = dblCalcModifier;
                                        dblLastTotal = dblTotalModifier;
                                    }
                                    else
                                    {
                                        dblCalcModifier = (objPolicyXCvgEnh.Modifier) * dblProRataFactor * iUARCount;
                                        dblTotalModifier = dblCalcModifier + dblLastTotal;
                                        objPolicyXCvgEnh.Amount = dblCalcModifier;
                                        dblLastTotal = dblTotalModifier;
                                    }

                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }


                    if (base.RoundFlag)
                        dblLastTotal = Math.Round(dblLastTotal, 0, MidpointRounding.AwayFromZero);
                    else
                        dblLastTotal = Math.Round(dblLastTotal, 2, MidpointRounding.AwayFromZero);

                    p_structPremiumAmounts.TotalModifier = dblLastTotal;


                }

            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.CalculateDeductiblePremium.Error", base.ClientId), objEx);
            }
            finally
            {
                sbSql = null;
                if (objReader != null)
                {
                    objReader.Close();
                }
            }

        }
        //Anu Tennyson for MITS 18229 ENDS
        private void CreateDiscountControl(XmlElement p_objGroup, Discount p_structDiscount , bool p_bDiscountEditable, bool p_bRangeVisible)
        {
            XmlElement objDisplayColumn = null;
            XmlElement objControlGroup = null;
            //pmahli 9/12/2007 VolumeDisocunt -Start
            DbReader objReader = null;
            string sSQL = string.Empty;
            bool bUseVolDiscount = false;
            //Check wheaher disocunt is volume discount  
            sSQL = "Select USE_VOLUME_DISCOUNT from SYS_POL_DISCOUNTS where Discount_RowId = " + p_structDiscount.NameId;
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                bUseVolDiscount = Conversion.ConvertObjToBool(objReader.GetValue("USE_VOLUME_DISCOUNT"), base.ClientId);
            }
            objReader.Close();
            //pmahli 9/12/2007  VolumeDisocunt - End
            string sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/" + CALC_PREM_DISCOUNTS + "/" + CALC_PREM_DISCOUNT + "/" + CALC_PREM_DISCOUNT_ID + "[. = '" + p_structDiscount.Id.ToString() + "']/../";

            string sDiscountAmountFactorTypeControl = "";
            string sDiscountId = "Discount" + p_structDiscount.Id.ToString();
            string sControlGroupDiscountId = "Discount_cg" + p_structDiscount.Id.ToString();
            string sDiscountAmountId = "DiscountAmount" + p_structDiscount.Id.ToString();
            string sDiscountLabelId = "DiscountLabel" + p_structDiscount.Id.ToString();
            string sDiscountBlankLabelId = "DiscountBlankLabel" + p_structDiscount.Id.ToString();
            string sDiscountPercentLabelId = "DiscountPerLabel" + p_structDiscount.Id.ToString();
            string sDiscountSpaceLabelId = "DiscountSpaceLabel" + p_structDiscount.Id.ToString();
            string sDiscountAmountFactorId = "DiscountAmountFactor" + p_structDiscount.Id.ToString();
            string sRangeButtonId = "RangeButton" + p_structDiscount.Id.ToString();
            string sRangeBlankId = "RangeBlank" + p_structDiscount.Id.ToString();
            string sDiscountNameId = "DiscountNameId" + p_structDiscount.Id.ToString();
            string sDiscountLevel = "DiscountLevel" + p_structDiscount.Id.ToString();
            string sDiscountType = "DiscountType" + p_structDiscount.Id.ToString();
            //pmahli 9/12/2007 VolumeDisocunt -Start 
            //if volume disocunt then type is currency
            if (bUseVolDiscount)
            {
                sDiscountAmountFactorTypeControl = "currency";
            }
            //pmahli 9/12/2007 Voulme Discount -End
            else
            {
                if (p_structDiscount.Type == "P")
                    sDiscountAmountFactorTypeControl = "numeric";
                else
                    sDiscountAmountFactorTypeControl = "currency";
            }
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);

            this.CreateLabelControl(objDisplayColumn, sDiscountBlankLabelId, "");
            this.CreateLabelControl(objDisplayColumn, sDiscountLabelId, p_structDiscount.Name);
            this.CreateControlGroup(objDisplayColumn, ref objControlGroup, sControlGroupDiscountId);

            this.CreateControl(objControlGroup, sDiscountAmountFactorId, "", sBasePath + CALC_PREM_DISCOUNT_AMOUNT_FACTOR, sDiscountAmountFactorTypeControl, "1", !p_bDiscountEditable, "5", "return OnDiscountFactorChange()");

            //pmahli 9/12/2007 Volume Discount 
            //If volume disocunt then % should not be appended
            if (p_structDiscount.Type == "P" && (!bUseVolDiscount))
                this.CreateLabelControl(objControlGroup, sDiscountPercentLabelId, "%", sBasePath + CALC_PREM_DISCOUNT_TYPE);
            else
                this.CreateSpaceControl(objControlGroup, "24", sDiscountSpaceLabelId);
            if (p_bRangeVisible)
                this.CreateButtonScriptControl(objControlGroup, sRangeButtonId, "...", "return OnRangeClicked( '" + p_structDiscount.Id.ToString() + "' , '" + p_structDiscount.NameId.ToString() + "');");
            else
                this.CreateSpaceControl(objControlGroup, "14", sRangeBlankId);
            this.CreateControl(objControlGroup, sDiscountAmountId, "", sBasePath + CALC_PREM_DISCOUNT_AMOUNT, "currency", "1", true, "15");

            this.CreateHiddenControl(objControlGroup, sDiscountType, p_structDiscount.Type, sBasePath + CALC_PREM_DISCOUNT_TYPE);
            this.CreateHiddenControl(objControlGroup, sDiscountNameId, p_structDiscount.NameId.ToString(), sBasePath + CALC_PREM_DISCOUNT_NAME_ID);
            this.CreateHiddenControl(objControlGroup, sDiscountLevel, p_structDiscount.Level.ToString(), sBasePath + CALC_PREM_DISCOUNT_LEVEL);

            objDisplayColumn.SetAttribute(CALC_PREM_LEVEL, p_structDiscount.Level.ToString());
            objDisplayColumn.SetAttribute(CALC_PREM_TYPE, CALC_PREM_TYPE_DISCOUNT);
        }

        private void CreateDiscountTierControl(XmlElement p_objGroup, DiscountTier p_structDiscountTier)
        {
            XmlElement objDisplayColumn = null;
            string sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/" + CALC_PREM_TIERS + "/" + CALC_PREM_TIER + "/" + CALC_PREM_TIER_ID + "[. = '" + p_structDiscountTier.Id.ToString() + "']/../";
            string sDiscountTierAmountId = "DiscountTierAmount" + p_structDiscountTier.NameId.ToString();
            string sDiscountTierLevel = "DiscountTierLevel" + p_structDiscountTier.NameId.ToString();
            string sDiscountTierNameId = "DiscountTierName" + p_structDiscountTier.NameId.ToString();

            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            this.CreateControl(objDisplayColumn, sDiscountTierAmountId, p_structDiscountTier.Name, sBasePath + CALC_PREM_TIER_AMOUNTS, "currency", "1", true);
            this.CreateHiddenControl(objDisplayColumn, sDiscountTierLevel, p_structDiscountTier.LevelId.ToString(), sBasePath + CALC_PREM_TIER_LEVEL);
            this.CreateHiddenControl(objDisplayColumn, sDiscountTierNameId, p_structDiscountTier.NameId.ToString(), sBasePath + CALC_PREM_TIER_NAME_ID);
            objDisplayColumn.SetAttribute(CALC_PREM_TIER_LEVEL, p_structDiscountTier.LevelId.ToString());
            objDisplayColumn.SetAttribute(CALC_PREM_TYPE, CALC_PREM_TYPE_TIER);
            objDisplayColumn.SetAttribute(CALC_PREM_TIER_NAME, p_structDiscountTier.NameId.ToString());
        }
        //Anu Tennyson for MITS 18229 Prem Calc. STARTS
        /// <summary>
        /// Function to create the screen xml before save
        /// </summary>
        /// <param name="p_objGroup">Control to create the page control</param>
        /// <param name="p_structPremiumAmounts">Premium Amounts Structure</param>
        /// <param name="p_objLastControl">XML Element containg the Information about the last control</param>
        /// <param name="p_iTransNumber">Trans Numver</param>
        /// <param name="p_bEditFlag">Flag to check whether the range is editable or not</param>
        /// <param name="p_structEnhancePolicyUI">Enhance policy UI structures</param>
        /// <param name="iParentLevel">Parent level.</param>
        private void CreateDeductibleBeforeSaveXml(XmlElement p_objGroup, ref PremiumAmounts p_structPremiumAmounts, ref XmlElement p_objLastControl, int p_iTransNumber, bool p_bEditFlag, EnhancePolicyUI p_structEnhancePolicyUI, int iParentLevel)
        {
            XmlElement objDisplayColumn = null;
            int iCoverageId = 0;
            string sCoverage = string.Empty;
            string sBasePath = string.Empty;
            int iFlatOrPercent = 0;
            string sFlatOrPercent = string.Empty;
            string sRangeButton = string.Empty;
            string sModifierBlankLabelId = string.Empty;
            string sControlGroupModifierId = string.Empty;
            string sModifierAmountFactorId = string.Empty;
            string sModifierPercentLabelId = string.Empty;
            string sModifierAmountId = string.Empty;
            string sModifierType = string.Empty;
            string sModiferLevel = string.Empty;
            string sModiferNameID = string.Empty;
            string sDeductibleNameId = string.Empty;
            string sType = string.Empty;
            string sModifierAmountFactorTypeControl = string.Empty;
            int iCount = 0;
            int iLevel = 0;
            XmlElement objControlGroup = null;
            StringBuilder sbSql = null;
            DbReader objReader = null;
            bool bIsRangeEditable = false;
            bool bIsRangeAvailable = false;
            bool bIsModifierEditable = false;
            int iUnique = 0;
            int iRowid = 0;
            string sUniqueId = string.Empty;


            foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
            {
                if (objPolicyXCvgEnh.PolcvgRowId < 0)
                {
                    sUniqueId = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.CoverageSeq.ToString();
                }
                else
                {
                    sUniqueId = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.PolcvgRowId.ToString();
                }
                if (objPolicyXCvgEnh.TransactionId == p_iTransNumber || objPolicyXCvgEnh.TransactionId == 0)
                {
                    bIsRangeAvailable = false;
                    bIsModifierEditable = false;
                    // If the Deductible and Modifier are zero, we still want to show the Coverage in Prem Calc Screen
                    // because it might be associated with ranges. User might wish to change the existing modifier, and select a range

                    //if (objPolicyXCvgEnh.Deductible == 0.0 && objPolicyXCvgEnh.Modifier == 0.0)
                    //{
                    //    continue;

                    //}

                    if (PolicyEnh.PolicyXExposureEnhList.Count == 0)
                    {
                        break;
                    }

                    iUnique = iCount++;
                    sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/" + CALC_PREM_MODIFIERS + "/" + CALC_PREM_MODIFIER + "/";

                    iCoverageId = objPolicyXCvgEnh.CoverageTypeCode;
                    sCoverage = base.LocalCache.GetCodeDesc(iCoverageId);
                    string sDeductibleTitle = sCoverage;
                    string sDeductibleLabelID = (CALC_PREM_DEDUCTIBLE_LABEL_ID + iCount).ToString();
                    int iState = PolicyEnh.State;
                    int iCoveragesType = objPolicyXCvgEnh.CoverageTypeCode;
                    try
                    {
                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT * FROM SYS_POL_RANGE, SYS_POL_EXP_RATES WHERE SYS_POL_EXP_RATES.LINE_OF_BUSINESS= ");
                        sbSql.Append(PolicyEnh.PolicyType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.STATE = ");
                        sbSql.Append(iState);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EXP_RATE_ROWID=SYS_POL_RANGE.RATE_ROWID AND ");
                        sbSql.Append("SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                        sbSql.Append(iCoveragesType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                        sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                        sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("'))");
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                        if (objReader.Read() && p_bEditFlag)
                        {
                            bIsRangeAvailable = true;
                        }
                        objReader.Close();
                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT * FROM SYS_POL_EXP_RATES WHERE SYS_POL_EXP_RATES.LINE_OF_BUSINESS= ");
                        sbSql.Append(PolicyEnh.PolicyType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.STATE = ");
                        sbSql.Append(iState);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                        sbSql.Append(iCoveragesType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                        sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                        sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("'))");
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                        if (objReader.Read())
                        {
                            iFlatOrPercent = objReader.GetInt("FLAT_OR_PERCENT");
                            sFlatOrPercent = base.LocalCache.GetShortCode(iFlatOrPercent);
                        }
                        objReader.Close();
                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT * FROM SYS_POL_SEL_DISC, SYS_POL_EXP_RATES WHERE (SYS_POL_SEL_DISC.LINE_OF_BUSINESS=0 OR SYS_POL_SEL_DISC.LINE_OF_BUSINESS= ");
                        sbSql.Append(PolicyEnh.PolicyType);
                        sbSql.Append(" )AND (SYS_POL_SEL_DISC.STATE=0 OR SYS_POL_SEL_DISC.STATE = ");
                        sbSql.Append(iState);
                        sbSql.Append(" ) AND NOT IN_USE_FLAG = 0 AND IS_RATE_SETUP = 1");
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EXP_RATE_ROWID=SYS_POL_SEL_DISC.DISCOUNT_ROWID AND ");
                        sbSql.Append("SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                        sbSql.Append(iCoveragesType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                        sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                        sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("'))");
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                        if (objReader.Read())
                        {
                            bIsModifierEditable = objReader.GetBoolean("OVERRIDE_FLAG");
                            iRowid = objReader.GetInt("DISCOUNT_ROWID");
                            iLevel = objReader.GetInt("PARENT_LEVEL");
                            if (iLevel == iParentLevel)
                            {
                                if (sFlatOrPercent == "P")
                                {
                                    sModifierAmountFactorTypeControl = "numeric";
                                }
                                else
                                {
                                    sModifierAmountFactorTypeControl = "currency";
                                }

                                sRangeButton = "CoverageRangeButton" + iUnique;
                                sModifierBlankLabelId = "CoverageModifier" + iUnique;
                                sControlGroupModifierId = "CoverageModifier_cg" + iUnique;
                                sModifierAmountFactorId = "CoverageModiferAmountFactor" + sUniqueId;
                                sModifierPercentLabelId = "CoveragePerLabel" + iUnique;
                                sModifierAmountId = "ModifierAmount" + sUniqueId;
                                sModifierType = "ModifierType" + iUnique;
                                sModiferLevel = "ModiferLevel" + iUnique;
                                sModiferNameID = "ModifierNameId" + iUnique;

                                bIsRangeEditable = bIsModifierEditable && !bIsRangeAvailable && p_bEditFlag;

                                this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
                                this.CreateLabelControl(objDisplayColumn, sModifierBlankLabelId, "");
                                this.CreateLabelControl(objDisplayColumn, sDeductibleLabelID, sDeductibleTitle);
                                this.CreateControlGroup(objDisplayColumn, ref objControlGroup, sControlGroupModifierId);
                                this.CreateControl(objControlGroup, sModifierAmountFactorId, "", sBasePath + CALC_PREM_MODIFIER_AMOUNT_FACTORID + sUniqueId, sModifierAmountFactorTypeControl, "1", !bIsRangeEditable, "5", "return OnModifierChange()");

                                if (bIsRangeAvailable)
                                {
                                    this.CreateLabelControl(objControlGroup, sModifierPercentLabelId, "%", sBasePath + CALC_PREM_MODIFIER_TYPE);
                                    if (objPolicyXCvgEnh.PolcvgRowId < 0)
                                    {
                                        this.CreateButtonScriptControl(objControlGroup, sRangeButton, "...", "return OnModifierRangeClicked( '" + objPolicyXCvgEnh.ExpRateRowId + "' , '" + objPolicyXCvgEnh.CoverageSeq + "');");
                                    }
                                    else
                                    {
                                        this.CreateButtonScriptControl(objControlGroup, sRangeButton, "...", "return OnModifierRangeClicked( '" + objPolicyXCvgEnh.ExpRateRowId + "' , '" + objPolicyXCvgEnh.PolcvgRowId + "');");
                                    }
                                }
                                this.CreateControl(objControlGroup, sModifierAmountId, "", sBasePath + CALC_PREM_MODIFIER_AMOUNT + sUniqueId, "currency", "1", true, "15");
                                p_objLastControl = objDisplayColumn;
                                objDisplayColumn.SetAttribute(CALC_PREM_DEDUCTIBLE_ADDED, Convert.ToString(objPolicyXCvgEnh.CoverageSeq));
                            }
                        }
                        objReader.Close();
                    }
                    catch (Exception objEx)
                    {
                        throw new RMAppException(Globalization.GetString("EnhancePolicy.CreateDeductibleXml.Error", base.ClientId), objEx);
                    }
                    finally
                    {
                        sbSql = null;
                        objPolicyXCvgEnh.Dispose();
                        if (objReader != null)
                        {
                            objReader.Close();
                        }
                    }
                }
            }
        }
        //Anu Tennyson
        //Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
        /// <summary>
        /// Function for creating the premium calculation page when coverages are added after save.
        /// </summary>
        /// <param name="p_objGroup">Control to create the page control</param>
        /// <param name="p_structPremiumAmounts">Premium Amounts Structure</param>
        /// <param name="p_iTransNumber">Trans Numver</param>
        /// <param name="p_bEditFlag">Flag to check whether the range is editable or not</param>
        private void CreateDeductibleXml(XmlElement p_objGroup, ref PremiumAmounts p_structPremiumAmounts,  int p_iTransNumber, bool p_bEditFlag, EnhancePolicyUI p_structEnhancePolicyUI)
        {
            XmlElement objDisplayColumn = null;
            int iCoverageId = 0;
            string sCoverage = string.Empty;
            string sBasePath = string.Empty;
            int iFlatOrPercent = 0;
            string sFlatOrPercent = string.Empty;
            string sRangeButton = string.Empty;
            string sModifierBlankLabelId = string.Empty;
            string sControlGroupModifierId = string.Empty;
            string sModifierAmountFactorId = string.Empty;
            string sModifierPercentLabelId = string.Empty;
            string sModifierAmountId = string.Empty;
            string sModifierType = string.Empty;
            string sModiferLevel = string.Empty;
            string sModiferNameID = string.Empty;
            string sDeductibleNameId = string.Empty;
            string sType = string.Empty;
            string sModifierAmountFactorTypeControl = string.Empty;
            int iCount = 0;
            int iParentLevel = 0;
            XmlElement objControlGroup = null;
            StringBuilder sbSql = null;
            DbReader objReader = null;
            bool bIsRangeEditable = false;
            bool bIsRangeAvailable = false;
            bool bIsModifierEditable = false;
            int iUnique = 0;
            int iRowid = 0;
            string sUniqueId = string.Empty;


            foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
            {
                if (objPolicyXCvgEnh.PolcvgRowId < 0)
                {
                    sUniqueId = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.CoverageSeq.ToString();
                }
                else
                {
                    sUniqueId = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.PolcvgRowId.ToString();
                }
                if (objPolicyXCvgEnh.TransactionId == p_iTransNumber || objPolicyXCvgEnh.TransactionId == 0)
                {
                    bIsRangeAvailable = false;
                    bIsModifierEditable = false;
                    // If the Deductible and Modifier are zero, we still want to show the Coverage in Prem Calc Screen
                    // because it might be associated with ranges. User might wish to change the existing modifier, and select a range

                    //if (objPolicyXCvgEnh.Deductible == 0.0 && objPolicyXCvgEnh.Modifier == 0.0)
                    //{
                    //    continue;

                    //}

                    if (PolicyEnh.PolicyXExposureEnhList.Count == 0)
                    {
                        break;
                    }
                    
                    iUnique = iCount++;
                    sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/" + CALC_PREM_MODIFIERS + "/" + CALC_PREM_MODIFIER + "/";

                    iCoverageId = objPolicyXCvgEnh.CoverageTypeCode;
                    sCoverage = base.LocalCache.GetCodeDesc(iCoverageId);
                    string sDeductibleTitle = sCoverage;
                    string sDeductibleLabelID = (CALC_PREM_DEDUCTIBLE_LABEL_ID + iCount).ToString();
                    int iState = PolicyEnh.State;
                    int iCoveragesType = objPolicyXCvgEnh.CoverageTypeCode;
                    try
                    {
                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT * FROM SYS_POL_RANGE, SYS_POL_EXP_RATES WHERE SYS_POL_EXP_RATES.LINE_OF_BUSINESS= ");
                        sbSql.Append(PolicyEnh.PolicyType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.STATE = ");
                        sbSql.Append(iState);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EXP_RATE_ROWID=SYS_POL_RANGE.RATE_ROWID AND ");
                        sbSql.Append("SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                        sbSql.Append(iCoveragesType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                        sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                        sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("'))");

                        objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                        foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                        {
                            if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                            {
                                if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType) == "EN" && base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus) == "PR")
                                {
                                    p_bEditFlag = true;
                                }
                            }
                        }
                        if (objReader.Read() && p_bEditFlag)
                        {
                            bIsRangeAvailable = true;
                        }
                        objReader.Close();
                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT * FROM SYS_POL_EXP_RATES WHERE SYS_POL_EXP_RATES.LINE_OF_BUSINESS= ");
                        sbSql.Append(PolicyEnh.PolicyType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.STATE = ");
                        sbSql.Append(iState);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                        sbSql.Append(iCoveragesType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                        sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                        sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("'))");
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                        if (objReader.Read())
                        {
                            iFlatOrPercent = objReader.GetInt("FLAT_OR_PERCENT");
                            sFlatOrPercent = base.LocalCache.GetShortCode(iFlatOrPercent);
                        }
                        objReader.Close();
                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT * FROM SYS_POL_SEL_DISC, SYS_POL_EXP_RATES WHERE (SYS_POL_SEL_DISC.LINE_OF_BUSINESS=0 OR SYS_POL_SEL_DISC.LINE_OF_BUSINESS= ");
                        sbSql.Append(PolicyEnh.PolicyType);
                        sbSql.Append(" )AND (SYS_POL_SEL_DISC.STATE=0 OR SYS_POL_SEL_DISC.STATE = ");
                        sbSql.Append(iState);
                        sbSql.Append(" ) AND NOT IN_USE_FLAG = 0 AND IS_RATE_SETUP = 1");
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EXP_RATE_ROWID=SYS_POL_SEL_DISC.DISCOUNT_ROWID AND ");
                        sbSql.Append("SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                        sbSql.Append(iCoveragesType);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                        sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                        sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                        sbSql.Append(p_structEnhancePolicyUI.PolicyUIData.EffectiveDate);
                        sbSql.Append("'))");

                        objReader = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString());
                        if (objReader.Read())
                        {
                            bIsModifierEditable = objReader.GetBoolean("OVERRIDE_FLAG");
                            iRowid = objReader.GetInt("DISCOUNT_ROWID");
                            iParentLevel = objReader.GetInt("PARENT_LEVEL");
                        if (sFlatOrPercent == "P")
                        {
                            sModifierAmountFactorTypeControl = "numeric";
                        }
                        else
                        {
                            sModifierAmountFactorTypeControl = "currency";
                        }

                        sRangeButton = "CoverageRangeButton" + iUnique;
                        sModifierBlankLabelId = "CoverageModifier" + iUnique;
                        sControlGroupModifierId = "CoverageModifier_cg" + iUnique;
                        sModifierAmountFactorId = "CoverageModiferAmountFactor" + sUniqueId;
                        sModifierPercentLabelId = "CoveragePerLabel" + iUnique;
                        sModifierAmountId = "ModifierAmount" + sUniqueId;
                        sModifierType = "ModifierType" + iUnique;
                        sModiferLevel = "ModiferLevel" + iUnique;
                        sModiferNameID = "ModifierNameId" + iUnique;
                        
                        bIsRangeEditable = bIsModifierEditable && !bIsRangeAvailable && p_bEditFlag;

                            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
                        this.CreateLabelControl(objDisplayColumn, sModifierBlankLabelId, "");
                        this.CreateLabelControl(objDisplayColumn, sDeductibleLabelID, sDeductibleTitle);
                        this.CreateControlGroup(objDisplayColumn, ref objControlGroup, sControlGroupModifierId);
                        this.CreateControl(objControlGroup, sModifierAmountFactorId, "", sBasePath + CALC_PREM_MODIFIER_AMOUNT_FACTORID + sUniqueId, sModifierAmountFactorTypeControl, "1", !bIsRangeEditable, "5", "return OnModifierChange()");

                        if (bIsRangeAvailable)
                        {
                            this.CreateLabelControl(objControlGroup, sModifierPercentLabelId, "%", sBasePath + CALC_PREM_MODIFIER_TYPE);
                            if (objPolicyXCvgEnh.PolcvgRowId < 0)
                            {
                                this.CreateButtonScriptControl(objControlGroup, sRangeButton, "...", "return OnModifierRangeClicked( '" + objPolicyXCvgEnh.ExpRateRowId + "' , '" + objPolicyXCvgEnh.CoverageSeq + "');");
                            }
                            else
                            {
                                this.CreateButtonScriptControl(objControlGroup, sRangeButton, "...", "return OnModifierRangeClicked( '" + objPolicyXCvgEnh.ExpRateRowId + "' , '" + objPolicyXCvgEnh.PolcvgRowId + "');");
                            }
                        }
                        this.CreateControl(objControlGroup, sModifierAmountId, "", sBasePath + CALC_PREM_MODIFIER_AMOUNT + sUniqueId, "currency", "1", true, "15");
                        objDisplayColumn.SetAttribute(CALC_PREM_DEDUCITBLE_LEVEL, Convert.ToString(iParentLevel));
                        objDisplayColumn.SetAttribute(CALC_PREM_TYPE, CALC_PREM_TYPE_MODIFIER); 
                        }
                        objReader.Close();
                    }
                    catch (Exception objEx)
                    {
                        throw new RMAppException(Globalization.GetString("EnhancePolicy.CreateDeductibleXml.Error", base.ClientId), objEx);
                    }
                    finally
                    {
                        sbSql = null;
                        objPolicyXCvgEnh.Dispose();
                        if (objReader != null)
                        {
                            objReader.Close();
                        }
                    }
                }
            }
        }
        //Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
        private void CreateManualPremiumXML(XmlElement p_objGroup, EnhancePolicyUI p_structEnhancePolicyUI, ref XmlElement p_objLastControl, bool p_bBuildFlag, ref PremiumAmounts p_structPremiumAmounts, int p_iTransNumber, string p_sLob, bool p_bEditFlag, ref  XmlDocument objPremiumCalculation, ref bool bFinalPremiumVisible)
        {
            XmlElement objDisplayColumn = null;
            XmlElement objControlGroup = null;
            string sBasePath = string.Empty;

            sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/";

            // Manual Premium
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            this.CreateControl(objDisplayColumn, CALC_PREM_MANUAL_PREMIUM_ID, CALC_PREM_MANUAL_PREMIUM_TITLE, sBasePath + CALC_PREM_MANUAL_PREMIUM_ID, "currency", "1" , true );
            if (p_bBuildFlag)
            {
                CreateDiscountTier(p_iTransNumber, p_objGroup, p_sLob, p_bEditFlag, p_structEnhancePolicyUI, ref  p_structPremiumAmounts, ref p_objLastControl, 0, ref bFinalPremiumVisible);
               // this.CreateFinalPremiumXML(p_objGroup, p_structEnhancePolicyUI, bFinalPremiumVisible);
                sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/";
                // Final Premium
                if (bFinalPremiumVisible)
                {
                    this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
                    this.CreateControl(objDisplayColumn, CALC_PREM_MANUAL_FINAL_PREMIUM_ID, CALC_PREM_MANUAL_FINAL_PREMIUM_TITLE, sBasePath + CALC_PREM_MANUAL_FINAL_PREMIUM_ID, "currency", "1", true);
                }
            }
            else
            {
                p_objLastControl = objDisplayColumn;
                //CreateDiscountTierEdit(p_iTransNumber, p_objGroup, p_sLob, p_bEditFlag, p_structEnhancePolicyUI, ref  p_structPremiumAmounts, ref p_objLastControl, 0, ref  objPremiumCalculation);
                //CreateDiscountTier(p_iTransNumber, p_objGroup, p_sLob, p_bEditFlag, p_structEnhancePolicyUI, ref  p_structPremiumAmounts, ref p_objLastControl, 0, ref bFinalPremiumVisible);
                // this.CreateFinalPremiumXML(p_objGroup, p_structEnhancePolicyUI, bFinalPremiumVisible);
                sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/";
                // Final Premium
                this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
                this.CreateControl(objDisplayColumn, CALC_PREM_MANUAL_FINAL_PREMIUM_ID, CALC_PREM_MANUAL_FINAL_PREMIUM_TITLE, sBasePath + CALC_PREM_MANUAL_FINAL_PREMIUM_ID, "currency", "1", true);
            }

            // Exp. Mod Factor
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            this.CreateLabelControl(objDisplayColumn, CALC_PREM_EXP_MOD_FACTOR_LABEL_BLANK_ID, "");
            this.CreateLabelControl(objDisplayColumn, CALC_PREM_EXP_MOD_FACTOR_LABEL_ID, CALC_PREM_EXP_MOD_FACTOR_TITLE);
            this.CreateControlGroup(objDisplayColumn, ref objControlGroup, CALC_PREM_EXP_MOD_FACTOR_CG_ID);
            this.CreateControl(objControlGroup, CALC_PREM_EXP_MOD_FACTOR_ID, "", sBasePath + CALC_PREM_EXP_MOD_FACTOR_ID, "numeric", "1", !p_structEnhancePolicyUI.PremiumCalculationUI.ExpModFactorEnabled, "5", "return OnExpModFactorChange()");
            this.CreateSpaceControl(objControlGroup, "44", CALC_PREM_EXP_MOD_FACTOR_SPACE_ID); 
            this.CreateControl(objControlGroup, CALC_PREM_EXP_MOD_FACTOR_AMOUNT_ID, CALC_PREM_EXP_MOD_FACTOR_AMOUNT_TITLE, sBasePath + CALC_PREM_EXP_MOD_FACTOR_AMOUNT_ID, "currency", "1", true, "15");

            // Modified Premium
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            this.CreateControl(objDisplayColumn, CALC_PREM_MODIFIED_PREMIUM_ID, CALC_PREM_MODIFIED_PREMIUM_TITLE, sBasePath + CALC_PREM_MODIFIED_PREMIUM_ID, "currency", "1",true);
            p_objLastControl = objDisplayColumn;


        }

        private void CreateFinalPremiumXML(XmlElement p_objGroup, EnhancePolicyUI p_strcutEnhancePolicyUI)
        {
            this.CreateFinalPremiumXML(p_objGroup, p_strcutEnhancePolicyUI, true);
        }

        private void CreateFinalPremiumXML(XmlElement p_objGroup, EnhancePolicyUI p_strcutEnhancePolicyUI, bool p_bFinalPremiumVisible)
        {
            XmlElement objDisplayColumn = null;
            XmlElement objControlGroup = null;
            string sBasePath = string.Empty;

            sBasePath = Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_ROOT + "/";

            
            // Final Premium
            if (p_bFinalPremiumVisible)
            {

                this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
                this.CreateControl(objDisplayColumn, CALC_PREM_FINAL_PREMIUM_ID, CALC_PREM_FINAL_PREMIUM_TITLE, sBasePath + CALC_PREM_FINAL_PREMIUM_ID, "currency", "1", true);
            }
            //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
            string sSQL = " SELECT * FROM SYS_POL_EXP_CONST WHERE LINE_OF_BUSINESS=" + PolicyEnh.PolicyType
                + " AND STATE=" + PolicyEnh.State + " AND NOT IN_USE_FLAG = 0"
                + " AND EFFECTIVE_DATE <= '" + p_strcutEnhancePolicyUI.PolicyUIData.EffectiveDate
                + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE='' OR EXPIRATION_DATE >= '" + p_strcutEnhancePolicyUI.PolicyUIData.EffectiveDate + "')";

            DbReader objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                //Expense Constant
                string sFlatorPercent = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));
                this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
                if (sFlatorPercent == "P")
                    this.CreateControl(objDisplayColumn, CALC_PREM_EXPENSE_CONST_ID, CALC_PREM_EXPENSE_CONST_TITLE, sBasePath + CALC_PREM_EXPENSE_CONST_ID, "numeric", "1", true);
                else
                {
                    this.CreateControl(objDisplayColumn, CALC_PREM_EXPENSE_CONST_ID, CALC_PREM_EXPENSE_CONST_TITLE, sBasePath + CALC_PREM_EXPENSE_CONST_ID, "currency", "1", true);
                }
                if (sFlatorPercent == "P")
                    this.CreateLabelControl(objDisplayColumn, "PExpenseConstant", "%");
                else
                    this.CreateSpaceControl(objDisplayColumn, "24","FExpenseConstant");

            }
            objReader.Close();

            //npadhy RMSC retrofit Starts
            sSQL = " SELECT * FROM SYS_POL_TAX WHERE LINE_OF_BUSINESS=" + PolicyEnh.PolicyType
                + " AND STATE=" + PolicyEnh.State + " AND NOT IN_USE_FLAG = 0"
                + " AND EFFECTIVE_DATE <= '" + p_strcutEnhancePolicyUI.PolicyUIData.EffectiveDate
                + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE='' OR EXPIRATION_DATE >= '" + p_strcutEnhancePolicyUI.PolicyUIData.EffectiveDate + "')";

            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                //Tax Amount
                string sFlatorPercent = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));
                this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
                if (sFlatorPercent == "P")
                {
                    this.CreateControl(objDisplayColumn, CALC_PREM_TAX_RATE_ID, CALC_PREM_TAX_RATE_TITLE, sBasePath + CALC_PREM_TAX_RATE_ID, "numeric", "1", true);
                    this.CreateLabelControl(objDisplayColumn, "PTax", "%", sBasePath + "PTAX");
                    this.CreateControlGroup(objDisplayColumn, ref objControlGroup, CALC_PREM_TAX_RATE_CG_ID);
                    this.CreateControl(objControlGroup, CALC_PREM_TAX_ID, "", sBasePath + CALC_PREM_TAX_ID, "currency", "1", true, "15");
                    this.CreateHiddenControl(objControlGroup, CALC_PREM_HID_TAX_TYPE_ID, "", sBasePath + CALC_PREM_HID_TAX_TYPE_ID);
                }
                else
                {
                    this.CreateControl(objDisplayColumn, CALC_PREM_TAX_RATE_ID, CALC_PREM_TAX_RATE_TITLE, sBasePath + CALC_PREM_TAX_RATE_ID, "currency", "1", true);
                    this.CreateLabelControl(objDisplayColumn, "FTax", "", sBasePath + "FTAX");
                    this.CreateControlGroup(objDisplayColumn, ref objControlGroup, CALC_PREM_TAX_RATE_CG_ID);
                    this.CreateControl(objControlGroup, CALC_PREM_TAX_ID, "", sBasePath + CALC_PREM_TAX_ID, "currency", "1", true, "15");
                    this.CreateHiddenControl(objControlGroup, CALC_PREM_HID_TAX_TYPE_ID, "", sBasePath + CALC_PREM_HID_TAX_TYPE_ID);
                }
                
            }
            objReader.Close();
            //npadhy RMSC retrofit Ends
            // Audited Premium
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            this.CreateControl(objDisplayColumn, CALC_PREM_AUDITED_PREMIUM_ID, CALC_PREM_AUDITED_PREMIUM_TITLE, sBasePath + CALC_PREM_AUDITED_PREMIUM_ID, "currency", "1", true);
            
            // Estimated Premium            
            this.CreateLabelControl(objDisplayColumn, CALC_PREM_ESTIMATED_PREMIUM_LABEL_ID, CALC_PREM_ESTIMATED_PREMIUM_TITLE);
            this.CreateControlGroup(objDisplayColumn, ref objControlGroup, CALC_PREM_ESTIMATED_PREMIUM_CG_ID);
            this.CreateControl(objControlGroup, CALC_PREM_ESTIMATED_PREMIUM_ID, "", sBasePath + CALC_PREM_ESTIMATED_PREMIUM_ID, "currency", "1", true);

            // Transaction Premium
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            this.CreateControl(objDisplayColumn, CALC_PREM_TRANSACTION_PREMIUM_ID, CALC_PREM_TRANSACTION_PREMIUM_TITLE, sBasePath + CALC_PREM_TRANSACTION_PREMIUM_ID, "currency", "1", true);

            //npadhy RMSC retrofit Starts
            this.CreateLabelControl(objDisplayColumn, CALC_PREM_TRANS_TAX_LABEL_ID, CALC_PREM_TRANS_TAX_LABEL_TITLE);
            this.CreateControlGroup(objDisplayColumn, ref objControlGroup, CALC_PREM_TRANS_TAX_CG_ID);
            this.CreateControl(objControlGroup, CALC_PREM_TRANS_TAX_ID, "", sBasePath + CALC_PREM_TRANS_TAX_ID, "currency", "1", true);            
            //npadhy RMSC retrofit Ends

            // Total Billed Premium
            //npadhy RMSC retrofit Starts - Modified for RMSC 
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            this.CreateControl(objDisplayColumn, CALC_PREM_TOTAL_BILLED_PREMIUM_ID, CALC_PREM_TOTAL_BILLED_PREMIUM_TITLE, sBasePath + CALC_PREM_TOTAL_BILLED_PREMIUM_ID, "currency", "1", true);
			//npadhy RMSC retrofit Ends - Modified for RMSC 
            //npadhy RMSC retrofit Starts, Commented for RMSC 
            //this.CreateLabelControl(objDisplayColumn, CALC_PREM_TOTAL_BILLED_PREMIUM_LABEL_ID, CALC_PREM_TOTAL_BILLED_PREMIUM_TITLE);
            //this.CreateControlGroup(objDisplayColumn, ref objControlGroup, CALC_PREM_TOTAL_BILLED_PREMIUM_CG_ID);
            //this.CreateControl(objControlGroup, CALC_PREM_TOTAL_BILLED_PREMIUM_ID, "", sBasePath + CALC_PREM_TOTAL_BILLED_PREMIUM_ID, "currency", "1", true);            
			//npadhy RMSC retrofit Ends

            // Waive Premium Control. 
            this.CreateDisplayColumn(p_objGroup, ref objDisplayColumn);
            if( p_strcutEnhancePolicyUI.PremiumCalculationUI.WaivePremiumVisible )
                this.CreateControl(objDisplayColumn, CALC_PREM_WAIVE_PREMIUM_ID, CALC_PREM_WAIVE_PREMIUM_TITLE, Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_WAIVE_PREMIUM_ID, "checkbox", "1" , "", "return OnWaivePremiumChecked();" );

            if ( p_strcutEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsVisible)
            {
                this.CreateLabelControl(objDisplayColumn, CALC_PREM_COMMENTS_LABEL_ID, CALC_PREM_COMMENTS_TITLE);
                this.CreateControlGroup(objDisplayColumn, ref objControlGroup, CALC_PREM_COMMENTS_CG_ID);            
                this.CreateControl(objControlGroup, CALC_PREM_COMMENTS_ID, "", Constants.INSTANCE_SYSEXDATA_PATH + CALC_PREM_COMMENTS_ID, "memo", "1", !p_strcutEnhancePolicyUI.PremiumCalculationUI.WaivePremiumCommentsEnabled);
            }

        }

        private bool IsWithingTermDates(string p_sEffectiveDate, string p_sExpirationDate)
        {
            if (Conversion.ConvertStrToInteger(p_sExpirationDate) - Conversion.ConvertStrToInteger(p_sEffectiveDate) > 0)
                return true;
            else
                return false;
        }

        private void CalculatePremium(int p_iTransNumber, PremiumCalculationUI p_structPremiumCalculationUI, ref PremiumAmounts p_structPremiumAmounts, EnhancePolicyUI p_structEnhancePolicyUI)
        {
            DbReader objReader = null;
            string sTransType = string.Empty;
            string sSQL = string.Empty;
            string sEffectiveDate = string.Empty;
            int iTermNumber = 0;
            string sOtherTransType = string.Empty;
            string sPrevTransEffDate = string.Empty;
            string sPrevTransExpDate = string.Empty;
            double dblPrevTotalEstimatedPremium = 0;
            double dblPrevTotalBilledPremium = 0;
            double dblPrevAuditPremium = 0;
            //Shruti for 11746
            double dblFirstTransEstimatedPrem = 0;
            //Shruti for 11746 ends
            DbReader objReader2 = null;
            bool bTierFound = true;
            int iPrevTierLevel = 1;
            double dblTotalDiscountForTier = 0;
            bool bIsLastDiscount = false;
            bool bLastTierFound = false;
            Discount structLastDiscount = new Discount();
            DiscountTier structLastDiscountTier = new DiscountTier();
            int iDiscountRowId = 0;//pmahli Volume DIscount 9/12/2007   
            bool bUseVolDisc = false;//pmahli Volume Discount 9/12/2007 
            //npadhy RMSC retrofit Starts
            bool bUseTax = false;
            double dTax = 0;
            double dTaxRate = 0;
            double dTranTax = 0;
            long lTaxSign = 0;
            string sTaxType = string.Empty;
            double dPrevTax;
            double dPrevExpConst = 0;
            double dPreTaxAmount = 0;
            double dPrevTranTax = 0;
            int intTaxType = 0;
            //npadhy Code Changes for Adjustment of .01
            double dAdjFactor;
            bUseTax = false;
            dTax = 0;
            dTaxRate = 0;
            lTaxSign = 0;
            sTaxType = "";
            dPrevTax = 0;
            dPrevExpConst = 0;
            //npadhy Assigned Zero to Adjustment Factor
            dAdjFactor = 0;

            //npadhy RMSC retrofit Ends

            bool blnSuccess = false;
            bool bFound = false;

            bool bWaivePremium = p_structPremiumCalculationUI.WaivePremiumSelected;

            if (PolicyEnh.PolicyXTransEnhList.Count > 0)
            {
                foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                    {
                        sTransType = base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionType);
                        iTermNumber = objPolicyXTransEnh.TermNumber;
                        break;
                    }
                }

                // Find the Term Effective Date
                sSQL = " SELECT EFFECTIVE_DATE FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId
                    + " AND TERM_NUMBER = " + iTermNumber + " AND SEQUENCE_ALPHA = 1 ";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    sEffectiveDate = objReader.GetString("EFFECTIVE_DATE");
                objReader.Close();
            }
            else
            {
                // means is quote
                if (PolicyEnh.PolicyXTermEnhList.Count > 0)
                {
                    foreach (object objTemp in PolicyEnh.PolicyXTermEnhList)
                    {
                        sEffectiveDate = ((PolicyXTermEnh)objTemp).EffectiveDate;
                        break;
                    }
                }
                else
                    return;
            }

            sSQL = String.Concat(" SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = ", PolicyEnh.PolicyId, " AND TERM_NUMBER = ", iTermNumber
                , " AND TRANSACTION_ID < ", p_iTransNumber, " ORDER BY TRANSACTION_ID DESC");

            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            while (objReader.Read())
            {

                sOtherTransType = base.LocalCache.GetShortCode(Conversion.CastToType<int>(Conversion.ConvertObjToStr(objReader.GetValue("TRANSACTION_TYPE")), out blnSuccess));
                if (sTransType != "AU" && !bFound)
                {
                    if (sOtherTransType != "AU")
                    {
                        sPrevTransEffDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                        sPrevTransExpDate = Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"));
                        bFound = true;
                    }
                }
            }

            for (int i = 0; i < 2; i++)
            {
                CalculatePremium(p_iTransNumber, ref p_structPremiumCalculationUI, ref p_structPremiumAmounts, p_structEnhancePolicyUI, i, ref structLastDiscountTier, ref bIsLastDiscount, ref bLastTierFound, ref dblTotalDiscountForTier, sPrevTransEffDate);//MITS 25291 rsushilaggar Date 06/22/2011
            }

            #region Calculate premium
            //while (bTierFound)
                //{
                //    bTierFound = false;
                //    dblTotalDiscountForTier = 0;




                //    this.CalculateDeductiblePremium(ref p_structPremiumAmounts, p_iTransNumber, p_structEnhancePolicyUI, iPrevTierLevel, structLastDiscountTier.Amount, sPrevTransEffDate);
                //    for (int iIndex = 0; iIndex < p_structPremiumAmounts.DiscountList.Count; iIndex++)
                //    {
                //        Discount structDiscount = (Discount)p_structPremiumAmounts.DiscountList[iIndex];

                //        if (structDiscount.Level == iPrevTierLevel)
                //        {
                //            bIsLastDiscount = true;
                //            structLastDiscount = structDiscount;
                //            //pmahli Volume Discount 9/12/2007 -start
                //            //To get the value of Volume Discount Flag
                //            iDiscountRowId = structDiscount.NameId;
                //            sSQL = " SELECT * FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = " + iDiscountRowId;
                //            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                //            if (objReader.Read())
                //            {
                //                bUseVolDisc = Conversion.ConvertObjToBool(objReader.GetValue("USE_VOLUME_DISCOUNT"));
                //            }
                //            objReader.Close();
                //            //pmahli Volume Discount 9/12/2007 -End
                //            if (iPrevTierLevel == 1)
                //            {
                //                if (structDiscount.Type == "P")
                //                {
                //                    //pmahli Volume Discount 9/12/2007 -start
                //                    if (bUseVolDisc)
                //                    {
                //                        structDiscount.Amount = CalculateVolumeDisocunt(iDiscountRowId, p_structPremiumAmounts.ModifiedPremium);
                //                        structDiscount.AmountFactor = structDiscount.Amount;
                //                        //psharma41: Mits 11131
                //                        structDiscount.Amount = structDiscount.Amount * (-1);
                //                    }
                //                    //pmahli Volume Discount 9/12/2007 - End
                //                    else
                //                        //psharma41: Mits 11131
                //                    {
                //                        //structDiscount.Amount = p_structPremiumAmounts.ModifiedPremium * (structDiscount.AmountFactor / 100);
                //                        structDiscount.Amount = p_structPremiumAmounts.ModifiedPremium * (structDiscount.AmountFactor / 100) * (-1);
                //                    }
                //                }
                //                else
                //                {
                //                    if (p_structPremiumAmounts.ModifiedPremium != 0)
                //                        //psharma41: Mits 11131
                //                        //structDiscount.Amount = structDiscount.AmountFactor;
                //                    {
                //                        structDiscount.Amount = structDiscount.AmountFactor * (-1);
                //                    }
                //                    else
                //                        structDiscount.Amount = 0;
                //                }
                //            }
                //            else
                //            {
                //                if (structDiscount.Type == "P")
                //                {
                //                    //pmahli Volume Discount 9/12/2007 -Start
                //                    if (bUseVolDisc)
                //                    {
                //                        structDiscount.Amount = CalculateVolumeDisocunt(iDiscountRowId, structLastDiscountTier.Amount);
                //                        structDiscount.AmountFactor = structDiscount.Amount;
                //                        //psharma41: Mits 11131
                //                        structDiscount.Amount = structDiscount.Amount * (-1);
                //                    }
                //                    //pmahli Volume Discount 9/12/2007 -End
                //                    else
                //                        //psharma41: Mits 11131
                //                        //structDiscount.Amount = structLastDiscountTier.Amount * (structDiscount.AmountFactor / 100);
                //                        structDiscount.Amount = structLastDiscountTier.Amount * (structDiscount.AmountFactor / 100) * (-1);
                //                }
                //                else
                //                {
                //                    if (structLastDiscountTier.Amount != 0)
                //                        //psharma41: Mits 11131
                //                        //structDiscount.Amount = structDiscount.AmountFactor;
                //                        structDiscount.Amount = structDiscount.AmountFactor * (-1);
                //                    else
                //                        structDiscount.Amount = 0;
                //                }
                //            }
                //            if (base.RoundFlag)
                //                structDiscount.Amount = Conversion.Round(structDiscount.Amount, 0);
                //            else
                //                structDiscount.Amount = Conversion.Round(structDiscount.Amount, 2);

                //            //psharma41 : Mits 11131
                //            //dblTotalDiscountForTier += structDiscount.Amount;
                //            dblTotalDiscountForTier = dblTotalDiscountForTier - structDiscount.Amount;

                //            p_structPremiumAmounts.DiscountList[iIndex] = structDiscount;
                //        }
                //    }

                //    // now that we've applied all the discounts for that level, calc the amount of the next disc tier
                //    for (int iIndex = 0; iIndex < p_structPremiumAmounts.DiscountTierList.Count; iIndex++)
                //    {
                //        DiscountTier structDiscountTier = (DiscountTier)p_structPremiumAmounts.DiscountTierList[iIndex];
                //        if (structDiscountTier.LevelId == iPrevTierLevel)
                //        {
                //            if (iPrevTierLevel == 1)
                //            {
                //                //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                //                //structDiscountTier.Amount = p_structPremiumAmounts.ModifiedPremium - dblTotalDiscountForTier;
                //                structDiscountTier.Amount = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                //                //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                //            }
                //            else
                //            {
                //                //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
                //                if (PolicyEnh.PolicyXCvgEnhList.Count > 0)
                //                {
                //                    structDiscountTier.Amount = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                //                }
                //                else
                //                {
                //                    structDiscountTier.Amount = structLastDiscountTier.Amount - dblTotalDiscountForTier;
                //                }
                //                //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
                //            }
                //            if (base.RoundFlag)
                //                structDiscountTier.Amount = Conversion.Round(structDiscountTier.Amount, 0);
                //            else
                //                structDiscountTier.Amount = Conversion.Round(structDiscountTier.Amount, 2);

                //            p_structPremiumAmounts.DiscountTierList[iIndex] = structDiscountTier;
                //            break;
                //        }
                //    }

                //    // Now find the next level
                //    foreach (DiscountTier structDiscountTier in p_structPremiumAmounts.DiscountTierList)
                //    {
                //        if (structDiscountTier.LevelId == iPrevTierLevel)
                //        {
                //            bTierFound = true;
                //            bIsLastDiscount = false;
                //            bLastTierFound = true;
                //            structLastDiscountTier = structDiscountTier;
                //            iPrevTierLevel = structDiscountTier.NameId;
                //            break;
                //        }
                //    }
                //}
            /********************************************************************/
            #endregion
            // Calculate the final amounts
                if (p_structPremiumAmounts.DiscountList.Count != 0 || p_structPremiumAmounts.DiscountTierList.Count != 0)
                {
                    if (!bIsLastDiscount)
                    {
                        // Grab the last tier amt
                        //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
                        if (PolicyEnh.PolicyXCvgEnhList.Count > 0)
                        {
                            p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.TotalModifier;
                        }
                        else
                        {
                            p_structPremiumAmounts.FinalPremium = structLastDiscountTier.Amount;
                        }
                        //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
                        //Commented by Anu Tennyson for MITS 18229
                        //p_structPremiumAmounts.FinalPremium = structLastDiscountTier.Amount;
                        //Commented by Anu Tennyson for MITS 18229 
                    }
                    else
                    {
                        if (bLastTierFound)
                        {
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
                            if (PolicyEnh.PolicyXCvgEnhList.Count > 0)
                            {
                                p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                            }
                            else
                            {
                                p_structPremiumAmounts.FinalPremium = structLastDiscountTier.Amount - dblTotalDiscountForTier;
                            }
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
                            //Commneted by Anu Tennyson
                            //p_structPremiumAmounts.FinalPremium = structLastDiscountTier.Amount - dblTotalDiscountForTier;
                            //Commneted by Anu Tennyson
                        }
                        else
                        {
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                            //p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.ModifiedPremium - dblTotalDiscountForTier;
                            p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                        }
                    }
                }
                else
                {
                    //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                    //p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.ModifiedPremium;
                    p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.TotalModifier;
                    //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                }

            // See If Audit.
            double dblEstiamtedPremium = 0;
            if (sTransType != "AU")
            {
                if (p_structPremiumAmounts.FinalPremium < 0)
                    p_structPremiumAmounts.EstimatedPremium = 0;
                else
                    p_structPremiumAmounts.EstimatedPremium = p_structPremiumAmounts.FinalPremium;
                dblEstiamtedPremium = p_structPremiumAmounts.EstimatedPremium;
            }
            else
            {
                if (p_structPremiumAmounts.FinalPremium < 0)
                    p_structPremiumAmounts.AuditedPremium = 0;
                else
                    p_structPremiumAmounts.AuditedPremium = p_structPremiumAmounts.FinalPremium;
                dblEstiamtedPremium = p_structPremiumAmounts.AuditedPremium;
            }

            // If there is a minumum billed threshhold
            bool bMinBillApplies = false;
            double dblMinimumBillAmount = 0;
            //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
            sSQL = " SELECT * FROM SYS_POL_BILL_THRSH WHERE LINE_OF_BUSINESS=" + PolicyEnh.PolicyType
                    + " AND STATE=" + PolicyEnh.State + " AND NOT IN_USE_FLAG = 0"
                    + " AND EFFECTIVE_DATE <= '" + sEffectiveDate
                    + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE='' OR EXPIRATION_DATE >= '" + sEffectiveDate + "')";
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                bMinBillApplies = true;
                dblMinimumBillAmount = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), base.ClientId);
            }
            objReader.Close();

            // If there is an expense constant
            double dblExpenseConstant = 0;
            //Umesh
            string sFlatorPercent = "";
            //Mukul(6/20/07) MITS 9789 Expiration date checked for blank value
            sSQL = " SELECT * FROM SYS_POL_EXP_CONST WHERE LINE_OF_BUSINESS=" + PolicyEnh.PolicyType
                + " AND STATE=" + PolicyEnh.State + " AND NOT IN_USE_FLAG = 0"
                + " AND EFFECTIVE_DATE <= '" + sEffectiveDate
                + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE='' OR EXPIRATION_DATE >= '" + sEffectiveDate + "')";
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                dblExpenseConstant = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), base.ClientId);
                sFlatorPercent = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));
                //pmahli MITS 9275 Added Expense Constant
                p_structPremiumAmounts.ExpenseConstant = dblExpenseConstant;
            }
            objReader.Close();

            //npadhy RMSC retrofit Starts
            sSQL = "SELECT * FROM SYS_POL_TAX WHERE LINE_OF_BUSINESS=" + PolicyEnh.PolicyType + " AND STATE=" + PolicyEnh.State + " AND NOT IN_USE_FLAG = 0"
                   + " AND EFFECTIVE_DATE <= '" + sEffectiveDate + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + sEffectiveDate + "')";
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                dTaxRate = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), base.ClientId);
                intTaxType = Conversion.ConvertObjToInt(objReader.GetValue("FLAT_OR_PERCENT"), base.ClientId);
                sTaxType = base.LocalCache.GetShortCode(intTaxType);
                bUseTax = true;
                p_structPremiumAmounts.TaxRate = dTaxRate;
                p_structPremiumAmounts.TaxFlatOrPercent = intTaxType;
            }
            else
            {
                dTaxRate = 0;
                bUseTax = false;
            }
            objReader.Close();
            //npadhy RMSC retrofit Ends

            // Determine the transaction prem amount
            if (PolicyEnh.PolicyXTransEnhList.Count == 0)
            {
                // Quote
                p_structPremiumAmounts.TransactionalPremium = dblEstiamtedPremium;
                if (bMinBillApplies && dblEstiamtedPremium < dblMinimumBillAmount)
                {
                    if (sFlatorPercent == "P")
                    {
                        //npadhy RMSC retrofit Starts
                        if (bUseTax && sTaxType.Trim() == "P")
                        {
                            dTax = Math.Round((dblMinimumBillAmount + (dblMinimumBillAmount * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            dTranTax = dTax;
                        }
                        else
                        {
                            dTax = dTaxRate;
                            dTranTax = dTaxRate;
                        }
                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblMinimumBillAmount * (dblExpenseConstant / 100)) + dTax;
                        p_structPremiumAmounts.TransactionTax = dTranTax;
                        p_structPremiumAmounts.Tax = dTax;
                    }
                    else
                    {
                        if (bUseTax && sTaxType.Trim() == "P")
                        {
                            dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            dTranTax = dTax;

                        }
                        else
                        {
                            dTax = dTaxRate;
                            dTranTax = dTaxRate;
                        }
                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                        p_structPremiumAmounts.TransactionTax = dTranTax;
                        p_structPremiumAmounts.Tax = dTax;
                    }
                }
                else
                {
                    if (sFlatorPercent == "P")
                    {
                        if (bUseTax && sTaxType.Trim() == "P")
                        {
                            dTax = Math.Round((dblEstiamtedPremium + (dblEstiamtedPremium * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            dTranTax = dTax;
                        }
                        else
                        {
                            dTax = dTaxRate;
                            dTranTax = dTaxRate;
                        }
                        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblEstiamtedPremium * (dblExpenseConstant / 100)) + dTax;
                        p_structPremiumAmounts.TransactionTax = dTranTax;
                        p_structPremiumAmounts.Tax = dTax;
                    }
                    else
                    {
                        if (bUseTax && sTaxType.Trim() == "P")
                        {
                            dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            dTranTax = dTax;

                        }
                        else
                        {
                            dTax = dTaxRate;
                            dTranTax = dTaxRate;
                        }
                        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                        p_structPremiumAmounts.TransactionTax = dTranTax;
                        p_structPremiumAmounts.Tax = dTax;
                    }
                }
                p_structPremiumAmounts.ChgBilledPremium = 0;

                //MITS 9885 : Umesh
                if (base.RoundFlag)
                {
                    p_structPremiumAmounts.TotalBilledPremium = Math.Round(p_structPremiumAmounts.TotalBilledPremium, 0, MidpointRounding.AwayFromZero);
                    p_structPremiumAmounts.TransactionTax = Math.Round(p_structPremiumAmounts.TransactionTax, 0, MidpointRounding.AwayFromZero);
                    p_structPremiumAmounts.Tax = Math.Round(p_structPremiumAmounts.Tax, 0, MidpointRounding.AwayFromZero);
                }
                else
                {
                    p_structPremiumAmounts.TotalBilledPremium = Math.Round(p_structPremiumAmounts.TotalBilledPremium, 2, MidpointRounding.AwayFromZero);
                    p_structPremiumAmounts.TransactionTax = Math.Round(p_structPremiumAmounts.TransactionTax, 2, MidpointRounding.AwayFromZero);
                    p_structPremiumAmounts.Tax = Math.Round(p_structPremiumAmounts.Tax, 2, MidpointRounding.AwayFromZero);
                }
                //npadhy RMSC retrofit Ends

                return;
            }
            else
            {
                bFound = false;


                if (!(sTransType == "NB" || sTransType == "RN"))
                {
                    sSQL = " SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + PolicyEnh.PolicyId
                        + " AND TERM_NUMBER=" + iTermNumber + " ORDER BY TRANSACTION_ID DESC";

                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        if (Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), base.ClientId) != p_iTransNumber)
                        {
                            sOtherTransType = base.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), base.ClientId));
                            //Shruti for 11746
                            if (sOtherTransType == "NB" || sOtherTransType == "RN")
                            {
                                dblFirstTransEstimatedPrem = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_EST_PREMIUM"), base.ClientId);
                            }
                            //Shruti for 11746 ends
                            if (sTransType != "AU" && !bFound)
                            {
                                if (sOtherTransType != "AU")
                                {
                                    // npadhy MITS 16277. The Transaction premium should be obtained by subtracting the two estimated premiums 
                                    // Current and Previous
                                    //if (sTransType != "CF")
                                    p_structPremiumAmounts.TransactionalPremium = p_structPremiumAmounts.EstimatedPremium - Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_EST_PREMIUM"), base.ClientId);
                                    //else
                                    //    p_structPremiumAmounts.TransactionalPremium = p_structPremiumAmounts.EstimatedPremium - Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_BILLED_PREM"));

                                    dblPrevTotalEstimatedPremium = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_EST_PREMIUM"), base.ClientId);

                                    dblPrevTotalBilledPremium = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_BILLED_PREM"), base.ClientId);
                                    //npadhy RMSC retrofit Starts
                                    dPrevTax = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_AMOUNT"), base.ClientId);
                                    dPrevTranTax = Conversion.ConvertObjToDouble(objReader.GetValue("TRAN_TAX"), base.ClientId);
                                    //npadhy RMSC retrofit Ends

                                    bFound = true;
                                }
                            }
                            else if (!bFound)
                            {
                                // Processing an audit
                                if (sOtherTransType != "AU")
                                {
                                    sSQL = " SELECT SUM(TRANSACTION_PREM) FROM POLICY_X_RTNG_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId
                                        + " AND TERM_NUMBER = " + iTermNumber
                                        + " AND TRANSACTION_ID <> " + p_iTransNumber;
                                    objReader2 = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                                    if (objReader2.Read())
                                    {
                                        p_structPremiumAmounts.EstimatedPremium = Conversion.ConvertObjToDouble(objReader2.GetValue(0), base.ClientId);
                                        dblPrevTotalEstimatedPremium = p_structPremiumAmounts.EstimatedPremium;
                                    }
                                    objReader2.Close();
                                    p_structPremiumAmounts.TransactionalPremium = p_structPremiumAmounts.AuditedPremium - p_structPremiumAmounts.EstimatedPremium;
                                    dblPrevTotalBilledPremium = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_BILLED_PREM"), base.ClientId);
                                    //npadhy RMSC retrofit Starts
                                    dPrevTax = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_AMOUNT"), base.ClientId);
                                    //npadhy RMSC retrofit Ends
                                }
                                else
                                {
                                    sSQL = " SELECT * FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID=" + Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_ID"), base.ClientId);
                                    objReader2 = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                                    if (objReader2.Read())
                                    {
                                        p_structPremiumAmounts.EstimatedPremium = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_EST_PREMIUM"), base.ClientId);
                                        p_structPremiumAmounts.TransactionalPremium = p_structPremiumAmounts.AuditedPremium - Conversion.ConvertObjToDouble(objReader2.GetValue("AUDITED_PREMIUM"), base.ClientId);
                                        //pmahli MITS 10006
                                        //in dblPrevAuditPremium, TOTAL_EST_PREMIUM was being assigned
                                        dblPrevAuditPremium = Conversion.ConvertObjToDouble(objReader2.GetValue("AUDITED_PREMIUM"), base.ClientId);
                                        dblPrevTotalEstimatedPremium = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_EST_PREMIUM"), base.ClientId);
                                        dblPrevTotalBilledPremium = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_BILLED_PREM"), base.ClientId);
                                        //npadhy RMSC retrofit Starts
                                        dPrevTax = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_AMOUNT"), base.ClientId);
                                        dPrevTranTax = Conversion.ConvertObjToDouble(objReader.GetValue("TRAN_TAX"), base.ClientId);
                                        //npadhy RMSC retrofit Starts
                                    }
                                    objReader2.Close();
                                }
                                bFound = true;
                            }
                        }
                    }
                    objReader.Close();
                }
                else
                {
                    // if this is new bus or renewal, txn amt is est prem amt
                    p_structPremiumAmounts.TransactionalPremium = p_structPremiumAmounts.EstimatedPremium;
                }
            }

            switch (sTransType)
            {
                // NEW BUSINESS, RENEWAL PREMIUM CALC
                case "NB":
                case "RN":
                    if (bMinBillApplies && p_structPremiumAmounts.TransactionalPremium < dblMinimumBillAmount)
                    {
                        if (sFlatorPercent == "P")
                        {
                            //npadhy RMSC retrofit Starts
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblMinimumBillAmount + (dblMinimumBillAmount * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = dTax;
                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = dTaxRate;
                            }
                            p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblMinimumBillAmount * (dblExpenseConstant / 100)) + dTax;
                            p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblMinimumBillAmount * (dblExpenseConstant / 100)) + dTax;
                            p_structPremiumAmounts.TransactionTax = dTranTax;
                            p_structPremiumAmounts.Tax = dTax;
                        }
                        else
                        {
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = dTax;

                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = dTaxRate;
                            }
                            p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                            p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                            p_structPremiumAmounts.TransactionTax = dTranTax;
                            p_structPremiumAmounts.Tax = dTax;
                        }
                    }
                    else
                    {
                        if (sFlatorPercent == "P")
                        {
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblEstiamtedPremium + (dblEstiamtedPremium * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = dTax;
                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = dTaxRate;
                            }
                            p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + (p_structPremiumAmounts.TransactionalPremium * (dblExpenseConstant / 100)) + dTranTax;
                            p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + (p_structPremiumAmounts.TransactionalPremium * (dblExpenseConstant / 100)) + dTranTax;
                            p_structPremiumAmounts.TransactionTax = dTranTax;
                            p_structPremiumAmounts.Tax = dTax;
                        }
                        else
                        {
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = dTax;
                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = dTaxRate;

                            }
                            p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant + dTranTax;
                            p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant + dTranTax;
                            p_structPremiumAmounts.TransactionTax = dTranTax;
                            p_structPremiumAmounts.Tax = dTax;
                            //npadhy RMSC retrofit Ends
                        }
                    }
                    break;

                // FLAT CANCELLATION PREMIUM CALC
                case "CF":

                    //Shruti for MITS 11746
                    //p_structPremiumAmounts.EstimatedPremium = 0;
                    //p_structPremiumAmounts.ChgBilledPremium = 0 - dblPrevTotalBilledPremium;
                    //p_structPremiumAmounts.TotalBilledPremium = 0;

                    if (sFlatorPercent == "P")
                    {
                        p_structPremiumAmounts.EstimatedPremium = 0;
                        p_structPremiumAmounts.TotalBilledPremium = dblFirstTransEstimatedPrem * (dblExpenseConstant / 100);
                        p_structPremiumAmounts.ChgBilledPremium = dblFirstTransEstimatedPrem * (dblExpenseConstant / 100) - dblPrevTotalBilledPremium;
                    }
                    else
                    {
                        p_structPremiumAmounts.EstimatedPremium = 0;
                        p_structPremiumAmounts.ChgBilledPremium = dblExpenseConstant - dblPrevTotalBilledPremium;
                        p_structPremiumAmounts.TotalBilledPremium = dblExpenseConstant;
                    }

                    // npadhy Start MITS 18950 When the Policy is cancelled Flat, we need to keep the Tax for expense constant as well.
                    if (bUseTax && sTaxType == "P")
                    {
                        dTax = Math.Round(p_structPremiumAmounts.TotalBilledPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                        dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                    }
                    else
                    {
                        dTax = dTaxRate;
                        dTranTax = dTaxRate;
                    }
                    p_structPremiumAmounts.TotalBilledPremium += dTax;
                    p_structPremiumAmounts.ChgBilledPremium += dTax;
                    // npadhy End MITS 18950 When the Policy is cancelled Flat, we need to keep the Tax for expense constant as well.

                    //npadhy RMSC retrofit Starts
                    p_structPremiumAmounts.Tax = 0;
                    p_structPremiumAmounts.TransactionTax = dTranTax;

                    // npadhy Start MITS 18938, 18941 The transaction premium is already calculated and is negative and This calculation 
                    // will make it positive. And the Tax need not to be deducted from the Transaction Premium
                    // p_structPremiumAmounts.TransactionalPremium = dblEstiamtedPremium - (p_structPremiumAmounts.TransactionalPremium - dPrevTax);
                    // npadhy End MITS 18938, 18941, 18950 The transaction premium is already calculated and is negative and This calculation 
                    // will make it positive. And the Tax need not to be deducted from the Transaction Premium

                    //npadhy RMSC retrofit Ends

                    break;

                //PRO-RATA CANCELLATION
                case "CPR":
                    //Mukul(6/27/2007)  Changes revert.  
                    // Start Naresh MITS 9845 Keep the Expense Constant while cancelling Pro Rata
                    if (bMinBillApplies)
                    {
                        if (dblPrevTotalEstimatedPremium > dblMinimumBillAmount)
                        {
                            if (dblEstiamtedPremium < dblMinimumBillAmount)
                            {
                                if (sFlatorPercent == "P")
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Conversion.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2);
                                        dTranTax = Conversion.Round(((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100)), 2);

                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = dTaxRate;
                                    }
                                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount - dblPrevTotalBilledPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                else
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round(((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100)), 2, MidpointRounding.AwayFromZero);

                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = dTaxRate;
                                    }
                                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount - dblPrevTotalBilledPremium + dblExpenseConstant + dTax;
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                            }
                            else
                            {
                                //********
                                if (sFlatorPercent == "P")
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round(((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100)), 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = dTaxRate;
                                    }
                                    // npadhy MITS 19168 Changes for CPR. The Total Billed Prem should be added with Tax and not Transaction Tax
                                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    // npadhy MITS 19168 Changes for CPR. The Total Billed Prem should be added with Tax and not Transaction Tax
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                else
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round(((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100)), 2, MidpointRounding.AwayFromZero);

                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = dTaxRate;
                                    }
                                    //Chnages done for MITS 22976 : start
                                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + dblExpenseConstant + dTax;
                                    //Chnages done for MITS 22976 : end
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                //********
                            }
                        }
                        else
                        {
                            // npadhy Invalid Test Case, When the Prev Total Estimated Prem is less than Minimum Bill Threshold, After Cancelling rge Policy the Estimated Premium will never be > Minimum Bill Threshold
                            if (dblEstiamtedPremium > dblMinimumBillAmount)
                            {
                                //********
                                if (sFlatorPercent == "P")
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = 0;
                                    }
                                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                else
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = 0;
                                    }
                                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + dblExpenseConstant + dTax;
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                //********
                            }
                            else
                            { //************
                                if (sFlatorPercent == "P")
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100)), 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = 0;
                                    }
                                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount - dblPrevTotalBilledPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                else
                                {
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100)), 2, MidpointRounding.AwayFromZero);
                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = 0;
                                    }
                                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount - dblPrevTotalBilledPremium + dblExpenseConstant + dTax;
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                            } //****************
                        }
                    }
                    else
                    {
                        if (sFlatorPercent == "P")
                        {
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = 0;
                            }
                            p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                            p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                            p_structPremiumAmounts.Tax = dTax;
                            p_structPremiumAmounts.TransactionTax = dTranTax;
                        }
                        else
                        {
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = 0;
                            }
                            p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                            p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + dblExpenseConstant + dTax;
                            p_structPremiumAmounts.Tax = dTax;
                            p_structPremiumAmounts.TransactionTax = dTranTax;
                        }
                    }
                    //*************************************************************
                    //npadhy Retrofitted the RMSC changes. Removed the Redundant Code of lines
                    //if (bMinBillApplies && dblEstiamtedPremium < dblMinimumBillAmount)
                    //{
                    //    if (sFlatorPercent == "P")
                    //    {
                    //        //Shruti - As discussed with Manish Shewani expense constant is calculated on the basis of previous premium.
                    //        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount - dblPrevTotalBilledPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //    }
                    //    else
                    //    {
                    //        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant;
                    //        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount - dblPrevTotalBilledPremium + dblExpenseConstant;
                    //    }
                    //}
                    //else
                    //{
                    //    if (sFlatorPercent == "P")
                    //    {
                    //        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //        p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //    }
                    //    else
                    //    {
                    //        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant;
                    //        p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium - dblPrevTotalBilledPremium + dblExpenseConstant;
                    //    }
                    //}
                    // End Naresh MITS 9845 Keep the Expense Constant while cancelling Pro Rata
                    break;

                // REINSTATEMENT PREMIUM CALC - consider min bill as well, no exp const
                case "RL":
                case "RNL":
                    if (bMinBillApplies)
                    {
                        if (dblEstiamtedPremium < dblMinimumBillAmount)
                        {
                            if (sFlatorPercent == "P")
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    dTax = Math.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dTranTax = dTax;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                }
                                p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                p_structPremiumAmounts.Tax = dTax;
                                p_structPremiumAmounts.TransactionTax = dTranTax;
                            }
                            else
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dTranTax = dTax;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                }
                                p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                                p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                p_structPremiumAmounts.Tax = dTax;
                                p_structPremiumAmounts.TransactionTax = dTranTax;
                            }
                        }
                        else
                        {
                            if (dblPrevTotalEstimatedPremium < dblMinimumBillAmount)
                            {
                                //Animesh Inserted RMSC 
                                //***************************
                                if (dblEstiamtedPremium > dblMinimumBillAmount)
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                }
                                else
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                }
                            }
                            else
                            {
                                if (dblEstiamtedPremium > dblMinimumBillAmount)
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Conversion.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2);
                                            dTranTax = Conversion.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        //Deb MITS: 25276
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalEstimatedPremium + p_structPremiumAmounts.TransactionalPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TotalBilledPremium - dblPrevTotalBilledPremium;
                                        //Deb MITS: 25276
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        // npadhy Start MITS 23088 Premium Calculation in case of Reinstate with and without lapse
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalEstimatedPremium + p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant + dTax;
                                        // npadhy End MITS 23088 Premium Calculation in case of Reinstate with and without lapse
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TotalBilledPremium - dblPrevTotalBilledPremium;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;

                                    }
                                }
                                else
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TotalBilledPremium - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (sOtherTransType == "CF")
                        {
                            if (sFlatorPercent == "P")
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    //Start(09/29/2010): Sumit - MITS# 22384 - Bracket ordering corrected.
                                    dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    //End: Sumit
                                    dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax + dAdjFactor;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                //Start(09/29/2010): Sumit - MITS# 22384
                                p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax + dAdjFactor;
                                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax + dAdjFactor;
                                //End: Sumit
                                p_structPremiumAmounts.Tax = dTax;
                            }
                            else
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax + dAdjFactor;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.Tax = dTax;
                            }
                        }
                        else
                        {
                            if (sFlatorPercent == "P")
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    //Start(09/29/2010): Sumit - MITS# 22384 - Bracket ordering corrected.
                                    dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    //End:Sumit
                                    dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax + dAdjFactor;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                //Start(09/29/2010): Sumit - MITS# 22384 
                                p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.EstimatedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax + dAdjFactor;
                                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TotalBilledPremium - dblPrevTotalBilledPremium + dAdjFactor;
                                //End:Sumit
                                p_structPremiumAmounts.Tax = dTax;
                            }
                            else
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax + dAdjFactor;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblPrevTotalBilledPremium + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.Tax = dTax;
                            }
                        }
                    }
                    //***************************Animesh Commented for RMSC 
                    //if (bMinBillApplies)
                    //{
                    //    if (dblEstiamtedPremium < dblMinimumBillAmount)
                    //    {
                    //        if (sFlatorPercent == "P")
                    //        {
                    //            p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //            p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                    //        }
                    //        else
                    //        {
                    //            p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant;
                    //            p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (dblPrevTotalEstimatedPremium < dblMinimumBillAmount)
                    //        {
                    //            if (sFlatorPercent == "P")
                    //            {
                    //                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalEstimatedPremium + dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //                p_structPremiumAmounts.ChgBilledPremium = dblPrevTotalEstimatedPremium + dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                    //            }
                    //            else
                    //            {
                    //                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalEstimatedPremium + dblEstiamtedPremium + dblExpenseConstant;
                    //                p_structPremiumAmounts.ChgBilledPremium = dblPrevTotalEstimatedPremium + dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium;
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (sFlatorPercent == "P")
                    //            {
                    //                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalEstimatedPremium + p_structPremiumAmounts.TransactionalPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));

                    //                // npadhy The Change in Billed Premium Should be calculated as Current Total Billed Premium - Prev Total Billed Prem
                    //                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TotalBilledPremium - dblPrevTotalBilledPremium;
                    //            }
                    //            else
                    //            {
                    //                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalEstimatedPremium + p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant;

                    //                // npadhy The Change in Billed Premium Should be calculated as Current Total Billed Premium - Prev Total Billed Prem
                    //                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TotalBilledPremium - dblPrevTotalBilledPremium;
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    if (sOtherTransType == "CF")
                    //    {
                    //        if (sFlatorPercent == "P")
                    //        {
                    //            p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //            p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //        }
                    //        else
                    //        {
                    //            p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant;
                    //            p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblExpenseConstant;

                    //        }
                    //    }
                    //    else
                    //    {
                    //        //pmahli MITS 9968
                    //        //case added when sFlatorPercent is percentage
                    //        if (sFlatorPercent == "P")
                    //        {
                    //            p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.EstimatedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //            p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TotalBilledPremium - dblPrevTotalBilledPremium;
                    //        }
                    //        else
                    //        {
                    //            p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TransactionalPremium + dblPrevTotalBilledPremium;
                    //            p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium;
                    //        }
                    //    }
                    //}
                    break;
                // ENDORSEMENT CALC
                case "EN":
                    if (bWaivePremium)
                    {
                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                        p_structPremiumAmounts.ChgBilledPremium = 0;
                        //RMSC Animesh Inserted 
                        if (bUseTax && sTaxType == "P")
                        {
                            dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            dTax = dTaxRate;
                            dTranTax = 0;
                        }

                        p_structPremiumAmounts.Tax = dTax;
                        p_structPremiumAmounts.TransactionTax = dTranTax;
                        //RMSC 
                    }
                    else
                    {
                        if (bMinBillApplies)
                        {
                            if (dblPrevTotalEstimatedPremium > dblMinimumBillAmount)
                            {
                                if (dblEstiamtedPremium < dblMinimumBillAmount)
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;

                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                }
                                else
                                {
                                    //RMSC 
                                    //*****************
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;


                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax;
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;

                                        }
                                    }
                                    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax;
                                    p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax;
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                    //*****************Animesh Commented RMSC 
                                    //p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                                    //p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium;
                                }
                            }
                            else
                            {
                                //means prev bill amt = min bill amt
                                if (dblEstiamtedPremium > dblMinimumBillAmount)
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        //RMSC
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTranTax;
                                    }
                                }
                                else
                                {
                                    //RMSC 
                                    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                                    p_structPremiumAmounts.ChgBilledPremium = 0;
                                    if (bUseTax && sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = 0;
                                    }
                                    p_structPremiumAmounts.Tax = dTax;
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                            }
                        }
                        else
                        {
                            //RMSC
                            if (sFlatorPercent == "P")
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax + dAdjFactor;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.Tax = dTax;
                            }
                            else
                            {
                                if (bUseTax && sTaxType == "P")
                                {
                                    dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dTranTax = Math.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);

                                    p_structPremiumAmounts.TransactionTax = dTranTax + dAdjFactor;
                                }
                                else
                                {
                                    dTax = dTaxRate;
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TransactionTax = dTranTax;
                                }
                                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                p_structPremiumAmounts.Tax = dTax;
                            }
                            //*****************
                            //RMSC
                            //p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                            //p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium;
                        }
                    }
                    //if (bWaivePremium)
                    //{
                    //    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                    //    p_structPremiumAmounts.ChgBilledPremium = 0;
                    //}
                    //else
                    //{
                    //    if (bMinBillApplies)
                    //    {
                    //        if (dblPrevTotalEstimatedPremium > dblMinimumBillAmount)
                    //        {
                    //            if (dblEstiamtedPremium < dblMinimumBillAmount)
                    //            {
                    //                if (sFlatorPercent == "P")
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                    //                }
                    //                else
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant;
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium;
                    //                }
                    //            }
                    //            else
                    //            {
                    //                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                    //                p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium;
                    //            }
                    //        }
                    //        else
                    //        {
                    //            //means prev bill amt = min bill amt
                    //            if (dblEstiamtedPremium > dblMinimumBillAmount)
                    //            {
                    //                if (sFlatorPercent == "P")
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                    //                }
                    //                else
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant;
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium;
                    //                }
                    //            }
                    //            else
                    //            {
                    //                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                    //                p_structPremiumAmounts.ChgBilledPremium = 0;
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                    //        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium;
                    //    }
                    //}
                    break;

                // AUDIT PREMIUM CALC
                case "AU":
                    if (bWaivePremium)
                    {
                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                        p_structPremiumAmounts.ChgBilledPremium = 0;
                        //RMSC
                        if (sFlatorPercent == "P")
                        {
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = 0;
                            }
                        }
                        else
                        {
                            if (bUseTax && sTaxType == "P")
                            {
                                dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                dTax = dTaxRate;
                                dTranTax = 0;
                            }
                        }
                        p_structPremiumAmounts.Tax = dTax;
                        p_structPremiumAmounts.TransactionTax = dTax;
                        //RMSC
                    }
                    else
                    {
                        if (bMinBillApplies)
                        {
                            if (dblPrevAuditPremium > dblMinimumBillAmount)
                            {
                                if (dblEstiamtedPremium < dblMinimumBillAmount)
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        //RMSC
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTax;
                                    }
                                    else
                                    {
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblMinimumBillAmount + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round((dblMinimumBillAmount - dblEstiamtedPremium + p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant + dTax;
                                        p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTax;
                                    }
                                }
                                else
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        //RMSC
                                        if (bUseTax && sTaxType == "P")
                                        {
                                            dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                            dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);

                                        }
                                        else
                                        {
                                            dTax = dTaxRate;
                                            dTranTax = 0;
                                        }
                                        p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTax;
                                    }
                                    else
                                    {
                                        //RMSC
                                        if (bUseTax)
                                        {
                                            if (sTaxType == "P")
                                            {
                                                dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTax;
                                            }
                                            else
                                            {
                                                dTax = dTaxRate;
                                                dTranTax = 0;
                                                p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTax;
                                            }
                                        }
                                        else
                                        {
                                            //pmahli MITS 10006
                                            //ChgBilledPremium and TotalBilledPremium were reversed
                                            p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium;
                                            p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                                            p_structPremiumAmounts.Tax = 0;
                                            p_structPremiumAmounts.TransactionTax = 0;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //means prev bill amt = min bill amt
                                if (dblEstiamtedPremium > dblMinimumBillAmount)
                                {
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax)
                                        {
                                            if (sTaxType == "P")
                                            {
                                                dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                                p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTax;
                                            }
                                            else
                                            {
                                                dTax = dTaxRate;
                                                dTranTax = 0;
                                                p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) + dTax;
                                                p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium + dTax;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTax;
                                            }
                                        }
                                        else
                                        {
                                            p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                                            p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                                            p_structPremiumAmounts.Tax = dTax;
                                            p_structPremiumAmounts.TransactionTax = dTax;
                                        }
                                    }
                                    else
                                    {
                                        if (bUseTax)
                                        {
                                            if (sTaxType == "P")
                                            {
                                                dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                dTranTax = Math.Round((dblEstiamtedPremium - dblMinimumBillAmount) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                                                p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTax;
                                            }
                                            else
                                            {
                                                dTax = dTaxRate;
                                                dTranTax = 0;
                                                p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant + dTax;
                                                p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium + dTax;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTax;
                                            }
                                        }
                                        else
                                        {
                                            p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant;
                                            p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium;
                                            p_structPremiumAmounts.Tax = dTax;
                                            p_structPremiumAmounts.TransactionTax = dTax;
                                        }
                                        //p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant;
                                        //p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium;
                                    }
                                }
                                else
                                {
                                    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                                    p_structPremiumAmounts.ChgBilledPremium = 0;
                                    //RMSC
                                    if (sFlatorPercent == "P")
                                    {
                                        if (bUseTax)
                                        {
                                            if (sTaxType == "P")
                                            {
                                                dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTranTax;
                                            }
                                            else
                                            {
                                                dTax = dTaxRate;
                                                dTranTax = 0;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTranTax;
                                            }
                                        }
                                        else
                                        {
                                            p_structPremiumAmounts.Tax = 0;
                                            p_structPremiumAmounts.TransactionTax = 0;
                                        }
                                    }
                                    else
                                    {
                                        if (bUseTax)
                                        {
                                            if (sTaxType == "P")
                                            {
                                                dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTranTax;
                                            }
                                            else
                                            {
                                                dTax = dTaxRate;
                                                dTranTax = 0;
                                                p_structPremiumAmounts.Tax = dTax;
                                                p_structPremiumAmounts.TransactionTax = dTranTax;
                                            }
                                        }
                                        else
                                        {
                                            p_structPremiumAmounts.Tax = 0;
                                            p_structPremiumAmounts.TransactionTax = 0;
                                        }
                                    }
                                    //RMSC
                                }
                            }
                        }
                        else
                        {
                            //RMSC
                            if (sFlatorPercent == "P")
                            {
                                if (bUseTax)
                                {
                                    if (sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100))) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTax;
                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = 0;
                                        dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTax;
                                    }
                                }
                                else
                                {
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dAdjFactor;
                                    p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dAdjFactor;
                                    p_structPremiumAmounts.Tax = 0;
                                    p_structPremiumAmounts.TransactionTax = 0;
                                }
                            }
                            else
                            {
                                if (bUseTax)
                                {
                                    if (sTaxType == "P")
                                    {
                                        dTax = Math.Round((dblEstiamtedPremium + dblExpenseConstant) * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dTranTax = Math.Round(p_structPremiumAmounts.TransactionalPremium * (dTaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                        dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTax;
                                    }
                                    else
                                    {
                                        dTax = dTaxRate;
                                        dTranTax = 0;
                                        dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dTranTax + dAdjFactor;
                                        p_structPremiumAmounts.Tax = dTax;
                                        p_structPremiumAmounts.TransactionTax = dTax;
                                    }
                                }
                                else
                                {
                                    dTranTax = 0;
                                    dAdjFactor = CalculateAdjustment(bUseTax, sTaxType, sTransType, dTranTax, dTaxRate);
                                    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium + dAdjFactor;
                                    p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium + dAdjFactor;
                                    p_structPremiumAmounts.Tax = 0;
                                    p_structPremiumAmounts.TransactionTax = 0;
                                }
                            }
                        }
                    }
                    //if (bWaivePremium)
                    //{
                    //    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                    //    p_structPremiumAmounts.ChgBilledPremium = 0;
                    //}
                    //else
                    //{
                    //    if (bMinBillApplies)
                    //    {
                    //        if (dblPrevAuditPremium > dblMinimumBillAmount)
                    //        {
                    //            if (dblEstiamtedPremium < dblMinimumBillAmount)
                    //            {
                    //                if (sFlatorPercent == "P")
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                    //                }
                    //                else
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblMinimumBillAmount + dblExpenseConstant;
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblMinimumBillAmount + dblExpenseConstant - dblPrevTotalBilledPremium;
                    //                }
                    //            }
                    //            else
                    //            {
                    //                if (sFlatorPercent == "P")
                    //                {
                    //                    //pmahli MITS 10006
                    //                    //ChgBilledPremium and TotalBilledPremium were reversed
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                    //                }
                    //                else
                    //                {
                    //                    //pmahli MITS 10006
                    //                    //ChgBilledPremium and TotalBilledPremium were reversed
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium;
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            //means prev bill amt = min bill amt
                    //            if (dblEstiamtedPremium > dblMinimumBillAmount)
                    //            {
                    //                if (sFlatorPercent == "P")
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100));
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + (dblFirstTransEstimatedPrem * (dblExpenseConstant / 100)) - dblPrevTotalBilledPremium;
                    //                }
                    //                else
                    //                {
                    //                    p_structPremiumAmounts.TotalBilledPremium = dblEstiamtedPremium + dblExpenseConstant;
                    //                    p_structPremiumAmounts.ChgBilledPremium = dblEstiamtedPremium + dblExpenseConstant - dblPrevTotalBilledPremium;
                    //                }
                    //            }
                    //            else
                    //            {
                    //                p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium;
                    //                p_structPremiumAmounts.ChgBilledPremium = 0;
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        p_structPremiumAmounts.TotalBilledPremium = dblPrevTotalBilledPremium + p_structPremiumAmounts.TransactionalPremium;
                    //        p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.TransactionalPremium;
                    //    }
                    //}
                    //if (bUseTax && sTaxType == "P")
                    //{
                    //    dTax = Conversion.Round((p_structPremiumAmounts.TotalBilledPremium) * (dTaxRate / 100), 2);
                    //    dTranTax = Conversion.Round((p_structPremiumAmounts.TransactionalPremium) * (dTaxRate / 100), 2);
                    //}
                    //else
                    //{
                    //    dTax = dTaxRate;
                    //    dTranTax = dTaxRate;
                    //}

                    //p_structPremiumAmounts.TotalBilledPremium = p_structPremiumAmounts.TotalBilledPremium + dTax;
                    //p_structPremiumAmounts.ChgBilledPremium = p_structPremiumAmounts.ChgBilledPremium + dTax;
                    //p_structPremiumAmounts.Tax = dTax;
                    //p_structPremiumAmounts.TransactionTax = dTranTax;
                    break;
            }
            //MITS 9885 : Umesh
            //changed by Gagan for MITS 18687 : Start
            if (base.RoundFlag)
            {
                p_structPremiumAmounts.TotalBilledPremium = Math.Round(p_structPremiumAmounts.TotalBilledPremium, 0, MidpointRounding.AwayFromZero);
                p_structPremiumAmounts.ChgBilledPremium = Math.Round(p_structPremiumAmounts.ChgBilledPremium, 0, MidpointRounding.AwayFromZero);
                p_structPremiumAmounts.Tax = Math.Round(p_structPremiumAmounts.Tax, 0, MidpointRounding.AwayFromZero);
                p_structPremiumAmounts.TransactionTax = Math.Round(p_structPremiumAmounts.TransactionTax, 0, MidpointRounding.AwayFromZero);
            }
            else
            {
                p_structPremiumAmounts.TotalBilledPremium = Math.Round(p_structPremiumAmounts.TotalBilledPremium, 2, MidpointRounding.AwayFromZero);
                p_structPremiumAmounts.ChgBilledPremium = Math.Round(p_structPremiumAmounts.ChgBilledPremium, 2, MidpointRounding.AwayFromZero);
                p_structPremiumAmounts.Tax = Math.Round(p_structPremiumAmounts.Tax, 2, MidpointRounding.AwayFromZero);
                p_structPremiumAmounts.TransactionTax = Math.Round(p_structPremiumAmounts.TransactionTax, 2, MidpointRounding.AwayFromZero);
            }
            //changed by Gagan for MITS 18687 : End

        }

        private void CalculatePremium(int p_iTransNumber, ref PremiumCalculationUI p_structPremiumCalculationUI, ref PremiumAmounts p_structPremiumAmounts, EnhancePolicyUI p_structEnhancePolicyUI, int p_iPrevTierLevel, ref DiscountTier structLastDiscountTier, ref bool bIsLastDiscount, ref bool bLastTierFound, ref double dblTotalDiscountForTier, string sPrevTransEffDate)
        {
            bool bTierFound = true;
            DbReader objReader = null;
            string sTransType = string.Empty;
            string sSQL = string.Empty;
            string sEffectiveDate = string.Empty;
            int iTermNumber = 0;
            string sOtherTransType = string.Empty;
            //string sPrevTransEffDate = string.Empty;
            string sPrevTransExpDate = string.Empty;
            double dblPrevTotalEstimatedPremium = 0;
            double dblPrevTotalBilledPremium = 0;
            double dblPrevAuditPremium = 0;
            //Shruti for 11746
            double dblFirstTransEstimatedPrem = 0;
            //Shruti for 11746 ends
            DbReader objReader2 = null;
            //bool bTierFound = true;
            int iPrevTierLevel = p_iPrevTierLevel;
            //int iOrigPrevTierlevel = p_iPrevTierLevel;
            //double dblTotalDiscountForTier = 0;
            //bool bIsLastDiscount = false;
            //bool bLastTierFound = false;
            Discount structLastDiscount = new Discount();
            //DiscountTier structLastDiscountTier = new DiscountTier();
            int iDiscountRowId = 0;//pmahli Volume DIscount 9/12/2007   
            bool bUseVolDisc = false;//pmahli Volume Discount 9/12/2007 
            //npadhy RMSC retrofit Starts
            bool bUseTax = false;
            double dTax = 0;
            double dTaxRate = 0;
            double dTranTax = 0;
            long lTaxSign = 0;
            string sTaxType = string.Empty;
            double dPrevTax;
            double dPrevExpConst = 0;
            double dPreTaxAmount = 0;
            double dPrevTranTax = 0;
            int intTaxType = 0;
            //npadhy Code Changes for Adjustment of .01
            while (bTierFound)
            {
                bTierFound = false;
                dblTotalDiscountForTier = 0;




                this.CalculateDeductiblePremium(ref p_structPremiumAmounts, p_iTransNumber, p_structEnhancePolicyUI, iPrevTierLevel, structLastDiscountTier.Amount, sPrevTransEffDate);
                for (int iIndex = 0; iIndex < p_structPremiumAmounts.DiscountList.Count; iIndex++)
                {
                    Discount structDiscount = (Discount)p_structPremiumAmounts.DiscountList[iIndex];

                    if (structDiscount.Level == iPrevTierLevel)
                    {
                        bIsLastDiscount = true;
                        structLastDiscount = structDiscount;
                        //pmahli Volume Discount 9/12/2007 -start
                        //To get the value of Volume Discount Flag
                        iDiscountRowId = structDiscount.NameId;
                        sSQL = " SELECT * FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = " + iDiscountRowId;
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReader.Read())
                        {
                            bUseVolDisc = Conversion.ConvertObjToBool(objReader.GetValue("USE_VOLUME_DISCOUNT"), base.ClientId);
                        }
                        objReader.Close();
                        //pmahli Volume Discount 9/12/2007 -End
                        if (iPrevTierLevel == 1)
                        {
                            if (structDiscount.Type == "P")
                            {
                                //pmahli Volume Discount 9/12/2007 -start
                                if (bUseVolDisc)
                                {
                                    structDiscount.Amount = CalculateVolumeDisocunt(iDiscountRowId, p_structPremiumAmounts.ModifiedPremium);
                                    structDiscount.AmountFactor = structDiscount.Amount;
                                    //psharma41: Mits 11131
                                    structDiscount.Amount = structDiscount.Amount * (-1);
                                }
                                //pmahli Volume Discount 9/12/2007 - End
                                else
                                //psharma41: Mits 11131
                                {
                                    //structDiscount.Amount = p_structPremiumAmounts.ModifiedPremium * (structDiscount.AmountFactor / 100);
                                    structDiscount.Amount = p_structPremiumAmounts.ModifiedPremium * (structDiscount.AmountFactor / 100) * (-1);
                                }
                            }
                            else
                            {
                                if (p_structPremiumAmounts.ModifiedPremium != 0)
                                //psharma41: Mits 11131
                                //structDiscount.Amount = structDiscount.AmountFactor;
                                {
                                    structDiscount.Amount = structDiscount.AmountFactor * (-1);
                                }
                                else
                                    structDiscount.Amount = 0;
                            }
                        }
                        else if (iPrevTierLevel == 0)
                        {
                            if (iPrevTierLevel == 0)
                            {
                                if (structDiscount.Type == "P")
                                {
                                    //pmahli Volume Discount 9/12/2007 -start
                                    if (bUseVolDisc)
                                    {
                                        structDiscount.Amount = CalculateVolumeDisocunt(iDiscountRowId, p_structPremiumAmounts.ManualPremium);
                                        structDiscount.AmountFactor = structDiscount.Amount;
                                        //psharma41: Mits 11131
                                        structDiscount.Amount = structDiscount.Amount * (-1);
                                    }
                                    //pmahli Volume Discount 9/12/2007 - End
                                    else
                                    //psharma41: Mits 11131
                                    {
                                        //structDiscount.Amount = p_structPremiumAmounts.ModifiedPremium * (structDiscount.AmountFactor / 100);
                                        structDiscount.Amount = p_structPremiumAmounts.ManualPremium * (structDiscount.AmountFactor / 100) * (-1);
                                    }
                                }
                                else
                                {
                                    if (p_structPremiumAmounts.ManualPremium != 0)
                                    //psharma41: Mits 11131
                                    //structDiscount.Amount = structDiscount.AmountFactor;
                                    {
                                        structDiscount.Amount = structDiscount.AmountFactor * (-1);
                                    }
                                    else
                                        structDiscount.Amount = 0;
                                }
                            }

                        }
                        else
                        {
                            if (structDiscount.Type == "P")
                            {
                                //pmahli Volume Discount 9/12/2007 -Start
                                if (bUseVolDisc)
                                {
                                    structDiscount.Amount = CalculateVolumeDisocunt(iDiscountRowId, structLastDiscountTier.Amount);
                                    structDiscount.AmountFactor = structDiscount.Amount;
                                    //psharma41: Mits 11131
                                    structDiscount.Amount = structDiscount.Amount * (-1);
                                }
                                //pmahli Volume Discount 9/12/2007 -End
                                else
                                    //psharma41: Mits 11131
                                    //structDiscount.Amount = structLastDiscountTier.Amount * (structDiscount.AmountFactor / 100);
                                    structDiscount.Amount = structLastDiscountTier.Amount * (structDiscount.AmountFactor / 100) * (-1);
                            }
                            else
                            {
                                if (structLastDiscountTier.Amount != 0)
                                    //psharma41: Mits 11131
                                    //structDiscount.Amount = structDiscount.AmountFactor;
                                    structDiscount.Amount = structDiscount.AmountFactor * (-1);
                                else
                                    structDiscount.Amount = 0;
                            }
                        }
                        if (base.RoundFlag)
                            structDiscount.Amount = Math.Round(structDiscount.Amount, 0, MidpointRounding.AwayFromZero);
                        else
                            structDiscount.Amount = Math.Round(structDiscount.Amount, 2, MidpointRounding.AwayFromZero);

                        //psharma41 : Mits 11131
                        //dblTotalDiscountForTier += structDiscount.Amount;
                        dblTotalDiscountForTier = dblTotalDiscountForTier - structDiscount.Amount;

                        p_structPremiumAmounts.DiscountList[iIndex] = structDiscount;
                    }
                }

                // now that we've applied all the discounts for that level, calc the amount of the next disc tier
                for (int iIndex = 0; iIndex < p_structPremiumAmounts.DiscountTierList.Count; iIndex++)
                {
                    DiscountTier structDiscountTier = (DiscountTier)p_structPremiumAmounts.DiscountTierList[iIndex];
                    // Check the Parent of the Discount Tier
                    int iParent = FindParent(structDiscountTier,p_structPremiumAmounts.DiscountTierList);

                    if (iParent != p_iPrevTierLevel)
                        continue;
                    if (structDiscountTier.LevelId == iPrevTierLevel)
                    {
                        if (iPrevTierLevel == 0)
                        {
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                            //structDiscountTier.Amount = p_structPremiumAmounts.ModifiedPremium - dblTotalDiscountForTier;
                            structDiscountTier.Amount = p_structPremiumAmounts.ManualPremium - dblTotalDiscountForTier;
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                        }
                        else if (iPrevTierLevel == 1)
                        {
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                            //structDiscountTier.Amount = p_structPremiumAmounts.ModifiedPremium - dblTotalDiscountForTier;
                            structDiscountTier.Amount = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                        }
                        else
                        {
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
                            if (PolicyEnh.PolicyXCvgEnhList.Count > 0)
                            {
                                structDiscountTier.Amount = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                            }
                            else
                            {
                                structDiscountTier.Amount = structLastDiscountTier.Amount - dblTotalDiscountForTier;
                            }
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
                        }
                        if (base.RoundFlag)
                            structDiscountTier.Amount = Math.Round(structDiscountTier.Amount, 0, MidpointRounding.AwayFromZero);
                        else
                            structDiscountTier.Amount = Math.Round(structDiscountTier.Amount, 2, MidpointRounding.AwayFromZero);

                        p_structPremiumAmounts.DiscountTierList[iIndex] = structDiscountTier;
                        break;
                    }
                }

                // Now find the next level
                foreach (DiscountTier structDiscountTier in p_structPremiumAmounts.DiscountTierList)
                {
                    int iParent = FindParent(structDiscountTier, p_structPremiumAmounts.DiscountTierList);

                    if (iParent != p_iPrevTierLevel)
                        continue;
                    if (structDiscountTier.LevelId == iPrevTierLevel)
                    {
                        bTierFound = true;
                        bIsLastDiscount = false;
                        bLastTierFound = true;
                        structLastDiscountTier = structDiscountTier;
                        iPrevTierLevel = structDiscountTier.NameId;
                        break;
                    }
                }
            }

            if (p_iPrevTierLevel == 0)
            {
                
                if (p_structPremiumAmounts.DiscountList.Count != 0 || p_structPremiumAmounts.DiscountTierList.Count != 0)
                {
                    if (!bIsLastDiscount)
                    {
                        // Grab the last tier amt
                        //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
                        if (PolicyEnh.PolicyXCvgEnhList.Count > 0)
                        {
                            p_structPremiumAmounts.ManualFinalPremium = p_structPremiumAmounts.TotalModifier;
                        }
                        else
                        {
                            p_structPremiumAmounts.ManualFinalPremium = structLastDiscountTier.Amount;
                        }
                        //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
                        //Commented by Anu Tennyson for MITS 18229
                        //p_structPremiumAmounts.FinalPremium = structLastDiscountTier.Amount;
                        //Commented by Anu Tennyson for MITS 18229 
                    }
                    else
                    {
                        if (bLastTierFound)
                        {
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
                            if (PolicyEnh.PolicyXCvgEnhList.Count > 0)
                            {
                                p_structPremiumAmounts.ManualFinalPremium = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                            }
                            else
                            {
                                p_structPremiumAmounts.ManualFinalPremium = structLastDiscountTier.Amount - dblTotalDiscountForTier;
                            }
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
                            //Commneted by Anu Tennyson
                            //p_structPremiumAmounts.FinalPremium = structLastDiscountTier.Amount - dblTotalDiscountForTier;
                            //Commneted by Anu Tennyson
                        }
                        else
                        {
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                            //p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.ModifiedPremium - dblTotalDiscountForTier;
                            p_structPremiumAmounts.ManualFinalPremium = p_structPremiumAmounts.TotalModifier - dblTotalDiscountForTier;
                            //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                        }
                    }
                }
                else
                {
                    //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                    //p_structPremiumAmounts.FinalPremium = p_structPremiumAmounts.ModifiedPremium;
                    p_structPremiumAmounts.ManualFinalPremium = p_structPremiumAmounts.TotalModifier;
                    //Changed by Anu Tennyson for VACo Prem. Calculation MITS 18229
                }
               // p_structPremiumAmounts.ManualFinalPremium = structLastDiscountTier.Amount;
                if (p_structPremiumAmounts.ExpModFactor > 0)
                {

                    p_structPremiumAmounts.ExpModFactorAmount = p_structPremiumAmounts.ManualFinalPremium * p_structPremiumAmounts.ExpModFactor;
                    p_structPremiumAmounts.ModifiedPremium = p_structPremiumAmounts.ExpModFactorAmount;
                    p_structPremiumAmounts.TotalModifier = p_structPremiumAmounts.ExpModFactorAmount;
                }
                else
                {
                    p_structPremiumAmounts.ModifiedPremium = p_structPremiumAmounts.ManualFinalPremium;
                }
            }
        }

        private int FindParent(DiscountTier p_structDiscountTier, ArrayList p_DiscountTierList)
        {
            int iParentId = 0;
            if (p_structDiscountTier.LevelId == 0 || p_structDiscountTier.LevelId == 1)
                iParentId = p_structDiscountTier.LevelId;
            else
            {
                for (int iIndex = 0; iIndex < p_DiscountTierList.Count; iIndex++)
                {
                    DiscountTier structDiscountTier = (DiscountTier)p_DiscountTierList[iIndex];
                    if (structDiscountTier.NameId == p_structDiscountTier.LevelId)
                    {
                        iParentId = FindParent(structDiscountTier, p_DiscountTierList);
                    }
                }
            }
            return iParentId;
        }


        ///Animesh Inserted New function to calculate the Adjustment value
        private double CalculateAdjustment(bool bUseTax,string sTaxType, string sTransType, double dTranTax, double dTaxRate) 
        {
            int iTranCount; 
            double dPrevTax;
            double dAdjFactor;
            double dTaxToCharge;
            double dCurrentTax=0;
            double dCurrentPremAmt=0 ;
            int iPrevTranIndex=0;
            PolicyXTransEnh objTrans; 
            bool bFound;
            int iIterator;
            string sTranType;
            try
            {
                bFound = false;
                iTranCount = PolicyEnh.PolicyXTransEnhList.Count;
                if (bUseTax)
                {
                    for (iIterator = iTranCount - 1; iIterator >= 0; iIterator--)
                    {
                        sTranType = base.LocalCache.GetShortCode(((PolicyEnh.PolicyXTransEnhList[iIterator] as PolicyXTransEnh).TransactionType));
                        if (sTranType != "AU")
                        {
                            iPrevTranIndex = iIterator;
                            break;
                        }
                    }
                    if (sTransType == "EN" || sTransType == "RL" || sTransType == "RNL")
                    {
                        dPrevTax = (PolicyEnh.PolicyXTransEnhList[iPrevTranIndex]as PolicyXTransEnh).TaxAmount;
                        dCurrentTax = dPrevTax + dTranTax;
                        dCurrentPremAmt = (PolicyEnh.PolicyXTransEnhList[iTranCount]as PolicyXTransEnh).TotalEstPremium;
                        bFound = true;
                    }
                    else if (sTransType == "AU")
                    {
                        dPrevTax = (PolicyEnh.PolicyXTransEnhList[iPrevTranIndex] as PolicyXTransEnh).TaxAmount;
                        dCurrentTax = dPrevTax + dTranTax;
                        dCurrentPremAmt = (PolicyEnh.PolicyXTransEnhList[iTranCount] as PolicyXTransEnh).TotalEstPremium + (PolicyEnh.PolicyXTransEnhList[iTranCount] as PolicyXTransEnh).TransactionPrem;
                    }
                    if (sTaxType == "P")
                    {
                        dTaxToCharge = dCurrentPremAmt * (dTaxRate / 100);
                    }
                    else
                    {
                        dTaxToCharge = dTaxRate;
                    }
                    if (bFound)
                    {
                        dAdjFactor = Math.Round(dTaxToCharge, 2, MidpointRounding.AwayFromZero) - dCurrentTax;
                    }
                    else
                    {
                        dAdjFactor = 0;
                    }
                }
                else
                {
                    dAdjFactor = 0;
                }
                return dAdjFactor;
            }
            catch (Exception ex)
            {
                return 0; 
            }

        }

        //pmahli Volume Discount 9/12/2007 -Start 
        //Calculate Volume Discount calculates discount on the basis Range entered if volume discount is turned on
        private double CalculateVolumeDisocunt(int p_iDiscountRowID, double p_iDiscountAmount)
        {
            DbReader objReader = null;
            string sSQL = string.Empty;
            double dblBegRange = 0;
            double dblEndRange = 0;
            double dblAmount = 0;
            double dblDiscount = 0;
            double dblRangeAmount = 0; //srajindersin MITS 30223 dt 06/07/2013
            sSQL = "Select * from SYS_POL_DISC_RANGE where Discount_RowId = " + p_iDiscountRowID + " Order By Beginning_Range";
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            while (objReader.Read())
            {
                dblBegRange = Conversion.ConvertObjToDouble(objReader.GetValue("BEGINNING_RANGE"), base.ClientId);
                dblEndRange = Conversion.ConvertObjToDouble(objReader.GetValue("END_RANGE"), base.ClientId);
                dblAmount = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), base.ClientId);

                //srajindersin MITS 30223 dt 06/07/2013
                if (dblBegRange == 0)
                    dblRangeAmount = dblEndRange - dblBegRange;
                else
                    dblRangeAmount = dblEndRange - dblBegRange + 1;

                if (p_iDiscountAmount >= dblRangeAmount && dblEndRange != 0)
                {
                    //srajindersin MITS 30223 dt 06/07/2013
                    dblDiscount = dblDiscount + (dblRangeAmount * dblAmount / 100);
                    p_iDiscountAmount -= dblRangeAmount;
                }
                else
                {
                    dblDiscount = dblDiscount + p_iDiscountAmount * dblAmount / 100;
                    break;
                }
            }
            return dblDiscount;
        }
        //pmahli Volume Discount 9/12/2007 -End
#endregion 

        #region Calculation Premium Changed
        
        public void UpdateDiscounts( int p_iTransNumber , XmlDocument p_objSysEx)
        {
            XmlElement objTemp = null;
            // Start Naresh MITS 9327
            XmlElement objPrevTranId = null;
            // End Naresh MITS 9327
            double dblExpModFactor = 0.0;
            //pmahli MITS 9870 - start
            XmlElement objPrevTransId = null;
            int iPrevTransId = 0;
            XmlElement objPostBackAction = null;
            string sPostBackAction = string.Empty;
            int iMaxTransID = 0;
            //pmahli MITS 9870-End
            try
            {
                bool bEditFlag = false;
                bool bBuildFlag = false;
                bool bCalcFlag = false;
                string sTransactionType = string.Empty;

                //pmahli MITS 9793
                //pmahli MITS 9870 as it was commom to both the MITS so code shifted to this position  
                objPostBackAction = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + EnhancePolicyManager.POST_BACK_ACTION);
                sPostBackAction = objPostBackAction.InnerText;

                // Set the Build , Re-Calculate Preimum Flag and Last Transaction Number .
                // pmahli MITS 9793 sPostBackAction passed in SetPremCalculateScreenFlag as parameter 
                this.SetPremCalculateScreenFlag(ref bBuildFlag, ref bCalcFlag, ref bEditFlag, ref p_iTransNumber, ref sTransactionType, sPostBackAction);

                if (p_iTransNumber == 0)
                {
                    this.AddDiscountAndTiers(p_objSysEx);
                }
                //Anu Tennyson for MITS 18229 - Prem Calculation STARTS
                
                this.AddModifier(p_objSysEx);
                //Anu Tennyson for MITS 18229 - ENDS
                // If Exp Mod Factor Change : 
                objTemp = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_EXP_MOD_FACTOR_ID);
                if (objTemp != null)
                    dblExpModFactor = Conversion.ConvertStrToDouble(objTemp.InnerText);
                else
                    dblExpModFactor = 0;
                //pmahli MITS 9870 -start
                //ExpModFactor was getting changed when there was navigation between Transaction
                objPrevTransId = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/PreSelectedTransactionId");
                iPrevTransId = Conversion.ConvertStrToInteger(objPrevTransId.InnerText);
                foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                    if (objPolicyXRtngEnh.TransactionId > iMaxTransID)
                        iMaxTransID = objPolicyXRtngEnh.TransactionId;
                //pmahli MITS 9870 - End 
                if (bCalcFlag)
                {
                    foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                    {
                        if (objPolicyXRtngEnh.TransactionId > iMaxTransID)
                            iMaxTransID = objPolicyXRtngEnh.TransactionId;
                        if (objPolicyXRtngEnh.TransactionId == p_iTransNumber)
                        {
                            //pmahli MITS 9870 -start 
                            //For Renew Transaction Change ExpModFactor is updated only once when previous transaction id is maximum 
                            //For Amend Transaction also Change ExpModFactor is updated only once when previous transaction id is maximum 
                            //pmahli MITS 9793 
                            //For Audit Transaction Change ExpModFactor is updated only once when previous transaction id is maximum 
                            if (sPostBackAction == EnhancePolicyManager.ACTION_TRANSACTION_CHANGE && (sTransactionType == "RN" || sTransactionType == "EN" || sTransactionType == "AU"))
                            {
                                if (iPrevTransId == iMaxTransID)
                                    objPolicyXRtngEnh.ExpModFactor = dblExpModFactor;
                            }
                            //pmahli MITS 9870 -End 
                            else
                            {
                                objPolicyXRtngEnh.ExpModFactor = dblExpModFactor;
                            }
                            this.CalculateModifiedPremium(objPolicyXRtngEnh);
                            break;
                        }
                    }
                }

                // If Discount Factor has change
                foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
                {
                    if (objPolicyXDcntEnh.TransactionId == p_iTransNumber)
                    {
                        objTemp = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_DISCOUNTS + "/" + CALC_PREM_DISCOUNT + "/" + CALC_PREM_DISCOUNT_ID + "[. = " + objPolicyXDcntEnh.DiscountId.ToString() + "]/../" + CALC_PREM_DISCOUNT_AMOUNT_FACTOR);
                        if (objTemp != null)
                            objPolicyXDcntEnh.DiscountFactor = Conversion.ConvertStrToDouble(objTemp.InnerText);

                        // Start Naresh MITS 9270 Discount Refresh Problem
                        objTemp = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_DISCOUNTS + "/" + CALC_PREM_DISCOUNT + "/" + CALC_PREM_DISCOUNT_ID + "[. = " + objPolicyXDcntEnh.DiscountId.ToString() + "]/../" + CALC_PREM_DISCOUNT_AMOUNT);
                        if (objTemp != null)
                            objPolicyXDcntEnh.DiscountAmt = Conversion.ConvertStrToDouble(objTemp.InnerText);
                        // End Naresh MITS 9270 Discount Refresh Problem

                    }

                }

                // If Waive Prem has Changed
                objTemp = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + CALC_PREM_WAIVE_PREMIUM_ID);
                // Start Naresh MITS 9327
                objPrevTranId = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/PreSelectedTransactionId");

                int iTransId = 0;
                foreach (PolicyXTransEnh objPolicyXTransEnh in this.PolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId > iTransId)
                    {
                        iTransId = objPolicyXTransEnh.TransactionId;
                    }
                }
                foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId == iTransId && Conversion.ConvertStrToInteger(objPrevTranId.InnerText) == iTransId)
                    {
                        if (base.LocalCache.GetShortCode(objPolicyXTransEnh.TransactionStatus) == "PR")
                        {
                            if(objTemp != null)
                                objPolicyXTransEnh.WaivePremiumInd = Conversion.ConvertStrToBool(objTemp.InnerText);
                            if (!objPolicyXTransEnh.WaivePremiumInd)
                                objPolicyXTransEnh.WaivePremComment = "";

                            break;
                        }
                    }
                }

            }
            finally
            {
                objPrevTranId = null;
            }
            // End Naresh MITS 9327
        }

        public void UpdateDiscountsTiersObject(int p_iTransNumber , XmlDocument p_objSysEx)
        {
            XmlElement objTemp = null;

            foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
            {
                if (objPolicyXDcntEnh.TransactionId == p_iTransNumber)
                {
                    objTemp = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_DISCOUNTS + "/" + CALC_PREM_DISCOUNT + "/" + CALC_PREM_DISCOUNT_ID + "[. = " + objPolicyXDcntEnh.DiscountId.ToString() + "]/../" + CALC_PREM_DISCOUNT_AMOUNT_FACTOR);
                    if (objTemp != null)
                        objPolicyXDcntEnh.DiscountFactor = Conversion.ConvertStrToDouble(objTemp.InnerText);

                    objTemp = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_DISCOUNTS + "/" + CALC_PREM_DISCOUNT + "/" + CALC_PREM_DISCOUNT_ID + "[. = " + objPolicyXDcntEnh.DiscountId.ToString() + "]/../" + CALC_PREM_DISCOUNT_AMOUNT);
                    if (objTemp != null)
                        objPolicyXDcntEnh.DiscountAmt = Conversion.ConvertStrToDouble(objTemp.InnerText);

                }

            }
            foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
            {
                if (objPolicyXDtierEnh.TransactionId == p_iTransNumber)
                {
                    objTemp = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_TIERS + "/" + CALC_PREM_TIER + "/" + CALC_PREM_TIER_ID + "[. = " + objPolicyXDtierEnh.DiscountTierId.ToString() + "]/../" + CALC_PREM_TIER_AMOUNTS);
                    if (objTemp != null)
                        objPolicyXDtierEnh.DiscTierAmt = Conversion.ConvertStrToDouble(objTemp.InnerText);
                }

            }
        }
        public void AddDiscountAndTiers(XmlDocument p_objSysEx)
        {

            PolicyXDcntEnh objPolicyXDcntEnhNew = null;
            PolicyXDtierEnh objPolicyXDtierEnhNew = null;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 STARTS
            XmlElement objModifierElement = null;
            int iCount = 0;
            string sPath = string.Empty;
            //Anu Tennyson for VACo Prem. Calculation MITS 18229 ENDS
            // Unsaved Quote. 
            // Add Discounts in the list. 
            foreach (XmlNode objDiscountNode in p_objSysEx.SelectNodes("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_DISCOUNTS + "/" + CALC_PREM_DISCOUNT))
            {
                objPolicyXDcntEnhNew = PolicyEnh.PolicyXDiscountEnhList.AddNew();
                objPolicyXDcntEnhNew.DiscountNameId = Conversion.ConvertStrToInteger(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_NAME_ID).InnerText);
                objPolicyXDcntEnhNew.DiscountFactor = Conversion.ConvertStrToDouble(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_AMOUNT_FACTOR).InnerText);
                objPolicyXDcntEnhNew.DiscountAmt = Conversion.ConvertStrToDouble(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_AMOUNT).InnerText);
                objPolicyXDcntEnhNew.DiscountLevelId = Conversion.ConvertStrToInteger(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_LEVEL).InnerText);
                objPolicyXDcntEnhNew.TransactionId = 0;
            }

            // Add Discounts Tiers in the list. 
            foreach (XmlNode objTierNode in p_objSysEx.SelectNodes("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_TIERS + "/" + CALC_PREM_TIER))
            {
                objPolicyXDtierEnhNew = PolicyEnh.PolicyXDiscountTierEnhList.AddNew();
                objPolicyXDtierEnhNew.DiscTierNameId = Conversion.ConvertStrToInteger(objTierNode.SelectSingleNode(CALC_PREM_TIER_NAME_ID).InnerText);
                objPolicyXDtierEnhNew.DiscTierLevelId = Conversion.ConvertStrToInteger(objTierNode.SelectSingleNode(CALC_PREM_TIER_LEVEL).InnerText);
                objPolicyXDtierEnhNew.DiscTierAmt = Conversion.ConvertStrToDouble(objTierNode.SelectSingleNode(CALC_PREM_TIER_AMOUNTS).InnerText);
                objPolicyXDtierEnhNew.TransactionId = 0;
            }
        }
        //Anu Tennyson for MITS 18229 - Prem Calc-STARTS
        /// <summary>
        /// Function to store the current value from the SysEx.
        /// </summary>
        /// <param name="p_objSysEx">SysEx XmlDocument</param>
        public void AddModifier(XmlDocument p_objSysEx)
        {
            XmlElement objModifierElement = null;
            int iCount = 0;
            string sPath = string.Empty;
            List<int> iCvgAdded = new List<int>();
            int iLastCvgSeq = 0;

            XmlElement objPostBackAction = null;
            string sPostBackAction = string.Empty;

            objPostBackAction = (XmlElement)p_objSysEx.SelectSingleNode("/SysExData/" + EnhancePolicyManager.POST_BACK_ACTION);
            sPostBackAction = objPostBackAction.InnerText;

            foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
            {
                iCvgAdded.Add(objPolicyXCvgEnh.CoverageSeq);
            }
            iCvgAdded.Sort();
            foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
            {
                if (objPolicyXCvgEnh.PolcvgRowId < 0)
                {
                    iLastCvgSeq = iCvgAdded[iCvgAdded.Count - 1];
                    if (iLastCvgSeq > 0)
                    {
                        objPolicyXCvgEnh.CoverageSeq = iLastCvgSeq + 1;
                        iCvgAdded.Add(iLastCvgSeq + 1);
                        iCvgAdded.Sort();
                    }
                    else if (iLastCvgSeq == 0)
                    {
                        objPolicyXCvgEnh.CoverageSeq = 1;
                    }
                    else
                    {
                        objPolicyXCvgEnh.CoverageSeq = objPolicyXCvgEnh.PolcvgRowId * (-1);
                    }
                }
            }
            //MITS 25290 rsushilaggar Date 06/22/2011
            if (string.Compare(sPostBackAction, "COVERAGES_EDIT") != 0)
            {
                foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                {
                    string sUnique = string.Empty;
                    if (objPolicyXCvgEnh.PolcvgRowId < 0)
                    {
                        sUnique = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.CoverageSeq.ToString();
                    }
                    else
                    {
                        sUnique = objPolicyXCvgEnh.ExpRateRowId.ToString() + objPolicyXCvgEnh.PolcvgRowId.ToString();
                    }
                // If the Deductible and Modifier are zero, we still want to show the Coverage in Prem Calc Screen
                // because it might be associated with ranges. User might wish to change the existing modifier, and select a range

                //if (objPolicyXCvgEnh.Deductible == 0.0 && objPolicyXCvgEnh.Modifier == 0.0)
                //{
                //    continue;

                //}
                    sPath = "/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_MODIFIERS + "/" + CALC_PREM_MODIFIER + "/" + CALC_PREM_MODIFIER_AMOUNT_FACTORID + sUnique;
                    objModifierElement = (XmlElement)p_objSysEx.SelectSingleNode(sPath);
                    if (objModifierElement != null)
                    {
                        objPolicyXCvgEnh.Modifier = Conversion.ConvertStrToDouble(p_objSysEx.SelectSingleNode(sPath).InnerText);
                        iCount++;
                    }

                }
            }
        }
        //Anu Tennyson for MITS 18229 - END

        public void UpdateDiscountAndTiers(XmlDocument p_objSysEx)
        {

            PolicyXDcntEnh objPolicyXDcntEnhNew = null;
            PolicyXDtierEnh objPolicyXDtierEnhNew = null;
            
            // Unsaved Quote. 
            // Add Discounts in the list. 
            foreach (XmlNode objDiscountNode in p_objSysEx.SelectNodes("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_DISCOUNTS + "/" + CALC_PREM_DISCOUNT))
            {
                int iDiscountId = Conversion.ConvertStrToInteger(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_ID).InnerText);
                objPolicyXDcntEnhNew = PolicyEnh.PolicyXDiscountEnhList[iDiscountId];

                objPolicyXDcntEnhNew.DiscountNameId = Conversion.ConvertStrToInteger(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_NAME_ID).InnerText);
                objPolicyXDcntEnhNew.DiscountFactor = Conversion.ConvertStrToDouble(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_AMOUNT_FACTOR).InnerText);
                objPolicyXDcntEnhNew.DiscountAmt = Conversion.ConvertStrToDouble(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_AMOUNT).InnerText);
                objPolicyXDcntEnhNew.DiscountLevelId = Conversion.ConvertStrToInteger(objDiscountNode.SelectSingleNode(CALC_PREM_DISCOUNT_LEVEL).InnerText);
                objPolicyXDcntEnhNew.TransactionId = 0;
            }

            // Add Discounts Tiers in the list. 
            foreach (XmlNode objTierNode in p_objSysEx.SelectNodes("/SysExData/" + CALC_PREM_ROOT + "/" + CALC_PREM_TIERS + "/" + CALC_PREM_TIER))
            {
                int iTierId = Conversion.ConvertStrToInteger(objTierNode.SelectSingleNode(CALC_PREM_TIER_ID).InnerText);
                objPolicyXDtierEnhNew = PolicyEnh.PolicyXDiscountTierEnhList[iTierId];

                objPolicyXDtierEnhNew.DiscTierNameId = Conversion.ConvertStrToInteger(objTierNode.SelectSingleNode(CALC_PREM_TIER_NAME_ID).InnerText);
                objPolicyXDtierEnhNew.DiscTierLevelId = Conversion.ConvertStrToInteger(objTierNode.SelectSingleNode(CALC_PREM_TIER_LEVEL).InnerText);
                objPolicyXDtierEnhNew.DiscTierAmt = Conversion.ConvertStrToDouble(objTierNode.SelectSingleNode(CALC_PREM_TIER_AMOUNTS).InnerText);
                objPolicyXDtierEnhNew.TransactionId = 0;
            }
        }
        
        public void CalculateModifiedPremium(PolicyXRtngEnh objPolicyXRtngEnh)
        {
            if (objPolicyXRtngEnh.ExpModFactor == 0)
                objPolicyXRtngEnh.ModifiedPremium = objPolicyXRtngEnh.ManualPremium;
            else
                objPolicyXRtngEnh.ModifiedPremium = objPolicyXRtngEnh.ManualPremium * objPolicyXRtngEnh.ExpModFactor;

            objPolicyXRtngEnh.ExpModAmount = Math.Abs(objPolicyXRtngEnh.ModifiedPremium - objPolicyXRtngEnh.ManualPremium);

            if (base.RoundFlag)
            {
                objPolicyXRtngEnh.ExpModAmount = Math.Round(objPolicyXRtngEnh.ExpModAmount, 0, MidpointRounding.AwayFromZero);
                objPolicyXRtngEnh.ModifiedPremium = Math.Round(objPolicyXRtngEnh.ModifiedPremium, 0, MidpointRounding.AwayFromZero);
            }
            else
            {
                objPolicyXRtngEnh.ExpModAmount = Math.Round(objPolicyXRtngEnh.ExpModAmount, 2, MidpointRounding.AwayFromZero);
                objPolicyXRtngEnh.ModifiedPremium = Math.Round(objPolicyXRtngEnh.ModifiedPremium, 2, MidpointRounding.AwayFromZero);
            }
        }
        #endregion 

        #region Calculate Earned Premium

        private bool ValdiateCalculateEarendPremiumDates( string p_sFormDate , string p_sToDate , int p_iPolicyId , ref int p_iSequenceNumber , ref int p_iTermNumber )
        {
            DbReader objReader = null ;
            string sSQL = string.Empty ;
            int iMaxTransactionId = 0;
            
            try
            {
                // First check the date range is valid
                if (Conversion.ConvertStrToInteger(p_sFormDate) - Conversion.ConvertStrToInteger(p_sToDate) > 0)
                    throw new RMAppException("From Date must be prior to To Date. Please modify date range.");

                // Find the correct term for the FromDate - was defaulted but user could have overridden
                sSQL = " SELECT DISTINCT(TERM_NUMBER) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_iPolicyId 
                    +   " AND EFFECTIVE_DATE <= '" + p_sFormDate + "'"
                    + " AND EXPIRATION_DATE > '" + p_sFormDate + "' ";
                objReader = DbFactory.GetDbReader( base.ConnectionString , sSQL );
                if( objReader.Read() )
                    p_iTermNumber = objReader.GetInt(0);
                else
                {
                    throw new RMAppException("No term record found that includes the date range entered. Please modify date range." );                    
                }
                objReader.Close(); 

                sSQL = " SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_iPolicyId 
                    +   " AND TERM_NUMBER = " + p_iTermNumber + " AND TERM_STATUS <> " 
                    +   base.CCacheFunctions.GetCodeIDWithShort( "PR", "TERM_STATUS" ) ;
                objReader = DbFactory.GetDbReader( base.ConnectionString , sSQL );
                if( objReader.Read() )
                    p_iSequenceNumber = objReader.GetInt(0);
                objReader.Close();

                if ( p_iSequenceNumber == 0)
                    throw new RMAppException("The term for the dates entered is provisional. Earned premium cannot be calculated on a provisional term. Please modify the date range if you wish to continue.");

                // Check to make sure the transaction associated is not provisional
                sSQL = " SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + p_iPolicyId 
                    +   " AND TERM_NUMBER = " + p_iTermNumber + " AND TERM_SEQ_ALPHA = " + p_iSequenceNumber ;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iMaxTransactionId = objReader.GetInt(0);
                objReader.Close();
                    
                sSQL = " SELECT TRANSACTION_STATUS , TRANSACTION_TYPE FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iMaxTransactionId ;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    if( base.CCacheFunctions.GetShortCode( objReader.GetInt( "TRANSACTION_STATUS")) == "PR" ) 
                    {
                        string sTransactionType = base.CCacheFunctions.GetShortCode( objReader.GetInt( "TRANSACTION_TYPE"));
                        if( sTransactionType == "CF" || sTransactionType == "CPR" || sTransactionType == "RL" || sTransactionType == "RNL" )
                            p_iSequenceNumber -- ;
                    }
                }
                objReader.Close();

                sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_iPolicyId 
                    +   " AND TERM_NUMBER = " + p_iTermNumber + " AND SEQUENCE_ALPHA = " + p_iSequenceNumber ;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    // This is the most recent term rec for this term num... make sure the from date is included with this recs term dates
                    if( Conversion.ConvertStrToInteger(p_sFormDate) < Conversion.ConvertStrToInteger( objReader.GetString( "EFFECTIVE_DATE" ) ) )
                        throw new RMAppException("The From Date entered is not included within the term dates of the most recent term record for this term. Please modify date range." );
            
                    // Check if there is a cancel date and the from date is not after the cancel date
                    if( objReader.GetString( "CANCEL_DATE" ) != "" )
                    {
                        if( Conversion.ConvertStrToInteger(p_sFormDate) > Conversion.ConvertStrToInteger( objReader.GetString( "CANCEL_DATE" ) ) )
                            throw new RMAppException("The policy cancellation date is prior to the From Date. Please modify date range." );
                
                    }

                    // Make sure the user hasn't crossed terms
                    if( Conversion.ConvertStrToInteger(p_sToDate) > Conversion.ConvertStrToInteger( objReader.GetString( "EXPIRATION_DATE" ) ) )
                        throw new RMAppException("Earned premium cannot be calculated across terms. Please re-enter the date range." );
            
                    // Check if there is a cancel date and the to date is not after the cancel date
                    if( objReader.GetString( "CANCEL_DATE" ) != "" )
                    {
                        if( Conversion.ConvertStrToInteger(p_sToDate) > Conversion.ConvertStrToInteger( objReader.GetString( "CANCEL_DATE" ) ) )
                            throw new RMAppException("The policy cancellation date is prior to the To Date. Please modify date range." );
                
                    }        
                }
                objReader.Close();
                return true ;
            }
            finally
            {
                if( objReader != null )
                    objReader.Close();
            }

        }

        private double CalculateEarnedPremium(string p_sFormDate, string p_sToDate, int p_iPolicyId, int p_iSequenceNumber, int p_iTermNumber)
        {
            /*
             * DISCLAIMER: ISO STANDARDS ARE THAT ROUNDING IS DONE AFTER EACH CALCULATION (ROUNDED TO 3 PLACES)
             * 
             * THEN A FINAL ROUNDING IS DONE AFTER ALL PARTS ARE TOTALLED AND NO DECIMAL PLACES ARE RETAINED
             */ 

            PolicyEnh objPolicyEnh = null;
            DbReader objReader = null ;
            // npadhy MITS 15882 Get the Expense Constant detail
            string sOtherTransType = string.Empty;
            string sTermEffective = string.Empty;
            string sTermExpiration = string.Empty;
            string sEffectiveDate = string.Empty;
            string sFlatorPercent = string.Empty;
            int iTermLength = 0;
            int iEPLength = 0;
            double dblFactor = 0.0;
            string sSQL = string.Empty ;
            int iMaxTransactionId = 0 ;
            double dblTotalBilled = 0 ;
            int iRatingId = 0 ;
            double dblExpConst = 0 ;
            // npadhy MITS 15882 Get the Expense Constant detail
            double dblFirstTransEstimatedPrem = 0;
            //npadhy RMSC retrofit Starts
            double dTaxAmount;
            //npadhy RMSC retrofit Ends


            try
            {
                //npadhy RMSC retrofit Starts
                dTaxAmount = 0;
                //npadhy RMSC retrofit Ends
                objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);
                objPolicyEnh.MoveTo(p_iPolicyId);

                foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                {
                    if (objPolicyXTermEnh.TermNumber == p_iTermNumber && objPolicyXTermEnh.SequenceAlpha == p_iSequenceNumber)
                    {
                        sTermEffective = objPolicyXTermEnh.EffectiveDate;
                        sTermExpiration = objPolicyXTermEnh.ExpirationDate;
                        break;
                    }
                }

                iTermLength = ((TimeSpan)Conversion.ToDate(sTermExpiration).Subtract(Conversion.ToDate(sTermEffective))).Days + 1;
                if (iTermLength == 366)
                    iTermLength = 365;

                iEPLength = ((TimeSpan)Conversion.ToDate(p_sToDate).Subtract(Conversion.ToDate(p_sFormDate))).Days + 1;
                if (iEPLength == 366)
                    iEPLength = 365;

                //dblFactor = Conversion.Round( (double)(iEPLength * 1.0 / iTermLength), 3);
                //pmahli MITS 10034 Changes done to correct earned Premium
                dblFactor = (iEPLength * 1.0 / iTermLength);


                // First find the txn that is either new bus or has the same eff date as nb/rn
                //pmahli 2/20/2007 corrected sSQL
                sSQL = " SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + p_iPolicyId
                    + " AND TERM_NUMBER=" + p_iTermNumber + " AND EFFECTIVE_DATE = " + sTermEffective;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iMaxTransactionId = objReader.GetInt(0);
                objReader.Close();

                //npadhy RMSC retrofit Starts
                //sSQL = "SELECT TOTAL_BILLED_PREM FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iMaxTransactionId;
                sSQL = "SELECT TOTAL_BILLED_PREM, TAX_AMOUNT FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iMaxTransactionId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    dblTotalBilled = objReader.GetDouble("TOTAL_BILLED_PREM");
                    dTaxAmount = Conversion.ConvertObjToDouble(objReader.GetValue("TAX_AMOUNT"), base.ClientId);
                }
                //npadhy RMSC retrofit Starts

                objReader.Close();

                sSQL = " SELECT MIN(RATING_ID) FROM POLICY_X_RTNG_ENH WHERE POLICY_ID=" + p_iPolicyId
                    + " AND TERM_NUMBER=" + p_iTermNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iRatingId = objReader.GetInt(0);
                objReader.Close();

                // Read to find out if there is an expense constant
                sSQL = "SELECT EXPENSE_CONSTANT FROM POLICY_X_RTNG_ENH WHERE RATING_ID = " + iRatingId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    //pmahli 2/20/2007 converted Getint to getdouble
                    dblExpConst = objReader.GetDouble("EXPENSE_CONSTANT");
                objReader.Close();

                // npadhy MITS 15563 The Expense Constant is being considerd as Flat.
                // Modifying the SQL and Calculation if Expense Constant is Percent

                // Get the Amount on which the Expense Constant needs to be calculated
                sSQL = " SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + objPolicyEnh.PolicyId
                        + " AND TERM_NUMBER=" + p_iTermNumber + " ORDER BY TRANSACTION_ID DESC";

                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                while (objReader.Read())
                {
                    sOtherTransType = base.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("TRANSACTION_TYPE"), base.ClientId));
                    if (sOtherTransType == "NB" || sOtherTransType == "RN")
                    {
                        dblFirstTransEstimatedPrem = Conversion.ConvertObjToDouble(objReader.GetValue("TOTAL_EST_PREMIUM"), base.ClientId);
                        sEffectiveDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                    }
                }

                // Get the Type of the Expense Constant
                sSQL = " SELECT * FROM SYS_POL_EXP_CONST WHERE LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType
                + " AND STATE=" + objPolicyEnh.State + " AND NOT IN_USE_FLAG = 0"
                + " AND EFFECTIVE_DATE <= '" + sEffectiveDate
                + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE='' OR EXPIRATION_DATE >= '" + sEffectiveDate + "')";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    sFlatorPercent = base.LocalCache.GetShortCode(objReader.GetInt("FLAT_OR_PERCENT"));
                }

                if (sFlatorPercent == "P")
                    dblExpConst = dblFirstTransEstimatedPrem * dblExpConst / 100;
                dblTotalBilled -= dblExpConst;

                //npadhy RMSC retrofit Starts
                dblTotalBilled -= dTaxAmount;
                //npadhy RMSC retrofit Ends
                dblTotalBilled = Math.Round((dblFactor * dblTotalBilled), 3, MidpointRounding.AwayFromZero);

                // Loop through to find all the other transactions that we need
                bool bStop = false;

                sSQL = " SELECT * FROM POLICY_X_TRANS_ENH WHERE POLICY_ID=" + p_iPolicyId
                    + " AND TERM_NUMBER=" + p_iTermNumber + " AND TRANSACTION_ID > " + iMaxTransactionId
                    + " AND TRANSACTION_TYPE <>" + base.CCacheFunctions.GetCodeIDWithShort("AU", "TRANSACTION_TYPE")
                    + " AND TRANSACTION_STATUS = " + base.CCacheFunctions.GetCodeIDWithShort("OK", "TRANSACTION_STATUS")
                    + " ORDER BY TRANSACTION_ID";

                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                string sPriorDate = string.Empty;
                double dblChgPremAmt = 0;

                while (objReader.Read())
                {
                    if (bStop)
                        break;
                    sPriorDate = objReader.GetString("EFFECTIVE_DATE");
                    if (Conversion.ConvertStrToInteger(sPriorDate) > Conversion.ConvertStrToInteger(p_sToDate))
                        bStop = true;
                    else
                    {
                        dblChgPremAmt = objReader.GetDouble("CHG_BILLED_PREMIUM");
                        //npadhy RMSC retrofit Starts - subtract tax from the change in billed premium
                        dTaxAmount = Conversion.ConvertObjToDouble(objReader.GetValue("TRAN_TAX"), base.ClientId);
                        dblChgPremAmt -= dTaxAmount;
                        //npadhy RMSC retrofit Ends

                        if (dblChgPremAmt != 0)
                        {
                            DateTime dtExpiratinDate = Conversion.ToDate(objReader.GetString("EXPIRATION_DATE"));
                            DateTime dtEffectiveDate = Conversion.ToDate(objReader.GetString("EFFECTIVE_DATE"));
                            iTermLength = ((TimeSpan)dtExpiratinDate.Subtract(dtEffectiveDate)).Days + 1;

                            DateTime dtFromDate = Conversion.ToDate(p_sFormDate);
                            DateTime dtToDate = Conversion.ToDate(p_sToDate);
                            DateTime dtPriorDate = Conversion.ToDate(sPriorDate);

                            if (Conversion.ConvertStrToInteger(sPriorDate) <= Conversion.ConvertStrToInteger(p_sFormDate))
                                iEPLength = ((TimeSpan)dtToDate.Subtract(dtFromDate)).Days + 1;
                            else
                                iEPLength = ((TimeSpan)dtToDate.Subtract(dtPriorDate)).Days + 1;

                            dblFactor = Math.Round((double)(iEPLength * 1.0 / iTermLength), 3, MidpointRounding.AwayFromZero);
                            dblChgPremAmt = Math.Round((dblChgPremAmt * dblFactor), 3, MidpointRounding.AwayFromZero);
                            dblTotalBilled += dblChgPremAmt;
                        }
                    }
                }
                objReader.Close();

                //dblTotalBilled = Conversion.Round(dblTotalBilled,0) + dblExpConst ;
                //pmahli MITS 10034
                if (base.RoundFlag)
                    dblTotalBilled = Math.Round(dblTotalBilled, 0, MidpointRounding.AwayFromZero);
                else
                    dblTotalBilled = Math.Round(dblTotalBilled, 2, MidpointRounding.AwayFromZero);
                return (dblTotalBilled);
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
            }

        }

        public XmlDocument CalculateEarnedPremium(string p_sFormDate, string p_sToDate, int p_iPolicyId )
        {
            XmlElement objRootNode = null;
            XmlDocument objPremium = null;

            int iSequenceNumber = 0 ;
            int iTermNumber = 0 ;
            double dblEarnedPremium = 0;

            //Manish Multicurrency
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
            LocalCache objCache = null;
            objCache = new LocalCache(base.ConnectionString,ClientId);

            try
            {
                objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), ref sShortCode, ref sDesc);
                sCurrency = sDesc.Split('|')[1];
                if (this.ValdiateCalculateEarendPremiumDates(p_sFormDate, p_sToDate , p_iPolicyId , ref iSequenceNumber , ref iTermNumber))
                {
                   dblEarnedPremium = this.CalculateEarnedPremium(p_sFormDate, p_sToDate, p_iPolicyId, iSequenceNumber, iTermNumber);
                    base.StartDocument(ref objPremium, ref objRootNode, "EarnedPremium");
                    base.CreateAndSetElement(objRootNode, "CalculatedPremium", (Convert.ToString(dblEarnedPremium)));
                    base.CreateAndSetElement(objRootNode, "EffectiveDate", p_sFormDate);
                    base.CreateAndSetElement(objRootNode, "BaseCurrencyType", sCurrency);
                }
                    
                return (objPremium);
            }
            finally
            {
                objPremium = null;
                objRootNode = null;
            }
        }

        #endregion 

      
        //pmahli EPA Discount Range Selection 2/2/2007
        
        #region Range Selection

        public XmlDocument GetDiscountRange( int p_iDiscountRowId )
        {
            XmlDocument objRangeSelection = null;
            XmlElement objRootElement = null;
            XmlElement objTermListXmlElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;
            DbReader objReader = null;
            DbReader objReader2 = null;
            string sSQL = string.Empty;
            string sType = string.Empty;
            int iIndex = 0;
            int iVolumeDiscount = 0;
            //rupal:start, 
            //multicurrency enh
            Riskmaster.Settings.SysSettings objSysSettings = null;
            string sValue=string.Empty;
            bool bOut=false;
            //rupal:end
            try
            {
                //rupal:start
                // Multicurrency Enh
                objSysSettings = new Settings.SysSettings(base.ConnectionString, base.ClientId);
                //rupal:end
                //pmahli-9/12/2007 -  Node added for volume discount
                sSQL = "SELECT USE_VOLUME_DISCOUNT FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = " + p_iDiscountRowId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if(objReader.Read())
                    iVolumeDiscount = Conversion.ConvertObjToInt(objReader.GetValue("USE_VOLUME_DISCOUNT"), base.ClientId);

                base.StartDocument(ref objRangeSelection, ref objRootElement, "DiscountRange");
                // Create Node for Volume Discount
                base.CreateAndSetElement(objRootElement, "UseVolumeDiscount", iVolumeDiscount.ToString());

                base.CreateElement(objRootElement, "TermList", ref objTermListXmlElement);
                //Create Header node for Grid
                base.CreateElement(objTermListXmlElement, "listhead", ref objListHeadXmlElement);
                base.CreateAndSetElement(objListHeadXmlElement, "RangeStart", "Range Start");
                base.CreateAndSetElement(objListHeadXmlElement, "RangeEnd", "Range End");
                base.CreateAndSetElement(objListHeadXmlElement, "Amount", "Amount");
                //pmahli 10/3/2007 Sorting of Disocunt Ranges
                sSQL = "SELECT * FROM SYS_POL_DISC_RANGE WHERE DISCOUNT_ROWID = " + p_iDiscountRowId +"ORDER BY DISC_RANGE_ROWID";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                while (objReader.Read())
                {
                    sSQL = "SELECT FLAT_OR_PERCENT FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = " + p_iDiscountRowId;
                    objReader2 = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader2.Read())
                    {
                        sType = base.CCacheFunctions.GetShortCode(Conversion.ConvertObjToInt(objReader2.GetValue(0), base.ClientId));
                    }
                    objReader2.Close();

                    base.CreateElement(objTermListXmlElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", "/Instance/Document/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex++ + "]");
                    //rupal:start, multicurrency enh
                    if (objSysSettings.UseMultiCurrency != 0)
                    {
                        sValue = CommonFunctions.ConvertCurrency(0, Conversion.CastToType<double>(objReader.GetValue("BEGINNING_RANGE").ToString(), out bOut), CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                        base.CreateAndSetElement(objOptionXmlElement, "RangeStart", sValue);
                    }
                    else
                    {
                        base.CreateAndSetElement(objOptionXmlElement, "RangeStart", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), Conversion.ConvertObjToDouble(objReader.GetValue("BEGINNING_RANGE"), base.ClientId), base.ConnectionString, ClientId));
                    }
                    //pmahli 10/3/200/7 Displaying Last end range of Volume Discount range as blank

                    if (Conversion.ConvertObjToDouble(objReader.GetValue("END_RANGE"), base.ClientId) == 0.0)
                    {
                        if (objSysSettings.UseMultiCurrency != 0)
                        {
                            base.CreateAndSetElement(objOptionXmlElement, "RangeEnd", "");
                        }
                        else
                        {
                            base.CreateAndSetElement(objOptionXmlElement, "RangeEnd", string.Format("{0:C}", ""));
                        }
                    }
                    else
                    {
                        if (objSysSettings.UseMultiCurrency != 0)
                        {
                            sValue = CommonFunctions.ConvertCurrency(0, Conversion.CastToType<double>(objReader.GetValue("END_RANGE").ToString(), out bOut), CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                            base.CreateAndSetElement(objOptionXmlElement, "RangeEnd", sValue);
                        }
                        else
                        {
                            base.CreateAndSetElement(objOptionXmlElement, "RangeEnd", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), Conversion.ConvertObjToDouble(objReader.GetValue("END_RANGE"), base.ClientId), base.ConnectionString, ClientId));
                        }
                    }
                    //rupal:end
                    if (sType == "P")
                    {
                        Double Amount = 0;
                        Amount = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), base.ClientId) / 100;
                        base.CreateAndSetElement(objOptionXmlElement, "Amount", string.Format("{0:P}", Amount ));                       
                    }
                    else
                    {
                        //rupal:start
                        if (objSysSettings.UseMultiCurrency != 0)
                        {
                            sValue = CommonFunctions.ConvertCurrency(0, Conversion.CastToType<double>(objReader.GetValue("AMOUNT").ToString(), out bOut), CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                            base.CreateAndSetElement(objOptionXmlElement, "Amount", sValue);
                        }
                        else
                        {
                            base.CreateAndSetElement(objOptionXmlElement, "Amount", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), base.ClientId), base.ConnectionString, ClientId));
                        }
                        //rupal:end
                    }

                    base.CreateAndSetElement(objOptionXmlElement, "AMOUNT_HIDDEN", Conversion.ConvertObjToStr(objReader.GetValue("AMOUNT")));
                    base.CreateAndSetElement(objOptionXmlElement, "DISC_RANGE_ROWID", Conversion.ConvertObjToStr(objReader.GetValue("DISC_RANGE_ROWID")));
              
                }
                objReader.Close();
                return (objRangeSelection);
            }
            finally
            {
                objRangeSelection = null;
                objRootElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                if (objReader != null)
                    objReader.Close();
                if (objReader2 != null)
                    objReader2.Close();
                //rupal:multicurrency enh
                objSysSettings = null;
            }
        }

        #endregion 
        // pmahli EPA Discount Range Selection 2/2/2007-End

        //Anu Tennyson for VACo Prem. Calculation MITS 18229
        #region Modifier Range Selection
        /// <summary>
        /// Function for showing the range when range button is selected for a Modifier
        /// </summary>
        /// <param name="p_iExpRateRowId">Exposure Rate Row ID</param>
        /// <returns></returns>
        public XmlDocument GetModifierRange(int p_iExpRateRowId)
        {
            string sSql = string.Empty;
            string sFlatPercSql = string.Empty;
            string sType = string.Empty;
            double dbAmount = 0;
            DbReader objReader = null;
            DbReader objFlatPercReader = null;
            XmlElement objTermListXmlElement = null;
            XmlElement objOptionXmlElement = null;
            XmlDocument objModifierRangeSelection = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objRootElement = null;
            int iIndex = 0;
            bool bIsSuccess = false;
            bool bListHeadFlag = false;
            //rupal:start. mits 27502
            Riskmaster.Settings.SysSettings objSysSettings = null;
            bool bOut = false;
            string sValue = string.Empty;
            //rupal:end
            try
            {
                //rupal:start,mits 27502
                //this is actually part of Multicurrency Enh
                objSysSettings = new Settings.SysSettings(base.ConnectionString, base.ClientId);                
                //rupal:end
                sSql = "SELECT * FROM SYS_POL_RANGE WHERE RATE_ROWID = " + p_iExpRateRowId + "ORDER BY RANGE_ROWID";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSql);
                base.StartDocument(ref objModifierRangeSelection, ref objRootElement, "ModifierRange");
                base.CreateElement(objRootElement, "TermList", ref objTermListXmlElement);
                //Create Header node for Grid
                base.CreateElement(objTermListXmlElement, "listhead", ref objListHeadXmlElement);
                //Commented to ask carrie
                //base.CreateAndSetElement(objListHeadXmlElement, "ModifierRangeStart", "Range Start");
                //base.CreateAndSetElement(objListHeadXmlElement, "ModifierRangeEnd", "Range End");
                //Commented to ask carrie
                base.CreateAndSetElement(objListHeadXmlElement, "Modifier", "Modifier");
                while (objReader.Read())
                {
                    sFlatPercSql = "SELECT FLAT_OR_PERCENT, AMOUNT FROM SYS_POL_EXP_RATES WHERE EXP_RATE_ROWID = " + p_iExpRateRowId;
                    objFlatPercReader = DbFactory.GetDbReader(base.ConnectionString, sFlatPercSql);
                    if (objFlatPercReader.Read())
                    {
                        sType = base.CCacheFunctions.GetShortCode(Conversion.CastToType<int>((objFlatPercReader.GetValue(0)).ToString(), out bIsSuccess));
                        dbAmount = Conversion.CastToType<double>((objFlatPercReader.GetValue(1)).ToString(), out bIsSuccess);
                    }
                    objFlatPercReader.Close();

                    base.CreateElement(objTermListXmlElement, "option", ref objOptionXmlElement);
                    objOptionXmlElement.SetAttribute("ref", "/Instance/Document/" + objOptionXmlElement.ParentNode.LocalName + "/option[" + iIndex++ + "]");
                    //Commented to ask carrie
                    if (objReader.GetValue("BEGINNING_RANGE").ToString() != "")
                    {
                        if (!bListHeadFlag)
                        {
                            bListHeadFlag = true;
                            base.CreateAndSetElement(objListHeadXmlElement, "ModifierRangeStart", "Range Start");
                            base.CreateAndSetElement(objListHeadXmlElement, "ModifierRangeEnd", "Range End");
                        }
                        //rupal:start, mits 27502
                        if (objSysSettings.UseMultiCurrency != 0)
                        {
                            sValue = CommonFunctions.ConvertCurrency(0, Conversion.CastToType<double>(objReader.GetValue("BEGINNING_RANGE").ToString(), out bOut), CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                            base.CreateAndSetElement(objOptionXmlElement, "ModifierRangeStart", sValue);
                        }
                        else
                        {
                            base.CreateAndSetElement(objOptionXmlElement, "ModifierRangeStart", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), Conversion.ConvertObjToDouble(objReader.GetValue("BEGINNING_RANGE"), base.ClientId), base.ConnectionString, ClientId));
                        }


                        if (objReader.GetValue("END_RANGE").ToString() == "")
                        {
                            if (objSysSettings.UseMultiCurrency != 0)
                            {
                                base.CreateAndSetElement(objOptionXmlElement, "ModifierRangeEnd", "");
                            }
                            else
                            {
                                base.CreateAndSetElement(objOptionXmlElement, "ModifierRangeEnd", string.Format("{0:C}", ""));
                            }

                        }
                        else
                        {
                            if (objSysSettings.UseMultiCurrency != 0)
                            {
                                sValue = CommonFunctions.ConvertCurrency(0, Conversion.CastToType<double>(objReader.GetValue("END_RANGE").ToString(), out bOut), CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                                base.CreateAndSetElement(objOptionXmlElement, "ModifierRangeEnd", sValue);
                            }
                            else
                            {
                                base.CreateAndSetElement(objOptionXmlElement, "ModifierRangeEnd", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), Conversion.ConvertObjToDouble(objReader.GetValue("END_RANGE"), base.ClientId), base.ConnectionString, ClientId));
                            }
                        }

                        //rupal:end

                    }
                    else
                    {
                        if (!bListHeadFlag)
                        {
                            bListHeadFlag = true;
                            base.CreateAndSetElement(objListHeadXmlElement, "Amount", "Amount");
                        }
                        base.CreateAndSetElement(objOptionXmlElement, "Amount", Convert.ToString(dbAmount));
                    }
                    //Commented to ask carrie
                    if (sType == "P")
                    {
                        Double Amount = 0;
                        Amount = Conversion.CastToType<double>(objReader.GetValue("MODIFIER").ToString(), out bIsSuccess);
                        Amount = Amount/100 ;
                        base.CreateAndSetElement(objOptionXmlElement, "Modifier", string.Format("{0:P}", Amount));
                    }
                    else
                    {
                        if (objSysSettings.UseMultiCurrency != 0)
                        {
                            sValue = CommonFunctions.ConvertCurrency(0, Conversion.CastToType<double>(objReader.GetValue("MODIFIER").ToString(), out bOut), CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                            base.CreateAndSetElement(objOptionXmlElement, "Modifier", sValue);
                        }
                        else
                        {
                            base.CreateAndSetElement(objOptionXmlElement, "Modifier", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), Conversion.ConvertObjToDouble(objReader.GetValue("MODIFIER"), base.ClientId), base.ConnectionString, ClientId));
                        }
                    }

                    base.CreateAndSetElement(objOptionXmlElement, "MODIFIER_HIDDEN", (objReader.GetValue("MODIFIER").ToString()));
                    base.CreateAndSetElement(objOptionXmlElement, "EXP_RATE_ROWID", (objReader.GetValue("RANGE_ROWID").ToString()));

                }
                objReader.Close();
                
            }
            finally
            {
                objRootElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                if (objReader != null)
                    objReader.Close();
                if (objFlatPercReader != null)
                    objFlatPercReader.Close();
                //rupal:mits 27502                
                 objSysSettings = null;
                //rupal
            }
            return (objModifierRangeSelection);
        }

        #endregion
        //Anu Tennyson for VACo Prem. Calculation MITS 18229
        #region base <Control> View XML generation Functions

        private void CreateGroup(XmlDocument p_objDoc, ref XmlElement p_objGroup, string p_sName, string p_sTitle)
        {
            p_objGroup = p_objDoc.CreateElement("group");
            p_objDoc.AppendChild(p_objGroup);

            p_objGroup.SetAttribute("name", p_sName);
            p_objGroup.SetAttribute("title", p_sTitle);

        }

        private void CreateDisplayColumn(XmlElement p_objGroup, ref XmlElement p_objDisplayColumn)
        {
            p_objDisplayColumn = p_objGroup.OwnerDocument.CreateElement("displaycolumn");
            p_objGroup.AppendChild(p_objDisplayColumn);
        }

        private void CreateControlGroup(XmlElement p_objDisplayColumn, ref XmlElement p_objControlGroup, string p_sControlId)
        {
            p_objControlGroup = p_objDisplayColumn.OwnerDocument.CreateElement("control");
            p_objDisplayColumn.AppendChild(p_objControlGroup);

            //p_objControlGroup.SetAttribute("ref", "Instance/UI/MissingRef");
            p_objControlGroup.SetAttribute("name", p_sControlId);
            p_objControlGroup.SetAttribute("type", "controlgroup");
            p_objControlGroup.SetAttribute("singlerow", "true");
        }

        private void CreateSpaceControl(XmlElement p_objRootElement, string p_sWidth, string p_sId)
        {
            XmlElement objControl = null;
            objControl = p_objRootElement.OwnerDocument.CreateElement("control");
            p_objRootElement.AppendChild(objControl);

            objControl.SetAttribute("id", p_sId);
            objControl.SetAttribute("width", p_sWidth);
            objControl.SetAttribute("type", "space");
        }

        private void CreateButtonScriptControl(XmlElement p_objRootElement, string p_sId, string p_sTitle , string p_sOnClick )
        {
            XmlElement objControl = null;
            objControl = p_objRootElement.OwnerDocument.CreateElement("control");
            p_objRootElement.AppendChild(objControl);

            objControl.SetAttribute("width", "20px");
            objControl.SetAttribute("title", p_sTitle);
            objControl.SetAttribute("pvtitle", p_sTitle);
            objControl.SetAttribute("name", p_sId);
            objControl.SetAttribute("id", p_sId);
            objControl.SetAttribute("functionname", p_sOnClick);
            objControl.SetAttribute("type", "buttonscript");
        }

        private void CreateLabelControl(XmlElement p_objRootElement, string p_sName, string p_sTitle)
        {
            XmlElement objControl = null;
            objControl = p_objRootElement.OwnerDocument.CreateElement("control");
            p_objRootElement.AppendChild(objControl);

            objControl.SetAttribute("name", p_sName);
            objControl.SetAttribute("title", p_sTitle);
            //objControl.SetAttribute("ref", Constants.MISSING_REF);
            objControl.SetAttribute("type", "labelonly");
        }

        private void CreateLabelControl(XmlElement p_objRootElement, string p_sName, string p_sTitle, string p_sRef)
        {
            XmlElement objControl = null;
            objControl = p_objRootElement.OwnerDocument.CreateElement("control");
            p_objRootElement.AppendChild(objControl);

            objControl.SetAttribute("name", p_sName);
            objControl.SetAttribute("title", p_sTitle);
            objControl.SetAttribute("ref", p_sRef);
            objControl.SetAttribute("type", "labelonly");
        }

        private void CreateHiddenControl(XmlElement p_objRootElement, string p_sName, string p_sValue, string p_sRef)
        {
            XmlElement objControl = null;
            objControl = p_objRootElement.OwnerDocument.CreateElement("control");
            p_objRootElement.AppendChild(objControl);

            objControl.SetAttribute("name", p_sName);
            objControl.SetAttribute("value", p_sValue);
            objControl.SetAttribute("ref", p_sRef);
            objControl.SetAttribute("type", "id");
        }



        private void CreateControl(XmlElement p_objRootElement, string p_sName, string p_sTitle, string p_sREF, string p_sType, string p_sTabIndex)
        {
            this.CreateControl(p_objRootElement, p_sName, p_sTitle, p_sREF, p_sType, p_sTabIndex, false , "" , "");
        }

        private void CreateControl(XmlElement p_objRootElement, string p_sName, string p_sTitle, string p_sREF, string p_sType, string p_sTabIndex , bool p_bReadonly )
        {
            this.CreateControl(p_objRootElement, p_sName, p_sTitle, p_sREF, p_sType, p_sTabIndex, p_bReadonly ,"" , "" );
        }

        private void CreateControl(XmlElement p_objRootElement, string p_sName, string p_sTitle, string p_sREF, string p_sType, string p_sTabIndex, bool p_bReadonly , string p_sWidth)
        {
            this.CreateControl(p_objRootElement, p_sName, p_sTitle, p_sREF, p_sType, p_sTabIndex, p_bReadonly, p_sWidth, "");
        }

        private void CreateControl(XmlElement p_objRootElement, string p_sName, string p_sTitle, string p_sREF, string p_sType, string p_sTabIndex, string p_sWidth )
        {
            this.CreateControl(p_objRootElement, p_sName, p_sTitle, p_sREF, p_sType, p_sTabIndex, false, p_sWidth , "" );
        }

        private void CreateControl(XmlElement p_objRootElement, string p_sName, string p_sTitle, string p_sREF, string p_sType, string p_sTabIndex, string p_sWidth , string p_sOnChange)
        {
            this.CreateControl(p_objRootElement, p_sName, p_sTitle, p_sREF, p_sType, p_sTabIndex, false, p_sWidth , p_sOnChange );
        }

        private void CreateControl(XmlElement p_objRootElement, string p_sName, string p_sTitle, string p_sREF, string p_sType, string p_sTabIndex, bool p_bReadonly, string p_sWidth, string p_sOnChange )
        {
            XmlElement objControl = null;
            objControl = p_objRootElement.OwnerDocument.CreateElement("control");
            p_objRootElement.AppendChild(objControl);

            objControl.SetAttribute("name", p_sName);
            objControl.SetAttribute("title", p_sTitle);
            objControl.SetAttribute("ref", p_sREF);
            objControl.SetAttribute("type", p_sType);
            objControl.SetAttribute("tabindex", p_sTabIndex);
            if (p_sWidth != "")
                objControl.SetAttribute("size", p_sWidth);
            else
                objControl.SetAttribute("size", "20");
            if( p_bReadonly )
                objControl.SetAttribute("readonly", true.ToString().ToLower());

            if (p_sType == "checkbox")
            {
                if (p_sOnChange != "")
                    objControl.SetAttribute("onclick", p_sOnChange);
            }
            else
            {
                if (p_sOnChange != "")
                    objControl.SetAttribute("onchange", p_sOnChange);
            }
        }

        #endregion

        // Naresh Connection Leak
        public void Dispose()
        {
            if (base.LocalCache != null)
            {
                base.LocalCache.Dispose();
            }
            if (base.DataModelFactory != null)
            {
                base.DataModelFactory.Dispose();
            }
        }

        //Added by Amitosh for mits 27535
        private  bool CompareEffDate(DateTime p_sEffDate)
        {
            bool bReturnValue = false;
            try
            {
            TimeSpan tDiff  = (new DateTime(p_sEffDate.Year, 2, 29)).Subtract(p_sEffDate);
                if (tDiff.Days >= 0)
                {
                    bReturnValue = true;
                }
            }
            catch (Exception e)
            {
                bReturnValue = false;
            }
           


            return bReturnValue;

        }

        private bool CompareExpDate(DateTime p_sExpDate)
        {
            bool bReturnValue = false;
            try
            {
                TimeSpan tDiff = p_sExpDate.Subtract(new DateTime(p_sExpDate.Year, 2, 29));
                if (tDiff.Days >= 0)
                {
                    bReturnValue = true;
                }
            }
            catch (Exception e)
            {
                bReturnValue = false;
            }
          
            return bReturnValue;

        }
        //End Amitosh
        
    }
}
