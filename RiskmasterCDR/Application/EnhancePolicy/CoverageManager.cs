﻿using System;
using System.Collections;
using System.Text;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.EnhancePolicy
{    
    public struct PolicyCoveragesUITemp
    {
        private bool bExceptionsEnabled;
        private bool bRemarksEnabled;
        private bool bCoverageTypeCodeEnabled;
        private bool bCoverageStatusEnabled;
        private bool bEffDateEnabled;
        private bool bExpDateEnabled;
        private bool bPolicyLimitEnabled;
        private bool bOccurrenceLimitEnabled;
        private bool bClaimLimitEnabled;
        private bool bTotalPaymentsEnabled;
        private bool bDeductiblelEnabled;
        private bool bSelfInsuredRetentionEnabled;
        private bool bNextPolicyIDEnabled;
        private bool bCancelNoticeDaysEnabled;
        private bool bNotificationEIdEnabled;
        private bool bBrokerLastNameEnabled;
        private bool bOkEnabled;
        private bool bCancelEnabled;
        //Anu Tennyson for MITS 18299 - UAR List On Coverages Screen -START
        private bool bUARaddedlistEnabled;
        //Anu Tennyson for MITS 18229 - UAR List On Coverages Screen - Ends
        public bool ExceptionsEnabled { get { return bExceptionsEnabled; } set { bExceptionsEnabled = value; } }
        public bool RemarksEnabled { get { return bRemarksEnabled; } set { bRemarksEnabled = value; } }
        public bool CoverageTypeCodeEnabled { get { return bCoverageTypeCodeEnabled; } set { bCoverageTypeCodeEnabled = value; } }
        public bool CoverageStatusEnabled { get { return bCoverageStatusEnabled; } set { bCoverageStatusEnabled = value; } }
        public bool EffDateEnabled { get { return bEffDateEnabled; } set { bEffDateEnabled = value; } }
        public bool ExpDateEnabled { get { return bExpDateEnabled; } set { bExpDateEnabled = value; } }
        public bool PolicyLimitEnabled { get { return bPolicyLimitEnabled; } set { bPolicyLimitEnabled = value; } }
        public bool OccurrenceLimitEnabled { get { return bOccurrenceLimitEnabled; } set { bOccurrenceLimitEnabled = value; } }
        public bool ClaimLimitEnabled { get { return bClaimLimitEnabled; } set { bClaimLimitEnabled = value; } }
        public bool TotalPaymentsEnabled { get { return bTotalPaymentsEnabled; } set { bTotalPaymentsEnabled = value; } }
        public bool DeductiblelEnabled { get { return bDeductiblelEnabled; } set { bDeductiblelEnabled = value; } }
        public bool SelfInsuredRetentionEnabled { get { return bSelfInsuredRetentionEnabled; } set { bSelfInsuredRetentionEnabled = value; } }
        public bool NextPolicyIDEnabled { get { return bNextPolicyIDEnabled; } set { bNextPolicyIDEnabled = value; } }
        public bool CancelNoticeDaysEnabled { get { return bCancelNoticeDaysEnabled; } set { bCancelNoticeDaysEnabled = value; } }
        public bool NotificationEIdEnabled { get { return bNotificationEIdEnabled; } set { bNotificationEIdEnabled = value; } }
        public bool BrokerLastNameEnabled { get { return bBrokerLastNameEnabled; } set { bBrokerLastNameEnabled = value; } }
        public bool OkEnabled { get { return bOkEnabled; } set { bOkEnabled = value; } }
        public bool CancelEnabled { get { return bCancelEnabled; } set { bCancelEnabled = value; } }
        //Anu Tennyson for MITS 18229 - UAR List on Coverages Screen - START
        public bool UARaddedlistEnabled { get { return bUARaddedlistEnabled; } set { bUARaddedlistEnabled = value; } }
        //Anu Tennyson for MITS 18229 - UAR List on Coverages Screen - Ends
    }
    
    // Naresh Connection Leak
    public class CoverageManager:Common,IDisposable
    {
        private const string SC_PROVISIONAL = "PR";
        private const string SC_DELETED = "DE";
        private const string TBL_COVEXP = "COVEXP_STATUS";
        private long m_lDSNId = 0;

        #region Constructor

        internal CoverageManager(PolicyEnh p_objPolicyEnh):base( p_objPolicyEnh.Context)
        {
            m_objPolicyEnh = p_objPolicyEnh;
        }
        public CoverageManager(string p_sDsnName, int p_sDsnId, string p_sUserName, string p_sPassword, int p_iClientId)
            : base(p_sDsnName, p_sUserName, p_sPassword, p_iClientId)
		{
            m_lDSNId = p_sDsnId;							
		}       

        #endregion

        #region Global Variables

        private PolicyEnh m_objPolicyEnh = null;

        private PolicyEnh PolicyEnh
        {
            get
            {
                return (m_objPolicyEnh);
            }
        }

          //Manish multicurrency
        //LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
        #endregion

        // Naresh Connection Leak
        public void Dispose()
        {
            if (base.LocalCache != null)
            {
                base.LocalCache.Dispose();
            }
            if (base.DataModelFactory != null)
            {
                base.DataModelFactory.Dispose();
            }
        }
        //Anu Tennyson for MITS 18231 STARTS 2/3/2010
        /// <summary>
        /// Code Table to find the Code Id.
        /// </summary>
        private const string CODE_TABLENAME = "RATE_MAINTENANCE_TYPE";
        //Anu Tennyson for MITS 18231 ENDS

        public void RenderCoverageList(XmlDocument objSysEx, int p_iTransNumber, Hashtable p_objUniqueIds)
        {
            XmlElement objCvgListElement = null;
            XmlElement objListHeadXmlElement = null;
            XmlElement objOptionXmlElement = null;

            // Create two Sorted List for Sorting the Xml, one will contain the positive Entries, and second will Contain Negative entries
            SortedList objPositiveEntries = null;
            SortedList objNegativeEntries = null;
            bool bSortingNecessary = false;

 

            int iIndex = 0;
            string sCvgCode = string.Empty;
            string sCvgDesc = string.Empty;
            string sStatusCode = string.Empty;
            string sStatusDesc = string.Empty;
            XmlAttribute objXmlAttribute = null;
            //Anu Tennyson MITS 18229 STARTS 12/21/2009
            string sFlatOrPercent = string.Empty;
            //Anu Tennyson MITS 18229 ENDS
            try
            {
                objCvgListElement = (XmlElement)objSysEx.SelectSingleNode("/SysExData/CoverageList");
                if (objCvgListElement != null)
                    objCvgListElement.ParentNode.RemoveChild(objCvgListElement);

                base.CreateElement(objSysEx.DocumentElement, "CoverageList", ref objCvgListElement);

                // Create HEADER nodes for Grid.
                base.CreateElement(objCvgListElement, "listhead", ref objListHeadXmlElement);


                base.CreateAndSetElement(objListHeadXmlElement, "Type", "Type");
                base.CreateAndSetElement(objListHeadXmlElement, "Description", "Description");
                base.CreateAndSetElement(objListHeadXmlElement, "Status", "Status");
                base.CreateAndSetElement(objListHeadXmlElement, "PolicyLimit", "Policy Limit");
                base.CreateAndSetElement(objListHeadXmlElement, "ClaimLimit", "Claim Limit");
                //Start: Neha Suresh Jain, 04/27/2010, MITS:20473, Occurrence wrongly spelt
                //base.CreateAndSetElement(objListHeadXmlElement, "OccurenceLimit", "Occurence Limit");
                base.CreateAndSetElement(objListHeadXmlElement, "OccurenceLimit", "Occurrence Limit");
                //End: Neha Suresh Jain, 04/27/2010, MITS:20473                
                base.CreateAndSetElement(objListHeadXmlElement, "Deductible", "Deductible");
                base.CreateAndSetElement(objListHeadXmlElement, "SIR", "SIR");
                base.CreateAndSetElement(objListHeadXmlElement, "AmendedDate", "Amended Date");
                base.CreateAndSetElement(objListHeadXmlElement, "EffectiveDate", "Effective Date");
                base.CreateAndSetElement(objListHeadXmlElement, "ExpirationDate", "Expiration Date");
                //Anu Tennyson MITS 18229 Starts 12/21/2009
                //Deb 
                //base.CreateAndSetElement(objListHeadXmlElement, "FlatOrPercent", "Flat Or Percent");
				//Anu Tennyson MITS 18229 Ends

                // Initialize the Sorted Lists
                objPositiveEntries = new SortedList();
                objNegativeEntries = new SortedList();

                foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                {
                    //Parijat:MITS 9887 .
                    //Since the newly added coverages have TransactionId as 0 in case of quote.
                    //but have fixed parent TransactionId in case of Policy (p_iTransNumber).
                    if (objPolicyXCvgEnh.TransactionId == p_iTransNumber || objPolicyXCvgEnh.TransactionId == 0)
                    //pmahli - 5/30/2007 Commented or Condition || objPolicyXCvgEnh.PolcvgRowId <= 0
                    //if (objPolicyXCvgEnh.TransactionId == p_iTransNumber || objPolicyXCvgEnh.PolcvgRowId <= 0)
                    {
                        base.CreateElement(objCvgListElement, "option", ref objOptionXmlElement);
                        objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SYSEXDATA_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");

                        base.LocalCache.GetCodeInfo(objPolicyXCvgEnh.CoverageTypeCode, ref sCvgCode, ref sCvgDesc);
                        base.LocalCache.GetCodeInfo(objPolicyXCvgEnh.Status, ref sStatusCode, ref sStatusDesc);

                        base.CreateAndSetElement(objOptionXmlElement, "Type", sCvgCode);
                        base.CreateAndSetElement(objOptionXmlElement, "Description", sCvgDesc);
                        base.CreateAndSetElement(objOptionXmlElement, "Status", sStatusDesc);
                        base.CreateAndSetElement(objOptionXmlElement, "PolicyLimit", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXCvgEnh.PolicyLimit, base.ConnectionString, ClientId));
                        base.CreateAndSetElement(objOptionXmlElement, "ClaimLimit", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXCvgEnh.ClaimLimit, base.ConnectionString, ClientId));
                        base.CreateAndSetElement(objOptionXmlElement, "OccurenceLimit", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXCvgEnh.OccurrenceLimit, base.ConnectionString, ClientId));
                        //Anu Tennyson MITS 18229 Starts 12/21/2009
                        sFlatOrPercent = base.LocalCache.GetShortCode(objPolicyXCvgEnh.FlatOrPercent);
                        //Start: Neha Suresh Jain, 05/11/2010,MITS 20772, code commented as deductible wil always be displayed as dollar amount
                        //if (sFlatOrPercent == "P")
                        //{
                        //    base.CreateAndSetElement(objOptionXmlElement, "Deductible", objPolicyXCvgEnh.Deductible + "%");
                        //}
                        //else
                        //{
                        base.CreateAndSetElement(objOptionXmlElement, "Deductible", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXCvgEnh.Deductible, base.ConnectionString, ClientId));
                        //}
                            //End:Neha Suresh Jain, 05/11/2010
                        //Anu Tennyson MITS 18229 Ends
                        base.CreateAndSetElement(objOptionXmlElement, "SIR", CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), objPolicyXCvgEnh.SelfInsRetention, base.ConnectionString, ClientId));
                        base.CreateAndSetElement(objOptionXmlElement, "AmendedDate", Conversion.GetDBDateFormat(objPolicyXCvgEnh.AmendedDate, "MM/dd/yyyy"));
                        base.CreateAndSetElement(objOptionXmlElement, "EffectiveDate", Conversion.GetDBDateFormat(objPolicyXCvgEnh.EffectiveDate, "MM/dd/yyyy"));
                        base.CreateAndSetElement(objOptionXmlElement, "ExpirationDate", Conversion.GetDBDateFormat(objPolicyXCvgEnh.ExpirationDate, "MM/dd/yyyy"));
						//Anu Tennyson MITS 18229 Starts 12/21/2009
                        base.CreateAndSetElement(objOptionXmlElement, "FlatOrPercent", sFlatOrPercent);
                        base.CreateAndSetElement(objOptionXmlElement, "DeductibleRangeRowId", objPolicyXCvgEnh.ExpRateRowId.ToString());
                        base.CreateAndSetElement(objOptionXmlElement, "Modifier", objPolicyXCvgEnh.Modifier.ToString());                       
						//Anu Tennyson MITS 18229 Ends
                        //Start: Neha Suresh Jain,05/14/2010,MITS 20772
                        base.CreateAndSetElement(objOptionXmlElement, "SelectedDeductible", objPolicyXCvgEnh.SelectedDeductible.ToString());
                        ////End:Neha Suresh Jain
                        string sSessionId = string.Empty;
                        if (p_objUniqueIds.ContainsKey(objPolicyXCvgEnh.PolcvgRowId))
                            sSessionId = (string)p_objUniqueIds[objPolicyXCvgEnh.PolcvgRowId];

                        base.CreateAndSetElement(objOptionXmlElement, "SessionId", sSessionId);
                        base.CreateAndSetElement(objOptionXmlElement, "CoveragesId", objPolicyXCvgEnh.PolcvgRowId.ToString());
                        if (objPolicyXCvgEnh.PolcvgRowId > 0)
                        {
                            objPositiveEntries.Add(objPolicyXCvgEnh.PolcvgRowId, objOptionXmlElement);
                            bSortingNecessary = true;
                        }
                        else if (objPolicyXCvgEnh.PolcvgRowId < 0)
                        {
                            objNegativeEntries.Add(objPolicyXCvgEnh.PolcvgRowId, objOptionXmlElement);
                            bSortingNecessary = true;
                        }

                    }
                }

                // Sort the XML based on the Exposure Id. The Exposure Id which are Positive are taken to be saved earlier,
                // The Negative ones are getting Inserted now, soe we need to have Exposure Id in the Order 1,2,3 -1,-2,-3
                if (bSortingNecessary)
                    base.SortXml(ref objCvgListElement, objPositiveEntries, objNegativeEntries);

                base.ResetSysExData(objSysEx, "SelectedCoveragesId", "");
                base.ResetSysExData(objSysEx, "NewAddedCoveragesSessionId", "");
                base.ResetSysExData(objSysEx, "DeletedRowCoveragesId", "");
                base.ResetSysExData(objSysEx,"SelectedPolcvgRowId", "");
                base.ResetSysExData(objSysEx, "CoverageCount", iIndex.ToString());

                // npadhy Changing the Location of this Code as this is increasing the index. And this will Change the Exposure Count
                //mona-start
                base.CreateElement(objCvgListElement, "option", ref objOptionXmlElement);
                objOptionXmlElement.SetAttribute("ref", Constants.INSTANCE_SYSEXDATA_PATH + objOptionXmlElement.ParentNode.LocalName + "/option[" + (++iIndex).ToString() + "]");
                objXmlAttribute = objSysEx.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objOptionXmlElement.Attributes.Append(objXmlAttribute);
                //objXmlAttribute = null;

                base.CreateAndSetElement(objOptionXmlElement, "Type", "");
                base.CreateAndSetElement(objOptionXmlElement, "Description", "");
                base.CreateAndSetElement(objOptionXmlElement, "Status", "");
                base.CreateAndSetElement(objOptionXmlElement, "PolicyLimit", string.Format("{0:C}", 0));
                base.CreateAndSetElement(objOptionXmlElement, "ClaimLimit", string.Format("{0:C}", 0));
                base.CreateAndSetElement(objOptionXmlElement, "OccurenceLimit", string.Format("{0:C}", 0));
                base.CreateAndSetElement(objOptionXmlElement, "Deductible", string.Format("{0:C}", 0));
                base.CreateAndSetElement(objOptionXmlElement, "SIR", string.Format("{0:C}", 0));
                base.CreateAndSetElement(objOptionXmlElement, "AmendedDate", "");
                base.CreateAndSetElement(objOptionXmlElement, "EffectiveDate", "");
                base.CreateAndSetElement(objOptionXmlElement, "ExpirationDate", "");
                base.CreateAndSetElement(objOptionXmlElement, "FlatOrPercent", "");
                base.CreateAndSetElement(objOptionXmlElement, "SessionId", "");
                base.CreateAndSetElement(objOptionXmlElement, "CoveragesId", "0");

               

                //mona-end
            }
            finally
            {
                objCvgListElement = null;
                objListHeadXmlElement = null;
                objOptionXmlElement = null;
                objXmlAttribute = null;

            }
        }
        //Anu Tennyson for MITS 18229 Starts 12/23/2009 - Commented
        //public XmlDocument GetCoverages(SessionManager objSessionManager, int p_iCoveragesId, string p_sSessionId, int p_iPolicyId,
        //            int p_iTransNumber, string p_sEffDate, string p_sExpDate)
        //Anu Tennyson for MITS 18229 Ends 12/23/2009 - Commented
        public XmlDocument GetCoverages(SessionManager objSessionManager, int p_iCoveragesId, string p_sSessionId, int p_iPolicyId,
                    int p_iTransNumber, string p_sEffDate, string p_sExpDate, int iState, string sLob, string sOrg, string sMode)
        {
            PolicyXCvgEnh objPolicyXCvgEnh = null;
            PolicyEnh objPolicyEnh = null;
            DbReader objReader = null;
            PolicyCoveragesUITemp structPolicyCoverage = new PolicyCoveragesUITemp();
            //string sSysUserName = "";      
            XmlElement objXMLElement = null;

            string sSQL = string.Empty;
            string sCvgEffDate = string.Empty; //pmahli MITS 9842 6/21/2007

            objPolicyXCvgEnh = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
            objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);

            if (! string.IsNullOrEmpty(p_sSessionId))
            {
                // Load from the session.
                string sSessionRawData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(p_sSessionId)).ToString();

                XmlDocument objPropertyStore = new XmlDocument();
                objPropertyStore.LoadXml(sSessionRawData);

                objPolicyXCvgEnh.PopulateObject(objPropertyStore);
            }
            else
            {
                // Load from the database.
                if (p_iCoveragesId != 0)
                    objPolicyXCvgEnh.MoveTo(p_iCoveragesId);
            }
            //Geeta 10/24/07 : Customized User List Enhancement                                            
            //string sSQLCUL = "SELECT USER_TABLE.USER_ID,USER_TABLE.FIRST_NAME,USER_TABLE.LAST_NAME," +
            //"USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID" +
            //" AND USER_DETAILS_TABLE.DSNID = " + m_lDSNId + " AND USER_TABLE.USER_ID =" + objPolicyXCvgEnh.NotificationUid + "ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME";

            //using (DbReader objRdr = DbFactory.GetDbReader(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(0), sSQLCUL))
            //{
            //    while (objRdr.Read())
            //    {
            //        sSysUserName = Conversion.ConvertObjToStr(objRdr.GetValue(1)) + " " + Conversion.ConvertObjToStr(objRdr.GetValue(2)) + " " + Conversion.ConvertObjToStr(objRdr.GetValue(3));
            //    }
            //    objRdr.Close();
            //}            
           
            //pmahli 3/22/2007 as the id with be over written by data model class
            //objPolicyXCvgEnh.TransactionId = p_iTransNumber;
            objPolicyXCvgEnh.PolicyId = p_iPolicyId;

            if (p_iPolicyId > 0)
            {
                objPolicyEnh.MoveTo(p_iPolicyId);
            }
            sCvgEffDate = objPolicyXCvgEnh.EffectiveDate.ToString(); //pmahli MITS 9842 6/21/2007
            string sPolicyStatus = "";
            string sTermEffectiveDate = "";
            string sTermExpirationDate = "";
            int iTransactionType = 0;
            string sTransactionType = "";
            string sTransactionStatus = "";
            string sTransactionDate = "";
            // Start Naresh MITS 9773 Amend Date was not coming in Coverages
            string sAmendDate = "";
            // End Naresh MITS 9773 Amend Date was not coming in Coverages
            int iTermNumber = 0;
            int iTermSeq = 0;
            string sCancelDate = "";

            if (objPolicyEnh.PolicyXTransEnhList.Count == 0)
            {
                if (objPolicyEnh.PolicyId == 0)
                    sPolicyStatus = "Q";
                else
                {
                    sPolicyStatus = base.CCacheFunctions.GetShortCode(objPolicyEnh.PolicyStatusCode);
                }
                sTermEffectiveDate = Conversion.GetDate(p_sEffDate);
                sTermExpirationDate = Conversion.GetDate(p_sExpDate);
            }
            else
            {
                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                {
                    if (objPolicyXTransEnh.TransactionId == p_iTransNumber)
                    {
                        iTransactionType = objPolicyXTransEnh.TransactionType;
                        sPolicyStatus = base.CCacheFunctions.GetShortCode(objPolicyXTransEnh.PolicyStatus);
                        sTransactionType = base.CCacheFunctions.GetShortCode(iTransactionType);
                        sTransactionStatus = base.CCacheFunctions.GetShortCode(objPolicyXTransEnh.TransactionStatus);
                        // Start Naresh The Effective Date was not coming Correct in Coverages
                        sTransactionDate = objPolicyXTransEnh.EffectiveDate;
                        // Start Naresh MITS 9773 Amend Date was not coming in Coverages
                        sAmendDate = objPolicyXTransEnh.EffectiveDate;
                        // End Naresh MITS 9773 Amend Date was not coming in Coverages
                        iTermNumber = objPolicyXTransEnh.TermNumber;
                        iTermSeq = objPolicyXTransEnh.TermSeqAlpha;
                    }
                }
                // Changed as Per Exposure
                sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + objPolicyEnh.PolicyId
                    + " AND TERM_NUMBER = " + iTermNumber + " AND SEQUENCE_ALPHA = " + iTermSeq;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    sTermEffectiveDate = objReader.GetString("EFFECTIVE_DATE");
                    sTermExpirationDate = objReader.GetString("EXPIRATION_DATE");
                    sCancelDate = objReader.GetString("CANCEL_DATE");
                }
                objReader.Close();
            }

            EnableControls(ref structPolicyCoverage);
            if (sTransactionType == "AU")
            {
                if (sTransactionStatus == "PR")
                {
                    EnableControls(ref structPolicyCoverage);
                    if (objPolicyXCvgEnh.CoverageTypeCode != 0)
                    {
                        structPolicyCoverage.CoverageTypeCodeEnabled = false;
                    }
                    objPolicyXCvgEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                    if (sCvgEffDate == "") //pmahli MITS 9842 6/21/2007
                    {
                        structPolicyCoverage.CoverageTypeCodeEnabled = true;
                        objPolicyXCvgEnh.TermNumber = iTermNumber;
                        objPolicyXCvgEnh.TransactionId = p_iTransNumber;
                        objPolicyXCvgEnh.PolicyId = objPolicyEnh.PolicyId;
                        objPolicyXCvgEnh.CovRenewableFlag = true;
                        objPolicyXCvgEnh.TransactionType = iTransactionType;
                        objPolicyXCvgEnh.SequenceAlpha = 1;
                        if (sCancelDate != "")
                        {
                            objPolicyXCvgEnh.ExpirationDate = sCancelDate;
                        }
                        else
                        {
                            objPolicyXCvgEnh.ExpirationDate = sTermExpirationDate;
                        }
                    }
                    // Start Naresh Add the Exposure Count 
                    // Putting the Exposure Count from Datamodel was incrementing the 
                    // Exposure Count at all times, during Convert to Policy etc.
                    if (objPolicyXCvgEnh.PolcvgRowId == 0)
                    {
                        objPolicyXCvgEnh.CoverageNumber = GetCvgCount(objPolicyEnh);
                    }
                    // End Naresh Exposure Count
                    if (objPolicyXCvgEnh.SequenceAlpha == 1)
                    {
                        structPolicyCoverage.EffDateEnabled = true;
                        structPolicyCoverage.ExpDateEnabled = true;
                    }
                    else
                    {
                        structPolicyCoverage.EffDateEnabled = false;
                        structPolicyCoverage.ExpDateEnabled = false;
                    }
                }
                else if (sTransactionStatus == "OK")
                {
                    DisableControls(ref structPolicyCoverage);
                    structPolicyCoverage.EffDateEnabled = false;
                    structPolicyCoverage.ExpDateEnabled = false;
                }
            }
            else
            {
                switch (sPolicyStatus)
                {
                    // Converted
                    case "CV":
                        DisableControls(ref structPolicyCoverage);
                        structPolicyCoverage.EffDateEnabled = false;
                        structPolicyCoverage.ExpDateEnabled = false;
                        break;
                    // New or Quote
                    case "Q":
                        objPolicyXCvgEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                        objPolicyXCvgEnh.EffectiveDate = sTermEffectiveDate;
                        objPolicyXCvgEnh.ExpirationDate = sTermExpirationDate;
                        EnableControls(ref structPolicyCoverage);
                        structPolicyCoverage.EffDateEnabled = false;
                        structPolicyCoverage.ExpDateEnabled = false;
                        break;
                    // In Effect
                    case "I":
                        if ((sTransactionType == "EN") &&
                           (sTransactionStatus == "PR") &&
                           (objPolicyXCvgEnh.Status != base.CCacheFunctions.GetCodeIDWithShort(SC_DELETED, TBL_COVEXP)))
                        {
                            EnableControls(ref structPolicyCoverage);
                            structPolicyCoverage.CoverageTypeCodeEnabled = false;
                            objPolicyXCvgEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                            if (objPolicyXCvgEnh.EffectiveDate == "")
                            {
                                structPolicyCoverage.CoverageTypeCodeEnabled = true;
                                objPolicyXCvgEnh.EffectiveDate = sTransactionDate;
                                objPolicyXCvgEnh.ExpirationDate = sTermExpirationDate;
                                objPolicyXCvgEnh.TermNumber = iTermNumber;
                                objPolicyXCvgEnh.CovRenewableFlag = true;
                                objPolicyXCvgEnh.TransactionType = iTransactionType;
                                objPolicyXCvgEnh.SequenceAlpha = 1;
                                objPolicyXCvgEnh.TransactionId = p_iTransNumber;
                                objPolicyXCvgEnh.PolicyId = objPolicyEnh.PolicyId;
                            }
                            // Start Naresh Add the Exposure Count 
                            // Putting the Exposure Count from Datamodel was incrementing the 
                            // Exposure Count at all times, during Convert to Policy etc.
                            if (objPolicyXCvgEnh.PolcvgRowId == 0)
                            {
                                // MITS 9770 Reopen Fixed
                                sAmendDate = "";
                                objPolicyXCvgEnh.CoverageNumber = GetCvgCount(objPolicyEnh);
                            }
                            // MITS 9770 Reopen Fixed
                            else
                            {
                                if (LocalCache.GetShortCode(objPolicyXCvgEnh.TransactionType) == "EN" && objPolicyXCvgEnh.SequenceAlpha == 1)
                                    sAmendDate = "";
                            }
                            // End Naresh Exposure Count
                        }
                        else
                        {
                            DisableControls(ref structPolicyCoverage);
                        }
                        structPolicyCoverage.EffDateEnabled = false;
                        structPolicyCoverage.ExpDateEnabled = false;
                        break;
                    // Provisionally Renewed
                    case "PR":
                        EnableControls(ref structPolicyCoverage);
                        if (objPolicyXCvgEnh.Status == 0)
                        {
                            objPolicyXCvgEnh.Status = base.CCacheFunctions.GetCodeIDWithShort(SC_PROVISIONAL, TBL_COVEXP);
                        }
                        if (objPolicyXCvgEnh.EffectiveDate == "")
                        {
                            objPolicyXCvgEnh.EffectiveDate = sTermEffectiveDate;
                            objPolicyXCvgEnh.ExpirationDate = sTermExpirationDate;
                            objPolicyXCvgEnh.TermNumber = iTermNumber;
                            objPolicyXCvgEnh.CovRenewableFlag = true;
                            objPolicyXCvgEnh.TransactionType = iTransactionType;
                            objPolicyXCvgEnh.SequenceAlpha = 1;
                            objPolicyXCvgEnh.TransactionId = p_iTransNumber;
                            objPolicyXCvgEnh.PolicyId = objPolicyEnh.PolicyId;
                        }
                        // Start Naresh Add the Coverage Number 
                        // Putting the Coverage Number from Datamodel was incrementing the 
                        // Coverage Number at all times, during Convert to Policy etc.
                        if (objPolicyXCvgEnh.PolcvgRowId == 0)
                        {
                            objPolicyXCvgEnh.CoverageNumber = GetCvgCount(objPolicyEnh);
                        }
                        // End Naresh Coverage Number
                        structPolicyCoverage.EffDateEnabled = false;
                        structPolicyCoverage.ExpDateEnabled = false;
                        break;
                    case "C":
                    case "R":
                    case "E":
                    default:
                        DisableControls(ref structPolicyCoverage);
                        structPolicyCoverage.EffDateEnabled = false;
                        structPolicyCoverage.ExpDateEnabled = false;
                        break;
                }
                //structPolicyCoverage.OkEnabled = false;
                // Naresh Code Commented for Do Not Bill Change
                //sEssp = "";//base.Essp;
                //if (sEssp == "1784")
                //{
                //    objPolicyXCvgEnh.CoverageTypeCode = base.CCacheFunctions.GetCodeIDWithShort("PL", "COVERAGE_TYPE");
                //}
            }
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;
            XmlElement objSuppNode = null;
            XmlElement objCoveragesNode = null;
            XmlElement objCoveragesUI = null;

            base.StartDocument(ref objReturnDoc, ref objRoot, "Coverages");
            base.CreateElement(objRoot, "SuppView", ref objSuppNode);
            base.CreateElement(objRoot, "CoveragesData", ref objCoveragesNode);

            base.CreateElement(objRoot, "CoverageUIStatus", ref objCoveragesUI);
            base.CreateAndSetElement(objCoveragesUI, "ExceptionEnable", structPolicyCoverage.ExceptionsEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "RemarksEnable", structPolicyCoverage.RemarksEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "CovTypeCodeEnable", structPolicyCoverage.CoverageTypeCodeEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "CoverageStatusEnable", structPolicyCoverage.CoverageStatusEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "EffDateEnable", structPolicyCoverage.EffDateEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "ExpDateEnable", structPolicyCoverage.ExpDateEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "PolicyLimitEnable", structPolicyCoverage.PolicyLimitEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "OccuranceLimitEnable", structPolicyCoverage.OccurrenceLimitEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "ClaimLimitEnable", structPolicyCoverage.ClaimLimitEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "TotalPaymentsEnable", structPolicyCoverage.TotalPaymentsEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "DeductibleEnable", structPolicyCoverage.DeductiblelEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "SelfInsuredRetentionEnable", structPolicyCoverage.SelfInsuredRetentionEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "NextPolicyIDEnable", structPolicyCoverage.NextPolicyIDEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "CancelNoticeDaysEnable", structPolicyCoverage.CancelNoticeDaysEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "NotificationEIdEnable", structPolicyCoverage.NotificationEIdEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "BrokerLastNameEnable", structPolicyCoverage.BrokerLastNameEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "OkEnable", structPolicyCoverage.OkEnabled.ToString());
            base.CreateAndSetElement(objCoveragesUI, "CancelEnable", structPolicyCoverage.CancelEnabled.ToString());
            //Anu Tennyson for MITS 18229 UAR List Added
            base.CreateAndSetElement(objCoveragesUI, "UARaddedlistEnabled", structPolicyCoverage.UARaddedlistEnabled.ToString());
            //Anu Tennyson for MITS 18229 UAR List Added
            //base.CreateAndSetElement(objCoveragesUI, "RMSysUserName", sSysUserName);

            // Start Naresh MITS 9773 Amend Date was not coming in Coverages
            // MITS 9770 Reopen Fixed
            if (sAmendDate != "")
                base.CreateAndSetElement(objCoveragesUI, "AmendDate", Conversion.ToDate(sAmendDate).ToString("MM/dd/yyyy"));
            else
                base.CreateAndSetElement(objCoveragesUI, "AmendDate", "");
            // End Naresh MITS 9773 Amend Date was not coming in Coverages
            base.CreateAndSetElement(objCoveragesUI, "TransactionType", sTransactionType);

            XmlDocument objSupp = objPolicyXCvgEnh.Supplementals.ViewXml;

            foreach (XmlNode objNode in objSupp.SelectNodes("//control"))
            {
                XmlAttribute objRefAttribute = objNode.Attributes["ref"];
                if (objRefAttribute != null)
                {
                    objRefAttribute.InnerText = objRefAttribute.InnerText.Replace("/Instance/*/Supplementals", "/Instance/Document/Coverages/CoveragesData/PolicyXCvgEnh/Supplementals");
                }
                //Raman : Adding an onblur event to enable UI Ok button on all supplemental controls
                objXMLElement = (XmlElement)objNode;
                if (sPolicyStatus == "Q")
                {
                    objXMLElement.SetAttribute("readonly", "false");
                }
                else
                {
                    if ((sTransactionType == "AU" || sTransactionType == "EN" || sTransactionType == "RN") && sTransactionStatus == "PR")
                    {
                        objXMLElement.SetAttribute("readonly", "false");
                    }
                    else
                    {
                        objXMLElement.SetAttribute("readonly", "true");
                    }
                }
                objXMLElement.SetAttribute("onchange", "CoveragesEnableOk('EnableAmend',this);setDataChanged(true);");
            }

            XmlDocument objCoverages = new XmlDocument();
            XmlDocument objSerilzationXMl = new XmlDocument();
            //Geeta 06/21/07 : Modified for MITS 9814
            objSerilzationXMl.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
            objCoverages.LoadXml(objPolicyXCvgEnh.SerializeObject(objSerilzationXMl));
            //Shruti for 9684
            XmlElement objTemNode = (XmlElement)objCoverages.SelectSingleNode("//PolicyXCvgEnh");
            XmlElement objTemElement = objCoverages.CreateElement("NextPolicyName");
            sSQL = " SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + objPolicyXCvgEnh.NextPolicyId;
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
            {
                objTemElement.InnerText = objReader.GetString("POLICY_NAME");
            }
            objReader.Close();
            objTemNode.AppendChild(objTemElement);
            // Start Naresh Issue with Next Policy Id fixed. When it was getting picked from Session
            objTemElement = objCoverages.CreateElement("PrevNextPolicyId");
            objTemElement.InnerText = objPolicyXCvgEnh.NextPolicyId.ToString();
            objTemNode.AppendChild(objTemElement);
            // End Naresh Issue with Next Policy Id fixed. When it was getting picked from Session
            //Shruti for 9684 ends
            objSuppNode.AppendChild(objReturnDoc.ImportNode((XmlNode)objSupp.DocumentElement, true));
            objCoveragesNode.AppendChild(objReturnDoc.ImportNode((XmlNode)objCoverages.DocumentElement, true));

            //Anu Tennyson MITS 18229 Starts 12/21/2009
            if (sMode == "edit")
            {
                bool bIsSuccess = false;
                string sCoveragesID = objReturnDoc.SelectSingleNode("/Coverages/CoveragesData/PolicyXCvgEnh/CoverageTypeCode").Attributes["codeid"].Value;
                int iCoveragesID = Conversion.CastToType<int>(sCoveragesID, out bIsSuccess);

                XmlDocument objXmlDoc = null;
                XmlElement objXmlRoot = null;
                XmlElement objCoverageUI = null;
                XmlDocument objDoc = null;
                string sFlatOrPercent = string.Empty;

                sFlatOrPercent = LocalCache.GetShortCode(objPolicyXCvgEnh.FlatOrPercent);
                base.StartDocument(ref objXmlDoc, ref objXmlRoot, "Coverages");
                base.CreateElement(objXmlRoot, "CoveragesData", ref objCoverageUI);
                base.CreateAndSetElement(objCoverageUI, "State", iState.ToString());
                base.CreateAndSetElement(objCoverageUI, "ORG", sOrg);
                base.CreateAndSetElement(objCoverageUI, "LOB", sLob);
                base.CreateAndSetElement(objCoverageUI, "EffDate", sTermEffectiveDate);
                base.CreateAndSetElement(objCoverageUI, "ExpDate", sTermExpirationDate);
                base.CreateElement(objXmlRoot, "CoverageTypeCode", ref objCoverageUI);
                objCoverageUI.SetAttribute("codeid", sCoveragesID);

                objDoc = CalculateDeductible(objSessionManager, objXmlDoc);
                if (objDoc.SelectSingleNode("//Coverages/Deductible/Range") != null)
                {
                    base.CreateAndSetElement(objReturnDoc.DocumentElement, "DeductibleBtn", "true");
                }
                else
                {
                    base.CreateAndSetElement(objReturnDoc.DocumentElement, "DeductibleBtn", "false");
                }
                if (objDoc.SelectSingleNode("//Coverages/Deductible/Amount") != null)
                {
                    string sAmount = objDoc.SelectSingleNode("//Coverages/Deductible/Amount").InnerText;
                    int iAmount = Conversion.CastToType<int>(sAmount, out bIsSuccess);
                    if (iAmount == 0)
                    {
                        base.CreateAndSetElement(objReturnDoc.DocumentElement, "DedPresent", "false");
                    }
                }
                base.CreateAndSetElement(objReturnDoc.DocumentElement, "FlatOrPercent", sFlatOrPercent);
               
            }
            //Manish multicurrency
            base.LocalCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), ref sShortCode, ref sDesc);
            sCurrency = sDesc.Split('|')[1];
            base.CreateAndSetElement(objRoot, "BaseCurrencyType", sCurrency);
            //Anu Tennyson MITS 18229 ENDS
            return objReturnDoc;

        }
        public XmlDocument SetCoverages(SessionManager objSessionManager, XmlDocument p_objPropertyStore)
        {
            try
            {
                // npadhy Start MITS 20721 When user Audits a Policy - user can enter Effective data for exposure beyond the term of the Policy
                //string sEffDateUI = p_objPropertyStore.SelectSingleNode("//Coverages/EffDate").InnerText;
                //string sExpDateUI = p_objPropertyStore.SelectSingleNode("//Coverages/ExpDate").InnerText;

                string sEffDateUI = p_objPropertyStore.SelectSingleNode("//EffectiveDate").InnerText;  
                string sExpDateUI = p_objPropertyStore.SelectSingleNode("//ExpirationDate").InnerText; 
                // npadhy End MITS 20721 When user Audits a Policy - user can enter Effective data for exposure beyond the term of the Policy

                int iTransId = Convert.ToInt32(p_objPropertyStore.SelectSingleNode("//Coverages/TransId").InnerText);
                int iPolId = Convert.ToInt32(p_objPropertyStore.SelectSingleNode("//Coverages/PolId").InnerText);
                XmlDocument p_objPropertyStoreNew = new XmlDocument();

                p_objPropertyStoreNew.LoadXml(p_objPropertyStore.SelectSingleNode("//Coverages/CoveragesData/PolicyXCvgEnh").OuterXml);

                XmlDocument objReturnDoc = null;
                XmlElement objRoot = null;
                base.StartDocument(ref objReturnDoc, ref objRoot, "SetCoverages");
                //  Shruti for MITS 9684
                XmlElement objTempParent = (XmlElement)p_objPropertyStoreNew.SelectSingleNode("//PolicyXCvgEnh");
                XmlNode objTempNode = p_objPropertyStoreNew.SelectSingleNode("//NextPolicyName");
                if (objTempNode != null)
                {
                    objTempParent.RemoveChild(objTempNode);
                }

                // Start Naresh Issue with Next Policy Id fixed. When it was getting picked from Session
                objTempNode = p_objPropertyStoreNew.SelectSingleNode("//NextPolicyId");

                XmlNode objTempNodePrev = p_objPropertyStoreNew.SelectSingleNode("//PrevNextPolicyId");
                if (objTempNodePrev != null)
                {
                    if (objTempNode.InnerText == "")
                    {
                        objTempNode.InnerText = objTempNodePrev.InnerText;
                    }
                    objTempParent.RemoveChild(objTempNodePrev);
                }
                // End Naresh Issue with Next Policy Id fixed. When it was getting picked from Session
                //Shruti for MITS 9684 ends
                this.ValidateFields(p_objPropertyStoreNew, sEffDateUI, sExpDateUI, iTransId, iPolId);

                string sUniqueId = Utilities.GenerateGuid();
                objSessionManager.SetBinaryItem(sUniqueId, Utilities.BinarySerialize(p_objPropertyStoreNew.OuterXml), base.ClientId);

                base.CreateAndSetElement(objRoot, "SessionId", sUniqueId);
                return objReturnDoc;
            }
            // Added catch block npadhy for MITS 20721 , 20722 : Start
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Coverage.SetCoverage.Error", base.ClientId), p_objEx);
            }
            // Added catch block npadhy for MITS 20721 , 20722 : End
            finally
            {
            }
        }
        private void DisableControls(ref PolicyCoveragesUITemp p_structPolicyCoverages)
        {
            p_structPolicyCoverages.ExceptionsEnabled = false;
            p_structPolicyCoverages.OkEnabled = false;
            p_structPolicyCoverages.RemarksEnabled = false;
            p_structPolicyCoverages.BrokerLastNameEnabled = false;
            p_structPolicyCoverages.CancelNoticeDaysEnabled = false;
            p_structPolicyCoverages.ClaimLimitEnabled = false;
            p_structPolicyCoverages.CoverageTypeCodeEnabled = false;
            p_structPolicyCoverages.CoverageStatusEnabled = false;
            p_structPolicyCoverages.DeductiblelEnabled = false;
            p_structPolicyCoverages.NextPolicyIDEnabled = false;
            p_structPolicyCoverages.NotificationEIdEnabled = false;
            p_structPolicyCoverages.OccurrenceLimitEnabled = false;
            p_structPolicyCoverages.PolicyLimitEnabled = false;
            p_structPolicyCoverages.SelfInsuredRetentionEnabled = false;
            p_structPolicyCoverages.TotalPaymentsEnabled = false;
            //Anu Tennyson for MITS 18229 UAR List Start
            p_structPolicyCoverages.UARaddedlistEnabled = false;
            //Anu Tennyson for MITS 18229 UAR List End
        }
        private void EnableControls(ref PolicyCoveragesUITemp p_structPolicyCoverages)
        {
            p_structPolicyCoverages.CancelEnabled = true;
            p_structPolicyCoverages.ExceptionsEnabled = true;
            p_structPolicyCoverages.OkEnabled = true;
            p_structPolicyCoverages.RemarksEnabled = true;
            p_structPolicyCoverages.BrokerLastNameEnabled = true;
            p_structPolicyCoverages.CancelNoticeDaysEnabled = true;
            p_structPolicyCoverages.ClaimLimitEnabled = true;
            p_structPolicyCoverages.CoverageTypeCodeEnabled = true;
            p_structPolicyCoverages.DeductiblelEnabled = true;
            p_structPolicyCoverages.NextPolicyIDEnabled = true;
            p_structPolicyCoverages.NotificationEIdEnabled = true;
            p_structPolicyCoverages.OccurrenceLimitEnabled = true;
            p_structPolicyCoverages.PolicyLimitEnabled = true;
            p_structPolicyCoverages.SelfInsuredRetentionEnabled = true;
            p_structPolicyCoverages.TotalPaymentsEnabled = true;
            //Anu Tennyson for MITS 18229 UAR List Start
            p_structPolicyCoverages.UARaddedlistEnabled = true;
            //Anu Tennyson for MITS 18229 UAR List End
        }
        private bool ValidateFields(XmlDocument objPropertyStore,string sEffDate , string sExpDate,int iTransactionId,int iPolicyId)
        {
            bool bValidate = false;
            PolicyEnh objPolicyEnh = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false); 
            PolicyXCvgEnh objPolicyXCvgEnh = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
            PolicyXTransEnh objPolicyXTransEnh = (PolicyXTransEnh)base.DataModelFactory.GetDataModelObject("PolicyXTransEnh", false);
            
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iTransIdMax = 0;
            string sStatus = string.Empty;

            DateTime objEffDateUI = System.DateTime.Today;
            DateTime objExpDateUI = System.DateTime.Today;
            DateTime objEffectiveDate = System.DateTime.Today;
            DateTime objExpirationDate = System.DateTime.Today;
            TimeSpan objDays;
            int iDays = 0;
            
            objPolicyEnh.MoveTo(iPolicyId);

            objPolicyXTransEnh.MoveTo(iTransactionId);
            int iTransactionType = objPolicyXTransEnh.TransactionType;
            string sTransactionType = base.CCacheFunctions.GetShortCode(iTransactionType);
            int iTermNumber = objPolicyXTransEnh.TermNumber;
            int iTermSeq = objPolicyXTransEnh.TermSeqAlpha;
            
            objPolicyXCvgEnh.PopulateObject(objPropertyStore);
          
            if (objPolicyEnh.PolicyXTransEnhList.Count == 0)
            {
                objEffectiveDate = Convert.ToDateTime(sEffDate);
                objExpirationDate = Convert.ToDateTime(sExpDate);
            }
            else
            {
                    sSQL = " SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + objPolicyEnh.PolicyId
                        + " AND TERM_NUMBER = " + iTermNumber + " AND SEQUENCE_ALPHA = " + iTermSeq;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader.Read())
                    {
                        objEffectiveDate  = Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetString("EFFECTIVE_DATE")));
                        objExpirationDate = Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetString("EXPIRATION_DATE")));
                    }
                    objReader.Close();
           }

            // npadhy Start MITS 20722 When the Effective Date or Expiration Date is blank, Throw the Validation Message
            if(!string.IsNullOrEmpty(sEffDate))
                objEffDateUI = Convert.ToDateTime(sEffDate);
            if (!string.IsNullOrEmpty(sExpDate))
                objExpDateUI = Convert.ToDateTime(sExpDate);
            // npadhy End MITS 20722 When the Effective Date or Expiration Date is blank, Throw the Validation Message
            objDays = objExpDateUI.Subtract(objEffDateUI);
            iDays = objDays.Days;

            //Geeta Customized User List Enhancement
            // npadhy 1/10/2008 This is not required as now the Ref of hidden control points to attribute code id directly
            //XmlNode objNodeTest = objPropertyStore.SelectSingleNode("//NotificationUid");            
            //if (objNodeTest != null)
            //{              
            //    objPropertyStore.SelectSingleNode("//NotificationUid").Attributes["codeid"].Value = objNodeTest.InnerText; 
            //}
    
            if (objPolicyXCvgEnh.CoverageTypeCode == 0)
            {
                throw new RMAppException(Globalization.GetString("CoverageManager.Validate.Status", base.ClientId));

            }
            else if (sEffDate == "")
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.EffectiveDate", base.ClientId));
            }
            else if (sExpDate == "")
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.ExpirationDate", base.ClientId));
            }
            else if (iDays < 0)
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.DateValidation1", base.ClientId));
            }
            else if ((objEffDateUI < objEffectiveDate) || (objEffDateUI > objExpirationDate))
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.DateValidation2", base.ClientId));
            }
            else if ((objExpDateUI < objEffectiveDate) || (objExpDateUI > objExpirationDate))
            {
                throw new RMAppException(Globalization.GetString("CvgExpManager.Validate.DateValidation3", base.ClientId));
            }
            else if (sTransactionType == "AU")
            {
                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + objPolicyXCvgEnh.PolicyId + " AND TERM_NUMBER = " + objPolicyXCvgEnh.TermNumber + "AND EFFECTIVE_DATE <=" + Conversion.ToDbDate(objEffDateUI) + " AND TRANSACTION_STATUS=" + base.CCacheFunctions.GetCodeIDWithShort("OK", "TRANSACTION_STATUS");
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iTransIdMax = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                sSQL = "SELECT POLICY_STATUS FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iTransIdMax;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                {
                    sStatus = base.CCacheFunctions.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    if (sStatus == "CF " || sStatus == "CPR")
                    {
                        throw new RMAppException(Globalization.GetString("CoverageManager.Validate.CoverageEffectiveDate", base.ClientId));
                    }
                }
                objReader.Close();
            }

            else
                bValidate = true;

            return bValidate;
        }
        // Start Naresh Added the Function here to Get the Coverage Number. 
        // In Datamodel this was creating Problem
        private int GetCvgCount(PolicyEnh p_objPolicyEnh)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            int Count = 0;
            sSQL = "SELECT MAX(COVERAGE_NUMBER) FROM POLICY_X_CVG_ENH WHERE POLICY_ID=" + p_objPolicyEnh.PolicyId;
            objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
            if (objReader.Read())
                Count = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
            foreach (PolicyXCvgEnh obj in p_objPolicyEnh.PolicyXCvgEnhList)
                Count = Math.Max(Count, obj.CoverageNumber);
            // Naresh Connection Leak Changes
            objReader.Close();
            return Count + 1;
        }
        // End Naresh Function ends
        //Anu Tennyson for MITS 18229 STARTS 12/23/2009
        /// <summary>
        /// Function to check when deductible button to be shown or not.
        /// </summary>
        /// <param name="objPropertyStore">XmlDocument which contains the information from the UI</param>
        /// <param name="objSessionManager">Session Manager (for handling session data)</param>
        /// <returns>XmlDocument objReturnDoc contains the information whether to show the button and childs of the clicked orgainzation</returns>
        public XmlDocument CheckDeductiblebtn(SessionManager objSessionManager, XmlDocument objPropertyStore)
        {
            StringBuilder sbSql = null;
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;
            XmlElement objCoverageUI = null;
            string sCalculateDeductible = string.Empty;
            int iDeductibleBtnGL = 0;
            int iDeductibleBtnGLTypE = 0;
            int iLobCodeId = 0;
            string sLob = string.Empty;
            bool bIsSuccess = false;
            string sBtnSet = "false";
            string sOrgFilter = string.Empty;
            string sSessionOrgData = string.Empty;
            string sOrg = string.Empty;
            string sEffdate = string.Empty;
            string sExpdate = string.Empty;
            string sDeductibleBtnGL = string.Empty;
            string sDeductibleBtnGLTypE = string.Empty;
            string sChildOrgHUniqueID = string.Empty;
            int iRateMaintenanceType = 0;
            int iStateId = 0;
            XmlDocument objChildOrgH = null;
          
            
            try
            {
                sCalculateDeductible = "false";
                if (objPropertyStore.SelectSingleNode("//Coverages/LOB") != null)
                {
                   sLob = objPropertyStore.SelectSingleNode("//Coverages/LOB").InnerText;
                }
                if (objPropertyStore.SelectSingleNode("//Coverages/Org") != null)
                {
                   sOrg = objPropertyStore.SelectSingleNode("//Coverages/Org").InnerText;
                }
                if (objPropertyStore.SelectSingleNode("//Coverages/EffectiveDate") != null)
                {
                   sEffdate = Conversion.GetDate(objPropertyStore.SelectSingleNode("//Coverages/EffectiveDate").InnerText);
                }
                if (objPropertyStore.SelectSingleNode("//Coverages/ExpirationDate") != null)
                {
                   sExpdate = Conversion.GetDate(objPropertyStore.SelectSingleNode("//Coverages/ExpirationDate").InnerText);
                }
                if (objPropertyStore.SelectSingleNode("//Coverages/State") != null)
                {
                   iStateId = Conversion.CastToType<int>(objPropertyStore.SelectSingleNode("//Coverages/State").InnerText, out bIsSuccess);
                }
                iLobCodeId = base.LocalCache.GetCodeId(sLob, "POLICY_LOB");
                sOrgFilter = base.OrgHierarchy(sOrg);
                sChildOrgHUniqueID = Utilities.GenerateGuid();
                objChildOrgH = new XmlDocument();
                sSessionOrgData = "<ChildOrgData>" + sOrgFilter + "</ChildOrgData>";
                objChildOrgH.LoadXml(sSessionOrgData);
                objSessionManager.SetBinaryItem(sChildOrgHUniqueID, Utilities.BinarySerialize(objChildOrgH.OuterXml), base.ClientId);
                base.StartDocument(ref objReturnDoc, ref objRoot, "Coverages");
                if (sLob == "GL" || sLob == "AL" || sLob == "PC")
                {
                    if (sLob == "GL")
                    {
                        sbSql = new StringBuilder();
                        iRateMaintenanceType = base.LocalCache.GetCodeId("E", CODE_TABLENAME);
                        sbSql.Append("SELECT COUNT(*) FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE = ");
                        sbSql.Append(iRateMaintenanceType);
                        sbSql.Append(" AND LINE_OF_BUSINESS = ");
                        sbSql.Append(iLobCodeId);
                        using (DbConnection objConn = DbFactory.GetDbConnection(base.ConnectionString))
                        {
                             objConn.Open();
                             sDeductibleBtnGLTypE = Convert.ToString(objConn.ExecuteScalar(Convert.ToString(sbSql)));
                             iDeductibleBtnGLTypE = Conversion.CastToType<int>(sDeductibleBtnGLTypE, out bIsSuccess);
                        }
                    }
                    if (iDeductibleBtnGLTypE != 0)
                    {
                        
                        base.CreateElement(objRoot, "ShowDeductibleBtnGL", ref objCoverageUI);
                        base.CreateAndSetElement(objCoverageUI, "ShowDeductibleBtnGLFlag", sBtnSet);
                        base.CreateAndSetElement(objCoverageUI, "CalculateDeductible", sCalculateDeductible);
                        base.CreateAndSetElement(objCoverageUI, "ChildOrgHierarchySessionID", sChildOrgHUniqueID);
                    }

                    if (iDeductibleBtnGLTypE == 0)
                    {
                        iRateMaintenanceType = base.LocalCache.GetCodeId("C", CODE_TABLENAME);

                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT COUNT(*) FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE = ");
                        sbSql.Append(iRateMaintenanceType);
                        sbSql.Append(" AND LINE_OF_BUSINESS = ");
                        sbSql.Append(iLobCodeId);
                        sbSql.Append(" AND	SYS_POL_EXP_RATES.STATE = ");
                        sbSql.Append(iStateId);
                        sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                        sbSql.Append(sEffdate);
                        sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                        sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                        sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                        sbSql.Append(sEffdate);
                        sbSql.Append("'))");
                        if (!(String.IsNullOrEmpty(sOrgFilter)))
                        {
                            sbSql.Append(" AND SYS_POL_EXP_RATES.ORG_HIERARCHY IN(");
                            sbSql.Append(sOrgFilter);
                            sbSql.Append(")");
                        }

                        using (DbConnection objConn = DbFactory.GetDbConnection(base.ConnectionString))
                        {
                            objConn.Open();
                            sDeductibleBtnGL = Convert.ToString(objConn.ExecuteScalar(Convert.ToString(sbSql)));
                            iDeductibleBtnGL = Conversion.CastToType<int>(sDeductibleBtnGL, out bIsSuccess);
                        }
                        if (iDeductibleBtnGL != 0)
                        {
                            sBtnSet = "true";
                            sCalculateDeductible = "true";
                        }

                        base.CreateElement(objRoot, "ShowDeductibleBtnGL", ref objCoverageUI);
                        base.CreateAndSetElement(objCoverageUI, "ShowDeductibleBtnGLFlag", sBtnSet);
                        base.CreateAndSetElement(objCoverageUI, "CalculateDeductible", sCalculateDeductible);
                        base.CreateAndSetElement(objCoverageUI, "ChildOrgHierarchySessionID", sChildOrgHUniqueID);
                        
                    }

                }
                //Manish multicurrency
                base.LocalCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), ref sShortCode, ref sDesc);
                 sCurrency = sDesc.Split('|')[1];
                base.CreateAndSetElement(objRoot, "BaseCurrencyType", sCurrency);
                
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.CheckDeductible.Error", base.ClientId), objEx);
            }

            finally
            {
                sbSql = null;
            }
            return objReturnDoc;
        }
        //Anu Tennyson for MITS 18229 ENDS

        //Anu Tennyson for MITS 18229 STARTS 12/23/2009
        /// <summary>
        /// Function for calculating deductible when the deductible button is enabled.
        /// </summary>
        /// <param name="objPropertyStore">XmlDocument passed from the UI layer</param>
        /// <param name="objSessionManager">Session Manager (for handling session data)</param>
        /// <returns>XmlDocument objReturnDoc contains the ranges .</returns>
        public XmlDocument GetDeductible(SessionManager objSessionManager, XmlDocument objPropertyStore)
        {
            StringBuilder sbSql = null;
            XmlDocument objXmlDocument = null;
            XmlDocument objXmlOrgH = null;
            XmlElement objRootElement = null;
            XmlElement objElement = null;
            bool bIsSuccess = false;
            //rupal:start, mits 27502
            //decimal iDeductible = 0.00m;
            //decimal iModifier = 0.00m;
            //decimal iAmount = 0.00m;
            double iDeductible = 0;
            double iModifier = 0;
            double iAmount = 0;
            //rupal:end
            string sLob = string.Empty;
            string sOrgSessionId = string.Empty;
            string sOrgH = string.Empty;
            string sEffdate = string.Empty;
            string sExpdate = string.Empty;
            int iStateId = 0;
            int iCoverageCodeId = 0;
            int iLobCodeId = 0;
            string sFlatOrPercent = string.Empty;
            string sDeductible = string.Empty;
            string sAmount = string.Empty;
            string sModifier = string.Empty;
            string sRateRowId = string.Empty;
            string sSessionOrgHData = string.Empty;
            int iType = 0;
            string sType = string.Empty;
            string sBeginningRange = string.Empty;
            string sEndRange = string.Empty;
            int iFlatPercent = 0;
            
            int iRateMaintenanceType = 0;
            int iRateRowId = 0;
            Riskmaster.Settings.SysSettings objSysSettings = null;
            
         
            try
            {
                //rupal:start,mits 27502
                //this is actually part of Multicurrency Enh
                objSysSettings = new Settings.SysSettings(base.ConnectionString, base.ClientId);
                //rupal:end
                if (objPropertyStore.SelectSingleNode("//Document/Lob") != null)
                {
                   sLob = objPropertyStore.SelectSingleNode("//Document/Lob").InnerText;
                }
                if (objPropertyStore.SelectSingleNode("//Document/OrgSessionId") != null)
                {
                    sOrgSessionId = objPropertyStore.SelectSingleNode("//Document/OrgSessionId").InnerText;
                }
                if (objPropertyStore.SelectSingleNode("//Document/Effdate") != null)
                {
                   sEffdate = Conversion.GetDate(objPropertyStore.SelectSingleNode("//Document/Effdate").InnerText);
                }
                if (objPropertyStore.SelectSingleNode("//Document/Expdate") != null)
                {
                   sExpdate = Conversion.GetDate(objPropertyStore.SelectSingleNode("//Document/Expdate").InnerText);
                }
                if (objPropertyStore.SelectSingleNode("//Document/State") != null)
                {
                   iStateId = Conversion.CastToType<int>(objPropertyStore.SelectSingleNode("//Document/State").InnerText, out bIsSuccess);
                }
                if (objPropertyStore.SelectSingleNode("//Document/CoverageTypeId") != null)
                {
                   iCoverageCodeId = Conversion.CastToType<int>(objPropertyStore.SelectSingleNode("//Document/CoverageTypeId").InnerText, out bIsSuccess);
                }
                if (!string.IsNullOrEmpty(sOrgSessionId))
                {
                    sSessionOrgHData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sOrgSessionId)).ToString();
                    objXmlOrgH = new XmlDocument();
                    objXmlOrgH.LoadXml(sSessionOrgHData);
                    if (objXmlOrgH.SelectSingleNode("//ChildOrgData") != null)
                    {
                        sOrgH = objXmlOrgH.SelectSingleNode("//ChildOrgData").InnerText;
                    }
                }

                iLobCodeId = base.LocalCache.GetCodeId(sLob, "POLICY_LOB");
                iRateMaintenanceType = base.LocalCache.GetCodeId("C", CODE_TABLENAME);

                sbSql = new StringBuilder();
                sbSql.Append("SELECT SYS_POL_RANGE.DEDUCTIBLE, SYS_POL_RANGE.RANGE_TYPE, SYS_POL_RANGE.MODIFIER, SYS_POL_RANGE.BEGINNING_RANGE, SYS_POL_RANGE.END_RANGE, SYS_POL_EXP_RATES.FLAT_OR_PERCENT, SYS_POL_EXP_RATES.AMOUNT, SYS_POL_EXP_RATES.EXP_RATE_ROWID FROM SYS_POL_EXP_RATES , SYS_POL_RANGE ");
                sbSql.Append("WHERE SYS_POL_EXP_RATES.RATE_MAINTENANCE_TYPE = ");
                sbSql.Append(iRateMaintenanceType);
                sbSql.Append(" AND SYS_POL_EXP_RATES.EXP_RATE_ROWID = SYS_POL_RANGE.RATE_ROWID ");
                sbSql.Append("AND SYS_POL_EXP_RATES.LINE_OF_BUSINESS = ");
                sbSql.Append(iLobCodeId);
                sbSql.Append(" AND	SYS_POL_EXP_RATES.STATE = ");
                sbSql.Append(iStateId);
                sbSql.Append(" AND SYS_POL_EXP_RATES.EXPOSURE_ID = ");
                sbSql.Append(iCoverageCodeId);
                sbSql.Append(" AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                sbSql.Append(sEffdate);
                sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                sbSql.Append(sEffdate);
                sbSql.Append("'))");

                if (!(String.IsNullOrEmpty(sOrgH)))
                {
                    sbSql.Append(" AND SYS_POL_EXP_RATES.ORG_HIERARCHY IN(");
                    sbSql.Append(sOrgH);
                    sbSql.Append(")");
                }

                using (DbReader objReader = DbFactory.GetDbReader(base.ConnectionString, Convert.ToString(sbSql)))
                {
                    objXmlDocument = new XmlDocument();
                    base.StartDocument(ref objXmlDocument, ref objRootElement, "Deductibles");
                    while (objReader.Read())
                    {
                        base.CreateElement(objRootElement, "Deductible", ref objElement);
                        sDeductible = Convert.ToString((objReader.GetValue("DEDUCTIBLE")));
                        //rupal:mits 27502
                        //iDeductible = Conversion.CastToType<decimal>(sDeductible, out bIsSuccess);
                        iDeductible = Conversion.CastToType<double>(sDeductible, out bIsSuccess);
                        sAmount = Convert.ToString((objReader.GetValue("AMOUNT")));
                        //rupal:mits 27502
                        //iAmount = Conversion.CastToType<decimal>(sAmount, out bIsSuccess);
                        iAmount = Conversion.CastToType<double>(sAmount, out bIsSuccess);
                        sRateRowId = Convert.ToString((objReader.GetValue("EXP_RATE_ROWID")));
                        iRateRowId = Conversion.CastToType<int>(sRateRowId, out bIsSuccess);
                        sModifier = Convert.ToString((objReader.GetValue("MODIFIER")));
                        //rupal:mits 27502
                        //iModifier = Conversion.CastToType<decimal>(sModifier, out bIsSuccess);
                        iModifier = Conversion.CastToType<double>(sModifier, out bIsSuccess);
                        iType = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("RANGE_TYPE")), out bIsSuccess);
                        sType = base.LocalCache.GetShortCode(iType);
                        if (sType == "D")
                        {
                            sType = "Deductible";
                        }
                        else if (sType == "R")
                        {
                            sType = "Range";
                        }
                        sBeginningRange = Convert.ToString((objReader.GetValue("BEGINNING_RANGE")));
                        sEndRange = Convert.ToString((objReader.GetValue("END_RANGE")));
                        iFlatPercent = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("FLAT_OR_PERCENT")), out bIsSuccess);
                        sFlatOrPercent = base.LocalCache.GetShortCode(iFlatPercent);

                       

                        if (sFlatOrPercent == "F")
                        {
                            //rupal:start, mits 27502
                            if (objSysSettings.UseMultiCurrency != 0)
                            {
                                //this is actually part of Multicurrency Enh
                                sModifier = CommonFunctions.ConvertCurrency(0, iModifier, CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                                sAmount = CommonFunctions.ConvertCurrency(0, iAmount, CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                                sDeductible = CommonFunctions.ConvertCurrency(0, iDeductible, CommonFunctions.NavFormType.None, base.ConnectionString, ClientId);
                            }
                            else
                            {
                                sModifier = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), iModifier, base.ConnectionString, ClientId);
                                sAmount = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), iAmount, base.ConnectionString, ClientId);
                                //Start: Neha Suresh Jain, MITS 20772, 05/20/2010
                                sDeductible = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectionString), iDeductible, base.ConnectionString, ClientId);
                                //End : Neha Suresh Jain
                            }
                            //rupal:end
                        }
                        else
                        {
                            sModifier = sModifier + "%";
                            sAmount = sAmount + "%";
                            //Start: Neha Suresh Jain, MITS 20772, 05/20/2010
                            sDeductible = sDeductible + "%";
                            //End : Neha Suresh Jain
                        }
                        objElement.SetAttribute("MODIFIER", sModifier);
                        objElement.SetAttribute("RATE_ROWID", sRateRowId);
                        //Start: Neha Suresh Jain, MITS 20772, 05/20/2010
                        //objElement.SetAttribute("DEDUCTIBLE", String.Format("{0:C}", iDeductible));
                        objElement.SetAttribute("DEDUCTIBLE", sDeductible);
                        //End : Neha Suresh Jain
                        objElement.SetAttribute("RANGE_TYPE", sType);
                        objElement.SetAttribute("BEGINNING_RANGE", sBeginningRange);
                        objElement.SetAttribute("END_RANGE", sEndRange);
                        objElement.SetAttribute("AMOUNT", sAmount);
                        //rupal:start, mits 27502
                        //this is actually part of Multicurrency Enh
                        objElement.SetAttribute("MODIFIER_VALUE", iModifier.ToString());
                        objElement.SetAttribute("DEDUCTIBLE_VALUE", iDeductible.ToString());
                        objElement.SetAttribute("AMOUNT_VALUE", iAmount.ToString());                        
                        //rupal:end
                    }
                    base.CreateAndSetElement(objRootElement, "FlatOrPercent", sFlatOrPercent);
                    base.CreateAndSetElement(objRootElement, "FlatOrPercentid", iFlatPercent.ToString());
                }
                
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.GetDeductible.Error", base.ClientId), objEx);
            }
            finally
            {
                //rupal:mits 27502                
                 objSysSettings = null;
                //rupal
                sbSql = null;
            }
            return objXmlDocument;
        }
        //Anu Tennyson for MITS 18229 ENDS


        //Anu Tennyson for MITS 18229 STARTS 12/23/2009
        /// <summary>
        /// Function for calculating deductible if the deductible button is visible in coverages page.
        /// </summary>
        /// <param name="objXmlIn">This XMLDocument will contain all the page data when the page is postback.</param>
        /// <param name="objSessionManager">Session Manager (for handling session data)</param>
        /// <returns>XmlDocument objReturnDoc Contains the Amount</returns>
        public XmlDocument CalculateDeductible(SessionManager objSessionManager, XmlDocument objXmlIn)
        {
            StringBuilder sbSql = null;
            string sOrgFilter = string.Empty;
            string sOrg = string.Empty;
            string sOrgHSessionID = string.Empty;
            string sLob = string.Empty;
            string sSessionOrgHData = string.Empty;
            bool bIsSuccess = false;
            string sEffDate = string.Empty;
            string sExpDate = string.Empty;
            int iState = 0;
            decimal iAmount = 0.00m;
            string sCoverageCodeId = string.Empty;
            int iExposureId = 0;
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;
            XmlDocument objXmlOrgH = null;
            int iDeductibleRange = 0;
            int iLobCode = 0;
            string sFlatOrPercent = string.Empty;
            int iFlatPercent = 0;
            int iRateMaintenanceType = 0;
            //Anu Tennyson for MITS 18229 - UAR List on Coverages Screen - Start
            string sUARAdded = string.Empty;
            string sUARName = string.Empty;
            XmlDocument objXMLUAR = new XmlDocument();
            XmlElement objCoveragesNode = null;
            XmlElement objDeductibleAmount = null;
            PolicyXCvgEnh objPolicyXCvgEnh = null;
            objPolicyXCvgEnh = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
            XmlDocument objCoverages = new XmlDocument();
            XmlDocument objSerilzationXML = new XmlDocument();
            if (objXmlIn.SelectSingleNode("//Coverages/CoveragesData/PolicyXCvgEnh") != null)
            {
                objXMLUAR.LoadXml(objXmlIn.SelectSingleNode("//Coverages/CoveragesData/PolicyXCvgEnh").OuterXml);
                if (objXMLUAR.SelectSingleNode("//NextPolicyName") != null)
                {
                    XmlNode objUARNode = objXMLUAR.SelectSingleNode("//NextPolicyName");
                    objUARNode.ParentNode.RemoveChild(objUARNode);
                }
                objPolicyXCvgEnh.PopulateObject(objXMLUAR);
                objSerilzationXML.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
                objCoverages.LoadXml(objPolicyXCvgEnh.SerializeObject(objSerilzationXML));
            }
            //Anu Tennyson for MITS 18229 - UAR List on Coverages Screen - Ends
            try
            {
                if (objXmlIn.SelectSingleNode("//State") != null)
                {
                   iState = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("//State").InnerText, out bIsSuccess);
                }
                if (objXmlIn.SelectSingleNode("//ORG") != null)
                {
                    sOrgHSessionID = objXmlIn.SelectSingleNode("//ORG").InnerText;
                }
                if (objXmlIn.SelectSingleNode("//LOB") != null)
                {
                   sLob = objXmlIn.SelectSingleNode("//LOB").InnerText;
                }
                if (objXmlIn.SelectSingleNode("//EffDate") != null)
                {
                   sEffDate = Conversion.GetDate(objXmlIn.SelectSingleNode("//EffDate").InnerText);
                }
                if (objXmlIn.SelectSingleNode("//ExpDate") != null)
                {
                   sExpDate = Conversion.GetDate(objXmlIn.SelectSingleNode("//ExpDate").InnerText);
                }
                if (objXmlIn.SelectSingleNode("//CoverageTypeCode") != null)
                {
                   sCoverageCodeId = objXmlIn.SelectSingleNode("//CoverageTypeCode").Attributes["codeid"].Value;
                }

                //Sumit (07/21/2010) - MITS# 20820
                if (objXmlIn.SelectSingleNode("//CoverageTypeId") != null)
                {
                    sCoverageCodeId = objXmlIn.SelectSingleNode("//CoverageTypeId").InnerText;
                }
                //End : Sumit

                if (!string.IsNullOrEmpty(sOrgHSessionID))
                {
                    sSessionOrgHData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sOrgHSessionID)).ToString();
                    objXmlOrgH = new XmlDocument();
                    objXmlOrgH.LoadXml(sSessionOrgHData);
                    if (objXmlOrgH.SelectSingleNode("//ChildOrgData") != null)
                    {
                        sOrg = objXmlOrgH.SelectSingleNode("//ChildOrgData").InnerText;
                    }
                }

                //Sumit (07/21/2010) - MITS# 20820 
                if (objXmlIn.SelectSingleNode("//Org") != null)
                {
                    sOrg = objXmlIn.SelectSingleNode("//Org").InnerText;
                }
                //End : Sumit

                iLobCode = base.LocalCache.GetCodeId(sLob, "POLICY_LOB");
                iRateMaintenanceType = base.LocalCache.GetCodeId("C", CODE_TABLENAME);

                sbSql = new StringBuilder();
                sbSql.Append("SELECT SYS_POL_EXP_RATES.AMOUNT, SYS_POL_EXP_RATES.EXP_RATE_ROWID, SYS_POL_EXP_RATES.FLAT_OR_PERCENT FROM SYS_POL_EXP_RATES WHERE ");
                sbSql.Append("SYS_POL_EXP_RATES.RATE_MAINTENANCE_TYPE = ");
                sbSql.Append(iRateMaintenanceType);
                sbSql.Append(" AND SYS_POL_EXP_RATES.LINE_OF_BUSINESS = ");
                sbSql.Append(iLobCode);
                sbSql.Append(" AND	SYS_POL_EXP_RATES.STATE = ");
                sbSql.Append(iState);
                sbSql.Append(" AND SYS_POL_EXP_RATES.EXPOSURE_ID = '");
                sbSql.Append(sCoverageCodeId);
                sbSql.Append("' AND SYS_POL_EXP_RATES.EFFECTIVE_DATE <= '");
                sbSql.Append(sEffDate);
                sbSql.Append("' AND ((SYS_POL_EXP_RATES.EXPIRATION_DATE IS NULL) ");
                sbSql.Append("OR (SYS_POL_EXP_RATES.EXPIRATION_DATE='') OR  ");
                sbSql.Append("(SYS_POL_EXP_RATES.EXPIRATION_DATE >= '");
                sbSql.Append(sEffDate);
                sbSql.Append("'))");

                if (!(String.IsNullOrEmpty(sOrg)))
                {
                    sbSql.Append(" AND SYS_POL_EXP_RATES.ORG_HIERARCHY IN(");
                    sbSql.Append(sOrg);
                    sbSql.Append(")");
                }

                using (DbReader objReader = DbFactory.GetDbReader(base.ConnectionString, Convert.ToString(sbSql)))
                {
                    base.StartDocument(ref objReturnDoc, ref objRoot, "Coverages");
                    base.CreateElement(objRoot, "CoveragesData", ref objCoveragesNode);
                    base.CreateElement(objRoot, "Deductible", ref objDeductibleAmount);
                    if (objReader.Read())
                    {
                        iAmount = Conversion.CastToType<decimal>(Convert.ToString(objReader.GetValue("AMOUNT")), out bIsSuccess);
                        iExposureId = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("EXP_RATE_ROWID")), out bIsSuccess);
                        iFlatPercent = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("FLAT_OR_PERCENT")), out bIsSuccess);

                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT COUNT(*) FROM SYS_POL_RANGE ");
                        sbSql.Append(" WHERE RATE_ROWID = ");
                        sbSql.Append(iExposureId);
                        using (DbConnection objConn = DbFactory.GetDbConnection(base.ConnectionString))
                        {
                            objConn.Open();
                            string sDeductibleRange = objConn.ExecuteScalar(Convert.ToString(sbSql)).ToString();
                            iDeductibleRange = Conversion.CastToType<int>(sDeductibleRange, out bIsSuccess);
                        }
                        sFlatOrPercent = base.LocalCache.GetShortCode(iFlatPercent);
                        if (iDeductibleRange != 0)
                        {
                            base.CreateAndSetElement(objDeductibleAmount, "Range", "Range");
                        }
                        else
                        {
                            base.CreateAndSetElement(objDeductibleAmount, "Amount", Convert.ToString(iAmount));
                            base.CreateAndSetElement(objDeductibleAmount, "RateRowId", Convert.ToString(iExposureId));                            
                        }
                        base.CreateAndSetElement(objDeductibleAmount, "FlatOrPercid", iFlatPercent.ToString());
                        base.CreateAndSetElement(objDeductibleAmount, "FlatOrPerc", sFlatOrPercent);
                        
                    }
                    else
                    {
                        base.CreateAndSetElement(objDeductibleAmount, "Amount", "0");
                        base.CreateAndSetElement(objDeductibleAmount, "RateRowId", Convert.ToString(iExposureId));
                    }
                    if (sUARAdded != string.Empty)
                    {
                        base.CreateAndSetElement(objDeductibleAmount, "UARAdded", sUARName);
                    }
                }
                //Anu Tennyson for MITS 18229 - UAR List on Coverages Screen - Start
                if (objCoverages.DocumentElement != null)
                {
                    objCoveragesNode.AppendChild(objReturnDoc.ImportNode((XmlNode)objCoverages.DocumentElement, true));
                }
                //Anu Tennyson for MITS 18229 - UAR List on Coverages Screen - End
            }
            catch (Exception objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.CalculateDeductible.Error", base.ClientId), objEx);
            }
            finally
            {
                sbSql = null;
            }
            
                return objReturnDoc;
        }
        //Anu Tennyson for MITS 18229 ENDS

        //Anu Tennyson for MITS 18229 - UAR List control Added on Coverages Page START
        /// <summary>
        /// The Function used to find all the UAR details added on Exposure screen.
        /// </summary>
        /// <param name="objSessionManager">Session Manager (for handling session data)</param>
        /// <param name="objXmlIn">Input xml containing the session id for the UAR Details added.</param>
        /// <returns>XML document containing the uar list</returns>
        public XmlDocument GetUARDetailsAdded(SessionManager objSessionManager, XmlDocument objXmlIn)
        {
            int iMax_Codes_Per_Page = 0;
            XmlDocument objReturnDoc = null;
            XmlElement objRoot = null;
            XmlElement objUAR = null;
            string sSessionId = string.Empty;
            string sSessionUARHData = string.Empty;
            string sUARAdded = string.Empty;
            string sVehicleName = string.Empty;
            string sVehicleID = string.Empty;
            StringBuilder sbUARList = null;
            int iExposureStart = 0;
            bool bIsSuccess = false;
            int iPageCount = 0;
            int iNoOfPage = 0;
            int iCount = 0;
            XmlDocument objXmlUAR = null;
            sbUARList = new StringBuilder();
            objReturnDoc = new XmlDocument();
            iMax_Codes_Per_Page = GetMaxRecordsonPage();
            if (objXmlIn.SelectSingleNode("//Document/SessionId") != null)
            {
                sSessionId = objXmlIn.SelectSingleNode("//Document/SessionId").InnerText;
            }
            if (objXmlIn.SelectSingleNode("//Document/ExposureStart") != null)
            {
                iExposureStart = Conversion.CastToType<int>(objXmlIn.SelectSingleNode("//Document/ExposureStart").InnerText, out bIsSuccess);
            }
            if (!string.IsNullOrEmpty(sSessionId))
            {
                sSessionUARHData = Utilities.BinaryDeserialize(objSessionManager.GetBinaryItem(sSessionId)).ToString();
                objXmlUAR = new XmlDocument();
                objXmlUAR.LoadXml(sSessionUARHData);
                if (objXmlUAR.SelectSingleNode("//UARAdded") != null)
                {
                    sUARAdded = objXmlUAR.SelectSingleNode("//UARAdded").InnerText;
                }
            }
            if (!string.IsNullOrEmpty(sUARAdded))
            {
                string[] sExposureList = sUARAdded.Split(new char[] { '~' });

                iPageCount = Conversion.CastToType<int>(Convert.ToString(sExposureList.Length / iMax_Codes_Per_Page), out bIsSuccess);
                if ((sExposureList.Length % iMax_Codes_Per_Page) != 0)
                {
                    iPageCount += 1;
                }
                iNoOfPage = iPageCount;

                for (int iExposureCount = iExposureStart; iExposureCount < sExposureList.Length; iExposureCount++)
                {
                    if (!(string.IsNullOrEmpty(sExposureList[iExposureCount])))
                    {
                        if (iCount == iMax_Codes_Per_Page)
                        {
                            break;
                        }
                        
                        string[] sExposureListBreak = sExposureList[iExposureCount].Split(new char[] { '|' });
                        if (sExposureListBreak[0] != null)
                        {
                            sVehicleName = sExposureListBreak[0];
                        }
                        if (sExposureListBreak[1] != null)
                        {
                            sVehicleID = sExposureListBreak[1];
                        }
                        
                        sbUARList.Append(sVehicleName);
                        sbUARList.Append("|");
                        sbUARList.Append(sVehicleID);
                        sbUARList.Append("~");
                        iCount++;
                    }

                }
            }
            base.StartDocument(ref objReturnDoc, ref objRoot, "UARAdded");
            base.CreateElement(objRoot, "UARs", ref objUAR);
            base.CreateAndSetElement(objUAR, "UARList", sbUARList.ToString());
            base.CreateAndSetElement(objUAR, "PageCount", iNoOfPage.ToString());

            return objReturnDoc;
        }
        private int GetMaxRecordsonPage()
        {
            StringBuilder sbSQL = null;
            int iReturn = 0;
            bool bIsSuccess = false;

            try
            {
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE");
                sbSQL.Append(" WHERE PARM_NAME = 'CODES_USER_LIMIT' ");

                using (DbReader objReader = DbFactory.GetDbReader(base.ConnectionString, sbSQL.ToString()))
                {
                    if (objReader.Read())
                    {
                        iReturn = Conversion.CastToType<int>(Convert.ToString(objReader.GetValue("STR_PARM_VALUE")), out bIsSuccess);
                    }
                    else
                    {
                        iReturn = 100;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                sbSQL = null;

            }
            return iReturn;
        }
        //Anu Tennyson for MITS 18229 - UAR List control Added on Coverages Page ENDS
       
    }
}
