using System;

namespace Riskmaster.Application.EnhancePolicy
{
	/**************************************************************
	 * $File		: Constants.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 12/20/2005
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class Constants
	{
        public const string TRANSACTION_STATUS_PROVISIONAL = "PR";
        public const string INSTANCE_SYSEXDATA_PATH = "/Instance/UI/FormVariables/SysExData/";
        public const string SYSEX_DOC_ROOT = "/SysExData//";
        public const string MISSING_REF = "Instance/UI/MissingRef";
        public const string ELEMENT_TYPE_CODE = "Code";
        public const string ELEMENT_TYPE_ORG = "Org";

        public const string CTL_POLICY_QUOTE_TAB = "policyinfo";
        public const string CTL_RADIO_QUOTE = "rdoQuote";
        public const string CTL_RADIO_POLICY = "rdoPolicy";
        public const string CTL_POLICY_QUOTE_NAME = "PolicyQuoteName";
        public const string CTL_POLICY_QUOTE_NUMBER = "PolicyQuoteNumber";
        public const string CTL_POLICY_QUOTE_STATUS = "PolicyQuoteStatus";
        public const string CTL_POLICY_QUOTE_TYPE = "PolicyQuoteType";
        public const string CTL_POLICY_QUOTE_STATE = "PolicyQuoteState";
        public const string CTL_BANK_ACCOUNT = "BankAccount";
        public const string CTL_PRIMARY_POLICY = "PrimaryPolicy";
        public const string CTL_EVENT_MADE_COVERAGE = "TODO";
        public const string CTL_CLAIMS_MADE_COVERAGE = "TODO";
        public const string CTL_ISSUE_DATE = "IssueDate";
        public const string CTL_REVIEW_DATE = "ReviewDate";
        public const string CTL_EFFECTIVE_DATE = "EffectiveDate";
        public const string CTL_EXPIRATION_DATE = "ExpirationDate";
        public const string CTL_CANCEL_DATE = "CancelDate";
        public const string CTL_RETRO_DATE = "RetroDate";
        public const string CTL_CANCEL_REASON = "CacnelReason";
        public const string CTL_NON_RENEW_REASON = "NonRenewReason";
        public const string CTL_LAST_UPDATED = "LastUpdated";
        public const string CTL_LAST_USER = "LastUser";
        // Shruti
        public const string CTL_MCO = "btnMCO";
        public const string CTL_CONVERT_TO_POLICY = "btnConvertToPolicy";
        public const string CTL_AMEND_POLICY = "btnAmendPolicy";
        public const string CTL_RENEW_POLICY = "btnRenewPolicy";
        public const string CTL_CANCEL_POLICY = "btnCancelPolicy";
        public const string CTL_REINSTATE_POLICY = "btnReinstatePolicy";
        public const string CTL_AUDIT_POLICY = "btnAuditPolicy";
        public const string CTL_PRINT_POLICY = "btnPrintPolicy";
        public const string CTL_POLICY_COVERAGE_TAB = "coverages"; // Added by csingh7 MITS 20427
        public const string CTL_ADD_COVERAGE = "TODO";
        public const string CTL_DELETE_COVERAmGE = "TODO";
        public const string CTL_EDIT_COVERAGE = "TODO";
        public const string CTL_POLICY_EXPOSURE_TAB = "unitatrisk"; // Added by csingh7 MITS 20427
        public const string CTL_ADD_EXPOSURE = "TODO";
        public const string CTL_DELETE_EXPOSURE = "TODO";
        public const string CTL_EDIT_EXPOSURE = "TODO";
        public const string CTL_ADD_MCO = "TODO";
        public const string CTL_DELETE_MCO = "TODO";
        public const string CTL_EDIT_MCO = "TODO";
        public const string CTL_ACCEPT_TRANSACTION = "btnAcceptTranaction";
        public const string CTL_DELETE_TRANSACTION = "btnDeleteTransaction";
        public const string CTL_EARNED_PREMIUM = "btnEarnedPremium";

        public const string CTL_INSURER = "inslastname";
        public const string CTL_INSURER_CONTACT = "inscontact";
        public const string CTL_INSURER_ADDRESS1 = "insaddr1";
        public const string CTL_INSURER_ADDRESS2 = "insaddr2";
        public const string CTL_INSURER_ADDRESS3 = "insaddr3";
        public const string CTL_INSURER_ADDRESS4 = "insaddr4";
        public const string CTL_INSURER_CITY = "inscity";
        public const string CTL_INSURER_STATE = "insstateid";
        public const string CTL_INSURER_ZIP = "inszipcode";
        public const string CTL_INSURER_COUNTRY = "inscountrycode";
        public const string CTL_INSURER_OFFICE_PHONE = "insphone1";
        public const string CTL_INSURER_ALT_PHONE = "insphone2";
        public const string CTL_INSURER_FAX = "insfaxnumber";
        public const string CTL_INSURER_ISURED = "insuredlist";

        public const string CTL_BROKER_LAST_NAME = "brklastname";
        public const string CTL_BROKER_FIRST_NAME = "brkfirstname";
        public const string CTL_BROKER_ADDRESS1 = "brkaddr1";
        public const string CTL_BROKER_ADDRESS2 = "brkaddr2";
        public const string CTL_BROKER_ADDRESS3 = "brkaddr3";
        public const string CTL_BROKER_ADDRESS4 = "brkaddr4";
        public const string CTL_BROKER_CITY = "brkcity";
        public const string CTL_BROKER_STATE = "brkstateid";
        public const string CTL_BROKER_ZIP = "brkzipcode";
        public const string CTL_BROKER_COUNTRY = "brkcountrycode";
        public const string CTL_BROKER_OFFICE_PHONE = "brkphone1";
        public const string CTL_BROKER_ALT_PHONE = "brkphone2";
        public const string CTL_BROKER_FAX = "brkfaxnumber";
        public const string CTL_BROKER_EMAIL_TYPE = "brkemailtypecode";
        public const string CTL_BROKER_EMAIL_ADD = "brkemailaddress";
        public const string CTL_BROKER_TAX_ID = "brktaxid";
        public const string CTL_BROKER_BROKER_FIRM = "brkparenteid";

        public const string CTL_POLICY_BILLING_TAB = "policybilling";
        public const string CTL_DONOTBILL = "donotbill";
        public const string CTL_POLICY_BILLING_ID = "PolicyBillingId";
        public const string CTL_BILL_TO_EID = "BillToEid";
        public const string CTL_NEW_BILL_TO_EID = "NewBillToEid";
        public const string CTL_BILL_X_INS_ENTITYID = "BillXIns_entityid";
        public const string CTL_BILL_X_NEW_INS_ENTITYID = "BillXNewIns_entityid";
        public const string CTL_PAY_PLAN_ROWID = "payplanrowid";
        public const string CTL_BILLING_RULE_ROWID = "billingrulerowid";
        public const string CTL_BILL_TO_TYPE = "billtotype";
        public const string CTL_BILL_TO_OVERRIDE = "billtooverride";
        public const string CTL_IND_LASTNAME_PAYPLAN = "indlastname";
        public const string CTL_NEW_LASTNAME = "newlastname";
        public const string CTL_IND_FIRSTNAME = "indfirstname";
        public const string CTL_NEW_FIRSTNAME = "newfirstname";
        public const string CTL_IND_ADDR1 = "indaddr1";
        public const string CTL_NEW_ADDR1 = "newaddr1";
        public const string CTL_IND_ADDR2 = "indaddr2";
        public const string CTL_NEW_ADDR2 = "newaddr2";
        public const string CTL_IND_ADDR3 = "indaddr3";
        public const string CTL_NEW_ADDR3 = "newaddr3";
        public const string CTL_IND_ADDR4 = "indaddr4";
        public const string CTL_NEW_ADDR4 = "newaddr4";
        public const string CTL_IND_CITY = "indcity";
        public const string CTL_NEW_CITY = "newcity";
        //public const string CTL_IND_STATE = "indstateinfo";
        public const string CTL_IND_STATE = "indstateid_codelookup";
        public const string CTL_NEW_STATE = "newstateinfo";
        public const string CTL_IND_ZIPCODE = "indzipcode";
        public const string CTL_NEW_ZIPCODE = "newzipcode";
        //public const string CTL_IND_COUNTRY = "indcountryinfo";
        public const string CTL_IND_COUNTRY = "indcountrycode_codelookup";
        public const string CTL_NEW_COUNTRY = "newcountryinfo";
        
        public const string POST_BACK_ACTION = "PostBackAction";
        public const string ACTION_ACCEPT_TRANSACTION = "ACCEPT_TRANSACTION";
        public const string ACTION_DELETE_TRANSACTION = "DELETE_TRANSACTION";
        public const string ACTION_AMEND_POLICY = "AMEND_POLICY";
        public const string ACTION_AUDIT_POLICY = "AUDIT_POLICY";
        public const string ACTION_CANCEL_POLICY = "CANCEL_POLICY";
        public const string ACTION_CONVERT_TO_POLICY = "CONVERT_TO_POLICY";
        public const string ACTION_CALCULATION_PREMIUM_CHANGED = "CALCULATION_PREMIUM_CHANGED";
        
        public const string ACTION_RENEW_POLICY = "RENEW_POLICY";
        public const string ACTION_REINSTATE_POLICY = "REINSTATE_POLICY";
        public const string ACTION_TRANSACTION_CHANGE = "TRANSACTION_CHANGE";
        public const string ACTION_EXPOSURE_EDIT = "EXPOSURE_EDIT";

        public const string SYSEX_AMEND_POLICY_TRANSACTION_DATE = "TransationDateForAmendPolicy";
        public const string SYSEX_AUDIT_POLICY_TERM_NUMBER = "TermNumberForAuditPolicy";
        public const string SYSEX_CANCEL_POLICY_TYPE = "TypeForCancelPolicy";
        public const string SYSEX_CANCEL_POLICY_REASON = "ReasonForCancelPolicy";
        public const string SYSEX_CANCEL_POLICY_DATE = "DateForCancelPolicy";
        public const string SYSEX_REINSTATE_POLICY_LAPSE_FLAG = "ReinstateWithLapseFlag";
        public const string SYSEX_REINSTATE_POLICY_DATE = "DateForReinstatePolicy";

        //Sumit - Start(03/16/2010) - MITS# 18229
        public const string INSTANCE_PCDOCUMENT_PATH = "/Instance/Document/Exposure/";
        public const string INSTANCE_ALDOCUMENT_PATH = "/Instance/Document/Exposure/";
        //Sumit - Start(04/27/2010) - MITS# 20483 - Path used to populate Schedule Grid option.
        public const string INSTANCE_SCHDDOCUMENT_PATH = "/Instance/Document/PolicyXUar/";
        //Sumit - End

        public const string DATA_CHANGED = "DataChanged";
        // Start Naresh MITS 9565 Added the Screen Title [Policy Number]
        public const string SYSEX_SUBTITLE = "SubTitle";
        // End Naresh MITS 9565 Added the Screen Title [Policy Number]

        // Start Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
        // Declare the Constant for the InsuredId and ControlType
        public const string SYSEX_INSURED_ID = "InsuredId";
        public const string SYSEX_CONTROL_TYPE = "ControlType";
        // End Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab

        #region Define Security Constants.
        public const int RMB_PEOPMAINT = 14000;
        public const int RMB_POLMGT_ENH = 23000;
        public const int RMB_POLMGT_COV_ENH = 23150;
        public const int RMB_POLMGT_EXP_ENH = 23350;
        //Start-Mridul Bansal. 01/04/10. MITS#19314. Provide security on LOB basis.
        public const int RMB_POLMGTGL_ENH = 23000;
        public const int RMB_POLMGTGL_COV_ENH = 23150;
        public const int RMB_POLMGTGL_EXP_ENH = 23350;
        public const int RMB_POLMGTPC_ENH = 42000;
        public const int RMB_POLMGTPC_COV_ENH = 42150;
        public const int RMB_POLMGTPC_EXP_ENH = 42350;
        public const int RMB_POLMGTAL_ENH = 26000;
        public const int RMB_POLMGTAL_COV_ENH = 26150;
        public const int RMB_POLMGTAL_EXP_ENH = 26350;
        public const int RMB_POLMGTWC_ENH = 25000;
        public const int RMB_POLMGTWC_COV_ENH = 25150;
        public const int RMB_POLMGTWC_EXP_ENH = 25350;
        //End-Mridul Bansal. 01/04/10. MITS#19314. Provide security on LOB basis.
        public const int RMB_POLMGT_ENH_CONVERT = 30;
        public const int RMB_POLMGT_ENH_AMEND = 31;
        public const int RMB_POLMGT_ENH_RENEW = 32;
        public const int RMB_POLMGT_ENH_CANCEL = 33;
        public const int RMB_POLMGT_ENH_REINSTATE = 34;
        public const int RMB_POLMGT_ENH_AUDIT = 35;
        public const int RMB_POLMGT_ENH_PRINT = 36; //MGaba2:MITS 11802
        #endregion    

        #region Premium Constants

        // For Root Level
        public const string CALC_PREM_ROOT = "PremiumAmounts";

        // For Discounts
        public const string CALC_PREM_DISCOUNTS = "Discounts";
        public const string CALC_PREM_DISCOUNT = "Discount";
        public const string CALC_PREM_DISCOUNT_AMOUNT = "Amount";
        //Anu Tennyson for VACo Prem. Calculation MITS STARTS 
        public const string CALC_PREM_MODIFIER_AMOUNT = "ModifierAmount";
        public const string CALC_PREM_DISCOUNT_AMOUNT_FACTOR = "AmountFactor";
        public const string CALC_PREM_MODIFIER_AMOUNT_FACTOR = "ModifierFactor";
        public const string CALC_PREM_MODIFIERS = "Modifiers";
        public const string CALC_PREM_MODIFIER = "Modifier";
        public const string CALC_PREM_MODIFIER_TYPE = "ModiferType";
        public const string CALC_PREM_MODIFIER_AMOUNT_FACTORID = "CoverageModiferAmountFactor";
        public const string CALC_PREM_MODIFIER_ID = "ModifierId";
        public const string CALC_PREM_DEDUCTIBLE_LABEL_ID = "Modifier";
        public const string CALC_PREM_DEDUCITBLE_LEVEL = "ModifierLevel";
        public const string CALC_PREM_TYPE_MODIFIER = "Type_Modifier";
        public const string CALC_PREM_DEDUCTIBLE_ADDED = "Added";
        //Anu Tennyson for VACo Prem. Calculation MITS ENDS
        public const string CALC_PREM_DISCOUNT_LEVEL = "Level";
        public const string CALC_PREM_DISCOUNT_NAME = "Name";
        public const string CALC_PREM_DISCOUNT_NAME_ID = "NameId";
        public const string CALC_PREM_DISCOUNT_TYPE = "Type";
        public const string CALC_PREM_DISCOUNT_ID = "Id";

        // For Tiers
        public const string CALC_PREM_TIERS = "Tiers";
        public const string CALC_PREM_TIER = "Tier";
        public const string CALC_PREM_TIER_AMOUNTS = "Amount";
        public const string CALC_PREM_TIER_LEVEL = "Level";
        public const string CALC_PREM_TIER_NAME = "Name";
        public const string CALC_PREM_TIER_NAME_ID = "NameId";
        public const string CALC_PREM_TIER_ID = "Id";
        //pmahli
        //For Expense Constant 
        public const string CALC_PREM_EXPENSE_CONST_TITLE = "Expense Constant";
        public const string CALC_PREM_EXPENSE_CONST_ID = "ExpenseConstant";
        // For Re-Arranging  of Discounts and Tiers
        public const string CALC_PREM_LEVEL = "Level";
        public const string CALC_PREM_TYPE = "Type";
        public const string CALC_PREM_TYPE_DISCOUNT = "Type_Discount";
        public const string CALC_PREM_TYPE_TIER = "Type_Tier";


        public const string CALC_PREM_MANUAL_PREMIUM_TITLE = "Manual Premium";
        public const string CALC_PREM_MANUAL_PREMIUM_ID = "ManualPremium";
        public const string CALC_PREM_EXP_MOD_FACTOR_TITLE = "Exp. Mod. Factor";
        public const string CALC_PREM_EXP_MOD_FACTOR_ID = "ExpModFactor";
        public const string CALC_PREM_EXP_MOD_FACTOR_LABEL_ID = "lblExpModFactor";
        public const string CALC_PREM_EXP_MOD_FACTOR_LABEL_BLANK_ID = "lblBlankExpModFactor";
        public const string CALC_PREM_EXP_MOD_FACTOR_CG_ID = "cgExpModFactor";
        public const string CALC_PREM_EXP_MOD_FACTOR_SPACE_ID = "spExpModFactor";
        public const string CALC_PREM_EXP_MOD_FACTOR_AMOUNT_TITLE = "";
        public const string CALC_PREM_EXP_MOD_FACTOR_AMOUNT_ID = "ExpModFactorAmount";
        public const string CALC_PREM_MODIFIED_PREMIUM_TITLE = "Modified Premium";
        public const string CALC_PREM_MODIFIED_PREMIUM_ID = "ModifiedPremium";
        public const string CALC_PREM_FINAL_PREMIUM_ID = "FinalPremium";
        public const string CALC_PREM_FINAL_PREMIUM_TITLE = "";
        public const string CALC_PREM_MANUAL_FINAL_PREMIUM_ID = "ManualFinalPremium";
        public const string CALC_PREM_MANUAL_FINAL_PREMIUM_TITLE = "";

        public const string CALC_PREM_ESTIMATED_PREMIUM_ID = "EstimatedPremium";
        public const string CALC_PREM_ESTIMATED_PREMIUM_TITLE = "Estimated Premium";
        public const string CALC_PREM_ESTIMATED_PREMIUM_LABEL_ID = "lblEstimatedPremium";
        public const string CALC_PREM_ESTIMATED_PREMIUM_CG_ID = "cgEstimatedPremium";

        public const string CALC_PREM_TRANSACTION_PREMIUM_ID = "TransactionPremium";
        public const string CALC_PREM_TRANSACTION_PREMIUM_TITLE = "Transaction Premium";

        public const string CALC_PREM_TOTAL_BILLED_PREMIUM_ID = "TotalBilledPremium";
        public const string CALC_PREM_TOTAL_BILLED_PREMIUM_TITLE = "Total Billed Premium";
        public const string CALC_PREM_TOTAL_BILLED_PREMIUM_LABEL_ID = "lblTotalBilledPremium";
        public const string CALC_PREM_TOTAL_BILLED_PREMIUM_CG_ID = "cgTotalBilledPremium";

        public const string CALC_PREM_AUDITED_PREMIUM_ID = "AuditedPremium";
        public const string CALC_PREM_AUDITED_PREMIUM_TITLE = "Audited Premium";

        public const string CALC_PREM_WAIVE_PREMIUM_ID = "WaiveTransactionPremium";
        public const string CALC_PREM_WAIVE_PREMIUM_TITLE = "Waive Transaction Premium?";

        public const string CALC_PREM_COMMENTS_ID = "WaiveTransactionPremiumComments";
        public const string CALC_PREM_COMMENTS_TITLE = "Comments";
        public const string CALC_PREM_COMMENTS_LABEL_ID = "lblWaiveTransactionPremiumComments";
        public const string CALC_PREM_COMMENTS_CG_ID = "cgWaiveTransactionPremiumComments";

        public const string CALC_PREM_CHG_BILLED_PREMIUM_ID = "changeInBilledPremium";
        
        //npadhy RMSC retrofit Start 
        public const string CALC_PREM_TAX_RATE_TITLE = "Tax";
        public const string CALC_PREM_TAX_RATE_LABEL_ID = "lblRateTax";
        public const string CALC_PREM_TAX_RATE_CG_ID = "cgRateTax";
        public const string CALC_PREM_TAX_RATE_ID = "TaxRate";
        public const string CALC_PREM_TAX_ID = "Tax";
        public const string CALC_PREM_TAX_TYPE_ID = "TaxType";
        public const string CALC_PREM_TRANS_TAX_LABEL_TITLE = "Transaction Tax";
        public const string CALC_PREM_TRANS_TAX_LABEL_ID = "lblTransactionTax";
        public const string CALC_PREM_TRANS_TAX_CG_ID = "cgTransactionTax";
        public const string CALC_PREM_TRANS_TAX_ID = "Transaction_Tax";
        public const string CALC_PREM_HID_TAX_TYPE_ID = "Tax_Type";
        //npadhy RMSC retrofit End

        #endregion 
	
	}
}
