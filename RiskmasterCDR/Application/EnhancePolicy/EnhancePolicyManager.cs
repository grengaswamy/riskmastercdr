﻿
using System;
using System.Collections;
using System.Text;
// Start Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
using System.Xml;
// End Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.EnhancePolicy
{
    /**************************************************************
	 * $File		: EnhancePolicyManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Vaibhav Kaushik
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/

    public struct EnhancePolicyUI
    {
        public PolicyUI PolicyUI;
        public PolicyUIData PolicyUIData;
        public PremiumCalculationUI PremiumCalculationUI;
        public CoverageUI CoverageUI;
        public ExposureUI ExposureUI;
        public McoUI McoUI;        
    }

    public struct PolicyUI
    {
        private string m_sPolicyTabCaption;
        private string m_sNameCaption;
        private string m_sNumberCaption;
        private string m_sStatusCaption;
        private string m_sTypeCaption;
        private string m_sStateCaption;
        private string m_sPrintCaption;
        private bool m_bIsQuote;

        private bool m_bPolicyNameEnabled;
        private bool m_bPolicyNumberEnabled;
        private bool m_bPolicyTypeEnabled;
        private bool m_bPolicyStateEnabled;

        private bool m_bIssueDateEnabled;
        private bool m_bReviewDateEnabled;
        private bool m_bEffectiveDateEnabled;
        private bool m_ExpirationDateEnabled;
        private bool m_CancelDateEnabled;
        private bool m_RetroDateEnabled;
        private bool m_bCancelReasonEnabled;

        private bool m_bNonRenewReasonEnabled;

        // Shruti
        private bool m_bMCOEnabled;
        private bool m_bConvertQuoteEnabled;
        private bool m_bReinstatePolicyEnabled;
        private bool m_bRenewPolicyEnabled;
        private bool m_bAmendPolicyEnabled;
        private bool m_bCancelPolicyEnabled;
        private bool m_bCalculateEarnedPremiumEnabled;
        private bool m_bCalculateEarnedPremiumVisible;
        private bool m_bAcceptTransacationEnabled;
        private bool m_bDeleteTransactionEnabled;
        private bool m_bPolicyPrintEnabled;

        private bool m_bPolicyStatusEnabled;
        
        private bool m_bBrokerEnabled;
        private bool m_bInsurerEnabled;
        private bool m_bPrimaryPolicyEnabled;
        private bool m_bCoverageEnabled;
        private string m_sCoverageType;
        private bool m_bBankAccountEnabled;

        private bool m_bAuditButtonVisbile;
        private bool m_bAuditButtonEnabled;

        public bool AuditButtonVisbile { get { return m_bAuditButtonVisbile; } set { m_bAuditButtonVisbile = value; } }
        public bool AuditButtonEnabled { get { return m_bAuditButtonEnabled; } set { m_bAuditButtonEnabled = value; } }

        public bool BrokerEnabled { get { return m_bBrokerEnabled; } set { m_bBrokerEnabled = value; } }
        public bool InsurerEnabled { get { return m_bInsurerEnabled; } set { m_bInsurerEnabled = value; } }
        public bool PrimaryPolicyEnabled { get { return m_bPrimaryPolicyEnabled; } set { m_bPrimaryPolicyEnabled = value; } }
        public bool CoverageEnabled { get { return m_bCoverageEnabled; } set { m_bCoverageEnabled = value; } }
        public string CoverageType { get { return m_sCoverageType; } set { m_sCoverageType = value; } }
        public bool BankAccountEnabled { get { return m_bBankAccountEnabled; } set { m_bBankAccountEnabled = value; } }


        public bool PolicyNameEnabled { get { return m_bPolicyNameEnabled; } set { m_bPolicyNameEnabled = value; } }
        public bool PolicyNumberEnabled { get { return m_bPolicyNumberEnabled; } set { m_bPolicyNumberEnabled = value; } }
        public bool PolicyTypeEnabled { get { return m_bPolicyTypeEnabled; } set { m_bPolicyTypeEnabled = value; } }
        public bool PolicyStateEnabled { get { return m_bPolicyStateEnabled; } set { m_bPolicyStateEnabled = value; } }
        public bool IssueDateEnabled { get { return m_bIssueDateEnabled; } set { m_bIssueDateEnabled = value; } }
        public bool ReviewDateEnabled { get { return m_bReviewDateEnabled; } set { m_bReviewDateEnabled = value; } }
        public bool EffectiveDateEnabled { get { return m_bEffectiveDateEnabled; } set { m_bEffectiveDateEnabled = value; } }
        public bool ExpirationDateEnabled { get { return m_ExpirationDateEnabled; } set { m_ExpirationDateEnabled = value; } }
        public bool CancelDateEnabled { get { return m_CancelDateEnabled; } set { m_CancelDateEnabled = value; } }
        public bool RetroDateEnabled { get { return m_RetroDateEnabled; } set { m_RetroDateEnabled = value; } }
        public bool CancelReasonEnabled { get { return m_bCancelReasonEnabled; } set { m_bCancelReasonEnabled = value; } }
        public bool NonRenewReasonEnabled { get { return m_bNonRenewReasonEnabled; } set { m_bNonRenewReasonEnabled = value; } }


        public string PolicyTabCaption { get { return m_sPolicyTabCaption; } set { m_sPolicyTabCaption = value; } }
        public string NameCaption { get { return m_sNameCaption; } set { m_sNameCaption = value; } }
        public string NumberCaption { get { return m_sNumberCaption; } set { m_sNumberCaption = value; } }
        public string StatusCaption { get { return m_sStatusCaption; } set { m_sStatusCaption = value; } }
        public string TypeCaption { get { return m_sTypeCaption; } set { m_sTypeCaption = value; } }
        public string StateCaption { get { return m_sStateCaption; } set { m_sStateCaption = value; } }
        public string PrintCaption { get { return m_sPrintCaption; } set { m_sPrintCaption = value; } }

        public bool IsQuote { get { return m_bIsQuote; } set { m_bIsQuote = value; } }
        
        // Shruti
        public bool MCOEnabled { get { return m_bMCOEnabled; } set { m_bMCOEnabled = value; } }
        public bool ConvertQuoteEnabled { get { return m_bConvertQuoteEnabled; } set { m_bConvertQuoteEnabled = value; } }
        public bool ReinstatePolicyEnabled { get { return m_bReinstatePolicyEnabled; } set { m_bReinstatePolicyEnabled = value; } }
        public bool RenewPolicyEnabled { get { return m_bRenewPolicyEnabled; } set { m_bRenewPolicyEnabled = value; } }
        public bool AmendPolicyEnabled { get { return m_bAmendPolicyEnabled; } set { m_bAmendPolicyEnabled = value; } }
        public bool CancelPolicyEnabled { get { return m_bCancelPolicyEnabled; } set { m_bCancelPolicyEnabled = value; } }
        public bool CalculateEarnedPremiumEnabled { get { return m_bCalculateEarnedPremiumEnabled; } set { m_bCalculateEarnedPremiumEnabled = value; } }
        public bool CalculateEarnedPremiumVisible { get { return m_bCalculateEarnedPremiumVisible; } set { m_bCalculateEarnedPremiumVisible = value; } }
        public bool AcceptTransacationEnabled { get { return m_bAcceptTransacationEnabled; } set { m_bAcceptTransacationEnabled = value; } }
        public bool DeleteTransactionEnabled { get { return m_bDeleteTransactionEnabled; } set { m_bDeleteTransactionEnabled = value; } }
        public bool PolicyPrintEnabled { get { return m_bPolicyPrintEnabled; } set { m_bPolicyPrintEnabled = value; } }

        public bool PolicyStatusEnabled { get { return m_bPolicyStatusEnabled; } set { m_bPolicyStatusEnabled = value; } }        
    }

    public struct PolicyUIData
    {
        private int m_iPolicyStatus;
        private string m_sPolicyNumber;
        private string m_sEffectiveDate;
        private string m_sExpirationDate;
        private string m_sCancelDate;
        private int m_iCancelReason;
        //npadhy RMSC retrofit Starts
        private double m_dBrokerCommission;
        //npadhy RMSC retrofit Ends
        
        public int PolicyStatus { get { return m_iPolicyStatus; } set { m_iPolicyStatus = value; } }
        public string PolicyNumber { get { return m_sPolicyNumber; } set { m_sPolicyNumber = value; } }
        public string EffectiveDate { get { return m_sEffectiveDate; } set { m_sEffectiveDate = value; } }
        public string ExpirationDate { get { return m_sExpirationDate; } set { m_sExpirationDate = value; } }
        public string CancelDate { get { return m_sCancelDate; } set { m_sCancelDate = value; } }
        public int CancelReason { get { return m_iCancelReason; } set { m_iCancelReason = value; } }
        //npadhy RMSC retrofit Starts
        public double BrokerCommission { get { return m_dBrokerCommission; } set { m_dBrokerCommission = value; } }
        //npadhy RMSC retrofit Ends

        //Mridul. 11/23/09. MITS:18229
        private int m_iPolicyType;
        public int PolicyType { get { return m_iPolicyType; } set { m_iPolicyType = value; } }
    }

    public struct PremiumCalculationUI
    {
        private bool m_bWaivePremiumSelected;
        private bool m_bWaivePremiumEnabled;
        private bool m_bWaivePremiumVisible;
        private bool m_bWaivePremiumCommentsEnabled;
        private bool m_bWaivePremiumCommentsVisible;
        private string m_sWaivePremiumComments;
        private bool m_bExpModFactorEnabled;

        public bool WaivePremiumSelected { get { return m_bWaivePremiumSelected; } set { m_bWaivePremiumSelected = value; } }
        public bool WaivePremiumEnabled { get { return m_bWaivePremiumEnabled; } set { m_bWaivePremiumEnabled = value; } }
        public bool WaivePremiumVisible { get { return m_bWaivePremiumVisible; } set { m_bWaivePremiumVisible = value; } }
        public bool WaivePremiumCommentsEnabled { get { return m_bWaivePremiumCommentsEnabled; } set { m_bWaivePremiumCommentsEnabled = value; } }
        public bool WaivePremiumCommentsVisible { get { return m_bWaivePremiumCommentsVisible; } set { m_bWaivePremiumCommentsVisible = value; } }
        public string WaivePremiumComments { get { return m_sWaivePremiumComments; } set { m_sWaivePremiumComments = value; } }
        public bool ExpModFactorEnabled { get { return m_bExpModFactorEnabled; } set { m_bExpModFactorEnabled = value; } }

    }

    public struct CoverageUI
    {
        private bool m_bAddCoverageEnabled;
        private bool m_bDeleteCoverageEnabled;
        private bool m_bEditCoverageEnabled;
        private bool m_bCoverageEnabled;//Added by csingh7 MITS 20427

        public bool AddCoverageEnabled { get { return m_bAddCoverageEnabled; } set { m_bAddCoverageEnabled = value; } }
        public bool DeleteCoverageEnabled { get { return m_bDeleteCoverageEnabled; } set { m_bDeleteCoverageEnabled = value; } }
        public bool EditCoverageEnabled { get { return m_bEditCoverageEnabled; } set { m_bEditCoverageEnabled = value; } }
        public bool CoverageEnabled { get { return m_bCoverageEnabled; } set { m_bCoverageEnabled = value; } }  //Added by csingh7 MITS 20427
    }

    public struct ExposureUI
    {
        private bool m_bAddExposureEnabled;
        private bool m_bDeleteExposureEnabled;
        private bool m_bEditExposureEnabled;
        private bool m_bExposureEnabled;    //Added by csingh7 MITS 20427

        public bool AddExposureEnabled { get { return m_bAddExposureEnabled; } set { m_bAddExposureEnabled = value; } }
        public bool DeleteExposureEnabled { get { return m_bDeleteExposureEnabled; } set { m_bDeleteExposureEnabled = value; } }
        public bool EditExposureEnabled { get { return m_bEditExposureEnabled; } set { m_bEditExposureEnabled = value; } }
        public bool ExposureEnabled { get { return m_bExposureEnabled; } set { m_bExposureEnabled = value; } }  //Added by csingh7 MITS 20427
    }

    public struct McoUI
    {
        private bool m_bAddMcoEnabled;
        private bool m_bDeleteMcoEnabled;
        private bool m_bEditMcoEnabled;

        public bool AddMcoEnabled { get { return m_bAddMcoEnabled; } set { m_bAddMcoEnabled = value; } }
        public bool DeleteMcoEnabled { get { return m_bDeleteMcoEnabled; } set { m_bDeleteMcoEnabled = value; } }
        public bool EditMcoEnabled { get { return m_bEditMcoEnabled; } set { m_bEditMcoEnabled = value; } }
    }


    public class EnhancePolicyManager : Common
    {
        #region Global Variables

        private PolicyEnh m_objPolicyEnh = null;
        private TransactionManager m_objTransactionManager;
        private PremiumManager m_objPremiumManager;
        private ExposureManager m_objExposureManager;
        private CoverageManager m_objCoverageManager;
        
        private PolicyEnh PolicyEnh
        {
            get
            {
                return (m_objPolicyEnh);
            }
        }

        public TransactionManager TransactionManager
        {
            get
            {
                if (m_objTransactionManager == null)
                    m_objTransactionManager = new TransactionManager(PolicyEnh);

                return (m_objTransactionManager);
            }
        }

        public PremiumManager PremiumManager
        {
            get
            {
                if (m_objPremiumManager == null)
                    m_objPremiumManager = new PremiumManager(PolicyEnh);

                return (m_objPremiumManager);
            }
        }

        public ExposureManager ExposureManager
        {
            get
            {
                if (m_objExposureManager == null)
                    m_objExposureManager = new ExposureManager(PolicyEnh);

                return (m_objExposureManager);
            }
        }

        public CoverageManager CoverageManager
        {
            get
            {
                if (m_objCoverageManager == null)
                    m_objCoverageManager = new CoverageManager(PolicyEnh);

                return (m_objCoverageManager);
            }
        }

        #endregion 

        #region Constructor

        public EnhancePolicyManager(PolicyEnh p_objPolicyEnh): base(p_objPolicyEnh.Context)
        {
            m_objPolicyEnh = p_objPolicyEnh;
        }
        
        #endregion

        
        public bool PolicyAlreadyUpdated()
        {
            DbReader objReader = null;
            string sSQL = string.Empty;

            try
            {
                sSQL = " SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                
                if (objReader.Read())
                    // loop through the objects in our list and if it's not there, error
                    foreach (PolicyXTransEnh objPolicyXTransEnh in PolicyEnh.PolicyXTransEnhList)
                        if (objPolicyXTransEnh.TransactionId == Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId))
                            return false;
                return true; 
            }
            finally
            {
                if( objReader != null )
                    objReader.Close();

            }
        }

        public bool QuoteAlreadyConverted()
        {
            DbReader objReader = null;
            string sSQL = string.Empty;

            try
            {
                // read the quote record to see if another user has already converted it.
                sSQL = "SELECT POLICY_STATUS_CODE FROM POLICY_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    if (base.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue("POLICY_STATUS_CODE"), base.ClientId)) == "CV" )
                        return true;
                return false;
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();

            }
        }
        //tmalhotra2 MITS 27948 start
        private void GetDeductibleData(ref int Deductible, ref int FlatOrPercent, string sCoverageTypeCode, string sPolicyType, string sState, string sEffectiveDate)
        {
            StringBuilder scSql = null;
             DbReader objReader1 = null;
             try
             {
                 scSql = new StringBuilder();
                 scSql.Append("SELECT SYS_POL_EXP_RATES.FLAT_OR_PERCENT, SYS_POL_RANGE.DEDUCTIBLE FROM SYS_POL_EXP_RATES INNER JOIN SYS_POL_RANGE ");
                 scSql.Append("ON SYS_POL_EXP_RATES.EXP_RATE_ROWID = SYS_POL_RANGE.RATE_ROWID WHERE EXPOSURE_ID = ");
                 scSql.Append(sCoverageTypeCode);
                 scSql.Append(" AND LINE_OF_BUSINESS = ");
                 scSql.Append(sPolicyType);
                 scSql.Append(" AND STATE = ");
                 scSql.Append(sState);
                 scSql.Append(" AND EFFECTIVE_DATE <= '");
                 scSql.Append(sEffectiveDate);
                 scSql.Append("' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '");
                 scSql.Append(sEffectiveDate);
                 scSql.Append("'))");

                 objReader1 = DbFactory.GetDbReader(base.ConnectionString, scSql.ToString());
                 if (objReader1.Read())
                 {
                     Deductible = Conversion.ConvertObjToInt(objReader1.GetValue(1), base.ClientId);
                     FlatOrPercent = Conversion.ConvertObjToInt(objReader1.GetValue(0), base.ClientId);
                 }
                 objReader1.Close();
             }
             finally
             {
                 if(objReader1!=null)
             {
                 objReader1.Close();
                 objReader1.Dispose();
                 objReader1 = null;

             }
             }
            //tmalhotra2 MITS 27948 end
        }

        public void AmendPolicy(string p_sTransationDate)
        {
            DbReader objReader = null;
            DbReader objReaderTemp = null;
            string sSQL = string.Empty;
            int iTermNumber = 0 ;
            int iTransNumber = 0;
            int iMaxTransNumber = 0;
            string sPolcvgRowIdFilter = string.Empty;
            string sPolicyXExpEnhIdFilter = string.Empty;
            //Anu Tennyson for MITS 18229 New field UAR List on Coverages Screen
            string sLOB = string.Empty;
            //Anu Tennyson Ends

            //Start:Sumit (09/13/2010)-MITS# 21871
            XmlDocument objSerializationXMl = new XmlDocument();
            XmlDocument objCoverages = new XmlDocument();
            XmlDocument objExposure = new XmlDocument();
            //End:Sumit

            PolicyXTransEnh objPolicyXTransEnhNew   = null; 
            PolicyXTransEnh objPolicyXTransEnhTemp  = (PolicyXTransEnh)base.DataModelFactory.GetDataModelObject("PolicyXTransEnh", false);
            PolicyXCvgEnh objPolicyXCvgEnhNew       = null;
            PolicyXCvgEnh objPolicyXCvgEnhTemp      = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
            PolicyXExpEnh objPolicyXExpEnhNew       = null; 
            PolicyXExpEnh objPolicyXExpEnhTemp      = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            PolicyXRtngEnh objPolicyXRtngEnhNew     = null;
            PolicyXRtngEnh objPolicyXRtngEnhTemp    = (PolicyXRtngEnh)base.DataModelFactory.GetDataModelObject("PolicyXRtngEnh", false);
            PolicyXDcntEnh objPolicyXDcntEnhNew     = null;
            PolicyXDcntEnh objPolicyXDcntEnhTemp    = (PolicyXDcntEnh)base.DataModelFactory.GetDataModelObject("PolicyXDcntEnh", false);
            PolicyXDtierEnh objPolicyXDtierEnhNew   = null;
            PolicyXDtierEnh objPolicyXDtierEnhTemp  = (PolicyXDtierEnh)base.DataModelFactory.GetDataModelObject("PolicyXDtierEnh", false);
            
            //Geeta 02/12/07 : Declare for Enhance Policy
            PolicyXBillEnh objNewBill = null;
            PolicyXBillEnh objTempBill = (PolicyXBillEnh)base.DataModelFactory.GetDataModelObject("PolicyXBillEnh", false);

            //Sumit - Start(05/20/2010) - MITS# 20484 - Added for Uar details
            PolicyXUar objPolicyXUarNew = null;
            //Sumit - End

            //Sumit - Start(05/17/2010) - MITS# 20483 - Added for Schedule details
            PolicyXPschedEnh objPolicyXScheduleNew = null;
            //Sumit - End
            try
            {
                sLOB = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
                // Find the Term Number 
                sSQL = " SELECT MAX(TERM_NUMBER) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId;
                objReader = DbFactory.GetDbReader( base.ConnectionString , sSQL );

                if (objReader.Read())
                    iTermNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                // Find the Transation Number
                sSQL = " SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = "
                        + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber
                        + " AND TRANSACTION_TYPE <> " + base.CCacheFunctions.GetCodeIDWithShort("AU", "POLICY_TXN_TYPE");
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iTransNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();


                // Find the Max Transaction Id
                sSQL = " SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = "
                        + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iMaxTransNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();


                //Read for each object and copy values over to new POLICY_X_TRANS_ENH
                if ( iTransNumber != 0 )
                {
                    objPolicyXTransEnhTemp.MoveTo(iTransNumber);
                    objPolicyXTransEnhNew = PolicyEnh.PolicyXTransEnhList.AddNew();

                    objPolicyXTransEnhNew.CoverageRcdCount  = objPolicyXTransEnhTemp.CoverageRcdCount;
                    objPolicyXTransEnhNew.EffectiveDate     = Conversion.ToDbDate(Convert.ToDateTime(p_sTransationDate));
                    objPolicyXTransEnhNew.ExpirationDate = objPolicyXTransEnhTemp.ExpirationDate;
                    objPolicyXTransEnhNew.ExposureRcdCount  = objPolicyXTransEnhTemp.ExposureRcdCount;
                    objPolicyXTransEnhNew.PolicyId          = objPolicyXTransEnhTemp.PolicyId;
                    objPolicyXTransEnhNew.PolicyStatus      = objPolicyXTransEnhTemp.PolicyStatus;
                    if (iTransNumber == iMaxTransNumber)
                    {
                        objPolicyXTransEnhNew.SequenceAlpha = objPolicyXTransEnhTemp.SequenceAlpha + 1;
                    }
                    else
                    {
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iMaxTransNumber;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                        {
                            objPolicyXTransEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                        }
                        objReaderTemp.Close();
                    }
                    objPolicyXTransEnhNew.TermNumber        = objPolicyXTransEnhTemp.TermNumber;
                    objPolicyXTransEnhNew.TermSeqAlpha      = objPolicyXTransEnhTemp.TermSeqAlpha;
                    objPolicyXTransEnhNew.TotalBilledPrem   = objPolicyXTransEnhTemp.TotalBilledPrem;
                    objPolicyXTransEnhNew.TotalEstPremium   = objPolicyXTransEnhTemp.TotalEstPremium;
                    objPolicyXTransEnhNew.TransactionDate   = Conversion.ToDbDate(DateTime.Now); 
                    objPolicyXTransEnhNew.TransactionPrem   = objPolicyXTransEnhTemp.TransactionPrem;
                    objPolicyXTransEnhNew.TransactionStatus = base.CCacheFunctions.GetCodeIDWithShort("PR", "TRANSACTION_STATUS");
                    objPolicyXTransEnhNew.TransactionType   = base.CCacheFunctions.GetCodeIDWithShort("EN", "POLICY_TXN_TYPE");
                    //Ends
                    //npadhy RMSC retrofit Starts
                    objPolicyXTransEnhNew.TaxAmount = objPolicyXTransEnhTemp.TaxAmount;
                    objPolicyXTransEnhNew.TranTax = objPolicyXTransEnhTemp.TranTax;    
                    //npadhy RMSC retrofit Starts
                }
              

                //Read for each object and copy values over to new POLICY_X_CVG_ENH

                sSQL = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_ENH WHERE TRANSACTION_ID =" + iTransNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                
                while (objReader.Read())
                {
                    objPolicyXCvgEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    objPolicyXCvgEnhNew = PolicyEnh.PolicyXCvgEnhList.AddNew();

                    objPolicyXCvgEnhNew.BrokerName          = objPolicyXCvgEnhTemp.BrokerName;
                    objPolicyXCvgEnhNew.CancelNoticeDays    = objPolicyXCvgEnhTemp.CancelNoticeDays;
                    objPolicyXCvgEnhNew.ClaimLimit          = objPolicyXCvgEnhTemp.ClaimLimit;
                    objPolicyXCvgEnhNew.CoverageNumber      = objPolicyXCvgEnhTemp.CoverageNumber;
                    objPolicyXCvgEnhNew.CoverageTypeCode    = objPolicyXCvgEnhTemp.CoverageTypeCode;
                    objPolicyXCvgEnhNew.CovRenewableFlag    = objPolicyXCvgEnhTemp.CovRenewableFlag;
                    objPolicyXCvgEnhNew.Deductible          = objPolicyXCvgEnhTemp.Deductible;
                    objPolicyXCvgEnhNew.AmendedDate         = objPolicyXCvgEnhTemp.AmendedDate;
                    objPolicyXCvgEnhNew.EffectiveDate       = objPolicyXCvgEnhTemp.EffectiveDate;
                    objPolicyXCvgEnhNew.Exceptions          = objPolicyXCvgEnhTemp.Exceptions;
                    objPolicyXCvgEnhNew.ExpirationDate      = objPolicyXCvgEnhTemp.ExpirationDate;
                    objPolicyXCvgEnhNew.NextPolicyId        = objPolicyXCvgEnhTemp.NextPolicyId;
                    objPolicyXCvgEnhNew.NotificationUid     = objPolicyXCvgEnhTemp.NotificationUid;
                    objPolicyXCvgEnhNew.OccurrenceLimit     = objPolicyXCvgEnhTemp.OccurrenceLimit;
                    objPolicyXCvgEnhNew.PolicyId            = objPolicyXCvgEnhTemp.PolicyId;
                    objPolicyXCvgEnhNew.PolicyLimit         = objPolicyXCvgEnhTemp.PolicyLimit;
                    objPolicyXCvgEnhNew.Remarks             = objPolicyXCvgEnhTemp.Remarks;
                    objPolicyXCvgEnhNew.SelfInsRetention    = objPolicyXCvgEnhTemp.SelfInsRetention;
                    //Anu Tennyson for MITS 18229 1/15/2010 STARTS
                    objPolicyXCvgEnhNew.FlatOrPercent       = objPolicyXCvgEnhTemp.FlatOrPercent;
                    objPolicyXCvgEnhNew.Modifier = objPolicyXCvgEnhTemp.Modifier;
                    objPolicyXCvgEnhNew.ExpRateRowId = objPolicyXCvgEnhTemp.ExpRateRowId;
                    objPolicyXCvgEnhNew.Amount = objPolicyXCvgEnhTemp.Amount;
                    objPolicyXCvgEnhNew.CoverageSeq = objPolicyXCvgEnhTemp.CoverageSeq;
                    //Anu Tennyson
                    //To Save the UAR list in POLICY_X_INSRD_ENH with new polciy id.
                    if (sLOB == "AL")
                    {
                        if (objPolicyXCvgEnhTemp.PolicyXCvgUarAddedAL != null)
                        {
                            foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objPolicyXCvgEnhTemp.PolicyXCvgUarAddedAL)
                            {
                                objPolicyXCvgEnhNew.PolicyXCvgUarAddedAL.Add(objUARDictionaryEntry.Value);
                            }
                        }
                    }
                    else if (sLOB == "PC")
                    {
                        if (objPolicyXCvgEnhTemp.PolicyXCvgUarAddedPC != null)
                        {
                            foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objPolicyXCvgEnhTemp.PolicyXCvgUarAddedPC)
                            {
                                objPolicyXCvgEnhNew.PolicyXCvgUarAddedPC.Add(objUARDictionaryEntry.Value);
                            }
                        }
                    }
                    //Anu Tennyson
                    //Anu Tennyson for MITS 18229 ENDS

                    if (iTransNumber == iMaxTransNumber)
                    {
                        objPolicyXCvgEnhNew.SequenceAlpha   = objPolicyXCvgEnhTemp.SequenceAlpha + 1;
                    }
                    else
                    {
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_CVG_ENH WHERE TRANSACTION_ID =" + iMaxTransNumber;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString,sSQL);
                        if(objReaderTemp.Read())
                        {
                            objPolicyXCvgEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                        }
                        objReaderTemp.Close();
                    }
                    objPolicyXCvgEnhNew.TermNumber          = objPolicyXCvgEnhTemp.TermNumber;
                    objPolicyXCvgEnhNew.TotalPayments       = objPolicyXCvgEnhTemp.TotalPayments;
                   //objPolicyXCvgEnhNew.TransactionId       = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXCvgEnhNew.TransactionType     = objPolicyXTransEnhNew.TransactionType;
                
                    sSQL = "SELECT STATUS FROM POLICY_X_CVG_ENH WHERE TRANSACTION_ID =" + iTransNumber;
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReaderTemp.Read())
                    {
                        if(base.CCacheFunctions.GetShortCode(Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId))=="OK")
                            objPolicyXCvgEnhNew.Status  = base.CCacheFunctions.GetCodeIDWithShort("PR","COVEXP_STATUS");
                        else
                            objPolicyXCvgEnhNew.Status  = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);
                    }
                    objReaderTemp.Close();
                    //pmahli Supplementals

                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
                    objCoverages.LoadXml(objPolicyXCvgEnhTemp.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objCoverages, objPolicyXCvgEnhNew);

                    ////Raman : added logic to filter on correct coverage row id while populating supplementals
                    //sPolcvgRowIdFilter = "Instance/PolicyEnh/PolicyXCvgEnhList/PolicyXCvgEnh[PolcvgRowId = " + objPolicyXCvgEnhTemp.PolcvgRowId.ToString() + "]";
                    //PopulateSupplementals(sPolcvgRowIdFilter, objPolicyXCvgEnhNew);
                    //End:Sumit
                        
                }
                objReader.Close();


                //Read for each object and copy values over to new POLICY_X_EXP_ENH

                sSQL = "SELECT EXPOSURE_ID FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                while (objReader.Read())
                {
                    objPolicyXExpEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    objPolicyXExpEnhNew = PolicyEnh.PolicyXExposureEnhList.AddNew();

                    objPolicyXExpEnhNew.CoverageCode    = objPolicyXExpEnhTemp.CoverageCode;
                    // Start Naresh MITS 9770 Term Number was not getting saved in Case of Amend. 
                    // This was Giving Problem In Rate Calc
                    objPolicyXExpEnhNew.TermNumber = objPolicyXExpEnhTemp.TermNumber;
                    // End Naresh MITS 9770 Term Number was not getting saved in Case of Amend. 
                    objPolicyXExpEnhNew.AmendedDate     = objPolicyXExpEnhTemp.AmendedDate;
                    objPolicyXExpEnhNew.EffectiveDate   = objPolicyXExpEnhTemp.EffectiveDate;
                    objPolicyXExpEnhNew.Exceptions      = objPolicyXExpEnhTemp.Exceptions;
                    objPolicyXExpEnhNew.ExpirationDate  = objPolicyXExpEnhTemp.ExpirationDate;
                    objPolicyXExpEnhNew.ExposureAmount  = objPolicyXExpEnhTemp.ExposureAmount;
                    objPolicyXExpEnhNew.ExposureCode    = objPolicyXExpEnhTemp.ExposureCode;
                    objPolicyXExpEnhNew.ExposureCount   = objPolicyXExpEnhTemp.ExposureCount;
                    objPolicyXExpEnhNew.ExposureRate    = objPolicyXExpEnhTemp.ExposureRate;
                    objPolicyXExpEnhNew.ExposureBaseRate = objPolicyXExpEnhTemp.ExposureBaseRate;
                    objPolicyXExpEnhNew.FullAnnPremAmt  = objPolicyXExpEnhTemp.FullAnnPremAmt;
                    objPolicyXExpEnhNew.HeirarchyLevel  = objPolicyXExpEnhTemp.HeirarchyLevel;
                    objPolicyXExpEnhNew.PolicyId        = objPolicyXExpEnhTemp.PolicyId;
                    objPolicyXExpEnhNew.PrAnnPremAmt    = objPolicyXExpEnhTemp.PrAnnPremAmt;
                    objPolicyXExpEnhNew.Remarks         = objPolicyXExpEnhTemp.Remarks;
                    objPolicyXExpEnhNew.RenewableFlag   = objPolicyXExpEnhTemp.RenewableFlag;
                    if (iTransNumber == iMaxTransNumber)
                    {
                        objPolicyXExpEnhNew.SequenceAlpha = objPolicyXExpEnhTemp.SequenceAlpha + 1;
                    }
                    else
                    {
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID = " + iMaxTransNumber;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                        {
                            objPolicyXExpEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                        }
                        objReaderTemp.Close();
                    }
                    
                    //objPolicyXExpEnhNew.TransactionId   = objPolicyXTransEnhNew.TransactionId; 
                    objPolicyXExpEnhNew.TransactionType = base.CCacheFunctions.GetCodeIDWithShort("EN", "POLICY_TXN_TYPE");
                   
                    sSQL = "SELECT STATUS FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString,sSQL);
                    if (objReaderTemp.Read())
                    {
                        if (base.CCacheFunctions.GetShortCode(Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId)) == "OK")
                            objPolicyXExpEnhNew.Status  = base.CCacheFunctions.GetCodeIDWithShort("PR", "COVEXP_STATUS");
                        else
                            objPolicyXExpEnhNew.Status  = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);
                    } 
                    objReaderTemp.Close();
                    //pmahli Supplementals
                    //Raman : added logic to filter on correct coverage row id while populating supplementals

                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXExpEnh><Supplementals/></PolicyXExpEnh>");
                    objExposure.LoadXml(objPolicyXExpEnhTemp.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objExposure, objPolicyXExpEnhNew);
                    //sPolicyXExpEnhIdFilter = "Instance/PolicyEnh/PolicyXExposureEnhList/PolicyXExpEnh[ExposureId = " + objPolicyXExpEnhTemp.ExposureId.ToString() + "]";
                    //PopulateSupplementals(sPolicyXExpEnhIdFilter, objPolicyXExpEnhNew);
                    //End:Sumit

                    //Sumit - Start(05/20/2010) - MITS# 20484 - Update Exposure Children
                    if (objPolicyXExpEnhTemp.PolicyXUarList != null)
                    {
                        foreach (PolicyXUar objPolicyXUar in objPolicyXExpEnhTemp.PolicyXUarList)
                        {
                            objPolicyXUarNew = objPolicyXExpEnhNew.PolicyXUarList.AddNew();
                            objPolicyXUarNew.UarRowId = objPolicyXUar.UarRowId;
                            objPolicyXUarNew.PolicyId = objPolicyXUar.PolicyId;
                            objPolicyXUarNew.ExposureId = objPolicyXUar.ExposureId;
                            objPolicyXUarNew.UarId = objPolicyXUar.UarId;
                            objPolicyXUarNew.PolicyStatusCode = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");

                            //Sumit - Start(05/17/2010) - MITS# 20483 - Update Uar Children
                            if (objPolicyXUar.PolicyXPschedEnhList != null)
                            {
                                foreach (PolicyXPschedEnh objPolicyXPsched in objPolicyXUar.PolicyXPschedEnhList)
                                {
                                    objPolicyXScheduleNew = objPolicyXUarNew.PolicyXPschedEnhList.AddNew();
                                    objPolicyXScheduleNew.PschedRowID = objPolicyXPsched.PschedRowID;
                                    objPolicyXScheduleNew.PolicyId = objPolicyXPsched.PolicyId;
                                    objPolicyXScheduleNew.UarId = objPolicyXPsched.UarId;
                                    objPolicyXScheduleNew.Name = objPolicyXPsched.Name;
                                    objPolicyXScheduleNew.Amount = objPolicyXPsched.Amount;
                                }
                            }
                            //Sumit - End
                        }
                    }
                    //Sumit - End
                }
                objReader.Close();

                //Read for each object and copy values over to new POLICY_X_RTNG_ENH
                sSQL = "SELECT RATING_ID FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                
                if (objReader.Read())
                {
                    objPolicyXRtngEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    objPolicyXRtngEnhNew = PolicyEnh.PolicyXRatingEnhList.AddNew();

                    objPolicyXRtngEnhNew.ExpModFactor   = objPolicyXRtngEnhTemp.ExpModFactor;
                    objPolicyXRtngEnhNew.PolicyId       = objPolicyXRtngEnhTemp.PolicyId;
                    if (iTransNumber == iMaxTransNumber)
                    {
                        objPolicyXRtngEnhNew.SequenceAlpha = objPolicyXRtngEnhTemp.SequenceAlpha + 1;
                    }
                    else
                    {
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID = " + iMaxTransNumber;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                        {
                            objPolicyXRtngEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                        }
                         objReaderTemp.Close();
                    }
                    objPolicyXRtngEnhNew.TermNumber         = objPolicyXRtngEnhTemp.TermNumber;
                    objPolicyXRtngEnhNew.TransactionType    = objPolicyXTransEnhNew.TransactionType;
                    objPolicyXRtngEnhNew.ManualPremium = objPolicyXRtngEnhTemp.ManualPremium ;
                    objPolicyXRtngEnhNew.ModifiedPremium = objPolicyXRtngEnhTemp.ModifiedPremium;
                    objPolicyXRtngEnhNew.TransactionId = -1;
                   
                }
                objReader.Close();

                //Read for each object and copy values over to new POLICY_X_DCNT_ENH

                sSQL = "SELECT DISCOUNT_ID FROM POLICY_X_DCNT_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                
                while (objReader.Read())
                {
                    objPolicyXDcntEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    objPolicyXDcntEnhNew = PolicyEnh.PolicyXDiscountEnhList.AddNew();

                    objPolicyXDcntEnhNew.DiscountFactor = objPolicyXDcntEnhTemp.DiscountFactor;
                    objPolicyXDcntEnhNew.DiscountAmt = objPolicyXDcntEnhTemp.DiscountAmt;
                    objPolicyXDcntEnhNew.DiscountLevelId = objPolicyXDcntEnhTemp.DiscountLevelId;
                    objPolicyXDcntEnhNew.DiscountNameId = objPolicyXDcntEnhTemp.DiscountNameId;
                    objPolicyXDcntEnhNew.PolicyId       = objPolicyXDcntEnhTemp.PolicyId;
                    objPolicyXDcntEnhNew.TransactionId = -1;
             }
                objReader.Close();

                //Read for each object and copy values over to new POLICY_X_DTIER_ENH

                sSQL = "SELECT DISCOUNT_TIER_ID FROM POLICY_X_DTIER_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                
                while(objReader.Read())
                {
                    objPolicyXDtierEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    objPolicyXDtierEnhNew = PolicyEnh.PolicyXDiscountTierEnhList.AddNew();

                    objPolicyXDtierEnhNew.DiscTierLevelId   = objPolicyXDtierEnhTemp.DiscTierLevelId;
                    objPolicyXDtierEnhNew.DiscTierNameId    = objPolicyXDtierEnhTemp.DiscTierNameId;
                    objPolicyXDtierEnhNew.DiscTierAmt = objPolicyXDtierEnhTemp.DiscTierAmt;
                    objPolicyXDtierEnhNew.PolicyId          = objPolicyXDtierEnhTemp.PolicyId;
                    objPolicyXDtierEnhNew.TransactionId = -1;
                }
                objReader.Close();
                //CLONE POLICY BILLING REC
                //Geeta 02/12/07 : Adding Billing Check Starts
                if (base.UseBillingSystem)
                {
                    sSQL = "SELECT POLICY_BILLING_ID FROM POLICY_X_BILL_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReader.Read())
                    {   
                        objTempBill.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                        objNewBill = PolicyEnh.PolicyXBillEnhList.AddNew();
                        
                        // Start Naresh MITS 9806 Checkbox was getting Unselected after Postback    
                        objNewBill.DoNotBill = objTempBill.DoNotBill;
                        // End Naresh MITS 9806 Checkbox was getting Unselected after Postback    
                        
                        objNewBill.BillToEid = objTempBill.BillToEid;
                        objNewBill.BillToOverride = objTempBill.BillToOverride;
                        objNewBill.BillToType = objTempBill.BillToType;
                        objNewBill.NewBillToEid = objTempBill.NewBillToEid;
                        objNewBill.PayPlanRowId = objTempBill.PayPlanRowId;
                        objNewBill.BillingRuleRowId = objTempBill.BillingRuleRowId;
                        objNewBill.TermNumber = iTermNumber;
                        objNewBill.TransactionId = -1 ;                        
                    }

                    objReader.Close();
                }
                //Geeta 02/12/07 : Adding Billing Check Ends
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
               objPolicyXTransEnhNew = null;
               objPolicyXTransEnhTemp = null;
               objPolicyXCvgEnhNew = null;
               objPolicyXCvgEnhTemp = null;
               objPolicyXExpEnhNew = null;
               objPolicyXExpEnhTemp = null;
               objPolicyXRtngEnhNew = null;
               objPolicyXRtngEnhTemp = null;
               objPolicyXDcntEnhNew = null;
               objPolicyXDcntEnhTemp = null;
               objPolicyXDtierEnhNew = null;
               objPolicyXDtierEnhTemp = null;
               //Start:Sumit (09/13/2010)-MITS# 21871
               objSerializationXMl = null;
               objExposure = null;
               objCoverages = null;
               //End:Sumit
            }
        
        }

        public void AuditPolicy(int p_iTermNumber)
        {
            DbReader objReader = null;
            DbReader objReaderTemp = null;
            string sSQL = string.Empty;
            int iSeq = 0;
            int iTransNumber = 0;
            int iMaxTransNumber = 0;
            string sEffDate = string.Empty;
            string sExpDate = string.Empty;
            string sCancelDate = string.Empty;
            bool bRoundAmts = false;
            string sPolcvgRowIdFilter = string.Empty;
            string sPolicyXExpEnhIdFilter = string.Empty;
            //Anu Tennyson for MITS 18229 : Prem Calc STARTS
            StringBuilder sbOrgH = null;
            //Anu Tennyson for MITS 18229 : Prem Calc ENDS

            //Start:Sumit (09/13/2010)-MITS# 21871
            XmlDocument objSerializationXMl = new XmlDocument();
            XmlDocument objCoverages = new XmlDocument();
            XmlDocument objExposure = new XmlDocument();
            //End:Sumit

            PolicyXTransEnh objPolicyXTransEnhNew   = null;
            PolicyXTransEnh objPolicyXTransEnhTemp  = (PolicyXTransEnh)base.DataModelFactory.GetDataModelObject("PolicyXTransEnh", false);
            PolicyXCvgEnh objPolicyXCvgEnhNew       = null;
            PolicyXCvgEnh objPolicyXCvgEnhTemp      = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
            PolicyXExpEnh objPolicyXExpEnhNew       = null;
            PolicyXExpEnh objPolicyXExpEnhTemp      = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
            PolicyXRtngEnh objPolicyXRtngEnhNew     = null;
            PolicyXRtngEnh objPolicyXRtngEnhTemp    = (PolicyXRtngEnh)base.DataModelFactory.GetDataModelObject("PolicyXRtngEnh", false);
            PolicyXDcntEnh objPolicyXDcntEnhNew     = null;
            PolicyXDcntEnh objPolicyXDcntEnhTemp    = (PolicyXDcntEnh)base.DataModelFactory.GetDataModelObject("PolicyXDcntEnh", false);
            PolicyXDtierEnh objPolicyXDtierEnhNew   = null;
            PolicyXDtierEnh objPolicyXDtierEnhTemp  = (PolicyXDtierEnh)base.DataModelFactory.GetDataModelObject("PolicyXDtierEnh", false);

            //Geeta 02/12/07 : Declare for Enhance Policy
            
            PolicyXBillEnh objNewBill = null;
            PolicyXBillEnh objTempBill = (PolicyXBillEnh)base.DataModelFactory.GetDataModelObject("PolicyXBillEnh", false);
            try
            {
            //get the latest transaction
                sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + p_iTermNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iSeq            = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                sSQL = "SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + p_iTermNumber + " AND SEQUENCE_ALPHA = " + iSeq;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    sEffDate      = Convert.ToString(Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"))));
                    sExpDate      = Convert.ToString(Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"))));

                    //rsushilaggar MITS 25400 Date 07/11/2011
                    if (!string.IsNullOrEmpty(objReader.GetValue("CANCEL_DATE").ToString()))
                    {
                        sCancelDate = Convert.ToString(Conversion.ToDate(Conversion.ConvertObjToStr(objReader.GetValue("CANCEL_DATE"))));
                    }
                    
                }
                objReader.Close();

                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + p_iTermNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iTransNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + p_iTermNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iMaxTransNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                 //Read for each object and copy values over to new POLICY_X_TRANS_ENH
                if (iTransNumber != 0)
                {
                    
                    objPolicyXTransEnhTemp.MoveTo(iTransNumber);
                    objPolicyXTransEnhNew = PolicyEnh.PolicyXTransEnhList.AddNew();

                    objPolicyXTransEnhNew.CoverageRcdCount  = objPolicyXTransEnhTemp.CoverageRcdCount;
                    objPolicyXTransEnhNew.EffectiveDate     = Conversion.ToDbDate(Convert.ToDateTime(sEffDate));
                    //MITS 21495 rsushilaggar date 06/22/2011 set term's cancel date as expiration date of audit transaction
                    objPolicyXTransEnhNew.ExpirationDate = !string.IsNullOrEmpty(sCancelDate) ? Conversion.ToDbDate(Convert.ToDateTime(sCancelDate)) : Conversion.ToDbDate(Convert.ToDateTime(sExpDate));
                    objPolicyXTransEnhNew.ExposureRcdCount  = objPolicyXTransEnhTemp.ExposureRcdCount;
                    objPolicyXTransEnhNew.PolicyId          = objPolicyXTransEnhTemp.PolicyId;
                    objPolicyXTransEnhNew.PolicyStatus      = objPolicyXTransEnhTemp.PolicyStatus;
                    objPolicyXTransEnhNew.TermSeqAlpha      = objPolicyXTransEnhTemp.TermSeqAlpha;
                    objPolicyXTransEnhNew.TermNumber        = objPolicyXTransEnhTemp.TermNumber;
                    if (iTransNumber == iMaxTransNumber)
                    {
                        objPolicyXTransEnhNew.SequenceAlpha = objPolicyXTransEnhTemp.SequenceAlpha + 1;
                    }
                    else
                    {
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iMaxTransNumber;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                        {
                            objPolicyXTransEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                        }
                        objReaderTemp.Close();
                    }
                    objPolicyXTransEnhNew.TotalEstPremium   = objPolicyXTransEnhTemp.TotalEstPremium;
                    objPolicyXTransEnhNew.TransactionDate   = Conversion.ToDbDate(DateTime.Now);
                    objPolicyXTransEnhNew.TransactionStatus = base.CCacheFunctions.GetCodeIDWithShort("PR", "TRANSACTION_STATUS");
                    objPolicyXTransEnhNew.TransactionType   = base.CCacheFunctions.GetCodeIDWithShort("AU", "POLICY_TXN_TYPE");
                }
                objReader.Close();

                
                //Read for each object and copy values over to new POLICY_X_CVG_ENH

                sSQL = "SELECT * FROM POLICY_X_CVG_ENH WHERE TRANSACTION_ID =" + iTransNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                while (objReader.Read())
                {
                    objPolicyXCvgEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue("POLCVG_ROW_ID"), base.ClientId));
                    objPolicyXCvgEnhNew = PolicyEnh.PolicyXCvgEnhList.AddNew();

                    objPolicyXCvgEnhNew.BrokerName          = objPolicyXCvgEnhTemp.BrokerName;
                    objPolicyXCvgEnhNew.CancelNoticeDays    = objPolicyXCvgEnhTemp.CancelNoticeDays;
                    objPolicyXCvgEnhNew.ClaimLimit          = objPolicyXCvgEnhTemp.ClaimLimit;
                    objPolicyXCvgEnhNew.CoverageNumber      = objPolicyXCvgEnhTemp.CoverageNumber;
                    objPolicyXCvgEnhNew.CoverageTypeCode    = objPolicyXCvgEnhTemp.CoverageTypeCode;
                    objPolicyXCvgEnhNew.CovRenewableFlag    = objPolicyXCvgEnhTemp.CovRenewableFlag;
                    objPolicyXCvgEnhNew.Deductible          = objPolicyXCvgEnhTemp.Deductible;
                    objPolicyXCvgEnhNew.Modifier            = objPolicyXCvgEnhTemp.Modifier;
                    objPolicyXCvgEnhNew.FlatOrPercent       = objPolicyXCvgEnhTemp.FlatOrPercent;
                    objPolicyXCvgEnhNew.ExpRateRowId        = objPolicyXCvgEnhTemp.ExpRateRowId;
                    //pmahli MITS 9969 7/9/2007
                    //The query which was changed for MITS 9842 revrted as change in datamodel for coverage number serves the purpose
                    //Term number also passed in the query to get effective date when there is more than 1 term
                    // pmahli MITS 9842
                    // Naresh The Function to Fetch the Term Number is 
                    // removed as now correct Term Number is getting Passed. 
                    // Renew Policy is Changed for the same
                    sSQL = "SELECT EFFECTIVE_DATE FROM POLICY_X_CVG_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND COVERAGE_TYPE_CODE = " + Conversion.ConvertObjToInt(objReader.GetValue("COVERAGE_TYPE_CODE"), base.ClientId) + " AND COVERAGE_NUMBER = " + Conversion.ConvertObjToInt(objReader.GetValue("COVERAGE_NUMBER"), base.ClientId) + " AND SEQUENCE_ALPHA = 1 AND TERM_NUMBER = " + p_iTermNumber;
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReaderTemp.Read())
                        objPolicyXCvgEnhNew.EffectiveDate   = Conversion.ToDbDate(Conversion.ToDate(Conversion.ConvertObjToStr(objReaderTemp.GetValue(0))));
                    objReaderTemp.Close();
                    objPolicyXCvgEnhNew.Exceptions          = objPolicyXCvgEnhTemp.Exceptions;
                    objPolicyXCvgEnhNew.ExpirationDate      = objPolicyXCvgEnhTemp.ExpirationDate;
                    objPolicyXCvgEnhNew.NextPolicyId        = objPolicyXCvgEnhTemp.NextPolicyId;
                    objPolicyXCvgEnhNew.NotificationUid     = objPolicyXCvgEnhTemp.NotificationUid;
                    objPolicyXCvgEnhNew.OccurrenceLimit     = objPolicyXCvgEnhTemp.OccurrenceLimit;
                    objPolicyXCvgEnhNew.PolicyId            = objPolicyXCvgEnhTemp.PolicyId;
                    objPolicyXCvgEnhNew.PolicyLimit         = objPolicyXCvgEnhTemp.PolicyLimit;
                    objPolicyXCvgEnhNew.Remarks             = objPolicyXCvgEnhTemp.Remarks;
                    objPolicyXCvgEnhNew.SelfInsRetention    = objPolicyXCvgEnhTemp.SelfInsRetention;
                    if (iTransNumber == iMaxTransNumber)
                    {
                        objPolicyXCvgEnhNew.SequenceAlpha   = objPolicyXCvgEnhTemp.SequenceAlpha + 1;
                    }
                    else
                    {
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_CVG_ENH WHERE TRANSACTION_ID =" + iMaxTransNumber+ " AND POLICY_ID=" + PolicyEnh.PolicyId +" AND COVERAGE_NUMBER =" + Conversion.ConvertObjToInt(objReader.GetValue("COVERAGE_NUMBER"), base.ClientId);
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                        {
                            objPolicyXCvgEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                        }
                        objReaderTemp.Close();
                    }
                    objPolicyXCvgEnhNew.Status              = base.CCacheFunctions.GetCodeIDWithShort("PR", "COVEXP_STATUS");
                    objPolicyXCvgEnhNew.TermNumber          = objPolicyXCvgEnhTemp.TermNumber;
                    objPolicyXCvgEnhNew.TotalPayments       = objPolicyXCvgEnhTemp.TotalPayments;
                    // Start Naresh Transaction Id was not getting saved in Coverages while Auditing Policy
                    objPolicyXCvgEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXCvgEnhNew.TransactionType     = objPolicyXTransEnhNew.TransactionType;
                    //pmahli Supplementals

                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
                    objCoverages.LoadXml(objPolicyXCvgEnhTemp.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objCoverages, objPolicyXCvgEnhNew);
                    //sPolcvgRowIdFilter = "Instance/PolicyEnh/PolicyXCvgEnhList/PolicyXCvgEnh[PolcvgRowId = " + objPolicyXCvgEnhTemp.PolcvgRowId.ToString() + "]";
                    //PopulateSupplementals(sPolcvgRowIdFilter, objPolicyXCvgEnhNew);
                    //End:Sumit

                }
                objReader.Close();
                
                //get the round amounts indicator
                sSQL = "SELECT ROUND_AMTS_FLAG FROM SYS_POL_OPTIONS";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if(objReader.Read())
                    bRoundAmts = Conversion.ConvertObjToBool(objReader.GetValue(0), base.ClientId);
                objReader.Close();
                
                //Read for each object and copy values over to new POLICY_X_EXP_ENH
                sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                double dblManualPremium = 0;
                //Anu Tennyson for MITS 18229 : STARTS Prem Calc
                sbOrgH = new StringBuilder();
                //foreach (System.Collections.DictionaryEntry objInsuredDictionaryEntry in PolicyEnh.0PolicyXInsuredEnh)
                foreach (PolicyXInsuredEnh objPolicyXInsuredEnh in PolicyEnh.PolicyXInsuredEnhList)
                {
                    sbOrgH.Append(objPolicyXInsuredEnh.InsuredEid);
                    sbOrgH.Append(",");
                }
                //Anu Tennyson for MITS 18229 : ENDS Prem Calc
                while (objReader.Read())
                {
                    objPolicyXExpEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue("EXPOSURE_ID"), base.ClientId));
                    objPolicyXExpEnhNew = PolicyEnh.PolicyXExposureEnhList.AddNew();

                    objPolicyXExpEnhNew.CoverageCode = objPolicyXExpEnhTemp.CoverageCode;
                    //pmahli MITS 9969 7/9/2007
                    //The query which was changed for MITS 9842 revrted as change in datamodel for exposure count serves the purpose
                    //Term number also passed in the query to get effective date when there is more than 1 term
                    //we need to use the original effective date for this covg
                    //pmahli MITS 9842 
                    sSQL = "SELECT EFFECTIVE_DATE FROM POLICY_X_EXP_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND EXPOSURE_CODE = " + Conversion.ConvertObjToInt(objReader.GetValue("EXPOSURE_CODE"), base.ClientId) + " AND EXPOSURE_COUNT = " + Conversion.ConvertObjToInt(objReader.GetValue("EXPOSURE_COUNT"), base.ClientId) + " AND SEQUENCE_ALPHA = 1 AND TERM_NUMBER = " + p_iTermNumber;
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReaderTemp.Read())
                        objPolicyXExpEnhNew.EffectiveDate = Conversion.ToDbDate(Conversion.ToDate(Conversion.ConvertObjToStr(objReaderTemp.GetValue(0))));
                    objReaderTemp.Close();
                    objPolicyXExpEnhNew.Exceptions = objPolicyXExpEnhTemp.Exceptions;
                    objPolicyXExpEnhNew.ExpirationDate = objPolicyXExpEnhTemp.ExpirationDate;
                    objPolicyXExpEnhNew.ExposureAmount = objPolicyXExpEnhTemp.ExposureAmount;
                    objPolicyXExpEnhNew.ExposureCode = objPolicyXExpEnhTemp.ExposureCode;
                    objPolicyXExpEnhNew.ExposureCount = objPolicyXExpEnhTemp.ExposureCount;
                    objPolicyXExpEnhNew.ExposureRate = objPolicyXExpEnhTemp.ExposureRate;
                    objPolicyXExpEnhNew.ExposureBaseRate = objPolicyXExpEnhTemp.ExposureBaseRate;
                    objPolicyXExpEnhNew.FullAnnPremAmt = objPolicyXExpEnhTemp.FullAnnPremAmt;
                    objPolicyXExpEnhNew.HeirarchyLevel = objPolicyXExpEnhTemp.HeirarchyLevel;
                    objPolicyXExpEnhNew.PolicyId = objPolicyXExpEnhTemp.PolicyId;
                    objPolicyXExpEnhNew.PrAnnPremAmt = objPolicyXExpEnhTemp.PrAnnPremAmt;
                    objPolicyXExpEnhNew.Remarks = objPolicyXExpEnhTemp.Remarks;
                    objPolicyXExpEnhNew.RenewableFlag = objPolicyXExpEnhTemp.RenewableFlag;
                    if (iTransNumber == iMaxTransNumber)
                    {
                        objPolicyXExpEnhNew.SequenceAlpha = objPolicyXExpEnhTemp.SequenceAlpha + 1;
                    }
                    else
                    {
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID = " + iMaxTransNumber +"AND POLICY_ID=" + PolicyEnh.PolicyId + " AND EXPOSURE_COUNT=" +Conversion.ConvertObjToInt(objReader.GetValue("EXPOSURE_COUNT"), base.ClientId);
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                        {
                            objPolicyXExpEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                        }
                    }
                    objReaderTemp.Close();
                    objPolicyXExpEnhNew.Status = base.CCacheFunctions.GetCodeIDWithShort("PR", "COVEXP_STATUS");
                    objPolicyXExpEnhNew.TermNumber = objPolicyXExpEnhTemp.TermNumber;
                    // Start Naresh Transaction Id was not getting saved in Exposures while Auditing Policy
                    objPolicyXExpEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXExpEnhNew.TransactionType = objPolicyXTransEnhNew.TransactionType;
                    //pmahli Suppementals
                    
                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXExpEnh><Supplementals/></PolicyXExpEnh>");
                    objExposure.LoadXml(objPolicyXExpEnhTemp.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objExposure, objPolicyXExpEnhNew);
                    //sPolicyXExpEnhIdFilter = "Instance/PolicyEnh/PolicyXExposureEnhList/PolicyXExpEnh[ExposureId = " + objPolicyXExpEnhTemp.ExposureId.ToString() + "]";
                    //PopulateSupplementals(sPolicyXExpEnhIdFilter, objPolicyXExpEnhNew);
                    //End:Sumit

                    //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
                    //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                    //base.Rate(ref objPolicyXExpEnhNew, PolicyEnh.State, bRoundAmts, objPolicyXExpEnhNew.PrAnnPremAmt, objPolicyXExpEnhNew.FullAnnPremAmt, "AU");
                    //base.Rate(ref objPolicyXExpEnhNew, PolicyEnh.State, bRoundAmts, objPolicyXExpEnhNew.PrAnnPremAmt, objPolicyXExpEnhNew.FullAnnPremAmt, "AU", PolicyEnh.PolicyType);
                    //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                    //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation ENDS
                    //Anu Tennyson for MITS 18229 : Prem Calc STARTS
                    base.Rate(ref objPolicyXExpEnhNew, PolicyEnh.State, bRoundAmts, objPolicyXExpEnhNew.PrAnnPremAmt, objPolicyXExpEnhNew.FullAnnPremAmt, "AU", PolicyEnh.PolicyType, sbOrgH.ToString());
                    //Anu Tennyson for MITS 18229 : Prem Calc ENDS
                    dblManualPremium += objPolicyXExpEnhNew.PrAnnPremAmt;
                }
                    objReader.Close();

                    //Read for each object and copy values over to new POLICY_X_RTNG_ENH
                    sSQL = "SELECT RATING_ID FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReader.Read())
                    {
                        objPolicyXRtngEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                        objPolicyXRtngEnhNew    = PolicyEnh.PolicyXRatingEnhList.AddNew();

                        objPolicyXRtngEnhNew.ExpModFactor       = objPolicyXRtngEnhTemp.ExpModFactor;
                        objPolicyXRtngEnhNew.PolicyId           = objPolicyXExpEnhTemp.PolicyId;
                        if (iTransNumber == iMaxTransNumber)
                        {
                            objPolicyXRtngEnhNew.SequenceAlpha = objPolicyXRtngEnhTemp.SequenceAlpha + 1;
                        }
                        else
                        {
                            sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID = " + iMaxTransNumber;
                            objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                            if (objReaderTemp.Read())
                            {
                                objPolicyXRtngEnhNew.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId) + 1;
                            }
                            objReaderTemp.Close();
                        }
                        objPolicyXRtngEnhNew.TermNumber         = objPolicyXRtngEnhTemp.TermNumber;
                        objPolicyXRtngEnhNew.TotalEstPremium    = objPolicyXRtngEnhTemp.TotalEstPremium;
                        objPolicyXRtngEnhNew.TransactionType    = objPolicyXTransEnhNew.TransactionType;
                    objPolicyXRtngEnhNew.TransactionId = -1;
                    objPolicyXRtngEnhNew.ManualPremium = dblManualPremium;
                    PremiumManager.CalculateModifiedPremium(objPolicyXRtngEnhNew);
                    }
                    objReader.Close();

                    //Read for each object and copy values over to new POLICY_X_DCNT_ENH

                    sSQL = "SELECT DISCOUNT_ID FROM POLICY_X_DCNT_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    while (objReader.Read())
                    {
                        objPolicyXDcntEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                        objPolicyXDcntEnhNew = PolicyEnh.PolicyXDiscountEnhList.AddNew();

                        objPolicyXDcntEnhNew.DiscountFactor     = objPolicyXDcntEnhTemp.DiscountFactor;
                        objPolicyXDcntEnhNew.DiscountLevelId    = objPolicyXDcntEnhTemp.DiscountLevelId;
                        objPolicyXDcntEnhNew.DiscountNameId     = objPolicyXDcntEnhTemp.DiscountNameId;
                        objPolicyXDcntEnhNew.PolicyId           = objPolicyXDcntEnhTemp.PolicyId;
                    objPolicyXDcntEnhNew.TransactionId = -1;
                    }
                    objReader.Close();
                    //Read for each object and copy values over to new POLICY_X_DTIER_ENH

                    sSQL = "SELECT DISCOUNT_TIER_ID FROM POLICY_X_DTIER_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    while (objReader.Read())
                    {
                        objPolicyXDtierEnhTemp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                        objPolicyXDtierEnhNew = PolicyEnh.PolicyXDiscountTierEnhList.AddNew();

                        objPolicyXDtierEnhNew.DiscTierLevelId   = objPolicyXDtierEnhTemp.DiscTierLevelId;
                        objPolicyXDtierEnhNew.DiscTierNameId    = objPolicyXDtierEnhTemp.DiscTierNameId;
                        objPolicyXDtierEnhNew.PolicyId          = objPolicyXDtierEnhTemp.PolicyId;
                        objPolicyXDtierEnhNew.TransactionId = -1;
                    }
                    objReader.Close();
                    //CLONE POLICY BILLING REC

                    sSQL = "SELECT POLICY_BILLING_ID FROM POLICY_X_BILL_ENH WHERE TRANSACTION_ID = " + iTransNumber;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReader.Read())
                    {
                        objTempBill.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                        objNewBill = PolicyEnh.PolicyXBillEnhList.AddNew();
                        // Start Naresh MITS 9806 Checkbox was getting Unselected after Postback    
                        objNewBill.DoNotBill = objTempBill.DoNotBill;
                        // End Naresh MITS 9806 Checkbox was getting Unselected after Postback   
                        objNewBill.BillToEid = objTempBill.BillToEid;
                        objNewBill.BillToOverride = objTempBill.BillToOverride;
                        objNewBill.BillToType = objTempBill.BillToType;
                        objNewBill.NewBillToEid = objTempBill.NewBillToEid;
                        objNewBill.PayPlanRowId = objTempBill.PayPlanRowId;
                        objNewBill.BillingRuleRowId = objTempBill.BillingRuleRowId;
                        objNewBill.TermNumber = p_iTermNumber ;
                        objNewBill.TransactionId = -1;

                    }

                    objReader.Close();               

            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
                if (objReaderTemp != null)
                    objReaderTemp.Close();
                objPolicyXTransEnhNew = null;
                objPolicyXTransEnhTemp = null;
                objPolicyXCvgEnhNew = null;
                objPolicyXCvgEnhTemp = null;
                objPolicyXExpEnhNew = null;
                objPolicyXExpEnhTemp = null;
                objPolicyXRtngEnhNew = null;
                objPolicyXRtngEnhTemp = null;
                objPolicyXDcntEnhNew = null;
                objPolicyXDcntEnhTemp = null;
                objPolicyXDtierEnhNew = null;
                objPolicyXDtierEnhTemp = null;
                //Start:Sumit (09/13/2010)-MITS# 21871
                objSerializationXMl = null;
                objExposure = null;
                objCoverages = null;
                //End:Sumit
            }

        }

        //Cancel Policy Function : Added by Divya 01/07
        public void CancelPolicy(string p_sCancelType, string p_sCancelDate, string p_sCancelReason)
        {

            DbReader objReader = null;
            DbReader objReaderTemp = null;
            PolicyXTermEnh objNewTerm = null;
            PolicyXTransEnh objCancelTrans = null;
            PolicyXBillEnh objNewBill = null;
            PolicyXCvgEnh objNewCvg = null;
            PolicyXExpEnh objNewExp = null;
            PolicyXDcntEnh objNewDisc = null;
            PolicyXRtngEnh objNewRating = null;
            PolicyXDtierEnh objNewTier = null;
            PolicyXCvgEnh objTempCvg = null;
            PolicyXTermEnh objTempTerm = null;
            PolicyXTransEnh objTempTrans = null;
            PolicyXBillEnh objTempBill = null;
            PolicyXExpEnh objTempExp = null;
            PolicyXDcntEnh objTempDisc = null;
            PolicyXRtngEnh objTempRating = null;
            PolicyXDtierEnh objTempTier = null;
            string sPolcvgRowIdFilter = string.Empty;
            string sPolicyXExpEnhIdFilter = string.Empty;
            int iTermNumber = 0, iSeqNumber = 0, iMaxTransNumber = 0, iTransID = 0;
            int iPOLICY_STATUS = base.LocalCache.GetTableId("POLICY_STATUS");
            int iTRANSACTION_TYPE = base.LocalCache.GetTableId("POLICY_TXN_TYPE");
            int iTRANSACTION_STATUS = base.LocalCache.GetTableId("TRANSACTION_STATUS");
            int iTERM_STATUS = base.LocalCache.GetTableId("TERM_STATUS");
            int iCOV_EXP_STATUS = base.LocalCache.GetTableId("COVEXP_STATUS");
            string sSQL = null;
            string sCancelTxnType = null;
            //Anu Tennyson for MITS 18229 A New Field UAR List Added on Coverages Screen
            string sLOB = string.Empty;
            StringBuilder sbOrgH = null;
            //Anu Tennyson Ends

            //Sumit - Start(05/20/2010) - MITS# 20484 - Added for Uar details
            PolicyXUar objPolicyXUarNew = null;
            //Sumit - End

            //Sumit - Start(05/17/2010) - MITS# 20483 - Added for Schedule details
            PolicyXPschedEnh objPolicyXScheduleNew = null;
            //Sumit - End

            //Start:Sumit (09/13/2010)-MITS# 21871
            XmlDocument objSerializationXMl = new XmlDocument();
            XmlDocument objCoverages = new XmlDocument();
            XmlDocument objExposure = new XmlDocument();
            //End:Sumit

            try
            {

                //Anu Tenyson for MITS 18229 UARList field Added on Coverages Screen
                sLOB = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
                //End

                //read to find the most recent(greatest) term number
                sSQL = "SELECT MAX(TERM_NUMBER) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId;

                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iTermNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);

                objReader.Close();


                //read to find the greatest term sequence alpha

                sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iSeqNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);

                objReader.Close();

                //read to find the correct term record

                sSQL = "SELECT TERM_ID FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber + " and SEQUENCE_ALPHA = " + iSeqNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);




                if (objReader.Read())
                {
                    objNewTerm = PolicyEnh.PolicyXTermEnhList.AddNew();
                    objTempTerm = (PolicyXTermEnh)base.DataModelFactory.GetDataModelObject("PolicyXTermEnh", false);
                    //Fetch the record corresponding to term Id in temp object                   
                    objTempTerm.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    objNewTerm.AuditedFlag = objTempTerm.AuditedFlag;
                    objNewTerm.CancelDate = Conversion.ToDbDate(Convert.ToDateTime(p_sCancelDate));
                    if (p_sCancelReason == "")
                        p_sCancelReason = "0";
                    objNewTerm.CancelReason = Convert.ToInt32(p_sCancelReason);
                    objNewTerm.CancelType = Convert.ToInt32(p_sCancelType);
                    objNewTerm.EffectiveDate = objTempTerm.EffectiveDate;
                    objNewTerm.ExpirationDate = objTempTerm.ExpirationDate;
                    objNewTerm.PolicyId = objTempTerm.PolicyId;
                    objNewTerm.PolicyNumber = objTempTerm.PolicyNumber;
                    objNewTerm.RenewalDate = objTempTerm.RenewalDate;
                    objNewTerm.RenewalInd = objTempTerm.RenewalInd;
                    objNewTerm.RenewalTermAmt = objTempTerm.RenewalTermAmt;
                    objNewTerm.RenewalTermCode = objTempTerm.RenewalTermCode;
                    objNewTerm.SequenceAlpha = objTempTerm.SequenceAlpha + 1;
                    objNewTerm.TermNumber = objTempTerm.TermNumber;
                    objNewTerm.TermStatus = objTempTerm.TermStatus;
                    //npadhy RMSC retrofit Starts
                    objNewTerm.BrokerCommission = objTempTerm.BrokerCommission;    
                    //npadhy RMSC retrofit Ends
                    objTempTerm = null;
                }
                objReader.Close();

                //read to find the most recent transaction record

                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber + " AND TRANSACTION_TYPE <> " + base.CCacheFunctions.GetCodeIDWithShort("AU", "POLICY_TXN_TYPE");
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iTransID = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TERM_NUMBER = " + iTermNumber;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                    iMaxTransNumber = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();




                //TRANSACTION - create the renewal transaction
                sSQL = "SELECT TRANSACTION_ID FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iTransID;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                {
                    objCancelTrans = PolicyEnh.PolicyXTransEnhList.AddNew();
                    objTempTrans = (PolicyXTransEnh)base.DataModelFactory.GetDataModelObject("PolicyXTransEnh", false);
                    objTempTrans.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    //get the coverage count

                    sSQL = "SELECT COUNT(*) FROM POLICY_X_CVG_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TRANSACTION_ID = " + iTransID + " AND STATUS <> " + base.CCacheFunctions.GetCodeIDWithShort("DE", "COVEXP_STATUS");
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReaderTemp.Read())
                        // npadhy MITS 18903 The Data needs to be picked from objReaderTemp and not objReader
                        objCancelTrans.CoverageRcdCount = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);
                    objReaderTemp.Close();

                    objCancelTrans.DataChanged = true;
                    objCancelTrans.EffectiveDate = objNewTerm.CancelDate;
                    objCancelTrans.ExpirationDate = objNewTerm.ExpirationDate;

                    //get the exposure count
                    sSQL = "SELECT COUNT(*) FROM POLICY_X_EXP_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TRANSACTION_ID = " + iTransID;
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReaderTemp.Read())
                        // npadhy MITS 18903 The Data needs to be picked from objReaderTemp and not objReader
                        objCancelTrans.ExposureRcdCount = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);
                    objReaderTemp.Close();

                    objCancelTrans.PolicyId = PolicyEnh.PolicyId;

                    if (iTransID == iMaxTransNumber)
                        objCancelTrans.SequenceAlpha = objTempTrans.SequenceAlpha + 1;
                    else
                    {
                        // npadhy MITS 18903 We do not need to get all the columns from this Table. So changed the Query
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID=" + iMaxTransNumber;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                        if (objReaderTemp.Read())
                            // npadhy MITS 18903 When the policy is cancelled after audited, the TransId and MaxTransNumber is different
                            // In that case, the control used to come to else and used to crash as here objreader was used and not objReaderTemp
                            objCancelTrans.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                        objReaderTemp.Close();
                    }

                    objCancelTrans.TermNumber = objNewTerm.TermNumber;
                    objCancelTrans.TermSeqAlpha = objNewTerm.SequenceAlpha;
                    objCancelTrans.TransactionDate = Conversion.ToDbDate(DateTime.Today);
                    objCancelTrans.TransactionStatus = base.CCacheFunctions.GetCodeIDWithShort("PR", "TRANSACTION_STATUS");

                    if (base.LocalCache.GetShortCode(Convert.ToInt32(p_sCancelType)) == "FL")
                    {
                        sCancelTxnType = "CF";
                        objCancelTrans.PolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("PCF", "POLICY_STATUS");
                    }

                    else
                    {
                        if (base.LocalCache.GetShortCode(Convert.ToInt32(p_sCancelType)) == "PR")
                        {
                            sCancelTxnType = "CPR";
                            objCancelTrans.PolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("PCP", "POLICY_STATUS");
                        }

                    }
                    objCancelTrans.TransactionType = base.CCacheFunctions.GetCodeIDWithShort(sCancelTxnType, "POLICY_TXN_TYPE");
                }
                objReader.Close();
                objTempTrans = null;
                //PolicyEnh.PolicyXTransEnhList.Add (objCancelTrans);


                //now set the rest of the values on the new term record that need to be set
                //objNewTerm.TransactionId = objCancelTrans.TransactionId;
                //PolicyEnh.PolicyXTermEnhList.Add (objNewTerm);


                //read to find all the coverages for the term
                sSQL = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TRANSACTION_ID = " + iTransID + " ORDER BY COVERAGE_NUMBER";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                //loop through the list of coverages and clone them for the new term

                objTempCvg = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
                while (objReader.Read())
                {
                    objNewCvg = PolicyEnh.PolicyXCvgEnhList.AddNew();


                    objTempCvg.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));


                    //get the values from the read and fill into new term record

                    objNewCvg.BrokerName = objTempCvg.BrokerName;
                    objNewCvg.CancelNoticeDays = objTempCvg.CancelNoticeDays;
                    objNewCvg.ClaimLimit = objTempCvg.ClaimLimit;
                    objNewCvg.CoverageNumber = objTempCvg.CoverageNumber;
                    objNewCvg.CovRenewableFlag = objTempCvg.CovRenewableFlag;
                    objNewCvg.CoverageTypeCode = objTempCvg.CoverageTypeCode;
                    objNewCvg.DataChanged = true;
                    objNewCvg.Deductible = objTempCvg.Deductible;
                    objNewCvg.EffectiveDate = objTempCvg.EffectiveDate;
                    objNewCvg.Exceptions = objTempCvg.Exceptions;
                    objNewCvg.ExpirationDate = objNewTerm.CancelDate;
                    objNewCvg.NextPolicyId = objTempCvg.NextPolicyId;
                    objNewCvg.NotificationUid = objTempCvg.NotificationUid;
                    objNewCvg.OccurrenceLimit = objTempCvg.OccurrenceLimit;
                    objNewCvg.PolicyId = objTempCvg.PolicyId;
                    objNewCvg.PolicyLimit = objTempCvg.PolicyLimit;
                    objNewCvg.Remarks = objTempCvg.Remarks;
                    objNewCvg.SelfInsRetention = objTempCvg.SelfInsRetention;
                    //Anu Tennyson for MITS 18229 1/15/2010 STARTS
                    objNewCvg.FlatOrPercent = objTempCvg.FlatOrPercent;
                    objNewCvg.Modifier = objTempCvg.Modifier;
                    objNewCvg.ExpRateRowId = objTempCvg.ExpRateRowId;
                    objNewCvg.Amount = objTempCvg.Amount;
                    objNewCvg.CoverageSeq = objTempCvg.CoverageSeq;
                    if (sLOB == "AL")
                    {
                        if (objTempCvg.PolicyXCvgUarAddedAL != null)
                        {
                            foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objTempCvg.PolicyXCvgUarAddedAL)
                            {
                                objNewCvg.PolicyXCvgUarAddedAL.Add(objUARDictionaryEntry.Value);
                            }
                        }
                    }
                    else if (sLOB == "PC")
                    {
                        if (objTempCvg.PolicyXCvgUarAddedPC != null)
                        {
                            foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objTempCvg.PolicyXCvgUarAddedPC)
                            {
                                objNewCvg.PolicyXCvgUarAddedPC.Add(objUARDictionaryEntry.Value);
                            }
                        }
                    }
                    //Anu Tennyson for MITS 18229 ENDS
                    if (iTransID == iMaxTransNumber)
                        objNewCvg.SequenceAlpha = objTempCvg.SequenceAlpha + 1;
                    else
                    {
                        // npadhy MITS 18903 We do not need to get all the columns from this Table. So changed the Query
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_CVG_ENH WHERE TRANSACTION_ID =" + iMaxTransNumber + " AND POLICY_ID=" + PolicyEnh.PolicyId + " AND COVERAGE_NUMBER =" + objTempCvg.CoverageNumber;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                            // npadhy MITS 18903 When the policy is cancelled after audited, the TransId and MaxTransNumber is different
                            // In that case, the control used to come to else and used to crash as here objreader was used and not objReaderTemp
                            objNewCvg.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;

                        objReaderTemp.Close();
                    }

                    if (base.LocalCache.GetShortCode(objTempCvg.Status) == "OK")
                        objNewCvg.Status = base.CCacheFunctions.GetCodeIDWithShort("PR", "COVEXP_STATUS");

                    objNewCvg.Status = objTempCvg.Status;
                    objNewCvg.TermNumber = objNewTerm.TermNumber;
                    objNewCvg.TotalPayments = objTempCvg.TotalPayments;

                    objNewCvg.TransactionType = objCancelTrans.TransactionType;
                    //pmahli Supplemetals
                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
                    objCoverages.LoadXml(objTempCvg.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objCoverages, objNewCvg);
                    //sPolcvgRowIdFilter = "Instance/PolicyEnh/PolicyXCvgEnhList/PolicyXCvgEnh[PolcvgRowId = " + objTempCvg.PolcvgRowId.ToString() + "]";
                    //PopulateSupplementals(sPolcvgRowIdFilter, objNewCvg);
                    //End:Sumit

                    //PolicyEnh.PolicyXCvgEnhList.Add(objNewCvg);


                }
                objTempCvg = null;
                objReader.Close();


               

                //read to find all the exp for the term
                sSQL = "SELECT EXPOSURE_ID FROM POLICY_X_EXP_ENH WHERE POLICY_ID = " + PolicyEnh.PolicyId + " AND TRANSACTION_ID = " + iTransID + " ORDER BY EXPOSURE_COUNT";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objTempExp = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                double dblManualPremium = 0;
                //Anu Tennyson for MITS 18229 STARTS : Prem Calc
                sbOrgH = new StringBuilder();
                foreach (PolicyXInsuredEnh objPolicyXInsuredEnh in PolicyEnh.PolicyXInsuredEnhList)
                {
                    sbOrgH.Append(objPolicyXInsuredEnh.InsuredEid);
                    sbOrgH.Append(",");
                }
                //Anu Tennyson for MITS 18229 ENDS : Prem Calc
                while (objReader.Read())
                {

                    //get the values from the read and fill into new term record
                    objNewExp = PolicyEnh.PolicyXExposureEnhList.AddNew();

                    objTempExp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    objNewExp.CoverageCode = objTempExp.CoverageCode;
                    objNewExp.DataChanged = true;
                    objNewExp.EffectiveDate = objTempExp.EffectiveDate;
                    objNewExp.Exceptions = objTempExp.Exceptions;
                    objNewExp.ExpirationDate = objNewTerm.CancelDate;
                    objNewExp.ExposureAmount = objTempExp.ExposureAmount;
                    objNewExp.ExposureCode = objTempExp.ExposureCode;
                    objNewExp.ExposureCount = objTempExp.ExposureCount;
                    objNewExp.ExposureRate = objTempExp.ExposureRate;
                    objNewExp.ExposureBaseRate = objTempExp.ExposureBaseRate;
                    objNewExp.FullAnnPremAmt = objTempExp.FullAnnPremAmt;
                    objNewExp.HeirarchyLevel = objTempExp.HeirarchyLevel;
                    objNewExp.PolicyId = PolicyEnh.PolicyId;
                    objNewExp.PrAnnPremAmt = objTempExp.PrAnnPremAmt;
                    objNewExp.Remarks = objTempExp.Remarks;
                    objNewExp.RenewableFlag = objTempExp.RenewableFlag;

                    if (iTransID == iMaxTransNumber)
                        objNewExp.SequenceAlpha = objTempExp.SequenceAlpha + 1;
                    else
                    {
                        // npadhy MITS 18903 We do not need to get all the columns from this Table. So changed the Query
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID =" + iMaxTransNumber + " AND POLICY_ID=" + PolicyEnh.PolicyId + " AND EXPOSURE_COUNT=" + objTempExp.ExposureCount;

                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                            objNewExp.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                        objReaderTemp.Close();
                    }

                    if (base.LocalCache.GetShortCode(objTempExp.Status) == "OK")
                        objNewExp.Status = base.CCacheFunctions.GetCodeIDWithShort("PR", "COVEXP_STATUS");
                    else
                        objNewExp.Status = objTempExp.Status;

                    objNewExp.TermNumber = objNewTerm.TermNumber;
                    objNewExp.TransactionType = objCancelTrans.TransactionType;
                    objNewExp.TransactionId = objCancelTrans.TransactionId;

                    //pmahli Supplementals
                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXExpEnh><Supplementals/></PolicyXExpEnh>");
                    objExposure.LoadXml(objTempExp.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objExposure, objNewExp);
                    //sPolicyXExpEnhIdFilter = "Instance/PolicyEnh/PolicyXExposureEnhList/PolicyXExpEnh[ExposureId = " + objTempExp.ExposureId.ToString() + "]";
                    //PopulateSupplementals(sPolicyXExpEnhIdFilter, objNewExp);
                    //End:Sumit

                    if (base.LocalCache.GetShortCode(objNewExp.Status) != "DE")
                        //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                        //base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, objNewExp.PrAnnPremAmt, objNewExp.FullAnnPremAmt, base.LocalCache.GetShortCode(objCancelTrans.TransactionType)); //TODO rate function
                        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
                        //base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, objNewExp.PrAnnPremAmt, objNewExp.FullAnnPremAmt, base.LocalCache.GetShortCode(objCancelTrans.TransactionType),PolicyEnh.PolicyType); //TODO rate function
                        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation ENDS
                        //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                        //Anu Tennyson for MITS 18229 : Prem Calc STARTS
                        base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, objNewExp.PrAnnPremAmt, objNewExp.FullAnnPremAmt, base.LocalCache.GetShortCode(objCancelTrans.TransactionType), PolicyEnh.PolicyType, sbOrgH.ToString()); //TODO rate function
                    //Anu Tennyson for MITS 18229 : Prem Calc ENDS
                    dblManualPremium += objNewExp.PrAnnPremAmt;
                    //PolicyEnh.PolicyXExposureEnhList.Add( objNewExp);

                    //Sumit - Start(05/20/2010) - MITS# 20484 - Update Exposure Children
                    if (objTempExp.PolicyXUarList != null)
                    {
                        foreach (PolicyXUar objPolicyXUar in objTempExp.PolicyXUarList)
                        {
                            objPolicyXUarNew = objNewExp.PolicyXUarList.AddNew();
                            objPolicyXUarNew.UarRowId = objPolicyXUar.UarRowId;
                            objPolicyXUarNew.PolicyId = objPolicyXUar.PolicyId;
                            objPolicyXUarNew.ExposureId = objPolicyXUar.ExposureId;
                            objPolicyXUarNew.UarId = objPolicyXUar.UarId;
                            objPolicyXUarNew.PolicyStatusCode = objCancelTrans.PolicyStatus;

                            //Sumit - Start(05/17/2010) - MITS# 20483 - Update Uar Children
                            if (objPolicyXUar.PolicyXPschedEnhList != null)
                            {
                                foreach (PolicyXPschedEnh objPolicyXPsched in objPolicyXUar.PolicyXPschedEnhList)
                                {
                                    objPolicyXScheduleNew = objPolicyXUarNew.PolicyXPschedEnhList.AddNew();
                                    objPolicyXScheduleNew.PschedRowID = objPolicyXPsched.PschedRowID;
                                    objPolicyXScheduleNew.PolicyId = objPolicyXPsched.PolicyId;
                                    objPolicyXScheduleNew.UarId = objPolicyXPsched.UarId;
                                    objPolicyXScheduleNew.Name = objPolicyXPsched.Name;
                                    objPolicyXScheduleNew.Amount = objPolicyXPsched.Amount;
                                }
                            }
                            //Sumit - End
                        }
                    }
                    //Sumit - End
                }//end while

                objReader.Close();



                //read to find the most recent rating record and store it in m_OldRating
                sSQL = "SELECT RATING_ID FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID = " + iTransID;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                if (objReader.Read())
                {
                    objNewRating = PolicyEnh.PolicyXRatingEnhList.AddNew();

                    objTempRating = (PolicyXRtngEnh)base.DataModelFactory.GetDataModelObject("PolicyXRtngEnh", false);

                    objTempRating.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    objReader.Close();

                    objNewRating.ExpModFactor = objTempRating.ExpModFactor;
                    objNewRating.PolicyId = PolicyEnh.PolicyId;
                    if (iTransID == iMaxTransNumber)
                        objNewRating.SequenceAlpha = objTempRating.SequenceAlpha + 1;
                    else
                    {
                        // npadhy MITS 18903 We do not need to get all the columns from this Table. So changed the Query
                        sSQL = "SELECT SEQUENCE_ALPHA FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID=" + iMaxTransNumber;
                        objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                        if (objReader.Read())

                            objNewRating.SequenceAlpha = Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                        objReader.Close();
                    }



                    objNewRating.TermNumber = objNewTerm.TermNumber;
                    objNewRating.TransactionId = -1;
                    objNewRating.TransactionType = objCancelTrans.TransactionType;
                    objNewRating.ManualPremium = dblManualPremium;
                    PremiumManager.CalculateModifiedPremium(objNewRating);
                    objTempRating = null;

                }
                else
                {
                    if (objReader != null)
                        objReader.Close();
                }

                //PolicyEnh.PolicyXRatingEnhList.Add(objNewRating);

                sSQL = "SELECT DISCOUNT_ID FROM POLICY_X_DCNT_ENH WHERE TRANSACTION_ID=" + iTransID;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objTempDisc = (PolicyXDcntEnh)base.DataModelFactory.GetDataModelObject("PolicyXDcntEnh", false);

                while (objReader.Read())
                {

                    //get the values from the read and fill into new term record
                    objNewDisc = PolicyEnh.PolicyXDiscountEnhList.AddNew();
                    objTempDisc.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    objNewDisc.DiscountFactor = objTempDisc.DiscountFactor;
                    objNewDisc.DiscountLevelId = objTempDisc.DiscountLevelId;
                    objNewDisc.DiscountNameId = objTempDisc.DiscountNameId;
                    objNewDisc.PolicyId = PolicyEnh.PolicyId;
                    objNewDisc.RatingId = objNewRating.RatingId;
                    objNewDisc.TransactionId = -1;
                    //PolicyEnh.PolicyXDiscountEnhList.Add(objNewDisc);
                }
                objTempDisc = null;
                objReader.Close();

                sSQL = "SELECT DISCOUNT_TIER_ID FROM POLICY_X_DTIER_ENH WHERE TRANSACTION_ID=" + iTransID;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                objTempTier = (PolicyXDtierEnh)base.DataModelFactory.GetDataModelObject("PolicyXDtierEnh", false);
                while (objReader.Read())
                {
                    //get the values from the read and fill into new term record

                    objNewTier = PolicyEnh.PolicyXDiscountTierEnhList.AddNew();
                    objTempTier.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    objNewTier.DiscTierLevelId = objTempTier.DiscTierLevelId;
                    objNewTier.DiscTierNameId = objTempTier.DiscTierNameId;
                    objNewTier.PolicyId = PolicyEnh.PolicyId;
                    objNewTier.RatingId = objNewRating.RatingId;
                    objNewTier.TransactionId = -1;
                    //PolicyEnh.PolicyXDiscountTierEnhList.Add(objNewTier);
                }
                objReader.Close();
                objTempTier = null;

                //CLONE POLICY BILLING REC
                if (base.UseBillingSystem )
                {
                    sSQL = "SELECT POLICY_BILLING_ID FROM POLICY_X_BILL_ENH WHERE TRANSACTION_ID = " + iTransID;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    objTempBill = (PolicyXBillEnh)base.DataModelFactory.GetDataModelObject("PolicyXBillEnh", false);

                    while (objReader.Read())
                    {

                        //get the values from the read and fill into new term record
                        objTempBill.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                        objNewBill = PolicyEnh.PolicyXBillEnhList.AddNew();
                        objNewBill.BillToEid = objTempBill.BillToEid;
                        objNewBill.BillToOverride = objTempBill.BillToOverride;
                        objNewBill.BillToType = objTempBill.BillToType;
                        objNewBill.NewBillToEid = objTempBill.NewBillToEid;
                        objNewBill.PayPlanRowId = objTempBill.PayPlanRowId;
                        objNewBill.BillingRuleRowId = objTempBill.BillingRuleRowId;
                        objNewBill.TermNumber = objNewTerm.TermNumber;
                        objNewBill.TransactionId = -1;
                        // Naresh Changes for DoNotBill field
                        objNewBill.DoNotBill = objTempBill.DoNotBill;
                    }
                    objReader.Close();
                    objTempBill = null;
                }
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }

            finally
            {
                objReader.Close();
                objCancelTrans = null;
                objNewBill = null;
                objNewCvg = null;
                objNewDisc = null;
                objNewExp = null;
                objNewRating = null;
                objNewTerm = null;
                objNewTier = null;
                objReader = null;
                //Start:Sumit (09/13/2010)-MITS# 21871
                objSerializationXMl = null;
                objExposure = null;
                objCoverages = null;
                //End:Sumit
            }

        }

        public void RenewPolicy()
        {
            try
            {

                String sSQL = "", sDt = "", sPolNum = "", sPolYear = "", sTransNum = "", sTermCode = "";
                DateTime objEffDate = DateTime.MinValue;
                DateTime objExpDate = DateTime.MinValue;

                string sOldEffDate = string.Empty;
                string sOldExpDate = string.Empty;
                DateTime dtOldExpDate = DateTime.MinValue;
                int iPolicyid = 0, iMaxTermNum = 0, iTermAmt = 0, iSeqNum = 0;
                double dPolYrNum = 0;
                bool bYear = false, bCovRenewFlag = false;
                string sPlacement = "";
                //Anu Tennyson for MITS 18229 STARTS 1/20/2010
                StringBuilder sbSql = null;
                int deductible = 0;
                int flatorpercent = 0;
                int iCountDed = 0;
                bool bIsSuccess = false;
                string sLob = string.Empty;
                StringBuilder sbOrgH = null;
                //Anu Tennyson for MITS 18229 ENDS

                //Sumit - Start(05/20/2010) - MITS# 20484 - Added for Uar details
                PolicyXUar objPolicyXUarNew = null;
                //Sumit - End

                //Sumit - Start(05/17/2010) - MITS# 20483 - Added for Schedule details
                PolicyXPschedEnh objPolicyXScheduleNew = null;
                //Sumit - End

                //Start:Sumit (09/13/2010)-MITS# 21871
                XmlDocument objSerializationXMl = new XmlDocument();
                XmlDocument objCoverages = new XmlDocument();
                XmlDocument objExposure = new XmlDocument();
                //End:Sumit

                //New objects for renewal
                PolicyXTransEnh objRenewalTrans = null;
                PolicyXTermEnh objNewTerm = null;
                PolicyXCvgEnh objNewCvg = null;
                PolicyXCvgEnh objTempCvg = null;
                PolicyXExpEnh objNewExp = null;
                PolicyXExpEnh objTempExp = null;
                PolicyXRtngEnh objNewRating = null;
                PolicyXBillEnh objNewBill = null;
                PolicyXBillEnh objTempBill = null;
                DbReader objReader = null;
                DbReader objReader2 = null;
                DbReader objReaderTemp = null;
                string sPolcvgRowIdFilter = string.Empty;
                string sPolicyXExpEnhIdFilter = string.Empty;
                //get settings from SYS_POL_OPtions
                sSQL = "SELECT USE_YEAR_FLAG,PLACEMENT FROM SYS_POL_OPTIONS";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    bYear = Conversion.ConvertObjToBool(objReader.GetValue(0) , base.ClientId);
                    sPlacement = base.LocalCache.GetShortCode(Conversion.ConvertObjToInt(objReader.GetValue(1), base.ClientId));
                }
                objReader.Close();


                sDt = Conversion.ToDbDate(DateTime.Now);
                iPolicyid = PolicyEnh.PolicyId;

                //read to find the most recent(greatest) term number
                sSQL = "SELECT MAX(TERM_NUMBER) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyid;

                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    iMaxTermNum = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                }
                objReader.Close();

                //read to find the greatest term sequence alpha
                sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyid + " AND TERM_NUMBER = " + iMaxTermNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    iSeqNum = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                }
                objReader.Close();


                //read to find the correct term record
                sSQL = "SELECT * FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyid + " AND TERM_NUMBER = " + iMaxTermNum + " and SEQUENCE_ALPHA = " + iSeqNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    //get the values from the read and fill into new term record
                    objNewTerm = PolicyEnh.PolicyXTermEnhList.AddNew();
                    objNewTerm.AuditedFlag = false;
                    //based on the renewal term amt/code set during orig policy acceptance, set new term dates
                    int iTermCode = Conversion.ConvertObjToInt(objReader.GetValue("RENEWAL_TERM_CODE"), base.ClientId);
                    if( iTermCode != 0 )
                        sTermCode = base.LocalCache.GetShortCode(iTermCode);
                    iTermAmt = Conversion.ConvertObjToInt(objReader.GetValue("RENEWAL_TERM_AMT"), base.ClientId);

                    sOldEffDate = Conversion.ConvertObjToStr(objReader.GetValue("EFFECTIVE_DATE"));
                    sOldExpDate = Conversion.ConvertObjToStr(objReader.GetValue("EXPIRATION_DATE"));
                    objEffDate = Conversion.ToDate(sOldEffDate);
                    objExpDate = Conversion.ToDate(sOldExpDate);


                    //If there is no setting for the State and LOB in Utility then
                    //take the first valid period for policy defined at creation
                    if (sTermCode == "" && iTermAmt == 0)
                    {
                        TimeSpan tempDays = objExpDate.Subtract(objEffDate);
                        iTermAmt = tempDays.Days;
                        //PJS MITS 11200, 02-27-08: added 1 day to correct the effective date 
                        iTermAmt = iTermAmt + 1; 
                        sTermCode = "D";
                    }
                    //alter the years for effective, expiration and renew date
                    objEffDate = objExpDate.AddDays(1);
                    objNewTerm.EffectiveDate = Conversion.ToDbDate(objEffDate);

                    if (sTermCode == "Y")
                        objExpDate = objExpDate.AddYears(iTermAmt);
                    else
                    {
                        if (sTermCode == "M")
                            objExpDate = objExpDate.AddMonths(iTermAmt);
                        else
                            if (sTermCode == "D")
                                objExpDate = objExpDate.AddDays(iTermAmt);
                    }
                    objNewTerm.ExpirationDate = Conversion.ToDbDate(objExpDate);

                    objNewTerm.PolicyId = iPolicyid;
                    sPolNum = Conversion.ConvertObjToStr(objReader.GetValue("POLICY_NUMBER"));
                    //if the use year flag is true, adjust the policy name accordingly

                    if (bYear == true)
                    {
                        if ((sPolNum != "") && ((sPolNum.Length) > 3))
                        {
                            if (sPlacement == "E")
                            {
                                sPolYear = sPolNum.Substring(sPolNum.Length - 4, 4);

                                if ((Double.TryParse(sPolYear, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out dPolYrNum)) == false)

                                    objNewTerm.PolicyNumber = sPolNum;
                                else
                                {
                                    sPolYear = Convert.ToString(dPolYrNum + 1);
                                    sPolNum = sPolNum.Substring(0, sPolNum.Length - 4) + sPolYear;
                                }
                            }

                            else
                            {
                                if (sPlacement == "B")
                                {
                                    sPolYear = sPolNum.Substring(0, 4);
                                    dPolYrNum = 0;
                                    if ((Double.TryParse(sPolYear, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out dPolYrNum)) == false)
                                        objNewTerm.PolicyNumber = sPolNum;
                                    else
                                    {
                                        sPolYear = Convert.ToString(dPolYrNum + 1);
                                        // npadhy MITS 18912 When the Policy Number is substringed after four chracters, 
                                        // taking entire string length would crash the system
                                        // sPolNum = sPolYear + sPolNum.Substring(sPolNum.Length - 4, sPolNum.Length);
                                        sPolNum = sPolYear + sPolNum.Substring(4);
                                    }
                                }
                            }
                        }
                        objNewTerm.PolicyNumber = sPolNum;
                    }
                    else
                        objNewTerm.PolicyNumber = sPolNum;

                    objNewTerm.RenewalInd = Conversion.ConvertObjToInt(objReader.GetValue("RENEWAL_IND"), base.ClientId);

                    objNewTerm.TermNumber = Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), base.ClientId) + 1;

                    objNewTerm.TermStatus = base.LocalCache.GetCodeId("PR", "TERM_STATUS");
                    objNewTerm.SequenceAlpha = 1;

                    //get the renewal info from utilities
                    sSQL = "SELECT * FROM SYS_POL_RNWL_INFO WHERE LINE_OF_BUSINESS=" + PolicyEnh.PolicyType + " AND STATE=" + PolicyEnh.State;
                    objReader2 = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    if (objReader2.Read())
                    {
                        objNewTerm.RenewalTermAmt = Conversion.ConvertObjToInt(objReader2.GetValue("AMOUNT"), base.ClientId);
                        objNewTerm.RenewalTermCode = Conversion.ConvertObjToInt(objReader2.GetValue("TERM_CODE"), base.ClientId);
                    }
                    objReader2.Close();
                }
                objReader.Close();

                //read to find the most recent transaction record
                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + iPolicyid + " AND TERM_NUMBER = " + iMaxTermNum + " AND TRANSACTION_TYPE <> " + base.LocalCache.GetCodeId("AU", "POLICY_TXN_TYPE");
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    sTransNum = Conversion.ConvertObjToStr(objReader.GetValue(0));
                objReader.Close();


                //TRANSACTION - create the renewal transaction
                objRenewalTrans = PolicyEnh.PolicyXTransEnhList.AddNew();
                //get the coverage count
                sSQL = "SELECT COUNT(*) FROM POLICY_X_CVG_ENH WHERE POLICY_ID = " + iPolicyid + " AND TRANSACTION_ID = " + sTransNum;
                objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReaderTemp.Read())
                    objRenewalTrans.CoverageRcdCount = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);
                objReaderTemp.Close();
                objRenewalTrans.DataChanged = true;
                objRenewalTrans.EffectiveDate = objNewTerm.EffectiveDate;

                //get the exposure count
                sSQL = "SELECT COUNT(*) FROM POLICY_X_EXP_ENH WHERE POLICY_ID = " + iPolicyid + " AND TRANSACTION_ID = " + sTransNum;
                objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReaderTemp.Read())
                {
                    objRenewalTrans.ExposureRcdCount = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);
                }
                objReaderTemp.Close();
                objRenewalTrans.ExpirationDate = objNewTerm.ExpirationDate;
                objRenewalTrans.PolicyId = PolicyEnh.PolicyId;
                objRenewalTrans.PolicyStatus = base.LocalCache.GetCodeId("PR", "POLICY_STATUS");
                objRenewalTrans.SequenceAlpha = 1;
                objRenewalTrans.TermNumber = objNewTerm.TermNumber;
                objRenewalTrans.TermSeqAlpha = 1;
                objRenewalTrans.TransactionDate = sDt;
                objRenewalTrans.TransactionStatus = base.LocalCache.GetCodeId("PR", "TRANSACTION_STATUS");
                objRenewalTrans.TransactionType = base.LocalCache.GetCodeId("RN", "POLICY_TXN_TYPE");

                //read to find all the coverages for the term
                sSQL = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_ENH WHERE POLICY_ID = " + iPolicyid + " AND TRANSACTION_ID = " + sTransNum + " AND STATUS <> " + base.LocalCache.GetCodeId("DE", "COVEXP_STATUS") + " ORDER BY COVERAGE_NUMBER";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                //loop through the list of coverages and clone them for the new term
                objTempCvg = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);
                while (objReader.Read())
                {
                    objTempCvg.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    bCovRenewFlag = objTempCvg.CovRenewableFlag;
                    if (bCovRenewFlag == true)
                    {
                        //get the values from the read and fill into new term record using temp object

                        objNewCvg = PolicyEnh.PolicyXCvgEnhList.AddNew();
                        objNewCvg.BrokerName = objTempCvg.BrokerName;
                        objNewCvg.CancelNoticeDays = objTempCvg.CancelNoticeDays;
                        objNewCvg.ClaimLimit = objTempCvg.ClaimLimit;
                        objNewCvg.CoverageNumber = objTempCvg.CoverageNumber;
                        objNewCvg.CoverageTypeCode = objTempCvg.CoverageTypeCode;
                        objNewCvg.CovRenewableFlag = true;
                        objNewCvg.DataChanged = true;
                        //Anu Tennyson for MITS 18229 1/20/2010 STARTS
                        sLob = LocalCache.GetShortCode(PolicyEnh.PolicyType);

                        if (sLob != "WC")
                        {
                        //tmalhotra2 MITS 27948
                        sbSql = new StringBuilder();
                        //sbSql.Append("SELECT COUNT(*) FROM SYS_POL_EXP_RATES WHERE EXPOSURE_ID = ");
                        //sbSql.Append(objNewCvg.CoverageTypeCode);
                        //sbSql.Append(" AND LINE_OF_BUSINESS = ");
                        //sbSql.Append(PolicyEnh.PolicyType);
                        //sbSql.Append(" AND STATE = ");
                        //sbSql.Append(PolicyEnh.State);
                        //sbSql.Append(" AND EFFECTIVE_DATE <= '");
                        //sbSql.Append(objNewTerm.EffectiveDate);
                        //sbSql.Append("' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '");
                        //sbSql.Append(objNewTerm.EffectiveDate);
                        //sbSql.Append("'))");
                        //using (DbConnection objConn = DbFactory.GetDbConnection(base.ConnectionString))
                        //{
                        //    objConn.Open();
                        //    iCountDed = Conversion.CastToType<int>(Convert.ToString(objConn.ExecuteScalar(Convert.ToString(sbSql))), out bIsSuccess);
                        //}
                        //
                        //if (iCountDed == 0)
                        //{
                        //    objNewCvg.Deductible = objTempCvg.Deductible;
                        //    objNewCvg.FlatOrPercent = objTempCvg.FlatOrPercent;
                        //}
                        //else
                        //{
                        //    GetDeductibleData(ref deductible, ref flatorpercent,objNewCvg.CoverageTypeCode.ToString(),PolicyEnh.PolicyType.ToString(),PolicyEnh.State.ToString(),objNewTerm.EffectiveDate);
                        //    objNewCvg.Deductible = deductible;
                        //    objNewCvg.FlatOrPercent = flatorpercent;
                        //}
                        //tmalhotra2 end

                        sbSql.Append("SELECT SYS_POL_EXP_RATES.FLAT_OR_PERCENT, SYS_POL_RANGE.DEDUCTIBLE FROM SYS_POL_EXP_RATES INNER JOIN SYS_POL_RANGE ");
                        sbSql.Append("ON SYS_POL_EXP_RATES.EXP_RATE_ROWID = SYS_POL_RANGE.RATE_ROWID WHERE EXPOSURE_ID = ");
                        sbSql.Append(objNewCvg.CoverageTypeCode);
                        sbSql.Append(" AND LINE_OF_BUSINESS = ");
                        sbSql.Append(PolicyEnh.PolicyType);
                        sbSql.Append(" AND STATE = ");
                        sbSql.Append(PolicyEnh.State);
                        sbSql.Append(" AND EFFECTIVE_DATE <= '");
                        sbSql.Append(objNewTerm.EffectiveDate);
                        sbSql.Append("' AND ((EXPIRATION_DATE IS NULL) OR (EXPIRATION_DATE='') OR (EXPIRATION_DATE >= '");
                        sbSql.Append(objNewTerm.EffectiveDate);
                        sbSql.Append("'))");

                            using (DbReader objRdr = DbFactory.GetDbReader(base.ConnectionString, sbSql.ToString()))
                            {
                                if (objRdr.Read())
                                {
                                    double iDeductible = Conversion.ConvertObjToDouble(objRdr.GetValue(1), base.ClientId);
                                    int iFlatOrPercent = Conversion.ConvertObjToInt(objRdr.GetValue(0), base.ClientId);

                                    string sFlatOrPercent = base.LocalCache.GetShortCode(iFlatOrPercent);
                                    if (sFlatOrPercent == "P")
                                    {

                                        objNewCvg.Deductible = (iDeductible * objTempCvg.PolicyLimit) / 100;
                                            objNewCvg.FlatOrPercent = iFlatOrPercent;
                                       
                                    }
                                    else
                                    {
                                        objNewCvg.Deductible = iDeductible;
                                        objNewCvg.FlatOrPercent = iFlatOrPercent;
                                    }

                                }
                                else
                                {
                                    objNewCvg.Deductible = objTempCvg.Deductible;
                                    objNewCvg.FlatOrPercent = objTempCvg.FlatOrPercent;
                                }
                            }



                        }
                        else
                        {
                            objNewCvg.Deductible = objTempCvg.Deductible;
                        }
                        objNewCvg.Modifier = objTempCvg.Modifier;
                        objNewCvg.ExpRateRowId = objTempCvg.ExpRateRowId;
                        objNewCvg.Amount = objTempCvg.Amount;
                        objNewCvg.CoverageSeq = objTempCvg.CoverageSeq;
                        if (sLob == "AL")
                        {
                            if (objTempCvg.PolicyXCvgUarAddedAL != null)
                            {
                                foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objTempCvg.PolicyXCvgUarAddedAL)
                                {
                                    objNewCvg.PolicyXCvgUarAddedAL.Add(objUARDictionaryEntry.Value);
                                }
                            }
                        }
                        else if (sLob == "PC")
                        {
                            if (objTempCvg.PolicyXCvgUarAddedPC != null)
                            {
                                foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objTempCvg.PolicyXCvgUarAddedPC)
                                {
                                    objNewCvg.PolicyXCvgUarAddedPC.Add(objUARDictionaryEntry.Value);
                                }
                            }
                        }
                        //Anu Tennyson for MITS 18229 ENDS
                        objNewCvg.EffectiveDate = objNewTerm.EffectiveDate;
                        objNewCvg.Exceptions = objTempCvg.Exceptions;
                        objNewCvg.ExpirationDate = objNewTerm.ExpirationDate;
                        objNewCvg.NextPolicyId = objTempCvg.NextPolicyId;
                        objNewCvg.NotificationUid = objTempCvg.NotificationUid;
                        objNewCvg.TotalPayments = 0;
                        objNewCvg.PolicyId = iPolicyid;
                        objNewCvg.PolicyLimit = objTempCvg.PolicyLimit;
                        objNewCvg.Remarks = objTempCvg.Remarks;
                        objNewCvg.SelfInsRetention = objTempCvg.SelfInsRetention;
                        objNewCvg.SequenceAlpha = 1;
                        objNewCvg.Status = base.LocalCache.GetCodeId("PR", "COVEXP_STATUS");
                        objNewCvg.TransactionType = objRenewalTrans.TransactionType;
                        // Start Naresh MITS 9969 Term Number was not Getting Saved in Coverages while Renewing Policy
                        objNewCvg.TermNumber = objNewTerm.TermNumber;
                        //pmahli Supplementals

                        //Start:Sumit (09/13/2010)-MITS# 21871
                        objSerializationXMl.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
                        objCoverages.LoadXml(objTempCvg.SerializeObject(objSerializationXMl));
                        PopulateSupplementalsFromXML(objCoverages, objNewCvg);
                        //sPolcvgRowIdFilter = "Instance/PolicyEnh/PolicyXCvgEnhList/PolicyXCvgEnh[PolcvgRowId = " + objTempCvg.PolcvgRowId.ToString() + "]";
                        //PopulateSupplementals(sPolcvgRowIdFilter, objNewCvg);
                        //End:Sumit
                    }
                }
                objReader.Close();
                objTempExp = null;

                //read to find all the exp for the term
                sSQL = "SELECT EXPOSURE_ID FROM POLICY_X_EXP_ENH WHERE POLICY_ID = " + iPolicyid + " AND TRANSACTION_ID = " + sTransNum + " AND STATUS <> " + base.LocalCache.GetCodeId("DE", "COVEXP_STATUS") + " ORDER BY EXPOSURE_COUNT";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objTempExp = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                //Anu Tennyson for MITS 18229 : Prem Calc Starts
                sbOrgH = new StringBuilder();
                foreach (PolicyXInsuredEnh objPolicyXInsuredEnh in PolicyEnh.PolicyXInsuredEnhList)
                {
                    sbOrgH.Append(objPolicyXInsuredEnh.InsuredEid);
                    sbOrgH.Append(",");
                }
                //Anu Tennyson for MITS 18229 : Prem Calc Ends
                double dblManualPremium = 0;
                while (objReader.Read())
                {
                    objTempExp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    //get the values from the read and fill into new term record
                    objNewExp = PolicyEnh.PolicyXExposureEnhList.AddNew();

                    objNewExp.CoverageCode = objTempExp.CoverageCode;
                    objNewExp.EffectiveDate = objNewTerm.EffectiveDate;
                    objNewExp.Exceptions = objTempExp.Exceptions;
                    objNewExp.ExpirationDate = objNewTerm.ExpirationDate;
                    objNewExp.ExposureAmount = objTempExp.ExposureAmount;
                    objNewExp.ExposureCode = objTempExp.ExposureCode;
                    objNewExp.ExposureCount = objTempExp.ExposureCount;
                    objNewExp.HeirarchyLevel = objTempExp.HeirarchyLevel;
                    objNewExp.PolicyId = iPolicyid;
                    objNewExp.Remarks = objTempExp.Remarks;
                    objNewExp.RenewableFlag = true;
                    objNewExp.SequenceAlpha = 1;
                    objNewExp.Status = base.LocalCache.GetCodeId("PR", "COVEXP_STATUS");
                    objNewExp.TransactionType = objRenewalTrans.TransactionType;
                    objNewExp.TermNumber = objNewTerm.TermNumber;
                    //pmahli Supplementals


                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXExpEnh><Supplementals/></PolicyXExpEnh>");
                    objExposure.LoadXml(objTempExp.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objExposure, objNewExp);
                    //sPolicyXExpEnhIdFilter = "Instance/PolicyEnh/PolicyXExposureEnhList/PolicyXExpEnh[ExposureId = " + objTempExp.ExposureId.ToString() + "]";
                    //PopulateSupplementals(sPolicyXExpEnhIdFilter, objNewExp);
                    //End:Sumit

                    //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
                    //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                    //base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, 0, 0, "RN",);
                    //base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, 0, 0, "RN", PolicyEnh.PolicyType);
                    //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                    //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation ENDS
                    //Anu Tennyson for MITS 18229 : Prem Calc STARTS
                    base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, 0, 0, "RN", PolicyEnh.PolicyType, sbOrgH.ToString());
                    //Anu Tennyson for MITS 18229 : Prem Calc ENDS
                    dblManualPremium += objNewExp.PrAnnPremAmt;
                    //Sumit - Start(05/20/2010) - MITS# 20484 - Update Exposure Children
                    if (objTempExp.PolicyXUarList != null)
                    {
                        foreach (PolicyXUar objPolicyXUar in objTempExp.PolicyXUarList)
                        {
                            objPolicyXUarNew = objNewExp.PolicyXUarList.AddNew();
                            objPolicyXUarNew.UarRowId = objPolicyXUar.UarRowId;
                            objPolicyXUarNew.PolicyId = objPolicyXUar.PolicyId;
                            objPolicyXUarNew.ExposureId = objPolicyXUar.ExposureId;
                            objPolicyXUarNew.UarId = objPolicyXUar.UarId;
                            objPolicyXUarNew.PolicyStatusCode = base.CCacheFunctions.GetCodeIDWithShort("PR", "POLICY_STATUS");

                            //Sumit - Start(05/17/2010) - MITS# 20483 - Update Uar Children
                            if (objPolicyXUar.PolicyXPschedEnhList != null)
                            {
                                foreach (PolicyXPschedEnh objPolicyXPsched in objPolicyXUar.PolicyXPschedEnhList)
                                {
                                    objPolicyXScheduleNew = objPolicyXUarNew.PolicyXPschedEnhList.AddNew();
                                    objPolicyXScheduleNew.PschedRowID = objPolicyXPsched.PschedRowID;
                                    objPolicyXScheduleNew.PolicyId = objPolicyXPsched.PolicyId;
                                    objPolicyXScheduleNew.UarId = objPolicyXPsched.UarId;
                                    objPolicyXScheduleNew.Name = objPolicyXPsched.Name;
                                    objPolicyXScheduleNew.Amount = objPolicyXPsched.Amount;
                                }
                            }
                            //Sumit - End
                        }
                    }
                    //Sumit - End
                }

                objReader.Close();

                //SET VALUES IN RATING REC
                objNewRating = PolicyEnh.PolicyXRatingEnhList.AddNew();
                objNewRating.PolicyId = iPolicyid;
                objNewRating.SequenceAlpha = 1;
                objNewRating.TermNumber = objNewTerm.TermNumber;
                objNewRating.TransactionType = objRenewalTrans.TransactionType;
                objNewRating.ManualPremium = dblManualPremium;
                objNewRating.ModifiedPremium = dblManualPremium;
                objNewRating.TransactionId = -1;

                //CLONE POLICY BILLING REC
                if (base.UseBillingSystem == true)
                {
                    sSQL = "SELECT POLICY_BILLING_ID FROM POLICY_X_BILL_ENH WHERE TRANSACTION_ID = " + sTransNum;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReader.Read())
                    {
                        objNewBill = PolicyEnh.PolicyXBillEnhList.AddNew();
                        objTempBill = (PolicyXBillEnh)base.DataModelFactory.GetDataModelObject("PolicyXBillEnh", false);
                        objTempBill.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                        // Start Naresh MITS 9806 Checkbox was getting Unselected after Postback    
                        objNewBill.DoNotBill = objTempBill.DoNotBill;
                        // End Naresh MITS 9806 Checkbox was getting Unselected after Postback   

                        objNewBill.BillToEid = objTempBill.BillToEid;
                        objNewBill.BillToOverride = objTempBill.BillToOverride;
                        objNewBill.BillToType = objTempBill.BillToType;
                        objNewBill.NewBillToEid = objTempBill.NewBillToEid;
                        objNewBill.PayPlanRowId = objTempBill.PayPlanRowId;
                        objNewBill.BillingRuleRowId = objTempBill.BillingRuleRowId;
                        objNewBill.TermNumber = objNewTerm.TermNumber;
                        objNewBill.TransactionId = -1;
                    }
                    objReader.Close();

                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
        }

        public void ReinstatePolicy(bool p_bWithLapse, string p_ReinsDate)
        {

            string sSQL = "", sDt = "", sReinstateType = "", sCancelDate = "", sCancelType = "";
            int iPolicyId = 0, iSeq = 0, iTermNum = 0, iNewTermNum = 0, iReinstateType = 0, iTermCancelled = 0, iTransType = 0, iMaxTran = 0, iTransNum = 0;
            double dPrevPremAmt = 0, dManPrem=0;

            //Sumit - Start(05/20/2010) - MITS# 20484 - Added for Uar details
            PolicyXUar objPolicyXUarNew = null;
            //Sumit - End

            //Sumit - Start(05/17/2010) - MITS# 20483 - Added for Schedule details
            PolicyXPschedEnh objPolicyXScheduleNew = null;
            //Sumit - End

            //Start:Sumit (09/13/2010)-MITS# 21871
            XmlDocument objSerializationXMl = new XmlDocument();
            XmlDocument objCoverages = new XmlDocument();
            XmlDocument objExposure = new XmlDocument();
            //End:Sumit

            //New objects for reinstatement
            PolicyXTransEnh objReinstatementTrans = null;
            PolicyXTermEnh objNewTerm = null;
            PolicyXCvgEnh objNewCvg = null;
            PolicyXExpEnh objNewExp = null;
            PolicyXRtngEnh objNewRating = null;
            PolicyXDcntEnh objNewDisc = null;
            PolicyXDtierEnh objNewTier = null;
            PolicyXBillEnh objNewBill = null;
            PolicyXTermEnh objTempTerm = null;
            DbReader objReader = null;
            string sPolcvgRowIdFilter = string.Empty;
            string sPolicyXExpEnhIdFilter = string.Empty;
            //Anu Tennyson for MITS 18229 A new Field UAR List added on Coverages Screen
            string sLob = string.Empty;
            StringBuilder sbOrgH = null;
            //Anu Tennyson Ends

            //Temp objects
            PolicyXCvgEnh objTempcvg = null;
            DbReader objReaderTemp = null;
            PolicyXExpEnh objTempExp = null;
            PolicyXDcntEnh objTempDisc = null;
            PolicyXBillEnh objTempBill = null;
            try
            {

                sDt = Conversion.ToDbDate(DateTime.Now);
                sbOrgH = new StringBuilder();

                iPolicyId = PolicyEnh.PolicyId;

                //Anu Tenyson for MITS 18229 UARList field Added on Coverages Screen
                sLob = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
                //End

                if (p_bWithLapse == true)
                {
                    iReinstateType = base.CCacheFunctions.GetCodeIDWithShort("RL", "REINSTATEMENT_TYPE");
                    sReinstateType = base.LocalCache.GetShortCode(iReinstateType);
                }
                else
                {
                    iReinstateType = base.CCacheFunctions.GetCodeIDWithShort("RNL", "REINSTATEMENT_TYPE");
                    sReinstateType = base.LocalCache.GetShortCode(iReinstateType);
                }

                //have to have all the cancel statuses that we use on the term rec
                iTermCancelled = base.CCacheFunctions.GetCodeIDWithShort("C", "TERM_STATUS");

                //read to find the most recent(greatest) term number
                sSQL = "SELECT MAX(TERM_NUMBER) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyId;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iTermNum = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                //read to find the greatest term sequence alpha

                sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + iTermNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iSeq = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();

                //read to find the correct term record

                sSQL = "SELECT TERM_ID FROM POLICY_X_TERM_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + iTermNum + " and SEQUENCE_ALPHA = " + iSeq;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    objTempTerm = (PolicyXTermEnh)base.DataModelFactory.GetDataModelObject("PolicyXTermEnh", false);

                    //Fetch the record corresponding to term Id in temp object                   

                    objTempTerm.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    //get the values from the read and fill into new term record

                    objNewTerm = PolicyEnh.PolicyXTermEnhList.AddNew();
                    objNewTerm.AuditedFlag = objTempTerm.AuditedFlag;
                    sCancelDate = objTempTerm.CancelDate;
                    sCancelType = base.LocalCache.GetShortCode(objTempTerm.CancelType);

                    if (sReinstateType == "RL")
                        objNewTerm.ReinstatementDate = Conversion.ToDbDate(Convert.ToDateTime(p_ReinsDate));
                    else
                        if (sReinstateType == "RNL")
                            objNewTerm.ReinstatementDate = objTempTerm.CancelDate;

                    objNewTerm.EffectiveDate = objTempTerm.EffectiveDate;
                    objNewTerm.ExpirationDate = objTempTerm.ExpirationDate;
                    objNewTerm.PolicyId = objTempTerm.PolicyId;
                    objNewTerm.PolicyNumber = objTempTerm.PolicyNumber;
                    objNewTerm.RenewalDate = objTempTerm.RenewalDate;
                    objNewTerm.RenewalInd = objTempTerm.RenewalInd;
                    objNewTerm.RenewalTermAmt = objTempTerm.RenewalTermAmt;
                    objNewTerm.RenewalTermCode = objTempTerm.RenewalTermCode;
                    objNewTerm.SequenceAlpha = objTempTerm.SequenceAlpha + 1;
                    objNewTerm.TermNumber = objTempTerm.TermNumber;
                    //npadhy RMSC retrofit Starts
                    objNewTerm.BrokerCommission = objTempTerm.BrokerCommission;
                    //npadhy RMSC retrofit Ends
                    iNewTermNum = objNewTerm.TermNumber;
                    //  objNewTerm.Termstatus =objTempTerm.TermStatus; TODO
                }

                objReader.Close();


                //read to find the most recent transaction record

                if (sCancelType == "FL")
                    iTransType = base.CCacheFunctions.GetCodeIDWithShort("CF", "TRANSACTION_TYPE");
                else
                    if (sCancelType == "PR")
                        iTransType = base.CCacheFunctions.GetCodeIDWithShort("PCP", "TRANSACTION_TYPE");


                //find the correct transaction....

                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + iTermNum + " AND TRANSACTION_TYPE <> " + base.CCacheFunctions.GetCodeIDWithShort("AU", "TRANSACTION_TYPE");
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())

                    iTransNum = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();


                sSQL = "SELECT MAX(TRANSACTION_ID) FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + iTermNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                    iMaxTran = Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId);
                objReader.Close();


                sSQL = "SELECT * FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID = " + iTransNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);


                if (objReader.Read())
                {
                    //TRANSACTION - create the  transaction
                    //get the coverage count
                    sSQL = "SELECT COUNT(*) FROM POLICY_X_CVG_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + iNewTermNum + " AND TRANSACTION_ID = " + iTransNum;
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                    objReinstatementTrans = PolicyEnh.PolicyXTransEnhList.AddNew();
                    if (objReaderTemp.Read())
                        objReinstatementTrans.CoverageRcdCount = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);

                    objReaderTemp.Close();
                    if (objNewTerm != null)
                    {
                        objReinstatementTrans.EffectiveDate = objNewTerm.ReinstatementDate;
                        objReinstatementTrans.ExpirationDate = objNewTerm.ExpirationDate;
                    }

                    //get the exposure count

                    sSQL = "SELECT COUNT(*) FROM POLICY_X_EXP_ENH WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + objNewTerm.TermNumber + " AND TRANSACTION_ID = " + iTransNum;
                    objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReaderTemp.Read())
                        objReinstatementTrans.ExposureRcdCount = Conversion.ConvertObjToInt(objReaderTemp.GetValue(0), base.ClientId);

                    objReaderTemp.Close();

                    objReinstatementTrans.PolicyId = iPolicyId;

                    if (sReinstateType == "RL")
                        objReinstatementTrans.PolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("PRL", "POLICY_STATUS");
                    else
                        if (sReinstateType == "RNL")
                            objReinstatementTrans.PolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("PRN", "POLICY_STATUS");

                    if (iTransNum == iMaxTran)

                        objReinstatementTrans.SequenceAlpha = Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                    else
                    {
                        sSQL = "SELECT * FROM POLICY_X_TRANS_ENH WHERE TRANSACTION_ID=" + iMaxTran;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                        if (objReaderTemp.Read())
                            objReinstatementTrans.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                        objReaderTemp.Close();
                    }

                    objReinstatementTrans.TransactionDate = sDt;
                    objReinstatementTrans.TransactionType = iReinstateType;
                    if (objNewTerm != null)
                    {
                        objReinstatementTrans.TermNumber = objNewTerm.TermNumber;
                        objReinstatementTrans.TermSeqAlpha = objNewTerm.SequenceAlpha;
                    }
                    objReinstatementTrans.TransactionStatus = base.CCacheFunctions.GetCodeIDWithShort("PR", "TRANSACTION_STATUS");
     
                }

                objReader.Close();

                //read to find all the coverages for the term
                sSQL = "SELECT POLCVG_ROW_ID FROM POLICY_X_CVG_ENH WHERE POLICY_ID = " + iPolicyId + " AND TRANSACTION_ID = " + iTransNum + " ORDER BY COVERAGE_NUMBER";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objTempcvg = (PolicyXCvgEnh)base.DataModelFactory.GetDataModelObject("PolicyXCvgEnh", false);

                while (objReader.Read())
                {
                    objTempcvg.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));

                    //loop through the list of coverages and clone them for the new term
                    //get the values from the read and fill into new term record
                    objNewCvg = PolicyEnh.PolicyXCvgEnhList.AddNew();
                    objNewCvg.BrokerName = objTempcvg.BrokerName;
                    objNewCvg.CancelNoticeDays = objTempcvg.CancelNoticeDays;
                    objNewCvg.ClaimLimit = objTempcvg.ClaimLimit;
                    objNewCvg.CoverageTypeCode = objTempcvg.CoverageTypeCode;
                    objNewCvg.CoverageNumber = objTempcvg.CoverageNumber;
                    objNewCvg.CovRenewableFlag = objTempcvg.CovRenewableFlag;
                    objNewCvg.Deductible = objTempcvg.Deductible;
                    objNewCvg.EffectiveDate = objNewTerm.ReinstatementDate;
                    objNewCvg.Exceptions = objTempcvg.Exceptions;
                    objNewCvg.ExpirationDate = objNewTerm.ExpirationDate;
                    objNewCvg.NextPolicyId = objTempcvg.NextPolicyId;
                    objNewCvg.NotificationUid = objTempcvg.NotificationUid;
                    objNewCvg.PolicyId = objTempcvg.PolicyId;
                    objNewCvg.PolicyLimit = objTempcvg.PolicyLimit;
                    objNewCvg.OccurrenceLimit = objTempcvg.OccurrenceLimit;
                    objNewCvg.Remarks = objTempcvg.Remarks;
                    objNewCvg.SelfInsRetention = objTempcvg.SelfInsRetention;
                    //Anu Tennyson for MITS 18229 1/15/2010 STARTS
                    objNewCvg.FlatOrPercent = objTempcvg.FlatOrPercent;
                    objNewCvg.Modifier = objTempcvg.Modifier;
                    objNewCvg.ExpRateRowId = objTempcvg.ExpRateRowId;
                    objNewCvg.Amount = objTempcvg.Amount;
                    objNewCvg.CoverageSeq = objTempcvg.CoverageSeq;
                    if (sLob == "AL")
                    {
                        if (objTempcvg.PolicyXCvgUarAddedAL != null)
                        {
                            foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objTempcvg.PolicyXCvgUarAddedAL)
                            {
                                objNewCvg.PolicyXCvgUarAddedAL.Add(objUARDictionaryEntry.Value);
                            }
                        }
                        else if (sLob == "PC")
                        {
                            if (objTempcvg.PolicyXCvgUarAddedPC != null)
                            {
                                foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objTempcvg.PolicyXCvgUarAddedPC)
                                {
                                    objNewCvg.PolicyXCvgUarAddedPC.Add(objUARDictionaryEntry.Value);
                                }
                            }
                        }
                    }
                    //Anu Tennyson for MITS 18229 ENDS
                    if (iTransNum == iMaxTran)
                        objNewCvg.SequenceAlpha = objTempcvg.SequenceAlpha + 1;
                    else
                    {
                        sSQL = "SELECT * FROM POLICY_X_CVG_ENH WHERE TRANSACTION_ID =" + iMaxTran + " AND POLICY_ID=" + iPolicyId + " AND COVERAGE_NUMBER =" + Conversion.ConvertObjToInt((objReader.GetValue(", base.ClientIdCOVERAGE_NUMBER")), base.ClientId);
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                            objNewCvg.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                        objReaderTemp.Close();
                    }

                    if (base.LocalCache.GetShortCode(objTempcvg.Status)== "CN")
                        objNewCvg.Status = base.CCacheFunctions.GetCodeIDWithShort("PR", "COVEXP_STATUS");
                    else
                        objNewCvg.Status = objTempcvg.Status;

                    objNewCvg.TotalPayments = objTempcvg.TotalPayments;
                    if (objReinstatementTrans != null)
                        objNewCvg.TransactionType = objReinstatementTrans.TransactionType;
                    //pmahli Supplementals
                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
                    objCoverages.LoadXml(objTempcvg.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objCoverages, objNewCvg);
                    //sPolcvgRowIdFilter = "Instance/PolicyEnh/PolicyXCvgEnhList/PolicyXCvgEnh[PolcvgRowId = " + objTempcvg.PolcvgRowId.ToString() + "]";
                    //PopulateSupplementals(sPolcvgRowIdFilter, objNewCvg);
                    //End:Sumit
                }

                objReader.Close();


                //read to find all the exp for the term
                sSQL = "SELECT EXPOSURE_ID FROM POLICY_X_EXP_ENH WHERE POLICY_ID = " + iPolicyId + " AND TRANSACTION_ID = " + iTransNum + " ORDER BY EXPOSURE_COUNT";
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objTempExp = (PolicyXExpEnh)base.DataModelFactory.GetDataModelObject("PolicyXExpEnh", false);
                dManPrem = 0;
                //Anu Tennyson for MITS 18229 : STARTS Prem Calc
                foreach (PolicyXInsuredEnh objPolicyXInsuredEnh in PolicyEnh.PolicyXInsuredEnhList)
                {
                    sbOrgH.Append(objPolicyXInsuredEnh.InsuredEid);
                    sbOrgH.Append(",");
                }
                //Anu Tennyson for MITS 18229 : ENDS Prem Calc
                while (objReader.Read())
                {
                    objTempExp.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    objNewExp = PolicyEnh.PolicyXExposureEnhList.AddNew();

                    objNewExp.CoverageCode = objTempExp.CoverageCode;
                    objNewExp.EffectiveDate = objNewTerm.ReinstatementDate;
                    objNewExp.Exceptions = objTempExp.Exceptions;
                    objNewExp.ExpirationDate = objNewTerm.ExpirationDate;
                    objNewExp.ExposureAmount = objTempExp.ExposureAmount;
                    objNewExp.ExposureCode = objTempExp.ExposureCode;
                    objNewExp.ExposureCount = objTempExp.ExposureCount;
                    objNewExp.ExposureRate = objTempExp.ExposureRate;
                    objNewExp.PolicyId = iPolicyId;
                    objNewExp.ExposureBaseRate = objTempExp.ExposureBaseRate;
                    objNewExp.FullAnnPremAmt = objTempExp.FullAnnPremAmt;
                    objNewExp.HeirarchyLevel = objTempExp.HeirarchyLevel;
                    objNewExp.PrAnnPremAmt = objTempExp.PrAnnPremAmt;
                    dPrevPremAmt =objNewExp.PrAnnPremAmt;
                    objNewExp.RenewableFlag = objTempExp.RenewableFlag;
                    objNewExp.Remarks = objTempExp.Remarks;
                    if (iTransNum == iMaxTran)
                        objNewExp.SequenceAlpha = objTempExp.SequenceAlpha + 1;
                    else
                    {
                        sSQL = "SELECT * FROM POLICY_X_EXP_ENH WHERE TRANSACTION_ID =" + iMaxTran + " AND POLICY_ID=" + iPolicyId + " AND EXPOSURE_COUNT=" + Conversion.ConvertObjToInt(objReader.GetValue("EXPOSURE_COUNT"), base.ClientId);
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                            objNewExp.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                        objReaderTemp.Close();
                    }
                    if (base.LocalCache.GetShortCode(objTempExp.Status) == "CN")
                        objNewExp.Status = base.CCacheFunctions.GetCodeIDWithShort("PR", "COVEXP_STATUS");
                    else
                        objNewExp.Status = objTempExp.Status;
                    if (objNewTerm != null)
                        objNewExp.TermNumber = objNewTerm.TermNumber;
                    if (objReinstatementTrans != null)
                        objNewExp.TransactionType = objReinstatementTrans.TransactionType;
                    //pmahli Supplemetals

                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXExpEnh><Supplementals/></PolicyXExpEnh>");
                    objExposure.LoadXml(objTempExp.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objExposure, objNewExp);
                    //sPolicyXExpEnhIdFilter = "Instance/PolicyEnh/PolicyXExposureEnhList/PolicyXExpEnh[ExposureId = " + objTempExp.ExposureId.ToString() + "]";
                    //PopulateSupplementals(sPolicyXExpEnhIdFilter, objNewExp);
                    //End:Sumit

                    if (base.LocalCache.GetShortCode(objNewExp.Status) == "PR")
                        //Start:Added by Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
                        //base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, dPrevPremAmt, 0, base.LocalCache.GetShortCode(objReinstatementTrans.TransactionType), Conversion.ToDate(objNewTerm.ReinstatementDate));
                        //base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, dPrevPremAmt, 0, base.LocalCache.GetShortCode(objReinstatementTrans.TransactionType), Conversion.ToDate(objNewTerm.ReinstatementDate),PolicyEnh.PolicyType);
                        //End:Nitin Goel:Correcting Premium Calculation functionality for Enhance Policy,05/19/2010,MITS#20742
                        //Commented by Anu Tennyson for adding org hierachy filteration in rate calculation STARTS
                        //Anu Tennyson for MITS 18229 : STARTS Prem Calc
                        base.Rate(ref objNewExp, PolicyEnh.State, base.RoundFlag, dPrevPremAmt, 0, base.LocalCache.GetShortCode(objReinstatementTrans.TransactionType), Conversion.ToDate(objNewTerm.ReinstatementDate), PolicyEnh.PolicyType, sbOrgH.ToString());
                        //Anu Tennyson for MITS 18229 : ENDS Prem Calc
                    dManPrem += objNewExp.PrAnnPremAmt;
                    //Sumit - Start(05/20/2010) - MITS# 20484 - Update Exposure Children
                    if (objTempExp.PolicyXUarList != null)
                    {
                        foreach (PolicyXUar objPolicyXUar in objTempExp.PolicyXUarList)
                        {
                            objPolicyXUarNew = objNewExp.PolicyXUarList.AddNew();
                            objPolicyXUarNew.UarRowId = objPolicyXUar.UarRowId;
                            objPolicyXUarNew.PolicyId = objPolicyXUar.PolicyId;
                            objPolicyXUarNew.ExposureId = objPolicyXUar.ExposureId;
                            objPolicyXUarNew.UarId = objPolicyXUar.UarId;
                            objPolicyXUarNew.PolicyStatusCode = objReinstatementTrans.PolicyStatus;

                            //Sumit - Start(05/17/2010) - MITS# 20483 - Update Uar Children
                            if (objPolicyXUar.PolicyXPschedEnhList != null)
                            {
                                foreach (PolicyXPschedEnh objPolicyXPsched in objPolicyXUar.PolicyXPschedEnhList)
                                {
                                    objPolicyXScheduleNew = objPolicyXUarNew.PolicyXPschedEnhList.AddNew();
                                    objPolicyXScheduleNew.PschedRowID = objPolicyXPsched.PschedRowID;
                                    objPolicyXScheduleNew.PolicyId = objPolicyXPsched.PolicyId;
                                    objPolicyXScheduleNew.UarId = objPolicyXPsched.UarId;
                                    objPolicyXScheduleNew.Name = objPolicyXPsched.Name;
                                    objPolicyXScheduleNew.Amount = objPolicyXPsched.Amount;
                                }
                            }
                            //Sumit - End
                        }
                    }
                }
                
                objReader.Close();

                //read to find the most recent rating record
                sSQL = "SELECT * FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID = " + iTransNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {

                    //get the values from the read and fill into new term record
                    //call premium calculation
                    objNewRating = PolicyEnh.PolicyXRatingEnhList.AddNew();
                    objNewRating.ExpModFactor = Conversion.ConvertObjToDouble(objReader.GetValue("EXP_MOD_FACTOR"), base.ClientId);
                    objNewRating.PolicyId = iPolicyId;
                   
                    if (iTransNum == iMaxTran)
                        objNewRating.SequenceAlpha = Conversion.ConvertObjToInt(objReader.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                    else
                    {
                        sSQL = "SELECT * FROM POLICY_X_RTNG_ENH WHERE TRANSACTION_ID=" + iMaxTran;
                        objReaderTemp = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                        if (objReaderTemp.Read())
                            objNewRating.SequenceAlpha = Conversion.ConvertObjToInt(objReaderTemp.GetValue("SEQUENCE_ALPHA"), base.ClientId) + 1;
                        objReaderTemp.Close();

                    }


                    if (objNewTerm != null)
                    {
                        objNewRating.TermNumber = objNewTerm.TermNumber;
                        objNewRating.TransactionType = objReinstatementTrans.TransactionType;
                    }
                    objNewRating.ManualPremium = dManPrem;
                    objNewRating.TransactionId = -1;
                    PremiumManager.CalculateModifiedPremium(objNewRating);
                   
                }

                objReader.Close();


                sSQL = "SELECT DISCOUNT_ID FROM POLICY_X_DCNT_ENH WHERE TRANSACTION_ID=" + iTransNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objTempDisc = (PolicyXDcntEnh)base.DataModelFactory.GetDataModelObject("PolicyXDcntEnh", false);

                while (objReader.Read())
                {
                    objTempDisc.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                    //get the values from the read and fill into new term record
                    objNewDisc = PolicyEnh.PolicyXDiscountEnhList.AddNew();
                    objNewDisc.DiscountFactor = objTempDisc.DiscountFactor;
                    objNewDisc.DiscountLevelId = objTempDisc.DiscountLevelId;
                    objNewDisc.DiscountNameId = objTempDisc.DiscountNameId;
                    objNewDisc.PolicyId = iPolicyId;
                    objNewDisc.TransactionId = -1;                                        
                }
                objReader.Close();
                objTempDisc = null;

                sSQL = "SELECT * FROM POLICY_X_DTIER_ENH WHERE TRANSACTION_ID=" + iTransNum;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                while (objReader.Read())
                {

                    objNewTier = PolicyEnh.PolicyXDiscountTierEnhList.AddNew();
                    objNewTier.DiscTierLevelId = Conversion.ConvertObjToInt(objReader.GetValue("DISC_TIER_LEVEL_ID"), base.ClientId);
                    objNewTier.DiscTierNameId = Conversion.ConvertObjToInt(objReader.GetValue("DISC_TIER_NAME_ID"), base.ClientId);
                    objNewTier.PolicyId = iPolicyId;
                    objNewTier.TransactionId = -1;

                }

                objReader.Close();

                if (base.UseBillingSystem)
                {
                    sSQL = "SELECT POLICY_BILLING_ID FROM POLICY_X_BILL_ENH WHERE TRANSACTION_ID = " + iTransNum;
                    objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);

                    if (objReader.Read())
                    {
                        objNewBill = PolicyEnh.PolicyXBillEnhList.AddNew();
                        objTempBill = (PolicyXBillEnh)base.DataModelFactory.GetDataModelObject("PolicyXBillEnh", false);
                        objTempBill.MoveTo(Conversion.ConvertObjToInt(objReader.GetValue(0), base.ClientId));
                        // Start Naresh MITS 9806 Checkbox was getting Unselected after Postback    
                        objNewBill.DoNotBill = objTempBill.DoNotBill;
                        // End Naresh MITS 9806 Checkbox was getting Unselected after Postback   
                        objNewBill.BillToEid = objTempBill.BillToEid;
                        objNewBill.BillToOverride = objTempBill.BillToOverride;
                        objNewBill.BillToType = objTempBill.BillToType;
                        objNewBill.NewBillToEid = objTempBill.NewBillToEid;
                        objNewBill.PayPlanRowId = objTempBill.PayPlanRowId;
                        objNewBill.BillingRuleRowId = objTempBill.BillingRuleRowId;
                        if (objNewTerm != null)
                            objNewBill.TermNumber = objNewTerm.TermNumber;
                        objNewBill.TransactionId = -1;
                    }
                    objReader.Close();
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", base.ClientId), p_objEx);
            }
            finally
            {
                objReinstatementTrans = null;
                objNewTerm = null;
                objNewCvg = null;
                objNewExp = null;
                objNewRating = null;
                objNewDisc = null;
                objNewTier = null;
                objNewBill = null;
                objTempTerm = null;
                objReader = null;
                //Temp objects
                objTempcvg = null;
                objReaderTemp = null;
                objTempExp = null;
                objTempDisc = null;
                objTempBill = null;
                //Start:Sumit (09/13/2010)-MITS# 21871
                objSerializationXMl = null;
                objExposure = null;
                objCoverages = null;
                //End:Sumit
            }
        }

        public PolicyEnh ConvertToPolicy()
        {
            PolicyEnh objPolicyEnhNew = null;
            PolicyXTransEnh objPolicyXTransEnhNew = null;
            PolicyXTermEnh objPolicyXTermEnhNew = null;
            PolicyXRtngEnh objPolicyXRtngEnhNew = null ;
            PolicyXMcoEnh objPolicyXMcoEnhNew = null;
            PolicyXCvgEnh objPolicyXCvgEnhNew = null;
            PolicyXExpEnh objPolicyXExpEnhNew = null;
            PolicyXDcntEnh objPolicyXDcntEnhNew = null;
            PolicyXDtierEnh objPolicyXDtierEnhNew = null;
            PolicyXRtngEnh objPolicyXRtngEnhTemp = null;
            //Add by kuladeep for Audit Issue MITS:24736 Start
            PolicyXInsuredEnh objPolicyXInsrdEnhNew = null;//skhare7 RMSC merge
            //Geeta Added for Billing
            PolicyXBillEnh objNewBill = null;
            //Sumit - Start(04/06/2010) - MITS# 18229 - Added for Uar details
            PolicyXUar objPolicyXUarNew = null;
            //Sumit - End

            //Sumit - Start(05/17/2010) - MITS# 20483 - Added for Schedule details
            PolicyXPschedEnh objPolicyXScheduleNew = null;
            //Sumit - End
            Billing.BillingMaster objBillingMaster = null;
            string sPolcvgRowIdFilter = string.Empty;
            string sPolicyXExpEnhIdFilter = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            string sTransNum = string.Empty;
            //Anu Tenyson for MITS 18229 UARList field Added on Coverages Screen
            string sLOB = string.Empty;
            //Anu Tennyson END
            int iTermAmount = 0;
            int iTermCode = 0;
            int iPolicyid = 0;

            //Start:Sumit (09/13/2010)-MITS# 21871
            XmlDocument objSerializationXMl = new XmlDocument();
            XmlDocument objCoverages = new XmlDocument();
            XmlDocument objExposure = new XmlDocument();
            //End:Sumit

            try
            {
                //Anu Tenyson for MITS 18229 UARList field Added on Coverages Screen
                sLOB = base.LocalCache.GetShortCode(PolicyEnh.PolicyType);
                //End
                /*
                 * In case of Billingsystem in use, Policy Would be saved using BMO 
                 * and before returning the function .
                 */

                // Find the rating record. 
                foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                {
                    objPolicyXRtngEnhTemp = objPolicyXRtngEnh;
                    break;
                }

                // Find the Renewal Info 
                sSQL = " SELECT * FROM SYS_POL_RNWL_INFO WHERE LINE_OF_BUSINESS="
                    + PolicyEnh.PolicyType + " AND STATE=" + PolicyEnh.State;
                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                if (objReader.Read())
                {
                    iTermAmount = Conversion.ConvertObjToInt(objReader.GetValue("AMOUNT"), base.ClientId);
                    iTermCode = Conversion.ConvertObjToInt(objReader.GetValue("TERM_CODE"), base.ClientId);
                }
                objReader.Close();

                // Create the Policy Record 
                objPolicyEnhNew = (PolicyEnh)base.DataModelFactory.GetDataModelObject("PolicyEnh", false);
                objPolicyEnhNew.PolicyIndicator = base.CCacheFunctions.GetCodeIDWithShort("P", "POLICY_INDICATOR");
                objPolicyEnhNew.PolicyStatusCode = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                objPolicyEnhNew.Comments = PolicyEnh.Comments;
                objPolicyEnhNew.Country = PolicyEnh.Country;
                objPolicyEnhNew.InsurerEid = PolicyEnh.InsurerEid;
                objPolicyEnhNew.IssueDate = PolicyEnh.IssueDate;
                objPolicyEnhNew.RetroDate = PolicyEnh.RetroDate;
                objPolicyEnhNew.BankAccId = PolicyEnh.BankAccId;
                objPolicyEnhNew.PolicyName = PolicyEnh.PolicyName;
                objPolicyEnhNew.PolicyType = PolicyEnh.PolicyType;
                objPolicyEnhNew.PrimaryPolicyFlg = PolicyEnh.PrimaryPolicyFlg;
                objPolicyEnhNew.ReviewDate = PolicyEnh.ReviewDate;
                objPolicyEnhNew.State = PolicyEnh.State;
                objPolicyEnhNew.SubAccRowId = PolicyEnh.SubAccRowId;
                objPolicyEnhNew.TriggerClaimFlag = PolicyEnh.TriggerClaimFlag;
                objPolicyEnhNew.TriggerEventFlag = PolicyEnh.TriggerEventFlag;
                if (PolicyEnh.BrokerEid != 0)
                    objPolicyEnhNew.BrokerEid = PolicyEnh.BrokerEid;
                //pmahli MITS 9841 Supplementals
                PopulateSupplementals("Instance/PolicyEnh", objPolicyEnhNew);

                // Add NEW TRANSACTION               
                objPolicyXTransEnhNew = objPolicyEnhNew.PolicyXTransEnhList.AddNew();
                objPolicyXTransEnhNew.PolicyId = 0;
                objPolicyXTransEnhNew.TransactionType = base.CCacheFunctions.GetCodeIDWithShort("NB", "POLICY_TXN_TYPE");
                objPolicyXTransEnhNew.TermNumber = 1;
                objPolicyXTransEnhNew.TermSeqAlpha = 1;
                objPolicyXTransEnhNew.SequenceAlpha = 1;
                objPolicyXTransEnhNew.EffectiveDate = base.GetLatestTerm(PolicyEnh).EffectiveDate;
                objPolicyXTransEnhNew.ExpirationDate = base.GetLatestTerm(PolicyEnh).ExpirationDate;
                objPolicyXTransEnhNew.TransactionStatus = base.CCacheFunctions.GetCodeIDWithShort("OK", "TRANSACTION_STATUS");
                objPolicyXTransEnhNew.DateApproved = Conversion.ToDbDate(DateTime.Now);
                objPolicyXTransEnhNew.TransactionPrem = objPolicyXRtngEnhTemp.TransactionPrem;
                objPolicyXTransEnhNew.TotalEstPremium = objPolicyXRtngEnhTemp.TotalEstPremium;
                objPolicyXTransEnhNew.TotalBilledPrem = objPolicyXRtngEnhTemp.TotalBilledPrem;
                objPolicyXTransEnhNew.ChgBilledPremium = objPolicyXRtngEnhTemp.TotalBilledPrem;
                objPolicyXTransEnhNew.PolicyStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");
                objPolicyXTransEnhNew.TransactionDate = Conversion.ToDbDate(DateTime.Now);
                //npadhy RMSC retrofit Starts
                objPolicyXTransEnhNew.TaxAmount = objPolicyXRtngEnhTemp.TaxAmount;
                objPolicyXTransEnhNew.TaxRate = objPolicyXRtngEnhTemp.TaxRate;
                objPolicyXTransEnhNew.TaxType = objPolicyXRtngEnhTemp.TaxType;
                objPolicyXTransEnhNew.TranTax = objPolicyXRtngEnhTemp.TranTax;
                //npadhy RMSC retrofit Starts

                // Add the First Term 
                objPolicyXTermEnhNew = objPolicyEnhNew.PolicyXTermEnhList.AddNew();
                objPolicyXTermEnhNew.PolicyId = 0;
                objPolicyXTermEnhNew.TermNumber = 1;
                objPolicyXTermEnhNew.RenewalTermCode = iTermCode;
                objPolicyXTermEnhNew.RenewalTermAmt = iTermAmount;
                objPolicyXTermEnhNew.SequenceAlpha = 1;
                objPolicyXTermEnhNew.TermStatus = base.CCacheFunctions.GetCodeIDWithShort("I", "TERM_STATUS");
                objPolicyXTermEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                objPolicyXTermEnhNew.RenewalInd = base.CCacheFunctions.GetCodeIDWithShort("M", "RENEWAL_INDICATOR");
                objPolicyXTermEnhNew.EffectiveDate = base.GetLatestTerm(PolicyEnh).EffectiveDate;
                objPolicyXTermEnhNew.ExpirationDate = base.GetLatestTerm(PolicyEnh).ExpirationDate;
                objPolicyXTermEnhNew.PolicyNumber = base.GetLatestTerm(PolicyEnh).PolicyNumber;
                //npadhy RMSC retrofit Starts
                objPolicyXTermEnhNew.BrokerCommission = base.GetLatestTerm(PolicyEnh).BrokerCommission;    
                //npadhy RMSC retrofit Ends


                // Add the MCO List
                foreach (PolicyXMcoEnh objPolicyXMcoEnh in PolicyEnh.PolicyXMcoEnhList)
                {
                    objPolicyXMcoEnhNew = objPolicyEnhNew.PolicyXMcoEnhList.AddNew();
                    objPolicyXMcoEnhNew.PolicyId = 0;
                    objPolicyXMcoEnhNew.McoBeginDate = objPolicyXMcoEnh.McoBeginDate;
                    objPolicyXMcoEnhNew.McoEid = objPolicyXMcoEnh.McoEid;
                    objPolicyXMcoEnhNew.McoEndDate = objPolicyXMcoEnh.McoEndDate;
                }

                //Add by kuladeep for Audit Issue MITS:24736 Start
                // Update the Insured. Resave with new PolicyId. 
                //if (PolicyEnh.PolicyXInsuredEnh != null)
                //{
                //    foreach (System.Collections.DictionaryEntry objDictionaryEntry in PolicyEnh.PolicyXInsuredEnh)
                //    {
                //        objPolicyEnhNew.PolicyXInsuredEnh.Add(objDictionaryEntry.Value);
                //    }
                //}
                if (PolicyEnh.PolicyXInsuredEnhList != null)
                {
                    foreach (PolicyXInsuredEnh objPolicyXInsrdEnh in PolicyEnh.PolicyXInsuredEnhList)
                    {
                        objPolicyXInsrdEnhNew = objPolicyEnhNew.PolicyXInsuredEnhList.AddNew();
                        objPolicyXInsrdEnhNew.PolicyId = 0;
                        objPolicyXInsrdEnhNew.InsuredEid = objPolicyXInsrdEnh.InsuredEid;
                        objPolicyXInsrdEnhNew.BillingInsrd = objPolicyXInsrdEnh.BillingInsrd;
                    }
                }
                //Add by kuladeep for Audit Issue MITS:24736 End//skhare7 RMSC merge

                // Add the Coverage List
                foreach (PolicyXCvgEnh objPolicyXCvgEnh in PolicyEnh.PolicyXCvgEnhList)
                {
                    objPolicyXCvgEnhNew = objPolicyEnhNew.PolicyXCvgEnhList.AddNew();
                    objPolicyXCvgEnhNew.PolicyId = 0;
                    objPolicyXCvgEnhNew.TransactionType = objPolicyXTransEnhNew.TransactionType;
                    objPolicyXCvgEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXCvgEnhNew.TermNumber = 1;
                    objPolicyXCvgEnhNew.Status = base.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");
                    objPolicyXCvgEnhNew.SequenceAlpha = 1;
                    objPolicyXCvgEnhNew.CovRenewableFlag = true;
                    objPolicyXCvgEnhNew.BrokerName = objPolicyXCvgEnh.BrokerName;
                    objPolicyXCvgEnhNew.CancelNoticeDays = objPolicyXCvgEnh.CancelNoticeDays;
                    objPolicyXCvgEnhNew.ClaimLimit = objPolicyXCvgEnh.ClaimLimit;
                    objPolicyXCvgEnhNew.CoverageTypeCode = objPolicyXCvgEnh.CoverageTypeCode;
                    objPolicyXCvgEnhNew.Exceptions = objPolicyXCvgEnh.Exceptions;
                    objPolicyXCvgEnhNew.NextPolicyId = objPolicyXCvgEnh.NextPolicyId;
                    objPolicyXCvgEnhNew.NotificationUid = objPolicyXCvgEnh.NotificationUid;
                    objPolicyXCvgEnhNew.OccurrenceLimit = objPolicyXCvgEnh.OccurrenceLimit;
                    objPolicyXCvgEnhNew.PolicyLimit = objPolicyXCvgEnh.PolicyLimit;
                    objPolicyXCvgEnhNew.Remarks = objPolicyXCvgEnh.Remarks;
                    objPolicyXCvgEnhNew.SelfInsRetention = objPolicyXCvgEnh.SelfInsRetention;
                    objPolicyXCvgEnhNew.TotalPayments = objPolicyXCvgEnh.TotalPayments;
                    objPolicyXCvgEnhNew.EffectiveDate = objPolicyXCvgEnh.EffectiveDate;
                    objPolicyXCvgEnhNew.ExpirationDate = objPolicyXCvgEnh.ExpirationDate;
                    objPolicyXCvgEnhNew.CoverageNumber = objPolicyXCvgEnh.CoverageNumber;
                    objPolicyXCvgEnhNew.Deductible = objPolicyXCvgEnh.Deductible;
                    //Anu Tennyson for MITS 18229 1/15/2010 STARTS
                    objPolicyXCvgEnhNew.FlatOrPercent = objPolicyXCvgEnh.FlatOrPercent;
                    objPolicyXCvgEnhNew.Modifier = objPolicyXCvgEnh.Modifier;
                    objPolicyXCvgEnhNew.ExpRateRowId = objPolicyXCvgEnh.ExpRateRowId;
                    objPolicyXCvgEnhNew.Amount = objPolicyXCvgEnh.Amount;
                    objPolicyXCvgEnhNew.CoverageSeq = objPolicyXCvgEnh.CoverageSeq;
                    //Anu Tennyson
                    //To Save the UAR list in POLICY_X_INSRD_ENH with new polciy id.
                    if (sLOB == "AL")
                    {
                        if (objPolicyXCvgEnh.PolicyXCvgUarAddedAL != null)
                        {
                            foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objPolicyXCvgEnh.PolicyXCvgUarAddedAL)
                            {
                                objPolicyXCvgEnhNew.PolicyXCvgUarAddedAL.Add(objUARDictionaryEntry.Value);
                            }
                        }
                    }
                    else if (sLOB == "PC")
                    {
                        if (objPolicyXCvgEnh.PolicyXCvgUarAddedPC != null)
                        {
                            foreach (System.Collections.DictionaryEntry objUARDictionaryEntry in objPolicyXCvgEnh.PolicyXCvgUarAddedPC)
                            {
                                objPolicyXCvgEnhNew.PolicyXCvgUarAddedPC.Add(objUARDictionaryEntry.Value);
                            }
                        }
                    }
                    //Anu Tennyson
                    //Anu Tennyson for MITS 18229 1/15/2010 ENDS
                    //pmahli MITS 9841 Supplementals

                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXCvgEnh><Supplementals/></PolicyXCvgEnh>");
                    objCoverages.LoadXml(objPolicyXCvgEnh.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objCoverages, objPolicyXCvgEnhNew);
                    //sPolcvgRowIdFilter = "Instance/PolicyEnh/PolicyXCvgEnhList/PolicyXCvgEnh[PolcvgRowId = " + objPolicyXCvgEnh.PolcvgRowId.ToString() + "]";
                    //PopulateSupplementals(sPolcvgRowIdFilter, objPolicyXCvgEnhNew);
                    //End:Sumit
                }

                // Add the Exposure List
                foreach (PolicyXExpEnh objPolicyXExpEnh in PolicyEnh.PolicyXExposureEnhList)
                {
                    objPolicyXExpEnhNew = objPolicyEnhNew.PolicyXExposureEnhList.AddNew();
                    objPolicyXExpEnhNew.PolicyId = 0;
                    objPolicyXExpEnhNew.TransactionType = objPolicyXTransEnhNew.TransactionType;
                    objPolicyXExpEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXExpEnhNew.TermNumber = 1;
                    objPolicyXExpEnhNew.Status = base.CCacheFunctions.GetCodeIDWithShort("OK", "COVEXP_STATUS");
                    objPolicyXExpEnhNew.SequenceAlpha = 1;
                    objPolicyXExpEnhNew.RenewableFlag = true;
                    objPolicyXExpEnhNew.CoverageCode = objPolicyXExpEnh.CoverageCode;
                    objPolicyXExpEnhNew.EffectiveDate = objPolicyXExpEnh.EffectiveDate;
                    objPolicyXExpEnhNew.Exceptions = objPolicyXExpEnh.Exceptions;
                    objPolicyXExpEnhNew.ExpirationDate = objPolicyXExpEnh.ExpirationDate;
                    objPolicyXExpEnhNew.ExposureAmount = objPolicyXExpEnh.ExposureAmount;
                    objPolicyXExpEnhNew.ExposureBaseRate = objPolicyXExpEnh.ExposureBaseRate;
                    objPolicyXExpEnhNew.ExposureCode = objPolicyXExpEnh.ExposureCode;
                    objPolicyXExpEnhNew.ExposureCount = objPolicyXExpEnh.ExposureCount;
                    objPolicyXExpEnhNew.ExposureRate = objPolicyXExpEnh.ExposureRate;
                    objPolicyXExpEnhNew.FullAnnPremAmt = objPolicyXExpEnh.FullAnnPremAmt;
                    objPolicyXExpEnhNew.HeirarchyLevel = objPolicyXExpEnh.HeirarchyLevel;
                    objPolicyXExpEnhNew.PrAnnPremAmt = objPolicyXExpEnh.PrAnnPremAmt;
                    objPolicyXExpEnhNew.PremAdjAmt = objPolicyXExpEnh.PremAdjAmt;
                    objPolicyXExpEnhNew.Remarks = objPolicyXExpEnh.Remarks;
                    //pmahli MITS 9841 Supplementals

                    //Start:Sumit (09/13/2010)-MITS# 21871
                    objSerializationXMl.LoadXml("<PolicyXExpEnh><Supplementals/></PolicyXExpEnh>");
                    objExposure.LoadXml(objPolicyXExpEnh.SerializeObject(objSerializationXMl));
                    PopulateSupplementalsFromXML(objExposure, objPolicyXExpEnhNew);
                    //sPolicyXExpEnhIdFilter = "Instance/PolicyEnh/PolicyXExposureEnhList/PolicyXExpEnh[ExposureId = " + objPolicyXExpEnh.ExposureId.ToString() + "]";
                    //PopulateSupplementals(sPolicyXExpEnhIdFilter, objPolicyXExpEnhNew);
                    //End:Sumit

                    //Sumit - Start(04/06/2010) - MITS# 18229 - Update Exposure Children
                    if (objPolicyXExpEnh.PolicyXUarList != null)
                    {
                        foreach (PolicyXUar objPolicyXUar in objPolicyXExpEnh.PolicyXUarList)
                        {
                            objPolicyXUarNew = objPolicyXExpEnhNew.PolicyXUarList.AddNew();
                            objPolicyXUarNew.UarRowId = objPolicyXUar.UarRowId;
                            objPolicyXUarNew.PolicyId = objPolicyXUar.PolicyId;
                            objPolicyXUarNew.ExposureId = objPolicyXUar.ExposureId;
                            objPolicyXUarNew.UarId = objPolicyXUar.UarId;
                            objPolicyXUarNew.PolicyStatusCode = base.CCacheFunctions.GetCodeIDWithShort("I", "POLICY_STATUS");

                            //Sumit - Start(05/17/2010) - MITS# 20483 - Update Uar Children
                            if (objPolicyXUar.PolicyXPschedEnhList != null)
                            {
                                foreach (PolicyXPschedEnh objPolicyXPsched in objPolicyXUar.PolicyXPschedEnhList)
                                {
                                    objPolicyXScheduleNew = objPolicyXUarNew.PolicyXPschedEnhList.AddNew();
                                    objPolicyXScheduleNew.PschedRowID = objPolicyXPsched.PschedRowID;
                                    objPolicyXScheduleNew.PolicyId = objPolicyXPsched.PolicyId;
                                    objPolicyXScheduleNew.UarId = objPolicyXPsched.UarId;
                                    objPolicyXScheduleNew.Name = objPolicyXPsched.Name;
                                    objPolicyXScheduleNew.Amount = objPolicyXPsched.Amount;
                                }
                            }
                            //Sumit - End
                        }
                    }
                    //Sumit - End
                }

                // Add the Rating Record List
                foreach (PolicyXRtngEnh objPolicyXRtngEnh in PolicyEnh.PolicyXRatingEnhList)
                {
                    objPolicyXRtngEnhNew = objPolicyEnhNew.PolicyXRatingEnhList.AddNew();
                    objPolicyXRtngEnhNew.PolicyId = 0;
                    objPolicyXRtngEnhNew.TransactionType = objPolicyXTransEnhNew.TransactionType;
                    objPolicyXRtngEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXRtngEnhNew.TermNumber = 1;
                    objPolicyXRtngEnhNew.SequenceAlpha = 1;
                    objPolicyXRtngEnhNew.ExpenseConstant = objPolicyXRtngEnh.ExpenseConstant;
                    objPolicyXRtngEnhNew.ExpModAmount = objPolicyXRtngEnh.ExpModAmount;
                    objPolicyXRtngEnhNew.ExpModFactor = objPolicyXRtngEnh.ExpModFactor;
                    objPolicyXRtngEnhNew.ManualPremium = objPolicyXRtngEnh.ManualPremium;
                    objPolicyXRtngEnhNew.ModifiedPremium = objPolicyXRtngEnh.ModifiedPremium;
                    objPolicyXRtngEnhNew.TotalBilledPrem = objPolicyXRtngEnh.TotalBilledPrem;
                    objPolicyXRtngEnhNew.TotalEstPremium = objPolicyXRtngEnh.TotalEstPremium;
                    objPolicyXRtngEnhNew.TransactionPrem = objPolicyXTransEnhNew.TransactionPrem;
                    //npadhy RMSC retrofit Starts
                    objPolicyXRtngEnhNew.TaxAmount = objPolicyXRtngEnh.TaxAmount;
                    objPolicyXRtngEnhNew.TaxRate = objPolicyXRtngEnh.TaxRate ;
                    objPolicyXRtngEnhNew.TaxType = objPolicyXRtngEnh.TaxType;
                    objPolicyXRtngEnhNew.TranTax = objPolicyXRtngEnh.TranTax;
                    //npadhy RMSC retrofit Ends

                }

                // Add the Discount List
                foreach (PolicyXDcntEnh objPolicyXDcntEnh in PolicyEnh.PolicyXDiscountEnhList)
                {
                    objPolicyXDcntEnhNew = objPolicyEnhNew.PolicyXDiscountEnhList.AddNew();
                    objPolicyXDcntEnhNew.PolicyId = 0;
                    objPolicyXDcntEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXDcntEnhNew.DiscountAmt = objPolicyXDcntEnh.DiscountAmt;
                    objPolicyXDcntEnhNew.DiscountFactor = objPolicyXDcntEnh.DiscountFactor;
                    objPolicyXDcntEnhNew.DiscountLevelId = objPolicyXDcntEnh.DiscountLevelId;
                    objPolicyXDcntEnhNew.DiscountNameId = objPolicyXDcntEnh.DiscountNameId;
                }

                // Update the Discount Tier List
                foreach (PolicyXDtierEnh objPolicyXDtierEnh in PolicyEnh.PolicyXDiscountTierEnhList)
                {
                    objPolicyXDtierEnhNew = objPolicyEnhNew.PolicyXDiscountTierEnhList.AddNew();
                    objPolicyXDtierEnhNew.PolicyId = 0;
                    objPolicyXDtierEnhNew.TransactionId = objPolicyXTransEnhNew.TransactionId;
                    objPolicyXDtierEnhNew.DiscTierAmt = objPolicyXDtierEnh.DiscTierAmt;
                    objPolicyXDtierEnhNew.DiscTierLevelId = objPolicyXDtierEnh.DiscTierLevelId;
                    objPolicyXDtierEnhNew.DiscTierNameId = objPolicyXDtierEnh.DiscTierNameId;
                }

                //Geeta 02/14/07 : Modified for Adding Billing Check                
                if (base.UseBillingSystem)
                {
                    foreach (PolicyXBillEnh objPolicyXBillEnh in PolicyEnh.PolicyXBillEnhList)
                    {
                        objNewBill = objPolicyEnhNew.PolicyXBillEnhList.AddNew();
                        objNewBill.BillingRuleRowId = objPolicyXBillEnh.BillingRuleRowId;
                        objNewBill.BillToEid = objPolicyXBillEnh.BillToEid;
                        objNewBill.BillToOverride = objPolicyXBillEnh.BillToOverride;
                        objNewBill.BillToType = objPolicyXBillEnh.BillToType;
                        objNewBill.PayPlanRowId = objPolicyXBillEnh.PayPlanRowId;
                        objNewBill.NewBillToEid = objPolicyXBillEnh.NewBillToEid;
                        objNewBill.TermNumber = 1;
                        objNewBill.TransactionId = objPolicyXTransEnhNew.TransactionId;
                        // Naresh Changes for DoNotBill field
                        objNewBill.DoNotBill = objPolicyXBillEnh.DoNotBill;
                    }

                    using (objBillingMaster = new Billing.BillingMaster(PolicyEnh.Context.Factory,base.ClientId))
                    {
                        objBillingMaster.Policies.Add(iPolicyid, objPolicyEnhNew);
                        // Naresh Changes for DoNotBill field
                        if (objNewBill.DoNotBill == false)
                            base.PostToBilling(objBillingMaster, objPolicyXTransEnhNew.TransactionId, "NB", PolicyEnh);
                        // Start Naresh Changes done to Merge the Changes done by Gagan at a later Date
                        objBillingMaster.Save(true);
                    }
                }

                return (objPolicyEnhNew);
            }
            finally
            {
                if (objReader != null)
                    objReader.Close();
                if (objConn != null)
                    objConn.Close();
                objPolicyXTransEnhNew = null;
                objPolicyXTermEnhNew = null;
                objPolicyXRtngEnhNew = null;
                objPolicyXMcoEnhNew = null;
                objPolicyXCvgEnhNew = null;
                objPolicyXExpEnhNew = null;
                objPolicyXDcntEnhNew = null;
                objPolicyXDtierEnhNew = null;
                objPolicyXRtngEnhTemp = null;
                objBillingMaster = null;
  //Add by kuladeep for Audit Issue MITS:24736//skhare7 RMSC merge
                objPolicyXInsrdEnhNew = null;
                objPolicyXUarNew = null;//paggarwal2
                //Start:Sumit (09/13/2010)-MITS# 21871
                objSerializationXMl = null;
                objExposure = null;
                objCoverages = null;
                //End:Sumit
            }
        }

        private XmlDocument m_propStore = new XmlDocument();
        public XmlDocument PropertyStore
        {
            get { return m_propStore; }
            set { m_propStore = value; }
        }
        private void PopulateSupplementals(string rootTagName, PolicyEnh pol)
        {
            string sProp = string.Empty;
            object o = null;
            foreach (XmlElement item in PropertyStore.SelectNodes(String.Format("/{0}/*", rootTagName)))
            {
                sProp = item.LocalName;

                if (sProp == "Supplementals")
                {
                    if (pol.GetType().GetProperty("Supplementals") != null)
                        if (pol.Supplementals != null)
                            pol.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                }
            }
        }
        private void PopulateSupplementals(string rootTagName, PolicyXCvgEnh pol)
        {
            string sProp = string.Empty;
            object o = null;
            foreach (XmlElement item in PropertyStore.SelectNodes(String.Format("/{0}/*", rootTagName)))
            {
                sProp = item.LocalName;
                
                if (sProp == "Supplementals")
                {
                    if (pol.GetType().GetProperty("Supplementals") != null)
                        if (pol.Supplementals != null)
                            pol.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                }
            }
        }
        private void PopulateSupplementals(string rootTagName, PolicyXExpEnh pol)
        {
            string sProp = string.Empty;
            object o = null;
            foreach (XmlElement item in PropertyStore.SelectNodes(String.Format("/{0}/*", rootTagName)))
            {
                sProp = item.LocalName;

                if (sProp == "Supplementals")
                {
                    if (pol.GetType().GetProperty("Supplementals") != null)
                        if (pol.Supplementals != null)
                            pol.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                }
            }
        }


        #region Populate Supplementals from XML
        /// <summary>
        /// Author:Sumit Kumar
        /// Date:09/13/2010
        /// MITS#: 21871
        /// </summary>
        /// <param name="objXmlDocument"></param>
        /// <param name="pol"></param>
        private void PopulateSupplementalsFromXML(XmlDocument objXmlDocument, PolicyXCvgEnh pol)
        {
            try
            {
                foreach (XmlElement item in objXmlDocument.SelectNodes("//Supplementals"))
                {
                    if (item!=null)
                    {
                        if (pol.GetType().GetProperty("Supplementals") != null)
                            if (pol.Supplementals != null)
                                pol.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.GetSupplementals.Error", base.ClientId), p_objEx);
            }
        }

        private void PopulateSupplementalsFromXML(XmlDocument objXmlDocument, PolicyXExpEnh pol)
        {
            try
            {
                foreach (XmlElement item in objXmlDocument.SelectNodes("//Supplementals"))
                {
                    if (item != null)
                    {
                        if (pol.GetType().GetProperty("Supplementals") != null)
                            if (pol.Supplementals != null)
                                pol.Supplementals.PopulateObject(Utilities.XmlElement2XmlDoc(item), false);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.GetSupplementals.Error", base.ClientId), p_objEx);
            }
        }

        #endregion Populate Supplementals from XML


        // Start Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
        // Define the function to Get the Insured Details from Database.
        #region Get Insured Details
        public XmlDocument InsuredSelect(int p_iInsuredId)
        {
            XmlDocument objXmlDocument = null;
            XmlElement objRootElement = null;
            XmlElement objChildElement = null;
            string sSQL = string.Empty;
            DbReader objReader = null;
            int iStateID = 0;
            int iCountryId = 0;
            string sAbbr = string.Empty;
            string sName = String.Empty;
            try
            {
                sSQL = "SELECT ENTITY_ID,LAST_NAME,FIRST_NAME,ADDR1,ADDR2,ADDR3,ADDR4,CITY,ZIP_CODE,STATE_ID,COUNTRY_CODE";
                sSQL += " FROM ENTITY ";
                sSQL += " WHERE ENTITY_ID = " + p_iInsuredId.ToString();

                objReader = DbFactory.GetDbReader(base.ConnectionString, sSQL);
                objXmlDocument = new XmlDocument();
                base.StartDocument(ref objXmlDocument, ref objRootElement, "InsuredDetails");
                if (objReader.Read())
                {
                    base.CreateAndSetElement(objRootElement, "EntityId", Conversion.ConvertObjToStr(objReader.GetValue("ENTITY_ID")));
                    base.CreateAndSetElement(objRootElement, "LastName", Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")));
                    base.CreateAndSetElement(objRootElement, "FirstName", Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")));
                    base.CreateAndSetElement(objRootElement, "Addr1", Conversion.ConvertObjToStr(objReader.GetValue("ADDR1")));
                    base.CreateAndSetElement(objRootElement, "Addr2", Conversion.ConvertObjToStr(objReader.GetValue("ADDR2")));
                    base.CreateAndSetElement(objRootElement, "Addr3", Conversion.ConvertObjToStr(objReader.GetValue("ADDR3")));
                    base.CreateAndSetElement(objRootElement, "Addr4", Conversion.ConvertObjToStr(objReader.GetValue("ADDR4")));
                    base.CreateAndSetElement(objRootElement, "City", Conversion.ConvertObjToStr(objReader.GetValue("CITY")));
                    base.CreateAndSetElement(objRootElement, "ZipCode", Conversion.ConvertObjToStr(objReader.GetValue("ZIP_CODE")));
                    iStateID = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), base.ClientId);
                    base.CCacheFunctions.GetStateInfo(iStateID, ref sAbbr, ref sName);
                    base.CreateAndSetElement(objRootElement, "StateId", sAbbr + " " + sName, ref objChildElement);
                    objChildElement.SetAttribute("codeid", iStateID.ToString());

                    iCountryId = Conversion.ConvertObjToInt(objReader.GetValue("COUNTRY_CODE"), base.ClientId);
                    base.CCacheFunctions.GetCodeInfo(iCountryId, ref sAbbr, ref sName);
                    base.CreateAndSetElement(objRootElement, "CountryCode", sAbbr + " " + sName, ref objChildElement);
                    objChildElement.SetAttribute("codeid", iCountryId.ToString());
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EnhancePolicy.GetInsuredDetails.Error", base.ClientId), p_objEx);
            }
            finally
            {
                objReader.Close();
            }
            return (objXmlDocument);

        }
        #endregion Get Insured Details
        // End Naresh (6/4/2007) MITS 9511 Populate the Insured details in the Policy Billing Tab
    }
}
