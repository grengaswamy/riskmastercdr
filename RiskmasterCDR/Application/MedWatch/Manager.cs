﻿
using System;
using System.Xml;
using System.IO;
using System.Collections;
using Riskmaster.DataModel;
using System.Text;
using Riskmaster.Application.DocumentManagement;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Db;
using Microsoft.VisualBasic;
using Riskmaster.Application.FileStorage;  

namespace Riskmaster.Application.MedWatch
{
	
	
	/// <summary>
    /// This class contains functions to be used in Medwatch report generation.
	/// </summary>
	public class Manager : IDisposable
	{
		#region FacilityType structure
		internal struct structFacilityType
		{
			/// <summary>
			/// Name
			/// </summary>
			private string p_sName;
			/// <summary>
			/// Address
			/// </summary>
			private string p_sAddress;
			/// <summary>
			/// City
			/// </summary>
			private string p_sCity;
			/// <summary>
			///	State
			/// </summary>
			private string p_sState;
			/// <summary>
			///	Zip
			/// </summary>
			private string p_sZip;
			/// <summary>
			/// Contact
			/// </summary>
			private string p_sContact;
			/// <summary>
			/// Phone
			/// </summary>
			private string p_sPhone;
			/// <summary>
			/// HCFA(Health Care Financing Administration) 
			/// </summary>
			private string p_sHCFA;
			/// <summary>
			/// Name
			/// </summary>
			public string Name 
			{
				get
				{
					return p_sName;
				}
				set
				{
					p_sName=value;
				}
			}
			/// <summary>
			/// Address
			/// </summary>
			public string Address
			{
				get
				{
					return p_sAddress;
				}
				set
				{
					p_sAddress=value;
				}
			}
			/// <summary>
			/// City
			/// </summary>
			public string City 
			{
				get
				{
					return p_sCity;
				}
				set
				{
					p_sCity=value;
				}
			}
			/// <summary>
			/// State
			/// </summary>
			public string State 
			{
				get
				{
					return p_sState;
				}
				set
				{
					p_sState=value;
				}
			}
			/// <summary>
			/// Zip
			/// </summary>
			public string Zip 
			{
				get
				{
					return p_sZip;
				}
				set
				{
					p_sZip=value;
				}
			}
			/// <summary>
			/// Contact
			/// </summary>
			public string Contact 
			{
				get
				{
					return p_sContact;
				}
				set
				{
					p_sContact=value;
				}
			}
		
			/// <summary>
			/// Phone
			/// </summary>
			public string Phone 
			{
				get
				{
					return p_sPhone;
				}
				set
				{
					p_sPhone=value;
				}
			}
			/// <summary>
			/// HCFA
			/// </summary>
			public string HCFA 
			{
				get
				{
					return p_sHCFA;
				}
				set
				{
					p_sHCFA=value;
				}
			}
		}
		#endregion

		#region Enums Declarations
		/// <summary>
		/// Report type
		/// </summary>
		public enum FdaReportType
		{
			FDA3500=1,
			FDA3500A=2,
		}
		#endregion

		#region Variable Declarations
		/// <summary>
		/// List of generated reports
		/// </summary>
		private ArrayList m_arrlstReports =null;
		/// <summary>
		/// Connection string for database
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Password
		/// </summary>
		private string m_sPassword="";
		/// <summary>
		/// Data source name to connect
		/// </summary>
		private string m_sDataSource="";
		/// <summary>
		/// Report class object
		/// </summary>
		private Report m_objReport=null;
		/// <summary>
		/// Generated Report number
		/// </summary>
		private string m_sUFDistReportNumber="";
		/// <summary>
		/// Cache object
		/// </summary>
		private LocalCache m_oCache = null;
		/// <summary>
		/// UserLogin object
		/// </summary>
		private UserLogin m_objLogin=null;
        private string m_strMedwatchFDFPath = string.Empty;
        private int m_iClientId = 0;
		#endregion.

		#region Constant Declarations
		/// <summary>
		/// Constant use to create generated reports filename
		/// </summary>
		private const string MED_CONST="_Medwatch_";
		/// <summary>
		/// Constant use to create a space between 2 columns of report.
		/// </summary>
		private const string SPACER="                    ";
		#endregion

		#region Manager class constructor
        public Manager(UserLogin objLogin, string p_sDSN, int p_iClientId)
		{
			m_sUserName=objLogin.LoginName;
			m_sPassword=objLogin.Password;
			m_objLogin=objLogin;
			m_sDSN=p_sDSN;
            m_iClientId = p_iClientId;
			m_oCache = new LocalCache(p_sDSN,m_iClientId);
            m_strMedwatchFDFPath = RMConfigurator.CombineFilePath(RMConfigurator.TempPath, "MedWatch");
	    }
		#endregion
        bool _disposed = false;
        /// <summary>
		/// Destructor Added by Shivendu to do clean up
		/// </summary>
        ~Manager()
		{
            Dispose();
		}
        /// <summary>
        /// Dipose function Added by Shivendu to do clean up
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                if (m_oCache != null)
                {
                    m_oCache.Dispose();
                    m_oCache = null;
                }
                GC.SuppressFinalize(this);
            }
        }
		#region Functions
		/// Name		: GenerateReport
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the data for the report based on report type
		/// </summary>
		/// <param name="p_eReportType">Report type</param>
		/// <param name="p_lEventId">Event id</param>
		/// <returns>Success -1 or Failure -0 in execution of the function</returns>
		public int GenerateReport(FdaReportType p_eReportType ,long  p_lEventId)
		{
			Event objEvent=null;
			PiPatient objPiPatient=null;
			long lEventCategory=0;
			long lEventIndCode=0;
			string sSQL="";
			string sShortCode="";
			string sCodeDesc="";
			int iPatientCount=0;
			int iPiCount=0;
			string sTemp="";
			DataModelFactory objDMF=null;
			EventMedwatch objEventMedwatch=null;
			DbReader objRead=null;
			long lSexCode=0;
			int iNoOfYears=0;
			int iReturnValue=0;
			try
			{
				m_arrlstReports = new ArrayList();
                objDMF = new DataModelFactory(m_objLogin, m_iClientId);
				objEvent = (Event)objDMF.GetDataModelObject("Event",false);
				objEvent.MoveTo((int)p_lEventId);
				lEventIndCode=objEvent.EventIndCode;
				sSQL = "SELECT SYS_EVENT_CAT.DISPLAY_ID"
					+" FROM SYS_EVENT_CAT, CODES"
					+" WHERE SYS_EVENT_CAT.EVENT_CAT_CODE = CODES.RELATED_CODE_ID"
					+" AND CODES.CODE_ID = " + lEventIndCode;
				objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
				if (objRead.Read())
				{
					lEventCategory=Common.Conversion.ConvertStrToLong(
						Common.Conversion.ConvertObjToStr(objRead.GetValue(0)));
				}
				iPiCount=objEvent.PiList.Count;
				iPatientCount=0;
				switch(lEventCategory)
				{
					case (long)Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment://equipment-related events.  print 1 report; if multiple patients show the first and add number of patients as note to box A5.
                        m_objReport = new Report(p_eReportType, m_sDSN, m_iClientId);
						foreach (PersonInvolved objPi in objEvent.PiList)
						{
							if (objPi.GetType()==typeof(PiPatient))
							{
								iPatientCount = iPatientCount + 1;
							
								if (iPatientCount==1)
								{
									objPiPatient=(PiPatient) objPi;
									
									m_objReport.Add(objPiPatient.Patient.MedicalRcdNo, "A_1");
									m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat(
										objPiPatient.PiEntity.BirthDate,"d"), "A_2_Dob");
									//new code
                                    iNoOfYears = Utilities.GetDateDiffInYears(objPi.PiEntity.BirthDate, objEvent.DateOfEvent, m_iClientId);				
									m_objReport.Add(iNoOfYears.ToString() + " Yrs", "A_2_Age");
									lSexCode=objPiPatient.PiEntity.SexCode;
									m_oCache.GetCodeInfo((int)lSexCode,ref sShortCode,ref sCodeDesc);
									m_objReport.Add(sShortCode, "A_3");
									m_objReport.Add(objPiPatient.Patient.Weight.ToString(), "A_4_Lbs");
									//new code
									if (objPiPatient.Patient.DateOfDeath.Trim().Length > 0)
									{
										m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat
											(objPiPatient.Patient.DateOfDeath,"d"), "B_2_DateOfDeath");
										m_objReport.Add("YES","B_2_Death");
									}
								}
							}
						}
						sTemp="";
						if (iPatientCount >1)
						{
							sTemp = "There are " + iPatientCount + " patients for this event." 
								+ Constants.VBCrLf;

						}
						CreateReport(objEvent, lEventCategory, sTemp);
						m_arrlstReports.Add(m_objReport);
						break;
                    case (long)Constants.EVENT_CATEGORY_DISPLAY_ID.Medication:  //medicine-related events.  if multiple patients print separate report for each patient
						
						foreach (PersonInvolved objPi in objEvent.PiList)
						{
							if (objPi.GetType()==typeof(PiPatient))
							{
                                m_objReport = new Report(p_eReportType, m_sDSN, m_iClientId);
								iPatientCount = iPatientCount + 1;
								objPiPatient=(PiPatient) objPi;
								
								m_objReport.Add(objPiPatient.Patient.MedicalRcdNo, "A_1");
								
								m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat(
									objPiPatient.PiEntity.BirthDate,"d"), "A_2_Dob");
								//new code
                                iNoOfYears = Utilities.GetDateDiffInYears(objPi.PiEntity.BirthDate, objEvent.DateOfEvent, m_iClientId);				
								m_objReport.Add(iNoOfYears.ToString() + " Yrs", "A_2_Age");
								lSexCode=objPiPatient.PiEntity.SexCode;
								m_oCache.GetCodeInfo((int)lSexCode,ref sShortCode,ref sCodeDesc);
								m_objReport.Add(sShortCode, "A_3");
								m_objReport.Add(objPiPatient.Patient.Weight.ToString(), "A_4_Lbs");
								//new code
								if (objPiPatient.Patient.DateOfDeath.Trim().Length > 0)
								{
									m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat
										(objPiPatient.Patient.DateOfDeath,"d"), "B_2_DateOfDeath");
									m_objReport.Add("YES","B_2_Death");
										
								}
								CreateReport(objEvent, lEventCategory, "");
								m_arrlstReports.Add(m_objReport);
							}
						}
						if (iPatientCount==0)
						{
                            m_objReport = new Report(p_eReportType, m_sDSN, m_iClientId);
							CreateReport(objEvent, lEventCategory, "");
							m_arrlstReports.Add(m_objReport);
						}
						break;
					default:
						throw new RMAppException("Manager.GenerateReport.UnknownEventCategoryError");
				}
				objEventMedwatch= (EventMedwatch)objDMF.GetDataModelObject("EventMedwatch",false);
				objEventMedwatch.MoveTo((int)p_lEventId);
				objEventMedwatch.LastReportDate=Riskmaster.Common.Conversion.ToDbDate(DateTime.Now.Date);
				objEventMedwatch.Save();
				objEventMedwatch=null;
				iReturnValue=1;
			}
			catch (DataModelException p_objException)
			{
				throw new RMAppException(Globalization.GetString("Manager.GenerateReport.DataModelError", m_iClientId),p_objException);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Manager.GenerateReport.Error", m_iClientId),p_objException);
			}
			finally
			{
				if (objRead != null) 
				{
					objRead.Close(); 
					objRead.Dispose();
				}
                //Modified by Shivendu
                if (objDMF != null)
                {
                    objDMF.UnInitialize();
                    objDMF.Dispose();

                }
                
				objEventMedwatch=null;
				objPiPatient=null;
				objEvent=null;
			}
			return iReturnValue;
		}
		/// Name		: DestroyReportsCollection
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes the generated reports based on the unique id embedded in the report names
		/// </summary>
		/// <param name="p_sID">Unique id embedded in generated report filename</param>
		/// <returns>Success -1 or Failure - 0 in execution of the function</returns>
		public int DestroyReportsCollection(string p_sID)
		{
			string sFolder="";
			string [] arrFiles=null;
			int iReturnValue=0;
			try
			{
                sFolder = m_strMedwatchFDFPath;
				arrFiles=Directory.GetFiles(sFolder,"*"+p_sID+"*.fdf");
				for (int i=0;i<arrFiles.Length;i++)
					File.Delete(arrFiles[i]);
				iReturnValue=1;
			}
			catch(DirectoryNotFoundException p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.DestroyReportsCollection.DirectoryNotFound", m_iClientId), p_objException);
			}
			catch (IOException p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.DestroyReportsCollection.FileInputOutputError", m_iClientId), p_objException);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.DestroyReportsCollection.Error", m_iClientId), p_objException);
			}
			finally
			{
				arrFiles=null;
			}
			return iReturnValue;
		}
		/// Name		: PrintReportsCollection
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Generates the report file(s)
		/// </summary>
		/// <param name="p_sID">Unique id to be embedded in the filename</param>
		/// <param name="p_sFileNames">Generated filenames separated by pipe symbol |</param>
		/// <returns>Success -1 or Failure - 0 in execution of the function</returns>
		public int PrintReportsCollection(string p_sID,out string p_sFileNames)
		{
			Report objMedWatchReport=null;
			string sFile="";
			string sFolder="";
			StreamWriter objFileWriter=null;
			int iResult=0;
			
			XmlDocument objDOMDocIds=null;
			XmlElement objElemParentDocIds=null;
			XmlElement objElemChildDocIds=null;
			MemoryStream objMemory=null;
			try
			{
				p_sFileNames="";
                sFolder = m_strMedwatchFDFPath;
				objDOMDocIds =new XmlDocument();
				objElemParentDocIds = objDOMDocIds.CreateElement("Reports");
				objDOMDocIds.AppendChild(objElemParentDocIds);
				for (int i=0;i<m_arrlstReports.Count;i++)
				{
					objMedWatchReport=(Report)m_arrlstReports[i];
					sFile = p_sID + MED_CONST + i.ToString() + ".fdf";
					objFileWriter=new StreamWriter(sFolder +"\\"+ sFile,false);
					objFileWriter.Write(objMedWatchReport.GetFdfFileString());
					objFileWriter.Close();
                    objMemory = Utilities.ConvertFilestreamToMemorystream(sFolder + "\\" + sFile, m_iClientId);
					objElemChildDocIds=objDOMDocIds.CreateElement("Report");
					objElemChildDocIds.SetAttribute("Filename",sFile);
					objElemChildDocIds.InnerText=Convert.ToBase64String(objMemory.ToArray());
					objDOMDocIds.FirstChild.AppendChild(objElemChildDocIds);
				}
				p_sFileNames=objDOMDocIds.InnerXml;
				iResult=1;
			}
			catch (IOException p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.PrintReportsCollection.FileInputOutputError", m_iClientId), p_objException);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Manager.PrintReportsCollection.Error", m_iClientId),p_objException);//rkaur27
			}
			finally
			{
				if (objFileWriter!=null)
				{
					objFileWriter.Close();
					objFileWriter=null;
				}
				objMedWatchReport=null;
				objFileWriter=null;
				objDOMDocIds=null;
				//Modified by Shivendu
                if (objMemory != null)
                {
                    objMemory.Dispose();
                    objMemory = null;
                }
				objElemParentDocIds=null;
				objElemChildDocIds=null;
			}
			return iResult;
		}
		/// Name		: GetAttachedFile
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Returns a memory stream of given attached file
		/// </summary>
		/// <param name="p_lDocId">Attached file document id</param>
		/// <param name="p_objMemory">Returns a memory stream of given attached file</param>
		/// <returns>Success -1 or Failure - 0 in execution of the function</returns>
		public int GetAttachedFile(long p_lDocId,StorageType p_enumStorageType,string p_sDestStoragePath,out MemoryStream p_objMemory)
		{
			DocumentManager objDocumentManager=null;
			MemoryStream objMemory=null;
			int iReturnVal=0;
			try
			{
                objDocumentManager = new DocumentManager(m_objLogin, m_iClientId);
				objDocumentManager.UserLoginName=m_sUserName;
				objDocumentManager.DocumentStorageType=p_enumStorageType;
				objDocumentManager.DestinationStoragePath=p_sDestStoragePath;
				objDocumentManager.ConnectionString=m_sDSN;
				objDocumentManager.RetrieveDocument(p_lDocId, 0, out objMemory);
				p_objMemory=objMemory;
				iReturnVal=1;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.GetAttachedFile.Error", m_iClientId), p_objException);
			}
			finally
			{
                //Added by Shivendu
                if (objMemory != null)
                {
                    objMemory.Dispose();
                    objMemory = null;
                }
				objDocumentManager=null;
			}
			return iReturnVal;
		}
		/// Name		: AttachReportsCollection
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Generates the report file(s) and attaches to the the event
		/// </summary>
		/// <param name="p_sID">Unique id to be embedded in the filename</param>
		/// <param name="p_iEventId">Event id</param>
		/// <param name="p_sFilePath">File path</param>
		/// <param name="p_sDocumentIds">Generated filenames document ids separated by pipe symbol |</param>
		/// <returns>Success -1 or Failure - 0 in execution of the function</returns>
		public int AttachReportsCollection(string p_sID,int p_iEventId,string p_sFilePath
			,StorageType p_enumStorageType,string p_sDestStoragePath,out string p_sDocumentIds)
		{
			Report objMedWatchReport=null;
			string sFile="";
			int iResult=0;
			DocumentManager objDocumentManager=null;
			string sFolder="";
			MemoryStream objMemory=null;
			XmlDocument objDOM=null;
		
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;

			XmlDocument objDOMDocIds=null;
			XmlElement objElemParentDocIds=null;
			XmlElement objElemChildDocIds=null;
			
			string p_sReportName="";
			long lDocumentId=0;
			try
			{
				
				
				p_sDocumentIds="";
                sFolder = m_strMedwatchFDFPath;
				objDOMDocIds =new XmlDocument();
				objElemParentDocIds = objDOMDocIds.CreateElement("Reports");
				objDOMDocIds.AppendChild(objElemParentDocIds);
				for (int i=0;i<m_arrlstReports.Count;i++)
				{
					objMedWatchReport=(Report)m_arrlstReports[i];
					p_sReportName=objMedWatchReport.ReportName;

					sFile = p_sID + MED_CONST + i.ToString() + ".fdf";

                    objDocumentManager = new DocumentManager(m_objLogin, m_iClientId);
					objDocumentManager.ConnectionString=m_sDSN;
					objDocumentManager.UserLoginName =m_sUserName;
					objDocumentManager.DocumentStorageType=p_enumStorageType;
					objDocumentManager.DestinationStoragePath=p_sDestStoragePath;

					objDOM =new XmlDocument();
					objElemParent = objDOM.CreateElement("data");
					objDOM.AppendChild(objElemParent);

					objElemChild=objDOM.CreateElement("Document");
					
					objElemTemp=objDOM.CreateElement("DocumentId");
					objElemTemp.InnerText="0";
					objElemChild.AppendChild(objElemTemp);
					
					objElemTemp=objDOM.CreateElement("FolderId");
					objElemTemp.InnerText="0";
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("CreateDate");
					objElemTemp.InnerText=Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
					objElemChild.AppendChild(objElemTemp);
														
					objElemTemp=objDOM.CreateElement("Category");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Name");
					objElemTemp.InnerText=p_sReportName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Class");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Subject");
					objElemTemp.InnerText=p_sReportName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Type");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Notes");
					objElemChild.AppendChild(objElemTemp);
					
					objElemTemp=objDOM.CreateElement("UserLoginName");
					objElemTemp.InnerText=m_sUserName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("FileName");
					objElemTemp.InnerText=sFile;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("FilePath");
					objElemTemp.InnerText=p_sFilePath;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Keywords");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("AttachTable");
					objElemTemp.InnerText="EVENT";
					objElemChild.AppendChild(objElemTemp);
					
					objElemTemp=objDOM.CreateElement("Subject");
					objElemTemp.InnerText=p_sReportName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("AttachRecordId");
					objElemTemp.InnerText=p_iEventId.ToString();
					objElemChild.AppendChild(objElemTemp);

					objDOM.FirstChild.AppendChild(objElemChild);

					objMemory=new MemoryStream();
                    objMemory = Utilities.ConvertFilestreamToMemorystream(sFolder + sFile, m_iClientId);
										
					objDocumentManager.AddDocument(objDOM.InnerXml,objMemory, 0, out lDocumentId);

					objElemChildDocIds=objDOMDocIds.CreateElement("Report");
					objElemChildDocIds.InnerText=lDocumentId.ToString();
					objDOMDocIds.FirstChild.AppendChild(objElemChildDocIds);
					
					objDocumentManager=null;
					objDOM=null;
					objMemory=null;
					objElemTemp = null;
					objElemChild=null;
					objElemParent=null;
				}
				
				p_sDocumentIds=objDOMDocIds.InnerXml;
				iResult=1;
			}
			catch(XmlException p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.AttachReportsCollection.XmlError", m_iClientId), p_objException);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("Manager.AttachReportsCollection.Error", m_iClientId),p_objException);//rkaur27
			}
			finally
			{
               
				objMedWatchReport=null;
				objDocumentManager=null;
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				objDOMDocIds=null;
				objElemParentDocIds=null;
				objElemChildDocIds=null;
			}
			return iResult;
		}
		/// Name		: PrintReportsCollection
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Generates the report file(s)
		/// </summary>
		/// <param name="p_sID">Unique id to be embedded in the filename</param>
		/// <param name="p_iEventId">Event id</param>
		/// <param name="p_sFilePath">File path</param>
		/// <param name="p_sFileNames">Generated filenames separated by pipe symbol |</param>
		/// <returns>Success -1 or Failure - 0 in execution of the function</returns>
		public int PrintReportsCollection(string p_sID,int p_iEventId,string p_sFilePath
			,StorageType p_enumStorageType,string p_sDestStoragePath,out string p_sFileNames)
		{
			Report objMedWatchReport=null;
			string sFile="";
			string sFolder="";
			StreamWriter objFileWriter=null;
			int iResult=0;
			long lDocumentId=0;
			DocumentManager objDocumentManager=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;

			XmlDocument objDOMDocIds=null;
			XmlElement objElemParentDocIds=null;
			XmlElement objElemChildDocIds=null;
			

			string p_sReportName="";
			MemoryStream objMemory=null;
			try
			{
				p_sFileNames="";
                sFolder = m_strMedwatchFDFPath;
				objDOMDocIds =new XmlDocument();
				objElemParentDocIds = objDOMDocIds.CreateElement("Report");
				objDOMDocIds.AppendChild(objElemParentDocIds);
				for (int i=0;i<m_arrlstReports.Count;i++)
				{
					objMedWatchReport=(Report)m_arrlstReports[i];
					p_sReportName=objMedWatchReport.ReportName;

					sFile = p_sID + MED_CONST + i.ToString() + ".fdf";
					objFileWriter=new StreamWriter(sFolder + sFile,false);
					objFileWriter.Write(objMedWatchReport.GetFdfFileString());
					objFileWriter.Close();	

					if (i==0)
					{
						p_sFileNames=sFolder + sFile;
					}
					else
					{
						p_sFileNames=p_sFileNames+"|"+sFolder + sFile;
					}

                    objDocumentManager = new DocumentManager(m_objLogin, m_iClientId);
					objDocumentManager.ConnectionString=m_sDSN;
					objDocumentManager.UserLoginName =m_sUserName;
					objDocumentManager.DocumentStorageType=p_enumStorageType;
					objDocumentManager.DestinationStoragePath=p_sDestStoragePath;

					objDOM =new XmlDocument();
					objElemParent = objDOM.CreateElement("data");
					objDOM.AppendChild(objElemParent);

					objElemChild=objDOM.CreateElement("Document");
					
					objElemTemp=objDOM.CreateElement("DocumentId");
					objElemTemp.InnerText="0";
					objElemChild.AppendChild(objElemTemp);
					
					objElemTemp=objDOM.CreateElement("FolderId");
					objElemTemp.InnerText="0";
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("CreateDate");
					objElemTemp.InnerText=Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now);
					objElemChild.AppendChild(objElemTemp);
														
					objElemTemp=objDOM.CreateElement("Category");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Name");
					objElemTemp.InnerText=p_sReportName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Class");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Subject");
					objElemTemp.InnerText=p_sReportName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Type");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Notes");
					objElemChild.AppendChild(objElemTemp);
					
					objElemTemp=objDOM.CreateElement("UserLoginName");
					objElemTemp.InnerText=m_sUserName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("FileName");
					objElemTemp.InnerText=sFile;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("FilePath");
					objElemTemp.InnerText=p_sFilePath;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("Keywords");
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("AttachTable");
					objElemTemp.InnerText="EVENT";
					objElemChild.AppendChild(objElemTemp);
					
					objElemTemp=objDOM.CreateElement("Subject");
					objElemTemp.InnerText=p_sReportName;
					objElemChild.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("AttachRecordId");
					objElemTemp.InnerText=p_iEventId.ToString();
					objElemChild.AppendChild(objElemTemp);

					objDOM.FirstChild.AppendChild(objElemChild);

					objMemory=new MemoryStream();
                    objMemory = Utilities.ConvertFilestreamToMemorystream(sFolder + sFile, m_iClientId);

                    objDocumentManager.AddDocument(objDOM.InnerXml,objMemory, 0, out lDocumentId);

					objElemChildDocIds=objDOMDocIds.CreateElement("Report");
					objElemChildDocIds.InnerText=sFolder + sFile;
					objDOMDocIds.FirstChild.AppendChild(objElemChildDocIds);

					objDocumentManager=null;
					objDOM=null;
					objMemory=null;
					objElemTemp = null;
					objElemChild=null;
					objElemParent=null;
				}
				
				p_sFileNames=objDOMDocIds.InnerXml;
				iResult=1;
			}
			catch(XmlException p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.PrintReportsCollection.XmlError", m_iClientId), p_objException);
			}
			catch (IOException p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.PrintReportsCollection.FileInputOutputError", m_iClientId), p_objException);
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.PrintReportsCollection.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objFileWriter!=null)
				{
					objFileWriter.Close();
					objFileWriter=null;
				}
				if (objMemory!=null)
				{
					objMemory.Close();
					objMemory=null;
				}
				objMedWatchReport=null;
				objFileWriter=null;
				objDocumentManager=null;
				objDOM=null;
				objElemTemp = null;
				objElemChild=null;
				objElemParent=null;
				objDOMDocIds=null;
				objElemParentDocIds=null;
				objElemChildDocIds=null;
			}
			return iResult;
		}
		/// Name		: CreateReport
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches the data for report
		/// </summary>
		/// <param name="p_objEvent">Passed Event object</param>
		/// <param name="p_lEventCategory">Event category</param>
		/// <param name="p_sDescription">Description representing patient related information</param>
		/// <returns>Success -true or Failure -false in execution of the function</returns>
		private bool CreateReport(Event p_objEvent,long p_lEventCategory,string p_sDescription)
		{
			string sSQL="";
			DbReader objRead=null;
			structFacilityType objFacilityType;
			EventMedwatch objEventMedwatch=null;
			string sDesc="";
			string sTemp1="";
			string sTemp2="";
			string sTemp3="";
			StringBuilder sbTemp=null;
			int iTemp=0;
			bool bReturnValue=false;
			try
			{
				sSQL = " SELECT OC_FAC_NAME,OC_FAC_ADDR1,OC_FAC_ADDR2," +
					"OC_FAC_ADDR3,OC_FAC_ADDR4,OC_FAC_PHONE,"+
					"OC_FAC_CONTACT,OC_FAC_HCFA_NO FROM SYS_PARMS ";
				objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
				objFacilityType=new structFacilityType();
				if (objRead.Read())
				{
					objFacilityType.Name=Common.Conversion.ConvertObjToStr(objRead.GetValue(0)).Trim();
					objFacilityType.Address=Common.Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim();
					objFacilityType.City=Common.Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
					objFacilityType.State=Common.Conversion.ConvertObjToStr(objRead.GetValue(3)).Trim();
					objFacilityType.Zip=Common.Conversion.ConvertObjToStr(objRead.GetValue(4)).Trim();
					objFacilityType.Phone=Common.Conversion.ConvertObjToStr(objRead.GetValue(5)).Trim();
					objFacilityType.Contact=Common.Conversion.ConvertObjToStr(objRead.GetValue(6)).Trim();
					objFacilityType.HCFA=Conversion.ConvertObjToStr(objRead.GetValue(7)).Trim();
				}
				objRead.Close();
				if (objFacilityType.HCFA.Length==0)
				{
					objFacilityType.HCFA="000000";
				}
				objEventMedwatch=p_objEvent.EventMedwatch;
                m_sUFDistReportNumber = objFacilityType.HCFA + "-" + objEventMedwatch.ReportYear.ToString()
                    + "-" + string.Format("{0:0000}", objEventMedwatch.ReportSerialNo);
				m_objReport.Add(m_sUFDistReportNumber,"UfDistReportNumber");
				//Section B
				if (objEventMedwatch.AdverseEventFlag)
				{
					m_objReport.Add("YES","B_1_Event");
				}
				if (objEventMedwatch.ProductProbFlag)
				{
					m_objReport.Add("YES","B_1_Product");
				}
				if (objEventMedwatch.LifeThreatFlag)
				{
					m_objReport.Add("YES","B_2_LifeThreatening");
				}
				if (objEventMedwatch.HospitalizFlag)
				{
					m_objReport.Add("YES","B_2_Hospitalization");
				}
				if (objEventMedwatch.DisabilityFlag)
				{
					m_objReport.Add("YES","B_2_Disability");
				}
				if (objEventMedwatch.CongenitalFlag)
				{
					m_objReport.Add("YES","B_2_Congenital");
				}
				if (objEventMedwatch.ReqdIntervFlag)
				{
					m_objReport.Add("YES","B_2_Intervention");
				}
				
				m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat
					(p_objEvent.DateOfEvent,"d"), "B_3");
				m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat(
					Riskmaster.Common.Conversion.ToDbDate(DateTime.Now),"d") ,"B_4");
				sDesc=p_sDescription+p_objEvent.EventDescription;
				sDesc=sDesc.Trim();
				if (sDesc.Length >1000)
				{
					m_objReport.Add (sDesc.Substring(0, 1000), "B_5");
				}
				else
				{
					m_objReport.Add (sDesc, "B_5");
				}
				if (!RelevantTests(p_objEvent.EventId))
				{
					throw new RMAppException("Manager.ExecuteRelevantTest.Error");
				}
				m_objReport.Add (objEventMedwatch.RelevantHistory, "B_7");
				//Section C--MedicationEvent only
				if (p_lEventCategory==(long)Constants.EVENT_CATEGORY_DISPLAY_ID.Medication)
				{
					m_objReport.Add(objEventMedwatch.MedName,"C_1_1");
					m_objReport.Add(objEventMedwatch.Dose+ ", " + objEventMedwatch.Frequency + ", " + objEventMedwatch.Route,"C_2_1");
					sTemp1 = objEventMedwatch.TherapyFromDate;
					sTemp2 = objEventMedwatch.TherapyToDate;
					if ( (Conversion.ConvertObjToInt(sTemp1, m_iClientId)==0)
						&& (Conversion.ConvertObjToInt(sTemp2, m_iClientId)==0))
					{
						m_objReport.Add(objEventMedwatch.Duration.ToString(),"C_3_1");
					}
					else
					{
						m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat
							(sTemp1,"d")+Riskmaster.Common.Conversion.GetDBDateFormat
							(sTemp2,"d"),"C_3_1");
					}
					//the application does not allow user to enter a value for this
					m_objReport.Add (objEventMedwatch.DiagnosisTest, "C_4_1");
					//the application does not offer a "doesn't apply" option
					if (objEventMedwatch.MwEventAbated)
					{
						m_objReport.Add("YES","C_5_1");
					}
					else
					{
						m_objReport.Add("NO","C_5_1");
					}
					//the application does not offer a "doesn't apply" option
					if (objEventMedwatch.MwEvntReappeared)
					{
						m_objReport.Add("YES","C_8_1");
					}
					else
					{
						m_objReport.Add("NO","C_8_1");
					}
					m_objReport.Add(objEventMedwatch.LotNumber,"C_6_1");
					m_objReport.Add(Riskmaster.Common.Conversion.GetDBDateFormat
						(objEventMedwatch.ExpirationDate,"d"),"C_7_1");
					m_objReport.Add(objEventMedwatch.NdcNumber.ToString(),"C_9");
					if (!ConcomitantProducts(p_objEvent.EventId,"C_10"))
					{
						throw new RMAppException("Manager.ExecuteConcomitantProducts.Error");
					}
							
				}
				//Section D--Equipment event only
				if (p_lEventCategory==(long)Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment)
				{
					m_objReport.Add (objEventMedwatch.BrandName, "D_1");
					iTemp=objEventMedwatch.EquipTypeCode;
					m_oCache.GetCodeInfo(iTemp,ref sTemp1 ,ref sTemp2);
					m_objReport.Add (sTemp2, "D_2");
				
					iTemp=objEventMedwatch.ManufacturerEntity.StateId;
					if (iTemp!=0)
					{
						m_oCache.GetStateInfo(iTemp,ref sTemp3 ,ref sTemp1);
					}
					sbTemp=new StringBuilder();
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.LastName.Trim());
					sbTemp=sbTemp.Append(Constants.VBCrLf);
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.Addr1.Trim());
					sTemp1=Riskmaster.Common.Conversion.ConvertObjToStr(sbTemp);
					sTemp2 = objEventMedwatch.ManufacturerEntity.Addr2.Trim();
					if (sTemp2.Length > 0)
					{
						sTemp1=sTemp1+Constants.VBCrLf+sTemp2;
					}
                    sTemp2 = objEventMedwatch.ManufacturerEntity.Addr3.Trim();
                    if (sTemp2.Length > 0)
                    {
                        sTemp1 = sTemp1 + Constants.VBCrLf + sTemp2;
                    }
                    sTemp2 = objEventMedwatch.ManufacturerEntity.Addr4.Trim();
                    if (sTemp2.Length > 0)
                    {
                        sTemp1 = sTemp1 + Constants.VBCrLf + sTemp2;
                    }
					sbTemp=new StringBuilder();
					sbTemp=sbTemp.Append(sTemp1);
					sbTemp=sbTemp.Append(Constants.VBCrLf);
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.City.Trim());
					sbTemp=sbTemp.Append(", ");
					sbTemp=sbTemp.Append(sTemp3);
					sbTemp=sbTemp.Append(" ");
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.ZipCode.Trim());
					sTemp1=Riskmaster.Common.Conversion.ConvertObjToStr(sbTemp);
					m_objReport.Add (sTemp1, "D_3");
					sTemp1="";
					sTemp2="";
					sTemp3="";
					iTemp =objEventMedwatch.DeviceOperCode;
					if (iTemp!=0)
					{
						m_oCache.GetCodeInfo(iTemp,ref sTemp1 ,ref sTemp2);
						switch (Conversion.ConvertObjToInt(sTemp1, m_iClientId))
						{
							case 1:
								sTemp2 = "PRO";
								break;
							case 2:
								sTemp2 = "LAY";
								break;
							case 3:
								sTemp2 = "OTHER";
								break;

						}
					}
					m_objReport.Add (sTemp2, "D_4");
					m_objReport.Add (Riskmaster.Common.Conversion.GetDBDateFormat
						(objEventMedwatch.EqExpirationDate,"d"), "D_5");
					m_objReport.Add (Riskmaster.Common.Conversion.GetDBDateFormat
						(objEventMedwatch.ImplantDate,"d"), "D_7");
					m_objReport.Add (Riskmaster.Common.Conversion.GetDBDateFormat
						(objEventMedwatch.ExplantDate,"d"), "D_8");
					m_objReport.Add (objEventMedwatch.ModelNumber, "D_6_Model");
					m_objReport.Add (objEventMedwatch.CatalogNumber, "D_6_Catalog");
					m_objReport.Add (objEventMedwatch.SerialNumber, "D_6_Serial");
					m_objReport.Add (objEventMedwatch.EqLotNumber, "D_6_Lot");
					m_objReport.Add (objEventMedwatch.OtherNumber, "D_6_Other");
					if (objEventMedwatch.DevAvailForEval)
					{
						m_objReport.Add("YES","D_9");
					}
					else
					{
						m_objReport.Add("NO","D_9");
					}
					sTemp1=objEventMedwatch.DeviceReturnDate.Trim();
					if (sTemp1.Length >0)
					{
						m_objReport.Add("Returned","D_9_Returned");
						m_objReport.Add (Riskmaster.Common.Conversion.GetDBDateFormat
							(sTemp1,"d"), "D_9_ReturnedDate");
					}
					if (!ConcomitantProducts(p_objEvent.EventId,"D_10"))
					{
						throw new RMAppException("Manager.ExecuteConcomitantProducts.Error");
					}
				}
				//Section E
				iTemp=objEventMedwatch.RptdByEntity.StateId;
				if (iTemp!=0)
				{
					m_oCache.GetCodeInfo(iTemp,ref sTemp3 ,ref sTemp1);
				}
				sbTemp=new StringBuilder();
				sbTemp=sbTemp.Append(objEventMedwatch.RptdByEntity.FirstName.Trim());
				sbTemp=sbTemp.Append(" ");
				sbTemp=sbTemp.Append(objEventMedwatch.RptdByEntity.LastName.Trim());
				sbTemp=sbTemp.Append(Constants.VBCrLf);
				sbTemp=sbTemp.Append(objEventMedwatch.RptdByEntity.Addr1.Trim());
				sTemp1=Riskmaster.Common.Conversion.ConvertObjToStr(sbTemp);
				sTemp2 = objEventMedwatch.RptdByEntity.Addr2.Trim();
				if (sTemp2.Length > 0)
				{
					sTemp1=sTemp1+Constants.VBCrLf+sTemp2;
				}
                sTemp2 = objEventMedwatch.RptdByEntity.Addr3.Trim();
                if (sTemp2.Length > 0)
                {
                    sTemp1 = sTemp1 + Constants.VBCrLf + sTemp2;
                }
                sTemp2 = objEventMedwatch.RptdByEntity.Addr4.Trim();
                if (sTemp2.Length > 0)
                {
                    sTemp1 = sTemp1 + Constants.VBCrLf + sTemp2;
                }
				sbTemp=new StringBuilder();
				sbTemp=sbTemp.Append(sTemp1);
				sbTemp=sbTemp.Append(Constants.VBCrLf);
				sbTemp=sbTemp.Append(objEventMedwatch.RptdByEntity.City.Trim());
				sbTemp=sbTemp.Append(", ");
				sbTemp=sbTemp.Append(sTemp3);
				sbTemp=sbTemp.Append(" ");
				sbTemp=sbTemp.Append(objEventMedwatch.RptdByEntity.ZipCode.Trim());
				sTemp1=Riskmaster.Common.Conversion.ConvertObjToStr(sbTemp);
				m_objReport.Add (sTemp1, "E_1_NameAddress");
				m_objReport.Add (objEventMedwatch.RptdByEntity.Phone1.Trim(), "E_1_Phone");
				if (objEventMedwatch.RptdByProFlag)
				{
					m_objReport.Add("YES","E_2");
				}
				else
				{
					m_objReport.Add("NO","E_2");
				}
				iTemp=objEventMedwatch.RptdByPosCode;
				if (iTemp!=0)
				{
					m_oCache.GetCodeInfo(iTemp,ref sTemp1 ,ref sTemp2);
					m_objReport.Add (sTemp2, "E_3");
				}
				if (Conversion.ConvertObjToInt(objEventMedwatch.DateRptdToFda, m_iClientId)!=0)
				{
					m_objReport.Add ("YES", "E_4");
				}
				else
				{
					m_objReport.Add ("NO", "E_4");
				}
				//Section F--Equipment event only
				if (p_lEventCategory==(long)Constants.EVENT_CATEGORY_DISPLAY_ID.Equipment)
				{
					m_objReport.Add ("USER", "F_1");
					m_objReport.Add (m_sUFDistReportNumber, "F_2");
					sbTemp=new StringBuilder();
					sbTemp=sbTemp.Append(objFacilityType.Name);
					sbTemp=sbTemp.Append(Constants.VBCrLf);
					sbTemp=sbTemp.Append(objFacilityType.Address);
					sbTemp=sbTemp.Append(Constants.VBCrLf);
					sbTemp=sbTemp.Append(objFacilityType.City);
					sbTemp=sbTemp.Append(", ");
					sbTemp=sbTemp.Append(objFacilityType.State);
					sbTemp=sbTemp.Append(", ");
					sbTemp=sbTemp.Append(objFacilityType.Zip);
					sTemp1=Riskmaster.Common.Conversion.ConvertObjToStr(sbTemp);
					m_objReport.Add (sTemp1, "F_3");	
					m_objReport.Add (objFacilityType.Contact, "F_4");	
					m_objReport.Add (objFacilityType.Phone, "F_5");	
					//F_6 not mapped
                    iTemp = Conversion.ConvertObjToInt(objEventMedwatch.FollowUpCount, m_iClientId);
					if (iTemp==0)
					{
						m_objReport.Add ("INITIAL", "F_7");	
					}
					else
					{
						m_objReport.Add ("FOLLOWUP", "F_7");	
						m_objReport.Add (iTemp.ToString(), "F_7_FollowUpNumber");	
					}
					m_objReport.Add (DateTime.Now.ToString(), "F_8");
					m_objReport.Add (objEventMedwatch.AgeOfDevice, "F_9");
					//F_10 is not mapped
					sTemp1=objEventMedwatch.DateRptdToFda.Trim();
					if (sTemp1.Length>0)
					{
						m_objReport.Add ("YES", "F_11");
						m_objReport.Add (Riskmaster.Common.Conversion.GetDBDateFormat
							(sTemp1,"d"), "F_11_DateSent");
					}
					else
					{
						m_objReport.Add ("NO", "F_11");
					}
					sTemp1="";
					sTemp2="";
					sTemp3="";
					iTemp =p_objEvent.LocationTypeCode;
					if (iTemp!=0)
					{
						m_oCache.GetCodeInfo(iTemp,ref sTemp1 ,ref sTemp2);
						switch (sTemp1)
						{
							case "HSP":
							case "HME":
							case "NHM":
							case "OTF":
							case "ODF":
							case "ASF":
							case "OTH":
								m_objReport.Add (sTemp1, "F_12");
								break;
							default:
								m_objReport.Add ("OTH", "F_12");
								m_objReport.Add (sTemp2, "F_12_OtherText");
								break;

						}
					}
					sTemp1=objEventMedwatch.DateRptdToMfg.Trim();
					if (sTemp1.Length >0)
					{
						m_objReport.Add("YES","F_13");
						m_objReport.Add (Riskmaster.Common.Conversion.GetDBDateFormat
							(sTemp1,"d"), "F_13_DateSent");
					}
					else
					{
						m_objReport.Add("NO","F_13");
					}


					iTemp=objEventMedwatch.ManufacturerEntity.StateId;
					if (iTemp!=0)
					{
						m_oCache.GetStateInfo(iTemp,ref sTemp3 ,ref sTemp1);
					}
					sbTemp=new StringBuilder();
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.LastName.Trim());
					sbTemp=sbTemp.Append(Constants.VBCrLf);
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.Addr1.Trim());
					sTemp1 = Riskmaster.Common.Conversion.ConvertObjToStr(sbTemp);
					sTemp2 =objEventMedwatch.ManufacturerEntity.Addr2.Trim();
					if (sTemp2.Length > 0)
					{
						sTemp1=sTemp1+Constants.VBCrLf+sTemp2;
					}
                    sTemp2 = objEventMedwatch.ManufacturerEntity.Addr3.Trim();
                    if (sTemp2.Length > 0)
                    {
                        sTemp1 = sTemp1 + Constants.VBCrLf + sTemp2;
                    }
                    sTemp2 = objEventMedwatch.ManufacturerEntity.Addr4.Trim();
                    if (sTemp2.Length > 0)
                    {
                        sTemp1 = sTemp1 + Constants.VBCrLf + sTemp2;
                    }
					sbTemp=new StringBuilder();
					sbTemp=sbTemp.Append(sTemp1);
					sbTemp=sbTemp.Append(Constants.VBCrLf);
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.City.Trim());
					sbTemp=sbTemp.Append(", ");
					sbTemp=sbTemp.Append(sTemp3);
					sbTemp=sbTemp.Append(" ");
					sbTemp=sbTemp.Append(objEventMedwatch.ManufacturerEntity.ZipCode.Trim());
					sTemp1=Riskmaster.Common.Conversion.ConvertObjToStr(sbTemp);
					m_objReport.Add (sTemp1, "F_14");
					
				}
				bReturnValue=true;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.CreateReport.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
					objRead.Dispose();
				}
				objEventMedwatch=null;
				sbTemp=null;
			}
			return bReturnValue;
		}
		/// Name		: ConcomitantProducts
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches product related information related to event
		/// </summary>
		/// <param name="p_lEventId">Event id</param>
		/// <param name="p_sItem">Report section to which to add the product information</param>
		/// <returns>Success -true or Failure -false in execution of the function</returns>
		private bool ConcomitantProducts(long p_lEventId,string p_sItem)
		{
			ArrayList arrlstOuter=null;
			string sSQL="";
			ArrayList arrlstInner=null;
			string sTemp="";
			string sConcomitantProducts="";
			DbReader objRead=null;
			bool bReturnValue=false;
			try
			{
				arrlstOuter=new ArrayList();
				sSQL = "SELECT FROM_DATE,TO_DATE,CONCOM_PRODUCT"
					+" FROM EV_X_CONCOM_PROD "
					+" WHERE EVENT_ID = " + p_lEventId
					+" ORDER BY FROM_DATE ";
				objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objRead.Read())
				{
					arrlstInner=new ArrayList();
					sTemp=Common.Conversion.ConvertObjToStr(objRead.GetValue(0)).Trim();
					arrlstInner.Add(sTemp);
					sTemp=Common.Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim();
					arrlstInner.Add(sTemp);
					sTemp=Common.Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
					arrlstInner.Add(sTemp);
					arrlstOuter.Add(arrlstInner);
				}
				if (arrlstOuter.Count>0)
				{
					if (arrlstOuter.Count > 3)
					{
						for (int i=0;i<3;i++)
						{
							arrlstInner=(ArrayList)arrlstOuter[i];
							sConcomitantProducts=sConcomitantProducts+Riskmaster.Common.Conversion.GetDBDateFormat(Common.Conversion.ConvertObjToStr(arrlstInner[0]),"d")
								+SPACER+Riskmaster.Common.Conversion.GetDBDateFormat(Common.Conversion.ConvertObjToStr(arrlstInner[1]),"d")
								+SPACER + 
								Common.Conversion.ConvertObjToStr(arrlstInner[2]) +
								Constants.VBCrLf;
						}
					}
					if (arrlstOuter.Count > 3)
					{
                        sConcomitantProducts = sConcomitantProducts + Globalization.GetString("Manager.Attach.Value", m_iClientId);
					}
					m_objReport.Add(sConcomitantProducts, p_sItem);
					if (arrlstOuter.Count > 3)
					{
						m_objReport.Add("D-10 Concomitant Medical Products - (Continued)"
							+SPACER+m_sUFDistReportNumber,"ConcomitantProducts_PageHeader");
						sConcomitantProducts="";
						for (int i=3;i<arrlstOuter.Count;i++)
						{
							arrlstInner=(ArrayList)arrlstOuter[i];
							sConcomitantProducts=sConcomitantProducts+Riskmaster.Common.Conversion.GetDBDateFormat(Common.Conversion.ConvertObjToStr(arrlstInner[0]),"d")
								+SPACER+Riskmaster.Common.Conversion.GetDBDateFormat(Common.Conversion.ConvertObjToStr(arrlstInner[1]),"d")
								+SPACER + 
								Common.Conversion.ConvertObjToStr(arrlstInner[2]) +
								Constants.VBCrLf;
						}
						
						m_objReport.Add(sConcomitantProducts, "ConcomitantProducts");
					}

				}
				bReturnValue=true;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.ConcomitantProducts.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
					objRead.Dispose();
				}
				arrlstOuter=null;
				arrlstInner=null;
				objRead=null;
			}
			return bReturnValue;
		}
		/// Name		: RelevantTests
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fetches tests related information related to event
		/// </summary>
		/// <param name="p_lEventId">Event id</param>
		/// <param name="p_sItem">Report section to which to add the tests information</param>
		/// <returns>Success -true or Failure -false in execution of the function</returns>
		private bool RelevantTests(long p_lEventId)
		{
			ArrayList arrlstOuter=null;
			string sSQL="";
			ArrayList arrlstInner=null;
			string sTemp="";
			string sRelevantTests="";
			DbReader objRead=null;
			bool bReturnValue=false;
			try
			{
				arrlstOuter=new ArrayList();
				sSQL = "SELECT TEST_DATE,LAB_TEST,RESULT"
					+" FROM EVENT_X_MEDW_TEST "
					+" WHERE EVENT_X_MEDW_TEST.EVENT_ID = " + p_lEventId
					+" ORDER BY TEST_DATE ";
				objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
				while (objRead.Read())
				{
					arrlstInner=new ArrayList();
					sTemp=Common.Conversion.ConvertObjToStr(objRead.GetValue(0)).Trim();
					arrlstInner.Add(sTemp);
					sTemp=Common.Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim();
					arrlstInner.Add(sTemp);
					sTemp=Common.Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
					arrlstInner.Add(sTemp);
					arrlstOuter.Add(arrlstInner);
				}
				if (arrlstOuter.Count>0)
				{
					if (arrlstOuter.Count > 3)
					{
						for (int i=0;i<3;i++)
						{
							arrlstInner=(ArrayList)arrlstOuter[i];
							sRelevantTests=sRelevantTests+Riskmaster.Common.Conversion.GetDBDateFormat(Common.Conversion.ConvertObjToStr(arrlstInner[0]),"d")
								+SPACER+Common.Conversion.ConvertObjToStr(arrlstInner[1])+Constants.VBCrLf
								+"Results:" + SPACER + Common.Conversion.ConvertObjToStr(arrlstInner[2]) +Constants.VBCrLf;
						}
					}
					if (arrlstOuter.Count > 3)
					{
                        sRelevantTests = sRelevantTests + Globalization.GetString("Manager.Attach.Value", m_iClientId);
					}
					m_objReport.Add(sRelevantTests, "B_6");
					if (arrlstOuter.Count > 3)
					{
						m_objReport.Add("B-6 Relevant Tests/Laboratory Data - (Continued)"
							+SPACER+m_sUFDistReportNumber,"RelevantTests_PageHeader");
						sRelevantTests="";
						for (int i=3;i<arrlstOuter.Count;i++)
						{
							arrlstInner=(ArrayList)arrlstOuter[i];
							sRelevantTests=sRelevantTests+Riskmaster.Common.Conversion.GetDBDateFormat(Common.Conversion.ConvertObjToStr(arrlstInner[0]),"d")
								+SPACER+Common.Conversion.ConvertObjToStr(arrlstInner[1])
								+ SPACER + Common.Conversion.ConvertObjToStr(arrlstInner[2]) +Constants.VBCrLf;
						}
						m_objReport.Add(sRelevantTests, "RelevantTests");
					}

				}
				bReturnValue=true;
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Manager.RelevantTests.Error", m_iClientId), p_objException);
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
					objRead.Dispose();
				}
				arrlstOuter=null;
				arrlstInner=null;
				objRead=null;
			}
			return bReturnValue;
		}
		#endregion

	}
}
