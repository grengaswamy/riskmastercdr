using System;
using Riskmaster.Common;

using System.Collections;
using Riskmaster.ExceptionTypes;
using System.Text;
namespace Riskmaster.Application.MedWatch
{

	/// <summary>
    /// This class contains functions to be used as a container for Medwatch report
	/// </summary>
	internal class Report
	{
		#region Variable Declarations
		/// <summary>
		/// Report fields
		/// </summary>
		ArrayList m_arrlstFields=null;
		/// <summary>
		/// Report fields values
		/// </summary>
		ArrayList m_arrlstValues=null;
		/// <summary>
		/// Report type
		/// </summary>
		private Manager.FdaReportType eReportType=Manager.FdaReportType.FDA3500A;

        private int m_iClientId = 0;
        private string m_sConnString = string.Empty;
		#endregion
		
		#region Constant Declarations
		/// <summary>
		/// Pdf header information
		/// </summary>
		private string PDFHEADER="%FDF-1.2" 
			+ Constants.VBCrLf +
			"1 0 obj <<"+
			Constants.VBCrLf +
			"/FDF <<"+ 
			Constants.VBCrLf +
			"/Fields"+
			Constants.VBCrLf +
			"[";
		#endregion

		#region Report class constructors
		
        //internal Report(int p_iClientId)
        //{
        //    m_iClientId = p_iClientId;
        //    m_arrlstValues=new ArrayList();
        //    m_arrlstFields=new ArrayList();
        //}
        /// <summary>
        /// Default constructor to create 3500A report
        /// </summary>
        internal Report(Manager.FdaReportType p_eReportType, string p_sConnString, int p_iClientId)
		{
            m_iClientId = p_iClientId;
            m_sConnString = p_sConnString;
			m_arrlstValues=new ArrayList();
			m_arrlstFields=new ArrayList();
			eReportType=p_eReportType;
		}
		#endregion
		
		#region Properties
		/// <summary>
		/// FDF header information
		/// </summary>
		private string Header
		{
			get
			{
				return PDFHEADER;
			}
		}
		/// <summary>
		/// Report name
		/// </summary>
		internal string ReportName
		{
			get
			{
				string sReportName="";

                if (eReportType == Manager.FdaReportType.FDA3500A)
                {
                    sReportName = RMConfigurationManager.GetNameValueSectionSettings("MedWatch", m_sConnString, m_iClientId)["MedWatch_3500A_ReportName"];
                }
                else
                {
                    sReportName = RMConfigurationManager.GetNameValueSectionSettings("MedWatch", m_sConnString, m_iClientId)["MedWatch_3500_ReportName"];
                }
				
				return sReportName;
			}
		}
		/// <summary>
		/// FDF footer information
		/// </summary>
		private string Footer
		{
			get
			{
				return "]" + Constants.VBCrLf +
					"/F (" + GetPdfURL() + ")" + Constants.VBCrLf +
					">>" + Constants.VBCrLf +
					">>" + Constants.VBCrLf +
					"endobj" + Constants.VBCrLf +
					"trailer" + Constants.VBCrLf +
					"<</Root 1 0 R>>" +
					Constants.VBCrLf +
					"%%EOF";
			}
		}
		#endregion

		#region Functions
		/// Name		: Add
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds a field and its value to the report
		/// </summary>
		/// <param name="p_sValue">Value</param>
		/// <param name="p_sFieldName">Field name</param>
		internal void Add(string p_sValue,string p_sFieldName)
		{
			m_arrlstFields.Add(p_sFieldName);
			m_arrlstValues.Add(p_sValue);
		}
		/// Name		: GetItem
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns field value based on passed index
		/// </summary>
		/// <param name="p_iIndex">Index</param>
		/// <returns></returns>
		private string GetItem(int p_iIndex)
		{
			return "<</T ("+Riskmaster.Common.Conversion.ConvertObjToStr(m_arrlstFields[p_iIndex])+") /V ("
				+Riskmaster.Common.Conversion.ConvertObjToStr(m_arrlstValues[p_iIndex])+")>>";
		}
		/// Name		: GetFdfValue
		/// Author		: Parag Sarin
		/// Date Created: 12/27/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the FDF string for report file generation
		/// </summary>
		internal string GetFdfFileString()
		{
			StringBuilder sbValue=null;
			string sReturnValue="";
			try
			{
				sbValue=new StringBuilder();
				for (int i=0;i<m_arrlstFields.Count;i++)
				{
					sbValue.Append(GetItem(i));
				}
				sReturnValue=this.Header + Constants.VBCrLf + 
					Riskmaster.Common.Conversion.ConvertObjToStr(sbValue) + 
					Constants.VBCrLf + this.Footer;

			}

			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("Report.GetFdfFileString.Error", m_iClientId), p_objException);
			}
			finally
			{
				sbValue=null;
			}
			return sReturnValue;
		}
		/// <summary>
		///  URL of the pdf report
		/// </summary>
		/// <returns>Pdf report URL</returns>
		private string GetPdfURL()
		{
            string sPdfValue = string.Empty;
			
			if (eReportType==Manager.FdaReportType.FDA3500A)
			{
                sPdfValue = RMConfigurationManager.GetNameValueSectionSettings("MedWatch", m_sConnString, m_iClientId)["MedWatch_PDF_3500A_URL"];
			}
			else
			{
                sPdfValue = RMConfigurationManager.GetNameValueSectionSettings("MedWatch", m_sConnString, m_iClientId)["MedWatch_PDF_3500_URL"];
			}
			
			return sPdfValue;
			
		}
		#endregion
		
	}
}
