﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Collections;
namespace Riskmaster.Application.RMUtilities
{
    public class HistorySetUp
    {
        const bool ENABLE_HISTORY = true;
        private string m_sConnectionString = string.Empty;
        private string m_sUserName = string.Empty;
        private int m_iClientId = 0; //psharma206	jira 102

        public HistorySetUp(string p_sConnectionstring, int p_iClientId) 
        {
            m_sConnectionString = p_sConnectionstring;
            m_iClientId = p_iClientId;
           
            
        }

        public HistorySetUp(string p_sLoginName, string p_sConnectionstring, int p_iClientId) 
        {
            m_sUserName = p_sLoginName;
            m_sConnectionString = p_sConnectionstring;
           m_iClientId = p_iClientId;
        }
        

        /// <summary>
        /// This function will update the status of history tracking on Database
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public bool UpdateHistoryFlags(XmlDocument p_objXmlDocument) 
        {
            XmlNode objXmlNode = null;            
            string sHistTrackConnStr = string.Empty;
            SysSettings objSysSettings = null;           
            string sSql=string.Empty ;             

            try
            {
               
                // HIST_TRACK_ENABLE flag =0 => Tracking is disable else enable
                objXmlNode = p_objXmlDocument.SelectSingleNode("//HistoryEnableFlag");
                if (objXmlNode != null)
                {
                    // Reading Connection String Config in case of enabling tracking on DB
                    if (objXmlNode.InnerText == ENABLE_HISTORY.ToString())
                    {
                        try
                        {
                            sHistTrackConnStr = RMConfigurationManager.GetConnectionString("HistoryDataSource",m_iClientId);//psharma206	jira 102
                            using (DbConnection objConn = DbFactory.GetDbConnection(sHistTrackConnStr))
                            {
                                objConn.Open();
                                objConn.Close();
                            }
                        }
                        catch (Exception p_objEx)
                        {
                            throw new RMAppException(Globalization.GetString("HistorySetUp.UpdateHistoryFlags.ConnectionStringNotPresent", m_iClientId), p_objEx);//psharma206	jira 102
                        }

                        //Selecting Columns which are required for reports and also setting enabled flag of corresponding tables
                        SaveReportRequiredFields();
                    }
                    else
                    {                        
                        //Setting Updated flag of already enabled tables so that history tracking executable should consider them
                        sSql = "UPDATE HIST_TRACK_TABLES SET UPDATED_FLAG = -1 WHERE ENABLED_FLAG = -1";

                        using (DbConnection objConn = DbFactory.GetDbConnection(m_sConnectionString))
                        {
                            objConn.Open();
                            objConn.ExecuteNonQuery(sSql);
                            objConn.Close();
                        } 
                    }
                    objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);//psharma206	jira 102
                    objSysSettings.HistTrackEnable = Convert.ToBoolean(objXmlNode.InnerText);
                }              
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistorySetUp.UpdateHistoryFlags.Error", m_iClientId), p_objEx);//psharma206	jira 102
            }
            finally
            {                
                objSysSettings = null;                
            }
            return true; 
        }


        /// <summary>
        /// Saving Columns which are defined as mandatory for Reports
        /// Setting enabled flag of corresponding Tables
        /// </summary>
        /// <returns></returns>
        private void SaveReportRequiredFields()
        {
            string sSql = string.Empty;
            StringBuilder sbColumns = null;
            LocalCache objCache = null;
            XmlNodeList objTableList = null;
            ArrayList arrColumns = null;
            StringBuilder sbTables = null;
            int iTableId = 0;
           
            try
            {
                

                arrColumns = new ArrayList();
                sbTables = new StringBuilder();
                sbColumns = new StringBuilder();

                objCache = new LocalCache(m_sConnectionString, m_iClientId);

                //Retreiving XML from Cache
                XmlDocument objDoc = objCache.GetXmlForHistTrack();

                if (objDoc != null)
                {
                    objTableList = objDoc.SelectNodes("//Table");
                    if (objTableList != null)
                    {
                        foreach (XmlNode objTable in objTableList)
                        {
                            foreach (XmlNode objColumn in objTable)
                            {
                                if (sbColumns.Length != 0)
                                {
                                    sbColumns.Append(",");
                                }
                                sbColumns.Append(Utilities.FormatSqlFieldValue(objColumn.Attributes["name"].Value));
                            }

                            sSql=string.Format(@"SELECT HT.TABLE_ID,HD.COLUMN_ID FROM HIST_TRACK_TABLES HT 
                                                 INNER JOIN HIST_TRACK_DICTIONARY HD 
                                                 ON HT.TABLE_ID = HD.TABLE_ID 
                                                 WHERE HT.TABLE_NAME ={0} AND HD.COLUMN_NAME IN ({1}) ", 
                                                 Utilities.FormatSqlFieldValue(objTable.Attributes["name"].Value), sbColumns.ToString());
                            sbColumns.Length = 0;
                            using (DbReader objReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                            {
                                while (objReader.Read())
                                {
                                    iTableId = objReader.GetInt32("TABLE_ID");
                                    arrColumns.Add(objReader.GetInt32("COLUMN_ID").ToString());
                                }
                            }
                            if (sbTables.Length != 0)
                            {
                                sbTables.Append(",");
                            }
                            sbTables.Append(iTableId);
                        }

                        if (sbTables.Length != 0)
                        {
                            HistoryDesigner objHD = new HistoryDesigner(m_sUserName, m_sConnectionString, m_iClientId);
                            if (objHD != null)
                            {
                                objHD.UpdateTables(sbTables.ToString(), "-1");
                                objHD.AddOrRemovePrimaryColumns(sbTables.ToString(), "Add");
                                if (arrColumns.Count != 0)
                                {
                                    objHD.AddColumns(arrColumns);
                                }
                            }
                        }
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistorySetUp.SaveReportRequiredFields.Error", m_iClientId), p_objEx);//psharma206	jira 102
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }

        /// <summary>
        /// This function gets the history tracking status flags from databse
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <returns></returns>
        public bool GetHistoryFlag(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut)
        {
          
            XmlNode objXmlNode = null;
            string sSQL = string.Empty;
            SysSettings objSysSettings=null ;
            string sParamValue=string.Empty ;
            try
            {
                objSysSettings = new SysSettings(m_sConnectionString, m_iClientId);    

                objXmlNode = p_objXmlIn.SelectSingleNode("//HistoryEnableFlag");
                if (objXmlNode != null)
                {
                    objXmlNode.InnerText = objSysSettings.HistTrackEnable.ToString();                                                           
                }                                                    
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HistorySetUp.GetHistoryFlag.Error", m_iClientId), p_objEx);      //psharma206	jira 102          
            }
            finally
            {
                                
                    objSysSettings = null;
                
            }
            p_objXmlOut = p_objXmlIn;
            return true;
        }       
    }
}
