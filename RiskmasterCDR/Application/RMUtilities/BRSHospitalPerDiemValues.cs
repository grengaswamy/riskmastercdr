using System;
using System.Xml;
using System.Data; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Umesh p Kusvaha
    ///Dated   :   4th,Oct 2007
    ///Purpose :   Shows the Per Diem's Value form
    /// </summary>
    public class BRSHospitalPerDiemValue : UtilitiesUIBase
    {
        /// <summary>
        /// Private string array variable for database field mapping
        /// </summary>
        private string[,] arrFields = {
			{"TableId","TABLE_ID"},
			{"ACPSurgicalStay","AC_SUR_STAY"},
			{"ACPNonSurgicalStay","AC_NONSUR_STAY"},
			{"TCSurgicalStay","TC_SUR_STAY"},
			{"TCNonSurgicalStay","TC_NONSUR_STAY"},
			{"StopLossAmt","STOP_LOSS_AMT"},
			{"StopLossPer", "STOP_LOSS_PER"},
			{"Rehab", "REHAB_PHYCH_PER"},
            {"HospCarePer", "OP_HOSP_PER"},
            {"SchedSurgPer", "OP_SCH_SURG_PER"},
            {"BillFeeSch", "AS_GREATER_PER"},
            {"BilledlessFeeSch", "AS_LESSEQUAL_PER"},
            {"NotInFeeSch", "AS_NOTINSCHED_PER"},
            {"FeeSche", "FL_FEE_SCHED"}
                                        };

        private int m_iClientId = 0;
        /// <summary>
        /// Default constructor with connection string
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>
        public BRSHospitalPerDiemValue(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            m_iClientId = p_iClientId;
            ConnectString = p_sConnString;
            this.Initialize();
        }

        
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            TableName = "WRCMP_PERDIEM_OPT";
            KeyField = "TABLE_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        

        

        

        /// <summary>
        /// Get the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            
            XmlNode objNode = null;
            XmlElement objNewElm = null;
            DbReader objRdr = null;
            string sSQL = "";
            LocalCache objCache = null;
            try
            {
                XMLDoc = p_objXmlDocument;
                //objNode = p_objXmlDocument.SelectSingleNode("//tableid");
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
                string sTableId = objNode.InnerText;
                base.WhereClause = "TABLE_ID =" + sTableId;
                base.Get();

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CalcTypeCode']");
                int iCalcType = Conversion.ConvertStrToInteger(objNode.InnerText);
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                int iStateRowId = objCache.GetStateRowID("FL");

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='FeeSche']");
                sSQL = "SELECT USER_TABLE_NAME,TABLE_ID FROM FEE_TABLES WHERE CALC_TYPE = 13  AND STATE= " + iStateRowId + " ORDER BY USER_TABLE_NAME";
                objRdr = DbFactory.GetDbReader(base.ConnectString, sSQL);
                while (objRdr.Read())
                {
                    objNewElm = XMLDoc.CreateElement("option");
                    objNewElm.SetAttribute("value", objRdr.GetInt("TABLE_ID").ToString());
                    objNewElm.InnerText = objRdr.GetString("USER_TABLE_NAME");
                    objNode.AppendChild(objNewElm);
                }
                objRdr.Dispose();

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BRSHospitalPerDiemValue.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Close();
                    objRdr.Dispose();
                }

                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }


        /// <summary>
        /// Save the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            int iTableId = 0;
            int iFeeSche = 0;
            double dACPSurgicalStay = 0;
            double dACPNonSurgicalStay = 0;
            double dTCSurgicalStay = 0;
            double dTCNonSurgicalStay = 0;
            double dStopLossAmt = 0;
            double dStopLossPer = 0;
            double dRehab = 0;
            double dHospCarePer = 0;
            double dSchedSurgPer = 0;
            double dBillFeeSch = 0;
            double dBilledlessFeeSch = 0;
            double dNotInFeeSch = 0;
         
            DbConnection objCon = null;
            DbTransaction objTran = null;
            DbCommand objCommand = null;
            XmlElement objElm = null;
            XmlNode objNode = null;
            DbReader objRdr = null;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
                if (objNode != null)
                    iTableId = Conversion.ConvertStrToInteger(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ACPSurgicalStay']");
                if (objNode != null)
                    dACPSurgicalStay = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ACPNonSurgicalStay']");
                if (objNode != null)
                    dACPNonSurgicalStay = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TCSurgicalStay']");
                if (objNode != null)
                    dTCSurgicalStay = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TCNonSurgicalStay']");
                if (objNode != null)
                    dTCNonSurgicalStay = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='StopLossAmt']");
                if (objNode != null)
                    dStopLossAmt = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='StopLossPer']");
                if (objNode != null)
                    dStopLossPer = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Rehab']");
                if (objNode != null)
                    dRehab = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='HospCarePer']");
                if (objNode != null)
                    dHospCarePer = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SchedSurgPer']");
                if (objNode != null)
                    dSchedSurgPer = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BillFeeSch']");
                if (objNode != null)
                    dBillFeeSch = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='BilledlessFeeSch']");
                if (objNode != null)
                    dBilledlessFeeSch = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='NotInFeeSch']");
                if (objNode != null)
                    dNotInFeeSch = Conversion.ConvertStrToDouble(objNode.InnerText);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='FeeSche']");
                if (objNode != null)
                    iFeeSche = Conversion.ConvertStrToInteger(((XmlElement)objNode).GetAttribute("value"));
                

                

                objCon = DbFactory.GetDbConnection(ConnectString);
                objCon.Open();
                objCommand = objCon.CreateCommand();
                objTran = objCon.BeginTransaction();
                objCommand.Transaction = objTran;
                objCommand.CommandType = CommandType.Text;
                objCommand.CommandText = "DELETE FROM WRCMP_PERDIEM_OPT WHERE TABLE_ID=" + iTableId;
                objCommand.ExecuteNonQuery();
                objCommand.CommandText = "INSERT INTO WRCMP_PERDIEM_OPT(TABLE_ID,AC_SUR_STAY,AC_NONSUR_STAY,TC_SUR_STAY,"+
                                         "TC_NONSUR_STAY,STOP_LOSS_AMT,STOP_LOSS_PER,REHAB_PHYCH_PER,OP_HOSP_PER,OP_SCH_SURG_PER,"+
                                         "AS_GREATER_PER,AS_LESSEQUAL_PER,AS_NOTINSCHED_PER,FL_FEE_SCHED)"+
                                         "VALUES(" + iTableId +"," + dACPSurgicalStay + "," + dACPNonSurgicalStay + "," +
                                         dTCSurgicalStay + "," + dTCNonSurgicalStay + "," + dStopLossAmt + "," + dStopLossPer + "," +
                                         dRehab + "," + dHospCarePer + "," + dSchedSurgPer + "," + dBillFeeSch + "," + dBilledlessFeeSch +
                                         "," + dNotInFeeSch + "," + iFeeSche + ")";
                objCommand.ExecuteNonQuery();
                objTran.Commit();
                objCon.Dispose();
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                if (objTran != null)
                    objTran.Rollback();
                throw new RMAppException(Globalization.GetString("BRSHospitalPerDiemValue.Save.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objTran != null)
                {
                    objTran.Dispose();
                    objTran = null;
                }
                if (objCon != null)
                {
                    objCon.Dispose();
                    objCon = null;
                }
                objCommand = null;
                objNode = null;
                if (objRdr != null)
                {
                    objRdr.Close();
                    objRdr.Dispose();
                    objRdr = null;
                }
            }
        }
    }
}