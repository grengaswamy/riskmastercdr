﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Fiscal information.
	/// </summary>
	public class FiscalCopy
	{
		#region Input/Output Xml
		/*
		 <FiscalCopy>
			<FromLineOfBusiness text="General Claims" value="241">General Claims</FromLineOfBusiness>
			<FromOrganization text="" value="2484">department2</FromOrganization>
			<ToLineOfBusiness text="" value="241">GC General Claims</ToLineOfBusiness>
			<ToOrganization text="Entire Organization" value="0" />
			<FiscalYear>2009</FiscalYear>
			<ToFiscalYear>2010</FiscalYear>
			<OptionsYears>
			<OptOnlyFiscalYear>All Years</OptOnlyFiscalYear>
			<OptOnlyFiscalYear>Only 2009</OptOnlyFiscalYear>
			</OptionsYears>
		</FiscalCopy>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
        private int iLangCode = 1033;
        private int m_iClientId = 0; //sonali-cloud
		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
        public int LanguageCode
        {
            set
            {
                iLangCode = value;
            }
        }
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnectString">Connection string</param>
		public FiscalCopy(string p_sConnectString, int p_iClientId)//sonali
		{
			m_sDSN=p_sConnectString;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/*
		<FiscalCopy>
			<ToLineOfBusiness value="241" />
			<ToOrganization value="0" text="Parent of entire Organization"/>
			<FromLineOfBusiness value="241" text="General Claims"/>
			<FromOrganization value="0" text=""/>
			<FiscalYear>2009</FiscalYear>
			<ToFiscalYear>2009</ToFiscalYear>
		</FiscalCopy>  
		*/
		/// <summary>
		/// Gets the Data to populate screen
		/// </summary>
		/// <param name="p_objDOC"></param>
		/// <returns>Xml Containing data to populate screen</returns>
		public XmlDocument Get(XmlDocument p_objDOC)
		{
			XmlNode objNode=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			LocalCache objCache=null;
			string sOrgId="";
			string sLobId="";
			string sFiscalYear="";
			int iFiscalYear=0;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("FiscalCopy");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("FromLineOfBusiness");
				objNode=p_objDOC.SelectSingleNode("//FromLineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.Attributes["text"]!=null)
					{
						objElemChild.SetAttribute("text",objNode.Attributes["text"].Value);	
					}
					else
					{
						objElemChild.SetAttribute("text","");
					}
					if (objNode.Attributes["value"]!=null)
					{
						objElemChild.SetAttribute("value",objNode.Attributes["value"].Value);	
						sLobId=objNode.Attributes["value"].Value;
					}
					else
					{
						objElemChild.SetAttribute("value","");
					}
					if (!sLobId.Trim().Equals("0") && !sLobId.Trim().Equals(""))
					{
                        objCache = new LocalCache(m_sDSN, m_iClientId);
                        objElemChild.InnerText = objCache.GetCodeDesc(Conversion.ConvertObjToInt(sLobId, m_iClientId));
						objCache.Dispose();
					}
					else
					{
                        objElemChild.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalCopy.Get.ParentAllLOB",m_iClientId), this.iLangCode.ToString());
					}
					
				}
				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("FromOrganization");
				objNode=p_objDOC.SelectSingleNode("//FromOrganization");
				if (objNode!=null)
				{
					if (objNode.Attributes["text"]!=null)
					{
						objElemChild.SetAttribute("text",objNode.Attributes["text"].Value);	
					}
					else
					{
						objElemChild.SetAttribute("text","");
					}
					if (objNode.Attributes["value"]!=null)
					{
						objElemChild.SetAttribute("value",objNode.Attributes["value"].Value);	
						sOrgId=objNode.Attributes["value"].Value;
					}
					else
					{
						objElemChild.SetAttribute("value","");
					}
					if (!sOrgId.Trim().Equals("0") && !sOrgId.Trim().Equals(""))
					{
                        objElemChild.InnerText = UTILITY.GetOrg(sOrgId, m_sDSN, m_iClientId).Split(new char[] { ' ' })[1];
					}
					else
					{
                        objElemChild.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalCopy.Get.ParentOrg", m_iClientId), this.iLangCode.ToString()); //sonali -cloud
					}
					
				}
					
				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("ToLineOfBusiness");
				objNode=p_objDOC.SelectSingleNode("//ToLineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.Attributes["text"]!=null)
					{
						objElemChild.SetAttribute("text",objNode.Attributes["text"].Value);	
					}
					else
					{
						objElemChild.SetAttribute("text","");
					}
					if (objNode.Attributes["value"]!=null)
					{
						objElemChild.SetAttribute("value",objNode.Attributes["value"].Value);	
						sLobId=objNode.Attributes["value"].Value;
					}
					else
					{
						objElemChild.SetAttribute("value","");
					}
					if (!sLobId.Trim().Equals("0") && !sLobId.Trim().Equals(""))
					{
						objElemChild.InnerText=UTILITY.GetCode(sLobId,m_sDSN,m_iClientId);
					}
					else
					{
                        objElemChild.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.Get.AllLOB", m_iClientId), this.iLangCode.ToString());//sonali-cloud
					}
				}
				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("ToOrganization");
				objNode=p_objDOC.SelectSingleNode("//ToOrganization");
				if (objNode!=null)
				{
					if (objNode.Attributes["text"]!=null)
					{
						objElemChild.SetAttribute("text",objNode.Attributes["text"].Value);	
					}
					else
					{
						objElemChild.SetAttribute("text","");
					}
					if (objNode.Attributes["value"]!=null)
					{
						objElemChild.SetAttribute("value",objNode.Attributes["value"].Value);	
						sOrgId=objNode.Attributes["value"].Value;
					}
					else
					{
						objElemChild.SetAttribute("value","");
					}
					if (!sOrgId.Trim().Equals("0") && !sOrgId.Trim().Equals(""))
					{
                        objElemChild.InnerText = UTILITY.GetOrg(sOrgId, m_sDSN, m_iClientId);
					}
					else
					{
						objElemChild.InnerText=CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalYearsList.Get.AllORG", m_iClientId),this.iLangCode.ToString());//sonali-cloud
					}
				}
				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("FiscalYear");
				objNode=p_objDOC.SelectSingleNode("//FiscalYear");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						objElemChild.InnerText=objNode.InnerText;
						sFiscalYear=objNode.InnerText;
					}
				}
				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("ToFiscalYear");
				if (sFiscalYear.Trim()!="")
				{
					iFiscalYear=Conversion.ConvertStrToInteger(sFiscalYear)+1;
				}
				objElemChild.InnerText=iFiscalYear.ToString();
				objDOM.FirstChild.AppendChild(objElemChild);
				objElemChild=objDOM.CreateElement("OptionsYears");
				objNode=objDOM.CreateElement("OptAllYears");
				objNode.InnerText="All Years";
				objElemChild.AppendChild(objNode);
				objNode=objDOM.CreateElement("OptOnlyFiscalYear");
                objNode.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetString("FiscalCopy.Get.Only", m_iClientId), this.iLangCode.ToString()) + " " + objNode.InnerText;//sonali-cloud
				objElemChild.AppendChild(objNode);
				objDOM.FirstChild.AppendChild(objElemChild);
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FiscalCopy.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
                if (objCache != null)
                    objCache.Dispose();
                objNode = null;
				objDOM=null;
				objElemParent=null;
				objElemChild=null;
			}
			

			
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
        /*
		<FiscalCopy>
			<ToLineOfBusiness value="241"/>
			<ToOrganization value="0"/>
			<FromLineOfBusiness value="241"/>
			<FromOrganization value="0"/>
			<FiscalYear>2009</FiscalYear>
			<ToFiscalYear>2009</ToFiscalYear>
		</FiscalCopy>  
		*/
		/// <summary>
		/// Save the Fiscal Copy data
		/// </summary>
		/// <param name="p_objDOC">Fiscal Data to be Saved</param>
		/// <param name="p_sError">Error while updating data</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objDOC,ref string p_sError)
		{
			StringBuilder sbSQL=null; 
			int iCnt=0; 
			string sFiscalYr=""; 
			string sdtFYStart=""; 
			string sdtFYEnd=""; 
			ArrayList alSQL=null;
			string sdtQ1Start=""; 
			string sdtQ1End=""; 
			string sdtQ2Start=""; 
			string sdtQ2End=""; 
			string sdtQ3Start=""; 
			string sdtQ3End=""; 
			string sdtQ4Start=""; 
			string sdtQ4End=""; 
			long lPeriods=0; 
			long lWeekDay=0; 
			long lFYDif=0; 
			long lTextFY=0;
			//Periods
			long lPeriodNo=0;	
			string sdtPerStart="";
			string sdtPerEnd="";
			string sLobCode="";
			string sOrgId="";
			string sFiscalYear="";
			string sToFiscalYear="";
			string sLobCodeValue="";
			string sOrgCodeValue="";
			long lFiscalYear=0;
			DbReader objReader=null;
			XmlNode objNode=null;
			bool bReturnValue=false;
			try
			{
				bReturnValue=ValidateOptions(p_objDOC,ref p_sError);
				if (bReturnValue==false)
				{
					return bReturnValue;
				}
				sbSQL=new StringBuilder();
				objNode=p_objDOC.SelectSingleNode(".//ToLineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sLobCode=objNode.Attributes["value"].Value;
				}
				sLobCode=sLobCode.Trim();
				objNode=p_objDOC.SelectSingleNode(".//ToOrganization");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sOrgId=objNode.Attributes["value"].Value;
				}
				sOrgId=sOrgId.Trim();
				objNode=p_objDOC.SelectSingleNode(".//ToFiscalYear");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					sToFiscalYear=objNode.InnerText;
				}
				sToFiscalYear=sToFiscalYear.Trim();
				objNode=p_objDOC.SelectSingleNode(".//FiscalYear");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sFiscalYear=objNode.InnerText;
						lFiscalYear=Conversion.ConvertStrToLong(objNode.InnerText.Trim());
					}
				}
				objNode=p_objDOC.SelectSingleNode(".//FromLineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sLobCodeValue=objNode.Attributes["value"].Value;
					
				}
				sLobCodeValue=sLobCodeValue.Trim();
				objNode=p_objDOC.SelectSingleNode(".//FromOrganization");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sOrgCodeValue=objNode.Attributes["value"].Value;
				}
				sOrgCodeValue=sOrgCodeValue.Trim();
				
				//-- Fiscal Year --
				sbSQL.Append("SELECT FISCAL_YEAR, FY_START_DATE, FY_END_DATE,");
				sbSQL.Append(" QTR1_START_DATE, QTR1_END_DATE, QTR2_START_DATE, QTR2_END_DATE,");
				sbSQL.Append(" QTR3_START_DATE, QTR3_END_DATE, QTR4_START_DATE, QTR4_END_DATE,");
				sbSQL.Append("  NUM_PERIODS, WEEK_END_DAY");
				sbSQL.Append(" FROM FISCAL_YEAR");
				if (!sLobCodeValue.Equals("0") && !sLobCodeValue.Equals(""))
				{
					sbSQL.Append("  WHERE LINE_OF_BUS_CODE="+sLobCodeValue);
				}
				else
				{
					sbSQL.Append("  WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)");
				}
				if (!sOrgCodeValue.Equals("0")&& !sOrgCodeValue.Equals(""))
				{
					sbSQL.Append("  AND ORG_EID ="+sOrgCodeValue);
				}
				else
				{
					sbSQL.Append("  AND (ORG_EID = 0 OR ORG_EID IS NULL)");
				}
				if (!sToFiscalYear.Equals(""))
				{
					sbSQL.Append("   AND FISCAL_YEAR = '"+sFiscalYear+"'");
				}
				objReader=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				if (!sToFiscalYear.Equals(""))
				{
					lTextFY=Conversion.ConvertStrToLong(sToFiscalYear);
					if ((lTextFY!=lFiscalYear)||(lTextFY!=0)||(lFiscalYear!=0))
					{
						lFYDif = lTextFY - lFiscalYear;
					}
				}
				alSQL=new ArrayList();
				while(objReader.Read())
				{
					iCnt++;
					sdtFYStart=Conversion.ConvertObjToStr(objReader.GetValue("FY_START_DATE"));	
					sdtFYEnd=Conversion.ConvertObjToStr(objReader.GetValue("FY_END_DATE"));	
					sdtQ1Start=Conversion.ConvertObjToStr(objReader.GetValue("QTR1_START_DATE"));
					sdtQ1End=Conversion.ConvertObjToStr(objReader.GetValue("QTR1_END_DATE"));
					sdtQ2Start=Conversion.ConvertObjToStr(objReader.GetValue("QTR2_START_DATE"));
					sdtQ2End=Conversion.ConvertObjToStr(objReader.GetValue("QTR2_END_DATE"));
					sdtQ3Start=Conversion.ConvertObjToStr(objReader.GetValue("QTR3_START_DATE"));
					sdtQ3End=Conversion.ConvertObjToStr(objReader.GetValue("QTR3_END_DATE"));
					sdtQ4Start=Conversion.ConvertObjToStr(objReader.GetValue("QTR4_START_DATE"));
					sdtQ4End=Conversion.ConvertObjToStr(objReader.GetValue("QTR4_END_DATE"));
                    lPeriods = Conversion.ConvertObjToInt64(objReader.GetValue("NUM_PERIODS"), m_iClientId);
                    lWeekDay = Conversion.ConvertObjToInt64(objReader.GetValue("WEEK_END_DAY"), m_iClientId);
					if (!sToFiscalYear.Equals(""))
					{
						sFiscalYr=sToFiscalYear;
						//recognize year increase (decrease)
                        //rsharma220 MITS 32551 Start
                        if (!string.IsNullOrEmpty(sdtFYStart))
                        sdtFYStart=Convert.ToString(Conversion.ConvertStrToLong(sdtFYStart.Substring(0,4))+lFYDif)+sdtFYStart.Substring(4);
                        if (!string.IsNullOrEmpty(sdtFYEnd))
                        sdtFYEnd=Convert.ToString(Conversion.ConvertStrToLong(sdtFYEnd.Substring(0,4))+lFYDif)+sdtFYEnd.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ1Start))
                        sdtQ1Start=Convert.ToString(Conversion.ConvertStrToLong(sdtQ1Start.Substring(0,4))+lFYDif)+sdtQ1Start.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ1End))
                        sdtQ1End=Convert.ToString(Conversion.ConvertStrToLong(sdtQ1End.Substring(0,4))+lFYDif)+sdtQ1End.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ2Start))
                        sdtQ2Start=Convert.ToString(Conversion.ConvertStrToLong(sdtQ2Start.Substring(0,4))+lFYDif)+sdtQ2Start.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ2End))
                        sdtQ2End=Convert.ToString(Conversion.ConvertStrToLong(sdtQ2End.Substring(0,4))+lFYDif)+sdtQ2End.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ3Start))
                        sdtQ3Start=Convert.ToString(Conversion.ConvertStrToLong(sdtQ3Start.Substring(0,4))+lFYDif)+sdtQ3Start.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ3End))
                        sdtQ3End=Convert.ToString(Conversion.ConvertStrToLong(sdtQ3End.Substring(0,4))+lFYDif)+sdtQ3End.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ4Start))
                        sdtQ4Start=Convert.ToString(Conversion.ConvertStrToLong(sdtQ4Start.Substring(0,4))+lFYDif)+sdtQ4Start.Substring(4);
                        if (!string.IsNullOrEmpty(sdtQ4End))
                        sdtQ4End=Convert.ToString(Conversion.ConvertStrToLong(sdtQ4End.Substring(0,4))+lFYDif)+sdtQ4End.Substring(4);
                        //rsharma220 MITS 32551 End
                    }
					else
					{
						sFiscalYr =Conversion.ConvertObjToStr(objReader.GetValue("FISCAL_YEAR"));
					}
					sbSQL=new StringBuilder();
					sbSQL.Append("INSERT INTO FISCAL_YEAR (FISCAL_YEAR, FY_START_DATE, FY_END_DATE,");
					sbSQL.Append(" QTR1_START_DATE, QTR1_END_DATE, QTR2_START_DATE, QTR2_END_DATE,");
					sbSQL.Append(" QTR3_START_DATE, QTR3_END_DATE, QTR4_START_DATE, QTR4_END_DATE,");
					sbSQL.Append("  NUM_PERIODS, WEEK_END_DAY, LINE_OF_BUS_CODE, ORG_EID)");
			
					sbSQL.Append(" VALUES ('"+ sFiscalYr + "', '" + sdtFYStart + "', '" + sdtFYEnd + "', '");
					sbSQL.Append( sdtQ1Start + "', '" + sdtQ1End + "', '" + sdtQ2Start + "', '" + sdtQ2End + "', '");
					sbSQL.Append( sdtQ3Start + "', '" + sdtQ3End + "', '" + sdtQ4Start + "', '" + sdtQ4End + "',");
					sbSQL.Append( lPeriods.ToString() + "," + lWeekDay.ToString() + ", " + sLobCode+ ", " + sOrgId + ")");

					
					alSQL.Add(sbSQL.ToString());
				}
				if (objReader!=null)
				{
					objReader.Close();
				}
				if (iCnt<1)
				{
                    UTILITY.Save(alSQL, m_sDSN, m_iClientId);
					return true;
				}
				sbSQL=new StringBuilder();
				sbSQL.Append("SELECT FISCAL_YEAR, PERIOD_NUMBER, PERIOD_START_DATE, PERIOD_END_DATE,");
				sbSQL.Append(" LINE_OF_BUS_CODE, ORG_EID");
				sbSQL.Append(" FROM FISCAL_YEAR_PERIOD");	
				if (!sLobCodeValue.Equals("0") && !sLobCodeValue.Equals(""))
				{
					sbSQL.Append("  WHERE LINE_OF_BUS_CODE="+sLobCodeValue);
				}
				else
				{
					sbSQL.Append("  WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)");
				}
				if (!sOrgCodeValue.Equals("0") && !sOrgCodeValue.Equals(""))
				{
					sbSQL.Append("  AND ORG_EID ="+sOrgCodeValue);
				}
				else
				{
					sbSQL.Append("  AND (ORG_EID = 0 OR ORG_EID IS NULL)");
				}
				if (!sToFiscalYear.Equals(""))
				{
					sbSQL.Append("   AND FISCAL_YEAR = '"+sFiscalYear+"'");
				}
				objReader=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
					
				while(objReader.Read())
				{
                    lPeriodNo = Conversion.ConvertObjToInt64(objReader.GetValue("PERIOD_NUMBER"), m_iClientId);
					sdtPerStart=Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_START_DATE"));
					sdtPerEnd=Conversion.ConvertObjToStr(objReader.GetValue("PERIOD_END_DATE"));
					if (!sToFiscalYear.Equals(""))
					{
						sFiscalYr = sToFiscalYear;
						sdtPerStart=Convert.ToString(Conversion.ConvertStrToLong(sdtPerStart.Substring(0,4))+lFYDif)+sdtPerStart.Substring(4);
						sdtPerEnd=Convert.ToString(Conversion.ConvertStrToLong(sdtPerEnd.Substring(0,4))+lFYDif)+sdtPerEnd.Substring(4);
					}
					else
					{
						sFiscalYr =Conversion.ConvertObjToStr(objReader.GetValue("FISCAL_YEAR"));	
					}
					sbSQL=new StringBuilder();
					sbSQL.Append("INSERT INTO FISCAL_YEAR_PERIOD(FISCAL_YEAR, PERIOD_NUMBER, PERIOD_START_DATE,");
					sbSQL.Append(" PERIOD_END_DATE, LINE_OF_BUS_CODE, ORG_EID)");
					sbSQL.Append(" VALUES ('"+sFiscalYr+ "', " + lPeriodNo + ",'" + sdtPerStart + "', '");
					sbSQL.Append(sdtPerEnd +"', " +  sLobCode + ", " + sOrgId + ")");

					alSQL.Add(sbSQL.ToString());
				}
				if (objReader!=null)
				{
					objReader.Close();
				}
                UTILITY.Save(alSQL, m_sDSN, m_iClientId);
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FiscalCopy.Save.Error", m_iClientId),p_objEx); //sonali-cloud
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Close();
				}
				sbSQL=null; 
				alSQL=null;
				objReader=null;
				objNode=null;
			}
		}
		#endregion

		#region Private Functions
		/// <summary>
		/// Validates the Fiscal Data
		/// </summary>
		/// <param name="p_objDOC">Data to validate</param>
		/// <param name="p_sError">Error while validating data</param>
		/// <returns>True/False</returns>
		private bool ValidateOptions(XmlDocument p_objDOC,ref string p_sError)
		{
			StringBuilder sbSQL=null;
			string sDateAdd="";
			string sDateSubtract="";
			string sLobCode="";
			string sOrgId="";
			string sToLobCode="";
			string sToOrgId="";
			string sToFiscalYear="";
			string sFiscalYear="";
			long lToFiscalYear=0;
			DbReader objReader=null;
			XmlNode objNode=null;
			bool bReturnValue=false;
			try
			{
				sDateAdd=Convert.ToString(DateTime.Now.Year+100);
				sDateSubtract=Convert.ToString(DateTime.Now.Year-100);
				sbSQL=new StringBuilder();
				objNode=p_objDOC.SelectSingleNode(".//FromLineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sLobCode=objNode.Attributes["value"].Value;
					
				}
				sLobCode=sLobCode.Trim();
				objNode=p_objDOC.SelectSingleNode(".//FiscalYear");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					sFiscalYear=objNode.InnerText;
				}
				sFiscalYear=sFiscalYear.Trim();
				objNode=p_objDOC.SelectSingleNode(".//FromOrganization");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sOrgId=objNode.Attributes["value"].Value;
				}
				sOrgId=sOrgId.Trim();
				objNode=p_objDOC.SelectSingleNode(".//ToLineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sToLobCode=objNode.Attributes["value"].Value;
				}
				sToLobCode=sToLobCode.Trim();
				objNode=p_objDOC.SelectSingleNode(".//ToOrganization");
				if (objNode!=null)
				{
					if (objNode.Attributes["value"]!=null)
						sToOrgId=objNode.Attributes["value"].Value;
				}
				sToOrgId=sToOrgId.Trim();
				objNode=p_objDOC.SelectSingleNode(".//ToFiscalYear");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sToFiscalYear=objNode.InnerText;
						lToFiscalYear=Conversion.ConvertStrToLong(sToFiscalYear);
					}
				}
				
				sToFiscalYear=sToFiscalYear.Trim();
				if (!sToFiscalYear.Trim().Equals(""))
				{
					if ((lToFiscalYear < (DateTime.Now.Year -100)) ||
						(lToFiscalYear > (DateTime.Now.Year +100))
						)
					{
                        p_sError = Globalization.GetString("FiscalCopy.ValidateOptions.YearError", m_iClientId); //sonali-cloud
						p_sError=p_sError.Replace("%%1",sDateSubtract);
						p_sError=p_sError.Replace("%%2",sDateAdd);
					}
				}
				if (sToLobCode.Equals(sLobCode) && sToOrgId.Equals(sOrgId))
				{
					if (sToFiscalYear.Equals(""))
					{
                        p_sError = Globalization.GetString("FiscalCopy.ValidateOptions.SameLOBOrgError", m_iClientId);//sonali-cloud
						return false;
					}
					else if (sFiscalYear.Equals(sToFiscalYear))
					{
                        p_sError = Globalization.GetString("FiscalCopy.ValidateOptions.SameYearLOBOrgError", m_iClientId);//sonali-cloud
						return false;
					}

				}
				sbSQL.Append("SELECT FISCAL_YEAR FROM FISCAL_YEAR");
				if (!sToLobCode.Equals("0") && !sToLobCode.Equals(""))
				{
					sbSQL.Append(" WHERE LINE_OF_BUS_CODE="+sToLobCode);
				}
				else
				{
					sbSQL.Append(" WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)");
				}
				if (!sToOrgId.Equals("0") && !sToOrgId.Equals(""))
				{
					sbSQL.Append(" AND ORG_EID ="+sToOrgId);
				}
				else
				{
					sbSQL.Append(" AND (ORG_EID = 0 OR ORG_EID IS NULL)");
				}
				if (!sToFiscalYear.Equals(""))
				{
					sbSQL.Append("  AND FISCAL_YEAR = '"+sToFiscalYear+"'");
				}
				objReader=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				if(objReader.Read())
				{
                    p_sError = Globalization.GetString("FiscalCopy.ValidateOptions.ExistsFiscalYearError", m_iClientId);//sonali-cloud
					return false;
				}
				if (objReader!=null)
				{
					objReader.Close();
				}
				sbSQL=new StringBuilder();
				sbSQL.Append(" SELECT FISCAL_YEAR FROM FISCAL_YEAR_PERIOD");
				if (!sToLobCode.Equals("0") && !sToLobCode.Equals(""))
				{
					sbSQL.Append(" WHERE LINE_OF_BUS_CODE="+sToLobCode);
				}
				else
				{
					sbSQL.Append(" WHERE (LINE_OF_BUS_CODE = 0 OR LINE_OF_BUS_CODE IS NULL)");
				}
				if (!sToOrgId.Equals("0") && !sToOrgId.Equals(""))
				{
					sbSQL.Append(" AND ORG_EID ="+sToOrgId);
				}
				else
				{
					sbSQL.Append(" AND (ORG_EID = 0 OR ORG_EID IS NULL)");
				}
				if (!sToFiscalYear.Equals(""))
				{
					sbSQL.Append("  AND FISCAL_YEAR = '"+sToFiscalYear+"'");
				}
				objReader=DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				if(objReader.Read())
				{
                    p_sError = Globalization.GetString("FiscalCopy.ValidateOptions.ExistsFiscalYearError", m_iClientId);//sonali-cloud
					return false;
				}
				if (objReader!=null)
				{
					objReader.Close();
				}

			bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FiscalCopy.ValidateOptions.Error", m_iClientId), p_objEx);//sonali-cloud
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Close();
				}
				sbSQL=null; 
				objReader=null;
				objNode=null;
			}
			return bReturnValue;
		}
		#endregion
		
	}
}
