using System;
using System.Xml; 
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   Holidays List Form 
	/// </summary>
	public class Holidays:UtilitiesUIListBase 
	{
		/// <summary>
		/// Private database field mapping string array
		/// </summary>
		private string[,] arrFields = {
			{"Date","HOL_DATE"},
			{"Desc","DESCRIPTION"},
			{"OrgId","ORG_EID"}                                          
									  };
        private int m_iClientId = 0; //sonali
		/// Name		: Holidays
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor with Connection String
		/// </summary>
		/// <param name="p_sConnString">Input Connection String parameter</param>
		public Holidays(string p_sConnString,int p_iClientId):base(p_sConnString, p_iClientId)//sonali-cloud
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "SYS_HOLIDAYS, ENTITY";
			base.KeyField = "HOL_DATE";
			base.RadioName = "HolidaysList";			
			base.WhereClause = "SYS_HOLIDAYS.ORG_EID = ENTITY.ENTITY_ID ";
			base.OrderByClause = " HOL_DATE,ORG_EID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for the holidays list
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data and structure</param>
		/// <returns>xml structure and data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			LocalCache objCache=null;
			XmlNodeList objNodLst = null;
			XmlElement objElement=null;
			string sOrgID=string.Empty;

			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				objNodLst = p_objXmlDocument.SelectNodes("//listrow");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				foreach(XmlElement objElm in objNodLst)
				{
					foreach(XmlElement objChildElement in objElm.ChildNodes)
					{
						if (objChildElement.Name=="OrgId")
						{	
							sOrgID=objChildElement.InnerText;
                            objChildElement.InnerText = UTILITY.GetOrg(objChildElement.InnerText, base.ConnectString, m_iClientId);
						}
						else if (objChildElement.Name=="RowId")
						{
							objElement=objChildElement;
						}
					}
					objElement.InnerText=objElement.InnerText + "|" + sOrgID;
				}
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Holidays.Get.Err",m_iClientId),p_objEx); //sonali
			}
			finally
			{
				if(objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
				objNodLst = null;
			}
		}
	}
}