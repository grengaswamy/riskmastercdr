﻿using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
    public class AutoDiscTransReserveMapping
    {
        /// <summary>
        /// User name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Connection string
        /// </summary>
        private string m_sDSN = "";

        private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Language Code
        /// </summary>
        private int iLangCode = 0;  //Aman ML Change

        private int m_iClientId=0;

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public AutoDiscTransReserveMapping() { }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>
        public AutoDiscTransReserveMapping(string p_sConnString, int p_iClientId)
            
        {
            try
            {
                m_sConnectionString = p_sConnString;
                m_iClientId = p_iClientId;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Constructor.Error", p_iClientId), p_objEx);
            }

        }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="p_sLoginName">Login Name</param>
        /// <param name="p_sConnectionString">Connection String</param>
        public AutoDiscTransReserveMapping(string p_sLoginName, string p_sConnectionString, int p_iClientId)
        {
            try
            {
                m_sUserName = p_sLoginName;
                m_sConnectionString = p_sConnectionString;
                m_iClientId = p_iClientId;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Constructor.Error", p_iClientId), p_objEx);
            }

        }
        #endregion
		#region Properties
        public int LanguageCode {
            get { return iLangCode; }
            set { iLangCode = value; }

        }
        #endregion
        #region Private Functions
        /// <summary>
        /// Initialize the properties
        /// </summary>
        new private void Initialize()
        {
            try
            {
              
                //InitFields(arrFields);
                //base.ChildArray = arrChilds;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AutoDiscTransReserveMapping.Initialize.Error", m_iClientId), p_objEx);
            }
        }
        #endregion

        public XmlDocument Get()
        {
            XmlNodeList objNodLst = null;
            XmlElement objElement = null;
            XmlNode objNode = null;
            string sValue = "";
            LocalCache objCache = null;
            string sShortCode = "";
            string sShortDesc = "";
            string sTransType = "";
            string sTableId = "";
            string sValue1 = "";
            string sValue2 = "";
            string sMastReserType = "";
            DbReader objRead = null;
            XmlNodeList objNodTmpLst = null;
            StringBuilder sbSql = null;
            XmlDocument XMLDoc = null;
            XmlElement objRoot = null;
            XmlDocument p_objXmlDocument = new XmlDocument();
            try
            {
               
                objRoot = p_objXmlDocument.CreateElement("ReserveTransSetup");
                p_objXmlDocument.AppendChild(objRoot);


                objCache = new LocalCache(m_sConnectionString, m_iClientId);
                sTransType = objCache.GetTableId("TRANS_TYPES").ToString();
                sMastReserType = objCache.GetTableId("RESERVE_TYPE").ToString();

                objCache.Dispose();
                sbSql = new StringBuilder();
                objNode = p_objXmlDocument.CreateElement("TransactionTypes");
                sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC");
                sbSql.Append(" FROM CODES,CODES_TEXT WHERE CODES.TABLE_ID = " + sTransType  );    
                sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND ");
                sbSql.Append(" CODES.DELETED_FLAG = 0");
                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
                //rsushilaggar MITS 18809 06/16/2010
                sbSql.Append(" ORDER BY CODES.SHORT_CODE");
               
                sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "TRANS_TYPES", this.LanguageCode);  //Aman ML Change
                objRead = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                //Aman Used the indexes to get the values
                while (objRead.Read())
                {
                    objElement = p_objXmlDocument.CreateElement("option");
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue(0)));
                    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim() + " "
                        + Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
                    objNode.AppendChild(objElement);
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }

                objRoot.AppendChild(objNode);
                sbSql = new StringBuilder();
                objNode = p_objXmlDocument.CreateElement("ReserveTypes");
                sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC");
                sbSql.Append(" FROM CODES,CODES_TEXT WHERE CODES.TABLE_ID = " + sMastReserType);
                sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND ");
                sbSql.Append(" CODES.DELETED_FLAG = 0");
                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
                //rsushilaggar MITS 18809 06/16/2010
                sbSql.Append(" ORDER BY CODES.SHORT_CODE");
                sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "RESERVE_TYPE", this.LanguageCode);  //Aman ML Change
                objRead = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                while (objRead.Read())
                {
                    objElement = p_objXmlDocument.CreateElement("option");
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue(0)));
                    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim() + " "
                        + Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
                    objNode.AppendChild(objElement);
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }

                objRoot.AppendChild(objNode);


                XmlElement objXMLElem = p_objXmlDocument.CreateElement("Mapping");
                XmlElement objListHead = null;
                XmlElement objListOption = null;

                objListHead = p_objXmlDocument.CreateElement("listhead");
                string sNode = "<MapRowId>RowId</MapRowId>";
                sNode = sNode + "<TransCodeId>TransCodeId</TransCodeId>";
                sNode = sNode + "<TransType>Transaction Type</TransType>";
                sNode = sNode + "<ReserveCodeid>ReserveCodeid</ReserveCodeid>";
                sNode = sNode + "<ReserveType>Reserve Type</ReserveType>";

                objListHead.InnerXml = sNode;
                objXMLElem.AppendChild(objListHead);


                sbSql = new StringBuilder();
                sbSql.Append("SELECT MAP_ROW_ID , TRANS_CODE_ID, RESERVE_CODE_ID FROM DISCOUNT_TRANS_RESERVE_MAPPI");

                objRead = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                 while (objRead.Read())
                 {
                     objListOption = p_objXmlDocument.CreateElement("option");

                     //Aman MITS 27754  -Start

                     //sNode = "<MapRowId>" + Conversion.ConvertObjToStr(objRead.GetValue("MAP_ROW_ID")) + "</MapRowId>";
                     //sNode = sNode + "<TransCodeId>" + Conversion.ConvertObjToStr(objRead.GetValue("TRANS_CODE_ID")) + "</TransCodeId>";
                     //sNode = sNode + "<TransType>" + System.Web.HttpUtility.HtmlDecode(GetCodeInfo(Conversion.ConvertObjToStr(objRead.GetValue("TRANS_CODE_ID")))) + "</TransType>";  //Aman MITS 27754

                     //sNode = sNode + "<ReserveCodeid>" + Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_CODE_ID")) + "</ReserveCodeid>";
                     //sNode = sNode + "<ReserveType>" + System.Web.HttpUtility.HtmlDecode(GetCodeInfo(Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_CODE_ID")))) + "</ReserveType>";  //Aman MITS 27754

                     //objListOption.InnerXml = sNode;

                    
                     XmlElement objListValues = p_objXmlDocument.CreateElement("MapRowId");
                     objListValues.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("MAP_ROW_ID"));
                     objListOption.AppendChild(objListValues);

                     objListValues = p_objXmlDocument.CreateElement("TransCodeId");
                     objListValues.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("TRANS_CODE_ID"));
                     objListOption.AppendChild(objListValues);

                     objListValues = p_objXmlDocument.CreateElement("TransType");
                     objListValues.InnerText = GetCodeInfo(Conversion.ConvertObjToStr(objRead.GetValue("TRANS_CODE_ID")));
                     objListOption.AppendChild(objListValues);

                     objListValues = p_objXmlDocument.CreateElement("ReserveCodeid");
                     objListValues.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_CODE_ID"));
                     objListOption.AppendChild(objListValues);

                     objListValues = p_objXmlDocument.CreateElement("ReserveType");
                     objListValues.InnerText = GetCodeInfo(Conversion.ConvertObjToStr(objRead.GetValue("RESERVE_CODE_ID")));
                     objListOption.AppendChild(objListValues);

                     //Aman MITS 27754 ---End

                     objXMLElem.AppendChild(objListOption);
                     
                 }

                 objRoot.AppendChild(objXMLElem); 

                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNodLst = null;
                objElement = null;
                objNode = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objRead != null)
                {
                    objRead.Dispose();
                }
                objNodTmpLst = null;
                sbSql = null;

            }
            return p_objXmlDocument;
        }

        public bool DeleteMapping(XmlDocument p_objXmlDocument)
        {
            DbConnection objDbConnection = null;
            int iMapRowId;
            
            string sSQL = string.Empty;
            bool bReturnValue = true;
            string sDate = string.Empty;
            string sUser = m_sUserName;
                      
            try
            {
                XmlNode objXmlNode = p_objXmlDocument.SelectSingleNode("/ReserveTransSetup/DeleteRowId");
                string[] strDeleteRowId = objXmlNode.InnerText.Split(new Char[] { ',' });

                objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                foreach (string sRowId in strDeleteRowId)
                {
                    iMapRowId = Conversion.ConvertStrToInteger(sRowId);

                    sSQL = "DELETE FROM DISCOUNT_TRANS_RESERVE_MAPPI WHERE MAP_ROW_ID= "
                      + iMapRowId.ToString() ;

                    objDbConnection.Open();
                    objDbConnection.ExecuteNonQuery(sSQL);
                    objDbConnection.Close();
                }



                bReturnValue = true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbConnection != null)
                {
                    objDbConnection = null;
                }

            }
            return bReturnValue;
        }
        public bool SaveMapping(XmlDocument p_objXmlDocument)
        {
            XmlElement objNode = null;
            DbConnection objDbConnection = null;
            int iTransCode;
            int iReseverCode;
            string sSQL = string.Empty;
            bool bReturnValue = true;
            string sDate = string.Empty;
            string sUser = m_sUserName;
            int iNextId = 0;
            try
            {
                XmlNodeList objNodeCollection = p_objXmlDocument.SelectNodes("/ReserveTransSetup/option");
                XmlNode objXnode = null;

                objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                foreach (XmlNode objXmlNode in objNodeCollection)
                {
                    objXnode = objXmlNode.SelectSingleNode("TransCodeId");
                    iTransCode = Conversion.ConvertStrToInteger(objXnode.InnerText);

                    objXnode = objXmlNode.SelectSingleNode("ReserveCodeid");
                    iReseverCode = Conversion.ConvertStrToInteger(objXnode.InnerText);


                    iNextId = Utilities.GetNextUID(m_sConnectionString, "DISCOUNT_TRANS_RESERVE_MAPPI", m_iClientId);
					

                    sDate = "'" + Conversion.ToDbDateTime(DateTime.Now) + "'";
                    sSQL = "INSERT INTO DISCOUNT_TRANS_RESERVE_MAPPI (MAP_ROW_ID, TRANS_CODE_ID, RESERVE_CODE_ID, DTTM_RCD_ADDED, ADDED_BY_USER, DTTM_RCD_LAST_UPD, UPDATED_BY_USER) VALUES ("
                      + iNextId.ToString() + ", " + iTransCode.ToString() + ", " + iReseverCode.ToString() + "," + sDate + ",'" + sUser + "'," + sDate + ",'" + sUser + "'" + ")";

                    objDbConnection.Open();
                    objDbConnection.ExecuteNonQuery(sSQL);
                    objDbConnection.Close();
                }



                bReturnValue = true;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbConnection != null)
                {
                    objDbConnection = null;
                }

            }
            return bReturnValue;
        }



        private string GetCodeInfo(string p_sCode)
        {           
            string sShortCode = "";
            string sCodeDesc = "";
            int icode = Conversion.ConvertStrToInteger(p_sCode);
            LocalCache m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
            m_objLocalCache.GetCodeInfo(icode, ref sShortCode, ref sCodeDesc, this.LanguageCode); //Aman MITS 31745
            return sShortCode + " " + sCodeDesc;

           
        }


    }
}
