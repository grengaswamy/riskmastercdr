using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.Collections;
using System.Data; 

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    /// Date		: 04/07/2010
    /// Author	    : Michael Capps
    /// Purpose     :   Implements the CheckStubTexts form
    /// </summary>
    public class CheckStubTexts : UtilitiesUIListBase
    {
        int m_iClientId = 0;
        /// <summary>
        /// Database field array
        /// </summary>
        private string[,] arrFields = {
			{"StateId","STATE_ID"},
			{"TransTypeCode","TRANS_TYPE_CODE"},
			{"CheckStubText","CHECK_STUB_TEXT"}		
									  };

        /// Name		: CheckStubTexts
        /// Date		: 04/07/2010
        /// Author	    : Michael Capps		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="p_sConnString">Input connection string</param>
        public CheckStubTexts(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

        /// Name		: Initialize
        /// Date		: 04/07/2010
        /// Author	    : Michael Capps		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables
        /// </summary>
        new private void Initialize()
        {
            base.TableName = "CHK_STUB_TEXT_MAP ";
            base.KeyField = "CHKSTBTXT_ROW_ID";
            base.RadioName = "CheckStubTextMappingList";
            base.WhereClause = "";
            base.OrderByClause = "CHKSTBTXT_ROW_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        /// Name		: Get
        /// Date		: 04/07/2010
        /// Author	    : Michael Capps
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default get method.Returns the data for the form
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with structure</param>
        /// <returns>Xml structure and output data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlNodeList objNodLst = null;
            LocalCache objCache = null;
            int iStateID = 0;
            XmlElement XmlStateId = null;
            
            try
            {
                XMLDoc = p_objXmlDocument;
                base.Get();
                objCache = new LocalCache(base.ConnectString,m_iClientId);
                objNodLst = p_objXmlDocument.GetElementsByTagName("listrow");
                foreach (XmlElement objElm in objNodLst)
                {
                    //start rsushilaggar Set the codeid attribute in the state xml tag
                    XmlStateId = (XmlElement)objElm.GetElementsByTagName("StateId")[0];
                   if (XmlStateId.InnerText != "")
                   {
                       iStateID = Convert.ToInt32(XmlStateId.InnerText);
                       XmlStateId.SetAttribute("codeid", XmlStateId.InnerText);
                       string p_SAbb = string.Empty;
                       string p_SAbb2 = string.Empty;
                       objCache.GetStateInfo(iStateID, ref p_SAbb, ref p_SAbb2);
                       XmlStateId.InnerText = p_SAbb + " " + p_SAbb2;
                   }
                    //end rsushilaggar
               }
               
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckStubTexts.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNodLst = null;
            }
        }
    }
}