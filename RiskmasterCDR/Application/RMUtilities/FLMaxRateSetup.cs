using System;
using Riskmaster.Db;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Shruti
    ///Dated   :   08/21/2007
    ///Purpose :   This class Creates, Edits, Fetches FL Max Rate information.
    /// </summary>
    public class FLMaxRateSetup : UtilityFormBase
    {
        int m_iClientId = 0;
        #region Private Variables
        /// <summary>
        /// Fields mapping with Database fields
        /// </summary>
        private string[,] arrFields = {
			{"Year", "MAX_YEAR_TEXT"},
			{"MaxRate","MAX_RATE_AMT"},
            {"MaxRateId","MAX_WC_RATE_ID"}
									  };
        /// <summary>
        /// Field mapping SQL queries
        /// </summary>
        //{"XMLTag", "Table Name", "Key Field", "Value Field", "Where Clause"}
        private string[,] arrChilds = {
			{"SysFLMaxRate" , "SELECT MAX_WC_RATE_ID,MAX_YEAR_TEXT,MAX_RATE_AMT FROM WC_MAX_RATE_CALC"}
									  };
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>
        public FLMaxRateSetup(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            try
            {
                m_iClientId = m_iClientId;
                ConnectString = p_sConnString;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRateSetup.Constructor.Error", m_iClientId), p_objEx);
            }
        }
        #endregion

        #region Private Functions
        new private void Initialize()
        {
            try
            {
                base.InitFields(arrFields);
                base.ChildArray = arrChilds;
                base.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRateSetup.Initialize.Error", m_iClientId), p_objEx);
            }
        }
        #endregion

        #region Public Functions
        /// Name		: Get
        /// Author		: Shruti
        /// Date Created: 08/21/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>Xml containing the data to be populated on screen</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            LocalCache objCache = null;
            XMLDoc = p_objXmlDocument;
            DbReader objRead = null;
            XmlElement objParent = null;
            XmlElement objElement = null;
            StringBuilder sbSql = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            DbReader objReadTemp = null;
            try
            {
                base.Get();
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/group[1]");
                objElement = (XmlElement)p_objXmlDocument.CreateElement("FLMaxRates");
                objParent.AppendChild(objElement);
                sbSql = new StringBuilder();
                sbSql.Append(" SELECT MAX_WC_RATE_ID,MAX_YEAR_TEXT,MAX_RATE_AMT FROM WC_MAX_RATE_CALC ORDER BY MAX_YEAR_TEXT");


                objLstRow = p_objXmlDocument.CreateElement("listhead");

                objRowTxt = p_objXmlDocument.CreateElement("rowhead");
                objRowTxt.SetAttribute("colname", "Year");
                objRowTxt.SetAttribute("width", "10");
                objRowTxt.InnerText = "Year";
                objLstRow.AppendChild(objRowTxt);

                objRowTxt = p_objXmlDocument.CreateElement("rowhead");
                objRowTxt.SetAttribute("colname", "MaxRate");
                objRowTxt.InnerText = "Max Rate";
                objRowTxt.SetAttribute("width", "10");
                objLstRow.AppendChild(objRowTxt);

                objElement.AppendChild(objLstRow);

                objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString());
                while (objRead.Read())
                {
                    objLstRow = p_objXmlDocument.CreateElement("FLMaxRate");

                    objRowTxt = p_objXmlDocument.CreateElement("Year");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("MAX_YEAR_TEXT"));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("MaxRate");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("MAX_RATE_AMT"));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("MaxRateId");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("MAX_WC_RATE_ID"));
                    objLstRow.AppendChild(objRowTxt);


                    objElement.AppendChild(objLstRow);

                }

                objCache.Dispose();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FLMaxRateSetup.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
                objCache = null;
                if (objRead != null)
                {
                    objRead.Close();
                    objRead.Dispose();
                }

                if (objReadTemp != null)
                {
                    objReadTemp.Close();
                    objReadTemp.Dispose();
                }
                objRead = null;

            }
            return p_objXmlDocument;
        }
        #endregion
    }
}