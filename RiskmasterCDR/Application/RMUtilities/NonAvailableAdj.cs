﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Data;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Animesh Sahai 
    ///Dated   :   22/Nov/2007
    ///Purpose :   NonAvailScreen form
    /// </summary>
    class NonAvailableAdj : UtilitiesUIBase
    {
        /// <summary>
        /// Database fields mapping string array
        /// </summary>
        private string[,] arrFields = {
			{"AdjRowId", "ROW_ID"},
            {"AdjusterEID","ENTITY_ID"},
			{"StartDateTime","ENTITY_NON_AVL_START"},
			{"EndDateTime","ENTITY_NON_AVL_END"}
          };
        private int m_iClientId=0;

        /// Name		: NonAvailableAdj
        /// Author		: Animesh Sahai 
        /// Date Created: 22/Nov/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default constructor with connection string
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>
        public NonAvailableAdj(string p_sConnString,int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            m_iClientId = p_iClientId;
            ConnectString = p_sConnString;
            this.Initialize();
        }

        /// Name		: Initialize
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            TableName = "ENTITY_NON_AVL";
            KeyField = "ROW_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        /// Name		: New
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Ge the structure for a new record
        /// </summary>
        /// <param name="p_objXMLDocument">XML Doc</param>
        /// <returns>Xmldocument with structure</returns>
        public XmlDocument New(XmlDocument p_objXMLDocument)
        {
            try
            {
                return p_objXMLDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjusterNA.New.Err", m_iClientId), p_objEx);
            }
        }

        /// Name		: Get
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm;
            try
            {
                XMLDoc = p_objXmlDocument;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
                if (objElm != null)
                {
                    if (objElm.GetAttribute("orgid") != "")
                    {
                        base.WhereClause = " ORG_EID=" + objElm.GetAttribute("orgid");
                    }
                }
                base.Get();
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
                if (objElm != null)
                {
                    objElm.SetAttribute("currentid", objElm.GetAttribute("orgid"));
                    p_objXmlDocument.SelectSingleNode("//control[@name='Org_orglist']").InnerText = objElm.GetAttribute("orgid");
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NAGetError", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
            }

        }

        /// Name		: Delete
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes a particular record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>next record if any</returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                XMLDoc = p_objXmlDocument;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
                if (objElm != null)
                {
                    base.WhereClause = "ORG_EID=" + objElm.InnerText;
                }
                base.Delete();
                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NADeleteError", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
            }
        }

        /// Name		: Save
        /// Author		: Animesh Sahai 
        /// Date Created: 2/Nov/2007
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string sAutoAdjID = "";
            string sOrgId = "";
            string sClaimCounter = "";
            string sAdjusterID = "";
            string sSQL = "";
            XmlElement objElm = null;
            DbConnection objConn = null;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AutoAssignID']");
                if (objElm != null)
                    sAutoAdjID = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AdjusterEID']");
                if (objElm != null)
                    sAdjusterID = objElm.GetAttribute("codeid");

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DepartmentEID']");
                if (objElm != null)
                    sOrgId = objElm.GetAttribute("codeid");

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ClaimCounter']");
                if (objElm != null)
                    sClaimCounter = objElm.InnerText;



                objConn = DbFactory.GetDbConnection(ConnectString);
                objConn.Open();

                if (sAutoAdjID != "")
                {
                    sSQL = "DELETE FROM AUTO_ASSIGN_ADJ WHERE AUTO_ASSIGN_ID =" + sAutoAdjID;
                    objConn.ExecuteNonQuery(sSQL);
                }


                sSQL = "INSERT INTO AUTO_ASSIGN_ADJ(ADJ_EID,DEPT_EID,CLAIM_COUNTER,ADDED_BY_USER,UPDATED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD) VALUES ( " + sAdjusterID + "," + sOrgId + "," + sClaimCounter + ", 'abc','aa','20071010','20071010')";
                objConn.ExecuteNonQuery(sSQL);

                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NASaveError", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }


    }
}

