using System;
using System.Xml; 
using Riskmaster.DataModel; 
using System.Collections; 
using Riskmaster.Db; 
using Riskmaster.Common; 
using System.Data; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag, Pankaj
	///Dated   :   1st,Mar 2005
	///Purpose :   Base Class form
	/// </summary>
	public class UtilitiesBase // INavigation //, IPersistence, IDisposable
	{	
		/// <summary>
		/// Key Field
		/// </summary>
		private string m_sKeyField="";

		/// <summary>
		/// Key Field Value=""
		/// </summary>
		private long m_lKeyFieldValue=0;

		/// <summary>
		/// Key Field when its non-numeric
		/// </summary>
		private string m_sKeyFieldValue="";

		/// <summary>
		/// Table Name
		/// </summary>
		private string m_sTableName="";

		/// <summary>
		/// Input Xml document
		/// </summary>
		private XmlDocument m_objXmlDoc=null;		

		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnString="";

		/// <summary>
		/// Key Tag
		/// </summary>
		private string m_sKeyTag="";

		/// <summary>
		/// Where Clause for SQL query
		/// </summary>
		private string m_sWhereClause="";

        //gagnihotri MITS 11995 Changes made for Audit table
        private string m_sUserName = "";
        //Ashish Ahuja
        private int m_sUserId = 0;

        private int m_iClientId = 0;//Add by kuladeep for review defect.
		
		/// <summary>
		/// Move First Constant
		/// </summary>
		internal const int MOVE_FIRST = 1;

		/// <summary>
		/// Move Last Constant
		/// </summary>
		internal const int MOVE_LAST= 2;

		/// <summary>
		/// Move Next Constant
		/// </summary>
		internal const int MOVE_NEXT= 3;

		/// <summary>
		/// Move Previous Constant
		/// </summary>
		internal const int MOVE_PREVIOUS = 4;		

		/// <summary>
		/// Datafield List
		/// </summary>
		internal DataFieldList m_FieldList=new DataFieldList(); 

		/// <summary>
		/// Child array variable
		/// </summary>
		protected string[,] m_arrChild =  new string[10,5];
		
		/// <summary>
		/// Property map collection variable
		/// </summary>
		internal Hashtable m_PropertyMap = new Hashtable();

		#region "Properties"
		
		/// <summary>
		/// TableName Property
		/// </summary>
		protected string TableName
		{
			get{return m_sTableName;}
			set{m_sTableName = value;}
		}

		/// <summary>
		/// KeyField Property
		/// </summary>
		protected string KeyField
		{
			get{return m_sKeyField;}
			set{m_sKeyField = value;}
		}

		/// <summary>
		/// WhereClause Property
		/// </summary>
		protected string WhereClause
		{
			get{return m_sWhereClause;}
			set{m_sWhereClause = value;}
		}

		/// <summary>
		/// Xml document Property
		/// </summary>
		protected XmlDocument XMLDoc
		{
			get{return m_objXmlDoc;}
			set{m_objXmlDoc = value;}
		}

		/// <summary>
		/// Connection String Property
		/// </summary>
		internal string ConnectString
		{
			get{return m_sConnString;}
			set{m_sConnString = value;}
		}

		/// <summary>
		/// Child array property
		/// </summary>
		protected string[,] ChildArray
		{
			set{m_arrChild = value;}
		}

		#endregion

		/// Name		: UtilitiesBase
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        internal UtilitiesBase(string p_sConnString, int p_iClientId)//Add by kuladeep for review defect.
		{
            m_iClientId = p_iClientId;//Add by kuladeep for review defect.
			Initialize();
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        internal UtilitiesBase(string p_sConnString, string p_sUserName, int p_iClientId)//Add by kuladeep for review defect.
        {
            m_iClientId = p_iClientId;//Add by kuladeep for review defect.
            Initialize();
            m_sUserName = p_sUserName;
        }

        //Ashish Ahuja
        internal UtilitiesBase(string p_sConnString, int p_sUserId, int p_iClientId)//Add by kuladeep for review defect.
        {
            m_iClientId = p_iClientId;//Add by kuladeep for review defect.
            Initialize();
            m_sUserId = p_sUserId;
        }


		/// Name		: UtilitiesBase
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDatabaseName">Dsn Name</param>
		internal UtilitiesBase(string p_sUserName, string p_sPassword, string p_sDatabaseName)
		{
			Initialize();
		}
		
		/// Name		: Initialize
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Blank Implementation
		/// </summary>
		protected void Initialize(){}

		/// Name		: InitFields
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Add all obect Fields into the Field Collection from our Field List.
		/// </summary>
		/// <param name="p_sFields">Fields List</param>
		protected virtual void InitFields(string[,] p_sFields)
		{ 
			bool bKeyFound=false;
			for(int i =0; i< p_sFields.GetLength(0);i++)
			{
				if((m_sKeyField==p_sFields[i,1])&& !bKeyFound)
				{
					m_sKeyTag = p_sFields[i,0];
					bKeyFound=true;
				}
				this.m_FieldList[p_sFields[i,0]] = p_sFields[i,1];
			}			
		}

	
		/// <summary>
		/// E V E N T S 
		/// </summary>
		public event InitObjEventHandler InitObj;


		/// Name		: MoveFirst
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveFirst
		/// </summary>
		protected virtual void MoveFirst()
		{
			MoveAndLoad(MOVE_FIRST);
		}

		/// Name		: MoveLast
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveLast
		/// </summary>
		protected virtual void MoveLast()
		{
			MoveAndLoad(MOVE_LAST);
		}

		/// Name		: MoveNext
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveNext
		/// </summary>
		protected virtual void MoveNext()
		{
			m_lKeyFieldValue = GetKeyFieldValue();
			MoveAndLoad(MOVE_NEXT);
		}

		/// Name		: MovePrevious
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MovePrevious
		/// </summary>
		protected virtual void MovePrevious()
		{
			m_lKeyFieldValue = GetKeyFieldValue();
			MoveAndLoad(MOVE_PREVIOUS);
		}

		/// Name		: MoveTo
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveTo
		/// </summary>
		/// <param name="keyValue">Key Value</param>
		protected virtual void MoveTo( params int[] keyValue){}

		/// Name		: MoveAndLoad
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveAndLoad
		/// </summary>
		/// <param name="moveDirection">Move Direction</param>
		protected void MoveAndLoad(int moveDirection)
		{
			DbReader rdr= null;
			string sSQL="";
			int lNewID=0;
			DbConnection objConn = null;
			try
			{
				switch (moveDirection)
				{
					case MOVE_FIRST:
						sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>0",this.m_sKeyField,this.m_sTableName, this.m_sKeyField);
						break;
					case MOVE_LAST:
						sSQL = String.Format("SELECT MAX({0}) FROM {1}",this.m_sKeyField,this.m_sTableName);
						break;
					case MOVE_NEXT:
						sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
						break;
					case MOVE_PREVIOUS:
						if(m_lKeyFieldValue==0)
							sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
						else
							sSQL = String.Format("SELECT MAX({0}) FROM {1} WHERE {2}<{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
						break;
				}

				objConn = DbFactory.GetDbConnection(m_sConnString);
				objConn.Open(); 
				rdr = objConn.ExecuteReader(sSQL);

				///Fetch the RowId
				if (rdr.Read())
					lNewID = rdr.GetInt(0);
				rdr.Close();
				objConn.Dispose(); 
				
				//Only if New Unique ID is not 0 and New Unique ID different than old one
				if (lNewID != 0 && lNewID != m_lKeyFieldValue)
				{
					m_lKeyFieldValue = lNewID;
				}
				Get();
			}
			finally
			{
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                }
			}
		}

		/// Name		: Save
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save method
		/// </summary>
		protected virtual void Save()
		{
			XmlNodeList objXmlNodeList = null;
			string sControlType="";
			string sSql = string.Empty;
			string sFieldNames = string.Empty;
			string sFieldValues = string.Empty; 
			DbConnection objConn = null;
            //abisht MITS 10526
            LocalCache objCache = null;
            //Added by bsharma33 for MITS 26943
            DbWriter writer = null;
            string sFieldName, sFieldValue;	
		    //End changes by bsharma33 MITS 26943
			try
			{
                objConn = DbFactory.GetDbConnection(m_sConnString);
                //Added by bsharma33 for MITS 26943
                writer = DbFactory.GetDbWriter(objConn);
                writer.Tables.Add(this.m_sTableName);
                //End changes by bsharma33 MITS 26943
                //abisht MITS 10526
                objCache = new LocalCache(m_sConnString, m_iClientId);
				if(!ValidateXml())
					throw new Exception("Unable to validate the Input XML"); 
			
				objXmlNodeList = m_objXmlDoc.GetElementsByTagName("control");
			
				m_lKeyFieldValue = GetKeyFieldValue();

				if(m_lKeyFieldValue >0 || m_sKeyFieldValue !="")
				{				
					foreach(XmlElement objXmlElement in objXmlNodeList)
					{
						if(!m_FieldList.ContainsField(objXmlElement.GetAttribute("name")))//For labels and other unwanted controls in UI to be ignored
							continue;
					
						//if(m_FieldList[objXmlElement.GetAttribute("name")].ToString() != m_sKeyField && objXmlElement.InnerText.Trim() != string.Empty)
						if(m_FieldList[objXmlElement.GetAttribute("name")].ToString() != m_sKeyField)//removed blank check as check box can be innertext can be empty
						{
							if(m_FieldList[objXmlElement.GetAttribute("name")].ToString() != m_sKeyField)
							{
								sControlType= objXmlElement.GetAttribute("type").Trim().ToLower();
								switch(sControlType)
								{
									case "radio":
									case "combobox":
                                        //Changed by bsharma33 for MITS 26943
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.GetAttribute("value").ToUpper() == "NULL"? "null" : "'" + objXmlElement.GetAttribute("value") + "'");
                                        sFieldValue = objXmlElement.GetAttribute("value").ToUpper() == "NULL" ? "null" : objXmlElement.GetAttribute("value");
                                        //End changes by bsharma33 for MITS 26943
                                        break;
									case "checkbox":
                                        //Changed by bsharma33 for MITS 26943
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.InnerText.ToLower() == "true"? "-1" : "0");
                                        sFieldValue = objXmlElement.InnerText.ToLower() == "true" ? "-1" : "0";
                                        //End changes by bsharma33 for MITS 26943
                                        break;
									case "date":
                                        //Changed by bsharma33 for MITS 26943
										//sSql += String.Format(" {0} = '{1}'," , this.m_FieldList[objXmlElement.GetAttribute("name")], Conversion.GetDate (objXmlElement.InnerText));
                                        sFieldValue = Conversion.GetDate(objXmlElement.InnerText);
                                        //End changes by bsharma33 for MITS 26943
										break;
									case "time":
                                        //Changed by bsharma33 for MITS 26943
										//sSql += String.Format(" {0} = '{1}'," , this.m_FieldList[objXmlElement.GetAttribute("name")], Conversion.GetTime(objXmlElement.InnerText));
                                        sFieldValue = Conversion.GetTime(objXmlElement.InnerText);
                                        //End changes by bsharma33 for MITS 26943
										break;
									case "code":
                                        //Changed by bsharma33 for MITS 26943
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.GetAttribute("codeid"));
                                        sFieldValue = objXmlElement.GetAttribute("codeid");
                                        //Endchanges by bsharma33 for MITS 26943
										break;
									case "orgh":
                                        //Changed by bsharma33 for MITS 26943
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.GetAttribute("codeid"));
                                        sFieldValue = objXmlElement.GetAttribute("codeid");
                                        //End changes by bsharma33 for MITS 26943
										break;
                                    case "numeric"://Parijat: Post Editable Enhanced Notes
                                        //Changed by bsharma33 for MITS 26943
                                        //sSql += String.Format(" {0} = {1},", this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.InnerText == "" ? "0" : objXmlElement.InnerText);
                                        sFieldValue = objXmlElement.InnerText == "" ? "0" : objXmlElement.InnerText;
                                        //End changes by bsharma33 for MITS 26943
                                        break;
									default:
                                        //Changed by bsharma33 for MITS 26943
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + objXmlElement.InnerText + "'");
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : objXmlElement.InnerText;
                                        //End changes by bsharma33 for MITS 26943
										break;
								}
                                //Added by bsharma33 for MITS 26943
                                sFieldName = this.m_FieldList[objXmlElement.GetAttribute("name")].ToString();
                                writer.Fields.Add(sFieldName, sFieldValue);
                                //End changes by bsharma33 for MITS 26943
							}
						}                        
					}
                    //Changed by bsharma33 for MITS 26943
					//if(sSql != string.Empty)
                    if(writer.Fields.Count>0)
					{
						//sSql = sSql.Substring(0,sSql.Length - 1);
						//Take care of string key field
                       // if (m_sKeyFieldValue != "")
                       // {
                            //gagnihotri MITS 11995 Changes made for Audit table
                            //sSql = String.Format("UPDATE {0} SET {1} WHERE {2} = '{3}'",this.m_sTableName,sSql,this.m_sKeyField,m_sKeyFieldValue);
                            //sSql = "UPDATE " + this.m_sTableName +
                             //       " SET " + sSql + ",UPDATED_BY_USER ='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                             //       " WHERE " + this.m_sKeyField + "='" + m_sKeyFieldValue + "'";

                            writer.Fields.Add("UPDATED_BY_USER", m_sUserName);
                            writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                            WhereClause = string.Format("{0}={1}", this.m_sKeyField, m_lKeyFieldValue);
                       // }
                        //else
                       // {
                            //gagnihotri MITS 11995 Changes made for Audit table
                            //sSql = String.Format("UPDATE {0} SET {1} WHERE {2} = {3}",this.m_sTableName,sSql,this.m_sKeyField,m_lKeyFieldValue);
                         //   sSql = "UPDATE " + this.m_sTableName +
                          //          " SET " + sSql + ",UPDATED_BY_USER ='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) +
                          //          " WHERE " + this.m_sKeyField + "=" + m_lKeyFieldValue ;
                       // }
						if(m_sWhereClause!="")
						{
                            
                            if (WhereClause.Length > 0)
                                WhereClause = WhereClause + " AND " + m_sWhereClause;
                            else
                                WhereClause =  m_sWhereClause;
                            //if( sSql.IndexOf("WHERE")>0)
                            //    sSql = sSql + " AND " +  m_sWhereClause;
                            //else
                            //    sSql = " WHERE " + m_sWhereClause;
						}
                        writer.Where.Add(WhereClause);
					}
                    //End chnages by bsharma33 for MITS 26943
				}
				else
				{
                    m_lKeyFieldValue = Riskmaster.Common.Utilities.GetNextUID(m_sConnString, m_sTableName, m_iClientId);

					foreach(XmlElement objXmlElement in objXmlNodeList)
					{	
						if(!m_FieldList.ContainsField(objXmlElement.GetAttribute("name")))//For labels and other unwanted controls in UI to be ignored
							continue;

						if(m_FieldList[objXmlElement.GetAttribute("name")] != null)
						{
							if(m_FieldList[objXmlElement.GetAttribute("name")].ToString() == m_sKeyField)
							{
                                //Changed by bsharma33 for MITS 26943
								//sFieldValues += String.Format("{0},", m_lKeyFieldValue);
								//sFieldNames += String.Format("{0},", this.m_FieldList[objXmlElement.GetAttribute("name")]);
                                sFieldName = Convert.ToString(this.m_FieldList[objXmlElement.GetAttribute("name")]);
                                sFieldValue = Convert.ToString(m_lKeyFieldValue);
                                writer.Fields.Add(sFieldName, sFieldValue);
                                //End chnages by bsharma33 for MITS 26943
							}
							else if(objXmlElement.InnerText.Trim() != string.Empty)
							{
								sControlType = objXmlElement.GetAttribute("type").Trim().ToLower();
								switch(sControlType)
								{
									case "radio":
									case "combobox":
                                        //Changed by bsharma33 for MITS 26943
										//sFieldValues += String.Format("{0},", objXmlElement.GetAttribute("value").ToUpper() == "NULL"? "null" : "'" + objXmlElement.GetAttribute("value") + "'");	
                                        sFieldValue = objXmlElement.GetAttribute("value").ToUpper() == "NULL" ? "null" : objXmlElement.GetAttribute("value");
                                        //End changes by bsharma33 for MITS 26943
										break;
									case "checkbox":
                                        //Changed by bsharma33 for MITS 26943
                                        if (objXmlElement.InnerText.ToLower().Trim().Equals("true"))
                                            //sFieldValues += String.Format("{0},","-1");
                                            sFieldValue = String.Format("{0},", "-1");
                                        else
                                            //sFieldValues += String.Format("{0},", "0");
                                            sFieldValue = String.Format("{0},", "0");
                                        //End changes by bsharma33 for MITS 26943
										break;
									case "date":
                                        //Changed by bsharma33 for MITS 26943
										//String.Format("{0},",objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + Conversion.GetDate(objXmlElement.InnerText) + "'");
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" :  Conversion.GetDate(objXmlElement.InnerText);
                                        //End changes by bsharma33 for MITS 26943
										break;
									case "time":
                                        //Changed by bsharma33 for MITS 26943
										//String.Format("{0},",objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + Conversion.GetTime(objXmlElement.InnerText) + "'");
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" :  Conversion.GetTime(objXmlElement.InnerText);
                                        //End changes by bsharma33 for MITS 26943
										break;
									case "code":
                                        //Changed by bsharma33 for MITS 26943
										//String.Format("{0},",objXmlElement.InnerText.ToUpper() == "NULL"? "null" :  objXmlElement.GetAttribute("codeid") );
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : objXmlElement.GetAttribute("codeid");
                                        //End changes by bsharma33 for MITS 26943
										break;
									case "orgh":
                                        //Changed by bsharma33 for MITS 26943
										//String.Format("{0},",objXmlElement.InnerText.ToUpper() == "NULL"? "null" :  objXmlElement.GetAttribute("codeid") );
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : objXmlElement.GetAttribute("codeid");
                                        //End changes by bsharma33 for MITS 26943
										break;
									default:
                                        //Changed by bsharma33 for MITS 26943
										//sFieldValues += String.Format("{0},", objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + objXmlElement.InnerText + "'");
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" :  objXmlElement.InnerText;
                                        //End changes by bsharma33 for MITS 26943
										break;
								}
                                //Changed by bsharma33 for MITS 26943
								//sFieldNames += String.Format("{0},", this.m_FieldList[objXmlElement.GetAttribute("name")]);
                                sFieldName = this.m_FieldList[objXmlElement.GetAttribute("name")].ToString();
                                writer.Fields.Add(sFieldName, sFieldValue);
                                //Enh changes by bsharma33 for MITS 26943
							}
						}
					}

					//if(sFieldNames != string.Empty && sFieldValues != string.Empty)
                    if(writer.Fields.Count>0)
					{

						//sFieldNames = sFieldNames.Substring(0,sFieldNames.Length - 1);
						//sFieldValues = sFieldValues.Substring(0,sFieldValues.Length - 1);
                        //gagnihotri MITS 11995 Changes made for Audit table
						//sSql = String.Format("INSERT INTO {0} ({1}) VALUES ({2})",this.m_sTableName,sFieldNames,sFieldValues);
                        //sSql = "INSERT INTO " + this.m_sTableName + "(sFieldNames, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES(" + sFieldValues + ",'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now);
                        writer.Fields.Add("UPDATED_BY_USER", m_sUserName);
                        writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
					}
				}			
                //objConn = DbFactory.GetDbConnection(m_sConnString);
                //objConn.Open(); 
                //objConn.ExecuteNonQuery(sSql);
                //objConn.Dispose();
                //abisht MITS 10526
                if (writer.Fields.Count > 0)
                {
                    writer.Execute();
                }
                objCache.RemoveAnyKeyFromCache("SYSINF_ORACLE_CASE_INS");
			}
			finally
			{
				objXmlNodeList = null;
                //abisht MITS 10526
                if (objCache != null)
                    objCache.Dispose();
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}
		
		/// Name		: Get
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Base Get Implementation
		/// </summary>
		protected virtual void Get()
		{
			string sSQL = "";
			DbConnection objConn = null;
			DbReader objDbReader = null;
			XmlNodeList objNodeList = null;
			string sValue = "";
			string sControlType="";

			try
			{

				if(m_lKeyFieldValue == 0)
					m_lKeyFieldValue = GetKeyFieldValue();
			
				if(m_lKeyFieldValue==0 && m_sKeyFieldValue =="")
					sSQL = String.Format("SELECT * FROM {0}", m_sTableName);
				else
				{
					//To take care of case where the key field is a string
					if(m_lKeyFieldValue>0)
						sSQL = String.Format("SELECT * FROM {0} WHERE {1} = {2}",m_sTableName,m_sKeyField,m_lKeyFieldValue);
					else
						sSQL = String.Format("SELECT * FROM {0} WHERE {1} = '{2}'",m_sTableName,m_sKeyField,m_sKeyFieldValue);
				}

				if(m_sWhereClause!="")
				{
					if( sSQL.IndexOf("WHERE")>0)
						sSQL = sSQL + " AND " +  m_sWhereClause;
					else
						sSQL = sSQL + " WHERE " + m_sWhereClause;
				}

				objConn = DbFactory.GetDbConnection(m_sConnString);
				objConn.Open(); 
				objDbReader = objConn.ExecuteReader(sSQL); 
				if (objDbReader != null)
				{
					if(objDbReader.Read())
					{
						foreach(string sName in m_FieldList.Keys)
						{
							sValue = objDbReader[m_FieldList[sName].ToString()].ToString();
							objNodeList = m_objXmlDoc.GetElementsByTagName("control");
							foreach(XmlElement objXmlElement in objNodeList)
							{
								if(objXmlElement.GetAttribute("name") == sName)
								{
									sControlType=objXmlElement.GetAttribute("type").ToLower();
									switch(sControlType)
									{
										case "checkbox":
											if (sValue.Equals("-1") || sValue.Equals("1") || sValue.Equals("True"))
												objXmlElement.InnerText = "True";
											else
												objXmlElement.InnerText = "";
											break;
											//Radio button and combo are same except appearance
										case "radio":
										case "combobox":
											objXmlElement.SetAttribute("value",sValue);  
											break;
										case "date":
											objXmlElement.InnerText = Conversion.GetDBDateFormat(sValue,"d"); 
											break;
										case "time":
											objXmlElement.InnerText = Conversion.GetDBTimeFormat(sValue,"t"); 
											break;
										case "code":
											objXmlElement.SetAttribute("codeid",sValue);
                                            objXmlElement.InnerText = UTILITY.GetCode(sValue, m_sConnString, m_iClientId);    
											break;
										case "orgh":
											objXmlElement.SetAttribute("codeid",sValue);
                                            objXmlElement.InnerText = UTILITY.GetOrg(sValue, m_sConnString, m_iClientId);
											break;
										default:
											objXmlElement.InnerText = sValue;
											break;
									}
								}	                            
							}
						}
					}
					objDbReader.Close();
				}
				objConn.Dispose(); 
				LoadChildXml(); 
			}
			finally
			{
				if(objDbReader != null)
				{
					objDbReader.Dispose();
					objDbReader=null;
				}
				if(objConn!= null)
				{
					objConn.Dispose();
					objConn=null;
				}
				objNodeList = null;
			}
		}
		
		/// Name		: Delete
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete method
		/// </summary>
		/// <returns>boolean success</returns>
		protected virtual bool Delete()
		{
			DbConnection objConn = null;
			try
			{
				m_lKeyFieldValue = GetKeyFieldValue();		
				objConn = DbFactory.GetDbConnection(m_sConnString);
				objConn.Open(); 
				objConn.ExecuteNonQuery(String.Format("DELETE FROM {0} WHERE {1} = {2}",m_sTableName,m_sKeyField,m_lKeyFieldValue)); 
				objConn.Dispose();
				return true;
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}

		/// Name		: ValidateXml
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Validate the input xml for consistency
		/// </summary>
		/// <returns>boolean success</returns>
		protected virtual bool ValidateXml()
		{
			XmlNodeList objNodeList = null;
			bool bFound = false;
			try
			{
				foreach(string sName in m_FieldList.Keys)
				{
					objNodeList = m_objXmlDoc.GetElementsByTagName("control");
				
					foreach(XmlElement objXmlElement in objNodeList)
					{
						if(objXmlElement.GetAttribute("name") == sName)
						{
							bFound = true;
							break;
						}
					}
					if(bFound == false)
						return false;
					bFound = false;
				}
				return true;
			}
			finally
			{
				objNodeList=null;
			}
		}

		/// Name		: GetKeyFieldValue
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the key field value from the xml
		/// </summary>
		/// <returns>key field value</returns>
		private long GetKeyFieldValue()
		{
			XmlNodeList objXmlNodeList;
			long lKeyFieldValue = 0;

			try
			{
				objXmlNodeList = m_objXmlDoc.GetElementsByTagName("control");
				foreach(XmlElement objXmlElement in objXmlNodeList)
				{
					if(objXmlElement.GetAttribute("name") == m_sKeyTag)
					{
						if(objXmlElement.GetAttribute("datatype")=="string")
							m_sKeyFieldValue= objXmlElement.InnerText;
						else							
							lKeyFieldValue = Conversion.ConvertStrToLong(objXmlElement.InnerText); 
						break;
					}
				}

				return lKeyFieldValue; 
			}
			finally
			{
				objXmlNodeList=null;
			}
		}

		/// Name		: LoadChildXml
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Populates the combo and other lists
		/// </summary>
		private void LoadChildXml()
		{ 
			XmlNodeList objNodeList = null;
			XmlElement objNewElm = null;
			DataSet objDs = null;
			string sSQL = "";

			try
			{
				for(int i =0; i< m_arrChild.GetLength(0);i++)
				{
					objNodeList = m_objXmlDoc.GetElementsByTagName("control");

					foreach(XmlElement objElm in objNodeList)
					{
						if(objElm.GetAttribute("name") == m_arrChild[i,0])
						{
							sSQL = String.Format("SELECT {0} , {1} FROM {2} WHERE {3}",m_arrChild[i,1],m_arrChild[i,2],m_arrChild[i,3],m_arrChild[i,4]);
                            objDs = DbFactory.GetDataSet(m_sConnString, sSQL, m_iClientId);//Add by kuladeep for review defect.
							foreach(DataRow objRow in objDs.Tables[0].Rows)
							{
								string sDesc = DbRow.GetStringField(objRow,m_arrChild[i,2]).ToLower();
								long lValue = DbRow.GetLngField(objRow,m_arrChild[i,1]);
								if (lValue == 0)
									sDesc ="None";							
								objNewElm = XMLDoc.CreateElement("option");
								objNewElm.SetAttribute("value", lValue.ToString());
								objNewElm.InnerText = UTILITY.CamelCase(sDesc);
								objElm.AppendChild(objNewElm);
							}
						}
					}
				}
			}
			finally
			{
				objNodeList = null;
				objNewElm = null;
				if(objDs!=null)
				{
					objDs.Dispose();
					objDs=null;
				}
			}
		}
	}

	/// <summary>
	/// DataFieldList contains the list of fields and their values for a single DataObject.
	/// </summary>
	internal class DataFieldList : IEnumerable 
	{
		/// <summary>
		/// Fields Collection
		/// </summary>
		protected System.Collections.Hashtable m_Fields=null;

		/// <summary>
		/// Default Constructor
		/// </summary>
		public DataFieldList()
		{
			m_Fields = new System.Collections.Hashtable();
		}

		/// <summary>
		/// Retrieves Item from the collection
		/// </summary>
		public object this[string PropertyName]
		{
			get
			{	
				return m_Fields[PropertyName]; 
			}
			set
			{
				if(!m_Fields.ContainsKey(PropertyName))
					m_Fields.Add(PropertyName,value);
				else
					m_Fields[PropertyName] = value;
			}
		}

		/// Name		: Clear
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		//Reset all field values to defaults.
		/// <summary>
		/// Clear
		/// </summary>
		public void Clear()
		{ 
			//Ugly hack because it is difficult to iterate over the keys collection making changes...
			object[] arr = new object[m_Fields.Keys.Count];
			m_Fields.Keys.CopyTo(arr,0);
			
			for(int i=0; i< arr.Length;i++)
				m_Fields[arr[i]] = "";
		}
		
		/// <summary>
		/// Initialize the Collection
		/// </summary>
		public void Empty()
		{
			m_Fields = new System.Collections.Hashtable();
		}		

		/// <summary>
		/// Returns the no of items in the collection
		/// </summary>
		/// <returns></returns>
		public int Count()
		{
			return m_Fields.Count;
		}
		
		/// <summary>
		/// Returns the Key collection
		/// </summary>
		internal ICollection Keys{get{return m_Fields.Keys;}}

		/// Name		: GetKeyArray
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the array for the collection
		/// </summary>
		/// <returns></returns>
		internal object[] GetKeyArray()
		{
			//Ugly hack because it is difficult to iterate over the keys collection making changes...
			object[] arr = new object[m_Fields.Keys.Count];
			m_Fields.Keys.CopyTo(arr,0);
			return arr;
		}

		/// Name		: GetEnumerator
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the Enumerator for the collection
		/// </summary>
		/// <returns></returns>
		public System.Collections.IEnumerator GetEnumerator()
		{
			return m_Fields.Keys.GetEnumerator();
		}
		
		/// Name		: ContainsField
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Checks if the collection contains the particular key item
		/// </summary>
		/// <param name="fieldName">Field Name</param>
		/// <returns>Boolean true false</returns>
		public bool ContainsField(string fieldName)
		{
			return m_Fields.ContainsKey(fieldName);
		}

		/// Name		: ToString
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// ToString is overridden to provide a comma separated list of field names suitable for use in SQL queries.
		/// </summary>
		/// <returns>String value</returns>
		override public string ToString()
		{
			string ret="";
			foreach (string s in m_Fields.Keys)
				ret += s.ToUpper() + ",";
			
			return ret.TrimEnd(',');
		}	
	}
}