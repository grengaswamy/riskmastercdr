﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Data;
using Riskmaster.Db;
using System.Text;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Animesh Sahai 
    ///Dated   :   31st,Oct 2007
    ///Purpose :   Adjuster Screen List Form 
    /// </summary>

    public class AdjusterScreens : UtilitiesUIListBase
    {
        /// <summary>
        /// Private database field mapping string array
        /// </summary>
        private string[,] arrFields = {
			{"AutoAssignID", "AUTO_ASSIGN_ID"},
			{"AdjusterEID", "ADJ_EID"},
            {"ClaimCounter", "CLAIM_COUNTER"},
            {"AddedByUser", "ADDED_BY_USER"},
            {"UpdatedByUser", "UPDATED_BY_USER"},
            {"DateTimeRecordAdded", "DTTM_RCD_ADDED"},
            {"DateTimeRecordLastUpdated", "DTTM_RCD_LAST_UPD"},
            {"ForceWorkItems", "FORCE_WORK_ITEMS"},
            {"SkipWorkItems", "SKIP_WORK_ITEMS"},
            {"LOBs", "LOB_CODES"},
            {"Departments", "DEPT_EIDS"},
            {"ClaimTypes", "CLAIM_TYPE_CODES"},
            {"Jurisdictions", "JURISDICTION_CODES"},
            {"CurrentlyAvailable", "AVAILABILITY"}
									  };


        /// Name		: AdjusterScreens
        /// Author		: Animesh Sahai 
        /// Date Created: 10/31/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default Constructor with Connection String
        /// </summary>
        /// <param name="p_sConnString">Input Connection String parameter</param>
        public AdjusterScreens(string p_sConnString, int p_iClientId) //rkaur27
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId; //rkaur27
            this.Initialize();
        }

        /// Name		: Initialize
        /// Author		: Animesh Sahai 
        /// Date Created: 10/31/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            base.TableName = "AUTO_ASSIGN_ADJ";
            base.KeyField = "AUTO_ASSIGN_ID";
            base.RadioName = "AdjusterScreensList";
            base.WhereClause = "";
            base.OrderByClause = "AUTO_ASSIGN_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }
        int m_iClientId = 0;//rkaur27
        /// Name		: Get
        /// Author		: Pankaj Chowdhury
        /// Date Created: 04/28/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the holidays list
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            LocalCache objCache = null;
            XmlNodeList objNodLst = null;
            string sAdjID = string.Empty;
            int lAdjID = 0; 

            try
            {
                XMLDoc = p_objXmlDocument;
                //base.Get();
                //New Enhancement functionality added
                p_objXmlDocument = populateScreenXML(p_objXmlDocument);
                //End
                objNodLst = p_objXmlDocument.SelectNodes("//listrow");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                foreach (XmlElement objElm in objNodLst)
                {
                    foreach (XmlElement objChildElement in objElm.ChildNodes)
                    {
                        if (objChildElement.Name == "AdjusterEID")
                        {
                            sAdjID = objChildElement.InnerText;
                            //objChildElement.InnerText = UTILITY.GetEmpDetails(objChildElement.InnerText, base.ConnectString);
						    lAdjID = sAdjID.Length;
                            if (lAdjID > 0 && sAdjID.Substring(0, 1) == "*")
                            {
                                objChildElement.InnerText = UTILITY.GetEmpDetails(objChildElement.InnerText.Substring(1, lAdjID - 1), base.ConnectString, m_iClientId) + " *";
                            }
                            else
                            {
                                objChildElement.InnerText = UTILITY.GetEmpDetails(objChildElement.InnerText, base.ConnectString, m_iClientId);
                            }
                        }
                        else if (objChildElement.Name == "RowId")
                        {
                            if (objChildElement.InnerText == "0")
                            {
                                //this is a blank row to separate rows with the same criteria later in Grid (UI)
                                //we don't want to continue as we don't have an Eid for Adjuster and it will error out when trying to get the adjuster's name.
                                break;
                            }
                        }
                        else if (objChildElement.Name == "Departments")
                        {
                            objChildElement.InnerText = UTILITY.GetMultiCodeInfo("orgh", objChildElement.InnerText, ref objCache, m_iClientId);
                            if (objChildElement.InnerText == string.Empty)
                            {
                                objChildElement.InnerText = "All";
                            }
                        }
                        else if (objChildElement.Name == "LOBs")
                        {
                            objChildElement.InnerText = UTILITY.GetMultiCodeInfo("LINE_OF_BUSINESS", objChildElement.InnerText, ref objCache, m_iClientId);
                            if (objChildElement.InnerText == string.Empty)
                            {
                                objChildElement.InnerText = "All";
                            }
                        }
                        else if (objChildElement.Name == "ClaimTypes")
                        {
                            objChildElement.InnerText = UTILITY.GetMultiCodeInfo("CLAIM_TYPE", objChildElement.InnerText, ref objCache, m_iClientId);
                            if (objChildElement.InnerText == string.Empty)
                            {
                                objChildElement.InnerText = "All";
                            }
                        }
                        else if (objChildElement.Name == "Jurisdictions")
                        {
                            objChildElement.InnerText = UTILITY.GetMultiCodeInfo("STATES", objChildElement.InnerText, ref objCache, m_iClientId);
                            if (objChildElement.InnerText == string.Empty)
                            {
                                objChildElement.InnerText = "All";
                            }
                        }
                    }
                    //objElement.InnerText=objElement.InnerText + "|" + sAdjID;
                }

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                if (p_objEx.Message != string.Empty)
                {
                    throw new RMAppException(p_objEx.Message, p_objEx);
                }
                throw new RMAppException(Globalization.GetString("AdjustersGetError", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objNodLst = null;
            }
        }

        //************************************************************************
        //New Function Added
        //// Author		: Animesh Sahai 
        /// Date Created: 14-Jan-2008
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// to populate the screen XML as per new requirement 
        /// grouped by department and then adjuster name
        /// </summary>
        protected XmlDocument populateScreenXML(XmlDocument p_objXmlDocument)
        {
            DataSet objDs = null;
            DataSet objDs2 = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            XmlNode objNode = null;
            XmlNodeList objNodLst = null;
            XmlElement objElm = null;
            string sColumnName = String.Empty;
            string adjEId = String.Empty;
            //string sSQL = "";
            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbSQL2 = new StringBuilder();
            DataFieldList m_FieldList = new DataFieldList();
            bool bIsOracle = false;
            bool bSeparateSameCriteria = true;
            string sSqlConcatenationIdentifier = "+"; // for MS SQL server
            string sSqlOracleConcatenationIdentifier = "||"; // for Oracle
            string availability = string.Empty;
            string sDept_EIds = string.Empty;
            string sLOB_Codes = string.Empty;
            string sClaimType_Codes = string.Empty;
            string sJurisdiction_Codes = string.Empty;
            string sUniqueCombination = string.Empty;
            string sNextAdj = string.Empty;
            string sOrderBy = string.Empty;
            string sDefaultOrderBy = string.Empty;
            DateTime DTTM;
            StringBuilder strSql = new StringBuilder();

            int SWI = 0;
            long lastDTTM = 0;

            for (int i = 0; i < arrFields.GetLength(0); i++)
            {
                m_FieldList[arrFields[i, 0]] = arrFields[i, 1];
            }
            
            try
            {
                using (Riskmaster.Db.DbReader objReader = DbFactory.GetDbReader(ConnectString, "SELECT PARM_NAME, STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME IN ('AAA_LOB_FLG', 'AAA_DEP_FLG', 'AAA_WRI_FLG', 'AAA_FWI_FLG', 'AAA_SWI_FLG', 'AAA_AVL_FLG', 'AAA_CLT_FLG', 'AAA_JUR_FLG', 'AAA_SSC_FLG')"))
                {
                    if (objReader != null)
                    {
                        objNode = p_objXmlDocument.SelectSingleNode("//AdjusterScreensList/listhead");
                        while (objReader.Read())
                        {
                            if (!Riskmaster.Common.Conversion.ConvertObjToBool(objReader[1], m_iClientId))
                            {
                                switch (objReader[0].ToString())
                                {
                                    case "AAA_LOB_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//LOBs"));
                                        break;
                                    case "AAA_DEP_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//Departments"));
                                        break;
                                    case "AAA_WRI_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//ClaimCounter"));
                                        break;
                                    case "AAA_FWI_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//ForceWorkItems"));
                                        break;
                                    case "AAA_SWI_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//SkipWorkItems"));
                                        break;
                                    case "AAA_AVL_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//CurrentlyAvailable"));
                                        break;
                                    case "AAA_CLT_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//ClaimTypes"));
                                        break;
                                    case "AAA_JUR_FLG":
                                        objNode.RemoveChild(objNode.SelectSingleNode("//Jurisdictions"));
                                        break;
                                    case "AAA_SSC_FLG":
                                        bSeparateSameCriteria = false;
                                        break;
                                }
                            }
                        }
                    }
                }

               sDefaultOrderBy = "DEPT_EIDS, CLAIM_TYPE_CODES, LOB_CODES, JURISDICTION_CODES";
         
                if (bSeparateSameCriteria)
                {
                    sDefaultOrderBy = sDefaultOrderBy + " ,ADJ_NAME";
                }
                else
                {
                    sDefaultOrderBy = "ADJ_NAME " + " ," + sDefaultOrderBy;
                }

                bIsOracle = DbFactory.IsOracleDatabase(ConnectString);
                if(bIsOracle)
                {
                    sSqlConcatenationIdentifier = sSqlOracleConcatenationIdentifier;
                }
                sbSQL = sbSQL.Append("SELECT AUTO_ASSIGN_ID, ADJ_EID, SKIP_WORK_ITEMS, FORCE_WORK_ITEMS, LOB_CODES, DEPT_EIDS, CLAIM_TYPE_CODES, JURISDICTION_CODES");
                //Nima 5/3/2010; Code clean up/showing Skip Items and Availability on the main setup screen
                //smishra54 11/15/2011 - claim counter fix as adjuster defined JURISDICTION_CODES should be compare with claim's state id not with event.
                
                //rupal:start, mits 28948
                //NO NEED TO USE THE FUNCTION GetMultiCodeFieldWhereClauseSearchForAnyItem BECAUSE HERE THE WHERE CLAUSE USES TwO DATABASE FEILDS INSTEAD OF SPACE SEPARATED ACTUAL VALUES
                //sbSQL = sbSQL.Append(string.Format(", (SELECT COUNT(*) FROM CLAIM_ADJUSTER CAJ INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID AND (ADJ.DEPT_EIDS IS NULL OR {0}) AND (ADJ.LOB_CODES IS NULL OR {1}) AND (ADJ.CLAIM_TYPE_CODES IS NULL OR {2}) AND (ADJ.JURISDICTION_CODES IS NULL OR {3})) CLAIM_COUNTER", Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.DEPT_EIDS", "EVT.DEPT_EID", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.LOB_CODES", "CLM.LINE_OF_BUS_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.CLAIM_TYPE_CODES", "CLM.CLAIM_TYPE_CODE", bIsOracle), Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("ADJ.JURISDICTION_CODES", "CLM.FILING_STATE_ID", bIsOracle)));
                if (!bIsOracle)
                {
                    sbSQL = sbSQL.Append(",(SELECT COUNT(CAJ.CLAIM_ID) FROM CLAIM_ADJUSTER CAJ  INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID  INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID  INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID  WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID  AND (ADJ.DEPT_EIDS IS NULL OR ADJ.DEPT_EIDS = '' OR (CHARINDEX(CONVERT(varchar,EVT.DEPT_EID), ADJ.DEPT_EIDS) > 0))  AND (ADJ.LOB_CODES IS NULL OR ADJ.LOB_CODES = '' OR (CHARINDEX(CONVERT(varchar,CLM.LINE_OF_BUS_CODE), ADJ.LOB_CODES) > 0))  AND (ADJ.CLAIM_TYPE_CODES IS NULL OR ADJ.CLAIM_TYPE_CODES='' OR (CHARINDEX(convert(varchar,CLM.CLAIM_TYPE_CODE) , ADJ.CLAIM_TYPE_CODES) > 0))  AND (ADJ.JURISDICTION_CODES IS NULL OR ADJ.JURISDICTION_CODES = '' OR (CHARINDEX(convert(varchar ,CLM.FILING_STATE_ID) , ADJ.JURISDICTION_CODES) > 0)) ) CLAIM_COUNTER ");
                }
                else
                {
                    sbSQL = sbSQL.Append(",(SELECT COUNT(CAJ.CLAIM_ID) FROM CLAIM_ADJUSTER CAJ INNER JOIN CLAIM CLM ON CAJ.CLAIM_ID = CLM.CLAIM_ID INNER JOIN CODES  CDS ON CLM.CLAIM_STATUS_CODE = CDS.CODE_ID INNER JOIN EVENT  EVT ON CLM.EVENT_ID = EVT.EVENT_ID WHERE CDS.SHORT_CODE <> 'C' AND CAJ.ADJUSTER_EID = ADJ.ADJ_EID AND (Trim(ADJ.DEPT_EIDS) IS NULL OR (INSTR(ADJ.DEPT_EIDS,EVT.DEPT_EID)>0)) AND (TRIM(ADJ.LOB_CODES) IS NULL OR (INSTR(ADJ.LOB_CODES,CLM.LINE_OF_BUS_CODE)>0)) AND (TRIM(ADJ.CLAIM_TYPE_CODES) IS NULL OR (INSTR(ADJ.CLAIM_TYPE_CODES,CLM.CLAIM_TYPE_CODE)>0)) AND (TRIM(ADJ.JURISDICTION_CODES) IS NULL OR (INSTR(ADJ.JURISDICTION_CODES,CLM.FILING_STATE_ID)>0)) ) CLAIM_COUNTER ");                    
                }
                //rupal:end

                //sbSQL = sbSQL.Append(", (SELECT LAST_NAME FROM ENTITY EN WHERE EN.ENTITY_ID=ADJ.DEPT_EID) DEPT_NAME");
                sbSQL = sbSQL.Append(string.Format(", ( SELECT LAST_NAME {0} ', ' {0} FIRST_NAME FROM ENTITY EN WHERE EN.ENTITY_ID=ADJ.ADJ_EID) ADJ_NAME", sSqlConcatenationIdentifier));
                sbSQL = sbSQL.Append(string.Format(" FROM AUTO_ASSIGN_ADJ ADJ ORDER BY {0}", sDefaultOrderBy));

                sbSQL2 = sbSQL2.Append(string.Format("SELECT ENTITY_ID, (CASE WHEN {0} >= ENTITY_NON_AVL_START AND {0} <= ENTITY_NON_AVL_END THEN '1No' ELSE CASE WHEN {0} < ENTITY_NON_AVL_START THEN '2Yes; till ' {1} ENTITY_NON_AVL_START ELSE '3Yes' END END) AVL FROM ENTITY_NON_AVL ORDER BY AVL", Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now), sSqlConcatenationIdentifier));

                using (objDs = DbFactory.GetDataSet(ConnectString, sbSQL.ToString(), m_iClientId))
                using (objDs2 = DbFactory.GetDataSet(ConnectString, sbSQL2.ToString(), m_iClientId))
                {

                    objNode = p_objXmlDocument.SelectSingleNode("//*[name()='" + base.RadioName + "']");
                    objNodLst = objNode.ChildNodes.Item(0).ChildNodes;

                    foreach (DataRow objRow in objDs.Tables[0].Rows)
                    {//skhare7 MITS 29107
                        sDept_EIds = objRow["DEPT_EIDS"].ToString().Trim();
                        sClaimType_Codes = objRow["CLAIM_TYPE_CODES"].ToString().Trim();
                        sLOB_Codes = objRow["LOB_CODES"].ToString().Trim();
                        sJurisdiction_Codes = objRow["JURISDICTION_CODES"].ToString().Trim();
                        //skhare7 MITS 29107 End
                        //this part is to separate rows with the same criteria later in Grid (UI) and also mark the next adjuster with *//added a blank space for MITS 29107
                        if (bSeparateSameCriteria && (sUniqueCombination != (sDept_EIds + sClaimType_Codes + sLOB_Codes + sJurisdiction_Codes + " ")))
                        {
                           
                           
                                sNextAdj = string.Empty;
                            lastDTTM = 0;
                            SWI = 0;//added a blank space for MITS 29107
                            sUniqueCombination = sDept_EIds + sClaimType_Codes + sLOB_Codes + sJurisdiction_Codes+" ";
                           

                            if (strSql.Length > 0)
                            {
                                strSql.Remove(0, strSql.Length);
                                //we create a blank row with RowId = 0; so later in UI rows with the same criteria are separedted.
                                objLstRow = p_objXmlDocument.CreateElement("listrow");
                                objRowTxt = p_objXmlDocument.CreateElement("RowId");
                                objRowTxt.InnerText = "0";
                                objLstRow.AppendChild(objRowTxt);
                                for (int i = 1; i < objNodLst.Count; i++)
                                {
                                    objRowTxt = p_objXmlDocument.CreateElement(objNodLst[i].Name);
                                    objLstRow.AppendChild(objRowTxt);
                                }
                                objNode.AppendChild(objLstRow);
                            }

                            strSql.Append(string.Format("SELECT {0}, {1}, {2}, {3}, {4} FROM {5} WHERE ({6} OR {7}) AND ({8} OR {9}) AND ({8} OR {9}) AND ({10} OR {11}) AND ({12} OR {13}) AND {1} NOT IN"
                                , "AUTO_ASSIGN_ID" //0
                                , "ADJ_EID" //1
                                , "FORCE_WORK_ITEMS" //2
                                , "SKIP_WORK_ITEMS" //3
                                , "DTTM_LAST_ASSIGNED" //4
                                , "AUTO_ASSIGN_ADJ" //5
                                , "LOB_CODES IS NULL" //6
                                , Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("LOB_CODES", sLOB_Codes, bIsOracle) //7
                                , "DEPT_EIDS IS NULL" //8
                                , Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("DEPT_EIDS", sDept_EIds, bIsOracle) //9
                                , "CLAIM_TYPE_CODES IS NULL" //10
                                , Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("CLAIM_TYPE_CODES", sClaimType_Codes, bIsOracle) //11
                                , "JURISDICTION_CODES IS NULL" //12
                                , Utilities.GetMultiCodeFieldWhereClauseSearchForAnyItem("JURISDICTION_CODES", sJurisdiction_Codes, bIsOracle)) //13
                            );
                            strSql.Append(string.Format(" (SELECT {0} FROM {1} WHERE {2} >= {3} AND {2} <= {4}) ORDER BY {5} DESC, {6} ASC"
                                , "DISTINCT ENTITY_ID" //0
                                , "ENTITY_NON_AVL" //1
                                , Riskmaster.Common.Conversion.ToDbDateTime(DateTime.Now) //2
                                , "ENTITY_NON_AVL_START" //3
                                , "ENTITY_NON_AVL_END" //4
                                , "FORCE_WORK_ITEMS" //5
                                , "DTTM_LAST_ASSIGNED") //6
                            );

                            using (Riskmaster.Db.DbReader objReader = DbFactory.GetDbReader(ConnectString, strSql.ToString()))
                            {
                                while (objReader != null && objReader.Read())
                                {
                                    if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], m_iClientId) > 0)
                                    {
                                        //update AUTO_ASSIGN_ADJ table and decrease ASSIGNMENTS_TO_SKIP by 1
                                        if (SWI == 0)
                                        {
                                            sNextAdj = Riskmaster.Common.Conversion.ConvertObjToStr(objReader[0]);
                                            SWI = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], m_iClientId);
                                            lastDTTM = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], m_iClientId);
                                        }
                                        else
                                        {
                                            if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], m_iClientId) < SWI)
                                            {
                                                sNextAdj = Riskmaster.Common.Conversion.ConvertObjToStr(objReader[0]);
                                                SWI = Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], m_iClientId);
                                                lastDTTM = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], m_iClientId);
                                            }
                                            else if (Riskmaster.Common.Conversion.ConvertObjToInt(objReader[3], m_iClientId) == SWI && Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], m_iClientId) < lastDTTM)
                                            {
                                                sNextAdj = Riskmaster.Common.Conversion.ConvertObjToStr(objReader[0]);
                                                lastDTTM = Riskmaster.Common.Conversion.ConvertObjToInt64(objReader[4], m_iClientId);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        sNextAdj = Riskmaster.Common.Conversion.ConvertObjToStr(objReader[0]);
                                        break;
                                    }
                                }
                            }
                        }
                        objLstRow = p_objXmlDocument.CreateElement("listrow");
                        if (base.KeyField.IndexOf('.') > 0)
                            sColumnName = (base.KeyField.ToString().Split('.'))[1].ToString();
                        else
                            sColumnName = base.KeyField.ToString();

                        //objLstRow.SetAttribute("rowid",objRow[sColumnName].ToString()); 
                        objRowTxt = p_objXmlDocument.CreateElement("RowId");
                        objRowTxt.InnerText = objRow[sColumnName].ToString();
                        objLstRow.AppendChild(objRowTxt);

                        adjEId = String.Empty;

                        for (int i = 1; i < objNodLst.Count; i++)
                        {
                            objElm = (XmlElement)objNodLst[i];
                            objRowTxt = p_objXmlDocument.CreateElement(objNodLst[i].Name);
                            if (m_FieldList.ContainsField(objElm.Name))//Fill data only when its in the column list
                            {

                                if (objElm.Name == "CurrentlyAvailable")
                                {
                                    foreach (DataRow objRow2 in objDs2.Tables[0].Rows)
                                    {
                                        if (objRow2[0].ToString() == adjEId)
                                        {
                                            availability = objRow2[1].ToString();
                                            if (availability.Length > 14)
                                            {
                                                DTTM = Riskmaster.Common.Conversion.ToDate(availability.Substring(availability.Length - 14, 14));
                                                objRowTxt.InnerText = availability.Substring(1, availability.Length - 15) + String.Format("{0:MM/dd/yyyy hh:mm:ss tt}", DTTM);
                                            }
                                            else if (availability.Length > 1)
                                            {
                                                objRowTxt.InnerText = availability.Substring(1, availability.Length - 1);
                                            }
                                            break;
                                        }
                                    }
                                    if (objRowTxt.InnerText == String.Empty)
                                    {
                                        objRowTxt.InnerText = "Yes"; // No record in ENTITY_NON_AVL matching the current Auto Adjuster record; hence the adjuster is available
                                    }
                                }
                                else
                                {
                                    objRowTxt.InnerText = objRow[m_FieldList[objElm.Name].ToString()].ToString();
                                    if (objElm.Name == "AdjusterEID")
                                    {
                                        adjEId = objRowTxt.InnerText;
                                        if (objRow["AUTO_ASSIGN_ID"].ToString() == sNextAdj)
                                        {
                                            objRowTxt.InnerText = "*" + objRowTxt.InnerText;
                                        }
                                    }
                                }
                            }
                            objLstRow.AppendChild(objRowTxt);
                        }
                        objNode.AppendChild((XmlNode)objLstRow);
                    }
                }

                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjustersPopulateError", m_iClientId), p_objEx);
            }

            finally
            {

            }
        }
    }
}
