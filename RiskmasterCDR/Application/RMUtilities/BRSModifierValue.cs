using System;
using System.Xml; 
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Db;
using System.Text;
using System.Collections.Generic;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   02nd,May 2005
	///Purpose :   Shows the Modifier Value form
	/// </summary>
	public class BRSModifierValue:UtilitiesUIBase 
	{
		/// <summary>
		/// Private string array variable for database field mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId","MODIFIER_ROW_ID"},
			{"ModifierCode","MODIFIER_CODE"},
			{"StartCPT","START_CPT"},
			{"EndCPT","END_CPT"},
			{"ModValue","MODIFIER_VALUE"},
			{"MaxRLV","MAX_RLV"},
			{"TableId", "FEETABLE_ID"}
									  };

        private int m_iClientId = 0;
		/// Name		: BRSModifierValue
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public BRSModifierValue(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			ConnectString = p_sConnString;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the variables and properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "BRS_MOD_VALUES";
			KeyField = "MODIFIER_ROW_ID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: New
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Ge the structure for a new record
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>Xmldocument with structure</returns>
		public XmlDocument New(XmlDocument p_objXMLDocument)
		{
			try
			{
				return p_objXMLDocument; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSModifierValue.New.Err", m_iClientId), p_objEx);
			}
		}

		/// Name		: Delete
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes a particular record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>next record if any</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDocument)
		{						
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Delete();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSModifierValue.Delete.Err", m_iClientId), p_objEx);
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the current record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data to save it</param>
		/// <returns>Saved data alogn with its xml structure</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //string sSQL = "";
            //END PenTesting - srajindersin - 9th Jan 2012
			
			int iTableId=0;
			int iRowId=0;
			int iModCod=0;
			string sStrtCpt="";
			string sEndCpt="";
			double dModVal=0;
			double dMaxRlv=0;
            XmlNode objNod=null;
			DbReader objRdr=null;
			DbConnection objCn=null;
            
			try
			{
				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
				if(objNod!=null)
					iRowId = Conversion.ConvertStrToInteger(objNod.InnerText);

				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
				if(objNod!=null)
					iTableId = Conversion.ConvertStrToInteger(objNod.InnerText);

				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='ModifierCode']");
				if(objNod!=null)
					iModCod = Conversion.ConvertStrToInteger(((XmlElement)objNod).GetAttribute("codeid"));

				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='StartCPT']");
				if(objNod!=null)
					sStrtCpt = objNod.InnerText;

				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='EndCPT']");
				if(objNod!=null)
					sEndCpt = objNod.InnerText;

				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='ModValue']");
				if(objNod!=null)
					dModVal = Conversion.ConvertStrToDouble(objNod.InnerText);

				objNod = p_objXmlDocument.SelectSingleNode("//control[@name='MaxRLV']");
				if(objNod!=null)
					dMaxRlv = Conversion.ConvertStrToDouble(objNod.InnerText);

                if (iRowId == 0)
                {
                    //PenTesting - srajindersin - 9th Jan 2012
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format("SELECT * FROM BRS_MOD_VALUES WHERE FEETABLE_ID = {0} ", "~FEETABLE_ID~"));
                    strSQL.Append(string.Format(" AND MODIFIER_CODE = {0} ", "~MODIFIER_CODE~"));
                    strSQL.Append(string.Format(" AND START_CPT = {0} ", "~START_CPT~"));
                    strSQL.Append(string.Format(" AND END_CPT = {0} ", "~END_CPT~"));

                    dictParams.Add("FEETABLE_ID", iTableId);
                    dictParams.Add("MODIFIER_CODE", iModCod);
                    dictParams.Add("START_CPT", sStrtCpt);
                    dictParams.Add("END_CPT", sEndCpt);

                    objRdr = DbFactory.ExecuteReader(ConnectString, strSQL.ToString(), dictParams);

                    //sSQL = "SELECT * FROM BRS_MOD_VALUES WHERE FEETABLE_ID = " + iTableId +
                    //    " AND MODIFIER_CODE = " + iModCod +
                    //    " AND START_CPT = '" + sStrtCpt +
                    //    "' AND END_CPT = '" + sEndCpt + "'";
                    //objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                    //END PenTesting - srajindersin - 9th Jan 2012

                    if (objRdr.Read())
                    {
                        throw new RMAppException(Globalization.GetString("BRSModifierValue.Save.DupErr", m_iClientId));
                    }

                    //PenTesting - srajindersin - 9th Jan 2012
                    strSQL = new StringBuilder();
                    strSQL.Append("INSERT INTO BRS_MOD_VALUES(MODIFIER_ROW_ID,FEETABLE_ID,MODIFIER_CODE,START_CPT,");
                    strSQL.Append(string.Format("END_CPT,MODIFIER_VALUE,MAX_RLV) VALUES( {0},", "~MODIFIER_ROW_ID~"));
                    strSQL.Append(string.Format("{0}, {1}, {2}, {3}, {4}, {5} )", "~FEETABLE_ID~", "~MODIFIER_CODE~", "~START_CPT~", "~END_CPT~", "~MODIFIER_VALUE~", "~MAX_RLV~"));

                    dictParams.Clear();
                    dictParams.Add("MODIFIER_ROW_ID", Utilities.GetNextUID(ConnectString, "BRS_MOD_VALUES", m_iClientId));
                    dictParams.Add("FEETABLE_ID", iTableId);
                    dictParams.Add("MODIFIER_CODE", iModCod);
                    dictParams.Add("START_CPT", sStrtCpt);
                    dictParams.Add("END_CPT", sEndCpt);
                    dictParams.Add("MODIFIER_VALUE", dModVal);
                    dictParams.Add("MAX_RLV", dMaxRlv);

                    //sSQL = "INSERT INTO BRS_MOD_VALUES(MODIFIER_ROW_ID,FEETABLE_ID,MODIFIER_CODE,START_CPT," +
                    //    "END_CPT,MODIFIER_VALUE,MAX_RLV) VALUES(" + Utilities.GetNextUID(ConnectString, "BRS_MOD_VALUES") +
                    //    "," + iTableId + "," + iModCod + ",'" + sStrtCpt + "','" + sEndCpt + "'," + dModVal + "," + dMaxRlv + ")";
                    //END PenTesting - srajindersin - 9th Jan 2012

                }
                else
                {//PenTesting - srajindersin - 9th Jan 2012
                    strSQL = new StringBuilder();
                    strSQL.Append(string.Format("UPDATE BRS_MOD_VALUES SET MODIFIER_CODE = {0}, START_CPT = {1}", "~MODIFIER_CODE~", "~START_CPT~"));
                    strSQL.Append(string.Format(", END_CPT = {0}, MODIFIER_VALUE = {1}", "~END_CPT~", "~MODIFIER_VALUE~"));
                    strSQL.Append(string.Format(", MAX_RLV = {0} WHERE MODIFIER_ROW_ID = {1}", "~MAX_RLV~", "~MODIFIER_ROW_ID~"));

                    dictParams.Clear();
                    dictParams.Add("MODIFIER_CODE", iModCod);
                    dictParams.Add("START_CPT", sStrtCpt);
                    dictParams.Add("END_CPT", sEndCpt);
                    dictParams.Add("MODIFIER_VALUE", dModVal);
                    dictParams.Add("MAX_RLV", dMaxRlv);
                    dictParams.Add("MODIFIER_ROW_ID", iRowId);

                    //sSQL = "UPDATE BRS_MOD_VALUES SET MODIFIER_CODE=" + iModCod + " ,START_CPT='" + sStrtCpt +
                    //    "', END_CPT='" + sEndCpt + "' ,MODIFIER_VALUE=" + dModVal + " ,MAX_RLV=" + dMaxRlv +
                    //    " WHERE MODIFIER_ROW_ID=" + iRowId;
                }//END PenTesting - srajindersin - 9th Jan 2012

                //PenTesting - srajindersin - 9th Jan 2012
                objRdr = DbFactory.ExecuteReader(ConnectString, strSQL.ToString(), dictParams);
                //objCn= DbFactory.GetDbConnection(ConnectString);
                //objCn.Open();
                //objCn.ExecuteNonQuery(sSQL);
                //END PenTesting - srajindersin - 9th Jan 2012

				return p_objXmlDocument;
			}
			catch(RMAppException objEx)
			{
				throw objEx; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSModifierValue.Save.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objNod=null;
				if(objCn!=null)
				{
					objCn.Dispose();
					objCn=null;
				}
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr=null;
				}
			}
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 07/07/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for a particular id
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>xml data and structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSModifierValue.Get.Err", m_iClientId), p_objEx);
			}
		}
	}
}