using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
 
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21 Mar 2005
	///Purpose :   This class Fetches OrgEntity Security Setup form data
	/// </summary>
	public class OrgEntitySecuritySetup:UtilitiesListBase 
	{

		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields = {
			{"GroupName","GROUP_NAME"}
									  };
        private int m_iClientId = 0;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public OrgEntitySecuritySetup(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			ConnectString = p_sConnString;
			this.Initialize(); 
		}

		/// <summary>
		/// Initializes the fields
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "ORG_SECURITY ";
			base.KeyField = "GROUP_ID";
			base.RadioName = "TableName";
			base.OrderByClause ="GROUP_NAME";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// <summary>
		/// Fetches the form data for Org Security Setup
		/// </summary>
		/// <param name="p_objXmlDocument">XML Doc</param>
		/// <returns>XML Doc with Data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				return XMLDoc; 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("OrgEntitySecuritySetup.Get.Err", m_iClientId), p_objEx); 
			}
		}
	}
}
