using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   21st,Mar 2005
	///Purpose :   Discount List implementation form
	/// </summary>
	public class DiscountList:UtilitiesUIListBase
	{
        private int m_iClientId = 0;

		/// <summary>
		/// String array for Discount List field mapping
		/// </summary>
        // Naresh Volume Discount Implementation
		private string[,] arrFields = {
			{"DiscountName","DISCOUNT_NAME"},
			{"State","STATE_ID"},
			{"EffectiveDate","EFFECTIVE_DATE"},
			{"ExpirationDate","EXPIRATION_DATE"},
            {"UseVolumeDiscount","USE_VOLUME_DISCOUNT"}
									  };

		/// Name		: DiscountList
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor for Discount List
		/// </summary>
		/// <param name="p_sConnString"></param>
        public DiscountList(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize private variables and properties
		/// </summary>
		new private void Initialize()
		{
            base.TableName = "SYS_POL_DISCOUNTS LEFT OUTER JOIN STATES ON SYS_POL_DISCOUNTS.STATE = STATES.STATE_ROW_ID";
			base.KeyField = "DISCOUNT_ROWID";
			base.RadioName = "DiscountList";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for the Discount List form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data structure</param>
		/// <returns>Xml document with data and the form structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			string sLOB = string.Empty;
			string sState = string.Empty;

			try
			{
				sLOB = p_objXmlDocument.SelectSingleNode("//LOBCode").InnerText; 
				sState = p_objXmlDocument.SelectSingleNode("//StateCode").InnerText;
                //Start:Added by Nitin Goel:Correct retreive for Discount setup list,05/17/2010,MITS#20744
                //base.WhereClause = " (LINE_OF_BUSINESS =" + sLOB + " OR LINE_OF_BUSINESS = 0) AND STATE =" + sState + " AND DISCOUNT_ROWID NOT IN (SELECT DISTINCT DISCOUNT_ROWID FROM SYS_POL_SEL_DISC)";
                base.WhereClause = " (LINE_OF_BUSINESS =" + sLOB + " OR LINE_OF_BUSINESS = 0) AND STATE =" + sState + " AND DISCOUNT_ROWID NOT IN (SELECT DISTINCT DISCOUNT_ROWID FROM SYS_POL_SEL_DISC WHERE IS_RATE_SETUP = 0)";
                //End:Nitin Goel:Correct retreive for Discount setup list,05/17/2010,MITS#20744
                XMLDoc = p_objXmlDocument;
				base.Get();
				return XMLDoc; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DiscountList.Get.Err", m_iClientId), p_objEx);
			}
		}

		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes the record for the Discount List form
		/// </summary>
		/// <param name="p_iDiscountRowId">Discount Row Id</param>
		/// <returns>true / false</returns>
		internal bool Delete(int p_iDiscountRowId)
		{
			try
			{
                base.TableName = "SYS_POL_DISCOUNTS";
				base.KeyFieldValue = p_iDiscountRowId.ToString();
				base.WhereClause = "";
				base.Delete(); 
				return true; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DiscountList.Delete.Error", m_iClientId), p_objEx);
			}
		}
	}
}