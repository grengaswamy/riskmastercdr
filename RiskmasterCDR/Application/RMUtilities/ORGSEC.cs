using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Xml;
using System.Collections.Generic;  //pmittal5
using System.Text; //pmittal5

using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using Riskmaster.Security.Encryption;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   14 April 2005
	///Purpose :   This class is responsible for Business Entity Security Setup module
	/// </summary>
	public class ORGSEC : IDisposable
    {  
        private int m_iLangCode=0;
        private int m_iClientId = 0;

		public ORGSEC(UserLogin p_objUserLogin, int p_iClientId)
		{
			m_objUserLogin = p_objUserLogin;
            m_iClientId = p_iClientId;
			Initialize(); 
		}
        public ORGSEC(UserLogin p_objUserLogin, string p_sCheckDefer, int p_iClientId)
        {
            m_objUserLogin = p_objUserLogin;
            m_sCheckDefer = p_sCheckDefer;
            m_iClientId = p_iClientId;
            Initialize();
        }
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        

        public void Dispose()
        {
            if (m_objDbConnection != null)
                m_objDbConnection.Dispose();
            if(m_objAdminDbConnection!=null)
                m_objAdminDbConnection.Dispose();
        }

        #region private class constant variables
        private const string VIEW_TABLES = "CLAIM|CLAIMANT|EMPLOYEE|ENTITY|EVENT|FUNDS|PERSON_INVOLVED|RESERVE_CURRENT|RESERVE_HISTORY|";
		private const string BES_SYSTEMFILEPATH = "BES_SystemFilePath";
		private const string BES_DEFFILENAME = "BES_DefFileName";
		private const string BES_CREATETRIGGER_ORACLE = "BES_CreateTrigger_Oracle";
		private const string BES_CREATETRIGGER_SQLSERVER = "BES_CreateTrigger_SQLSERVER";
		private const string BES_CREATETRIGGER_DB2 = "BES_CreateTrigger_DB2";
		private const string BES_DROPTRIGGER_SQLSERVER = "BES_DROPTrigger_SQLSERVER";
		//Mjain8 Added 7/31/2006:Complex password for sql server 2005 in accordance of password policy
		private const string BES_COMPLEX_PASSWORD = "R1$km@5ter";
        #endregion

        #region private class variables

        private string m_sConnString = "";
		private string m_sAdminConnString = "";
        //nadim for 14334
      
        private string sExistingUserName = string.Empty;
        private string sExistingGroupName = string.Empty;
        private bool m_bMultipleUser = false;
        private bool m_bMultipleGroups = false; //pmittal5 Mits 21710
        //nadim for 14334
		private int m_iUserId = 0;
		private string m_sDSNId = "";
		private string m_sDBOUserId = "";
		private string m_sDBOPwd = "";
		private string m_sSecurityDSN = "";
		private string m_sDataBaseName = "";
        private string m_sOraBESRole = "";
        private string m_sCheckDefer = "";
        private string m_sUserLogin = "";  //gagnihotri MITS 11995 Changes made for Audit table
        private bool m_bShowOnlyRole = false;
		private bool m_bIsAdmin = false;
		private bool m_bIsBrokerSupport = false;
		private bool m_bIsInsurerSupport = false;
		private bool m_bIsAllOrgEntities = false;
        private bool g_bBoeingBrokerSupport = false;//Parijat: Boeing broker support
		private eDatabaseType m_stDataBaseType;
		private DbConnection m_objDbConnection = null;
		private DbConnection m_objAdminDbConnection = null;
        private DbCommand m_objDbCommand = null;
        private DbTransaction m_objAdminDbTrans = null;
        private DbCommand m_objAdminDbCommand = null;
        private DbTransaction m_objDbTrans = null;
		private UserLogin m_objUserLogin = null;
		private bool bIsOrgSecEnabled = false;
		private bool bIsOrgSecAdmin = false;
		private ArrayList arDeptList = null;
		private ArrayList arParentList = null;
        //abisht MITS 10669
        private string m_sOveridingPassWord = string.Empty;
        private string m_sOverideFlag = "0";

        // Ash - cloud, load RM messages via connection stirng during class initialize
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;
        NameValueCollection m_BESSettings = new NameValueCollection();
        private string sBESSynSetting = "FALSE";//Deb MITS 29215


        #endregion

        #region "Properties"
        /// <summary>
		/// Read/Write property for IsAdmin
		/// </summary>
		public bool IsAdmin
		{
			get
			{
				return m_bIsAdmin;
			}
			set
			{
				m_bIsAdmin = value;
			}
		}

		/// <summary>
		/// Read/Write property for AdminConnString
		/// </summary>
		public string AdminConnString
		{
			get
			{
				return m_sAdminConnString;
			}
			set
			{
				m_sAdminConnString = value;
			}
		}

		public DbConnection objAdminDbConnection
		{
			get
			{
				if (m_objAdminDbConnection != null)
					return m_objAdminDbConnection;
				else
				{
					if (m_sAdminConnString != "")
					{
						m_objAdminDbConnection = DbFactory.GetDbConnection(m_sAdminConnString);
						m_objAdminDbConnection.Open();
						return m_objAdminDbConnection;
					}
					else
						return m_objDbConnection;
				}
			}
		}

        public string GetBESOraRole
        {
            get
            {
                return m_sOraBESRole;
            }
            set
            {
                m_sOraBESRole = value;
            }
        }

        public bool GetShowOnlyRole
        {
            get
            {
                return m_bShowOnlyRole;
            }
            set
            {
                m_bShowOnlyRole = value;
            }
        }


		/// <summary>
		/// Read/Write property for BrokerSupport
		/// </summary>
		internal bool BrokerSupportFlag
		{
			get
			{
				return m_bIsBrokerSupport;
			}
			set
			{
				m_bIsBrokerSupport = value;
			}
		}

		/// <summary>
		/// Read/Write property for InsurerSupport
		/// </summary>
		internal bool InsurerSupportFlag
		{
			get
			{
				return m_bIsInsurerSupport;
			}
		}

		/// <summary>
		/// Read/Write property for DataBaseType
		/// </summary>
		internal eDatabaseType DataBaseType
		{
			get
			{
				return m_stDataBaseType;
			}
		}

		
		/// <summary>
		/// Read/Write property for All Organisation Entities
		/// </summary>
		internal bool AllOrgEntities
		{
			get
			{
				return m_bIsAllOrgEntities;
			}
			set
			{
				m_bIsAllOrgEntities = value;
			}
		}
		#endregion

		private void Initialize()
		{
			DbReader objDbReader = null;
            DataSet objDataSet = null;
            string sSQL = string.Empty;

			try
			{
                //Initialize the collection values
                NameValueCollection nvCollSettings = UTILITY.GetConfigUtilitySettings(m_objUserLogin.objRiskmasterDatabase.ConnectionString, m_iClientId);
                //Copy the original read-only collection so that the collection can be updated
                m_BESSettings.Add(nvCollSettings);
                m_BESSettings[BES_SYSTEMFILEPATH] = UTILITY.GetAppFilesDirectory("BES");

				m_sConnString = m_objUserLogin.objRiskmasterDatabase.ConnectionString;
				m_objDbConnection = DbFactory.GetDbConnection(m_sConnString);
				m_objDbConnection.Open();
                m_objDbCommand = m_objDbConnection.CreateCommand();
                if (m_sCheckDefer=="1")
                {
                    m_objDbTrans = m_objDbConnection.BeginTransaction();
                    m_objDbCommand.Transaction = m_objDbTrans;
                }
                
				m_stDataBaseType = m_objDbConnection.DatabaseType;
				m_sDataBaseName = m_objDbConnection.Database; 			 

				bIsOrgSecEnabled = m_objUserLogin.objRiskmasterDatabase.OrgSecFlag;
				bIsOrgSecAdmin = m_objUserLogin.IsOrgSecAdmin;
				m_iUserId = m_objUserLogin.UserId; 
				m_sDSNId = m_objUserLogin.DatabaseId.ToString();
				m_sSecurityDSN = SecurityDatabase.GetSecurityDsn(m_iClientId);   
                m_sUserLogin = m_objUserLogin.LoginName;    //gagnihotri MITS 11995 Changes made for Audit table
				m_sDBOUserId = m_objUserLogin.objRiskmasterDatabase.RMUserId;
				m_sDBOPwd = m_objUserLogin.objRiskmasterDatabase.RMPassword;
                sBESSynSetting = RMConfigurationManager.GetAppSetting("BESSetupSynchronously", m_sConnString, m_iClientId);//Deb MITS 29215

                // Ash - cloud, Load configurable RM messages from DB
                m_MessageSettings = RMConfigurationSettings.GetRMMessages(m_sConnString, m_iClientId);

				if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
				{
					sSQL = "SELECT * FROM ORG_SECURITY WHERE 0=1";
					//objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
                    objDataSet = null;
                    objDataSet = DbFactory.GetDataSet(m_sConnString, sSQL, m_iClientId);
					//if(objDbReader != null)
                    if (objDataSet.Tables[0]!= null)
					{
                        //Nadim added for Rem
                      XmlDocument  objXml = new XmlDocument();
                      string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                      if (! string.IsNullOrEmpty(strContent))
                        {
                            objXml.LoadXml(strContent);
                          XmlNode  objXmlNode = objXml.SelectSingleNode("//SpecialSettings/ViewOnlyBES");
                            if (objXmlNode != null)
                            {
                                if (objXmlNode.InnerText == "-1")
                                {
                                    //if(UTILITY.IsFieldExists(objDbReader,"GROUP_BROKERS"))
                                    if (UTILITY.IsFieldExists(objDataSet, "GROUP_BROKERS", m_iClientId))
                                        m_bIsBrokerSupport = true;
                                    //if(UTILITY.IsFieldExists(objDbReader,"GROUP_INSURERS"))
                                    if (UTILITY.IsFieldExists(objDataSet, "GROUP_INSURERS", m_iClientId))
                                        m_bIsInsurerSupport = true;
                                }
                                else
                                {
                                    m_bIsBrokerSupport = false;
                                    m_bIsInsurerSupport = false;
                                }
                            }
                        }
					}
				}

                //Parijat: Boeing Broker Support
                if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    sSQL = "SELECT USE_BROKER_BES FROM SYS_PARMS";
                    objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            string sValue = "";
                            sValue = objDbReader[0].ToString();
                            if (sValue =="-1") 
                                g_bBoeingBrokerSupport = true;
                            else
                                g_bBoeingBrokerSupport = false;
                        }
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }
                //
				arDeptList = new ArrayList();
				arParentList = new ArrayList();
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ORGSEC.Initialize.Error", m_iClientId), p_objExp);
			}
			finally
			{   
				if(m_objUserLogin != null)
					m_objUserLogin = null;
                if (objDataSet != null)
                    objDataSet.Dispose();
			}
		}

		/// Name		: LoadOrgSet
		/// Author		: Anurag Agarwal
		/// Date Created	: 15 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Creates Default Views.
		/// </summary>
		public bool LoadOrgSet(XmlDocument p_objXMLDocument)
        {
            #region local variables
            string sSQL = "";
			DbReader objDbReader = null;
			string sTableName = "";
			string sIndexTableName = "";
			ArrayList arrDeferredIndexes = null;
			string sFilePath = "";
			DbConnection objDbConnection = null;
            string sConfRec = string.Empty; //Confidential Record
            #endregion

            try
			{
                if (p_objXMLDocument.SelectSingleNode("//ConfRec") != null)
                {
                    sConfRec = ((XmlElement)p_objXMLDocument.SelectSingleNode("//ConfRec")).InnerText;
                    if (sConfRec == "") sConfRec = "0";

                }
                /*//commented by Nadim for R6
                if (!LoginAdmin(p_objXMLDocument))
					return false;
                //commented by Nadim for R6*/

                //commented by Nadim for R6
                if (IsOrgSetFirstTime())
                {
                    arrDeferredIndexes = new ArrayList();

                    //if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
                    //{
                    //    sSQL = "SELECT TABNAME FROM SYSCAT.TABLES WHERE TYPE = 'T'";
                    //    objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);

                    //    if(objDbReader != null)
                    //    {
                    //        while(objDbReader.Read())
                    //        {
                    //            sTableName = objDbReader["TABNAME"].ToString().ToUpper();
                    //            if(VIEW_TABLES.IndexOf(sTableName + "|") >= 0)
                    //            {
                    //                //Create index statement & queue off for later execution
                    //                if(sTableName.Length > 14) 
                    //                    sIndexTableName = sTableName.Substring(0,14) + "_SDI";
                    //                else
                    //                    sIndexTableName = sTableName + "_SDI";

                    //                sSQL = "CREATE INDEX " + sIndexTableName + " ON " + sTableName + "(SEC_DEPT_EID)";

                    //                arrDeferredIndexes.Add(sSQL); 		

                    //                sSQL = "DROP INDEX " + sIndexTableName;
                    //                DropIndex(sSQL, m_objDbConnection);  
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
                    //        sSQL = "sp_tables NULL, NULL, NULL, N'''TABLE'''";
                    //    else
                    //    {
                    //        //... want list only from creator's account
                    //        sSQL = "SELECT TABLESPACE_NAME,TABLE_NAME FROM USER_TABLES";
                    //    }

                    //    objDbReader = m_objDbConnection.ExecuteReader(sSQL);
                    //    if(objDbReader != null)
                    //    {
                    //        while(objDbReader.Read())
                    //        {
                    //            //if(objDbReader[1].ToString().ToUpper() == m_sDBOUserId.ToUpper())
                    //            //{
                    //                sTableName = objDbReader["TABLE_NAME"].ToString().ToUpper();
                    //                if(VIEW_TABLES.IndexOf(sTableName + "|") >= 0)
                    //                {
                    //                    sSQL = "CREATE INDEX " + sTableName + "_SDI ON " + sTableName + "(SEC_DEPT_EID)";

                    //                    arrDeferredIndexes.Add(sSQL); 		

                    //                    if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                    //                        sSQL = "DROP INDEX " + sTableName + "_SDI";
                    //                    else if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
                    //                        sSQL = "DROP INDEX " + sTableName + "." + sTableName + "_SDI";
                    //                    DropIndex(sSQL, m_objDbConnection);  
                    //                }
                    //            //}
                    //        }
                    //    }

                    //    if(objDbReader != null)
                    //        objDbReader.Dispose();
                    //}

                    //Nuke the ENTITY_MAP tables in case this is a reinstall
                   // RunSql("DELETE FROM ENTITY_MAP", m_objDbConnection);
                   // RunSql("DELETE FROM ENTITY_MAP_USER", m_objDbConnection);
                    //Bulk update the depts throughout the tables. Faster before we install triggers.

                    //if(!BulkUpdateSecDepts())
                    //    return false;

                    //Create indices on Security Access field
                    //if(arrDeferredIndexes.Count > 0)
                    //{
                    //    for(int i = 0;i < arrDeferredIndexes.Count;i++)
                    //        CreateIndex(arrDeferredIndexes[i].ToString(),m_objDbConnection);
                    //}

                    //Create triggers
                    //if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                    //{
                    //    sFilePath = String.Format(@"{0}\{1}", m_BESSettings[BES_SYSTEMFILEPATH], m_BESSettings[BES_CREATETRIGGER_ORACLE]);

                    //    if(!ExcecuteScript(sFilePath,false))
                    //        return false;
                    //}
                    //else if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
                    //{
                    //    sFilePath = String.Format(@"{0}\{1}", m_BESSettings[BES_SYSTEMFILEPATH], m_BESSettings[BES_DROPTRIGGER_SQLSERVER]);
                    //    ExcecuteScript(sFilePath,true);
                    //    sFilePath = String.Format(@"{0}\{1}", m_BESSettings[BES_SYSTEMFILEPATH], m_BESSettings[BES_CREATETRIGGER_SQLSERVER]);
                    //    if(!ExcecuteScript(sFilePath,false))
                    //        return false;
                    //}
                    //if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
                    //{
                    //    sFilePath = String.Format(@"{0}\{1}", m_BESSettings[BES_SYSTEMFILEPATH], m_BESSettings[BES_CREATETRIGGER_DB2]);
                    //    if(!ExcecuteScript(sFilePath,false))
                    //        return false;
                    //}

                    //Saving a default group
                    SaveDefaultGroupDef();

                    objDbConnection = DbFactory.GetDbConnection(m_sSecurityDSN);
                    objDbConnection.Open();

                    //Flip on flag in db that indicates org security is in force
                    //RunSql("UPDATE DATA_SOURCE_TABLE SET DEF_BES_FLAG = -1 WHERE DSNID = " + m_sDSNId, objDbConnection);
                    RunSql("UPDATE DATA_SOURCE_TABLE SET DEF_BES_FLAG = -1, CONF_FLAG =  " + sConfRec + " WHERE DSNID = " + m_sDSNId, objDbConnection);
                }
                //commented by Nadim for R6*/
                //added by nadim for r6
                // SaveDefaultGroupDef();
                else
                {
                    objDbConnection = DbFactory.GetDbConnection(m_sSecurityDSN);
                    objDbConnection.Open();

                    //Flip on flag in db that indicates org security is in force
                    //                RunSql("UPDATE DATA_SOURCE_TABLE SET ORGSEC_FLAG = -1 WHERE DSNID = " + m_sDSNId, objDbConnection);
                    //RunSql("UPDATE DATA_SOURCE_TABLE SET DEF_BES_FLAG = -1 WHERE DSNID = " + m_sDSNId, objDbConnection);
                    RunSql("UPDATE DATA_SOURCE_TABLE SET DEF_BES_FLAG = -1, CONF_FLAG =  " + sConfRec + "  WHERE DSNID = " + m_sDSNId, objDbConnection);
                }
                //added by nadim for r6
				return true;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ORGSEC.LoadOrgSet.Error", m_iClientId), p_objExp);  
			}
			finally
			{
                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
			}
		}

		/// Name		: LoadGroupList
		/// Author		: Anurag Agarwal
		/// Date Created	: 25 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Loads the Current Org Bussiness Entity Securiy Groups list
		/// </summary>
		/// <returns>XML document containing the list</returns>
		public XmlDocument LoadGroupList(XmlDocument p_objXMLDocument)
		{
			StringBuilder sbSQL = null;
			DbReader objDbReader = null;
			XmlElement objControlXMLElement = null;
			XmlElement objLstRow = null;
			XmlElement objRowTxt = null;
            XmlElement objRowStatus = null;

			try
			{
				objControlXMLElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//OrgEntitySecurityList");

				//sSQL = "SELECT * FROM ORG_SECURITY WHERE DELETED_FLAG=0 ORDER BY GROUP_NAME";
                sbSQL = new StringBuilder();
                sbSQL.Append("SELECT * FROM ");
                sbSQL.Append("(SELECT SUPER_GROUP_ID, MIN(GM.UPDATED_FLAG) UPDATED_FLAG,MIN(GM.STATUS_FLAG) STATUS_FLAG,MAX(GM.DELETED_FLAG) DELETED_FLAG,OS.GROUP_NAME ");
                sbSQL.Append("FROM GROUP_MAP GM ");
                sbSQL.Append("INNER JOIN ORG_SECURITY OS ");
                sbSQL.Append(" ON GM.SUPER_GROUP_ID = OS.GROUP_ID ");
                sbSQL.Append(" GROUP BY SUPER_GROUP_ID,OS.GROUP_NAME ) ORG_TABLE ");
                sbSQL.Append(" WHERE DELETED_FLAG =0 ORDER BY GROUP_NAME ");
                objDbReader = DbFactory.GetDbReader(m_sConnString, sbSQL.ToString());
				
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						objLstRow = p_objXMLDocument.CreateElement("Option");
						objRowTxt = p_objXMLDocument.CreateElement("RowId");
                        //abisht MITS 10669
						objRowTxt.InnerText = string.Format("{0:00000}", objDbReader["SUPER_GROUP_ID"]);
						objLstRow.AppendChild(objRowTxt);
						objRowTxt = p_objXMLDocument.CreateElement("GroupName");
						objRowTxt.InnerText = objDbReader["GROUP_NAME"].ToString();
						objLstRow.AppendChild(objRowTxt);
                        //nadim
                        objRowStatus = p_objXMLDocument.CreateElement("GroupStatus");
                        if (objDbReader.GetInt32("UPDATED_FLAG") == -1 && objDbReader.GetInt32("STATUS_FLAG") == 0)
                        {
                            objRowStatus.InnerText = "In Process";
                        }
                        else if (objDbReader.GetInt32("UPDATED_FLAG") == -1 && objDbReader.GetInt32("STATUS_FLAG") == -1)
                        {
                            objRowStatus.InnerText = "Failed";
                        }
                        else
                        {
                            objRowStatus.InnerText = "Completed";
                        }
                        objLstRow.AppendChild(objRowStatus);
                        //nadim 
						objControlXMLElement.AppendChild((XmlNode)objLstRow); 
					}
				}

				return p_objXMLDocument;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ORGSEC.LoadGroupList.Error", m_iClientId), p_objExp);
			}
			finally
			{
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
			}
		}

		/// Name		: SaveDefaultGroupDef
		/// Author		: Anurag Agarwal
		/// Date Created	: 25 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves the Default Group defination for the GroupId
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group to be Saved</param>
		/// <returns>True for success & False for failure</returns>
		private void SaveDefaultGroupDef()
        {
            #region local variables
            string sSQL = "";
			int iGroupId = 0;
			string sGroupName = "";
			string sEncript1 = "";
			string sEncript2 = "";
			DbConnection objDbConn = null;
			LocalCache objLocalCache = null;
			DbReader objDbReader = null;
			bool bIsGroupNameExist = false;
            bool bIsGroupIdExist = false;
            List<int> lstGroupIds = new List<int>(); //pmittal5
            #endregion

            try
			{
                sGroupName = m_BESSettings["BES_DefaultGroupName"].ToString();

				//Check for the presence of Default group
				sSQL = "SELECT * FROM ORG_SECURITY WHERE GROUP_NAME='" + sGroupName + "'";
				objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
				if(objDbReader != null)
				{
					if(objDbReader.Read())
						bIsGroupNameExist = true;
                    if (bIsGroupNameExist)
                        iGroupId = objDbReader.GetInt32("GROUP_ID");
				}
				if(objDbReader != null)
					objDbReader.Close();

                objLocalCache = new LocalCache(m_sConnString, m_iClientId);
                //Code Added by nadim for BES
                //added  DBMS_IS_SQLSRVR to make sure we are able to have multiple Databases BES enabled based on 
                //if one security database is there in one SQLserver
                //if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                if (!bIsGroupNameExist)
                {
                    if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE || m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                    {
                        sSQL = "SELECT MAX(GROUP_ID) FROM USER_ORGSEC";
                        objDbReader = DbFactory.GetDbReader(m_sSecurityDSN, sSQL);
                        if (objDbReader != null)
                        {
                            if (objDbReader.Read())
                                iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;
                            else
                                iGroupId = 1;
                            if (objDbReader != null)
                                objDbReader.Close();
                        }
                    }
                }
                //Code Added by  nadim for BES
                //else if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
				
				//Clear out new ORG_SECURITY_EIDS table
                //it is possible not all installations will have this table
                //Deb :MITS 29627
                //sSQL = String.Format("DELETE FROM ORG_SECURITY_EIDS WHERE GROUP_ID = {0}", iGroupId);
                //RunSql(sSQL, m_objDbConnection);
                //Deb :MITS 29627
				

				//Write entity selection info (used to reconstruct ENTITY_MAP if necessary)
				//Update ORG_SECURITY profile
				if(!bIsGroupNameExist)  
				{
					sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ENTITIES,DELETED_FLAG) VALUES(" + iGroupId + "," + 
						Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + "<ALL>" + "',0)";
					RunSql(sSQL, m_objDbConnection);
                    sSQL = "INSERT INTO GROUP_MAP (SUPER_GROUP_ID,GROUP_ID,DELETED_FLAG) VALUES(" + iGroupId + "," + iGroupId + ",0)";
                    RunSql(sSQL, m_objDbConnection);
                    //End - pmittal5
				}

                //Deb : MITS 33316 
                try
                {
                    sSQL = "INSERT INTO ORG_SECURITY_USER (GROUP_ID,USER_ID) VALUES (" + iGroupId + "," + m_iUserId + ")";
                    RunSql(sSQL, m_objDbConnection);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("PK_ORG_SECURITY_USER"))
                    {
                        return;
                    }
                    else
                    {
                        throw ex;
                    }
                }
                //Deb : MITS 33316 

				if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
				{
					//DBO signals that the login routines should use the DSN login
					sEncript1 = RMCryptography.EncryptString("DBO");
					sEncript2 = RMCryptography.EncryptString("DBO");
				}
				else
				{
					sEncript1 = RMCryptography.EncryptString(m_sDBOPwd);
					sEncript2 = RMCryptography.EncryptString(m_sDBOUserId);
				}

				objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
				objDbConn.Open();
                //gagnihotri MITS 11995 Changes made for Audit table
                sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES (" + m_sDSNId + "," + m_iUserId + 
                    ",'" + sEncript2 + "'," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
				RunSql(sSQL,objDbConn);
                lstGroupIds.Add(iGroupId); //pmittal5
				if(objDbConn != null)
					objDbConn.Close();

				m_bIsAllOrgEntities = true;
                //Deb MITS 29215
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
                {
                    BuildEntityMap_SQLSybase(iGroupId, "", "", lstGroupIds, false,true,"NEW");
                }
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    //BuildEntityMap_Oracle(iGroupId, "", "", "",true);
                    BuildEntityMap_Oracle(iGroupId, "", "", "", lstGroupIds, false, true, "NEW");
                }
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
                {
                    //BuildEntityMap_DB2(iGroupId, "", "", "");
                    BuildEntityMap_DB2(iGroupId, "", "", "", lstGroupIds, true, "NEW");
                }
                //Deb MITS 29215
			}
			finally
			{
				if(objLocalCache != null)
					objLocalCache.Dispose();

				if(objDbReader != null)
					objDbReader.Dispose();

				if(objDbConn != null)
					objDbConn.Dispose();
			}
		}

        /// <summary>
        /// Parijat --In order to meet the r5 needs
        /// </summary>
        /// <param name="p_objXMLDocument"></param>
        /// <returns></returns>
        public XmlDocument LoadGroupDef(XmlDocument p_objXMLDocument)
        {
            #region local variables
            string sSQL = "";
            DbReader objDbReader = null;
            int iGroupId = 0;
            XmlNodeList objXmlNodeList = null;
            XmlElement objSelectElement = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            XmlNodeList objNodLst = null;
            XmlElement objChildElm = null;
            string sTemp = "";
            string sGroupEntity = "";
            int iPos = 0;
            int iEId = 0;
            LocalCache objLocalCache = null;
            
            string sAbbr = "";
            string sName = "";
            Hashtable objHashtable = null;
            string sFname = "";
            string sLname = "";
            string sListItemsValues = "";
            string sTempTitle = "";

            XmlDocument objXml = null;               //Umesh
            XmlNode objSessionDSN = null;               // Umesh
            XmlNode objViewEnhanceBES = null; //Umesh
            DbReader objRdr = null;                   //Umesh
            bool bViewEnhanceBES = false;               //Umesh
            string sValue = String.Empty, strContent = string.Empty;
            StringBuilder sGroupIds = new StringBuilder(); 
            int iConfRec = 0;
            #endregion

            try
            {
                objLocalCache = new LocalCache(m_sConnString, m_iClientId);

                iConfRec = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//ConfRec")).InnerText); 
                if (p_objXMLDocument != null)
                {
                    objXmlNodeList = p_objXMLDocument.SelectSingleNode("BESGroup").ChildNodes;//.GetElementsByTagName("control");
                }
                #region Boeing
                ////PJS :02-18-2009 - New setting USE_BROKER_BES  

                //sSQL = "SELECT USE_BROKER_BES FROM SYS_PARMS";
                //objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                //if (objDbReader != null)
                //{
                //    if (objDbReader.Read())
                //    {
                //        sValue = objDbReader[0].ToString();
                //        if (sValue =="-1") 
                //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//UseBrokerBes")).InnerText = "1";
                //        else
                //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//UseBrokerBes")).InnerText = "0";
                //    }
                //}
                //if (objDbReader != null)
                //    objDbReader.Close();
                if (g_bBoeingBrokerSupport == true)
                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//UseBrokerBes")).InnerText = "1";
                else
                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//UseBrokerBes")).InnerText = "0";
                #endregion
                iGroupId = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//RowId")).InnerText);

                if (iGroupId == 0)
                {
                    //Nadim-in case of oracle  db group name is coming different in case of new and edit.

                    //pmittal5 Mits 20486- Password change functionality in case of SQL also.
                    //if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                    if (iConfRec == 0)
                    {
                        sSQL = "SELECT MAX(GROUP_ID) FROM USER_ORGSEC";
                        objDbReader = DbFactory.GetDbReader(m_sSecurityDSN, sSQL);
                        if (objDbReader != null)
                        {
                            if (objDbReader.Read())
                                iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;
                            else
                                iGroupId = 1;

                        }
                        if (objDbReader != null)
                            objDbReader.Close();

                        ((XmlElement)p_objXMLDocument.SelectSingleNode("//RowId")).InnerText = iGroupId.ToString();
                        //((XmlElement)p_objXMLDocument.SelectSingleNode("//labGroupName")).InnerText = "sg" + string.Format("{0:00000}", iGroupId);
                        ((XmlElement)p_objXMLDocument.SelectSingleNode("//labGroupName")).InnerText = FormatBESUserName(Conversion.ConvertStrToInteger(m_sDSNId), m_sConnString, iGroupId);

                    }
                    //Nadim-in case of oracle  db group name is coming different in case of new and edit.

                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectOrgEntitiesGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectOrgEntitiesGroupTitle").InnerText = "New Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectAdjusterGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectAdjusterGroupTitle").InnerText = "New Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectBrokerInsurerDefGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectBrokerInsurerDefGroupTitle").InnerText = "New Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectUserAssigGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectUserAssigGroupTitle").InnerText = "New Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//DB2LoginAccountInfoGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//DB2LoginAccountInfoGroupTitle").InnerText = "New Security Group" + sTempTitle;

                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//BrokerSupport")).InnerText = Conversion.ConvertBoolToInt(m_bIsBrokerSupport, m_iClientId).ToString();
                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//InsurerSupport")).InnerText = Conversion.ConvertBoolToInt(m_bIsInsurerSupport, m_iClientId).ToString();
                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//DBType")).InnerText = Conversion.ConvertObjToInt(m_stDataBaseType, m_iClientId).ToString();

                    //To send value for defer check
                    objXml = new XmlDocument();
                    strContent = RMSessionManager.GetCustomContent(m_iClientId);
                    if (!string.IsNullOrEmpty(strContent))
                    {
                        objXml.LoadXml(strContent);
                        //objRdr.Dispose();
                        objViewEnhanceBES = objXml.SelectSingleNode("//SpecialSettings/EnhanceBES");
                        if (objViewEnhanceBES != null)
                        {
                            if (objViewEnhanceBES.InnerText == "-1")
                            {
                                ((XmlElement)p_objXMLDocument.SelectSingleNode("//ViewEnhanceBES")).InnerText = "yes";
                            }
                        }
                    }
                    //To send value for defer check
                    return p_objXMLDocument;
                }
                else
                {
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectOrgEntitiesGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectOrgEntitiesGroupTitle").InnerText = "Edit Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectAdjusterGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectAdjusterGroupTitle").InnerText = "Edit Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectBrokerInsurerDefGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectBrokerInsurerDefGroupTitle").InnerText = "Edit Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//SelectUserAssigGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//SelectUserAssigGroupTitle").InnerText =  "Edit Security Group" + sTempTitle;
                    sTempTitle = p_objXMLDocument.SelectSingleNode("//DB2LoginAccountInfoGroupTitle").InnerText;
                    p_objXMLDocument.SelectSingleNode("//DB2LoginAccountInfoGroupTitle").InnerText = "Edit Security Group" + sTempTitle;
                }

                ((XmlElement)p_objXMLDocument.SelectSingleNode("//BrokerSupport")).InnerText = Conversion.ConvertBoolToInt(m_bIsBrokerSupport, m_iClientId).ToString();
                ((XmlElement)p_objXMLDocument.SelectSingleNode("//InsurerSupport")).InnerText = Conversion.ConvertBoolToInt(m_bIsInsurerSupport, m_iClientId).ToString();
                ((XmlElement)p_objXMLDocument.SelectSingleNode("//DBType")).InnerText = Conversion.ConvertObjToInt(m_stDataBaseType, m_iClientId).ToString();

                // Nadim changed for Assisgning Oracle role to bes user-start
                XmlElement oOracleRole = (XmlElement)p_objXMLDocument.SelectSingleNode("//OracleRole");
                if (oOracleRole != null)
                {
                    GetBESOraRole = oOracleRole.InnerText;
                }
                // Nadim changed for Assisgning Oracle role to bes user-end
                objHashtable = new Hashtable();

                sSQL = "SELECT * FROM ORG_SECURITY WHERE GROUP_ID = " + iGroupId;
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        foreach (XmlElement objXmlElement in objXmlNodeList)
                        {
                            if (!objHashtable.ContainsKey(objXmlElement.Name))//.GetAttribute("name")
                            {
                                objHashtable.Add(objXmlElement.Name, objXmlElement.Name);//.GetAttribute("name")

                                switch (objXmlElement.Name.ToLower())//.GetAttribute("name")
                                {
                                    #region "Case groupname"
                                    case "groupname":
                                        objXmlElement.InnerText = objDbReader["GROUP_NAME"].ToString();
                                        break;
                                    #endregion

                                    #region "Case optorgentities"
                                    case "optorgentitiesvalue": //optorgentities"
                                        if (objDbReader["GROUP_ENTITIES"].ToString().Trim() == "<ALL>")
                                            p_objXMLDocument.SelectSingleNode("//optOrgEntitiesValue").InnerText = "1";
                                        else
                                        {
                                            p_objXMLDocument.SelectSingleNode("//optOrgEntitiesValue").InnerText = "0";

                                            sTemp = objDbReader["GROUP_ENTITIES"].ToString();

                                            objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//SelEntities");
                                            objNodLst = objSelectElement.GetElementsByTagName("rowhead");
                                            objLstRow = p_objXMLDocument.CreateElement("listrow");

                                            sListItemsValues = "";
                                            while (sTemp != "")
                                            {
                                                iPos = sTemp.IndexOf(",");
                                                if (iPos == -1)
                                                    iEId = Conversion.ConvertStrToInteger(sTemp);
                                                else
                                                    iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0, iPos));
                                                sGroupEntity = "[" + Utilities.GetOrgTableName(objLocalCache.GetOrgTableId(iEId)) + "]";

                                                for (int j = 0; j < objNodLst.Count; j++)
                                                {
                                                    objChildElm = (XmlElement)objNodLst[j];

                                                    objRowTxt = p_objXMLDocument.CreateElement("rowtext");
                                                    objRowTxt.SetAttribute("type", "label");
                                                    objRowTxt.SetAttribute("name", objChildElm.GetAttribute("colname"));
                                                    objRowTxt.SetAttribute("value", iEId.ToString());
                                                    objLocalCache.GetOrgInfo(iEId, ref sAbbr, ref sName);
                                                    objRowTxt.SetAttribute("title", sGroupEntity + " " + sName);

                                                    sListItemsValues = sListItemsValues + objRowTxt.GetAttribute("title") + "|" + objRowTxt.GetAttribute("value") + "|";
                                                    objLstRow.AppendChild(objRowTxt);
                                                }
                                                if (iPos == -1)
                                                    sTemp = "";
                                                else
                                                    sTemp = sTemp.Substring(iPos + 1);
                                            }
                                            objSelectElement.AppendChild((XmlNode)objLstRow);
                                            if (sListItemsValues != "")
                                            {
                                                if (sListItemsValues.IndexOf("|") != -1)
                                                    sListItemsValues = sListItemsValues.Substring(0, sListItemsValues.Length - 1);
                                            }
                                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//SelEntitiesListItems")).InnerText = sListItemsValues;
                                        }
                                        break;
                                    #endregion

                                    #region "Case optbrokers"
                                    case "optbrokersvalue": //optbrokers
                                        if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                                        {
                                            if (m_bIsBrokerSupport)
                                            {
                                                sTemp = objDbReader["GROUP_BROKERS"].ToString();
                                                if (sTemp != "")
                                                {
                                                    p_objXMLDocument.SelectSingleNode("//optBrokersValue").InnerText = "2";

                                                    objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//Brokers");
                                                    objNodLst = objSelectElement.GetElementsByTagName("rowhead");
                                                    objLstRow = p_objXMLDocument.CreateElement("listrow");

                                                    while (sTemp != "")
                                                    {
                                                        iPos = sTemp.IndexOf(",");
                                                        if (iPos == -1)
                                                            iEId = Conversion.ConvertStrToInteger(sTemp);
                                                        else
                                                            iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0, iPos));

                                                        if (iEId != 0)
                                                        {
                                                            for (int j = 0; j < objNodLst.Count; j++)
                                                            {
                                                                objChildElm = (XmlElement)objNodLst[j];

                                                                objRowTxt = p_objXMLDocument.CreateElement("rowtext");
                                                                objRowTxt.SetAttribute("type", "label");
                                                                objRowTxt.SetAttribute("name", objChildElm.GetAttribute("colname"));
                                                                objRowTxt.SetAttribute("value", iEId.ToString());
                                                                objLocalCache.GetEntityInfo(iEId, ref sFname, ref sLname);
                                                                objRowTxt.SetAttribute("title", sFname + ", " + sLname);
                                                                objLstRow.AppendChild(objRowTxt);
                                                            }
                                                        }
                                                        if (iPos == -1)
                                                            sTemp = "";
                                                        else
                                                            sTemp = sTemp.Substring(iPos + 1);
                                                    }
                                                    objSelectElement.AppendChild((XmlNode)objLstRow);
                                                }
                                                else
                                                    p_objXMLDocument.SelectSingleNode("//optBrokersValue").InnerText =  "1";
                                            }

                                            if (m_bIsInsurerSupport)
                                            {
                                                sTemp = objDbReader["GROUP_INSURERS"].ToString();
                                                if (sTemp != "")
                                                {
                                                    p_objXMLDocument.SelectSingleNode("//optInsurersValue").InnerText = "2";

                                                    objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//Insurers");
                                                    objNodLst = objSelectElement.GetElementsByTagName("rowhead");
                                                    objLstRow = p_objXMLDocument.CreateElement("listrow");

                                                    while (sTemp != "")
                                                    {
                                                        iPos = sTemp.IndexOf(",");
                                                        if (iPos == -1)
                                                            iEId = Conversion.ConvertStrToInteger(sTemp);
                                                        else
                                                            iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0, iPos));

                                                        if (iEId != 0)
                                                        {
                                                            for (int j = 0; j < objNodLst.Count; j++)
                                                            {
                                                                objChildElm = (XmlElement)objNodLst[j];

                                                                objRowTxt = p_objXMLDocument.CreateElement("rowtext");
                                                                objRowTxt.SetAttribute("type", "label");
                                                                objRowTxt.SetAttribute("name", objChildElm.GetAttribute("colname"));
                                                                objRowTxt.SetAttribute("value", iEId.ToString());
                                                                objLocalCache.GetEntityInfo(iEId, ref sFname, ref sLname);
                                                                objRowTxt.SetAttribute("title", sFname + ", " + sLname);
                                                                objLstRow.AppendChild(objRowTxt);
                                                            }
                                                        }
                                                        if (iPos == -1)
                                                            sTemp = "";
                                                        else
                                                            sTemp = sTemp.Substring(iPos + 1);
                                                    }
                                                    objSelectElement.AppendChild((XmlNode)objLstRow);
                                                }
                                                else
                                                    p_objXMLDocument.SelectSingleNode("//optInsurersValue").InnerText = "1";
                                            }
                                        }
                                        if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)//Parijat: Boeing Broker Support
                                        {
                                            if (g_bBoeingBrokerSupport)
                                            {
                                                sTemp = objDbReader["GROUP_BROKERS"].ToString();
                                                if (sTemp != "")
                                                {
                                                    p_objXMLDocument.SelectSingleNode("//optBrokersValue").InnerText = "2";

                                                    objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//Brokers");
                                                    objNodLst = objSelectElement.GetElementsByTagName("rowhead");
                                                    objLstRow = p_objXMLDocument.CreateElement("listrow");

                                                    while (sTemp != "")
                                                    {
                                                        iPos = sTemp.IndexOf(",");
                                                        if (iPos == -1)
                                                            iEId = Conversion.ConvertStrToInteger(sTemp);
                                                        else
                                                            iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0, iPos));

                                                        if (iEId != 0)
                                                        {
                                                            for (int j = 0; j < objNodLst.Count; j++)
                                                            {
                                                                objChildElm = (XmlElement)objNodLst[j];

                                                                objRowTxt = p_objXMLDocument.CreateElement("rowtext");
                                                                objRowTxt.SetAttribute("type", "label");
                                                                objRowTxt.SetAttribute("name", objChildElm.GetAttribute("colname"));
                                                                objRowTxt.SetAttribute("value", iEId.ToString());
                                                                objLocalCache.GetEntityInfo(iEId, ref sFname, ref sLname);
                                                                objRowTxt.SetAttribute("title", sFname + ", " + sLname);
                                                                objLstRow.AppendChild(objRowTxt);
                                                            }
                                                        }
                                                        if (iPos == -1)
                                                            sTemp = "";
                                                        else
                                                            sTemp = sTemp.Substring(iPos + 1);
                                                    }
                                                    objSelectElement.AppendChild((XmlNode)objLstRow);
                                                }
                                                else
                                                    p_objXMLDocument.SelectSingleNode("//optBrokersValue").InnerText = "1";
                                            }
                                        }
                                        break;
                                    #endregion

                                    #region "Case optadjtexttypes"
                                    case "optadjtexttypesvalue":   //optadjtexttypes
                                        try
                                        {
                                            sTemp = objDbReader["GROUP_ADJ_TEXT_TYPES"].ToString();
                                            if (sTemp != "")
                                            {
                                                if (sTemp.Substring(0, 1) == "-")
                                                {
                                                    p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue").InnerText = "3";
                                                    sTemp = sTemp.Substring(1);
                                                }
                                                else
                                                    p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue").InnerText = "2";

                                                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//AdjTextTypes");
                                                objNodLst = objSelectElement.GetElementsByTagName("rowhead");
                                                objLstRow = p_objXMLDocument.CreateElement("listrow");

                                                sListItemsValues = "";
                                                while (sTemp != "")
                                                {
                                                    iPos = sTemp.IndexOf(",");
                                                    if (iPos == -1)
                                                        iEId = Conversion.ConvertStrToInteger(sTemp);
                                                    else
                                                        iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0, iPos));

                                                    if (iEId != 0)
                                                    {
                                                        for (int j = 0; j < objNodLst.Count; j++)
                                                        {
                                                            objChildElm = (XmlElement)objNodLst[j];

                                                            objRowTxt = p_objXMLDocument.CreateElement("rowtext");
                                                            objRowTxt.SetAttribute("type", "label");
                                                            objRowTxt.SetAttribute("name", objChildElm.GetAttribute("colname"));
                                                            objRowTxt.SetAttribute("value", iEId.ToString());
                                                            objRowTxt.SetAttribute("title", objLocalCache.GetShortCode(iEId) + " - " + objLocalCache.GetCodeDesc(iEId,this.LanguageCode)); //tmalhotra3- ML Changes

                                                            sListItemsValues = sListItemsValues + objRowTxt.GetAttribute("title") + "|" + objRowTxt.GetAttribute("value") + "|";

                                                            objLstRow.AppendChild(objRowTxt);
                                                        }
                                                    }
                                                    if (iPos == -1)
                                                        sTemp = "";
                                                    else
                                                        sTemp = sTemp.Substring(iPos + 1);
                                                }
                                                objSelectElement.AppendChild((XmlNode)objLstRow);
                                                if (sListItemsValues != "")
                                                {
                                                    if (sListItemsValues.IndexOf("|") != -1)
                                                        sListItemsValues = sListItemsValues.Substring(0, sListItemsValues.Length - 1);
                                                }
                                                ((XmlElement)p_objXMLDocument.SelectSingleNode("//AdjusterListItems")).InnerText = sListItemsValues;
                                            }
                                            else
                                                p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue").InnerText = "1";
                                        }
                                        catch (Exception e)
                                        {
                                            p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue").InnerText = "1";
                                        }

                                        break;

                                    #endregion
                                    #region "Case optEnhNoteTypesValue"
                                    case "optenhnotetypesvalue":   //optEnhNoteTypesValue
                                        try
                                        {
                                            sTemp = objDbReader["GROUP_ENH_NOTES_TYPES"].ToString();
                                            if (sTemp != "")
                                            {
                                                if (sTemp.Substring(0, 1) == "-")
                                                {
                                                    p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue").InnerText = "3";
                                                    sTemp = sTemp.Substring(1);
                                                }
                                                else
                                                    p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue").InnerText = "2";

                                                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//EnhNoteTypes");
                                                objNodLst = objSelectElement.GetElementsByTagName("rowhead");
                                                objLstRow = p_objXMLDocument.CreateElement("listrow");

                                                sListItemsValues = "";
                                                while (sTemp != "")
                                                {
                                                    iPos = sTemp.IndexOf(",");
                                                    if (iPos == -1)
                                                        iEId = Conversion.ConvertStrToInteger(sTemp);
                                                    else
                                                        iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0, iPos));

                                                    if (iEId != 0)
                                                    {
                                                        for (int j = 0; j < objNodLst.Count; j++)
                                                        {
                                                            objChildElm = (XmlElement)objNodLst[j];

                                                            objRowTxt = p_objXMLDocument.CreateElement("rowtext");
                                                            objRowTxt.SetAttribute("type", "label");
                                                            objRowTxt.SetAttribute("name", objChildElm.GetAttribute("colname"));
                                                            objRowTxt.SetAttribute("value", iEId.ToString());
                                                            objRowTxt.SetAttribute("title", objLocalCache.GetShortCode(iEId) + " - " + objLocalCache.GetCodeDesc(iEId,this.LanguageCode));//tmalhotra3 ML Changes

                                                            sListItemsValues = sListItemsValues + objRowTxt.GetAttribute("title") + "|" + objRowTxt.GetAttribute("value") + "|";

                                                            objLstRow.AppendChild(objRowTxt);
                                                        }
                                                    }
                                                    if (iPos == -1)
                                                        sTemp = "";
                                                    else
                                                        sTemp = sTemp.Substring(iPos + 1);
                                                }
                                                objSelectElement.AppendChild((XmlNode)objLstRow);
                                                if (sListItemsValues != "")
                                                {
                                                    if (sListItemsValues.IndexOf("|") != -1)
                                                        sListItemsValues = sListItemsValues.Substring(0, sListItemsValues.Length - 1);
                                                }
                                                ((XmlElement)p_objXMLDocument.SelectSingleNode("//NotesListItems")).InnerText = sListItemsValues;
                                            }
                                            else
                                                p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue").InnerText = "1";
                                        }
                                        catch (Exception e)
                                        {
                                            p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue").InnerText = "1";
                                        }

                                        break;

                                    #endregion
                                }
                            }
                        }
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }

                sSQL = "SELECT GROUP_ID FROM GROUP_MAP WHERE SUPER_GROUP_ID = " + iGroupId;
                using (objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            if (sGroupIds.Length > 0)
                                sGroupIds.Append(", " + objDbReader[0].ToString());
                            else
                                sGroupIds.Append(objDbReader[0].ToString());
                        }
                    }
                }//End- pmittal5
                sSQL = "SELECT * FROM ORG_SECURITY_USER WHERE GROUP_ID IN (" + sGroupIds + ")";
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                if (objDbReader != null)
                {
                    objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//Users");
                    objNodLst = objSelectElement.GetElementsByTagName("rowhead");
                    objLstRow = p_objXMLDocument.CreateElement("listrow");

                    sListItemsValues = "";
                    while (objDbReader.Read())
                    {
                        for (int j = 0; j < objNodLst.Count; j++)
                        {
                            objChildElm = (XmlElement)objNodLst[j];

                            objRowTxt = p_objXMLDocument.CreateElement("rowtext");
                            objRowTxt.SetAttribute("type", "label");
                            objRowTxt.SetAttribute("name", objChildElm.GetAttribute("colname"));
                            objRowTxt.SetAttribute("value", objDbReader["USER_ID"].ToString());
                            objRowTxt.SetAttribute("title", UTILITY.GetUser(Conversion.ConvertStrToInteger(objDbReader["USER_ID"].ToString()), m_sDSNId, m_sSecurityDSN, m_iClientId));

                            sListItemsValues = sListItemsValues + objRowTxt.GetAttribute("title") + "|" + objRowTxt.GetAttribute("value") + "|";

                            objLstRow.AppendChild(objRowTxt);
                        }
                    }
                    objSelectElement.AppendChild((XmlNode)objLstRow);
                    if (sListItemsValues != "")
                    {
                        if (sListItemsValues.IndexOf("|") != -1)
                            sListItemsValues = sListItemsValues.Substring(0, sListItemsValues.Length - 1);
                    }
                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//UserListItems")).InnerText = sListItemsValues;

                }
                if (objDbReader != null)
                    objDbReader.Close();


                //abisht 10669
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE && iConfRec == 0)
                {
                    

                    sSQL = "SELECT * FROM USER_ORGSEC WHERE DSN_ID = " + m_sDSNId + " AND GROUP_ID IN (" + sGroupIds + ")";
                    objDbReader = DbFactory.GetDbReader(m_sSecurityDSN, sSQL);
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//PassWord")).InnerText = RMCryptography.DecryptString(objDbReader["DB_PWD"].ToString());
                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//UserId")).InnerText = RMCryptography.DecryptString(objDbReader["DB_UID"].ToString());
                        }
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }
                // REM Umesh 06/01/2007
                objXml = new XmlDocument();
                strContent = RMSessionManager.GetCustomContent(m_iClientId);
                if (! string.IsNullOrEmpty(strContent))
                {
                    objXml.LoadXml(strContent);
                    //objRdr.Dispose();
                    objViewEnhanceBES = objXml.SelectSingleNode("//SpecialSettings/EnhanceBES");
                    if (objViewEnhanceBES != null)
                    {
                        if (objViewEnhanceBES.InnerText == "-1")
                        {
                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//ViewEnhanceBES")).InnerText = "yes";
                        }
                    }
                }
                
                return p_objXMLDocument;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("ORGSEC.LoadGroupDef.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objLocalCache != null)
                {
                    //objLocalCache.Clear();
                    objLocalCache.Dispose();
                }
                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                if (objRdr != null)
                {
                    objRdr.Close();
                    objRdr.Dispose();
                }
            }
        }

		/// Name		: LoadGroupDef
		/// Author		: Anurag Agarwal
		/// Date Created	: 25 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Loads the Group defination for the GroupId
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group to be Loaded</param>
		/// <returns>XMLDocument containing Group def details</returns>
        //public XmlDocument LoadGroupDef_old(XmlDocument p_objXMLDocument)
        //{
        //    #region local variables
        //    string sSQL = "";
        //    DbReader objDbReader = null;
        //    int iGroupId = 0;
        //    XmlNodeList objXmlNodeList = null;
        //    XmlElement objSelectElement = null;
        //    XmlElement objLstRow = null;
        //    XmlElement objRowTxt = null;
        //    XmlNodeList objNodLst = null;
        //    XmlElement objChildElm = null;
        //    string sTemp = "";
        //    string sGroupEntity = "";
        //    int iPos = 0;
        //    int iEId = 0;
        //    LocalCache objLocalCache = null;
			
        //    string sAbbr = "";
        //    string sName = "";
        //    Hashtable objHashtable = null;
        //    string sFname="";
        //    string sLname="";
        //    string sListItemsValues = "";
        //    string sTempTitle = "";

        //    XmlDocument objXml = null;               //Umesh
        //    XmlNode objSessionDSN = null;               // Umesh
        //    XmlNode objViewEnhanceBES = null; //Umesh
        //    DbReader objRdr = null;                   //Umesh
        //    bool bViewEnhanceBES = false;               //Umesh
        //    #endregion

        //    try
        //    {
        //        objLocalCache = new LocalCache(m_sConnString);

        //        if(p_objXMLDocument != null)
        //        {
        //            objXmlNodeList = p_objXMLDocument.GetElementsByTagName("control");  
        //        }
				
        //        iGroupId = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText); 

        //        if(iGroupId == 0)
        //        {
        //            if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
        //            {
        //                sSQL = "SELECT MAX(GROUP_ID) FROM ORG_SECURITY";
        //                objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
        //                if(objDbReader != null)
        //                {
        //                    if(objDbReader.Read())
        //                        iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1; 
        //                    else
        //                        iGroupId = 1;
        //                }
        //                if (objDbReader != null)
        //                    objDbReader.Close();
						
        //                ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText = iGroupId.ToString();
        //                ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='labGroupName']")).InnerText = "sg" + string.Format("{0:00000}",iGroupId);
        //            }
					
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectOrgEntities']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectOrgEntities']")).SetAttribute("title","New Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectAdjuster']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectAdjuster']")).SetAttribute("title","New Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectBrokerInsurerDef']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectBrokerInsurerDef']")).SetAttribute("title","New Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectUserAssig']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectUserAssig']")).SetAttribute("title","New Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='DB2LoginAccountInfo']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='DB2LoginAccountInfo']")).SetAttribute("title","New Security Group" + sTempTitle);
                    
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='BrokerSupport']")).InnerText = Conversion.ConvertBoolToInt(m_bIsBrokerSupport).ToString();
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='InsurerSupport']")).InnerText = Conversion.ConvertBoolToInt(m_bIsInsurerSupport).ToString();
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='DBType']")).InnerText = Conversion.ConvertObjToInt(m_stDataBaseType).ToString();
					
        //            return p_objXMLDocument;
        //        }
        //        else
        //        {
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectOrgEntities']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectOrgEntities']")).SetAttribute("title","Edit Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectAdjuster']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectAdjuster']")).SetAttribute("title","Edit Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectBrokerInsurerDef']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectBrokerInsurerDef']")).SetAttribute("title","Edit Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectUserAssig']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='SelectUserAssig']")).SetAttribute("title","Edit Security Group" + sTempTitle);
        //            sTempTitle = ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='DB2LoginAccountInfo']")).GetAttribute("title");
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='DB2LoginAccountInfo']")).SetAttribute("title","Edit Security Group" + sTempTitle);
        //        }

        //        ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='BrokerSupport']")).InnerText = Conversion.ConvertBoolToInt(m_bIsBrokerSupport).ToString();
        //        ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='InsurerSupport']")).InnerText = Conversion.ConvertBoolToInt(m_bIsInsurerSupport).ToString();
        //        ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='DBType']")).InnerText = Conversion.ConvertObjToInt(m_stDataBaseType).ToString();

        //        objHashtable = new Hashtable();

        //        sSQL = "SELECT * FROM ORG_SECURITY WHERE GROUP_ID = " + iGroupId;	
        //        objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
        //        if(objDbReader != null)
        //        {
        //            if(objDbReader.Read())
        //            {
        //                foreach(XmlElement objXmlElement in objXmlNodeList)
        //                {
        //                    if(!objHashtable.ContainsKey(objXmlElement.GetAttribute("name")))
        //                    {
        //                        objHashtable.Add(objXmlElement.GetAttribute("name"),objXmlElement.GetAttribute("name"));

        //                        switch(objXmlElement.GetAttribute("name").ToLower())
        //                        {
        //                                #region "Case groupname"
        //                            case "groupname":
        //                                objXmlElement.InnerText = objDbReader["GROUP_NAME"].ToString();  
        //                                break;
        //                                #endregion

        //                                #region "Case optorgentities"
        //                            case "optorgentities":
        //                                if(objDbReader["GROUP_ENTITIES"].ToString().Trim() == "<ALL>")
        //                                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optOrgEntities']")).SetAttribute("value","1");
        //                                else
        //                                {
        //                                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optOrgEntities']")).SetAttribute("value","0");  

        //                                    sTemp = objDbReader["GROUP_ENTITIES"].ToString();

        //                                    objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='SelEntities']"); 
        //                                    objNodLst  = objSelectElement.GetElementsByTagName("rowhead");
        //                                    objLstRow = p_objXMLDocument.CreateElement("listrow");

        //                                    sListItemsValues = "";
        //                                    while(sTemp != "")
        //                                    {
        //                                        iPos = sTemp.IndexOf(",");
        //                                        if(iPos == -1)
        //                                            iEId = Conversion.ConvertStrToInteger(sTemp);
        //                                        else
        //                                            iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0,iPos));
        //                                        sGroupEntity = "[" + Utilities.GetOrgTableName(objLocalCache.GetOrgTableId(iEId)) + "]";

        //                                        for(int j = 0 ; j< objNodLst.Count;j++)
        //                                        {
        //                                            objChildElm = (XmlElement)objNodLst[j];

        //                                            objRowTxt = p_objXMLDocument.CreateElement("rowtext");
        //                                            objRowTxt.SetAttribute("type" , "label");
        //                                            objRowTxt.SetAttribute("name" , objChildElm.GetAttribute("colname"));
        //                                            objRowTxt.SetAttribute("value" , iEId.ToString());
        //                                            objLocalCache.GetOrgInfo(iEId,ref sAbbr,ref sName);
        //                                            objRowTxt.SetAttribute("title", sGroupEntity + " " + sName);

        //                                            sListItemsValues = sListItemsValues + objRowTxt.GetAttribute("title") + "|" + objRowTxt.GetAttribute("value") + "|";
        //                                            objLstRow.AppendChild(objRowTxt);
        //                                        }
        //                                        if(iPos == -1)
        //                                            sTemp = "";
        //                                        else
        //                                            sTemp = sTemp.Substring(iPos + 1);
        //                                    }
        //                                    objSelectElement.AppendChild((XmlNode)objLstRow);
        //                                    if(sListItemsValues != "")
        //                                    {
        //                                        if(sListItemsValues.IndexOf("|") != -1)  
        //                                            sListItemsValues = sListItemsValues.Substring(0,sListItemsValues.Length - 1);    
        //                                    }
        //                                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='SelEntitiesListItems']")).InnerText = sListItemsValues; 		
        //                                }
        //                                break;
        //                                #endregion

        //                                #region "Case optbrokers"
        //                            case "optbrokers":
        //                                if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //                                {
        //                                    if(m_bIsBrokerSupport)
        //                                    {
        //                                        sTemp = objDbReader["GROUP_BROKERS"].ToString();
        //                                        if(sTemp != "")
        //                                        {
        //                                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optBrokers']")).SetAttribute("value","2");

        //                                            objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='Brokers']"); 
        //                                            objNodLst  = objSelectElement.GetElementsByTagName("rowhead");
        //                                            objLstRow = p_objXMLDocument.CreateElement("listrow");

        //                                            while(sTemp != "")
        //                                            {
        //                                                iPos = sTemp.IndexOf(",");
        //                                                if(iPos == -1)
        //                                                    iEId = Conversion.ConvertStrToInteger(sTemp);
        //                                                else
        //                                                    iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0,iPos));
													
        //                                                if(iEId != 0)
        //                                                {
        //                                                    for(int j = 0 ; j< objNodLst.Count;j++)
        //                                                    {
        //                                                        objChildElm = (XmlElement)objNodLst[j];

        //                                                        objRowTxt = p_objXMLDocument.CreateElement("rowtext");
        //                                                        objRowTxt.SetAttribute("type" , "label");
        //                                                        objRowTxt.SetAttribute("name" , objChildElm.GetAttribute("colname"));
        //                                                        objRowTxt.SetAttribute("value" , iEId.ToString());
        //                                                        objLocalCache.GetEntityInfo(iEId,ref sFname,ref sLname);
        //                                                        objRowTxt.SetAttribute("title", sFname + ", " + sLname);
        //                                                        objLstRow.AppendChild(objRowTxt);
        //                                                    }
        //                                                }
        //                                                if(iPos == -1)
        //                                                    sTemp = "";
        //                                                else
        //                                                    sTemp = sTemp.Substring(iPos + 1);
        //                                            }
        //                                            objSelectElement.AppendChild((XmlNode)objLstRow); 
        //                                        }
        //                                        else
        //                                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optBrokers']")).SetAttribute("value","1");
        //                                    }

        //                                    if(m_bIsInsurerSupport)
        //                                    {
        //                                        sTemp = objDbReader["GROUP_INSURERS"].ToString();
        //                                        if(sTemp != "")
        //                                        {
        //                                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optInsurers']")).SetAttribute("value","2");

        //                                            objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='Insurers']"); 
        //                                            objNodLst  = objSelectElement.GetElementsByTagName("rowhead");
        //                                            objLstRow = p_objXMLDocument.CreateElement("listrow");

        //                                            while(sTemp != "")
        //                                            {
        //                                                iPos = sTemp.IndexOf(",");
        //                                                if(iPos == -1)
        //                                                    iEId = Conversion.ConvertStrToInteger(sTemp);
        //                                                else
        //                                                    iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0,iPos));
													
        //                                                if(iEId != 0)
        //                                                {
        //                                                    for(int j = 0 ; j< objNodLst.Count;j++)
        //                                                    {
        //                                                        objChildElm = (XmlElement)objNodLst[j];

        //                                                        objRowTxt = p_objXMLDocument.CreateElement("rowtext");
        //                                                        objRowTxt.SetAttribute("type" , "label");
        //                                                        objRowTxt.SetAttribute("name" , objChildElm.GetAttribute("colname"));
        //                                                        objRowTxt.SetAttribute("value" , iEId.ToString());
        //                                                        objLocalCache.GetEntityInfo(iEId,ref sFname,ref sLname);
        //                                                        objRowTxt.SetAttribute("title", sFname + ", " + sLname);
        //                                                        objLstRow.AppendChild(objRowTxt);
        //                                                    }
        //                                                }
        //                                                if(iPos == -1)
        //                                                    sTemp = "";
        //                                                else
        //                                                    sTemp = sTemp.Substring(iPos + 1);
        //                                            }
        //                                            objSelectElement.AppendChild((XmlNode)objLstRow); 
        //                                        }
        //                                        else
        //                                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optInsurers']")).SetAttribute("value","1");
        //                                    }
        //                                }
        //                                break;
        //                                #endregion

        //                                #region "Case optadjtexttypes"
        //                            case "optadjtexttypes":
        //                                try
        //                                {
        //                                    sTemp = objDbReader["GROUP_ADJ_TEXT_TYPES"].ToString();
        //                                    if(sTemp != "")
        //                                    {
        //                                        if(sTemp.Substring(0,1) == "-")
        //                                        {
        //                                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).SetAttribute("value","3");
        //                                            sTemp = sTemp.Substring(1); 
        //                                        }
        //                                        else
        //                                            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).SetAttribute("value","2");

        //                                        objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='AdjTextTypes']"); 
        //                                        objNodLst  = objSelectElement.GetElementsByTagName("rowhead");
        //                                        objLstRow = p_objXMLDocument.CreateElement("listrow");
											
        //                                        sListItemsValues = "";
        //                                        while(sTemp != "")
        //                                        {
        //                                            iPos = sTemp.IndexOf(",");
        //                                            if(iPos == -1)
        //                                                iEId = Conversion.ConvertStrToInteger(sTemp);
        //                                            else
        //                                                iEId = Conversion.ConvertStrToInteger(sTemp.Substring(0,iPos));
													
        //                                            if(iEId != 0)
        //                                            {
        //                                                for(int j = 0 ; j< objNodLst.Count;j++)
        //                                                {
        //                                                    objChildElm = (XmlElement)objNodLst[j];

        //                                                    objRowTxt = p_objXMLDocument.CreateElement("rowtext");
        //                                                    objRowTxt.SetAttribute("type" , "label");
        //                                                    objRowTxt.SetAttribute("name" , objChildElm.GetAttribute("colname"));
        //                                                    objRowTxt.SetAttribute("value" , iEId.ToString());
        //                                                    objRowTxt.SetAttribute("title", objLocalCache.GetShortCode(iEId) + " - " + objLocalCache.GetCodeDesc(iEId));

        //                                                    sListItemsValues = sListItemsValues + objRowTxt.GetAttribute("title") + "|" + objRowTxt.GetAttribute("value") + "|";

        //                                                    objLstRow.AppendChild(objRowTxt);
        //                                                }
        //                                            }
        //                                            if(iPos == -1)
        //                                                sTemp = "";
        //                                            else
        //                                                sTemp = sTemp.Substring(iPos + 1);
        //                                        }
        //                                        objSelectElement.AppendChild((XmlNode)objLstRow);
        //                                        if(sListItemsValues != "")
        //                                        {
        //                                            if(sListItemsValues.IndexOf("|") != -1)  
        //                                                sListItemsValues = sListItemsValues.Substring(0,sListItemsValues.Length - 1);    
        //                                        }
        //                                        ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='AdjusterListItems']")).InnerText = sListItemsValues;
        //                                    }
        //                                    else
        //                                        ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).SetAttribute("value","1");
        //                                }
        //                                catch(Exception e)
        //                                {
        //                                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).SetAttribute("value","1");
        //                                }

        //                                break;

        //                                #endregion
        //                        }
        //                    }
        //                }
        //            }
        //            if(objDbReader != null)
        //                objDbReader.Close();
        //        }

        //        sSQL = "SELECT * FROM ORG_SECURITY_USER WHERE GROUP_ID = " + iGroupId;
        //        objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
        //        if(objDbReader != null)
        //        {
        //            objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='Users']"); 
        //            objNodLst  = objSelectElement.GetElementsByTagName("rowhead");
        //            objLstRow = p_objXMLDocument.CreateElement("listrow");

        //            sListItemsValues = "";
        //            while(objDbReader.Read())
        //            {
        //                for(int j = 0 ; j< objNodLst.Count;j++)
        //                {
        //                    objChildElm = (XmlElement)objNodLst[j];

        //                    objRowTxt = p_objXMLDocument.CreateElement("rowtext");
        //                    objRowTxt.SetAttribute("type" , "label");
        //                    objRowTxt.SetAttribute("name" , objChildElm.GetAttribute("colname"));
        //                    objRowTxt.SetAttribute("value" , objDbReader["USER_ID"].ToString());
        //                    objRowTxt.SetAttribute("title", UTILITY.GetUser(Conversion.ConvertStrToInteger(objDbReader["USER_ID"].ToString()), m_sDSNId, m_sSecurityDSN));

        //                    sListItemsValues = sListItemsValues + objRowTxt.GetAttribute("title") + "|" + objRowTxt.GetAttribute("value") + "|";

        //                    objLstRow.AppendChild(objRowTxt);
        //                }
        //            }
        //            objSelectElement.AppendChild((XmlNode)objLstRow);
        //            if(sListItemsValues != "")
        //            {
        //                if(sListItemsValues.IndexOf("|") != -1)  
        //                    sListItemsValues = sListItemsValues.Substring(0,sListItemsValues.Length - 1);    
        //            }
        //            ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='UserListItems']")).InnerText = sListItemsValues;
					
        //        }
        //        if (objDbReader != null)
        //            objDbReader.Close();


        //        //abisht 10669
        //        if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //        {
        //            sSQL = "SELECT * FROM USER_ORGSEC WHERE DSN_ID = " + m_sDSNId + " AND GROUP_ID = " + iGroupId;
        //            objDbReader = DbFactory.GetDbReader(m_sSecurityDSN, sSQL);
        //            if (objDbReader != null)
        //            {
        //                if (objDbReader.Read())
        //                {
        //                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='PassWord']")).InnerText = RMCryptography.DecryptString(objDbReader["DB_PWD"].ToString());
        //                }
        //            }
        //            if (objDbReader != null)
        //                objDbReader.Close();
        //        }

        //        objXml = new XmlDocument();
        //        string strContent = RMSessionManager.GetCustomContent();
        //        if (!string.IsNullOrEmpty(strContent))
        //        {
        //            objXml.LoadXml(strContent);
        //            objViewEnhanceBES = objXml.SelectSingleNode("//SpecialSettings/EnhanceBES");
        //            if (objViewEnhanceBES != null)
        //            {
        //                if (objViewEnhanceBES.InnerText == "-1")
        //                {
        //                    ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='ViewEnhanceBES']")).InnerText = "yes";
        //                }
        //            }
        //        }
        //        return p_objXMLDocument;
        //    }
        //    catch(Exception p_objExp)
        //    {
        //        throw new RMAppException(Globalization.GetString("ORGSEC.LoadGroupDef.Error"),p_objExp);   
        //    }
        //    finally
        //    {
        //        if (objLocalCache != null)
        //        {
        //            //objLocalCache.Clear();
        //            objLocalCache.Dispose();
        //        }
        //        if(m_objDbConnection != null)
        //            m_objDbConnection.Dispose();
        //        if (objDbReader != null)
        //        {
        //            objDbReader.Close();
        //            objDbReader.Dispose();
        //        }
        //        if (objRdr != null)
        //        {
        //            objRdr.Close();
        //            objRdr.Dispose();
        //        }
        //    }
        //}

        /// <summary>
        /// Parijat-- Created in order to meet the R5 needs
        /// </summary>
        /// <param name="p_objXMLDocument"></param>
        /// <returns></returns>
        public XmlDocument SaveGroupDef(XmlDocument p_objXMLDocument)
        {
            #region local variable
            string sSQL = "";
            int iGroupId = 0;
            string sTemp = "";
            int iNum = 0;
            string sBrokers = "";
            string sInsurers = "";
            string sAdjTypes = "";
            string sNoteTypes = string.Empty;
            string sGroupName = "";
            string sEncript1 = "";
            string sEncript2 = "";
            string[] arrFields;
            string sListText = "";
            string sEncriptUser = "";
            DbConnection objDbConn = null;
            XmlElement objSelectElement = null;
            XmlNodeList objNodeList = null;
            LocalCache objLocalCache = null;
            DbReader objDbReader = null;
            int iConfRec = 0; //pmittal5
            int iSuperGroupId = 0; //pmittal5
            List<int> lstGroupIds = new List<int>();  //pmittal5
            #endregion

            try
            {
                objLocalCache = new LocalCache(m_sConnString, m_iClientId);
                //pmittal5 03/11/10 - Confidential Record
                iConfRec = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//ConfRec")).InnerText); 
                //abisht MITS 10669
                m_sOveridingPassWord = ((XmlElement)p_objXMLDocument.SelectSingleNode("//PassWord")).InnerText;
                m_sOverideFlag = ((XmlElement)p_objXMLDocument.SelectSingleNode("//OveridePassFlag")).InnerText;

                m_bIsAllOrgEntities = false;
//Updating group_id from User_orgSec in place of Org_security for both SQL and oracle.
                //changed by nadim..start
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE || m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                {
                    sSQL = "SELECT MAX(GROUP_ID) FROM USER_ORGSEC";
                    objDbReader = DbFactory.GetDbReader(m_sSecurityDSN, sSQL);
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                            iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;
                        else
                            iGroupId = 1;

                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }
                //else if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                //{
                //    sSQL = "SELECT MAX(GROUP_ID) FROM ORG_SECURITY";
                //    objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                //    if (objDbReader != null)
                //    {
                //        if (objDbReader.Read())
                //            iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;
                //        else
                //            iGroupId = 1;

                //    }
                //    if (objDbReader != null)
                //        objDbReader.Close();
                //}

                iSuperGroupId = iGroupId; //pmittal5
                //changed by nadim-End
                //Write out policy brokers & insurers IFF selected and dbms is oracle
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optBrokersValue")).InnerText == "2")
                    {
                        sListText = p_objXMLDocument.SelectSingleNode("//BrokersListItems").InnerText;
                        if (sListText.Trim() != "")
                        {
                            arrFields = sListText.Split('|');
                            if (arrFields.Length > 0)
                            {
                                for (int i = 0; i < arrFields.Length; i++)
                                {
                                    if ((i % 2) > 0)
                                    {
                                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                        if (sBrokers != "")
                                            sBrokers += ",";
                                        sBrokers += iNum.ToString();
                                    }
                                }
                            }
                        }
                    }

                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optInsurersValue")).InnerText == "2")
                    {
                        sListText = p_objXMLDocument.SelectSingleNode("//InsurersListItems").InnerText;
                        if (sListText.Trim() != "")
                        {
                            arrFields = sListText.Split('|');
                            if (arrFields.Length > 0)
                            {
                                for (int i = 0; i < arrFields.Length; i++)
                                {
                                    if ((i % 2) > 0)
                                    {
                                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                        if (sInsurers != "")
                                            sInsurers += ",";
                                        sInsurers += iNum.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                //Parijat :Boeing broker Support
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR && g_bBoeingBrokerSupport==true )
                {
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optBrokersValue")).InnerText == "2")
                    {
                        sListText = p_objXMLDocument.SelectSingleNode("//BrokersListItems").InnerText;
                        if (sListText.Trim() != "")
                        {
                            arrFields = sListText.Split('|');
                            if (arrFields.Length > 0)
                            {
                                for (int i = 0; i < arrFields.Length; i++)
                                {
                                    if ((i % 2) > 0)
                                    {
                                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                        if (sBrokers != "")
                                            sBrokers += ",";
                                        sBrokers += iNum.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                //Write out adjuster text types
                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                {
                    sListText = p_objXMLDocument.SelectSingleNode("//AdjusterListItems").InnerText;
                    if (sListText.Trim() != "")
                    {
                        arrFields = sListText.Split('|');
                        if (arrFields.Length > 0)
                        {
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if ((i % 2) > 0)
                                {
                                    iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                    if (sAdjTypes != "")
                                        sAdjTypes += ",";
                                    sAdjTypes += iNum.ToString();
                                }
                            }
                        }
                    }
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                        sAdjTypes = "-" + sAdjTypes;
                }

                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue")).InnerText == "3")
                {
                    sListText = p_objXMLDocument.SelectSingleNode("//NotesListItems").InnerText;
                    if (sListText.Trim() != "")
                    {
                        arrFields = sListText.Split('|');
                        if (arrFields.Length > 0)
                        {
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if ((i % 2) > 0)
                                {
                                    iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                    if (sNoteTypes != "")
                                        sNoteTypes += ",";
                                    sNoteTypes += iNum.ToString();
                                }
                            }
                        }
                    }
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue")).InnerText == "3")
                        sNoteTypes = "-" + sNoteTypes;
                }
                //Clear out new ORG_SECURITY_EIDS table
                //Deb :MITS 29627
                //try
                //{
                //    //it is possible not all installations will have this table
                //    sSQL = "DELETE FROM ORG_SECURITY_EIDS WHERE GROUP_ID = " + iGroupId;
                //    RunSql(sSQL, m_objDbConnection);
                //}
                //catch { }
                //Deb :MITS 29627
                //nadim for 14334..Check for duplicate user(whether this user is already entered in another group or not..)
               
                bool bError = CheckDuplicateUser(p_objXMLDocument, iGroupId);
                if (!bError)
                {
                    if (m_bMultipleUser == true)
                    {
                        throw new RMAppException("Users  '" + sExistingUserName + "'  Already Exist  in Groups(s)  '" + sExistingGroupName + "'");
                    }
                    else
                        throw new RMAppException("User  '" + sExistingUserName + "'  Already Exists  in Group(s)  '" + sExistingGroupName + "'");
                }

               
                //nadim for 14334s

                //pmittal5 Mits 21710 - Show error if All Org Entities are selected in some other group apart from Admin
                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optOrgEntitiesValue")).InnerText == "1")
                {
                    bError = CheckAllEntitiesGroup(iGroupId);
                    if (!bError)
                    {
                        if (m_bMultipleGroups)
                        {
                            throw new RMAppException("There are already Groups '" + sExistingGroupName + "' with All Organizational Entities selected. Use Selected Organizational Entities and Save the Group.");
                        }
                        else
                        {
                            throw new RMAppException("There is already a Group '" + sExistingGroupName + "' with All Organizational Entities selected. Use Selected Organizational Entities and Save the Group.");
                        }
                    }
                }
                //End - pmittal5

                //Write entity selection info (used to reconstruct ENTITY_MAP if necessary)

                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optOrgEntitiesValue")).InnerText == "1")
                {
                    sTemp = "<ALL>";
                    m_bIsAllOrgEntities = true;
                }
                else
                {
                    sTemp = "";
                    sListText = p_objXMLDocument.SelectSingleNode("//SelEntitiesListItems").InnerText;
                    if (sListText.Trim() != "")
                    {
                        arrFields = sListText.Split('|');
                        if (arrFields.Length > 0)
                        {
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if ((i % 2) > 0)
                                {
                                    iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                    if (sBESSynSetting.ToUpper() != "FALSE") //Deb : MITS 29215
                                     EnumDepts(iNum, m_objDbConnection);
                                    if (sTemp != "")
                                        sTemp += ",";
                                    sTemp += iNum.ToString();
                                    //Deb :MITS 29627
                                    //it is possible not all installations will have this table "ORG_SECURITY_EIDS"
                                    //try
                                    //{
                                    //    //Write entry to ORG_SECURITY_EIDS table. Allows trigger resync of ENTITY_MAP table.
                                    //    sSQL = "INSERT INTO ORG_SECURITY_EIDS(GROUP_ID,ENTITY_ID,ENTITY_TABLE_ID) VALUES (" + iGroupId + "," + iNum + "," + objLocalCache.GetOrgTableId(iNum) + ")";
                                    //    RunSql(sSQL, m_objDbConnection);
                                    //}
                                    //catch { }
                                    //Deb :MITS 29627
                                }
                            }
                        }
                    }
                    m_bIsAllOrgEntities = false;
                }

                //Update ORG_SECURITY profile
                sGroupName = ((XmlElement)p_objXMLDocument.SelectSingleNode("//GroupName")).InnerText;

                if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                {//Boeing Broker Support
                    if (m_bIsInsurerSupport && m_bIsBrokerSupport)
                    {
                        if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_BROKERS,GROUP_INSURERS," +
                                "GROUP_ADJ_TEXT_TYPES,GROUP_ENTITIES,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES(" + iGroupId + "," +
                                Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sBrokers + "','" +
                                sInsurers + "'," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",'" + sTemp + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                        else
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_BROKERS,GROUP_INSURERS," +
                                "GROUP_ENTITIES,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES(" + iGroupId + "," +
                                Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sBrokers + "','" +
                                sInsurers + "','" + sTemp + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                    }
                    else
                    {
                        if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ADJ_TEXT_TYPES," +
                                "GROUP_ENTITIES,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES(" + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) +
                                ",0," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",'" + sTemp + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                        else
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ENTITIES,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES("
                                + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sTemp + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                    }

                }
                else
                {//Boeing Broker Support
                    if (g_bBoeingBrokerSupport)
                    {
                        if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_BROKERS," +
                                "GROUP_ADJ_TEXT_TYPES,GROUP_ENTITIES,GROUP_INSURERS,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES(" + iGroupId + "," +
                                Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sBrokers + "'" +
                                 "," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",'" + sTemp + "','" + sInsurers + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                        else
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_BROKERS," +
                                "GROUP_ENTITIES,GROUP_INSURERS,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES(" + iGroupId + "," +
                                Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sBrokers + "'," 
                                 + "'" + sTemp + "','" + sInsurers + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                    }
                    else
                    {
                        if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ADJ_TEXT_TYPES," +
                                "GROUP_ENTITIES,GROUP_INSURERS,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES(" + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) +
                                ",0," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",'" + sTemp + "','" + sInsurers + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                        else
                        {
                            sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ENTITIES,GROUP_INSURERS,DELETED_FLAG,GROUP_ENH_NOTES_TYPES) VALUES("
                                + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sTemp + "','" + sInsurers + "',0" + "," + Utilities.FormatSqlFieldValue(sNoteTypes) + ")";
                        }
                    }
                    
                }
                RunSql(sSQL, m_objDbConnection);

                //build user list
                sListText = p_objXMLDocument.SelectSingleNode("//UserListItems").InnerText;

                objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
                objDbConn.Open();
                bool bInserted = false; //pmittal5
                if (sListText.Trim() != "")
                {
                    arrFields = sListText.Split('|');
                    if (arrFields.Length > 0)
                    {
                        for (int i = 0; i < arrFields.Length; i++)
                        {
                            if ((i % 2) > 0)
                            {
                                iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                sSQL = "INSERT INTO ORG_SECURITY_USER (GROUP_ID,USER_ID) VALUES(" + iGroupId + "," + iNum + ")";
                                RunSql(sSQL, m_objDbConnection);

                                if (m_bIsAllOrgEntities)
                                {
                                    if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                                    {
                                        //DBO signals that the login routines should use the DSN login
                                        sEncript1 = RMCryptography.EncryptString("DBO");
                                        sEncript2 = RMCryptography.EncryptString("DBO");
                                    }
                                    else
                                    {
                                        sEncript1 = RMCryptography.EncryptString(m_sDBOPwd);
                                        sEncript2 = RMCryptography.EncryptString(m_sDBOUserId);
                                    }
                                }
                                else
                                {

                                    sEncriptUser = FormatBESUserName(Conversion.ConvertStrToInteger(m_sDSNId), m_sConnString, iGroupId);

                                  sEncript2 = RMCryptography.EncryptString(sEncriptUser);
                                    //Mjain8 Added for sql server 2005 compatibility
                                    if ((m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR))
                                        sEncript1 = RMCryptography.EncryptString(BES_COMPLEX_PASSWORD + string.Format("{0:00000}", iGroupId));
                                    else
                                        sEncript1 = sEncript2;
                                }
                                //abisht 10669
                                //pmittal5 - Password change functionality in case of SQL also.
                                //if (m_sOverideFlag == "0" || m_sOverideFlag == "" || m_stDataBaseType != eDatabaseType.DBMS_IS_ORACLE)
                                if (m_sOverideFlag == "0" || m_sOverideFlag == "")
                                {
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD,PWD_CHANGED_FLAG) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ",0)";
                                }
                                else
                                {
                                    sEncript1 = RMCryptography.EncryptString(m_sOveridingPassWord);
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD,PWD_CHANGED_FLAG) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ",-1)";
                                }
                                //abisht.commented the code and moved it to if else condition.
                                //sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ")";
                                RunSql(sSQL, objDbConn);
                                if (iConfRec != 0 || (iConfRec == 0 && !bInserted))
                                {
                                    sSQL = "INSERT INTO GROUP_MAP (SUPER_GROUP_ID,GROUP_ID,DELETED_FLAG) VALUES(" + iSuperGroupId + "," + iGroupId + ",0)";
                                    RunSql(sSQL, m_objDbConnection);
                                    bInserted = true;
                                    lstGroupIds.Add(iGroupId);
                                }
                                if (iConfRec != 0)
                                    iGroupId++;
                                //End - pmittal5
                            }
                        }
                    }
                }
                if (objDbConn != null)
                    objDbConn.Close();

                if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
                    BuildEntityMap_SQLSybase(iSuperGroupId, sAdjTypes, sBrokers, lstGroupIds, true, true, "NEW");
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                    //BuildEntityMap_Oracle(iGroupId, sBrokers, sInsurers, sAdjTypes,true);
                    BuildEntityMap_Oracle(iSuperGroupId, sBrokers, sInsurers, sAdjTypes, lstGroupIds, true, true, "NEW");
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
                    //BuildEntityMap_DB2(iGroupId, sBrokers, sInsurers, sAdjTypes);
                    BuildEntityMap_DB2(iSuperGroupId, sBrokers, sInsurers, sAdjTypes, lstGroupIds, true, "NEW");

                return p_objXMLDocument;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("ORGSEC.SaveGroupDef.Error",m_iClientId), p_objExp);
            }
            finally
            {
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();

                if (objDbConn != null)
                    objDbConn.Dispose();
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
            }
        }

		/// Name		: SaveGroupDef
		/// Author		: Anurag Agarwal
		/// Date Created	: 25 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves the Group defination for the GroupId
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group to be Saved</param>
		/// <returns>True for success & False for failure</returns>
        //public XmlDocument SaveGroupDef_old(XmlDocument p_objXMLDocument)
        //{
        //    #region local variables
        //    string sSQL = "";
        //    int iGroupId = 0;
        //    string sTemp = "";
        //    int iNum = 0;
        //    string sBrokers = "";
        //    string sInsurers = "";
        //    string sAdjTypes = "";
        //    string sGroupName = "";
        //    string sEncript1 = "";
        //    string sEncript2 = "";
        //    string [] arrFields;
        //    string sListText = "";
        //    DbConnection objDbConn = null;
        //    XmlElement objSelectElement = null;
        //    XmlNodeList objNodeList = null;
        //    LocalCache objLocalCache = null;
        //    DbReader objDbReader = null;
        //    #endregion

        //    try
        //    {
        //        objLocalCache = new LocalCache(m_sConnString);
        //        //abisht MITS 10669
        //        m_sOveridingPassWord = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='PassWord']")).InnerText;
        //        m_sOverideFlag = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='OveridePassFlag']")).InnerText;
				
        //        m_bIsAllOrgEntities = false;

        //        if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //        {
        //            sSQL = "SELECT MAX(GROUP_ID) FROM USER_ORGSEC";
        //            objDbReader = DbFactory.GetDbReader(m_sSecurityDSN,sSQL);
        //            if(objDbReader != null)
        //            {
        //                if(objDbReader.Read())
        //                    iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;
        //            }
        //            if (objDbReader != null)
        //                objDbReader.Close();
        //        }
        //        else if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
        //        {
        //            sSQL = "SELECT MAX(GROUP_ID) FROM ORG_SECURITY";
        //            objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
        //            if(objDbReader != null)
        //            {
        //                if(objDbReader.Read())
        //                    iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1; 
        //                else
        //                    iGroupId = 1;
        //            }
        //            if (objDbReader != null)
        //                objDbReader.Close();
        //        }
        //        else if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
        //        {
        //            iGroupId = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText);
        //        }

        //        //Write out policy brokers & insurers IFF selected and dbms is oracle
        //        if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //        {
        //            if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optBrokers']")).GetAttribute("value") == "2")
        //            {
        //                sListText = p_objXMLDocument.SelectSingleNode("//control[@name='BrokersListItems']").InnerText;
        //                if(sListText.Trim() != "")
        //                {
        //                    arrFields = sListText.Split('|');
        //                    if(arrFields.Length > 0)
        //                    {
        //                        for(int i = 0; i < arrFields.Length; i++)
        //                        {
        //                            if((i % 2) > 0)
        //                            {
        //                                iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                                if(sBrokers != "")
        //                                    sBrokers += ",";
        //                                sBrokers += iNum.ToString();
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optInsurers']")).GetAttribute("value") == "2")
        //            {
        //                sListText = p_objXMLDocument.SelectSingleNode("//control[@name='InsurersListItems']").InnerText;
        //                if(sListText.Trim() != "")
        //                {
        //                    arrFields = sListText.Split('|');
        //                    if(arrFields.Length > 0)
        //                    {
        //                        for(int i = 0; i < arrFields.Length; i++)
        //                        {
        //                            if((i % 2) > 0)
        //                            {
        //                                iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                                if(sInsurers != "")
        //                                    sInsurers += ",";
        //                                sInsurers += iNum.ToString();
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
				
        //        //Write out adjuster text types
        //        if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "3")
        //        {
        //            sListText = p_objXMLDocument.SelectSingleNode("//control[@name='AdjusterListItems']").InnerText;
        //            if(sListText.Trim() != "")
        //            {
        //                arrFields = sListText.Split('|');
        //                if(arrFields.Length > 0)
        //                {
        //                    for(int i = 0; i < arrFields.Length; i++)
        //                    {
        //                        if((i % 2) > 0)
        //                        {
        //                            iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                            if(sAdjTypes != "")
        //                                sAdjTypes += ",";
        //                            sAdjTypes += iNum.ToString();
        //                        }
        //                    }
        //                }
        //            }
        //            if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "3")
        //                sAdjTypes = "-" + sAdjTypes;
        //        }

        //        //Clear out new ORG_SECURITY_EIDS table
        //        try
        //        {
        //            //it is possible not all installations will have this table
        //            sSQL = "DELETE FROM ORG_SECURITY_EIDS WHERE GROUP_ID = " + iGroupId;
        //            RunSql(sSQL, m_objDbConnection);   
        //        }	
        //        catch{}

        //        //nadim for 14334..Check for duplicate user(whether this user is already entered in another group or not..)
        //        bool bError = CheckDuplicateUser(p_objXMLDocument, iGroupId);
        //        if (!bError)
        //        {
        //            if (m_bMultipleUser == true)
        //            {
        //                throw new RMAppException("Users  '" + sExistingUserName + "'  Already Exist  in Groups(s)  '" + sExistingGroupName + "'");
        //            }
        //            else
        //                throw new RMAppException("User  '" + sExistingUserName + "'  Already Exists  in Group(s)  '" + sExistingGroupName + "'");
        //        }

                //nadim for 14334
				//Write entity selection info (used to reconstruct ENTITY_MAP if necessary)
				
        //        if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optOrgEntities']")).GetAttribute("value") == "1")
        //        {
        //            sTemp =	"<ALL>";
        //            m_bIsAllOrgEntities = true;
        //        }
        //        else
        //        {
        //            sTemp = "";
        //            sListText = p_objXMLDocument.SelectSingleNode("//control[@name='SelEntitiesListItems']").InnerText;
        //            if(sListText.Trim() != "")
        //            {
        //                arrFields = sListText.Split('|');
        //                if(arrFields.Length > 0)
        //                {
        //                    for(int i = 0; i < arrFields.Length; i++)
        //                    {
        //                        if((i % 2) > 0)
        //                        {
        //                            iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                            EnumDepts(iNum,m_objDbConnection); 
        //                            if(sTemp != "")
        //                                sTemp += ",";
        //                            sTemp += iNum.ToString();

        //                            //it is possible not all installations will have this table "ORG_SECURITY_EIDS"
        //                            try
        //                            {
        //                                //Write entry to ORG_SECURITY_EIDS table. Allows trigger resync of ENTITY_MAP table.
        //                                sSQL = "INSERT INTO ORG_SECURITY_EIDS(GROUP_ID,ENTITY_ID,ENTITY_TABLE_ID) VALUES (" + iGroupId + "," + iNum + "," + objLocalCache.GetOrgTableId(iNum) + ")";
        //                                RunSql(sSQL, m_objDbConnection);   
        //                            }
        //                            catch {}
        //                        }
        //                    }
        //                }
        //            }
        //            m_bIsAllOrgEntities = false;
        //        }	

        //        //Update ORG_SECURITY profile
        //        sGroupName = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='GroupName']")).InnerText; 

        //        if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //        {
        //            if(m_bIsInsurerSupport && m_bIsInsurerSupport)
        //            {
        //                if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "3")
        //                {
        //                    sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_BROKERS,GROUP_INSURERS," +
        //                        "GROUP_ADJ_TEXT_TYPES,GROUP_ENTITIES,DELETED_FLAG) VALUES(" + iGroupId + "," + 
        //                        Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sBrokers + "','" + 
        //                        sInsurers + "'," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",'" + sTemp + "',0)";
        //                }
        //                else
        //                {
        //                    sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_BROKERS,GROUP_INSURERS," +
        //                        "GROUP_ENTITIES,DELETED_FLAG) VALUES(" + iGroupId + "," + 
        //                        Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sBrokers + "','" + 
        //                        sInsurers + "','" + sTemp + "',0)";
        //                }
        //            }
        //            else
        //            {
        //                if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "3")
        //                {
        //                    sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ADJ_TEXT_TYPES," + 
        //                        "GROUP_ENTITIES,DELETED_FLAG) VALUES(" + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + 
        //                        ",0," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",'" + sTemp + "'0)";
        //                }
        //                else
        //                {
        //                    sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ENTITIES,DELETED_FLAG) VALUES("
        //                        + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sTemp + "',0)";
        //                }
        //            }

        //        }
        //        else
        //        {
        //            if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "3")
        //            {
        //                sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ADJ_TEXT_TYPES," + 
        //                    "GROUP_ENTITIES,DELETED_FLAG) VALUES(" + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + 
        //                    ",0," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",'" + sTemp + "',0)";
        //            }
        //            else
        //            {
        //                sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_DEPT,GROUP_ENTITIES,DELETED_FLAG) VALUES("
        //                    + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + ",0,'" + sTemp + "',0)";
        //            }
        //        }
        //        RunSql(sSQL, m_objDbConnection);

				

        //        //build user list
        //        sListText = p_objXMLDocument.SelectSingleNode("//control[@name='UserListItems']").InnerText;

        //        objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
        //        objDbConn.Open();
				
        //        if(sListText.Trim() != "")
        //        {
        //            arrFields = sListText.Split('|');
        //            if(arrFields.Length > 0)
        //            {
        //                for(int i = 0; i < arrFields.Length; i++)
        //                {
        //                    if((i % 2) > 0)
        //                    {
        //                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                        sSQL = "INSERT INTO ORG_SECURITY_USER (GROUP_ID,USER_ID) VALUES(" + iGroupId + "," + iNum + ")";
        //                        RunSql(sSQL,m_objDbConnection);

        //                        if(m_bIsAllOrgEntities)
        //                        {
        //                            if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //                            {
        //                                //DBO signals that the login routines should use the DSN login
        //                                sEncript1 = RMCryptography.EncryptString("DBO");
        //                                sEncript2 = RMCryptography.EncryptString("DBO");
        //                            }
        //                            else
        //                            {
        //                                sEncript1 = RMCryptography.EncryptString(m_sDBOPwd);
        //                                sEncript2 = RMCryptography.EncryptString(m_sDBOUserId);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            sEncript2 = RMCryptography.EncryptString("sg" + string.Format("{0:00000}",iGroupId));
        //                            if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
        //                                sEncript1 = RMCryptography.EncryptString(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='DB2Password']")).InnerText);
        //                            //Mjain8 Added for sql server 2005 compatibility
        //                            else if((m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR))
        //                                sEncript1=RMCryptography.EncryptString(BES_COMPLEX_PASSWORD + string.Format("{0:00000}",iGroupId));
        //                            else
        //                                sEncript1 = sEncript2; 
        //                        }
        //                        //abisht 10669
        //                        if (m_sOverideFlag == "0" || m_sOverideFlag == "" || m_stDataBaseType != eDatabaseType.DBMS_IS_ORACLE)
        //                        {
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
        //                        }
        //                        else
        //                        {
        //                            sEncript1 = RMCryptography.EncryptString(m_sOveridingPassWord);
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
        //                        }
        //                        //abisht.commented the code and moved it to if else condition.
        //                        //sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ")";
        //                        RunSql(sSQL,objDbConn);
        //                    }
        //                }
        //            }
        //        }
        //        if(objDbConn != null)
        //            objDbConn.Close();
 
        //        if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
        //            BuildEntityMap_SQLSybase(iGroupId, sAdjTypes, sBrokers,true);//Parijat:Boeing --Broker Support
        //        else if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //            BuildEntityMap_Oracle(iGroupId, sBrokers, sInsurers, sAdjTypes,true);
			
        //        return p_objXMLDocument;
        //    }
        //    catch(Exception p_objExp)
        //    {
        //        throw new RMAppException(Globalization.GetString("ORGSEC.SaveGroupDef.Error"),p_objExp);   
        //    }
        //    finally
        //    {
        //        if (objLocalCache != null)
        //            objLocalCache.Dispose();
        //        if(m_objDbConnection != null)
        //            m_objDbConnection.Dispose();

        //        if(objDbConn != null)
        //            objDbConn.Dispose();
        //        if (objDbReader != null)
        //        {
        //            objDbReader.Close();
        //            objDbReader.Dispose();
        //        }
        //    }
        //}

        //nadim for 14334
        //New method added by Nadim for 14334 to check whether a duplicate user exist or not-Code start
        /// Name		: CheckDuplicateUser
        /// Author		: Nadim Zafar
        /// Date Created	: 12 Mar 2009
        /// ************************************************************
        public bool CheckDuplicateUser(XmlDocument xmlDoc, int iGroupId)
        {
            #region local variables
            string sListTextNew = "";
            string sCurrentUserId = "";
            string sUserId = "";
            string sExistingUserId = "";
            string sSqlUserId = "";
            string sSqlUsername = "";
            string sExistingUser = "";
            string sExistingGroup = "";
            string sSqlGroup = "";
            string[] arrCurrentUser;
            int iExistingUserCount = 0;
            int iNumNew = 0;

            DbReader objReaderExistingUser = null;
            DbReader objReaderExistingUserName = null;
            DbReader objReaderExistingGroup = null;
            //Deb : Fixed the Confidential Issue MITS 29092
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            StringBuilder sGroupIds = new StringBuilder();
            //Deb : Fixed the Confidential Issue MITS 29092
            #endregion

            try
            {
                sListTextNew = xmlDoc.SelectSingleNode("//UserListItems").InnerText;
                if (sListTextNew.Trim() != "")
                {
                    arrCurrentUser = sListTextNew.Split('|');
                    if (arrCurrentUser.Length > 0)
                    {
                        for (int iUserCount = 1; iUserCount < arrCurrentUser.Length; iUserCount += 2)
                        {
                            iNumNew = Conversion.ConvertStrToInteger(arrCurrentUser[iUserCount].ToString());
                            sCurrentUserId = sCurrentUserId + (string)(iNumNew + ",");
                        }
                    }
                }
                sCurrentUserId = sCurrentUserId.TrimEnd(',');

                //Deb : Fixed the Confidential Issue MITS 29092
                sSQL = "SELECT GROUP_ID FROM GROUP_MAP WHERE SUPER_GROUP_ID = " + iGroupId;
                using (objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            if (sGroupIds.Length > 0)
                                sGroupIds.Append(", " + objDbReader[0].ToString());
                            else
                                sGroupIds.Append(objDbReader[0].ToString());
                        }
                    }
                }
                //sSqlUserId = "SELECT USER_ID FROM ORG_SECURITY_USER WHERE GROUP_ID<>" + iGroupId + "AND USER_ID IN (" + sCurrentUserId + ")";
                if(sGroupIds.Length > 0)
                 sSqlUserId = "SELECT USER_ID FROM ORG_SECURITY_USER WHERE GROUP_ID NOT IN (" + sGroupIds + ") AND USER_ID IN (" + sCurrentUserId + ")";
                else
                 sSqlUserId = "SELECT USER_ID FROM ORG_SECURITY_USER WHERE GROUP_ID<>" + iGroupId + "AND USER_ID IN (" + sCurrentUserId + ")";
                //Deb : Fixed the Confidential Issue MITS 29092
                
                objReaderExistingUser = DbFactory.GetDbReader(m_sConnString, sSqlUserId);
                if (objReaderExistingUser != null)
                {
                    while (objReaderExistingUser.Read())
                    {
                        sUserId = objReaderExistingUser.GetInt32("USER_ID").ToString();
                        sExistingUserId = sExistingUserId + sUserId + ",";
                    }
                    objReaderExistingUser.Close();
                }
                sExistingUserId = sExistingUserId.TrimEnd(',');
                if (sExistingUserId != "")
                {
                    sSqlUsername = "SELECT FIRST_NAME,LAST_NAME FROM USER_TABLE WHERE USER_ID IN (" + sExistingUserId + ")";
                    objReaderExistingUserName = DbFactory.GetDbReader(m_sSecurityDSN, sSqlUsername);
                    if (objReaderExistingUserName != null)
                    {
                        while (objReaderExistingUserName.Read())
                        {
                            sExistingUser = objReaderExistingUserName.GetString("FIRST_NAME") + " " + objReaderExistingUserName.GetString("LAST_NAME");
                            sExistingUserName = sExistingUserName + sExistingUser + ",";
                            iExistingUserCount = iExistingUserCount + 1;
                        }
                        sExistingUserName = sExistingUserName.TrimEnd(',');
                        objReaderExistingUserName.Close();
                    }
                    if (iExistingUserCount > 1)
                        m_bMultipleUser = true;
                    sSqlGroup = "SELECT GROUP_NAME FROM ORG_SECURITY WHERE GROUP_ID IN (SELECT GROUP_ID FROM ORG_SECURITY_USER WHERE USER_ID IN (" + sExistingUserId + "))";
                    objReaderExistingGroup = DbFactory.GetDbReader(m_sConnString, sSqlGroup);
                    if (objReaderExistingGroup != null)
                    {
                        while (objReaderExistingGroup.Read())
                        {
                            sExistingGroup = objReaderExistingGroup.GetString("GROUP_NAME");
                            sExistingGroupName = sExistingGroupName + sExistingGroup + ",";
                        }
                        sExistingGroupName = sExistingGroupName.TrimEnd(',');
                        objReaderExistingGroup.Close();
                    }
                    return false;
                }
            }
            catch{}
            finally
            {
                if (objReaderExistingUserName != null)
                    objReaderExistingUserName.Dispose();

                if (objReaderExistingUser != null)
                    objReaderExistingUser.Dispose();
                if (objReaderExistingGroup != null)
                    objReaderExistingGroup.Dispose();
                if (objDbReader != null)
                    objDbReader.Dispose();
            }

            return true;
        }

        // pmittal5 for 21710
        // New method added by to check whether a Group with All Org Entities already exists
        /// Name		: CheckAllEntitiesGroup
        /// Author		: Priya Mittal
        /// Date Created: 08/11/2010
        /// ************************************************************
        private bool CheckAllEntitiesGroup(int p_iGroupId)
        {
            string sSql = string.Empty;
            int iExistingGroupCount = 0;
            sExistingGroupName = string.Empty;
            sSql = "SELECT GROUP_NAME FROM ORG_SECURITY WHERE GROUP_ENTITIES LIKE '<ALL>' AND GROUP_ID != " + p_iGroupId;
            using (DbReader objReader = DbFactory.GetDbReader(m_sConnString,sSql))
            {
                if (objReader != null)
                {
                    while(objReader.Read())
                    {
                        sExistingGroupName = sExistingGroupName + objReader.GetString(0) + ",";
                        iExistingGroupCount = iExistingGroupCount + 1;
                    }
                    sExistingGroupName = sExistingGroupName.TrimEnd(',');
                    if (iExistingGroupCount > 1)
                    {
                        m_bMultipleGroups = true;
                    }
                }
             }
             if (!String.IsNullOrEmpty(sExistingGroupName))
             {
                 return false;
             }
            return true;
        }
        //End - pmittal5

        //nadim for 14334
        public string FormatBESUserName(int iDSNId, String sMainDSN ,int iGroupID )
        {
            //string sListTextNew = "";
            //string sCurrentUserId = "";
            string sMainDbName = "";
            //string sExistingUserId = "";
            string sSql = "";
           
            string sUserName = "";
            //string sExistingUser = "";
            //string sExistingGroup = "";
            //string sSqlGroup = "";
            //string[] arrCurrentUser;
            int iDbId = 0;
            int iPos1 = 0;
            int iPos2 = 0;
            int iLen = 0;

            DbReader objReader = null;
            //DbReader objReaderExistingUserName = null;
            //DbReader objReaderExistingGroup = null;
            
            try
            {

                
                
                if ((m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR))
                {
                    iPos1 = sMainDSN.IndexOf("Database=");
                    iPos2 = sMainDSN.IndexOf("UID=");
                    sMainDbName = sMainDSN.Substring(iPos1 + 9, iPos2 - iPos1 - 10);
                    sSql = "SELECT DATABASE_ID FROM SYS.DATABASES WHERE NAME='" + sMainDbName+"'";
                }
                else
                {
                    iPos1 = sMainDSN.IndexOf("UID=");
                    iPos2 = sMainDSN.IndexOf("PWD=");
                    sMainDbName = sMainDSN.Substring(iPos1 + 4, iPos2 - iPos1 - 5);
                    //sSql = "SELECT USER_ID FROM ALL_USERS WHERE USERNAME='" + sMainDbName.ToUpper()+"'";
                    sSql = "SELECT USER_ID FROM USER_USERS";
                }

                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                        {
                            iDbId = objReader.GetInt32("DATABASE_ID");
                        }
                        else
                        {
                            iDbId = objReader.GetInt32("USER_ID");
                        }
                        //        sUserId = objReaderExistingUser.GetInt32("USER_ID").ToString();
                        //        sExistingUserId = sExistingUserId + sUserId + ",";
                    }
                    objReader.Close();
                }
                sUserName = "BES" + string.Format("{0:0000}", iDbId) + string.Format("{0:0000}", iDSNId) + string.Format("{0:00000}", iGroupID);

            }
            catch { }

            finally
            {
                if (objReader != null)
                    objReader.Dispose();

                //if (objReaderExistingUser != null)
                //    objReaderExistingUser.Dispose();
                //if (objReaderExistingGroup != null)
                //    objReaderExistingGroup.Dispose();
            }

            return sUserName;
        }



        //New method added by Nadim for 14334 to check whether a duplicate user exist or not-Code End
        /// <summary>
        /// Parijat- created in order to meet R5 needs
        /// </summary>
        /// <param name="p_objXMLDocument"></param>
        /// <returns></returns>
        public XmlDocument UpdateGroupDef(XmlDocument p_objXMLDocument)
        {
            #region local variables
            string sSQL = "";
            int iGroupId = 0;
            string sTemp = "";
            int iNum = 0;
            string sBrokers = "";
            string sInsurers = "";
            string sAdjTypes = "";
            string sNoteTypes = string.Empty;
            string sGroupName = "";
            string sEncript1 = "";
            string sEncript2 = "";
            string sEncriptUser = "";
            DbConnection objDbConn = null;
            DbTransaction objTrans = null;         // Umesh
            DbCommand objCommand = null;           // Umesh

            LocalCache objLocalCache = null;
            bool bIsTransaction = false;
            string sListText = "";
            string[] arrFields;
            bool bChkDefer = false;      // Umesh
            StringBuilder sGroupIds = new StringBuilder(); //pmittal5
            DbReader objDbReader = null; //pmittal5
            int iSuperGroupId = 0; //pmittal5
            int iConfRec = 0; //pmittal5
            List<int> lstGroupIds = new List<int>(); //pmittal5
            bool bUsersChanged = false;
            List<int> lstExistingUsers = new List<int>();
            List<int> lstUpdatedUsers = new List<int>();
            bool bEntitiessChanged = false;
            #endregion

            try
            {
                objLocalCache = new LocalCache(m_sConnString, m_iClientId);

                iGroupId = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//RowId")).InnerText);
                iConfRec = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//ConfRec")).InnerText);
                //abisht MITS 10669
                m_sOveridingPassWord = ((XmlElement)p_objXMLDocument.SelectSingleNode("//PassWord")).InnerText;
                m_sOverideFlag = ((XmlElement)p_objXMLDocument.SelectSingleNode("//OveridePassFlag")).InnerText;
                //REM Umesh  : to get the status of checkbox defer
                if (m_sCheckDefer == "1")
                    bChkDefer = true;
                //END REM

                //pmittal5 Mits 21710 - Show error if All Org Entities are selected in some other group apart from Admin
                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optOrgEntitiesValue")).InnerText == "1")
                {
                    bool bError = CheckAllEntitiesGroup(iGroupId);
                    if (!bError)
                    {
                        if (m_bMultipleGroups)
                        {
                            throw new RMAppException("There are already Groups '" + sExistingGroupName + "' with All Organizational Entities selected. Use Selected Organizational Entities and Save the Group.");
                        }
                        else
                        {
                            throw new RMAppException("There is already a Group '" + sExistingGroupName + "' with All Organizational Entities selected. Use Selected Organizational Entities and Save the Group.");
                        }
                    }
                }
                //End - pmittal5

               
                //rsushilaggar MCIC BES issue, moving this code snippet up to resolve the issue -- MITS 28000
                //nadim for 14334..Check for duplicate user(whether this user is already entered in another group or not..)

                XmlNode objIgnoreDuplicateUser = p_objXMLDocument.SelectSingleNode("//IgnoreDuplicateUser");
                if (objIgnoreDuplicateUser == null)
                {
                    bool bError = CheckDuplicateUser(p_objXMLDocument, iGroupId);
                    if (!bError)
                    {
                        if (m_bMultipleUser == true)
                        {
                            throw new RMAppException("Users  '" + sExistingUserName + "'  Already Exist  in Groups(s)  '" + sExistingGroupName + "'");
                        }
                        else
                            throw new RMAppException("User  '" + sExistingUserName + "'  Already Exists  in Group(s)  '" + sExistingGroupName + "'");
                    }
                }

                //nadim for 14334
                //end rsushilaggar

                //pmittal5 03/10/10 
                //In case Confidential Flag is ON, Consecutive GroupIds are stored for each User in BES tables.
                    //During updation, if Users are changed, all entries from BES tables are removed, and new entries are made with newly generated GroupIds, in order to avoid duplicate GroupIds.
                //If Confidential Record is OFF, separate GroupIDs are not generated for selected Users, so no need to regenerate it.

                //Fetching Group_Id for corressponding Super_Group_Id
                sSQL = "SELECT GROUP_ID FROM GROUP_MAP WHERE SUPER_GROUP_ID = " + iGroupId; 
                using (objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            lstGroupIds.Add(objDbReader.GetInt(0));
                            if (sGroupIds.Length > 0)
                                sGroupIds.Append(", " + objDbReader[0].ToString());
                            else
                                sGroupIds.Append(objDbReader[0].ToString());
                        }
                    }
                }

                //For Confidential Flag, check if Users for the Group have been changed. In case no change has been made in selected Users, there is no need to delete and then add records with new Group Ids in BES tables
                //View Definitions also wont change in this scenario
                if (iConfRec != 0)
                {
                    sSQL = "SELECT USER_ID FROM ORG_SECURITY_USER WHERE GROUP_ID IN (" + sGroupIds + ")";
                    using (objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL))
                    {
                        if (objDbReader != null)
                        {
                            while (objDbReader.Read())
                            {
                                lstExistingUsers.Add(objDbReader.GetInt(0)); //Add existing users in list
                            }
                        }
                        if (lstExistingUsers.Count > 0)
                            lstExistingUsers.Sort();
                    }
                    sListText = p_objXMLDocument.SelectSingleNode("//UserListItems").InnerText; //Fetch User Ids which are entered during update
                    if (sListText.Trim() != "")  //Compare existing users with new ones
                    {
                        arrFields = sListText.Split('|');
                        for (int i = 0; i < arrFields.Length; i++)
                        {
                            if (i % 2 > 0) //arrFields contains UserName and UserId alternatively. Fetch only UserIds.
                            {
                                lstUpdatedUsers.Add(Conversion.ConvertStrToInteger(arrFields[i]));
                            }
                        }
                        if (lstUpdatedUsers.Count > 0)
                            lstUpdatedUsers.Sort();
                    }

                    if (lstExistingUsers.Count != lstUpdatedUsers.Count)
                    {
                        bUsersChanged = true;
                    }
                    else
                    {
                        if (lstExistingUsers.Count != 0 && lstUpdatedUsers.Count != 0)
                        {
                            for (int i = 0; i < lstExistingUsers.Count; i++)
                            {
                                if (lstExistingUsers[i] != lstUpdatedUsers[i])
                                {
                                    bUsersChanged = true;
                                    break;
                                }
                            }
                        }
                        else
                            bUsersChanged = false;
                    }
                }
                //Moved Above by Deb
                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optOrgEntitiesValue")).InnerText == "1")
                {
                    sTemp = "<ALL>";
                    m_bIsAllOrgEntities = true;
                }
                else
                {
                    List<int> lstExistingEntities = new List<int>();
                    List<int> lstUpdatedEntities = new List<int>();
                    StringBuilder sExistingEnt = new StringBuilder();
                    sSQL = "SELECT GROUP_ENTITIES FROM ORG_SECURITY WHERE GROUP_ID IN (" + sGroupIds + ")";
                    using (objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL))
                    {
                        if (objDbReader != null)
                        {
                            while (objDbReader.Read())
                            {
                                if (sExistingEnt.Length > 0)
                                    sExistingEnt.Append(", " + objDbReader[0].ToString());
                                else
                                    sExistingEnt.Append(objDbReader[0].ToString());
                            }
                        }
                        
                    }
                    foreach (string sEid in sExistingEnt.ToString().Split(','))
                    {
                        lstExistingEntities.Add(int.Parse(sEid));
                    }
                    if (lstExistingEntities.Count > 0)
                        lstExistingEntities.Sort();
                    sTemp = "";
                    sListText = p_objXMLDocument.SelectSingleNode("//SelEntitiesListItems").InnerText;
                    if (sListText.Trim() != "")
                    {
                        arrFields = sListText.Split('|');
                        if (arrFields.Length > 0)
                        {
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if ((i % 2) > 0)
                                {
                                    iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                    lstUpdatedEntities.Add(iNum);
                                    if (sTemp != "")
                                        sTemp += ",";
                                    sTemp += iNum.ToString();
                                }
                            }
                        }
                        if (lstUpdatedEntities.Count > 0)
                            lstUpdatedEntities.Sort();
                    }
                    if (lstExistingEntities.Count != lstUpdatedEntities.Count)
                    {
                        bEntitiessChanged = true;
                    }
                    else
                    {
                        if (lstExistingEntities.Count != 0 && lstUpdatedEntities.Count != 0)
                        {
                            for (int i = 0; i < lstExistingEntities.Count; i++)
                            {
                                if (lstExistingEntities[i] != lstUpdatedEntities[i])
                                {
                                    bEntitiessChanged = true;
                                    break;
                                }
                            }
                        }
                        else
                            bEntitiessChanged = false;
                    }
                }
                //Moved Above by Deb
                //Set DELETED_FLAG to -1 in GROUP_MAP table for the group, so that all the views for this group are dropped when BESScheduler is run
                if (iConfRec != 0 && bUsersChanged) //Do this only if users are also updated
                {
                    lstGroupIds.Clear(); //lstGroupIds will be filled with new GroupIds in the later section of code
                    sSQL = "UPDATE GROUP_MAP SET DELETED_FLAG = -1 WHERE SUPER_GROUP_ID = " + iGroupId;
                    m_objDbCommand.CommandText = sSQL;
                    m_objDbCommand.ExecuteNonQuery();
                }

                //GROUP_ID in ORG_SECURITY is  Super Group Id i.e. actual BES Group ID 
                if (iConfRec != 0 && bUsersChanged) //Do this only if users are also updated
                {
                    sSQL = "DELETE FROM ORG_SECURITY WHERE GROUP_ID = " + iGroupId;
                    m_objDbCommand.CommandText = sSQL;
                    m_objDbCommand.ExecuteNonQuery();
                }
                //Deb MITS 29360-Commented Out
                //GROUP_ID in ENTITY_MAP is  Super Group Id i.e. actual BES Group ID 
                //if (bEntitiessChanged)//Deb : Added the check
                //{
                //    sSQL = "DELETE FROM ENTITY_MAP WHERE GROUP_ID = " + iGroupId;
                //    m_objDbCommand.CommandText = sSQL;
                //    m_objDbCommand.ExecuteNonQuery();
                //}
                //Deb MITS 29360
                //GROUP_ID in ORG_SECURITY_USER is  User Level Group Id
                sSQL = "DELETE FROM ORG_SECURITY_USER WHERE GROUP_ID IN (" + sGroupIds + ")";
                m_objDbCommand.CommandText = sSQL;
                m_objDbCommand.ExecuteNonQuery();
                //Deb MITS 29360-Commented out
                //GROUP_ID in ENTITY_MAP_USER is  User Level Group Id
                //sSQL = "DELETE FROM ENTITY_MAP_USER WHERE GROUP_ID IN (" + sGroupIds + ")";
                //m_objDbCommand.CommandText = sSQL;
                //m_objDbCommand.ExecuteNonQuery();
                //End - pmittal5
                //build entity list
                //Clear out new ORG_SECURITY_EIDS table
                //Deb MITS 29360
                //Deb MITS 29627 --Commemnted
                //if (bEntitiessChanged) //Deb : Added the check
                //{
                //    try
                //    {
                //        //it is possible not all installations will have this table
                //        sSQL = "DELETE FROM ORG_SECURITY_EIDS WHERE GROUP_ID = " + iGroupId;
                //        //RunSql(sSQL, m_objDbConnection);
                //        m_objDbCommand.CommandText = sSQL;
                //        m_objDbCommand.ExecuteNonQuery();
                //    }
                //    catch { }
                //}
                //Deb MITS 29627
                if (iConfRec != 0 && bUsersChanged)  // Regenerate Group_Id only if users are changed and Conf_Flag is ON
                {
                    sSQL = "SELECT MAX(GROUP_ID) FROM USER_ORGSEC";
                    using (objDbReader = DbFactory.GetDbReader(m_sSecurityDSN, sSQL))
                    {
                        if (objDbReader != null)
                        {
                            if (objDbReader.Read())
                                iGroupId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;

                        }
                    }
                }

                objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
                objDbConn.Open();
                objCommand = objDbConn.CreateCommand();
                if (bChkDefer)
                {
                    objTrans = objDbConn.BeginTransaction();
                    objCommand.Transaction = objTrans;
                    bIsTransaction = true;
                }
                //GROUP_ID in USER_ORGSEC is  User Level Group Id
                sSQL = "DELETE FROM USER_ORGSEC WHERE GROUP_ID IN (" + sGroupIds + ") AND DSN_ID = " + m_sDSNId;//Deb MITS 29360
                objCommand.CommandText = sSQL;
                objCommand.ExecuteNonQuery(); 
                //End -pmittal5

                //Write entity selection info
                if (bEntitiessChanged) //Deb : Added the check
                {
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optOrgEntitiesValue")).InnerText == "1")
                    {
                        sTemp = "<ALL>";
                        m_bIsAllOrgEntities = true;
                    }
                    else
                    {
                        sTemp = "";
                        sListText = p_objXMLDocument.SelectSingleNode("//SelEntitiesListItems").InnerText;
                        if (sListText.Trim() != "")
                        {
                            arrFields = sListText.Split('|');
                            if (arrFields.Length > 0)
                            {
                                for (int i = 0; i < arrFields.Length; i++)
                                {
                                    if ((i % 2) > 0)
                                    {
                                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                        if (sBESSynSetting.ToUpper() != "FALSE") //Deb : MITS 29215
                                         EnumDepts(iNum, m_objDbConnection);
                                        if (sTemp != "")
                                            sTemp += ",";
                                        sTemp += iNum.ToString();
                                        //Deb MITS 29627
                                        //try
                                        //{
                                        //    //Write entry to ORG_SECURITY_EIDS table. Allows trigger resync of ENTITY_MAP table.
                                        //    sSQL = "INSERT INTO ORG_SECURITY_EIDS(GROUP_ID,ENTITY_ID,ENTITY_TABLE_ID) VALUES (" + iGroupId + "," + iNum + "," + objLocalCache.GetOrgTableId(iNum) + ")";
                                        //    //RunSql(sSQL, m_objDbConnection); 
                                        //    m_objDbCommand.CommandText = sSQL;
                                        //    m_objDbCommand.ExecuteNonQuery();
                                        //}
                                        //catch { }
                                        //Deb MITS 29627
                                    }
                                }
                            }
                        }
                        m_bIsAllOrgEntities = false;
                    }
                }
                //Write out policy brokers & insurers IFF selected and dbms is oracle
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optBrokersValue")).InnerText == "2")
                    {
                        sListText = p_objXMLDocument.SelectSingleNode("//BrokersListItems").InnerText;
                        if (sListText.Trim() != "")
                        {
                            arrFields = sListText.Split('|');
                            if (arrFields.Length > 0)
                            {
                                for (int i = 0; i < arrFields.Length; i++)
                                {
                                    if ((i % 2) > 0)
                                    {
                                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                        if (sBrokers != "")
                                            sBrokers += ",";
                                        sBrokers += iNum.ToString();
                                    }
                                }
                            }
                        }
                    }

                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optInsurersValue")).InnerText == "2")
                    {
                        sListText = p_objXMLDocument.SelectSingleNode("//InsurersListItems").InnerText;
                        if (sListText.Trim() != "")
                        {
                            arrFields = sListText.Split('|');
                            if (arrFields.Length > 0)
                            {
                                for (int i = 0; i < arrFields.Length; i++)
                                {
                                    if ((i % 2) > 0)
                                    {
                                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                        if (sInsurers != "")
                                            sInsurers += ",";
                                        sInsurers += iNum.ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                //Parijat :Boeing broker Support
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR && g_bBoeingBrokerSupport == true)
                {
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optBrokersValue")).InnerText == "2")
                    {
                        sListText = p_objXMLDocument.SelectSingleNode("//BrokersListItems").InnerText;
                        if (sListText.Trim() != "")
                        {
                            arrFields = sListText.Split('|');
                            if (arrFields.Length > 0)
                            {
                                for (int i = 0; i < arrFields.Length; i++)
                                {
                                    if ((i % 2) > 0)
                                    {
                                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                        if (sBrokers != "")
                                            sBrokers += ",";
                                        sBrokers += iNum.ToString();
                                    }
                                }
                            }
                        }
                    }
                }

                //Write out adjuster text types
                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                {
                    sListText = p_objXMLDocument.SelectSingleNode("//AdjusterListItems").InnerText;
                    if (sListText.Trim() != "")
                    {
                        arrFields = sListText.Split('|');
                        if (arrFields.Length > 0)
                        {
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if ((i % 2) > 0)
                                {
                                    iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                    if (sAdjTypes != "")
                                        sAdjTypes += ",";
                                    sAdjTypes += iNum.ToString();
                                }
                            }
                        }
                    }
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optAdjTextTypesValue")).InnerText == "3")
                        sAdjTypes = "-" + sAdjTypes;
                }
                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue")).InnerText == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue")).InnerText == "3")
                {
                    sListText = p_objXMLDocument.SelectSingleNode("//NotesListItems").InnerText;
                    if (sListText.Trim() != "")
                    {
                        arrFields = sListText.Split('|');
                        if (arrFields.Length > 0)
                        {
                            for (int i = 0; i < arrFields.Length; i++)
                            {
                                if ((i % 2) > 0)
                                {
                                    iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                    if (sNoteTypes != "")
                                        sNoteTypes += ",";
                                    sNoteTypes += iNum.ToString();
                                }
                            }
                        }
                    }
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//optEnhNoteTypesValue")).InnerText == "3")
                        sNoteTypes = "-" + sNoteTypes;
                }
//Boeing Broker Support
                //Update ORG_SECURITY profile
                if (g_bBoeingBrokerSupport)
                {
                    sGroupName = ((XmlElement)p_objXMLDocument.SelectSingleNode("//GroupName")).InnerText;
                    if (iConfRec != 0 && bUsersChanged)  // Group_Id was changed if users are changed and Conf_Flag is ON
                    {
                        sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_ENTITIES,GROUP_DEPT,GROUP_ADJ_TEXT_TYPES," +
                                " DELETED_FLAG,GROUP_ENH_NOTES_TYPES";
                        if (sBrokers != "")
                            sSQL += ",GROUP_BROKERS";
                        if (sInsurers != "")
                            sSQL += ",GROUP_INSURERS";
                        sSQL += ") VALUES (" + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + ",'" + sTemp + "'," +
                                " 0," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",0," + Utilities.FormatSqlFieldValue(sNoteTypes);
                        if (sBrokers != "")
                            sSQL += ",'" + sBrokers + "'";
                        if (sInsurers != "")
                            sSQL += ",'" + sInsurers + "'";
                        sSQL += ")";
                    }
                    else
                    {
                        sSQL = "UPDATE ORG_SECURITY SET GROUP_NAME = " + Utilities.FormatSqlFieldValue(sGroupName) + ", GROUP_DEPT = 0, GROUP_ENTITIES = '" + sTemp + "'";
                        if (sBrokers != "")
                            sSQL += ", GROUP_BROKERS = '" + sBrokers + "'";
                        if (sInsurers != "")
                            sSQL += ", GROUP_INSURERS = '" + sInsurers + "'";
                        sSQL += ", GROUP_ADJ_TEXT_TYPES = " + Utilities.FormatSqlFieldValue(sAdjTypes);
                        sSQL += ", GROUP_ENH_NOTES_TYPES = " + Utilities.FormatSqlFieldValue(sNoteTypes);
                        sSQL += " WHERE GROUP_ID = " + iGroupId;
                    }
                }
                else
                {
                    sGroupName = ((XmlElement)p_objXMLDocument.SelectSingleNode("//GroupName")).InnerText;
                    if (iConfRec != 0 && bUsersChanged)  // Group_Id was changed if users are changed and Conf_Flag is ON
                    {
                        sSQL = "INSERT INTO ORG_SECURITY (GROUP_ID,GROUP_NAME,GROUP_ENTITIES,GROUP_DEPT,GROUP_ADJ_TEXT_TYPES," +
                                " DELETED_FLAG,GROUP_ENH_NOTES_TYPES";
                        if (m_bIsInsurerSupport && m_bIsBrokerSupport)
                            sSQL += ",GROUP_BROKERS,GROUP_INSURERS";
                        sSQL += ") VALUES (" + iGroupId + "," + Utilities.FormatSqlFieldValue(sGroupName) + ",'" + sTemp + "'," +
                                " 0," + Utilities.FormatSqlFieldValue(sAdjTypes) + ",0," + Utilities.FormatSqlFieldValue(sNoteTypes);
                        if (m_bIsInsurerSupport && m_bIsBrokerSupport)
                            sSQL += ",'" + sBrokers + "','" + sInsurers + "'";
                        sSQL += ")";
                    }
                    else
                    {
                        sSQL = "UPDATE ORG_SECURITY SET GROUP_NAME = " + Utilities.FormatSqlFieldValue(sGroupName) + ", GROUP_DEPT = 0, GROUP_ENTITIES = '" + sTemp + "'";
                        //Nadim for Rem
                        if (m_bIsInsurerSupport && m_bIsBrokerSupport)
                        {
                            sSQL += ", GROUP_BROKERS = '" + sBrokers + "'";

                            sSQL += ", GROUP_INSURERS = '" + sInsurers + "'";
                        }
                        //Nadim for rem
                        sSQL += ", GROUP_ADJ_TEXT_TYPES = " + Utilities.FormatSqlFieldValue(sAdjTypes);
                        sSQL += ", GROUP_ENH_NOTES_TYPES = " + Utilities.FormatSqlFieldValue(sNoteTypes);
                        sSQL += " WHERE GROUP_ID = " + iGroupId;
                    }
                }
                iSuperGroupId = iGroupId;
                //End - pmittal5
                //RunSql(sSQL, m_objDbConnection);
                m_objDbCommand.CommandText = sSQL;
                m_objDbCommand.ExecuteNonQuery();

                //save users
                //RunSql("DELETE FROM ORG_SECURITY_USER WHERE GROUP_ID = " + iGroupId,m_objDbConnection);
                //m_objDbCommand.CommandText = "DELETE FROM ORG_SECURITY_USER WHERE GROUP_ID = " + iGroupId;
                //m_objDbCommand.ExecuteNonQuery();

                //objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
                //objDbConn.Open();
                //objCommand = objDbConn.CreateCommand();
                //if (bChkDefer)
                //{
                //    objTrans = objDbConn.BeginTransaction();
                //    objCommand.Transaction = objTrans;
                //    bIsTransaction = true;
                //}
                //objCommand.CommandText = "DELETE FROM USER_ORGSEC WHERE GROUP_ID = " + iGroupId + " AND DSN_ID = " + m_sDSNId;
                //objCommand.ExecuteNonQuery();

                //RunSql("DELETE FROM USER_ORGSEC WHERE GROUP_ID = " + iGroupId + " AND DSN_ID = " + m_sDSNId,objDbConn);

                sListText = p_objXMLDocument.SelectSingleNode("//UserListItems").InnerText;
                if (sListText.Trim() != "")
                {
                    arrFields = sListText.Split('|');
                    if (arrFields.Length > 0)
                    {
                        for (int i = 0; i < arrFields.Length; i++)
                        {
                            if ((i % 2) > 0)
                            {
                                iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
                                sSQL = "INSERT INTO ORG_SECURITY_USER (GROUP_ID,USER_ID) VALUES(" + iGroupId + "," + iNum + ")";
                                //RunSql(sSQL,m_objDbConnection);
                                m_objDbCommand.CommandText = sSQL;
                                m_objDbCommand.ExecuteNonQuery();

                                if (m_bIsAllOrgEntities)
                                {
                                    if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                                    {
                                        //DBO signals that the login routines should use the DSN login
                                        sEncript1 = RMCryptography.EncryptString("DBO");
                                        sEncript2 = RMCryptography.EncryptString("DBO");
                                    }
                                    else
                                    {
                                        sEncript1 = RMCryptography.EncryptString(m_sDBOPwd);
                                        sEncript2 = RMCryptography.EncryptString(m_sDBOUserId);
                                    }
                                }
                                else
                                {

                                    sEncriptUser = FormatBESUserName(Conversion.ConvertStrToInteger(m_sDSNId), m_sConnString, iGroupId);
                                    sEncript2 = RMCryptography.EncryptString(sEncriptUser);
                                    //Mjain8 Added for sql server 2005 compatibility
                                    if ((m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR))
                                       sEncript1 = RMCryptography.EncryptString(BES_COMPLEX_PASSWORD + string.Format("{0:00000}", iGroupId));
                                    else
                                        sEncript1 = sEncript2;
                                }

                                //RunSql("DELETE FROM USER_ORGSEC WHERE USER_ID = " + iNum + " AND DSN_ID = " + m_sDSNId,objDbConn);
                                //Deb MITS 29360
                                //objCommand.CommandText = "DELETE FROM USER_ORGSEC WHERE USER_ID = " + iNum + " AND DSN_ID = " + m_sDSNId;
                                //objCommand.ExecuteNonQuery();
                                //Deb MITS 29360
                                //abisht 10669
                                //pmittal5 - Password change functionality in case of SQL also.
                                //if (m_sOverideFlag == "0" || m_sOverideFlag == "" || m_stDataBaseType != eDatabaseType.DBMS_IS_ORACLE)
                                if (m_sOverideFlag == "0" || m_sOverideFlag == "")
                                {
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD,PWD_CHANGED_FLAG) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ",0)";
                                }
                                else
                                {
                                    sEncript1 = RMCryptography.EncryptString(m_sOveridingPassWord);
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD,PWD_CHANGED_FLAG) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ",-1)";
                                }
                                //RunSql(sSQL,objDbConn);
                                objCommand.CommandText = sSQL;
                                objCommand.ExecuteNonQuery();
                                //pmittal5 03/10/10 - Insert new Group_Ids in GROUP_MAP only if Conf_Flag is ON and users are changed
                                if(iConfRec != 0 && bUsersChanged)
                                {
                                    sSQL = "INSERT INTO GROUP_MAP (SUPER_GROUP_ID,GROUP_ID,DELETED_FLAG) VALUES(" + iSuperGroupId + "," + iGroupId + ",0)";
                                    m_objDbCommand.CommandText = sSQL;
                                    m_objDbCommand.ExecuteNonQuery();
                                    lstGroupIds.Add(iGroupId);
                                }
                                if(iConfRec != 0)
                                    iGroupId++;
                                //End - pmittal5
                        }
                    }
                }
                }
                //pmittal5 - UPDATED_FLAG of Group_map and Org_security will be set to -1 only for those groups for which users have been changed
                bool bSetUpdatedFlag = false;
                if (iConfRec != 0 && bUsersChanged)
                    bSetUpdatedFlag = true;
                if (bEntitiessChanged) //Deb : Added the check
                    bSetUpdatedFlag = true;
                if ((m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)) //Deb : Added the check
                    //BuildEntityMap_SQLSybase(iGroupId, sAdjTypes, sBrokers,false);//Parijat:Boeing --Broker Support
                    BuildEntityMap_SQLSybase(iSuperGroupId, sAdjTypes, sBrokers, lstGroupIds, bSetUpdatedFlag, bEntitiessChanged,"UPDATE");
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE) //Deb : Added the check
                    //BuildEntityMap_Oracle(iGroupId, sBrokers, sInsurers, sAdjTypes,false);)
                    BuildEntityMap_Oracle(iSuperGroupId, sBrokers, sInsurers, sAdjTypes, lstGroupIds, bSetUpdatedFlag, bEntitiessChanged,"UPDATE");
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_DB2) //Deb : Added the check
                    //BuildEntityMap_DB2(iGroupId, sBrokers, sInsurers, sAdjTypes);
                    BuildEntityMap_DB2(iSuperGroupId, sBrokers, sInsurers, sAdjTypes, lstGroupIds, bEntitiessChanged,"UPDATE");
                //End - pmittal5

                arDeptList = null;

                if (bChkDefer)
                {
                    objTrans.Commit();

                    if (m_objAdminDbCommand != m_objDbCommand)
                    {
                        m_objDbTrans.Commit();
                        m_objAdminDbTrans.Commit();
                    }
                    else
                        m_objDbTrans.Commit();
                }

                if (objDbConn != null)
                    objDbConn.Close();

                return p_objXMLDocument;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                m_objDbTrans.Rollback();
                m_objDbConnection.Close();
                if (bIsTransaction)
                objTrans.Rollback();
                objDbConn.Close();
                m_objAdminDbTrans.Rollback();
                m_objAdminDbConnection.Close();
                throw new RMAppException(Globalization.GetString("ORGSEC.UpdateGroupDef.Error",m_iClientId), p_objExp);
            }
            finally
            {
                objCommand = null;
                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();

                if (objDbConn != null)
                    objDbConn.Dispose();
                if (objAdminDbConnection != null)
                    objAdminDbConnection.Dispose();
                if (m_objAdminDbCommand != null)
                    m_objAdminDbCommand = null;
                if (m_objDbCommand != null)
                    m_objDbCommand = null;
                if (m_objDbTrans != null)
                    m_objDbTrans.Dispose();
                if (m_objAdminDbTrans != null)
                    m_objAdminDbTrans.Dispose();
                if (objDbReader != null)  //pmittal5
                    objDbReader.Dispose();
            }
        }

		/// Name		: UpdateGroupDef
		/// Author		: Anurag Agarwal
		/// Date Created	: 25 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves the Group defination for the GroupId
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group to be Saved</param>
		/// <returns>True for success & False for failure</returns>
        //public XmlDocument UpdateGroupDef_old(XmlDocument p_objXMLDocument)
        //{
        //    string sSQL = "";
        //    int iGroupId = 0;
        //    string sTemp = "";
        //    int iNum = 0;
        //    string sBrokers = "";
        //    string sInsurers = "";
        //    string sAdjTypes = "";
        //    string sGroupName = "";
        //    string sEncript1 = "";
        //    string sEncript2 = "";
        //    DbConnection objDbConn = null;
        //    DbTransaction objTrans = null;         // Umesh
        //    DbCommand objCommand = null;           // Umesh
            
        //    LocalCache objLocalCache = null;
			
        //    string sListText = "";
        //    string [] arrFields;
        //    bool bChkDefer = false;      // Umesh
	
        //    try
        //    {
		
        //        objLocalCache = new LocalCache(m_sConnString);

        //        iGroupId = Conversion.ConvertStrToInteger(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText); 
        //        //abisht MITS 10669
        //        m_sOveridingPassWord = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='PassWord']")).InnerText;
        //        m_sOverideFlag = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='OveridePassFlag']")).InnerText;
        //        //REM Umesh  : to get the status of checkbox defer
        //        if (m_sCheckDefer == "1")
        //            bChkDefer = true;
        //        //END REM
        //        //build entity list
        //        //Clear out new ORG_SECURITY_EIDS table
                
        //        try
        //        {
        //            //it is possible not all installations will have this table
        //            sSQL = "DELETE FROM ORG_SECURITY_EIDS WHERE GROUP_ID = " + iGroupId;
        //            //RunSql(sSQL, m_objDbConnection);
        //            m_objDbCommand.CommandText = sSQL;
        //            m_objDbCommand.ExecuteNonQuery();

        //        }	
        //        catch{}

        //        //Write entity selection info
        //        if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optOrgEntities']")).GetAttribute("value") == "1")
        //        {
        //            sTemp =	"<ALL>";
        //            m_bIsAllOrgEntities = true;
        //        }
        //        else
        //        {
        //            sTemp = "";
        //            sListText = p_objXMLDocument.SelectSingleNode("//control[@name='SelEntitiesListItems']").InnerText;
        //            if(sListText.Trim() != "")
        //            {
        //                arrFields = sListText.Split('|');
        //                if(arrFields.Length > 0)
        //                {
        //                    for(int i = 0; i < arrFields.Length; i++)
        //                    {
        //                        if((i % 2) > 0)
        //                        {
        //                            iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                            EnumDepts(iNum,m_objDbConnection); 
        //                            if(sTemp != "")
        //                                sTemp += ",";
        //                            sTemp += iNum.ToString();

        //                            try
        //                            {
        //                                //Write entry to ORG_SECURITY_EIDS table. Allows trigger resync of ENTITY_MAP table.
        //                                sSQL = "INSERT INTO ORG_SECURITY_EIDS(GROUP_ID,ENTITY_ID,ENTITY_TABLE_ID) VALUES (" + iGroupId + "," + iNum + "," + objLocalCache.GetOrgTableId(iNum) + ")";
        //                                //RunSql(sSQL, m_objDbConnection); 
        //                                m_objDbCommand.CommandText = sSQL;
        //                                m_objDbCommand.ExecuteNonQuery();
        //                            }
        //                            catch {}
        //                        }
        //                    }
        //                }
        //            }
        //            m_bIsAllOrgEntities = false;
        //        }	

        //        //Write out policy brokers & insurers IFF selected and dbms is oracle
        //        if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //        {
        //            if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optBrokers']")).GetAttribute("value") == "2")
        //            {
        //                sListText = p_objXMLDocument.SelectSingleNode("//control[@name='BrokersListItems']").InnerText;
        //                if(sListText.Trim() != "")
        //                {
        //                    arrFields = sListText.Split('|');
        //                    if(arrFields.Length > 0)
        //                    {
        //                        for(int i = 0; i < arrFields.Length; i++)
        //                        {
        //                            if((i % 2) > 0)
        //                            {
        //                                iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                                if(sBrokers != "")
        //                                    sBrokers += ",";
        //                                sBrokers += iNum.ToString();
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optInsurers']")).GetAttribute("value") == "2")
        //            {
        //                sListText = p_objXMLDocument.SelectSingleNode("//control[@name='InsurersListItems']").InnerText;
        //                if(sListText.Trim() != "")
        //                {
        //                    arrFields = sListText.Split('|');
        //                    if(arrFields.Length > 0)
        //                    {
        //                        for(int i = 0; i < arrFields.Length; i++)
        //                        {
        //                            if((i % 2) > 0)
        //                            {
        //                                iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                                if(sInsurers != "")
        //                                    sInsurers += ",";
        //                                sInsurers += iNum.ToString();
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }

        //        //Write out adjuster text types
        //        if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "2" || ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "3")
        //        {
        //            sListText = p_objXMLDocument.SelectSingleNode("//control[@name='AdjusterListItems']").InnerText;
        //            if(sListText.Trim() != "")
        //            {
        //                arrFields = sListText.Split('|');
        //                if(arrFields.Length > 0)
        //                {
        //                    for(int i = 0; i < arrFields.Length; i++)
        //                    {
        //                        if((i % 2) > 0)
        //                        {
        //                            iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                            if(sAdjTypes != "")
        //                                sAdjTypes += ",";
        //                            sAdjTypes += iNum.ToString();
        //                        }
        //                    }
        //                }
        //            }
        //            if(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='optAdjTextTypes']")).GetAttribute("value") == "3")
        //                sAdjTypes = "-" + sAdjTypes;
        //        }

        //        //Update ORG_SECURITY profile
        //        sGroupName = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='GroupName']")).InnerText; 
        //        sSQL = "UPDATE ORG_SECURITY SET GROUP_NAME = " + Utilities.FormatSqlFieldValue(sGroupName) + ", GROUP_DEPT = 0, GROUP_ENTITIES = '" + sTemp + "'";
        //        if(sBrokers != "")
        //            sSQL += ", GROUP_BROKERS = '" + sBrokers + "'";
        //        if(sInsurers != "")
        //            sSQL += ", GROUP_INSURERS = '" + sInsurers + "'";
        //        sSQL += ", GROUP_ADJ_TEXT_TYPES = " + Utilities.FormatSqlFieldValue(sAdjTypes);	
        //        sSQL += " WHERE GROUP_ID = " + iGroupId;
        //        //RunSql(sSQL, m_objDbConnection);
        //        m_objDbCommand.CommandText = sSQL;
        //        m_objDbCommand.ExecuteNonQuery();

        //        //save users
        //        //RunSql("DELETE FROM ORG_SECURITY_USER WHERE GROUP_ID = " + iGroupId,m_objDbConnection);
        //        m_objDbCommand.CommandText = "DELETE FROM ORG_SECURITY_USER WHERE GROUP_ID = " + iGroupId;
        //        m_objDbCommand.ExecuteNonQuery();

        //        objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
        //        objDbConn.Open();
        //        objCommand = objDbConn.CreateCommand();
        //        if (bChkDefer)
        //        {
        //            objTrans = objDbConn.BeginTransaction();
        //            objCommand.Transaction = objTrans;
        //        }
        //        objCommand.CommandText = "DELETE FROM USER_ORGSEC WHERE GROUP_ID = " + iGroupId + " AND DSN_ID = " + m_sDSNId;
        //        objCommand.ExecuteNonQuery();

        //        //RunSql("DELETE FROM USER_ORGSEC WHERE GROUP_ID = " + iGroupId + " AND DSN_ID = " + m_sDSNId,objDbConn);

				

        //        sListText = p_objXMLDocument.SelectSingleNode("//control[@name='UserListItems']").InnerText;
        //        if(sListText.Trim() != "")
        //        {
        //            arrFields = sListText.Split('|');
        //            if(arrFields.Length > 0)
        //            {
        //                for(int i = 0; i < arrFields.Length; i++)
        //                {
        //                    if((i % 2) > 0)
        //                    {
        //                        iNum = Conversion.ConvertStrToInteger(arrFields[i].ToString());
        //                        sSQL = "INSERT INTO ORG_SECURITY_USER (GROUP_ID,USER_ID) VALUES(" + iGroupId + "," + iNum + ")";
        //                        //RunSql(sSQL,m_objDbConnection);
        //                        m_objDbCommand.CommandText = sSQL;
        //                        m_objDbCommand.ExecuteNonQuery();

        //                        if(m_bIsAllOrgEntities)
        //                        {
        //                            if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //                            {
        //                                //DBO signals that the login routines should use the DSN login
        //                                sEncript1 = RMCryptography.EncryptString("DBO");
        //                                sEncript2 = RMCryptography.EncryptString("DBO");
        //                            }
        //                            else
        //                            {
        //                                sEncript1 = RMCryptography.EncryptString(m_sDBOPwd);
        //                                sEncript2 = RMCryptography.EncryptString(m_sDBOUserId);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            sEncript2 = RMCryptography.EncryptString("sg" + string.Format("{0:00000}",iGroupId));
        //                            if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
        //                                sEncript1 = RMCryptography.EncryptString(((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='DB2Password']")).InnerText);
        //                                //Mjain8 Added for sql server 2005 compatibility
        //                            else if((m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR))
        //                                sEncript1=RMCryptography.EncryptString(BES_COMPLEX_PASSWORD + string.Format("{0:00000}",iGroupId));
        //                            else
        //                                sEncript1 = sEncript2; 
        //                        }
                                
        //                        //RunSql("DELETE FROM USER_ORGSEC WHERE USER_ID = " + iNum + " AND DSN_ID = " + m_sDSNId,objDbConn);
        //                        objCommand.CommandText = "DELETE FROM USER_ORGSEC WHERE USER_ID = " + iNum + " AND DSN_ID = " + m_sDSNId;
        //                        objCommand.ExecuteNonQuery();
        //                        //abisht 10669
        //                        if (m_sOverideFlag == "0" || m_sOverideFlag == "" || m_stDataBaseType != eDatabaseType.DBMS_IS_ORACLE)
        //                        {
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
        //                        }
        //                        else
        //                        {
        //                            sEncript1 = RMCryptography.EncryptString(m_sOveridingPassWord);
        //                            //gagnihotri MITS 11995 Changes made for Audit table
        //                            sSQL = "INSERT INTO USER_ORGSEC (DSN_ID, USER_ID, DB_UID, DB_PWD, GROUP_ID, UPDATED_BY_USER, DTTM_RCD_LAST_UPD) VALUES (" + m_sDSNId + "," + iNum + "," + Utilities.FormatSqlFieldValue(sEncript2) + "," + Utilities.FormatSqlFieldValue(sEncript1) + "," + iGroupId + ",'" + m_sUserLogin + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
        //                        }
        //                        //RunSql(sSQL,objDbConn);
        //                        objCommand.CommandText = sSQL;
        //                        objCommand.ExecuteNonQuery();
        //                    }
        //                }
        //            }
        //        }

        //        if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
        //            BuildEntityMap_SQLSybase(iGroupId, sAdjTypes,sBrokers,false);//Parijat:Boeing --Broker Support
        //        else if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
        //            BuildEntityMap_Oracle(iGroupId, sBrokers, sInsurers, sAdjTypes,false);
        //        else if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
        //            BuildEntityMap_DB2(iGroupId, sBrokers, sInsurers, sAdjTypes);

        //        arDeptList = null;

        //        if (bChkDefer)
        //        {
        //            objTrans.Commit();

        //            if (m_objAdminDbCommand != m_objDbCommand)
        //            {
        //                m_objDbTrans.Commit();
        //                m_objAdminDbTrans.Commit();
        //            }
        //            else
        //                m_objDbTrans.Commit();
        //        }
                
        //        if (objDbConn != null)
        //        objDbConn.Close();
		
        //        return p_objXMLDocument;
        //    }
        //    catch(Exception p_objExp)
        //    {
        //        m_objDbTrans.Rollback();
        //        m_objDbConnection.Close();
        //        objTrans.Rollback();
        //        objDbConn.Close();
        //        m_objAdminDbTrans.Rollback();
        //        m_objAdminDbConnection.Close();
        //        throw new RMAppException(Globalization.GetString("ORGSEC.UpdateGroupDef.Error"),p_objExp);   
        //    }
        //    finally
        //    {
        //        objCommand = null;
        //        if (objLocalCache != null)
        //            objLocalCache.Dispose();
        //        if(m_objDbConnection != null)
        //            m_objDbConnection.Dispose();

        //        if(objDbConn != null)
        //            objDbConn.Dispose();
        //        if (objAdminDbConnection != null)
        //            objAdminDbConnection.Dispose();
        //        if (m_objAdminDbCommand != null)
        //            m_objAdminDbCommand = null;
        //        if (m_objDbCommand != null)
        //            m_objDbCommand = null;
        //        if (m_objDbTrans != null)
        //            m_objDbTrans.Dispose();
        //        if (m_objAdminDbTrans != null)
        //            m_objAdminDbTrans.Dispose();
        //    }
        //}

		/// Name		: DeleteGroupDef
		/// Author		: Anurag Agarwal
		/// Date Created	: 19 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes the Group defination
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group to be deleted</param>
		/// <returns>True for success & False for failure</returns>
		public bool DeleteGroupDef(int p_iGroupId)
		{
			DbConnection objDbConn = null;
            DbReader objReader = null;  //pmittal5
            StringBuilder sGroupIds = new StringBuilder();  //pmittal5
			try
			{
                //pmittal5 - Confidential Record 
                RunSql("UPDATE GROUP_MAP SET DELETED_FLAG=-1 WHERE SUPER_GROUP_ID = " + p_iGroupId, m_objDbConnection);
                using (objReader = DbFactory.GetDbReader(m_sConnString, "SELECT GROUP_ID FROM GROUP_MAP WHERE SUPER_GROUP_ID = " + p_iGroupId))
                {
                    if (objReader != null)
                    {
                        while (objReader.Read())
                        {
                            if (sGroupIds.Length > 0)
                                sGroupIds.Append(",");
                            sGroupIds.Append(objReader[0].ToString());
                        }
                    }
                }
                //RunSql("UPDATE  ORG_SECURITY SET DELETED_FLAG=-1 WHERE GROUP_ID = " + p_iGroupId,m_objDbConnection);
                RunSql("DELETE FROM ORG_SECURITY WHERE GROUP_ID = " + p_iGroupId, m_objDbConnection);
                //Deb :MITS 29627
                //try
                //{
                //    RunSql("DELETE FROM ORG_SECURITY_EIDS WHERE GROUP_ID = " + p_iGroupId, m_objDbConnection);
                //}
                //catch { }
                //Deb :MITS 29627
				//RunSql("DELETE FROM ORG_SECURITY_USER WHERE GROUP_ID = " + p_iGroupId,m_objDbConnection);
                RunSql("DELETE FROM ORG_SECURITY_USER WHERE GROUP_ID IN (" + sGroupIds + ")", m_objDbConnection);

				objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
				objDbConn.Open();
                RunSql("DELETE FROM USER_ORGSEC WHERE GROUP_ID IN (" + sGroupIds + ") AND DSN_ID = " + m_sDSNId, objDbConn);
				if(objDbConn != null)
					objDbConn.Close();

				if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
					//DropBESGroup_SQLServer(p_iGroupId);   //pmittal5
                    DropBESGroup_SQLServer(p_iGroupId,sGroupIds); 
				else if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
					//DropBESGroup_Oracle(p_iGroupId);      //pmittal5
                    DropBESGroup_Oracle(p_iGroupId,sGroupIds); 
				else if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
					DropBESGroup_DB2(p_iGroupId);
				
				return true;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("ORGSEC.DeleteGroupDef.Error",m_iClientId),p_objExp);   
			}
			finally
			{
				if(m_objDbConnection != null)
					m_objDbConnection.Dispose();

				if(objDbConn != null)
					objDbConn.Dispose();
                if (objReader != null)
                    objReader.Dispose();
 			}
		}

		/// Name		: DisableOrgSec
		/// Author		: Anurag Agarwal
		/// Date Created	: 29 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Disable the Entity Security setup
		/// </summary>
		/// <returns>True for success & False for failure</returns>
		public bool DisableOrgSec(XmlDocument p_objXMLDocument)
		{
			string sSQL = "";
			DbReader objDbReader = null;
			DbConnection objDbConn = null;
			string sGroupId = "";
			string sFileName = "";
			StreamReader objStreamReader = null;
			string sContent = "";
			int iPos = 0;
			string sTemp = "";

			try
			{
                /*commented by nadim for R6
                if (!LoginAdmin(p_objXMLDocument))
					return false;
                
				//drop triggers/stored procedures
				if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
				{
					DropTrigger("DROP TRIGGER EVENT_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER EVENT_UPD",m_objDbConnection);
					DropTrigger("DROP TRIGGER CLAIM_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER CLAIM_UPD",m_objDbConnection);
					DropTrigger("DROP TRIGGER PER_INV_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER CLAIMANT_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER FUNDS_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER RSV_HISTORY_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER RSV_CURRENT_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER ENTITY_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER EMPLOYEE_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER EMPLOYEE_UPD",m_objDbConnection);
					DropTrigger("DROP TRIGGER EVENT_BROKER_UPD",m_objDbConnection);
					DropTrigger("DROP TRIGGER CLAIM_BROKER_UPD",m_objDbConnection);
					DropTrigger("DROP TRIGGER POLICY_UPD",m_objDbConnection);
				}
				else if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR)
				{
					DropTrigger("DROP TRIGGER EVENT_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER EVENT_UPD",m_objDbConnection);
					DropTrigger("DROP TRIGGER CLAIM_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER CLAIM_UPD",m_objDbConnection);
					DropTrigger("DROP TRIGGER PERSON_INVOLVED_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER CLAIMANT_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER FUNDS_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER RESERVE_HISTORY_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER RESERVE_CURRENT_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER ENTITY_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER EMPLOYEE_INS",m_objDbConnection);
					DropTrigger("DROP TRIGGER EMPLOYEE_UPD",m_objDbConnection);
				}
				
				//drop user logins
				sSQL = "SELECT * FROM ENTITY_MAP_USER WHERE GROUP_ID > 0";
				objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						sGroupId = string.Format("{0:00000}",Conversion.ConvertStrToInteger(objDbReader["GROUP_ID"].ToString())); 
						if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
						{
							//Drop per-user views
							DropView("DROP VIEW sg" + sGroupId + ".CLAIM",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".CLAIMANT",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".EMPLOYEE",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".ENTITY",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".EVENT",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".FUNDS",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".PERSON_INVOLVED",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".RESERVE_CURRENT",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".RESERVE_HISTORY",m_objDbConnection);
				
                            sFileName = String.Format(@"{0}\{1}", m_BESSettings[BES_SYSTEMFILEPATH], m_BESSettings[BES_DEFFILENAME]);
							
							if (sFileName != "")
							{
								objStreamReader = System.IO.File.OpenText(sFileName);
								while(true)
								{
									if( (sContent = objStreamReader.ReadLine()) != null)
									{
										if(sContent != "")
										{
											iPos = sContent.IndexOf(" AS ");
											sTemp = sContent.Substring(11,iPos - 12);
											DropView("DROP VIEW sg" + sGroupId + "." + sTemp,m_objDbConnection);   
										}
									}
									else
										break;
								}
								objStreamReader.Close();
							}

							//Drop user accounts that may have been created previously
							DropUserAccount("sp_dropuser 'sg" + sGroupId + "'",m_sConnString);   
							DropUserAccount("sp_droplogin 'sg" + sGroupId + "'",m_sConnString);  
						}
						else if(m_stDataBaseType == eDatabaseType.DBMS_IS_DB2)
						{
							DropView("DROP VIEW sg" + sGroupId + ".CLAIM",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".CLAIMANT",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".EMPLOYEE",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".ENTITY",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".EVENT",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".FUNDS",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".PERSON_INVOLVED",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".RESERVE_CURRENT",m_objDbConnection);
							DropView("DROP VIEW sg" + sGroupId + ".RESERVE_HISTORY",m_objDbConnection);
						}
						else if(m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
						{
							DropUserAccount("drop user sg" + sGroupId + " CASCADE",objAdminDbConnection);   
						}
					}	
				}
				*/
                //check if full disable is selected
				if(((XmlElement)p_objXMLDocument.SelectSingleNode("//Document/DisableBESFlag")).InnerText == "1")
				{
					RunSql("DELETE FROM ENTITY_MAP_USER",m_objDbConnection);
					RunSql("DELETE FROM ENTITY_MAP",m_objDbConnection);
					RunSql("DELETE FROM ORG_SECURITY_USER",m_objDbConnection);
                    //pmittal5 03/09/10 - Confidential Record
                    //Deb :MITS 29627
                    //try
                    //{
                    //    RunSql("DELETE FROM ORG_SECURITY_EIDS", m_objDbConnection);
                    //}
                    //catch { }
                    //Deb :MITS 29627
                    //RunSql("UPDATE ORG_SECURITY  SET DELETED_FLAG = -1 WHERE GROUP_ENTITIES NOT LIKE '%<ALL>%'", m_objDbConnection);
                    RunSql("DELETE FROM ORG_SECURITY", m_objDbConnection);
                    //RunSql("DELETE FROM ORG_SECURITY WHERE GROUP_ENTITIES NOT LIKE '%<ALL>%'", m_objDbConnection);
                    //RunSql("UPDATE GROUP_MAP SET DELETED_FLAG = -1 WHERE SUPER_GROUP_ID NOT IN (SELECT GROUP_ID FROM ORG_SECURITY)",m_objDbConnection);
                    RunSql("UPDATE GROUP_MAP SET DELETED_FLAG = -1",m_objDbConnection);

                    //Delete entries from CONF_EVENT_PERM in case of Confidential Records
                    sSQL = "SELECT COUNT(*) FROM CONF_EVENT_PERM";
                    if (m_objDbConnection.ExecuteScalar(sSQL).ToString() != "0")
                    {
                        RunSql("DELETE FROM CONF_EVENT_PERM", m_objDbConnection);
                        RunSql("UPDATE EVENT SET CONF_FLAG = 0 , CONF_EVENT_ID = 0 WHERE CONF_FLAG = -1",m_objDbConnection);
                        RunSql("UPDATE CLAIM SET CONF_FLAG = 0 WHERE CONF_FLAG = -1", m_objDbConnection);
                        RunSql("UPDATE PERSON_INVOLVED SET CONF_FLAG = 0 WHERE CONF_FLAG = -1", m_objDbConnection);
                    }
                    //End - pmittal5
				}
					
				objDbConn = DbFactory.GetDbConnection(m_sSecurityDSN);
				objDbConn.Open();
				
				//set datasource flag to false
                //pmittal5 03/09/10 - Confidential Record
                //sSQL = "UPDATE DATA_SOURCE_TABLE SET ORGSEC_FLAG = 0 ,DEF_BES_FLAG = 0 WHERE DSNID = " + m_sDSNId;
                sSQL = "UPDATE DATA_SOURCE_TABLE SET ORGSEC_FLAG = 0 ,CONF_FLAG = 0, DEF_BES_FLAG = 0 WHERE DSNID = " + m_sDSNId;
				RunSql(sSQL,objDbConn);

                if (((XmlElement)p_objXMLDocument.SelectSingleNode("//Document/DisableBESFlag")).InnerText == "1")
                {
                    sSQL = "DELETE FROM USER_ORGSEC WHERE DSN_ID=" + m_sDSNId;
                    RunSql(sSQL, objDbConn);
                }

				if(objDbConn != null) 
					objDbConn.Close();
			
				return true;
			}
			catch(Exception p_objExp)
			{			
				throw new RMAppException(Globalization.GetString("ORGSEC.DisableOrgSec.Error",m_iClientId),p_objExp);   
			}
			finally
			{
				if(m_objDbConnection != null)
					m_objDbConnection.Dispose();
                if(objDbReader!=null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

				if(objDbConn != null)
					objDbConn.Dispose(); 
			}
		}

		/// Name		: PrintOrgSec
		/// Author		: Anurag Agarwal
		/// Date Created	: 29 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Prints the Entity Security setup
		/// </summary>
		/// <returns>True for success & False for failure</returns>
		public MemoryStream PrintOrgSec()
        {
            #region local variables
            PrintWrapper objPrintWrapper = null;
			LocalCache objLocalCache = null;
			MemoryStream objMemoryStream = null;
			string sTitle = "";
			string sSQL = "";
			DbReader objDbReader = null;
			DbReader objTempDbReader = null;
			bool bFirst = false;
			string sGroupName = "";
			int iPos = 0;
			string sTemp = "";
			string sAbbr="";
			string sLname="";
			string sCompanyName = "";
			string sPath="";
			double dBorderWidth = 0;
			double dMarginWidth = 0;
			double dPageWidth = 0;
			double dPageHeight = 0;
			double dClientWidth = 0;
			double dClientHeight = 0;
			double dTotalLines = 0;
			double dTextHeight = 0;
			string sDate = "";
			string sFooterText = "";
			double dBodyHeight = 0;
            #endregion

            try
			{
				objPrintWrapper = new PrintWrapper(m_iClientId);

				objPrintWrapper.StartDoc(false);

                objLocalCache = new LocalCache(m_sConnString, m_iClientId);
				
				sDate = DateTime.Now.ToShortDateString();
				
				sCompanyName = UTILITY.GetClientInfo(m_sConnString, m_iClientId);
				iPos = sCompanyName.IndexOf("\\r");
				if(iPos != -1)
					sCompanyName = sCompanyName.Substring(0,iPos);
				else
					sCompanyName = "";
				//PrintBorder
				dBorderWidth = 2;
				dMarginWidth = 75;
				dPageWidth = objPrintWrapper.PageWidth - 2 * (dBorderWidth + dMarginWidth);
				dPageHeight = objPrintWrapper.PageHeight - 2 * (dBorderWidth + dMarginWidth);
				dClientWidth = objPrintWrapper.PageWidth - 2 * dMarginWidth;
				dClientHeight = objPrintWrapper.PageHeight - 2 * (dMarginWidth + 50);
				//Moved from top. Referenced ClientHeight before it was set.
				dTotalLines = Math.Abs(dClientHeight / objPrintWrapper.GetTextHeight("W") - 10);  
				dTextHeight = objPrintWrapper.GetTextHeight(sCompanyName);
  
				objPrintWrapper.SetFont("Arial",10);
				objPrintWrapper.SetFontBold(true);
				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth,dMarginWidth);

				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth,dMarginWidth, dPageHeight - dMarginWidth);
				objPrintWrapper.PrintLine(dPageWidth - dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth, dPageHeight - dMarginWidth);
				objPrintWrapper.PrintLine(dMarginWidth,dPageHeight - dMarginWidth,dPageWidth - dMarginWidth,dPageHeight - dMarginWidth);
				
				//print header
				objPrintWrapper.PrintLine(dMarginWidth,dMarginWidth + 500,dPageWidth - dMarginWidth,dMarginWidth + 500);
				objPrintWrapper.FillRect(dMarginWidth,dMarginWidth,dPageWidth - dMarginWidth,dMarginWidth + 500);

				objPrintWrapper.CurrentX = dMarginWidth + objPrintWrapper.GetTextWidth(" ");
				objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;

				objPrintWrapper.CurrentX = (dPageWidth - objPrintWrapper.GetTextWidth(sCompanyName)) / 2;
				objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
				objPrintWrapper.PrintText(sCompanyName,true);

				objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sDate + "W");
				objPrintWrapper.CurrentY = (500 - objPrintWrapper.GetTextHeight(sCompanyName)) / 2 + dMarginWidth;
				objPrintWrapper.PrintText(sDate,true);
				
				//print footer
				objPrintWrapper.PrintLine(dMarginWidth,dPageHeight - dMarginWidth - 500,dPageWidth - dMarginWidth - 500,dPageHeight - dMarginWidth);
				objPrintWrapper.FillRect(dMarginWidth,dPageHeight - dMarginWidth - 500,dPageWidth - dMarginWidth,dPageHeight - dMarginWidth);
                sFooterText = String.Format("{0} - {1}  ", Globalization.GetString("ORGSEC.PrintOrgSec.ConfidentialData",m_iClientId), m_MessageSettings["DEF_RMCAPTION"]);
				objPrintWrapper.CurrentX = dClientWidth - objPrintWrapper.GetTextWidth(sFooterText) - 1;
				objPrintWrapper.CurrentY = (dPageHeight - 500) + objPrintWrapper.GetTextWidth("W");
				objPrintWrapper.SetFontItalic(true);  
				objPrintWrapper.PrintText(sFooterText,true);
				objPrintWrapper.SetFontItalic(false);
				sFooterText = Globalization.GetString("ORGSEC.PrintOrgSec.PageX",m_iClientId) + " " + objPrintWrapper.PageNumber;
				objPrintWrapper.CurrentX = (dPageWidth - objPrintWrapper.GetTextWidth(sFooterText)) / 2;
				objPrintWrapper.SetFontBold(true);  
				objPrintWrapper.PrintText(sFooterText,true);
				objPrintWrapper.SetFontBold(false);

				dBodyHeight = dClientHeight - 2 * 500;
				objPrintWrapper.PageLimit = dBodyHeight;

				//Print Body
				sTitle = "Entity Security Setup";

				objPrintWrapper.SetFont("Arial",7);
				objPrintWrapper.CurrentY = dMarginWidth + 500 + objPrintWrapper.GetTextHeight("W");
				objPrintWrapper.CurrentX = (objPrintWrapper.PageWidth - objPrintWrapper.GetTextWidth(sTitle))/2;
				

				objPrintWrapper.SetFontBold(true); 
				objPrintWrapper.PrintText(sTitle,false);
				objPrintWrapper.SetFontBold(false);
				
				objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");

				sSQL = "SELECT * FROM ORG_SECURITY";
				objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
						objPrintWrapper.CurrentX = 100;

						objPrintWrapper.SetFontBold(true); 
						objPrintWrapper.PrintText("Group Name :",true);
						objPrintWrapper.SetFontBold(false);
						objPrintWrapper.PrintText(objDbReader["GROUP_NAME"].ToString(),false);
						
						objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
						objPrintWrapper.CurrentX = 300;
						objPrintWrapper.SetFontBold(true); 
						objPrintWrapper.PrintText("Group Entities :",true);   	
						objPrintWrapper.SetFontBold(false);

						sGroupName = objDbReader["GROUP_ENTITIES"].ToString();
						bFirst = true;
						while(sGroupName != "")
						{
							iPos = sGroupName.IndexOf(",");
							if(iPos == -1)
								sTemp = sGroupName;
							else
								sTemp = sGroupName.Substring(0,iPos);
							objLocalCache.GetOrgInfo(Conversion.ConvertStrToInteger(sTemp),ref sAbbr,ref sLname);
							sTemp = sLname;
							if(sTemp != "")
							{
								if(objPrintWrapper.CurrentX + objPrintWrapper.GetTextWidth(sTemp) >= objPrintWrapper.PageWidth - 100)
								{
									objPrintWrapper.PrintText("",false);
									objPrintWrapper.CurrentX = 300;
									bFirst = true;
								}
								if(!bFirst)
								{
									objPrintWrapper.PrintText(", ",true);
								}
								objPrintWrapper.PrintText(sTemp,true);
								bFirst = false;
							}
							if(iPos == -1)
								sGroupName = "";
							else
								sGroupName = sGroupName.Substring(iPos + 1);  
						}
						objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
						objPrintWrapper.PrintText("",false);		

						objPrintWrapper.CurrentX = 300;
						objPrintWrapper.SetFontBold(true); 
						objPrintWrapper.PrintText("Group Users :",true);   	
						objPrintWrapper.SetFontBold(false);

						//sSQL = "SELECT * FROM ORG_SECURITY_USER WHERE GROUP_ID = " + objDbReader["GROUP_ID"].ToString();
                        sSQL = "SELECT ORG_SECURITY_USER.* FROM ORG_SECURITY_USER INNER JOIN GROUP_MAP ON "+
                                " ORG_SECURITY_USER.GROUP_ID = GROUP_MAP.GROUP_ID WHERE GROUP_MAP.SUPER_GROUP_ID = " + objDbReader["GROUP_ID"].ToString();
						objTempDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
						bFirst = true;
						if(objTempDbReader != null)
						{
							while(objTempDbReader.Read())
							{
								sTemp = UTILITY.GetUser(Conversion.ConvertStrToInteger(objTempDbReader["USER_ID"].ToString()),m_sDSNId,m_sSecurityDSN, m_iClientId);
								if(sTemp != "")
								{
									if(objPrintWrapper.CurrentX + objPrintWrapper.GetTextWidth(sTemp) >= objPrintWrapper.PageWidth - 100)
									{
										objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
										objPrintWrapper.PrintText("",false);
										objPrintWrapper.CurrentX = 300;
										bFirst = true;
									}
									if(!bFirst)
										objPrintWrapper.PrintText(", ",true);
									objPrintWrapper.PrintText(sTemp,true);
									bFirst = false;
								}
							}
							objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
							objPrintWrapper.PrintLine(dMarginWidth,objPrintWrapper.CurrentY,objPrintWrapper.PageWidth - 300,objPrintWrapper.CurrentY);  

							if(objTempDbReader != null)
								objTempDbReader.Close();
						}
					}
					if(objDbReader != null)
						objDbReader.Close();
				}

				objPrintWrapper.CurrentY += objPrintWrapper.GetTextHeight("W");
				objPrintWrapper.PrintText("",false);

				objPrintWrapper.EndDoc();

				objMemoryStream = new MemoryStream();

                sPath = RMConfigurator.TempPath;
				sPath=sPath+Path.GetFileName(Path.GetTempFileName()).Replace(".tmp",".pdf");;
				objPrintWrapper.GetPDFStream(sPath,ref objMemoryStream); 
				File.Delete(sPath);
				return objMemoryStream;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ORGSEC.PrintOrgSec.Error", m_iClientId), p_objExp);
			}
		}

		/// Name		: IsOrgSetFirstTime
		/// Author		: Anurag Agarwal
		/// Date Created	: 14 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Checks whether setup is running for first time or not
		/// </summary>
		/// <returns>True if setup is for first time else False</returns>
		public bool IsOrgSetFirstTime()
		{
			DbReader objDbReader = null;
			string sSQL = "";
			bool bIsFirst = false;

			try
			{
//				sSQL = "SELECT * FROM DATA_SOURCE_TABLE WHERE DSNID = " + m_sDSNId + " AND ORGSEC_FLAG <> 0 AND ORGSEC_FLAG IS NOT NULL";
                sSQL = "SELECT * FROM DATA_SOURCE_TABLE WHERE DSNID = " + m_sDSNId + " AND  DEF_BES_FLAG <> 0 AND  DEF_BES_FLAG IS NOT NULL";
               
				objDbReader = DbFactory.GetDbReader(m_sSecurityDSN,sSQL);
				if(objDbReader != null)
				{
					if(objDbReader.Read())
						bIsFirst = false;
					else
						bIsFirst = true;

                    if (objDbReader != null)
                        objDbReader.Close();
				}
	
				return bIsFirst;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ORGSEC.IsOrgSetFirstTime.Error", m_iClientId), p_objExp);   
			}
		}

        public bool IsConfRecActivate()
        {
            DbReader objDbReader = null;
            string sSQL = "";
            bool bIsConfRecActivate = false;
            try
            {
                sSQL = "SELECT CONF_FLAG FROM DATA_SOURCE_TABLE WHERE DSNID = " + m_sDSNId + " AND CONF_FLAG <> 0 AND CONF_FLAG IS NOT NULL";
                objDbReader = DbFactory.GetDbReader(m_sSecurityDSN, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                        bIsConfRecActivate = true;
                    else
                        bIsConfRecActivate = false;

                    objDbReader.Close();
                }

                return bIsConfRecActivate;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("ORGSEC.IsConfRecActivate.Error", m_iClientId), p_objExp);
            }
        }
		/// Name		: LoginAdmin
		/// Author		: Anurag Agarwal
		/// Date Created	: 14 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Checks the User for database access permissions. For SQL Server it 
		/// checks for dbo rola and for Oracle it checks for DBA role 
		/// </summary>
		/// <returns>True if User is dbo/DBA else otherwise</returns>
		private bool LoginAdmin(XmlDocument p_objXMLDocument)
		{
			DbReader objDbReader = null;
            string sSQL = string.Empty;
            string sOraBESRole = string.Empty;
            XmlDocument objXml = null;               //Umesh
            XmlNode objSessionDSN = null;               // Umesh
            XmlNode objViewEnhanceBES = null; //Umesh
            DbReader objRdr = null;                   //Umesh
            string strDbConnString = string.Empty;
            const string DB_OWNER_ROLE = "db_owner";
            const string SYSADMIN_ROLE = "sysadmin";

            try
            {//Parijat-----------MITS 12810 : Changes done by svaidya3 had been merged for this function
                if (m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
                {
                    if (string.IsNullOrEmpty(m_sAdminConnString))
                    {
                        m_sConnString = m_sConnString + "pooling=False;";
                        strDbConnString = m_sConnString;
                    }
                    else
                    {
                        m_sAdminConnString = m_sAdminConnString + "pooling=False;";
                        //nadim changed for Admin connection string
                        //strDbConnString = m_sConnString;
                        strDbConnString = m_sAdminConnString;
                        //nadim changed for Admin connection string
                    }
                    //Check if the user is a member of the db_owner role
                    if (UTILITY.IsSQLRoleMember(strDbConnString, DB_OWNER_ROLE))
                    {
                        m_bIsAdmin = true;
                        if (!string.IsNullOrEmpty(m_sAdminConnString))
                        {
                            //Parses out the User ID and Pwd from the database connection string
                            UTILITY.GetUIdPwdFromConString(m_sAdminConnString, ref m_sDBOUserId, ref m_sDBOPwd);
                            m_sConnString = m_sAdminConnString;
                            if (m_objDbConnection.State == ConnectionState.Open)
                            {
                                m_objDbConnection.Close();
                                m_objDbConnection.ConnectionString = m_sAdminConnString;
                                m_objDbConnection.Open();
                            }//if
                        }//if
                    }//if
                    else
                    {
                        m_bIsAdmin = false;
                    }//else
                }//if
                else if (m_stDataBaseType == eDatabaseType.DBMS_IS_ORACLE)
                {
                    if (bIsOrgSecEnabled && !bIsOrgSecAdmin)
                        throw new RMAppException(Globalization.GetString("ORGSEC.LoginAdmin.AccessError", m_iClientId));

                    sSQL = "SELECT USERNAME,GRANTED_ROLE FROM USER_ROLE_PRIVS WHERE GRANTED_ROLE = 'DBA'";
                    if (m_sAdminConnString != "")
                        objDbReader = DbFactory.GetDbReader(m_sAdminConnString, sSQL);
                    else
                        objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                        {
                            if (objDbReader["USERNAME"].ToString().Trim().Length > 0)
                            {
                                UTILITY.GetUIdPwdFromConString(m_sConnString, ref m_sDBOUserId, ref m_sDBOPwd);
                                if (m_sAdminConnString == "")
                                {
                                    m_sAdminConnString = m_sConnString;
                                }
                                m_bIsAdmin = true;
                                m_objAdminDbConnection = DbFactory.GetDbConnection(m_sAdminConnString);
                                m_objAdminDbConnection.Open();
                            }
                        }
                        else
                            m_bIsAdmin = false;
                        //throw new RMAppException(Globalization.GetString("ORGSEC.LoginAdmin.NoDBAPrivileges"));
                        if(objDbReader!= null)
                            objDbReader.Close(); // Umesh
                    }

                    objXml = new XmlDocument();
                    string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                    if (!string.IsNullOrEmpty(strContent))
                    {
                        objXml.LoadXml(strContent);
                        objViewEnhanceBES = objXml.SelectSingleNode("//SpecialSettings/EnhanceBES");
                        if (objViewEnhanceBES != null)
                        {
                            if (objViewEnhanceBES.InnerText == "-1")
                            {
                                if (p_objXMLDocument.SelectSingleNode("//OracleRole") != null)
                                {
                                    sOraBESRole = ((XmlElement)p_objXMLDocument.SelectSingleNode("//OracleRole")).InnerText.Trim(); ;
                                    if (sOraBESRole != "")
                                    {
                                        sSQL = "SELECT distinct(GRANTED_ROLE) FROM DBA_ROLE_PRIVS WHERE GRANTED_ROLE = '" + sOraBESRole.ToUpper() + "'";
                                        if (m_sAdminConnString != "")
                                            objDbReader = DbFactory.GetDbReader(m_sAdminConnString, sSQL);
                                        else
                                            objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                                        if (objDbReader != null)
                                        {
                                            if (!objDbReader.Read())
                                            {
                                                m_bIsAdmin = false;
                                                throw new RMAppException(Globalization.GetString("ORGSEC.LoginAdmin.NoOracleRole", m_iClientId));
                                            }
                                        }
                                    }
                                    else if (sOraBESRole == "" && m_bIsAdmin == true)
                                    {
                                        m_bIsAdmin = false;
                                        m_bShowOnlyRole = true;
                                    }
                                }
                            }
                        }
                    }
                }

                if (objDbReader != null)
                    objDbReader.Close();

                return m_bIsAdmin;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("ORGSEC.LoginAdmin.Error", m_iClientId) + " " + p_objExp.Message, p_objExp);
            }
            finally
            {
                if(objDbReader!=null)
                    objDbReader.Dispose();
                if (objRdr != null)
                    objRdr.Dispose();
            }
		}

		/// Name		: BulkUpdateSecDepts
		/// Author		: Anurag Agarwal
		/// Date Created	: 15 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Update Security fields of base tables with dept. codes
		/// </summary>
		private bool BulkUpdateSecDepts()
		{
			string sSQL = "";
			DbReader objDbReader = null;

			int iMinId = 0;
			int iMaxId = 0;
			int iLoopCount = 0;

			//Updating event records with department id's
			sSQL = "SELECT MIN(EVENT_ID),MAX(EVENT_ID) FROM EVENT";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				sSQL = "UPDATE EVENT SET SEC_DEPT_EID = DEPT_EID WHERE EVENT_ID BETWEEN " + iLoopCount  + " AND " + (iLoopCount + 4999);
				RunSql(sSQL, m_objDbConnection);  
			}

			//Updating person involved records with department id's
			sSQL = "SELECT MIN(PI_ROW_ID),MAX(PI_ROW_ID) FROM PERSON_INVOLVED";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
					sSQL = "UPDATE PERSON_INVOLVED SET PERSON_INVOLVED.SEC_DEPT_EID = EVENT.DEPT_EID FROM " + 
						"PERSON_INVOLVED,EVENT WHERE PI_ROW_ID BETWEEN " + iLoopCount + " AND " + 
						(iLoopCount + 4999) + " AND EVENT.EVENT_ID = PERSON_INVOLVED.EVENT_ID";
				else
					sSQL = "UPDATE PERSON_INVOLVED SET SEC_DEPT_EID = (SELECT DEPT_EID FROM EVENT WHERE EVENT.EVENT_ID " + 
						"= PERSON_INVOLVED.EVENT_ID) WHERE PI_ROW_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
					
				RunSql(sSQL, m_objDbConnection);  
			}
				
			//Updating entity records with department id's
			sSQL = "SELECT MIN(ENTITY_ID),MAX(ENTITY_ID) FROM ENTITY";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				sSQL = "UPDATE ENTITY SET SEC_DEPT_EID = 0 WHERE ENTITY_ID BETWEEN " + iLoopCount  + " AND " + (iLoopCount + 4999);
				RunSql(sSQL, m_objDbConnection);  
			}

			//Updating Org. Hierarchy records with department id's
			for(iLoopCount = 1005; iLoopCount <= 1012; iLoopCount++)
			{
				sSQL = "UPDATE ENTITY SET SEC_DEPT_EID = ENTITY_ID WHERE ENTITY_TABLE_ID = " + iLoopCount;
				RunSql(sSQL, m_objDbConnection);  
			}

			//Updating employee records with department id's
			sSQL = "SELECT MIN(EMPLOYEE_EID),MAX(EMPLOYEE_EID) FROM EMPLOYEE";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				sSQL = "UPDATE EMPLOYEE SET SEC_DEPT_EID = DEPT_ASSIGNED_EID WHERE EMPLOYEE_EID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
				RunSql(sSQL, m_objDbConnection);  
			}
			
			//Updating claim records with department id's
			sSQL = "SELECT MIN(CLAIM_ID),MAX(CLAIM_ID) FROM CLAIM";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
					sSQL = "UPDATE CLAIM SET CLAIM.SEC_DEPT_EID = EVENT.DEPT_EID FROM CLAIM,EVENT WHERE " + 
						"CLAIM_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999) + " AND EVENT.EVENT_ID = CLAIM.EVENT_ID";
				else
					sSQL = "UPDATE CLAIM SET SEC_DEPT_EID = (SELECT DEPT_EID FROM EVENT WHERE EVENT.EVENT_ID =" + 
						" CLAIM.EVENT_ID) WHERE CLAIM_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
					
				RunSql(sSQL, m_objDbConnection);  
			}
				
			//Updating claimant records with department id's
			sSQL = "SELECT MIN(CLAIMANT_ROW_ID),MAX(CLAIMANT_ROW_ID) FROM CLAIMANT";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
					sSQL = "UPDATE CLAIMANT SET CLAIMANT.SEC_DEPT_EID = CLAIM.SEC_DEPT_EID FROM CLAIMANT,CLAIM WHERE " + 
						"CLAIMANT_ROW_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999) + " AND CLAIMANT.CLAIM_ID = CLAIM.CLAIM_ID";
				else
					sSQL = "UPDATE CLAIMANT SET SEC_DEPT_EID = (SELECT SEC_DEPT_EID FROM CLAIM WHERE CLAIM.CLAIM_ID = " + 
						"CLAIMANT.CLAIM_ID) WHERE CLAIMANT_ROW_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
					
				RunSql(sSQL, m_objDbConnection);  
			}

			//Updating funds records with department id's
			sSQL = "SELECT MIN(TRANS_ID),MAX(TRANS_ID) FROM FUNDS";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				if(m_stDataBaseType == eDatabaseType.DBMS_IS_SQLSRVR || m_stDataBaseType == eDatabaseType.DBMS_IS_SYBASE)
					sSQL = "UPDATE FUNDS SET FUNDS.SEC_DEPT_EID = CLAIM.SEC_DEPT_EID FROM FUNDS,CLAIM WHERE FUNDS.CLAIM_ID " + 
						"= CLAIM.CLAIM_ID AND FUNDS.TRANS_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
				else
					sSQL = "UPDATE FUNDS SET SEC_DEPT_EID = (SELECT SEC_DEPT_EID FROM CLAIM WHERE CLAIM.CLAIM_ID = " + 
						"FUNDS.CLAIM_ID) WHERE TRANS_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
					
				RunSql(sSQL, m_objDbConnection);  
			}

			//Updating current reserve records with department id's
			sSQL = "SELECT MIN(RC_ROW_ID),MAX(RC_ROW_ID) FROM RESERVE_CURRENT";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				sSQL = "UPDATE RESERVE_CURRENT SET SEC_DEPT_EID = (SELECT SEC_DEPT_EID FROM CLAIM WHERE CLAIM.CLAIM_ID = RESERVE_CURRENT.CLAIM_ID) WHERE RC_ROW_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
				RunSql(sSQL, m_objDbConnection);  
			}

			//Updating historical reserve records with department id's
			sSQL = "SELECT MIN(RSV_ROW_ID),MAX(RSV_ROW_ID) FROM RESERVE_HISTORY";
			objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
			if(objDbReader != null)
			{
				if(objDbReader.Read())
				{
					if(objDbReader[0] != null)
						iMinId = Conversion.ConvertStrToInteger(objDbReader[0].ToString());

					if(objDbReader[1] != null)
						iMaxId = Conversion.ConvertStrToInteger(objDbReader[1].ToString());
				}
				if(objDbReader != null)
					objDbReader.Close();
			}
			for(iLoopCount = iMinId; iLoopCount <= iMaxId; iLoopCount = iLoopCount + 5000)
			{
				sSQL = "UPDATE RESERVE_HISTORY SET SEC_DEPT_EID = (SELECT SEC_DEPT_EID FROM CLAIM WHERE CLAIM.CLAIM_ID = " + 
					"RESERVE_HISTORY.CLAIM_ID) WHERE RSV_ROW_ID BETWEEN " + iLoopCount + " AND " + (iLoopCount + 4999);
				RunSql(sSQL, m_objDbConnection);  
			}

			return true;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_sScriptFilePath"></param>
        /// <param name="p_bIgnoreErrors"></param>
        /// <returns></returns>
		private bool ExcecuteScript(string p_sScriptFilePath, bool p_bIgnoreErrors)
		{
			string sContent;
			bool bIsProceed = false;
			string sSQL = "";

			System.IO.StreamReader objReader = System.IO.File.OpenText(p_sScriptFilePath);
			while(true)
			{
				if( (sContent = objReader.ReadLine()) != null)
				{
					if(sContent.Trim() != "")
					{
						bIsProceed = true;

						if(sContent.ToUpper().Trim() != "GO" && sContent.ToUpper().Trim() != "/")
						{
							bIsProceed = false;
								
							sSQL += " " + sContent; 
						}

						if(bIsProceed)
						{
							if(!p_bIgnoreErrors)
								RunSql(sSQL, m_objDbConnection);
							else
							{
								try
								{
									RunSql(sSQL, m_objDbConnection);

								}
								catch(Exception)
								{
								}
							}
							sSQL = "";
						}
					}
				}
				else
					break;
			}
				
			objReader.Close();
			return true;
		}

		/// Name		: DropBESGroup_SQLServer
		/// Author		: Anurag Agarwal
		/// Date Created	: 19 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes the Group defination information for SQL SERVER & SYBASE
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group whose info to be deleted</param>
		//private void DropBESGroup_SQLServer(int p_iGroupId)
        private void DropBESGroup_SQLServer(int p_iGroupId, StringBuilder p_sGroupIds)
		{
			string sDefFilePath = "";
			System.IO.StreamReader objStreamReader = null;
			string sContent = "";
			int iPos = 0;
			string sTemp = "";
			string sGroupId = "";

			sGroupId = string.Format("{0:00000}",p_iGroupId);
			
			//Clean out any entity map or user to group mapping that may exist
			RunSql("DELETE FROM ENTITY_MAP WHERE GROUP_ID = " + sGroupId,m_objDbConnection);
            RunSql("DELETE FROM ENTITY_MAP_USER WHERE GROUP_ID IN (" + p_sGroupIds + ")", m_objDbConnection);

			//Drop per-user views
			/*DropView("DROP VIEW sg" + sGroupId + ".CLAIM",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".CLAIMANT",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".EMPLOYEE",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".ENTITY",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".EVENT",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".FUNDS",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".PERSON_INVOLVED",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".RESERVE_CURRENT",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".RESERVE_HISTORY",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".FINANCIAL_HIST",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".ADJUST_DATED_TEXT",m_objDbConnection); //Support for adjuster text type screening in BES
            DropView("DROP VIEW sg" + sGroupId + ".POLICY", m_objDbConnection);  //MITS 9466
            //nadim for 15620-implement ENH POLICY in BES
            DropView("DROP VIEW sg" + sGroupId + ".POLICY_ENH", m_objDbConnection);*/
            //nadim for 15620-implement ENH POLICY in BES

           /* sDefFilePath = String.Format(@"{0}\{1}", m_BESSettings[BES_SYSTEMFILEPATH], m_BESSettings[BES_DEFFILENAME]);

            if (!String.IsNullOrEmpty(sDefFilePath))
			{
				objStreamReader = System.IO.File.OpenText(sDefFilePath);
				while(true)
				{
					if( (sContent = objStreamReader.ReadLine()) != null)
					{
						if(sContent != "")
						{
							iPos = sContent.IndexOf(" AS ");
							sTemp = sContent.Substring(12,iPos - 12);
							DropView("DROP VIEW sg" + sGroupId + "." + sTemp,m_objDbConnection);   
						}
					}
					else
						break;
				}
				objStreamReader.Close();
			}

			//Drop user accounts that may have been created previously
			DropUserAccount("sp_dropuser 'sg" + sGroupId + "'",m_sConnString);   
			DropUserAccount("sp_droplogin 'sg" + sGroupId + "'",m_sConnString);   */

		}

		/// Name		: DropBESGroup_Oracle
		/// Author		: Anurag Agarwal
		/// Date Created	: 20 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes the Group defination information for ORACLE
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group whose info to be deleted</param>
        // private void DropBESGroup_Oracle(int p_iGroupId)
        private void DropBESGroup_Oracle(int p_iGroupId, StringBuilder p_sGroupIds)
        {
            string sGroupId = "";

            sGroupId = string.Format("{0:00000}", p_iGroupId);
            if (objAdminDbConnection.ConnectionString != m_objDbConnection.ConnectionString)
            {
                m_objAdminDbCommand = objAdminDbConnection.CreateCommand();
                if (m_sCheckDefer == "1")
                {
                    m_objAdminDbTrans = objAdminDbConnection.BeginTransaction();
                    m_objAdminDbCommand.Transaction = m_objAdminDbTrans;
                }
            }
            else
                m_objAdminDbCommand = m_objDbCommand;


            //Clean out any entity map or user to group mapping that may exist
            //RunSql("DELETE FROM ENTITY_MAP WHERE GROUP_ID = " + sGroupId,m_objDbConnection);
            //RunSql("DELETE FROM ENTITY_MAP_USER WHERE GROUP_ID = " + sGroupId,m_objDbConnection);
            if (m_sCheckDefer != "1")
            {
                m_objDbCommand.CommandText = "DELETE FROM ENTITY_MAP WHERE GROUP_ID = " + sGroupId;
                m_objDbCommand.ExecuteNonQuery();
            }
            //m_objDbCommand.CommandText = "DELETE FROM ENTITY_MAP_USER WHERE GROUP_ID = " + sGroupId;  //pmittal5
            m_objDbCommand.CommandText = "DELETE FROM ENTITY_MAP_USER WHERE GROUP_ID IN (" + p_sGroupIds + ")";
              m_objDbCommand.ExecuteNonQuery();

          /*  if (m_sCheckDefer != "1")
            {
                //Drop user accounts that may have been created previously
                //Drop per-user views
                DropOraView("DROP VIEW sg" + sGroupId + ".CLAIM", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".CLAIMANT", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".EMPLOYEE", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".ENTITY", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".EVENT", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".FUNDS", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".PERSON_INVOLVED", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".RESERVE_CURRENT", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".RESERVE_HISTORY", m_objAdminDbCommand);
                DropOraView("DROP VIEW sg" + sGroupId + ".FINANCIAL_HIST", m_objAdminDbCommand);
                //may not be there if broker security not enabled
                DropOraView("DROP VIEW sg" + sGroupId + ".POLICY", m_objAdminDbCommand);
                //on older versions, the synonym may still be there and needs to be dropped so a view can replace it
                DropOraView("DROP SYNONYM sg" + sGroupId + ".POLICY", m_objAdminDbCommand);
                //nadim for 15620-implement ENH POLICY in BES
                DropOraView("DROP VIEW sg" + sGroupId + ".POLICY_ENH", m_objAdminDbCommand);
                DropOraView("DROP SYNONYM sg" + sGroupId + ".POLICY_ENH", m_objAdminDbCommand);
                //nadim for 15620-implement ENH POLICY in BES
                //Drop synonym to make way for view if selected
                DropOraView("DROP SYNONYM sg" + sGroupId + ".ADJUST_DATED_TEXT", m_objAdminDbCommand);
                //Won't exist unless adj text type screening is in use
                DropOraView("DROP VIEW sg" + sGroupId + ".ADJUST_DATED_TEXT", m_objAdminDbCommand); //Support for adjuster text type screening in BES

                //Drop user accounts that may have been created previously
                DropOraView("drop user sg" + sGroupId + " CASCADE", m_objAdminDbCommand);
            }*/
        }

		/// Name		: DropBESGroup_DB2
		/// Author		: Anurag Agarwal
		/// Date Created	: 20 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes the Group defination information for DB2
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group whose info to be deleted</param>
		private void DropBESGroup_DB2(int p_iGroupId)
		{
			string sGroupId = "";

			sGroupId = string.Format("{0:00000}",p_iGroupId);

			//Clean out any entity map or user to group mapping that may exist
			RunSql("DELETE FROM ENTITY_MAP WHERE GROUP_ID = " + sGroupId,m_objDbConnection);
			RunSql("DELETE FROM ENTITY_MAP_USER WHERE GROUP_ID = " + sGroupId,m_objDbConnection);

			//Drop user accounts that may have been created previously
			//Drop per-user views
			DropView("DROP VIEW sg" + sGroupId + ".CLAIM",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".CLAIMANT",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".EMPLOYEE",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".ENTITY",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".EVENT",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".FUNDS",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".PERSON_INVOLVED",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".RESERVE_CURRENT",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".RESERVE_HISTORY",m_objDbConnection);
			DropView("DROP VIEW sg" + sGroupId + ".FINANCIAL_HIST",m_objDbConnection);
			//may not be there if broker security not enabled
			DropView("DROP VIEW sg" + sGroupId + ".POLICY",m_objDbConnection);
			//on older versions, the synonym may still be there and needs to be dropped so a view can replace it
			DropView("DROP ALIAS sg" + sGroupId + ".POLICY",m_objDbConnection);
			//Drop synonym to make way for view if selected
			DropView("DROP ALIAS sg" + sGroupId + ".ADJUST_DATED_TEXT",m_objDbConnection);
			//Won't exist unless adj text type screening is in use
			DropView("DROP VIEW sg" + sGroupId + ".ADJUST_DATED_TEXT",m_objDbConnection); //Support for adjuster text type screening in BES		
		}

		/// Name		: BuildEntityMap_SQLSybase
		/// Author		: Anurag Agarwal
		/// Date Created	: 20 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// BuildEntityMap for SQL Server or Sybase
		/// </summary>
		/// <param name="p_iGroupId">The group Id of the group whose info to be deleted</param>
		/// <param name="p_sAdjTextTypes"></param>
        private void BuildEntityMap_SQLSybase(int p_iGroupId, string p_sAdjTextTypes, string sBrokers, List<int> p_lstGroupIds, bool p_bSetUpdatedFlag, bool p_bEntitiessChanged,string p_sStatus)
        {
            #region local variables
            string sTables = "";
			string sSQL = "";
			string sEntry = "";
			string sPrevEntry = "";
			string sGroupId = "";
			string sTableName = "";
            string sEncriptUser = "";
			DbReader objDbReader = null;

			int iPrev = 0;
			int iCurr = 0;
			int iBegin = 0;
			int iEnd = 0;
			int iCount = 0;
			string sWhere = "";
            string sWherePol = "";
			string sTemp = "";
            //nadim for 15620-implement ENH POLICY in BES
            string sWhereEnhPol = string.Empty;
            //nadim for 15620-implement ENH POLICY in BES
			string sDefFilePath = "";
			System.IO.StreamReader objStreamReader = null;
			string sContent = "";
			int iPos = 0;
			int iPos2 = 0;
			string sTemp2 = "";
            StringBuilder sGroupIds = new StringBuilder(); //pmittal5
            #endregion

            DbCommand objCommand = m_objDbConnection.CreateCommand();
			objCommand.CommandType = CommandType.Text;    

            sTables = VIEW_TABLES + "POLICY|"; //MITS 9466
            //nadim for 15620-implement ENH POLICY in BES
            sTables = VIEW_TABLES + "POLICY_ENH|";
            //nadim for 15620-implement ENH POLICY in BES
 
			if(p_sAdjTextTypes != "")
				sTables = sTables + "ADJUST_DATED_TEXT|";

			//Clean out previous group definition
	/*commented by Nadim for R6*/
            foreach (int iGroupId in p_lstGroupIds)
            {
                if (sGroupIds.Length > 0)
                    sGroupIds.Append(",");
                sGroupIds.Append(iGroupId.ToString());
            }
            if ((sBESSynSetting.ToUpper() != "FALSE" || p_sStatus == "NEW") && p_bEntitiessChanged)//Deb MITS 29627
                DropBESGroup_SQLServer(p_iGroupId, sGroupIds);
            //commented by Nadim for R6*/
            if ((sBESSynSetting.ToUpper() != "FALSE" || p_sStatus == "NEW") && p_bEntitiessChanged)//Deb MITS 29627
            {
                //Create Entity Access Map
                RunSql("INSERT INTO ENTITY_MAP(ENTITY_ID,GROUP_ID) VALUES (0," + p_iGroupId + ")", m_objDbConnection);

                if (!m_bIsAllOrgEntities)
                {
                    for (int i = 0; i < arDeptList.Count; i++)
                    {
                        if (Conversion.ConvertStrToInteger(arDeptList[i].ToString()) != 0)
                        {
                            sSQL = "INSERT INTO ENTITY_MAP (ENTITY_ID,GROUP_ID) VALUES (" + arDeptList[i] + "," + p_iGroupId + ")";
                            RunSql(sSQL, m_objDbConnection);
                        }
                    }
                    for (int i = 0; i < arParentList.Count; i++)
                    {
                        sEntry = Conversion.ConvertObjToStr(arParentList[i]);
                        if (sEntry != "0,0" && sEntry != sPrevEntry)
                        {
                            sSQL = "INSERT INTO ENTITY_MAP (ENTITY_ID,GROUP_ID) VALUES (" +
                                sEntry.Substring(0, sEntry.IndexOf(",")) + "," + p_iGroupId + ")";
                            RunSql(sSQL, m_objDbConnection);
                        }
                        sPrevEntry = sEntry;
                    }
                }

                //Setup user account for group
                /*//commented by Nadim for R6
                sGroupId = string.Format("{0:00000}",p_iGroupId);

                if(!m_bIsAllOrgEntities)
                {//Parijat---- MITS 12810 : Changes done by svaidya3 had been merged
                    //Add Login based on the specific SQL Server platform (i.e. SQL 2000 OR SQL 2005/2008)
                    sSQL = UTILITY.AddSQLLogin(m_objDbConnection.ConnectionString, "sg" + sGroupId, BES_COMPLEX_PASSWORD + sGroupId, m_sDataBaseName);				
                    AddUserAccount(sSQL,m_objDbConnection);

                    //Add user based on the specific SQL Server platform (i.e. SQL 2000 OR SQL 2005/2008)
                    sSQL = UTILITY.AddSQLUser(m_objDbConnection.ConnectionString, "sg" + sGroupId);
                    AddUserAccount(sSQL,m_objDbConnection);

                    sSQL = "GRANT CREATE TABLE, CREATE VIEW TO sg" + sGroupId;
                    AddUserAccount(sSQL,m_objDbConnection);
                    //Changed by  Nadim(4/15/2009) - Start
                    //Add Schema based on the specific SQL Server platform (i.e. SQL 2000 OR SQL 2005/2008)
                    sSQL = UTILITY.AddSQLSchema(m_objDbConnection.ConnectionString, "sg" + sGroupId);
                    if (sSQL != "")
                    {
                        AddUserAccount(sSQL, m_objDbConnection);

                        // If the DB Version is 2000, the SQL string would be blank, in that case the next function call is not necessary

                        sSQL = UTILITY.AddSQLSchema2User(m_objDbConnection.ConnectionString, "sg" + sGroupId);
                        AddUserAccount(sSQL, m_objDbConnection);
                    }
                    //Changed by  Nadim(4/15/2009)-End
                }
                //commented by Nadim for R6*/
                foreach (int iUserGroupId in p_lstGroupIds)
                {
                    if (m_bIsAllOrgEntities)
                    {
                        //sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + m_sDBOUserId + "', " + p_iGroupId + ",0)";
                        sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + m_sDBOUserId + "', " + iUserGroupId + ",0)";
                    }
                    else
                    {
                        //sEncriptUser = FormatBESUserName(Conversion.ConvertStrToInteger(m_sDSNId), m_sConnString, p_iGroupId);
                        //sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + sEncriptUser + "', " + p_iGroupId + ",0)";
                        sEncriptUser = FormatBESUserName(Conversion.ConvertStrToInteger(m_sDSNId), m_sConnString, iUserGroupId);
                        sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + sEncriptUser + "', " + iUserGroupId + ",0)";
                    }

                    //Try catch to catch Unique index errors in Entity_Map_User. When we select All Org then 
                    //the userid is same so when try to insert exception is thrown by database.
                    try
                    {
                        RunSql(sSQL, m_objDbConnection);
                    }
                    catch (Exception)
                    {
                        throw; //throw the exception in its current state
                    }
                    if (!m_bIsAllOrgEntities)
                    {
                        if ((p_bSetUpdatedFlag || m_sOverideFlag.Trim() == "1") && (sBESSynSetting.ToUpper() == "FALSE" || p_sStatus == "NEW")) //Deb :MITS 29215
                        {
                            sSQL = "UPDATE GROUP_MAP SET UPDATED_FLAG=-1,STATUS_FLAG=0 WHERE DELETED_FLAG=0 AND SUPER_GROUP_ID=" + p_iGroupId + " AND GROUP_ID=" + iUserGroupId;
                            RunSql(sSQL, m_objDbConnection);
                        }
                    }
                    //pmittal5 Mits 21705 - ENTITY_MAP_USER has Unique constraint on User Name, so while adding multiple users for Admin group with same User 'sa', it throws an error.
                    else
                    {
                        break;
                    }
                }
            }
            //End - pmittal5
				
			//Grant permissions on tables to new account
            /*//commented by Nadim for R6
            if(!m_bIsAllOrgEntities)
			{
				sSQL = "sp_tables NULL, NULL, NULL, N'''TABLE'''";
				objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						sTableName = objDbReader["TABLE_NAME"].ToString().ToUpper();
						//Line of code writen by JP, "TABLE_SCHEM" col was not found so used "TABLE_OWNER"
						//Limit the permission set to only select since no other permissions should be granted
                        //to BES users anyway
                        //Changed by nadim(4/15/2009)-Start
                        //sSQL = "GRANT SELECT ON " + objDbReader["TABLE_OWNER"].ToString() + "." + sTableName + " TO sg" + sGroupId;
                        sSQL = "GRANT SELECT,UPDATE,INSERT,DELETE ON " + objDbReader["TABLE_OWNER"].ToString() + "." + sTableName + " TO sg" + sGroupId;
                        //Changed by nadim(4/15/2009)-End
						RunSql(sSQL, m_objDbConnection);
					}
				}//if
					
				//Create per-user views
				//TODO: If Not xRMSysParmsInfo.bEnhancedBES Then
                //Parijat: Boeing - Broker Support
                if (!UTILITY.IsEnhancedBES(m_sConnString))
                {
				sSQL = "SELECT * FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId  + " ORDER BY ENTITY_ID";
				objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);

				iPrev = 0;
				iCurr = 0;
				iBegin = -1;
				iEnd = 0;
				iCount = 0;
                sWherePol = "";//MITS 9466
                sWhereEnhPol = string.Empty;////nadim for 15620-implement ENH POLICY in BES
				sTemp = "";
				sWhere = "CASE ";
                sWherePol = "CASE ";//MITS 9466
                sWhereEnhPol = "CASE ";////nadim for 15620-implement ENH POLICY in BES

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						iCurr = Conversion.ConvertStrToInteger(objDbReader["ENTITY_ID"].ToString());
						if(iBegin == -1)
							iBegin = iCurr;
						else if(iCurr > iPrev + 1)
						{
							iEnd = iPrev;

							if(iBegin == iEnd)
							{
								if(sTemp != "")
									sTemp += ",";
								sTemp += iBegin.ToString();
								iCount++;
								if(iCount == 20)
								{
									sWhere += " WHEN SEC_DEPT_EID IN (" + sTemp + ") THEN 1";
                                    sWherePol += " WHEN PXI.INSURED_EID IN (" + sTemp + ") THEN 1";   //MITS 9466
                                    //nadim for 15620-implement ENH POLICY in BES
                                    sWhereEnhPol += " WHEN PXII.INSURED_EID IN (" + sTemp + ") THEN 1";
                                    //nadim for 15620-implement ENH POLICY in BES
									sTemp = "";
									iCount = 0;
								}
							}
							else
                            {
								sWhere += " WHEN SEC_DEPT_EID BETWEEN " + iBegin + " AND " + iEnd + " THEN 1";
                                sWherePol += " WHEN PXI.INSURED_EID BETWEEN " + iBegin + " AND " + iEnd + " THEN 1";  //MITS 9466
                                //nadim for 15620-implement ENH POLICY in BES
                                sWhereEnhPol += " WHEN PXII.INSURED_EID BETWEEN " + iBegin + " AND " + iEnd + " THEN 1";
                                //nadim for 15620-implement ENH POLICY in BES
                            }
							iBegin = iCurr; 
						}

						iPrev = iCurr; 
					}
					if(sTemp != "")
                    {
						sWhere += " WHEN SEC_DEPT_EID IN (" + sTemp + ") THEN 1";
                        sWherePol += " WHEN PXI.INSURED_EID IN (" + sTemp + ") THEN 1";  //MITS 9466
                        //nadim for 15620-implement ENH POLICY in BES
                        sWhereEnhPol += " WHEN PXII.INSURED_EID IN (" + sTemp + ") THEN 1";
                        //nadim for 15620-implement ENH POLICY in BES
                    }
					if(iBegin == iCurr)
                    {
                        sWhere += " WHEN SEC_DEPT_EID = " + iBegin + " THEN 1";
                        sWherePol += " WHEN PXI.INSURED_EID = " + iBegin + " THEN 1";  //MITS 9466
                        //nadim for 15620-implement ENH POLICY in BES
                        sWhereEnhPol += " WHEN PXII.INSURED_EID = " + iBegin + " THEN 1";
                        //nadim for 15620-implement ENH POLICY in BES
                    }
					else
                    {
                        sWhere += " WHEN SEC_DEPT_EID BETWEEN " + iBegin + " AND " + iCurr + " THEN 1";
                        sWherePol += " WHEN PXI.INSURED_EID BETWEEN " + iBegin + " AND " + iCurr + " THEN 1";  //MITS 9466
                        //nadim for 15620-implement ENH POLICY in BES
                        sWhereEnhPol += " WHEN PXII.INSURED_EID BETWEEN " + iBegin + " AND " + iCurr + " THEN 1";
                        //nadim for 15620-implement ENH POLICY in BES
                    }

					sWhere += " ELSE 0 END = 1";
                    sWherePol += " ELSE 0 END = 1";  //MITS 9466
                    //nadim for 15620-implement ENH POLICY in BES
                    sWhereEnhPol += " ELSE 0 END = 1";
                    //nadim for 15620-implement ENH POLICY in BES
				}//if(objDbReader != null)

				//Create views for group

				//impersonate the BES group user based on the specific SQL Server platform
                string strSQL = UTILITY.ExecuteAsSQLUser(m_objDbConnection.ConnectionString, "sg" + sGroupId);
				AddUserAccount(strSQL,m_objDbConnection);

				CreateView("CREATE VIEW CLAIM AS SELECT * FROM dbo.CLAIM WHERE " + sWhere,m_objDbConnection);
				CreateView("CREATE VIEW CLAIMANT AS SELECT * FROM dbo.CLAIMANT WHERE " + sWhere,m_objDbConnection);
				CreateView("CREATE VIEW EMPLOYEE AS SELECT * FROM dbo.EMPLOYEE WHERE " + sWhere,m_objDbConnection);
                   //Parijat: Boeing - Broker Support
                    if (g_bBoeingBrokerSupport)
                        CreateView("CREATE VIEW ENTITY AS SELECT * FROM dbo.ENTITY WHERE " + sWhere +" AND ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP" + ((sBrokers.Trim() == "")? "": " WHERE BROKER_FRM_EID IN (" + sBrokers + ",0) OR BROKER_FRM_EID IS NULL") + ") OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP))",m_objDbConnection);
                    else
                        CreateView("CREATE VIEW ENTITY AS SELECT * FROM dbo.ENTITY WHERE " + sWhere,m_objDbConnection);
				CreateView("CREATE VIEW EVENT AS SELECT * FROM dbo.EVENT WHERE " + sWhere,m_objDbConnection);
				CreateView("CREATE VIEW FUNDS AS SELECT * FROM dbo.FUNDS WHERE " + sWhere,m_objDbConnection);
				CreateView("CREATE VIEW PERSON_INVOLVED AS SELECT * FROM dbo.PERSON_INVOLVED WHERE " + sWhere,m_objDbConnection);
				CreateView("CREATE VIEW RESERVE_CURRENT AS SELECT * FROM dbo.RESERVE_CURRENT WHERE " + sWhere,m_objDbConnection);
				CreateView("CREATE VIEW RESERVE_HISTORY AS SELECT * FROM dbo.RESERVE_HISTORY WHERE " + sWhere,m_objDbConnection);
                //nadim for 15620-implement ENH POLICY in BES
                if (g_bBoeingBrokerSupport)
                {
                    CreateView("CREATE VIEW POLICY AS SELECT * FROM dbo.POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP WHERE PRGRAM_TYPE_EID IN (SELECT ENTITY_ID FROM dbo.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP" + ((sBrokers.Trim() == "") ? "" : " WHERE BROKER_FRM_EID IN (" + sBrokers + ",0) OR BROKER_FRM_EID IS NULL") + ") OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP)) OR PRGRAM_TYPE_EID = 0 OR PRGRAM_TYPE_EID IS NULL) OR POLICY_ID NOT IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP)", m_objDbConnection);
                    CreateView("CREATE VIEW POLICY_ENH AS SELECT * FROM dbo.POLICY_ENH WHERE POLICY_ID IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP WHERE PRGRAM_TYPE_EID IN (SELECT ENTITY_ID FROM dbo.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP" + ((sBrokers.Trim() == "") ? "" : " WHERE BROKER_FRM_EID IN (" + sBrokers + ",0) OR BROKER_FRM_EID IS NULL") + ") OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP)) OR PRGRAM_TYPE_EID = 0 OR PRGRAM_TYPE_EID IS NULL) OR POLICY_ID NOT IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP)", m_objDbConnection);
                }
                else
                {
                    CreateView("CREATE VIEW POLICY AS SELECT * FROM dbo.POLICY WHERE POLICY_ID IN (SELECT PXI.POLICY_ID FROM dbo.POLICY_X_INSURED PXI WHERE " + sWherePol + ")", m_objDbConnection);	//MITS 9466
                    //CreateView("CREATE VIEW POLICY_ENH AS SELECT * FROM dbo.POLICY_ENH WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM dbo.POLICY_X_INSURED PXII WHERE " + sWhereEnhPol + ")", m_objDbConnection);
                    CreateView("CREATE VIEW POLICY_ENH AS SELECT * FROM dbo.POLICY_ENH WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM dbo.POLICY_X_INSRD_ENH PXII WHERE " + sWhereEnhPol + ")", m_objDbConnection);
                }            
                   
                  //nadim for 15620-implement ENH POLICY in BES
               }//if
                //Enhanced BES is turned on
               else
               {
                    //impersonate the BES group user based on the specific SQL Server platform
                   //nadim-impersonate the BES group user based on the specific SQL Server platform-start
                   string strSQL = UTILITY.ExecuteAsSQLUser(m_objDbConnection.ConnectionString, "sg" + sGroupId);
                   AddUserAccount(strSQL, m_objDbConnection);
                   //AddUserAccount("SETUSER 'sg" + sGroupId + "'", m_objDbConnection);
                   //nadim-impersonate the BES group user based on the specific SQL Server platform-End

                    //Create views using subqueries for Enhanced BES rather than standard concatenated views
                    CreateView("CREATE VIEW CLAIM AS SELECT * FROM dbo.CLAIM WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    CreateView("CREATE VIEW CLAIMANT AS SELECT * FROM dbo.CLAIMANT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    CreateView("CREATE VIEW EMPLOYEE AS SELECT * FROM dbo.EMPLOYEE WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    //Parijat: Boeing - Broker Support
                    if(g_bBoeingBrokerSupport)
                        CreateView("CREATE VIEW ENTITY AS SELECT * FROM dbo.ENTITY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ") " + " AND ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP" + ((sBrokers.Trim() == "")? "": " WHERE BROKER_FRM_EID IN (" + sBrokers + ",0) OR BROKER_FRM_EID IS NULL") + ") OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP))", m_objDbConnection);
                    else
                         CreateView("CREATE VIEW ENTITY AS SELECT * FROM dbo.ENTITY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    CreateView("CREATE VIEW EVENT AS SELECT * FROM dbo.EVENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    CreateView("CREATE VIEW FUNDS AS SELECT * FROM dbo.FUNDS WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    CreateView("CREATE VIEW PERSON_INVOLVED AS SELECT * FROM dbo.PERSON_INVOLVED WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    CreateView("CREATE VIEW RESERVE_CURRENT AS SELECT * FROM dbo.RESERVE_CURRENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    CreateView("CREATE VIEW RESERVE_HISTORY AS SELECT * FROM dbo.RESERVE_HISTORY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")", m_objDbConnection);
                    //Parijat :for Boeing changed requirement. Now we add view only for Policy and that too based on Broker Firm instead of Broker.
                    //nadim for 15620-implement ENH POLICY in BES
                    if (g_bBoeingBrokerSupport)
                    {
                        CreateView("CREATE VIEW POLICY AS SELECT * FROM dbo.POLICY WHERE POLICY_ID IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP WHERE PRGRAM_TYPE_EID IN (SELECT ENTITY_ID FROM dbo.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP" + ((sBrokers.Trim() == "") ? "" : " WHERE BROKER_FRM_EID IN (" + sBrokers + ",0) OR BROKER_FRM_EID IS NULL") + ") OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP)) OR PRGRAM_TYPE_EID = 0 OR PRGRAM_TYPE_EID IS NULL) OR POLICY_ID NOT IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP)", m_objDbConnection);
                        CreateView("CREATE VIEW POLICY_ENH AS SELECT * FROM dbo.POLICY_ENH WHERE POLICY_ID IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP WHERE PRGRAM_TYPE_EID IN (SELECT ENTITY_ID FROM dbo.ENTITY WHERE ENTITY_ID IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP" + ((sBrokers.Trim() == "") ? "" : " WHERE BROKER_FRM_EID IN (" + sBrokers + ",0) OR BROKER_FRM_EID IS NULL") + ") OR ENTITY_ID NOT IN (SELECT ENTITY_ID FROM dbo.ENTITY_SUPP)) OR PRGRAM_TYPE_EID = 0 OR PRGRAM_TYPE_EID IS NULL) OR POLICY_ID NOT IN (SELECT POLICY_ID FROM dbo.POLICY_SUPP)", m_objDbConnection);

                    }
                    else
                    {
                        CreateView("CREATE VIEW POLICY AS SELECT * FROM dbo.POLICY WHERE POLICY_ID IN (SELECT PXI.POLICY_ID FROM dbo.POLICY_X_INSURED PXI,dbo.ENTITY_MAP EM WHERE PXI.INSURED_EID = EM.ENTITY_ID AND EM.GROUP_ID =" + p_iGroupId + ")", m_objDbConnection);
                        //CreateView("CREATE VIEW POLICY_ENH AS SELECT * FROM dbo.POLICY_ENH WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM dbo.POLICY_X_INSURED PXII,dbo.ENTITY_MAP EMI WHERE PXII.INSURED_EID = EMI.ENTITY_ID AND EMI.GROUP_ID =" + p_iGroupId + ")", m_objDbConnection);
                        CreateView("CREATE VIEW POLICY_ENH AS SELECT * FROM dbo.POLICY_ENH WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM dbo.POLICY_X_INSRD_ENH PXII,dbo.ENTITY_MAP EMI WHERE PXII.INSURED_EID = EMI.ENTITY_ID AND EMI.GROUP_ID =" + p_iGroupId + ")", m_objDbConnection);
                    }                  
                    
                   //nadim for 15620-implement ENH POLICY in BES
                }//else
//				//************As discussed with JP this has to be removed
//				//If Not xRMSysParmsInfo.bEnhancedBES Then
////								else
////								{
//									RunSql("SETUSER 'sg" + sGroupId + "'",m_objDbConnection);
//																				
//									CreateView("CREATE VIEW CLAIM AS SELECT * FROM dbo.CLAIM WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW CLAIMANT AS SELECT * FROM dbo.CLAIMANT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW EMPLOYEE AS SELECT * FROM dbo.EMPLOYEE WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW ENTITY AS SELECT * FROM dbo.ENTITY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW EVENT AS SELECT * FROM dbo.EVENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW FUNDS AS SELECT * FROM dbo.FUNDS WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW PERSON_INVOLVED AS SELECT * FROM dbo.PERSON_INVOLVED WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW RESERVE_CURRENT AS SELECT * FROM dbo.RESERVE_CURRENT WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
//									CreateView("CREATE VIEW RESERVE_HISTORY AS SELECT * FROM dbo.RESERVE_HISTORY WHERE SEC_DEPT_EID IN (SELECT ENTITY_ID FROM ENTITY_MAP WHERE GROUP_ID = " + p_iGroupId + ")",m_objDbConnection);
////								}

				//Create adjuster text view if selected
				if(p_sAdjTextTypes != "")
				{
					sSQL = "CREATE VIEW ADJUST_DATED_TEXT AS SELECT * FROM dbo.ADJUST_DATED_TEXT WHERE ";
					if(p_sAdjTextTypes.Substring(0,1) == "-")
						sSQL = sSQL + " TEXT_TYPE_CODE NOT IN (" + p_sAdjTextTypes.Substring(1)  + ")";
					else
						sSQL = sSQL + " TEXT_TYPE_CODE IN (" + p_sAdjTextTypes + ")";

					CreateView(sSQL, m_objDbConnection);

				}

				//Create views for any custom extensions (usually ADM tables)
                sDefFilePath = String.Format(@"{0}\{1}", m_BESSettings[BES_SYSTEMFILEPATH], m_BESSettings[BES_DEFFILENAME]);

                if (!String.IsNullOrEmpty(sDefFilePath))
				{
					objStreamReader = System.IO.File.OpenText(sDefFilePath);
					while(true)
					{
						if( (sContent = objStreamReader.ReadLine()) != null)
						{
							if(sContent.Trim() != "")
							{
								iPos = sContent.IndexOf(" WHERE ");
								iPos2 = sContent.IndexOf("=");
								sTemp = sContent.Substring(iPos + 7,iPos2 - (iPos + 8));
								sTemp2 = sContent.Substring(0,iPos + 7) + sWhere.Replace("SEC_DEPT_EID",sTemp);  
								CreateView(sTemp2,m_objDbConnection);   
							}
						}
						else
							break;
					}
					objStreamReader.Close();
				}

				//restore admin execute privileges dependent on the SQL Server platform
                string strRevertSQL = UTILITY.RevertSQLUser(m_objDbConnection.ConnectionString);
                RunSql(strRevertSQL, m_objDbConnection);
			}//if(!m_bIsAllOrgEntities)
            //commented by Nadim for R6*/

            //commented by Nadim for R6
            //RunSql("UPDATE STATISTICS ENTITY_MAP",m_objDbConnection);
            //commented by Nadim for R6
            if (!m_bIsAllOrgEntities)
            {
                sSQL = string.Empty;
                //pmittal5 Mits 20486 - Password change functionality on Sql also
                if ((p_bSetUpdatedFlag || m_sOverideFlag.Trim() == "1") && (sBESSynSetting.ToUpper() == "FALSE" || p_sStatus == "NEW")) //Deb :MITS 29215
                {
                    sSQL = "UPDATE GROUP_MAP SET UPDATED_FLAG=-1,STATUS_FLAG=0 WHERE DELETED_FLAG=0 AND SUPER_GROUP_ID=" + p_iGroupId;
                }

                try
                {
                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        RunSql(sSQL, m_objDbConnection);
                    }
                }
                catch (Exception)
                {
                    throw; //throw the exception in its current state
                }
            }
            
		}	

		/// Name		: BuildEntityMap_DB2
		/// Author		: Anurag Agarwal
		/// Date Created	: 25 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// BuildEntityMap for DB2
		/// </summary>
		/// <param name="p_iGroupId">Group Id</param>
		/// <param name="p_sAdjTextTypes"></param>
		//private void BuildEntityMap_DB2(int p_iGroupId, string p_sBrokers, string p_sInsurers, string p_sAdjTextTypes)
        private void BuildEntityMap_DB2(int p_iGroupId, string p_sBrokers, string p_sInsurers, string p_sAdjTextTypes, List<int> lstGroupIds, bool p_bEntitiessChanged, string p_sStatus)
		{
			string sTables = "";
			string sSQL = "";
			string sEntry = "";
			string sPrevEntry = "";
			string sGroupId = "";
			string sTableName = "";
			string sSchema = "";
			DbReader objDbReader = null;
			DbReader objTempDbReader = null;
			LocalCache objLocalCache = null;

			string sOrgWhere = "";
			string sBrokerWhere = "";
			string sInsurerWhere = "";

            sTables = VIEW_TABLES + "POLICY|POLICY_ENH";//nadim for 15620-implement ENH POLICY in BES
 
			try
			{
				if(p_sAdjTextTypes != "")
					sTables = sTables + "ADJUST_DATED_TEXT|";

				//Clean out previous group definition
                if ((sBESSynSetting.ToUpper() != "FALSE" || p_sStatus == "NEW") && p_bEntitiessChanged)//Deb MITS 29627
                    DropBESGroup_DB2(p_iGroupId);
                if ((sBESSynSetting.ToUpper() != "FALSE" || p_sStatus == "NEW") && p_bEntitiessChanged)//Deb MITS 29627
                {
                    //Create Entity Access Map
                    RunSql("INSERT INTO ENTITY_MAP(ENTITY_ID,GROUP_ID) VALUES (0," + p_iGroupId + ")", m_objDbConnection);

                    if (!m_bIsAllOrgEntities)
                    {
                        //Put in selected levels and their parents
                        for (int i = 0; i < arDeptList.Count; i++)
                        {
                            if (Conversion.ConvertStrToInteger(arDeptList[i].ToString()) != 0)
                            {
                                sSQL = "INSERT INTO ENTITY_MAP (ENTITY_ID,GROUP_ID) VALUES (" + arDeptList[i] + "," + p_iGroupId + ")";
                                RunSql(sSQL, m_objDbConnection);
                            }
                        }
                        for (int i = 0; i < arParentList.Count; i++)
                        {
                            sEntry = Conversion.ConvertObjToStr(arParentList[i]);
                            if (sEntry != "0,0" && sEntry != sPrevEntry)
                            {
                                sSQL = "INSERT INTO ENTITY_MAP (ENTITY_ID,GROUP_ID) VALUES (" +
                                    sEntry.Substring(0, sEntry.IndexOf(",")) + "," + p_iGroupId + ")";
                                RunSql(sSQL, m_objDbConnection);
                            }
                            sPrevEntry = sEntry;
                        }
                    }

                    //Setup user account for group
                    sGroupId = string.Format("{0:00000}", p_iGroupId);

                    //Insert account info into orgsec login table
                    if (m_bIsAllOrgEntities)
                        sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('DBO', " + p_iGroupId + ",0)";
                    else
                        sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('sg" + sGroupId + "', " + p_iGroupId + ",0)";
                    //Try catch to catch Unique index errors in Entity_Map_User. When we select All Org then 
                    //the userid is same so when try to insert exception is thrown by database.
                    try
                    {
                        RunSql(sSQL, m_objDbConnection);
                    }
                    catch (Exception) { }
                }
				//Now, do per-table grants and view/synonym creation
				if(!m_bIsAllOrgEntities)
				{
					//Get the schema to use by checking the catalog and using the schema of the SYS_PARMS table
					sSQL = "SELECT TABSCHEMA FROM SYSCAT.TABLES WHERE TABNAME = 'SYS_PARMS' AND TYPE = 'T'";
					objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
					if(objDbReader != null)
					{
						if(objDbReader.Read())
						{
							sSchema = objDbReader["TABSCHEMA"].ToString().ToUpper();
						}
						if(objDbReader != null)
							objDbReader.Close();
					}

					//Grant permissions on tables to new account
					sSQL = "SELECT TABNAME FROM SYSCAT.TABLES WHERE TABSCHEMA = '" + sSchema + "' AND TYPE = 'T' ORDER BY TABNAME";
					objDbReader = DbFactory.GetDbReader(m_sConnString,sSQL);
					if(objDbReader != null)
					{
						while(objDbReader.Read())
						{
							sTableName = objDbReader["TABNAME"].ToString().ToUpper();
							//Grant standard privs on tables
							sSQL = "GRANT ALL ON " + sSchema + "." + sTableName + " TO USER sg" + sGroupId;
							RunSql(sSQL, m_objDbConnection);  

							//Create aliases for all tables not secured through a view (defined below)
							//Need to screen out search & merge temp tables.
							if(sTables.IndexOf(sTableName + "|") == -1 && sTableName.Substring(0,2).ToLower() == "s_" && sTableName.Substring(0,2).ToLower() == "m_")
							{
								sSQL = "SELECT TABNAME FROM SYSCAT.TABLES WHERE TABSCHEMA = 'SG" + sGroupId + "' AND TYPE = 'A' AND TABNAME = '" + sTableName + "'";
								objTempDbReader = DbFactory.GetDbReader(sSQL,m_sConnString);
								if(objTempDbReader != null)
								{
									if(objTempDbReader.Read())
									{
										sSQL = "CREATE ALIAS sg" + sGroupId + "." + sTableName + " FOR " + sSchema + "." + sTableName;
										RunSql(sSQL, m_objDbConnection);
										sSQL = "GRANT ALL ON sg" + sGroupId + "." + sTableName + " TO USER sg" + sGroupId;
										CreateView(sSQL, m_objDbConnection);  
									}
									if(objTempDbReader != null)
										objTempDbReader.Close();
								}	
							}
						}
						if(objDbReader != null)
							objDbReader.Close();
					}

					//Create views for secured tables
					sOrgWhere = "SEC_DEPT_EID IN (SELECT EM.ENTITY_ID FROM ENTITY_MAP EM WHERE EM.GROUP_ID = " + p_iGroupId + ")";
					if(m_bIsBrokerSupport && p_sBrokers != "")
						sBrokerWhere = " AND SEC_BROKER_EID IN (" + p_sBrokers + ",0)";
					if(m_bIsInsurerSupport && p_sInsurers != "")
						sInsurerWhere = " AND SEC_INSURER_EID IN (" + p_sInsurers + ",0)";

					CreateView("CREATE VIEW sg" + sGroupId + ".CLAIM AS SELECT CLAIM.* FROM " + 
						sSchema + ".CLAIM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".CLAIM TO USER sg" + sGroupId,m_objDbConnection);

					CreateView("CREATE VIEW sg" + sGroupId + ".CLAIMANT AS SELECT CLAIMANT.* FROM " + 
						sSchema + ".CLAIMANT WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".CLAIMANT TO USER sg" + sGroupId,m_objDbConnection);

					//Can't secure employees or entities by broker  - only by OH
					CreateView("CREATE VIEW sg" + sGroupId + ".EMPLOYEE AS SELECT EMPLOYEE.* FROM " + 
						sSchema + ".EMPLOYEE WHERE " + sOrgWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".EMPLOYEE TO USER sg" + sGroupId,m_objDbConnection);

					CreateView("CREATE VIEW sg" + sGroupId + ".EVENT AS SELECT EVENT.* FROM " + 
						sSchema + ".EVENT WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".EVENT TO USER sg" + sGroupId,m_objDbConnection);
					
					CreateView("CREATE VIEW sg" + sGroupId + ".FUNDS AS SELECT FUNDS.* FROM " + 
						sSchema + ".FUNDS WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".FUNDS TO USER sg" + sGroupId,m_objDbConnection);
						
					CreateView("CREATE VIEW sg" + sGroupId + ".PERSON_INVOLVED AS SELECT PERSON_INVOLVED.* FROM " + 
						sSchema + ".PERSON_INVOLVED WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".PERSON_INVOLVED TO USER sg" + sGroupId,m_objDbConnection);

					CreateView("CREATE VIEW sg" + sGroupId + ".RESERVE_CURRENT AS SELECT RESERVE_CURRENT.* FROM " + 
						sSchema + ".RESERVE_CURRENT WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".RESERVE_CURRENT TO USER sg" + sGroupId,m_objDbConnection);

					CreateView("CREATE VIEW sg" + sGroupId + ".RESERVE_HISTORY AS SELECT RESERVE_HISTORY.* FROM " + 
						sSchema + ".RESERVE_HISTORY WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere,m_objDbConnection);
					CreateView("GRANT ALL ON sg" + sGroupId + ".RESERVE_HISTORY TO USER sg" + sGroupId,m_objDbConnection);

					//Do policy view - a little more complex than the rest
					sSQL = "CREATE VIEW sg" + sGroupId + ".POLICY AS SELECT POLICY.* FROM " + sSchema + ".POLICY " + 
						"WHERE POLICY_ID IN (SELECT PXI.POLICY_ID FROM " + sSchema + ".POLICY_X_INSURED PXI," + 
						sSchema + ".ENTITY_MAP EM WHERE PXI.INSURED_EID = EM.ENTITY_ID AND EM.GROUP_ID = " + p_iGroupId + ")";
					if(m_bIsBrokerSupport && p_sBrokers != "")
						sSQL += " AND BROKER_EID IN (" + p_sBrokers + ",0)";
					if(m_bIsInsurerSupport && p_sInsurers != "")
						sSQL += " AND INSURER_EID IN (" + p_sBrokers + ",0)";
					CreateView(sSQL, m_objDbConnection); 
					CreateView("GRANT ALL ON sg" + sGroupId + ".POLICY TO USER sg" + sGroupId,m_objDbConnection);

                    objLocalCache = new LocalCache(m_sConnString, m_iClientId);
                    //nadim for 15620-implement ENH POLICY in BES
                    sSQL = "CREATE VIEW sg" + sGroupId + ".POLICY_ENH AS SELECT POLICY_ENH.* FROM " + sSchema + ".POLICY_ENH " +
                        "WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM " + sSchema + ".POLICY_X_INSURED PXII," +
                        sSchema + ".ENTITY_MAP EMI WHERE PXII.INSURED_EID = EMI.ENTITY_ID AND EMI.GROUP_ID = " + p_iGroupId + ")";
                    if (m_bIsBrokerSupport && p_sBrokers != "")
                        sSQL += " AND BROKER_EID IN (" + p_sBrokers + ",0)";
                    if (m_bIsInsurerSupport && p_sInsurers != "")
                        sSQL += " AND INSURER_EID IN (" + p_sBrokers + ",0)";
                    CreateView(sSQL, m_objDbConnection);
                    CreateView("GRANT ALL ON sg" + sGroupId + ".POLICY_ENH TO USER sg" + sGroupId, m_objDbConnection);

                    objLocalCache = new LocalCache(m_sConnString, m_iClientId);
                    //nadim for 15620-implement ENH POLICY in BES
					 
					//Do entity view - special requirements
					sSQL = "CREATE VIEW sg" + sGroupId + ".ENTITY AS SELECT ENTITY.* FROM " + sSchema + ".ENTITY " + 
						"WHERE " +sOrgWhere;
					if(m_bIsBrokerSupport && p_sBrokers != "")
						sSQL += " AND (ENTITY.ENTITY_TABLE_ID <> " + objLocalCache.GetTableId("BROKER") + " OR ENTITY.ENTITY_ID IN (" + p_sBrokers + "))";
					if(m_bIsInsurerSupport && p_sInsurers != "")
						sSQL += " AND (ENTITY.ENTITY_TABLE_ID <> " + objLocalCache.GetTableId("INSURERS") + " OR ENTITY.ENTITY_ID IN (" + p_sInsurers + "))";
					CreateView(sSQL, m_objDbConnection); 
					CreateView("GRANT ALL ON sg" + sGroupId + ".ENTITY TO USER sg" + sGroupId,m_objDbConnection);

					//Create adjuster text view if selected
					if(p_sAdjTextTypes != "")
					{
						sSQL = "CREATE VIEW sg" + sGroupId + ".ADJUST_DATED_TEXT AS SELECT ADJUST_DATED_TEXT.* FROM " 
							+ sSchema + ".ADJUST_DATED_TEXT WHERE ";
						if(p_sAdjTextTypes.Substring(0,1) == "-")
							sSQL = sSQL + " TEXT_TYPE_CODE NOT IN (" + p_sAdjTextTypes.Substring(1)  + ")";
						else
							sSQL = sSQL + " TEXT_TYPE_CODE IN (" + p_sAdjTextTypes + ")";
						CreateView(sSQL, m_objDbConnection);
						CreateView("GRANT ALL ON sg" + sGroupId + ".ADJUST_DATED_TEXT TO USER sg" + sGroupId,m_objDbConnection);

					}
				}
			}
			finally
			{ 
				if(objDbReader != null)
					objDbReader.Dispose();

				if(objTempDbReader != null)
					objTempDbReader.Dispose();

				if(objLocalCache != null)
					objLocalCache.Dispose();
			}
		}	

		/// Name		: BuildEntityMap_Oracle
		/// Author		: Anurag Agarwal
		/// Date Created	: 25 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// BuildEntityMap for Oracle
		/// </summary>
		/// <param name="p_iGroupId">Group Id</param>
		/// <param name="p_sAdjTextTypes"></param>
        //private void BuildEntityMap_Oracle(int p_iGroupId, string p_sBrokers, string p_sInsurers, string p_sAdjTextTypes, bool p_bNewgroup)
        private void BuildEntityMap_Oracle(int p_iGroupId, string p_sBrokers, string p_sInsurers, string p_sAdjTextTypes, List<int> p_lstGroupIds, bool p_bSetUpdatedFlag, bool p_bEntitiessChanged, string p_sStatus)
		{
			string sTables = "";
			string sSQL = "";
			string sEntry = "";
			string sPrevEntry = "";
			string sGroupId = "";
			string sTableName = "";
			string sDefTableNameSpace = "";
			string sTempTableNameSpace = "";
            string sEncriptUser = "";
			DbReader objDbReader = null;
			DbReader objTempDbReader = null;
            DbReader objRdr = null;                   //Umesh
			LocalCache objLocalCache = null;
			//SysSettings objSysSettings = null;
            XmlDocument objXml = null;               //Umesh
            XmlNode objXmlNode = null; //Umesh
			string sOrgWhere = "";
			string sBrokerWhere = "";
			string sInsurerWhere = "";
			string sDBO = "";
            bool bViewOnlyBesForOracle = false; //Umesh
            bool bEnhanceBesForOracle = false; //Umesh
            StringBuilder sGroupIds = new StringBuilder();

            try
            {
                objXml = new XmlDocument();
                string strContent = RMSessionManager.GetCustomContent(m_iClientId);
                if (!string.IsNullOrEmpty(strContent))
                {
                    objXml.LoadXml(strContent);
                    //objRdr.Dispose();
                    objXmlNode = objXml.SelectSingleNode("//SpecialSettings/ViewOnlyBES");
                    if (objXmlNode != null)
                    {
                        if (objXmlNode.InnerText == "-1")
                        {
                            bViewOnlyBesForOracle = true;
                        }
                    }
                    objXmlNode = objXml.SelectSingleNode("//SpecialSettings/EnhanceBES");
                    if (objXmlNode != null)
                    {
                        if (objXmlNode.InnerText == "-1")
                        {
                            bEnhanceBesForOracle = true;
                        }
                    }
                }
                sDBO = m_sDBOUserId;

                //objSysSettings = new SysSettings(m_sConnString);

                //if(objSysSettings.ESSP == 1557)
                if (bViewOnlyBesForOracle)
                    sTables = VIEW_TABLES + "FINANCIAL_HIST|POLICY|POLICY_ENH|";//nadim for 15620-implement ENH POLICY in BES
                else
                    sTables = VIEW_TABLES + "POLICY|POLICY_ENH|";//nadim for 15620-implement ENH POLICY in BES

                if (!bEnhanceBesForOracle)//REM  Umesh 04/20/2007
                {
                    if (p_sAdjTextTypes != "")
                        sTables = sTables + "ADJUST_DATED_TEXT|";
                }
                else
                    sTables = sTables + "ADJUST_DATED_TEXT|";


                //Clean out previous group definition
                //commented by Nadim for R6
                foreach (int iGroupId in p_lstGroupIds)
                {
                    if (sGroupIds.Length > 0)
                        sGroupIds.Append(",");
                    sGroupIds.Append(iGroupId.ToString());
                }
                if ((sBESSynSetting.ToUpper() != "FALSE" || p_sStatus == "NEW") && p_bEntitiessChanged)//Deb MITS 29627
                    DropBESGroup_Oracle(p_iGroupId, sGroupIds);
                //commented by Nadim for R6
                if ((sBESSynSetting.ToUpper() != "FALSE" || p_sStatus == "NEW") && p_bEntitiessChanged)//Deb MITS 29627
                {
                    if (m_sCheckDefer != "1")                           //REM Enhancement   04/20/2007
                    {

                        //Create Entity Access Map
                        // RunSql("INSERT INTO ENTITY_MAP(ENTITY_ID,GROUP_ID) VALUES (0," + p_iGroupId + ")", m_objDbConnection);
                        m_objDbCommand.CommandText = "INSERT INTO ENTITY_MAP(ENTITY_ID,GROUP_ID) VALUES (0," + p_iGroupId + ")";
                        m_objDbCommand.ExecuteNonQuery();

                        if (!m_bIsAllOrgEntities)
                        {
                            //Put in selected levels and their parents
                            for (int i = 0; i < arDeptList.Count; i++)
                            {
                                if (Conversion.ConvertStrToInteger(arDeptList[i].ToString()) != 0)
                                {
                                    sSQL = "INSERT INTO ENTITY_MAP (ENTITY_ID,GROUP_ID) VALUES (" + arDeptList[i] + "," + p_iGroupId + ")";
                                    //RunSql(sSQL, m_objDbConnection);
                                    m_objDbCommand.CommandText = sSQL;
                                    m_objDbCommand.ExecuteNonQuery();
                                }
                            }
                            for (int i = 0; i < arParentList.Count; i++)
                            {
                                sEntry = Conversion.ConvertObjToStr(arParentList[i]);
                                if (sEntry != "0,0" && sEntry != sPrevEntry)
                                {
                                    sSQL = "INSERT INTO ENTITY_MAP (ENTITY_ID,GROUP_ID) VALUES (" +
                                        sEntry.Substring(0, sEntry.IndexOf(",")) + "," + p_iGroupId + ")";
                                    // RunSql(sSQL, m_objDbConnection);
                                    m_objDbCommand.CommandText = sSQL;
                                    m_objDbCommand.ExecuteNonQuery();
                                }
                                sPrevEntry = sEntry;
                            }
                        }
                    }

                    //Setup user account for group
                    /*    //commented by Nadim for R6-
                      //  sGroupId = string.Format("{0:00000}", p_iGroupId);
                

                        if (m_sCheckDefer!="1")                           //REM Enhancement   04/20/2007
                        {
                            //Now, do per-table grants and view/synonym creation
                            if (!m_bIsAllOrgEntities)
                            {
                                //Determine default and temp tablespace settings for this user
                                sSQL = "SELECT DEFAULT_TABLESPACE,TEMPORARY_TABLESPACE FROM USER_USERS";
                                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                                if (objDbReader != null)
                                {
                                    if (objDbReader.Read())
                                    {
                                        sDefTableNameSpace = objDbReader["DEFAULT_TABLESPACE"].ToString().ToUpper();
                                        sTempTableNameSpace = objDbReader["TEMPORARY_TABLESPACE"].ToString().ToUpper();
                                    }
                                    if (objDbReader != null)
                                        objDbReader.Close();
                                }

                                //Formulate SQL to create new Oracle account for this group
                                //abisht MITS 10669
                                if (m_sOverideFlag == "0" || m_sOverideFlag == "")
                                {
                                    sSQL = "CREATE USER sg" + sGroupId + " IDENTIFIED BY sg" + sGroupId +
                                        " DEFAULT TABLESPACE " + sDefTableNameSpace + " TEMPORARY TABLESPACE " + sTempTableNameSpace;
                                }
                                else
                                {
                                    sSQL = "CREATE USER sg" + sGroupId + " IDENTIFIED BY " + m_sOveridingPassWord +
                                        " DEFAULT TABLESPACE " + sDefTableNameSpace + " TEMPORARY TABLESPACE " + sTempTableNameSpace;
                                }

                                //TODO: to ask for code which prompt user for SQL verification
                                // Prompt user to create new account. Gives a DBA a chance to modify our SQL before proceeding.
                                // Load frmOrgSecSQLConfirm
                                // frmOrgSecSQLConfirm.Caption = "Confirm Create User"
                                // frmOrgSecSQLConfirm.labMsg = "RISKMASTER/World will now create a new Oracle account (sg" & sGroupID & ") to use for this group."
                                // frmOrgSecSQLConfirm.txtSQL = sSQL
                                // subCenterForm frmOrgSecSQLConfirm
                                // frmOrgSecSQLConfirm.Show vbModal
                                // sSQL = frmOrgSecSQLConfirm.txtSQL
                                // Unload frmOrgSecSQLConfirm

                                //Go ahead and create new account
                                try
                                {
                                    //RunSql(sSQL, objAdminDbConnection);
                                    m_objAdminDbCommand.CommandText = sSQL;
                                    m_objAdminDbCommand.ExecuteNonQuery();
                                }
                                catch (Exception p_objException)
                                {
                                    //ignore if user account already exists
                                    if (p_objException.Message.IndexOf("ORA-01920") == -1)
                                        throw new Exception("", p_objException);
                                }

                                //RunSql("GRANT CONNECT,RESOURCE,UNLIMITED TABLESPACE TO sg" + sGroupId, objAdminDbConnection);
                                m_objAdminDbCommand.CommandText = "GRANT CONNECT,RESOURCE,UNLIMITED TABLESPACE TO sg" + sGroupId;
                                m_objAdminDbCommand.ExecuteNonQuery();
                            }
                        }
                        //commented by Nadim for R6-*/
                    //Insert account info into orgsec login table
                    foreach (int iUserGroupId in p_lstGroupIds)
                    {
                        if (m_bIsAllOrgEntities)
                            //sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + sDBO + "', " + p_iGroupId + ",0)";
                            sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + sDBO + "', " + iUserGroupId + ",0)";
                        else
                        {
                            //sEncriptUser = FormatBESUserName(Conversion.ConvertStrToInteger(m_sDSNId), m_sConnString, p_iGroupId);
                            //sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + sEncriptUser + "', " + p_iGroupId + ",0)";
                            sEncriptUser = FormatBESUserName(Conversion.ConvertStrToInteger(m_sDSNId), m_sConnString, iUserGroupId);
                            sSQL = "INSERT INTO ENTITY_MAP_USER (USER_NAME,GROUP_ID,DEPT_EID) VALUES ('" + sEncriptUser + "', " + iUserGroupId + ",0)";
                        }
                        //Try catch to catch Unique index errors in Entity_Map_User. When we select All Org then 
                        //the userid is same so when try to insert exception is thrown by database.
                        try
                        {
                            //RunSql(sSQL, m_objDbConnection);
                            m_objDbCommand.CommandText = sSQL;
                            m_objDbCommand.ExecuteNonQuery();

                        }
                        catch (Exception Exp) { }
                        if (!m_bIsAllOrgEntities)
                        {
                            if ((p_bSetUpdatedFlag || m_sOverideFlag.Trim() == "1") && (sBESSynSetting.ToUpper() == "FALSE" || p_sStatus == "NEW")) //Deb :MITS 29215
                            {
                                sSQL = "UPDATE GROUP_MAP SET UPDATED_FLAG=-1,STATUS_FLAG=0 WHERE DELETED_FLAG=0 AND SUPER_GROUP_ID=" + p_iGroupId + " AND GROUP_ID=" + iUserGroupId;
                                RunSql(sSQL, m_objDbConnection);
                            }
                        }
                    }
                }
                //End - pmittal5

                        //					Anurag 29 June 2006 - Query taken from RMWorld - DTG Rocket is firing this query when 
                        //					called by RMWorld. I got it by attaching a profiler in between RMWorld & oracle database.
                        //					But it is taking to much time to execute so shifted to diff query, this is commented
                        //					& not deleted for reference

                        //					sSQL = "select * from (select null table_qualifier, o1.owner table_owner, " + 
                        //							"o1.object_name table_name, decode(o1.owner,'SYS', decode(o1.object_type,'TABLE'," + 
                        //							"'SYSTEM TABLE','VIEW', 'SYSTEM VIEW', o1.object_type), 'SYSTEM', " + 
                        //							"decode(o1.object_type,'TABLE','SYSTEM TABLE','VIEW', 'SYSTEM VIEW', o1.object_type), " + 
                        //							"o1.object_type) table_type,  null remarks from all_objects o1 where o1.object_type " + 
                        //							"in ('TABLE', 'VIEW') union select null table_qualifier, s.owner table_owner, " + 
                        //							"s.synonym_name table_name, 'SYNONYM' table_type,  null remarks from all_objects o3, " + 
                        //							"all_synonyms s where o3.object_type in ('TABLE','VIEW') and s.table_owner= o3.owner " + 
                        //							"and s.table_name = o3.object_name union select null table_qualifier, s1.owner " + 
                        //							"table_owner, s1.synonym_name table_name, 'SYNONYM' table_type,  null remarks from " + 
                        //							"all_synonyms s1 where s1.db_link is not null ) tables WHERE 1=1  " + 
                        //							"AND ( table_type = 'TABLE' )  ORDER BY 4,2,3";

                       /* //commented by Nadim for R6-
                        if (!bEnhanceBesForOracle)//REM  Umesh 04/20/2007
                        {
                            //Grant permissions on tables to new account --

                            sSQL = "SELECT TABLE_NAME FROM USER_TABLES ";

                            objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                            if (objDbReader != null)
                            {
                                while (objDbReader.Read())
                                {
                                    sTableName = objDbReader["TABLE_NAME"].ToString().ToUpper();

                                    //For tables which are deleted/dropped. I could have used DROPPED = 'NO' in query
                                    //but i guess this column is not supported in 9i.
                                    if (sTableName.IndexOf("BIN$") >= 0)
                                        continue;

                                    //Grant standard privs on tables
                                    sSQL = "grant select,insert,update,delete on " + sDBO + "." + sTableName + " to sg" + sGroupId;
                                    CreateOraView(sSQL, m_objDbCommand);
                                   
                                    // Create synonyms for all tables not secured through a view (defined below)
                                    //Need to screen out search & merge temp tables.
                                    if (sTables.IndexOf(sTableName + "|") == -1 && sTableName.Substring(0, 2).ToLower() != "s_" && sTableName.Substring(0, 2).ToLower() != "m_")
                                    {
                                        sSQL = "create synonym sg" + sGroupId + "." + sTableName + " for " + sDBO + "." + sTableName;
                                        CreateOraView(sSQL,  m_objAdminDbCommand);
                                       

                                    }
                                }
                                if (objDbReader != null)
                                    objDbReader.Close();
                            }
                        }
                        //commented by Nadim for R6-*/
                        //REM UMEHS 04/20/2007

                        /*//commented by Nadim for R6-
                        else
                        {
                            sSQL = "grant " + m_sOraBESRole.ToUpper() + " to sg" + sGroupId;
                            CreateOraView(sSQL, m_objDbCommand);    // ... execute through table owners account
                            if (!bViewOnlyBesForOracle)
                            {
                                sSQL = "SELECT TABLE_NAME FROM USER_TABLES ";

                                objDbReader = DbFactory.GetDbReader(m_sConnString, sSQL);
                                if (objDbReader != null)
                                {
                                    while (objDbReader.Read())
                                    {
                                        sTableName = objDbReader["TABLE_NAME"].ToString().ToUpper();

                                        //For tables which are deleted/dropped. I could have used DROPPED = 'NO' in query
                                        //but i guess this column is not supported in 9i.
                                        if (sTableName.IndexOf("BIN$") >= 0)
                                            continue;

                                        //Grant standard privs on tables
                                        sSQL = "grant select,insert,update,delete on " + sDBO + "." + sTableName + " to sg" + sGroupId;
                                        CreateOraView(sSQL, m_objDbCommand);
                                        
                                    }
                                    if (objDbReader != null)
                                        objDbReader.Close();
                                }
                            }
                            else
                            {
                                sTables = sTables + "POLICY_X_INSURED|ENTITY_MAP|";
                                string[] sTablesArr = new string[20];
                                sTablesArr = sTables.Split('|');
                                for (int i = 0; i < sTablesArr.Length; i++)
                                {
                                     // Grant standard privs on tables
                                     sTableName = sTablesArr[i];
                                     sSQL = "grant select,insert,update,delete on " + sTableName + " to sg" + sGroupId;
                                     CreateOraView(sSQL,  m_objDbCommand);   //... execute through table owners account (dbODBC1)
                                     
                                }
    
                            }
                            
                        }   //REM End
                        //commented by Nadim for R6-*/
                        //Create views for secured tables
                        //Standard org sec definition.
                        //if(objSysSettings.ESSP != 1557)
                     /*   //commented by Nadim for R6-
                        if (!bViewOnlyBesForOracle)
                        {
                            sOrgWhere = "SEC_DEPT_EID IN (SELECT EM.ENTITY_ID FROM ENTITY_MAP EM WHERE EM.GROUP_ID = " + p_iGroupId + ")";
                            //sOrgWhere = "SEC_DEPT_EID IN (SELECT EM.ENTITY_ID FROM ENTITY_MAP EM WHERE EM.GROUP_ID = " + p_iGroupId;
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sBrokerWhere = " AND SEC_BROKER_EID IN (" + p_sBrokers + ",0)";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sInsurerWhere = " AND SEC_INSURER_EID IN (" + p_sInsurers + ",0)";

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".CLAIM AS SELECT CLAIM.* FROM " +
                                sDBO + ".CLAIM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);
                         /*   //commented by Nadim for R6-
                           // CreateOraView("CREATE VIEW sg" + sGroupId + ".CLAIMANT AS SELECT /*+ INDEX(CLAIMANT) */ 
                        //CLAIMANT.* FROM " +
                           //    sDBO + ".CLAIMANT WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);
                          /*  //commented by Nadim for R6-
                            //Can't secure employees or entities by broker  - only by OH
                            CreateOraView("CREATE VIEW sg" + sGroupId + ".EMPLOYEE AS SELECT EMPLOYEE.* FROM " +
                                sDBO + ".EMPLOYEE WHERE " + sOrgWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".EVENT AS SELECT EVENT.* FROM " +
                                sDBO + ".EVENT WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".FUNDS AS SELECT FUNDS.* FROM " +
                                sDBO + ".FUNDS WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".PERSON_INVOLVED AS SELECT PERSON_INVOLVED.* FROM " +
                                sDBO + ".PERSON_INVOLVED WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".RESERVE_CURRENT AS SELECT RESERVE_CURRENT.* FROM " +
                                sDBO + ".RESERVE_CURRENT WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".RESERVE_HISTORY AS SELECT RESERVE_HISTORY.* FROM " +
                                sDBO + ".RESERVE_HISTORY WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            //Do policy view - a little more complex than the rest
                            sSQL = "CREATE VIEW sg" + sGroupId + ".POLICY AS SELECT POLICY.* FROM " + sDBO + ".POLICY " +
                                "WHERE POLICY_ID IN (SELECT PXI.POLICY_ID FROM " + sDBO + ".POLICY_X_INSURED PXI," +
                                sDBO + ".ENTITY_MAP EM WHERE PXI.INSURED_EID = EM.ENTITY_ID AND EM.GROUP_ID = " + p_iGroupId + ")";
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sSQL += " AND BROKER_EID IN (" + p_sBrokers + ",0)";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sSQL += " AND INSURER_EID IN (" + p_sBrokers + ",0)";
                            CreateOraView(sSQL, m_objAdminDbCommand);
                            //nadim for 15620-implement ENH POLICY in BES
                            string sSqlPolicy = string.Empty;
                            sSqlPolicy = "CREATE VIEW sg" + sGroupId + ".POLICY_ENH AS SELECT POLICY_ENH.* FROM " + sDBO + ".POLICY_ENH " +
                                "WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM " + sDBO + ".POLICY_X_INSRD_ENH PXII," +
                                sDBO + ".ENTITY_MAP EMI WHERE PXII.INSURED_EID = EMI.ENTITY_ID AND EMI.GROUP_ID = " + p_iGroupId + ")";
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sSqlPolicy += " AND BROKER_EID IN (" + p_sBrokers + ",0)";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sSqlPolicy += " AND INSURER_EID IN (" + p_sBrokers + ",0)";
                            CreateOraView(sSqlPolicy, m_objAdminDbCommand);
                            //nadim for 15620-implement ENH POLICY in BES

                            objLocalCache = new LocalCache(m_sConnString);

                            //Do entity view - special requirements
                            sSQL = "CREATE VIEW sg" + sGroupId + ".ENTITY AS SELECT ENTITY.* FROM " + sDBO + ".ENTITY " +
                                "WHERE " + sOrgWhere;
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sSQL += " AND (ENTITY.ENTITY_TABLE_ID <> " + objLocalCache.GetTableId("BROKER") + " OR ENTITY.ENTITY_ID IN (" + p_sBrokers + "))";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sSQL += " AND (ENTITY.ENTITY_TABLE_ID <> " + objLocalCache.GetTableId("INSURERS") + " OR ENTITY.ENTITY_ID IN (" + p_sInsurers + "))";
                            CreateOraView(sSQL, m_objAdminDbCommand);
                        }
                        else //Custom for REM
                        {
                            sOrgWhere = "SEC_DEPT_EID = EM.ENTITY_ID AND EM.GROUP_ID = " + p_iGroupId ;
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sBrokerWhere = " AND SEC_BROKER_EID IN (" + p_sBrokers + ",0)";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sInsurerWhere = " AND SEC_INSURER_EID IN (" + p_sInsurers + ",0)";

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".CLAIM AS SELECT CLAIM.* FROM " +
                                sDBO + ".CLAIM," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);
                        //commented by Nadim for R6-
                           // CreateOraView("CREATE VIEW sg" + sGroupId + ".CLAIMANT AS SELECT /*+ INDEX(CLAIMANT) */ 
                        //CLAIMANT.* FROM " +
                             //   sDBO + ".CLAIMANT," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);
                       /* //commented by Nadim for R6-
                            //Can't secure employees or entities by broker  - only by OH
                            CreateOraView("CREATE VIEW sg" + sGroupId + ".EMPLOYEE AS SELECT EMPLOYEE.* FROM " +
                                sDBO + ".EMPLOYEE," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".EVENT AS SELECT EVENT.* FROM " +
                                sDBO + ".EVENT," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".FUNDS AS SELECT FUNDS.* FROM " +
                                sDBO + ".FUNDS," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".PERSON_INVOLVED AS SELECT PERSON_INVOLVED.* FROM " +
                                sDBO + ".PERSON_INVOLVED," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".RESERVE_CURRENT AS SELECT RESERVE_CURRENT.* FROM " +
                                sDBO + ".RESERVE_CURRENT," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".RESERVE_HISTORY AS SELECT RESERVE_HISTORY.* FROM " +
                                sDBO + ".RESERVE_HISTORY," + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere + sBrokerWhere + sInsurerWhere, m_objAdminDbCommand);

                            CreateOraView("CREATE VIEW sg" + sGroupId + ".FINANCIAL_HIST AS SELECT FINANCIAL_HIST.* FROM sg" + sGroupId + ".CLAIM," +
                                sDBO + ".FINANCIAL_HIST WHERE sg" + sGroupId + ".CLAIM.CLAIM_ID = " + sDBO + ".FINANCIAL_HIST.CLAIM_ID", m_objAdminDbCommand);

                            //Do policy view - a little more complex than the rest
                            sSQL = "CREATE VIEW sg" + sGroupId + ".POLICY AS SELECT POLICY.* FROM " + sDBO + ".POLICY " +
                                "WHERE POLICY_ID IN (SELECT PXI.POLICY_ID FROM " + sDBO + ".POLICY_X_INSURED PXI," +
                                sDBO + ".ENTITY_MAP EM WHERE PXI.INSURED_EID = EM.ENTITY_ID AND EM.GROUP_ID = " + p_iGroupId + ")";
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sSQL += " AND BROKER_EID IN (" + p_sBrokers + ",0)";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sSQL += " AND INSURER_EID IN (" + p_sBrokers + ",0)";
                            CreateOraView(sSQL, m_objAdminDbCommand);
                            //nadim for 15620-implement ENH POLICY in BES
                            string sSqlOrclEnh = string.Empty;
                            sSqlOrclEnh = "CREATE VIEW sg" + sGroupId + ".POLICY_ENH AS SELECT POLICY_ENH.* FROM " + sDBO + ".POLICY_ENH " +
                              "WHERE POLICY_ID IN (SELECT PXII.POLICY_ID FROM " + sDBO + ".POLICY_X_INSRD_ENH PXII," +
                              sDBO + ".ENTITY_MAP EMI WHERE PXII.INSURED_EID = EMI.ENTITY_ID AND EMI.GROUP_ID = " + p_iGroupId + ")";
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sSqlOrclEnh += " AND BROKER_EID IN (" + p_sBrokers + ",0)";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sSqlOrclEnh += " AND INSURER_EID IN (" + p_sBrokers + ",0)";
                            CreateOraView(sSqlOrclEnh, m_objAdminDbCommand);
                            //nadim for 15620-implement ENH POLICY in BES

                            objLocalCache = new LocalCache(m_sConnString);

                            //Do entity view - special requirements
                            sSQL = "CREATE VIEW sg" + sGroupId + ".ENTITY AS SELECT ENTITY.* FROM " + sDBO + ".ENTITY , " + sDBO + ".ENTITY_MAP EM WHERE " + sOrgWhere;
                            if (m_bIsBrokerSupport && p_sBrokers != "")
                                sSQL += " AND (ENTITY.ENTITY_TABLE_ID <> " + objLocalCache.GetTableId("BROKER") + " OR ENTITY.ENTITY_ID IN (" + p_sBrokers + "))";
                            if (m_bIsInsurerSupport && p_sInsurers != "")
                                sSQL += " AND (ENTITY.ENTITY_TABLE_ID <> " + objLocalCache.GetTableId("INSURERS") + " OR ENTITY.ENTITY_ID IN (" + p_sInsurers + "))";
                            CreateOraView(sSQL, m_objAdminDbCommand);
                        }
                        //Create adjuster text view if selected
                        if (p_sAdjTextTypes != "")
                        {
                            sSQL = "CREATE VIEW sg" + sGroupId + ".ADJUST_DATED_TEXT AS SELECT ADJUST_DATED_TEXT.* FROM "
                                + sDBO + ".ADJUST_DATED_TEXT WHERE ";
                            if (p_sAdjTextTypes.Substring(0, 1) == "-")
                                sSQL = sSQL + " TEXT_TYPE_CODE NOT IN (" + p_sAdjTextTypes.Substring(1) + ")";
                            else
                                sSQL = sSQL + " TEXT_TYPE_CODE IN (" + p_sAdjTextTypes + ")";
                            CreateOraView(sSQL, m_objAdminDbCommand);
                        }
                        //commented by Nadim for R6-*/
                        
                        // REM Umesh 04/20/2007
                       /*//commented by Nadim for R6-
                        else
                        {
                            if (bEnhanceBesForOracle)
                            {
                                sSQL = "CREATE VIEW sg" + sGroupId + ".ADJUST_DATED_TEXT AS SELECT ADJUST_DATED_TEXT.* FROM " + sDBO + ".ADJUST_DATED_TEXT";
                                CreateOraView(sSQL, m_objAdminDbCommand);
                            }
                        }
                         //commented by Nadim for R6-*/
                    //}
                //}
            }

            finally
            {
                if (objDbReader != null)
                    objDbReader.Close();

                if (objTempDbReader != null)
                    objTempDbReader.Dispose();

                if (objLocalCache != null)
                    objLocalCache.Dispose();
                if (objRdr != null)
                    objRdr.Dispose();
                objXml = null;
                objXml = null;
                objXmlNode = null;
                bViewOnlyBesForOracle = false;
                bEnhanceBesForOracle = false;
            }
            if (!m_bIsAllOrgEntities)
            {
                sSQL = string.Empty;
                //if (p_bNewgroup || m_sOverideFlag.Trim() == "1")  //pmittal5
                if ((p_bSetUpdatedFlag || m_sOverideFlag.Trim() == "1") && (sBESSynSetting.ToUpper() == "FALSE" || p_sStatus == "NEW")) //Deb :MITS 29215
                {
                    
                    sSQL = "UPDATE GROUP_MAP SET UPDATED_FLAG=-1,STATUS_FLAG=0 WHERE DELETED_FLAG=0 AND SUPER_GROUP_ID=" + p_iGroupId;
                }
                
                try
                {
                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        m_objDbCommand.CommandText = sSQL;
                        m_objDbCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception)
                {
                    throw; //throw the exception in its current state
                }
            }
		}

		/// <summary>
		/// Returns the new connection string with DBA role.
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>connection String</returns>
		public string GetNewConnString(XmlDocument p_objXMLDocument)
		{
			int iPos = 0;
			string sUserId = "";
			string sPWD = "";
			string sConStr = "";

			try
			{
				if(p_objXMLDocument.SelectSingleNode("//AdminUserID") != null)
				{
					sUserId =((XmlElement)p_objXMLDocument.SelectSingleNode("//AdminUserID")).InnerText;
					sPWD =((XmlElement)p_objXMLDocument.SelectSingleNode("//AdminPwd")).InnerText;

					if(sUserId.Trim() != "" && sPWD.Trim() != "")
					{
						sConStr = m_sConnString;
						//UTILITY.GetUIdPwdFromConString(sConStr,ref sUserId,ref sPWD);
						iPos = sConStr.IndexOf("UID=");
						return sConStr.Substring(0,iPos + 4) + sUserId + ";" + "PWD=" + sPWD + ";"; 
					}
					else
						return "";
				}
				else
					return "";
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ORGSEC.GetNewConnString.Err", m_iClientId),p_objEx); 
			}
		}

		private void DropView(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql); 
			}
			catch{}
		}

        private void DropOraView(string p_sSql, DbCommand p_objCommand)
        {
            try
            {
                p_objCommand.CommandText = p_sSql;
                p_objCommand.ExecuteNonQuery();
            }
            catch { }
        }

		private void DropIndex(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql); 
			}
			catch{}
		}

		private void CreateIndex(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql); 
			}
			catch{}
		}

		private void CreateView(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql); 
			}
			catch{}
		}

        private void CreateOraView(string p_sSql, DbCommand p_objCommand)
        {
            try
            {
                p_objCommand.CommandText = p_sSql;
                p_objCommand.ExecuteNonQuery();
            }
            catch(Exception) {}
        }

		private void DropTrigger(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql); 
			}
			catch{}
		}

		private void DropUserAccount(string p_sSPName, string p_sParams, DbConnection p_objDbConnection)
		{
			DbCommand objCommand = null;
			DbParameter objParms = null;

			try
			{
				objCommand = p_objDbConnection.CreateCommand(); 
				objCommand.CommandType = System.Data.CommandType.StoredProcedure;
				objCommand.CommandText = p_sSPName;

				objParms =  objCommand.CreateParameter();
				objParms.Value = p_sParams;
				objParms.ParameterName = "@name_in_db";
				objParms.Direction = System.Data.ParameterDirection.Input;
				    
				objCommand.Parameters.Add(objParms);  
				objCommand.CommandTimeout=5;
				objCommand.ExecuteNonQuery();
			}
			finally
			{
				if(objParms != null)
					objParms = null;

				if(objCommand != null)
					objCommand = null;
			}
		}

		private void DropUserAccount(string p_sSql, string p_sConnectionString)
		{
			try
			{
                DbFactory.GetDataSet(p_sConnectionString, p_sSql, m_iClientId);
			}
			catch{}
		}

		private void AddUserAccount(string p_sSql, string p_sConnectionString)
		{
			try
			{
                DbFactory.GetDataSet(p_sConnectionString, p_sSql, m_iClientId);
			}
			catch(Exception p_objExp)
			{
				string sExp = p_objExp.Message; 
			}
		}

		private void DropUserAccount(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql);
			}
			catch{}
		}

		private void AddUserAccount(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql);
			}
			catch(Exception p_objExp)
			{
				string sExp = p_objExp.Message; 
			}
		}

		private void RunSql(string p_sSql, DbConnection p_objConnection)
		{
			try
			{
				p_objConnection.ExecuteNonQuery(p_sSql);  
			}
			catch(Exception p_objExp)
			{
				throw p_objExp;
			}
		}

        private void EnumDepts(int p_iEntityId, DbConnection p_objDbConnection)
        {
            int iTableId = 0;
            string sOrgTableName = "";
            int iDeptId = 0;
            int iEntityId = 0;
            int iParentId = 0;
            DbReader objDBReader = null;
            string sAbbr = "";
            string sName = "";
            LocalCache objLocalCache = null;
            string sConnString = "";
            DbConnection objDbConnection = null;

            try
            {
                objLocalCache = new LocalCache(m_sConnString, m_iClientId);
                iTableId = objLocalCache.GetOrgTableId(p_iEntityId);
                objLocalCache.Dispose();
                sConnString = p_objDbConnection.ConnectionString;
                objDbConnection = DbFactory.GetDbConnection(sConnString);
                objDbConnection.Open();
                //				if(iTableId == 1012)
                //				{
                //					if(arDeptList.IndexOf(p_iEntityId) < 0)
                //						arDeptList.Add(p_iEntityId);  
                //				}
                //				else
                //				{
                if (arDeptList.IndexOf(p_iEntityId) < 0)
                    arDeptList.Add(p_iEntityId);
                sOrgTableName = Conversion.EntityTableIdToOrgTableName(iTableId);
                int iClientID = Conversion.ConvertObjToInt(objDbConnection.ExecuteScalar(String.Format("SELECT CLIENT_EID FROM ORG_HIERARCHY WHERE {0}_EID={1}", sOrgTableName, p_iEntityId)), m_iClientId);
                objDBReader = objDbConnection.ExecuteReader(String.Format("SELECT DEPARTMENT_EID FROM ORG_HIERARCHY WHERE {0}_EID={1}", sOrgTableName, p_iEntityId));
                if (objDBReader != null)
                {
                    while (objDBReader.Read())
                    {
                        iDeptId = Conversion.ConvertObjToInt(objDBReader["DEPARTMENT_EID"].ToString(), m_iClientId);
                        if (arDeptList.IndexOf(iDeptId) < 0)
                            arDeptList.Add(iDeptId);
                        iEntityId = iDeptId;
                        while (iEntityId != iClientID && iEntityId != 0)
                        {
                            objLocalCache = new LocalCache(m_sConnString, m_iClientId);
                            iTableId = objLocalCache.GetOrgTableId(iEntityId);
                            objLocalCache.GetParentOrgInfo(iDeptId, iTableId - 1, ref sAbbr, ref sName, ref iParentId);
                            if (arDeptList.IndexOf(iParentId) < 0)
                                arDeptList.Add(iParentId);
                            iEntityId = iParentId;
                            objLocalCache.Dispose();
                        }
                    }
                }
                // }
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
                if (objDBReader != null)
                {
                    objDBReader.Dispose();
                    objDBReader = null;
                }
                if (objDbConnection != null)
                    objDbConnection.Dispose();

                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }
        }
	}
}
