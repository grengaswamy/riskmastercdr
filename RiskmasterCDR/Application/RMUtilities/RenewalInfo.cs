using System;
using System.Xml; 
using Riskmaster.Db;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21st,Mar 2005
	///Purpose :   RenewalInfo implementation form
	/// </summary>	
	public class RenewalInfo:UtilitiesUIBase 
	{
		/// <summary>
		/// User Name
		/// </summary>
		private string m_sUserName = "";
		
		/// <summary>
		/// String array for Base Class field mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "RENEWAL_INFO_ROWID"},
			{"AddedByUser", "ADDED_BY_USER"},
			{"DttmRcdAdded", "DTTM_RCD_ADDED"},
			{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
			{"UpdatedByUser", "UPDATED_BY_USER"},
			{"LOBcode", "LINE_OF_BUSINESS"},
			{"Statecode", "STATE"},
			{"Amount", "AMOUNT"},
			{"TermCode", "TERM_CODE"}
			};

		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnectionString = "";

        private int m_iClientId = 0;

		/// <summary>
		/// XML Doc
		/// </summary>
		private XmlDocument m_objXMLDocument = null;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public RenewalInfo(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
			m_sConnectionString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// <summary>
		/// Overloaded Constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public RenewalInfo(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			m_sUserName = p_sLoginName;
			ConnectString = p_sConnectionString;
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
			this.Initialize();
		}

		/// <summary>
		/// Initilizes the fields
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_POL_RNWL_INFO";
			KeyField = "RENEWAL_INFO_ROWID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// <summary>
		/// Get the data for the RenewalInfo form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data structure</param>
		/// <returns>Xml document with data and the form structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				return XMLDoc; 
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("RenewalInfo.Get.Error", m_iClientId), p_objExp);   
			}
		}

		/// <summary>
		/// Saves the data in the Database
		/// </summary>
		/// <param name="p_objXmlDocument">XML Doc with Data</param>
		/// <returns>XMl Doc</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				m_objXMLDocument = p_objXmlDocument;
				if(ValidateXMLData()) 
				{
					FillXML();
					base.Save();
				}
				return XMLDoc;
			}
			catch(RMAppException p_objExp)
			{
				throw p_objExp; 
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("RenewalInfo.Save.Error", m_iClientId), p_objExp);   
			}
		}

		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes Renewal Info record
		/// </summary>
		/// <param name="p_iRenewalInfoRowId">Renewal Info ID</param>
		/// <returns>True for sucess and false for failure</returns>
		internal bool Delete(int p_iRenewalInfoRowId)
		{
			string sSQL = "";
			DbConnection objCon = null;

            try
            {
                if (p_iRenewalInfoRowId != 0)
                {
                    sSQL = "DELETE FROM SYS_POL_RNWL_INFO WHERE RENEWAL_INFO_ROWID = " + p_iRenewalInfoRowId;
                    objCon = DbFactory.GetDbConnection(m_sConnectionString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);

                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("RenewalInfo.Delete.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
            }
		}
		/// <summary>
		/// Fills the XML for user info
		/// </summary>
		private void FillXML()
		{
			string sThresholdRowId = "";
			XmlNodeList objNodeList = null;
			string sNodename = string.Empty;

            // npadhy Start MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sAddedByUser = string.Empty;
            string sDateTimeAdded = string.Empty;
            // npadhy End MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB


			sThresholdRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;

            // npadhy Start MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB
            if (sThresholdRowId != "")
            {
                sSQL = "SELECT ADDED_BY_USER, DTTM_RCD_ADDED FROM SYS_POL_RNWL_INFO WHERE RENEWAL_INFO_ROWID=" + sThresholdRowId;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        sAddedByUser = objDbReader["ADDED_BY_USER"].ToString();
                        sDateTimeAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
                    }
                }
                objDbReader.Dispose();
            }
            // npadhy End MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB

			objNodeList=m_objXMLDocument.GetElementsByTagName("control");

			foreach(XmlElement objXMLElement in objNodeList)
			{
				sNodename = objXMLElement.GetAttribute("name");  
				switch(sNodename)
				{
					case "AddedByUser":
						if(sThresholdRowId == "")
							objXMLElement.InnerText = m_sUserName;
                        // npadhy Start MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
                        else
                            objXMLElement.InnerText = sAddedByUser;
                        // npadhy End MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
						break;
					case "DttmRcdAdded":
						if(sThresholdRowId == "")
                            objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
                        // npadhy Start MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
                        else
                            objXMLElement.InnerText = sDateTimeAdded;
                        // npadhy End MITS 20714 When the Renewal Info is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
						break;
					case "DttmRcdLastUpd":
                        objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
						break;
					case "UpdatedByUser":
						objXMLElement.InnerText = m_sUserName; 
						break;
				}
			}
		}

		/// <summary>
		/// validates the XML 
		/// </summary>
		/// <returns>boolean for success/failure</returns>
		private bool ValidateXMLData()
		{
			string sLOBCode = "";
			string sStateCode = "";
			string sSQL = "";
			string sRenewalInfoRowId = "";
			DbReader objDBReader = null;

			try
			{
				sRenewalInfoRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;
				sLOBCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='LOBcode']")).GetAttribute("codeid");
				sStateCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='Statecode']")).GetAttribute("codeid");

				sSQL = "SELECT RENEWAL_INFO_ROWID FROM SYS_POL_RNWL_INFO WHERE LINE_OF_BUSINESS = " + 
					sLOBCode + " AND STATE = " + sStateCode;
				
				objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

				while(objDBReader.Read())
				{
					if(objDBReader["RENEWAL_INFO_ROWID"].ToString() != sRenewalInfoRowId)
                        throw new RMAppException(Globalization.GetString("RenewalInfo.ValidateXMLData.AlreadyExists", m_iClientId));
				}
				objDBReader.Close();

				return true;
			}
			finally
			{
				if(objDBReader != null)
					objDBReader.Dispose();
			}
		}
	}

	/// <summary>
	/// Summary description for RenewalInfo.
	/// </summary>
	public class RenewalInformation
	{
		public RenewalInformation()
		{
		}

		/// <summary>
		/// Stores the RenewalInfoRowId value
		/// </summary>
		private int m_iRenewalInfoRowId = 0;

		/// <summary>
		/// Stores the LOB value
		/// </summary>
		private int m_iLOB = 0;

		/// <summary>
		/// Stores the State value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the Amount value
		/// </summary>
		private double m_dAmount = 0.0;

		/// <summary>
		/// Stores the TermCode value
		/// </summary>
		private int m_iTermCode = 0;

		/// <summary>
		/// Stores the EffectiveDate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the ExpirationDate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

		/// <summary>
		/// Read/Write property for RenewalInfoRowId
		/// </summary>
		internal int RenewalInfoRowId
		{
			get
			{
				return m_iRenewalInfoRowId;
			}
			set
			{
				m_iRenewalInfoRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for LOB
		/// </summary>
		internal int LOB
		{
			get
			{
				return m_iLOB;
			}
			set
			{
				m_iLOB = value;
			}
		}

		/// <summary>
		/// Read/Write property for State
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for Amount
		/// </summary>
		internal double Amount
		{
			get
			{
				return m_dAmount;
			}
			set
			{
				m_dAmount = value;
			}
		}

		/// <summary>
		/// Read/Write property for TermCode
		/// </summary>
		internal int TermCode
		{
			get
			{
				return m_iTermCode;
			}
			set
			{
				m_iTermCode = value;
			}
		}

		/// <summary>
		/// Read/Write property for EffectiveDate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpirationDate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
	}
}
