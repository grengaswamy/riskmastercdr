using System;
using System.Xml;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21 Mar 2005
	///Purpose :   This class Fetches & saves the CPCredentials data.
	/// </summary>
	public class CPCredentials
	{
		/// <summary>
		/// URL for CP
		/// </summary>
		private string m_sCPUrl = "";

		/// <summary>
		/// UserID
		/// </summary>
		private string m_sUserId = "";

		/// <summary>
		/// Password
		/// </summary>
		private string m_sPassword = "";

		/// <summary>
		/// Default Constructor
		/// </summary>
		public CPCredentials(){}

		#region "Properties"
		/// <summary>
		/// CP URL
		/// </summary>
		internal string CPUrl
		{
			get
			{
				return m_sCPUrl;
			}
		}

		/// <summary>
		/// UserID
		/// </summary>
		internal string UserID
		{
			get
			{
				return m_sUserId;
			}
		}

		/// <summary>
		/// Password
		/// </summary>
		internal string Password
		{
			get
			{
				return m_sPassword;
			}
		}
		#endregion

		/// <summary>
		/// Returns the form data XML
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>XML Doc</returns>
		public XmlDocument Get(XmlDocument p_objXMLDocument)
		{			
			return p_objXMLDocument;
		}

		/// <summary>
		/// Sets the properties from the XML passed
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		public void Save(XmlDocument p_objXMLDocument)
		{
			m_sCPUrl = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='CPUrl']")).InnerText;
			m_sUserId = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='UserID']")).InnerText;
			m_sPassword = ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='Password']")).InnerText;
		}
	}
}
