﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.ExceptionTypes;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;

namespace Riskmaster.Application.RMUtilities
{
    public class HistoryMetaData
    {
        private int m_iClientId = 0;
         private string m_sConnectionString = "";
         public HistoryMetaData(string p_sConnectionstring, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_sConnectionString = p_sConnectionstring;           
        }
         public bool GetColumnsInfo(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut)
         {
             XmlNode objXmlNode = null;
             XmlNode objRootNode = null;
             XmlAttribute objXMLAtt = null;
             string sSelectedName = string.Empty;
             string sSelectedTableId = "0";
             string sSql = string.Empty;
             string sTableName = string.Empty;
             string sColumnName = string.Empty;
             
             try
             {


                 objXmlNode = p_objXmlOut.CreateElement("Document");
                 p_objXmlOut.AppendChild(objXmlNode);

                 objRootNode = p_objXmlOut.CreateElement("Root");

                 objXmlNode = p_objXmlIn.SelectSingleNode("//SelectedTableId");
                 if (objXmlNode != null && !string.IsNullOrEmpty(objXmlNode.InnerText))
                 {
                     sSelectedTableId = objXmlNode.InnerText;
                     sSql=string.Format(@"SELECT HD.COLUMN_NAME, HD.COLUMN_ID,HD.USER_PROMPT,HD.RMDATA_TYPE,HD.SYSTEM_TABLE_NAME 
                                       FROM HIST_TRACK_DICTIONARY HD INNER JOIN HIST_TRACK_TABLES HT 
                                       ON HT.TABLE_ID=HD.TABLE_ID WHERE HT.TABLE_NAME = {0} 
                                       ORDER BY HD.USER_PROMPT", Utilities.FormatSqlFieldValue(sSelectedTableId));

                     using (DbReader objDBReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                     {
                         while (objDBReader.Read())
                         {

                             objXmlNode = p_objXmlOut.CreateElement("Column");

                             objXMLAtt = p_objXmlOut.CreateAttribute("Id");
                             objXMLAtt.Value = objDBReader.GetInt("COLUMN_ID").ToString();
                             objXmlNode.Attributes.Append(objXMLAtt);

                             objXMLAtt = p_objXmlOut.CreateAttribute("ColumnName");
                             objXMLAtt.Value = objDBReader.GetString("COLUMN_NAME").ToString();
                             objXmlNode.Attributes.Append(objXMLAtt);

                             objXMLAtt = p_objXmlOut.CreateAttribute("UserPrompt");
                             objXMLAtt.Value = objDBReader.GetString("USER_PROMPT");
                             objXmlNode.Attributes.Append(objXMLAtt);

                             objXMLAtt = p_objXmlOut.CreateAttribute("RMDataType");
                             objXMLAtt.Value = objDBReader.GetInt("RMDATA_TYPE").ToString();
                             objXmlNode.Attributes.Append(objXMLAtt);

                             objXMLAtt = p_objXmlOut.CreateAttribute("SystemTableName");
                             objXMLAtt.Value = objDBReader.GetString("SYSTEM_TABLE_NAME").ToString();
                             objXmlNode.Attributes.Append(objXMLAtt);

                             objRootNode.AppendChild(objXmlNode);
                            
                         }
                     }
                 }
                 p_objXmlOut.FirstChild.AppendChild(objRootNode);                
                
             }
             catch (Exception p_objEx)
             {
                 throw new RMAppException(Globalization.GetString("HistoryMetaData.GetColumnsInfo.Error", m_iClientId), p_objEx);
             }
             return true;
         }

         public XmlDocument GetTables(XmlDocument p_objXmlDocument)
         {
             XmlDocument objXmlResponse = null;
             XmlNode objXmlNode = null;
             XmlNode objRootNode = null;
            
             XmlAttribute objXMLAtt = null;

             
             string sSql = string.Empty;

             String sSQLForColumns = String.Empty;
             String sTableId = String.Empty;               
            
             try
             {
                 objXmlResponse = new XmlDocument();
                 objXmlNode = objXmlResponse.CreateElement("Document");
                 objXmlResponse.AppendChild(objXmlNode);

                 objRootNode = objXmlResponse.CreateElement("TableList");

                 objXmlNode = objXmlResponse.CreateElement("Table");
                 objXMLAtt = objXmlResponse.CreateAttribute("Id");                
                 objXMLAtt.Value = "0";
                 objXmlNode.Attributes.Append(objXMLAtt);
                 objXMLAtt = objXmlResponse.CreateAttribute("UserPrompt");
                 objXMLAtt.Value = "";
                 objXmlNode.Attributes.Append(objXMLAtt);
                 objRootNode.AppendChild(objXmlNode);

                 sSql=string.Format("SELECT TABLE_ID,TABLE_NAME FROM HIST_TRACK_TABLES WHERE TABLE_NAME NOT LIKE '%_SUPP' ORDER BY USER_PROMPT");
                 using (DbReader objDBReaderForTables = DbFactory.GetDbReader(m_sConnectionString, sSql))
                 {
                     while (objDBReaderForTables.Read())
                     {
                         objXmlNode = objXmlResponse.CreateElement("Table");

                         objXMLAtt = objXmlResponse.CreateAttribute("Id");
                         sTableId = objDBReaderForTables.GetInt32("TABLE_ID").ToString();
                         objXMLAtt.Value = sTableId;
                         objXmlNode.Attributes.Append(objXMLAtt);

                         objXMLAtt = objXmlResponse.CreateAttribute("UserPrompt");
                         objXMLAtt.Value = objDBReaderForTables.GetString("TABLE_NAME");
                         objXmlNode.Attributes.Append(objXMLAtt);
                         
                         objRootNode.AppendChild(objXmlNode);
                     }
                 }
                 objXmlResponse.FirstChild.AppendChild(objRootNode);
                 
                 return objXmlResponse;

             }
             catch (Exception p_objEx)
             {
                 throw new RMAppException(Globalization.GetString("HistoryMetaData.GetTables.Error", m_iClientId), p_objEx);

             }
             finally
             {

             }
         }
         public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut)
         {
             XmlNode objNode = null;
             string sSql = string.Empty;    

            try
             {  
                 objNode = p_objXmlIn.SelectSingleNode("//Columns");
                 using (DbConnection objDb =DbFactory.GetDbConnection(m_sConnectionString))
                 {
                     objDb.Open();

                     foreach (XmlNode objColumn in objNode)
                     {
                         sSql = string.Format(@"UPDATE HIST_TRACK_DICTIONARY SET USER_PROMPT={0},RMDATA_TYPE={1},
                                              SYSTEM_TABLE_NAME={2} WHERE COLUMN_ID={3}", 
                                              Utilities.FormatSqlFieldValue(objColumn.Attributes["UserPrompt"].Value), 
                                              objColumn.Attributes["RMDataType"].Value, 
                                              Utilities.FormatSqlFieldValue(objColumn.Attributes["SystemTableName"].Value), 
                                              objColumn.Attributes["Id"].Value);
                         objDb.ExecuteNonQuery(sSql);                         
                     }
                     objDb.Close();
                 }                 
             }
             catch (Exception p_objEx)
             {
                 throw new RMAppException(Globalization.GetString("HistoryMetaData.Save.Error", m_iClientId), p_objEx);
             }
             return true;
         }
    }
}
