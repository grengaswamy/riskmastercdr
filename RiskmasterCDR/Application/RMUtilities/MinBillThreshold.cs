﻿using System;
using System.Xml; 
using Riskmaster.Db;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21 Mar 2005
	///Purpose :   This class Creates, Edits, Fetches Minimum threshold List.
	/// </summary>
	public class MinBillThreshold:UtilitiesUIBase 
	{
		private string m_sUserName = "";

        UserLogin m_oUserlogin = null;//vkumar258 ML Changes
		/// <summary>
		/// String array for database fields mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "THRESHOLD_ROWID"},
			{"AddedByUser", "ADDED_BY_USER"},
			{"DttmRcdAdded", "DTTM_RCD_ADDED"},
			{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
			{"UpdatedByUser", "UPDATED_BY_USER"},
			{"UseThreshold", "IN_USE_FLAG"},
			{"LOBcode", "LINE_OF_BUSINESS"},
			{"Statecode", "STATE"},
			{"Amount", "AMOUNT"},
			{"EffectiveDate", "EFFECTIVE_DATE"},
			{"ExpirationDate", "EXPIRATION_DATE"}
									  };

		private string m_sConnectionString = "";
		private XmlDocument m_objXMLDocument = null;
        private int m_iClientId = 0;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public MinBillThreshold(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
			m_sConnectionString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public MinBillThreshold(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			m_sUserName = p_sLoginName;
			ConnectString = p_sConnectionString;
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

        public MinBillThreshold(UserLogin p_oUserLogin,  string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_oUserLogin, p_sConnectionString, p_iClientId)//vkumar258 ML changes
        {
            m_oUserlogin = p_oUserLogin;
            m_sUserName = p_sLoginName;
            ConnectString = p_sConnectionString;
            m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

		/// <summary>
		/// Initialize
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_POL_BILL_THRSH";
			KeyField = "THRESHOLD_ROWID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// <summary>
		/// Fetches the form data for Minimum Bill Threshold
		/// </summary>
		/// <param name="p_objXmlDocument">XML form structure</param>
		/// <returns>XMl Doc with data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XMLDoc = p_objXmlDocument;
			try
			{
				base.Get();
				return XMLDoc; 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("MinBillThreshold.Get.Err", m_iClientId), p_objEx); 
			}
		}

		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				m_objXMLDocument = p_objXmlDocument;
				if(ValidateXMLData()) 
				{
					FillXML();
					base.Save();
				}
				return XMLDoc;
			}
			catch(RMAppException p_objExp)
			{
				throw p_objExp; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("MinBillThreshold.Save.Err", m_iClientId), p_objEx); 
			}
		}
		
		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes Min Bill Threshold record
		/// </summary>
		/// <param name="p_iThresholdRowId">Min Bill Threshold ID</param>
		/// <returns>True for sucess and false for failure</returns>
		internal bool Delete(int p_iThresholdRowId)
		{
			string sSQL = "";
			DbConnection objCon = null;

            try
            {
                if (p_iThresholdRowId != 0)
                {
                    sSQL = "DELETE FROM SYS_POL_BILL_THRSH WHERE THRESHOLD_ROWID = " + p_iThresholdRowId;
                    objCon = DbFactory.GetDbConnection(m_sConnectionString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);

                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("MinBillThreshold.Delete.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                }
            }
		}
	
		private void FillXML()
		{
			string sThresholdRowId = "";
			XmlNodeList objNodeList = null;
			string sNodename = string.Empty;

            // npadhy When the Minimum Billed Threshold is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB

            //Start:Sumit(10/27/2010)- MITS# 22841 - Commented as AddedByUser and DateTimeAdded comes in Input XML so there is no need to fetch the same from DB.

            //string sSQL = string.Empty;
            //DbReader objDbReader = null;
            //string sAddedByUser = string.Empty;
            //string sDateTimeAdded = string.Empty;
            // npadhy When the Minimum Billed Threshold is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB

			sThresholdRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;

            //if (sThresholdRowId != "")
            //{
            //    sSQL = "SELECT ADDED_BY_USER, DTTM_RCD_ADDED FROM SYS_POL_RNWL_INFO WHERE RENEWAL_INFO_ROWID=" + sThresholdRowId;
            //    objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
            //    if (objDbReader != null)
            //    {
            //        if (objDbReader.Read())
            //        {
            //            sAddedByUser = objDbReader["ADDED_BY_USER"].ToString();
            //            sDateTimeAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
            //        }
            //    }
            //    objDbReader.Dispose();
            //}

			objNodeList=m_objXMLDocument.GetElementsByTagName("control");

			foreach(XmlElement objXMLElement in objNodeList)
			{
				sNodename = objXMLElement.GetAttribute("name");  
				switch(sNodename)
				{
					case "AddedByUser":
						if(sThresholdRowId == "")
							objXMLElement.InnerText = m_sUserName;
                        // npadhy When the Minimum Billed Threshold is Updated the Date Time Added and Added by User is not passed.
                        //// Now it is getting passed as the same Value as in DB
                        //else
                        //    objXMLElement.InnerText = sAddedByUser;
                        // npadhy When the Minimum Billed Threshold is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
						break;
					case "DttmRcdAdded":
						if(sThresholdRowId == "")
                            objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
                        // npadhy When the Minimum Billed Threshold is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
                        //else
                        //    objXMLElement.InnerText = sDateTimeAdded;
                        // npadhy When the Minimum Billed Threshold is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
                        //End:Sumit
						break;
					case "DttmRcdLastUpd":
                        objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
						break;
					case "UpdatedByUser":
						objXMLElement.InnerText = m_sUserName; 
						break;
				}
			}
            objNodeList = null;
		}

		private bool ValidateXMLData()
		{
			string sLOBCode = "";
			string sStateCode = "";
			string sEffectiveDate = "";
			string sExpirationDate = "";
			string sSQL = "";
			string sThresholdRowId = "";
			DbReader objDBReader = null;
			DbReader objTempDBReader = null;
			bool bIsReaderHasRows = false;

			try
			{
				sThresholdRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;
				sLOBCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='LOBcode']")).GetAttribute("codeid");
				sStateCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='Statecode']")).GetAttribute("codeid");
				sEffectiveDate = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='EffectiveDate']")).InnerText;
				sExpirationDate = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='ExpirationDate']")).InnerText;

				if(sExpirationDate == "")
				{
					sSQL = "SELECT THRESHOLD_ROWID FROM SYS_POL_BILL_THRSH WHERE LINE_OF_BUSINESS = " + 
						sLOBCode + " AND STATE = " + sStateCode + " AND EXPIRATION_DATE IS NULL ";
				
					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

					while(objDBReader.Read())
					{
						bIsReaderHasRows = true;

						if(objDBReader["THRESHOLD_ROWID"].ToString() != sThresholdRowId)
                            throw new RMAppException(Globalization.GetString("MinBillThreshold.ValidateXMLData.AlreadyExists", m_iClientId));
						else
						{
							sSQL = "SELECT COUNT(THRESHOLD_ROWID) FROM SYS_POL_BILL_THRSH WHERE" + 
								" LINE_OF_BUSINESS = " + sLOBCode + " AND STATE = " + sStateCode + 
								" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'";

							objTempDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
							if(objTempDBReader.Read())
							{
								if(Conversion.ConvertObjToInt(objTempDBReader.GetValue(0), m_iClientId) > 0)
                                    throw new RMAppException(Globalization.GetString("MinBillThreshold.ValidateXMLData.AlreadyExistsWithDate", m_iClientId));
							}
							objTempDBReader.Close();
						}
					}
					objDBReader.Close();

					if(bIsReaderHasRows == false)
					{
						sSQL = "SELECT THRESHOLD_ROWID FROM SYS_POL_BILL_THRSH WHERE" + 
							" LINE_OF_BUSINESS = " + sLOBCode + " AND STATE = " + sStateCode + 
							" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'";

						objTempDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
						while(objTempDBReader.Read())
						{
							if(objTempDBReader["THRESHOLD_ROWID"].ToString() != sThresholdRowId)
                                throw new RMAppException(Globalization.GetString("MinBillThreshold.ValidateXMLData.AlreadyExistsWithDate", m_iClientId));
						}
						objTempDBReader.Close();
					}
				}
				else
				{
					sSQL = "SELECT THRESHOLD_ROWID, EFFECTIVE_DATE FROM SYS_POL_BILL_THRSH WHERE LINE_OF_BUSINESS = " + 
						sLOBCode + " AND STATE = " + sStateCode + " AND EXPIRATION_DATE IS NULL ";
				
					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

					while(objDBReader.Read())
					{
						bIsReaderHasRows = true;

						if(objDBReader["THRESHOLD_ROWID"].ToString() != sThresholdRowId)
						{
                            //Start:Sumit(09/28/2010)-MITS# 21804
                            //if(Conversion.ToDate(sEffectiveDate) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim()))
                            if ((Conversion.ToDate(Conversion.GetDate(sEffectiveDate)) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim())) || (Conversion.ToDate(Conversion.GetDate(sExpirationDate)) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim())))
                                //End:Sumit
                                throw new RMAppException(Globalization.GetString("MinBillThreshold.ValidateXMLData.AlreadyExistsWithDate", m_iClientId));
							else
							{
								sSQL = "SELECT THRESHOLD_ROWID FROM SYS_POL_BILL_THRSH WHERE" + 
									" LINE_OF_BUSINESS = " + sLOBCode + " AND STATE = " + sStateCode + 
									" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'" + 
									" AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

								objTempDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
								while(objTempDBReader.Read())
								{
									if(objTempDBReader["THRESHOLD_ROWID"].ToString() != sThresholdRowId)
                                        throw new RMAppException(Globalization.GetString("MinBillThreshold.ValidateXMLData.AlreadyExistsWithDate", m_iClientId));
								}
								objTempDBReader.Close();
							}
						}
						else
						{
                            // npadhy Start MITS 20714 The Dates were not getting Updated in The Minimuk Bill Threshold as 
                            // The SQL Query written was fething the Count, but later was trying to access THRESHOLD_ROWID
							//sSQL = "SELECT COUNT(THRESHOLD_ROWID) FROM SYS_POL_BILL_THRSH WHERE" + 
                            sSQL = "SELECT THRESHOLD_ROWID FROM SYS_POL_BILL_THRSH WHERE" +
                            // npadhy End MITS 20714 The Dates were not getting Updated in The Minimuk Bill Threshold as 
                            // The SQL Query written was fething the Count, but later was trying to access THRESHOLD_ROWID
								" LINE_OF_BUSINESS = " + sLOBCode + " AND STATE = " + sStateCode + 
								" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'" + 
								" AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

							objTempDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
							while(objTempDBReader.Read())
							{
								if(objTempDBReader["THRESHOLD_ROWID"].ToString() != sThresholdRowId)
                                    throw new RMAppException(Globalization.GetString("MinBillThreshold.ValidateXMLData.AlreadyExistsWithDate", m_iClientId));
							}
							objTempDBReader.Close();
						}
					}
					objDBReader.Close();
 
					if(bIsReaderHasRows == false)
					{
						sSQL = "SELECT THRESHOLD_ROWID FROM SYS_POL_BILL_THRSH WHERE" + 
							" LINE_OF_BUSINESS = " + sLOBCode + " AND STATE = " + sStateCode + 
							" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'" + 
							" AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

						objTempDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
						while(objTempDBReader.Read())
						{
							if(objTempDBReader["THRESHOLD_ROWID"].ToString() != sThresholdRowId)
                                throw new RMAppException(Globalization.GetString("MinBillThreshold.ValidateXMLData.AlreadyExistsWithDate", m_iClientId));
						}
						objTempDBReader.Close();
					}
				}
				return true;
			}
			finally
			{
				if(objDBReader != null)
					objDBReader.Dispose();
				if(objTempDBReader != null)
					objTempDBReader.Dispose();
			}
		}
	}

	/// <summary>
	/// Summary description for Threshold.
	/// </summary>
	public class Threshold
	{
		public Threshold()
		{
		}

		/// <summary>
		/// Stores the ThresholdRowId value
		/// </summary>
		private int m_iThresholdRowId = 0;

		/// <summary>
		/// Stores the Use value
		/// </summary>
		private bool m_bUse = false;

		/// <summary>
		/// Stores the LOB value
		/// </summary>
		private int m_iLOB = 0;

		/// <summary>
		/// Stores the State value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the Amount value
		/// </summary>
		private double m_dAmount = 0.0;

		/// <summary>
		/// Stores the EffectiveDate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the ExpirationDate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

		/// <summary>
		/// Read/Write property for ThresholdRowId
		/// </summary>
		internal int ThresholdRowId
		{
			get
			{
				return m_iThresholdRowId;
			}
			set
			{
				m_iThresholdRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for Use
		/// </summary>
		internal bool Use
		{
			get
			{
				return m_bUse;
			}
			set
			{
				m_bUse = value;
			}
		}

		/// <summary>
		/// Read/Write property for LOB
		/// </summary>
		internal int LOB
		{
			get
			{
				return m_iLOB;
			}
			set
			{
				m_iLOB = value;
			}
		}

		/// <summary>
		/// Read/Write property for State
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for Amount
		/// </summary>
		internal double Amount
		{
			get
			{
				return m_dAmount;
			}
			set
			{
				m_dAmount = value;
			}
		}

		/// <summary>
		/// Read/Write property for EffectiveDate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpirationDate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
	}
}
