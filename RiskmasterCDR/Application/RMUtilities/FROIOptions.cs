﻿
using System;
using System.Xml;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.Text;
using System.Collections.Generic;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	/// Author  :   Mihika Agrawal
	/// Dated   :   09/28/2005
	/// Purpose :   Contains methods to get and set various FROI/Juris Options
	/// </summary>
	public class FROIOptions
	{
		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable to store whether FROI or Juris Options is called
		/// </summary>
		private string m_sFormName = "";

        private int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
		public FROIOptions(string p_sDsnName , string p_sUserName , string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
			this.Initialize();
		}
		#endregion

		#region Public Method
		/// Name		: GetFROIOptions
		/// Author		: Mihika Agrawal
		/// Date Created: 09/28/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the FROI Options data and generates an XML
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetFROIOptions(XmlDocument p_objXmlDoc)
		{
			DbReader objReader = null;
			LocalCache objCache = null;
            //Entity objEntity = null;

			XmlElement objParentElm = null;
			XmlElement objRowElm = null;
			XmlElement objChildElm = null;

			string sSQL = string.Empty;
			int iJurisRowId = 0;
			int iSelectedEID = 0;
            int iRecCount = 0;
            int iFilterJurisID = 0;
            int iFilterEntity=0;
           
			try
			{
				objParentElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FROI");
				if (objParentElm != null)
					m_sFormName = "FROI";
				else
					m_sFormName = "JURIS";
                objChildElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelOrgHier");
                iFilterEntity = Conversion.ConvertStrToInteger(objChildElm.GetAttribute("codeid"));
                if(iFilterEntity==0)
                    iFilterEntity=-1; 
                objChildElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//JurisState");
                iFilterJurisID = Conversion.ConvertStrToInteger(objChildElm.GetAttribute("codeid"));
                if(iFilterJurisID==0)
                    iFilterJurisID=-1;
                if (iFilterEntity == -1 && iFilterJurisID == -1)
                {
                    sSQL = "SELECT count(JURIS_ROW_ID) FROM " + m_sFormName + "_OPTIONS";
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    if (objReader.Read())
                        iRecCount = objReader.GetInt16(0);
                    objReader.Close();
                    if (iRecCount <= 100)
                    {
                        sSQL = "SELECT JURIS_ROW_ID,SELECTED_EID,LAST_NAME,ADDR1,ENTITY_TABLE_ID FROM " + m_sFormName + "_OPTIONS LEFT OUTER JOIN ENTITY on " + m_sFormName + "_OPTIONS.SELECTED_EID=ENTITY.ENTITY_ID";
                    }
                    else
                    {
                        sSQL = "SELECT JURIS_ROW_ID,SELECTED_EID,LAST_NAME,ADDR1,ENTITY_TABLE_ID FROM " + m_sFormName + "_OPTIONS LEFT OUTER JOIN ENTITY on " + m_sFormName + "_OPTIONS.SELECTED_EID=ENTITY.ENTITY_ID WHERE JURIS_ROW_ID=" + iFilterJurisID + " AND SELECTED_EID=" + iFilterEntity;
                    }
                }
                else
                {
                    if (iFilterEntity == -1)
                    {
                        sSQL = "SELECT JURIS_ROW_ID,SELECTED_EID,LAST_NAME,ADDR1,ENTITY_TABLE_ID FROM " + m_sFormName + "_OPTIONS LEFT OUTER JOIN ENTITY on " + m_sFormName + "_OPTIONS.SELECTED_EID=ENTITY.ENTITY_ID WHERE JURIS_ROW_ID=" + iFilterJurisID;
                    }
                    else if (iFilterJurisID == -1)
                    {
                        sSQL = "SELECT JURIS_ROW_ID,SELECTED_EID,LAST_NAME,ADDR1,ENTITY_TABLE_ID FROM " + m_sFormName + "_OPTIONS LEFT OUTER JOIN ENTITY on " + m_sFormName + "_OPTIONS.SELECTED_EID=ENTITY.ENTITY_ID WHERE SELECTED_EID=" + iFilterEntity;
                    }
                    else 
                    {
                        sSQL = "SELECT JURIS_ROW_ID,SELECTED_EID,LAST_NAME,ADDR1,ENTITY_TABLE_ID FROM " + m_sFormName + "_OPTIONS LEFT OUTER JOIN ENTITY on " + m_sFormName + "_OPTIONS.SELECTED_EID=ENTITY.ENTITY_ID WHERE JURIS_ROW_ID=" + iFilterJurisID + " AND SELECTED_EID=" + iFilterEntity;
                    }
                }
                objChildElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//RecordCount");
                objChildElm.InnerText = iRecCount.ToString();
                objChildElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//GetValueForKey");
                    //if (objChildElm.InnerText == "" && objChildElm != null)
                    {
                        objParentElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//OptionsList");

                        objCache = new LocalCache(m_sConnectionString, m_iClientId);
                        //Changed for MITS 19158
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        //objEntity = (DataModel.Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                        while (objReader.Read())
                        {
                            iJurisRowId = objReader.GetInt32("JURIS_ROW_ID");
                            iSelectedEID = objReader.GetInt32("SELECTED_EID");

                            //if (iSelectedEID > 0)
                            //{
                            //    objEntity.MoveTo(iSelectedEID);
                            //}
                            objRowElm = p_objXmlDoc.CreateElement("listrow");

                            // Jurisdiction
                            objChildElm = objRowElm.OwnerDocument.CreateElement("Jurisdiction");
                            if (iJurisRowId < 1)
                                objChildElm.InnerText = "<>Default (Jurisdictions Without Settings)";
                            else
                            {
                                string sAbbreviation = string.Empty;
                                string sName = string.Empty;
                                objCache.GetStateInfo(iJurisRowId, ref sAbbreviation, ref sName);
                                //sgoel6 07/29/2009
                                //objChildElm.InnerText = sName.Substring(0, 1).ToUpper() + sName.Substring(1).ToLower();
                                if (sName != "")
                                    objChildElm.InnerText = sName.Substring(0, 1).ToUpper() + sName.Substring(1).ToLower();
                                else
                                    objChildElm.InnerText = sName;
                            }
                            objRowElm.AppendChild(objChildElm);

                            // Entity
                            objChildElm = objRowElm.OwnerDocument.CreateElement("Entity");
                            if (iSelectedEID > 0)
                                objChildElm.InnerText = objReader.GetString("LAST_NAME") + "; " + objReader.GetString("ADDR1");
                            else
                                objChildElm.InnerText = "<>Default (Entities Without Settings)";
                            objRowElm.AppendChild(objChildElm);

                            // Organization - Level
                            objChildElm = objRowElm.OwnerDocument.CreateElement("Org-Level");
                            if (iSelectedEID > 0)
                                objChildElm.InnerText = objCache.GetTableName(objReader.GetInt32("ENTITY_TABLE_ID"));
                            else
                                objChildElm.InnerText = "ALL";
                            objRowElm.AppendChild(objChildElm);

                            // key
                            objChildElm = objRowElm.OwnerDocument.CreateElement("Key");
                            objChildElm.InnerText = "k" + iJurisRowId.ToString() + ";" + iSelectedEID.ToString();
                            objRowElm.AppendChild(objChildElm);

                            objParentElm.AppendChild(objRowElm);
                            //MGaba2: MITS 19613:Start                            
                                objChildElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//GetValueForKey");
                                if (objChildElm != null && objChildElm.InnerText == "")
                                {
                                    objChildElm.InnerText = "k" + iJurisRowId.ToString() + ";" + iSelectedEID.ToString();
                                }                               
                            //MGaba2: MITS 19613:End
                        }
                    }
                
                    // Call the method 'PopulateOptions' to populate rest of the entries for the selected record.
				p_objXmlDoc = PopulateOptions(p_objXmlDoc);	
			
				// XML for FROI Forms
				objParentElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FROIFormsList");
				if (objParentElm != null)
				{
					objRowElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FROIFormsList/listrow");
					if (objRowElm == null)
					{
						sSQL = "SELECT JURIS_FORMS.FORM_ID, STATES.STATE_NAME, JURIS_FORMS.FORM_NAME "
							+ "FROM JURIS_FORMS, STATES " 
							+ "WHERE JURIS_FORMS.STATE_ROW_ID = STATES.STATE_ROW_ID "
							+ "ORDER BY STATES.STATE_NAME,JURIS_FORMS.FORM_NAME";

						if (objReader != null)
							objReader.Close();
						objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
						while (objReader.Read())
						{
							objRowElm = p_objXmlDoc.CreateElement("listrow");

							// Form Name
							objChildElm = objRowElm.OwnerDocument.CreateElement("FormName");
							objChildElm.InnerText = objReader.GetString("STATE_NAME") + " - " + objReader.GetString("FORM_NAME");
							objRowElm.AppendChild(objChildElm);

                            // Form ID
                            objChildElm = objRowElm.OwnerDocument.CreateElement("FormID");
                            objChildElm.InnerText = objReader.GetValue("FORM_ID").ToString();
                            objRowElm.AppendChild(objChildElm);


							objParentElm.AppendChild(objRowElm);
						}
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FROIOptions.GetFROIOptions.Error", m_iClientId) , p_objEx);				
			}
			finally
			{
				if (objCache != null)
				{
					objCache.Dispose();
				}
				if (objReader != null)
				{
					objReader.Close();
				}
                //if (objEntity != null)
                //{
                //    objEntity.Dispose();
                //}
				objChildElm = null;
				objRowElm = null;
				objParentElm = null;
                

			}
			return p_objXmlDoc;
		}

		/// Name		: Save
		/// Author		: Mihika Agrawal
		/// Date Created: 10/04/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data for FROI Options
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data</param>
		/// <returns>Returns the saved xml data</returns>
		public XmlDocument Save(XmlDocument p_objXmlDoc)
		{
			DbReader objReader = null;
			DbConnection objConn = null;
			DbCommand objCommand=null;

			XmlElement objParentElm = null;

			string sKey = string.Empty;

			#region Variable Declaration
			string sPrintDesc = string.Empty;
			
			string sClaimAdmin = string.Empty;
			string sClmAdmOrgLvl = string.Empty;
			string sDefClmAdm = string.Empty;
			string sClmAdmTPA = string.Empty;

			string sCarrier = string.Empty;
			string sCarOrgLvl = string.Empty;
			string sDefCarr = string.Empty;

			string sConOpt = string.Empty;
			string sUseTitle = string.Empty;
			string sPreparer = string.Empty;
			string sAttachToClm = string.Empty;
			string sPrintClmTop = string.Empty;
			string sNCCI = string.Empty;
			string sEmployer = string.Empty;
			string sWorkLoss = string.Empty;

			string sNE = string.Empty;
			string sNEOrgHier = string.Empty;
			string sNEDefID = string.Empty;

			string sTN = string.Empty;
			string sTNOrgHier = string.Empty;
			string sTNDefID = string.Empty;
			
			string sVA = string.Empty;
			string sVAOrgHier = string.Empty;
			string sVADefID = string.Empty;

			string sTPA = string.Empty;

			string sSelectedEID = string.Empty;
			string sJurisRowID = string.Empty;

			string sLastUpdateDateTime = string.Empty;
			string sUpdateUser = string.Empty;

			string sBaseLevel = string.Empty;
			string sAcordCopies = string.Empty;
			string sPrintAcordForms = string.Empty;
			string sSuspresDefTime = string.Empty;
			string sPerNotOpt = string.Empty;
			string sCarrClmNumOpt = string.Empty;
			string sCheckStatusOpt = string.Empty;
            //start by Shivendu for MITS 11367
            string sTpaUseOpt = string.Empty;
            string sFroiTpaEid = string.Empty;
            //end by Shivendu for MITS 11367
            LocalCache objCache = null;     //Rijul 340
			#endregion

			string sSql = string.Empty;

			try
			{
                objCache = new LocalCache(m_sConnectionString, m_iClientId);     //Rijul 340
				objParentElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FROI");
				if (objParentElm != null)
					m_sFormName = "FROI";
				else
					m_sFormName = "JURIS";

				sKey = p_objXmlDoc.SelectSingleNode("//GetValueForKey").InnerText;
				if (sKey != "")
				{
					sKey = sKey.Substring(1);
					sJurisRowID = sKey.Substring(0, sKey.IndexOf(";"));
					sSelectedEID = sKey.Substring(sKey.IndexOf(";") + 1);

					#region Assignment of Values
					// OSHA Description
					if (p_objXmlDoc.SelectSingleNode("//Description") != null)
						sPrintDesc = p_objXmlDoc.SelectSingleNode("//Description").InnerText;

					// Claim Administrator
					if (p_objXmlDoc.SelectSingleNode("//ClaimAdministrator") != null)
					{
						sClaimAdmin = p_objXmlDoc.SelectSingleNode("//ClaimAdministrator").InnerText;
						if (sClaimAdmin == "2")
							sClmAdmOrgLvl = p_objXmlDoc.SelectSingleNode("//ClAdmOrgLvl").InnerText;
						else
							sClmAdmOrgLvl = "0";
						if (sClaimAdmin == "4")
							sDefClmAdm = p_objXmlDoc.SelectSingleNode("//ClAdmDefEID").Attributes.GetNamedItem("codeid").InnerText;
						else
							sDefClmAdm = "0";
						if (sClaimAdmin == "5")
							sClmAdmTPA = p_objXmlDoc.SelectSingleNode("//ClAdmTPAEID").Attributes.GetNamedItem("codeid").InnerText;
						else
							sClmAdmTPA = "0";
					}

					// Raman Bhatia: RMX Phase 2 Juris Options Changes for EDI
					
					if (p_objXmlDoc.SelectSingleNode("//CarrClmNumOpt") != null)
					{
						sCarrClmNumOpt = p_objXmlDoc.SelectSingleNode("//CarrClmNumOpt").InnerText;
					}

					// Raman Bhatia: RMX Phase 2 Juris Options Changes for EDI
					
					if (p_objXmlDoc.SelectSingleNode("//CheckStatusOpt") != null)
					{
						sCheckStatusOpt = p_objXmlDoc.SelectSingleNode("//CheckStatusOpt").InnerText;
					}

					// Carrier
					if (p_objXmlDoc.SelectSingleNode("//Carrier") != null)
					{
						sCarrier = p_objXmlDoc.SelectSingleNode("//Carrier").InnerText;
						if (sCarrier == "1")
							sCarOrgLvl = p_objXmlDoc.SelectSingleNode("//CarrierOrgLevel").InnerText;
						else
							sCarOrgLvl = "0";
						if (sCarrier == "2")
							sDefCarr = p_objXmlDoc.SelectSingleNode("//CarrierDefEID").Attributes.GetNamedItem("codeid").InnerText;
						else
							sDefCarr = "0";
					}

					// Contact Information
					if (p_objXmlDoc.SelectSingleNode("//ContactInfo") != null)
						sConOpt = p_objXmlDoc.SelectSingleNode("//ContactInfo").InnerText;
					// Use Title
					if (p_objXmlDoc.SelectSingleNode("//UseTitle") != null)
						sUseTitle = p_objXmlDoc.SelectSingleNode("//UseTitle").InnerText;
					// FROI Preparer
					if (p_objXmlDoc.SelectSingleNode("//PreparerInfo") != null)
						sPreparer = p_objXmlDoc.SelectSingleNode("//PreparerInfo").InnerText;
					// Attach To Claim
					if (p_objXmlDoc.SelectSingleNode("//AttachToClaim") != null)
						sAttachToClm = p_objXmlDoc.SelectSingleNode("//AttachToClaim").InnerText;
					// Print Claim Number at top
					if (p_objXmlDoc.SelectSingleNode("//PrintClaimNum") != null)
						sPrintClmTop = p_objXmlDoc.SelectSingleNode("//PrintClaimNum").InnerText;
					// Print NCCI Nodes
					if (p_objXmlDoc.SelectSingleNode("//PrintNCCI") != null)
						sNCCI = p_objXmlDoc.SelectSingleNode("//PrintNCCI").InnerText;
					// Employer Level
					if (p_objXmlDoc.SelectSingleNode("//EmployerLevel") != null)
						sEmployer = p_objXmlDoc.SelectSingleNode("//EmployerLevel").InnerText;
					// Workloss
					if (p_objXmlDoc.SelectSingleNode("//WorkLoss") != null)
						sWorkLoss = p_objXmlDoc.SelectSingleNode("//WorkLoss").InnerText;

					// Nebraska
					if (p_objXmlDoc.SelectSingleNode("//NEParentOption") != null)
					{
						sNE = p_objXmlDoc.SelectSingleNode("//NEParentOption").InnerText;
						if (sNE == "1")
							sNEOrgHier = p_objXmlDoc.SelectSingleNode("//NEOrgHier").InnerText;
						else
							sNEOrgHier = "0";
						if (sNE == "2")
							sNEDefID = p_objXmlDoc.SelectSingleNode("//NEDefID").Attributes.GetNamedItem("codeid").InnerText;
						else
							sNEDefID = "0";
					}

					// Tennessesse
					if (p_objXmlDoc.SelectSingleNode("//TNParentOption") != null)
					{
						sTN = p_objXmlDoc.SelectSingleNode("//TNParentOption").InnerText;
						if (sTN == "1")
							sTNOrgHier = p_objXmlDoc.SelectSingleNode("//TNOrgHier").InnerText;
						else
							sTNOrgHier = "0";
						if (sTN == "2")
							sTNDefID = p_objXmlDoc.SelectSingleNode("//TNDefID").Attributes.GetNamedItem("codeid").InnerText;
						else
							sTNDefID = "0";
					}

					// Virginia
					if (p_objXmlDoc.SelectSingleNode("//VAParentOption") != null)
					{
						sVA = p_objXmlDoc.SelectSingleNode("//VAParentOption").InnerText;
						if (sVA == "1")
							sVAOrgHier = p_objXmlDoc.SelectSingleNode("//VAOrgHier").InnerText;
						else
							sVAOrgHier = "0";
						if (sVA == "2")
							sVADefID = p_objXmlDoc.SelectSingleNode("//VADefID").Attributes.GetNamedItem("codeid").InnerText;
						else
							sVADefID = "0";
					}

					// TPA Entity ID
                    if (p_objXmlDoc.SelectSingleNode("//TpaUseOpt") != null)
                        sTpaUseOpt = p_objXmlDoc.SelectSingleNode("//TpaUseOpt").InnerText;

                    //if (p_objXmlDoc.SelectSingleNode("//TPAEID") != null) //commented repeating code MITS 29587
                    //    sTPA = p_objXmlDoc.SelectSingleNode("//TPAEID").Attributes.GetNamedItem("codeid").InnerText; //commented repeating code MITS 29587
                    if (sTpaUseOpt != "0")
                    {

                        if (p_objXmlDoc.SelectSingleNode("//TPAEID") != null)
                            sTPA = p_objXmlDoc.SelectSingleNode("//TPAEID").Attributes.GetNamedItem("codeid").InnerText;
                    }
                    else
                    {
                        sTPA = "0";
                    }

					// Updated by User
					sUpdateUser = m_sUserName;
					sLastUpdateDateTime = Conversion.ToDbDateTime(DateTime.Now);

					sAcordCopies = "0";
					sBaseLevel = "0";
					sPrintAcordForms = "0";
					sPerNotOpt = "0";
					sSuspresDefTime = "0";
                    //Start by Shivendu for MITS 11367
                    if (p_objXmlDoc.SelectSingleNode("//TpaUseOpt") != null)
                        sTpaUseOpt = p_objXmlDoc.SelectSingleNode("//TpaUseOpt").InnerText;
                    if (sTpaUseOpt != "0")
                    {

                        if (p_objXmlDoc.SelectSingleNode("//FROITPAEID") != null)
                            sFroiTpaEid = p_objXmlDoc.SelectSingleNode("//FROITPAEID").Attributes.GetNamedItem("codeid").InnerText;
                    }
                    else
                    {
                        sFroiTpaEid = "0";
                    }
                    //End by Shivendu for MITS 11367
					#endregion

					sSql = "SELECT * FROM " + m_sFormName + "_OPTIONS WHERE SELECTED_EID = " 
						+ sSelectedEID
						+ " AND JURIS_ROW_ID = "
						+ sJurisRowID;
					objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
					if (objReader.Read()) // Record already present
					{
						sSql = "UPDATE " + m_sFormName + "_OPTIONS SET "
                            + " PRINT_OSHA_DESC = '" + sPrintDesc + "'" // Added for MITS 29587 sgupta243
							+ ", CLMADMIN_ORG_LEVEL = " + sClmAdmOrgLvl
                            ////MITS-10753 - Added ATTACH_TO_CLAIM to update it from the juris option
                            //+ ", ATTACH_TO_CLAIM = " + sAttachToClm
                            ////MITS-10753 End
							+ ", DEF_CLAM_ADMIN_EID = " + sDefClmAdm
							+ ", PRINT_CLAIM_ADMIN = " + sClaimAdmin
							+ ", CLAIMADMIN_TPA_EID = " + sClmAdmTPA
							+ ", CARRIER_ORG_LEVEL = " + sCarOrgLvl
							+ ", DEF_CARRIER_EID = " + sDefCarr
							+ ", USE_DEF_CARRIER = " + sCarrier
							+ ", FROI_PREPARER = " + sPreparer
							+ ", EMPLOYER_LEVEL = " + sEmployer
							+ ", DTTM_RCD_LAST_UPD = '" + sLastUpdateDateTime + "'"
							+ ", UPDATED_BY_USER = '" + sUpdateUser + "'";

						if (m_sFormName == "FROI")
							sSql = sSql + ", CONTACT_OPTION = " + sConOpt
								+ ", USE_TITLE = " + sUseTitle
								+ ", ATTACH_TO_CLAIM = " + sAttachToClm
								+ ", PRINT_CLMNUMB_TOP = " + sPrintClmTop
								+ ", FORCE_PRINT_NCCI = " + sNCCI
								+ ", WORK_LOSS_FIELD = " + sWorkLoss
								+ ", NE_PARENT_OPTION = " + sNE
								+ ", NE_PARENT_ORG_HIER = " + sNEOrgHier
								+ ", NE_PARENT_DEF_ID = " + sNEDefID
								+ ", TN_PARENT_OPTION = " + sTN
								+ ", TN_PARENT_ORG_HIER = " + sTNOrgHier
								+ ", TN_PARENT_DEF_ID = " + sTNDefID
								+ ", VA_PARENT_OPTION = " + sVA
								+ ", VA_PARENT_DEF_ID = " + sVADefID
								+ ", VIRGIN_PARNT_LEVEL = " + sVAOrgHier
								+ ", SUSPRES_DEFLT_TIME = " + sSuspresDefTime
                                + ", TPA_USE_OPT = " + sTpaUseOpt //Added by Shivendu for MITS 11367
                                + ", TPA_EID = " + sFroiTpaEid   //Added by Shivendu for MITS 11367 //Removed Semi colon:MITS 13124
                                + ", CARR_CLAIM_NUM_OPT = " + sCarrClmNumOpt;   //Added by MGaba2 for MITS 13124
						else if (m_sFormName == "JURIS")
							// Raman Bhatia: RMX Phase 2 Juris Options Changes for EDI
						{
                            //MITS-10753 - Added ATTACH_TO_CLAIM to update it from the juris option.MITS 11934 also required this.
                            //MITS 29587 - sgupta243 Added TPA_USE_OPT to update TPA_USE_OPT option
                            sSql = sSql + ", TPA_EID = " + sTPA + ", CARR_CLAIM_NUM_OPT = " + sCarrClmNumOpt + ", CHECK_STATUS_OPT = " + sCheckStatusOpt + ", ATTACH_TO_CLAIM = " + sAttachToClm + ",TPA_USE_OPT = " + sTpaUseOpt;
                            //MITS 29587 End
                            //MITS-10753 End
						}

						sSql = sSql + " WHERE SELECTED_EID = " + sSelectedEID 
							+ " AND JURIS_ROW_ID = " + sJurisRowID;

						objConn = DbFactory.GetDbConnection(m_sConnectionString);
						objConn.Open();
						objCommand = objConn.CreateCommand();
						objCommand.CommandType = CommandType.Text;

						objCommand.CommandText = sSql;
						objCommand.ExecuteNonQuery();
                        //Rijul 340
                        bool blnSuccess;
                        if (m_objDataModelFactory.Context.InternalSettings.SysSettings.UseEntityRole)
                        {
                            using(EntityXRole objEntityXRole = m_objDataModelFactory.GetDataModelObject("EntityXRole", false) as EntityXRole)
                            {
                            if (Conversion.CastToType<int>(sDefClmAdm, out blnSuccess) > 0)
                                objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.WC_DEF_CLAIM_ADMIN.ToString
                                        ()), Conversion.CastToType<int>(sDefClmAdm, out blnSuccess), m_sUserName);
                            if (Conversion.CastToType<int>(sClmAdmTPA, out blnSuccess) > 0)
                                objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.CLAIM_ADMIN_TPA.ToString
                                        ()), Conversion.CastToType<int>(sClmAdmTPA, out blnSuccess), m_sUserName);
                            if (Conversion.CastToType<int>(sNEDefID, out blnSuccess) > 0)
                                objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.DEFAULT_PARENT.ToString
                                        ()), Conversion.CastToType<int>(sNEDefID, out blnSuccess), m_sUserName);
                            if (Conversion.CastToType<int>(sTNDefID, out blnSuccess) > 0)
                                objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.DEFAULT_PARENT.ToString
                                        ()), Conversion.CastToType<int>(sTNDefID, out blnSuccess), m_sUserName);
                            if (Conversion.CastToType<int>(sVADefID, out blnSuccess) > 0)
                                objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.DEFAULT_PARENT.ToString
                                        ()), Conversion.CastToType<int>(sVADefID, out blnSuccess), m_sUserName);
                            if (Conversion.CastToType<int>(sDefCarr, out blnSuccess) > 0)
                                objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.INSURERS.ToString()),
                                    Conversion.CastToType<int>(sDefCarr, out blnSuccess), m_sUserName);
                            }
                        }
                        //Rijul 340 End
					}				
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIOptions.Save.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Dispose();
				}				
				if (objConn != null)
				{
					objConn.Dispose();
				}
				objCommand = null;
				objParentElm = null;
			}

			return p_objXmlDoc;
		}

		/// Name		: Delete
		/// Author		: Mihika Agrawal
		/// Date Created: 10/04/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deleting a FROI Criteria
		/// </summary>
		/// <param name="p_objXmlDoc">Xml Input Structure</param>
		/// <returns>Xml Output structure</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDoc)
		{
			DbConnection objConn = null;
			DbCommand objCommand=null;

			XmlElement objParentElm = null;

			string sKey = string.Empty;
			string sSelectedEID = string.Empty;
			string sJurisRowId = string.Empty;

			string sSql = string.Empty;

			try
			{
				objParentElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FROI");
				if (objParentElm != null)
					m_sFormName = "FROI";
				else
					m_sFormName = "JURIS";
				sKey = p_objXmlDoc.SelectSingleNode("//GetValueForKey").InnerText;
				if (sKey != "")
				{
					sKey = sKey.Substring(1);
					sJurisRowId = sKey.Substring(0, sKey.IndexOf(";"));
					sSelectedEID = sKey.Substring(sKey.IndexOf(";") + 1);

					if (!(sJurisRowId == "-1" && sSelectedEID == "-1"))
					{
						sSql = "DELETE FROM " + m_sFormName + "_OPTIONS WHERE SELECTED_EID = " 
							+ sSelectedEID
							+ " AND JURIS_ROW_ID = "
							+ sJurisRowId;

						objConn = DbFactory.GetDbConnection(m_sConnectionString);
						objConn.Open();
						objCommand = objConn.CreateCommand();
						objCommand.CommandType = CommandType.Text;

						objCommand.CommandText = sSql;
						objCommand.ExecuteNonQuery();
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIOptions.Delete.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Dispose();
					
				}
				objCommand = null;
                objParentElm = null;
			}

			return p_objXmlDoc;		
		}

		/// Name		: New
		/// Author		: Mihika Agrawal
		/// Date Created: 10/04/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Creates and Saves the data for FROI Options for a new record
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data</param>
		/// <returns>Returns the saved xml data</returns>
		public XmlDocument New(XmlDocument p_objXmlDoc)
		{
			DbReader objReader = null;
			DbConnection objConn = null;
			DbCommand objCommand=null;

			string sSql = string.Empty;
			string sSelectedEID = string.Empty;
			string sJurisRowID = string.Empty;
			string sLastUpdateDateTime = string.Empty;
			string sAddedUser = string.Empty;

			#region Default Values
			string sBaseLevel = "0";
			string sPrintDesc = "1"; // OSHA Description
			string sClmAdmOrgLvl = "0"; // Claim Admin Org Hier - Not selected
            //MITS-10753 - Added default value for 
            string sCarrClmNumOpt = "1";
            //MITS-10753 End
			string sDefClmAdm = "0"; // Def Claim Adm - Not selected
			string sClaimAdmin = "1"; // By default 'Nothing' is selected
			string sClmAdmTPA = "0"; // not selected
			string sCarOrgLvl = "0"; // Carrier Org Hier - not selected
			string sDefCarr = "0"; // Def Carrier - not selected
			string sCarrier = "3"; // By default 'Linked by policy numer' is selected
			string sConOpt = "1"; // By default first entry is selected
			string sUseTitle = "0"; // First entry
			string sPreparer = "3"; // First entry
			string sAttachToClm = "0"; // First entry
			string sPrintClmTop = "1"; // First entry
			string sNCCI = "1"; // First entry
			string sEmployer = "1005"; // 'Client' is selected
			string sWorkLoss = "1"; // First entry
			string sNE ="3"; // 'Linked by poilicy' selected by default
			string sNEOrgHier = "0";
			string sNEDefID = "0";
			string sTN = "3"; // 'Linked by poilicy' selected by default
			string sTNOrgHier = "0";
			string sTNDefID = "0";
			string sVA ="1"; // 'Client/Company name' selected by default
			string sVADefID ="0";
			string sVAOrgHier = "1012"; // 'Department' by default
			string sAcordCopies = "0";
			string sPrintAcordForms = "0";
			string sSuspresDefTime = "0";
			string sPerNotOpt = "0";
			string sTPAEID = "0";
			#endregion
			try
			{
				m_sFormName = p_objXmlDoc.SelectSingleNode("//FormName").InnerText;
				if (m_sFormName == "JURIS")
				{
					//Change in the default values
					sBaseLevel = "1005";
					sVAOrgHier = "1005";
				}
				sSelectedEID = p_objXmlDoc.SelectSingleNode("//OrgHierEntity").InnerText;
				if (sSelectedEID != "")
				{
					if (sSelectedEID != "-1") // In case sone entity is selected
						sSelectedEID = p_objXmlDoc.SelectSingleNode("//SelOrgHier").Attributes.GetNamedItem("codeid").InnerText;
					if (sSelectedEID == "0")
						sSelectedEID = "-1";
				
					sJurisRowID = p_objXmlDoc.SelectSingleNode("//Jurisdictions").InnerText;
					if (sJurisRowID != "-1") // In case some jurisdiction is selected
						sJurisRowID = p_objXmlDoc.SelectSingleNode("//JurisState").Attributes.GetNamedItem("codeid").InnerText;
					if (sJurisRowID == "0")
						sJurisRowID = "-1";

					sSql = "SELECT * FROM " + m_sFormName + "_OPTIONS WHERE SELECTED_EID = " 
						+ sSelectedEID
						+ " AND JURIS_ROW_ID = "
						+ sJurisRowID;
					objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
					if (!objReader.Read()) // Check if Record is already present
					{
						sLastUpdateDateTime = Conversion.ToDbDateTime(DateTime.Now);
						sAddedUser = m_sUserName;
                        //MITS-10753 - Modified sSQL; Added the CARR_CLAIM_NUM_OPT in it. It is integer in the db and it's value should be filled
						sSql = "INSERT INTO " + m_sFormName + "_OPTIONS (BASE_LEVEL, SELECTED_EID, JURIS_ROW_ID, "
							+ "PRINT_OSHA_DESC, CLMADMIN_ORG_LEVEL, DEF_CLAM_ADMIN_EID, PRINT_CLAIM_ADMIN, "
							+ "CLAIMADMIN_TPA_EID, CARRIER_ORG_LEVEL, DEF_CARRIER_EID, USE_DEF_CARRIER, "
							+ "FROI_PREPARER, ATTACH_TO_CLAIM, EMPLOYER_LEVEL, VIRGIN_PARNT_LEVEL, "
							+ "DTTM_RCD_ADDED, ADDED_BY_USER, DTTM_RCD_LAST_UPD, UPDATED_BY_USER, "
                            + "ACORD_COPIES, PRINT_ACORD_FORMS, CARR_CLAIM_NUM_OPT, ";

						if (m_sFormName == "FROI")
							sSql = sSql + "CONTACT_OPTION, USE_TITLE, PRINT_CLMNUMB_TOP, FORCE_PRINT_NCCI, "
								+ "WORK_LOSS_FIELD, NE_PARENT_OPTION, NE_PARENT_ORG_HIER, NE_PARENT_DEF_ID, "
								+ "TN_PARENT_OPTION, TN_PARENT_ORG_HIER, TN_PARENT_DEF_ID, "
								+ "VA_PARENT_OPTION, VA_PARENT_DEF_ID, SUSPRES_DEFLT_TIME, PERSON_NOTFIED_OPT) ";
						else
							sSql = sSql + "TPA_EID) ";
                        //MITS-10753 - Modified sSQL; Added the value for CARR_CLAIM_NUM_OPT in the query
						sSql = sSql + "VALUES (" + sBaseLevel + ", " + sSelectedEID + ", " + sJurisRowID + ", "
							+ sPrintDesc + ", " + sClmAdmOrgLvl + ", " + sDefClmAdm + ", " + sClaimAdmin + ", "
							+ sClmAdmTPA + ", " + sCarOrgLvl + ", " + sDefCarr + ", " + sCarrier + ", "
							+ sPreparer + ", " + sAttachToClm + ", " + sEmployer + ", " + sVAOrgHier + ", '"
							+ sLastUpdateDateTime + "', '" + sAddedUser + "', '" + sLastUpdateDateTime + "', '" + sAddedUser + "', "
                            + sAcordCopies + ", " + sPrintAcordForms + ", " + sCarrClmNumOpt + ", ";
                        //MITS-10753 End - Modified the sql query
						if (m_sFormName == "FROI")
							sSql = sSql + sConOpt + ", " + sUseTitle + ", " + sPrintClmTop + ", " + sNCCI + ", " 
								+ sWorkLoss + ", " + sNE + ", " + sNEOrgHier + ", " + sNEDefID + ", " 
								+ sTN + ", " + sTNOrgHier + ", " + sTNDefID + ", " 
								+ sVA + ", " + sVADefID + ", " + sSuspresDefTime + ", " + sPerNotOpt + ")";
						else
							sSql = sSql + sTPAEID + ")";

						objConn = DbFactory.GetDbConnection(m_sConnectionString);
						objConn.Open();
						objCommand = objConn.CreateCommand();
						objCommand.CommandType = CommandType.Text;

						objCommand.CommandText = sSql;
						objCommand.ExecuteNonQuery();
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIOptions.New.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Dispose();
				}				
				if (objConn != null)
				{
					objConn.Dispose();
				}
				objCommand = null;
			}
			return p_objXmlDoc;
		}

		/// Name		: GetFROIHistory
		/// Author		: Mihika Agrawal
		/// Date Created: 10/04/2005	
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the history for a FROI Form
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data</param>
		/// <returns>Returns the saved xml data</returns>
		public XmlDocument GetFROIHistory(XmlDocument p_objXmlDoc)
		{
			DbReader objReader = null;
			XmlElement objParentElm = null;
			XmlElement objRowElm = null;
			XmlElement objChildElm = null;

			string sSql = string.Empty;
			string sFormID = string.Empty;

			try
			{
				sFormID = p_objXmlDoc.SelectSingleNode("//FormID").InnerText;
				objParentElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//List");

				sSql = "SELECT JURIS_FORMS_HIST.DATE_PRINTED, JURIS_FORMS_HIST.TIME_PRINTED, CLAIM.CLAIM_NUMBER, JURIS_FORMS_HIST.USER_ID "
					+ "FROM JURIS_FORMS_HIST, CLAIM "
					+ " WHERE JURIS_FORMS_HIST.FORM_ID = " 
					+ sFormID 
					+ " AND JURIS_FORMS_HIST.CLAIM_ID = CLAIM.CLAIM_ID "
					+ "ORDER BY DATE_PRINTED DESC, TIME_PRINTED DESC";

				objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
				while (objReader.Read())
				{
					objRowElm = p_objXmlDoc.CreateElement("listrow");

					// Claim Number
					objChildElm = objRowElm.OwnerDocument.CreateElement("ClaimNumber");
					objChildElm.InnerText = objReader.GetValue("CLAIM_NUMBER").ToString();
					objRowElm.AppendChild(objChildElm);

					// Date Printed
					objChildElm = objRowElm.OwnerDocument.CreateElement("DatePrinted");
					objChildElm.InnerText = Conversion.GetDBDateFormat(objReader.GetValue("DATE_PRINTED").ToString(), "d");
					objRowElm.AppendChild(objChildElm);

					// Time Printed
					objChildElm = objRowElm.OwnerDocument.CreateElement("TimePrinted");
					objChildElm.InnerText = Conversion.GetDBTimeFormat(objReader.GetValue("TIME_PRINTED").ToString(), "t");
					objRowElm.AppendChild(objChildElm);

					// User ID
					objChildElm = objRowElm.OwnerDocument.CreateElement("UserID");
					objChildElm.InnerText = objReader.GetValue("USER_ID").ToString();
					objRowElm.AppendChild(objChildElm);

					objParentElm.AppendChild(objRowElm);
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIOptions.GetFROIHistory.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Dispose();
				}
				objParentElm = null;
				objRowElm = null;
				objChildElm = null;
			}
			return p_objXmlDoc;
		}
		#endregion

		#region Private Methods

		/// Name		: PopulateOptions
		/// Author		: Mihika Agrawal
		/// Date Created: 09/28/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Populates the XML with data for FROI Options
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data</param>
		/// <returns>Returns the saved xml data</returns>
		private XmlDocument PopulateOptions(XmlDocument p_objXmlDoc)
		{
			XmlElement objElem = null;
			DbReader objReader = null;
			Entity objEntity = null;

			string sSql = string.Empty;
			string sKey = string.Empty;
			string sCodeId = string.Empty;
	
			try
			{
				objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//GetValueForKey");
				objEntity = (DataModel.Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
			
				if (objElem.InnerText == "" || objElem == null) // If first call, then populate data for the default settings' record
				{
					sSql = "SELECT * FROM " + m_sFormName + "_OPTIONS WHERE SELECTED_EID = -1 AND JURIS_ROW_ID = -1";
				}
				else // key is present and data is required for a particular record
				{
					sKey = objElem.InnerText.Trim();
					sKey = sKey.Substring(1);
					sSql = "SELECT * FROM " + m_sFormName + "_OPTIONS WHERE JURIS_ROW_ID = " 
						+ sKey.Substring(0, sKey.IndexOf(";"))
						+ " AND SELECTED_EID  = "
						+ sKey.Substring(sKey.IndexOf(";") + 1);
				}

				objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);

                if (objReader != null && objReader.Read())
				{
                    //abisht MITS 11278.Commented code and moved it to if condition.
                    //objReader.Read();

					#region Populate data from query to XML
					// Description
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//Description");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("PRINT_OSHA_DESC").ToString();
						//Raman Bhatia 7/6/2006 Following code doesnt make much sense
						/*
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "1";
						*/
					}
                    //Start by Shivendu for MITS 11367
                    if (m_sFormName == "FROI")
                    {
                        objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//TpaUseOpt");
                        if (objElem != null)
                        {
                            //Added by Shivendu to do a "" check
                            if (objReader.GetValue("TPA_USE_OPT").ToString() != "")
                            {
                                objElem.InnerText = objReader.GetValue("TPA_USE_OPT").ToString();
                            }
                            else
                            {
                                objElem.InnerText = "0";
                            }

                            
                        }

                        objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//FROITPAEID");
                        if (objElem != null)
                        {
                            sCodeId = objReader.GetValue("TPA_EID").ToString();
                            if (sCodeId != "0" && sCodeId != "")
                            {
                                objElem.SetAttribute("codeid", sCodeId);
                                objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
                                objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
                            }
                            else
                            {
                                objElem.SetAttribute("codeid", "0");
                                objElem.InnerText = "";
                            }
                        }
                    }
                    //End by Shivendu for MITS 11367

                    //Start by sgupta243 for MITS 29587

                    if (m_sFormName == "JURIS")
                    {
                        objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//TpaUseOpt");
                        if (objElem != null)
                        {
                            //Added to do a "" check
                            if (objReader.GetValue("TPA_USE_OPT").ToString() != "")
                            {
                                objElem.InnerText = objReader.GetValue("TPA_USE_OPT").ToString();
                            }
                            else
                            {
                                objElem.InnerText = "0";
                            }


                        }
                        // TPA EID
                        objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//TPAEID");
                        if (objElem != null)
                        {
                            sCodeId = objReader.GetValue("TPA_EID").ToString();
                            if (sCodeId != "0" && sCodeId != "")
                            {
                                objElem.SetAttribute("codeid", sCodeId);
                                objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
                                objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
                            }
                            else
                            {
                                objElem.SetAttribute("codeid", "0");
                                objElem.InnerText = "";
                            }
                        }
                    }

                    //End by sgupta243 for MITS 29587
					
                    // Claim Administrator
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClaimAdministrator");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("PRINT_CLAIM_ADMIN").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "1";
					}

					// Claim Admin - org level
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClAdmOrgLvl");
					if (objElem != null)
						objElem.InnerText = objReader.GetValue("CLMADMIN_ORG_LEVEL").ToString();

					// Claim Admin - deafult entity id
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClAdmDefEID");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("DEF_CLAM_ADMIN_EID").ToString();
						if (sCodeId != "0" && sCodeId != "")
						{
							objElem.SetAttribute("codeid", sCodeId);
							objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
							objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
						}
						else
						{
							objElem.SetAttribute("codeid", "0");
							objElem.InnerText = "";
						}
					}

					//Raman Bhatia RMX Phase 2 : Juris Options changes for EDI
					
					// Claim Number Options
					
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//CarrClmNumOpt");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("CARR_CLAIM_NUM_OPT").ToString();
						objElem.InnerText = sCodeId;
					}

					// Financial Reporting (EDI and Forms)

					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//CheckStatusOpt");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("CHECK_STATUS_OPT").ToString();
						objElem.InnerText = sCodeId;
                        if (Conversion.ConvertStrToInteger(objElem.InnerText) <= 0)
                            objElem.InnerText = "0";
					}

					// Claim Admin - TPA entity id
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//ClAdmTPAEID");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("CLAIMADMIN_TPA_EID").ToString();
						if (sCodeId != "0" && sCodeId != "")
						{
							objElem.SetAttribute("codeid", sCodeId);
							objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
							objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
						}					
						else
						{
							objElem.SetAttribute("codeid", "0");
							objElem.InnerText = "";
						}
					}

					// Carrier
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//Carrier");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("USE_DEF_CARRIER").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "3";
					}

					// Carrier - org level
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//CarrierOrgLevel");
					if (objElem != null)
						objElem.InnerText = objReader.GetValue("CARRIER_ORG_LEVEL").ToString();

					// Carrier - default entity id
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//CarrierDefEID");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("DEF_CARRIER_EID").ToString();
						if (sCodeId != "0" && sCodeId != "")
						{
							objElem.SetAttribute("codeid", sCodeId);
							objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
							objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
						}
						else
						{
							objElem.SetAttribute("codeid", "0");
							objElem.InnerText = "";
						}
					}

					// Contact Information
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//ContactInfo");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("CONTACT_OPTION").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "1";
					}

					// Use Title/Position Code
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//UseTitle");
					if (objElem != null)
					{
						objElem.InnerText = Conversion.ConvertObjToInt(objReader.GetValue("USE_TITLE"), m_iClientId).ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<0)
							objElem.InnerText = "0";
					}

					// Preparer Info
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//PreparerInfo");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("FROI_PREPARER").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "3";
					}

					// Attach To Claim
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//AttachToClaim");
					if (objElem != null)
					{
						objElem.InnerText = Conversion.ConvertObjToInt(objReader.GetValue("ATTACH_TO_CLAIM"), m_iClientId).ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<0)
							objElem.InnerText = "0";
					}

					// Print Claim Number at top
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//PrintClaimNum");
					if (objElem != null)
					{
						objElem.InnerText = Conversion.ConvertObjToInt(objReader.GetValue("PRINT_CLMNUMB_TOP"), m_iClientId).ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<0)
							objElem.InnerText = "1";
					}

					// Force Printing of NCCI
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//PrintNCCI");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("FORCE_PRINT_NCCI").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "1";
					}

					// Employer Org Level
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//EmployerLevel");
					if (objElem != null)
						objElem.InnerText = objReader.GetValue("EMPLOYER_LEVEL").ToString();

					// Work Loss
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//WorkLoss");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("WORK_LOSS_FIELD").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "1";
					}

					// NE Parent Option
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//NEParentOption");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("NE_PARENT_OPTION").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "3";
					}

					// NE Org Hier
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//NEOrgHier");
					if (objElem != null)
						objElem.InnerText = objReader.GetValue("NE_PARENT_ORG_HIER").ToString();

					// NE Default Entity ID
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//NEDefID");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("NE_PARENT_DEF_ID").ToString();
						if (sCodeId != "0" && sCodeId != "")
						{
							objElem.SetAttribute("codeid", sCodeId);
							objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
							objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
						}
						else
						{
							objElem.SetAttribute("codeid", "0");
							objElem.InnerText = "";
						}
					}

					// TN Parent Option
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//TNParentOption");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("TN_PARENT_OPTION").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "3";
					}

					// TN Org Hier
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//TNOrgHier");
					if (objElem != null)
						objElem.InnerText = objReader.GetValue("TN_PARENT_ORG_HIER").ToString();

					// TN Default Entity ID
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//TNDefID");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("TN_PARENT_DEF_ID").ToString();
						if (sCodeId != "0" && sCodeId != "")
						{
							objElem.SetAttribute("codeid", sCodeId);
							objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
							objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
						}
						else
						{
							objElem.SetAttribute("codeid", "0");
							objElem.InnerText = "";
						}
					}

					// VA Parent Option
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//VAParentOption");
					if (objElem != null)
					{
						objElem.InnerText = objReader.GetValue("VA_PARENT_OPTION").ToString();
						if (Conversion.ConvertStrToInteger(objElem.InnerText)<=0)
							objElem.InnerText = "1";
					}

					// VA Org Hier
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//VAOrgHier");
					if (objElem != null)
						objElem.InnerText = objReader.GetValue("VIRGIN_PARNT_LEVEL").ToString();

					// VA Default Entity ID
					objElem = (XmlElement)p_objXmlDoc.SelectSingleNode("//VADefID");
					if (objElem != null)
					{
						sCodeId = objReader.GetValue("VA_PARENT_DEF_ID").ToString();
						if (sCodeId != "0" && sCodeId != "")
						{
							objElem.SetAttribute("codeid", sCodeId);
							objEntity.MoveTo(Conversion.ConvertStrToInteger(sCodeId));
							objElem.InnerText = objEntity.LastName + "; " + objEntity.Addr1;
						}
						else
						{
							objElem.SetAttribute("codeid", "0");
							objElem.InnerText = "";
						}
					}

					
				
					#endregion
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIOptions.PopulateOptions.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if (objReader != null)
				{
					objReader.Dispose();
				}
				if (objEntity != null)
				{
					objEntity.Dispose();
				}
				objElem = null;
			}
			return p_objXmlDoc;
		}

		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx ;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIOptions.Initialize.Error", m_iClientId), p_objEx);				
			}			
		}
		#endregion
	}
}
