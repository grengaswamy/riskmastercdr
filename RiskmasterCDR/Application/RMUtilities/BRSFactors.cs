using System;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Xml;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   04th,May 2005
	///Purpose :   BRS Factors List Form 
	/// </summary>
	public class BRSFactors :UtilitiesUIListBase
	{
        private int m_iClientId = 0;

		/// <summary>
		/// String array for database fields mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "CLINICAL_CAT_CODE"},
			{"ClinicalCatCode","CLINICAL_CAT_CODE"},
			{"ClinicalCategory","CLINICAL_CAT_CODE"},
			{"ConversionFact","FACTOR"}
									  };
        
		/// Name		: BRSFactors
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
		public BRSFactors(string p_sConnString, int p_iClientId):base(p_sConnString, p_iClientId)
		{
			base.ConnectString = p_sConnString;
            m_iClientId = p_iClientId; 
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "WRCMP_FACTOR";
			base.KeyField = "TABLE_ID";
			base.RadioName = "BRSFactorsList";
			base.OrderByClause = "";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for a particular id
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>xml data and structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{	
			XmlNodeList objNodLst=null;
			LocalCache objCache=null;
			XmlNode objNode = null;
			try
			{
				XMLDoc = p_objXmlDocument;
                objCache = new LocalCache(ConnectString, m_iClientId);  
				objNode = p_objXmlDocument.SelectSingleNode("//TableId");
				string sTableId = objNode.InnerText;
				base.WhereClause  = "TABLE_ID =" + sTableId;
				base.Get();
				objNodLst = p_objXmlDocument.SelectNodes("//listrow//ClinicalCatCode");
				foreach(XmlElement objElm in objNodLst)
				{
					objElm.ParentNode.LastChild.InnerText= sTableId + "|" + objElm.InnerText; 
					objElm.InnerText = objCache.GetShortCode(Conversion.ConvertStrToInteger(objElm.InnerText)); 
				}

				objNodLst = p_objXmlDocument.SelectNodes("//listrow//ClinicalCategory");
				foreach(XmlElement objElm in objNodLst)
				{
					objElm.InnerText = objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objElm.InnerText)); 
				}

				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFactors.Get.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objNodLst=null;
				objNode = null;
				if(objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
			}
		}
	}
}