using System;
using System.Data; 
using System.Xml;
using Riskmaster.Db;
using System.Text;
using System.Globalization;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	/// Summary description for FROIPreparerInfo.
	/// </summary>
	public class FROIPreparerInfo
	{
		public FROIPreparerInfo(string userId,string connectionString, int p_iClientId)
		{
			m_userId=userId;
			m_connectionString=connectionString;
            m_iClientId = p_iClientId;
			//
			// TODO: Add constructor logic here
			//
		}
	    private enum FROIOptions
		{
			iNoFieldData,iNoFROIOptions,iFROIOptions,iNoRecord //3/14 Sumit no records case is added
		}
		private string m_userId;
		private string m_connectionString;
		private int iSkipPrompt;
        private int m_iClientId = 0;

		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			try
			{
				XmlDocument xml = GetUserPref();
				XmlNode node = xml.SelectSingleNode("//FROIOptions");
				if ( node != null )
				{
					p_objXmlDocument.SelectSingleNode("//FROIPreparer/preparerPhone").InnerText	=	node.Attributes.GetNamedItem("phone").InnerText;
					p_objXmlDocument.SelectSingleNode("//FROIPreparer/preparerName").InnerText	=	node.Attributes.GetNamedItem("name").InnerText;
					p_objXmlDocument.SelectSingleNode("//FROIPreparer/preparerTitle").InnerText	=	node.Attributes.GetNamedItem("title").InnerText;
					p_objXmlDocument.SelectSingleNode("//FROIPreparer/rememberInfo").InnerText	=	node.Attributes.GetNamedItem("skipprompt").InnerText;
				}
				return p_objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIPreparerInfo.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
              
			}
		}

		private XmlDocument GetUserPref()
		{
			XmlDocument xml  = new XmlDocument();
			XmlNode node  =null;
			try
			{
				string sTemp;
				iSkipPrompt = 0;
				string sName = string.Empty;
				string sTitle = string.Empty;
				string sPhone = string.Empty;
				string sXMLText = string.Empty;
				switch (XMLExists(ref sXMLText))
				{
					case FROIOptions.iNoFieldData:
					{
						break;
					}
					case FROIOptions.iFROIOptions:
					{
						xml = new XmlDocument();
						xml.LoadXml( sXMLText );
						node = xml.SelectSingleNode("//FROIOptions");
						if ( node != null )
						{
							sTemp =	node.Attributes.GetNamedItem("skipprompt").InnerText.Trim();
							if( sTemp == "")
							{
								iSkipPrompt = -2;
							}
							else
							{
								iSkipPrompt = Conversion.ConvertStrToInteger( node.Attributes.GetNamedItem("skipprompt").InnerText);
							}
						}
						break;
					}
					case FROIOptions.iNoFROIOptions:
					{
						break;
					}
					default:
					{
						break;
					}	
				}
				return xml;
			
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FROIPreparerInfo.Get.Error", m_iClientId),p_objEx);
			}
			finally
			{
				xml  =null;
				node  =null;
			}
		}

		private FROIPreparerInfo.FROIOptions XMLExists(ref string sXMLText)
		{
			string sSQL;
			sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + m_userId;
			DbReader objReader=null;
			try
			{
				objReader = DbFactory.GetDbReader(m_connectionString,sSQL);
				bool bRecords = false;
				while( objReader.Read() )
				{
					bRecords = true;
					sXMLText += objReader.GetValue(0).ToString();
				}
				if (!bRecords) return FROIOptions.iNoRecord;
					
				if ( sXMLText == "" )
				{
					return FROIOptions.iNoFieldData;
				}
							
				if ( sXMLText.ToUpper().IndexOf("FroiOptions".ToUpper())!=-1)
				{
					return FROIOptions.iFROIOptions;
				}
				else
					return FROIOptions.iNoFROIOptions;
			}
			catch( RMAppException p_objException )
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{			
				throw p_objException;
			}
			finally
			{
                if(objReader!= null)
				    objReader.Dispose();
			}
		}
	
		public bool Save(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			string sName = string.Empty;
			string sTitle = string.Empty;
			string sPhone = string.Empty;
			string sRememberInfo = string.Empty;
			string sXMLText = string.Empty;
			
			string sSQL="";
			DbConnection objConn = null;																				
			DbCommand objCommand=null;
            DbParameter objParam = null;                    //Umesh
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//FROIPreparer/preparerName"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sName=objNode.InnerText;
						//sName=sName.Replace("'","''");   //MITS_8257  by Umesh
				}
				objNode=p_objXmlDocument.SelectSingleNode("//FROIPreparer/preparerTitle"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sTitle=objNode.InnerText;
					    //sTitle=sTitle.Replace("'","''");  //MITS_8257  by Umesh
						
				}
				objNode = p_objXmlDocument.SelectSingleNode("//FROIPreparer/preparerPhone");  
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sPhone=objNode.InnerText;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//FROIPreparer/rememberInfo");  
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sRememberInfo=objNode.InnerText;
					if (sRememberInfo == "")
						sRememberInfo = "0";
				}
				string sXml=string.Empty;
				//3/14 decalring at the start
				XmlDocument xml = new XmlDocument();
				
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                // insert/update using SQL command  using parameter : Umesh
                objParam = objCommand.CreateParameter();
                objParam.Direction = ParameterDirection.Input;
                objParam.ParameterName = "XML";
                objParam.SourceColumn = "PREF_XML";
				switch ( XMLExists(ref sXml))
				{
					case FROIOptions.iNoFROIOptions:
					{
						//Sumit 3/14 case where user pref is there but no froi tag
						xml.LoadXml(sXml);						
						XmlElement objElem=xml.CreateElement("FROIOptions");							
						objElem.Attributes.Append(xml.CreateAttribute("phone"));
						objElem.Attributes.Append(xml.CreateAttribute("name"));
						objElem.Attributes.Append(xml.CreateAttribute("title"));
						objElem.Attributes.Append(xml.CreateAttribute("skipprompt"));
						objElem.Attributes.GetNamedItem("phone").InnerText=	sPhone;
						objElem.Attributes.GetNamedItem("name").InnerText=	sName;
						objElem.Attributes.GetNamedItem("title").InnerText=	sTitle;
						objElem.Attributes.GetNamedItem("skipprompt").InnerText = sRememberInfo;
						xml.SelectSingleNode("//setting").AppendChild(objElem);							
                        objParam.Value = xml.InnerXml;
                        sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID = " + m_userId;
						break;
					}
					case FROIOptions.iFROIOptions:
					{
						//sumit3/14 case when an already present froi tag needs to be updated
						xml.LoadXml(sXml);						
						XmlElement objElem = (XmlElement) xml.SelectSingleNode("//FROIOptions");
						objElem.Attributes.GetNamedItem("phone").InnerText=	sPhone;
						objElem.Attributes.GetNamedItem("name").InnerText=	sName;
						objElem.Attributes.GetNamedItem("title").InnerText=	sTitle;
						objElem.Attributes.GetNamedItem("skipprompt").InnerText = sRememberInfo;
                        objParam.Value = xml.OuterXml;
                        sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID = " + m_userId;
						break;						
					}
					case FROIOptions.iNoFieldData:
					{//3/14 sumit - we have a record but its blank
						sXml = CreateUserPrefXmlFroiPreparer(sPhone,sRememberInfo,sTitle,sName).OuterXml;
                        objParam.Value = sXml;
                        sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~  WHERE USER_ID = " + m_userId;
						break;
					}
					case FROIOptions.iNoRecord:
					{//3/14 sumit - we have no record for this user at all so its an inser case
						sXml = CreateUserPrefXmlFroiPreparer(sPhone,sRememberInfo,sTitle,sName).OuterXml;
                        objParam.Value = sXml;
						sSQL = "INSERT INTO USER_PREF_XML(PREF_XML,USER_ID) VALUES( ~XML~,"+ m_userId +")";
						break;
					}
				}
			
                //objConn = DbFactory.GetDbConnection(m_connectionString);
                //objConn.Open();
                //objCommand=objConn.CreateCommand();

                objCommand.Parameters.Add(objParam);
				objCommand.CommandText=sSQL;
				objCommand.ExecuteNonQuery();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIPreparerInfo.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
                if(objConn != null)
				    objConn.Dispose(); 
				objCommand=null;
                objParam = null;      
			}
			return true;
		}

		/// <summary>
		/// Used to create the user pref xml for froi preparer
		/// </summary>
		/// <param name="sPhone">phone num of preparer</param>
		/// <param name="sRememberInfo">Checkbox on UI for prompt</param>
		/// <param name="sTitle">title of preparer</param>
		/// <param name="sName">name of preparer</param>
		/// <returns>Xmldocument containing the structure for preparer</returns>
		private XmlDocument CreateUserPrefXmlFroiPreparer(string sPhone,string sRememberInfo,string sTitle,string sName)
		{
			XmlDocument xml = new XmlDocument();
			XmlNode rootNode = xml.CreateElement("setting");
			XmlNode childNode = xml.CreateElement("FROIOptions");
			childNode.Attributes.Append(xml.CreateAttribute("phone"));
			childNode.Attributes.Append(xml.CreateAttribute("name"));
			childNode.Attributes.Append(xml.CreateAttribute("title"));
			childNode.Attributes.Append(xml.CreateAttribute("skipprompt"));						
			childNode.Attributes.GetNamedItem("phone").InnerText=	sPhone;
			childNode.Attributes.GetNamedItem("name").InnerText=	sName;
			childNode.Attributes.GetNamedItem("title").InnerText=	sTitle;
			childNode.Attributes.GetNamedItem("skipprompt").InnerText = sRememberInfo;
			rootNode.AppendChild(childNode);
			xml.InnerXml = rootNode.OuterXml;
			return xml;
		}

	}
}
