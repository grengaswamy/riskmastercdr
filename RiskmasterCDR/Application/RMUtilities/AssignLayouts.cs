using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches User layout selection information.
	/// </summary>
	public class AssignLayouts
	{
		#region Input/Output Xml
		/*
		     <LayoutUsers>
				<TopDownUsers>
					<option value="u2">sarin parag (p)</option>
				</TopDownUsers>
				<TabLayoutUsers>
					<option value="u3">test test (test)</option>
				</TabLayoutUsers>
			</LayoutUsers>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// Secure DB Connection string
		/// </summary>
		private string m_sSecDSN="";
		/// <summary>
		/// DSN id
		/// </summary>
		private string m_sDsnId="";
        private int m_iClientId = 0;
		
		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		/// <summary>
		/// Secure DB Connection string
		/// </summary>
		public string SecDSN
		{
			set
			{
				m_sSecDSN=value;
			}	

		}
        public int ClientId
        {
            get
            {
                return m_iClientId;
            }
            set
            {
                m_iClientId = value;
            }
        }
		/// <summary>
		/// DSN id
		/// </summary>
		public string DsnId
		{
			set
			{
				m_sDsnId=value;
			}	

		}
		#endregion

		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public AssignLayouts()
		{
		}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		/// <param name="p_sDsnId">Dsn id</param>
		/// <param name="p_sSecureDSN">Secure DB Connection string</param>
        public AssignLayouts(string p_sDSN, string p_sDsnId, string p_sSecureDSN, int p_iClientId)
		{
			m_sDSN=p_sDSN;
			m_sDsnId=p_sDsnId;
			m_sSecDSN=p_sSecureDSN;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Public Functions
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves Layout information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			ArrayList objQuery=null;
			string sQuery="";
			bool bReturnValue=false;
			string sIsGroup="";
			XmlNode objTempNode=null;
			string[] arrTopDownUsers=null;
			string[] arrTabLayoutUsers=null;
			try
			{
				arrTopDownUsers=new string[0];
				arrTabLayoutUsers=new string[0];
				objQuery=new ArrayList();
				sQuery="DELETE FROM NET_LAYOUTS_MEMBERS";
				objQuery.Add(sQuery);
				objTempNode=p_objXmlDocument.SelectSingleNode("/LayoutUsers/TopDownUsers");
				if (objTempNode!=null)
				{
					if (objTempNode.InnerText!=null)
					{
						if (!objTempNode.InnerText.Trim().Equals(""))
						arrTopDownUsers=objTempNode.InnerText.Split(" ".ToCharArray());
					}
					for (int i=0;i<arrTopDownUsers.Length;i++)
					{
						sIsGroup=arrTopDownUsers[i].Substring(0,1);
						if (sIsGroup.Equals("g"))
						{
							sIsGroup="1";
						}
						else
						{
							sIsGroup="0";
						}

						sQuery="INSERT INTO NET_LAYOUTS_MEMBERS(LAYOUT_ID, MEMBER_ID, ISGROUP) VALUES(1,"+arrTopDownUsers[i].Substring(1)+","+sIsGroup+")";
						objQuery.Add(sQuery);
					}
				}
				objTempNode=p_objXmlDocument.SelectSingleNode("/LayoutUsers/TabLayoutUsers");
				if (objTempNode!=null)
				{
					if (objTempNode.InnerText!=null)
					{
						if (!objTempNode.InnerText.Trim().Equals(""))
						arrTabLayoutUsers=objTempNode.InnerText.Split(" ".ToCharArray());
					}
					for (int i=0;i<arrTabLayoutUsers.Length;i++)
					{
						sIsGroup=arrTabLayoutUsers[i].Substring(0,1);
						if (sIsGroup.Equals("g"))
						{
							sIsGroup="1";
						}
						else
						{
							sIsGroup="0";
						}

						sQuery="INSERT INTO NET_LAYOUTS_MEMBERS(LAYOUT_ID, MEMBER_ID, ISGROUP) VALUES(2,"+arrTabLayoutUsers[i].Substring(1)+","+sIsGroup+")";
						objQuery.Add(sQuery);
					}
				}
                UTILITY.Save(objQuery, m_sDSN, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("AssignLayouts.Save.Error",m_iClientId),p_objEx);
			}
			finally
			{
				objQuery=null;
				objTempNode=null;
				arrTopDownUsers=null;
				arrTabLayoutUsers=null;
			}
			return bReturnValue;
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get()
		{
			DbReader objReader=null;
			string sName="";
			string sFName="";
			string sLName="";
			XmlDocument objDOM=null;
			StringBuilder sbSql=null;
			StringBuilder sbTemp=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			try
			{
				sbSql=new StringBuilder();
				sbTemp=new StringBuilder();
				sbSql.Append("SELECT MEMBER_ID FROM NET_LAYOUTS_MEMBERS WHERE ISGROUP = 0 AND LAYOUT_ID = 2");
				objReader = DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				while  (objReader.Read())
				{
					if (sbTemp.ToString().Equals(""))
                        sbTemp.Append(Conversion.ConvertObjToStr(objReader.GetValue("MEMBER_ID")));
					else
						sbTemp.Append(","+Conversion.ConvertObjToStr(objReader.GetValue("MEMBER_ID")));
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				sbSql=new StringBuilder();
				if (!sbTemp.ToString().Equals(""))
				{
					sbSql.Append("SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID="+m_sDsnId);
					sbSql.Append(" AND USER_TABLE.USER_ID NOT IN ( "+sbTemp.ToString()+") ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
				}
				else
				{
					sbSql.Append("SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID="+m_sDsnId);
					sbSql.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
				}
				objDOM=new XmlDocument();

				
				objElemParent = objDOM.CreateElement("LayoutUsers");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("TopDownUsers");
				objDOM.FirstChild.AppendChild(objElemChild);
				objReader = DbFactory.GetDbReader(m_sSecDSN,sbSql.ToString());
				while (objReader.Read())
				{	
					objElemTemp=objDOM.CreateElement("option");
					objElemTemp.SetAttribute("value","u"+Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
					sLName=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
					sFName=Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
					sLName=sLName+" "+sFName;
					sName=Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
					sName=sLName+" ("+sName+")";
					objElemTemp.InnerText=sName;
					objElemChild.AppendChild(objElemTemp);
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}

				sbSql=new StringBuilder();
				sbTemp=new StringBuilder();
				sbSql.Append("SELECT MEMBER_ID FROM NET_LAYOUTS_MEMBERS WHERE ISGROUP = 0 AND LAYOUT_ID = 2");
				objReader = DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				while  (objReader.Read())
				{
					if (sbTemp.ToString().Equals(""))
						sbTemp.Append(Conversion.ConvertObjToStr(objReader.GetValue("MEMBER_ID")));
					else
						sbTemp.Append(","+Conversion.ConvertObjToStr(objReader.GetValue("MEMBER_ID")));
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				sbSql=new StringBuilder();
				if (!sbTemp.ToString().Equals(""))
				{
					sbSql.Append("SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID="+m_sDsnId);
					sbSql.Append(" AND USER_TABLE.USER_ID IN ( "+sbTemp.ToString()+") ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
				}
				
				objElemChild=objDOM.CreateElement("TabLayoutUsers");
				objDOM.FirstChild.AppendChild(objElemChild);
				if (!sbTemp.ToString().Equals(""))
				{
					objReader = DbFactory.GetDbReader(m_sSecDSN,sbSql.ToString());
					while (objReader.Read())
					{	
						objElemTemp=objDOM.CreateElement("option");
						objElemTemp.SetAttribute("value","u"+Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
						sLName=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
						sFName=Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
						sLName=sLName+" "+sFName;
						sName=Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
						sName=sLName+" ("+sName+")";
						objElemTemp.InnerText=sName;
						objElemChild.AppendChild(objElemTemp);
					}
					if (!objReader.IsClosed)
					{
						objReader.Close();
					}
				}
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("AssignLayouts.Get.Error",m_iClientId),p_objEx);
			}
			finally
			{
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
                if (objReader != null)
                    objReader.Dispose();
				objDOM=null;
				sbSql=null;
				sbTemp=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
			}
		}
		#endregion
	}
}
