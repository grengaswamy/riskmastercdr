/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 06/04/2014 | 33371   | ajohari2   | Created new class for TPA changes
**********************************************************************************************/

using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
    //MITS 33371 ajohari2
    /// <summary>
    ///Author  :   Ankit Johari
    ///Dated   :   7th,May 2014
    ///Purpose :   Policy Search Data Mapping List Form 
    /// </summary>
    public class PolicySearchDataMapping : UtilitiesUIListBase
    {
        /// <summary>
        /// Private database field mapping string array
        /// </summary>
        private string[,] arrFields = {
            {"RowId","ROW_ID"},
			{"PolSys","POLICY_SYSTEM_NAME"},
			{"AgentCode","AGENT_CODE"},
            {"MCO","MCO_CODE"},
            {"PCO","PCO_CODE"},
            {"BusUnit","BUS_UNIT_CODE"},
            {"BusSeg","BUS_SEGMENT_CODE"},
            {"SecGroup","GROUP_NAME"}
        	                               
        };

        private int m_iClientId = 0;

        /// Name   : Policy Search Data Mapping
        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014	
        ///************************************************************
        /// <summary>
        /// Default Constructor with Connection String
        /// </summary>
        /// <param name="p_sConnString">Input Connection String parameter</param>
        public PolicySearchDataMapping(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

        /// Name   : Policy Search Data Mapping
        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014		
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            base.TableName = "TPA_POLICY_PARMS,POLICY_X_WEB,ORG_SECURITY";
            base.KeyField = "ROW_ID";
            base.RadioName = "PolicySearchDataMappingList";
            base.WhereClause = "TPA_POLICY_PARMS.POLICY_SYSTEM_ID = POLICY_X_WEB.POLICY_SYSTEM_ID AND TPA_POLICY_PARMS.GROUP_ID = ORG_SECURITY.GROUP_ID ";
            base.OrderByClause = "POLICY_X_WEB.POLICY_SYSTEM_NAME,ORG_SECURITY.GROUP_NAME";
            base.InitFields(arrFields);
            base.Initialize();

        }

        /// Name   : Get
        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014		
        /// <summary>
        /// Get the data for the Policy Search Data Mapping list
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            LocalCache objCache = null;
            XmlNodeList objNodLst = null;
            XmlElement objElement = null;

            try
            {
                XMLDoc = p_objXmlDocument;
                base.Get();
                objNodLst = p_objXmlDocument.SelectNodes("//listrow");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                foreach (XmlElement objElm in objNodLst)
                {
                    foreach (XmlElement objChildElement in objElm.ChildNodes)
                    {
                        if (objChildElement.Name == "RowId")
                        {
                            objElement = objChildElement;
                        }
                    }
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchDataMapping.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                objNodLst = null;
            }
        }
    }
}