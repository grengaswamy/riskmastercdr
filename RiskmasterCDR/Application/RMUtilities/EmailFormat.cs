﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.IO;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   shobhana Khare
    ///Dated   :    June 2010 
    ///Purpose :   This class is used to  generalize the Email Format for different modules 
    ////// </summary>
    public class EmailFormat
    {
        #region Variable Declaration
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;

        private int m_iClientId = 0;
       
      //  private string m_sFormName = "";
        #endregion
        StreamReader strReader;
      //  private string m_sDSN = "";
        public EmailFormat(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
			this.Initialize();
		}
        private void Initialize()
        {
            try
            {
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROIOptions.Initialize.Error", m_iClientId), p_objEx);
            }
        }
		//skhare7 fn is transferred to Data Model Reserve current for R8
        //skhare7:This fn is used for reading the text file and replace the general fields to their values

        //public string GetEmailFormat(string p_sModuleName,int p_iClaimId)
        //{
        //    LocalCache objCache = null;
        //    //commented after review
        //    StringBuilder sbSQL;
            
        //    DbReader objRead = null;
        //    Claim objClaim = null;
        //    Claimant objPrimaryClaimant = null;
          
        //    ClaimAdjuster objCurAdj = null;
        //    Event objEvent = null;
           
        //      int iIndex=0;
        //      string sTextFormat = "";
        //      string sRSWstatus = "";
        //      try
        //      {
        //          if (p_sModuleName.Contains("_"))
        //              iIndex = p_sModuleName.IndexOf('_');
        //          sRSWstatus = p_sModuleName.Substring(iIndex + 1);
        //          p_sModuleName = p_sModuleName.Substring(0, iIndex);

        //          if (p_sModuleName == "RSWSheet")
        //          {
        //              if (sRSWstatus == "APP")
        //              {
        //                  strReader = new StreamReader(RMConfigurator.AppFilesPath + @"\RSWSheet\RSWAPP_EMAIL.txt");
        //                    sTextFormat = strReader.ReadToEnd();
        //              }

        //              else if (sRSWstatus == "REJ")
        //              {
        //                  strReader = new StreamReader(RMConfigurator.AppFilesPath + @"\RSWSheet\RSWREJ_EMAIL.txt");
        //                  sTextFormat = strReader.ReadToEnd();
        //              }
        //              else if (sRSWstatus == "SUB")
        //              {
        //                  strReader = new StreamReader(RMConfigurator.AppFilesPath + @"\RSWSheet\RSWSUB_EMAIL.txt");
        //                  sTextFormat = strReader.ReadToEnd();
        //              }
        //          }

                
        //          objClaim = (DataModel.Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
        //          objClaim.MoveTo(p_iClaimId);
        //          objPrimaryClaimant = objClaim.PrimaryClaimant;
                  
        //          objCurAdj = objClaim.CurrentAdjuster;
        //          objEvent = (objClaim.Parent as Event);
                 

        //          if (sTextFormat.Contains("CLAIM_NUMBER"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{CLAIM_NUMBER}",objClaim.ClaimNumber);
                  
        //          }
        //          if (sTextFormat.Contains("EVENT_NUMBER"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{EVENT_NUMBER}", objEvent.EventNumber);

        //          }
              
        //          if (sTextFormat.Contains("DATE_OF_CLAIM"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{DATE_OF_CLAIM}", Riskmaster.Common.Conversion.GetDBDateFormat(objClaim.DateOfClaim, "MM/dd/yyyy"));

        //          }
        //        //  MITS 22442
        //          if (sTextFormat.Contains("TIME_OF_CLAIM"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{TIME_OF_CLAIM}",Riskmaster.Common.Conversion.GetDBTimeFormat((objClaim.TimeOfClaim),"HH:mm:00"));

        //          }
        //          if (sTextFormat.Contains("FILE_NO"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{FILE_NO}", objClaim.FileNumber);

        //          }
                
        //          if (sTextFormat.Contains("SERVICE_CODE"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{SERVICE_CODE}", Convert.ToString(objClaim.ServiceCode));

        //          }
               
        //         if (sTextFormat.Contains("CLAIMANT_NAME"))
        //          {
        //              string sClaimantName = string.Empty;
        //              if (objPrimaryClaimant.ClaimantEntity.LastName != "" && objPrimaryClaimant.ClaimantEntity.FirstName != "")
        //              {
        //                  sClaimantName = objPrimaryClaimant.ClaimantEntity.FirstName + objPrimaryClaimant.ClaimantEntity.LastName;

        //              }
        //              else
        //              {
        //                  sClaimantName = objPrimaryClaimant.ClaimantEntity.LastName;
        //              }
        //              sTextFormat = sTextFormat.Replace("{CLAIMANT_NAME}", (sClaimantName));
        //          }

        //          if (sTextFormat.Contains("CURRENT_ADJUSTER"))
        //          {
        //              string sAdjusterName = string.Empty;
        //              if (objCurAdj.AdjusterEntity.LastName != "" && objCurAdj.AdjusterEntity.FirstName != "")
        //              {
        //                  sAdjusterName = objCurAdj.AdjusterEntity.FirstName + objCurAdj.AdjusterEntity.LastName;

        //              }
        //              else
        //              {
        //                  sAdjusterName =  objCurAdj.AdjusterEntity.LastName;
        //              }
        //              sTextFormat = sTextFormat.Replace("{CURRENT_ADJUSTER}", (sAdjusterName));
        //          }

        //          if (sTextFormat.Contains("EVENT_DATE"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{EVENT_DATE}", Riskmaster.Common.Conversion.GetDBDateFormat(objEvent.DateOfEvent, "MM/dd/yyyy"));

        //          }
        //          if (sTextFormat.Contains("EVENT_LOCATION"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{EVENT_LOCATION}", (objEvent.LocationAreaDesc));

        //          }
                
        //          if (sTextFormat.Contains("INJURY_FROM_DATE"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{INJURY_FROM_DATE}", Riskmaster.Common.Conversion.GetDBDateFormat(objEvent.InjuryFromDate,"MM/dd/yyyy"));

        //          }
        //          if (sTextFormat.Contains("INJURY_TO_DATE"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{INJURY_TO_DATE}", Riskmaster.Common.Conversion.GetDBDateFormat(objEvent.InjuryToDate, "MM/dd/yyyy"));

        //          }
        //        //  MITS 22442
        //          if (sTextFormat.Contains("TIME_OF_EVENT"))
        //          {
        //              sTextFormat = sTextFormat.Replace("{TIME_OF_EVENT}", Riskmaster.Common.Conversion.GetDBTimeFormat((objEvent.TimeOfEvent),"HH:mm:00"));

        //          }
        //         // MITS 22442
        //           sbSQL=  new StringBuilder();
        //           sbSQL.Append("SELECT CT.CODE_DESC  CLAIMTYPE,ET.CODE_DESC EVENTTYPE ,CL.CODE_DESC ");
        //           sbSQL.Append(" LOB FROM  CODES_TEXT CL INNER JOIN CLAIM ON CL.CODE_ID=CLAIM.LINE_OF_BUS_CODE ");
        //           sbSQL.Append(" INNER JOIN  CODES_TEXT CT on CLAIM.CLAIM_TYPE_CODE =CT.CODE_ID INNER JOIN ");
        //           sbSQL.Append("EVENT E on E.EVENT_ID=CLAIM.EVENT_ID inner join CODES_TEXT ");
        //           sbSQL.Append("  ET on ET.CODE_ID=E.EVENT_TYPE_CODE WHERE CLAIM_ID= " + p_iClaimId);

        //           DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString());
        //          if (objReader != null)
        //          {
        //              if (objReader.Read())
        //              {
        //                  //lLOB = objReader.GetInt32("LINE_OF_BUS_CODE");

        //                  if (sTextFormat.Contains("LINE_OF_BUSINESS"))
        //                  {
        //                      if ((objReader.GetValue("LOB")!=DBNull.Value))
        //                      {
        //                          sTextFormat = sTextFormat.Replace("{LINE_OF_BUSINESS}", objReader.GetString("LOB"));
        //                      }
                          
        //                  }
        //                  if (sTextFormat.Contains("CLAIM_TYPE"))
        //                  {
        //                      if(objReader.GetValue("CLAIMTYPE")!=DBNull.Value)
        //                      sTextFormat = sTextFormat.Replace("{CLAIM_TYPE}", objReader.GetString("CLAIMTYPE"));


        //                  }
        //                  if (sTextFormat.Contains("EVENT_TYPE"))
        //                  {
        //                      if (objReader.GetValue("EVENTTYPE") != DBNull.Value)
        //                      sTextFormat = sTextFormat.Replace("{EVENT_TYPE}", objReader.GetString("EVENTTYPE"));


        //                  }
        //              }
        //              objReader.Close();
        //              objReader.Dispose();
        //          }


        //         return sTextFormat;
        //      }
        //      catch (Exception ex)
        //      {
        //          throw new RMAppException("There is some problem in reading the text file.");
        //      }
        //      finally
        //      {
        //          if (objCache != null)
        //              objCache.Dispose();
        //          if (objRead != null)
        //              objRead.Close();
        //          sbSQL = null;
        //      }
            
           
        //}
    }//skhare7 fn is transferred to Data Model Reserve current for R8 End


}
