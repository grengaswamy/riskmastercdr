﻿/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 05/14/2015 | RMA-4606         | nshah28   | Import third party Currency Exchange Rates from flat file
 **********************************************************************************************/
using System;
using System.Data;
using System.Xml;
using Riskmaster.Db;
using System.Text;
using System.Globalization;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;


namespace Riskmaster.Application.RMUtilities
{
  ///<summary>
  ///Author  :   Ishan Jha
  ///Dated   :   12/16/2011
  ///Purpose :   This class Creates, Edits Multi Currency information.
  /// </summary>
  public class MultiCurrency
  {
    #region Private Functions
    private string m_sUserName = string.Empty;

    /// <summary>
    /// Connection String
    /// </summary>
    private string m_sConnectString = string.Empty;
    private int m_iClientId = 0;//sonali
    #endregion Private Functions

    #region Public Functions
    public MultiCurrency(string p_sLoginName, string p_sConnectString,int p_iClientId)//sonali
    {
      m_sUserName = p_sLoginName;
      m_sConnectString = p_sConnectString;
      m_iClientId = p_iClientId;
    }

    
    /// Name		: Get
    ///Author  :   Ishan Jha
    ///Dated   :   12/16/2011	
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// Function Gets data to populate Screen
    /// </summary>
    /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
    /// <returns>Xml containing the data to be populated on screen</returns>

    public XmlDocument Get(XmlDocument p_objXmlDocument)
    {
      XmlElement objElement = null;
      string sSQL = string.Empty;
      DbReader objRead = null;
      XmlElement objLstRow = null;
      XmlElement objRowTxt = null;
      string sSourceCurrencyId = string.Empty;
      string sDestinationCurrencyId = string.Empty;
      try
      {
        sSQL = "SELECT CURR_ROW_ID,EXCHANGE_RATE,DESTINATION_CURRENCY_CODE,SOURCE_CURRENCY_CODE,DESTINATIONCURR.SHORT_CODE DESTINATION , SOURCECURR.SHORT_CODE SOURCE FROM CURRENCY_RATE,CODES_TEXT SOURCECURR,CODES_TEXT DESTINATIONCURR where DESTINATIONCURR.CODE_ID = DESTINATION_CURRENCY_CODE and SOURCECURR.CODE_ID =SOURCE_CURRENCY_CODE ORDER BY CURR_ROW_ID";
        objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);

        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//ExchangeRateList");

        while (objRead.Read())
        {
          objLstRow = p_objXmlDocument.CreateElement("listrow");

          //we will use Conversion.ConvertObjToStr insteaof Cpnversion.CastToType as want the output as string and CastToType function accepts string as input
          objRowTxt = p_objXmlDocument.CreateElement("CurrencyRowId");
          objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("CURR_ROW_ID"));
          objLstRow.AppendChild(objRowTxt);


          objRowTxt = p_objXmlDocument.CreateElement("SourceCurrency");
          objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SOURCE"));
          objRowTxt.SetAttribute("codeid", sSourceCurrencyId);
          objLstRow.AppendChild(objRowTxt);
          

          objRowTxt = p_objXmlDocument.CreateElement("DestinationCurrency");
          objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("DESTINATION"));
          objRowTxt.SetAttribute("codeid", sDestinationCurrencyId);
          objLstRow.AppendChild(objRowTxt);

          objRowTxt = p_objXmlDocument.CreateElement("ExchangeRate");
          objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("EXCHANGE_RATE"));
          objLstRow.AppendChild(objRowTxt);

          objElement.AppendChild(objLstRow);
        }
        if (!objRead.IsClosed)
        {
          objRead.Close();
        }
        return p_objXmlDocument;
      }
      catch (Exception p_objEx)
      {
        throw new RMAppException(Globalization.GetString("MultiCurrency.Get.Error",m_iClientId), p_objEx);//sonali
      }
      finally
      {
        if (!objRead.IsClosed)
        {
          objRead.Close();
        }
        objRead = null;
      }
    }
    /// Name		: Save
    ///Author  :   Ishan Jha
    ///Dated   :   12/16/2011
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// Saves the data to the database
    /// </summary>
    /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
    /// <returns>True/False</returns>
    public XmlDocument Save(XmlDocument p_objXmlDocument)
    {
      string sRowId = string.Empty;
      string sSourceCurrencyId = string.Empty;
      string sDestinationCurrencyId = string.Empty;
      string sExchangeRate = string.Empty;
            string sExchangeRateDate = string.Empty; //JIRA RMA-4606 nshah28
      string sSQL = string.Empty;
      StringBuilder sbSql = new StringBuilder();
      int iNextUID = 0;
      XmlElement objElm = null;
      DbConnection objConn = null;
      CommonFunctions.CacheTable m_Cache = null;
      try
      {
        if (ValidateXMLData(p_objXmlDocument))
        {
        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CurrencyRowId']");
        if (objElm != null)
          sRowId = objElm.InnerText;

        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceCurrency']");
        if (objElm != null)
          sSourceCurrencyId = objElm.GetAttribute("codeid");

        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DestinationCurrency']");
        if (objElm != null)
          sDestinationCurrencyId = objElm.GetAttribute("codeid");


        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ExchangeRate']");
        if (objElm != null)
          sExchangeRate = objElm.InnerText;
        
                    //JIRA RMA-4606 nshah28 start(This node will come only when "CurrencyExchangeInterface.exe" will run
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ExchangeRateDate']");
                    if (objElm != null)
                        sExchangeRateDate = objElm.InnerText;
                    //JIRA RMA-4606 nshah28 end

        objConn = DbFactory.GetDbConnection(m_sConnectString);
        objConn.Open();

        if (sRowId != "")
        {
          string sLastUpdatedDttm = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));
          string sLastUpdated = Conversion.GetDate(DateTime.Now.ToString("yyyyMMdd"));
          //sSQL = "UPDATE CURRENCY_RATE SET EXCHANGE_RATE=" + sExchangeRate + " WHERE CURR_ROW_ID=" + sRowId;
                        // sSQL = string.Format("UPDATE CURRENCY_RATE SET SOURCE_CURRENCY_CODE = {0} , DESTINATION_CURRENCY_CODE = {1} , EXCHANGE_RATE = {2}, EFFECTIVE_DATE = {3}, DTTM_RCD_ADDED = {4} WHERE CURR_ROW_ID = {5}", sSourceCurrencyId, sDestinationCurrencyId, sExchangeRate, sLastUpdated, sLastUpdatedDttm, sRowId);  
                        //JIRA RMA-4606 nshah28 start
                        /* DbReader oReader = null;
                        sSQL = string.Format("SELECT CURR_ROW_ID,SOURCE_CURRENCY_CODE,DESTINATION_CURRENCY_CODE, EXCHANGE_RATE, EFFECTIVE_DATE, DTTM_RCD_ADDED , ADDED_BY_USER FROM CURRENCY_RATE WHERE CURR_ROW_ID = {0}",sRowId);
                       oReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                       while (oReader.Read())
                       {
                          //Get data from oReader and Insert into "CURRENCY_RATE_HIST"
                       } */

                        iNextUID = Utilities.GetNextUID(m_sConnectString, "CURRENCY_RATE_HIST", m_iClientId);
                        sSQL = string.Format("INSERT INTO CURRENCY_RATE_HIST(ROW_ID,CR_CURR_ROW_ID,CR_DESTINATION_CURRENCY_CODE,CR_SOURCE_CURRENCY_CODE,CR_EXCHANGE_RATE,CR_EXCHANGE_RATE_DATE,CR_EFFECTIVE_DATE,CR_DTTM_RCD_ADDED,CR_ADDED_BY_USER,CR_UPDATED_BY_USER,DTTM_RCD_ADDED,ADDED_BY_USER) SELECT {0},CURR_ROW_ID,DESTINATION_CURRENCY_CODE,SOURCE_CURRENCY_CODE,EXCHANGE_RATE,EXCHANGE_RATE_DATE,EFFECTIVE_DATE,DTTM_RCD_ADDED,ADDED_BY_USER,UPDATED_BY_USER,{1},'{2}' FROM CURRENCY_RATE where CURR_ROW_ID={3}", iNextUID, sLastUpdatedDttm, m_sUserName, sRowId);
                        objConn.ExecuteNonQuery(sSQL);

                        if (sExchangeRateDate == "") //If blank, then will not insert this. NULL will get inserted
                        {
                            sSQL = string.Format("UPDATE CURRENCY_RATE SET SOURCE_CURRENCY_CODE = {0} , DESTINATION_CURRENCY_CODE = {1} , EXCHANGE_RATE = {2}, EFFECTIVE_DATE = {3}, DTTM_RCD_ADDED = {4}, UPDATED_BY_USER='{5}' WHERE CURR_ROW_ID = {6}", sSourceCurrencyId, sDestinationCurrencyId, sExchangeRate, sLastUpdated, sLastUpdatedDttm, m_sUserName, sRowId);
                        }
                        else
                        {
                            sSQL = string.Format("UPDATE CURRENCY_RATE SET SOURCE_CURRENCY_CODE = {0} , DESTINATION_CURRENCY_CODE = {1} , EXCHANGE_RATE = {2}, EFFECTIVE_DATE = {3}, DTTM_RCD_ADDED = {4}, EXCHANGE_RATE_DATE={5}, UPDATED_BY_USER='{6}' WHERE CURR_ROW_ID = {7}", sSourceCurrencyId, sDestinationCurrencyId, sExchangeRate, sLastUpdated, sLastUpdatedDttm, sExchangeRateDate, m_sUserName, sRowId);
                        }
                        //JIRA RMA-4606 nshah28 end

                    }
                    else
                    {
            iNextUID = Utilities.GetNextUID(m_sConnectString, "CURRENCY_RATE", m_iClientId);
          sbSql = sbSql.Append("INSERT INTO CURRENCY_RATE");
                        //sbSql = sbSql.Append("(CURR_ROW_ID,SOURCE_CURRENCY_CODE,DESTINATION_CURRENCY_CODE, EXCHANGE_RATE, EFFECTIVE_DATE, DTTM_RCD_ADDED , ADDED_BY_USER)");
                        //JIRA RMA-4606 nshah28 start
                        sbSql = sbSql.Append("(CURR_ROW_ID,SOURCE_CURRENCY_CODE,DESTINATION_CURRENCY_CODE, EXCHANGE_RATE,EXCHANGE_RATE_DATE, EFFECTIVE_DATE, DTTM_RCD_ADDED , ADDED_BY_USER, UPDATED_BY_USER)");
                        //JIRA RMA-4606 nshah28 end
          sbSql = sbSql.Append(" VALUES(");
          sbSql = sbSql.Append(iNextUID.ToString());
          sbSql = sbSql.Append(",");
          sbSql = sbSql.Append(sSourceCurrencyId);
          sbSql = sbSql.Append(",");
          sbSql = sbSql.Append(sDestinationCurrencyId);
          sbSql = sbSql.Append(",");
          sbSql = sbSql.Append(sExchangeRate);
                        //JIRA RMA-4606 nshah28 start
                        sbSql = sbSql.Append(",");
                        sbSql = sbSql.Append(sExchangeRateDate == "" ? "NULL" : sExchangeRateDate);
                        //JIRA RMA-4606 nshah28 end
                        sbSql = sbSql.Append(",");
          sbSql = sbSql.Append("'" + Conversion.GetDate(DateTime.Now.ToString("yyyyMMdd")) + "'");
          sbSql = sbSql.Append(",");
          sbSql = sbSql.Append("'" + Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss")) + "'");
          sbSql = sbSql.Append(",");
          sbSql = sbSql.Append("'" + m_sUserName + "'");
                        //JIRA RMA-4606 nshah28 start
                        sbSql = sbSql.Append(",");
                        sbSql = sbSql.Append("'" + m_sUserName + "'");
                        //JIRA RMA-4606 nshah28 end
          sbSql = sbSql.Append(")");
          sSQL = sbSql.ToString();
        }

        objConn.ExecuteNonQuery(sSQL);
        m_Cache = new CommonFunctions.CacheTable(m_iClientId); 
        m_Cache.Remove(String.Format("{0}_{1}_{2}", sSourceCurrencyId, sDestinationCurrencyId, m_sConnectString));
        }
        else
        {
          throw new RMAppException(Globalization.GetString("CurrencyInfo.ValidateXMLData.AlreadyExists",m_iClientId));//sonali
        }
        return p_objXmlDocument;
      }
      catch (RMAppException p_objEx)
      {
        throw p_objEx;
      }
      catch (Exception p_objEx)
      {
        throw new RMAppException(Globalization.GetString("MultiCurrency.Save.Error",m_iClientId), p_objEx);//sonali
      }
      finally
      {
        objElm = null;
        if (objConn != null)
        {
          objConn.Dispose();
          objConn = null;
        }
        sbSql = null;
        if (m_Cache != null)
        {
            m_Cache = null;
        }
      }
    }

	private bool ValidateXMLData(XmlDocument p_objXmlDocument)
		{
      string sRowId = string.Empty;
      string sSourceCurrencyId = string.Empty;
      string sDestinationCurrencyId = string.Empty;
      string sSQL = string.Empty;
			DbReader objDBReader = null;

			try
			{
				sRowId = ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CurrencyRowId']")).InnerText;
				sSourceCurrencyId = ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceCurrency']")).GetAttribute("codeid");
				sDestinationCurrencyId = ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DestinationCurrency']")).GetAttribute("codeid");

				sSQL = "SELECT CURR_ROW_ID FROM CURRENCY_RATE WHERE SOURCE_CURRENCY_CODE = " + 
					sSourceCurrencyId + " AND DESTINATION_CURRENCY_CODE = " + sDestinationCurrencyId;
				
				objDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

				while(objDBReader.Read())
				{
          if (objDBReader["CURR_ROW_ID"].ToString() != sRowId)
            return false;
				}
				objDBReader.Close();

				return true;
			}
			finally
			{
				if(objDBReader != null)
					objDBReader.Dispose();
			}
		}
    /// Name		: GetSelectedMultCurrencyInfo
    /// Author		: Ishan Jha
    /// Date Created: 16/12/2011
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// Get the data for the Selected Multi Currency
    /// </summary>
    /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
    /// <returns>xml structure and data</returns>
    public XmlDocument GetSelectedMultiCurrencyInfo(XmlDocument p_objXmlDocument)
    {
      XmlElement objElement = null;
      string sSQL = string.Empty;
      DbReader objRead = null;
      string sStateName = string.Empty;
      string sSourceCurrencyId = string.Empty;
      string sDestinationCurrencyId = string.Empty;
      try
      {

        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='CurrencyRowId']");
        if (objElement != null)
        {
          if (objElement.InnerText != null)
          {
            if (objElement.InnerText.Trim() != "")
            {
              sSQL = "SELECT CURR_ROW_ID,EXCHANGE_RATE,DESTINATION_CURRENCY_CODE,SOURCE_CURRENCY_CODE,DESTINATIONCURR.SHORT_CODE DESTINATION , SOURCECURR.SHORT_CODE SOURCE FROM CURRENCY_RATE,CODES_TEXT SOURCECURR,CODES_TEXT DESTINATIONCURR where DESTINATIONCURR.CODE_ID = DESTINATION_CURRENCY_CODE and SOURCECURR.CODE_ID =SOURCE_CURRENCY_CODE and CURRENCY_RATE.CURR_ROW_ID= " + objElement.InnerText;
              objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);
              while (objRead.Read())
              {
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SourceCurrency']");
                sSourceCurrencyId = Conversion.ConvertObjToStr(objRead.GetValue("SOURCE_CURRENCY_CODE"));
                objElement.Attributes["codeid"].Value = sSourceCurrencyId;
                objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SOURCE"));


                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DestinationCurrency']");
                sDestinationCurrencyId = Conversion.ConvertObjToStr(objRead.GetValue("DESTINATION_CURRENCY_CODE"));
                objElement.Attributes["codeid"].Value = sDestinationCurrencyId;
                objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("DESTINATION"));


                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ExchangeRate']");
                objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("EXCHANGE_RATE"));
                             
              }
              if (!objRead.IsClosed)
              {
                objRead.Close();
              }
            }
          }
        }
        return p_objXmlDocument;
      }
      catch (Exception p_objEx)
      {
        throw new RMAppException(Globalization.GetString("MultiCurrency.GetSelectedMultiCurrencyInfo.Error",m_iClientId), p_objEx);//sonali
      }
      finally
      {
        if (objRead != null)
        {
          if (!objRead.IsClosed)
          {
            objRead.Close();
          }
          objRead = null;
        }
      }
    }
    /// Name		: Delete
    ///Author  :   Ishan Jha
    ///Dated   :   12/16/2011	
    ///************************************************************
    /// Amendment History
    ///************************************************************
    /// Date Amended   *   Amendment   *    Author
    ///************************************************************
    /// <summary>
    /// Deletes the MultiCurrency information
    /// </summary>
    /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
    /// <returns>True/False</returns>
    public XmlDocument Delete(XmlDocument p_objXmlDocument)
    {
      string sRowId = string.Empty;
      XmlElement objElm = null;
      DbConnection objConn = null;
      string sSQL = string.Empty;
            string sLastUpdatedDttm = string.Empty; //RMA-10698(Issue of 4606) nshah28 
            int iNextUID = 0;//RMA-10698(Issue of 4606) nshah28 
      try
      {
        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='MultiCurrencySetupGrid']");
        if (objElm != null)
          sRowId = objElm.InnerText;

                if (sRowId != "")
                {
                    objConn = DbFactory.GetDbConnection(m_sConnectString);
                    objConn.Open();

                    //RMA-10698(Issue of 4606) nshah28  start(Insert record in history table when user delete)
                    sLastUpdatedDttm = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));
                    iNextUID = Utilities.GetNextUID(m_sConnectString, "CURRENCY_RATE_HIST", m_iClientId);
                    sSQL = string.Format("INSERT INTO CURRENCY_RATE_HIST(ROW_ID,CR_CURR_ROW_ID,CR_DESTINATION_CURRENCY_CODE,CR_SOURCE_CURRENCY_CODE,CR_EXCHANGE_RATE,CR_EXCHANGE_RATE_DATE,CR_EFFECTIVE_DATE,CR_DTTM_RCD_ADDED,CR_ADDED_BY_USER,CR_UPDATED_BY_USER,DTTM_RCD_ADDED,ADDED_BY_USER) SELECT {0},CURR_ROW_ID,DESTINATION_CURRENCY_CODE,SOURCE_CURRENCY_CODE,EXCHANGE_RATE,EXCHANGE_RATE_DATE,EFFECTIVE_DATE,DTTM_RCD_ADDED,ADDED_BY_USER,UPDATED_BY_USER,{1},'{2}' FROM CURRENCY_RATE where CURR_ROW_ID={3}", iNextUID, sLastUpdatedDttm, m_sUserName, sRowId);
                    objConn.ExecuteNonQuery(sSQL);
                    //RMA-10698(Issue of 4606) nshah28  end

                    sSQL = "DELETE FROM CURRENCY_RATE WHERE CURR_ROW_ID=" + sRowId;
                    objConn.ExecuteNonQuery(sSQL);
                }
               
        return p_objXmlDocument;
      }
      catch (Exception p_objEx)
      {
        throw new RMAppException(Globalization.GetString("MultiCurrency.Delete.Error",m_iClientId), p_objEx);//sonali
      }
      finally
      {
        objElm = null;
        if (objConn != null)
        {
          objConn.Dispose();
          objConn = null;
        }
      }
    }
    # endregion Public Functions
  }
}
    