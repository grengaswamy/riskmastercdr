﻿
using Microsoft.Office.Interop.Excel;
using DAO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Threading;
using System.Data.OleDb;
using System.Xml;

using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	//File Import path in the Riskmaster.Config
//	<RMUtilities>
//	<WPAInfoDevFile>C:\RISKMASTER\WIZARD\CODE\RMUtilities\Files\wpainfo.def</WPAInfoDevFile>
//	<ImportFeeSchedulePath>C:\RISKMASTER\WIZARD\CODE\RMUtilities\Files\Brs</ImportFeeSchedulePath>
//	</RMUtilities>

	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   02nd,May 2005
	///Purpose :   Import Schedule Form Implementation
	/// </summary>
	[System.Runtime.InteropServices.GuidAttribute("1DA5F402-E298-4240-B2D2-95D83D73A306")]
	public class BRSImportSchedule:IDisposable 
	{
		/// <summary>
		/// Connection String variable
		/// </summary>
		private string m_sConnStr="";

		/// <summary>
		/// String file input path
		/// </summary>
		string m_sInpPath="";

		/// <summary>
		/// File output path
		/// </summary>
		string m_strBRSSourcePath = string.Empty, m_strBRSTargetPath = string.Empty;

		/// <summary>
		/// Dao Engine variable
		/// </summary>
		_DBEngine m_objdbEng = null;

		/// <summary>
		/// Dao Workspace variable
		/// </summary>
		Workspace m_objWs=null;

		/// <summary>
		/// Fee Schedule name variable
		/// </summary>
		string m_sFeeSchedName ="";

		/// <summary>
		/// Xml document input
		/// </summary>
		XmlDocument m_objXmlDoc=null;

        int m_iClientId = 0;//Add by kuladeep for Cloud.

        NameValueCollection m_RMUtilSettings = null;//rkaur27
        NameValueCollection m_BRSSettings = null;//rkaur27

		///<summary>
		/// abisht MITS 10623 Static Variable to handle exception in fee import thread.
		///</summary>
		private static string m_objEx = string.Empty;

		/// Name		: BRSImportSchedule
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
		/// <param name="p_objXmlDoc">xml document</param>
        public BRSImportSchedule(string p_sConnStr, XmlDocument p_objXmlDoc, int p_iClientId)
		{
			m_sConnStr = p_sConnStr;
			m_objdbEng = new DBEngineClass(); 
			m_objWs = m_objdbEng.CreateWorkspace("", "admin", "", WorkspaceTypeEnum.dbUseJet); 
			m_objXmlDoc= p_objXmlDoc;
			m_strBRSSourcePath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "BRSSourceFolder");
			m_strBRSTargetPath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "BRSTargetFolder");
            m_iClientId = p_iClientId;//Add by kuladeep for Cloud.
            m_RMUtilSettings = RMConfigurationManager.GetNameValueSectionSettings("RMUtilities", m_sConnStr, m_iClientId);//rkaur27
            m_BRSSettings = RMConfigurationManager.GetNameValueSectionSettings("BillingReviewSystem", m_sConnStr, m_iClientId);//rkaur27
            m_sInpPath = m_BRSSettings["ImportFeeSchedulePath"];//sonali
		}

		/// Name		: FillFeeSchdType
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Empty the ScheduleType combo variables depending on the provider
		/// </summary>
		/// <param name="p_objXmlDoc">Input Xml Document</param>
		/// <param name="p_sProvider">Provider String</param>
		private void FillFeeSchdType(XmlDocument p_objXmlDoc, string p_sProvider)
		{
			XmlNodeList objNodeLst=null;
			try
			{
				objNodeLst = p_objXmlDoc.SelectNodes("//control[@name='FeeSchedType']//level[@provider!='" + p_sProvider + "']");
				foreach(XmlElement objElm in objNodeLst)
				{
					p_objXmlDoc.RemoveChild(objElm);  
				}
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.FillFeeSchdType.Err", m_iClientId), p_objEx);   
			}
			finally
			{
				objNodeLst = null;
			}
		}

		/// Name		: ImportSchedule
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Main method for the Import of the schedule. Starts a new thread
		/// </summary>
		/// <returns>xml document with data</returns>
		public XmlDocument ImportSchedule()
		{
			XmlElement objElm=null;
			Database objSrcDb=null;
			Recordset objSrcRs=null;
			int iFeeSched = 0;
			string sSourcePath="";
			string sSQL="";

			try
			{
				objElm = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='FeeScheduleName']");
				if(objElm!=null)
					m_sFeeSchedName = objElm.InnerText;
				
				objElm = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='UploadStatus']");
				if(objElm!=null)
					if(objElm.InnerText=="1")
					{
						//Uploading
						sSQL = "SELECT USER_TABLE_NAME,TABLE_ID FROM FEE_TABLES WHERE USER_TABLE_NAME='" + m_sFeeSchedName + "'";
						using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
						{
							if (objRdr.Read())
							{
								if (objRdr.GetInt32("TABLE_ID") > 0)
								{
									//Import Sccessful
									objElm.InnerText = "-1";
									((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='StatusMessage']")).SetAttribute("title", "Fee Schedule  " + m_sFeeSchedName +
										Globalization.GetString("BRSImportSchedule.ImportSchedule.ImportSuccess", m_iClientId));
									//((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='ImgWait']")).SetAttribute("type","hidden");
								}
								else
								{
									//Import Going On
									((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='StatusMessage']")).SetAttribute("title",
                                        Globalization.GetString("BRSImportSchedule.ImportSchedule.Importing", m_iClientId));
								}
							}
							else
							{
								//Import failed
								objElm.InnerText = "0";
								//abisht MITS 10623
								if (m_objEx != string.Empty)
								{
									((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='StatusMessage']")).SetAttribute("title", m_objEx);
									((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='StatusMessage']")).SetAttribute("type", "message");
									((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='StatusMessage']")).SetAttribute("color", "red");
									//((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='ImgWait']")).SetAttribute("type", "hidden");
									m_objEx = string.Empty;
								}
								else
								{
									((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='StatusMessage']")).SetAttribute("title",
                                        Globalization.GetString("BRSImportSchedule.ImportSchedule.ImportFailed", m_iClientId));
									//((XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='ImgWait']")).SetAttribute("type","hidden");
								}
							}
						}//using
						return m_objXmlDoc;
					}

				using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT USER_TABLE_NAME FROM FEE_TABLES WHERE USER_TABLE_NAME='" + m_sFeeSchedName + "'"))
				{
					if (objRdr.Read())
					{
						throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportSchedule.DupSched", m_iClientId));   //csingh7 : BRSIssue
						//m_objXmlDoc.SelectSingleNode("//control[@name='ErrMsg']").InnerText = Globalization.GetString("BRSImportSchedule.ImportSchedule.DupSched");
						//return m_objXmlDoc; 
					}
				}//using
				sSourcePath = m_strBRSSourcePath;

				objElm = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='FeeScheduleType']");
				if(objElm!=null)
					iFeeSched = Conversion.ConvertStrToInteger(objElm.GetAttribute("value"));
				switch(iFeeSched)
				{
					case 2:
					case 13:
						if(!File.Exists(sSourcePath + "\\" + "WRKMAS.DBF"))	
						{
							throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportSchedule.MissingWrkMas", m_iClientId));    //csingh7 : BRSIssue
							//m_objXmlDoc.SelectSingleNode("//control[@name='ErrMsg']").InnerText = Globalization.GetString("BRSImportSchedule.ImportSchedule.MissingWrkMas");
							//return m_objXmlDoc;
						}
                        //zmohammad MITs 35939 : Commenting out this piece. It looks like something was meant to be implemented but just....fell apart midway. 
                        //else
                        //{
                            //objSrcDb  = m_objWs.OpenDatabase(sSourcePath,0,true,"dBASE IV;");
                            //objSrcRs = objSrcDb.OpenRecordset("WRKMAS", RecordsetTypeEnum.dbOpenForwardOnly ,
                            //    RecordsetOptionEnum.dbForwardOnly,LockTypeEnum.dbOptimistic);
                            //if(objSrcRs.Fields["STATE"].Value.ToString()=="NV")
                            //{
                            //    throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportSchedule.PaymentGrps"));    //csingh7 : BRSIssue
                            //    //m_objXmlDoc.SelectSingleNode("//control[@name='ErrMsg']").InnerText = Globalization.GetString("BRSImportSchedule.ImportSchedule.PaymentGrps");
                            //    //objSrcRs.Close();
                            //    //objSrcDb.Close();  
                            //    //return m_objXmlDoc;
                            //}
                        //}
						break;
				}
				
				BRSImportSchedule objImp = new BRSImportSchedule(m_sConnStr,m_objXmlDoc, m_iClientId); 				
				Thread thread = new Thread(new ThreadStart(objImp.ImportScheduleThread));
				thread.Start(); 
						
				sSQL ="INSERT INTO FEE_TABLES (USER_TABLE_NAME, TABLE_ID, " +
					"TABLE_TYPE, CALC_TYPE, ODBC_NAME, START_DATE, END_DATE, " +
					"CALCULATION_TYPE, STATE, PRIORITY, UCR_PERCENTILES, " +
					"DEFAULT_PERCENTILE) VALUES ('" +
					m_sFeeSchedName + "',0,0,0,'','','',0,0,0,0,0)";
				DbFactory.ExecuteNonQuery(m_sConnStr, sSQL);

				m_objXmlDoc.SelectSingleNode("//control[@name='UploadStatus']").InnerText="1";
				return m_objXmlDoc;
			}
			finally
			{
				
				if (objElm != null)
				{
					objElm = null;
				}

				if (objSrcDb != null)
				{
					objSrcDb = null;
				}

				if (objSrcRs != null)
				{
					objSrcRs = null;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="p_sXml"></param>
		/// <param name="sExt"></param>
		/// <returns></returns>
		public XmlDocument FileTranfer(string p_sXml,string sExt)
		{
			string sFilePath = "";
			string sFileName="";
			string sBinaryFormat="";
			XmlDocument objXmlDocument = null;

			try
			{
				sBinaryFormat=p_sXml;
				sFilePath = m_strBRSSourcePath;

				if(!Directory.Exists(sFilePath))
					Directory.CreateDirectory(sFilePath);   					
				sFileName=sFilePath+"\\"+sExt;

				char[] b64Data = sBinaryFormat.ToCharArray();
				byte[] decodeData = Convert.FromBase64CharArray(b64Data, 0, b64Data.Length);
				FileStream fs = new FileStream(sFileName, FileMode.Create, FileAccess.Write, FileShare.None);
				fs.Write(decodeData, 0, decodeData.Length);
				fs.Close();
			}
			catch (Exception p_objException)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.FileTransfer.Error", m_iClientId), p_objException);
			}
			
			return(objXmlDocument);				
		}

		/// Name		: ImportScheduleThread
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Main method for the Import of the schedule
		/// </summary>
		/// <param name="p_objXmlDoc">Xml input document with data and xml structure</param>
		/// <returns>xml document with data</returns>
		private void ImportScheduleThread()
		{
			#region local variables
			int iUcrPerc=0;
			int iDefPerc=0;
			int iNextUid=0;
			int iCalcType=0;
			int iFeeTblCount=0;
			int iProvider=0;
			int iFeeSchedType=0;
			int iNoOfFiles=0;
			int iStateId=0;
			string sSQL="";
			string sTargetDb="";
			string sSourceDb="";
			string sStartDt="";
			string sTargetDir="";
			DirectoryInfo objDI=null;
			XmlElement objProvider=null;
			XmlElement objFeeSchedType=null;
			XmlElement objElm =null;
			LocalCache objCache=null;
			#endregion

			try
			{
				sSourceDb = m_strBRSSourcePath;
				sTargetDir = m_strBRSTargetPath;
				if(!Directory.Exists(sTargetDir))
					Directory.CreateDirectory(sTargetDir);   

				objProvider = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='Provider']");
				if(objProvider!=null)
					iProvider = Conversion.ConvertStrToInteger(objProvider.GetAttribute("value") );

				objFeeSchedType = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='FeeScheduleType']");
				if(objFeeSchedType!=null)
					iFeeSchedType = Conversion.ConvertStrToInteger(objFeeSchedType.GetAttribute("value"));
 
				objElm = (XmlElement)m_objXmlDoc.SelectSingleNode("//control[@name='FeeScheduleName']");
				if(objElm!=null)
					m_sFeeSchedName = objElm.InnerText;

                sTargetDb = String.Format("{0}\\{1}.mdb", sTargetDir, m_sFeeSchedName); 

				//Check for duplicate fee schedule
				sSQL =  "SELECT FEESCHD_NAME FROM BRS_FEESCHD WHERE FEESCHD_NAME = '" + objElm.InnerText + "'";
                using (DbReader objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    if (objRdr.Read())
                        throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportSchedule.DupSched", m_iClientId));

                }//using
 
				objDI = new DirectoryInfo(sSourceDb);
			 
				switch(iProvider)
				{					
					case 1://ingenix/Medicode
						iNoOfFiles = objDI.GetFiles("*.dbf").Length;
						break;
					case 2://OWCP fee sched enhancement
						iNoOfFiles = objDI.GetFiles("*.xls").Length;
						break;
				}

				if(iNoOfFiles==0)
                    throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportSchedule.MissingDbf", m_iClientId));
   
				switch(iProvider)
				{
					case 1:
					switch(iFeeSchedType)
					{
						case 0:
							ImportUCR94(sSourceDb,sTargetDb);
							break;
						case 1:
							ImportUCR95(sSourceDb,sTargetDb);
							break;
						case 2:
							ImportWorkersComp(sSourceDb,sTargetDb, ref sStartDt, ref iStateId);
							break;
						case 3:
							ImportOutpatient(sSourceDb,sTargetDb);
							break;
						case 4:
							ImportHCPCS(sSourceDb,sTargetDb);
							break;
						case 7:
							ImportAnesthesia(sSourceDb,sTargetDb);
							break;
						case 8:
							ImportDental(sSourceDb,sTargetDb);
							break;
						case 13:
							ImportWC2000(sSourceDb,sTargetDb, ref sStartDt, ref iStateId);
							break;
					}
						break;
					case 2:
					switch(iFeeSchedType)
					{
						case 19:
							ImportOWCP(m_objXmlDoc,sSourceDb,sTargetDb);   
							break;
					}
						break;
				} 

				sSQL = "INSERT INTO BRS_FEESCHD (CALC_TYPE, DB_PATH, FEESCHD_NAME) VALUES" +
						" (" + iFeeSchedType + ",'" + sTargetDb +  "','" + m_sFeeSchedName + "')";
                DbFactory.ExecuteNonQuery(m_sConnStr, sSQL);
  
				sSQL = "SELECT COUNT(*) FROM FEE_TABLES WHERE ODBC_NAME = '" + m_sFeeSchedName + "'";
				iFeeTblCount = Conversion.ConvertObjToInt( DbFactory.ExecuteScalar(m_sConnStr, sSQL), m_iClientId);

				if(iFeeTblCount ==0)
				{
					if ( iFeeSchedType < 5  )
					{
						if ( iFeeSchedType!=2)
							iCalcType = 1;
						else
							iCalcType = 3;
					}
					else
					{
						if(iFeeSchedType>8)
							iCalcType = 3;
						else
						{
							if (iFeeSchedType ==7)
								iCalcType =4;
							else 
								iCalcType =7;
						}
					}

                    objCache = new LocalCache(m_sConnStr, m_iClientId);

					if(sStartDt =="")
						sStartDt = DateTime.Now.Year + "0101";
					if(iStateId==0)
					{
						if(iCalcType==3)
							iStateId=objCache.GetCodeId(m_sFeeSchedName.Substring(0,2).ToUpper(),objCache.GetTableName(46));    

					}
						
						if (iCalcType==1)
						{
							iUcrPerc = 1;
							iDefPerc = 60;
						}
						else
						{
							if(iCalcType==5)
							{
								iUcrPerc = 11;
								iDefPerc = 60;
							}
							else
							{
								iUcrPerc = 0;
								iDefPerc = 0;
							}
						}
				}
				sSQL ="DELETE FROM FEE_TABLES WHERE TABLE_ID=0 AND USER_TABLE_NAME='" + m_sFeeSchedName +  "'";
                DbFactory.ExecuteNonQuery(m_sConnStr, sSQL);

                iNextUid = Utilities.GetNextUID(m_sConnStr, "FEE_TABLES", m_iClientId);  
											
				sSQL ="INSERT INTO FEE_TABLES (USER_TABLE_NAME, TABLE_ID, " +
					"TABLE_TYPE, CALC_TYPE, ODBC_NAME, START_DATE, END_DATE, " +
					"CALCULATION_TYPE, STATE, PRIORITY, UCR_PERCENTILES, " +
					"DEFAULT_PERCENTILE) VALUES ('" +
					m_sFeeSchedName + "'," + iNextUid + ",1," + iFeeSchedType + ",'" + m_sFeeSchedName + "','" +
					sStartDt + "','00000000'," + iCalcType + "," + iStateId + ",1," + iUcrPerc + "," + iDefPerc + ")";
                DbFactory.ExecuteNonQuery(m_sConnStr, sSQL);
			}
			catch(RMAppException p_objEx)
			{
                //It's running in the thread, the exception may not be caught by the caller. Have to log here
                Exception ex = p_objEx;
                while (ex != null)
                {
                    Log.Write(ex.Message + " Stack Trace: " + ex.StackTrace, Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                    ex = ex.InnerException;
                }

				sSQL ="DELETE FROM FEE_TABLES WHERE TABLE_ID=0 AND USER_TABLE_NAME='" + m_sFeeSchedName +  "'";
                DbFactory.ExecuteNonQuery(m_sConnStr, sSQL);
				//abisht MITS 10623
				//if (p_objEx = Globalization.GetString("BRSImportSchedule.CopyRecords.Dbfmissing"))
				m_objEx = p_objEx.Message;
				//throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				//It's running in the thread, the exception may not be caught by the caller. Have to log here
				Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT,m_iClientId);

				sSQL ="DELETE FROM FEE_TABLES WHERE TABLE_ID=0 AND USER_TABLE_NAME='" + m_sFeeSchedName +  "'";
				DbFactory.ExecuteNonQuery(m_sConnStr,sSQL);

                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportSchedule.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if (objDI != null)
				{
					objDI = null;
				}
				if (objProvider != null)
				{
					objProvider = null;
				}
				if (objFeeSchedType != null)
				{
					objFeeSchedType = null;
				}

				if (objElm != null)
				{
					objElm = null;
				}
				if(objCache !=null)
				{
					objCache.Dispose();
					objCache =null;
				}
				Dispose();
			   
			}
		}

		/// Name		: ImportUCR94
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import UCR94
		/// </summary>
		/// <param name="p_sSourceDb">Source Database</param>
		/// <param name="p_sTargetDb">Target Database</param>
		private void ImportUCR94(string p_sSourceDb, string p_sTargetDb)
		{
			try
			{
				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);
				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				m_objWs.Close();
				m_objWs = null;

				string sODBCConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;", p_sTargetDb);

				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE UCRCPT (PROC_ TEXT(5),[DESC] TEXT(48))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE UCRFCT (GEOZIP TEXT(3),SPEC TEXT(2),BEGPROC TEXT(5)," +
					"ENDPROC TEXT(5),PER50 TEXT(7),PER60 TEXT(7),PER70 TEXT(7),PER75 TEXT(7),PER80 TEXT(7)," +
					"PER85 TEXT(7),PER90 TEXT(7),PER95 TEXT(7))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE UCRRVS (RVSID TEXT(3),PROC_ TEXT(5),RV TEXT(7),RVSUB TEXT(2))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE UCRZIP (BEGZIP TEXT(3),ENDZIP TEXT(3),GEOZIP TEXT(3)," +
					"RVSID TEXT(3))");

				List<string> oTableList = new List<string>();
				oTableList.Add("UCRCPT");
				oTableList.Add("UCRFCT");
				oTableList.Add("UCRRVS");
				oTableList.Add("UCRZIP");

				CopyFoxproRecords(p_sSourceDb, p_sTargetDb, oTableList);
				
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE UNIQUE INDEX INDEX_ONE ON UCRFCT (GEOZIP,SPEC,BEGPROC)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_TWO ON UCRFCT (BEGPROC)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_THREE ON UCRFCT(GEOZIP)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_FOUR ON UCRFCT(ENDPROC)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE UNIQUE INDEX INDEX_FIVE ON UCRCPT(PROC_)");
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportUCR94.Err", m_iClientId), p_objEx);
			}
			finally
			{
				
			}
		}

		/// Name		: ImportUCR95
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import UCR 95
		/// </summary>
		/// <param name="p_sSourceDb">Source Database</param>
		/// <param name="p_sTargetDb">Target Database</param>
		private void ImportUCR95(string p_sSourceDb,string p_sTargetDb)
		{
			try
			{
				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);
				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				m_objWs.Close();
				m_objWs = null;

				string sODBCConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;", p_sTargetDb);
				DbFactory.ExecuteNonQuery(sODBCConnectionString,"CREATE TABLE UCRCPTCE (PROC_ TEXT(5),[DESC] TEXT(48),SUBCODE TEXT(3))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE UCRFCT (GEOZIP TEXT(3),SPEC TEXT(2),BEGPROC TEXT(5)," +
					"ENDPROC TEXT(5),PER25 TEXT(7),PER30 TEXT(7),PER40 TEXT(7),PER50 TEXT(7),PER60 TEXT(7),PER70 TEXT(7),PER75 TEXT(7),PER80 TEXT(7),PER85 TEXT(7),PER90 TEXT(7),PER95 TEXT(7))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE UCRRVSCE (RVSID TEXT(3),PROC_ TEXT(5),RV TEXT(7)," +
					"RVSUB TEXT(2),MOD TEXT(2))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE UCRZIP (BEGZIP TEXT(3),ENDZIP TEXT(3),GEOZIP TEXT(3)," +
					"[DESC] TEXT(40),ZIPAREA TEXT(40),RVSID TEXT(3))");

				List<string> oTableList = new List<string>();
				oTableList.Add("UCRCPTCE");
				oTableList.Add("UCRFCT");
				oTableList.Add("UCRRVSCE");
				oTableList.Add("UCRZIP");

				CopyFoxproRecords(p_sSourceDb, p_sTargetDb, oTableList);
				
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE UNIQUE INDEX INDEX_ONE ON UCRFCT (GEOZIP,SPEC,BEGPROC)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_TWO ON UCRFCT (BEGPROC)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_THREE ON UCRFCT (GEOZIP)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_FOUR ON UCRFCT (ENDPROC)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE UNIQUE INDEX INDEX_FIVE ON UCRCPTCE (PROC_)");
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportUCR95.Err", m_iClientId), p_objEx);
			}
			finally
			{
			
			}
		}

		/// Name		: ImportWorkersComp
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import Workers Compensation
		/// </summary>
		/// <param name="p_sSourceDb">Source database</param>
		/// <param name="p_sTargetDb">Target database</param>
		/// <param name="p_sStartDate">Start date</param>
		/// <param name="p_iStateId">State id</param>
		private void ImportWorkersComp(string p_sSourceDb,string p_sTargetDb,ref string p_sStartDate,
			ref int p_iStateId)
		{
			string sStateCode="";
			string sStartYr="";
			LocalCache objCache = null;

			try
			{
				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);
	
				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				m_objWs.Close();
				m_objWs = null;

				string sODBCConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;", p_sTargetDb);

				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKFCT (STUDYID TEXT(3), STATEID TEXT(4), MODIFIER TEXT(3), " +
					"TOS TEXT(2), POS TEXT(2), SPEC TEXT(2), BEGCPT TEXT(6), ENDCPT TEXT(6), FACTOR TEXT(7))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKMAS (STATE TEXT(2), EFFDATE TEXT(6), FEEFLAG TEXT(1), " +
					"STUDY TEXT(3))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKMOD (STUDYID TEXT(3), STATEID TEXT(4), BEGCPT TEXT(6), " +
					"ENDCPT TEXT(6), MODIFIER TEXT(3), PERCENT TEXT(3))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKUVA (STUDYID TEXT(3), STATEID TEXT(4),CPT TEXT(6), " +
					"MODIFIER TEXT(3), TOS TEXT(2), POS TEXT(2),SPEC TEXT(2), [DESC] TEXT(48), " +
					"RLV TEXT(7), FOLLUP TEXT(3), SUBCODE TEXT(1))");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKZIP (STUDY TEXT(3), BEGZIP TEXT(5),ENDZIP TEXT(5), " +
					"STATEID TEXT(4), [DESC] TEXT(40), AREAS TEXT(40))");

				List<string> oTableList = new List<string>();
				oTableList.Add("WRKMAS");
				oTableList.Add("WRKZIP");
				oTableList.Add("WRKUVA");
				oTableList.Add("WRKFCT");
				oTableList.Add("WRKMOD");

				CopyFoxproRecords(p_sSourceDb, p_sTargetDb, oTableList);

				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_ONE ON WRKUVA (STUDYID, STATEID, CPT)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_TWO ON WRKUVA (CPT)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE UNIQUE INDEX INDEX_THREE ON WRKZIP (STUDY, BEGZIP, ENDZIP)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_FOUR ON WRKFCT (STUDYID, STATEID, BEGCPT, ENDCPT, SPEC)");
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_FIVE ON WRKMOD (STUDYID, STATEID, BEGCPT, ENDCPT, MODIFIER)");

				using (DbReader oReader = DbFactory.GetDbReader(sODBCConnectionString, "SELECT * FROM WRKMAS"))
				{
					if (oReader.Read())
					{
						sStateCode = oReader["STATE"].ToString();
						if (sStateCode.ToUpper() == "NV")
							GetNVGroupCPTAndAmount(p_sTargetDb);
					}
				}

				using (DbReader oReader = DbFactory.GetDbReader(sODBCConnectionString, "SELECT STATEID FROM WRKFCT"))
				{
					if (oReader.Read())
					{
						sStateCode = oReader["STATEID"].ToString().Substring(0, 2);
					}
					oReader.Close();
					if (!string.IsNullOrEmpty(sStateCode))
					{
						using (DbReader oDateReader = DbFactory.GetDbReader(sODBCConnectionString, string.Format("SELECT EFFDATE,STATE FROM WRKMAS WHERE STATE = '{0}'", sStateCode)))
						{
							if (oDateReader.Read())
							{
                                objCache = new LocalCache(m_sConnStr, m_iClientId);
								string sVal = oDateReader["EFFDATE"].ToString();
								sStartYr = sVal.Substring(sVal.Length - 2, 2);
								if (Conversion.ConvertStrToInteger(sStartYr) > 50)
									sStartYr = "19" + sStartYr;
								else
									sStartYr = "20" + sStartYr;
								p_sStartDate = sStartYr + sVal.Substring(0, 4);
								p_iStateId = objCache.GetCodeId(m_sFeeSchedName.Substring(0, 2).ToUpper(), objCache.GetTableName(46));
							}
						}
					}
				}
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportWorkersComp.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if (objCache != null)
					objCache.Dispose();
			}
		}

		/// Name		: ImportWC2000
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import Workers compensation 2000
		/// </summary>
		/// <param name="p_sSourceDb">Source Database</param>
		/// <param name="p_sTargetDb">Target Database</param>
		/// <param name="p_sStartDt">Start date</param>
		/// <param name="p_iStateId">State id</param>
		private void ImportWC2000(string p_sSourceDb,string p_sTargetDb,ref string p_sStartDt, ref int p_iStateId)
		{
			string sStateCode="";
			LocalCache objCache = null;

			try
			{
                if (File.Exists(p_sTargetDb))
                    File.Delete(p_sTargetDb);
                
                m_objWs.CreateDatabase(p_sTargetDb, LanguageConstants.dbLangGeneral,
                    DatabaseTypeEnum.dbDecrypt);
                m_objWs.Close();
                m_objWs = null;

                string sODBCConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;", p_sTargetDb);

                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKMAS (STATE TEXT(2), STUDYID TEXT(4),EFFDATE TEXT(10), " +
                    "FEEFLAG TEXT(1), PRODCODE TEXT(2))");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKZIP (STUDYID TEXT(4), STATEID TEXT(4),BEGZIP TEXT(5), " +
                    "ENDZIP TEXT(5), GEOZIP TEXT(3),[DESC] TEXT(40), PRODCODE TEXT(2))");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKUVA (STUDYID TEXT(4), STATEID TEXT(4), " +
                    "[PROC] TEXT(7), MODIFIER TEXT(3), TOS TEXT(2), POS TEXT(2), " +
                    "SPECIALTY TEXT(2), UNIQUESUB TEXT(2), [DESC] TEXT(255), RVUFEE TEXT(20), " +
                    "FOLLUP TEXT(3), SUBCODE TEXT(3), BYREPORT TEXT(2), " +
                    "STARRED TEXT(1), QUESTION TEXT(1), ASSISTSURG TEXT(1), " +
                    "NOTCOVERED TEXT(2), INDICATOR TEXT(2), FREQUENCY TEXT(10), " +
                    "PRODCODE TEXT(2))");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKFCT (STUDYID TEXT(4), STATEID TEXT(4), " +
                    "MODIFIER TEXT(3), TOS TEXT(2), POS TEXT(2), SPECIALTY TEXT(2), " +
                    "UNIQUESUB TEXT(2), BEGPROC TEXT(7), ENDPROC TEXT(7), " +
                    "FACTOR TEXT(8), PRODCODE TEXT(2))");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE WRKMOD (STUDYID TEXT(4), STATEID TEXT(4), " +
                    "BEGPROC TEXT(7), ENDPROC TEXT(7), MODIFIER TEXT(3), " +
                    "PERCENT TEXT(6), UNIT TEXT(5), PRODCODE TEXT(2))");

                List<string> oTableList = new List<string>();
                oTableList.Add("WRKMAS");
                oTableList.Add("WRKZIP");
                oTableList.Add("WRKUVA");
                oTableList.Add("WRKFCT");
                oTableList.Add("WRKMOD");

                CopyFoxproRecords(p_sSourceDb, p_sTargetDb, oTableList);

                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_ONE ON " +
                    "WRKUVA (STUDYID, STATEID, PROC)");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_TWO ON WRKUVA (PROC)");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE UNIQUE INDEX INDEX_THREE ON " +
                    "WRKZIP (STUDYID, BEGZIP, ENDZIP)");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_FOUR ON " +
                    "WRKFCT (STUDYID, STATEID, BEGPROC, ENDPROC, SPECIALTY)");
                DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE INDEX INDEX_FIVE ON " +
                    "WRKMOD (STUDYID, STATEID, BEGPROC, ENDPROC, MODIFIER)");

                sStateCode = string.Empty;
                using (DbReader oReader = DbFactory.GetDbReader(sODBCConnectionString, "SELECT * FROM WRKMAS"))
                {
                    if (oReader.Read())
                    {
                        sStateCode = oReader["STATE"].ToString().Substring(0, 2);
                    }
                }
                if (sStateCode.ToUpper() == "NV")
                {
                    GetNVGroupCPTAndAmount(p_sTargetDb);
                }

                sStateCode = string.Empty;
                using (DbReader oReader = DbFactory.GetDbReader(sODBCConnectionString, "SELECT * FROM WRKFCT"))
                {
                    if (oReader.Read())
                    {
                        sStateCode = oReader["STATEID"].ToString().Substring(0, 2);
                    }
                }
                if (!string.IsNullOrEmpty(sStateCode))
                {
                    using (DbReader oReader = DbFactory.GetDbReader(sODBCConnectionString, string.Format("SELECT * FROM WRKMAS WHERE STATE='{0}'", sStateCode)))
                    {
                        objCache = new LocalCache(m_sConnStr, m_iClientId);
                        if (oReader.Read())
                        {
                            p_sStartDt = Conversion.GetDate(oReader["EFFDATE"].ToString());
                            p_iStateId = objCache.GetStateRowID(oReader["STATE"].ToString());
                        }
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportWC2000.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }

		/// Name		: ImportOutpatient
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import Outpatient
		/// </summary>
		/// <param name="p_sSourceDb">Source database</param>
		/// <param name="p_sTargetDb">Target database</param>
		private void ImportOutpatient(string p_sSourceDb,string p_sTargetDb)
		{
			Database objSrcDb=null;
			Database objDesDb=null;

			try
			{
				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);

				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				objDesDb = m_objWs.OpenDatabase(p_sTargetDb,false,false,"");  			
				objSrcDb  = m_objWs.OpenDatabase(p_sSourceDb,0,true,"dBASE IV;");
				objDesDb.Execute("CREATE TABLE OPUCPTCE (PROC_ TEXT(5),[DESC] TEXT(48),SUBCODE TEXT(3))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE OPUFCT (GEOZIP TEXT(3),SPEC TEXT(2),BEGPROC TEXT(5)," + 
					"ENDPROC TEXT(5),PER25 TEXT(7),PER30 TEXT(7),PER40 TEXT(7),PER50 TEXT(7),PER60 TEXT(7)," + 
					"PER70 TEXT(7),PER75 TEXT(7),PER80 TEXT(7),PER85 TEXT(7),PER90 TEXT(7),PER95 TEXT(7))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE OPURVSCE (RVSID TEXT(3),PROC_ TEXT(5),RV TEXT(7)," + 
					"RVSUB TEXT(2),MOD TEXT(2))",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE OPUSPC (SPEC TEXT(2),[DESC] TEXT(30))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE OPUWRD (WORD TEXT(15),CODE TEXT(5),COUNT DOUBLE)",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE OPUZIP (BEGZIP TEXT(3),ENDZIP TEXT(3),GEOZIP TEXT(3)," + 
					"[DESC] TEXT(40),ZIPAREA TEXT(40),RVSID TEXT(3))",DatabaseTypeEnum.dbDecrypt);
				CopyRecords(objSrcDb,objDesDb);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_ONE ON OPUFCT (GEOZIP,SPEC,BEGPROC)",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_TWO ON OPUFCT (BEGPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_THREE ON OPUFCT (GEOZIP)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_FOUR ON OPUFCT (ENDPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_FIVE ON OPUCPTCE (PROC_)",DatabaseTypeEnum.dbDecrypt);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportOutpatient.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objSrcDb!=null)
				{
					objSrcDb.Close();
					objSrcDb=null;
				}
				if(objSrcDb!=null)
				{
					objDesDb.Close(); 
					objDesDb=null;
				}
			}
		}

		/// Name		: ImportHCPCS
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// An error occurred in Importing HCPCS
		/// </summary>
		/// <param name="p_sSourceDb">Source Database</param>
		/// <param name="p_sTargetDb">Target Database</param>
		private void ImportHCPCS(string p_sSourceDb,string p_sTargetDb)
		{
			Database objSrcDb=null;
			Database objDesDb=null;

			try
			{
				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);

				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				objDesDb = m_objWs.OpenDatabase(p_sTargetDb,false,false,"");  			
				objSrcDb  = m_objWs.OpenDatabase(p_sSourceDb,0,true,"dBASE IV;");
				objDesDb.Execute("CREATE TABLE HCPCPTCE (PROC_ TEXT(5),[DESC] TEXT(48),SUBCODE TEXT(3))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE HCPFCT (GEOZIP TEXT(3),SPEC TEXT(2),BEGPROC TEXT(5)," + 
					"ENDPROC TEXT(5),PER25 TEXT(7),PER30 TEXT(7),PER40 TEXT(7),PER50 TEXT(7),PER60 TEXT(7),PER70 TEXT(7),PER75 TEXT(7),PER80 TEXT(7),PER85 TEXT(7),PER90 TEXT(7),PER95 TEXT(7))",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE HCPRVSCE (RVSID TEXT(3),PROC_ TEXT(5),RV TEXT(7)," + 
					"RVSUB TEXT(2),MOD TEXT(2))",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE HCPSPC (SPEC TEXT(2),[DESC] TEXT(30))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE HCPWRD (WORD TEXT(15),CODE TEXT(5),COUNT DOUBLE)",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE HCPZIP (BEGZIP TEXT(3),ENDZIP TEXT(3),GEOZIP TEXT(3)," + 
					"[DESC] TEXT(40),ZIPAREA TEXT(40),RVSID TEXT(3))",DatabaseTypeEnum.dbDecrypt);
				CopyRecords(objSrcDb,objDesDb);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_ONE ON HCPFCT (GEOZIP,SPEC,BEGPROC)",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_TWO ON HCPFCT (BEGPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_THREE ON HCPFCT (GEOZIP)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_FOUR ON HCPFCT (ENDPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_FIVE ON HCPCPTCE (PROC_)",DatabaseTypeEnum.dbDecrypt);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportHCPCS.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objSrcDb!=null)
				{
					objSrcDb.Close();
					objSrcDb=null;
				}
				if(objSrcDb!=null)
				{
					objDesDb.Close(); 
					objDesDb=null;
				}
			}
		}

		/// Name		: ImportAnesthesia
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import Anesthesia
		/// </summary>
		/// <param name="p_sSourceDb">Source Database</param>
		/// <param name="p_sTargetDb">Target Database</param>
		private void ImportAnesthesia(string p_sSourceDb,string p_sTargetDb)
		{
			Database objSrcDb=null;
			Database objDesDb=null;  

			try
			{
				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);

				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				objDesDb = m_objWs.OpenDatabase(p_sTargetDb,false,false,"");  			
				objSrcDb  = m_objWs.OpenDatabase(p_sSourceDb,0,true,"dBASE IV;");
				objDesDb.Execute("CREATE TABLE ANECPT (CPT TEXT(5),SUBCODE TEXT(3),[DESC] TEXT(48))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE ANECRX (CPT TEXT(5),ANE TEXT(5),ANESUB TEXT(2))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE ANEFCT (GEOZIP TEXT(3),SPEC TEXT(2),BEGPROC TEXT(5)," + 
					"ENDPROC TEXT(5),LOBASE TEXT(7),HIBASE TEXT(7),LOMIN TEXT(7),HIMIN TEXT(7))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE ANERVS (RVSID TEXT(3),CPT TEXT(5),RV TEXT(7)," + 
					"RVSUB TEXT(2),MOD TEXT(2))",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE ANEZIP (BEGZIP TEXT(3),ENDZIP TEXT(3),GEOZIP TEXT(3)," + 
					"[DESC] TEXT(40),ZIPAREA TEXT(40),RVSID TEXT(3))",DatabaseTypeEnum.dbDecrypt);
				CopyRecords(objSrcDb,objDesDb);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_ONE ON ANEFCT (GEOZIP,SPEC,BEGPROC)",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_TWO ON ANEFCT (BEGPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_THREE ON ANEFCT (GEOZIP)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_FOUR ON ANEFCT (ENDPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_FIVE ON ANECPT(CPT)",DatabaseTypeEnum.dbDecrypt);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportAnesthesia.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objSrcDb!=null)
				{
					objSrcDb.Close();
					objSrcDb=null;
				}
				if(objSrcDb!=null)
				{
					objDesDb.Close(); 
					objDesDb=null;
				}
			}
		}

		/// Name		: ImportDental
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import Dental
		/// </summary>
		/// <param name="p_sSourceDb">Source Database</param>
		/// <param name="p_sTargetDb">Target Database</param>
		private void ImportDental(string p_sSourceDb,string p_sTargetDb)
		{
			Database objSrcDb=null;
			Database objDesDb=null;

			try
			{
				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);

				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				objDesDb = m_objWs.OpenDatabase(p_sTargetDb,false,false,"");  			
				objSrcDb  = m_objWs.OpenDatabase(p_sSourceDb,0,true,"dBASE IV;");
				objDesDb.Execute("CREATE TABLE DENADACE (PROC_ TEXT(5),[DESC] TEXT(48),SUBCODE TEXT(3))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE DENFCT (GEOZIP TEXT(3),SPEC TEXT(2),BEGPROC TEXT(5)," + 
					"ENDPROC TEXT(5),PER25 TEXT(7),PER30 TEXT(7),PER40 TEXT(7),PER50 TEXT(7),PER60 TEXT(7)," + 
					"PER70 TEXT(7),PER75 TEXT(7),PER80 TEXT(7),PER85 TEXT(7),PER90 TEXT(7),PER95 TEXT(7))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE DENRVSCE (RVSID TEXT(3),PROC_ TEXT(5),RV TEXT(7)," + 
					"RVSUB TEXT(2),MOD TEXT(2))",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE DENSPC (SPEC TEXT(2),[DESC] TEXT(30))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE DENWRD (WORD TEXT(15),CODE TEXT(5),COUNT DOUBLE)",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE DENZIP (BEGZIP TEXT(3),ENDZIP TEXT(3),GEOZIP TEXT(3)," + 
					"[DESC] TEXT(40),ZIPAREA TEXT(40),RVSID TEXT(3))",DatabaseTypeEnum.dbDecrypt);
				CopyRecords(objSrcDb,objDesDb);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_ONE ON DENFCT(GEOZIP,SPEC,BEGPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_TWO ON DENFCT(BEGPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_THREE ON DENFCT (GEOZIP)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_FOUR ON DENFCT (ENDPROC)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE UNIQUE INDEX INDEX_FIVE ON DENADACE (PROC_)",DatabaseTypeEnum.dbDecrypt);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportDental.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objSrcDb!=null)
				{
					objSrcDb.Close();
					objSrcDb=null;
				}
				if(objSrcDb!=null)
				{
					objDesDb.Close(); 
					objDesDb=null;
				}
			}
		}

		/// Name		: ImportOWCP
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import OWCP
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document</param>
		/// <param name="p_sSourceDb">Source database</param>
		/// <param name="p_sTargetDb">Target database</param>
		private void ImportOWCP(XmlDocument p_objXmlDoc, string p_sSourceDb,string p_sTargetDb)
		{
			string sRVUname="";
			string sGPCIname="";
			string sModsname="";
			XmlElement objElm = null;
			Database objSrcDb=null;
			Database objDesDb=null;

			try
			{
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='CptRvu']");
				if(objElm!=null)
					sRVUname =  objElm.InnerText;

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Gpci']");
				if(objElm!=null)
					sGPCIname =  objElm.InnerText;

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Mods']");
				if(objElm!=null)
					sModsname =  objElm.InnerText;

				if(File.Exists(p_sTargetDb))
					File.Delete(p_sTargetDb);

				m_objWs.CreateDatabase( p_sTargetDb,LanguageConstants.dbLangGeneral,DatabaseTypeEnum.dbDecrypt);    
				objDesDb = m_objWs.OpenDatabase(p_sTargetDb,false,false,""); 
				objSrcDb  = m_objWs.OpenDatabase(p_sSourceDb,0,true,"dBASE IV;");
				objDesDb.Execute("CREATE TABLE CODE_RVU_CF (CPT TEXT(5),STATUS TEXT(1),MODIFIER TEXT(2)," + 
					"MOD_LEVEL TEXT(2), WORK_RVU FLOAT, NF_PE_RVU FLOAT, PE_RVU FLOAT, MPE_RVU FLOAT, " + 
					"CF FLOAT, END_DATE TEXT(10) ,[DESC] TEXT(100))",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE GPCI_BY_ZIP (ZIP TEXT(5),STATE TEXT(2),MSA TEXT(5)," + 
					"WORK_GPCI FLOAT,PE_GPCI FLOAT,ME_GPCI FLOAT,MSA_NAME TEXT(50))",
					DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE TABLE MODS (MOD_LEVEL TEXT(2),MODIFIER TEXT(2),MULTIPLIER LONG)",
					DatabaseTypeEnum.dbDecrypt);
				ImportExcelFile(objDesDb, p_sSourceDb, sRVUname, "CODE_RVU_CF", 1,
					"CPT,STATUS,MODIFIER,MOD_LEVEL,WORK_RVU,NF_PE_RVU,PE_RVU,MPE_RVU,CF,END_DATE,[DESC]", 1, 13, 11);
				ImportExcelFile(objDesDb, p_sSourceDb, sGPCIname, "GPCI_BY_ZIP", 1,
					"ZIP,STATE,MSA,WORK_GPCI,PE_GPCI,ME_GPCI,MSA_NAME", 1, 8, 7);
				ImportExcelFile(objDesDb, p_sSourceDb, sModsname, "MODS", 1, "MOD_LEVEL,MODIFIER,MULTIPLIER", 1, 9, 3);

				objDesDb.Execute("CREATE INDEX INDEX_ONE ON CODE_RVU_CF(CPT)",DatabaseTypeEnum.dbDecrypt);
				objDesDb.Execute("CREATE INDEX INDEX_TWO ON GPCI_BY_ZIP(ZIP)",DatabaseTypeEnum.dbDecrypt);
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportOWCP.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objSrcDb!=null)
				{
					objSrcDb.Close();
					objSrcDb=null;
				}
				if(objSrcDb!=null)
				{
					objDesDb.Close(); 
					objDesDb=null;
				}
			}
		}

		/// Name		: ImportExcelFile
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Import Excel file
		/// </summary>
		/// <param name="p_objTargetDb">Target database</param>
		/// <param name="p_sSourcePath">Source path</param>
		/// <param name="p_sSourceFile">Source file</param>
		/// <param name="p_sTargetTable">Target table</param>
		/// <param name="p_iWorkSheet">Worksheet number</param>
		/// <param name="p_sFieldLst">Field list</param>
		/// <param name="p_iStartCol">Start column</param>
		/// <param name="p_iStartRow">Start row</param>
		/// <param name="p_iEndCol">End column</param>
		private void ImportExcelFile(Database p_objTargetDb, string p_sSourcePath, 
			string p_sSourceFile, string p_sTargetTable, int p_iWorkSheet, string p_sFieldLst, 
			int p_iStartCol, int p_iStartRow, int p_iEndCol)
		{
			int iRow=0;
			StringBuilder  sSQL=null;
			StringBuilder  sTmp=null;
			StringBuilder  sValues=null;
			ApplicationClass excelApp = null;
			Workbook newWorkbook = null;
			Worksheet excelWorksheet = null;
			object objRange=null;

			try
			{
				sSQL = new StringBuilder();
				sTmp = new StringBuilder();
				sValues = new StringBuilder();
				iRow = p_iStartRow; 
				excelApp = new ApplicationClass();
				newWorkbook = excelApp.Workbooks.Open(p_sSourcePath + "\\" + p_sSourceFile ,
					0, false, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "",
					true, false, 0, true, false, false);				
				excelWorksheet = (Worksheet)newWorkbook.Worksheets[p_iWorkSheet];
				objRange = ((Range)excelWorksheet.Cells[iRow, p_iStartCol]).Value2 ;
				while(objRange!=null)
				{
					sSQL.Remove(0,sSQL.Length); 
					sValues.Remove(0,sValues.Length); 
					sSQL.Append( "INSERT INTO " + p_sTargetTable + " (" + p_sFieldLst + ")" + " VALUES (");
					for(int iCol = 1; iCol <= p_iEndCol; iCol ++)
					{
						sTmp.Remove(0,sTmp.Length);
						Object objR = ((Range)excelWorksheet.Cells[iRow, iCol]).Value2;
						if(objR!=null)
							sTmp.Append( objR.ToString());						
						sTmp = sTmp.Replace("'","''");  

						if(iCol!= p_iStartCol)
							sValues.Append ( ",");

						if(Conversion.ConvertStrToInteger(sTmp.ToString()).ToString() == sTmp.ToString() ) 
							sValues.Append(sTmp);
						else
							sValues.Append("'" + sTmp + "'");
						objR = null;
					}
					sSQL.Append(sValues + ")");
					p_objTargetDb.Execute(sSQL.ToString(),DatabaseTypeEnum.dbDecrypt);
					iRow++;
					objRange = ((Range)excelWorksheet.Cells[iRow, p_iStartCol]).Value2;
				}
				newWorkbook.Close(System.Reflection.Missing.Value,System.Reflection.Missing.Value, 
					System.Reflection.Missing.Value);
				excelApp.Quit(); 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportExcelFile.ImportErr", m_iClientId), p_objEx);   
			}
			finally
			{				
				sSQL=null;
				sTmp=null;
				sValues=null;
				excelWorksheet = null;
				newWorkbook = null;
				excelApp = null;
				objRange=null;
			}
		}

		/// Name		: CopyRecords
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Copy the records
		/// </summary>
		/// <param name="p_objSourceDb">Source Database</param>
		/// <param name="p_objTargetDb">Destination Database</param>
		private void CopyRecords(Database p_objSourceDb, Database p_objTargetDb)
		{
			bool bFirstCol=false;
			string sSQL="";
			string sSQLStudy="";
			string sFileMissing = string.Empty; //abisht MITS 10623
			Recordset objDestRs=null;
			Recordset objSrcRs=null;

			try
			{
				foreach(TableDef  objSrc in p_objSourceDb.TableDefs)
				{
					if(objSrc.Name.ToUpper()=="WRKFCT" )
					{
						sSQLStudy = "STUDYID";
						foreach(Field objFld  in objSrc.Fields)
						{
							if(objFld.Name.ToUpper()=="STUDY")
								sSQLStudy = "STUDY";
						}
					}
				}
				

				//abisht MITS 10623
				foreach(TableDef objTarget in p_objTargetDb.TableDefs)
				{
					if (objTarget.Attributes ==0)
					{
						if(!File.Exists(p_objSourceDb.Name + "\\" +	objTarget.Name + ".dbf") ) 
						{
							if (!File.Exists(p_objSourceDb.Name + "\\" + objTarget.Name + ".dbf"))
							{
								
								if (sFileMissing == string.Empty)
								{
									sFileMissing = objTarget.Name + ".dbf";
								}
								else
								{
									sFileMissing = sFileMissing + ", " + objTarget.Name + ".dbf";
								}
							}
						}
					}
				}
				foreach(TableDef objTarget in p_objTargetDb.TableDefs)
				{
					if (objTarget.Attributes ==0)
					{
						//if (!File.Exists(p_objSourceDb.Name + "\\" + objTarget.Name + ".dbf"))
						//{
						//    //abisht MITS 10623 COMMENTED CODE
						//    throw new RMAppException(sFileMissing);
						//    //throw new RMAppException(Globalization.GetString("BRSImportSchedule.CopyRecords.Dbfmissing"));
						//}
						if (sFileMissing != string.Empty)
						{
							string sFileMissingExp = "The fee tables " + sFileMissing + " is missing. Please upload all the files and try again. Please make sure you have uploaded all files needed for importing the fee schedule before clicking on the finish button.";
							sFileMissing = string.Empty;
							throw new RMAppException(sFileMissingExp);
						}
						//abisht MITS 10623 END OF CHANGES
						sSQL = "SELECT ";
						bFirstCol = true;
						foreach(Field objFld in objTarget.Fields)
						{
							if (bFirstCol)
								bFirstCol =false;
							else if(objFld.Name!="")
								sSQL += ",";
							if(objFld.Name.ToUpper()=="MOD")
								sSQL += "[MOD]";
							else if (objFld.Name == "RVUFEE")
								sSQL += "[RVU/FEE]";
							else if (objFld.Name == "STUDYID")
								sSQL += sSQLStudy;
							else
								sSQL += objFld.Name; 
						}
						sSQL += " FROM " + objTarget.Name;

//						objSrcRs = p_objSourceDb.OpenRecordset(sSQL, RecordsetTypeEnum.dbOpenForwardOnly ,
//							RecordsetOptionEnum.dbForwardOnly,LockTypeEnum.dbOptimistic);
						
						objSrcRs = p_objSourceDb.OpenRecordset(objTarget.Name, RecordsetTypeEnum.dbOpenForwardOnly ,
							RecordsetOptionEnum.dbForwardOnly,LockTypeEnum.dbOptimistic);
						objDestRs = p_objTargetDb.OpenRecordset(objTarget.Name,RecordsetTypeEnum.dbOpenTable,
							RecordsetOptionEnum.dbAppendOnly,LockTypeEnum.dbOptimistic);
						
						m_objWs.BeginTrans();
						while(!objSrcRs.EOF ) 
						{
							objDestRs.AddNew();
							foreach(Field objFld in objDestRs.Fields)
							{
								string sCol="";

								if(objFld.Name.ToUpper()=="STUDYID")
									sCol = sSQLStudy;
								else
								{
									if (objFld.Name.ToUpper()=="RVUFEE")
										sCol = "RVU/FEE";
									else
										sCol = objFld.Name; 
								}
								objFld.Value = objSrcRs.Fields[sCol];
							}
							objDestRs.Update(1,false) ;
							objSrcRs.MoveNext(); 
						}
						m_objWs.CommitTrans(0); 
						objDestRs.Close();
						objSrcRs.Close();
					}
				}
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportUCR94.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objDestRs =null;
				objSrcRs = null;
			}
		}



		/// Name		: CopyRecords
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Copy the records for Foxpro file
		/// </summary>
		/// <param name="p_objSourceDb">Source Database</param>
		/// <param name="p_objTargetDb">Destination Database</param>
		private void CopyFoxproRecords(string p_sSourceDb, string p_sTargetDb, List<string> p_TableList)
		{
			string sSQL = "";
			string sFileMissing = string.Empty;
			string sValues = string.Empty;
			int iRecords = 0;
			string sSQLStudy = "";

			string strFoxProConnString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=dBASE IV;User ID=Admin;Password=;", p_sSourceDb);
			string sMDBConnectionString = String.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", p_sTargetDb);

			OleDbCommand oledbFoxproCmd = null;
			OleDbCommand oledbAccessCmd = null;
			List<string> oFieldNameList = new List<string>();
			string sColumns = string.Empty;
			Dictionary<string, string> dicMDBTableFieldList = new Dictionary<string, string>();
			Dictionary<string, List<string>> dicFoxTableFieldList = new Dictionary<string, List<string>>();

			try
			{
				//Check if the import source files exist or not
				foreach (string sTableName in p_TableList)
				{
					string sFilePath = string.Format(@"{0}\{1}.dbf", p_sSourceDb, sTableName);
					if (!File.Exists(sFilePath))
					{
						if (!string.IsNullOrEmpty(sFileMissing))
							sFileMissing += ",";
						sFileMissing += string.Format("{0}.dbf", sTableName);
					}
				}
				if ( !string.IsNullOrEmpty(sFileMissing))
				{
					string sFileMissingExp = "The fee tables " + sFileMissing + " is missing. Please upload all the files and try again. Please make sure you have uploaded all files needed for importing the fee schedule before clicking on the finish button.";
					sFileMissing = string.Empty;
					throw new RMAppException(sFileMissingExp);
				}

				using (OleDbConnection oMDBConnection = new OleDbConnection(sMDBConnectionString))
				{
					oMDBConnection.Open();
					oledbAccessCmd = oMDBConnection.CreateCommand();

					using (OleDbConnection oledbFoxProConn = new OleDbConnection(strFoxProConnString))
					{
						oledbFoxProConn.Open();
						oledbFoxproCmd = oledbFoxProConn.CreateCommand();

						//Special handling for STUDY column in WC fee schedule import
						if (p_TableList.Contains("WRKFCT"))
						{
							oledbFoxproCmd.CommandText = string.Format("SELECT * FROM WRKFCT.dbf");
							using (OleDbDataReader oleReader = oledbFoxproCmd.ExecuteReader())
							{
								int iFieldCount = oleReader.FieldCount;
								string sFieldName = string.Empty;
								sSQLStudy = "STUDYID";
								for (int i = 0; i < iFieldCount; i++)
								{
									sFieldName = oleReader.GetName(i);
									if (sFieldName.ToUpper() == "STUDY")
										sSQLStudy = "STUDY";
								}
							}
						}

						//Retrieve all the field names
						foreach (string sTableName in p_TableList)
						{
							oledbAccessCmd.CommandText = string.Format("SELECT * FROM {0}", sTableName);
							using (OleDbDataReader oleReader = oledbAccessCmd.ExecuteReader())
							{
								int iFieldCount = oleReader.FieldCount;
								string sMDBFieldName = string.Empty;
								string sFoxFieldName = string.Empty;

								oFieldNameList = new List<string>();
								sColumns = string.Empty;
								for (int i = 0; i < iFieldCount; i++)
								{
									sMDBFieldName = oleReader.GetName(i);
									if (!string.IsNullOrEmpty(sColumns))
										sColumns += ",";
									sColumns += EscapeReservedWords(sMDBFieldName);

									sFoxFieldName = sMDBFieldName;
									if (sTableName.ToUpper() == "WRKFCT" && sFoxFieldName.ToUpper() == "STUDYID")
									{
										sFoxFieldName = sSQLStudy;
									}
									sFoxFieldName = MapFieldName(sFoxFieldName);

									oFieldNameList.Add(sFoxFieldName);
								}
								dicMDBTableFieldList.Add(sTableName, sColumns);
								dicFoxTableFieldList.Add(sTableName, oFieldNameList);
							}
						}

						//Copy data from Foxpro to Access database
						foreach (string sTableName in p_TableList)
						{
							oledbFoxproCmd.CommandText = string.Format("SELECT * FROM {0}.dbf", sTableName);
							using (OleDbDataReader oleReader = oledbFoxproCmd.ExecuteReader())
							{
								sSQL = string.Format("INSERT INTO {0} ( {1} ) VALUES(", sTableName, dicMDBTableFieldList[sTableName]);
								iRecords = 0;
								string sColumnValue = string.Empty;

								//Insert into the fee database
								while (oleReader.Read())
								{
									sValues = string.Empty;
									foreach (string sFoxFieldName in dicFoxTableFieldList[sTableName])
									{
										sColumnValue = oleReader[sFoxFieldName].ToString();
										sColumnValue = sColumnValue.Replace("'", "''");
										if (!string.IsNullOrEmpty(sValues))
											sValues += ",";
										sValues += "'" + sColumnValue + "'";
									}

									oledbAccessCmd.CommandText = sSQL + sValues + ")";
									oledbAccessCmd.ExecuteNonQuery();
								}
							}
						}
					}
				}
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException("Exception in CopyFoxporRecords function", p_objEx);
			}
			finally
			{
			}
		}

		/// <summary>
		/// Map field name from Access to Foxpro
		/// </summary>
		/// <param name="sFieldName"></param>
		/// <returns></returns>
		private string MapFieldName(string sFieldName)
		{
			string sReturnFieldName = sFieldName;

			if (sFieldName.ToUpper() == "RVUFEE")
				sReturnFieldName = "RVU/FEE";

			return sReturnFieldName;
		}


		/// <summary>
		/// Special handling for database reserved key words (put [] around the field)
		/// </summary>
		/// <param name="sFieldName"></param>
		/// <returns></returns>
		private string EscapeReservedWords(string sFieldName)
		{
			string sReturnFieldName = sFieldName;
			switch (sFieldName)
			{
				case "RVU/FEE":
					sReturnFieldName = "[RVUFEE]";
					break;
				case "DESC":
				case "INDICATOR":
				case "PROC":
				case "PERCENT":
					sReturnFieldName = string.Format("[{0}]", sFieldName);
					break;
			}

			return sReturnFieldName;
		}

		/// Name		: GetNVGroupCPTAndAmount
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// GetNVGroupCPTAndAmount
		/// </summary>
		/// <param name="p_objDb">Input database</param>
		private void GetNVGroupCPTAndAmount(string p_sTargetDb)
		{
			string sFile = string.Empty, sSQL = string.Empty;
			StreamReader objSRdr=null;

			try
			{
				sFile = String.Format("{0}\\NVGRPAMT.TXT", RMConfigurationManager.GetNameValueSectionSettings("BillingReviewSystem", m_sConnStr, m_iClientId)["ImportFeeSchedulePath"]);//rkaur27
								
				string sODBCConnectionString = String.Format(@"Driver={{Microsoft Access Driver (*.mdb)}};Dbq={0};Uid=;Pwd=;", p_sTargetDb);
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE TABLE GROUP_AMOUNT (GROUP_ID LONG,AMOUNT DOUBLE)");

				if (File.Exists(sFile))
				{				
					sSQL = "INSERT INTO GROUP_AMOUNT (GROUP_ID,AMOUNT) VALUES (";
					using (objSRdr = new StreamReader(sFile))
					{
						string sLine = "";
						while ((sLine = objSRdr.ReadLine())!= null)
						{
							if(sLine.IndexOf(",")==0)
								throw new RMAppException(Globalization.GetString("BRSImportSchedule.GetNVGroupCPTAndAmount.Err", m_iClientId));
  
							sLine = sSQL + sLine;
							DbFactory.ExecuteNonQuery(sODBCConnectionString, sLine);
						}
					}
				}
				DbFactory.ExecuteNonQuery(sODBCConnectionString, "CREATE UNIQUE INDEX IDX_PK ON GROUP_AMOUNT (GROUP_ID)");
				sFile += "\\" + "NVGRPAMT.TXT";
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSImportSchedule.ImportUCR94.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objSRdr = null;
			}
		}

		#region Void DeleteFiles()
		/// <summary>
		/// DeleteFiles
		/// abisht MITS 10623 11/15/2007
		/// Function to delete files that are present in the BRSSourceFolderPath.
		/// </summary>
		public void DeleteFiles()
		{   
			foreach (string fileReturned in Directory.GetFiles(m_strBRSSourcePath))
			{
				//abisht added try catch so that even if error comes the foreach loop is completed	
				try
				{
					if (File.Exists(fileReturned))
						File.Delete(fileReturned);
				}
				catch(Exception ex)
				{
					Log.Write(ex.Message, "CommonWebServiceLog",m_iClientId);
				}
			}
		   
		}
		#endregion

		/// Name		: Dispose
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Public Dispose
		/// </summary>
		public void Dispose()
		{
			if(m_objWs!=null)
			{
				m_objWs.Close();
				m_objWs = null;
			}
			m_objdbEng = null;			
		}
	}
}
