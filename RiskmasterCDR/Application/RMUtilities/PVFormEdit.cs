using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.AdminTracking; 
using Riskmaster.DataModel;
using Riskmaster.Settings; 
using System.Collections.Generic; //rkulavil : RMA-5233 
using System.Linq;
using System.Xml.Linq;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   17 May 2005
	///Purpose :   This class is responsible for Creating, saving the View Definations
	/// </summary>
	public class PVFormEdit
	{
        private string m_netViewFormsConnString = "";
        private string s_DataSourceId = "329";

        //Raman 07/20/2009: removing dependency on Riskmaster.Config

        private static string PV_FORMLISTFILE = "formlist.xml";
        private static string PV_FORMLISTFILEPATH = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "PV");
        
        //private const string PV_FORMLISTFILEPATH = "PV_FORMLISTFilePath";
		//private const string PV_FORMLISTFILE = "PV_FormListFile";
        //Deb ML Changes
        private string p_PageId="0";
        private string p_LangCode="1033";
        private int m_iClientId = 0; //mbahl3 JIRA [RMACLOUD-123]
        Dictionary<string, string> strDictResourceValues = null; //rkulavil : RMA-5233 
        //Deb ML Changes
		#region Private variables
		private string m_sConnString = "";
		private string m_sUserId="";
		private string m_sPassword="";
		private string m_sDSNName="";
		private string m_sDSNId="";
		private string m_sSecurityDSN="";
        private static Dictionary<string, List<XElement>> m_dictDefaultViewXmls = null;//asharma326 JIRA 6411
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserId">User Id</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDSNName">DSN Name</param>
		/// <param name="p_sDSNId">DSN Id</param>
		/// <param name="p_sConnString">Conn String</param>
		public PVFormEdit(string p_sUserId, string p_sPassword,string p_sDSNName,string p_sDSNId, string p_sConnString , int p_iClientId)//mbahl3  JIRA [RMACLOUD-123]
		{
			m_sDSNName = p_sDSNName;
			m_sUserId = p_sUserId;
			m_sPassword = p_sPassword;
			m_sConnString = p_sConnString;
			m_sDSNId = p_sDSNId;
            s_DataSourceId = p_sDSNId;
            m_iClientId = p_iClientId;//mbahl3  JIRA [RMACLOUD-123]
            m_sSecurityDSN = SecurityDatabase.GetSecurityDsn(m_iClientId); //mbahl3  JIRA [RMACLOUD-123]
            //m_netViewFormsConnString = "Driver={SQL Native Client};Server=20.198.58.52;Database=RMX_PAGE;UID=sa;PWD=rmserver_123;";
            m_netViewFormsConnString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
        }
		

		#endregion

		#region "private functions"
		/// <summary>
		/// Sets the attribute for showing buttons on top of UI
		/// </summary>
		/// <param name="p_objDoc">XML Document containg page Info</param>
		/// <returns>boolean for success/failure</returns>
		private bool ShowButtonsOnTop(XmlDocument p_objDoc)
		{
			XmlElement  objElemForm=null;
			try
			{
				objElemForm=(XmlElement)p_objDoc.GetElementsByTagName("form").Item(0);
				if (objElemForm.GetAttribute("topbuttons").Equals("1"))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			finally
			{
				objElemForm = null;
			}
		}

		/// <summary>
        /// It removes nodes from XML such as control nodes, button nodes & group nodes. 
        /// Change by Naresh : It now removes the hidden control/Javascript from the XML and copies them from the base xml to this view xml.
		/// </summary>
		/// <param name="p_objTargetDoc">The View Xml</param>
		/// <param name="p_objSourceDoc">The Base Xml</param>
		/// <returns></returns>
		private bool RemoveAllControls(XmlDocument p_objTargetDoc, XmlDocument p_objSourceDoc)
		{
			XmlElement objElemForm=null;
			XmlNodeList objXmlNodeList = null;
			//ArrayList arrNode = null;
			int iNodeCount = 0;

			try
			{
                if (p_objTargetDoc == null)
					return false;

                //objElemForm = (XmlElement)p_objTargetDoc.GetElementsByTagName("form").Item(0);
                //objXmlNodeList = p_objTargetDoc.SelectNodes("//control[@type='id']");
                //if(objXmlNodeList != null)
                //{
                //    //arrNode = new ArrayList();
                //    foreach(XmlNode objXMLNode in objXmlNodeList)
                //    {
                //        //arrNode.Add(objXMLNode.ParentNode.RemoveChild(objXMLNode));  
                //        // Naresh Remove the controls of type id from the View Xml
                //        objXMLNode.ParentNode.RemoveChild(objXMLNode);
                //    }

                //    //for(int i = 0; i <= arrNode.Count - 1; i++)
                //    //    objElemForm.AppendChild((XmlNode)arrNode[i]); 

                //    // Copy the Controls of Type Id from base xml to the view xml
                //    //objXmlNodeList = p_objSourceDoc.SelectNodes("//control[@type='id']");

                //    //if (objXmlNodeList != null)
                //    //{
                //    //    foreach (XmlNode objXMLNode in objXmlNodeList)
                //    //    {
                //    //        objElemForm.AppendChild(p_objTargetDoc.ImportNode(objXMLNode,true));
                //    //    }
                //    //}

                //}
                ////arrNode=null;

                ////TR2320 2/26/06 Pankaj- Preserve the hidden controls in the same xml.
                //objXmlNodeList = p_objTargetDoc.SelectNodes("//control[@type='hidden']");
                //if(objXmlNodeList != null)
                //{
                //    //arrNode = new ArrayList();
                //    foreach(XmlNode objXMLNode in objXmlNodeList)
                //    {
                //        //arrNode.Add(objXMLNode.ParentNode.RemoveChild(objXMLNode));  
                //        // Naresh Remove the controls of type hiddden from the View Xml
                //        objXMLNode.ParentNode.RemoveChild(objXMLNode);
                //    }

                //    //for(int i = 0; i <= arrNode.Count - 1; i++)
                //    //    objElemForm.AppendChild((XmlNode)arrNode[i]); 

                //    // Copy the Controls of Type hidden from base xml to the view xml
                //    objXmlNodeList = p_objSourceDoc.SelectNodes("//control[@type='hidden']");

                //    if (objXmlNodeList != null)
                //    {
                //        foreach (XmlNode objXMLNode in objXmlNodeList)
                //        {
                //            objElemForm.AppendChild(p_objTargetDoc.ImportNode(objXMLNode, true));
                //        }
                //    }
                //}
			
                ////Mukul Added 1/15/07 MITS 8678
                ////arrNode=null;
                //objXmlNodeList = p_objTargetDoc.SelectNodes("//control[@type='javascript']");
                //if(objXmlNodeList != null)
                //{
                //    //arrNode = new ArrayList();
                //    foreach(XmlNode objXMLNode in objXmlNodeList)
                //    {
                //        //arrNode.Add(objXMLNode.ParentNode.RemoveChild(objXMLNode)); 
                //        // Naresh Remove the controls of type hiddden from the View Xml
                //        objXMLNode.ParentNode.RemoveChild(objXMLNode);
                //    }

                //    //for(int i = 0; i <= arrNode.Count - 1; i++)
                //    //    objElemForm.AppendChild((XmlNode)arrNode[i]); 
                //    // Copy the Controls of Type hidden from base xml to the view xml
                //    objXmlNodeList = p_objSourceDoc.SelectNodes("//control[@type='javascript']");

                //    if (objXmlNodeList != null)
                //    {
                //        foreach (XmlNode objXMLNode in objXmlNodeList)
                //        {
                //            objElemForm.AppendChild(p_objTargetDoc.ImportNode(objXMLNode, true));
                //        }
                //    }
                //}
                //objXmlNodeList = p_objTargetDoc.GetElementsByTagName("group");
                //if(objXmlNodeList != null)
                //{
                //    iNodeCount = objXmlNodeList.Count;
                //    while(iNodeCount > 0)
                //    {
                //        objXmlNodeList.Item(0).ParentNode.RemoveChild(objXmlNodeList.Item(0));
                //        iNodeCount--;
                //    }
                //}
                ////TR2320 2/26/06 Pankaj- Remove anything other than id and hidden
                ////Mukul Added javascript not to be removed 1/15/07 MITS 8678
                //objXmlNodeList = p_objTargetDoc.SelectNodes("//control[@type!='id' and @type!='hidden' and @type!='javascript']");
                //if(objXmlNodeList != null)
                //{
                //    for(int i = 0; i<= objXmlNodeList.Count-1; i++)
                //    {
                //        objXmlNodeList.Item(i).ParentNode.RemoveChild(objXmlNodeList.Item(i));
                //    }			
                //}

                objXmlNodeList = p_objTargetDoc.GetElementsByTagName("button");
				if(objXmlNodeList != null)
				{
					iNodeCount = objXmlNodeList.Count;
					while(iNodeCount > 0)
					{
						objXmlNodeList.Item(0).ParentNode.RemoveChild(objXmlNodeList.Item(0));
						iNodeCount--;
					}
				}
                //Added by Shivendu for MITS 8617
                objXmlNodeList = p_objTargetDoc.GetElementsByTagName("buttonscript");
                if (objXmlNodeList != null)
                {
                    iNodeCount = objXmlNodeList.Count;
                    while (iNodeCount > 0)
                    {
                        objXmlNodeList.Item(0).ParentNode.RemoveChild(objXmlNodeList.Item(0));
                        iNodeCount--;
                    }
                }
                //Added by Shivendu for MITS 8617

                try
                {
                    objXmlNodeList = p_objTargetDoc.GetElementsByTagName("displaycolumn");
                    if (objXmlNodeList != null)
                    {
                        iNodeCount = objXmlNodeList.Count;
                        while (iNodeCount > 0)
                        {
                            objXmlNodeList.Item(0).ParentNode.RemoveChild(objXmlNodeList.Item(0));
                            iNodeCount--;
                        }
                    }
                }
                catch(Exception e)
                {
                }

                objXmlNodeList = p_objTargetDoc.GetElementsByTagName("group");
                if (objXmlNodeList != null)
                {
                    iNodeCount = objXmlNodeList.Count;
                    while (iNodeCount > 0)
                    {
                        objXmlNodeList.Item(0).ParentNode.RemoveChild(objXmlNodeList.Item(0));
                        iNodeCount--;
                    }
                }

				return true;
			}
			finally
			{
				objElemForm=null;
				objXmlNodeList = null;
				//arrNode = null;
			}
		}

		/// <summary>
		/// Gets the title attribute for the xmlelement passed
		/// </summary>
		/// <param name="p_objElement">XML Element</param>
		/// <returns>Title value</returns>
		private string GetFieldTitle(XmlElement p_objElement)
		{
			string sReturnValue="";
			if (p_objElement.GetAttribute("title")!=null)
			{
				if (p_objElement.GetAttribute("title").Trim()!="")
				{
					sReturnValue=p_objElement.GetAttribute("title");
				}
			}
			if (p_objElement.GetAttribute("required")!=null)
			{
				if (p_objElement.GetAttribute("required").Trim()=="yes")
				{
					sReturnValue="- "+sReturnValue;
				}
			}
			if (p_objElement.GetAttribute("title")=="")
			{
				//-- ABhateja 08.18.2006, -START-
				//-- MITS 7629, If title attribute is null, set pvtitle value.
				if (p_objElement.GetAttribute("pvtitle")!=null)
					if (p_objElement.GetAttribute("pvtitle").Trim() != "")
						sReturnValue = p_objElement.GetAttribute("pvtitle");
					else
						sReturnValue="-";
				//-- ABhateja 08.18.2006, -END-
			}
			return sReturnValue;
		}

		/// <summary>
		/// Copy XML Element to XMLDocument 
		/// </summary>
		/// <param name="p_objXMlEle">XML Element which is to be copied</param>
		/// <param name="p_objXMlDocTo">XML Doc into which the XML Element need to be copied</param>
		/// <returns>Copied XML Element</returns>
		private XmlElement CopyXMLElement(XmlElement p_objXMlEle, XmlDocument p_objXMlDocTo)
		{
			XmlElement objXMLEle = null;
			
			if(p_objXMlEle == null || p_objXMlDocTo == null)
				return null;

			objXMLEle = p_objXMlDocTo.CreateElement(p_objXMlEle.Name);
			for(int i = 0; i < p_objXMlEle.Attributes.Count; i++)
				objXMLEle.SetAttribute(p_objXMlEle.Attributes[i].Name,p_objXMlEle.Attributes[i].Value);   
 
			//TR#780 nodes with innertext were giving problems. innerxml works fine
//			foreach(XmlNode objXMLNode in p_objXMlEle.ChildNodes)
//				objXMLEle.AppendChild(objXMLNode); 			
			if((p_objXMlEle.ChildNodes.Count>0)&& (p_objXMlEle.InnerXml.IndexOf("control")<0))
				//copy inner xml for combos and other. dont do it when there are inner controls. eg. control group. will be done separately
				objXMLEle.InnerXml = p_objXMlEle.InnerXml;  

			return objXMLEle;
		}

		/// <summary>
		/// Inserts XML Element to the Node after doing some formatting to the element values
		/// </summary>
		/// <param name="p_objElement">Element to be copied</param>
		/// <param name="p_objNode">Node in which the element needs to be copied</param>
		/// <param name="p_objXMLDocument">For creating new child elements</param>
		private void WriteFieldOption(XmlElement p_objElement,XmlNode p_objNode,XmlDocument p_objXMLDocument)
		{
			string sName="";
            string reserveflag = "1";
            SysSettings objSys = null; //vgupta79 for MITS-37962
            objSys = new SysSettings(m_sConnString, m_iClientId); 
			XmlElement objElemTemp=null;
			sName=p_objElement.Name.ToLower();
            //mbahl3 5/01/2012 Power View Issue---Start
            string sFormName = string.Empty;
            XmlElement objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVFormEdit/FormName");
            if (objSelectElement != null)
            {
                sFormName = objSelectElement.InnerText.Replace("%", "|");
                
            }

            //mbahl3 5/01/2012 Power View Issue---End
			if (sName=="control")
			{
				if (p_objElement.GetAttribute("type")!=null)
				{
					if (p_objElement.GetAttribute("type")=="message")
					{	
						objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
						objElemTemp.SetAttribute("value",p_objElement.GetAttribute("name"));
                        //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        //{
                        //    objElemTemp.SetAttribute("basereadonly", "1");
                        //}
						objElemTemp.InnerText="[MS* "+p_objElement.GetAttribute("title") + " *MS]";
						p_objNode.AppendChild(objElemTemp);	
					}
					else if(p_objElement.GetAttribute("type")=="textlabel")//Text label special case as the title is in innertext and not as an attrib
					{
						objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
						objElemTemp.SetAttribute("value",p_objElement.GetAttribute("name"));
                        //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        //{
                        //    objElemTemp.SetAttribute("basereadonly", "1");
                        //}
						objElemTemp.InnerText = GetFieldTitle(p_objElement);
						p_objNode.AppendChild(objElemTemp);	
					}
                    //asharma326 JIRA# 6411 Starts 
                    else if ((p_objElement.GetAttribute("type") == "linebreak"))
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        objElemTemp.InnerText = "[LB* " + "Line Break" + " *LB]";
                        p_objNode.AppendChild(objElemTemp);
                    }
                    //asharma326 JIRA# 6411 Ends
                    
					else if(p_objElement.GetAttribute("type")=="controlgroup")//Inner controls inside the control groups need to be taken care of
					{
						objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
						objElemTemp.SetAttribute("value",p_objElement.GetAttribute("name"));
                        //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        //{
                        //    objElemTemp.SetAttribute("basereadonly", "1");
                        //}
						objElemTemp.InnerText="[Hidden Control Group]"; //+ displays the control groups. cannot be moved individually
						p_objNode.AppendChild(objElemTemp);	

						foreach(XmlElement objCtl in p_objElement.SelectNodes("//control[@name='"+ p_objElement.GetAttribute("name") + "']//control"))
						{
                            objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
							objElemTemp.SetAttribute("value",objCtl.GetAttribute("name"));
                            //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                            //{
                            //    objElemTemp.SetAttribute("basereadonly", "1");
                            //}
							objElemTemp.InnerText = "+" + GetFieldTitle(objCtl);
							p_objNode.AppendChild(objElemTemp);	
						}
					}
                    //Code updated by pawan to byppass spaces implementation for 11210 on tabviews 
                    else if (p_objElement.GetAttribute("type") == "space")
                    {   if (p_objElement.GetAttribute("name").ToString().Substring(0,7).Equals("pvspace"))
                        {
                            objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                            objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                            //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                            //{
                            //    objElemTemp.SetAttribute("basereadonly", "1");
                            //}
                            objElemTemp.InnerText = "[HS* " + "New Space" + " *HS]";
                            p_objNode.AppendChild(objElemTemp);
                        }
                    }
                    //Code change ends implementation for 11210 on tabviews by pawan 
                   //Added rjhamb Mits22221
                    //mbahl3 5/01/2011 mits 26793 Power View Issue---Start 
                    else if (string.Compare(p_objElement.GetAttribute("type"), "phonetype", true) == 0 && !string.IsNullOrEmpty(p_objElement.GetAttribute("helpmsg")))
					{
						objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg") + "|" + p_objElement.GetAttribute("type"));
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg"));
                        if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        {
                            objElemTemp.SetAttribute("basereadonly", "1");
                        }
                        else
                        {
                            objElemTemp.SetAttribute("basereadonly", "0");
                        }
                        objElemTemp.InnerText=GetFieldTitle(p_objElement);
						p_objNode.AppendChild(objElemTemp);	
					}
                    //mbahl3 5/01/2011 mits 26793 Power View Issue---Start 
                    //Added rjhamb Mits22221
                    //Added rjhamb Mits 23065
                    //mbahl3 5/01/2011 mits 26793 Power View Issue---Start 
                    else if (string.Compare(p_objElement.GetAttribute("mandatory"), "true", true) == 0 && sFormName.StartsWith("admintracking|"))
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg") + "|" + "mandatory");
                        if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        {
                            objElemTemp.SetAttribute("basereadonly", "1");
                        }
                        else
                        {
                            objElemTemp.SetAttribute("basereadonly", "0");
                        }
                        objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        p_objNode.AppendChild(objElemTemp);
                        //mbahl3 5/01/2011 Power View Issue---End
                    }
                    //Added rjhamb Mits 23065
					else
					{
                    //Added by vgupta79 for MITS-37962
                        if (p_objElement.GetAttribute("name") == "ReserveTypeCodeFt" && sFormName == "split.xml")
						{
                            if (!objSys.UseResFilter)
                            {
                                reserveflag = "0";
                            }
                        }
						else if (p_objElement.GetAttribute("name") == "ReserveTypeCode" && sFormName == "split.xml")
                        {
                            if (objSys.UseResFilter)
                            {
                                reserveflag = "0";
                            }
                        }

                        if (reserveflag == "1")
                        {

						objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        if (!string.IsNullOrEmpty(p_objElement.GetAttribute("PowerViewReadOnly")))//asharma326 jira 7508 IsPowerViewReadOnly from base xml field
                            objElemTemp.SetAttribute("powerviewreadonly", p_objElement.GetAttribute("PowerViewReadOnly") + "|");    
                            
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg"));
                        
                        if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))//asharma326 JIRA 6411
                        {
                            objElemTemp.SetAttribute("basereadonly", "1");
                        }
                        else
                        {
                            objElemTemp.SetAttribute("basereadonly", "0");
                        }
						objElemTemp.InnerText=GetFieldTitle(p_objElement);
						p_objNode.AppendChild(objElemTemp);
                        }
					}
				}
			}
			else if (sName=="group")
			{
				objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
				objElemTemp.SetAttribute("value",p_objElement.GetAttribute("name"));
                //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                //{
                //    objElemTemp.SetAttribute("basereadonly", "1");
                //}
				objElemTemp.InnerText="[*** "+p_objElement.GetAttribute("title")+" ***]";
				p_objNode.AppendChild(objElemTemp);	
			}
            else if (sName == "button" || sName == "buttonscript")
			{
				if (p_objElement.ParentNode.Name=="toolbar")
				{
					objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
					objElemTemp.SetAttribute("value",p_objElement.GetAttribute("type"));
                    //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                    //{
                    //    objElemTemp.SetAttribute("basereadonly", "1");
                    //}
					objElemTemp.InnerText=GetFieldTitle(p_objElement);
					p_objNode.AppendChild(objElemTemp);	
				}
				else
				{
					objElemTemp=p_objXMLDocument.CreateElement(p_objNode.Name + "option");
					objElemTemp.SetAttribute("value",p_objElement.GetAttribute("name"));
                    //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                    //{
                    //    objElemTemp.SetAttribute("basereadonly", "1");
                    //}
					objElemTemp.InnerText=GetFieldTitle(p_objElement);
					p_objNode.AppendChild(objElemTemp);	
				}
			}
		}
        //Deb ML Changes
        /// <summary>
        /// WriteFieldOptionNew
        /// </summary>
        /// <param name="p_objElement"></param>
        /// <param name="p_objNode"></param>
        /// <param name="p_objXMLDocument"></param>
        private void WriteFieldOptionNew(XmlElement p_objElement, XmlNode p_objNode, XmlDocument p_objXMLDocument)
        {
            string sName = "";
            string reserveflag = "1";
            SysSettings objSys = null; //vgupta79 for MITS-37962
            objSys = new SysSettings(m_sConnString, m_iClientId);
            XmlElement objElemTemp = null;
            sName = p_objElement.Name.ToLower();
            string sFormName = string.Empty;
            XmlElement objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVFormEdit/FormName");
            if (objSelectElement != null)
            {
                sFormName = objSelectElement.InnerText.Replace("%", "|");

            }

            if (sName == "control")
            {
                if (p_objElement.GetAttribute("type") != null)
                {
                    if (p_objElement.GetAttribute("type") == "message")
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        //{
                        //    objElemTemp.SetAttribute("basereadonly", "1");
                        //}

                        //Deb ML Changes
                        //objElemTemp.InnerText="[MS* "+p_objElement.GetAttribute("title") + " *MS]";
                        string sKeyName = p_objElement.GetAttribute("name");
                        //rkulavil : RMA-5233 
                        //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        // akaushik5 Changed for MITS 30290 Starts
                        //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                        string sKeyValue = string.Empty;
                        if (!string.IsNullOrEmpty(p_PageId))
                        {
                            //rkulavil : RMA-5233 start
                            // sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                            strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                            //rkulavil : RMA-5233 end
                        }
                        // akaushik5 Changed for MITS 30290 Ends
                        if (!string.IsNullOrEmpty(sKeyValue))
                        {
                            objElemTemp.InnerText = "[MS* " + sKeyValue + " *MS]";
                        }
                        else
                        {
                            objElemTemp.InnerText = "[MS* " + p_objElement.GetAttribute("title") + " *MS]";
                        }
                        //Deb ML Changes
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else if (p_objElement.GetAttribute("type") == "textlabel")//Text label special case as the title is in innertext and not as an attrib
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        //Deb ML Changes
                        //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        string sKeyName = p_objElement.GetAttribute("name");
                        //rkulavil : RMA-5233 
                        //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        // akaushik5 Changed for MITS 30290 Starts
                        //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                        string sKeyValue = string.Empty;
                        if (!string.IsNullOrEmpty(p_PageId))
                        {
                            //rkulavil : RMA-5233 start
                            //sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                            strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                            //rkulavil : RMA-5233 end
                        }
                        // akaushik5 Changed for MITS 30290 Ends
                        if (!string.IsNullOrEmpty(sKeyValue))
                        {
                            objElemTemp.InnerText = sKeyValue;
                        }
                        else
                        {
                            objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        }
                        //Deb ML Changes
                        p_objNode.AppendChild(objElemTemp);
                    }
                    //asharma326 JIRA# 6411 Starts 
                    else if ((p_objElement.GetAttribute("type") == "linebreak"))
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        objElemTemp.InnerText = "[LB* " + "Line Break" + " *LB]";
                        p_objNode.AppendChild(objElemTemp);
                    }
                    //asharma326 JIRA# 6411 Ends

                    else if (p_objElement.GetAttribute("type") == "controlgroup")//Inner controls inside the control groups need to be taken care of
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                        objElemTemp.InnerText = "[Hidden Control Group]"; //+ displays the control groups. cannot be moved individually
                        p_objNode.AppendChild(objElemTemp);

                        foreach (XmlElement objCtl in p_objElement.SelectNodes("//control[@name='" + p_objElement.GetAttribute("name") + "']//control"))
                        {
                            objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                            objElemTemp.SetAttribute("value", objCtl.GetAttribute("name"));
                            //Deb ML Changes
                            //objElemTemp.InnerText = "+" + GetFieldTitle(objCtl);
                            //string sKeyName = p_objElement.GetAttribute("name"); ttumula2 : RMA 7055
                            string sKeyName = objCtl.GetAttribute("name");
                            //rkulavil : RMA-5233 
                            //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                            // akaushik5 Changed for MITS 30290 Starts
                            //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                            string sKeyValue = string.Empty;
                            if (!string.IsNullOrEmpty(p_PageId))
                            {
                                //rkulavil : RMA-5233 start
                                // sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                                strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                                //rkulavil : RMA-5233 end
                            }
                            // akaushik5 Changed for MITS 30290 Ends
                            if (!string.IsNullOrEmpty(sKeyValue))
                            {
                                objElemTemp.InnerText = "+" + sKeyValue;
                            }
                            else
                            {
                                objElemTemp.InnerText = "+" + GetFieldTitle(objCtl);
                            }
                            //Deb ML Changes
                            p_objNode.AppendChild(objElemTemp);
                        }
                    }
                    //Code updated by pawan to byppass spaces implementation for 11210 on tabviews 
                    else if (p_objElement.GetAttribute("type") == "space")
                    {
                        if (p_objElement.GetAttribute("name").ToString().Substring(0, 7).Equals("pvspace"))
                        {
                            objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                            objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                            objElemTemp.InnerText = "[HS* " + "New Space" + " *HS]";
                            p_objNode.AppendChild(objElemTemp);
                        }
                    }

                    //asharma326 JIRA# 6411 Starts 
                    //else if ((p_objElement.GetAttribute("type") == "linebreak"))
                    //{
                    //    objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                    //    objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                    //    objElemTemp.InnerText = "[LB* " + GetFieldTitle(p_objElement) + " *LB]";
                    //    p_objNode.AppendChild(objElemTemp);
                    //}
                    //asharma326 JIRA# 6411 Ends


                    else if (string.Compare(p_objElement.GetAttribute("type"), "phonetype", true) == 0 && !string.IsNullOrEmpty(p_objElement.GetAttribute("helpmsg")))
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        //objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg") + "|" + p_objElement.GetAttribute("type"));
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg"));
                        if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        {
                            objElemTemp.SetAttribute("basereadonly", "1");
                        }
                        else
                        {
                            objElemTemp.SetAttribute("basereadonly", "0");
                        }
                        //Deb ML Changes
                        //objElemTemp.InnerText=GetFieldTitle(p_objElement);
                        string sKeyName = p_objElement.GetAttribute("name");
                        //rkulavil : RMA-5233 
                        //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        // akaushik5 Changed for MITS 30290 Starts
                        //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                        string sKeyValue = string.Empty;
                        if (!string.IsNullOrEmpty(p_PageId))
                        {
                            //rkulavil : RMA-5233 start
                            //sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                            strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                            //rkulavil : RMA-5233 end
                        }
                        // akaushik5 Changed for MITS 30290 Ends
                        if (!string.IsNullOrEmpty(sKeyValue))
                        {
                            objElemTemp.InnerText = sKeyValue;
                        }
                        else
                        {
                            objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        }
                        //Deb ML Changes
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else if (string.Compare(p_objElement.GetAttribute("mandatory"), "true", true) == 0 && sFormName.StartsWith("admintracking|"))
                    {
                        objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                        objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg") + "|" + "mandatory");
                        if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                        {
                            objElemTemp.SetAttribute("basereadonly", "1");
                        }
                        //Deb ML Changes
                        //objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        string sKeyName = p_objElement.GetAttribute("name");
                        //rkulavil : RMA-5233 
                        //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                        // akaushik5 Changed for MITS 30290 Starts
                        //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                        string sKeyValue = string.Empty;
                        if (!string.IsNullOrEmpty(p_PageId))
                        {
                            //rkulavil : RMA-5233 start
                            //sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                            strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                            //rkulavil : RMA-5233 end
                        }
                        // akaushik5 Changed for MITS 30290 Ends
                        if (!string.IsNullOrEmpty(sKeyValue))
                        {
                            objElemTemp.InnerText = sKeyValue;
                        }
                        else
                        {
                            objElemTemp.InnerText = GetFieldTitle(p_objElement);
                        }
                        //Deb ML Changes
                        p_objNode.AppendChild(objElemTemp);
                    }
                    else
                    {
                        //Added by vgupta79 for MITS-37962
                        if (p_objElement.GetAttribute("name") == "ReserveTypeCodeFt" && sFormName == "split.xml")
                        {
                            if (!objSys.UseResFilter)
                            {
                                reserveflag = "0";
                            }
                        }

                        else if (p_objElement.GetAttribute("name") == "ReserveTypeCode" && sFormName == "split.xml")
                        {
                            if (objSys.UseResFilter)
                            {
                                reserveflag = "0";
                            }
                        }
                        if (reserveflag == "1")
                        {
                            objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                            if (!string.IsNullOrEmpty(p_objElement.GetAttribute("PowerViewReadOnly")))//asharma326 jira 7508
                                objElemTemp.SetAttribute("powerviewreadonly", p_objElement.GetAttribute("PowerViewReadOnly") + "|");

                            objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name") + "|" + p_objElement.GetAttribute("helpmsg"));
                            if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))//asharma326 JIRA 6411
                            {
                                objElemTemp.SetAttribute("basereadonly", "1");
                            }
                            else
                            {
                                objElemTemp.SetAttribute("basereadonly", "0");
                            }
                            //Deb ML Changes
                            //objElemTemp.InnerText=GetFieldTitle(p_objElement);
                            string sKeyName = p_objElement.GetAttribute("name");
                            //rkulavil : RMA-5233 
                            //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                            // akaushik5 Changed for MITS 30290 Starts
                            //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                            string sKeyValue = string.Empty;
                            if (!string.IsNullOrEmpty(p_PageId))
                            {
                                //rkulavil : RMA-5233 start
                                //sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                                strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                                //rkulavil : RMA-5233 end
                            }
                            // akaushik5 Changed for MITS 30290 Ends
                            if (!string.IsNullOrEmpty(sKeyValue))
                            {
                                objElemTemp.InnerText = sKeyValue;
                            }
                            else
                            {
                                objElemTemp.InnerText = GetFieldTitle(p_objElement);
                            }
                            //Deb ML Changes
                            p_objNode.AppendChild(objElemTemp);
                        }
                    }
                }
            }
            else if (sName == "group")
            {
                objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                //{
                //    objElemTemp.SetAttribute("basereadonly", "1");
                //}
                //Deb ML Changes
                //objElemTemp.InnerText="[*** "+p_objElement.GetAttribute("title")+" ***]";
                string sKeyName = p_objElement.GetAttribute("name");
                //rkulavil : RMA-5233 
                //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                // akaushik5 Changed for MITS 30290 Starts
                //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                string sKeyValue = string.Empty;
                if (!string.IsNullOrEmpty(p_PageId))
                {
                    //rkulavil : RMA-5233 start
                    //sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                    strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                    //rkulavil : RMA-5233 end
                }
                // akaushik5 Changed for MITS 30290 Ends

                if (!string.IsNullOrEmpty(sKeyValue))
                {
                    objElemTemp.InnerText = "[*** " + sKeyValue + " ***]";
                }
                else
                {
                    objElemTemp.InnerText = "[*** " + p_objElement.GetAttribute("title") + " ***]";
                }
                //Deb ML Changes
                p_objNode.AppendChild(objElemTemp);
            }
            else if (sName == "button" || sName == "buttonscript")
            {
                if (p_objElement.ParentNode.Name == "toolbar")
                {
                    objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                    objElemTemp.SetAttribute("value", p_objElement.GetAttribute("type"));
                    //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                    //{
                    //    objElemTemp.SetAttribute("basereadonly", "1");
                    //}
                    //Deb ML Changes
                    //objElemTemp.InnerText=GetFieldTitle(p_objElement);
                    string sKeyName = p_objElement.GetAttribute("type");
                    //rkulavil : RMA-5233 
                    //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                    // akaushik5 Changed for MITS 30290 Starts
                    //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                    string sKeyValue = string.Empty;
                    if (!string.IsNullOrEmpty(p_PageId))
                    {
                        //rkulavil : RMA-5233 start
                        //sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                        strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                        //rkulavil : RMA-5233 end
                    }
                    // akaushik5 Changed for MITS 30290 Ends
                    if (!string.IsNullOrEmpty(sKeyValue))
                    {
                        objElemTemp.InnerText = sKeyValue;
                    }
                    else
                    {
                        objElemTemp.InnerText = GetFieldTitle(p_objElement);
                    }
                    //Deb ML Changes
                    p_objNode.AppendChild(objElemTemp);
                }
                else
                {
                    objElemTemp = p_objXMLDocument.CreateElement(p_objNode.Name + "option");
                    objElemTemp.SetAttribute("value", p_objElement.GetAttribute("name"));
                    //if (this.CheckDefaultReadOnly(0, sFormName, p_objElement.GetAttribute("name")))
                    //{
                    //    objElemTemp.SetAttribute("basereadonly", "1");
                    //}
                    //Deb ML Changes
                    //objElemTemp.InnerText=GetFieldTitle(p_objElement);
                    string sKeyName = p_objElement.GetAttribute("name");
                    //rkulavil : RMA-5233 
                    //string sbSql = string.Format("SELECT RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} AND RESOURCE_KEY='{2}' ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode, sKeyName);
                    // akaushik5 Changed for MITS 30290 Starts
                    //string sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                    string sKeyValue = string.Empty;
                    if (!string.IsNullOrEmpty(p_PageId))
                    {
                        //rkulavil : RMA-5233 start
                        //sKeyValue = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, sbSql));
                        strDictResourceValues.TryGetValue(sKeyName, out sKeyValue);
                        //rkulavil : RMA-5233 end
                    }
                    // akaushik5 Changed for MITS 30290 Ends
                    if (!string.IsNullOrEmpty(sKeyValue))
                    {
                        objElemTemp.InnerText = sKeyValue;
                    }
                    else
                    {
                        objElemTemp.InnerText = GetFieldTitle(p_objElement);
                    }
                    //Deb ML Changes
                    p_objNode.AppendChild(objElemTemp);
                }
            }
        }
        /// <summary>
        /// asharma326 JIRA 6411 for checking whether Field is readonly in defaultpowerview or not.
        /// </summary>
        /// <param name="p_iViewId"></param>
        /// <param name="sFormName"></param>
        /// <param name="sFieldName"></param>
        /// <returns></returns>

        private bool CheckDefaultReadOnly(int p_iViewId, string sFormName,string sFieldName)
        {
            string sSql = string.Empty, sXml = string.Empty, sReturnValue=string.Empty ;
            DbReader objReader = null;
            XElement xEle = null;
            try
            {
                if (m_dictDefaultViewXmls == null)
                    m_dictDefaultViewXmls=new Dictionary<string, List<XElement>>();
                if (!m_dictDefaultViewXmls.ContainsKey(sFormName))
                {
                    sSql = string.Format("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='{1}' AND DATA_SOURCE_ID='{2}'", p_iViewId, sFormName, m_sDSNId);
                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sSql);
                    if (objReader.Read())
                    {
                        sXml = Conversion.ConvertObjToStr(objReader.GetValue("VIEW_XML"));
                    }
                    objReader.Dispose();
                    xEle = XElement.Parse(sXml);
                    var IsFieldDisabledInDefaultView = (from el in xEle.Descendants("control")
                                                        where el.Attribute("type") != null && (string)el.Attribute("type").Value.ToLower() == "readonly"
                                                        select el).ToList();
                    m_dictDefaultViewXmls.Add(sFormName, IsFieldDisabledInDefaultView);
                }

                if (m_dictDefaultViewXmls.ContainsKey(sFormName))
                {
                    var elements =m_dictDefaultViewXmls.Where(x => x.Key == sFormName).Select(x => x.Value).FirstOrDefault().ToList();
                    var returnvalues = elements.Attributes("name").Where(s => s.Value == sFieldName);
                    if (returnvalues.Count() >0)
                        return true;
                }
                return false;
            }
            catch (Exception exe)
            {
                return false;
            }
        }
        //Deb ML Changes
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Anurag Agarwal
		/// Date Created: 05/17/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get(XmlDocument p_objXMLDocument)
		{
			DbReader objReader=null;
			XmlElement objSelectElement=null;
			StringBuilder sbSql=null;
			string sViewId="";
			string sFormName="";
			string sXml="";
			string sChecked="";
			string sPVTitle = "";
			XmlNode objNode=null;
			XmlNodeList objNodeList=null;
            // npadhy MITS 16920 Added to check if the Toolbar button is present in Base Xml, if it is then only that button will appear in Powerviews
            XmlNodeList objBaseNodeList = null;
			XmlDocument objXMLlstFields=null;
			XmlDocument objXMLForm=null;
			XmlElement objForm=null;
			XmlNodeList objXMLSysRequired=null;
			XmlNodeList objXMLSysRequiredHidden=null;
			XmlNodeList objXMLPVTitle = null;
			bool bAdt=false;
			string sReset="";
			XmlElement objXMLTempEle = null;
            bool bIgnoreScripting = false;

            // Naresh Check if the Scripting needs to be ignored
            XmlElement objIgnoreScripting = null;

            //rsolanki2: extensibility updates 
            string sExtendedFormsList = string.Empty;
            //MGaba2:11/15/2010
            XmlAttribute objAttribute = null;
            LobSettings objLOBSettings = null;
            ColLobSettings objColLOBSettings=null;
            SysSettings objSys = null;
            XmlNode oClaimDepartment = null; //MITS 33588 , Nitin Goel, 07/31/2014

			try
			{
                //rsushilaggar changes made for performance improvement
                objSys = new SysSettings(m_sConnString, m_iClientId); //mbahl3  JIRA [RMACLOUD-123]

				objSelectElement=(XmlElement)p_objXMLDocument.SelectSingleNode("//PVFormEdit/Reset");
				if (objSelectElement!=null)
				{
					sReset=objSelectElement.InnerText;
				}

                objIgnoreScripting = (XmlElement)p_objXMLDocument.SelectSingleNode("//form/group/IgnoreScripting");
                
				sbSql=new StringBuilder();
				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVFormEdit/FormName"); 
				
				sFormName=objSelectElement.InnerText.Replace("%","|");

				if(sFormName.Trim() == "")
					return null;

				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVList/RowId"); 
				sViewId=objSelectElement.InnerText;
                p_PageId = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, string.Format("SELECT PAGE_ID FROM PAGE_INFO WHERE PAGE_NAME='{0}'", sFormName.ToLower().Substring(0, sFormName.ToLower().IndexOf(".xml")) + ".aspx")));
                p_LangCode = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, string.Format("SELECT  LANGUAGE_CODE FROM NET_VIEWS WHERE VIEW_ID={0} AND DATA_SOURCE_ID={1}", sViewId, s_DataSourceId)));
                //Deb : MITS 32849 
                if (string.IsNullOrEmpty(p_PageId)) { p_PageId = "0"; }
                if (string.IsNullOrEmpty(p_LangCode)) { p_LangCode = "1033"; }
                //Deb : MITS 32849 
                //rkulavil : RMA-5233 start
                strDictResourceValues = new Dictionary<string, string>();
                sbSql.Length = 0;
                sbSql = sbSql.AppendFormat("SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode);
                objReader = DbFactory.ExecuteReader(m_netViewFormsConnString, sbSql.ToString());
                while (objReader.Read())
                {
                    strDictResourceValues.Add(objReader.GetString(0), objReader.GetString(1));
                }
                objReader.Close();
                sbSql.Length = 0;
                //rkulavil : RMA-5233 end
                // npadhy The Page Names have been changed to all small case from R5. So we nee to compare with the lower case form names
                sbSql.Append("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID =" 
                    + sViewId 
                    + " AND LOWER(FORM_NAME)='" 
                    + sFormName.ToLower() 
                    + "' AND DATA_SOURCE_ID='" 
                    + s_DataSourceId 
                    + "'");
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
				if(objReader.Read())
				{
					sXml=Conversion.ConvertObjToStr(objReader.GetValue("VIEW_XML"));
					
				}
				objReader.Dispose();
                

                //rsolanki2: extensibility updates - start 
                //reading the custom view_name into a string
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, 
                    string.Concat("SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 and DATA_SOURCE_ID='"
                        ,s_DataSourceId 
                        , "'"));
                
                sbSql.Length=0;
                while (objReader.Read())
                {
                    sbSql.Append(Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME")));
                    sbSql.Append("|");
                }
                sExtendedFormsList = "|" + sbSql.ToString();
                objReader.Dispose();
                //rsolanki2:  extensibility updates  - end


				if (sXml.Trim()==""  || (sReset=="1"))
				{
					sbSql.Length=0;
					if (sFormName.StartsWith("admintracking|"))
						//Admin Tracking form
						sbSql.Append("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID=0 AND FORM_NAME='admintracking.xml' AND DATA_SOURCE_ID='" + s_DataSourceId + "'"); 						
                    else
                    {    //TODO-BSB Add logic for Extended Screen Definition
                        //int ViewId = UTILITY.IsExtendedScreen(sFormName) ? Constants.Views.EXTENDED_SCREEN_VIEWID : Constants.Views.BASE_VIEWID;

                        //rsolanki2: extensibility updates:
                        //extended page list is already added into sExtendedFormsList 
                        int ViewId = sExtendedFormsList.ToLower().Contains(string.Concat("|", sFormName.ToLower(), "|"))  
                                        ? Constants.Views.EXTENDED_SCREEN_VIEWID 
                                        : Constants.Views.BASE_VIEWID;
                        sbSql.Append(String.Format("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='{1}' AND DATA_SOURCE_ID='{2}'"
                            ,ViewId,sFormName.ToLower()
                            ,s_DataSourceId));
                    }
					objXMLForm=new XmlDocument();
                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
					if(objReader.Read())
					{
						sXml = Conversion.ConvertObjToStr(objReader["VIEW_XML"]);
						objXMLForm.LoadXml(sXml);
					}
					objReader.Dispose(); 
					if((sReset=="1")&& (sFormName.StartsWith("admintracking|")))
						objXMLForm = GetDefaultAdmView(sFormName);
				}
				else
				{
					objXMLForm=new XmlDocument();
					objXMLForm.LoadXml(sXml);
				}
				
				try
				{
					//((XmlElement)p_objXMLDocument.SelectSingleNode("//group[@name='ViewDefination']")).SetAttribute("title",((XmlElement)objXMLForm.GetElementsByTagName("form").Item(0)).GetAttribute("title"));  
				}
				catch{}
				
				// Load field list from default view
				objXMLlstFields=new XmlDocument();

				if (sFormName.StartsWith("admintracking|"))
				{
					objXMLlstFields = GetDefaultAdmView(sFormName);
                    //Added for Mits 23065
                    XmlNode objTempNode = null;
                    XmlAttribute objAttr = null;
                    objTempNode = objXMLForm.SelectSingleNode("//control[@name='recordid']");
                    if (objTempNode != null)
                    {
                        objAttr = objXMLForm.CreateAttribute("mandatory");
                        objAttr.InnerText = "true";
                        objTempNode.Attributes.Append(objAttr);
                    }
                    objTempNode = objXMLForm.SelectSingleNode("//control[@name='btnGoTo']");
                    if (objTempNode != null)
                    {
                        objAttr = objXMLForm.CreateAttribute("mandatory");
                        objAttr.InnerText = "true";
                        objTempNode.Attributes.Append(objAttr);
                    }
                    //Added for Mits 23065
				}
				else
				{
					//TODO-BSB Add Extended Screen Support
                    //int ViewId = UTILITY.IsExtendedScreen(sFormName) ? Constants.Views.EXTENDED_SCREEN_VIEWID : Constants.Views.BASE_VIEWID;
                    
                    //rsolanki2: extensibility updates
                    //extended page list is already added into sExtendedFormsList 
                    int ViewId = sExtendedFormsList.ToLower().Contains(string.Concat("|", sFormName.ToLower(), "|"))  
                                    ? Constants.Views.EXTENDED_SCREEN_VIEWID 
                                    : Constants.Views.BASE_VIEWID;
                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString
                        , (String.Format("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='{1}' AND DATA_SOURCE_ID='{2}'"
                            , ViewId
                            , sFormName.ToLower()
                            , s_DataSourceId)));
					if(objReader.Read())
					{
						sXml = Conversion.ConvertObjToStr(objReader["VIEW_XML"]);
						objXMLlstFields.LoadXml(sXml);

                        if (objIgnoreScripting != null && objIgnoreScripting.InnerText.ToLower() == "true")
                            bIgnoreScripting = true;

                        // npadhy Checked in the Utility as well
                        // MAC AddSuppDefinitionIgnoreScripts is not defined.
                        if(!bIgnoreScripting)
                            UTILITY.AddSuppDefinition(m_sUserId,m_sPassword,m_sConnString,m_sDSNName,objXMLlstFields,m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
                        else
                            UTILITY.AddSuppDefinitionIgnoreScripts(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objXMLlstFields, m_iClientId); 
                        
					}
				}
				objReader.Dispose();

				//Geeta 11-03-2006 : Reverted to fix Mits No.7612
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimwc.xml")
                if (sFormName == "claimwc.xml" || sFormName == "claimwcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    XmlNode oCM;

                    // Determine if Unit stat is in use or not
                    //rsushilaggar changes made for performance improvement
                    bool bUnitStat = objSys.UnitStat == -1 ? true : false;
                   
                    //objReader = DbFactory.GetDbReader(m_sConnString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'UNIT_STAT'");
                    //if (objReader.Read())
                    //{
                    //    bUnitStat = (Conversion.ConvertStrToInteger(objReader.GetString(0)) != 0);
                    //}
                    
                    //end rsushilaggar
                    objReader.Close();
                    objReader.Dispose();

                    if (!bUnitStat)
                    {
                        // ... kill out of form
                        oCM = objXMLForm.SelectSingleNode("//group[@name='unitstat']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);

                        // ... kill out of field list
                    }
					

                }
 
				// JP 02-28-2006   Strip out one of the case mgt tabs - powerviews can't have both because the <group> tags get renamed and fda won't know to kill one of them.
				// akaushik5 Changed for MITS 30290 Starts
                // if (sFormName == "claimwc.xml" || sFormName == "claimdi.xml")
                if (sFormName == "claimwc.xml" || sFormName == "claimwcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
     				{
					XmlNode oCM;

					// Determine if case mgt is in use or not
                    //rsushilaggar changes made for performance improvement
                    bool bCaseMgt = objSys.CaseMgmtFlag == -1 ? true : false;

                    //objReader = DbFactory.GetDbReader(m_sConnString,"SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME = 'CASE_MGT_FLAG'");
                    //if(objReader.Read())
                    //{
                    //    bCaseMgt=(Conversion.ConvertStrToInteger(objReader.GetString(0)) != 0);
                    //}
                    //end rsushilaggar
					objReader.Close();
					objReader.Dispose();
				
					if (bCaseMgt)  
					{
						// ... kill out of form
						oCM = objXMLForm.SelectSingleNode("//group[@name='employmentinfowocasemgmt']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//group[@name='employeeeventdetail']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);

						// ... kill out of field list
						oCM = objXMLlstFields.SelectSingleNode("//group[@name='employmentinfowocasemgmt']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//group[@name='employeeeventdetail']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
					}
					else
					{
						// ... kill out of form
						oCM = objXMLForm.SelectSingleNode("//group[@name='medicalinfo']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//group[@name='casemgt']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//group[@name='employmentinfo']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//control[@name='empworkdaystarttime']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//group[@name='employeeeventdetail']//control[@name='empregularjobflag']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//control[@name='emplostconsciousness']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//control[@name='emposharecordable']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLForm.SelectSingleNode("//control[@name='emposhaaccdesc']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);

						// ... kill out of field list
						oCM = objXMLlstFields.SelectSingleNode("//group[@name='medicalinfo']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//group[@name='casemgt']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//group[@name='employmentinfo']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//control[@name='empworkdaystarttime']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//group[@name='employeeeventdetail']//control[@name='empregularjobflag']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//control[@name='emplostconsciousness']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//control[@name='emposharecordable']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
						oCM = objXMLlstFields.SelectSingleNode("//control[@name='emposhaaccdesc']");
						if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
					}
				}
                //Start rsushilaggar MITS MITS 38356
                else if (sFormName == "claimdi.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    XmlNode oCM;
                    //rsushilaggar changes made for performance improvement
                    bool bCaseMgt = objSys.CaseMgmtFlag == -1 ? true : false;

                    if (bCaseMgt)
                    {
                        // ... kill out of form
                        oCM = objXMLForm.SelectSingleNode("//group[@name='employmentinfowocasemgmt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);

                        // ... kill out of field list
                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='employmentinfowocasemgmt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                    }
                    else
                    {
                        // ... kill out of form
                        //oCM = objXMLForm.SelectSingleNode("//group[@name='medicalinfo']");
                        //if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//group[@name='casemgt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        //oCM = objXMLForm.SelectSingleNode("//group[@name='employmentinfo']");
                        //if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='empworkdaystarttime']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='emplostconsciousness']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='emposharecordable']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='emposhaaccdesc']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);

                        // ... kill out of field list
                        //oCM = objXMLlstFields.SelectSingleNode("//group[@name='medicalinfo']");
                        //if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='casemgt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        //oCM = objXMLlstFields.SelectSingleNode("//group[@name='employmentinfo']");
                        //if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='empworkdaystarttime']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='emplostconsciousness']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='emposharecordable']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='emposhaaccdesc']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                    }
                }	//End rsushilaggar MITS MITS 38356
                else if (sFormName == "claimpc.xml")
                {//MGaba2: In case Filter property for selected policy is selected,Policy name control should not be deleted from powerview
                    bool bFilterPropertyForPolicy = false;
                    objColLOBSettings= new ColLobSettings(m_sConnString,m_iClientId);
                    
                    objLOBSettings = objColLOBSettings[845];                    
                    bFilterPropertyForPolicy = objLOBSettings.IsPolicyFilter;
                    
                    if (bFilterPropertyForPolicy)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLForm.CreateAttribute("mandatory");
                            objAttribute.InnerText = "true";
                            objNode.Attributes.Append(objAttribute);
                        }
                    }
                }

                if (sFormName == "entitymaint.xml")
                {
                    if (objSys.UseEntityRole)
                    {
                        XmlNode oCM = objXMLlstFields.SelectSingleNode("//control[@name='lastname']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                    }
                    else
                    {
                        XmlNode oCM = objXMLlstFields.SelectSingleNode("//control[@name='erlastname']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                    }
                }
                

                //BOB Enhancement : ukusvaha
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "event.xml" || sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml" || sFormName == "claimdi.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc 
                if (sFormName == "event.xml" || sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml" || sFormName == "claimdi.xml" || sFormName == "claimwcd.xml" || sFormName == "claimvad.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    //changed by Amitosh for MITS 27487
                    //condition to check Advance claim setting
                    //if  setting if off then department will be mandatory 
                    //objSys = new SysSettings(m_sConnString);
                    if (objSys.AutoPopulateDpt != -1)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLForm.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        }
                        objNode = objXMLForm.SelectSingleNode("//control[@name='ev_depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLForm.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        }

                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLlstFields.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        }
                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='ev_depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLlstFields.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        } 
                    }
                }
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimvad.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm != 0)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                    else
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='multipolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                }
                //BOB Enhancement End
                //dvatsa-JIRA 12099(start)
                if (sFormName == "funds.xml" && objSys.MultiCovgPerClm == 0)
                {
                    objNode = objXMLForm.SelectSingleNode("//control[@name='IsSupplementalPay']");
                    if (objNode != null)
                    {
                        objNode.ParentNode.RemoveChild(objNode);
                    }
                }
                //dvatsa-JIRA 12099(end)
				//Setting the hidden element for the required values
                if (sFormName == "bankaccount.xml" || sFormName == "bankaccountsub.xml")
                {
                    if (objSys.MultiCovgPerClm == 0)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }

                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                        
                    }
                   
                }
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml"||sFormName == "claimwc.xml")
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm == 0)
                    {

                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objAttribute = objNode.Attributes["required"];
                            objAttribute.InnerText = "no";
                           // objNode.Attributes.Append(objAttribute);
                        }

                        objNode = objXMLForm.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objAttribute = objNode.Attributes["required"];
                            objAttribute.InnerText = "no";
                          //  objNode.Attributes.Append(objAttribute);
                        }
                    }
                }
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimvad.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm != 0)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                    else
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='multipolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                }
                //dvatsa-JIRA 12099(start)
                if (sFormName == "funds.xml" && objSys.MultiCovgPerClm == 0)
                {
                    objNode = objXMLForm.SelectSingleNode("//control[@name='IsSupplementalPay']");
                    if (objNode != null)
                    {
                        objNode.ParentNode.RemoveChild(objNode);
                    }
                }
                //dvatsa -JIRA 12099(end)
				string sElementValue = "";
				objXMLSysRequired=objXMLlstFields.SelectNodes("//control[@required='yes']");
				objXMLSysRequiredHidden=objXMLForm.SelectNodes("//control[@type='hidden']");

				//-- ABhateja 08.18.2006, -START-
				//-- MITS 7629, If title attribute is null, set pvtitle value.
				objXMLPVTitle = objXMLForm.SelectNodes("//control[@pvtitle != '']");
				if(objXMLPVTitle != null)
				{
					foreach(XmlElement objElem in objXMLPVTitle)
					{
						if(objElem.ParentNode.Attributes.GetNamedItem("type") != null)
						{
							if(objElem.ParentNode.Attributes.GetNamedItem("type").Value == "controlgroup")
								sPVTitle += "+" + objElem.GetAttribute("name") + ","; 
							else
								sPVTitle += objElem.GetAttribute("name") + ",";
						}
						else
						{
							sPVTitle += objElem.GetAttribute("name") + ",";
						}
					}
					//objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='pvtitle']"); 
					if(objSelectElement != null)
						objSelectElement.SetAttribute("value",sPVTitle);
				}
				//-- ABhateja 08.18.2006, -END-


				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//sysrequired"); 
				if(objSelectElement != null)
				{
					foreach(XmlElement objElem in objXMLSysRequired)
						sElementValue += objElem.GetAttribute("name") + ",";  	
					foreach(XmlElement objElem in objXMLSysRequiredHidden)
						sElementValue += objElem.GetAttribute("name") + ",";  	
				}
				objSelectElement.SetAttribute("value",sElementValue);
                objSelectElement.InnerText = sElementValue;
                //asharma326 JIRA 6411
                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//IsBaseEditAllow");
                //check whether 
                if (objSelectElement != null)
                {
                    var BaseEditAllow = objSys.PowerViewReadOnly;
                    objSelectElement.SetAttribute("value", BaseEditAllow.ToString());
                    objSelectElement.InnerText = BaseEditAllow.ToString();
                }

				objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//FormFields");

                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimvad.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm != 0)
                    {
                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                    else
                    {
                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='multipolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                }
                //dvatsa-JIRA 12099(start)
                if (sFormName == "funds.xml" && objSys.MultiCovgPerClm == 0)
                {
                    objNode = objXMLForm.SelectSingleNode("//control[@name='IsSupplementalPay']");
                    if (objNode != null)
                    {
                        objNode.ParentNode.RemoveChild(objNode);
                    }
                }
                //dvatsa-JIRA 12099(end)
                objNode = objXMLlstFields.GetElementsByTagName("form").Item(0);
                //Start:Added by Nitin goel, MITS 33588,07/31/2014               
                if (sFormName.Equals("claimgc.xml") || sFormName.Equals("claimpc.xml") || sFormName.Equals("claimva.xml") || sFormName.Equals("claimwc.xml"))
                {
                    if (objSys.UseInsuredClaimDept != -1)
                    {
                        oClaimDepartment = objNode.SelectSingleNode("//control[@name='clm_insdepteid']");
                        if (oClaimDepartment != null)
                            oClaimDepartment.ParentNode.RemoveChild(oClaimDepartment);
                    }
                }
                //end:added by nitin goel

                //rkatyal4 : 7767 start
                if (objSys.UseEntityRole)
                {
                    RemoveTbnodes(sFormName, objNode); //rkatyal4: Refactored

                    #region Employee.xml
                    if (sFormName.Equals("employee.xml"))
                    {
                        XmlNode ENode = objNode.SelectSingleNode("//group[@name='employee']");
                        string[] Enodes = { "employeenumber", "phone1", "phone2", "maritalstatcode", "feinnumber"};
                        EditDropdownField(Enodes, ENode);

                        XmlNode EInfNode = objNode.SelectSingleNode("//group[@name='employeeinfo']");
                        string[] EInfNodes = { "emailtypecode", "emailaddress", "costcentercode", "hdDttmRcdAdded", "hdDttmRcdLastUpd", "hdupdatedbyuser", "hdaddedbyuser" };
                        EditDropdownField(EInfNodes, EInfNode);

                        XmlNode addinfoNode = objNode.SelectSingleNode("//group[@name='addressesinfo']");
                        addinfoNode.ParentNode.RemoveChild(addinfoNode);

                        XmlNode PEntinfoNode = objNode.SelectSingleNode("//group[@name='entityidtypeinfo']");
                        PEntinfoNode.ParentNode.RemoveChild(PEntinfoNode);
                    }
                    #endregion

                    #region Driver.xml
                    if (sFormName.Equals("driver.xml"))
                    {
                        XmlNode PNode = objNode.SelectSingleNode("//group[@name='Driverinfo']");
                        string[] nodes = {"entitytableid","maritalstatcode","LicenceDate","DriversLicNo" };
                        EditDropdownField(nodes, PNode);
                    }
                    #endregion

                    #region Staff.xml
                    if (sFormName.Equals("staff.xml"))
                    {
                        XmlNode StfNode = objNode.SelectSingleNode("//group[@name='staff']");
                        string[] StfNodes = { "medstaffnumber", "beepernumber", "cellularnumber", "emergencycontact", "phone1", "phone2" };
                        EditDropdownField(StfNodes, StfNode);
                        
                        XmlNode PinfoNode = objNode.SelectSingleNode("//group[@name='staffinfo']");
                        string[] PinfoNodes = { "primarypolicyid", "maritalstatcode", "hiredate", "licencenumber", "homeaddr1", "homeaddr2", "homeaddr3", "homeaddr4", "homecity", "homestateid", "homezipcode", "licenceissuedate", "licencestateid", "licenceexpirydate", "licencedeaexpdate", "licencedeanumber" };
                        EditDropdownField(PinfoNodes, PinfoNode);

                        XmlNode addinfoNode = objNode.SelectSingleNode("//group[@name='addressesinfo']");
                        addinfoNode.ParentNode.RemoveChild(addinfoNode);

                        XmlNode PEntinfoNode = objNode.SelectSingleNode("//group[@name='entityidtypeinfo']");
                        PEntinfoNode.ParentNode.RemoveChild(PEntinfoNode);
                    }
                    #endregion

                    #region Patient.xml
                    if (sFormName.Equals("patient.xml"))
                    {
                        XmlNode PNode = objNode.SelectSingleNode("//group[@name='patient']");
                        string[] PNodes = { "patphone1", "patphone2", "patmaritalstatcode" };
                        EditDropdownField(PNodes, PNode);

                        XmlNode addinfoNode = objNode.SelectSingleNode("//group[@name='addressesinfo']");
                        addinfoNode.ParentNode.RemoveChild(addinfoNode);

                        XmlNode PEntinfoNode = objNode.SelectSingleNode("//group[@name='entityidtypeinfo']");
                        PEntinfoNode.ParentNode.RemoveChild(PEntinfoNode);
                    }
                    #endregion

                    #region Physician.xml
                    if (sFormName.Equals("physician.xml"))
                    {
                        XmlNode PhyNode = objNode.SelectSingleNode("//group[@name='physician']");
                        string[] PhyNodes = { "physiciannumber", "medstaffnumber", "phone1", "phone2", "beepernumber", "cellularnumber", "emergencycontact" };
                        EditDropdownField(PhyNodes, PhyNode);

                        XmlNode PhInfNode = objNode.SelectSingleNode("//group[@name='physicianinfo']");
                        string[] PhInfNodes = { "medicarenumber", "primaryspecialty", "subspecialtylist", "maritalstatcode", "mailingaddr1", "mailingcity", "mailingaddr2", "homeaddr1", "homeaddr2", "homeaddr3", "homeaddr4", "homecity", "mailingstateid", "homestateid", "mailingzipcode", "homezipcode", "primarypolicyid" };
                        EditDropdownField(PhInfNodes, PhInfNode);
                        

                        XmlNode StaffInfNode = objNode.SelectSingleNode("//group[@name='staffinfo']");
                        int StfInfnChildIndx = 0;

                        foreach (XmlNode RemNode in StaffInfNode.ChildNodes)
                        {
                            if (RemNode.NodeType.Equals(XmlNodeType.Comment))
                                continue;
                            else if (RemNode.NodeType.Equals(XmlNodeType.Element))
                            {
                                int RemChildCount = RemNode.ChildNodes.Count;
                                int RemChildIndex = 0;
                                for (int i = 0; i < RemChildCount; i++)
                                {
                                    XmlNode xNode = RemNode.ChildNodes[RemChildIndex];
                                    if (xNode.NodeType.Equals(XmlNodeType.Comment))
                                    {
                                        RemChildIndex++;
                                        continue;
                                    }
                                    else if (xNode.NodeType.Equals(XmlNodeType.Element))
                                    {
                                        if (xNode.Attributes["name"].Value == "npinumber")
                                        {
                                            RemNode.RemoveChild(RemNode.ChildNodes[RemChildIndex]);
                                        }
                                        else
                                        {
                                            RemChildIndex++;
                                            continue;
                                        }
                                    }
                                }
                            }
                            StfInfnChildIndx++;
                        }

                        XmlNode EduNode = objNode.SelectSingleNode("//group[@name='education']");
                        string[] EduNodes = { "conteducation", "membership", "teachingexp" };
                        EditDropdownField(EduNodes, EduNode);

                        XmlNode addinfoNode = objNode.SelectSingleNode("//group[@name='addressesinfo']");
                        addinfoNode.ParentNode.RemoveChild(addinfoNode);

                        XmlNode PEntinfoNode = objNode.SelectSingleNode("//group[@name='entityidtypeinfo']");
                        PEntinfoNode.ParentNode.RemoveChild(PEntinfoNode);
                    }
                    #endregion
                }
                //rkatyal4 : 7767 end
				foreach(XmlNode objElem in objNode.ChildNodes)
				{
					if (objElem.NodeType== XmlNodeType.Element)// NODE_ELEMENT=1
					{
						if (((XmlElement)objElem).GetAttribute("type")!= "id")
						{
							if (((XmlElement)objElem).GetAttribute("type")!="hidden")
							{
								if (((XmlElement)objElem).GetAttribute("type")!="javascript")
								{
								if (objElem.Name.ToLower()=="control")
								{
									if (((XmlElement)objElem).GetAttribute("required")!=null)
									{
										if (((XmlElement)objElem).GetAttribute("required").ToLower() != "yes")
										{
											WriteFieldOptionNew((XmlElement)objElem,objForm,p_objXMLDocument);
										}
									}
								}
								else if (objElem.Name.ToLower()=="group")
								{
									WriteFieldOptionNew((XmlElement)objElem,objForm,p_objXMLDocument);
									/*
									 ' The XML structure is
											' <group ..>
											'	<displaycolumn>
											'		<control ../>
											'		<control ../>
											'	</displaycolumn>
											' </group>
									 */
									//' Parse all child nodes of <group>
									foreach(XmlNode objGroupChildNode in objElem.ChildNodes)
									{
										//If the child node is <displaycolumn>
										if (objGroupChildNode.Name.ToLower()=="displaycolumn")
										{
											//' Parse all child nodes of <displaycolumn>
											foreach(XmlNode objDisplayColumnChildNode in objGroupChildNode.ChildNodes)
											{
												//' If the child node is <control>
												if (objDisplayColumnChildNode.Name.ToLower()=="control")
												{
													if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!=null)
													{
														if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!="id")
														{
															//'-- Check for hidden fields
															if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!="hidden")
															{
																	if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!="javascript")
																	{
																WriteFieldOptionNew((XmlElement)objDisplayColumnChildNode,objForm,p_objXMLDocument);
															}
																}

														}
													}
												}
											}
										}
									}
								}
							}
							}
						}//end if (objElem.GetAttribute("type")!="hidden")
					}//end if (objElem.NodeType==1)
				}//end for

                objXMLTempEle = p_objXMLDocument.CreateElement("FormFieldsoption");
				objXMLTempEle.SetAttribute("value","newgroupcaption");
				objXMLTempEle.InnerText="[*** Add New Group Caption ***]";
				objForm.AppendChild(objXMLTempEle);

                objXMLTempEle = p_objXMLDocument.CreateElement("FormFieldsoption");
				objXMLTempEle.SetAttribute("value","newgroupcaption");
				objXMLTempEle.InnerText="[MS* Add New Message *MS]";
				objForm.AppendChild(objXMLTempEle);

                //Code updated by pawan to byppass spaces implementation for 11210 on tabviews 
                objXMLTempEle = p_objXMLDocument.CreateElement("FormFieldsoption");
                objXMLTempEle.SetAttribute("value", "newgroupcaption");
                objXMLTempEle.InnerText = "[HS* New Space *HS]";
                objForm.AppendChild(objXMLTempEle);
                //Code change ends implementation for 11210 on tabviews by pawan 

				objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//FormList"); 
                //if condition added bu Shivendu for MITS 11492
                if (sReset == "1")
                {
                    objNode = objXMLlstFields.GetElementsByTagName("form").Item(0);
                }
                else
                {
                    objNode = objXMLForm.GetElementsByTagName("form").Item(0);
                }
				foreach(XmlNode objElem in objNode.ChildNodes)
				{
					if (objElem.NodeType== XmlNodeType.Element)// NODE_ELEMENT=1
					{
						if ( ((XmlElement)objElem).GetAttribute("type")!="id")
						{
							if (((XmlElement)objElem).GetAttribute("type")!="hidden")
							{
								if (((XmlElement)objElem).GetAttribute("type")!="javascript")
								{
								if (objElem.Name.ToLower()=="control")
								{
									WriteFieldOption((XmlElement)objElem,objForm,p_objXMLDocument);
								}
								else if (objElem.Name.ToLower()=="group")
								{
                                    //rkatyal4: 7767 start
                                    if (objSys.UseEntityRole)
                                    {
                                        #region Employee Field List
                                        if (sFormName.Equals("employee.xml"))
                                        {
                                            if (objElem.Attributes["name"].Value == "employee")
                                            {
                                                string[] fieldList = { "employeenumber", "phone1", "phone2", "maritalstatcode", "feinnumber" };
                                                EditFieldList(fieldList, objElem);
                                            }
                                            
                                            if (objElem.Attributes["name"].Value == "employeeinfo")
                                            {
                                                string[] fieldList = { "emailtypecode", "emailaddress", "costcentercode", "hdDttmRcdAdded", "hdDttmRcdLastUpd", "hdupdatedbyuser", "hdaddedbyuser" };
                                                EditFieldList(fieldList, objElem);
                                            }
                                            if (objElem.Attributes["name"].Value == "addressesinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                            if (objElem.Attributes["name"].Value == "entityidtypeinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                        }
                                        #endregion

                                        #region Driver Field list
                                        if (sFormName.Equals("driver.xml"))
                                        {
                                            string[] fieldList = { "entitytableid", "maritalstatcode", "DriversLicNo", "LicenceDate" };
                                                EditFieldList(fieldList, objElem);
                                        }
                                        #endregion

                                        #region staff Field list
                                        if (sFormName.Equals("staff.xml"))
                                        {
                                            if (objElem.Attributes["name"].Value == "staff")
                                            {
                                                string[] fieldList = { "medstaffnumber", "beepernumber", "cellularnumber", "emergencycontact", "phone1", "phone2" };
                                                EditFieldList(fieldList, objElem);
                                            }

                                            if (objElem.Attributes["name"].Value == "staffinfo")
                                            {
                                                string[] fieldList = { "primarypolicyid", "maritalstatcode", "hiredate", "licencenumber", "homeaddr1", "homeaddr2", "homeaddr3", "homeaddr4", "homecity", "homestateid", "homezipcode", "licenceissuedate", "licencestateid", "licenceexpirydate", "licencedeaexpdate", "licencedeanumber" };
                                                EditFieldList(fieldList, objElem);
                                            }

                                            if (objElem.Attributes["name"].Value == "addressesinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                            if (objElem.Attributes["name"].Value == "entityidtypeinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                        }
                                        #endregion

                                        #region Patient Field List
                                        if (sFormName.Equals("patient.xml"))
                                        {
                                            if (objElem.Attributes["name"].Value == "patient")
                                                foreach (XmlNode Xpnode in objElem)
                                                {
                                                    string[] fieldList = { "patphone1", "patphone2", "patmaritalstatcode" };
                                                    EditFieldList(fieldList, objElem);
                                                }

                                            if (objElem.Attributes["name"].Value == "addressesinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                            if (objElem.Attributes["name"].Value == "entityidtypeinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                        }
                                        #endregion

                                        #region Physician Field List
                                        if (sFormName.Equals("physician.xml"))
                                        {
                                            if (objElem.Attributes["name"].Value == "physician")
                                            {
                                                string[] fieldList = { "physiciannumber", "medstaffnumber", "phone1", "phone2", "beepernumber", "cellularnumber", "emergencycontact" };
                                                EditFieldList(fieldList, objElem);
                                            }

                                            if (objElem.Attributes["name"].Value == "physicianinfo")
                                            {
                                                string[] fieldList = { "medicarenumber", "primaryspecialty", "subspecialtylist", "maritalstatcode", "mailingaddr1", "mailingcity", "mailingaddr2", "homeaddr1", "homeaddr2", "homeaddr3", "homeaddr4", "homecity", "mailingstateid", "homestateid", "mailingzipcode", "homezipcode", "primarypolicyid" };
                                                EditFieldList(fieldList, objElem);
                                            }

                                            if (objElem.Attributes["name"].Value == "staffinfo")
                                                foreach (XmlNode Xpnode in objElem)
                                                {
                                                    if (Xpnode.NodeType.Equals(XmlNodeType.Comment))
                                                        continue;
                                                    else if (Xpnode.NodeType.Equals(XmlNodeType.Element))
                                                    {
                                                        int XpChildCount = Xpnode.ChildNodes.Count;
                                                        int XpChildIndex = 0;
                                                        for (int i = 0; i < XpChildCount; i++)
                                                        {
                                                            XmlNode xNode = Xpnode.ChildNodes[XpChildIndex];
                                                            if (xNode.NodeType.Equals(XmlNodeType.Comment))
                                                            {
                                                                XpChildIndex++;
                                                                continue;
                                                            }
                                                            else if (xNode.NodeType.Equals(XmlNodeType.Element))
                                                            {
                                                                if (xNode.Attributes["name"].Value == "npinumber")
                                                                {
                                                                    Xpnode.RemoveChild(Xpnode.ChildNodes[XpChildIndex]);
                                                                }
                                                                else
                                                                {
                                                                    XpChildIndex++;
                                                                    continue;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            if (objElem.Attributes["name"].Value == "education")
                                            {
                                                string[] fieldList = { "conteducation", "membership", "teachingexp" };
                                                EditFieldList(fieldList, objElem);
                                            }

                                            if (objElem.Attributes["name"].Value == "addressesinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                            if (objElem.Attributes["name"].Value == "entityidtypeinfo")
                                            {
                                                objElem.InnerXml.Remove(0);
                                            }
                                        }
                                        #endregion
                                    }
                                    //rkatyal4: 7767 end
									WriteFieldOption((XmlElement)objElem,objForm,p_objXMLDocument);
									/*
									 ' The XML structure is
											' <group ..>
											'	<displaycolumn>
											'		<control ../>
											'		<control ../>
											'	</displaycolumn>
											' </group>
									 */
									//' Parse all child nodes of <group>
									foreach(XmlNode  objGroupChildNode in objElem.ChildNodes)
									{
										//If the child node is <displaycolumn>
										if (objGroupChildNode.Name.ToLower()=="displaycolumn")
										{
											//' Parse all child nodes of <displaycolumn>
											foreach(XmlNode  objDisplayColumnChildNode in objGroupChildNode.ChildNodes)
											{
												//' If the child node is <control>
												if (objDisplayColumnChildNode.Name.ToLower()=="control")
												{
													if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!=null)
													{
														if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!="id")
														{
															//'-- Check for hidden fields
															if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!="hidden")
															{
																if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type")!="javascript")
																{
																	WriteFieldOption((XmlElement)objDisplayColumnChildNode,objForm,p_objXMLDocument);
																}
															}
														}
													}
												}
											}
										}//end if (objGroupChildNode.Name.ToLower()=="displaycolumn")
										else if (objGroupChildNode.Name.ToLower()=="message")
										{
											WriteFieldOption((XmlElement)objGroupChildNode,objForm,p_objXMLDocument);
										}
									}
								}
							}
							}
						}//end if (objElem.GetAttribute("type")!="hidden")
					}//end if (objElem.NodeType==1)
				}//end for
				objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//CommandButtons"); 
				objNodeList=objXMLlstFields.SelectNodes("form/button");
				foreach(XmlElement objElem in objNodeList)
				{
					WriteFieldOptionNew(objElem,objForm,p_objXMLDocument);
                }

               

                objBaseNodeList = objXMLlstFields.SelectNodes("form/buttonscript|form/button");
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ButtonList");
                //Code modified by nadim  for MITS 12782
                objNodeList = objXMLForm.SelectNodes("form/buttonscript|form/button");
                foreach (XmlElement objElem in objNodeList)
                {
                    foreach (XmlElement objBaseElem in objBaseNodeList)
                    {
                        if (objBaseElem.Attributes["name"] == null) continue;
                        if (objElem.Attributes["name"].Value == objBaseElem.Attributes["name"].Value)
                        {
                            WriteFieldOption(objElem, objForm, p_objXMLDocument);
                            break;
                        }
                    }
                }

                //nadim MITS 12782
                //Added by Shivendu for MITS 8617
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//CommandButtons");
                objNodeList = objXMLlstFields.SelectNodes("form/buttonscript");
                foreach (XmlElement objElem in objNodeList)
                {
                    WriteFieldOptionNew(objElem, objForm, p_objXMLDocument);
                }
                //nadim MITS 12782..
                //Code commented because it fails to arrange button and button script in desired order,rather
                //it add these button sequentially.
                //objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='ButtonList']");
                //objNodeList = objXMLForm.SelectNodes("form/buttonscript");
                //foreach (XmlElement objElem in objNodeList)
                //{
                //    WriteFieldOption(objElem, objForm, p_objXMLDocument);
                //}
                //Added by Shivendu for MITS 8617

                //Code Commented by nadim For MITS 12782

				objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ToolBarButtons"); 
				objBaseNodeList=objXMLlstFields.SelectNodes("form/toolbar/button");
                foreach (XmlElement objElem in objBaseNodeList)
				{
					WriteFieldOptionNew(objElem,objForm,p_objXMLDocument);
				}

				objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ToolBarList"); 
				objNodeList=objXMLForm.SelectNodes("form/toolbar/button");
				foreach(XmlElement objElem in objNodeList)
				{
                    // npadhy MITS 16920 Added to check if the Toolbar button is present in Base Xml, if it is then only that button will appear in Powerviews
                    foreach (XmlElement objBaseElem in objBaseNodeList)
                    {
                        if (objElem.Attributes["type"].Value == objBaseElem.Attributes["type"].Value)
                        {
                            WriteFieldOption(objElem, objForm, p_objXMLDocument);
                            break;
                        }
                    }
				}

                //srajindersin 4/22/2014 MITS 36097
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ToolBarList");
                objNodeList = objXMLForm.SelectNodes("form/buttonscript");
                foreach (XmlElement objElem in objNodeList)
                {
                    // npadhy MITS 16920 Added to check if the Toolbar button is present in Base Xml, if it is then only that button will appear in Powerviews
                    foreach (XmlElement objBaseElem in objBaseNodeList)
                    {
                        if(objBaseElem.Attributes["name"] == null) continue;
                        if (objElem.Attributes["name"].Value == objBaseElem.Attributes["name"].Value)
                        {
                            //WriteFieldOption(objElem, objForm, p_objXMLDocument);
                            XmlElement objElemTemp = p_objXMLDocument.CreateElement(objForm.Name + "option");
                            objElemTemp.SetAttribute("value", objBaseElem.GetAttribute("type"));
                            objElemTemp.InnerText = GetFieldTitle(objElem);
                            objForm.AppendChild(objElemTemp);
                            break;
                        }
                    }

                }

                //srajindersin 4/22/2014 MITS 36097
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ToolBarList");
                //Code modified by nadim  for MITS 12782
                objNodeList = objXMLForm.SelectNodes("form/button");
                objBaseNodeList = objXMLlstFields.SelectNodes("form/toolbar/button");
                foreach (XmlElement objElem in objNodeList)
                {
                    foreach (XmlElement objBaseElem in objBaseNodeList)
                    {
                        if (objBaseElem.Attributes["name"] == null) continue;
                        if (objElem.Attributes["name"].Value == objBaseElem.Attributes["name"].Value)
                        {
                            XmlElement objElemTemp = p_objXMLDocument.CreateElement(objForm.Name + "option");
                            objElemTemp.SetAttribute("value", objBaseElem.GetAttribute("type"));
                            objElemTemp.InnerText = GetFieldTitle(objElem);
                            objForm.AppendChild(objElemTemp);
                            break;
                            //WriteFieldOption(objElem, objForm, p_objXMLDocument);
                        }
                    }
                }

				objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//OnlyTopDownLayout"); 
				objForm.SetAttribute("value","1");
                objForm.InnerText = "true";
				if (ShowButtonsOnTop(objXMLForm))
				{
					objForm.InnerText="1";
				}
				else
				{
					objForm.InnerText="0";
				}
				if (bAdt)
				{
					objForm = (XmlElement) objXMLForm.GetElementsByTagName("form").Item(0);
					if (objForm.GetAttribute("goto")!=null)
					{
						sChecked=objForm.GetAttribute("goto");
					}
					objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//AdmTracGoButton"); 
					objForm.SetAttribute("value","1");
					objForm.SetAttribute("visible","1");
					objForm.InnerText=sChecked;	
				}

				objForm = (XmlElement) objXMLForm.GetElementsByTagName("form").Item(0);
				if (objForm.GetAttribute("readonly")!=null)
					sChecked=objForm.GetAttribute("readonly");
				objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ReadOnlyForm"); 
				objForm.SetAttribute("value","1");
				objForm.InnerText=sChecked;
                //Added By Abhishek for Top-down layout------ Start
                sChecked = "false";
                objForm = (XmlElement)objXMLForm.GetElementsByTagName("form").Item(0);
                if (objForm.GetAttribute("LayOut") != null)
                    sChecked = objForm.GetAttribute("LayOut");

                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//IsTopDownLayout");
                objForm.SetAttribute("value", "1");
                objForm.InnerText = sChecked;
                //Added By Abhishek for Top-down layout------ End
				return p_objXMLDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVFormEdit.Get.Error", m_iClientId),p_objEx); //mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{
				if(objReader != null)
				{
					if (!objReader.IsClosed)
					{
						objReader.Close();                        
					}
                    objReader.Dispose();					
				}
				objSelectElement=null;
				sbSql=null;
				objNode=null;
				objNodeList=null;
                objBaseNodeList = null;
				objXMLlstFields=null;
				objXMLForm=null;
				objForm=null;
				objXMLSysRequired=null;
				objXMLSysRequiredHidden=null;
                objLOBSettings = null;
			}
		}

        //Rkatyal : Code refactoring start
        private void RemoveTbnodes(string sFormName, XmlNode objNode)
        {
            if (sFormName.Equals("driver.xml") || sFormName.Equals("staff.xml") || sFormName.Equals("patient.xml") || sFormName.Equals("physician.xml"))
            {
                string[] nodes = { "lookup", "attach", "diary", "withholding", "BankingInfo" };
                foreach (string node in nodes)
                {
                    XmlNode RemNode = objNode.SelectSingleNode("//button[@type='" + node + "']");
                    RemNode.ParentNode.RemoveChild(RemNode);
                }
            }
            else if (sFormName.Equals("employee.xml"))
            {
                string[] nodes = { "lookup", "attach", "diary", "comments", "withholding", "BankingInfo" };
                foreach (string node in nodes)
                {
                    XmlNode RemNode = objNode.SelectSingleNode("//button[@type='" + node + "']");
                    RemNode.ParentNode.RemoveChild(RemNode);
                }
            }
        }

        private void EditDropdownField(string[] nodes, XmlNode PNode)
        {
            int PnChildIndx = 0;

            foreach (XmlNode RemNode in PNode.ChildNodes)
            {
                if (RemNode.NodeType.Equals(XmlNodeType.Comment))
                    continue;
                else if (RemNode.NodeType.Equals(XmlNodeType.Element))
                {
                    int RemChildCount = RemNode.ChildNodes.Count;
                    int RemChildIndex = 0;
                    for (int i = 0; i < RemChildCount; i++)
                    {
                        XmlNode xNode = RemNode.ChildNodes[RemChildIndex];
                        if (xNode.NodeType.Equals(XmlNodeType.Comment))
                        {
                            RemChildIndex++;
                            continue;
                        }
                        else if (xNode.NodeType.Equals(XmlNodeType.Element))
                        {
                            //if (xNode.Attributes["name"].Value == "entitytableid" || xNode.Attributes["name"].Value == "maritalstatcode" || xNode.Attributes["name"].Value == "DriversLicNo" || xNode.Attributes["name"].Value == "LicenceDate")
                            if (nodes.Contains<string>(xNode.Attributes["name"].Value))
                            {
                                RemChildIndex++;
                                continue;
                            }
                            else
                            {
                                RemNode.RemoveChild(RemNode.ChildNodes[RemChildIndex]);
                            }
                        }
                    }
                }
                PnChildIndx++;
            }
        }

        private void EditFieldList(string[] nodes, XmlNode objElem)
        {
            foreach (XmlNode Xpnode in objElem)
            {
                if (Xpnode.NodeType.Equals(XmlNodeType.Comment))
                    continue;
                else if (Xpnode.NodeType.Equals(XmlNodeType.Element))
                {
                    int XpChildCount = Xpnode.ChildNodes.Count;
                    int XpChildIndex = 0;
                    for (int i = 0; i < XpChildCount; i++)
                    {
                        XmlNode xNode = Xpnode.ChildNodes[XpChildIndex];
                        if (xNode.NodeType.Equals(XmlNodeType.Comment))
                        {
                            XpChildIndex++;
                            continue;
                        }
                        else if (xNode.NodeType.Equals(XmlNodeType.Element))
                        {
                            if (nodes.Contains<string>(xNode.Attributes["name"].Value))
                            {
                                XpChildIndex++;
                                continue;
                            }
                            else
                            {
                                Xpnode.RemoveChild(Xpnode.ChildNodes[XpChildIndex]);
                            }
                        }
                    }
                }
            }
        }
        //Rkatyal : Code refactoring end

        //Deb ML Changes
        /// <summary>
        /// GetNew
        /// </summary>
        /// <param name="p_objXMLDocument"></param>
        /// <returns></returns>
        public XmlDocument GetNew(XmlDocument p_objXMLDocument)
        {
            DbReader objReader = null;
            XmlElement objSelectElement = null;
            StringBuilder sbSql = null;
            string sViewId = "";
            string sFormName = "";
            string sXml = "";
            string sChecked = "";
            string sPVTitle = "";
            XmlNode objNode = null;
            XmlNodeList objNodeList = null;
            XmlNodeList objBaseNodeList = null;
            XmlDocument objXMLlstFields = null;
            XmlDocument objXMLForm = null;
            XmlElement objForm = null;
            XmlNodeList objXMLSysRequired = null;
            XmlNodeList objXMLSysRequiredHidden = null;
            XmlNodeList objXMLPVTitle = null;
            bool bAdt = false;
            string sReset = "";
            XmlElement objXMLTempEle = null;
            bool bIgnoreScripting = false;

            XmlElement objIgnoreScripting = null;

            string sExtendedFormsList = string.Empty;
            XmlAttribute objAttribute = null;
            LobSettings objLOBSettings = null;
            ColLobSettings objColLOBSettings = null;
            SysSettings objSys = null;

            try
            {
                objSys = new SysSettings(m_sConnString,m_iClientId);//mbahl3 JIRA [RMACLOUD-123]

                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVFormEdit/Reset");
                if (objSelectElement != null)
                {
                    sReset = objSelectElement.InnerText;
                }

                objIgnoreScripting = (XmlElement)p_objXMLDocument.SelectSingleNode("//form/group/IgnoreScripting");

                sbSql = new StringBuilder();
                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVFormEdit/FormName");

                sFormName = objSelectElement.InnerText.Replace("%", "|");

                if (sFormName.Trim() == "")
                    return null;

                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//PVList/RowId");
                sViewId = objSelectElement.InnerText;
                p_PageId = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, string.Format("SELECT PAGE_ID FROM PAGE_INFO WHERE PAGE_NAME='{0}'", sFormName.ToLower().Substring(0, sFormName.ToLower().IndexOf(".xml")) + ".aspx")));
                p_LangCode = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, string.Format("SELECT  LANGUAGE_CODE FROM NET_VIEWS WHERE VIEW_ID={0} AND DATA_SOURCE_ID={1}", sViewId, s_DataSourceId)));
                //rkulavil : RMA-5233 start
                if (string.IsNullOrEmpty(p_PageId)) { p_PageId = "0"; }
                if (string.IsNullOrEmpty(p_LangCode)) { p_LangCode = "1033"; }
               // bool bSuccess = false;
                strDictResourceValues = new Dictionary<string, string>();
                sbSql.Length = 0;
                sbSql = sbSql.AppendFormat("SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE  PAGE_ID={0} AND LANGUAGE_ID={1} ORDER BY RESOURCE_TYPE", p_PageId, p_LangCode);
                objReader = DbFactory.ExecuteReader(m_netViewFormsConnString, sbSql.ToString());
                while (objReader.Read())
                {
                    strDictResourceValues.Add(objReader.GetString(0), objReader.GetString(1));
                }
                objReader.Close();
                //rkulavil : RMA-5233 end
                

                sbSql.Length = 0;
                sbSql.Append("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID ="
                    + sViewId
                    + " AND LOWER(FORM_NAME)='"
                    + sFormName.ToLower()
                    + "' AND DATA_SOURCE_ID='"
                    + s_DataSourceId
                    + "'");
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
                if (objReader.Read())
                {
                    sXml = Conversion.ConvertObjToStr(objReader.GetValue("VIEW_XML"));

                }
                objReader.Dispose();


                objReader = DbFactory.GetDbReader(m_netViewFormsConnString,
                    string.Concat("SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 and DATA_SOURCE_ID='"
                        , s_DataSourceId
                        , "'"));

                sbSql.Length = 0;
                while (objReader.Read())
                {
                    sbSql.Append(Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME")));
                    sbSql.Append("|");
                }
                sExtendedFormsList = "|" + sbSql.ToString();
                objReader.Dispose();

                if (sXml.Trim() == "" || (sReset == "1"))
                {
                    sbSql.Length = 0;
                    if (sFormName.StartsWith("admintracking|"))
                        sbSql.Append("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID=0 AND FORM_NAME='admintracking.xml' AND DATA_SOURCE_ID='" + s_DataSourceId + "'");
                    else
                    {    
                        int ViewId = sExtendedFormsList.ToLower().Contains(string.Concat("|", sFormName.ToLower(), "|"))
                                        ? Constants.Views.EXTENDED_SCREEN_VIEWID
                                        : Constants.Views.BASE_VIEWID;
                        sbSql.Append(String.Format("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='{1}' AND DATA_SOURCE_ID='{2}'"
                            , ViewId, sFormName.ToLower()
                            , s_DataSourceId));
                    }
                    objXMLForm = new XmlDocument();
                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
                    if (objReader.Read())
                    {
                        sXml = Conversion.ConvertObjToStr(objReader["VIEW_XML"]);
                        objXMLForm.LoadXml(sXml);
                    }
                    objReader.Dispose();
                    if ((sReset == "1") && (sFormName.StartsWith("admintracking|")))
                        objXMLForm = GetDefaultAdmView(sFormName);
                }
                else
                {
                    objXMLForm = new XmlDocument();
                    objXMLForm.LoadXml(sXml);
                }

                try
                {
                }
                catch { }

                objXMLlstFields = new XmlDocument();

                if (sFormName.StartsWith("admintracking|"))
                {
                    objXMLlstFields = GetDefaultAdmView(sFormName);
                    XmlNode objTempNode = null;
                    XmlAttribute objAttr = null;
                    objTempNode = objXMLForm.SelectSingleNode("//control[@name='recordid']");
                    if (objTempNode != null)
                    {
                        objAttr = objXMLForm.CreateAttribute("mandatory");
                        objAttr.InnerText = "true";
                        objTempNode.Attributes.Append(objAttr);
                    }
                    objTempNode = objXMLForm.SelectSingleNode("//control[@name='btnGoTo']");
                    if (objTempNode != null)
                    {
                        objAttr = objXMLForm.CreateAttribute("mandatory");
                        objAttr.InnerText = "true";
                        objTempNode.Attributes.Append(objAttr);
                    }
                }
                else
                {
                    int ViewId = sExtendedFormsList.ToLower().Contains(string.Concat("|", sFormName.ToLower(), "|"))
                                    ? Constants.Views.EXTENDED_SCREEN_VIEWID
                                    : Constants.Views.BASE_VIEWID;
                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString
                        , (String.Format("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='{1}' AND DATA_SOURCE_ID='{2}'"
                            , ViewId
                            , sFormName.ToLower()
                            , s_DataSourceId)));
                    if (objReader.Read())
                    {
                        sXml = Conversion.ConvertObjToStr(objReader["VIEW_XML"]);
                        objXMLlstFields.LoadXml(sXml);

                        if (objIgnoreScripting != null && objIgnoreScripting.InnerText.ToLower() == "true")
                            bIgnoreScripting = true;

                        if (!bIgnoreScripting)
                            UTILITY.AddSuppDefinition(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objXMLlstFields, m_iClientId);//mbahl3 JIRA [RMACLOUD-123]
                        else
                            UTILITY.AddSuppDefinitionIgnoreScripts(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objXMLlstFields, m_iClientId);

                    }
                }
                objReader.Dispose();

                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimwc.xml")
                if (sFormName == "claimwc.xml" || sFormName == "claimwcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    XmlNode oCM;

                    bool bUnitStat = objSys.UnitStat == -1 ? true : false;
                    objReader.Close();
                    objReader.Dispose();

                    if (!bUnitStat)
                    {
                        oCM = objXMLForm.SelectSingleNode("//group[@name='unitstat']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                    }


                }

                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimwc.xml" || sFormName == "claimdi.xml")
                if (sFormName == "claimwc.xml" || sFormName == "claimdi.xml" || sFormName == "claimwcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    XmlNode oCM;

                    bool bCaseMgt = objSys.CaseMgmtFlag == -1 ? true : false;
                    objReader.Close();
                    objReader.Dispose();

                    if (bCaseMgt)
                    {
                        oCM = objXMLForm.SelectSingleNode("//group[@name='employmentinfowocasemgmt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//group[@name='employeeeventdetail']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='employmentinfowocasemgmt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='employeeeventdetail']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                    }
                    else
                    {
                        oCM = objXMLForm.SelectSingleNode("//group[@name='medicalinfo']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//group[@name='casemgt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//group[@name='employmentinfo']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='empworkdaystarttime']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//group[@name='employeeeventdetail']//control[@name='empregularjobflag']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='emplostconsciousness']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='emposharecordable']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLForm.SelectSingleNode("//control[@name='emposhaaccdesc']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);

                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='medicalinfo']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='casemgt']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='employmentinfo']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='empworkdaystarttime']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//group[@name='employeeeventdetail']//control[@name='empregularjobflag']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='emplostconsciousness']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='emposharecordable']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                        oCM = objXMLlstFields.SelectSingleNode("//control[@name='emposhaaccdesc']");
                        if (oCM != null) oCM.ParentNode.RemoveChild(oCM);
                    }
                }
                else if (sFormName == "claimpc.xml")
                {
                    bool bFilterPropertyForPolicy = false;
                    objColLOBSettings = new ColLobSettings(m_sConnString,m_iClientId);

                    objLOBSettings = objColLOBSettings[845];
                    bFilterPropertyForPolicy = objLOBSettings.IsPolicyFilter;

                    if (bFilterPropertyForPolicy)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLForm.CreateAttribute("mandatory");
                            objAttribute.InnerText = "true";
                            objNode.Attributes.Append(objAttribute);
                        }
                    }
                }
                
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "event.xml" || sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml" || sFormName == "claimdi.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "event.xml" || sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimvad.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimdi.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.AutoPopulateDpt != -1)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLForm.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        }
                        objNode = objXMLForm.SelectSingleNode("//control[@name='ev_depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLForm.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        }

                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLlstFields.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        }
                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='ev_depteid']");
                        if (objNode != null)
                        {
                            objAttribute = objXMLlstFields.CreateAttribute("required");
                            objAttribute.InnerText = "yes";
                            objNode.Attributes.Append(objAttribute);
                        }
                    }
                }
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimvad.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm != 0)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                    else
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='multipolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                }
                if (sFormName == "bankaccount.xml" || sFormName == "bankaccountsub.xml")
                {
                    if (objSys.MultiCovgPerClm == 0)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }

                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }

                    }

                }
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml" || sFormName == "claimwc.xml")
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm == 0)
                    {

                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objAttribute = objNode.Attributes["required"];
                            objAttribute.InnerText = "no";
                        }

                        objNode = objXMLForm.SelectSingleNode("//control[@name='policyLOBCode']");
                        if (objNode != null)
                        {
                            objAttribute = objNode.Attributes["required"];
                            objAttribute.InnerText = "no";
                        }
                    }
                }
                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimvad.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm != 0)
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                    else
                    {
                        objNode = objXMLForm.SelectSingleNode("//control[@name='multipolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                }
                string sElementValue = "";
                objXMLSysRequired = objXMLlstFields.SelectNodes("//control[@required='yes']");
                objXMLSysRequiredHidden = objXMLForm.SelectNodes("//control[@type='hidden']");

                objXMLPVTitle = objXMLForm.SelectNodes("//control[@pvtitle != '']");
                if (objXMLPVTitle != null)
                {
                    foreach (XmlElement objElem in objXMLPVTitle)
                    {
                        if (objElem.ParentNode.Attributes.GetNamedItem("type") != null)
                        {
                            if (objElem.ParentNode.Attributes.GetNamedItem("type").Value == "controlgroup")
                                sPVTitle += "+" + objElem.GetAttribute("name") + ",";
                            else
                                sPVTitle += objElem.GetAttribute("name") + ",";
                        }
                        else
                        {
                            sPVTitle += objElem.GetAttribute("name") + ",";
                        }
                    }
                    if (objSelectElement != null)
                        objSelectElement.SetAttribute("value", sPVTitle);
                }


                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//sysrequired");
                if (objSelectElement != null)
                {
                    foreach (XmlElement objElem in objXMLSysRequired)
                        sElementValue += objElem.GetAttribute("name") + ",";
                    foreach (XmlElement objElem in objXMLSysRequiredHidden)
                        sElementValue += objElem.GetAttribute("name") + ",";
                }
                objSelectElement.SetAttribute("value", sElementValue);
                objSelectElement.InnerText = sElementValue;

                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//FormFields");

                // akaushik5 Changed for MITS 30290 Starts
                //if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimwc.xml")
                // kkaur8 added claimvad.xml for MITS 33072
                //rsharma220 MITS 33073 added claimgc
                if (sFormName == "claimgc.xml" || sFormName == "claimpc.xml" || sFormName == "claimva.xml" || sFormName == "claimvad.xml" || sFormName == "claimwc.xml" || sFormName == "claimwcd.xml" || sFormName == "claimgcd.xml")
                // akaushik5 Changed for MITS 30290 Ends
                {
                    if (objSys.MultiCovgPerClm != 0)
                    {
                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='primarypolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                    else
                    {
                        objNode = objXMLlstFields.SelectSingleNode("//control[@name='multipolicyid']");
                        if (objNode != null)
                        {
                            objNode.ParentNode.RemoveChild(objNode);
                        }
                    }
                }
                objNode = objXMLlstFields.GetElementsByTagName("form").Item(0);
                foreach (XmlNode objElem in objNode.ChildNodes)
                {
                    if (objElem.NodeType == XmlNodeType.Element)// NODE_ELEMENT=1
                    {
                        if (((XmlElement)objElem).GetAttribute("type") != "id")
                        {
                            if (((XmlElement)objElem).GetAttribute("type") != "hidden")
                            {
                                if (((XmlElement)objElem).GetAttribute("type") != "javascript")
                                {
                                    if (objElem.Name.ToLower() == "control")
                                    {
                                        if (((XmlElement)objElem).GetAttribute("required") != null)
                                        {
                                            if (((XmlElement)objElem).GetAttribute("required").ToLower() != "yes")
                                            {
                                                WriteFieldOptionNew((XmlElement)objElem, objForm, p_objXMLDocument);
                                            }
                                        }
                                    }
                                    else if (objElem.Name.ToLower() == "group")
                                    {
                                        WriteFieldOptionNew((XmlElement)objElem, objForm, p_objXMLDocument);
                                        /*
                                         ' The XML structure is
                                                ' <group ..>
                                                '	<displaycolumn>
                                                '		<control ../>
                                                '		<control ../>
                                                '	</displaycolumn>
                                                ' </group>
                                         */
                                        //' Parse all child nodes of <group>
                                        foreach (XmlNode objGroupChildNode in objElem.ChildNodes)
                                        {
                                            //If the child node is <displaycolumn>
                                            if (objGroupChildNode.Name.ToLower() == "displaycolumn")
                                            {
                                                //' Parse all child nodes of <displaycolumn>
                                                foreach (XmlNode objDisplayColumnChildNode in objGroupChildNode.ChildNodes)
                                                {
                                                    //' If the child node is <control>
                                                    if (objDisplayColumnChildNode.Name.ToLower() == "control")
                                                    {
                                                        if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != null)
                                                        {
                                                            if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "id")
                                                            {
                                                                //'-- Check for hidden fields
                                                                if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "hidden")
                                                                {
                                                                    if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "javascript")
                                                                    {
                                                                        WriteFieldOptionNew((XmlElement)objDisplayColumnChildNode, objForm, p_objXMLDocument);
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }//end if (objElem.GetAttribute("type")!="hidden")
                    }//end if (objElem.NodeType==1)
                }//end for

                objXMLTempEle = p_objXMLDocument.CreateElement("FormFieldsoption");
                objXMLTempEle.SetAttribute("value", "newgroupcaption");
                objXMLTempEle.InnerText = "[*** Add New Group Caption ***]";
                objForm.AppendChild(objXMLTempEle);

                objXMLTempEle = p_objXMLDocument.CreateElement("FormFieldsoption");
                objXMLTempEle.SetAttribute("value", "newgroupcaption");
                objXMLTempEle.InnerText = "[MS* Add New Message *MS]";
                objForm.AppendChild(objXMLTempEle);

                objXMLTempEle = p_objXMLDocument.CreateElement("FormFieldsoption");
                objXMLTempEle.SetAttribute("value", "newgroupcaption");
                objXMLTempEle.InnerText = "[HS* New Space *HS]";
                objForm.AppendChild(objXMLTempEle);

                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//FormList");
                if (sReset == "1")
                {
                    objNode = objXMLlstFields.GetElementsByTagName("form").Item(0);
                }
                else
                {
                    objNode = objXMLForm.GetElementsByTagName("form").Item(0);
                }
                foreach (XmlNode objElem in objNode.ChildNodes)
                {
                    if (objElem.NodeType == XmlNodeType.Element)// NODE_ELEMENT=1
                    {
                        if (((XmlElement)objElem).GetAttribute("type") != "id")
                        {
                            if (((XmlElement)objElem).GetAttribute("type") != "hidden")
                            {
                                if (((XmlElement)objElem).GetAttribute("type") != "javascript")
                                {
                                    if (objElem.Name.ToLower() == "control")
                                    {
                                        WriteFieldOptionNew((XmlElement)objElem, objForm, p_objXMLDocument);
                                    }
                                    else if (objElem.Name.ToLower() == "group")
                                    {
                                        WriteFieldOptionNew((XmlElement)objElem, objForm, p_objXMLDocument);
                                        /*
                                         ' The XML structure is
                                                ' <group ..>
                                                '	<displaycolumn>
                                                '		<control ../>
                                                '		<control ../>
                                                '	</displaycolumn>
                                                ' </group>
                                         */
                                        //' Parse all child nodes of <group>
                                        foreach (XmlNode objGroupChildNode in objElem.ChildNodes)
                                        {
                                            //If the child node is <displaycolumn>
                                            if (objGroupChildNode.Name.ToLower() == "displaycolumn")
                                            {
                                                //' Parse all child nodes of <displaycolumn>
                                                foreach (XmlNode objDisplayColumnChildNode in objGroupChildNode.ChildNodes)
                                                {
                                                    //' If the child node is <control>
                                                    if (objDisplayColumnChildNode.Name.ToLower() == "control")
                                                    {
                                                        if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != null)
                                                        {
                                                            if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "id")
                                                            {
                                                                //'-- Check for hidden fields
                                                                if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "hidden")
                                                                {
                                                                    if (((XmlElement)objDisplayColumnChildNode).GetAttribute("type") != "javascript")
                                                                    {
                                                                        WriteFieldOptionNew((XmlElement)objDisplayColumnChildNode, objForm, p_objXMLDocument);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }//end if (objGroupChildNode.Name.ToLower()=="displaycolumn")
                                            else if (objGroupChildNode.Name.ToLower() == "message")
                                            {
                                                WriteFieldOptionNew((XmlElement)objGroupChildNode, objForm, p_objXMLDocument);
                                            }
                                        }
                                    }
                                }
                            }
                        }//end if (objElem.GetAttribute("type")!="hidden")
                    }//end if (objElem.NodeType==1)
                }//end for
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//CommandButtons");
                objNodeList = objXMLlstFields.SelectNodes("form/button");
                foreach (XmlElement objElem in objNodeList)
                {
                    WriteFieldOptionNew(objElem, objForm, p_objXMLDocument);
                }

                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ButtonList");
                objNodeList = objXMLForm.SelectNodes("form/button|form/buttonscript");
                foreach (XmlElement objElem in objNodeList)
                {
                    WriteFieldOptionNew(objElem, objForm, p_objXMLDocument);
                }
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//CommandButtons");
                objNodeList = objXMLlstFields.SelectNodes("form/buttonscript");
                foreach (XmlElement objElem in objNodeList)
                {
                    WriteFieldOptionNew(objElem, objForm, p_objXMLDocument);
                }

                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ToolBarButtons");
                objBaseNodeList = objXMLlstFields.SelectNodes("form/toolbar/button");
                foreach (XmlElement objElem in objBaseNodeList)
                {
                    WriteFieldOptionNew(objElem, objForm, p_objXMLDocument);
                }

                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ToolBarList");
                objNodeList = objXMLForm.SelectNodes("form/toolbar/button");
                foreach (XmlElement objElem in objNodeList)
                {
                    foreach (XmlElement objBaseElem in objBaseNodeList)
                    {
                        if (objElem.Attributes["type"].Value == objBaseElem.Attributes["type"].Value)
                        {
                            WriteFieldOptionNew(objElem, objForm, p_objXMLDocument);
                            break;
                        }
                    }

                }
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//OnlyTopDownLayout");
                objForm.SetAttribute("value", "1");
                objForm.InnerText = "true";
                if (ShowButtonsOnTop(objXMLForm))
                {
                    objForm.InnerText = "1";
                }
                else
                {
                    objForm.InnerText = "0";
                }
                if (bAdt)
                {
                    objForm = (XmlElement)objXMLForm.GetElementsByTagName("form").Item(0);
                    if (objForm.GetAttribute("goto") != null)
                    {
                        sChecked = objForm.GetAttribute("goto");
                    }
                    objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//AdmTracGoButton");
                    objForm.SetAttribute("value", "1");
                    objForm.SetAttribute("visible", "1");
                    objForm.InnerText = sChecked;
                }

                objForm = (XmlElement)objXMLForm.GetElementsByTagName("form").Item(0);
                if (objForm.GetAttribute("readonly") != null)
                    sChecked = objForm.GetAttribute("readonly");
                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//ReadOnlyForm");
                objForm.SetAttribute("value", "1");
                objForm.InnerText = sChecked;
                sChecked = "false";
                objForm = (XmlElement)objXMLForm.GetElementsByTagName("form").Item(0);
                if (objForm.GetAttribute("LayOut") != null)
                    sChecked = objForm.GetAttribute("LayOut");

                objForm = (XmlElement)p_objXMLDocument.SelectSingleNode("//IsTopDownLayout");
                objForm.SetAttribute("value", "1");
                objForm.InnerText = sChecked;
                return p_objXMLDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PVFormEdit.Get.Error", m_iClientId), p_objEx); //mbahl3 JIRA [RMACLOUD-123]
            }
            finally
            {
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }
                objSelectElement = null;
                sbSql = null;
                objNode = null;
                objNodeList = null;
                objBaseNodeList = null;
                objXMLlstFields = null;
                objXMLForm = null;
                objForm = null;
                objXMLSysRequired = null;
                objXMLSysRequiredHidden = null;
                objLOBSettings = null;
            }
        }
        //Deb ML Changes

		/// Name		: Save
		/// Author		: Anurag Agarwal
		/// Date Created: 05/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the Form View info to the XML file.
		/// </summary>
		/// <returns>XmlDocument containing data to be saved</returns>
		public bool Save(XmlDocument p_objXMLDocument)
		{
			StringBuilder sbSql=null;
			DbReader objReader=null;
			DbConnection objDbConnection = null;
			XmlElement objXMLControlEle = null;
			XmlElement objXMLFormEle = null;
			XmlElement objXMLParentEle = null;
			XmlDocument objXMLForm = null;
			XmlElement objXMLEle = null;
			XmlDocument objXMLlstFields = null;
			XmlElement objSelectElement = null;
			DbCommand objCmd=null;
			DbParameter objParam=null;
            DbParameter objParamAspx = null;
			string sSelectedFields = "";
			string sSelectedFieldCaptions = "";
			string sSelectedFieldHelp = "";
            var sSelectedFieldReadOnly = string.Empty;
			string sSelectedButtons = "";
			string sSelectedButtonCaption = "";
			string sSelectedToolBar = "";
			string sSelectedToolBarCaption = "";
			string sViewId = "";
			string [] arrFields;
			string [] arrCaptions;
			string [] arrHelps;
            string[] arrReadOnly=null;
			string sExt = "";
			string sViewXML = "";
			ADMForms objADMForms = null;
			int iNewNode = 0;
			string sFormName = "";
			string sReset = "";
			string sXml = "";
            StringBuilder sViewAspx = new StringBuilder("");
			string sGroupName="";
			string sGroupTitle="";
			int iOddTabIndex = 1;
			int iEvenTabIndex = 2;
			int iIncrement = 10;
			bool bAdminTracking = false;
            //Added by Shivendu for MITS 8617
            bool buttonScript = false;
            //Added by Shivendu for MITS 8617
            bool bReadOnly = false;
            //nadim MITS 12782
            bool isPrevbuttonScript = false;
            //Added by Abhishek for top-down layout
            bool isTopDownLayout = false;

            bool bIgnoreScripting = false;

            // Naresh Check if the Scripting needs to be ignored
            XmlElement objIgnoreScripting = null;

            // Naresh Do not Convert the Xml to Aspx if the function is called from the PVUpgrade Utility
            XmlElement objIgnoreASPXConversion = null;

            //rsolanki2: extensibility updates 
            string sExtendedFormsList = string.Empty;

            // Naresh Reset the TabIndex for Powerviews if it is more than the limit of Smallint
            int iTabIndex = 0;
            string sFormTitle = string.Empty;
            XmlElement objXMLTitle = null;
            string sKeyName = string.Empty;
			try
			{
                //Added by Abhishek for top-down layout
                if (p_objXMLDocument.SelectSingleNode("//IsTopDownLayout").InnerText.ToLower() == "true" || p_objXMLDocument.SelectSingleNode("//IsTopDownLayout").InnerText == "1")
                {
                    isTopDownLayout = true;
                }

                objIgnoreScripting = (XmlElement)p_objXMLDocument.SelectSingleNode("//form/group/IgnoreScripting");
                objIgnoreASPXConversion = (XmlElement)p_objXMLDocument.SelectSingleNode("//form/group/IgnoreASPXGeneration");

				objSelectElement=(XmlElement)p_objXMLDocument.SelectSingleNode("//Reset");
				if (objSelectElement!=null)
				{
					sReset=objSelectElement.InnerText;
				}
				sbSql=new StringBuilder();
				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//FormName"); 
				
				sFormName=objSelectElement.InnerText;
				sExt=".xml";

				sFormName=sFormName.Substring(0,sFormName.Length-4).Replace("%","|");

				objSelectElement.InnerText=sFormName;

               

				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//RowId"); 
				sViewId=objSelectElement.InnerText;
                
                //rsolanki2: extensibility updates - start 
                //reading the custom view_name into a string
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, 
                    string.Concat("SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 and DATA_SOURCE_ID='"
                        ,s_DataSourceId 
                        , "'"));
                
                sbSql.Length=0;
                while (objReader.Read())
                {
                    sbSql.Append(Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME")));
                    sbSql.Append("|");
                }
                sExtendedFormsList = "|" + sbSql.ToString();
                objReader.Dispose();
                sbSql.Length = 0;
                //rsolanki2:  extensibility updates  - end


                sbSql.Append("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND LOWER(FORM_NAME)='" + (sFormName + sExt).ToLower() + "' AND DATA_SOURCE_ID='" + s_DataSourceId + "'");
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
				if(objReader.Read())
				{
					sXml=Conversion.ConvertObjToStr(objReader.GetValue("VIEW_XML"));
				}
				objReader.Dispose(); 

				objXMLForm = new XmlDocument();
				if (sXml.Trim()==""  || (sReset=="1"))
				{
					if (sFormName.StartsWith("admintracking"))
					{
						objXMLForm = GetDefaultAdmView(sFormName);  
					}
					else
					{
						//objXMLForm.Load(sAppPath);
						//UTILITY.AddSuppDefinition(m_sUserId,m_sPassword,m_sConnString,m_sDSNName,objXMLForm);
					}
				}
				else
				{
					objXMLForm.LoadXml(sXml);
				}

				objXMLlstFields=new XmlDocument();
				if(sFormName.StartsWith("admintracking"))
				{ 
					objXMLlstFields = GetDefaultAdmView(sFormName);
					bAdminTracking = true;
				}
				else
				{
					//TODO-BSB Add Extended Screen Support
                    //int ViewId = UTILITY.IsExtendedScreen(sFormName) ? Constants.Views.EXTENDED_SCREEN_VIEWID : Constants.Views.BASE_VIEWID;


                    //rsolanki2: extensibility updates
                    //extended page list is already added into sExtendedFormsList 
                    int ViewId = sExtendedFormsList.ToLower().Contains(string.Concat("|", sFormName.ToLower(), ".xml|"))
                                    ? Constants.Views.EXTENDED_SCREEN_VIEWID
                                    : Constants.Views.BASE_VIEWID;
                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString
                        , (String.Format("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='{1}' AND DATA_SOURCE_ID='{2}'"
                            , ViewId
                            , (sFormName + sExt).ToLower()
                            , s_DataSourceId)));

					if(objReader.Read())
					{
						sXml = Conversion.ConvertObjToStr(objReader["VIEW_XML"]);
						objXMLlstFields.LoadXml(sXml); 
					}

                    if (objIgnoreScripting != null && objIgnoreScripting.InnerText.ToLower() == "true")
                        bIgnoreScripting = true;

                    // npadhy Checked in the Utility as well
                    // MAC : AddSuppDefinitionIgnoreScripts is not defined.
                    if (!bIgnoreScripting)
                        UTILITY.AddSuppDefinition(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objXMLlstFields,m_iClientId);//mbahl3 JIRA [RMACLOUD-123]
                    else
                        UTILITY.AddSuppDefinitionIgnoreScripts(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objXMLlstFields, m_iClientId);

                    //Raman: 08/13/2009: If top down layout is selected then we can make all control classes to be completerow wherever it is half
                    //MITS 17476 and 17479
                    if (isTopDownLayout)
                    {
                        string sXML = objXMLlstFields.OuterXml;
                        sXml = sXML.Replace("class=\"half\"", "class=\"completerow\"");
                        objXMLlstFields.LoadXml(sXml);
                    }
				}

				if(sReset != "1")
				{
					//Getting the selected fields
					sSelectedFields = ((XmlElement)p_objXMLDocument.SelectSingleNode("//Fields")).InnerText;
					sSelectedFieldCaptions = ((XmlElement)p_objXMLDocument.SelectSingleNode("//Captions")).InnerText;
					sSelectedFieldHelp = ((XmlElement)p_objXMLDocument.SelectSingleNode("//Helps")).InnerText;
                    if (((XmlElement)p_objXMLDocument.SelectSingleNode("//ReadOnly"))!=null) //asharma326 JIRA 7508
                    {
                        sSelectedFieldReadOnly = ((XmlElement)p_objXMLDocument.SelectSingleNode("//ReadOnly")).InnerText;
                    }

                    RemoveAllControls(objXMLForm, objXMLlstFields);

                    //rkulavil : RMA-5233 start                       
                    objXMLTitle = objXMLForm.SelectSingleNode("/form") as XmlElement;
                    sKeyName = objXMLTitle.GetAttribute("name");
                    if (strDictResourceValues != null)
                    {
                        strDictResourceValues.TryGetValue(sKeyName, out sFormTitle);
                    }
                    else 
                    {
                        sFormTitle = Convert.ToString(DbFactory.ExecuteScalar(m_netViewFormsConnString, string.Format(@"SELECT DISTINCT LR.RESOURCE_VALUE FROM LOCAL_RESOURCE LR
                                    INNER JOIN PAGE_INFO PI ON PI.PAGE_ID = LR.PAGE_ID 
                                    INNER JOIN NET_VIEWS NV ON NV.LANGUAGE_CODE=LR.LANGUAGE_ID
                                    WHERE PI.PAGE_NAME = '{0}' AND LR.RESOURCE_KEY='{1}' ", sKeyName + ".aspx", sKeyName)));
                    }
                    if (sFormTitle != string.Empty)
                    {
                        if (objXMLTitle != null)
                        {
                            objXMLTitle.SetAttribute("title", sFormTitle);
                        }
                    }
                    //rkulavil : RMA-5233 end 
                    objXMLFormEle = (XmlElement)objXMLForm.GetElementsByTagName("form").Item(0);
                    if (p_objXMLDocument.SelectSingleNode("//ReadOnlyForm").InnerText == "1" || p_objXMLDocument.SelectSingleNode("//ReadOnlyForm").InnerText.ToLower() == "true")
                    {
                        objXMLFormEle.SetAttribute("readonly", "1");
                        bReadOnly = true;
                    }
                    else
                    {
                        objXMLFormEle.SetAttribute("readonly", "0");
                        bReadOnly = false;
                    }

                    if (p_objXMLDocument.SelectSingleNode("//OnlyTopDownLayout").InnerText == "1" || p_objXMLDocument.SelectSingleNode("//OnlyTopDownLayout").InnerText.ToLower() == "true")
						objXMLFormEle.SetAttribute("topbuttons","1");
					else
						objXMLFormEle.SetAttribute("topbuttons","0");

                    //Added by Abhishek for top-downlayout
                    if (p_objXMLDocument.SelectSingleNode("//IsTopDownLayout").InnerText.ToLower() == "true" || p_objXMLDocument.SelectSingleNode("//IsTopDownLayout").InnerText == "1")
                        objXMLFormEle.SetAttribute("LayOut", "1");
                    else
                        objXMLFormEle.SetAttribute("LayOut", "0");

                    //apeykov Added for JIRA RMA-7548/MITS 37713 Start
                    if (!String.IsNullOrEmpty(sViewId))
                        objXMLFormEle.SetAttribute("viewid", sViewId);
                    //apeykov Added for JIRA RMA-7548/MITS 37713 End

                    if(p_objXMLDocument.SelectSingleNode("//AdmTracGoButton")!=null)
                    if (p_objXMLDocument.SelectSingleNode("//AdmTracGoButton").InnerText == "1" || p_objXMLDocument.SelectSingleNode("//AdmTracGoButton").InnerText.ToLower() == "true")
						objXMLFormEle.SetAttribute("goto","1");
					else
						objXMLFormEle.SetAttribute("goto","0");

                    // akaushik5 Added for MITS 30290 Starts
                    if (p_objXMLDocument.SelectSingleNode("//FormRequiredTabs") != null && objXMLFormEle.SelectSingleNode("//internal[@name='txtRequiredTabs']") != null)
                    {
                        (objXMLFormEle.SelectSingleNode("//internal[@name='txtRequiredTabs']") as XmlElement).SetAttribute("value", p_objXMLDocument.SelectSingleNode("//FormRequiredTabs").InnerText);
                    }
                    // akaushik5 Added for MITS 30290 Ends
				
					arrFields = sSelectedFields.Split('|');  
					arrCaptions = sSelectedFieldCaptions.Split('|');
					arrHelps = sSelectedFieldHelp.Split('|');
                    if (sSelectedFieldReadOnly != null)
                        arrReadOnly = sSelectedFieldReadOnly.Split('|');
					objXMLParentEle = objXMLFormEle;

                    //Raman : 08/20/2009
                    //When supplemental grid control is on left in tab view and another control is on right then we should not touch the 
                    //tabindex of the right control as it will be displayed on the left anyhow
                    bool bIsSuppGridOnLeft = false;
                    int iGridTabIndex = 0;
                    
                    if(arrFields.Length > 0)
					{
						for(int i = 0; i < arrFields.Length; i++)
						{
							if(arrCaptions[i].ToString().Length > 4 && arrCaptions[i].ToString().Substring(0,5).Equals("[*** "))
							{
								sGroupName= arrFields[i].ToString();
								sGroupTitle= arrCaptions[i].ToString();
								objXMLEle = objXMLForm.CreateElement("group");
								objXMLEle.SetAttribute("name",sGroupName); 
								objXMLEle.SetAttribute("title",ModifyGroupTitle(sGroupTitle));
								if(objXMLForm.SelectNodes("//group").Count==0)
								{
									objXMLEle.SetAttribute("selected","1");
									if( bAdminTracking )
										objXMLFormEle.AppendChild(objXMLEle);
									else
										objXMLFormEle.InsertAfter(objXMLEle, objXMLForm.SelectSingleNode("//toolbar"));
								}
								else
									objXMLFormEle.InsertAfter(objXMLEle, objXMLForm.SelectNodes("//group")[objXMLForm.SelectNodes("//group").Count -1] );
								objXMLParentEle = objXMLEle;
								iNewNode = 1;
							}
							else if(arrCaptions[i].ToString().Length > 4 && arrCaptions[i].ToString().Substring(0,5).Equals("[MS* "))
							{
                                //Neha Goel MITS#36955 - RMA -4692  Message control needs to be assigned on right column also. Currently it can be placed only in right column
                                //**********Neha goel commented below part for MITS#36955  - RMA -4692 *****************
                                ////Message control will be in its own displaycolumn, and it's parent should be group. MITS 9294                          
                                //    while (objXMLParentEle.Name != "group")
                                //    {
                                //        objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode;
                                //    }
                                //    objXMLEle = objXMLForm.CreateElement("displaycolumn");
                                //    objXMLParentEle.AppendChild(objXMLEle);
                                //    objXMLParentEle = objXMLEle;
                                //    objXMLEle = objXMLForm.CreateElement("control");
                                //    objXMLEle.SetAttribute("name", "message" + i.ToString());
                                //    objXMLEle.SetAttribute("type", "message");
                                //    objXMLEle.SetAttribute("title", arrCaptions[i].ToString().Substring(5, arrCaptions[i].ToString().Length - 10));
                                //    objXMLParentEle.AppendChild(objXMLEle);
                                //    objXMLParentEle = objXMLEle;
                                //    iNewNode = 1;     
                                //**********Neha goel commented end MITS#36955 - RMA -4692 *****************
                                //**********Neha goel added below part for MITS#36955 - RMA -4692 *****************
                                //Neha Goel MITS#36955 - RMA -4692   Message control needs to be added in right column of the screen also. Currently it can be placed only in left column of the screen.
                                if ((iNewNode % 2) != 0)
                                {
                                    if (objXMLParentEle.Name.Equals("control", StringComparison.OrdinalIgnoreCase) && objXMLParentEle.ParentNode.ParentNode!=null)
                                        objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode.ParentNode;
                                    if (iNewNode != 1 && objXMLParentEle.ParentNode!=null)
                                        objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode;
                                    if (objXMLParentEle.Name.Equals("group",StringComparison.OrdinalIgnoreCase))//dont create displaycolumn if no group. this is for admin tracking
                                    {
                                        objXMLEle = objXMLForm.CreateElement("displaycolumn");
                                        objXMLParentEle.AppendChild(objXMLEle);
                                        objXMLParentEle = objXMLEle;
                                    }
                                }

                                objXMLEle = objXMLForm.CreateElement("control");
                                objXMLEle.SetAttribute("name", "message" + i.ToString());
                                objXMLEle.SetAttribute("type", "message");
                                objXMLEle.SetAttribute("title", arrCaptions[i].ToString().Substring(5, arrCaptions[i].ToString().Length - 10));
                                objXMLParentEle.AppendChild(objXMLEle);
                                iNewNode++;
                                //Neha Goel MITS#36955 - RMA -4692 : code end
							}
                            //Handling Line Breaks Asharma326 JIRA 6411 Starts
                            else if (arrCaptions[i].ToString().Length > 4 && arrCaptions[i].ToString().Substring(0, 5).Equals("[LB* "))
                            {
                                //dead try catch for bypassing error bcz there are scenarios when "|" is come as blank in arrFields 
                                try
                                {
                                    //iNewNode = 1;
                                    //Message control will be in its own displaycolumn, and it's parent should be group. MITS 9294
                                    if ((iNewNode % 2) != 0)
                                    {
                                        if (objXMLParentEle.Name == "control")
                                            objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode.ParentNode;
                                        if (iNewNode != 1)
                                            objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode;
                                        if (objXMLParentEle.Name == "group")//dont create displaycolumn if no group. this is for admin tracking
                                        {
                                            objXMLEle = objXMLForm.CreateElement("displaycolumn");
                                            objXMLParentEle.AppendChild(objXMLEle);
                                            objXMLParentEle = objXMLEle;
                                        }
                                    }

                                    //objXMLEle = CopyXMLElement((XmlElement)objXMLlstFields.SelectSingleNode("//control[@name='" + arrFields[i].ToString() + "']"), objXMLForm);
                                    objXMLEle = objXMLForm.CreateElement("control");
                                    objXMLEle.SetAttribute("name", "linebreak" + i.ToString());
                                    objXMLEle.SetAttribute("type", "linebreak");
                                    objXMLEle.SetAttribute("title", "");
                                    objXMLParentEle.AppendChild(objXMLEle);
                                }
                                catch (Exception)
                                {
                                    //throw;
                                }
                                iNewNode++;
                            }
                            //Handling Line Breaks Asharma326 JIRA 6411 Ends
                            //Code updated by pawan to byppass spaces implementation for 11210 on tabviews 
                            else if (arrCaptions[i].ToString().Length > 4 && arrCaptions[i].ToString().Substring(0, 5).Equals("[HS* "))
                            {
                                //Message control will be in its own displaycolumn, and it's parent should be group. MITS 9294
                                if ((iNewNode % 2) != 0)
                                {
                                    if (objXMLParentEle.Name == "control")
                                        objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode.ParentNode;
                                    if (iNewNode != 1)
                                        objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode;
                                    if (objXMLParentEle.Name == "group")//dont create displaycolumn if no group. this is for admin tracking
                                    {
                                        objXMLEle = objXMLForm.CreateElement("displaycolumn");
                                        objXMLParentEle.AppendChild(objXMLEle);
                                        objXMLParentEle = objXMLEle;
                                    }
                                }                               
                               
                                objXMLEle = objXMLForm.CreateElement("control");
                                objXMLEle.SetAttribute("name", "pvspace" + i.ToString());
                                objXMLEle.SetAttribute("type", "space");
                                objXMLEle.SetAttribute("title", "");
                                objXMLParentEle.AppendChild(objXMLEle);
                                iNewNode++;
                            }
                            //Code change ends implementation for 11210 on tabviews by pawan 
							else if(arrCaptions[i].ToString()=="[Hidden Control Group]")//handle controlgroup main
							{
								iNewNode=1;
								//MITS 9294
								while(objXMLParentEle.Name != "group")
								{
									objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode;
								}
								objXMLEle = objXMLForm.CreateElement("displaycolumn");
								objXMLParentEle.AppendChild(objXMLEle);
								objXMLParentEle = objXMLEle;
								objXMLEle = CopyXMLElement((XmlElement)objXMLlstFields.SelectSingleNode("//control[@name='" + arrFields[i].ToString() + "']"),objXMLForm);
								objXMLParentEle.AppendChild(objXMLEle);
								objXMLParentEle = objXMLEle;
							}
							else if(arrCaptions[i].ToString().IndexOf("+")==0)//handle control group individual elements
							{
								objXMLControlEle = (XmlElement)objXMLlstFields.SelectSingleNode("//control[@name='" + objXMLParentEle.GetAttribute("name") +"']//control[@name='"+  arrFields[i].ToString() + "']"); 

								if(objXMLControlEle != null)
								{
									objXMLEle = CopyXMLElement(objXMLControlEle,objXMLForm);
									objXMLParentEle.AppendChild(objXMLEle);
									if(arrCaptions[i].ToString().Length > 2 && arrCaptions[i].ToString().Substring(0,2).Equals("- "))
									{
										arrCaptions[i] = arrCaptions[i].ToString().Substring(2);
										if(objXMLEle.GetAttribute("required") != "yes")
											objXMLEle.SetAttribute("required","yes");
									}

									if (objXMLEle.GetAttributeNode("pvtitle") != null)
									{}//	objXMLEle.SetAttribute("title","");
									else
										objXMLEle.SetAttribute("title",arrCaptions[i].ToString().Replace("+",""));
									
									if(arrHelps[i].ToString()!="")
										objXMLEle.SetAttribute("helpmsg",arrHelps[i].ToString());

                                    if (arrReadOnly != null && arrReadOnly.Length == arrCaptions.Length && arrReadOnly[i].ToString().ToLower() == "true")//asharma326 JIRA 6411
                                        objXMLEle.SetAttribute("PowerViewReadOnly", "true");  
									
									if(objXMLControlEle.GetAttribute("type")=="textlabel")//Text label has the title in the inner text. special case
										objXMLEle.InnerText= arrCaptions[i].ToString();
								}
							}
							else
							{
                                if ((iNewNode % 2) != 0) 
								{
									if(objXMLParentEle.Name=="control")
										objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode.ParentNode;
									if(iNewNode != 1)
										objXMLParentEle = (XmlElement)objXMLParentEle.ParentNode;
									if(objXMLParentEle.Name=="group")//dont create displaycolumn if no group. this is for admin tracking
									{
										objXMLEle = objXMLForm.CreateElement("displaycolumn");
										objXMLParentEle.AppendChild(objXMLEle);
										objXMLParentEle = objXMLEle;	
									}
								}
								iNewNode++;
								objXMLControlEle = (XmlElement)objXMLlstFields.SelectSingleNode("//control[@name='" + arrFields[i].ToString() + "']"); 

								if(objXMLControlEle != null)
								{
                                    //MITS 13045: Amit Bansal :  Tab index incorrect in client powerview in Top-Down layout on 07/08/2009 Started
                                    if (isTopDownLayout)
                                    {
                                        while (iTabIndex > 32767)
                                        {
                                            iTabIndex = iTabIndex - 32767;
                                        }
                                        iTabIndex = iTabIndex + 10;
                                        objXMLControlEle.SetAttribute("tabindex", iTabIndex.ToString());
                                    }
                                    else
                                    {
                                        //MITS 7861 : Raman Bhatia : If a field is removed or added in power view then whole tabbling order was getting disturbed
                                        //Fixing tab-index while adding controls to xml to fix the issue

                                        //Ashish Ahuja : Mits 30264 -- To maintain the tabbing if more then one gridview added in same tab
                                        //if ((iNewNode % 2) != 0)
                                        if (((iNewNode % 2) != 0) && (string.Compare(objXMLControlEle.GetAttribute("type"), "gridandbuttons", true) != 0 ))
                                        {
                                            //mdhamija : MITS 20079:the code prevents the multiplication of the tabindex by 100 in case of Type field for OtherPerson page
                                            int itabindex = Conversion.ConvertStrToInteger(objXMLControlEle.GetAttribute("tabindex"));
                                            if ((!(arrFields[i].ToString().Equals("entitytableidtextname"))) && itabindex != 1)
                                            {
                                            //Not a good piece of code but basically we are trying to shift tab-indexes of controls on the right by a big amount so that
                                            //they remain greater than tabindexes of left controls even after they are incremented to retain taborder..

                                            //Ashish Ahuja : Mits 30264 -- Increased the amount of tab for more controls in same tab
                                            //iTabIndex = (Conversion.ConvertStrToInteger(objXMLControlEle.GetAttribute("tabindex")) * 100);
                                            iTabIndex = (Conversion.ConvertStrToInteger(objXMLControlEle.GetAttribute("tabindex")) * 1000);


                                            while (iTabIndex > 32767)
                                            {
                                                iTabIndex = iTabIndex - 32767;
                                            }

                                            if (bIsSuppGridOnLeft)
                                            {
                                                iTabIndex = iGridTabIndex;
                                                bIsSuppGridOnLeft = false;
                                                objXMLControlEle.SetAttribute("tabindex", iTabIndex.ToString());
                                            }
                                            else
                                            {
                                                objXMLControlEle.SetAttribute("tabindex", iTabIndex.ToString());

                                                if (((Conversion.ConvertStrToInteger(objXMLControlEle.GetAttribute("tabindex")) - iEvenTabIndex) != 1) && (iEvenTabIndex != 2))
                                                {

                                                    iTabIndex = iEvenTabIndex + iIncrement;

                                                    while (iTabIndex > 32767)
                                                    {
                                                        iTabIndex = iTabIndex - 32767;
                                                    }
                                                    objXMLControlEle.SetAttribute("tabindex", iTabIndex.ToString());

                                                } // if 
                                                iEvenTabIndex = Conversion.ConvertStrToInteger(objXMLControlEle.GetAttribute("tabindex"));
                                            }

                                        }
                                        } // if
                                        else
                                        {
                                            if (((Conversion.ConvertStrToInteger(objXMLControlEle.GetAttribute("tabindex")) - iOddTabIndex) != 1) && (iOddTabIndex != 1))
                                            {
                                                iTabIndex = iOddTabIndex + iIncrement;


                                                while (iTabIndex > 32767)
                                                {
                                                    iTabIndex = iTabIndex - 32767;
                                                }
                                                if (objXMLControlEle.GetAttribute("type") == "ZapatecGrid" || objXMLControlEle.GetAttribute("type") == "linebreak")
                                                {
                                                    bIsSuppGridOnLeft = true;
                                                    iGridTabIndex = iTabIndex;
                                                }
                                                objXMLControlEle.SetAttribute("tabindex", iTabIndex.ToString());

                                            } // if
                                            iOddTabIndex = Conversion.ConvertStrToInteger(objXMLControlEle.GetAttribute("tabindex"));
                                        } // else
                                    }
                                    //MITS 13045: Amit Bansal :  Tab index incorrect in client powerview in Top-Down layout on 07/08/2009 End
									objXMLEle = CopyXMLElement(objXMLControlEle,objXMLForm);
									
									//Admin tracking control could child of form element
									if( objXMLParentEle.Name == "form" )
										objXMLParentEle.AppendChild(objXMLEle);
									else
										objXMLParentEle.AppendChild(objXMLEle);
									if(arrCaptions[i].ToString().Length > 2 && arrCaptions[i].ToString().Substring(0,2).Equals("- "))
									{
										arrCaptions[i] = arrCaptions[i].ToString().Substring(2);
										if(objXMLEle.GetAttribute("required") != "yes")
											objXMLEle.SetAttribute("required","yes");
									}
                                    //pmittal5 Mits 22100 - Title should not be added in case of "Label Only" fields present in Non-Occ Claims screen
                                    if (objXMLControlEle.GetAttribute("type") == "labelonly" && (objXMLControlEle.GetAttribute("name") == "labelonly1" || objXMLControlEle.GetAttribute("name") == "labelonly2" || objXMLControlEle.GetAttribute("name") == "labelonly3"))
									    objXMLEle.SetAttribute("title","");
                                    else if (string.Compare(objXMLControlEle.GetAttribute("type"), "gridandbuttons", true) == 0 || string.Compare(objXMLControlEle.GetAttribute("type"), "Grid", true) == 0) //Added else if clause for Mits 22004 and MITS 22295
                                    {
                                        string sCaption = arrCaptions[i].ToString().Replace("-", "");
                                        if (sCaption != string.Empty)
                                        {
                                            objXMLEle.SetAttribute("gridtitle", sCaption);
                                            objXMLEle.SetAttribute("pvtitle", sCaption);
                                        }
                                        else
                                        {
                                            objXMLEle.SetAttribute("pvtitle", objXMLEle.GetAttribute("gridtitle"));
                                        }
                                    }
                                    else
                                    {
                                        objXMLEle.SetAttribute("title", arrCaptions[i].ToString().Replace("-", ""));
                                        //rsharma220 MITS 31131 Start
                                        if (objXMLEle.GetAttribute("type") == "radio")
                                        {
                                          objXMLEle.SetAttribute("label", arrCaptions[i].ToString().Replace("-", ""));
                                        }
                                        //rsharma220 MITS 31131 End
                                    }
                                    //End - pmittal5
									if(arrHelps[i].ToString()!="")
										objXMLEle.SetAttribute("helpmsg",arrHelps[i].ToString());

                                    if (arrReadOnly != null && arrReadOnly.Length == arrCaptions.Length && arrReadOnly[i].ToString().ToLower() == "true")//asharma326 JIRA 6411
                                        objXMLEle.SetAttribute("PowerViewReadOnly", "true");  

									if(objXMLControlEle.GetAttribute("type")=="textlabel")//Text label has the title in the inner text. special case
										objXMLEle.InnerText= arrCaptions[i].ToString();

                                   if (string.Compare(objXMLControlEle.GetAttribute("type"), "phonetype", true) == 0) //Added if clause for Mits 22221
                                    {
                                        objXMLEle.SetAttribute("ignorelabel", "true");
                                    }
								}
							}
						}
					}

                    // Copy the Controls of Type Id from base xml to the view xml
                    XmlElement objElemForm = (XmlElement)objXMLForm.GetElementsByTagName("form").Item(0);
                    XmlNodeList objXmlNodeList = objXMLlstFields.SelectNodes("//control[@type='id']");

                    if (objXmlNodeList != null)
                    {
                        foreach (XmlNode objXMLNode in objXmlNodeList)
                        {
                            objElemForm.AppendChild(objXMLForm.ImportNode(objXMLNode, true));
                        }
                    }

                    objXmlNodeList = objXMLlstFields.SelectNodes("//control[@type='hidden']");

                    if (objXmlNodeList != null)
                    {
                        foreach (XmlNode objXMLNode in objXmlNodeList)
                        {
                            objElemForm.AppendChild(objXMLForm.ImportNode(objXMLNode, true));
                        }
                    }

                    objXmlNodeList = objXMLlstFields.SelectNodes("//control[@type='javascript']");

                    if (objXmlNodeList != null)
                    {
                        foreach (XmlNode objXMLNode in objXmlNodeList)
                        {
                            objElemForm.AppendChild(objXMLForm.ImportNode(objXMLNode, true));
                        }
                    }

                    //Raman: 06/24/2009 : Hack to solve duplicate controls in admin tracking
                    if (sFormName.StartsWith("admintracking"))
                    {
                        if(objXMLForm.SelectNodes("//control[@name='btnGoTo']").Count == 2)
                        {
                            XmlNode objCtrl = objXMLForm.SelectSingleNode("/form/control[@name='btnGoTo']");
                            if (objCtrl != null)
                            {
                                objCtrl.ParentNode.RemoveChild(objCtrl);
                            }
                            objCtrl = objXMLForm.SelectSingleNode("/form/control[@name='recordid']");
                            if (objCtrl != null)
                            {
                                objCtrl.ParentNode.RemoveChild(objCtrl);
                            }
                        }
                    }

					//Getting the selected buttons
					sSelectedButtons = ((XmlElement)p_objXMLDocument.SelectSingleNode("//Buttons")).InnerText;
					sSelectedButtonCaption = ((XmlElement)p_objXMLDocument.SelectSingleNode("//ButtonCaptions")).InnerText;

					arrFields = sSelectedButtons.Split('|');  
					arrCaptions = sSelectedButtonCaption.Split('|');
					if(arrFields.Length > 0 && arrCaptions.Length > 0)
					{
						objXMLParentEle = (XmlElement)objXMLForm.GetElementsByTagName("form").Item(0);
						for(int i = 0; i < arrFields.Length; i++)
						{
							objXMLControlEle = (XmlElement)objXMLlstFields.SelectSingleNode("//button[@name='" + arrFields[i] + "']"); 
							//Added by Shivendu for MITS 8617
                            if (objXMLControlEle == null)
                            {
                                objXMLControlEle = (XmlElement)objXMLlstFields.SelectSingleNode("//buttonscript[@name='" + arrFields[i] + "']");
                                buttonScript = true;
                            }
                            //Added by Shivendu for MITS 8617
                            //Code Modified by nadim for MITS 12782,to save the command buttons in the order  required
                            if (objXMLControlEle != null)
                            {
                                objXMLEle = CopyXMLElement(objXMLControlEle, objXMLForm);

                                if (!buttonScript) //If statement is added by Shivendu for MITS 8617
                                {

                                    if (objXMLForm.SelectNodes("//button").Count == 0 && objXMLForm.SelectNodes("//buttonscript").Count == 0)
                                    {
                                        objXMLParentEle.InsertAfter(objXMLEle, objXMLForm.SelectSingleNode("//section"));
                                    }
                                    else
                                    {
                                        if (isPrevbuttonScript)
                                        {
                                            objXMLParentEle.InsertAfter(objXMLEle, objXMLForm.SelectNodes("//buttonscript")[objXMLForm.SelectNodes("//buttonscript").Count - 1]);
                                        }
                                        else
                                        {
                                            objXMLParentEle.InsertAfter(objXMLEle, objXMLForm.SelectNodes("//button")[objXMLForm.SelectNodes("//button").Count - 1]);

                                        }
                                    }
                                    isPrevbuttonScript = false;
                                }
                                else //Entire else block is added by Shivendu for MITS 8617
                                {

                                    if (objXMLForm.SelectNodes("//buttonscript").Count == 0 && objXMLForm.SelectNodes("//button").Count == 0)
                                    {
                                        objXMLParentEle.InsertAfter(objXMLEle, objXMLForm.SelectSingleNode("//section"));
                                    }
                                    else
                                    {
                                        if (isPrevbuttonScript)
                                        {
                                            objXMLParentEle.InsertAfter(objXMLEle, objXMLForm.SelectNodes("//buttonscript")[objXMLForm.SelectNodes("//buttonscript").Count - 1]);
                                        }
                                        else
                                        {
                                            objXMLParentEle.InsertAfter(objXMLEle, objXMLForm.SelectNodes("//button")[objXMLForm.SelectNodes("//button").Count - 1]);

                                        }

                                    }
                                    isPrevbuttonScript = true;
                                }
                                //Code ends here MITS 12782
                                //objXMLParentEle.AppendChild(objXMLEle);
                                objXMLEle.SetAttribute("title", arrCaptions[i].ToString());
                            }
                            buttonScript = false;
						}
					}

					sSelectedToolBar = ((XmlElement)p_objXMLDocument.SelectSingleNode("//Toolbar")).InnerText;
					sSelectedToolBarCaption = ((XmlElement)p_objXMLDocument.SelectSingleNode("//ToolbarCaptions")).InnerText;

					arrFields = sSelectedToolBar.Split('|');  
					arrCaptions = sSelectedToolBarCaption.Split('|');
					if(arrFields.Length > 0 && arrCaptions.Length > 0)
					{
						objXMLParentEle = (XmlElement)objXMLForm.GetElementsByTagName("toolbar").Item(0);
						for(int i = 0; i < arrFields.Length; i++)
						{
							objXMLControlEle = (XmlElement)objXMLlstFields.SelectSingleNode("//toolbar/button[@type='" + arrFields[i].ToString() + "']"); 
							if(objXMLControlEle != null)
							{
								objXMLEle = CopyXMLElement(objXMLControlEle,objXMLForm);
								objXMLParentEle.AppendChild(objXMLEle);
								objXMLEle.SetAttribute("title",arrCaptions[i].ToString()); 
							}
						}
					}

                    //Move id type of control to the first display column
                    //					foreach(XmlElement  objNod in objXMLForm.SelectNodes("/form/control[@type='id']"))
                    //					{
                    //						objXMLForm.SelectSingleNode("/form/group/displaycolumn[1]").AppendChild(objNod);
                    //						objNod.ParentNode.RemoveChild(objNod); 
                    //					}

                    if (Conversion.ConvertStrToInteger(((XmlElement)objXMLForm.SelectSingleNode("//form")).GetAttribute("viewid")) == 0)  //add ttumula2 for RMA-10434 and including viewid in viewxml while creating powerview 
                        ((XmlElement)objXMLForm.SelectSingleNode("//form")).SetAttribute("viewid", sViewId.ToString());
					sViewXML = objXMLForm.OuterXml;
                    //Added by Shivendu for R5
                   PowerViewUpgrade pvUpgrade = new PowerViewUpgrade(m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
                   
                    if (objIgnoreASPXConversion == null)
                       pvUpgrade.UpgradeXmlToAspx(bReadOnly, isTopDownLayout, objXMLForm, sFormName + sExt, out sViewAspx);

                    objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
					objDbConnection.Open(); 
					sbSql.Remove(0,sbSql.Length);  
					objCmd=objDbConnection.CreateCommand();
					objParam=objCmd.CreateParameter();
                    objParamAspx = objCmd.CreateParameter();
					objParam.Direction=ParameterDirection.Input;
                    objParamAspx.Direction = ParameterDirection.Input;
					objParam.Value=sViewXML ;
                    objParamAspx.Value = sViewAspx.ToString();
					objParam.ParameterName = "XML";
                    objParamAspx.ParameterName = "ASPX";
					objParam.SourceColumn="VIEW_XML";
                    objParamAspx.SourceColumn = "PAGE_CONTENT";
					objCmd.Parameters.Add(objParam);
                    objCmd.Parameters.Add(objParamAspx);
                    sbSql.Append("UPDATE NET_VIEW_FORMS SET VIEW_XML=~XML~,PAGE_CONTENT=~ASPX~,PAGE_NAME = '" 
                        + sFormName.Replace("|", "") 
                        + ".aspx" 
                        + "',LAST_UPDATED = '" 
                        + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
                        + "' WHERE DATA_SOURCE_ID =" 
                        + s_DataSourceId 
                        + " AND VIEW_ID=" 
                        + sViewId 
                        + " AND LOWER(FORM_NAME) = '" 
                        + (sFormName + sExt).ToLower() 
                        + "'");

					objCmd.CommandText=sbSql.ToString();
					objCmd.ExecuteNonQuery();
					if(objDbConnection != null)
					{
						objDbConnection.Dispose();
					}
				}
				return true;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVFormEdit.Save.Error", m_iClientId),p_objEx); //mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{
				sbSql=null;
				objCmd=null;
                if (objReader != null)
                {
                    if (!(objReader.IsClosed))
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }
				if(objDbConnection != null)
				{
                    if (objDbConnection.State == ConnectionState.Open)
                    {
                        objDbConnection.Close();
                    }
					objDbConnection.Dispose();
				}
				objXMLControlEle = null;
				objXMLParentEle = null;
				objXMLForm = null;
				objXMLEle = null;
				objXMLlstFields = null;
				objSelectElement = null;
				if(objADMForms != null)
					objADMForms = null;
			}
		} 

		internal XmlDocument GetDefaultAdmView(string p_sAdm)
		{
            XmlNode objNod = null;
            XmlNode objGrpNod = null;
            XmlNode objNewDisp = null;
            XmlElement objNewElm = null;
			DbReader objReader=null;
			XmlDocument objGrp=new XmlDocument(); 
			XmlDocument objXMLFormDoc = new XmlDocument();
			string sAdm="";
			string sViewXML="";
            try
            {
                string[] arr = p_sAdm.Split('%');
                if (arr.Length == 1)
                    arr = p_sAdm.Split('|');

                if (arr.Length <= 1)
                    return null;
                if (arr[1].EndsWith(".xml"))
                    sAdm = arr[1].Substring(0, arr[1].Length - 4);
                else
                    sAdm = arr[1];

                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, "SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND FORM_NAME='admintracking.xml' AND DATA_SOURCE_ID='" + s_DataSourceId + "'");
                if (objReader != null)
                {
                    if (objReader.Read())
                        sViewXML = objReader["VIEW_XML"].ToString();
                    objReader.Dispose();
                }
                objXMLFormDoc.LoadXml(sViewXML);
                DataModelFactory objDmf;
                if (m_sUserId == "" && m_sPassword == "" && m_sConnString != "")
                {
                    objDmf = DataModelFactory.CreateDataModelFactory(m_sConnString,m_iClientId);
                }
                else
                {
                    objDmf = new DataModelFactory(m_sDSNName, m_sUserId, m_sPassword, m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
                }
                ADMTable objAdm = (ADMTable)objDmf.GetDataModelObject("ADMTable", false);
                objAdm.TableName = sAdm;
                sViewXML = objAdm.ViewXml.OuterXml;
                objGrp.LoadXml(sViewXML);
                objAdm.Dispose();
                objDmf.Dispose();
                XmlElement newGrp = objXMLFormDoc.CreateElement("group");
                foreach (XmlAttribute atr in ((XmlElement)objGrp.SelectSingleNode("//group")).Attributes)
                    newGrp.SetAttribute(atr.Name, atr.Value);

                objXMLFormDoc.SelectSingleNode("//form").AppendChild(newGrp);
                newGrp.InnerXml = objGrp.SelectSingleNode("//group").InnerXml;
                //put recordid and go button inside the group. MCIC Changes 01/25/08
                objGrpNod = objXMLFormDoc.SelectSingleNode("/form/group[@name='admgroup']");
                objNod = objXMLFormDoc.SelectSingleNode("//form/control[@name='recordid']");
                if (objNod != null)
                {
                    objNewElm = CopyXMLElement((XmlElement)objNod, objXMLFormDoc);
                    objNewDisp = objXMLFormDoc.CreateElement("displaycolumn");
                    objNewDisp.AppendChild(objNewElm);
                    objGrpNod.InsertBefore(objNewDisp, objGrpNod.FirstChild);
                    objNod.ParentNode.RemoveChild(objNod);
                }
                objNod = objXMLFormDoc.SelectSingleNode("//form/control[@name='btnGoTo']");
                if (objNod != null)
                {
                    objNewElm = CopyXMLElement((XmlElement)objNod, objXMLFormDoc);
                    objNewDisp = objXMLFormDoc.CreateElement("displaycolumn");
                    objNewDisp.AppendChild(objNewElm);
                    objGrpNod.InsertBefore(objNewDisp, objGrpNod.FirstChild);
                    objNod.ParentNode.RemoveChild(objNod);
                }
                return objXMLFormDoc;
            }
            finally
            {
                if (objReader != null)
                {
                    if (!(objReader.IsClosed))
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }

            }
		}

		private string ModifyGroupTitle(string p_sTitle)
		{
			return p_sTitle.Replace("[***","").Replace("***]","").Trim();  
		}

        // akaushik5 Added for MITS 30290 Starts
        /// <summary>
        /// Getrequireds the tabs.
        /// </summary>
        /// <param name="p_objXmlOut">The p_obj XML out.</param>
        public void GetrequiredTabs(ref XmlDocument p_objXmlOut)
        {
            StringBuilder sbSql = new StringBuilder();
            XmlElement objSelectElement = (XmlElement)p_objXmlOut.SelectSingleNode("//PVList/RowId");
            string sViewId = objSelectElement.InnerText;
            string sXml = string.Empty;

            objSelectElement = (XmlElement)p_objXmlOut.SelectSingleNode("//PVFormEdit/FormName");
            string sFormName = objSelectElement.InnerText.Replace("%", "|").Replace(".xml", string.Empty) + "d.xml";

            sbSql.Append("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID ="
                    + sViewId
                    + " AND LOWER(FORM_NAME)='"
                    + sFormName.ToLower()
                    + "' AND DATA_SOURCE_ID='"
                    + s_DataSourceId
                    + "'");
            using (DbReader objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString()))
            {
                if (objReader.Read())
                {
                    sXml = Conversion.ConvertObjToStr(objReader.GetValue("VIEW_XML"));
                }
            }

            if (!string.IsNullOrEmpty(sXml))
            {
                XmlDocument objXMLForm = new XmlDocument();
                objXMLForm.LoadXml(sXml);
                objSelectElement = objXMLForm.SelectSingleNode("//form/internal[@name='txtRequiredTabs']") as XmlElement;
                if (objSelectElement != null)
                {
                    XmlElement objXMLTempEle = p_objXmlOut.SelectSingleNode("//form/group/FormRequiredTabs") as XmlElement;
                    objXMLTempEle.InnerText = objSelectElement.GetAttribute("value");
                }
            }
        }
        // akaushik5 Added for MITS 30290 Ends
        #endregion
    }
}
