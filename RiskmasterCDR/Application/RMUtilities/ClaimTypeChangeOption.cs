using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Claim Type information.
	/// </summary>
	public class ClaimTypeChangeOption:UtilitiesUIBase
	{
		#region Private variables
		/// <summary>
		/// Fields mapping with database fields
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "CLCHGOPT_ROW_ID"},
			{"Lob","LOB"},
			{"CurClaimType","SOURCE_CLAIM_TYPE"},
			{"NewClaimType","DEST_CLAIM_TYPE"},
			{"TrigType","TRIG_TRANS_TYPE"},
            {"IncSchChecks","INC_SCHEDULED_CHECKS"}
									  };

//		private XmlDocument m_objXMLDocument = null;

        int m_iClientId = 0;//rkaur27
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public ClaimTypeChangeOption(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId) //rkaur27
		{
			try
			{
				ConnectString = p_sConnString;
				this.Initialize();
                m_iClientId = p_iClientId;//rkaur27
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimTypeChangeOption.Constructor.Error", m_iClientId),p_objEx);
			}
		}
		#endregion

		#region Private Functions
		new private void Initialize()
		{
			try
			{
				TableName = "CL_CHG_OPTIONS";
				KeyField = "CLCHGOPT_ROW_ID";
				base.InitFields(arrFields);
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimTypeChangeOption.Initialize.Error", m_iClientId),p_objEx);
			}
		}
		#endregion
		
		#region Public Functions
		/// <summary>
		/// Gets the xml structure for the form
		/// </summary>
		/// <param name="p_objXMLDocument">Input xml document</param>
		/// <returns>xml structure</returns>
		public XmlDocument New(XmlDocument p_objXMLDocument)
		{
			try
			{
				return p_objXMLDocument; 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimTypeChangeOption.New.Error", m_iClientId),p_objEx);
			}
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode =null;
			try
			{
				XMLDoc = p_objXmlDocument;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']"); 
				if (objNode.InnerText!=null && !objNode.InnerText.Equals(""))
					base.Get();
//				objNodLst = p_objXmlDocument.GetElementsByTagName("rowtext");
//				foreach(XmlElement objElm in objNodLst)
//				{
//					objElm.SetAttribute("title", UTILITY.GetCode(objElm.GetAttribute("title"),base.ConnectString)); 
//				}
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimTypeChangeOptions.Get.Err", m_iClientId),p_objEx);
			}
			finally
			{
//				objNodLst = null;
			}
		}

		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data to the database
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
            XmlNode objXmlNode = null;
            DbConnection objConn = null;
            int iLobCode = 0;
            int iSourceClaimCode = 0;
            int iTrigTypeCode = 0;
            int iIncSchChecks = 0;
            string sSql = string.Empty;
            int iRecord = 0;
			try
			{
				XMLDoc = p_objXmlDocument;
//				m_objXMLDocument = p_objXmlDocument;
                //Added for Mits-14673:Proper prompt message is required(when combination of Lob,SOURCE_CLAIM_TYPE and TRIG_TRANS_TYPE already exists).
                objXmlNode = p_objXmlDocument.SelectSingleNode("//control[@name='Lob']");
                if (objXmlNode != null && objXmlNode.Attributes["codeid"] != null)
                {
                    iLobCode = Convert.ToInt32(objXmlNode.Attributes["codeid"].Value);
                }
                objXmlNode = p_objXmlDocument.SelectSingleNode("//control[@name='CurClaimType']");
                if (objXmlNode != null && objXmlNode.Attributes["codeid"] != null)
                {
                    iSourceClaimCode = Convert.ToInt32(objXmlNode.Attributes["codeid"].Value);
                }
                objXmlNode = p_objXmlDocument.SelectSingleNode("//control[@name='TrigType']");
                if (objXmlNode != null && objXmlNode.Attributes["codeid"]!=null)
                {
                    iTrigTypeCode = Convert.ToInt32(objXmlNode.Attributes["codeid"].Value);
                }
                objXmlNode = p_objXmlDocument.SelectSingleNode("//control[@name='IncSchChecks']");
                if (objXmlNode != null && objXmlNode.Attributes["codeid"] != null)
                {
                    if (!string.IsNullOrEmpty(objXmlNode.Attributes["codeid"].Value))
                    {
                        iIncSchChecks = Convert.ToInt32(objXmlNode.Attributes["codeid"].Value);
                    }
                }
                sSql = "SELECT COUNT(*) FROM CL_CHG_OPTIONS WHERE LOB=" + iLobCode + "AND SOURCE_CLAIM_TYPE=" + iSourceClaimCode + "AND TRIG_TRANS_TYPE=" + iTrigTypeCode + "AND INC_SCHEDULED_CHECKS=" + iIncSchChecks;
                objConn = DbFactory.GetDbConnection(ConnectString);
                objConn.Open();
                iRecord=Convert.ToInt32(objConn.ExecuteScalar(sSql));
                objConn.Dispose();
                if (iRecord > 0)
                {
                    throw new RMAppException(Globalization.GetString("ClaimTypeChangeOption.UniqueIndex.Error", m_iClientId));
                }
                //Added for Mits-14673:Proper prompt message is required(when combination of Lob,SOURCE_CLAIM_TYPE and TRIG_TRANS_TYPE already exists).
				base.Save();
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
               
                throw new RMAppException(Globalization.GetString("ClaimTypeChangeOption.Save.Error", m_iClientId), p_objEx);
               
			}
			
		} 
		/// Name		: Delete
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes the Claim type information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Delete(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
			XMLDoc = p_objXmlDocument;
			try
			{
				base.Delete();
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimTypeChangeOption.Delete.Error", m_iClientId),p_objEx);
			}
		}
		#endregion
	
	}
}
