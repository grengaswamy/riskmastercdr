﻿///********************************************************************
/// Amendment History
///********************************************************************
/// Date Amended   *            Amendment               *    Author
/// 02/10/2014         MITS 34260 - duplicate claim         Ngupta36 
/// 09/20/2014 		   rma-857 	-  Incurred Handling - 	    ngupta36  
///********************************************************************
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
//Start(07/05/2010):Sumit
using Riskmaster.Settings;
//End:Sumit

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Line of business information.
	/// </summary>
	public class LOCParms
	{
		#region Input XML
		/*
		<LOBParms>
			<Options>
			<ReserveOptions>
			<optClosedReserves1>0</optClosedReserves1>
			<optClosedReserves2>0</optClosedReserves2>
			<optClosedReserves3>0</optClosedReserves3>
			<optReserveTracking>1</optReserveTracking> 
			<optClosedReserves4>
			</optClosedReserves4>
			<AdjustReservesCheck>0</AdjustReservesCheck>
			<AutoAdjustPerReserve>-1</AutoAdjustPerReserve>
			<CollResBal>0</CollResBal>
			<chkInsuff>0</chkInsuff>
			<ResClaimType>0</ResClaimType>
			<ReserveClaimTypes></ReserveClaimTypes>
			<NoResAdjBelowZero>
			</NoResAdjBelowZero>
			<ClaimTypes>55</ClaimTypes>
			<ReserveTypes>375 376 1069 378</ReserveTypes>
	         
			</ReserveOptions>
			<ClaimOptions>
			<IncClmTypeFlag>0</IncClmTypeFlag>
			<IncYearFlag>-1</IncYearFlag>
			<IncLobCodeFlag>0</IncLobCodeFlag>
			<IncOrgFlag>0</IncOrgFlag>
			<optTrigger>1</optTrigger>
			<LevelTypes>1006</LevelTypes>
			<Duplicate>-1</Duplicate>
			<PeriodTypes>1737</PeriodTypes>
	      
			<CaptionTypes>1007</CaptionTypes>

			<UseAdjusterText>0</UseAdjusterText>
			<DupClaimCriteria>
	      
			</DupClaimCriteria>
			</ClaimOptions>
			<AutoCloseOptions>
			<AutoCloseTransTypeCode>375</AutoCloseTransTypeCode>
			</AutoCloseOptions>
			<AutoAdjustOptions>
			<AutoAdjustTransTypeCode>3787</AutoAdjustTransTypeCode>

			</AutoAdjustOptions>
			</Options>
			<LineOfBusiness>
	  
			</LineOfBusiness>
		</LOBParms>
		*/
		#endregion

		#region Output Xml
		/*
		 <LOBParms>
		<Options>
			<ReserveOptions>
			<optClosedReserves1>0</optClosedReserves1>
			<optClosedReserves2>0</optClosedReserves2>
			<optClosedReserves3>0</optClosedReserves3>
			<optClosedReserves4>
			</optClosedReserves4>
			<AdjustReservesCheck>0</AdjustReservesCheck>
			<AutoAdjustPerReserve>-1</AutoAdjustPerReserve>
			<CollResBal>0</CollResBal>
			<chkInsuff>0</chkInsuff>
			<ResClaimType>0</ResClaimType>
			<NoResAdjBelowZero>
			</NoResAdjBelowZero>
			<optReserveTracking>1</optReserveTracking>
			<ClaimTypes>
				<Types value="4547">11 - 11</Types>
				<Types value="4548">33 - 33</Types>
				<Types value="4085">97 - 8</Types>
				<Types value="2439">APD - Auto Property Damage</Types>
				<Types value="55">BI - Bodily Injury</Types>
				<Types value="1736">EO - Errors &amp; Omissions</Types>
				<Types value="59">NA. - Not Applicable</Types>
				<Types value="4084">OP - oj</Types>
				<Types value="60">PD - Property Damage</Types>
				<Types value="5243">TST - TEST MITS4939</Types>
			</ClaimTypes>
			<ReserveTypes>
				<Reserves>
				<Reserve value="369">I Indemnity</Reserve>
				<Reserve value="370">M Medical</Reserve>
				<Reserve value="371">L Litigation</Reserve>
				<Reserve value="372">C Compensation</Reserve>
				<Reserve value="374">O Other</Reserve>
				<Reserve value="375">PD Property Damage</Reserve>
				<Reserve value="376">BI Bodily Injury</Reserve>
				<Reserve value="377">CA Cargo</Reserve>
				<Reserve value="378">EQ Equipment</Reserve>
				<Reserve value="1068">A Adjusting</Reserve>
				<Reserve value="1069">LG Legal</Reserve>
				<Reserve value="3562">NONPOST Non-Postable</Reserve>
				<Reserve value="4004">MJ MJ</Reserve>
				</Reserves>
			</ReserveTypes>
			</ReserveOptions>
			<ClaimOptions>
			<IncClmTypeFlag>0</IncClmTypeFlag>
			<IncYearFlag>-1</IncYearFlag>
			<IncLobCodeFlag>0</IncLobCodeFlag>
			<IncOrgFlag>0</IncOrgFlag>
			<optTrigger>1</optTrigger>
			<LevelTypes>
				<Types value="1005">Client</Types>
				<Types value="1006" selected="1">Company</Types>
				<Types value="1007">Operation</Types>
				<Types value="1008">Region</Types>
				<Types value="1009">Division</Types>
				<Types value="1010">Location</Types>
				<Types value="1011">Facility</Types>
				<Types value="1012">Department</Types>
			</LevelTypes>
			<Duplicate>-1</Duplicate>
			<PeriodTypes>
				<Types value="1737" selected="1">PB - Bi-Weekly</Types>
				<Types value="1038">PD - Day</Types>
				<Types value="1040">PM - Month</Types>
				<Types value="1041">PQ - Quarter</Types>
				<Types value="1039">PW - Week</Types>
				<Types value="1042">PY - Year</Types>
			</PeriodTypes>
			<CaptionTypes>
				<Types value="1005">Client</Types>
				<Types value="1006">Company</Types>
				<Types value="1007" selected="1">Operation</Types>
				<Types value="1008">Region</Types>
				<Types value="1009">Division</Types>
				<Types value="1010">Location</Types>
				<Types value="1011">Facility</Types>
			</CaptionTypes>
			<UseAdjusterText>0</UseAdjusterText>
			<DupClaimCriteria>
				<Types value="0">0 - No Additional Criteria</Types>
				<Types value="1">1 - Department and Event Date within X Days</Types>
				<Types value="2">2 - Operation Org Level, Clmnt Last Name and Event Date within X Days</Types>
				<Types value="3" selected="1">3 - Dept, Clm Type, Clmnt SS#, and Event Date within 56 Days</Types>
			</DupClaimCriteria>
			</ClaimOptions>
			<AutoCloseOptions>
			<AutoCloseTransTypeCode>
				<Types value="375">PD Property Damage</Types>
			</AutoCloseTransTypeCode>
			</AutoCloseOptions>
			<AutoAdjustOptions>
			<AutoAdjustTransTypeCode>
				<Types value="3787">AFRT Air Freight</Types>
			</AutoAdjustTransTypeCode>
			</AutoAdjustOptions>
		</Options>
		<LineOfBusiness>
			<Types value="844">DI - Short Term Disability</Types>
			<Types value="241">GC - General Claims</Types>
			<Types value="242">VA - Vehicle Accident Claims</Types>
			<Types value="243">WC - Workers' Compensation </Types>
		</LineOfBusiness>
	</LOBParms>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields = {
            //nnorouzi; MITS 18989 and cleaning up the code start
            {"CLM_ACC_LMT_FLAG","CLM_ACC_LMT_FLAG"}, 
            {"NO_WEEKS_PER_MONTH","NO_WEEKS_PER_MONTH"},	
            {"PAY_DET_LMT_FLAG","PAY_DET_LMT_FLAG"}, 
            {"PAY_LMT_FLAG","PAY_LMT_FLAG"}, 
            {"PRT_CHK_LMT_FLAG","PRT_CHK_LMT_FLAG"}, 
            {"RES_LMT_FLAG","RES_LMT_FLAG"}, 
            {"LINE_OF_BUS_CODE","LINE_OF_BUS_CODE"}, 
            {"EVENT_TRIGGER","EVENT_TRIGGER"}, 
            {"EST_NUM_OF_CLAIMS","EST_NUM_OF_CLAIMS"}, 
            {"PER_CL_LMT_FLAG","PER_CL_LMT_FLAG"}, 
            {"DUP_PAY_CRITERIA","DUP_PAY_CRITERIA"}, 
            {"DUP_NUM_DAYS","DUP_NUM_DAYS"}, 
            {"USE_CLAIM_PROGRESS_NOTES","USE_CLAIM_PROGRESS_NOTES"}, 
            {"USE_CLAIM_COMMENTS","USE_CLAIM_COMMENTS"},
            //mcapps2 MITS 23295
            {"IncludeScheduledChecks","INC_SCHEDULED_CHECKS"},
			{"optClosedReserves1","BAL_TO_ZERO"},								
			{"optClosedReserves2","SET_TO_ZERO"},
			{"optClosedReserves3","NEG_BAL_TO_ZERO"},
			{"optReserveTracking","RESERVE_TRACKING"},
			{"AdjustReservesCheck","ADJ_RSV"},								
			{"AutoAdjustPerReserve","ADJ_RSV_CLAIM"},
            //MGaba2 :R6 Reserve Worksheet :  Start
            {"UseReserveWorksheet","USE_RSV_WK"},
            {"AttachReserveWorksheet","ATTACH_RSV_WK"},
            //MGaba2 :R6 Reserve Worksheet:End
            {"ReasonCommentsRequiredRSW","REASON_COMMENTS_RSV_WK"},
			{"CollResBal","COLL_IN_RSV_BAL"},
		  //Shruti for MITS 8551 starts
		    {"CollIncBal","COLL_IN_INCUR_BAL"},
		  //Shruti for MITS 8551 ends
			{"chkInsuff","INSUF_FUNDS_FLAG"},
            //Shruti for EMI Changes
            {"chkInsuffAutoPay","USE_AUTOCHK_RESERVE_FLAG"},
			{"ResClaimType","RES_BY_CLM_TYPE"},
			{"IncClmTypeFlag","INC_CLM_TYPE_FLAG"},
			{"IncYearFlag","INC_YEAR_FLAG"},
			{"NoResAdjBelowZero","NONEG_RES_ADJ_FLAG"},
			{"IncLobCodeFlag","INC_LOB_CODE_FLAG"},
			{"IncOrgFlag","INC_ORG_FLAG"},
			{"LevelTypes","ORG_TABLE_ID"},
			{"Duplicate","DUPLICATE_FLAG"},
			{"PeriodTypes","DURATION_CODE"},
			{"CaptionTypes","CAPTION_LEVEL"},
            {"UseAdjusterText","USE_ADJTEXT"},
            {"UseISOClaimSubmission","USE_ISO_SUBMISSION"}, //rsushilaggar: ISO Claim Search
            //sumit - MITS #18227 - Start
			{"UseEnhPol","USE_ENH_POL_FLAG"},
            //sumit - MITS #18227 - End
            //smahajan6 - MITS #18230 - 02/03/2010 - Start
            {"IsPolicyFilter","IS_POLICY_FILTER"},
            //smahajan6 - MITS #18230 - 02/03/2010 - End
             //Start:Added by Nitin goel, MITS 30910, 01/22/2013
             {"AllowToApplyDedPayments", "ALLOW_TO_APPLY_DED"},
             {"DedRecTransactionType", "DED_REC_TRANS_TYPE"},
             {"DedRecReserveType", "DED_REC_RESERVE_TYPE"},
           
            //End:Added by Nitin goel, MITS 30910, 01/22/2013

            //Start: Added by Sumit Agarwal: 10/13/2014
            {"DedRecoveryIdentifierChar", "DED_RECOVERY_IDENTIFIER_CHAR"},
            {"EnableDiminishingDedFirstParty", "DIM_DED_FP_FLAG"},
            {"EnableSharedAggregateDedThirdParty", "SHARED_AGG_DED_TP_FLAG"},
            {"EnableDistributionDedAmtToSubsequentPmtThirdParty", "DIS_DED_AMT_SUB_PMT_TP_FLAG"},
            //End: Added by Sumit Agarwal: 10/13/2014

            //tanwar2 - mits 30910 - start
            {"DimPercentPeryrNum","DIM_PERCENT_PERYR_NUM"},
            {"PreventPrintZeroCheck","PREVENT_ZEROCHECK_FLAG"},
            //
            //tanwar2 - mits 30910 - end
            //spahariya 29221 Gap-12 - start
            {"DirectToPrinter","DIRECT_TO_PRINTER"},
             {"PrinterName","PRINTER_NAME"},
              {"PaperBinForLetters","LETTER_PAPER_BIN"},
			  {"PreventCollectionForAllRsvTypes", "RESTRICT_COLL_ALL_RSV_TYPE"}, //857
              {"PreventCollectionForPerRsvTypes", "RESTRICT_COLL_PER_RSV_TYPE"}, //857
              {"AddRecoveryReservetoTotalBalanceAmount","ADD_RCV_RSV_TO_BAL_AMT"},//asharma326 JIRA 871
              {"AddRecoveryReservetoTotalIncurredAmount","ADD_RCV_RSV_TO_INC_AMT"},//asharma326 JIRA 872
              {"PerRsvCollInRsvBal","PER_RSV_COLL_IN_RSV_BAL"},
              {"PerRsvCollInIncBal","PER_RSV_COLL_IN_INCR_BAL"},
              {"ManualDedRecoveryIdentifierChar", "MAN_DED_RECOVERY_IDENTIFIER"}
									  };
            //nnorouzi; MITS 18989 and cleaning up the code end

		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
        private string m_sUserName = ""; //gagnihotri MITS 11995 Changes made for Audit table
        /// <summary>
        /// Language code of the user logged in
        /// </summary>
        private int m_iLangCode = 0; //Aman MITS 31626
        private int m_iClientId = 0;//rkaur27
		#endregion

		#region Properties
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
        //Aman MITS 31626
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
		#endregion

		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnectString">Connection string</param>
		public LOCParms(string p_sConnectString, int p_iClientId) //rkaur27
		{
			m_sDSN=p_sConnectString;
            m_iClientId = p_iClientId; //rkaur27
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        public LOCParms(string p_sConnectString, string p_sUserName, int p_iClientId)//rkaur27
        {
            m_sDSN = p_sConnectString;
            m_sUserName = p_sUserName;
            m_iClientId = p_iClientId; //rkaur27
        }
		#endregion

		#region Public Functions
		/// Name		: GetReserveType
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Reserve type
		/// </summary>
		/// <param name="p_objDOC">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the Reserve types</returns>
		public XmlDocument GetReserveType(XmlDocument p_objDOC)
		{
			XmlDocument objDOM=null;
			XmlNode objNode=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			string sLobCode="";
			string sReserveByClaim="";
			string sClaimCode="";
			DbReader objReader=null;
			try
			{
				objNode=p_objDOC.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sLobCode=objNode.InnerText;
				}
				objNode=p_objDOC.SelectSingleNode(".//ResClaimType");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sReserveByClaim=objNode.InnerText;
				}
				objNode=p_objDOC.SelectSingleNode(".//ClaimTypes");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sClaimCode=objNode.InnerText;
					}
				}
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("LOBParms");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Reserves");
				objDOM.FirstChild.AppendChild(objElemChild);
				if (sReserveByClaim.Trim().ToLower().Equals("true"))
                {   //MITS 10306   : Umesh
					//sSQL="SELECT * FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE="+sLobCode
					//	+" AND CLAIM_TYPE_CODE="+sClaimCode;
                    sSQL = "SELECT SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_CLM_TYPE_RES, CODES_TEXT, CODES " +
                    "WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND " +
                    "CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + sLobCode + " AND SYS_CLM_TYPE_RES.CLAIM_TYPE_CODE=" + sClaimCode +
                    " ORDER BY CODES_TEXT.CODE_DESC";
					
						
				}
				else
                {   //MITS 10306   : Umesh
					//sSQL=" SELECT * FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE="+sLobCode;
                    sSQL = "SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_LOB_RESERVES, CODES_TEXT, CODES " +
                        "WHERE SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND " +
                        "CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + sLobCode +
                        " ORDER BY CODES_TEXT.CODE_DESC";
				}
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while  (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("Reserve");
					objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")));
					//objElemTemp.InnerText=UTILITY.GetCode(Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")),m_sDSN);;
                    objElemTemp.InnerText = objReader.GetString("SHORT_CODE") + " " + objReader.GetString("CODE_DESC");
					objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.GetReserveType.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objDOM=null;
				objNode=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
				if (objReader!=null)
				{
					objReader.Dispose();
				}
				
			}
		}
		/// <summary>
		/// Updates Claim Reserve Type
		/// </summary>
		/// <param name="p_objDOC">Input Xml containing parameters</param>
		/// <returns>True/False</returns>
		public bool UpdateSysClaimTypeReserve(XmlDocument p_objDOC)
		{
			string sSQL="";
			XmlNode objNode=null;
			string sLobCode="";
			int iTableId=0;
			LocalCache objCache=null;
			ArrayList alSql=null;
			DbReader objRead=null;
			DbReader objRead1=null;
			string sResByClaimType="";
			try
			{
				objNode=p_objDOC.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sLobCode=objNode.InnerText;
				}
				objNode=p_objDOC.SelectSingleNode(".//UpdateClaimResType");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sResByClaimType=objNode.InnerText;
				}
			
				alSql=new ArrayList();

				for(int i=13;i<= arrFields.GetUpperBound(0);i++)
				{
					string sValue1=arrFields[i,0];
					string sValue2=arrFields[i,1];
					p_objDOC.PreserveWhitespace=false;
					objNode=p_objDOC.SelectSingleNode(".//"+sValue1);
					string sTemp="";
					if(objNode != null)
					{
						if (objNode.Attributes["type"]!=null)
						{
							if (objNode.Attributes["type"].Value.Equals("checkbox") || objNode.Attributes["type"].Value.Equals("radio"))
							{
								if (objNode.InnerText!=null)
								{
									if (objNode.InnerText.Trim().ToLower().Equals("true") || objNode.InnerText.Trim().ToLower().Equals("-1"))
									{
										sTemp="-1";
									}
									else
									{
										sTemp="0";
									}
								}
								else
								{
									sTemp="0";
								}
							}
							sSQL += String.Format(" {0} = {1}," , sValue2, sTemp == ""? "null" : "'" + sTemp + "'");
						}
						else
						{
							if (objNode.InnerText!=null)
							{
								sSQL += String.Format(" {0} = {1}," , sValue2, objNode.InnerText.ToUpper() == ""? "null" : "'" + objNode.InnerText + "'");
							}
						}
					}
				}
				if(sSQL != string.Empty)
				{
					sSQL = sSQL.Substring(0,sSQL.Length - 1);
					sSQL = String.Format("UPDATE {0} SET {1} WHERE {2}","SYS_PARMS_LOB",sSQL," LINE_OF_BUS_CODE ="+sLobCode);
				}
				alSql.Add(sSQL);
				
				sSQL="DELETE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE = "+sLobCode;
				alSql.Add(sSQL);

				if (sResByClaimType.Trim().ToUpper().Equals("N"))
				{
					sSQL="DELETE FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE = "+sLobCode;
					alSql.Add(sSQL);
					
					sSQL = "SELECT DISTINCT RESERVE_TYPE_CODE, LINE_OF_BUS_CODE FROM";
					sSQL = sSQL + " SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE = " + sLobCode;
				
					objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
					if(objRead!=null)
					{
                        objCache = new LocalCache(m_sDSN, m_iClientId);
						iTableId=objCache.GetTableId("CLAIM_TYPE");
						objCache.Dispose();
						while(objRead.Read())
						{						
							sSQL = "SELECT DISTINCT CLAIM_TYPE_CODE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE = " + sLobCode;
							objRead1=DbFactory.GetDbReader(m_sDSN,sSQL);
							if(objRead1!=null)
							{
								while(objRead1.Read())
								{
									sSQL="INSERT INTO SYS_CLM_TYPE_RES (LINE_OF_BUS_CODE, RESERVE_TYPE_CODE, CLAIM_TYPE_CODE) VALUES('" + objRead.GetValue(1) + "','" + objRead.GetValue(0) + "','" + objRead1.GetValue(0) + "')";
									alSql.Add(sSQL);									
								}
							}
                            //gagnihotri MITS 11995 Changes made for Audit table
                            sSQL = "INSERT INTO SYS_LOB_RESERVES(reserve_type_code,LINE_OF_BUS_CODE,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES('" + objRead.GetValue(0) + "','" + objRead.GetValue(1) + "','" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
							alSql.Add(sSQL);
						}
					}

					if (objRead!=null)
					{
						if (!objRead.IsClosed)
						{
							objRead.Close();
						}
					}
					if (objRead1!=null)
					{
						if (!objRead1.IsClosed)
						{
							objRead1.Close();
						}
					}
				}
				else
				{
                    objCache = new LocalCache(m_sDSN, m_iClientId);
					iTableId=objCache.GetTableId("CLAIM_TYPE");
					objCache.Dispose();
					sSQL="INSERT INTO SYS_CLM_TYPE_RES (LINE_OF_BUS_CODE, RESERVE_TYPE_CODE, CLAIM_TYPE_CODE) ";
					sSQL = sSQL + "SELECT CODES.LINE_OF_BUS_CODE, SYS_LOB_RESERVES.RESERVE_TYPE_CODE, CODES.CODE_ID "
						+"FROM SYS_LOB_RESERVES, CODES WHERE CODES.LINE_OF_BUS_CODE = " + sLobCode + " AND CODES.TABLE_ID = " +iTableId.ToString()
						+" AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = CODES.LINE_OF_BUS_CODE";
					alSql.Add(sSQL);
				}

				sSQL="UPDATE GLOSSARY SET DTTM_LAST_UPDATE ="+Conversion.ToDbDateTime(DateTime.Now)+ " WHERE SYSTEM_TABLE_NAME ='SYS_LOB_RESERVES'";
				alSql.Add(sSQL);
                UTILITY.Save(alSql, m_sDSN, m_iClientId);
				return true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.UpdateSysClaimTypeReserve.Error", m_iClientId),p_objEx);
			}
			finally
			{
                if (objRead != null)
                    objRead.Dispose();
                if (objRead1 != null)
                    objRead1.Dispose();
				objNode=null;
                if (objCache != null)
                    objCache.Dispose();
				alSql=null;
			}
		}
		/// Name		: SaveDupNoOfDays
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves duplicate number of Days
		/// </summary>
		/// <param name="p_objDOC">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public XmlDocument SaveDupNoOfDays(XmlDocument p_objDOC)
		{
			string sSQL="";
			XmlNode objNode=null;
			ArrayList alSql=null;
			string sDupPayCriteria="";
			string sLobCode="";
			SortedList objList=null;
			XmlElement objElemTempTypes=null;
			XmlElement objElemTemp=null;
			string sDupDays="";
            StringBuilder sbSQL = null; //34260 
            DbConnection objConn = null; //34260
			try
			{
				alSql=new ArrayList();
				objNode=p_objDOC.SelectSingleNode(".//DupClaimCriteria");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sDupPayCriteria=objNode.InnerText;
				}
				objNode=p_objDOC.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sLobCode=objNode.InnerText;
				}
				objNode=p_objDOC.SelectSingleNode(".//DupNoOfDays");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sSQL="UPDATE SYS_PARMS_LOB SET DUP_NUM_DAYS="+objNode.InnerText
							+",DUP_PAY_CRITERIA="+ sDupPayCriteria+ " WHERE LINE_OF_BUS_CODE= " + sLobCode;
						alSql.Add(sSQL);
					}
				}
				//UTILITY.Save(alSql,m_sDSN);
				objList=new SortedList();

                //Start rsushilaggar MITS 20705 06/16/2010
                //objList.Add(0,"0 - No Additional Criteria");
                //objList.Add(1,"1 - Department and Event Date within X Days");
                //objList.Add(2,"2 - Operation Org Level, Clmnt Last Name and Event Date within X Days");
                //objList.Add(3,"3 - Dept, Clm Type, Clmnt SS#, and Event Date within X Days");
                //MITS 34260 -  START
                sbSQL = new StringBuilder();
                objConn = DbFactory.GetDbConnection(m_sDSN);
                objConn.Open();
                sbSQL.Append("select STR_PARM_VALUE from PARMS_NAME_VALUE where PARM_NAME like 'MULTI_COVG_CLM'");
                string iCarrierClaims = (objConn.ExecuteScalar(sbSQL.ToString())).ToString();
                sbSQL.Remove(0, sbSQL.Length);
                //MITS 34260 -  END
                objList.Add(0, "0 - No Additional Criteria");
                objList.Add(1, "1 - Department and Event Date within X Days");
                objList.Add(2, "2 - Operation Org Level and Event Date within X Days");
                objList.Add(3, "3 - Dept, Clm Type and Event Date within X Days");
                //End rsushilaggar
                //MITS 34260 -  START
                if (iCarrierClaims == "-1")
                    objList.Add(4, "4 - Policy Number, Claim Type and Event Date within X days");
                //MITS 34260 -  END
				
				objElemTemp = (XmlElement)p_objDOC
					.SelectSingleNode("//DupClaimCriteria");
				objElemTemp.InnerText="";
				IDictionaryEnumerator objEnum=objList.GetEnumerator();
				sDupDays=objNode.InnerText;

				while (objEnum.MoveNext())
				{
					

					objElemTempTypes=p_objDOC.CreateElement("Types");
					objElemTempTypes.SetAttribute("value",objEnum.Key.ToString());
					objElemTempTypes.InnerText=objEnum.Value.ToString();
					if (sDupPayCriteria.ToString().Equals(objEnum.Key.ToString()))
					{
						objElemTempTypes.SetAttribute("selected","1");
						if (!sDupDays.Trim().Equals(""))
						{
							objElemTempTypes.InnerText=objElemTempTypes.InnerText.Replace("X",sDupDays);
						}
					}
					objElemTemp.AppendChild(objElemTempTypes);
				}
				return p_objDOC;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.SaveDupNoOfDays.Error", m_iClientId),p_objEx);
			}
			finally
			{
			    objNode=null;
			    alSql=null;
                objList = null;
                objElemTempTypes = null;
                objElemTemp = null;
			}
			
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument  p_objXmlDocument)
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTempTypes=null;
			XmlElement objElemTemp=null;
			DbReader objReader=null;
			StringBuilder objSql=null;
			XmlNode objNode=null;
			LocalCache objCache=null;
            StringBuilder sbSQL = null; //34260 
            DbConnection objConn = null; //34260
			int iTableId=0;
			string sDupDays="";
			string sOrgId ="";
			string sDurationCode="";
			string sCaptionLevel="";
			int iDupCriteria=0;
			SortedList objList=null;
			string sResByClaimType="";
			DataSet objDs=null;
			XmlDocument objDocTemp=null;
			string sTemp="";
			string sLobCode="";
			int iCount=0;
            XmlElement objElem = null;
            //Start(07/05/2010):Sumit
            bool bAddLOB = false;
            SysSettings objSysSettings = null;
            //End:Sumit
            string IsmultiCovgClm = string.Empty;//Deb:BOB

            //tanwar2 - mits 30910 - start
            string sIsPolicySystemInterface = string.Empty;
            //tanwar2 - mits 30910 - end
            string sPrinterName = string.Empty; //spahariya MITS 29221
			try
			{
                //Start(07/05/2010):Sumit
                objSysSettings = new SysSettings(m_sDSN, m_iClientId);
                //End: Sumit

				objNode=p_objXmlDocument.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						if (objNode.InnerText!="")
							sLobCode=objNode.InnerText;
					}
				}
				objDOM =new XmlDocument();
                objElem = objDOM.CreateElement("LOB");
                objDOM.AppendChild(objElem);

				objElemParent = objDOM.CreateElement("LOBParms");
                //objDOM.AppendChild(objElemParent);
                objDOM.FirstChild.AppendChild(objElemParent);

                objElemChild=objDOM.CreateElement("Options");
              
                
				//objDOM.FirstChild.AppendChild(objElemChild);
               
                objDOM.FirstChild.FirstChild.AppendChild(objElemChild);
                objCache = new LocalCache(m_sDSN, m_iClientId);
				iTableId=objCache.GetTableId("LINE_OF_BUSINESS");
				objCache.Dispose();
                objDs = UTILITY.GetCodeList(iTableId.ToString(), m_sDSN, m_iClientId);
				objElemTemp = objDOM.CreateElement("LineOfBusiness");	
				if (objDs.Tables[0]!=null)
				{
					foreach (DataRow dr in objDs.Tables[0].Rows)
					{
                        //Start(07/08/2010): Sumit
                        bAddLOB = false;
                        switch (Conversion.ConvertObjToStr(dr["SHORT_CODE"]))
                        {
                            case "GC":
                                if (objSysSettings.UseGCLOB)
                                    bAddLOB = true;
                                break;
                            case "DI":
                                if (objSysSettings.UseDILOB)
                                    bAddLOB = true;
                                    break;
                            case "PC":
                                    if (objSysSettings.UsePCLOB)
                                        bAddLOB = true;
                                break;
                            case "VA":
                                if (objSysSettings.UseVALOB)
                                    bAddLOB = true;
                                    break;
                            case "WC":
                                if (objSysSettings.UseWCLOB)
                                    bAddLOB = true;
                                    break;
                        }

                        if (bAddLOB)
                        {
                            objElemTempTypes = objDOM.CreateElement("Types");
                            objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                            objElemTempTypes.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"] + " - " + Conversion.ConvertObjToStr(dr["CODE_DESC"]));
                            objElemTemp.AppendChild(objElemTempTypes);
                            if (sLobCode.Trim().Equals(""))
                                sLobCode = Conversion.ConvertObjToStr(dr["CODE_ID"]);
                            if (sLobCode.Trim().Equals(Conversion.ConvertObjToStr(dr["CODE_ID"])))
                            {
                                objElemTempTypes.SetAttribute("selected", "1");
                            }
                            iCount++;
                        }
                        //End: Sumit
					}
				
				}
				//objDOM.FirstChild.AppendChild(objElemTemp);//asif
                objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
				
				if (!sLobCode.Equals(""))
				{
                    //nnorouzi; cleaning up the code start
					objSql=new StringBuilder();

                    objSql.Append("SELECT ");

                    for (int i = 0; i <= arrFields.GetUpperBound(0); i++)
                    {
                        if (i > 0)
                        {
                            objSql.Append(", ");
                        }
                        objSql.Append(arrFields[i,1]);

                    }

                    objSql.Append(string.Format(" FROM {0} WHERE LINE_OF_BUS_CODE = {1}", "SYS_PARMS_LOB", sLobCode));
                    //nnorouzi; cleaning up the code end

					objReader = DbFactory.GetDbReader(m_sDSN,objSql.ToString());
					
					if (objReader.Read())
					{
						sOrgId =Conversion.ConvertObjToStr(objReader.GetValue("ORG_TABLE_ID"));
						sDurationCode=Conversion.ConvertObjToStr(objReader.GetValue("DURATION_CODE"));
						sCaptionLevel=Conversion.ConvertObjToStr(objReader.GetValue("CAPTION_LEVEL"));
					
					
						objElemParent=objDOM.CreateElement("ReserveOptions");
						//objDOM.FirstChild.AppendChild(objElemParent);//asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
						objElemTemp=objDOM.CreateElement("optClosedReserves1");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("BAL_TO_ZERO"));
						
                        objElemParent.AppendChild(objElemTemp);

						objElemTemp=objDOM.CreateElement("optClosedReserves2");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("SET_TO_ZERO"));
						
                        objElemParent.AppendChild(objElemTemp);

						objElemTemp=objDOM.CreateElement("optClosedReserves3");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("NEG_BAL_TO_ZERO"));
						
                        objElemParent.AppendChild(objElemTemp);

						objElemTemp=objDOM.CreateElement("optClosedReserves4");
						objElemTemp.InnerText="";
						
                        objElemParent.AppendChild(objElemTemp);
					
						objElemTemp=objDOM.CreateElement("AdjustReservesCheck");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("ADJ_RSV"));
						objElemParent.AppendChild(objElemTemp);

                        //MGaba2:R6 Reserve Worksheet related settings in LOB Parameter :Start
                        string sRSWTemp=string.Empty ;
                        objElemTemp = objDOM.CreateElement("UseReserveWorksheet");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("USE_RSV_WK"));
                        objElemParent.AppendChild(objElemTemp);

                        objElemTemp = objDOM.CreateElement("AttachReserveWorksheet");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ATTACH_RSV_WK"));
                        objElemParent.AppendChild(objElemTemp);

                        objElemTemp = objDOM.CreateElement("ReasonCommentsRequiredRSW");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("REASON_COMMENTS_RSV_WK"));
                        objElemParent.AppendChild(objElemTemp);

                        objElemTemp = objDOM.CreateElement("RSWCLaimTypes");
                        //string strTempAutoAdjustTypes = string.Empty;

                        objDs = DbFactory.GetDataSet(m_sDSN, "SELECT CLAIM_TYPE_CODE FROM SYS_LOB_RSW_CLAIM_TYPE WHERE LINE_OF_BUS_CODE=" + sLobCode, m_iClientId);
                        if (objDs.Tables[0] != null)
                        {
                            foreach (DataRow dr in objDs.Tables[0].Rows)
                            {
                                objElemTempTypes = objDOM.CreateElement("Types");
                                objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]));
                                sRSWTemp = sRSWTemp + " " + Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]);
                                objElemTempTypes.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]), m_sDSN,m_iClientId);
                                objElemTemp.AppendChild(objElemTempTypes);
                                //strTempAutoAdjustTypes = strTempAutoAdjustTypes + " " + Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]);//Asif
                            }                            
                        }

                        objElemTemp.SetAttribute("codeid", sRSWTemp);//123
                        objElemParent.AppendChild(objElemTemp);

                       
                        objElemTemp = objDOM.CreateElement("RSWCLaimStatus");
                        sRSWTemp = string.Empty;

                        objDs = DbFactory.GetDataSet(m_sDSN, "SELECT CLAIM_STATUS_CODE FROM SYS_LOB_RSW_CLAIM_STATUS WHERE LINE_OF_BUS_CODE=" + sLobCode, m_iClientId);
                        if (objDs.Tables[0] != null)
                        {
                            foreach (DataRow dr in objDs.Tables[0].Rows)
                            {
                                objElemTempTypes = objDOM.CreateElement("Types");
                                objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CLAIM_STATUS_CODE"]));
                                sRSWTemp = sRSWTemp + " " + Conversion.ConvertObjToStr(dr["CLAIM_STATUS_CODE"]);
                                objElemTempTypes.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(dr["CLAIM_STATUS_CODE"]), m_sDSN, m_iClientId);
                                objElemTemp.AppendChild(objElemTempTypes);
                            }
                        }

                        objElemTemp.SetAttribute("codeid", sRSWTemp);//123
                        objElemParent.AppendChild(objElemTemp);

                        //MGaba2:R6 Reserve Worksheet :End

						objElemTemp=objDOM.CreateElement("AutoAdjustPerReserve");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("ADJ_RSV_CLAIM"));
						objElemParent.AppendChild(objElemTemp);
						objElemTemp=objDOM.CreateElement("CollResBal");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("COLL_IN_RSV_BAL"));
						objElemParent.AppendChild(objElemTemp);
						//Shruti for MITS 8551 starts
						objElemTemp=objDOM.CreateElement("CollIncBal");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("COLL_IN_INCUR_BAL"));
						objElemParent.AppendChild(objElemTemp);
						//Shruti for MITS 8551 ends
                        //asharma326 JIRA 870 Starts
                        objElemTemp = objDOM.CreateElement("PerRsvCollInRsvBal");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("PER_RSV_COLL_IN_RSV_BAL"));
                        objElemParent.AppendChild(objElemTemp);

                        objElemTemp = objDOM.CreateElement("PerRsvCollInIncBal");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("PER_RSV_COLL_IN_INCR_BAL"));
                        objElemParent.AppendChild(objElemTemp);
                        //asharma326 JIRA 870 Ends
						objElemTemp=objDOM.CreateElement("chkInsuff");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("INSUF_FUNDS_FLAG"));
                        objElemParent.AppendChild(objElemTemp);

                        objElemTemp = objDOM.CreateElement("chkInsuffAutoPay");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("USE_AUTOCHK_RESERVE_FLAG"));
						objElemParent.AppendChild(objElemTemp);

						objElemTemp=objDOM.CreateElement("ResClaimType");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("RES_BY_CLM_TYPE"));
						sResByClaimType=objElemTemp.InnerText;
						objElemParent.AppendChild(objElemTemp);
				
						objElemTemp=objDOM.CreateElement("NoResAdjBelowZero");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("NONEG_RES_ADJ_FLAG"));
						objElemParent.AppendChild(objElemTemp);
                        //asharma326 JIRA 871 Starts
                        objElemTemp = objDOM.CreateElement("AddRecoveryReservetoTotalBalanceAmount");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ADD_RCV_RSV_TO_BAL_AMT"));
                        objElemParent.AppendChild(objElemTemp);
                        //asharma326 JIRA 871 Ends
                        //asharma326 JIRA 872 Starts
                        objElemTemp = objDOM.CreateElement("AddRecoveryReservetoTotalIncurredAmount");
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ADD_RCV_RSV_TO_INC_AMT"));
                        objElemParent.AppendChild(objElemTemp);
                        //asharma326 JIRA 872 Ends

						objElemTemp=objDOM.CreateElement("optReserveTracking");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TRACKING"));
						objElemParent.AppendChild(objElemTemp);

                        //JIRA-857 Starts
                        if (Conversion.ConvertObjToStr(objReader.GetValue("RESTRICT_COLL_ALL_RSV_TYPE")) != "-1")
                        {
                            objElemTemp = objDOM.CreateElement("PreventCollectionForAllRsvTypes");
                            objElemTemp.InnerText = "0";
                            objElemParent.AppendChild(objElemTemp);
                        }
                        else
                        {
                            objElemTemp = objDOM.CreateElement("PreventCollectionForAllRsvTypes");
                            objElemTemp.InnerText = "-1";
                            objElemParent.AppendChild(objElemTemp);
                        }

                        if (Conversion.ConvertObjToStr(objReader.GetValue("RESTRICT_COLL_PER_RSV_TYPE")) != "-1")
                        {
                            objElemTemp = objDOM.CreateElement("PreventCollectionForPerRsvTypes");
                            objElemTemp.InnerText = "0";
                            objElemParent.AppendChild(objElemTemp);
                        }
                        else
                        {
                            objElemTemp = objDOM.CreateElement("PreventCollectionForPerRsvTypes");
                            objElemTemp.InnerText = "-1";
                            objElemParent.AppendChild(objElemTemp);
                        }
                        //857 Ends

                        objCache = new LocalCache(m_sDSN, m_iClientId);
						iTableId=objCache.GetTableId("CLAIM_TYPE");
						objCache.Dispose();
						objSql=new StringBuilder();
						objSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT");
						objSql.Append(" WHERE CODES.TABLE_ID ="+iTableId.ToString());
                        objSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND "); 
						objSql.Append(" (CODES.LINE_OF_BUS_CODE = "+sLobCode );
                        // MITS 9797 :  Claim Type List not as per rmworld.. modified by Raman Bhatia
                        objSql.Append(" OR CODES.LINE_OF_BUS_CODE = 0 ) AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0");
                        //objSql.Append(" OR CODES.LINE_OF_BUS_CODE = 0 OR CODES.LINE_OF_BUS_CODE IS NULL) AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0");
						objSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC");

                        objSql = CommonFunctions.PrepareMultilingualQuery(objSql.ToString(), "CLAIM_TYPE", this.LanguageCode); //Aman MITS 31626

                        objDs = DbFactory.GetDataSet(m_sDSN, objSql.ToString(), m_iClientId);
						
						//TR2647 Raman Bhatia - Claim Type passed by UI is not getting considered
						objNode=p_objXmlDocument.SelectSingleNode(".//ClaimTypes");
						if (objNode!=null)
						{
							if (objNode.InnerText!=null)
							{
								if (objNode.InnerText!="")
									sTemp=objNode.InnerText;
							}
						}

						objElemTemp = objDOM.CreateElement("ClaimTypes");
						if (objDs.Tables[0]!=null)
						{
							for(int i=0;i<objDs.Tables[0].Rows.Count;i++)
							{
								DataRow dr =objDs.Tables[0].Rows[i];
								//TR2647 Raman Bhatia - Claim Type passed by UI is not getting considered						
								if ((i==0)&&(sTemp==""))
								{
									sTemp=Conversion.ConvertObjToStr(dr[0]);
								}
								objElemTempTypes=objDOM.CreateElement("Types");
								objElemTempTypes.SetAttribute("value",Conversion.ConvertObjToStr(dr[0]));
								//TR2647 Raman Bhatia - Claim Type passed by UI is not getting considered
								if (sTemp.Trim().Equals(Conversion.ConvertObjToStr(dr[0])))
								{
									objElemTempTypes.SetAttribute("selected","1");
								}
								objElemTempTypes.InnerText=Conversion.ConvertObjToStr(dr[1]+" - "+Conversion.ConvertObjToStr(dr[2]));
								objElemTemp.AppendChild(objElemTempTypes);
							}
				
						}//end if
						
						objElemParent.AppendChild(objElemTemp);
						objElemTemp = objDOM.CreateElement("ReserveTypes");

                        string strTemp = string.Empty;//asif
						if (sResByClaimType.Trim().Equals("-1"))
						{
                            
							objDocTemp=GetReserveType(sLobCode,sTemp,true,ref strTemp);

						}
						else
						{
                            
                            objDocTemp = GetReserveType(sLobCode, sTemp, false, ref strTemp);
						}

						objNode=objDOM.ImportNode(objDocTemp.DocumentElement,true);
                        //Asif Start
                       
                        objElemTemp.SetAttribute("codeid",strTemp);//asif

                        //Asif End
						objElemTemp.AppendChild(objNode);
						objElemParent.AppendChild(objElemTemp);
						//objDOM.FirstChild.FirstChild.AppendChild(objElemParent);asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                        //asharma326 JIRA 870 Starts
                        strTemp = string.Empty;
                        objElemTemp = objDOM.CreateElement("PerReserveTypesBalance");
                        objDocTemp = this.GetPerReserveType(sLobCode, -1, ref strTemp);
                        objNode = objDOM.ImportNode(objDocTemp.DocumentElement, true);
                        objElemTemp.SetAttribute("codeid", strTemp);
                        objElemTemp.AppendChild(objNode);
                        objElemParent.AppendChild(objElemTemp);
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

                        strTemp = string.Empty;
                        objElemTemp = objDOM.CreateElement("PerReserveTypesIncurred");
                        objDocTemp = this.GetPerReserveType(sLobCode, 0, ref strTemp);
                        objNode = objDOM.ImportNode(objDocTemp.DocumentElement, true);
                        objElemTemp.SetAttribute("codeid", strTemp);
                        objElemTemp.AppendChild(objNode);
                        objElemParent.AppendChild(objElemTemp);
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                        //asharma326 JIRA 870 Ends
						objElemParent=objDOM.CreateElement("ClaimOptions");

						//objDOM.FirstChild.AppendChild(objElemParent);//asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

						objElemTemp=objDOM.CreateElement("IncClmTypeFlag");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("INC_CLM_TYPE_FLAG"));
						objElemParent.AppendChild(objElemTemp);objElemTemp=objDOM.CreateElement("IncYearFlag");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("INC_YEAR_FLAG"));
						objElemParent.AppendChild(objElemTemp);
						objElemTemp=objDOM.CreateElement("IncLobCodeFlag");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("INC_LOB_CODE_FLAG"));
						objElemParent.AppendChild(objElemTemp);
						objElemTemp=objDOM.CreateElement("IncOrgFlag");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("INC_ORG_FLAG"));
						objElemParent.AppendChild(objElemTemp);
						objElemTemp=objDOM.CreateElement("optTrigger");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("EVENT_TRIGGER"));
						if (objElemTemp.InnerText=="-1")
						{
							objElemTemp.InnerText="1";
						}
						objElemParent.AppendChild(objElemTemp);
						objElemTemp = objDOM.CreateElement("LevelTypes");
                        objDs = DbFactory.GetDataSet(m_sDSN, "select GLOSSARY_TEXT.table_id,table_name from glossary,GLOSSARY_TEXT where glossary_type_code=5 and GLOSSARY_TEXT.table_id=glossary.table_id", m_iClientId);
						if (objDs.Tables[0]!=null)
						{
						
							foreach (DataRow dr in objDs.Tables[0].Rows)
							{
								objElemTempTypes=objDOM.CreateElement("Types");
								objElemTempTypes.SetAttribute("value",Conversion.ConvertObjToStr(dr["table_id"]));
								objElemTempTypes.InnerText=Conversion.ConvertObjToStr(dr["table_name"]);
								if (sOrgId.Trim().Equals(Conversion.ConvertObjToStr(dr["table_id"])))
								{
									objElemTempTypes.SetAttribute("selected","1");
								}
								objElemTemp.AppendChild(objElemTempTypes);
							}
				
						}//end if
						objElemParent.AppendChild(objElemTemp);
						objElemTemp=objDOM.CreateElement("Duplicate");
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("DUPLICATE_FLAG"));
						objElemParent.AppendChild(objElemTemp);
                        objCache = new LocalCache(m_sDSN, m_iClientId);
						iTableId=objCache.GetTableId("PERIOD_TYPES");
						objCache.Dispose();
                        objDs = UTILITY.GetCodeList(iTableId.ToString(), m_sDSN, m_iClientId);
						objElemTemp = objDOM.CreateElement("PeriodTypes");
						if (objDs.Tables[0]!=null)
						{
						
							foreach (DataRow dr in objDs.Tables[0].Rows)
							{
								objElemTempTypes=objDOM.CreateElement("Types");
								objElemTempTypes.SetAttribute("value",Conversion.ConvertObjToStr(dr["CODE_ID"]));
								objElemTempTypes.InnerText=Conversion.ConvertObjToStr(dr["SHORT_CODE"]+" - "+Conversion.ConvertObjToStr(dr["CODE_DESC"]));
								if (sDurationCode.Trim().Equals(Conversion.ConvertObjToStr(dr["CODE_ID"])))
								{
									objElemTempTypes.SetAttribute("selected","1");
								}
								objElemTemp.AppendChild(objElemTempTypes);
							}
				
						}//end if
						objElemParent.AppendChild(objElemTemp);

					}
				 
					objElemTemp = objDOM.CreateElement("CaptionTypes");
                    objDs = DbFactory.GetDataSet(m_sDSN, "SELECT TABLE_ID, TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID BETWEEN 1005 AND 1012 ORDER BY TABLE_ID", m_iClientId);//MITS 34436 asingh263
					if (objDs.Tables[0]!=null)
					{
						
						foreach (DataRow dr in objDs.Tables[0].Rows)
						{
							objElemTempTypes=objDOM.CreateElement("Types");
							objElemTempTypes.SetAttribute("value",Conversion.ConvertObjToStr(dr["table_id"]));
							objElemTempTypes.InnerText=Conversion.ConvertObjToStr(dr["TABLE_NAME"]);
							if (sCaptionLevel.Trim().Equals(Conversion.ConvertObjToStr(dr["table_id"])))
							{
								objElemTempTypes.SetAttribute("selected","1");
							}

							objElemTemp.AppendChild(objElemTempTypes);
						}
				

					}//end if
					objElemParent.AppendChild(objElemTemp);
					objElemTemp=objDOM.CreateElement("UseAdjusterText");
					objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("USE_ADJTEXT"));
					objElemParent.AppendChild(objElemTemp);
					
					//raman has modified code for claimprogressnotes
					//objElemTemp=objDOM.CreateElement("UseClaimProgressNotes");Asif Changed to Enhanced Noted
                    objElemTemp = objDOM.CreateElement("UseEnhancedNotes");
					objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("USE_CLAIM_PROGRESS_NOTES"));
					objElemParent.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("WeeksPerMonth");
					objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("NO_WEEKS_PER_MONTH"));
					objElemParent.AppendChild(objElemTemp);

					objElemTemp=objDOM.CreateElement("UseClaimComments");
					objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("USE_CLAIM_COMMENTS"));
					objElemParent.AppendChild(objElemTemp);

                    //rsushilaggar: ISO Claim Search
                    objElemTemp = objDOM.CreateElement("UseISOClaimSubmission");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("USE_ISO_SUBMISSION"));
                    objElemParent.AppendChild(objElemTemp);
                    //end : rsushilaggar
                    //Sumit - 09/21/2009 MITS#18227 Create "Use Enhanced Policy Flag" and get the value from DB 
                    objElemTemp = objDOM.CreateElement("UseEnhPol");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("USE_ENH_POL_FLAG"));
                    if(!IsActCodeRequired())
                        objElemTemp.SetAttribute("IsActCodeReq", "Y");
                    else
                        objElemTemp.SetAttribute("IsActCodeReq", "N");
                    objElemParent.AppendChild(objElemTemp);
                    //smahajan6 - MITS #18230 - 02/03/2010 - Start
                    objElemTemp = objDOM.CreateElement("IsPolicyFilter");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("IS_POLICY_FILTER"));
                    objElemParent.AppendChild(objElemTemp);
                    //smahajan6 - MITS #18230 - 02/03/2010 - End

                    //spahariya - 07/30/2010 MITS#29221 G.12 - Start 
                    objElemTemp = objDOM.CreateElement("DirectToPrinter");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DIRECT_TO_PRINTER"));
                    objElemParent.AppendChild(objElemTemp);
                    objNode = p_objXmlDocument.SelectSingleNode(".//PrinterName");
                    if (objNode != null)
                    {
                        if (objNode.InnerText != null)
                        {
                            if (objNode.InnerText != "")
                                sPrinterName = objNode.InnerText;
                        }
                    }
                    objElemTemp = objDOM.CreateElement("PrinterName");
                    if (sPrinterName != "")
                        objElemTemp.InnerText = sPrinterName;
                    else
                        objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("PRINTER_NAME"));
                    objElemParent.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("PaperBinForLetters");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("LETTER_PAPER_BIN"));
                    objElemParent.AppendChild(objElemTemp);
                    //End  07/30/2010 MITS#29221 G.12
					objList=new SortedList();
					if (objReader.GetValue("DUP_PAY_CRITERIA")!=DBNull.Value)
					{
						sDupDays=Conversion.ConvertObjToStr(objReader.GetValue("DUP_NUM_DAYS"));
					}	
					else
					{
						sDupDays="0";
					}
					objElemTemp=objDOM.CreateElement("DupNoOfDays");
					objElemTemp.InnerText=sDupDays;
					objElemParent.AppendChild(objElemTemp);
					if (objReader.GetValue("DUP_PAY_CRITERIA")!=DBNull.Value)
					{
						iDupCriteria=Conversion.ConvertObjToInt(objReader.GetValue("DUP_PAY_CRITERIA"), m_iClientId);
					}
					else
					{
						iDupCriteria=0;
					}
                    //MITS 34260 -  START
                    sbSQL = new StringBuilder();
                    objConn = DbFactory.GetDbConnection(m_sDSN);
                    objConn.Open();
                    sbSQL.Append("select STR_PARM_VALUE from PARMS_NAME_VALUE where PARM_NAME like 'MULTI_COVG_CLM'");
                    string iCarrierClaims = (objConn.ExecuteScalar(sbSQL.ToString())).ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    //MITS 34260 -  END
					objList.Add(0,"0 - No Additional Criteria");
					objList.Add(1,"1 - Department and Event Date within X Days");
                    objList.Add(2, "2 - Operation Org Level, Claimant Last Name and Event Date within X Days");//skhare7 MITS 27422
                    objList.Add(3, "3 - Department, Claim Type, Claimant SS#, and Event Date within X days");//skhare7 MITS 27425
                    //MITS 34260 -  START
                    if (iCarrierClaims=="-1")
                    	objList.Add(4, "4 - Policy Number, Claim Type and Event Date within X days");
                    //MITS 34260 -  END
					objElemTemp = objDOM.CreateElement("DupClaimCriteria");
					IDictionaryEnumerator objEnum=objList.GetEnumerator();
					while (objEnum.MoveNext())
					{
					
						objElemTempTypes=objDOM.CreateElement("Types");
						objElemTempTypes.SetAttribute("value",objEnum.Key.ToString());
						objElemTempTypes.InnerText=objEnum.Value.ToString();
						if (iDupCriteria.ToString().Equals(objEnum.Key.ToString()))
						{
							objElemTempTypes.SetAttribute("selected","1");
							if (!sDupDays.Trim().Equals(""))
							{
								objElemTempTypes.InnerText=objElemTempTypes.InnerText.Replace("X",sDupDays);
							}
						}
						objElemTemp.AppendChild(objElemTempTypes);
					}
				
					objElemParent.AppendChild(objElemTemp);
					//objDOM.FirstChild.FirstChild.AppendChild(objElemParent);asif
                    objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
				
					objElemParent=objDOM.CreateElement("AutoAdjustOptions");

					//objDOM.FirstChild.AppendChild(objElemParent);//asif
                    objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

					objElemTemp = objDOM.CreateElement("AutoAdjustTransTypeCode");
                    string strTempAutoAdjustTypes = string.Empty;



                    objDs = DbFactory.GetDataSet(m_sDSN, "SELECT * FROM SYS_RES_AUTOADJ WHERE LINE_OF_BUS_CODE=" + sLobCode, m_iClientId);
					if (objDs.Tables[0]!=null)
					{
						
						foreach (DataRow dr in objDs.Tables[0].Rows)
						{
							objElemTempTypes=objDOM.CreateElement("Types");
							objElemTempTypes.SetAttribute("value",Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]));
							objElemTempTypes.InnerText=UTILITY.GetCode(Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]),m_sDSN,m_iClientId);
							objElemTemp.AppendChild(objElemTempTypes);
                            strTempAutoAdjustTypes = strTempAutoAdjustTypes + " " + Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]);//Asif
						}
					}//end if

                    objElemTemp.SetAttribute("codeid", strTempAutoAdjustTypes);//123
					objElemParent.AppendChild(objElemTemp);
					//objDOM.FirstChild.FirstChild.AppendChild(objElemParent);//asif
                    objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                    //857 Starts
                    objElemParent = objDOM.CreateElement("CollOptions");
                    objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                    objElemTemp = objDOM.CreateElement("RsvCollRsvTypeCode");
                    string strTempElemTempTypes = string.Empty;

                    objDs = DbFactory.GetDataSet(m_sDSN, "SELECT * FROM SYS_RES_COLL_NOT_ALLOWED WHERE LINE_OF_BUS_CODE=" + sLobCode,m_iClientId);
                    if (objDs.Tables[0] != null)
                    {
                        foreach (DataRow dr in objDs.Tables[0].Rows)
                        {
                            objElemTempTypes = objDOM.CreateElement("Types");
                            objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]));
                            objElemTempTypes.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]), m_sDSN, m_iClientId);
                            objElemTemp.AppendChild(objElemTempTypes);
                            strTempElemTempTypes = strTempElemTempTypes + " " + Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]);
                        }
                    }
                    objElemTemp.SetAttribute("codeid", strTempElemTempTypes);
                    objElemParent.AppendChild(objElemTemp);
                    //857 Ends
					objElemParent=objDOM.CreateElement("AutoCloseOptions");

					//objDOM.FirstChild.AppendChild(objElemParent);//asif
                    objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);//asif
					objElemTemp = objDOM.CreateElement("AutoCloseTransTypeCode");
                    //string strTempElemTempTypes=string.Empty;
                    strTempElemTempTypes = string.Empty; //JIRA-857
                   // strTemp = string.Empty;
                    objDs = DbFactory.GetDataSet(m_sDSN, "SELECT * FROM SYS_TRANS_CLOSE WHERE LINE_OF_BUS_CODE=" + sLobCode, m_iClientId);
					if (objDs.Tables[0]!=null)
					{
						
						foreach (DataRow dr in objDs.Tables[0].Rows)
						{
							objElemTempTypes=objDOM.CreateElement("Types");
							objElemTempTypes.SetAttribute("value",Conversion.ConvertObjToStr(dr["TRANS_TYPE_CODE"]));
							objElemTempTypes.InnerText=UTILITY.GetCode(Conversion.ConvertObjToStr(dr["TRANS_TYPE_CODE"]),m_sDSN,m_iClientId);
							objElemTemp.AppendChild(objElemTempTypes);
                            strTempElemTempTypes = strTempElemTempTypes + " " + Conversion.ConvertObjToStr(dr["TRANS_TYPE_CODE"]);//Asif
						}
					}//end if
                    objElemTemp.SetAttribute("codeid", strTempElemTempTypes);//Asif
					objElemParent.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("IncludeScheduledChecks");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("INC_SCHEDULED_CHECKS"));
                    objElemParent.AppendChild(objElemTemp);
                    //tanwar2 - mits 30910 - start
                    objElemParent = objDOM.CreateElement("DeductibleOptions");

                    //Start:Added by Nitin goel, MITS 30910, 01/22/2013
                    objElemTemp = objDOM.CreateElement("AllowToApplyDedPayments");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("ALLOW_TO_APPLY_DED"));
                    objElemParent.AppendChild(objElemTemp);
                    objElemTemp = objDOM.CreateElement("systemcontrol");
                    objElemTemp.SetAttribute("name", "DedRecTransactionType");
                    objElemTemp.SetAttribute("codeid", Conversion.ConvertObjToStr(objReader.GetValue("DED_REC_TRANS_TYPE")));
                    objElemTemp.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(objReader.GetValue("DED_REC_TRANS_TYPE")), m_sDSN, m_iClientId);
                    objElemParent.AppendChild(objElemTemp);
                    objElemTemp = objDOM.CreateElement("systemcontrol");
                    objElemTemp.SetAttribute("name", "DedRecReserveType");
                    objElemTemp.SetAttribute("codeid", Conversion.ConvertObjToStr(objReader.GetValue("DED_REC_RESERVE_TYPE")));
                    objElemTemp.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(objReader.GetValue("DED_REC_RESERVE_TYPE")), m_sDSN, m_iClientId);
                    objElemParent.AppendChild(objElemTemp);
                    objElemTemp = objDOM.CreateElement("ToStoreMasterRecoveryCode");
                    objCache = new LocalCache(m_sDSN, m_iClientId);
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objCache.GetCodeId("R", "MASTER_RESERVE"));
                    objCache.Dispose();
                    objElemParent.AppendChild(objElemTemp);

                    //Start:Added by Sumit Agarwal: 10/13/2014
                    objElemTemp = objDOM.CreateElement("DedRecoveryIdentifierChar");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DED_RECOVERY_IDENTIFIER_CHAR"));
                    objElemParent.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("ManualDedRecoveryIdentifierChar");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("MAN_DED_RECOVERY_IDENTIFIER"));
                    objElemParent.AppendChild(objElemTemp);
                    objElemTemp = objDOM.CreateElement("EnableDiminishingDedFirstParty");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DIM_DED_FP_FLAG"));
                    objElemParent.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("EnableSharedAggregateDedThirdParty");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SHARED_AGG_DED_TP_FLAG"));
                    objElemParent.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("EnableDistributionDedAmtToSubsequentPmtThirdParty");
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("DIS_DED_AMT_SUB_PMT_TP_FLAG"));
                    objElemParent.AppendChild(objElemTemp);
                    //End:Added by Sumit Agarwal: 10/13/2014

                    //objElemTemp = objDOM.CreateElement("AllowEditDeductibleReserve");
                    //objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("NOT_ALLOW_TO_MODIFY_DEDRES"));
                    //objElemParent.AppendChild(objElemTemp);
                    

                    //tanwar2 - mits 30910 - start
                    objElemTemp = objDOM.CreateElement("DiminishingPercent");
                    objElemTemp.InnerText = string.Format("{0:f2}", objReader.GetDouble("DIM_PERCENT_PERYR_NUM"));
                    objElemParent.AppendChild(objElemTemp);

                    objElemTemp = objDOM.CreateElement("PreventPrintZeroCheck");
                    objElemTemp.InnerText = objReader.GetValue("PREVENT_ZEROCHECK_FLAG").ToString();
                    objElemParent.AppendChild(objElemTemp);
                    //tanwar2 - mits 30910 - end

					//objDOM.FirstChild.FirstChild.AppendChild(objElemParent);//asif
                    objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

                    
                }//new

                //Deb:BOB
                objReader = DbFactory.GetDbReader(m_sDSN, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME ='MULTI_COVG_CLM'");
                while (objReader.Read())
                {
                    IsmultiCovgClm = Conversion.ConvertObjToStr(objReader.GetValue("STR_PARM_VALUE"));
                }
                objNode = objDOM.SelectSingleNode("//IsPolicyFilter");
                XmlAttribute mulClmAttr = objDOM.CreateAttribute("IsmultiCovgClm");
                mulClmAttr.Value = IsmultiCovgClm;
                objNode.Attributes.Append(mulClmAttr);
                //tanwar2 - mits 30910 - start
                objReader = DbFactory.GetDbReader(m_sDSN, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME ='USE_POLICY_INTERFACE'");
                if (objReader.Read())
                {
                    sIsPolicySystemInterface = objReader["STR_PARM_VALUE"].ToString();
                }
                XmlAttribute PolicySysAttr = objDOM.CreateAttribute("IsPolicySystemInterface");
                PolicySysAttr.Value = sIsPolicySystemInterface;

                objNode.Attributes.Append(PolicySysAttr);
                //tanwar2 - mits 30910 - end

				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.Get.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objDOM=null;
				objElemParent=null;
				objElemChild=null;
				objElemTempTypes=null;
				objElemTemp=null;
				objSql=null;
				objNode=null;
                if(objCache != null)
				    objCache.Dispose();
				objList=null;
				objDs=null;
				objDocTemp=null;
				if (objReader!=null)
				{
					objReader.Dispose();
				}
                //Start(07/05/2010):Sumit
                objSysSettings = null;
                //End:Sumit
			}
			
		}
        //asharma326 JIRA 870 Starts
        private XmlDocument GetPerReserveType(string sLobCode, int sRsvIncrCode, ref string strCodeId)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemTemp = null;
            string sSQL = "";
            DbReader objReader = null;
            try
            {
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("Reserves");
                objDOM.AppendChild(objElemParent);
                sSQL = string.Format("SELECT SYS_RES_COLL_IN_RSV_INCR_BAL.RES_TYPE_CODE,CODES_TEXT.SHORT_CODE,CODES_TEXT.CODE_DESC FROM SYS_RES_COLL_IN_RSV_INCR_BAL, CODES_TEXT, CODES" +
                       " WHERE SYS_RES_COLL_IN_RSV_INCR_BAL.RES_TYPE_CODE = CODES_TEXT.CODE_ID AND " +
                       " CODES_TEXT.CODE_ID = CODES.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = {0} AND CODES.DELETED_FLAG <> -1 " +
                       " AND SYS_RES_COLL_IN_RSV_INCR_BAL.LINE_OF_BUS_CODE = {1} AND SYS_RES_COLL_IN_RSV_INCR_BAL.COLL_IN_RSV_BAL={2} " +
                       " ORDER BY CODES_TEXT.CODE_DESC", m_iLangCode, sLobCode, sRsvIncrCode);
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("Reserve");
                    objElemTemp.SetAttribute("value", Convert.ToString(objReader.GetValue(0)));
                    objElemTemp.InnerText = objReader.GetString(1) + " " + objReader.GetString(2);
                    objDOM.FirstChild.AppendChild(objElemTemp);
                    strCodeId = strCodeId + " " + Conversion.ConvertObjToStr(objReader.GetValue(0));
                }

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LOCParms.GetPerReserveType.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemTemp = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }

            }
        }
        //asharma326 JIRA 870 Ends

		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data to the database
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			ArrayList alQuery=null;
			XmlNode objNode=null;
			string sSql = string.Empty;
			bool bReturn=false;
			bool sDuplicateClaimCriteriaDisabled=false;
			bool bClm_AdjTxtDisabled=false;
			bool bClm_PrgNotesDisabled=false;
			string sLobCode="";
			string sDupPayCriteria="";
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sLobCode=objNode.InnerText;
				}
				for(int i=13;i<= arrFields.GetUpperBound(0);i++)
				{
					string sValue1=arrFields[i,0];
					string sValue2=arrFields[i,1];
					p_objXmlDocument.PreserveWhitespace=false;
					objNode=p_objXmlDocument.SelectSingleNode(".//"+sValue1);
					string sTemp="";
					if(objNode != null)
					{
						if (objNode.Attributes["type"]!=null)
						{
							if (objNode.Attributes["type"].Value.Equals("checkbox") || objNode.Attributes["type"].Value.Equals("radio"))
							{
								if (objNode.InnerText!=null)
								{
									if (objNode.InnerText.Trim().ToLower().Equals("true") || objNode.InnerText.Trim().ToLower().Equals("-1"))
									{
										sTemp="-1";
									}
									else
									{
										sTemp="0";
									}
								}
								else
								{
									sTemp="0";
								}
							}
							sSql += String.Format(" {0} = {1}," , sValue2, sTemp == ""? "null" : "'" + sTemp + "'");
						}
						else
						{
							if (objNode.InnerText!=null)
							{
								sSql += String.Format(" {0} = {1}," , sValue2, objNode.InnerText.ToUpper() == ""? "null" : "'" + objNode.InnerText + "'");
							}
						}
					}
				}
				if(sSql != string.Empty)
				{
					sSql = sSql.Substring(0,sSql.Length - 1);
                    //gagnihotri MITS 11995 Changes made for Audit table
                    sSql = sSql + ",UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now);
					sSql = String.Format("UPDATE {0} SET {1} WHERE {2}","SYS_PARMS_LOB",sSql," LINE_OF_BUS_CODE ="+sLobCode);
				}
				alQuery=new ArrayList();
				alQuery.Add(sSql);
				SaveAutoOptions(alQuery,p_objXmlDocument,"SYS_TRANS_CLOSE","GetAutoClose");
				SaveAutoOptions(alQuery,p_objXmlDocument,"SYS_RES_AUTOADJ","GetAutoAdjust");
                SaveCollOptions(alQuery, p_objXmlDocument, "SYS_RES_COLL_NOT_ALLOWED", "GetCollOptions"); //JIRA-857
                SavePerReserveTypes(alQuery, p_objXmlDocument);//asharma326 JIRA 870
				SaveReserveTypes(alQuery,p_objXmlDocument);
                //MGaba2:R6: Saving Reserve Worksheet related settings in LOB Parameter :Start
                SaveRSWClaimTypesAndStatus(alQuery, p_objXmlDocument);
                //MGaba2:R6: Saving Reserve Worksheet related settings in LOB Parameter:End
				objNode=p_objXmlDocument.SelectSingleNode("//Duplicate");
				if(objNode != null)
				{
					if (objNode.InnerText !=null)
					{
						if (!objNode.InnerText.ToLower().Equals("true"))
						{
                            //gagnihotri MITS 11995 Changes made for Audit table
                            sSql = "UPDATE SYS_PARMS_LOB SET DUPLICATE_FLAG=0,DUP_PAY_CRITERIA=-1,DUP_NUM_DAYS=0,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" +Conversion.ToDbDateTime(DateTime.Now)+ "WHERE LINE_OF_BUS_CODE =" + sLobCode;
							sDuplicateClaimCriteriaDisabled = true;
							alQuery.Add(sSql);
						}
						else
						{
                            //gagnihotri MITS 11995 Changes made for Audit table
							sSql = "UPDATE SYS_PARMS_LOB SET DUPLICATE_FLAG=-1,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" +Conversion.ToDbDateTime(DateTime.Now)+ "WHERE LINE_OF_BUS_CODE ="+sLobCode;
							alQuery.Add(sSql);
						}
					}
				}
				objNode=p_objXmlDocument.SelectSingleNode("//IncOrgFlag");
				if(objNode != null)
				{
					if (objNode.InnerText !=null)
					{
						if (!objNode.InnerText.ToLower().Equals("true"))
						{
                            //gagnihotri MITS 11995 Changes made for Audit table
                            sSql = "UPDATE SYS_PARMS_LOB SET ORG_TABLE_ID=0,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
							alQuery.Add(sSql);
						}
					}
				}
				objNode=p_objXmlDocument.SelectSingleNode("//WeeksPerMonth");
				if(objNode != null)
				{
					if (objNode.InnerText !=null)
					{
                        //Changed by Gagan for MITS 9318 : Start
                        if (objNode.InnerText == "")
                            objNode.InnerText = "0";
                        //Changed by Gagan for MITS 9318 : End
             
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSql = "UPDATE SYS_PARMS_LOB SET NO_WEEKS_PER_MONTH = " + objNode.InnerText + ",UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
							alQuery.Add(sSql);
					}
				}
				objNode=p_objXmlDocument.SelectSingleNode("//optTrigger");
				if(objNode != null)
				{
					if (objNode.InnerText !=null)
					{
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSql = "UPDATE SYS_PARMS_LOB SET EVENT_TRIGGER=" + objNode.InnerText + ",UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
						alQuery.Add(sSql);
					}
				}


				objNode=p_objXmlDocument.SelectSingleNode(".//DupClaimCriteria");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sDupPayCriteria=objNode.InnerText;
				}
				if(sDupPayCriteria.Trim() == "")
					sDupPayCriteria = "0";

				if(sDuplicateClaimCriteriaDisabled == false)
				{
					objNode=p_objXmlDocument.SelectSingleNode(".//DupNoOfDays");
					if (objNode!=null)
					{
						if (objNode.InnerText!=null)
						{
                            //gagnihotri MITS 11995 Changes made for Audit table
							sSql="UPDATE SYS_PARMS_LOB SET DUP_NUM_DAYS="+objNode.InnerText
                                + ",DUP_PAY_CRITERIA=" + sDupPayCriteria + ",UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + " WHERE LINE_OF_BUS_CODE= " + sLobCode;
							alQuery.Add(sSql);
						}
					}
				}

				
				//raman has added code for claimprogressnote
				objNode=p_objXmlDocument.SelectSingleNode(".//UseAdjusterText");
				if (objNode!=null)
				{
					if (!objNode.InnerText.ToLower().Equals("true"))
					{
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSql = "UPDATE SYS_PARMS_LOB SET USE_ADJTEXT = 0,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
						bClm_AdjTxtDisabled = true;
						alQuery.Add(sSql);
					}
					else
					{
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSql = "UPDATE SYS_PARMS_LOB SET USE_ADJTEXT = -1 , USE_CLAIM_COMMENTS = -1,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
						bClm_AdjTxtDisabled = false;
						alQuery.Add(sSql);
					}
				}


				objNode=p_objXmlDocument.SelectSingleNode(".//UseEnhancedNotes");
				if (objNode!=null)
				{
					if (!objNode.InnerText.ToLower().Equals("true"))
					{
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSql = "UPDATE SYS_PARMS_LOB SET USE_CLAIM_PROGRESS_NOTES = 0 , USE_CLAIM_COMMENTS = -1,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
						bClm_PrgNotesDisabled = true;
						alQuery.Add(sSql);
					}
					else
					{
                        //gagnihotri MITS 11995 Changes made for Audit table
                        sSql = "UPDATE SYS_PARMS_LOB SET USE_CLAIM_PROGRESS_NOTES = -1,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
						bClm_PrgNotesDisabled = false;
						alQuery.Add(sSql);
					}
				}

                //Sumit - 09/21/2009 MITS#18227 Update SYS_PARMS_LOB table based on the selection of "Use Enhanced Policy Flag" 
                objNode = p_objXmlDocument.SelectSingleNode(".//UseEnhPol");
                if (objNode != null)
                {
                    if (!objNode.InnerText.ToLower().Equals("true"))
                    {
                        sSql = "UPDATE SYS_PARMS_LOB SET USE_ENH_POL_FLAG = 0 ,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
                        alQuery.Add(sSql);
                    }
                    else
                    {
                        sSql = "UPDATE SYS_PARMS_LOB SET USE_ENH_POL_FLAG = -1,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
                        alQuery.Add(sSql);
                    }
                }

                //smahajan6 - MITS #18230 - 02/03/2010 - Start
                objNode = p_objXmlDocument.SelectSingleNode(".//IsPolicyFilter");
                if (objNode != null)
                {
                    if (!objNode.InnerText.ToLower().Equals("true"))
                    {
                        sSql = "UPDATE SYS_PARMS_LOB SET IS_POLICY_FILTER = 0 ,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
                        alQuery.Add(sSql);
                    }
                    else
                    {
                        sSql = "UPDATE SYS_PARMS_LOB SET IS_POLICY_FILTER = -1,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
                        alQuery.Add(sSql);
                    }
                }
                //smahajan6 - MITS #18230 - 02/03/2010 - End

				if(bClm_AdjTxtDisabled == true && bClm_PrgNotesDisabled == false)
				
				{
					objNode=p_objXmlDocument.SelectSingleNode(".//UseClaimComments");
					if (objNode!=null)
					{
						if (!objNode.InnerText.ToLower().Equals("true"))
						{
                            //gagnihotri MITS 11995 Changes made for Audit table
                            sSql = "UPDATE SYS_PARMS_LOB SET USE_CLAIM_COMMENTS = 0,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
							alQuery.Add(sSql);
						}
						else
						{
                            //gagnihotri MITS 11995 Changes made for Audit table
                            sSql = "UPDATE SYS_PARMS_LOB SET USE_CLAIM_COMMENTS = -1,UPDATED_BY_USER='" + m_sUserName + "',DTTM_RCD_LAST_UPD=" + Conversion.ToDbDateTime(DateTime.Now) + "WHERE LINE_OF_BUS_CODE =" + sLobCode;
							alQuery.Add(sSql);
						}
					}
				}
                //start - changed by Nikhil.Code review changes
                StringBuilder sbSql = null;
                sbSql = new StringBuilder();
               
                //End -  code review changes
                objNode = p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='DedRecTransactionType']");
                if (objNode != null && objNode.Attributes["codeid"] != null)
                {

                    //Start - Changed by Nikhil.Code review changes 
                  
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("UPDATE SYS_PARMS_LOB SET DED_REC_TRANS_TYPE='").Append(objNode.Attributes["codeid"].Value);
                    sbSql.Append("' ,UPDATED_BY_USER= '").Append(m_sUserName);
                    sbSql.Append("',DTTM_RCD_LAST_UPD= ").Append(Conversion.ToDbDateTime(DateTime.Now));
                    sbSql.Append(" WHERE LINE_OF_BUS_CODE = ").Append(sLobCode);
                    alQuery.Add(sbSql.ToString());
                    //End - Code review changes


                }
                objNode = p_objXmlDocument.SelectSingleNode("//systemcontrol[@name='DedRecReserveType']");
                if (objNode != null && objNode.Attributes["codeid"] != null)
                {
                    //Start -  changed by Nikhil.Code review changes

                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("UPDATE SYS_PARMS_LOB SET DED_REC_RESERVE_TYPE='").Append(objNode.Attributes["codeid"].Value);
                    sbSql.Append("' ,UPDATED_BY_USER= '").Append(m_sUserName);
                    sbSql.Append("',DTTM_RCD_LAST_UPD= ").Append(Conversion.ToDbDateTime(DateTime.Now));
                    sbSql.Append(" WHERE LINE_OF_BUS_CODE = ").Append(sLobCode);
                    alQuery.Add(sbSql.ToString());
                    //End -  code review changes
                }
                
                //tanwar2 - mits 30910 - start

                objNode = p_objXmlDocument.SelectSingleNode("//DiminishingPercent");
                if (objNode != null)
                {

                    //Start -  changed by Nikhil.Code review changes

                   
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("UPDATE SYS_PARMS_LOB SET DIM_PERCENT_PERYR_NUM='").Append(objNode.InnerText);
                    sbSql.Append("' ,UPDATED_BY_USER= '").Append(m_sUserName);
                    sbSql.Append("',DTTM_RCD_LAST_UPD= ").Append(Conversion.ToDbDateTime(DateTime.Now));
                    sbSql.Append(" WHERE LINE_OF_BUS_CODE = ").Append(sLobCode);
                    alQuery.Add(sbSql.ToString());
                    //End -  code review changes
                  
                }

                //Start: Added by Sumit Agarwal to update DED_RECOVERY_IDENTIFIER_CHAR: 10/14/2014
                objNode = p_objXmlDocument.SelectSingleNode("//DedRecoveryIdentifierChar");
                if (objNode != null)
                {
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("UPDATE SYS_PARMS_LOB SET DED_RECOVERY_IDENTIFIER_CHAR='").Append(objNode.InnerText);
                    sbSql.Append("' ,UPDATED_BY_USER= '").Append(m_sUserName);
                    sbSql.Append("',DTTM_RCD_LAST_UPD= ").Append(Conversion.ToDbDateTime(DateTime.Now));
                    sbSql.Append(" WHERE LINE_OF_BUS_CODE = ").Append(sLobCode);
                    alQuery.Add(sbSql.ToString());
                }
                //End: Added by Sumit Agarwal to update DED_RECOVERY_IDENTIFIER_CHAR: 10/14/2014

                objNode = p_objXmlDocument.SelectSingleNode("//ManualDedRecoveryIdentifierChar");
                if (objNode != null)
                {
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("UPDATE SYS_PARMS_LOB SET MAN_DED_RECOVERY_IDENTIFIER='").Append(objNode.InnerText);
                    sbSql.Append("' ,UPDATED_BY_USER= '").Append(m_sUserName);
                    sbSql.Append("',DTTM_RCD_LAST_UPD= ").Append(Conversion.ToDbDateTime(DateTime.Now));
                    sbSql.Append(" WHERE LINE_OF_BUS_CODE = ").Append(sLobCode);
                    alQuery.Add(sbSql.ToString());
                }
                //tanwar2 - mits 30910 - end
                UTILITY.Save(alQuery, m_sDSN, m_iClientId);
				bReturn=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.Save.Error", m_iClientId),p_objEx);
			}
			finally
			{
				alQuery=null;
				objNode=null;
			}
			return bReturn;
		}

        //asharma326 JIRA 870 Starts
        private bool SavePerReserveTypes(ArrayList alQuery, XmlDocument p_objXmlDocument)
        {
            string[] arrIds = null;
            XmlNode objNode = null;
            DbReader objRead = null;
            DbConnection objConn = null;
            string sSQL = string.Empty, sLobCode = string.Empty, sPerReserveTypesBalance = string.Empty, sPerReserveTypesIncurred = string.Empty;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode(".//LineOfBusiness");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                        sLobCode = objNode.InnerText;
                }
                objNode = p_objXmlDocument.SelectSingleNode(".//GetAllPerReserveTypesBalance");
                if (objNode != null)
                {
                    if (objNode.InnerText != null && !string.IsNullOrEmpty(objNode.InnerText))
                        sPerReserveTypesBalance = objNode.InnerText;
                }
                objNode = p_objXmlDocument.SelectSingleNode(".//GetAllPerReserveTypesIncurred");
                if (objNode != null)
                {
                    if (objNode.InnerText != null && !string.IsNullOrEmpty(objNode.InnerText))
                        sPerReserveTypesIncurred = objNode.InnerText;
                }

                objConn = DbFactory.GetDbConnection(m_sDSN);
                objConn.Open();
                objConn.ExecuteNonQuery("DELETE FROM SYS_RES_COLL_IN_RSV_INCR_BAL WHERE LINE_OF_BUS_CODE = " + sLobCode);
                objConn.Dispose();
                arrIds = sPerReserveTypesBalance.Split(',');
                if (arrIds.Length > 0 && !string.IsNullOrEmpty(arrIds[0]))
                {
                    foreach (string item in arrIds)
                    {
                        sSQL = string.Format("INSERT INTO SYS_RES_COLL_IN_RSV_INCR_BAL values ({0},{1},-1)", sLobCode, item);
                        alQuery.Add(sSQL);
                    }
                }
                //Array.Clear(arrIds, 0, arrIds.Length);
                arrIds = null;
                arrIds = sPerReserveTypesIncurred.Split(',');
                if (arrIds.Length > 0 && !string.IsNullOrEmpty(arrIds[0]))
                {
                    foreach (string item in arrIds)
                    {
                        sSQL = string.Format("INSERT INTO SYS_RES_COLL_IN_RSV_INCR_BAL values ({0},{1},0)", sLobCode, item);
                        alQuery.Add(sSQL);
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LOCParms.SavePerReserveTypes.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //arrIds = null;
                objNode = null;
                if (objRead != null)
                {
                    objRead.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
            return true;
        }
        //asharma326 JIRA 870 Ends
		#endregion

		#region Private Functions
		private bool SaveReserveTypes(ArrayList p_alSql,XmlDocument p_objDOC)
		{
			string sSQL="";
			string[] arrIds=null;
			XmlNode objNode=null;
			DbReader objRead=null;
			DbConnection objConn = null;
			string sResByClaimType="";
			string sClaimType="";
			string sLobCode="";
			try
			{
				objNode=p_objDOC.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sLobCode=objNode.InnerText;
				}
				objNode=p_objDOC.SelectSingleNode(".//ResClaimType");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sResByClaimType=objNode.InnerText;
				}
			
				if (sResByClaimType.Trim().ToLower().Equals("true"))
				{
					objNode=p_objDOC.SelectSingleNode(".//ClaimTypes");
					if (objNode!=null)
					{
						if (objNode.InnerText!=null)
							sClaimType=objNode.InnerText;
					}
					sSQL="DELETE FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE="+sLobCode+" AND claim_type_code="+sClaimType;
					p_alSql.Add(sSQL);
					objNode=p_objDOC.SelectSingleNode("//GetClaimReserveTypes");
					if (objNode!=null)
					{
						if (objNode.InnerText!=null)
						{
							if (!objNode.InnerText.Trim().Equals(""))
							{
								arrIds=objNode.InnerText.Split(','.ToString().ToCharArray());
								foreach (string sTemp in arrIds)
								{
									if (!sTemp.Trim().Equals(""))
									{
										sSQL="INSERT INTO SYS_CLM_TYPE_RES(reserve_type_code,LINE_OF_BUS_CODE,claim_type_code) VALUES('"+sTemp+"','"+sLobCode+"','"+sClaimType+"')";
										p_alSql.Add(sSQL);
									}
								}
							}
						}
					}
				}
				else
				{
					objNode=p_objDOC.SelectSingleNode("//GetClaimReserveTypes");
					if (objNode!=null)
					{
						// Defect# 1296, 1297, 1298: Existing reserve types do not get deleted.
						objConn = DbFactory.GetDbConnection(m_sDSN);
						objConn.Open();
						objConn.ExecuteNonQuery("DELETE from SYS_LOB_RESERVES where LINE_OF_BUS_CODE="+sLobCode);
						objConn.Dispose();
						if (objNode.InnerText!=null)
						{
							if (!objNode.InnerText.Trim().Equals(""))
							{
								arrIds=objNode.InnerText.Split(','.ToString().ToCharArray());
								foreach (string sTemp in arrIds)
								{
									if (!sTemp.Trim().Equals(""))
									{
										sSQL="SELECT reserve_type_code FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE="+sLobCode +"AND reserve_type_code="+sTemp;
                                        objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
										
										if (!objRead.Read())
										{
                                            //gagnihotri MITS 11995 Changes made for Audit table
                                            sSQL = "INSERT INTO SYS_LOB_RESERVES(reserve_type_code,LINE_OF_BUS_CODE,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES('"
                                                + sTemp + "','" + sLobCode + "','" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ")";
											p_alSql.Add(sSQL);
										}
									}
				
								}
								if (objRead!=null)
								{
									if (!objRead.IsClosed)
									{
										objRead.Close();
										objRead.Dispose();
									}
								}
							}
						}
					}
				}
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.SaveReserveTypes.Error", m_iClientId),p_objEx);
			}
			finally
			{
				arrIds=null;
				objNode=null;
				if (objRead!=null)
				{
					objRead.Dispose();
				}
				if (objConn!= null)
				{
					objConn.Dispose();
				}
			}
			return true;
        }
        /// <summary>
        /// MGaba2:R6: function to save Claim Type and Status used for Reserve WorkSheet
        /// </summary>
        /// <param name="p_alSql"></param>
        /// <param name="p_objDOC"></param>
        /// <returns></returns>
        private bool SaveRSWClaimTypesAndStatus(ArrayList p_alSql,XmlDocument p_objDOC)
        {
            string sSQL = "";
            string[] arrRSW = null;
            XmlNode objNode = null;                    
            string sLobCode = "";
            try
            {
                objNode=p_objDOC.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sLobCode=objNode.InnerText;
				}

                //Saving Claim Types :Start
                objNode = p_objDOC.SelectSingleNode("//GetAllRSWClaimTypes");
                if (objNode != null)
                {                       
                   
                    sSQL = "DELETE from SYS_LOB_RSW_CLAIM_TYPE where LINE_OF_BUS_CODE=" + sLobCode;
                    p_alSql.Add(sSQL); 
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                        {
                            arrRSW = objNode.InnerText.Split(','.ToString().ToCharArray());
                            foreach (string sClaimType in arrRSW)
                            {
                                if (!sClaimType.Trim().Equals(""))
                                {
                                    sSQL = "INSERT INTO SYS_LOB_RSW_CLAIM_TYPE(LINE_OF_BUS_CODE,CLAIM_TYPE_CODE,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES("
                                                + sLobCode + "," + sClaimType + ",'" + m_sUserName + "','" + Conversion.ToDbDateTime(DateTime.Now) + "')";
                                    p_alSql.Add(sSQL);
                                }
                            }
                         }

                    }                                
                }                
                //Saving Claim Types :End

                //Saving Claim Status :Start
                objNode = p_objDOC.SelectSingleNode("//GetAllRSWClaimStatus");
                if (objNode != null)
                {

                    sSQL = "DELETE from SYS_LOB_RSW_CLAIM_STATUS where LINE_OF_BUS_CODE=" + sLobCode;
                    p_alSql.Add(sSQL);                    
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                        {
                            arrRSW = objNode.InnerText.Split(','.ToString().ToCharArray());
                            foreach (string sClaimStatus in arrRSW)
                            {
                                if (!sClaimStatus.Trim().Equals(""))
                                {
                                    sSQL = "INSERT INTO SYS_LOB_RSW_CLAIM_STATUS(LINE_OF_BUS_CODE,CLAIM_STATUS_CODE,UPDATED_BY_USER,DTTM_RCD_LAST_UPD) VALUES("
                                                + sLobCode + "," + sClaimStatus + ",'" + m_sUserName + "','" + Conversion.ToDbDateTime(DateTime.Now) + "')";
                                    p_alSql.Add(sSQL);
                                }
                            }
                        }

                    }
                }
                //Saving Claim Status :End
            } 
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LOCParms.SaveRSWClaimTypesAndStatus.Error", m_iClientId), p_objEx);
            }
            finally
            {
                arrRSW = null;
                objNode = null;
            }
            return true;
        }

        /// <summary>
		/// Gets the Reserve type
		/// </summary>
		/// <param name="p_sLobCode">LOB code</param>
		/// <param name="p_sClaimCode">Claim code</param>
		/// <param name="bReserveByClaim">Reserve based to claim type or not</param>
		/// <returns>Xml containing the Reserve types</returns>
		public XmlDocument GetReserveType(string p_sLobCode,string p_sClaimCode,bool bReserveByClaim,ref string strCodeId)//asif
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			DbReader objReader=null;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("Reserves");
				objDOM.AppendChild(objElemParent);
                //Aman MITS 31626 --start made changes in the query for ML
				if (bReserveByClaim)
                {   //MITS 10306   : Umesh
                    //sSQL = "SELECT * FROM SYS_CLM_TYPE_RES WHERE LINE_OF_BUS_CODE=" + p_sLobCode + " AND CLAIM_TYPE_CODE=" + p_sClaimCode;
                    sSQL = "SELECT SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_CLM_TYPE_RES, CODES_TEXT, CODES " +
                    "WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND " +
                    "CODES_TEXT.CODE_ID = CODES.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND CODES.DELETED_FLAG <> -1 AND SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + p_sLobCode + " AND SYS_CLM_TYPE_RES.CLAIM_TYPE_CODE=" + p_sClaimCode +
                    " ORDER BY CODES_TEXT.CODE_DESC";
					
						
				}
				else
                {   //MITS 10306   : Umesh
					//sSQL=" SELECT * FROM SYS_LOB_RESERVES WHERE LINE_OF_BUS_CODE="+p_sLobCode;
                    sSQL = "SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_LOB_RESERVES, CODES_TEXT, CODES " +
                        "WHERE SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND " +
                        "CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES_TEXT.LANGUAGE_CODE = 1033  AND CODES.DELETED_FLAG <> -1 AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + p_sLobCode +
                        " ORDER BY CODES_TEXT.CODE_DESC";
				}

                StringBuilder sbSQL = new StringBuilder();
                sbSQL = CommonFunctions.PrepareMultilingualQuery(sSQL, "RESERVE_TYPE", this.LanguageCode);
                //Aman MITS 31626 --end made changes in the query for ML
                objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
				while(objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("Reserve");
					//objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")));
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0))); //Aman MITS 31626
					//objElemTemp.InnerText=UTILITY.GetCode(Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")),m_sDSN);;
                    //objElemTemp.InnerText = objReader.GetString("SHORT_CODE") + " " + objReader.GetString("CODE_DESC");
                    objElemTemp.InnerText = objReader.GetString(1) + " " + objReader.GetString(2); //Aman MITS 31626
					objDOM.FirstChild.AppendChild(objElemTemp);
                    //Asif Start

                    strCodeId = strCodeId + " " +Conversion.ConvertObjToStr(objReader.GetValue(0));
                   


				}
				
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.GetReserveType.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objDOM=null;
				objElemParent=null;
				objElemTemp=null;
				if (objReader!=null)
				{
					objReader.Dispose();
				}
				
			}
		}
		private bool SaveAutoOptions(ArrayList p_alSql,XmlDocument p_objDOC,string p_sTableName,string p_sTagName)
		{
			string sSQL="";
			string[] arrIds=null;
			XmlNode objNode=null;
			string sLobCode="";
			try
			{
				objNode=p_objDOC.SelectSingleNode(".//LineOfBusiness");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sLobCode=objNode.InnerText;
				}
				objNode=p_objDOC.SelectSingleNode(".//DupNoOfDays");
				sSQL="DELETE FROM "+p_sTableName+" WHERE LINE_OF_BUS_CODE="+sLobCode;
				p_alSql.Add(sSQL);
				objNode=p_objDOC.SelectSingleNode("//"+p_sTagName);
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						if (!objNode.InnerText.Trim().Equals(""))
						{
							arrIds=objNode.InnerText.Split(','.ToString().ToCharArray());
							foreach (string sTemp in arrIds)
							{
								if (!sTemp.Trim().Equals(""))
								{
									sSQL="INSERT INTO "+p_sTableName +" VALUES('"+sTemp+"','"+sLobCode+"')";
									p_alSql.Add(sSQL);
								}
							}
						}
					}
				}
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("LOCParms.SaveAutoOptions.Error", m_iClientId),p_objEx);
			}
			finally
			{
				arrIds=null;
				objNode=null;
			}
			return true;
		}
        //JIRA-857 Start
        private bool SaveCollOptions(ArrayList p_alSql, XmlDocument p_objDOC, string p_sTableName, string p_sTagName)
        {
            string sSQL = "";
            string[] arrIds = null;
            XmlNode objNode = null;
            string sLobCode = "";
            try
            {
                objNode = p_objDOC.SelectSingleNode(".//LineOfBusiness");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                        sLobCode = objNode.InnerText;
                }
                //objNode = p_objDOC.SelectSingleNode(".//DupNoOfDays");
                sSQL = "DELETE FROM " + p_sTableName + " WHERE LINE_OF_BUS_CODE=" + sLobCode;
                p_alSql.Add(sSQL);
                objNode = p_objDOC.SelectSingleNode("//" + p_sTagName);
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (!objNode.InnerText.Trim().Equals(""))
                        {
                            arrIds = objNode.InnerText.Split(','.ToString().ToCharArray());
                            foreach (string sTemp in arrIds)
                            {
                                if (!sTemp.Trim().Equals(""))
                                {
                                    sSQL = "INSERT INTO " + p_sTableName + " VALUES('" + sTemp + "','" + sLobCode + "')";
                                    p_alSql.Add(sSQL);
                                }
                            }
                        }
                    }
                }

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LOCParms.SaveAutoOptions.Error", m_iClientId), p_objEx); //add in DB-Global_Resources according to new function
            }
            finally
            {
                arrIds = null;
                objNode = null;
            }
            return true;
        }
        //JIRA-857 Ends
        /// <summary>
        /// Author - Sumit Kumar
        /// Dated - 09/21/2009
        /// Purpose - This function checks the Enhanced Policy Flags in SYS_PARMS_LOB table
        /// MITS # - 18227
        /// </summary>
        private bool IsActCodeRequired()
        {
            string sSQL = "";
            bool bIsActCodeReqd = false;

            try
            {
                sSQL = "SELECT COUNT(USE_ENH_POL_FLAG) FROM SYS_PARMS_LOB WHERE USE_ENH_POL_FLAG = -1 AND LINE_OF_BUS_CODE <> 844";
                bIsActCodeReqd = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(m_sDSN, sSQL), m_iClientId) > 0 ? true : false;
                return bIsActCodeReqd;    
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("LOCParms.ActivationCodeRequired.Error", m_iClientId), p_objEx);
            }
        }
		#endregion
	}
}
