using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   21 Mar 2005
	///Purpose :   This class Creates, Edits, Fetches Billing Rules List.
	/// </summary>
	public class BillingRuleList
	{
		private string m_sDSN="";
        int m_iClientId = 0;
		/// Name		: BillingRuleList
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor with Connection String
		/// </summary>
		/// <param name="p_sConnString">Connection String with Connection Details</param>
		public BillingRuleList(string p_sConnString, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sDSN = p_sConnString;
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the details of Billing Rules List
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with xml Structure</param>
		/// <returns>Returns the data with the xml structure</returns>
		public XmlDocument Get()
		{
			LocalCache objCache=null;
			string sSQL="";
			DbReader objRead=null; 
			XmlElement objLstRow=null;
			XmlDocument objDOC=null;
			XmlElement objElement=null;
			
			try
			{
				objCache = new LocalCache(m_sDSN,m_iClientId);
				sSQL="SELECT BILLING_RULE_ROWID,BILLING_RULE_CODE FROM SYS_BILL_BLNG_RULE WHERE IN_USE_FLAG <>0";
				objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
				objDOC=new XmlDocument();
				objElement=objDOC.CreateElement("Codes");
				while (objRead.Read())
				{
					objLstRow = objDOC.CreateElement("Code");
					objLstRow.SetAttribute("id",Conversion.ConvertObjToStr(objRead.GetValue("BILLING_RULE_ROWID")));
					objLstRow.SetAttribute("shortcode",objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("BILLING_RULE_CODE")))));
					objLstRow.InnerText=objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("BILLING_RULE_CODE"))));
					objElement.AppendChild(objLstRow);
				}
                objDOC.AppendChild(objElement);
				if (objCache!=null)
					objCache.Dispose();
				if (objRead!=null)
					objRead.Close();
				return objDOC; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRuleList.Get.Err", m_iClientId), p_objEx); 
			}
			finally
			{

                if (objCache != null)
                    objCache.Dispose();
				if(objRead!=null)
			    	objRead.Dispose(); 
				objLstRow=null;
				objDOC=null;
				objElement=null;
			}
		}
	}
}