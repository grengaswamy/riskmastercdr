using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Collections.Generic;
using Riskmaster.Security;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Edits, Fetches Logged in user session information.
	/// </summary>
	public class LoggedInUserList
	{
		#region Input/Output Xml
		/*
			<Sessions>
				<option email="" name="c sc (csc)" database="BROADSPIRE" phone="(   )    -    " datetime="5/5/2005 5:21 PM">11e3bba7-f659-4660-8fb4-e4f9a369e26c</option>
			</Sessions>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// Security DB Connection string
		/// </summary>
		private string m_sSecDSN="";

        private int m_iClientId = 0;

		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		/// <summary>
		/// Security DB Connection string
		/// </summary>
		public string SecDSN
		{
			set
			{
				m_sSecDSN=value;
			}	

		}
		#endregion

		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public LoggedInUserList(){}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
		/// <param name="p_sSecureDSN">Security Connection string</param>
		public LoggedInUserList(string p_sDSN,string p_sSecureDSN, int p_iClientId)
		{
			m_sDSN=p_sDSN;
			m_sSecDSN=p_sSecureDSN;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Private Function
		/// <summary>		
		/// This method will return a valid date and time in the specified format		
		/// </summary>		
		/// <param name="p_sDataValue">A valid date time string that will be formatted</param>	
		/// <param name="p_sDateFormat">The format in which the date part will be formatted</param>	
		/// <param name="p_sTimeFormat">The format in which the time part will be formatted</param>	
		/// <returns>It returns date time string the specified format</returns>
		private string GetDBDTTMFormat(string p_sDataValue,string p_sDateFormat,string p_sTimeFormat)
		{
			string sReturnString="";
			string sDatePart="";
			string sTimePart="";
			string sTempDataValue="";
			DateTime datDataValue=DateTime.MinValue;
			try
			{
				if ((p_sDataValue == null) || (p_sDataValue.Trim() == ""))
					return "";
				else if(p_sDataValue.Trim() != "")
				{
					sTempDataValue = p_sDataValue;
					sTempDataValue = sTempDataValue.Substring(4,2) + "-" + sTempDataValue.Substring(6,2) + "-" + sTempDataValue.Substring(0,4);
					datDataValue = System.DateTime.Parse(sTempDataValue);
					sDatePart= datDataValue.ToString(p_sDateFormat);

					sTempDataValue = p_sDataValue;
					sTempDataValue = sTempDataValue.Substring(8,2) + ":" + sTempDataValue.Substring(10,2) + ":" + "00";
					datDataValue = System.DateTime.Parse(sTempDataValue);
					sTimePart= datDataValue.ToString(p_sTimeFormat);					
					sReturnString = sDatePart+ " " + sTimePart;
				}						
			}			
			catch{return "";}
			return sReturnString;
		}
		#endregion
		
		#region Public Function
		/// Name		: DeleteSession
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes Session information
		/// </summary>
		/// <param name="p_objDOC">Xml containing sessionid to be deleted</param>
		/// <returns>True/False</returns>
		public bool DeleteSession(XmlDocument p_objDOC)
		{
			bool bReturn=false;
            string sSession = string.Empty;
			XmlNode objNode=null;
			try
			{
				objNode=p_objDOC.SelectSingleNode("//Sessions");
				if (objNode!=null)
				{
					if (objNode.InnerText !=null)
					{
						sSession=objNode.InnerText;
                        RMSessionManager.RemoveSession(m_sDSN, sSession, string.Empty);
						bReturn=true;
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LoggedInUserList.DeleteSession.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
			}
			return bReturn;
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(string p_sSessionObjUser)
		{
			UserLogin objLogin=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			XmlDocument objDOM=null;
			try
			{
				objDOM=new XmlDocument();
                List<UserSession> arrUserSessions = RMSessionManager.GetAllSessions(m_sDSN);
		
				objElemParent = objDOM.CreateElement("Sessions");
				objDOM.AppendChild(objElemParent);

                foreach (UserSession objSession in arrUserSessions)
                {
					objLogin = (UserLogin) Utilities.BinaryDeserialize(objSession.BinaryItems["OBJ_USER"].BinaryValue);
					objElemTemp=objDOM.CreateElement("option");
					objElemTemp.SetAttribute("email",objLogin.objUser.Email);
                    objElemTemp.SetAttribute("name", String.Format("{0} {1} ({2})", objLogin.objUser.FirstName, objLogin.objUser.LastName, objLogin.LoginName));
					objElemTemp.SetAttribute("database",objLogin.objRiskmasterDatabase.DataSourceName);
					objElemTemp.SetAttribute("phone",objLogin.objUser.OfficePhone);
					objElemTemp.SetAttribute("datetime",GetDBDTTMFormat(objSession.LastChanged,"d","t"));
					objElemTemp.InnerText=objSession.SessionID;
					objElemParent.AppendChild(objElemTemp);
                }//foreach

				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("LoggedInUserList.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{				
				objLogin=null;
				objElemParent=null;
				objElemTemp=null;
				objDOM=null;
			}
		}
		#endregion
	}
}
