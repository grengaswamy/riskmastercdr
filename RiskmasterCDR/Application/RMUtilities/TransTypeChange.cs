using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Db;
using System.Text;
using Riskmaster.Security;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   25th,Apr 2005
	///Purpose :   Implements the Transaction Type Change Utility Form
	/// </summary>
	public class TransTypeChange
	{
		/// <summary>
		/// Connection String variable
		/// </summary>
		private string m_sConnStr="";
        /// <summary>
        /// Language Code of the user
        /// </summary>
        private int m_iLangCode = 0;  //Aman ML Change

        private int m_iClientId = 0; //psharma206 jira 109

        UserLogin m_oUserLogin = null;//vkumar258 RMA-6037

		/// Name		: TransTypeChange
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/25/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
        public TransTypeChange(string p_sConnStr, int p_iClientId) //psharma206 jira 109
		{
			m_sConnStr = p_sConnStr;
            m_iClientId = p_iClientId;//psharma206 jira 109
		}

        public TransTypeChange(UserLogin p_oUserLogin, string p_sConnStr, int p_iClientId) //vkumar258 RMA-6037
        {
            m_oUserLogin = p_oUserLogin;
            m_sConnStr = p_sConnStr;
            m_iClientId = p_iClientId;
		}
        #region Properties
        //Aman ML Change
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion
        /// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/25/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		//Input xml structure 
//		<?xml version="1.0" encoding="utf-8" ?>
//		<TransTypeChange>
//		<PaymentCtlNum>g1000</PaymentCtlNum>
//		<FundsTransSplit></FundsTransSplit>
//		</TransTypeChange>
		/// <summary>
		/// Gets the data for a particular Control Number
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml structure document</param>
		/// <param name="p_objErrs">Errors Collection</param>
		/// <returns>Xml document containing the data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDoc)
		{			
			string sControlNo="";
			string sSQL="";
			string sPayee="";
			int iTransId=0;
			int iResType=0;
			DbReader objRdr = null;
			DbReader objRdrCds = null;
			XmlElement objElm=null;
			XmlElement objNewTransDtl=null;
			XmlElement objNewCod=null;
            StringBuilder sbSQL = null;
            //Aman MITS 31747 --start
            int iUserLangCode = 0;
            int iBaseLangCode = 0;
            bool isBaseLangCode = false; 
			try
			{
                //Aman MITS 31747
                iUserLangCode = this.LanguageCode;
                iBaseLangCode = Convert.ToInt32(RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").ToString().Split('|')[0]);

                if (int.Equals(iUserLangCode, 0) || int.Equals(iBaseLangCode, iUserLangCode))
                    isBaseLangCode = true;
                //Aman MITS 31747

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//PaymentCtlNum");
				if(objElm!=null)
					sControlNo = objElm.InnerText.Trim();
				if (sControlNo.Equals(""))
					return p_objXmlDoc;
				sSQL = "SELECT * FROM FUNDS WHERE CTL_NUMBER='" + sControlNo.Replace("'","''") + "'";
				objRdr=DbFactory.GetDbReader(m_sConnStr,sSQL);
				if(objRdr.Read()) 
				{
					sPayee = objRdr.GetString("FIRST_NAME") + " " + objRdr.GetString("LAST_NAME");
					iTransId = objRdr.GetInt32("TRANS_ID");	
				}
				else
				{
					if (objRdr!=null)
					{
						objRdr.Close();
						objRdr.Dispose(); 
					}
					return p_objXmlDoc;
				}
				if (objRdr!=null)
				{
					objRdr.Close();
					objRdr.Dispose(); 
				}
				
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FundsTransSplit");  
				objElm.SetAttribute("payee" , sPayee);
                //Aman ML Change --start
                sbSQL = new StringBuilder();
                //sbSQL.Append("SELECT FTS.SPLIT_ROW_ID,FTS.TRANS_TYPE_CODE,");
                //sbSQL.Append("C1.SHORT_CODE,CT1.CODE_DESC,C1.RELATED_CODE_ID,FTS.RESERVE_TYPE_CODE,C2.SHORT_CODE, ");
                //sbSQL.Append("CT2.CODE_DESC,FTS.AMOUNT,FTS.FROM_DATE,FTS.TO_DATE FROM FUNDS_TRANS_SPLIT FTS,CODES C1, ");
                //sbSQL.Append("CODES_TEXT CT1, CODES C2, CODES_TEXT CT2 WHERE TRANS_ID = " + iTransId +" ");
                //sbSQL.Append(" AND FTS.TRANS_TYPE_CODE = C1.CODE_ID AND FTS.RESERVE_TYPE_CODE = C2.CODE_ID AND");
                //sbSQL.Append(" C1.CODE_ID = CT1.CODE_ID AND C2.CODE_ID = CT2.CODE_ID AND CT1.LANGUAGE_CODE = 1033 ");

                //sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(), "TRANS_TYPES", this.LanguageCode);
                //sbSQL = sbSQL.Replace(" AND CODES.CODE_ID NOT IN (", " AND C1.CODE_ID NOT IN (");
                
                //objRdr = DbFactory.GetDbReader(m_sConnStr, "SELECT FTS.SPLIT_ROW_ID,FTS.TRANS_TYPE_CODE," + 
                //    "C1.SHORT_CODE,CT1.CODE_DESC,C1.RELATED_CODE_ID,FTS.RESERVE_TYPE_CODE,C2.SHORT_CODE," + 
                //    "CT2.CODE_DESC,FTS.AMOUNT,FTS.FROM_DATE,FTS.TO_DATE FROM FUNDS_TRANS_SPLIT FTS,CODES C1," + 
                //    "CODES_TEXT CT1, CODES C2, CODES_TEXT CT2 WHERE TRANS_ID = " + iTransId + 
                //    " AND FTS.TRANS_TYPE_CODE = C1.CODE_ID AND FTS.RESERVE_TYPE_CODE = C2.CODE_ID AND " + 
                //    "C1.CODE_ID = CT1.CODE_ID AND C2.CODE_ID = CT2.CODE_ID");
               
                sbSQL.Append("SELECT FTS.SPLIT_ROW_ID,FTS.TRANS_TYPE_CODE,FTS.RESERVE_TYPE_CODE,FTS.AMOUNT,FTS.FROM_DATE,FTS.TO_DATE, ");               
                sbSQL.Append("C1.SHORT_CODE, C1.RELATED_CODE_ID, CT1.CODE_DESC, ");

                if (!isBaseLangCode)
                    sbSQL.Append("U_CT1.CODE_DESC USER1_CODE_DESC, ");
                else
                    sbSQL.Append(" '' USER1_CODE_DESC,  ");

                sbSQL.Append("C2.SHORT_CODE, CT2.CODE_DESC, ");

                if (!isBaseLangCode)
                    sbSQL.Append("U_CT2.CODE_DESC USER2_CODE_DESC ");
                else
                    sbSQL.Append(" '' USER2_CODE_DESC ");

                sbSQL.Append("FROM FUNDS_TRANS_SPLIT FTS ");
                sbSQL.Append("INNER JOIN CODES C1 ON  C1.CODE_ID = FTS.TRANS_TYPE_CODE ");
                sbSQL.Append("INNER JOIN CODES_TEXT CT1 ON CT1.CODE_ID = C1.CODE_ID AND CT1.LANGUAGE_CODE = " + iBaseLangCode);
                if (!isBaseLangCode)
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT U_CT1 ON U_CT1.CODE_ID = C1.CODE_ID AND U_CT1.LANGUAGE_CODE = " + iUserLangCode);
                sbSQL.Append(" INNER JOIN CODES C2 ON C2.CODE_ID = FTS.RESERVE_TYPE_CODE ");
                sbSQL.Append("INNER JOIN CODES_TEXT CT2 ON CT2.CODE_ID = C2.CODE_ID AND CT2.LANGUAGE_CODE = " + iBaseLangCode);
                if (!isBaseLangCode)
                    sbSQL.Append(" LEFT OUTER JOIN CODES_TEXT U_CT2 ON U_CT2.CODE_ID = C2.CODE_ID AND U_CT2.LANGUAGE_CODE = " + iUserLangCode);
                sbSQL.Append(" WHERE TRANS_ID = " + iTransId +" ");


               
                objRdr = DbFactory.GetDbReader(m_sConnStr, sbSQL.ToString());                
                //Aman ML Change --end
				while(objRdr.Read())
				{
                    string sReserveDesc = string.Empty;
                    sbSQL = new StringBuilder();   //Aman ML Change
                    //iResType = objRdr.GetInt32(4);
                    iResType = objRdr.GetInt32(7); 
					objNewTransDtl = p_objXmlDoc.CreateElement("TransDetail");
					objNewTransDtl.SetAttribute("id" , objRdr.GetInt32("SPLIT_ROW_ID").ToString());
                    objNewTransDtl.SetAttribute("transcode" , objRdr.GetInt32(1).ToString()); 
					objNewTransDtl.SetAttribute("reservecode" , iResType.ToString()); 					
                    if (string.IsNullOrEmpty(Convert.ToString((objRdr.GetValue("USER2_CODE_DESC")))))
                    {
                        objNewTransDtl.SetAttribute("reservedesc", "Reserve: " + objRdr.GetString(10).ToString() + " - " +
                            objRdr.GetString(11).ToString() + ";     Amount: " +
                            Conversion.ConvertObjToStr(string.Format("{0:C}", objRdr.GetDouble(3)))); //bpaskova MITS 37587
                    }
                    else
                    {
                        objNewTransDtl.SetAttribute("reservedesc", "Reserve: " + objRdr.GetString(10).ToString() + " - " +
                            objRdr.GetString("USER2_CODE_DESC").ToString() + ";     Amount: " +
                            Conversion.ConvertObjToStr(string.Format("{0:C}", objRdr.GetDouble(3)))); //bpaskova MITS 37587
                    }
                    objNewTransDtl.SetAttribute("fromdt", Conversion.GetUIDate(Conversion.ConvertObjToStr(objRdr.GetValue("FROM_DATE")), m_oUserLogin.objUser.NlsCode.ToString(), m_iClientId));//vkumar258 RMA-6037
                    objNewTransDtl.SetAttribute("todt", Conversion.GetUIDate(Conversion.ConvertObjToStr(objRdr.GetValue("TO_DATE")), m_oUserLogin.objUser.NlsCode.ToString(), m_iClientId));//vkumar258 RMA-6037
                    if (string.IsNullOrEmpty(Convert.ToString((objRdr.GetValue("USER1_CODE_DESC")))))
                    {
                        objNewTransDtl.SetAttribute("title", objRdr.GetString(6) + " - " + objRdr.GetString(8));
                    }
                    else
                    {
                        objNewTransDtl.SetAttribute("title", objRdr.GetString(6) + " - " + objRdr.GetString("USER1_CODE_DESC"));
                    }
                    //Aman MITS 31747 --end
					objElm.AppendChild(objNewTransDtl);
                    //Aman ML Change --START
                    //sSQL= "SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE " + 
                    //        "CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = 1075 AND " + 
                    //        "CODES.RELATED_CODE_ID = " + iResType + " AND CODES.CODE_ID <> " + iTransId;
                    sbSQL.Append("SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT WHERE ");
                    sbSQL.Append("CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.TABLE_ID = 1075 AND ");
                    sbSQL.Append("CODES_TEXT.LANGUAGE_CODE = 1033 AND ");
                    sbSQL.Append("CODES.RELATED_CODE_ID = " + iResType);
                    sbSQL.Append(" AND CODES.CODE_ID <> " + iTransId);
                    sbSQL = CommonFunctions.PrepareMultilingualQuery(sbSQL.ToString(),"TRANS_TYPES",this.LanguageCode);
                    objRdrCds = DbFactory.GetDbReader(m_sConnStr, sbSQL.ToString());  //Aman ML Change --END
					while(objRdrCds.Read())
					{
						objNewCod = p_objXmlDoc.CreateElement("Code");
						objNewCod.SetAttribute("title" ,  objRdrCds.GetString("SHORT_CODE") + " - " + 
							objRdrCds.GetString("CODE_DESC")  );
						objNewCod.SetAttribute("id",  objRdrCds.GetInt32("CODE_ID").ToString());
						objNewTransDtl.AppendChild(objNewCod);
					}
					if (objRdrCds!=null)
					{
						objRdrCds.Close();
						objRdrCds.Dispose();  
					}
				}
				if (objRdr!=null)
				{
					objRdr.Close();
					objRdr.Dispose();
				}
				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("TransTypeChange.Get.GetErr", m_iClientId), p_objEx);//psharma206 jira 109
			}
			finally
			{
				if(objRdr != null)
				{
					objRdr.Dispose();
					objRdr = null;
				}
				if(objRdrCds!=null)
				{
					objRdrCds.Dispose();
					objRdrCds=null; 
				}
				objElm=null;
				objNewTransDtl=null;
				objNewCod=null;
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/25/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		//Input xml structure
//		<?xml version="1.0" encoding="utf-8"?>
//		<TransTypeChange>
//			<PaymentCtlNum>g1000</PaymentCtlNum>
//			<FundsTransSplit payee="">
//				<TransDetail id="5" transcode="2362" reservecode="370" reservedesc="Reserve: M - Medical     Amount: 175   From/To Dates:" fromdt="20020329" todt="20020401" title="DB - Doctor Bill">
//					<Code title="BI - Bodily Injury" id="291" />
//					<Code title="DB - Doctor Bill" id="2362" />
//					<Code title="HB - Hospital Bill" id="2363" />
//					<Code title="WO - Write Off" id="2364" />
//					<Code title="RX - RX" id="3841" />
//				</TransDetail>
//			</FundsTransSplit>
//		</TransTypeChange>
		/// <summary>
		/// Saves the changed data for a particular payment control number
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data</param>
		/// <returns>Saved xml document</returns>
		public XmlDocument Save(XmlDocument p_objXmlDoc)
		{
			int iSplitRowId=0;
			int iTransTypeCode=0;
			string sFromDt="";
			string sToDt="";
			string sSQL="";
			DbConnection objConn = null;
			XmlNodeList objNodLst=null;
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnStr);
				objConn.Open();
				objNodLst = p_objXmlDoc.GetElementsByTagName("TransDetail");
				foreach(XmlElement objNod in objNodLst)
				{
					iSplitRowId = Conversion.ConvertStrToInteger(objNod.GetAttribute("id").ToString()) ;
					sFromDt  = objNod.GetAttribute("fromdt");
                    sToDt  = objNod.GetAttribute("todt");
					iTransTypeCode = Conversion.ConvertStrToInteger(objNod.GetAttribute("transcode").ToString());
					if((iSplitRowId!=0)&&(iTransTypeCode!=0))
					{
						if (!sFromDt.Trim().Equals(""))
						{
							sFromDt=Conversion.GetDate(sFromDt);
						}
						if (!sToDt.Trim().Equals(""))
						{
							sToDt=Conversion.GetDate(sToDt);
						}
						sSQL="UPDATE FUNDS_TRANS_SPLIT SET TRANS_TYPE_CODE = " + iTransTypeCode.ToString() 
							+", FROM_DATE='" + sFromDt + "', TO_DATE='" + sToDt + "'  WHERE SPLIT_ROW_ID = " + iSplitRowId; 
						objConn.ExecuteNonQuery(sSQL);
					}
				}
				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("TransTypeChange.Save.SaveErr", m_iClientId), p_objEx);//psharma206 jira 109
			}
			finally
			{
				if(objConn!=null) 
				{
					objConn.Dispose(); 
					objConn = null;
				}
				objNodLst=null;
			}
		}
	}
}