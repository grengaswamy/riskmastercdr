﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Settings;
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
    public class PurgeHistorySetUp
    {
        private string m_sConnectionString = "";
        private string m_sUserName = "";
        private int m_iClientId = 0;//psharma206 jira 103
        UserLogin m_oUserLogin = null;

        public PurgeHistorySetUp(string p_sConnectionstring, string p_sLoginName, int p_iClientId)//psharma206 jira 103 
        {
            m_sConnectionString = p_sConnectionstring;
            m_sUserName = p_sLoginName;
            m_iClientId = p_iClientId;//psharma206 jira 103
        }

        public PurgeHistorySetUp(UserLogin p_oUserLogin, string p_sConnectionstring, string p_sLoginName, int p_iClientId)//vkumar258 ML changes 
        {
            m_oUserLogin = p_oUserLogin;
            m_sConnectionString = p_sConnectionstring;
            m_sUserName = p_sLoginName;
            m_iClientId = p_iClientId;//psharma206 jira 103
        }

        /// <summary>
        /// This function will retrieve the records from HIST_TRACK_PURGE
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <returns></returns>
        public bool GetPurgeEntries(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut)
        {

            XmlNode objXmlNode = null;
            XmlNode objXmlNodeChild = null;
            XmlAttribute objXmlAttr = null;

            string sSQL = string.Empty;
            int iRecordCount = 0;
            int iTableId = 0;            
            
            try
            {
                p_objXmlOut = p_objXmlIn;
                objXmlNode = p_objXmlOut.CreateElement("PurgeHistoryGrid");
                p_objXmlOut.SelectSingleNode("/Document/Form").AppendChild(objXmlNode);

                objXmlNode = p_objXmlOut.CreateElement("listhead");
                objXmlNodeChild = p_objXmlOut.CreateElement("TableName");
                objXmlNodeChild.InnerText = "Table Name";
                objXmlNode.AppendChild(objXmlNodeChild);
                objXmlNodeChild = p_objXmlOut.CreateElement("FromDate");
                objXmlNodeChild.InnerText = "From Date";
                objXmlNode.AppendChild(objXmlNodeChild);
                objXmlNodeChild = p_objXmlOut.CreateElement("ToDate");
                objXmlNodeChild.InnerText = "To Date";
                objXmlNode.AppendChild(objXmlNodeChild);

                //p_objXmlOut.FirstChild.AppendChild(objXmlNode);
                p_objXmlOut.SelectSingleNode("/Document/Form/PurgeHistoryGrid").AppendChild(objXmlNode);

                sSQL = @"SELECT HP.RECORD_ID,HP.TABLE_ID,HT.USER_PROMPT,HP.FROM_DATE,HP.TO_DATE FROM HIST_TRACK_PURGE HP 
                        LEFT OUTER JOIN HIST_TRACK_TABLES HT ON HP.TABLE_ID=HT.TABLE_ID WHERE HP.DELETED_FLAG <> -1 ORDER 
                        BY HT.USER_PROMPT";
                using (DbReader objDBReader = DbFactory.GetDbReader(m_sConnectionString, sSQL))
                {
                    while (objDBReader.Read())
                    {
                        iRecordCount++;
                        objXmlNode = p_objXmlOut.CreateElement("option");
                        objXmlAttr = p_objXmlOut.CreateAttribute("ref");
                        objXmlAttr.InnerText = "/Document/Form/PurgeHistoryGrid/option[" + iRecordCount + "]";
                        objXmlNode.Attributes.Append(objXmlAttr);

                        objXmlNodeChild = p_objXmlOut.CreateElement("TableId");
                        iTableId = objDBReader.GetInt("TABLE_ID");
                        objXmlNodeChild.InnerText = iTableId.ToString();
                        objXmlNode.AppendChild(objXmlNodeChild);

                        objXmlNodeChild = p_objXmlOut.CreateElement("TableName");
                        if (iTableId == -1)
                        {
                            objXmlNodeChild.InnerText = "<ALL>";
                        }
                        else
                        {
                            objXmlNodeChild.InnerText = objDBReader.GetString("USER_PROMPT");
                        }
                        objXmlNode.AppendChild(objXmlNodeChild);

                        objXmlNodeChild = p_objXmlOut.CreateElement("FromDate");
                        objXmlNodeChild.InnerText = Conversion.GetUIDate(objDBReader.GetString("FROM_DATE"), m_oUserLogin.objUser.NlsCode.ToString(),m_iClientId);
                        objXmlNode.AppendChild(objXmlNodeChild);

                        objXmlNodeChild = p_objXmlOut.CreateElement("ToDate");
                        objXmlNodeChild.InnerText = Conversion.GetUIDate(objDBReader.GetString("TO_DATE"), m_oUserLogin.objUser.NlsCode.ToString(), m_iClientId);
                        objXmlNode.AppendChild(objXmlNodeChild);

                        objXmlNodeChild = p_objXmlOut.CreateElement("RecordId");
                        objXmlNodeChild.InnerText = objDBReader.GetInt("RECORD_ID").ToString();
                        objXmlNode.AppendChild(objXmlNodeChild);

                        p_objXmlOut.SelectSingleNode("/Document/Form/PurgeHistoryGrid").AppendChild(objXmlNode);
                    }
                }               

                iRecordCount++;
                objXmlNode = p_objXmlOut.CreateElement("option");
                objXmlAttr = p_objXmlOut.CreateAttribute("ref");
                objXmlAttr.InnerText = "/Document/Form/PurgeHistoryGrid/option[" + iRecordCount + "]";
                objXmlNode.Attributes.Append(objXmlAttr);
                objXmlAttr = p_objXmlOut.CreateAttribute("type");
                objXmlAttr.InnerText = "new";
                objXmlNode.Attributes.Append(objXmlAttr);

                objXmlNodeChild = p_objXmlOut.CreateElement("TableName");
                objXmlNode.AppendChild(objXmlNodeChild);

                objXmlNodeChild = p_objXmlOut.CreateElement("FromDate");
                objXmlNode.AppendChild(objXmlNodeChild);

                objXmlNodeChild = p_objXmlOut.CreateElement("ToDate");
                objXmlNode.AppendChild(objXmlNodeChild);

                objXmlNodeChild = p_objXmlOut.CreateElement("TableId");
                objXmlNode.AppendChild(objXmlNodeChild);

                objXmlNodeChild = p_objXmlOut.CreateElement("RecordId");
                objXmlNode.AppendChild(objXmlNodeChild);

                p_objXmlOut.SelectSingleNode("/Document/Form/PurgeHistoryGrid").AppendChild(objXmlNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PurgeHistorySetUp.GetPurgeEntries.Error", m_iClientId), p_objEx);//psharma206 jira 103
            }            
            return true;
        }

        /// <summary>
        /// This function will bring the list of tables from hist_track_tables
        /// This list will be used in populating combo box while adding/editing record
        /// </summary>
        /// <param name="p_objXmlOut"></param>
        public bool GetTableList(ref XmlDocument p_objXmlOut)
        {
            XmlNode objRootNode = null;
            XmlNode objNode = null;
            XmlAttribute objAtr = null;
            string sSql = string.Empty;
            LocalCache objLocalCache = null;           
            string sTableName = string.Empty;

            try
            {
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
              
                objRootNode = p_objXmlOut.CreateElement("TableList");

                objNode = p_objXmlOut.CreateElement("Table");

                objAtr = p_objXmlOut.CreateAttribute("Id");
                objAtr.InnerText = "0";
                objNode.Attributes.Append(objAtr);

                objAtr = p_objXmlOut.CreateAttribute("Name");
                objAtr.InnerText = "";
                objNode.Attributes.Append(objAtr);

                objRootNode.AppendChild(objNode);


                objNode = p_objXmlOut.CreateElement("Table");

                objAtr = p_objXmlOut.CreateAttribute("Id");
                objAtr.InnerText = "-1";
                objNode.Attributes.Append(objAtr);

                objAtr = p_objXmlOut.CreateAttribute("Name");
                objAtr.InnerText = "<ALL>";
                objNode.Attributes.Append(objAtr);

                objRootNode.AppendChild(objNode);
                //MGaba2:Purge history will list only those Tables on which we have either enabled history tracking or for which audit table is already created
                sSql = "SELECT TABLE_ID,TABLE_NAME,USER_PROMPT FROM HIST_TRACK_TABLES WHERE CREATED_TABLE_FLAG = -1 ORDER BY USER_PROMPT";
                using (DbReader objDR = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    while (objDR.Read())
                    {
                        sTableName = objDR.GetString("TABLE_NAME");                        

                        objNode = p_objXmlOut.CreateElement("Table");

                        objAtr = p_objXmlOut.CreateAttribute("Id");
                        objAtr.InnerText = objDR.GetInt("TABLE_ID").ToString();
                        objNode.Attributes.Append(objAtr);

                        objAtr = p_objXmlOut.CreateAttribute("Name");
                        objAtr.InnerText = objDR.GetString("USER_PROMPT");
                        objNode.Attributes.Append(objAtr);

                        objRootNode.AppendChild(objNode);
                    }
                }
                p_objXmlOut.AppendChild(objRootNode);

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PurgeHistorySetUp.GetTableList.Error", m_iClientId), p_objEx);//psharma206 jira 103
            }
            finally
            {              
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }

            }
            return true;
        }

        /// <summary>
        /// This function will save the record in hist_track_purge when save button
        /// on popup is clicked
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <returns></returns>
        public bool Save(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut)
        {
            DbConnection oConnection=null ;
            string sSql = string.Empty;
            XmlNode objNode = null;
            int iTableId=0;
            string sFromDate=string.Empty ;
            string sToDate=string.Empty ;

            try
            {
                objNode = p_objXmlIn.SelectSingleNode("//TableId");
                if (objNode != null )
                {
                    iTableId= Convert.ToInt32(objNode.InnerText);
                }
                objNode = p_objXmlIn.SelectSingleNode("//FromDate");
                if (objNode != null )
                {
                    sFromDate = Conversion.GetDate(objNode.InnerText);
                }
                objNode = p_objXmlIn.SelectSingleNode("//ToDate");
                if (objNode != null )
                {
                    sToDate = Conversion.GetDate(objNode.InnerText);                    
                }

                objNode = p_objXmlIn.SelectSingleNode("//RecordId");
                if (objNode != null )
                {
                    oConnection = DbFactory.GetDbConnection(m_sConnectionString);

                    if (objNode.InnerText != "")
                    {
                        //update
                        sSql =string.Format(@"UPDATE HIST_TRACK_PURGE SET TABLE_ID ={0}, FROM_DATE='{1}', TO_DATE='{2}', 
                                              UPDATED_BY_USER='{3}', DTTM_RCD_LAST_UPD='{4}' WHERE RECORD_ID ={5}",
                                              iTableId,sFromDate,sToDate,m_sUserName,System.DateTime.Now.ToString("yyyyMMddHHmmss"),
                                              objNode.InnerText);
                    }
                    else
                    {
                        //insert
                        if (oConnection.DatabaseType == eDatabaseType.DBMS_IS_SQLSRVR)
                        {
                            sSql = string.Format(@"INSERT INTO HIST_TRACK_PURGE(TABLE_ID,FROM_DATE,TO_DATE,DELETED_FLAG,ADDED_BY_USER,
                                                 DTTM_RCD_ADDED) VALUES({0},'{1}','{2}',0,'{3}','{4}')", iTableId, sFromDate, sToDate,
                                                 m_sUserName, System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                        }
                        else
                        {//We have sequence in Oracle,so need to update the value of record it by using nextval
                            sSql = string.Format(@"INSERT INTO HIST_TRACK_PURGE(RECORD_ID,TABLE_ID,FROM_DATE,TO_DATE,DELETED_FLAG,
                                                 ADDED_BY_USER,DTTM_RCD_ADDED) VALUES(HIST_TRACK_PURGE_SEQ.NEXTVAL,{0},'{1}','{2}',0,'{3}','{4}')",                                                                                                                    
                                                 iTableId, sFromDate, sToDate, m_sUserName, System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                        }


                    }
                    using (oConnection)
                    {
                        oConnection.Open();
                        oConnection.ExecuteNonQuery(sSql);
                        oConnection.Close();
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PurgeHistorySetUp.Save.Error", m_iClientId), p_objEx);//psharma206 jira 103
            }
            finally
            {
                if (oConnection != null)
                {
                    oConnection.Dispose();
                    oConnection = null;
                }
            }
            return true;
        }

        /// <summary>
        /// This function will update the deleted_flag in hist_track_purge if record is deleted from the grid
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <returns></returns>
        public bool Delete(XmlDocument p_objXmlIn, ref XmlDocument p_objXmlOut)
        {
            string sSql = string.Empty;
            XmlNode objNode = null;

            try
            {
                objNode = p_objXmlIn.SelectSingleNode("//RecordId");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText) )
                {
                    sSql =string.Format(@"UPDATE HIST_TRACK_PURGE SET DELETED_FLAG=-1,UPDATED_BY_USER='{0}',DTTM_RCD_LAST_UPD='{1}'
                                        WHERE RECORD_ID={2}", m_sUserName, System.DateTime.Now.ToString("yyyyMMddHHmmss"), 
                                        objNode.InnerText);
                    using (DbConnection oConnection = DbFactory.GetDbConnection(m_sConnectionString))
                    {
                        oConnection.Open();
                        oConnection.ExecuteNonQuery(sSql);
                        oConnection.Close();
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PurgeHistorySetUp.Delete.Error", m_iClientId), p_objEx);//psharma206 jira 103
            }
            return true;
        }
    }

}
