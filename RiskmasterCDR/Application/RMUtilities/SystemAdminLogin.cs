using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   15 April 2005
	///Purpose :   This class is used to get Users DBA loginname & password
	/// </summary>	
	public class SystemAdminLogin
	{
        private string m_sConnString = string.Empty;
        private int m_iClientId = 0;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public SystemAdminLogin(string p_sConnString, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sConnString = p_sConnString;
		}

		/// <summary>
		/// Gets the details of SystemAdminLogin
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with xml Structure</param>
		/// <returns>Returns the data with the xml structure</returns>
		public XmlDocument Get(XmlDocument p_objXMLDocument)
		{
            XmlDocument objXml = null;               //Umesh
            XmlNode objViewEnhanceBES = null; //Umesh
           
			if(DbFactory.IsOracleDatabase(m_sConnString))
			{
				((XmlElement)p_objXMLDocument.SelectSingleNode("//form[@name='SystemAdminLogin']")).SetAttribute("title","DBA Login");
				((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='DescLabe1']")).SetAttribute("title","Please login to an Oracle account with DBA privileges. This account information will only be used to create a new user account for the purpose of Business Entity Security.");				
                // REM Umesh 04/04/2007
            objXml = new XmlDocument();
            string strContent = RMSessionManager.GetCustomContent(m_iClientId);
            if (! string.IsNullOrEmpty(strContent))
            {
                objXml.LoadXml(strContent);
                objViewEnhanceBES= objXml.SelectSingleNode("//SpecialSettings/EnhanceBES");
                if (objViewEnhanceBES != null)
                {
                    if (objViewEnhanceBES.InnerText == "-1")
                    {
                        ((XmlElement)p_objXMLDocument.SelectSingleNode("//Document/ViewEnhanceBES")).InnerText = "yes";
                    }
                }
            }
            // REM End
			}
                //Nadim Title changed on admin screen
            else if (DbFactory.GetDatabaseType(m_sConnString) == eDatabaseType.DBMS_IS_SQLSRVR)
			{
				((XmlElement)p_objXMLDocument.SelectSingleNode("//form[@name='SystemAdminLogin']")).SetAttribute("title", "System Admin. Required");
                ((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='DescLabe1']")).SetAttribute("title", "A database user with server role(sysadmin/securityadmin) and database owner privileges on the context RISKMASTER database should be entered here.");
			}

			return p_objXMLDocument;
			
		}

		/// <summary>
		/// Returns the new connection string with DBA role.
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>connection String</returns>
		public string GetNewConnString(XmlDocument p_objXMLDocument)
		{
			int iPos = 0;
			string sUserId = "";
			string sPWD = "";
			
			try
			{
				sUserId =((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='AdminUserID']")).InnerText;
				sPWD =((XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='AdminPwd']")).InnerText;

				if(sUserId.Trim() != "" && sPWD.Trim() != "")
				{
					UTILITY.GetUIdPwdFromConString(m_sConnString,ref sUserId,ref sPWD);
					iPos = m_sConnString.IndexOf("UID=");
					return m_sConnString.Substring(0,iPos + 3) + sUserId + ";" + "PWD=" + sPWD + ";"; 
				}
				else
					return "";
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SystemAdminLogin.GetNewConnString.Err", m_iClientId), p_objEx); 
			}
		}
	}
}
