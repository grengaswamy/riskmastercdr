using System;
using System.Xml; 
using Riskmaster.Db;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 
using Riskmaster.Security;
using System.Text;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21 Mar 2005
	///Purpose :   This class Creates, Edits, Fetches Selected Discount List.
	/// </summary>
	public class SelectedDisc:UtilitiesUIBase
	{
		private string m_sUserName = "";
		
		/// <summary>
		/// String array for database fields mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "SEL_DISC_ROWID"},
			{"DiscountName", "DISCOUNT_ROWID"},
			{"AddedByUser", "ADDED_BY_USER"},
			{"DttmRcdAdded", "DTTM_RCD_ADDED"},
			{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
			{"UpdatedByUser", "UPDATED_BY_USER"},
			{"UseDiscTier", "IN_USE_FLAG"},
			{"Override", "OVERRIDE_FLAG"},
			{"LOBcode", "LINE_OF_BUSINESS"},
			{"Statecode", "STATE"},
			{"ParentLevel", "PARENT_LEVEL"},
            //Added bY Tushar MITS 18231
            {"ISRateSetup", "IS_RATE_SETUP"}
            //End
									  };

		private string m_sConnectionString = "";

		private XmlDocument m_objXMLDocument = null;
        private int m_iClientId = 0;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public SelectedDisc(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
			m_sConnectionString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// <summary>
		/// Overloaded Constructor
		/// </summary>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDatabaseName">Database Name</param>
        public SelectedDisc(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			m_sUserName = p_sLoginName;
			m_sConnectionString = p_sConnectionString;
			ConnectString = m_sConnectionString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// <summary>
		/// Initialize
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_POL_SEL_DISC";
			KeyField = "SEL_DISC_ROWID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// <summary>
		/// Gets the details of Selected Discount
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with xml Structure</param>
		/// <returns>Returns the data with the xml structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			StringBuilder sSQL = new StringBuilder() ;
			string sDiscountRowId = string.Empty  ;
			string sParentLavel = string.Empty  ;
			DbReader objDBReader = null;

			XmlElement objElement =null;
            //Tushar:MITS 18231
            string sISRateSetup = string.Empty ;
            //End

			try
			{

				XMLDoc = p_objXmlDocument;
				base.Get();

				sDiscountRowId = ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DiscountName']")).InnerText;
                sISRateSetup = ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ISRateSetup']")).InnerText;
				if(! string.IsNullOrEmpty(sDiscountRowId) || sDiscountRowId != "0")
				{
                    //Added by Tushar for Rate Setup
                    if (sISRateSetup == "1")
                    {
                        sSQL.Append("SELECT CODES_TEXT.CODE_DESC AS COVERAGE_NAME FROM SYS_POL_EXP_RATES,SYS_POL_SEL_DISC,CODES_TEXT");
                        sSQL.Append(" WHERE SYS_POL_EXP_RATES.EXP_RATE_ROWID=SYS_POL_SEL_DISC.DISCOUNT_ROWID");
                        sSQL.Append(" AND SYS_POL_EXP_RATES.EXPOSURE_ID=CODES_TEXT.CODE_ID");
                            //Anu Tennyson Changed ISRATESETUP to IS_RATE_SETUP
                        sSQL.AppendFormat(" AND SYS_POL_SEL_DISC.IS_RATE_SETUP=1 AND SYS_POL_SEL_DISC.DISCOUNT_ROWID = {0}", sDiscountRowId);
                    }
                    else
                    {
                        // Naresh Volume Discount Implementation
                        sSQL.AppendFormat("SELECT DISCOUNT_NAME,USE_VOLUME_DISCOUNT FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = {0}", sDiscountRowId);
                    }

					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL.ToString());

					if(objDBReader != null)
                        if (objDBReader.Read())
                        {
                            //Added by Tushar for Rate Setup
                            if (sISRateSetup == "1")
                            {
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DiscountName']");
                                objElement.InnerText = objDBReader["COVERAGE_NAME"].ToString();
                                objElement.SetAttribute("codeid", sDiscountRowId);
                            }
                            else
                            {
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DiscountName']");
                                objElement.InnerText = objDBReader["DISCOUNT_NAME"].ToString();
                                objElement.SetAttribute("codeid", sDiscountRowId);

                                // Naresh Volume Discount Implementation
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UseVolumeDiscount']");
                                objElement.InnerText = objDBReader["USE_VOLUME_DISCOUNT"].ToString();
                            }
                        }
                        else
                        {
                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DiscountName']");
                            objElement.InnerText = "";
                        }
					if(objDBReader != null)
						objDBReader.Close();
				}

				sParentLavel = ((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ParentLevel']")).InnerText;
				if(sParentLavel != "" || sParentLavel != "0")
				{
                    sSQL.Length = 0;
					sSQL.AppendFormat("SELECT TIER_NAME FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = {0}",sParentLavel);

					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL.ToString());

					if(objDBReader != null)
						if(objDBReader.Read())
						{
							objElement=(XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ParentLevel']");
							objElement.InnerText = objDBReader["TIER_NAME"].ToString(); 
							objElement.SetAttribute("codeid",sParentLavel);
						}
					if(objDBReader != null)
						objDBReader.Close();
				}

                // Start Naresh MITS 9559 MITS 9559 validating the selected discount is applicable for lob/state or not.

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FormTitle']");
                //Added by Tushar for Rate Setup
                if (sISRateSetup == "1")
                    objElement.InnerText = "RateSetup";
                else
	                objElement.InnerText = "SelectedDisc";

                // End Naresh MITS 9559 MITS 9559 validating the selected discount is applicable for lob/state or not.

				return p_objXmlDocument; 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SelectedDisc.Get.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				if(objDBReader != null)
					objDBReader.Dispose();
                objElement = null;
			}
		}

		/// <summary>
		/// Saves Selected Discount Data
		/// </summary>
		/// <param name="p_objXmlDocument">XML Doc containing fomr data</param>
		/// <returns>XMl Doc</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				m_objXMLDocument = p_objXmlDocument;
				if(ValidateXMLData()) 
				{
					FillXML();
					UpdateLevelAndDisountName();
					base.Save();
				}
				return XMLDoc;
			}
			catch(RMAppException p_objExp)
			{
				throw p_objExp; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("SelectedDisc.Save.Err", m_iClientId), p_objEx); 
			}
		}

		private void UpdateLevelAndDisountName()
		{
			XmlElement objElement=null;

			objElement=(XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='ParentLevel']");
			objElement.InnerText=objElement.Attributes["codeid"].Value;

			objElement=(XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='DiscountName']");
			objElement.InnerText=objElement.Attributes["codeid"].Value;
		}

		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes Selected Disc record
		/// </summary>
		/// <param name="p_iDiscountRowid">Selected Disc ID</param>
		/// <returns>True for sucess and false for failure</returns>
        /// p_iISRATESETUP added By Tushar for MITS 18231
        internal bool Delete(int p_iDiscountRowid, int p_iISRATESETUP)
        {
            StringBuilder sSQL = new StringBuilder();
            DbConnection objCon = null;

            try
            {
                if (p_iDiscountRowid != 0)
                {
                    sSQL.AppendFormat("DELETE FROM SYS_POL_SEL_DISC WHERE SEL_DISC_ROWID = {0} AND IS_RATE_SETUP = {1}",p_iDiscountRowid,p_iISRATESETUP);
                    objCon = DbFactory.GetDbConnection(m_sConnectionString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL.ToString());
                    
                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("SelectedDisc.Delete.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if(objCon != null)
                    objCon.Dispose();
            }
		}
		private void FillXML()
		{
            string sThresholdRowId = string.Empty;
			XmlNodeList objNodeList = null;
			string sNodename = string.Empty;

            // npadhy Start MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB
            string sSQL = string.Empty;
            DbReader objDbReader = null;
            string sAddedByUser = string.Empty;
            string sDateTimeAdded = string.Empty;
            // npadhy End MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB

			sThresholdRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;

            // npadhy Start MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB
            if (sThresholdRowId != "")
            {
                sSQL = "SELECT ADDED_BY_USER, DTTM_RCD_ADDED FROM SYS_POL_SEL_DISC WHERE SEL_DISC_ROWID = " + sThresholdRowId;
                objDbReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        sAddedByUser = objDbReader["ADDED_BY_USER"].ToString();
                        sDateTimeAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
                    }
                }
                objDbReader.Dispose();
            }
            // npadhy End MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
            // Now it is getting passed as the same Value as in DB

			objNodeList=m_objXMLDocument.GetElementsByTagName("control");

			foreach(XmlElement objXMLElement in objNodeList)
			{
				sNodename = objXMLElement.GetAttribute("name");  
				switch(sNodename)
				{
					case "AddedByUser":
						if(sThresholdRowId == "")
							objXMLElement.InnerText = m_sUserName;
                        // npadhy Start MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
                        else
                            objXMLElement.InnerText = sAddedByUser;
                        // npadhy End MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
						break;
					case "DttmRcdAdded":
						if(sThresholdRowId == "")
                            objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
                        // npadhy Start MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
                        else
                            objXMLElement.InnerText = sDateTimeAdded;
                        // npadhy End MITS 20714 When the Selected Discount is Updated the Date Time Added and Added by User is not passed.
                        // Now it is getting passed as the same Value as in DB
						break;
					case "DttmRcdLastUpd":
                        objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
						break;
					case "UpdatedByUser":
						objXMLElement.InnerText = m_sUserName; 
						break;
				}
			}
		}

		private bool ValidateXMLData()
		{
            string sLOBCode = string.Empty;
            string sStateCode = string.Empty;
            string sParentLevel = string.Empty;
            string sSQL = string.Empty;
            string sSelDiscRowId = string.Empty;
            string sDiscountRowId = string.Empty;
			DbReader objDBReader = null;
            string sRowID = string.Empty    ;
            string sState = string.Empty;
			bool bUse = false;
            bool blnSuccess = false;
            string sLOB = string.Empty;
			int iCount = 0;
            int iIsRateSetup = 0;
            SysSettings objSettings = null;


			try
			{
				sSelDiscRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;
				sDiscountRowId = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='DiscountName']")).Attributes["codeid"].Value;
				sLOBCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='LOBcode']")).Attributes["codeid"].Value;
				sStateCode = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='Statecode']")).Attributes["codeid"].Value;
				sParentLevel = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='ParentLevel']")).Attributes["codeid"].Value;
                iIsRateSetup = Conversion.CastToType<Int32>(((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='ISRateSetup']")).InnerText, out blnSuccess);
                objSettings = new SysSettings(m_sConnectionString, m_iClientId);

				if(sParentLevel.Trim() == "")
                    throw new RMAppException(Globalization.GetString("SelectedDisc.ValidateXMLData.ApplLevelMissing", m_iClientId));
				else
				{
					// make sure that the LOB matches
					sSQL = "SELECT * FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + sParentLevel;

					objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

					if(objDBReader.Read())
					{
						sState = objDBReader["STATE"].ToString();
						sLOB = objDBReader["LINE_OF_BUSINESS"].ToString();

						if(sLOB.Trim() != string.Empty)
						{
							if(sLOBCode.Trim() ==  string.Empty)
                                throw new RMAppException(Globalization.GetString("SelectedDisc.ValidateXMLData.IncorrectLOB", m_iClientId));
							else if(sLOBCode.Trim() != sLOB.Trim())
                                throw new RMAppException(Globalization.GetString("SelectedDisc.ValidateXMLData.InvalidLOB", m_iClientId));
						}
						if(sState.Trim() !=  string.Empty)
						{
							if(sStateCode.Trim() ==  string.Empty)
                                throw new RMAppException(Globalization.GetString("SelectedDisc.ValidateXMLData.IncorrectState", m_iClientId));
							else if(sStateCode.Trim() != sState.Trim())
                                throw new RMAppException(Globalization.GetString("SelectedDisc.ValidateXMLData.StateMatchError", m_iClientId));
						}
					}//if(objDBReader.Read())

					if(objDBReader != null)
						objDBReader.Close();

                    // npadhy Start MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
                    // Instead of hardcoding 15, we will fetch the value for DB
					//have to make sure there aren't more than 15 selected for one LOB and state at one time
					// BUT should be able to edit the current one looking at
					if(iIsRateSetup == 0 && (sSelDiscRowId == "0" || string.IsNullOrEmpty(sSelDiscRowId)))
					{
                        sSQL = "SELECT * FROM SYS_POL_SEL_DISC WHERE ( LINE_OF_BUSINESS = " + sLOBCode + " OR LINE_OF_BUSINESS = 0 ) AND IS_RATE_SETUP = 0";

						objDBReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);

						while(objDBReader.Read())
						{
							sState = objDBReader["STATE"].ToString();
							sRowID = objDBReader["DISCOUNT_ROWID"].ToString();
							bUse = Conversion.ConvertStrToBool(objDBReader["IN_USE_FLAG"].ToString());

							if(sState.Trim() == sStateCode.Trim() || sState == string.Empty)
							{
								if(bUse)
								{
									if(sRowID == sDiscountRowId)
                                        throw new RMAppException(Globalization.GetString("SelectedDisc.ValidateXMLData.AlreadyInUse", m_iClientId));	
									iCount++;
								}
							}
						}//while(objDBReader.Read())
					
					
						if(iCount >= objSettings.MaxDiscounts)
                            throw new RMAppException(string.Format(Globalization.GetString("SelectedDisc.ValidateXMLData.DiscLimitCrossed", m_iClientId), objSettings.MaxDiscounts));
                        // npadhy End MITS 21557 10/05/2010 Added Settings for Maximum Tiers and Selected Discounts
					}	
				}
				return true;
			}
			finally
			{
				if(objDBReader != null)
					objDBReader.Dispose();

                if (objSettings != null)
                    objSettings = null;
			}
		}
	}

	/// <summary>
	/// Summary description for SelectedDiscount.
	/// </summary>
	public class SelectedDiscount
	{
		public SelectedDiscount()
		{
		}

		/// <summary>
		/// Stores the SelDiscRowId value
		/// </summary>
		private int m_iSelDiscRowId = 0;

		/// <summary>
		/// Stores the Use value
		/// </summary>
		private bool m_bUse = false;

		/// <summary>
		/// Stores the Override value
		/// </summary>
		private bool m_bOverride = false;

		/// <summary>
		/// Stores the DiscountRowid value
		/// </summary>
		private int m_iDiscountRowid = 0;

		/// <summary>
		/// Stores the LOB value
		/// </summary>
		private int m_iLOB = 0;

		/// <summary>
		/// Stores the State value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the ParentLevel value
		/// </summary>
		private string m_sParentLevel = string.Empty;

		/// <summary>
		/// Stores the EffectiveDate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the ExpirationDate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;
		
		/// <summary>
		/// Read/Write property for SelDiscRowId
		/// </summary>
		internal int SelDiscRowId
		{
			get
			{
				return m_iSelDiscRowId;
			}
			set
			{
				m_iSelDiscRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for Use
		/// </summary>
		internal bool Use
		{
			get
			{
				return m_bUse;
			}
			set
			{
				m_bUse = value;
			}
		}

		/// <summary>
		/// Read/Write property for Override
		/// </summary>
		internal bool Override
		{
			get
			{
				return m_bOverride;
			}
			set
			{
				m_bOverride = value;
			}
		}

		/// <summary>
		/// Read/Write property for DiscountRowid
		/// </summary>
		internal int DiscountRowid
		{
			get
			{
				return m_iDiscountRowid;
			}
			set
			{
				m_iDiscountRowid = value;
			}
		}

        /// <summary>
        /// Read/Write property for ISrateSetup
        /// Added By Tushar for MITS 18231
        /// </summary>
        internal bool ISRateSetUp
        {
            get;
            set;
        }


		/// <summary>
		/// Read/Write property for LOB
		/// </summary>
		internal int LOB
		{
			get
			{
				return m_iLOB;
			}
			set
			{
				m_iLOB = value;
			}
		}

		/// <summary>
		/// Read/Write property for State
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for ParentLevel
		/// </summary>
		internal string ParentLevel
		{
			get
			{
				return m_sParentLevel;
			}
			set
			{
				m_sParentLevel = value;
			}
		}

		/// <summary>
		/// Read/Write property for EffectiveDate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpirationDate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
	}
}
