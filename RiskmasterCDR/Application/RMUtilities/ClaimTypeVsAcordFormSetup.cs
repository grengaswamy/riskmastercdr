using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Claim type and Acord form mapping.
	/// </summary>
	public class ClaimTypeVsAcordFormSetup
	{
		#region Input/Output Xml
		/*
		 <ClaimTypeVsAcordFormSetup>
			<control name="ACORDForm" type="combobox" title="ACORD Form">
				<option value="">
				</option>
				<option value="9">Automobile Loss Notice</option>
				<option value="10">General Liability Notice of Occurrence/Claim</option>
				<option value="8">Property Loss Notice</option>
			</control>
			<control name="ClaimType" type="combobox" title="Claim Type">
				<option value="">
				</option>
				<option value="55">BI - Bodily Injury</option>
			</control>
			<control name="AcordClaimTypeList" type="radiolist" radio="0">
				<listhead>
				<rowhead colname="AcordFormCol">ACORD Form</rowhead>
				<rowhead colname="ClaimTypeCol">Claim Type</rowhead>
				</listhead>
				<listrow>
				<rowtext type="label" name="AcordValue" title="Property Loss Notice" />
				<rowtext type="label" name="ClaimValue" title="BI - Bodily Injury" />
				</listrow>
			</control>
		</ClaimTypeVsAcordFormSetup>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// DSN id
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// Login id
		/// </summary>
		private string m_sLoginId="";
        /// <summary>
        /// Language Code
        /// </summary>
        private int m_iLangCode = 0;

        private int m_iClientId = 0;
		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		/// <summary>
		/// User id
		/// </summary>
		public string LoginId
		{
			set
			{
				m_sLoginId=value;
			}	

		}
        
        /// <summary>
        /// Aman ML Change
        /// Language Code
        /// </summary>
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }           
        }
		#endregion

		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public ClaimTypeVsAcordFormSetup(){}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sDSN">DSN name</param>
		/// <param name="p_sLoginId">Login id</param>
		public ClaimTypeVsAcordFormSetup(string p_sDSN,string p_sLoginId, int p_iClientId)
		{
			m_sDSN=p_sDSN;
			m_sLoginId=p_sLoginId;
            m_iClientId = p_iClientId;
		}
		#endregion 

		#region Private Functions
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//control[@name='"+p_sValue+"']");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}
		#endregion

		#region Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get()
		{
			XmlElement objElement=null;
			XmlNode objNode=null;
			DbReader objRead=null;
			XmlElement objRowTxt=null;
			XmlElement objLstRow=null;
			XmlDocument objXmlDocument=null;
			string sClaimType="";
			LocalCache objCache=null;
			StringBuilder sbSql=null;
			StringBuilder sbTemp=null;
			try
			{
				objXmlDocument=new XmlDocument();
				sbTemp=new StringBuilder();
				sbTemp.Append("<ClaimTypeVsAcordFormSetup>");
				sbTemp.Append("<control name='ACORDForm' type='combobox' title='ACORD Form'> </control>");
				sbTemp.Append("<control name='ClaimType' type='combobox' title='Claim Type'> </control>");
				sbTemp.Append("<control name='AcordClaimTypeList' type='radiolist' radio='0'> </control>");
				sbTemp.Append("</ClaimTypeVsAcordFormSetup>");
				objXmlDocument.LoadXml(sbTemp.ToString());
				objNode=objXmlDocument.SelectSingleNode("//control[@name='ACORDForm']"); 
				objElement=objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				objNode.AppendChild(objElement);
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT FORM_TITLE,FORM_ID,PRIMARY_FORM_FLAG ");
				sbSql.Append(" FROM CL_FORMS, STATES");
				sbSql.Append(" WHERE CL_FORMS.STATE_ROW_ID = STATES.STATE_ROW_ID");
				sbSql.Append(" AND STATES.STATE_ID = 'AC'");
				sbSql.Append(" AND PRIMARY_FORM_FLAG = -1");
				sbSql.Append(" ORDER BY FORM_TITLE");
				objRead=DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("FORM_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("FORM_TITLE"));
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objNode=objXmlDocument.SelectSingleNode("//control[@name='ClaimType']"); 
				objElement=objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				objNode.AppendChild(objElement);
                objCache = new LocalCache(m_sDSN, m_iClientId);
				sClaimType=objCache.GetTableId("CLAIM_TYPE").ToString();
				objCache.Dispose();
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT CODES.*, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
				sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");                
				sbSql.Append(" AND CODES.TABLE_ID ="+sClaimType);
                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
                sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "CLAIM_TYPE", this.LanguageCode);  //Aman ML Change
				objRead=DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")).Trim()+" - "
						+Conversion.ConvertObjToStr(objRead.GetValue("CODE_DESC")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objElement = (XmlElement)objXmlDocument.SelectSingleNode("//control[@name='AcordClaimTypeList']");  
				
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT CL_FORMS.FORM_TITLE , CODES.SHORT_CODE, CODES_TEXT.CODE_DESC,CLAIM_ACCORD_MAPPI.ACORD_FORM_ID,CLAIM_ACCORD_MAPPI.CLAIM_TYPE_CODE ");
				sbSql.Append(" FROM CLAIM_ACCORD_MAPPI, CL_FORMS, CODES, CODES_TEXT");
				sbSql.Append(" WHERE CLAIM_ACCORD_MAPPI.ACORD_FORM_ID = CL_FORMS.FORM_ID");
				sbSql.Append(" AND CLAIM_ACCORD_MAPPI.CLAIM_TYPE_CODE = CODES_TEXT.CODE_ID");
				sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID");
                sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
                sbSql.Append(" ORDER BY FORM_TITLE,CODES.SHORT_CODE");			
				
				objLstRow = objXmlDocument.CreateElement("listhead");

				objRowTxt = objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "AcordFormCol");
				objRowTxt.InnerText="ACORD Form";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "ClaimTypeCol");
				objRowTxt.InnerText="Claim Type";
				objLstRow.AppendChild(objRowTxt);
				
				objElement.AppendChild(objLstRow);
                sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "CLAIM_TYPE", this.LanguageCode);  //Aman ML Change
				objRead=DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				while (objRead.Read())
				{
					objLstRow = objXmlDocument.CreateElement("listrow");
					objLstRow.SetAttribute("value" , Conversion.ConvertObjToStr(objRead.GetValue("ACORD_FORM_ID")).Trim()+"_"+Conversion.ConvertObjToStr(objRead.GetValue("CLAIM_TYPE_CODE")).Trim());
					objRowTxt = objXmlDocument.CreateElement("rowtextaccord");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "AcordValue");
                    objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("CODE1")));// Modified Unit testing issue
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = objXmlDocument.CreateElement("rowtextclaim");
					objRowTxt.SetAttribute("type" , "label");
					objRowTxt.SetAttribute("name" , "ClaimValue");
                    objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("CODE2")).Trim() + " - " // Modified Unit testing issue
						+Conversion.ConvertObjToStr(objRead.GetValue("CODE_DESC")).Trim());
					objLstRow.AppendChild(objRowTxt);
					objElement.AppendChild(objLstRow);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClaimTypeVsAcordFormSetup.Get.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objElement=null;
				objNode=null;
                if (objRead != null)
                    objRead.Dispose();

				if (objCache!=null)
				{
					objCache.Dispose();
				}
				objCache=null;
				sbSql=null;
				sbTemp=null;
			}
			return objXmlDocument;
		}
		/// Name		: DeleteMapping
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes Mapping information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool DeleteMapping(XmlDocument p_objXmlDocument)
		{
			ArrayList arrlstSql=null;
			string sSQL="";
			bool bReturnValue=false;
			string sAcordId="";
			string sTemp="";
			string sClaimTypeId="";
			string[] arrValues=null;
			XmlNode objTempNode=null;
			try
			{
                objTempNode = p_objXmlDocument.SelectSingleNode("//ACORDFormSel");
				if (objTempNode!=null)
				{
					arrlstSql=new ArrayList();
					if (objTempNode.InnerText!=null)
					{
						arrValues=objTempNode.InnerText.Split(" ".ToCharArray());
					}
					for (int i=0;i<arrValues.Length;i++)
					{
						sTemp=arrValues[i];
						sAcordId=arrValues[i].Split("_".ToCharArray())[0];
						sClaimTypeId=arrValues[i].Split("_".ToCharArray())[1];
						sSQL="DELETE FROM CLAIM_ACCORD_MAPPI WHERE ACORD_FORM_ID="+sAcordId+" AND CLAIM_TYPE_CODE="+sClaimTypeId;
						arrlstSql.Add(sSQL);
					}
				}
                UTILITY.Save(arrlstSql, m_sDSN, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ClaimTypeVsAcordFormSetup.DeleteMapping.Error", m_iClientId), p_objEx);
			}
			finally
			{
				arrlstSql=null;
				objTempNode=null;
			}
			return bReturnValue;
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves Claim Acord form Mapping
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			DbReader objRead=null;
			string sAcordId="";
			string sDate="";
			string sClaimTypeId="";
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			ArrayList arrlstSql=null;
			bool bReturnVal=false;
			string sSQL="";
			try
			{
				arrlstSql=new ArrayList();               
                sAcordId = p_objXmlDocument.SelectSingleNode("//ACORDFormSel").InnerText ;
                sClaimTypeId = p_objXmlDocument.SelectSingleNode("//ClaimTypeSel").InnerText ;

				if (sAcordId!="" && sClaimTypeId!="")
				{
					sSQL="SELECT CLAIM_TYPE_CODE FROM CLAIM_ACCORD_MAPPI WHERE ACORD_FORM_ID='"+sAcordId+"' AND CLAIM_TYPE_CODE='"+sClaimTypeId+"'";
					objRead=DbFactory.GetDbReader(m_sDSN,sSQL);
					if (objRead.Read())
					{
						objRead.Close();
						throw new RMAppException(Globalization.GetString("ClaimTypeVsAcordFormSetup.Save.AlreadyExistsError", m_iClientId)); 
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
					sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
					sSQL="INSERT INTO CLAIM_ACCORD_MAPPI(CLAIM_TYPE_CODE, ACORD_FORM_ID, DTTM_RCD_ADDED, ADDED_BY_USER, DTTM_RCD_LAST_UPD, UPDATED_BY_USER) VALUES ('"+sClaimTypeId
						+"','"+sAcordId+"',"+sDate+",'"+m_sLoginId+"',"+sDate+",'"+m_sLoginId+"')";
					arrlstSql.Add(sSQL);
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
                    objConn.Dispose(); 
					objCommand=null;
					objTran=null;
				}
				bReturnVal=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("ClaimTypeVsAcordFormSetup.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{

                if (objRead != null)
                    objRead.Dispose();
                if (objConn != null)
                {
                    if (objConn.State == ConnectionState.Open)
                    {
                        objConn.Close();
                    }
                    objConn.Dispose();
                }
                if (objTran != null)
                    objTran.Dispose();

				objNode=null;
				objCommand=null;
				arrlstSql=null;
			}
			return bReturnVal;
		}

	#endregion
	}
}
