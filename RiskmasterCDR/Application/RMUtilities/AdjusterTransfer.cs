using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches adjuster transfer information.
	/// </summary>
	public class AdjusterTransfer
	{
		#region Input/Output Xml
		/*
		<Adjuster>
			<control name="AvailClaims" type="combobox" title="Available Claims:">
				<option value="295">DI2003000295</option>
			</control>
			<control name="FromAdjuster" type="combobox" title="From Adjuster" value="168">
				<option value="">
				</option>
				<option value="168">Adjuster, Anna</option>
			 
			</control>

			<control name="ToAdjuster" type="combobox" title="To Adjuster" value="218">
				<option value="">
				</option>
				<option value="168">Adjuster, Anna</option>

			</control>
			<control name="SelectedClaims" type="combobox" title="Selected Claims" value="295">
			</control>
			<control name="OnlyOpenClaims" type="checkbox" title="Only Open Claims" value="1" />
		</Adjuster>
		*/
		#endregion

		#region private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
        private int m_iClientId = 0;//psharma206 jira     98

		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		#endregion

		#region Constructor
		/// <summary>
		/// Default Constructor
		/// </summary>
		public AdjusterTransfer(){}
		/// <summary>
		/// Constuctor
		/// </summary>
		/// <param name="p_sDSN">Connection string</param>
        public AdjusterTransfer(string p_sDSN, int p_iClientId)//psharma206 jira     98
		{
			m_sDSN=p_sDSN;
            m_iClientId = p_iClientId;//psharma206 jira     98
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get()
		{
			XmlElement objElement=null;
			XmlNode objNode=null;
			XmlNode objToNode=null;
			DataSet objDS=null;
			XmlDocument objXmlDocument=null;
			StringBuilder sbSql=null;
			try
			{
				objXmlDocument=new XmlDocument();
				sbSql=new StringBuilder();
				sbSql.Append("<Adjuster>");
                sbSql.Append("<control name='AvailClaims' type='combobox'> </control>");
				sbSql.Append("<control name='FromAdjuster' type='combobox' title='From Adjuster'> </control>");
				sbSql.Append("<control name='ToAdjuster' type='combobox' title='To Adjuster'> </control>");
				sbSql.Append("<control name='SelectedClaims' type='combobox' title='Selected Claims'> </control>");
				sbSql.Append("<control name='OnlyOpenClaims' type='checkbox' title='Only Open Claims' value='1'/>");
				sbSql.Append("</Adjuster>");
				objXmlDocument.LoadXml(sbSql.ToString());
				objNode=objXmlDocument.SelectSingleNode("//control[@name='FromAdjuster']"); 
				objElement=objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				objNode.AppendChild(objElement);
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT LAST_NAME,FIRST_NAME,ENTITY_ID  ");
				sbSql.Append(" FROM ENTITY WHERE ENTITY_TABLE_ID=1047");
				sbSql.Append(" AND DELETED_FLAG=0");
                sbSql.Append(" ORDER BY UPPER(LAST_NAME)");
                //nadim MITS 12167
				objDS=DbFactory.GetDataSet(m_sDSN,sbSql.ToString(), m_iClientId);
				foreach(DataRow dr in objDS.Tables[0].Rows)
				{
					objElement=objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(dr["ENTITY_ID"]));
					objElement.InnerText=Conversion.ConvertObjToStr(dr["LAST_NAME"])+", "+Conversion.ConvertObjToStr(dr["FIRST_NAME"]);
					objNode.AppendChild(objElement);
				}
				objToNode=objXmlDocument.SelectSingleNode("//control[@name='ToAdjuster']"); 
				objElement=objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				objToNode.AppendChild(objElement);
				foreach(DataRow dr in objDS.Tables[0].Rows)
				{
					objElement=objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(dr["ENTITY_ID"]));
					objElement.InnerText=Conversion.ConvertObjToStr(dr["LAST_NAME"])+", "+Conversion.ConvertObjToStr(dr["FIRST_NAME"]);
					objToNode.AppendChild(objElement);
				}
				return objXmlDocument;
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AdjusterTransfer.Get.Error", m_iClientId), p_objEx);//psharma206 jira     98	
			}
			finally
			{
				objXmlDocument=null;
				objElement=null;
				objNode=null;
				objToNode=null;
				sbSql=null;
				objDS.Dispose();
			}
			
		}
		/// Name		: GetAvailableClaims
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves Claims data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument GetAvailableClaims(XmlDocument p_objDoc)
		{
			XmlElement objElement=null;
			XmlNode objNode=null;
			DataSet objDS=null;
			string sAdjusterId="";
			StringBuilder sbSql=null;
			string sOnlyOpenClaims="";
			XmlDocument objXmlDocument=null;
			try
			{
				objXmlDocument=new XmlDocument();
				sbSql=new StringBuilder();
				sbSql.Append("<Adjuster>");
                //amrit:added
				sbSql.Append("<control name='AvailClaims' type='combobox' title='Available Claims:'> </control>");
                //ends
                sbSql.Append("<control name='FromAdjuster' type='combobox' title='From Adjuster'> </control>");
                sbSql.Append("<control name='ToAdjuster' type='combobox' title='To Adjuster'> </control>");
                sbSql.Append("<control name='SelectedClaims' type='combobox' title='Selected Claims'> </control>");
                sbSql.Append("<control name='OnlyOpenClaims' type='checkbox' title='Only Open Claims' value='1'/>");
				sbSql.Append("</Adjuster>");
				objXmlDocument.LoadXml(sbSql.ToString());
				objNode=p_objDoc.SelectSingleNode("//control[@name='OnlyOpenClaims']"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					sOnlyOpenClaims=objNode.InnerText;
				}
				objNode=p_objDoc.SelectSingleNode("//control[@name='FromAdjuster']"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					sAdjusterId=objNode.InnerText;
				}
				if (sAdjusterId!="")
				{
					objNode=objXmlDocument.SelectSingleNode("//control[@name='AvailClaims']"); 
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT CLAIM_NUMBER,CLAIM.CLAIM_ID   ");
					sbSql.Append(" FROM CLAIM, CLAIM_ADJUSTER, CODES ");
					sbSql.Append(" WHERE CLAIM.CLAIM_ID=CLAIM_ADJUSTER.CLAIM_ID  ");
					sbSql.Append(" AND CLAIM.CLAIM_STATUS_CODE = CODES.CODE_ID  ");
					sbSql.Append(" AND CLAIM.CLAIM_STATUS_CODE = CODES.CODE_ID  ");
					sbSql.Append(" AND ADJUSTER_EID ="+sAdjusterId);
					sbSql.Append(" AND CURRENT_ADJ_FLAG <> 0 ");
					if (sOnlyOpenClaims.Trim().ToLower().Equals("true"))
					{
						sbSql.Append(" AND CODES.RELATED_CODE_ID = 9");
					}
					sbSql.Append(" ORDER BY CLAIM_NUMBER");//Deb: MITS 32510 
                    objDS = DbFactory.GetDataSet(m_sDSN, sbSql.ToString(), m_iClientId);
					foreach(DataRow dr in objDS.Tables[0].Rows)
					{
						objElement=objXmlDocument.CreateElement("option");
						objElement.SetAttribute("value",Conversion.ConvertObjToStr(dr["CLAIM_ID"]));
						objElement.InnerText=Conversion.ConvertObjToStr(dr["CLAIM_NUMBER"]);
						objNode.AppendChild(objElement);
					}
				}
				return objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AdjusterTransfer.GetAvailableClaims.Error", m_iClientId), p_objEx);//psharma206 jira     98	
			}
			finally
			{
				objXmlDocument=null;
				objElement=null;
				objNode=null;
				sbSql=null;
				if (objDS!=null)
				objDS.Dispose();
			}
			
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Transfers Adjsusters
		/// </summary>
		/// <param name="p_objXmlDocument">XmlDocument containing data</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			string sCurrentAdjId="";
			string sFutureAdjId="";
			string[] arrSelectedClaims=null;
			ArrayList arrlstSql=null;
			bool bReturnVal=false;
			string sSQL="";
			try
			{
				arrlstSql=new ArrayList();
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='FromAdjuster']"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					sCurrentAdjId=objNode.InnerText;
				}
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='ToAdjuster']"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					sFutureAdjId=objNode.InnerText;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SelectedClaims']");  
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					arrSelectedClaims=objNode.InnerText.Split(' ');
				}
				for(int i=0;i<arrSelectedClaims.Length;i++)
				{
					if (IsAdjusterExists(sFutureAdjId,arrSelectedClaims[i]))
					{
						//Abhishek MITS 10654 changed CURRENT_ADJ_FLAG= -1 from 1
						sSQL = "UPDATE CLAIM_ADJUSTER SET CURRENT_ADJ_FLAG=-1 WHERE CLAIM_ID IN (" + arrSelectedClaims[i]
								+ ") AND ADJUSTER_EID=" + sFutureAdjId;
						arrlstSql.Add(sSQL);
					}
					else
					{
						arrlstSql.Add(InsertNewAdjuster(sFutureAdjId,arrSelectedClaims[i]));
					}
					
						sSQL = "UPDATE CLAIM_ADJUSTER SET CURRENT_ADJ_FLAG=0 WHERE CLAIM_ID IN (" + arrSelectedClaims[i]
                        // akaushik5 Changed for MITS 33513 Starts
                        //+ ") AND ADJUSTER_EID=" + sCurrentAdjId;
                           + ") AND CURRENT_ADJ_FLAG <> 0 AND ADJUSTER_EID <> " + sFutureAdjId;
                    // akaushik5 Changed for MITS 33513 Ends
                        arrlstSql.Add(sSQL);
				}

                UTILITY.Save(arrlstSql, m_sDSN, m_iClientId);
				bReturnVal=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AdjusterTransfer.Save.Error", m_iClientId), p_objEx);//psharma206 jira     98	
			}
			finally
			{
				objNode=null;
				arrSelectedClaims=null;
				arrlstSql=null;
			}
			return bReturnVal;
		}
		#endregion

		#region Private Functions
		private string InsertNewAdjuster(string p_sFutureAdjId,string p_sClaimId)
		{
			//insert row with new adjuster for given claim ID
			int iNextId=0;
			string sInsertQuery="";
			try
			{
                iNextId = Utilities.GetNextUID(m_sDSN, "CLAIM_ADJUSTER", m_iClientId);
				sInsertQuery="INSERT INTO CLAIM_ADJUSTER (ADJ_ROW_ID,ADJUSTER_EID,CLAIM_ID,CURRENT_ADJ_FLAG) VALUES ("+iNextId.ToString()
					+ ","+p_sFutureAdjId+","+p_sClaimId+",-1)";
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AdjusterTransfer.InsertNewAdjuster.Error", m_iClientId), p_objEx);//psharma206 jira     98	
			}
			return sInsertQuery;
		}
		private bool IsAdjusterExists(string p_sFutureAdjusterId,string p_sClaimId)
		{
			StringBuilder sbSQL=null;
			string sAdjusterId="";
			bool bReturn=false;
			DbReader objReader=null;
			try
			{
				sbSQL=new StringBuilder();
				sbSQL.Append("SELECT ADJUSTER_EID");
				sbSQL.Append(" FROM CLAIM_ADJUSTER");
				sbSQL.Append(" WHERE CLAIM_ID="+p_sClaimId);
				objReader = DbFactory.GetDbReader(m_sDSN,sbSQL.ToString());
				while  (objReader.Read())
				{
					sAdjusterId= Conversion.ConvertObjToStr(objReader.GetValue("ADJUSTER_EID"));
					if (sAdjusterId.Equals(p_sFutureAdjusterId))
					{
						bReturn= true;
						break;
					}
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AdjusterTransfer.IsAdjusterExists.Error", m_iClientId), p_objEx);//psharma206 jira     98	
			}
			finally
			{
				sbSQL=null;
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
                if (objReader != null)
                    objReader.Dispose();
			}
			return bReturn;
		}
		#endregion

	}
}
