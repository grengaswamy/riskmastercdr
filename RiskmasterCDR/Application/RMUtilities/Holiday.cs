using System;
using System.Xml;
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Db;
using Riskmaster.Security;//JIRA RMA-9281 ajohari2

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   Holiday form
	/// </summary>
	public class Holiday:UtilitiesUIBase  
	{
		/// <summary>
		/// Database fields mapping string array
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "HOL_DATE"},
			{"Date", "HOL_DATE"},
			{"Desc", "DESCRIPTION"},
			{"Org", "ORG_EID"}
		};
        private int m_iClientId = 0; //sonali
        UserLogin m_oUserlogin = null;//JIRA RMA-9281 ajohari2

		/// Name		: Holiday
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public Holiday(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)//sonali-cloud
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

        public Holiday(UserLogin p_oUserLogin, string p_sConnString, int p_iClientId)
            : base(p_oUserLogin, p_sConnString, p_iClientId)//sonali-cloud //JIRA RMA-9281 ajohari2
        {
            m_oUserlogin = p_oUserLogin; //JIRA RMA-9281 ajohari2
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the variables and properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_HOLIDAYS";
			KeyField = "HOL_DATE";
			base.InitFields(arrFields);
			base.Initialize();
		}
		
		/// Name		: New
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Ge the structure for a new record
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>Xmldocument with structure</returns>
		public XmlDocument New(XmlDocument p_objXMLDocument)
		{
			try
			{
				return p_objXMLDocument; 
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Holiday.New.Err",m_iClientId),p_objEx);//sonali
			}
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for a particular id
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>xml data and structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlElement objElm;
			try
			{
				XMLDoc = p_objXmlDocument;
				objElm=(XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
				if(objElm!=null)
				{
					if(objElm.GetAttribute("orgid")!="")
					{
						base.WhereClause = " ORG_EID=" + objElm.GetAttribute("orgid"); 
					}
				}
				base.Get();
				objElm=(XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
				if(objElm!=null)
				{
					objElm.SetAttribute("currentid",objElm.GetAttribute("orgid"));  
                    p_objXmlDocument.SelectSingleNode("//control[@name='Org_orglist']").InnerText= objElm.GetAttribute("orgid");
				}
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("Holiday.Get.Err",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				objElm=null;
			}

		}

		/// Name		: Delete
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes a particular record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>next record if any</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDocument)
		{		
			XmlElement objElm =null;
				try
				{
					XMLDoc = p_objXmlDocument;
					objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
					if(objElm!=null)
					{
						base.WhereClause = "ORG_EID=" + objElm.InnerText; 
					}
					base.Delete();
					return XMLDoc;
				}
				catch(Exception p_objEx)
				{
					throw new RMAppException(Globalization.GetString("Holiday.Delete.Err",m_iClientId),p_objEx);//sonali
				}
				finally
				{
					objElm=null;
				}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the current record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data to save it</param>
		/// <returns>Saved data alogn with its xml structure</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			string[] arrId;
			string sRowId="";
			string sOrgId="";
			string sDesc="";
			string sDate="";
			string sSQL="";
            int iCounter=0; //zmohammad MITS 31442
			string sCurrentId="";
			XmlElement objElm=null;
			DbConnection objConn=null;
            Riskmaster.Db.DbReader objReader; //zmohammad MITS 31442
			try
			{
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
				if(objElm!=null)
					sRowId= Conversion.GetDate(objElm.InnerText);

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Date']");
				if(objElm!=null)
					sDate= Conversion.GetDate(objElm.InnerText);

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Desc']");
                if (objElm != null)
                {
                    //added by amrit for handling mits:14616
                    sDesc = objElm.InnerText;
                    sDesc = sDesc.Replace("'", "''");
                    //mits:ends14616
                }
				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
				if(objElm!=null)
					sOrgId=objElm.GetAttribute("codeid");

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Org']");
				if(objElm!=null)
					sCurrentId=objElm.GetAttribute("currentid");

                arrId = p_objXmlDocument.SelectSingleNode("//control[@name='Org_orglist']").InnerText.Split('|');//zmohammad MITS 31442
				
				objConn = DbFactory.GetDbConnection(ConnectString);
				objConn.Open(); 
                //zmohammad MITS 31442 start
                if (sRowId == "")
                {
                sSQL = "SELECT * FROM SYS_HOLIDAYS WHERE HOL_DATE='" + sDate + "'";
                objReader = objConn.ExecuteReader(sSQL);
                while (objReader.Read())
                    {
                        iCounter = 1;
                        throw new Exception();  
                    }
                objReader.Close();
                }
                //zmohammad MITS 31442 end
                if (sRowId != "")
                {
                    sSQL = "DELETE FROM SYS_HOLIDAYS WHERE HOL_DATE='" + sRowId + "' AND ORG_EID=" + sCurrentId;
                    objConn.ExecuteNonQuery(sSQL);
                }


                for (int iCtr = 0; iCtr < arrId.Length; iCtr++)
                {
                    if (arrId[iCtr] != "")
                    {
                        sSQL = "INSERT INTO SYS_HOLIDAYS VALUES('" + sDate + "','" + sDesc + "'," + arrId[iCtr] + ")";
                        objConn.ExecuteNonQuery(sSQL);
                    }
                }

                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                //zmohammad MITS 31442
                if (iCounter != 0)
                {
                    throw new RMAppException(Globalization.GetString("Holiday.Record.Exist",m_iClientId), p_objEx);//sonali
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("Holiday.Save.Err",m_iClientId), p_objEx);//sonali
                }
            }
			finally
			{
				objElm=null;
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}
	}
}