﻿using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;//Rijul 340
using Riskmaster.Settings;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches tax mapping information.
	/// </summary>
	public class TaxMapping
	{
		#region XML Format
			/*
			<TaxMapping>
				<TaxMapValues>
					<FederalTaxes value="" stateId="" payee="" transTypeid="" payeeid="" transType="" taxPercent="" />
					<StateTax value="" payee="" payeeid="" transTypeid="" transType="" taxPercent="" stateId=""></StateTax>
				</TaxMapValues>
				<Transactions>
					<Transaction value=""></Transaction> 
					<Transaction value=""></Transaction>
				</Transactions>
				<States>
					<State value=""></State>
					<State value=""></State>
				</States>
			</TaxMapping>
			*/
		#endregion

		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
        private string m_sDSN = "";
        private int m_iClientId = 0;//sonali
        //Rijul Start : Worked for 340
        private string DsnName = "";
        private string m_Password = "";
        private string m_UserName = "";         
        private DataModelFactory m_objDataModelFactory = null;
        Riskmaster.Settings.SysSettings objSysSettings = null; 
        //Rijul 340 end
		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		#endregion

		#region Constructor
        ///// <summary>
        ///// Constructor
        ///// </summary>
        ///// <param name="p_sConnectString">Connection string</param>
        //public TaxMapping(string p_sConnectString,int p_iClientId=0)//sonali
        //{
        //    m_sDSN = p_sConnectString;
        //    m_iClientId = p_iClientId;
        //}

        //Rijul Start : Worked for 340 - Added UserName
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="p_sUserName">Current Login User Name</param>
		/// <param name="p_sConnectString">Connection string</param>        
		public TaxMapping(string p_sConnectString,int p_iClientId)//sonali
		{
            m_sDSN = p_sConnectString;           
            m_iClientId = p_iClientId;
		}

        public TaxMapping(string p_sUserName, string p_sConnectString, string p_sDsnName, string p_sPassword, int p_iClientId)
        {
            m_sDSN = p_sConnectString;
            m_UserName = p_sUserName;
            DsnName = p_sDsnName;
            m_Password = p_sPassword;
            m_iClientId = p_iClientId;
        }
        //Rijul 340 End
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves Tax mapping data
		/// </summary>
		/// <returns>Xml containing Tax mapping data</returns>
		public XmlDocument Get()
		{
			XmlDocument objDOM=null;
			XmlNode objNode=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			string sTaxName="";
			XmlElement objElemTemp=null;
			DbReader objReader=null;
			XmlDocument objDocTemp=null;
			string sStateId="";
			StringBuilder objSql=null;
			objSql=new StringBuilder();
			string sFname="";
			string sLname="";
			string sEname="";
			LocalCache objCache=null;
			string sShortCode="";
			string sShortDesc="";
			try
			{
				objSql.Append(" SELECT * FROM TAX_MAPPING");
				objReader = DbFactory.GetDbReader(m_sDSN,objSql.ToString());
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("TaxMapping");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("TaxMapValues");
				objDOM.FirstChild.AppendChild(objElemChild);
				objCache=new LocalCache(m_sDSN,m_iClientId);
				while (objReader.Read())
				{
                    objCache.GetEntityInfo(Conversion.ConvertObjToInt64(objReader.GetValue("TAX_EID"), m_iClientId), ref sFname, ref sLname);
					sEname=sLname;
					if (!sFname.Trim().Equals(""))
					{
						sEname+=", "+sFname;
					}
					sStateId=Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID"));
					if (sStateId=="-1")
					{
						sTaxName=Conversion.ConvertObjToStr(objReader.GetValue("TAX_NAME")).Trim().ToUpper();
						if (sTaxName.Equals("FEDERAL")
							||sTaxName.Equals("SOCIAL_SECURITY")
							||sTaxName.Equals("MEDICARE")
							)
						{
							objElemTemp=objDOM.CreateElement("FederalTaxes");
							objElemTemp.SetAttribute("value",sTaxName);
							objElemTemp.SetAttribute("stateId","");
							objElemTemp.SetAttribute("payee",sEname);
							objElemTemp.SetAttribute("transTypeid","");
							objElemTemp.SetAttribute("payeeid",Conversion.ConvertObjToStr(objReader.GetValue("TAX_EID")));
							objElemTemp.SetAttribute("transType",Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE_ID")));
							objElemTemp.SetAttribute("taxPercent",Conversion.ConvertObjToStr(objReader.GetValue("TAX_PERCENTAGE")));
							objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
						}
					}
					else
					{
						objElemTemp=objDOM.CreateElement("StateTax");
						objElemTemp.SetAttribute("value","STATE");
						objElemTemp.SetAttribute("payee",sEname);
						objElemTemp.SetAttribute("payeeid",Conversion.ConvertObjToStr(objReader.GetValue("TAX_EID")));
						objElemTemp.SetAttribute("transTypeid",Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE_ID")));
						objElemTemp.SetAttribute("transType",objCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId)));
						objElemTemp.SetAttribute("taxPercent",Conversion.ConvertObjToStr(objReader.GetValue("TAX_PERCENTAGE")));
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID"))
							+"-"+Conversion.ConvertObjToStr(objReader.GetValue("TAX_EID"))
							+"-"+Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE_ID"));
						objCache.GetStateInfo(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID"))),ref sShortCode,ref sShortDesc);
						objElemTemp.SetAttribute("stateId",sShortDesc);
						objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
					}
					sEname="";
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				objCache.Dispose();
				objDocTemp=GetTransTypes();
				objNode =objDOM.ImportNode(objDocTemp.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNode);
				objDocTemp=GetStates();
				objNode =objDOM.ImportNode(objDocTemp.DocumentElement,true);
				objDOM.FirstChild.AppendChild(objNode);

				objElemParent = (XmlElement) objDOM.SelectSingleNode("//TaxMapping");
				objElemChild = objDOM.CreateElement("Offsets");
				objElemParent.AppendChild(objElemChild);
				objElemParent = (XmlElement) objDOM.SelectSingleNode("//Offsets");
				string sSQL = "SELECT * FROM OFFSET_MAPPING";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL.ToString());
				
				while(objReader.Read())
				{
					objElemChild = objDOM.CreateElement("Offset");
					objElemChild.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE_ID"));
					objElemChild.SetAttribute("name" , Conversion.ConvertObjToStr(objReader.GetValue("OFFSET_NAME")));
					objElemParent.AppendChild(objElemChild);
				}
				objReader.Close();

				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("TaxMapping.Get.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				objDOM=null;
				objNode=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
				if (!objReader.IsClosed)
				{
					objReader.Close();
                    objReader.Dispose();
				}
				objReader=null;
				objDocTemp=null;
				objSql=null;
                if (objCache != null)
                    objCache.Dispose();
			}
			
		}
		/// Name		: DeleteStateTaxInfo
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete State Tax information
		/// </summary>
		/// <param name="p_sXml">Xml containing data</param>
		/// <returns>boolean</returns>
		public bool DeleteStateTaxInfo(XmlDocument p_objDOC)
		{
			string sSql=null;
			ArrayList arrlstSql=null;
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			XmlNode objNode=null;
			XmlDocument objDOM=null;
			string []arrValues=null;
			try
			{
				arrlstSql=new ArrayList();
				objDOM=p_objDOC;
				objNode = objDOM.SelectSingleNode("//DeleteMapping");
				if (objNode.InnerText!=null)
				{
					arrValues=objNode.InnerText.Split(" ".ToCharArray());
				}
				arrlstSql=new ArrayList();
				if (arrValues!=null)
				{
					for (int i=0;i<arrValues.Length;i++)
					{
						if (!arrValues[i].Trim().Equals(""))
						{
							sSql="DELETE FROM TAX_MAPPING WHERE TAX_NAME='STATE' AND TAX_EID='"+arrValues[i].Split("-".ToCharArray())[1]
								+"' AND TRANS_TYPE_CODE_ID='"+arrValues[i].Split("-".ToCharArray())[2]+
								"' AND STATE_ID='"+arrValues[i].Split("-".ToCharArray())[0]+"'";
							arrlstSql.Add(sSql);
						}
					}
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
					//objTran=null;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("TaxMapping.DeleteStateTaxInfo.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				arrlstSql=null;
				if(objConn != null)
                    objConn.Dispose();
				if (objTran!=null)
                    objTran.Dispose();
				objCommand=null;
				objNode=null;
				objDOM=null;
				arrValues=null;
			}
			return true;	
		}
		/// Name		: SaveStateTaxInfo
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves State tax information
		/// </summary>
		/// <param name="p_sXml">Xml containing state tax data to save</param>
		/// <returns>bool</returns>
		public bool SaveStateTaxInfo(XmlDocument p_objDOC)
		{
			string sSql=null;
			ArrayList arrlstSql=null;
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			XmlNodeList objNodeList=null;
			DbReader objDBReader=null;
			XmlDocument objDOM=null;
            //Rijul 340
            string sPayeeId = string.Empty;
            LocalCache objCache = null;
            //Rijul 340 end
			try
			{
                objSysSettings = new Settings.SysSettings(m_sDSN, m_iClientId);//Rijul 340
                objCache = new LocalCache(m_sDSN, m_iClientId);     //Rijul 340
                m_objDataModelFactory = new DataModelFactory(DsnName, m_UserName, m_Password,m_iClientId);     //Rijul 340
				arrlstSql=new ArrayList();
				objDOM=p_objDOC;
				objNodeList = objDOM.SelectNodes("//TaxMapping/TaxMapValues/StateTax");
				foreach (XmlNode objTmpNode in objNodeList)
				{
					sSql = "SELECT TAX_NAME FROM TAX_MAPPING WHERE TAX_NAME = 'STATE' AND TAX_EID ='" + objTmpNode.Attributes["payeeid"].Value+"' AND TRANS_TYPE_CODE_ID='"+
						objTmpNode.Attributes["transTypeid"].Value+"'";
					objDBReader = DbFactory.GetDbReader(m_sDSN,sSql);
					if(objDBReader.Read())
					{
                        throw new RMAppException(Globalization.GetString("TaxMapping.SaveStateTaxInfo.AlreadyExists", m_iClientId));//sonali
					}
					objDBReader.Close();
                    //Rijul 340 
                    sPayeeId=objTmpNode.Attributes["payeeid"].Value;
					if (!sPayeeId.Trim().Equals(""))
                    //Rijul 340 end
					{
					sSql = " INSERT INTO TAX_MAPPING (TAX_NAME, TAX_EID, TRANS_TYPE_CODE_ID, TAX_PERCENTAGE, STATE_ID) VALUES ('"
						+objTmpNode.Attributes["value"].Value+"','"+objTmpNode.Attributes["payeeid"].Value+"','"
						+objTmpNode.Attributes["transTypeid"].Value+"','"+objTmpNode.Attributes["taxPercent"].Value+"','"+objTmpNode.Attributes["stateId"].Value+"')";
					arrlstSql.Add(sSql);	
				}
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
                        //Rijul 340
                        bool blnSuccess;
                        if (objSysSettings.UseEntityRole)
                        {
                            using (EntityXRole objEntityXRole = m_objDataModelFactory.GetDataModelObject("EntityXRole", false) as EntityXRole)
                            {
                                if (Conversion.CastToType<int>(sPayeeId, out blnSuccess) > 0)
                                    objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.TAX_ENTITY.ToString
                                            ()), Conversion.CastToType<int>(sPayeeId, out blnSuccess), m_UserName);
                            }
                        }
                        //Rijul 340 end
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
					//objTran=null;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("TaxMapping.SaveStateTaxInfo.Error", m_iClientId), p_objEx);//sonali
			}
			finally
			{
				arrlstSql=null;
				if (objConn != null)
                    objConn.Dispose();
				if(objTran!=null)
                    objTran.Dispose();
				objCommand=null;
				objNodeList=null;
				objDOM=null;
                if (objDBReader != null)
                {
                    objDBReader.Close();
                    objDBReader.Dispose();
                }
			}
			return true;	
		}
		/// Name		: SaveFederalTaxInfo
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves Federal tax information
		/// </summary>
		/// <param name="p_sXml">Xml containing state tax data to save</param>
		/// <returns></returns>
		public bool SaveFederalTaxInfo(XmlDocument p_objDOC)
		{
			string sSql=null;
			ArrayList arrlstSql=null;
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			XmlNodeList objNodeList=null;
			XmlDocument objDOM=null;
            //Rijul 340
            string sPayeeId = string.Empty;
            LocalCache objCache = null;
            //Rijul 340 end
			try
			{
                objSysSettings = new Settings.SysSettings(m_sDSN, m_iClientId); 
                objCache = new LocalCache(m_sDSN, m_iClientId);     //Rijul 340
                m_objDataModelFactory = new DataModelFactory(DsnName, m_UserName, m_Password, m_iClientId);//Rijul 340
				arrlstSql=new ArrayList();
				sSql=" DELETE FROM TAX_MAPPING WHERE STATE_ID =-1";
				arrlstSql.Add(sSql);
				objDOM=p_objDOC;
             //   added:amrit
				//objNodeList = objDOM.SelectNodes("//TaxMapping/TaxMapValues/FederalTax/FederalTaxes");
                objNodeList = objDOM.SelectNodes("//FederalTaxes");
              //  changes ends
				foreach (XmlNode objTmpNode in objNodeList)
				{
					sPayeeId=objTmpNode.Attributes["payeeid"].Value;
					if (!sPayeeId.Trim().Equals(""))
					{
						sSql = " INSERT INTO TAX_MAPPING (TAX_NAME, TAX_EID, TRANS_TYPE_CODE_ID, TAX_PERCENTAGE, STATE_ID) VALUES ('"
							+objTmpNode.Attributes["value"].Value+"','"+objTmpNode.Attributes["payeeid"].Value+"','"
							+objTmpNode.Attributes["transTypeid"].Value+"','"+objTmpNode.Attributes["taxPercent"].Value+"','-1')";
						arrlstSql.Add(sSql);
                        //Rijul 340 
                        bool blnSuccess;
                        if (objSysSettings.UseEntityRole)
                        {
                            using (EntityXRole objEntityXRole = m_objDataModelFactory.GetDataModelObject("EntityXRole", false) as EntityXRole)
                            {
                                if (Conversion.CastToType<int>(sPayeeId, out blnSuccess) > 0)
                                    objEntityXRole.UpdateEntityXRole(objCache.GetTableId(Globalization.EntityGlossaryTableNames.TAX_ENTITY.ToString
                                            ()), Conversion.CastToType<int>(sPayeeId, out blnSuccess), m_UserName);
                            }
                        }
                        //Rijul 340 end
					}
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
					//objTran=null;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("TaxMapping.SaveFederalTaxInfo.Error", m_iClientId), p_objEx);//sonali
			}
			finally
			{
				arrlstSql=null;
                if (objConn != null)
                    objConn.Dispose();
                if (objTran != null)
                    objTran.Dispose();
				objCommand=null;
				objNodeList=null;
				objDOM=null;
			}
			return true;	
		}
		/// Name		: SaveOffsets
		/// Author		: Raman Bhatia
		/// Date Created: 04/01/2007		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves Offsets Mapping
		/// </summary>
		/// <param name="p_sXml">Xml containing state tax data to save</param>
		/// <returns></returns>
		public bool SaveOffsets(XmlDocument p_objDOC)
		{
			string sSql=null;
			ArrayList arrlstSql=null;
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			XmlNodeList objNodeList=null;
			XmlDocument objDOM=null;
			try
			{
				arrlstSql=new ArrayList();
				sSql=" DELETE FROM OFFSET_MAPPING ";
				arrlstSql.Add(sSql);
				objDOM=p_objDOC;
				objNodeList = objDOM.SelectNodes("//TaxMapping/Offsets/Offset");
				foreach (XmlNode objTmpNode in objNodeList)
				{
					sSql = " INSERT INTO OFFSET_MAPPING (OFFSET_NAME, TRANS_TYPE_CODE_ID) VALUES ('"
							+objTmpNode.Attributes["name"].Value+"','"+objTmpNode.InnerText+"')";
						arrlstSql.Add(sSql);
				
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
					//objTran=null;
				}
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("TaxMapping.SaveFederalTaxInfo.Error", m_iClientId), p_objEx);//sonali
			}
			finally
			{
				arrlstSql=null;
                if (objConn != null)
                    objConn.Dispose();
                if (objTran != null)
                    objTran.Dispose();
				objCommand=null;
				objNodeList=null;
				objDOM=null;
			}
			return true;	
		}
		#endregion

		#region Private Functions
		private XmlDocument GetStates()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			string sSQL="";
			DbReader objReader=null;
			try
			{
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("States");
				objDOM.AppendChild(objElemParent);
				sSQL="SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE STATE_ROW_ID > 0 AND STATE_ROW_ID NOT IN(SELECT STATE_ID FROM TAX_MAPPING WHERE STATE_ID > 0) ORDER BY STATE_ID ASC";
				objReader = DbFactory.GetDbReader(m_sDSN,sSQL);
				while  (objReader.Read())
				{
					objElemTemp=objDOM.CreateElement("State");
					objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetValue(0)));
					objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue(1))+" - "+Conversion.ConvertObjToStr(objReader.GetValue(2));
					objDOM.FirstChild.AppendChild(objElemTemp);
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("TaxMapping.GetStates.Error", m_iClientId), p_objEx);//sonali
			}
			finally
			{
				objDOM=null;
				objElemParent=null;
				objElemTemp=null;
                if (objReader!=null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                
			}
			
		}

		private XmlDocument GetTransTypes()
		{
			DataSet objDs=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemTemp=null;
			string sTransType="";
			LocalCache objCache=null;
			try
			{
                objCache = new LocalCache(m_sDSN, m_iClientId);
				sTransType=Conversion.ConvertObjToStr(objCache.GetTableId("TRANS_TYPES"));
				objCache.Dispose();
                objDs = UTILITY.GetCodeList(sTransType, m_sDSN, m_iClientId);
				
				if (objDs.Tables[0]!=null)
				{
					objDOM =new XmlDocument();
					objElemParent = objDOM.CreateElement("Transactions");
					objDOM.AppendChild(objElemParent);
					foreach (DataRow dr in objDs.Tables[0].Rows)
					{
						objElemTemp=objDOM.CreateElement("Transaction");
						objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(dr["CODE_ID"]));
						objElemTemp.InnerText=Conversion.ConvertObjToStr(dr["SHORT_CODE"]+" - "+Conversion.ConvertObjToStr(dr["CODE_DESC"]));
						objDOM.FirstChild.AppendChild(objElemTemp);
					}
				
				}//end if
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("TaxMapping.GetTransTypes.Error", m_iClientId), p_objEx);//sonali
			}
			finally
			{				
                if (objDs != null)
                    objDs.Dispose();                
				objDOM=null;
				objElemParent=null;
				objElemTemp=null;
				objCache=null;
			}
			
		}
		#endregion
		
	}
}
