﻿using System;
using System.Xml; 
using System.Data; 
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;  
using Riskmaster.Settings;
using Riskmaster.Security;
using Riskmaster.DataModel;  
using System.Collections;
using System.Collections.Generic;
using System.Text; //mbahl3
using System.Configuration;
using System.Text.RegularExpressions;
using Riskmaster.Cache;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   20th,Apr 2005
	///Purpose :   Implements the Search Wizard Form
	/// </summary>
	public class SearchWizard
	{

		/// <summary>
		/// User Id
		/// </summary>
		string m_sUser="";

		/// <summary>
		/// Password
		/// </summary>
		string m_sPass="";

		/// <summary>
		/// Dsn Name
		/// </summary>
		string m_sDsn="";

		/// <summary>
		/// Connection String
		/// </summary>
		string m_sConnStr = "";

		/// <summary>
		/// Security Dsn
		/// </summary>
		string m_sSecDsn = "";

		/// <summary>
		/// Dsn ID
		/// </summary>
		int m_iDSNId = 0;
        int m_iGroupId = 0; //mbahl3
        //dvatsa
        private int m_iClientId = 0;
		string m_sViewConnString = string.Empty;
		/// Name		: SearchWizard
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
        /// dvatsa
		public SearchWizard(string p_sUserName, string p_sPassword,string p_sDatabaseName,int p_iClientId)
		{
            //dvatsa
            m_iClientId = p_iClientId;
            UserLogin objUserLogin = new UserLogin(p_sUserName, p_sPassword, p_sDatabaseName, m_iClientId);			
			m_iDSNId = objUserLogin.objRiskmasterDatabase.DataSourceId; 
			m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;                       
            m_sSecDsn = Security.SecurityDatabase.GetSecurityDsn(m_iClientId);
            //dvatsa
            m_iGroupId = objUserLogin.GroupId; //mbahl3
			objUserLogin = null;
			m_sUser=p_sUserName;
			m_sPass=p_sPassword;
			m_sDsn=p_sDatabaseName;
            
			m_sViewConnString = ConfigurationInfo.GetViewConnectionString(m_iClientId);
		}

		/// Name		: GetStep1
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get method returns the data for Search Wizard. This function works for both a new and existing
		/// search view. If view id is present it picks that data only.
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetStep1_old(XmlDocument p_objXmlDoc)
		{
			int iViewId=0;
			int iCatId=0;
			string sSQL="";
			XmlElement objElm=null;
			XmlElement objNewElm=null;
			DbReader objRdr=null;
            //Added by Shivendu for MITS 9892
            bool bUseEnhPol = false;
            //Added by Shivendu for MITS 9892
            SysSettings objSys = null;
            try
			{
				//Get the view ID if its an existing view
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
				if(objElm!=null)
					iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);

				//Get the data for First Screen
				if(iViewId>0)
				{
					sSQL = "SELECT * FROM SEARCH_VIEW WHERE VIEW_ID = " + iViewId;
					objRdr=DbFactory.GetDbReader(m_sConnStr, sSQL);
					if(objRdr.Read())
					{
						iCatId = objRdr.GetInt32("CAT_ID");
						objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SearchName']");
						if(objElm!=null)
							objElm.InnerText = objRdr.GetString("VIEW_NAME");

						objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SearchDesc']");
						if(objElm!=null)
							objElm.InnerText = objRdr.GetString("VIEW_DESC");

						if(objRdr.GetString("VIEW_AT_TABLE") !="") 
						{
							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AdmTable']");
							if(objElm!=null)
								objElm.SetAttribute("sysname", objRdr.GetString("VIEW_AT_TABLE"));
						}
					}
					objRdr.Close(); 
				}

				//Get the Search Categories
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SearchType']");
				
				sSQL="SELECT CAT_ID,CAT_NAME FROM SEARCH_CAT ORDER BY CAT_ID";
				objRdr=DbFactory.GetDbReader(m_sConnStr, sSQL);
                //Start by Shivendu for MITS 9892
                //dvatsa
                objSys = new SysSettings(m_sConnStr,m_iClientId);
                bUseEnhPol = Conversion.ConvertLongToBool(objSys.UseEnhPolFlag, m_iClientId);
                //End by Shivendu for MITS 9892
                while(objRdr.Read())
				{
					//Start by Shivendu for MITS 9892
                    if (bUseEnhPol && (objRdr.GetInt32("CAT_ID").ToString() == (string)"6"))
                        continue;
                    if (!bUseEnhPol && (objRdr.GetInt32("CAT_ID").ToString() == (string)"21"))
                        continue;
                    //Start by Shivendu for MITS 9892
                    objNewElm=p_objXmlDoc.CreateElement("level");
					objNewElm.SetAttribute("id",objRdr.GetInt32("CAT_ID").ToString());
					objNewElm.SetAttribute("name",objRdr.GetString("CAT_NAME").ToString());
					if(objRdr.GetInt32("CAT_ID").ToString()==iCatId.ToString() )
						objNewElm.SetAttribute("selected","1");
					else
						objNewElm.SetAttribute("selected","0");
					objElm.AppendChild(objNewElm);
				}
				
				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{

                //dvatsa
				throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr",m_iClientId),p_objEx);
			}
			finally
			{
				objElm=null;
				objNewElm=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
                objSys = null;
			}
		}
        /// <summary>
        /// Created By Parijat :To meet R5 needs
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument GetStep1(XmlDocument p_objXmlDoc)
        {
            int iViewId = 0;
            int iCatId = 0;
            string sSQL = "";
            XmlElement objElm = null;
            XmlElement objNewElm = null;
            DbReader objRdr = null;
            //Start-Mridul Bansal. 01/15/10. MITS#18229.
            //Added by Shivendu for MITS 9892
            //bool bUseEnhPol = false;
            int iSearchCatId = 0;
            bool bUseEnhPolForGL = false;
            bool bUseEnhPolForAL = false;
            bool bUseEnhPolForPC = false;
            bool bUseEnhPolForWC = false;

            const int LOBFORGC = 241;
            const int LOBFORWC = 243;
            const int LOBFORVA = 242;
            const int LOBFORPC = 845;
            ColLobSettings objLobSettings = null;
            //Added by Shivendu for MITS 9892
            //SysSettings objSys = null;
            //End-Mridul Bansal. 01/15/10. MITS#18229.
            string sLangCode = string.Empty;
            const string SearchKeyword="srh";
            string sCATName=string.Empty;
            string sResourceVal = string.Empty;
            string sPageId = string.Empty;
            Dictionary<string, string> strDictParams = new Dictionary<string, string>();
            Dictionary<string, string> strDictResourceValues = new Dictionary<string, string>();
            int iLangCode = 0;
            try
            {
                sLangCode = ((XmlElement)p_objXmlDoc.SelectSingleNode("//LangId")).InnerText;
                objElm = ((XmlElement)p_objXmlDoc.SelectSingleNode("//PageId"));
                if(objElm != null)
                    sPageId = objElm.InnerText;
                //Get the view ID if its an existing view
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
                if (objElm != null)
                    iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);
              iLangCode = Convert.ToInt32(sLangCode);
                //Get the data for First Screen
                if (iViewId > 0 || iViewId <= -100) //akaur9 Mobile Adjuster --ViewId <= -100 used for the Mobile searches like Claim Search, PI Search.. hard coded in base sql.
                {
                    sSQL = "SELECT * FROM SEARCH_VIEW WHERE VIEW_ID = " + iViewId;
                    objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                    if (objRdr.Read())
                    {
                        iCatId = objRdr.GetInt32("CAT_ID");
                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SearchName");
                        if (objElm != null)
                            objElm.InnerText = objRdr.GetString("VIEW_NAME");

                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SearchDesc");
                        if (objElm != null)
                            objElm.InnerText = objRdr.GetString("VIEW_DESC");

                        if (objRdr.GetString("VIEW_AT_TABLE") != "")
                        {
                            objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AdmTable");
                            if (objElm != null)
                                objElm.SetAttribute("sysname", objRdr.GetString("VIEW_AT_TABLE"));
                        }

                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefaultChkboxValue");
                        if (objElm != null)
                            objElm.InnerText=objRdr.GetBoolean("CHECKBOX_FLAG").ToString();
                    }
                    objRdr.Close();
                }

                //Get the Search Categories
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SearchType");

                if (!strDictParams.ContainsKey("PageId"))
                    strDictParams.Add("PageId", sPageId);
                sSQL = string.Format("SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND LANGUAGE_ID={0}", Convert.ToInt32(iLangCode));
                objRdr = DbFactory.ExecuteReader(m_sViewConnString, sSQL.ToString(), strDictParams);
                while (objRdr.Read())
                {
                    strDictResourceValues.Add(Conversion.ConvertObjToStr(objRdr.GetValue(0)), Conversion.ConvertObjToStr(objRdr.GetValue(1)));
                }
                objRdr.Close();
                sSQL = "SELECT CAT_ID,CAT_NAME FROM SEARCH_CAT ORDER BY CAT_ID";
                objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                //Start-Mridul Bansal. 01/15/10. MITS#18229.
                ////Start by Shivendu for MITS 9892
                //objSys = new SysSettings(m_sConnStr);
                //bUseEnhPol = Conversion.ConvertLongToBool(objSys.UseEnhPolFlag);
                ////End by Shivendu for MITS 9892
                objLobSettings = new ColLobSettings(m_sConnStr,m_iClientId);
                bUseEnhPolForAL = objLobSettings[LOBFORVA].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolForGL = objLobSettings[LOBFORGC].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolForPC = objLobSettings[LOBFORPC].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolForWC = objLobSettings[LOBFORWC].UseEnhPolFlag == -1 ? true : false;
                //End-Mridul Bansal. 01/15/10. MITS#18229.
                while (objRdr.Read())
                {
                    //Start-Mridul Bansal. 01/15/10. MITS#18229.
                    ////Start by Shivendu for MITS 9892
                    //if (bUseEnhPol && (objRdr.GetInt32("CAT_ID").ToString() == (string)"6"))
                    //    continue;
                    //if (!bUseEnhPol && (objRdr.GetInt32("CAT_ID").ToString() == (string)"21"))
                    //    continue;
                    //iCatId = Conversion.ConvertObjToInt(objRdr.GetInt32("CAT_ID"), m_iClientId);
                    iSearchCatId = Conversion.ConvertObjToInt(objRdr.GetInt32("CAT_ID"), m_iClientId);
                    if (bUseEnhPolForAL && bUseEnhPolForGL && bUseEnhPolForPC && bUseEnhPolForWC && iSearchCatId.ToString() == "6")
                        continue;
                    if (!bUseEnhPolForAL && !bUseEnhPolForGL && !bUseEnhPolForPC && !bUseEnhPolForWC && iSearchCatId.ToString() == "21")
                        continue;
                    //Start by Shivendu for MITS 9892
                    //End-Mridul Bansal. 01/15/10. MITS#18229.
                    objNewElm = p_objXmlDoc.CreateElement("level");
                    objNewElm.SetAttribute("id", objRdr.GetInt32("CAT_ID").ToString());
                    sCATName=objRdr.GetString("CAT_NAME");
                    strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCATName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                    objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sCATName);                   
                    if (objRdr.GetInt32("CAT_ID").ToString() == iCatId.ToString())
                        objNewElm.SetAttribute("selected", "1");
                    else
                        objNewElm.SetAttribute("selected", "0");
                    objElm.AppendChild(objNewElm);
                }

                return p_objXmlDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                objNewElm = null;
                if (objRdr != null)
                {
                    objRdr.Dispose();
                }
                //Start-Mridul Bansal. 01/15/10. MITS#18229.
                //objSys = null;
                objLobSettings = null;
                //end-Mridul Bansal. 01/15/10. MITS#18229.
            }
        }

		/// Name		: GetStep2n3
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get method returns the data for Search Wizard
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetStep2n3_Old(XmlDocument p_objXmlDoc)
		{
			int iViewId=0;
			int iCatId=0;
			int iFieldId=0;
			int iTableId=0;
			string sSQL="";
			string sFieldName="";
			string sFieldType="";
			string sTableName="";
			bool bUseEnhPol=false;
			XmlElement objElm=null;
			XmlElement objNewElm=null;
			XmlElement objParentSrch=null;
			XmlElement objParentDisp=null;
			DbReader objRdr=null;
			DbReader objRdrSupp=null;
			SysSettings objSys=null;
			LocalCache objCache=null;
			XmlElement objAvlSrch=null;
			XmlElement objAvlDisp=null;
			XmlElement objFldsToSrch=null;
			XmlElement objFldsToDisp=null;
			string sCatName="";

			XmlElement objFirstAccessStep2=null;
			XmlElement objFirstAccessStep3=null;
			string sCodeIds = "";
			string [] arrCodes = {""};
			string [] arrAttributes = {""};
			string sTemp = "";

			try
			{

                objSys = new SysSettings(m_sConnStr, m_iClientId);  //dvatsa
                bUseEnhPol = Conversion.ConvertLongToBool(objSys.UseEnhPolFlag, m_iClientId); 
				objSys=null;
				//Get the view ID if its a existing view
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
				if(objElm!=null)
					iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);
				//Get CAT_ID. It should be populated both in new and edit cases
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SearchType']/level[@selected='1']");
				if(objElm!=null)
					iCatId = Conversion.ConvertStrToInteger(objElm.GetAttribute("id"));
				objAvlSrch = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FldsAvlSrch']");
				objAvlDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FldsAvlDisp']");
				if(iCatId!=20)
				{
					sSQL = "SELECT DISPLAY_CAT,FIELD_ID,FIELD_DESC,FIELD_TYPE,FIELD_TABLE FROM SEARCH_DICTIONARY" + 
						" WHERE CAT_ID = " + iCatId + " ORDER BY DISPLAY_CAT,FIELD_DESC";
					objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
					while(objRdr.Read())
					{
						sCatName = objRdr.GetString("DISPLAY_CAT").ToString();
						sFieldType = objRdr.GetString("FIELD_TABLE").ToString();
						objParentSrch = (XmlElement)objAvlSrch.SelectSingleNode("./DisplayCategory[@name='" + sCatName + "']");
						objParentDisp = (XmlElement)objAvlDisp.SelectSingleNode("./DisplayCategory[@name='" + sCatName + "']");
						if(objParentSrch==null )
						{
							//Add Display Category for Search
							objParentSrch=p_objXmlDoc.CreateElement("DisplayCategory");
							objParentSrch.SetAttribute("id",objRdr.GetInt32("FIELD_TYPE").ToString());
							objParentSrch.SetAttribute("name",sCatName);
							objParentSrch.SetAttribute("table",objRdr.GetString("FIELD_TABLE").ToString());
							objAvlSrch.AppendChild(objParentSrch);
						}
						if(objParentDisp==null)
						{
							//Add Display Category for Display
							objParentDisp=p_objXmlDoc.CreateElement("DisplayCategory");
							objParentDisp.SetAttribute("id",objRdr.GetInt32("FIELD_TYPE").ToString());
							objParentDisp.SetAttribute("name",sCatName);
							objParentDisp.SetAttribute("table",objRdr.GetString("FIELD_TABLE").ToString());
							objAvlDisp.AppendChild(objParentDisp);
						}
						if(objRdr.GetInt32("FIELD_TYPE")!=8)
						{	
							int iLen = sCatName.Length;
							if(iLen>6)
								iLen = 6;
							if(sCatName.Substring(0,iLen).ToUpper()=="POLICY")
							{
								if(bUseEnhPol)
								{
									if(sFieldType.Substring(sFieldType.Length - 4,4)=="_ENH" )//Modified by Shivendu.
									{
										objNewElm=p_objXmlDoc.CreateElement("DisplayField");
										objNewElm.SetAttribute("id",objRdr.GetInt32("FIELD_ID").ToString());
										sFieldName=objRdr.GetString("FIELD_DESC").ToString();
										sFieldName=sFieldName.Replace("_"," ");
										objNewElm.SetAttribute("name",sFieldName);
										objNewElm.SetAttribute("suppflag", "0");
										objNewElm.SetAttribute("querytype", objRdr.GetInt32("FIELD_TYPE").ToString());
										objParentSrch.AppendChild(objNewElm);
										objParentDisp.AppendChild(objNewElm);
									}
								}
								else //enhpolicy else
								{
                                    if (sFieldType.Substring(sFieldType.Length - 4, 4) != "_ENH")//Modified by Shivendu.
									{
										objNewElm=p_objXmlDoc.CreateElement("DisplayField");
										objNewElm.SetAttribute("id",objRdr.GetInt32("FIELD_ID").ToString());
										sFieldName=objRdr.GetString("FIELD_DESC").ToString();
										sFieldName=sFieldName.Replace("_"," ");
										objNewElm.SetAttribute("name",sFieldName);
										objNewElm.SetAttribute("suppflag", "0");
										objNewElm.SetAttribute("querytype", objRdr.GetInt32("FIELD_TYPE").ToString());
										objParentSrch.AppendChild(objNewElm);
										objParentDisp.AppendChild(objNewElm);
									}
								}//enhpolicy else end
							}
							else //policy
							{
								//Add Display Field
								objNewElm=p_objXmlDoc.CreateElement("DisplayField");
								objNewElm.SetAttribute("id",objRdr.GetInt32("FIELD_ID").ToString());
								sFieldName=objRdr.GetString("FIELD_DESC").ToString();
								sFieldName=sFieldName.Replace("_"," ");
								objNewElm.SetAttribute("name",sFieldName);
								objNewElm.SetAttribute("suppflag", "0");
								objNewElm.SetAttribute("querytype", objRdr.GetInt32("FIELD_TYPE").ToString());
								objParentSrch.AppendChild(objNewElm);
								objParentDisp.AppendChild(objNewElm);
							}//policy end
						}
						else//field type 8
						{
							
                            //Supplemental fields
                            //Modified by Shivendu for Supplemental Grid. Field type 17(Grid) should not be included
							sSQL = "SELECT FIELD_ID,USER_PROMPT FROM" +
								" SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + objRdr.GetString("FIELD_TABLE") + "'" +
                                " AND FIELD_TYPE <> 7 AND FIELD_TYPE <> 20 AND FIELD_TYPE <> 4 AND FIELD_TYPE <> 17 AND " + 
								" FIELD_TYPE <> 11 AND FIELD_TYPE <> 5 ORDER BY USER_PROMPT";
					
							objRdrSupp=DbFactory.GetDbReader(m_sConnStr,sSQL);
							while(objRdrSupp.Read())
							{
								sFieldName = objRdrSupp.GetString("USER_PROMPT");
								sFieldName=sFieldName.Replace("_"," ");
								iFieldId = objRdrSupp.GetInt32("FIELD_ID");
								objNewElm=p_objXmlDoc.CreateElement("DisplayField");
								objNewElm.SetAttribute("id",iFieldId.ToString());
								sFieldName=sFieldName.Replace("_"," ");
								objNewElm.SetAttribute("name",sFieldName);
								objNewElm.SetAttribute("suppflag", "-1");
								objNewElm.SetAttribute("querytype", "null");
								objParentSrch.AppendChild(objNewElm);
								objParentDisp.AppendChild(objNewElm);
							}
						}//field type 8 end
					}//while ends
				}
				else//cat id if else
				{
					//Case of Admin Tracking					
                    objCache = new LocalCache(m_sConnStr, m_iClientId); 
					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AdmTable']/level[@selected='1']");
					if(objElm!=null)
						iTableId = Conversion.ConvertStrToInteger(objElm.GetAttribute("id"));
					sTableName =  objCache.GetTableName(iTableId); 
					objCache.Dispose();

					objParentSrch = (XmlElement)objAvlSrch.SelectSingleNode("./DisplayCategory[@name='" + sTableName + "']");
					objParentDisp = (XmlElement)objAvlDisp.SelectSingleNode("./DisplayCategory[@name='" + sTableName + "']");

					if(objParentSrch==null )
					{
						//Add Display Category for Search
						objParentSrch=p_objXmlDoc.CreateElement("DisplayCategory");
						objParentSrch.SetAttribute("id",iTableId.ToString());
						objParentSrch.SetAttribute("name",sTableName);
						objAvlSrch.AppendChild(objParentSrch);
					}
					if(objParentDisp==null)
					{
						//Add Display Category for Display
						objParentDisp=p_objXmlDoc.CreateElement("DisplayCategory");
						objParentDisp.SetAttribute("id",iTableId.ToString());
						objParentDisp.SetAttribute("name",sTableName);
						objAvlDisp.AppendChild(objParentDisp);
					}

					sSQL = "SELECT FIELD_ID,USER_PROMPT FROM" +
						" SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sTableName + "'" +
						" AND FIELD_TYPE <> 7 AND FIELD_TYPE <> 20 AND FIELD_TYPE <> 4 AND FIELD_TYPE <> 11 AND FIELD_TYPE <> 5" + 
						" ORDER BY USER_PROMPT";
					objRdrSupp = DbFactory.GetDbReader(m_sConnStr,sSQL);
					while(objRdrSupp.Read())
					{
						sFieldName = objRdrSupp.GetString("USER_PROMPT");
						sFieldName=sFieldName.Replace("_"," ");
						iFieldId = objRdrSupp.GetInt32("FIELD_ID");
						objNewElm=p_objXmlDoc.CreateElement("DisplayField");
						objNewElm.SetAttribute("id",iFieldId.ToString());
						objNewElm.SetAttribute("name",sFieldName.ToString());
						objNewElm.SetAttribute("suppflag", "-1");
						objNewElm.SetAttribute("querytype", "null");
						objParentSrch.AppendChild(objNewElm);
						objParentDisp.AppendChild(objNewElm);
					}
				}//catid else end

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label2']");
				objFirstAccessStep2 = (XmlElement)p_objXmlDoc.SelectSingleNode("//Step2/control[@name='FirstAccess']");
				objFirstAccessStep3 = (XmlElement)p_objXmlDoc.SelectSingleNode("//Step4/control[@name='FirstAccess']");

				//Populate the existing views
				if (iViewId > 0)
				{
					// If its the first access
					if (objFirstAccessStep2.InnerText == "true" || objFirstAccessStep3.InnerText == "true")
					{
						// If the search type selected is same as the original type the populate from the database
						if (objElm.InnerText == p_objXmlDoc.SelectSingleNode("//control[@name='OriginalType']").InnerText)
						{
							// Check whether the admin table has changed or not in case of admin search.
							if (!(iCatId == 20 && p_objXmlDoc.SelectSingleNode("//control[@name='Label3']").InnerText == "changed"))
							{
								//Existing View
								objFldsToDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FldsToDisp']");
								objFldsToSrch = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FldsToSrch']");

								//Populate the Selected Fields
								sSQL = "SELECT SEARCH_VIEW_DEF.SEQ_NUM AS SEQNUM, SEARCH_VIEW_DEF.FIELD_ID, USER_PROMPT  FIELD_DESC, ITEM_TYPE, SUPP_FIELD_FLAG, QUERY_FIELD_TYPE" +
									" FROM SEARCH_VIEW_DEF, SUPP_DICTIONARY" +
									" WHERE SEARCH_VIEW_DEF.FIELD_ID=SUPP_DICTIONARY.FIELD_ID AND SUPP_FIELD_FLAG=-1 AND SEARCH_VIEW_DEF.VIEW_ID = " + iViewId;

								if (iCatId != 20)
									sSQL = sSQL + "UNION SELECT SEARCH_VIEW_DEF.SEQ_NUM AS SEQNUM, SEARCH_VIEW_DEF.FIELD_ID, FIELD_DESC, ITEM_TYPE, SUPP_FIELD_FLAG, QUERY_FIELD_TYPE" +
										" FROM SEARCH_VIEW_DEF, SEARCH_DICTIONARY" +
										" WHERE SEARCH_VIEW_DEF.FIELD_ID=SEARCH_DICTIONARY.FIELD_ID AND SEARCH_VIEW_DEF.VIEW_ID = " + iViewId;
							
								sSQL = sSQL + " ORDER BY SEQNUM";
								objRdr = DbFactory.GetDbReader(m_sConnStr,sSQL);
								sFieldName="";
								while(objRdr.Read())
								{
									if(objRdr.GetInt32("ITEM_TYPE")==0 && objFirstAccessStep2.InnerText == "true")
									{
										objNewElm=p_objXmlDoc.CreateElement("FieldToSearch");
										objNewElm.SetAttribute("id", objRdr.GetInt32("FIELD_ID").ToString());
										sFieldName=objRdr.GetString("FIELD_DESC").ToString();
										sFieldName=sFieldName.Replace("_"," ");
										objNewElm.SetAttribute("name",sFieldName);
										objNewElm.SetAttribute("itemtype",objRdr.GetInt32("ITEM_TYPE").ToString());
										objNewElm.SetAttribute("suppflag",objRdr.GetInt32("SUPP_FIELD_FLAG").ToString());
										objNewElm.SetAttribute("querytype",objRdr.GetInt32("QUERY_FIELD_TYPE").ToString());
										objFldsToSrch.AppendChild(objNewElm);
									}
									else if (objRdr.GetInt32("ITEM_TYPE")!=0 && objFirstAccessStep3.InnerText == "true")
									{
										objNewElm=p_objXmlDoc.CreateElement("FieldToDisplay");
										objNewElm.SetAttribute("id", objRdr.GetInt32("FIELD_ID").ToString());
										sFieldName=objRdr.GetString("FIELD_DESC").ToString();
										sFieldName=sFieldName.Replace("_"," ");
										objNewElm.SetAttribute("name",sFieldName);
										objNewElm.SetAttribute("itemtype",objRdr.GetInt32("ITEM_TYPE").ToString());
										objNewElm.SetAttribute("suppflag",objRdr.GetInt32("SUPP_FIELD_FLAG").ToString());
										objNewElm.SetAttribute("querytype",objRdr.GetInt32("QUERY_FIELD_TYPE").ToString());
										objFldsToDisp.AppendChild(objNewElm);
									}
									sFieldName="";
								}// end -while
							}
						}
					}
				}

				// If the search type is not changed the populate from list
				if (objElm.InnerText == p_objXmlDoc.SelectSingleNode("//control[@name='OriginalType']").InnerText)
				{
					//Geeta 10-Oct-06 : If check removed for fixing MITS no 7940
					//if (!(iCatId == 20 && p_objXmlDoc.SelectSingleNode("//control[@name='Label3']").InnerText == "changed"))
					//{
						if(objFirstAccessStep2.InnerText == "false")
						{
							// for fields to search
							objFldsToSrch = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FldsToSrch']");
							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SrchList']");
							if (objElm != null)
							{
								sCodeIds = objElm.InnerText;
								if (sCodeIds.Trim() != "")
								{
									arrCodes = new string[1];
									if(sCodeIds.IndexOf("|") > 0)
										arrCodes = sCodeIds.Split('|');
									else
										arrCodes[0] = sCodeIds;

									for(int i = 0; i < arrCodes.Length; i++)  
									{
										sTemp = arrCodes[i].Trim();
										arrAttributes = new string[1];
										if(sTemp.IndexOf("_") > 0)
											arrAttributes = sTemp.Split('_');
										else
											arrAttributes[0] = sTemp;

										objNewElm=p_objXmlDoc.CreateElement("FieldToSearch");
										objNewElm.SetAttribute("id", arrAttributes[0]);
										objNewElm.SetAttribute("name",arrAttributes[1]);
										objNewElm.SetAttribute("itemtype","0");
										objNewElm.SetAttribute("suppflag", arrAttributes[2]);
										objNewElm.SetAttribute("querytype",arrAttributes[3]);

										objFldsToSrch.AppendChild(objNewElm);
									}
								}
							}				
						}
						if(objFirstAccessStep3.InnerText == "false")
						{

							//Existing View
							objFldsToDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FldsToDisp']");
							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DispList']");
							if (objElm != null)
							{
								sCodeIds = objElm.InnerText;
								if (sCodeIds.Trim() != "")
								{
									arrCodes = new string[1];
									if(sCodeIds.IndexOf("|") > 0)
										arrCodes = sCodeIds.Split('|');
									else
										arrCodes[0] = sCodeIds;

									for(int i = 0; i < arrCodes.Length; i++)  
									{
										sTemp = arrCodes[i].Trim();
										arrAttributes = new string[1];
										if(sTemp.IndexOf("_") > 0)
											arrAttributes = sTemp.Split('_');
										else
											arrAttributes[0] = sTemp;

										objNewElm=p_objXmlDoc.CreateElement("FieldToDisplay");
										objNewElm.SetAttribute("id", arrAttributes[0]);
										objNewElm.SetAttribute("name",arrAttributes[1]);
										objNewElm.SetAttribute("itemtype","1");
										objNewElm.SetAttribute("suppflag", arrAttributes[2]);
										objNewElm.SetAttribute("querytype",arrAttributes[3]);

										objFldsToDisp.AppendChild(objNewElm);
									}
								}
							}
						}
						//*********************************************************************
						//******************** Mohit for Bug No. 000214 ***********************
						//*********************************************************************
						if (iViewId == 0)
						{
							if(objFirstAccessStep3.InnerText == "true")
							{
								//Step3 Opened first time
							
								objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SrchList']");
								objFldsToDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FldsToDisp']");
								if (objElm != null)
								{
									sCodeIds = objElm.InnerText;
									if (sCodeIds.Trim() != "")
									{
										arrCodes = new string[1];
										if(sCodeIds.IndexOf("|") > 0)
											arrCodes = sCodeIds.Split('|');
										else
											arrCodes[0] = sCodeIds;

										for(int i = 0; i < arrCodes.Length; i++)  
										{
											sTemp = arrCodes[i].Trim();
											arrAttributes = new string[1];
											if(sTemp.IndexOf("_") > 0)
												arrAttributes = sTemp.Split('_');
											else
												arrAttributes[0] = sTemp;

											objNewElm=p_objXmlDoc.CreateElement("FieldToDisplay");
											objNewElm.SetAttribute("id", arrAttributes[0]);
											objNewElm.SetAttribute("name",arrAttributes[1]);
											objNewElm.SetAttribute("itemtype","0");
											objNewElm.SetAttribute("suppflag", arrAttributes[2]);
											objNewElm.SetAttribute("querytype",arrAttributes[3]);

											objFldsToDisp.AppendChild(objNewElm);
										}
									}
								}
							}
						}
						//*********************************************************************
					//}
				}
				
				return p_objXmlDoc;
			}//try end
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                //dvatsa
				throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr",m_iClientId),p_objEx);
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
				if(objRdrSupp!=null)
				{
					objRdrSupp.Dispose();
				}
                if (objCache != null)
                {
                    objCache.Dispose();
                }
				objElm=null;
				objNewElm=null;
				objParentSrch=null;
				objParentDisp=null;
				objSys=null;
				objAvlSrch=null;
				objAvlDisp=null;
				objFldsToSrch=null;
				objFldsToDisp=null;
                objFirstAccessStep2 = null;
                objFirstAccessStep3 = null;
                
			}
		}
     
        
        /// <summary>
        /// Created By parijat --to meet the r5 needs
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument GetStep2n3(XmlDocument p_objXmlDoc)
        {
            int iViewId = 0;
            int iCatId = 0;
            int iFieldId = 0;
            int iTableId = 0;
            string sSQL = "";
            string sFieldName = "";
            string sFieldType = "";
            string sTableName = "";
            //Start : Sumit Kumar. 07/23/2010
            //bool bUseEnhPol = false;
            bool bUseEnhPolForGL = false;
            bool bUseEnhPolForAL = false;
            bool bUseEnhPolForPC = false;
            bool bUseEnhPolForWC = false;

            const int LOBFORGC = 241;
            const int LOBFORWC = 243;
            const int LOBFORVA = 242;
            const int LOBFORPC = 845;
            ColLobSettings objLobSettings = null;
            //End: Sumit

            XmlElement objElm = null;
            XmlElement objNewElm = null;
            XmlElement objParentSrch = null;
            XmlElement objParentDisp = null;
            DbReader objRdr = null;
            DbReader objRdrSupp = null;
            SysSettings objSys = null;
            LocalCache objCache = null;
            XmlElement objAvlSrch = null;
            XmlElement objAvlDisp = null;
            XmlElement objFldsToSrch = null;
            XmlElement objFldsToDisp = null;
            string sCatName = "";
            XmlElement objChkDisplay = null; // Multi Currency
            ArrayList sMultiCurr = new ArrayList(); // Multi Currency

            XmlElement objFirstAccessStep2 = null;
            XmlElement objFirstAccessStep3 = null;
            string sCodeIds = "";
            string[] arrCodes = { "" };
            string[] arrAttributes = { "" };
            string sTemp = "";
            bool bUseMulAddresses = false;//Added Rakhi For R7:Add Emp Data Elements
            bool bUseEntityApproval = false; //Entity approval field will be removed from query designer based on this value
            string sFieldTable = string.Empty;//Added Rakhi For R7:Add Emp Data Elements
            List<int> arrAddressFields=new List<int>();//Added Rakhi For R7:Add Emp Data Elements
            bool bAddressField = false;//Added Rakhi For R7:Add Emp Data Elements
            string sCarrierClaimSet = string.Empty;
            string sLangCode = string.Empty;
            const string SearchKeyword = "srh";            
            string sPageId = string.Empty;
            Dictionary<string, string> strDictParams2n3 = new Dictionary<string, string>();
            Dictionary<string, string> strDictResourceValues = new Dictionary<string, string>();
            string sResourceVal = string.Empty;
            int iLangCode = 0;
            StringBuilder sql = null;

            try
            {
                sLangCode = ((XmlElement)p_objXmlDoc.SelectSingleNode("//LangId")).InnerText;
                objElm = ((XmlElement)p_objXmlDoc.SelectSingleNode("//PageId"));
                if (objElm != null)
                    sPageId = objElm.InnerText;
                iLangCode = Convert.ToInt32(sLangCode);
                objSys = new SysSettings(m_sConnStr,m_iClientId);//dvatsa
                //Start : Sumit Kumar. 07/23/2010
                //bUseEnhPol = Conversion.ConvertLongToBool(objSys.UseEnhPolFlag);
                objLobSettings = new ColLobSettings(m_sConnStr,m_iClientId);
                bUseEnhPolForAL = objLobSettings[LOBFORVA].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolForGL = objLobSettings[LOBFORGC].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolForPC = objLobSettings[LOBFORPC].UseEnhPolFlag == -1 ? true : false;
                bUseEnhPolForWC = objLobSettings[LOBFORWC].UseEnhPolFlag == -1 ? true : false;
                //End: Sumit

                bUseMulAddresses = objSys.UseMultipleAddresses; //Added Rakhi For R7:Add Emp Data Elements
                bUseEntityApproval = objSys.UseEntityApproval;
                //skhare7 R8 enhancement changed the code of Amitosh
                sCarrierClaimSet = objSys.MultiCovgPerClm.ToString();
                //skhare7 R8 enhancement changed the code of Amitosh End
                //objSys = null;
                //Get the view ID if its a existing view
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
                if (objElm != null)
                    iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);
                //Get CAT_ID. It should be populated both in new and edit cases
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectedSearchTypeId");
                if (objElm != null)
                    iCatId = Conversion.ConvertStrToInteger(objElm.InnerText); 
                objAvlSrch = (XmlElement)p_objXmlDoc.SelectSingleNode("//FldsAvlSrch");//categories
                objAvlDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//FldsAvlDisp");//categories + fields to display
                objChkDisplay = (XmlElement)p_objXmlDoc.SelectSingleNode("//ChkDisplay");//selected fields to display and not search 
                if (!strDictParams2n3.ContainsKey("PageId"))
                    strDictParams2n3.Add("PageId", sPageId);
                sSQL = string.Format("SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND LANGUAGE_ID={0}", Convert.ToInt32(iLangCode));
                objRdr = DbFactory.ExecuteReader(m_sViewConnString, sSQL.ToString(), strDictParams2n3);
                while (objRdr.Read())
                {
                    strDictResourceValues.Add(Conversion.ConvertObjToStr(objRdr.GetValue(0)), Conversion.ConvertObjToStr(objRdr.GetValue(1)));
                }
                objRdr.Close();
                if (iCatId != 20)
                {
                    //Deb MITS 27773
                    //if (objSys.UseMultiCurrency == -1)   // if multi currency on
                    //{
                        sSQL = "SELECT DISPLAY_CAT,FIELD_ID,FIELD_DESC,FIELD_TYPE,FIELD_TABLE,FIELD_NAME FROM SEARCH_DICTIONARY" +
                          " WHERE CAT_ID = " + iCatId + " ORDER BY DISPLAY_CAT,FIELD_DESC";
                    //}
                    //else
                    //{
                    //    sSQL = "SELECT DISPLAY_CAT,FIELD_ID,FIELD_DESC,FIELD_TYPE,FIELD_TABLE,FIELD_NAME FROM SEARCH_DICTIONARY" +
                    //        " WHERE CAT_ID = " + iCatId + " and FIELD_NAME not in ('CLAIM_CURRENCY_AMOUNT','PMT_CURRENCY_AMOUNT'," +
                    //        "'PMT_CURRENCY_INVOICE_AMOUNT','PMT_CURRENCY_AUTO_DISCOUNT','CLAIM_CURRENCY_INVOICE_AMOUNT','CLAIM_CURRENCY_AUTO_DISCOUNT') ORDER BY DISPLAY_CAT,FIELD_DESC";
                    //}
                    //objSys = null;
                    //Deb MITS 27773

                    objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                    while (objRdr.Read())
                    {
                        //Deb MITS 27773
                        if (objSys.UseMultiCurrency == 0 && (objRdr.GetString("FIELD_NAME").ToString() == "CLAIM_CURRENCY_AMOUNT" || objRdr.GetString("FIELD_NAME").ToString() == "PMT_CURRENCY_AMOUNT" || objRdr.GetString("FIELD_NAME").ToString() == "PMT_CURRENCY_INVOICE_AMOUNT" || objRdr.GetString("FIELD_NAME").ToString() == "PMT_CURRENCY_AUTO_DISCOUNT" || objRdr.GetString("FIELD_NAME").ToString() == "CLAIM_CURRENCY_INVOICE_AMOUNT" || objRdr.GetString("FIELD_NAME").ToString() == "CLAIM_CURRENCY_AUTO_DISCOUNT"))
                        {
                            continue;
                        }
                        //Deb MITS 27773
                        //rupal:start, display first & final feild in search wizard only if carrier claim is on
                        if (iCatId == 7 && objRdr.GetString("FIELD_NAME").ToString() == "IS_FIRST_FINAL" && sCarrierClaimSet == "0")
                        {
                            continue;
                        }
                        //rupal:end

                        sCatName = objRdr.GetString("DISPLAY_CAT").ToString();
                        //Added by Amitosh for R8 enhancement of PropertyLoss
                        if (sCatName == "Property Info" && sCarrierClaimSet == "-1")
                        {
                            continue;
                        }
                        if (sCatName == "Property Information" && sCarrierClaimSet == "0")
                        {
                            continue;
                        }      //end Amitosh
                      //  skhare7  R8 enhancement
                        if ((sCatName == "Liability Info" && sCarrierClaimSet == "0"))
                        {
                            continue;
                        }
                        //End
                        sFieldType = objRdr.GetString("FIELD_TABLE").ToString();

                        if (objRdr.GetString("FIELD_NAME").ToString().ToUpper()=="ENTITY_APPROVAL_STATUS" && !bUseEntityApproval)
                        {
                            arrAddressFields.Add(objRdr.GetInt32("FIELD_ID"));
                            continue;
                        }
                        //Added Rakhi For R7:Add Emp Data Elements:For Preventing Addresses to be loaded in Category if setting is off
                        if (String.Compare(sFieldType,"entity_x_addresses",true)==0 && !bUseMulAddresses) 
                        {
                            switch (iCatId)
                            {
                                case 3:
                                case 4:
                                case 8:
                                case 9:
                                case 10:
                                    arrAddressFields.Add(objRdr.GetInt32("FIELD_ID"));
                                    continue;

                            }
                        }
                        //Added Rakhi For R7:Add Emp Data Elements
                        objParentSrch = (XmlElement)objAvlSrch.SelectSingleNode("./DisplayCategory[@defaultname='" + sCatName + "']");
                        objParentDisp = (XmlElement)objAvlDisp.SelectSingleNode("./DisplayCategory[@defaultname='" + sCatName + "']");

                        // Ishan : Multi Currency: Added arraylist to show following fields in display and not in search                        
                        sMultiCurr.Add("PMT_CURRENCY_AMOUNT");
                        sMultiCurr.Add("CLAIM_CURRENCY_AMOUNT");
                        sMultiCurr.Add("PMT_CURRENCY_INVOICE_AMOUNT");
                        sMultiCurr.Add("PMT_CURRENCY_AUTO_DISCOUNT");
                        sMultiCurr.Add("CLAIM_CURRENCY_INVOICE_AMOUNT");
                        sMultiCurr.Add("CLAIM_CURRENCY_AUTO_DISCOUNT");

                        if (!strDictParams2n3.ContainsKey("PageId"))
                            strDictParams2n3.Add("PageId", sPageId);
                        if (objParentSrch == null)
                        {
                            //Add Display Category for Search
                            objParentSrch = p_objXmlDoc.CreateElement("DisplayCategory");
                            objParentSrch.SetAttribute("id", objRdr.GetInt32("FIELD_TYPE").ToString());
                            strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                            objParentSrch.SetAttribute("name", sResourceVal != null ? sResourceVal : sCatName);
                            objParentSrch.SetAttribute("defaultname", sCatName);                            
                            objParentSrch.SetAttribute("table", objRdr.GetString("FIELD_TABLE").ToString());
                            objAvlSrch.AppendChild(objParentSrch);
                        }
                        if (objParentDisp == null)
                        {
                            //Add Display Category for Display
                            objParentDisp = p_objXmlDoc.CreateElement("DisplayCategory");
                            objParentDisp.SetAttribute("id", objRdr.GetInt32("FIELD_TYPE").ToString());
                            strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                            objParentDisp.SetAttribute("name", sResourceVal != null ? sResourceVal : sCatName);
                            objParentDisp.SetAttribute("defaultname", sCatName);                            
                            objParentDisp.SetAttribute("table", objRdr.GetString("FIELD_TABLE").ToString());
                            objAvlDisp.AppendChild(objParentDisp);
                        }
                        if (objRdr.GetInt32("FIELD_TYPE") != 8)
                        {
                            int iLen = sCatName.Length;
                            if (iLen > 6)
                                iLen = 6;
                            if (sCatName.Substring(0, iLen).ToUpper() == "POLICY")
                            {
                                //Start : Sumit Kumar. 07/23/2010
                                //if (bUseEnhPol)
                                //if (bUseEnhPolForAL && bUseEnhPolForGL && bUseEnhPolForPC && bUseEnhPolForWC)
                                if (bUseEnhPolForAL || bUseEnhPolForGL || bUseEnhPolForPC || bUseEnhPolForWC)  //Changed by Amitosh for MITS 27531  
                                //End: Sumit
                                {
                                    if (sFieldType.Substring(sFieldType.Length - 4, 4) == "_ENH")
                                    {
                                        objNewElm = p_objXmlDoc.CreateElement("DisplayField");
                                        objNewElm.SetAttribute("id", objRdr.GetInt32("FIELD_ID").ToString());
                                        sFieldName = objRdr.GetString("FIELD_DESC").ToString();
                                        sFieldName = sFieldName.Replace("_", " ");
                                        strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                        objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sFieldName);
                                        objNewElm.SetAttribute("suppflag", "0");
                                        objNewElm.SetAttribute("querytype", objRdr.GetInt32("FIELD_TYPE").ToString());
                                        objParentSrch.AppendChild(objNewElm);
                                        objParentDisp.AppendChild(objNewElm);
                                    }
                                }
                                //else //enhpolicy else
                                if (!bUseEnhPolForAL || !bUseEnhPolForGL || !bUseEnhPolForPC || !bUseEnhPolForWC)  //Changed by Amitosh for MITS 27531  
                                {
                                    if (sFieldType.Substring(sFieldType.Length - 4, 4) != "_ENH")//Modified by Shivendu.
                                    {
                                        objNewElm = p_objXmlDoc.CreateElement("DisplayField");
                                        objNewElm.SetAttribute("id", objRdr.GetInt32("FIELD_ID").ToString());
                                        sFieldName = objRdr.GetString("FIELD_DESC").ToString();
                                        sFieldName = sFieldName.Replace("_", " ");
                                        strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                        objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sFieldName);
                                        objNewElm.SetAttribute("suppflag", "0");
                                        objNewElm.SetAttribute("querytype", objRdr.GetInt32("FIELD_TYPE").ToString());
                                        objParentSrch.AppendChild(objNewElm);
                                        objParentDisp.AppendChild(objNewElm);
                                    }
                                }//enhpolicy else end
                            }
                            else //policy
                            {
                                //Add Display Field
                                objNewElm = p_objXmlDoc.CreateElement("DisplayField");
                                objNewElm.SetAttribute("id", objRdr.GetInt32("FIELD_ID").ToString());
                                sFieldName = objRdr.GetString("FIELD_DESC").ToString();
                                sFieldName = sFieldName.Replace("_", " ");
                                strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sFieldName);
                                objNewElm.SetAttribute("suppflag", "0");
                                objNewElm.SetAttribute("querytype", objRdr.GetInt32("FIELD_TYPE").ToString());
                                // Ishan : Multi Currency
                                if (!sMultiCurr.Contains(objRdr.GetString("FIELD_NAME").ToString()) || objChkDisplay.InnerText == Boolean.TrueString)
                                {
                                    objParentSrch.AppendChild(objNewElm);
                                    objParentDisp.AppendChild(objNewElm);
                                }
                            }//policy end
                        }
                        else//field type 8
                        {

                            //Supplemental fields
                            //Modified by Shivendu for Supplemental Grid. Field type 17(Grid) should not be included
                            // npadhy - RMA-6346 Supplemental field which are deleted Should not be available, for including them into Search Query Criteria
                            //asharma326 added case 25 for excluding HTML Text JIRA 6422
                            //agupta298 added case 25 for excluding Hyperlink JIRA 4691
                            sSQL = "SELECT FIELD_ID,USER_PROMPT FROM" +
                                " SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + objRdr.GetString("FIELD_TABLE") + "'" +
                                //MGaba2:MITS 16476 Added search for Multi Text/Codes field type in supplementals
                                //" AND FIELD_TYPE <> 7 AND FIELD_TYPE <> 20 AND FIELD_TYPE <> 4 AND FIELD_TYPE <> 17 AND " +                                
                                //" FIELD_TYPE <> 11 AND FIELD_TYPE <> 5 ORDER BY USER_PROMPT";
                                " AND FIELD_TYPE <> 7 AND FIELD_TYPE <> 20 AND FIELD_TYPE <> 4 AND FIELD_TYPE <> 17 AND FIELD_TYPE<> 25 AND FIELD_TYPE<> 23 AND DELETE_FLAG <> -1" +
                                 " ORDER BY USER_PROMPT";

                            objRdrSupp = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            while (objRdrSupp.Read())
                            {
                                sFieldName = objRdrSupp.GetString("USER_PROMPT");
                                sFieldName = sFieldName.Replace("_", " ");
                                iFieldId = objRdrSupp.GetInt32("FIELD_ID");
                                objNewElm = p_objXmlDoc.CreateElement("DisplayField");
                                objNewElm.SetAttribute("id", iFieldId.ToString());
                                sFieldName = sFieldName.Replace("_", " ");
                                strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sFieldName);
                                objNewElm.SetAttribute("suppflag", "-1");
                                objNewElm.SetAttribute("querytype", "null");
                                // Ishan : Multi Currency
                                if (!sMultiCurr.Contains(objRdr.GetString("FIELD_NAME").ToString()) || objChkDisplay.InnerText == Boolean.TrueString)
                                {
                                    objParentSrch.AppendChild(objNewElm);
                                    objParentDisp.AppendChild(objNewElm);
                                }
                            }
                        }//field type 8 end
                    }//while ends
                    //Deb MITS 27773
                    objSys = null;
                }
                else//cat id if else
                {
                    //Case of Admin Tracking					
                    objCache = new LocalCache(m_sConnStr, m_iClientId);
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AdmTableSelectedId");
                    if (objElm != null)
                        iTableId = Conversion.ConvertStrToInteger(objElm.InnerText);
                    sTableName = objCache.GetTableName(iTableId);
                    objCache.Dispose();

                    //Added:Yukti, JIRA 4327
                    //objParentSrch = (XmlElement)objAvlSrch.SelectSingleNode("./DisplayCategory[@name='" + sTableName + "']");
                    //objParentDisp = (XmlElement)objAvlDisp.SelectSingleNode("./DisplayCategory[@name='" + sTableName + "']");
                    objParentSrch = (XmlElement)objAvlSrch.SelectSingleNode("./DisplayCategory[@defaultname='" + sTableName + "']");
                    objParentDisp = (XmlElement)objAvlDisp.SelectSingleNode("./DisplayCategory[@defaultname='" + sTableName + "']");
                    //Ended:Yukti, JIRA 4327

                    if (objParentSrch == null)
                    {
                        //Add Display Category for Search
                        objParentSrch = p_objXmlDoc.CreateElement("DisplayCategory");
                        objParentSrch.SetAttribute("id", iTableId.ToString());
                        //Added:Yukti, JIRA 4327
                        //objParentSrch.SetAttribute("name", sTableName);
                        strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sTableName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                        objParentSrch.SetAttribute("name", sResourceVal != null ? sResourceVal : sTableName);
                        //Ended:Yukti, JIRA 4327
                        objParentSrch.SetAttribute("defaultname", sTableName);       
                        objAvlSrch.AppendChild(objParentSrch);
                    }
                    if (objParentDisp == null)
                    {
                        //Add Display Category for Display
                        objParentDisp = p_objXmlDoc.CreateElement("DisplayCategory");
                        objParentDisp.SetAttribute("id", iTableId.ToString());
                        //Added:Yukti, JIRA 4327
                        //objParentDisp.SetAttribute("name", sTableName);
                        strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sTableName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                        objParentDisp.SetAttribute("name", sResourceVal != null ? sResourceVal : sTableName);
                        //Added:Yukti, JIRA 4327
                        objParentDisp.SetAttribute("defaultname", sTableName); 
                        objAvlDisp.AppendChild(objParentDisp);
                    }

                    // npadhy - RMA-6346 Supplemental field which are deleted Should not be available, for including them into Search Query Criteria
                    ////Added:Yukti, JIRA 4327, Added SUPP_TABLE_NAME in query
                    // condition FIELD_TYPE<> 17 has been added by Nitin in order to filter Supplimental Grid field
                    // for Mits 16301 on 07/05/2009
                    //asharma326 added case 25 for excluding HTML Text
                    //agupta298 added case 23 for excluding Hyperlink JIRA 4691
                    sSQL = "SELECT FIELD_ID,USER_PROMPT, SUPP_TABLE_NAME FROM" +
                        " SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = '" + sTableName + "'" +
                        //MGaba2:MITS 16476 Added search for Multi Text/Codes field type in supplementals
                        " AND FIELD_TYPE <> 7 AND FIELD_TYPE <> 20 AND FIELD_TYPE <> 4 " +
                        //" AND FIELD_TYPE <> 7 AND FIELD_TYPE <> 20 AND FIELD_TYPE <> 4 AND FIELD_TYPE <> 11 AND FIELD_TYPE <> 5" +
                        " AND FIELD_TYPE<> 17 AND FIELD_TYPE<> 25 AND FIELD_TYPE<> 23 AND DELETE_FLAG <> -1 ORDER BY USER_PROMPT";

                    objRdrSupp = DbFactory.GetDbReader(m_sConnStr, sSQL);
                    while (objRdrSupp.Read())
                    {
                        sFieldName = objRdrSupp.GetString("USER_PROMPT");
                        sFieldName = sFieldName.Replace("_", " ");
                        iFieldId = objRdrSupp.GetInt32("FIELD_ID");
                        objNewElm = p_objXmlDoc.CreateElement("DisplayField");
                        objNewElm.SetAttribute("id", iFieldId.ToString());
                        //Added:Yukti, JIRA 4327
                        //objNewElm.SetAttribute("name", sFieldName.ToString());
                        strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(objRdrSupp.GetString("SUPP_TABLE_NAME"), "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                        objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sFieldName);
                        //Ended:Yukti, JIRA 4327
                        objNewElm.SetAttribute("suppflag", "-1");
                        objNewElm.SetAttribute("querytype", "null");
                        objParentSrch.AppendChild(objNewElm);
                        objParentDisp.AppendChild(objNewElm);
                    }
                }//catid else end

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Label2");//value which is selected currently as a search type
                objFirstAccessStep2 = (XmlElement)p_objXmlDoc.SelectSingleNode("//Step2FirstAccess");
                objFirstAccessStep3 = (XmlElement)p_objXmlDoc.SelectSingleNode("//Step4FirstAccess");

                //Populate the existing views
                if (iViewId > 0 || iViewId <= -100) //akaur9 ViewId <= -100 used for the Mobile searches like Claim Search hard coded in base sql.)
                {
                    // If its the first access
                    if (objFirstAccessStep2.InnerText == "true" || objFirstAccessStep3.InnerText == "true")
                    {
                        // If the search type selected is same as the original type the populate from the database
                        if (objElm.InnerText == p_objXmlDoc.SelectSingleNode("//OriginalType").InnerText)//value which was being populated before changing the searchType to something else
                        {
                            // Check whether the admin table has changed or not in case of admin search.
                            if (!(iCatId == 20 && p_objXmlDoc.SelectSingleNode("//Label3").InnerText == "changed"))//admin tracking change
                            {
                                //Existing View
                                objFldsToDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//FldsToDisp");//fields which have to be displayed on step 3
                                objFldsToSrch = (XmlElement)p_objXmlDoc.SelectSingleNode("//FldsToSrch");//fields which have to be searched for step 3

                                //Populate the Selected Fields
                                //Added DISPLAY_CAT field in the query, JIRA 2170
                                sql = new StringBuilder();
                                sql = sql.Append("SELECT SEARCH_VIEW_DEF.SEQ_NUM AS SEQNUM, SEARCH_VIEW_DEF.FIELD_ID,USER_PROMPT FIELD_DESC, ITEM_TYPE, SUPP_FIELD_FLAG, QUERY_FIELD_TYPE ");
                                if (iCatId !=20) 
                                {
                                sql=sql.Append(",'' DISPLAY_CAT ");
                                }
                                sql=sql.Append(" FROM SEARCH_VIEW_DEF ");
                                // npadhy - RMA-6346 Supplemental field which are deleted Should not be available, for including them into Search Query Criteria
                                // While updating the view. If the fields were present in the Search View, they should not appear any more.
                                sql=sql.Append(" JOIN SUPP_DICTIONARY ON SEARCH_VIEW_DEF.FIELD_ID=SUPP_DICTIONARY.FIELD_ID AND SUPP_FIELD_FLAG=-1 AND DELETE_FLAG <> -1");
                                //if (iCatId !=20) 
                                //{
                                //sql=sql.Append(" JOIN SEARCH_DICTIONARY ON SEARCH_VIEW_DEF.FIELD_ID=SEARCH_DICTIONARY.FIELD_ID AND  SUPP_FIELD_FLAG=0");
                                //}
                                sql=sql.AppendFormat(" AND SEARCH_VIEW_DEF.VIEW_ID ={0}",iViewId);
                               // sSQL = sql.ToString();
                                if (iCatId != 20)
                                {
                                    sql = sql.Append(" UNION SELECT SEARCH_VIEW_DEF.SEQ_NUM AS SEQNUM, SEARCH_VIEW_DEF.FIELD_ID, FIELD_DESC, ITEM_TYPE, SUPP_FIELD_FLAG, QUERY_FIELD_TYPE,SEARCH_DICTIONARY.DISPLAY_CAT");
                                    sql = sql.Append(" FROM SEARCH_VIEW_DEF, SEARCH_DICTIONARY");
                                    sql = sql.AppendFormat(" WHERE SEARCH_VIEW_DEF.FIELD_ID=SEARCH_DICTIONARY.FIELD_ID AND SUPP_FIELD_FLAG=0 AND SEARCH_VIEW_DEF.VIEW_ID ={0} ", iViewId);
                                }
                                sql = sql.Append(" ORDER BY SEQNUM");
                                objRdr = DbFactory.GetDbReader(m_sConnStr, sql.ToString());
                                sFieldName = "";
                                while (objRdr.Read())
                                {
                                   //Added Rakhi for R7:Add Emp Data Elements-To Prevent Address Fields from an exisiting Search to be displayed if the setting is off.
                                    if (!bUseMulAddresses)
                                    {
                                        bAddressField=false;
                                        switch (iCatId)
                                        {
                                            case 3:
                                            case 4:
                                            case 8:
                                            case 9:
                                            case 10:
                                                foreach(int iAddressFieldId in arrAddressFields)
                                                {
                                                    if (iAddressFieldId == objRdr.GetInt32("FIELD_ID"))
                                                    {
                                                        bAddressField = true;
                                                        break;
                                                    }
                                                }
                                                if (bAddressField)
                                                { continue; }
                                                break;

                                        }
                                    }
                                    //Added Rakhi for R7:Add Emp Data Elements-To Prevent Address Fields from an exisiting Search to be displayed if the setting is off.
                                    if (objRdr.GetInt32("ITEM_TYPE") == 0 && objFirstAccessStep2.InnerText == "true")
                                    {
                                        objNewElm = p_objXmlDoc.CreateElement("FieldToSearch");
                                        objNewElm.SetAttribute("id", objRdr.GetInt32("FIELD_ID").ToString());
                                        sFieldName = objRdr.GetString("FIELD_DESC").ToString();
                                        sFieldName = sFieldName.Replace("_", " ");
                                        if (iCatId != 20)
                                        {
                                            if (!string.IsNullOrEmpty(objRdr.GetString("DISPLAY_CAT")) && objRdr.GetString("DISPLAY_CAT") != null && iCatId != 20)
                                            {
                                                sCatName = (iCatId != 20 ? objRdr.GetString("DISPLAY_CAT") : sTableName);
                                                strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                            }
                                            else
                                            {
                                                sResourceVal = null;
                                            }
                                        }
                                        else
                                        {
                                            sCatName = (iCatId != 20 ? objRdr.GetString("DISPLAY_CAT") : sTableName);
                                            strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                        }
                                      
                                            objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sFieldName);
                                            objNewElm.SetAttribute("itemtype", objRdr.GetInt32("ITEM_TYPE").ToString());
                                            objNewElm.SetAttribute("suppflag", objRdr.GetInt32("SUPP_FIELD_FLAG").ToString());
                                            if (objRdr.GetInt32("SUPP_FIELD_FLAG") != -1)
                                                objNewElm.SetAttribute("querytype", objRdr.GetInt32("QUERY_FIELD_TYPE").ToString());
                                            else
                                                objNewElm.SetAttribute("querytype", "null");
                                            objFldsToSrch.AppendChild(objNewElm);
                                            
                                        
                                    }
                                    else if (objRdr.GetInt32("ITEM_TYPE") != 0 && objFirstAccessStep3.InnerText == "true")
                                    {
                                        objNewElm = p_objXmlDoc.CreateElement("FieldToDisplay");
                                        objNewElm.SetAttribute("id", objRdr.GetInt32("FIELD_ID").ToString());
                                        sFieldName = objRdr.GetString("FIELD_DESC").ToString();
                                        sFieldName = sFieldName.Replace("_", " ");
                                        if (iCatId != 20)
                                        {
                                            if (!string.IsNullOrEmpty(objRdr.GetString("DISPLAY_CAT")) && objRdr.GetString("DISPLAY_CAT") != null && iCatId != 20)
                                            {
                                                sCatName = (iCatId != 20 ? objRdr.GetString("DISPLAY_CAT") : sTableName);
                                                strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                            }
                                            else
                                            {
                                                sResourceVal = null;
                                            }
                                        }
                                        else
                                        {
                                            sCatName = (iCatId != 20 ? objRdr.GetString("DISPLAY_CAT") : sTableName);
                                            strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(sCatName, "[^a-zA-Z0-9]+", "").Replace(" ", "") + Regex.Replace(sFieldName, "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                                        }
                                        

                                        objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : sFieldName);
                                        objNewElm.SetAttribute("itemtype", objRdr.GetInt32("ITEM_TYPE").ToString());
                                        objNewElm.SetAttribute("suppflag", objRdr.GetInt32("SUPP_FIELD_FLAG").ToString());
                                        if (objRdr.GetInt32("SUPP_FIELD_FLAG") != -1)
                                            objNewElm.SetAttribute("querytype", objRdr.GetInt32("QUERY_FIELD_TYPE").ToString());
                                        else
                                            objNewElm.SetAttribute("querytype", "null");
                                        objFldsToDisp.AppendChild(objNewElm);
                                    }
                                    sFieldName = "";
                                }// end -while
                            }
                        }
                    }
                }

                // If the search type is not changed the populate from list
                if (objElm.InnerText == p_objXmlDoc.SelectSingleNode("//OriginalType").InnerText)//value which was being populated before changing the searchType to something else
                {
                    //Geeta 10-Oct-06 : If check removed for fixing MITS no 7940
                    //if (!(iCatId == 20 && p_objXmlDoc.SelectSingleNode("//control[@name='Label3']").InnerText == "changed"))
                    //{
                    if (objFirstAccessStep2.InnerText == "false")
                    {
                        // for fields to search
                        objFldsToSrch = (XmlElement)p_objXmlDoc.SelectSingleNode("//FldsToSrch");//fields which are needed to be searched for step 3 
                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SrchList");//collection of fieklds that need to be searched
                        if (objElm != null)
                        {
                            sCodeIds = objElm.InnerText;
                            if (sCodeIds.Trim() != "")
                            {
                                arrCodes = new string[1];
                                if (sCodeIds.IndexOf("|") > 0)
                                    arrCodes = sCodeIds.Split('|');
                                else
                                    arrCodes[0] = sCodeIds;

                                for (int i = 0; i < arrCodes.Length; i++)
                                {
                                    sTemp = arrCodes[i].Trim();
                                    arrAttributes = new string[1];
                                    if (sTemp.IndexOf("_") > 0)
                                        arrAttributes = sTemp.Split('_');
                                    else
                                        arrAttributes[0] = sTemp;

                                    objNewElm = p_objXmlDoc.CreateElement("FieldToSearch");
                                    objNewElm.SetAttribute("id", arrAttributes[0]);
                                    objNewElm.SetAttribute("name", arrAttributes[1]);
                                    objNewElm.SetAttribute("itemtype", "0");
                                    objNewElm.SetAttribute("suppflag", arrAttributes[2]);
                                    objNewElm.SetAttribute("querytype", arrAttributes[3]);

                                    objFldsToSrch.AppendChild(objNewElm);
                                }
                            }
                        }
                    }
                    if (objFirstAccessStep3.InnerText == "false")
                    {

                        //Existing View
                        objFldsToDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//FldsToDisp");//which need to be searched for 4th step
                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DispList");//list of fields that need to be searched for step 4
                        if (objElm != null)
                        {
                            sCodeIds = objElm.InnerText;
                            if (sCodeIds.Trim() != "")
                            {
                                arrCodes = new string[1];
                                if (sCodeIds.IndexOf("|") > 0)
                                    arrCodes = sCodeIds.Split('|');
                                else
                                    arrCodes[0] = sCodeIds;

                                for (int i = 0; i < arrCodes.Length; i++)
                                {
                                    sTemp = arrCodes[i].Trim();
                                    arrAttributes = new string[1];
                                    if (sTemp.IndexOf("_") > 0)
                                        arrAttributes = sTemp.Split('_');
                                    else
                                        arrAttributes[0] = sTemp;

                                    objNewElm = p_objXmlDoc.CreateElement("FieldToDisplay");
                                    objNewElm.SetAttribute("id", arrAttributes[0]);
                                    objNewElm.SetAttribute("name", arrAttributes[1]);
                                    objNewElm.SetAttribute("itemtype", "1");
                                    objNewElm.SetAttribute("suppflag", arrAttributes[2]);
                                    objNewElm.SetAttribute("querytype", arrAttributes[3]);

                                    objFldsToDisp.AppendChild(objNewElm);
                                }
                            }
                        }
                    }
                    //*********************************************************************
                    //******************** Mohit for Bug No. 000214 ***********************
                    //*********************************************************************
                    if (iViewId == 0)
                    {
                        if (objFirstAccessStep3.InnerText == "true")
                        {
                            //Step3 Opened first time

                            objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SrchList");
                            objFldsToDisp = (XmlElement)p_objXmlDoc.SelectSingleNode("//FldsToDisp");
                            if (objElm != null)
                            {
                                sCodeIds = objElm.InnerText;
                                if (sCodeIds.Trim() != "")
                                {
                                    arrCodes = new string[1];
                                    if (sCodeIds.IndexOf("|") > 0)
                                        arrCodes = sCodeIds.Split('|');
                                    else
                                        arrCodes[0] = sCodeIds;

                                    for (int i = 0; i < arrCodes.Length; i++)
                                    {
                                        sTemp = arrCodes[i].Trim();
                                        arrAttributes = new string[1];
                                        if (sTemp.IndexOf("_") > 0)
                                            arrAttributes = sTemp.Split('_');
                                        else
                                            arrAttributes[0] = sTemp;

                                        objNewElm = p_objXmlDoc.CreateElement("FieldToDisplay");
                                        objNewElm.SetAttribute("id", arrAttributes[0]);
                                        objNewElm.SetAttribute("name", arrAttributes[1]);
                                        objNewElm.SetAttribute("itemtype", "0");
                                        objNewElm.SetAttribute("suppflag", arrAttributes[2]);
                                        objNewElm.SetAttribute("querytype", arrAttributes[3]);

                                        objFldsToDisp.AppendChild(objNewElm);
                                    }
                                }
                            }
                        }
                    }
                    //*********************************************************************
                    //}
                }

                return p_objXmlDoc;
            }//try end
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr",m_iClientId), p_objEx);
            }
            finally
            {
                if (objRdr != null)
                {
                    objRdr.Dispose();
                }
                if (objRdrSupp != null)
                {
                    objRdrSupp.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objElm = null;
                objNewElm = null;
                objParentSrch = null;
                objParentDisp = null;
                objSys = null;
                objAvlSrch = null;
                objAvlDisp = null;
                objFldsToSrch = null;
                objFldsToDisp = null;
                objFirstAccessStep2 = null;
                objFirstAccessStep3 = null;

            }
        }

		/// Name		: GetStep4
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Populates the tab for Users and Groups
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetStep4_Old(XmlDocument p_objXmlDoc)
		{
			string sSQL="";
			int iCatId=0;
			int iViewId=0;
			XmlElement objElm=null;
			XmlElement objRowTxt=null;
			DbReader objRdr=null;

			XmlElement objPrevType = null;
			XmlElement objCurrType = null;
			XmlElement objFirstAccess = null;
			string sCodeIds = "";
			string [] arrCodes = {""};
			string [] arrAttributes = {""};
			string sTemp = "";

			try
			{

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
				if(objElm!=null)
					iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SearchType']/level[@selected='1']");
				if(objElm!=null)
					iCatId = Conversion.ConvertStrToInteger(objElm.GetAttribute("id"));

				LoadGroupUserList(p_objXmlDoc);

				objPrevType = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='OriginalType']");
				objCurrType = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Label2']");
				objFirstAccess = (XmlElement)p_objXmlDoc.SelectSingleNode("//Step5/control[@name='FirstAccess']");

				// If existing view
				if(iViewId>0)
				{
					// If the access is for the first time and type has not changed
					if (objFirstAccess.InnerText == "true" && objPrevType.InnerText == objCurrType.InnerText)
					{
						// If admin search and admin table has changed then default 'All Users' to be selected
						if (!(iCatId == 20 && p_objXmlDoc.SelectSingleNode("//control[@name='Label3']").InnerText == "changed"))
						{
							sSQL = "SELECT * FROM SEARCH_VIEW_PERM WHERE VIEW_ID = " + iViewId + " AND USER_ID = 0 AND GROUP_ID = 0";
							objRdr=DbFactory.GetDbReader(m_sConnStr,sSQL);
							if(objRdr.Read())
							{
								objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AllUsers']");
								if(objElm!=null)
									objElm.SetAttribute("selected","1");  
							}
							else
							{
								objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SelectUsers']");
								if(objElm!=null)
									objElm.SetAttribute("selected","1");


						
								//Load Selected Users and Groups
								sSQL = "SELECT * FROM SEARCH_VIEW_PERM WHERE VIEW_ID = " + iViewId;
								objRdr = DbFactory.GetDbReader(m_sConnStr,sSQL);
								while(objRdr.Read())
								{	
									if(objRdr.GetInt32("USER_ID")!=0)
									{
										objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='UsersPermitted']/listrow");
										XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Users']/listrow/rowtext[@id='" + objRdr.GetInt32("USER_ID").ToString() +"']");
										if (objNewElm != null)
										{
											objRowTxt = p_objXmlDoc.CreateElement("rowtext");
											objRowTxt.SetAttribute("type" , "user");
											objRowTxt.SetAttribute("id" , objRdr.GetInt32("USER_ID").ToString());							
											objRowTxt.SetAttribute("title", objNewElm.GetAttribute("title"));
											objElm.AppendChild(objRowTxt);					
											objNewElm = null;
										}
									}
									else if(objRdr.GetInt32("GROUP_ID")!=0)
									{
										objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='GroupsPermitted']/listrow");
										XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='Groups']/listrow/rowtext[@id='" + objRdr.GetInt32("GROUP_ID").ToString() +"']");
										if (objNewElm != null)
										{
											objRowTxt = p_objXmlDoc.CreateElement("rowtext");
											objRowTxt.SetAttribute("type" , "group");
											objRowTxt.SetAttribute("id" , objRdr.GetInt32("GROUP_ID").ToString());
											objRowTxt.SetAttribute("title", objNewElm.GetAttribute("title"));
											objElm.AppendChild(objRowTxt);					
											objNewElm = null;
										}
									}
								}
								objRdr.Dispose();
							}
						}
						else
						{
							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AllUsers']");
							if(objElm!=null)
								objElm.SetAttribute("selected","1"); 
						}
					}
						// else if the access is not first but the search type has not changed load from the list
					else if (objFirstAccess.InnerText == "false" && objPrevType.InnerText == objCurrType.InnerText)
					{

						objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='UsersPermitted']/listrow");
						XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='UserGroupList']");
						if (objNewElm != null)
						{
							sCodeIds = objNewElm.InnerText;
							if (sCodeIds.Trim() != "")
							{
								arrCodes = new string[1];
								if(sCodeIds.IndexOf("|") > 0)
									arrCodes = sCodeIds.Split('|');
								else
									arrCodes[0] = sCodeIds;

								for(int i = 0; i < arrCodes.Length; i++)  
								{
									sTemp = arrCodes[i].Trim();
									arrAttributes = new string[1];
									if(sTemp.IndexOf("_") > 0)
										arrAttributes = sTemp.Split('_');
									else
										arrAttributes[0] = sTemp;

									objRowTxt=p_objXmlDoc.CreateElement("rowtext");
									objRowTxt.SetAttribute("type", arrAttributes[0]);
									objRowTxt.SetAttribute("id",arrAttributes[1]);
									objRowTxt.SetAttribute("title", arrAttributes[2]);

									objElm.AppendChild(objRowTxt);
								}
							}
						}
						objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AllUsers']");
						if (objElm.InnerText == "SelectUsers")
							objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SelectUsers']");
						if(objElm!=null)
							objElm.SetAttribute("selected","1");
					}
					else
					{
						objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AllUsers']");
						if(objElm!=null)
							objElm.SetAttribute("selected","1"); 
					}
				}
					// if not an existing view
				else if(objFirstAccess.InnerText == "false" && objPrevType.InnerText == objCurrType.InnerText)
				{
					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='UsersPermitted']/listrow");
					XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='UserGroupList']");
					if (objNewElm != null)
					{
						sCodeIds = objNewElm.InnerText;
						if (sCodeIds.Trim() != "")
						{
							arrCodes = new string[1];
							if(sCodeIds.IndexOf("|") > 0)
								arrCodes = sCodeIds.Split('|');
							else
								arrCodes[0] = sCodeIds;

							for(int i = 0; i < arrCodes.Length; i++)  
							{
								sTemp = arrCodes[i].Trim();
								arrAttributes = new string[1];
								if(sTemp.IndexOf("_") > 0)
									arrAttributes = sTemp.Split('_');
								else
									arrAttributes[0] = sTemp;

								objRowTxt=p_objXmlDoc.CreateElement("rowtext");
								objRowTxt.SetAttribute("type", arrAttributes[0]);
								objRowTxt.SetAttribute("id",arrAttributes[1]);
								objRowTxt.SetAttribute("title", arrAttributes[2]);

								objElm.AppendChild(objRowTxt);
							}
						}
					}
					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AllUsers']");
					if (objElm.InnerText == "SelectUsers")
						objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SelectUsers']");
					if(objElm!=null)
						objElm.SetAttribute("selected","1");
				}
				else
				{
					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AllUsers']");
					if(objElm!=null)
						objElm.SetAttribute("selected","1"); 
				}
				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr",m_iClientId),p_objEx);//dvatsa
			}
			finally
			{
				objElm=null;
				objRowTxt=null;
                objPrevType = null;
                objCurrType = null;
                objFirstAccess = null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
			}
		}

        /// <summary>
        /// Created By Parijat --Inorder to meet the R5 needs
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument GetStep4(XmlDocument p_objXmlDoc)
        {
            string sSQL = "";
            int iCatId = 0;
            int iViewId = 0;
            XmlElement objElm = null;
            XmlElement objRowTxt = null;
            DbReader objRdr = null;

            XmlElement objPrevType = null;
            XmlElement objCurrType = null;
            XmlElement objFirstAccess = null;
            string sCodeIds = "";
            string[] arrCodes = { "" };
            string[] arrAttributes = { "" };
            string sTemp = "";

            try
            {

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
                if (objElm != null)
                    iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectedSearchTypeId");
                if (objElm != null)
                    iCatId = Conversion.ConvertStrToInteger(objElm.InnerText);

                LoadGroupUserList(p_objXmlDoc);

                objPrevType = (XmlElement)p_objXmlDoc.SelectSingleNode("//OriginalType");
                objCurrType = (XmlElement)p_objXmlDoc.SelectSingleNode("//Label2");
                objFirstAccess = (XmlElement)p_objXmlDoc.SelectSingleNode("//Step5FirstAccess");

                // If existing view
                if (iViewId > 0 || iViewId <= -100) //akaur9 Mobile Adjuster -- ViewId <= -100 used for the Mobile searches like Claim Search, PI Search.. hard coded in base sql.)
                {
                    // If the access is for the first time and type has not changed
                    if (objFirstAccess.InnerText == "true" && objPrevType.InnerText == objCurrType.InnerText)
                    {
                        // If admin search and admin table has changed then default 'All Users' to be selected
                        if (!(iCatId == 20 && p_objXmlDoc.SelectSingleNode("//Label3").InnerText == "changed"))
                        {
                            sSQL = "SELECT * FROM SEARCH_VIEW_PERM WHERE VIEW_ID = " + iViewId + " AND USER_ID = 0 AND GROUP_ID = 0";
                            objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                            if (objRdr.Read())
                            {
                                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsersSelected");
                                if (objElm != null)
                                    objElm.InnerText = "1";
                            }
                            else
                            {
                                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectUsersSelected");
                                if (objElm != null)
                                    objElm.InnerText = "1";



                                //Load Selected Users and Groups
                                sSQL = "SELECT * FROM SEARCH_VIEW_PERM WHERE VIEW_ID = " + iViewId;
                                objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                                while (objRdr.Read())
                                {
                                    if (objRdr.GetInt32("USER_ID") != 0)
                                    {
                                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersPermitted/listrow");
                                        XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Users/listrow/rowtext[@id='" + objRdr.GetInt32("USER_ID").ToString() + "']");
                                        if (objNewElm != null)
                                        {
                                            objRowTxt = p_objXmlDoc.CreateElement("rowtext");
                                            objRowTxt.SetAttribute("type", "user");
                                            objRowTxt.SetAttribute("id", objRdr.GetInt32("USER_ID").ToString());
                                            objRowTxt.SetAttribute("title", objNewElm.GetAttribute("title"));
                                            objElm.AppendChild(objRowTxt);
                                            objNewElm = null;
                                        }
                                    }
                                    else if (objRdr.GetInt32("GROUP_ID") != 0)
                                    {
                                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//GroupsPermitted/listrow");
                                        XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Groups/listrow/rowtext[@id='" + objRdr.GetInt32("GROUP_ID").ToString() + "']");
                                        if (objNewElm != null)
                                        {
                                            objRowTxt = p_objXmlDoc.CreateElement("rowtext");
                                            objRowTxt.SetAttribute("type", "group");
                                            objRowTxt.SetAttribute("id", objRdr.GetInt32("GROUP_ID").ToString());
                                            objRowTxt.SetAttribute("title", objNewElm.GetAttribute("title"));
                                            objElm.AppendChild(objRowTxt);
                                            objNewElm = null;
                                        }
                                    }
                                }
                                objRdr.Dispose();
                            }
                        }
                        else
                        {
                            objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsersSelected");
                            if (objElm != null)
                                objElm.InnerText =  "1";
                        }
                    }
                    // else if the access is not first but the search type has not changed load from the list
                    else if (objFirstAccess.InnerText == "false" && objPrevType.InnerText == objCurrType.InnerText)
                    {

                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersPermitted/listrow");
                        XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UserGroupList");
                        if (objNewElm != null)
                        {
                            sCodeIds = objNewElm.InnerText;
                            if (sCodeIds.Trim() != "")
                            {
                                arrCodes = new string[1];
                                if (sCodeIds.IndexOf("|") > 0)
                                    arrCodes = sCodeIds.Split('|');
                                else
                                    arrCodes[0] = sCodeIds;

                                for (int i = 0; i < arrCodes.Length; i++)
                                {
                                    sTemp = arrCodes[i].Trim();
                                    arrAttributes = new string[1];
                                    if (sTemp.IndexOf("_") > 0)
                                        arrAttributes = sTemp.Split('_');
                                    else
                                        arrAttributes[0] = sTemp;

                                    objRowTxt = p_objXmlDoc.CreateElement("rowtext");
                                    objRowTxt.SetAttribute("type", arrAttributes[0]);
                                    objRowTxt.SetAttribute("id", arrAttributes[1]);
                                    objRowTxt.SetAttribute("title", arrAttributes[2]);

                                    objElm.AppendChild(objRowTxt);
                                }
                            }
                        }
                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsers");
                        if (objElm.InnerText == "SelectUsers")
                            objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectUsersSelected");
                        else
                            objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsersSelected");
                        
                        if (objElm != null)
                            objElm.InnerText = "1";
                    }
                    else
                    {
                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsersSelected");
                        if (objElm != null)
                            objElm.InnerText= "1";
                    }
                }
                // if not an existing view
                else if (objFirstAccess.InnerText == "false" && objPrevType.InnerText == objCurrType.InnerText)
                {
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UsersPermitted/listrow");
                    XmlElement objNewElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UserGroupList");
                    if (objNewElm != null)
                    {
                        sCodeIds = objNewElm.InnerText;
                        if (sCodeIds.Trim() != "")
                        {
                            arrCodes = new string[1];
                            if (sCodeIds.IndexOf("|") > 0)
                                arrCodes = sCodeIds.Split('|');
                            else
                                arrCodes[0] = sCodeIds;

                            for (int i = 0; i < arrCodes.Length; i++)
                            {
                                sTemp = arrCodes[i].Trim();
                                arrAttributes = new string[1];
                                if (sTemp.IndexOf("_") > 0)
                                    arrAttributes = sTemp.Split('_');
                                else
                                    arrAttributes[0] = sTemp;

                                objRowTxt = p_objXmlDoc.CreateElement("rowtext");
                                objRowTxt.SetAttribute("type", arrAttributes[0]);
                                objRowTxt.SetAttribute("id", arrAttributes[1]);
                                objRowTxt.SetAttribute("title", arrAttributes[2]);

                                objElm.AppendChild(objRowTxt);
                            }
                        }
                    }
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsers");
                    if (objElm.InnerText == "SelectUsers")
                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelectUsersSelected");
                    else
                        objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsersSelected");

                    if (objElm != null)
                        objElm.InnerText = "1";
                }
                else
                {
                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsersSelected");
                    if (objElm != null)
                        objElm.InnerText = "1";
                }
                return p_objXmlDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                objRowTxt = null;
                objPrevType = null;
                objCurrType = null;
                objFirstAccess = null;
                if (objRdr != null)
                {
                    objRdr.Dispose();
                }
            }
        }
		/// Name		: GetAdmTrckStep
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Populates the tab for Admin Tracking
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument GetAdmTrckStep_Old(XmlDocument p_objXmlDoc)
		{
			XmlElement objElm = null;
			XmlElement objNewElm = null;
			Array arrObj =null;
			LocalCache objCache=null;
			DbReader objRdr=null;
			string sSQL = "";
			string sTableName="";
			int iViewId=0;
			int iTableId=0;
			try
			{
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
				if(objElm!=null)
					iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);


				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AdmTable']");
				if(objElm!=null)
					sTableName = objElm.GetAttribute("sysname");
				if(sTableName!="")
				{
					//Case of Admin Tracking					
                    objCache = new LocalCache(m_sConnStr, m_iClientId);
					iTableId = objCache.GetTableId(sTableName); 
					objCache.Dispose();
				}

				sSQL = @"SELECT GLOSSARY.TABLE_ID,GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME 
							FROM GLOSSARY, GLOSSARY_TEXT WHERE 
							GLOSSARY.GLOSSARY_TYPE_CODE=468  AND 
							GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND 
							GLOSSARY_TEXT.LANGUAGE_CODE=1033 ORDER BY TABLE_NAME";
				objRdr=DbFactory.GetDbReader(m_sConnStr, sSQL);

				while(objRdr.Read())
				{
					objNewElm=p_objXmlDoc.CreateElement("level");
					objNewElm.SetAttribute("id",objRdr.GetInt("TABLE_ID").ToString());
					objNewElm.SetAttribute("name",objRdr.GetString("TABLE_NAME"));
					objNewElm.SetAttribute("sysname", objRdr.GetString("SYSTEM_TABLE_NAME"));
					if(objRdr.GetInt("TABLE_ID").ToString() ==iTableId.ToString() )
						objNewElm.SetAttribute("selected","1");
					else
						objNewElm.SetAttribute("selected","0");
					objElm.AppendChild(objNewElm);
				}
				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                //dvatsa
				throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr",m_iClientId),p_objEx);
			}
			finally
			{	
				objElm = null;
				objNewElm = null;
				arrObj =null;
                if (objCache != null)
                    objCache.Dispose();
                if (objRdr != null)
                    objRdr.Dispose();
               
			}
		}
        /// <summary>
        /// Created by Parijat inorder to meet the r5 needs
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument GetAdmTrckStep(XmlDocument p_objXmlDoc)
        {
            XmlElement objElm = null;
            XmlElement objNewElm = null;
            Array arrObj = null;
            LocalCache objCache = null;
            DbReader objRdr = null;
            string sSQL = "";
            string sTableName = "";
            int iViewId = 0;
            int iTableId = 0;
            string sSQLRetrieve = "";  //mbahl3 mits 23632
            string sQl = ""; //mbahl3 mits 23632
            //Added:Yukti
            string sLangCode = string.Empty;
            const string SearchKeyword = "srh";
            string sPageId = string.Empty;
            Dictionary<string, string> strDictParams2n3 = new Dictionary<string, string>();
            Dictionary<string, string> strDictResourceValues = new Dictionary<string, string>();
            string sResourceVal = string.Empty;
            int iLangCode = 0;
            try
            {
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//ViewID");
                if (objElm != null)
                    iViewId = Conversion.ConvertStrToInteger(objElm.InnerText);

                //Added:Yukti, JIRA 4327
                sLangCode = ((XmlElement)p_objXmlDoc.SelectSingleNode("//LangId")).InnerText;
                objElm = ((XmlElement)p_objXmlDoc.SelectSingleNode("//PageId"));
                if (objElm != null)
                    sPageId = objElm.InnerText;
                iLangCode = Convert.ToInt32(sLangCode);

                if (!strDictParams2n3.ContainsKey("PageId"))
                    strDictParams2n3.Add("PageId", sPageId);
                sSQL = string.Format("SELECT RESOURCE_KEY, RESOURCE_VALUE FROM LOCAL_RESOURCE WHERE PAGE_ID=~PageId~ AND LANGUAGE_ID={0}", Convert.ToInt32(iLangCode));
                objRdr = DbFactory.ExecuteReader(m_sViewConnString, sSQL.ToString(), strDictParams2n3);
                while (objRdr.Read())
                {
                    strDictResourceValues.Add(Conversion.ConvertObjToStr(objRdr.GetValue(0)), Conversion.ConvertObjToStr(objRdr.GetValue(1)));
                }
                objRdr.Close();
                //Ended:Yukti,JIRA 4327
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AdmTable");
                if (objElm != null)
                    sTableName = objElm.GetAttribute("sysname");
                if (sTableName != "")
                {
                    //Case of Admin Tracking					
                    objCache = new LocalCache(m_sConnStr, m_iClientId);
                    iTableId = objCache.GetTableId(sTableName);
                    objCache.Dispose();
                }
                //mbahl3 mits 23632
                //                sSQL = @"SELECT GLOSSARY.TABLE_ID,GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME 
                //							FROM GLOSSARY, GLOSSARY_TEXT WHERE 
                //							GLOSSARY.GLOSSARY_TYPE_CODE=468  AND 
                //							GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND 
                //							GLOSSARY_TEXT.LANGUAGE_CODE=1033 ORDER BY TABLE_NAME";

                sSQLRetrieve = @"SELECT 5000000 + GLOSSARY.TABLE_ID*20 FUNC_ID,GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME
FROM GLOSSARY, GLOSSARY_TEXT WHERE GLOSSARY.GLOSSARY_TYPE_CODE=468  AND 
GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND GLOSSARY_TEXT.LANGUAGE_CODE=1033   ORDER BY TABLE_NAME";
                  objRdr = DbFactory.GetDbReader(m_sConnStr, sSQLRetrieve);
                  StringBuilder sbGetvalue = new StringBuilder(); 
                
               
                             while (objRdr.Read())
                {
                    sbGetvalue.Append(Conversion.ConvertObjToStr(objRdr.GetValue(0)) + ",");
                }
                             string sFuncId = sbGetvalue.ToString();
                if (!string.IsNullOrEmpty(sFuncId))
                {
                    sFuncId = sFuncId.Substring(0, sFuncId.Length - 2);
                }
                sSQL = @" SELECT * FROM GROUP_PERMISSIONS WHERE FUNC_ID IN(" + sFuncId + ")" ;

                     objRdr = DbFactory.GetDbReader(m_sConnStr, sSQL);
                     sQl = @"SELECT GLOSSARY.TABLE_ID, GLOSSARY.SYSTEM_TABLE_NAME, GLOSSARY_TEXT.TABLE_NAME FROM GROUP_PERMISSIONS,GLOSSARY_TEXT,GLOSSARY WHERE GROUP_PERMISSIONS.FUNC_ID =  5000000 + GLOSSARY.TABLE_ID*20	AND	GLOSSARY.GLOSSARY_TYPE_CODE=468 AND GLOSSARY_TEXT.TABLE_ID=GLOSSARY.TABLE_ID AND GLOSSARY_TEXT.LANGUAGE_CODE=1033 AND GROUP_PERMISSIONS.GROUP_ID =" + m_iGroupId + " ORDER BY TABLE_NAME";
                     objRdr = DbFactory.GetDbReader(m_sConnStr, sQl);
                while (objRdr.Read())
                {
                    objNewElm = p_objXmlDoc.CreateElement("level");
                    objNewElm.SetAttribute("id", objRdr.GetInt("TABLE_ID").ToString());
                    //objNewElm.SetAttribute("name", objRdr.GetString("TABLE_NAME"));
                    strDictResourceValues.TryGetValue(SearchKeyword + Regex.Replace(objRdr.GetString("TABLE_NAME"), "[^a-zA-Z0-9]+", "").Replace(" ", ""), out sResourceVal);
                    objNewElm.SetAttribute("name", sResourceVal != null ? sResourceVal : objRdr.GetString("TABLE_NAME"));                            
                    objNewElm.SetAttribute("sysname", objRdr.GetString("SYSTEM_TABLE_NAME"));
                    if (objRdr.GetInt("TABLE_ID").ToString() == iTableId.ToString())
                        objNewElm.SetAttribute("selected", "1");
                    else
                        objNewElm.SetAttribute("selected", "0");
                    objElm.AppendChild(objNewElm);
                }
                //MBAHL3 MITS 23632
                return p_objXmlDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("SearchWizard.Get.GetErr",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                objNewElm = null;
                arrObj = null;
                if (objCache != null)
                    objCache.Dispose();
                if (objRdr != null)
                    objRdr.Dispose();

            }
        }

		/// Name		: LoadGroupUserList
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Populate the Users and Groups List
		/// </summary>
		private void LoadGroupUserList(XmlDocument p_objXmlDoc)
		{
			string sSQL="";
			DbReader objRdr = null;
			XmlElement objRowTxt=null;
			XmlElement objElm=null;
			try
			{
				//Load the Group List
				sSQL = "SELECT GROUP_ID,GROUP_NAME FROM USER_GROUPS";
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Groups/listrow");

				objRdr = DbFactory.GetDbReader(m_sConnStr,sSQL);
				while(objRdr.Read())
				{
					objRowTxt = p_objXmlDoc.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "group");
					objRowTxt.SetAttribute("id" , objRdr.GetInt32("GROUP_ID").ToString());
					objRowTxt.SetAttribute("title", objRdr.GetString("GROUP_NAME"));
					objElm.AppendChild(objRowTxt);
				}
				objRdr.Close(); 			

				//Load the Users List
				sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME " + 
					" FROM USER_DETAILS_TABLE,USER_TABLE" + 
					" WHERE USER_DETAILS_TABLE.DSNID = " + m_iDSNId +    
					" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID";
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//Users/listrow");

				objRdr = DbFactory.GetDbReader(m_sSecDsn  ,sSQL);
				while(objRdr.Read())
				{
					string sLastName = objRdr.GetString("LAST_NAME");
					string sName = objRdr.GetString("FIRST_NAME");
					if(sLastName!="")
						sName = sLastName + ", " + sName;
					objRowTxt = p_objXmlDoc.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "user");
					objRowTxt.SetAttribute("id" , objRdr.GetInt32("USER_ID").ToString());
					objRowTxt.SetAttribute("title", sName);
					objElm.AppendChild(objRowTxt);
				}
			}
			catch(Exception p_objEx)
			{
                //dvatsa
				throw new RMAppException(Globalization.GetString("SearchWizard.LoadGroupUserList.Err",m_iClientId),p_objEx);
			}
			finally
			{
				if(objRdr!=null) 
					objRdr.Dispose(); 
				objRowTxt=null;
				objElm=null;
			}
		}

        /// <summary>
        ///Created by : Parijat-- In order to meet the R5 needs
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument Save(XmlDocument p_objXmlDoc)
        {
            int iViewId = 0;
            int iAdmTableId = 0;
            int iSearchType = 0;
            int iSeqNo = 0;
            string sSearchName = "";
            string sSearchDesc = "";
            string sAdmTable = "";
            int iDefaultCheckBoxValue = 0;
            LocalCache objCache = null;
            XmlElement objElm = null;
            DbConnection objConn = null;
            DbTransaction objTran = null;
            DbCommand objCommand = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnStr);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                objTran = objConn.BeginTransaction();
                objCommand.Transaction = objTran;
                objCommand.CommandType = CommandType.Text;
                if (p_objXmlDoc.SelectSingleNode("//DefaultChkboxValue").InnerText == "True")
                {
                    iDefaultCheckBoxValue = 1;
                }
                iViewId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ViewID").InnerText);
                sSearchName = p_objXmlDoc.SelectSingleNode("//SearchName").InnerText;

                //ddhiman: 01/20/2012: Commented for MITS 27210
 				//sSearchName = sSearchName.Replace("'", "''");  // Added by Charanpreet for MITS 12370
                //End ddhiman

                sSearchDesc = p_objXmlDoc.SelectSingleNode("//SearchDesc").InnerText;
                if (p_objXmlDoc.SelectSingleNode("//SelectedSearchTypeId").InnerText != "")
                        iSearchType = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//SelectedSearchTypeId").InnerText);

                if (iSearchType == 20)
                {
                    objCache = new LocalCache(m_sConnStr, m_iClientId);
                    //objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AdmTable']/level[@selected='1']");AdmTableSelectedId
                    //if (objElm != null)
                    if (p_objXmlDoc.SelectSingleNode("//AdmTableSelectedId").InnerText !="")
                        iAdmTableId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//AdmTableSelectedId").InnerText);
                    sAdmTable = objCache.GetTableName(iAdmTableId);
                    objCache.Dispose();
                }
                if (iViewId == 0)
                    iViewId = Utilities.GetNextUID(m_sConnStr, "SEARCH_VIEW", m_iClientId);

                //Commit SEARCH_VIEW record
                objCommand.CommandText = "DELETE FROM SEARCH_VIEW WHERE VIEW_ID = " + iViewId;
                objCommand.ExecuteNonQuery();
                //User parameterized SQL to avoid SQL injection attack
                objCommand.CommandText = "INSERT INTO SEARCH_VIEW(VIEW_ID,CAT_ID,VIEW_NAME,VIEW_DESC,VIEW_AT_TABLE,CHECKBOX_FLAG, " +
                    "DTTM_RCD_ADDED, DTTM_RCD_LAST_UPD, ADDED_BY_USER, UPDATED_BY_USER) VALUES (" +
                    iViewId + "," + iSearchType + ",~VIEW_NAME~, ~VIEW_DESC~ ,~VIEW_AT_TABLE~," + iDefaultCheckBoxValue +
                    ",'" + Conversion.ToDbDateTime(DateTime.Now) + "', '" + Conversion.ToDbDateTime(DateTime.Now) + "','" +
                    m_sUser + "', '" + m_sUser + "')";
                DbParameter oParam = objCommand.CreateParameter();
                oParam.ParameterName = "VIEW_NAME";
                oParam.Value = sSearchName;
                objCommand.Parameters.Add(oParam);
                oParam = objCommand.CreateParameter();
                oParam.ParameterName = "VIEW_DESC";
                oParam.Value = sSearchDesc;
                objCommand.Parameters.Add(oParam);
                oParam = objCommand.CreateParameter();
                oParam.ParameterName = "VIEW_AT_TABLE";
                oParam.Value = sAdmTable;
                objCommand.Parameters.Add(oParam);
                objCommand.ExecuteNonQuery();

                //Write SEARCH_VIEW_DEF field records
                objCommand.CommandText = "DELETE FROM SEARCH_VIEW_DEF WHERE VIEW_ID = " + iViewId;
                objCommand.ExecuteNonQuery();

                string sTemp = "";
                string sCodeIds = "";
                string[] arrCodes = { "" };
                string[] arrAttributes = { "" };

                //Update the Fields to search
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SrchList");
                if (objElm != null)
                {
                    sCodeIds = objElm.InnerText;
                    if (sCodeIds.Trim() != "")
                    {
                        arrCodes = new string[1];
                        if (sCodeIds.IndexOf("|") > 0)
                            arrCodes = sCodeIds.Split('|');
                        else
                            arrCodes[0] = sCodeIds;

                        for (int i = 0; i < arrCodes.Length; i++)
                        {
                            sTemp = arrCodes[i].Trim();
                            arrAttributes = new string[1];
                            if (sTemp.IndexOf("_") > 0)
                                arrAttributes = sTemp.Split('_');
                            else
                                arrAttributes[0] = sTemp;

                            int iFieldId = Conversion.ConvertStrToInteger(arrAttributes[0]);
                            int iSupFldFlg = Conversion.ConvertStrToInteger(arrAttributes[2]);

                            if (iSupFldFlg == 0)
                            {
                                objCommand.CommandText = "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG, QUERY_FIELD_TYPE) VALUES (" +
                                    iViewId + "," + iFieldId + "," + (iSeqNo + 1) + ",0,0," + Conversion.ConvertStrToInteger(arrAttributes[3]) + ")";
                                objCommand.ExecuteNonQuery();
                            }
                            else
                            {
                                objCommand.CommandText = "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG) VALUES (" +
                                    iViewId + "," + iFieldId + "," + (iSeqNo + 1) + ",0,-1)";
                                objCommand.ExecuteNonQuery();
                            }
                            iSeqNo++;
                        }// end of for
                    }
                }
                iSeqNo = 0;
                //Update the fields to display
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DispList");
                if (objElm != null)
                {
                    sCodeIds = objElm.InnerText;
                    if (sCodeIds.Trim() != "")
                    {
                        arrCodes = new string[1];
                        if (sCodeIds.IndexOf("|") > 0)
                            arrCodes = sCodeIds.Split('|');
                        else
                            arrCodes[0] = sCodeIds;

                        for (int i = 0; i < arrCodes.Length; i++)
                        {
                            sTemp = arrCodes[i].Trim();
                            arrAttributes = new string[1];
                            if (sTemp.IndexOf("_") > 0)
                                arrAttributes = sTemp.Split('_');
                            else
                                arrAttributes[0] = sTemp;

                            int iFieldId = Conversion.ConvertStrToInteger(arrAttributes[0]);
                            int iSupFldFlg = Conversion.ConvertStrToInteger(arrAttributes[2]);

                            if (iSupFldFlg == 0)
                            {
                                //objCommand.CommandText   =  "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG, QUERY_FIELD_TYPE) VALUES (" +
                                //	iViewId + "," + iFieldId + "," + iSeqNo++ + ",1,0," + Conversion.ConvertStrToInteger(arrAttributes[3]) + ")";
                                objCommand.CommandText = "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG, QUERY_FIELD_TYPE) VALUES (" +
                                        iViewId + "," + iFieldId + "," + (iSeqNo + 1) + ",1,0,null" + ")";
                                objCommand.ExecuteNonQuery();
                            }
                            else
                            {
                                objCommand.CommandText = "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG) VALUES (" +
                                    iViewId + "," + iFieldId + "," + (iSeqNo + 1) + ",1,-1)";
                                objCommand.ExecuteNonQuery();
                            }
                            iSeqNo++;
                        }// end of for
                    }
                }

                objCommand.CommandText = "DELETE FROM SEARCH_VIEW_PERM WHERE VIEW_ID = " + iViewId;
                objCommand.ExecuteNonQuery();

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//AllUsers");
                if (objElm.InnerText == "AllUsers")
                {
                    objCommand.CommandText = "INSERT INTO SEARCH_VIEW_PERM(VIEW_ID,USER_ID,GROUP_ID) VALUES (" + iViewId + ",0,0)";
                    objCommand.ExecuteNonQuery();
                }
                else
                {

                    objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//UserGroupList");
                    if (objElm != null)
                    {
                        sCodeIds = objElm.InnerText;
                        if (sCodeIds.Trim() != "")
                        {
                            arrCodes = new string[1];
                            if (sCodeIds.IndexOf("|") > 0)
                                arrCodes = sCodeIds.Split('|');
                            else
                                arrCodes[0] = sCodeIds;

                            for (int i = 0; i < arrCodes.Length; i++)
                            {
                                sTemp = arrCodes[i].Trim();
                                arrAttributes = new string[1];
                                if (sTemp.IndexOf("_") > 0)
                                    arrAttributes = sTemp.Split('_');
                                else
                                    arrAttributes[0] = sTemp;

                                if (arrAttributes[0] == "user")
                                    objCommand.CommandText = "INSERT INTO SEARCH_VIEW_PERM(VIEW_ID,USER_ID,GROUP_ID) VALUES (" +
                                        iViewId + "," + Conversion.ConvertStrToInteger(arrAttributes[1]) + ",0)";
                                else if (arrAttributes[0] == "group")
                                    objCommand.CommandText = "INSERT INTO SEARCH_VIEW_PERM(VIEW_ID,USER_ID,GROUP_ID) VALUES (" +
                                        iViewId + ",0," + Conversion.ConvertStrToInteger(arrAttributes[1]) + ")";

                                objCommand.ExecuteNonQuery();

                            }// end of for
                        }
                    }
                }
                objTran.Commit();
                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                if (objTran != null)
                    objTran.Rollback();
                //dvatsa
                throw new RMAppException(Globalization.GetString("SearchWizard.Save.SaveErr",m_iClientId), p_objEx);
            }
            finally
            {
                objCommand = null;
                if (objTran != null)
                {
                    objTran.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objElm = null;

            }
        }
		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data for Search Wizard
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data</param>
		/// <returns>Returns the saved xml data</returns>
		public XmlDocument Save_Old(XmlDocument p_objXmlDoc)
		{
			int iViewId=0;
			int iAdmTableId=0;
			int iSearchType=0;	
			int iSeqNo=0;
			string sSearchName="";
			string sSearchDesc="";			
			string sAdmTable="";
			LocalCache objCache=null;
			XmlElement objElm=null;
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;

			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnStr);
				objConn.Open();
				objCommand=objConn.CreateCommand();
				objTran=objConn.BeginTransaction();
				objCommand.Transaction=objTran;
				objCommand.CommandType=CommandType.Text;

				iViewId = Conversion.ConvertStrToInteger(p_objXmlDoc.SelectSingleNode("//ViewID").InnerText);
				sSearchName = p_objXmlDoc.SelectSingleNode("//control[@name='SearchName']").InnerText;
				sSearchDesc = p_objXmlDoc.SelectSingleNode("//control[@name='SearchDesc']").InnerText;
                iSearchType = Conversion.ConvertObjToInt(((XmlElement)p_objXmlDoc.SelectSingleNode(", m_iClientId//control[@name='SearchType']/level[@selected='1']")).GetAttribute("id"), m_iClientId); 
				
				if(iSearchType==20)
				{
                    objCache = new LocalCache(m_sConnStr, m_iClientId); 
					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AdmTable']/level[@selected='1']");
					if(objElm!=null)
						iAdmTableId = Conversion.ConvertStrToInteger(objElm.GetAttribute("id"));
					sAdmTable =  objCache.GetTableName(iAdmTableId); 
					objCache.Dispose();
				}
				if(iViewId ==0)
                    iViewId = Utilities.GetNextUID(m_sConnStr, "SEARCH_VIEW", m_iClientId);

				//Commit SEARCH_VIEW record
				objCommand.CommandText="DELETE FROM SEARCH_VIEW WHERE VIEW_ID = " + iViewId;
				objCommand.ExecuteNonQuery();
				objCommand.CommandText="INSERT INTO SEARCH_VIEW(VIEW_ID,CAT_ID,VIEW_NAME,VIEW_DESC,VIEW_AT_TABLE) VALUES (" +
					iViewId  + "," + iSearchType + ",'" + sSearchName  + "','" + sSearchDesc + "','" + sAdmTable + "')";
				objCommand.ExecuteNonQuery();

				//Write SEARCH_VIEW_DEF field records
				objCommand.CommandText="DELETE FROM SEARCH_VIEW_DEF WHERE VIEW_ID = " + iViewId;
				objCommand.ExecuteNonQuery();
				
				string sTemp = "";
				string sCodeIds = "";
				string [] arrCodes = {""};
				string [] arrAttributes = {""};

				//Update the Fields to search
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SrchList']");
				if (objElm != null)
				{
					sCodeIds = objElm.InnerText;
					if (sCodeIds.Trim() != "")
					{
						arrCodes = new string[1];
						if(sCodeIds.IndexOf("|") > 0)
							arrCodes = sCodeIds.Split('|');
						else
							arrCodes[0] = sCodeIds;

						for(int i = 0; i < arrCodes.Length; i++)  
						{
							sTemp = arrCodes[i].Trim();
							arrAttributes = new string[1];
							if(sTemp.IndexOf("_") > 0)
								arrAttributes = sTemp.Split('_');
							else
								arrAttributes[0] = sTemp;

							int iFieldId = Conversion.ConvertStrToInteger(arrAttributes[0]);
							int iSupFldFlg = Conversion.ConvertStrToInteger(arrAttributes[2]);

							if(iSupFldFlg==0)
							{
								objCommand.CommandText   =  "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG, QUERY_FIELD_TYPE) VALUES (" +
									iViewId + "," + iFieldId + "," + (iSeqNo+1) + ",0,0," + Conversion.ConvertStrToInteger(arrAttributes[3]) + ")";
								objCommand.ExecuteNonQuery();
							}
							else
							{         
								objCommand.CommandText = "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG) VALUES (" + 
									iViewId + "," + iFieldId  + "," + (iSeqNo+1) + ",0,-1)";
								objCommand.ExecuteNonQuery();
							}
							iSeqNo++;
						}// end of for
					}
				}
				iSeqNo=0;
				//Update the fields to display
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='DispList']");
				if (objElm != null)
				{
					sCodeIds = objElm.InnerText;
					if (sCodeIds.Trim() != "")
					{
						arrCodes = new string[1];
						if(sCodeIds.IndexOf("|") > 0)
							arrCodes = sCodeIds.Split('|');
						else
							arrCodes[0] = sCodeIds;

						for(int i = 0; i < arrCodes.Length; i++)  
						{
							sTemp = arrCodes[i].Trim();
							arrAttributes = new string[1];
							if(sTemp.IndexOf("_") > 0)
								arrAttributes = sTemp.Split('_');
							else
								arrAttributes[0] = sTemp;

							int iFieldId = Conversion.ConvertStrToInteger(arrAttributes[0]);
							int iSupFldFlg = Conversion.ConvertStrToInteger(arrAttributes[2]);

							if(iSupFldFlg==0)
							{
								//objCommand.CommandText   =  "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG, QUERY_FIELD_TYPE) VALUES (" +
								//	iViewId + "," + iFieldId + "," + iSeqNo++ + ",1,0," + Conversion.ConvertStrToInteger(arrAttributes[3]) + ")";
								objCommand.CommandText   =  "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG, QUERY_FIELD_TYPE) VALUES (" +
										iViewId + "," + iFieldId + "," + (iSeqNo+1) + ",1,0,null" + ")";
								objCommand.ExecuteNonQuery();
							}
							else
							{         
								objCommand.CommandText = "INSERT INTO SEARCH_VIEW_DEF(VIEW_ID,FIELD_ID,SEQ_NUM,ITEM_TYPE,SUPP_FIELD_FLAG) VALUES (" + 
									iViewId + "," + iFieldId  + "," + (iSeqNo+1) + ",1,-1)";
								objCommand.ExecuteNonQuery();
							}
							iSeqNo++;
						}// end of for
					}
				}

				objCommand.CommandText="DELETE FROM SEARCH_VIEW_PERM WHERE VIEW_ID = " + iViewId;
				objCommand.ExecuteNonQuery();

				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='AllUsers']");
				if(objElm.InnerText == "AllUsers")
				{
					objCommand.CommandText = "INSERT INTO SEARCH_VIEW_PERM(VIEW_ID,USER_ID,GROUP_ID) VALUES (" + iViewId  + ",0,0)";
					objCommand.ExecuteNonQuery();
				}
				else
				{

					objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='UserGroupList']");
					if (objElm != null)
					{
						sCodeIds = objElm.InnerText;
						if (sCodeIds.Trim() != "")
						{
							arrCodes = new string[1];
							if(sCodeIds.IndexOf("|") > 0)
								arrCodes = sCodeIds.Split('|');
							else
								arrCodes[0] = sCodeIds;

							for(int i = 0; i < arrCodes.Length; i++)  
							{
								sTemp = arrCodes[i].Trim();
								arrAttributes = new string[1];
								if(sTemp.IndexOf("_") > 0)
									arrAttributes = sTemp.Split('_');
								else
									arrAttributes[0] = sTemp;

								if (arrAttributes[0] == "user")
									objCommand.CommandText = "INSERT INTO SEARCH_VIEW_PERM(VIEW_ID,USER_ID,GROUP_ID) VALUES (" + 
										iViewId  + "," + Conversion.ConvertStrToInteger(arrAttributes[1]) + ",0)";
								else if (arrAttributes[0] == "group")
									objCommand.CommandText = "INSERT INTO SEARCH_VIEW_PERM(VIEW_ID,USER_ID,GROUP_ID) VALUES (" + 
										iViewId  + ",0," + Conversion.ConvertStrToInteger(arrAttributes[1]) + ")";

								objCommand.ExecuteNonQuery();
							
							}// end of for
						}
					}
				}
				objTran.Commit();
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
				if(objTran!=null) 
					objTran.Rollback();
                //dvatsa
				throw new RMAppException(Globalization.GetString("SearchWizard.Save.SaveErr",m_iClientId),p_objEx);
			}
			finally
			{
				objCommand=null;
                if (objTran != null)
                {
                    objTran.Dispose();
                }
				if(objConn!=null) 
				{
					objConn.Dispose(); 
				}
				if(objCache!=null)
				{
					objCache.Dispose();
				}
				objElm=null;
                
			}
		}
	}
}
