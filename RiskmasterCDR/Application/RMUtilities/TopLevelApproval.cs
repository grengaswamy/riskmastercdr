using System;
using System.Xml;
using Riskmaster.DataModel; 
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.Settings;
using System.Text;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21 Mar 2005
	///Purpose :   This class Fetches TopLevelApproval Data.
	/// </summary>
	public class TopLevelApproval:UtilitiesMultiBase 
	{	
		/// <summary>
		/// User ID
		/// </summary>
		private int m_iUserId = 0;

		/// <summary>
		/// DSN ID
		/// </summary>
		private int m_iDSNId = 0;

        private int m_iClientId = 0;

		/// <summary>
		/// String array for Base Class field mapping
		/// </summary>
		private string[,] arrFields = {										 
			{"GCLOB", "LOB_CODE"},
			{"GCUserName","USER_ID"},
			{"GCReserveMax","RESERVE_MAX"},
			{"GCPaymentMax","PAYMENT_MAX"},
            {"GCClaimIncurredMax","CLAIM_INCURRED_MAX"},//Jira 6385- Incurred Limit
			{"VALOB", "LOB_CODE"},
			{"VAUserName","USER_ID"},
			{"VAReserveMax","RESERVE_MAX"},
			{"VAPaymentMax","PAYMENT_MAX"},
            {"VAClaimIncurredMax","CLAIM_INCURRED_MAX"},//Jira 6385- Incurred Limit
			{"WCLOB", "LOB_CODE"},
			{"WCUserName","USER_ID"},
			{"WCReserveMax","RESERVE_MAX"},
			{"WCPaymentMax","PAYMENT_MAX"},
            {"WCClaimIncurredMax","CLAIM_INCURRED_MAX"},//Jira 6385- Incurred Limit
			{"STDLOB", "LOB_CODE"},
			{"STDUserName","USER_ID"},
			{"STDReserveMax","RESERVE_MAX"},
			{"STDPaymentMax","PAYMENT_MAX"},
            {"STDClaimIncurredMax","CLAIM_INCURRED_MAX"},//Jira 6385- Incurred Limit
            //Anu Tennyson MITS 18145 Changes made for LOB
            {"PCLOB", "LOB_CODE"},
			{"PCUserName","USER_ID"},
			{"PCReserveMax","RESERVE_MAX"},
			{"PCPaymentMax","PAYMENT_MAX"},
            {"PCClaimIncurredMax","CLAIM_INCURRED_MAX"},//Jira 6385- Incurred Limit
			{"TLLOB", "LOB_CODE"},
			{"TLUserName","USER_ID"}
									  };

		/// <summary>
		/// String array for Base Class Key fields field mapping
		/// </summary>
		private string[,] arrKeyFields = {										 
			{"241", "GCUserName,GCLOB,GCPaymentMax,GCReserveMax,GCClaimIncurredMax"},//Jira 6385- Incurred Limit
			{"242", "VAUserName,VALOB,VAPaymentMax,VAReserveMax,VAClaimIncurredMax" },//Jira 6385- Incurred Limit
			{"243", "WCUserName,WCLOB,WCPaymentMax,WCReserveMax,WCClaimIncurredMax"},//Jira 6385- Incurred Limit
			{"844", "STDUserName,STDLOB,STDPaymentMax,STDReserveMax,STDClaimIncurredMax"},//Jira 6385- Incurred Limit
            //Anu Tennyson MITS 18145 Changes made for LOB
            {"845", "PCUserName,PCLOB,PCPaymentMax,PCReserveMax,PCClaimIncurredMax"},//Jira 6385- Incurred Limit
			{"-1", "TLLOB,TLUserName"}
			};

//		/// <summary>
//		/// String array for Base Class child field mapping
//		/// </summary>
//		private string[,] arrChilds = {
//			{"GCUserName", "USER_DETAILS_TABLE.USER_ID", "USERNAME", "USER_DETAILS_TABLE,USER_TABLE" , "USER_DETAILS_TABLE.DSNID = {0} AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_TABLE.USER_ID = {1}", "USER_TABLE.FIRST_NAME + ' ' + USER_TABLE.LAST_NAME AS USERNAME"},
//			{"VAUserName", "USER_DETAILS_TABLE.USER_ID", "USERNAME", "USER_DETAILS_TABLE,USER_TABLE" , "USER_DETAILS_TABLE.DSNID = {0} AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_TABLE.USER_ID = {1}", "USER_TABLE.FIRST_NAME + ' ' + USER_TABLE.LAST_NAME AS USERNAME"},
//			{"WCUserName", "USER_DETAILS_TABLE.USER_ID", "USERNAME", "USER_DETAILS_TABLE,USER_TABLE" , "USER_DETAILS_TABLE.DSNID = {0} AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_TABLE.USER_ID = {1}", "USER_TABLE.FIRST_NAME + ' ' + USER_TABLE.LAST_NAME AS USERNAME"},
//			{"STDUserName", "USER_DETAILS_TABLE.USER_ID", "USERNAME", "USER_DETAILS_TABLE,USER_TABLE" , "USER_DETAILS_TABLE.DSNID = {0} AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_TABLE.USER_ID = {1}", "USER_TABLE.FIRST_NAME + ' ' + USER_TABLE.LAST_NAME AS USERNAME"},
//			{"TLUserName", "USER_DETAILS_TABLE.USER_ID", "USERNAME", "USER_DETAILS_TABLE,USER_TABLE" , "USER_DETAILS_TABLE.DSNID = {0} AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_TABLE.USER_ID = {1}", "USER_TABLE.FIRST_NAME + ' ' + USER_TABLE.LAST_NAME AS USERNAME"}
//			};
		

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString"></param>
        public TopLevelApproval(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			ConnectString = p_sConnString;
			this.Initialize(); 
		}

		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sUserId">UserId</param>
		/// <param name="p_sDSNId">DsnId</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public TopLevelApproval(int p_iUserId, int p_iDSNId, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			try
			{
                m_iClientId = p_iClientId;
				m_iUserId = p_iUserId;
				m_iDSNId = p_iDSNId; 
				this.DsnId=p_iDSNId;
				ConnectString = p_sConnectionString;
//				ChildConnectString = Security.SecurityDatabase.GetSecurityDsn(0);       
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("TopLevelApproval.Constructor.Err", m_iClientId),p_objEx); 
			}
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        public TopLevelApproval(int p_iUserId, int p_iDSNId, string p_sConnectionString, string p_sUserName, int p_iClientId)
            : base(p_sConnectionString, p_sUserName, p_iClientId)
        {
            try
            {
                m_iClientId = p_iClientId;
                m_iUserId = p_iUserId;
                m_iDSNId = p_iDSNId;
                this.DsnId = p_iDSNId;
                ConnectString = p_sConnectionString;;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TopLevelApproval.Constructor.Err", m_iClientId), p_objEx);
            }
        }

		/// <summary>
		/// Initialize
		/// </summary>
		new private void Initialize()
		{
			TableName = "LOB_SUP_APPROVAL";
			base.m_sKeyField = "LOB_CODE";
//			base.ChildArray = arrChilds;
			base.InitFields(arrFields);
			base.InitKeyFields( arrKeyFields);
			base.Initialize();
		}

		/// <summary>
		/// Get the data for the TopLevelApproval form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data structure</param>
		/// <returns>Xml document with data and the form structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
            SysSettings objSettings = null;
            StringBuilder sbLOBSelection = null;
            XmlNode objXmlNode = null;
            try
			{
				XMLDoc = p_objXmlDocument;
//				for(int i = 0; i < 5 ;i++)
//				{
//					arrChilds[i,4] = ((string)arrChilds[i,4]).Replace("{0}",m_iDSNId.ToString()); 
//					arrChilds[i,4] = ((string)arrChilds[i,4]).Replace("{1}",m_iUserId.ToString());
//				}
//				base.ChildArray = arrChilds;
				base.Get();
                //Mgaba2: R8:SuperVisory Approval:Start
                //LOB selection on General System Parameters should be taken into consideration
                //In Case Carrier Claims is ON,only GC and WC Lob's should be visible
                objSettings = new SysSettings(ConnectString, m_iClientId);
                sbLOBSelection = new StringBuilder();
                if (!objSettings.UseGCLOB)
                {
                    sbLOBSelection.Append("GC");
                }
                if (!objSettings.UseVALOB || objSettings.MultiCovgPerClm.ToString() == "-1")
                {
                    sbLOBSelection.Append("*VA");
                }
                if (!objSettings.UseWCLOB)
                {
                    sbLOBSelection.Append("*WC");
                }
                if (!objSettings.UseDILOB || objSettings.MultiCovgPerClm.ToString() == "-1")
                {
                    sbLOBSelection.Append("*DI");
                }
                if (!objSettings.UsePCLOB || objSettings.MultiCovgPerClm.ToString() == "-1")
                {
                    sbLOBSelection.Append("*PC");
                }

                objXmlNode = p_objXmlDocument.SelectSingleNode("//LOBsDeselectedInGenSysParms");
                if (objXmlNode != null)
                {
                    objXmlNode.InnerText  = sbLOBSelection.ToString();
                }
                //Mgaba2: R8:SuperVisory Approval:End
				return p_objXmlDocument;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("TopLevelApproval.Get.Error", m_iClientId), p_objExp);   
			}
		}

		/// <summary>
		/// Saves the data in the Database
		/// </summary>
		/// <param name="p_objXmlDocument">XML Doc with Data</param>
		/// <returns>XMl Doc</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Save(true); 
				return XMLDoc;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("TopLevelApproval.Save.Error", m_iClientId), p_objExp);   
			}
		}
	}
}
