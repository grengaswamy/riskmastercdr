using System;
using Riskmaster.Db; 
using System.Xml; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   WPA Auto Diary Setup List Form 
	/// </summary>
	public class WPAAutoDiary : UtilitiesUIListBase 
	{
		/// <summary>
		/// String array for database field mapping
		/// </summary>
		private string[,] arrFields = {
			{"AutoName","AUTO_NAME"}
			};
        private int m_iClientId = 0;
		/// Name			: WPAAutoDiary
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 28 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public WPAAutoDiary(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			ConnectString = p_sConnString;
			this.Initialize(); 
		}

		/// Name			: Initialize
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 28 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize private variables and properties
		/// </summary>
		new private void Initialize()
		{
			TableName = "WPA_AUTO_SET";
			KeyField = "AUTO_ROW_ID";
			RadioName = "WPAAutoDiariesList";
			WhereClause = "1=1";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name			: Get
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 28 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Retrieves the data for the WPA Auto Diary Setup
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Output xml document with data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				return XMLDoc; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAAutoDiary.Get.Err", m_iClientId), p_objEx);
			}
		}

		/// Name			: Delete
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 28 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Deletes a particular Auto Diary Definition
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document</param>
		/// <returns>Returns the list</returns>
		public void Delete(XmlDocument p_objXmlDoc)
		{
			int iAutoId=0;
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			XmlElement objElm=null;
			try
			{
				objElm = (XmlElement) p_objXmlDoc.SelectSingleNode("//AutoDiaryId");
				if(objElm!=null)
					iAutoId = Conversion.ConvertStrToInteger(objElm.InnerText);
					if(iAutoId>0)
					{						
						objConn = DbFactory.GetDbConnection(base.ConnectString);
						objConn.Open();
						objCommand=objConn.CreateCommand();
						objTran=objConn.BeginTransaction();
						objCommand.Transaction=objTran;

						objCommand.CommandText = "DELETE FROM WPA_AUTO_FILTER WHERE AUTO_ID = " + iAutoId;
						objCommand.ExecuteNonQuery();

						objCommand.CommandText = "DELETE FROM WPA_AUTO_ACT WHERE AUTO_ID = " + iAutoId; 
						objCommand.ExecuteNonQuery();

						objCommand.CommandText = "DELETE FROM WPA_AUTO_USER WHERE AUTO_ID = " + iAutoId; 
						objCommand.ExecuteNonQuery();

						objCommand.CommandText = "DELETE FROM WPA_AUTO_SET WHERE AUTO_ROW_ID = " + iAutoId;
						objCommand.ExecuteNonQuery();
				
						objTran.Commit();
					}

			}
			catch(Exception p_objEx)
			{
				if(objTran!=null)
					objTran.Rollback();
                throw new RMAppException(Globalization.GetString("WPAAutoDiary.Delete.Err", m_iClientId), p_objEx);
			}
			finally
			{
				if(objConn != null)
				{
					objConn.Dispose();
					objConn = null;
				}
				if(objTran!=null)
				{
					objTran.Dispose();
					objTran=null;
				}
				objCommand=null;
			}
		}
	}
}