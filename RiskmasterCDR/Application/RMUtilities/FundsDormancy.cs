﻿using System;
using System.Data;
using System.Xml;
using Riskmaster.Db;
using System.Text;
using System.Globalization;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;

namespace Riskmaster.Application.RMUtilities
{
    public class FundsDormancy
    {
        /// Name			: Riskmaster.Application.FundsDormancy
        /// Author			: Rupal Kotak
        /// Date Created	: 05 Sep 2011		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************




        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = string.Empty;

        /// <summary>
        /// Connection String
        /// </summary>
        private string m_sConnectString = string.Empty;

        private int m_iClientId = 0;

        public FundsDormancy(string p_sLoginName, string p_sConnectString, int p_iClientId)
        {
            m_sUserName = p_sLoginName;
            m_sConnectString = p_sConnectString;
            m_iClientId = p_iClientId; 
        }

        /// Name		: Get
        /// Author		: Rupal Kotak
        /// Date Created: 08 Sep 2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Gets the list of Funds Dormancy
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml Template</param>
        /// <returns>XML Document with Fund Dormancy List</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;
            DbReader objRead = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            string sJurisdictionId = string.Empty;
            try
            {
                sSQL = "SELECT FUNDS_DORMANCY.*,STATES.STATE_NAME FROM FUNDS_DORMANCY,STATES WHERE FUNDS_DORMANCY.STATE_ID = STATES.STATE_ROW_ID ORDER BY STATES.STATE_NAME";
                objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//FundsDormancyList");

                while (objRead.Read())
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");

                    //we will use Conversion.ConvertObjToStr insteaof Cpnversion.CastToType as want the output as string and CastToType function accepts string as input
                    objRowTxt = p_objXmlDocument.CreateElement("DormancyRowId");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("DORMANCY_ROW_ID"));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("JurisdictionId");
                    sJurisdictionId = Conversion.ConvertObjToStr(objRead.GetValue("STATE_ID"));
                    objRowTxt.InnerText = sJurisdictionId;
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("Jurisdiction");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("STATE_NAME"));
                    objRowTxt.SetAttribute("codeid", sJurisdictionId);
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("UnclaimedDays");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("UNCLAIMED_DAYS"));
                    //if (objRowTxt.InnerText == "0")
                    //    objRowTxt.InnerText = "";
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("EscheatDays");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("ESCHEAT_DAYS"));
                    //if (objRowTxt.InnerText == "0")
                    //    objRowTxt.InnerText = "";
                    objLstRow.AppendChild(objRowTxt);

                    objElement.AppendChild(objLstRow);
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsDormancy.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                objRead = null;
            }
        }

        /// Name		: Delete
        /// Author		: Rupal Kotak
        /// Date Created: 08 Sep 2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes the specified record based on Dormancy Row Id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml Template</param>
        /// <returns>XML Document</returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            XmlElement objElm = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FundsDormancyGrid']");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objConn = DbFactory.GetDbConnection(m_sConnectString);
                objConn.Open();

                if (sRowId != "")
                {
                    sSQL = "DELETE FROM FUNDS_DORMANCY WHERE DORMANCY_ROW_ID=" + sRowId;
                }
                objConn.ExecuteNonQuery(sSQL);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsDormancy.Delete.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        /// Name		: Save
        /// Author		: Rupal Kotak
        /// Date Created: 08 Sep 2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data alogn with its xml structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            string sJurisdictionId = string.Empty;
            string sUnclaimedDays = string.Empty;
            string sEschetDays = string.Empty;
            string sSQL = string.Empty;
            StringBuilder sbSql = new StringBuilder();
            int iNextUID = 0;
            XmlElement objElm = null;
            DbConnection objConn = null;
            try
            {

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DormancyRowId']");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdiction']");
                if (objElm != null)
                    sJurisdictionId = objElm.GetAttribute("codeid");

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UnclaimedDays']");
                if (objElm != null)
                    sUnclaimedDays = objElm.InnerText;
                if (sUnclaimedDays == string.Empty)
                    sUnclaimedDays = "0";//if no days were defined we set the value zero.zero indicates no days are defined for unclaimed dormancy

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EscheatDays']");
                if (objElm != null)
                    sEschetDays = objElm.InnerText;
                if (sEschetDays == string.Empty)
                    sEschetDays = "0";//if no days were defined we set the value zero.zero indicates no days are defined for escheat dormancy

                objConn = DbFactory.GetDbConnection(m_sConnectString);
                objConn.Open();

                if (sRowId != "")
                {
                    string sLastUpdated = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));
                    sSQL = "UPDATE FUNDS_DORMANCY SET UNCLAIMED_DAYS=" + sUnclaimedDays + ", ESCHEAT_DAYS = " + sEschetDays + " ,DTTM_RCD_LAST_UPD = '" + sLastUpdated + "',UPDATED_BY_USER='" + m_sUserName + "'  WHERE DORMANCY_ROW_ID=" + sRowId;
                }
                else
                {

                    iNextUID = Utilities.GetNextUID(m_sConnectString, "FUNDS_DORMANCY", m_iClientId);
                    sbSql = sbSql.Append("INSERT INTO FUNDS_DORMANCY");
                    sbSql = sbSql.Append("(DORMANCY_ROW_ID,STATE_ID, UNCLAIMED_DAYS, ESCHEAT_DAYS, DTTM_RCD_ADDED , ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER)");
                    sbSql = sbSql.Append(" VALUES(");
                    sbSql = sbSql.Append(iNextUID.ToString());
                    sbSql = sbSql.Append(",");
                    sbSql = sbSql.Append(sJurisdictionId);
                    sbSql = sbSql.Append(",");
                    sbSql = sbSql.Append(sUnclaimedDays);
                    sbSql = sbSql.Append(",");
                    sbSql = sbSql.Append(sEschetDays);
                    sbSql = sbSql.Append(",");
                    sbSql = sbSql.Append("'" + Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss")) + "'");
                    sbSql = sbSql.Append(",");
                    sbSql = sbSql.Append("'" + m_sUserName + "'");
                    sbSql = sbSql.Append(",");
                    sbSql = sbSql.Append("'" + Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss")) + "'");
                    sbSql = sbSql.Append(",");
                    sbSql = sbSql.Append("'" + m_sUserName + "'");
                    sbSql = sbSql.Append(")");
                    sSQL = sbSql.ToString();
                }

                objConn.ExecuteNonQuery(sSQL);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsDormancy.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                sbSql = null;
            }
        }


        /// Name		: GetSelectedFundsDormancyInfo
        /// Author		: Rupal Kotak
        /// Date Created: 8 Sep 2011
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get the data for the Selected Funds Dormancy
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data and structure</param>
        /// <returns>xml structure and data</returns>
        public XmlDocument GetSelectedFundsDormancyInfo(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;
            DbReader objRead = null;
            string sStateName = string.Empty;
            string sJurisdictionId = string.Empty;
            try
            {

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DormancyRowId']");
                if (objElement != null)
                {
                    if (objElement.InnerText != null)
                    {
                        if (objElement.InnerText.Trim() != "")
                        {
                            sSQL = "SELECT FUNDS_DORMANCY.*,STATES.STATE_NAME FROM FUNDS_DORMANCY,STATES WHERE FUNDS_DORMANCY.STATE_ID = STATES.STATE_ROW_ID AND FUNDS_DORMANCY.DORMANCY_ROW_ID = " + objElement.InnerText;
                            objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objRead.Read())
                            {
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdiction']");
                                sJurisdictionId = Conversion.ConvertObjToStr(objRead.GetValue("STATE_ID"));
                                objElement.Attributes["codeid"].Value = sJurisdictionId;
                                sStateName = Conversion.ConvertObjToStr(objRead.GetValue("STATE_NAME"));
                                objElement.InnerText = sStateName;

                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='JurisdictionId']");
                                objElement.InnerText = sJurisdictionId;

                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UnclaimedDays']");
                                objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("UNCLAIMED_DAYS"));
                                //if (objElement.InnerText == "0")
                                //    objElement.InnerText = "";

                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EscheatDays']");
                                objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("ESCHEAT_DAYS"));
                                //if (objElement.InnerText == "0")
                                //    objElement.InnerText = "";

                            }
                            if (!objRead.IsClosed)
                            {
                                objRead.Close();
                            }
                            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//group");
                            objElement.Attributes["title"].Value = "Jurisdiction [" + sStateName + "]";
                        }
                    }
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FundsDormancy.GetSelectedFundsDormancyInfo.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objRead != null)
                {
                    if (!objRead.IsClosed)
                    {
                        objRead.Close();
                    }
                    objRead = null;
                }
            }
        }
    }
}