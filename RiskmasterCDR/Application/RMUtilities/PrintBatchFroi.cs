﻿using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.Settings;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Text;
using System.IO;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Nadim zafar
    ///Dated   :   
    ///Purpose :   Implements FROI Batch Printing Options form
    /// </summary>
    public class PrintBatchFroi : UtilitiesBase
    {
        static long m_lHighLevel;
        static long m_lLowLevel;
        static long m_lExcludeClosedClaims;
        static string sFromDate = string.Empty;
        static string sToDate = string.Empty;
        static string sBatchOption = string.Empty;
        static string m_sConnstring = string.Empty;
        static Boolean bState = false;
        static Boolean bState2 = false;
        static string m_strPDFOutputPath = string.Empty;
        private DbConnection m_objDbConnection = null;
        private int m_iClientId = 0;

        /// Name		: PrintBatchFroi
        /// Author		: Nadim Zafar
        /// Date Created: 22,Feb 2010	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default Constructor with Connection String
        /// </summary>
        /// <param name="p_sConnString">Connection String parameter</param>
        public PrintBatchFroi(string p_sConnString,int p_iClientId)
            : base(p_sConnString,p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_sConnstring = p_sConnString;
            this.Initialize(m_sConnstring, m_iClientId);
        }



        /// Name		: Initialize
        /// Author		: Nadim Zafar
        /// Date Created: 22,Feb 2010		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the properties and variables
        /// </summary>
        private void Initialize(string p_sConnString, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_sConnstring = p_sConnString;
            base.Initialize();
            m_objDbConnection = DbFactory.GetDbConnection(m_sConnstring);
            m_objDbConnection.Open();
            //string m_sConnstring = p_sConnString;
        }

        /// Name		: Get
        /// Author		: nadim zafar
        /// Date Created: 22,Feb 2010		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Public default get method
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>Xmldocument with data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            string sSQL = "";
            DbReader objDbReader = null;
            XmlElement objControlXMLElement = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            SysSettings objsettings = null;
            XmlNode objXmlNodeState1 = null;


            try
            {
                objsettings = new SysSettings(m_sConnstring, m_iClientId);
                m_lHighLevel = objsettings.FroiBatHighLvl;
                m_lLowLevel = objsettings.FroiBatLowLvl;
                m_lExcludeClosedClaims = objsettings.FroiBatCldClm;


                sFromDate = p_objXmlDocument.SelectSingleNode("//FromDate").InnerText;
                sToDate = p_objXmlDocument.SelectSingleNode("//ToDate").InnerText;
                sBatchOption = p_objXmlDocument.SelectSingleNode("//PrintedClaimsToInclude").InnerText;


                objXmlNodeState1 = p_objXmlDocument.SelectSingleNode("//State1");
                if (objXmlNodeState1 != null)
                {
                    if (objXmlNodeState1.InnerText.ToLower().Trim() == "true")
                    {
                        bState = true;
                    }
                }

                if (!bState)
                {
                    objXmlNodeState1 = p_objXmlDocument.SelectSingleNode("//State2");
                    if (objXmlNodeState1 != null)
                    {
                        if (objXmlNodeState1.InnerText.ToLower().Trim() == "true")
                        {
                            bState2 = true;
                        }
                    }
                }

                //if (p_objXmlDocument.SelectSingleNode("//State1").InnerText == "True")
                //{
                //    bState = true;
                //}
                //else if (p_objXmlDocument.SelectSingleNode("//State2").InnerText == "True")
                //{
                //    bState2 = true;
                //}

                objControlXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PrintBatchFroi");
                // sSQL = "SELECT * FROM CLAIM WHERE CLAIM_ID BETWEEN 250 AND 300";
                sSQL = LoadSQL_Froi(m_iClientId);
                using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            objLstRow = p_objXmlDocument.CreateElement("Option");
                            objRowTxt = p_objXmlDocument.CreateElement("RowId");
                            objRowTxt.InnerText = string.Format("{0:00000}", objDbReader["CLAIM_ID"]);
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("ClaimNumber");
                            objRowTxt.InnerText = string.Format("{0:00000}", objDbReader["CLAIM_NUMBER"]);
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("ClaimDate");
                            objRowTxt.InnerText = objDbReader["DATE_OF_CLAIM"].ToString();
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("ClaimantName");
                            objRowTxt.InnerText = objDbReader["PI_LAST_NAME"].ToString();
                            objRowTxt.InnerText = objRowTxt.InnerText + "," + objDbReader["PI_FIRST_NAME"].ToString();
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("Jurisdiction");
                            objRowTxt.InnerText = objDbReader["STATE_ID"].ToString();
                            objRowTxt.InnerText = objRowTxt.InnerText + " " + objDbReader["STATE_NAME"].ToString();
                            objLstRow.AppendChild(objRowTxt);

                            objControlXMLElement.AppendChild((XmlNode)objLstRow);
                        }

                    }

                }
                bState = false;
                bState2 = false;
                return p_objXmlDocument;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintBatchFroi.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }
        }

        /// Name		: GetPath
        /// Author		: nadim zafar
        /// Date Created: 22,Feb 2010		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Public default get method
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>Xmldocument with data</returns>
        public XmlDocument GetPath(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDocument = new XmlDocument();
            XmlElement objRootNode = null;
            XmlElement objFroiNode = null;
            XmlElement objAcordNode = null;
            XmlElement objChildNode = null; 
            XmlElement objRootNodeFroi = null;
            XmlElement objRootNodeAcord = null;
            DirectoryInfo objFolder = null;
            string strFileName = string.Empty;
            int iSNo4Froi = 0;
            int iSNo4Acord = 0;
            string[] strArray; 
            string sBatchId = string.Empty;
            int iDocId = 0;
            string sSQL = string.Empty;
            string sSortFroi = string.Empty;
            string sSortAcord = string.Empty;
            string sSortColumnFroi = string.Empty;
            string sSortColumnAcord = string.Empty;
            string sDate = string.Empty;
            DbReader objDbReader = null;
            try
            {

                StartDocument(ref objDocument, ref objRootNode, "LinkBatchFroiAcord", m_iClientId);
                CreateElement(objRootNode, "FroiFileName", ref objFroiNode, m_iClientId);
                CreateElement(objRootNode, "AcordFileName", ref objAcordNode, m_iClientId); 

               // m_strPDFOutputPath = RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, @"froi");
               
                //objFolder = new DirectoryInfo(m_strPDFOutputPath);
                sSortFroi = p_objXmlDocument.SelectSingleNode("//SortExprFroi").InnerText;
                sSortAcord= p_objXmlDocument.SelectSingleNode("//SortExprAcord").InnerText;
                sSortColumnFroi = p_objXmlDocument.SelectSingleNode("//SortColumnFroi").InnerText;
                sSortColumnAcord = p_objXmlDocument.SelectSingleNode("//SortColumnAcord").InnerText;
             
                if (sSortColumnFroi == null || sSortColumnFroi == "")
                {
                    sSQL = "SELECT DOCUMENT_ID,DOCUMENT_FILENAME,CREATE_DATE FROM  DOCUMENT WHERE DOCUMENT_FILENAME LIKE '%FROI_FINAL_%' AND UPLOAD_STATUS=-1";
                    
                }
                else
                {
                    sSQL = "SELECT DOCUMENT_ID,DOCUMENT_FILENAME,CREATE_DATE FROM  DOCUMENT WHERE DOCUMENT_FILENAME LIKE '%FROI_FINAL_%' AND UPLOAD_STATUS=-1 ORDER BY " + sSortColumnFroi + " " + sSortFroi;

                }
                using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            strFileName = objDbReader.GetString("DOCUMENT_FILENAME");
                            iDocId = objDbReader.GetInt32("DOCUMENT_ID");
                            sDate = objDbReader.GetString("CREATE_DATE");
                            sDate = Conversion.GetDBDTTMFormat(sDate, "d", "t");
                            if (strFileName.Contains("FROI_FINAL"))
                                strArray = strFileName.Split('_');
                            else
                                continue;
                          
                            sBatchId = strArray[3];

                           
                                iSNo4Froi = iSNo4Froi + 1;
                                CreateElement(objFroiNode, "FileName", ref objChildNode,m_iClientId);
                                
                                CreateAndSetElement(objChildNode, "chkFROI", "",m_iClientId);
                                CreateAndSetElement(objChildNode, "SNO", iSNo4Froi.ToString(),m_iClientId);
                            
                            
                            CreateAndSetElement(objChildNode, "DocId", iDocId.ToString(), m_iClientId);
                            CreateAndSetElement(objChildNode, "BatchId", sBatchId, m_iClientId);
                            CreateAndSetElement(objChildNode, "FileName", strFileName, m_iClientId);
                            CreateAndSetElement(objChildNode, "FileCount", strArray[2], m_iClientId);
                            CreateAndSetElement(objChildNode, "DateCreated", sDate, m_iClientId);



                        }
                    }
                }
                if (sSortColumnAcord == null || sSortColumnAcord == "")
                {
                    sSQL = "SELECT DOCUMENT_ID,DOCUMENT_FILENAME,CREATE_DATE FROM  DOCUMENT WHERE DOCUMENT_FILENAME LIKE '%ACORD_FINAL_%'  AND UPLOAD_STATUS=-1";

                }
                else
                {
                    sSQL = "SELECT DOCUMENT_ID,DOCUMENT_FILENAME,CREATE_DATE FROM  DOCUMENT WHERE DOCUMENT_FILENAME LIKE '%ACORD_FINAL_%' AND UPLOAD_STATUS=-1 ORDER BY " + sSortColumnAcord + " " + sSortAcord;

                }
                using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            strFileName = objDbReader.GetString("DOCUMENT_FILENAME");
                            iDocId = objDbReader.GetInt32("DOCUMENT_ID");
                            sDate = objDbReader.GetString("CREATE_DATE");
                            sDate = Conversion.GetDBDTTMFormat(sDate, "d", "t");
                            if (strFileName.Contains("ACORD_FINAL"))
                                strArray = strFileName.Split('_');
                            else
                                continue;
                           
                            sBatchId = strArray[3];

                           
                                iSNo4Acord = iSNo4Acord + 1;
                                CreateElement(objAcordNode, "FileName", ref objChildNode, m_iClientId);
                               
                                CreateAndSetElement(objChildNode, "chkACORD", "", m_iClientId);
                                CreateAndSetElement(objChildNode, "SNO", iSNo4Acord.ToString(), m_iClientId);
                           

                            CreateAndSetElement(objChildNode, "DocId", iDocId.ToString(), m_iClientId);
                            CreateAndSetElement(objChildNode, "BatchId", sBatchId, m_iClientId);
                            CreateAndSetElement(objChildNode, "FileName", strFileName, m_iClientId);
                            CreateAndSetElement(objChildNode, "FileCount", strArray[2], m_iClientId);
                            CreateAndSetElement(objChildNode, "DateCreated", sDate, m_iClientId);



                        }
                    }
                }

             /*   if (objFolder.Exists)
                {
                    foreach (FileInfo objFileInfo in objFolder.GetFiles())
                    {
                        strFileName = objFileInfo.Name;
                        if (strFileName.Contains("FROI_FINAL") || strFileName.Contains("ACORD_FINAL"))
                            strArray = strFileName.Split('_');
                        else
                            continue;
                        sBatchId = strArray[3].Replace(".pdf", "");
                        
                        if (strFileName.Contains("FROI_FINAL"))
                        {
                            iSNo4Froi = iSNo4Froi + 1;
                            CreateElement(objFroiNode, "FileName", ref objChildNode);
                            CreateAndSetElement(objChildNode, "SNO", iSNo4Froi.ToString());
                        }
                        else if (strFileName.Contains("ACORD_FINAL"))
                        {
                            iSNo4Acord = iSNo4Acord + 1;
                            CreateElement(objAcordNode, "FileName", ref objChildNode);
                            CreateAndSetElement(objChildNode, "SNO", iSNo4Acord.ToString());
                        }
                        
                        CreateAndSetElement(objChildNode, "BatchId", sBatchId);
                        CreateAndSetElement(objChildNode, "FileName", strFileName);
                        CreateAndSetElement(objChildNode, "FileCount", strArray[2]);
                    }

                }   */         
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintBatchFroi.GetPath.Err", m_iClientId), p_objEx);
            }
            finally
            {
                //objRootNode = null;
                //objNoticesNode = null;
                //objInvoicesNode = null;
                //objFolder = null;
            }

            return objDocument;
        }
               
           

        /// Name		: Get
        /// Author		: nadim zafar
        /// Date Created: 22,Feb 2010		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Public default get Acordmethod
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>Xmldocument with data</returns>
        public XmlDocument GetAcord(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            string sSQL = "";
            DbReader objDbReader = null;
            XmlElement objControlXMLElement = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            SysSettings objsettings = null;
            XmlNode objXmlNodeState1 = null;
           


            try
            {
                objsettings = new SysSettings(m_sConnstring, m_iClientId);
                m_lHighLevel = objsettings.FroiBatHighLvl;
                m_lLowLevel = objsettings.FroiBatLowLvl;
                m_lExcludeClosedClaims = objsettings.FroiBatCldClm;


                sFromDate = p_objXmlDocument.SelectSingleNode("//FromDate").InnerText;
                sToDate = p_objXmlDocument.SelectSingleNode("//ToDate").InnerText;
                sBatchOption = p_objXmlDocument.SelectSingleNode("//PrintedClaimsToInclude").InnerText;


                objXmlNodeState1 = p_objXmlDocument.SelectSingleNode("//State1");
                if (objXmlNodeState1 != null)
                {
                    if (objXmlNodeState1.InnerText.ToLower().Trim() == "true")
                    {
                        bState = true;
                    }
                }

                if (!bState)
                {
                    objXmlNodeState1 = p_objXmlDocument.SelectSingleNode("//State2");
                    if (objXmlNodeState1 != null)
                    {
                        if (objXmlNodeState1.InnerText.ToLower().Trim() == "true")
                        {
                            bState2 = true;
                        }
                    }
                }

                //if (p_objXmlDocument.SelectSingleNode("//State1").InnerText == "True")
                //{
                //    bState = true;
                //}
                //else if (p_objXmlDocument.SelectSingleNode("//State2").InnerText == "True")
                //{
                //    bState2 = true;
                //}

                objControlXMLElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PrintBatchAcord");
                // sSQL = "SELECT * FROM CLAIM WHERE CLAIM_ID BETWEEN 250 AND 300";
                sSQL = LoadSQL_Acord(m_iClientId);
                using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {
                            objLstRow = p_objXmlDocument.CreateElement("Option");
                            objRowTxt = p_objXmlDocument.CreateElement("RowId");
                            objRowTxt.InnerText = string.Format("{0:00000}", objDbReader["CLAIM_ID"])+"~"+objDbReader["FORM_ID"].ToString();
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("ClaimId");
                            objRowTxt.InnerText = string.Format("{0:00000}", objDbReader["CLAIM_ID"]) ;
                            objLstRow.AppendChild(objRowTxt);

                            //<FormTitle>Acord Form</FormTitle><ClaimSubType>Claim Type</ClaimSubType><ClaimNumber>Claim Number</ClaimNumber><ClaimDate>Claim Date</ClaimDate><ClaimantName>Claimant Name</ClaimantName>");

                            objRowTxt = p_objXmlDocument.CreateElement("FormTitle");
                            objRowTxt.InnerText =objDbReader["FORM_TITLE"].ToString();
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("ClaimSubType");
                            objRowTxt.InnerText = objDbReader["CLAIM_SUB_TYPE"].ToString();
                            objLstRow.AppendChild(objRowTxt);


                            objRowTxt = p_objXmlDocument.CreateElement("ClaimNumber");
                            objRowTxt.InnerText = string.Format("{0:00000}", objDbReader["CLAIM_NUMBER"]);
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("ClaimDate");
                            objRowTxt.InnerText = objDbReader["DATE_OF_CLAIM"].ToString();
                            objLstRow.AppendChild(objRowTxt);

                            objRowTxt = p_objXmlDocument.CreateElement("ClaimantName");
                            objRowTxt.InnerText = objDbReader["PI_LAST_NAME"].ToString();
                            objRowTxt.InnerText = objRowTxt.InnerText + "," + objDbReader["PI_FIRST_NAME"].ToString();
                            objLstRow.AppendChild(objRowTxt);


                            

                            //objRowTxt = p_objXmlDocument.CreateElement("Jurisdiction");
                            //objRowTxt.InnerText = objDbReader["STATE_ID"].ToString();
                            //objRowTxt.InnerText = objRowTxt.InnerText + " " + objDbReader["STATE_NAME"].ToString();
                            //objLstRow.AppendChild(objRowTxt);

                            objControlXMLElement.AppendChild((XmlNode)objLstRow);
                        }

                    }

                }
                bState = false;
                bState2 = false;
                return p_objXmlDocument;

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintBatchAcord.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }
        }



        private static string LoadSQL_Acord(int p_iClientId)
        {

            string sTmp = string.Empty;
            string SQL = string.Empty;
            //string sClosedStatus =string.Empty;
            string sSQLClosedStatus = string.Empty;
            string sSQLTables = string.Empty;
            string sSQLFields = string.Empty;
            string sSQLWhere = string.Empty;
            string sText = string.Empty;
            string sTmp2 = string.Empty;
            string sHistoryTable = string.Empty;
            string sOptionsSQL = string.Empty;
            int iCodeId = 0;
            LocalCache objCache = null;
            DbReader objDbReader = null;
            StringBuilder sGroupIds = null;
            StringBuilder sClosedStatus = null;

            try
            {


                objCache = new LocalCache(m_sConnstring, p_iClientId);

                //sSQLFields = "SELECT DISTINCT CLAIM_NUMBER, DATE_OF_CLAIM" +
                //      ", PI.LAST_NAME AS PI_LAST_NAME, PI.FIRST_NAME AS PI_FIRST_NAME" +
                //      ", STATES.STATE_ID, STATE_NAME, CLAIM.CLAIM_ID ";

                sSQLFields = " SELECT DISTINCT CL_FORMS.FORM_TITLE,CL_FORMS.FORM_ID, CODES_TEXT.CODE_DESC AS CLAIM_SUB_TYPE, " +
                              "CLAIM_NUMBER, DATE_OF_CLAIM, PI.LAST_NAME AS PI_LAST_NAME, PI.FIRST_NAME AS PI_FIRST_NAME, CLAIM.CLAIM_ID";



                if (m_lHighLevel > 1004)
                {
                    sSQLFields = sSQLFields + ", ORG_A.LAST_NAME  ORG_A_LAST_NAME";
                }

                if (m_lLowLevel > 1004)
                {
                    sSQLFields = sSQLFields + ", ORG_B.LAST_NAME  ORG_B_LAST_NAME";
                }

                sSQLTables = " FROM CLAIM,EVENT,CLAIMANT,ENTITY PI, CLAIM_Accord_MAPPI, CL_FORMS, CODES_TEXT";

                //sSQLTables = " FROM CLAIM,CLAIMANT,STATES,ENTITY AS PI";

               

                if (m_lHighLevel > 1004)
                {
                    sSQLTables = sSQLTables + ",ORG_HIERARCHY,PERSON_INVOLVED,ENTITY  ORG_A";
                }

                if (m_lLowLevel > 1004)
                {
                    sSQLTables = sSQLTables + ",ENTITY  ORG_B";
                }


                sSQLWhere = " WHERE CLAIM.LINE_OF_BUS_CODE <> 243 AND CLAIM.LINE_OF_BUS_CODE <> 844 AND  CLAIM.LINE_OF_BUS_CODE <> 845 AND CLAIM.EVENT_ID = EVENT.EVENT_ID" +
                " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID" +
                " AND CLAIMANT.CLAIMANT_EID = PI.ENTITY_ID" +
                " AND CLAIM.CLAIM_TYPE_CODE = CLAIM_Accord_MAPPI.CLAIM_TYPE_CODE" +
                " AND CLAIM_Accord_MAPPI.CLAIM_TYPE_CODE = CODES_TEXT.CODE_ID" + 
                " AND CLAIM_Accord_MAPPI.ACORD_FORM_ID = CL_FORMS.FORM_ID";



                //sSQLWhere = " WHERE CLAIM.LINE_OF_BUS_CODE = 243" +
                //    " AND CLAIM.FILING_STATE_ID = STATES.STATE_ROW_ID" +
                //    " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID" +
                //    " AND CLAIMANT.CLAIMANT_EID = PI.ENTITY_ID";



                sHistoryTable = " CL_FORMS_HIST";

                if (m_lHighLevel > 1004)
                {
                    //sSQLWhere = sSQLWhere + " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID" +
                    //                        " AND CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID" +
                    //                        " AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = ORG_HIERARCHY.DEPARTMENT_EID" +
                    //                        " AND ORG_A.ENTITY_ID = ORG_HIERARCHY." + (sGetOrgHierarchyLevel(m_lHighLevel)) + "_EID";



                    sSQLWhere = sSQLWhere + " AND EVENT.DEPT_EID = ORG_HIERARCHY.DEPARTMENT_EID" +
                                " AND ORG_A.ENTITY_ID = ORG_HIERARCHY." + (sGetOrgHierarchyLevel(m_lHighLevel, p_iClientId)) + "_EID";
                                
                    // " AND ORG_A.ENTITY_ID = ORG_HIERARCHY.CLIENT_EID";
                }

                if (m_lLowLevel > 1004)
                {
                    //sSQLWhere = sSQLWhere + " AND ORG_B.ENTITY_ID = ORG_HIERARCHY." + (sGetOrgHierarchyLevel(m_lLowLevel)) + "_EID";
                    sSQLWhere = sSQLWhere + " AND ORG_B.ENTITY_ID = ORG_HIERARCHY." + (sGetOrgHierarchyLevel(m_lLowLevel,p_iClientId)) + "_EID";
        
                    //sSQLWhere = sSQLWhere + " AND ORG_B.ENTITY_ID = ORG_HIERARCHY.CLIENT_EID";
                }

                if (m_lExcludeClosedClaims == 1)
                {
                    sSQLClosedStatus = " SELECT CODES_B.CODE_ID" +
                                       " FROM CODES AS CODES_A,CODES AS CODES_B" +
                                       " WHERE CODES_A.TABLE_ID = " + objCache.GetTableId("STATUS") +
                                       " AND CODES_A.SHORT_CODE = 'C'" +
                                       " AND CODES_A.CODE_ID = CODES_B.RELATED_CODE_ID" +
                                       " AND CODES_B.TABLE_ID = " + objCache.GetTableId("CLAIM_STATUS");

                    


                    //
                    // sSQL = "SELECT * FROM ORG_SECURITY WHERE GROUP_ID > 0 AND DELETED_FLAG =-1 AND GROUP_ENTITIES NOT LIKE '%<ALL>%'";
                    using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQLClosedStatus))
                    {
                        if (objDbReader != null)
                        {
                            sClosedStatus = new StringBuilder();
                            sClosedStatus.Append("(");
                            while (objDbReader.Read())
                            {
                                iCodeId = objDbReader.GetInt32("CODE_ID");
                                sClosedStatus.Append(iCodeId);
                                sClosedStatus.Append(",");


                            }

                            sClosedStatus.Remove(sClosedStatus.Length - 1, 1);
                            sClosedStatus.Append(")");
                        }
                    }
                    sSQLWhere = sSQLWhere + " AND CLAIM_STATUS_CODE NOT IN" + sClosedStatus;
                    //

                    //if (S2) {
                    //    Do While Not DB_EOF(iRS2)
                    //        sClosedStatus = sClosedStatus & sAnyVarToString(vDB_GetData(iRS2, "CODE_ID")) & ","
                    //        DB_MoveNext iRS2
                    //    Loop
                    //    sClosedStatus = Trim(sClosedStatus)
                    //    if( sClosedStatus > "" )
                    //    {
                    //        sClosedStatus = Left(sClosedStatus, Len(sClosedStatus) - 1)
                    //        sSQLWhere = sSQLWhere + " AND CLAIM_STATUS_CODE NOT IN" + 
                    //                    "(" + sClosedStatus + ")";
                    //    }
                    //    subDB_CloseRecordset iRS2, DB_DROP
                    //}
                }

                SQL = sSQLFields + sSQLTables + sSQLWhere;

                sOptionsSQL = ClaimSelectOptionsSQL(sHistoryTable);

                SQL = SQL + sOptionsSQL;
                return SQL;

            }
            catch (Exception exc)
            {
                throw (exc);
            }

            finally
            {
                if (objCache != null)
                    objCache.Dispose();
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }

        }


        private static string LoadSQL_Froi(int p_iClientId)
        {

            string sTmp = string.Empty;
            string SQL = string.Empty;
            //string sClosedStatus =string.Empty;
            string sSQLClosedStatus = string.Empty;
            string sSQLTables = string.Empty;
            string sSQLFields = string.Empty;
            string sSQLWhere = string.Empty;
            string sText = string.Empty;
            string sTmp2 = string.Empty;
            string sHistoryTable = string.Empty;
            string sOptionsSQL = string.Empty;
            int iCodeId = 0;
            LocalCache objCache = null;
            DbReader objDbReader = null;
            StringBuilder sGroupIds = null;
            StringBuilder sClosedStatus = null;

            try
            {


                objCache = new LocalCache(m_sConnstring, p_iClientId);

                sSQLFields = "SELECT DISTINCT CLAIM_NUMBER, DATE_OF_CLAIM" +
                      ", PI.LAST_NAME AS PI_LAST_NAME, PI.FIRST_NAME AS PI_FIRST_NAME" +
                      ", STATES.STATE_ID, STATE_NAME, CLAIM.CLAIM_ID ";

                if (m_lHighLevel > 1004)
                {
                    sSQLFields = sSQLFields + ", ORG_A.LAST_NAME  ORG_A_LAST_NAME";
                }

                if (m_lLowLevel > 1004)
                {
                    sSQLFields = sSQLFields + ", ORG_B.LAST_NAME  ORG_B_LAST_NAME";
                }

                //sSQLTables = " FROM CLAIM,CLAIMANT,STATES,ENTITY AS PI";
                sSQLTables = " FROM CLAIM,CLAIMANT,STATES,ENTITY  PI";

                if (m_lHighLevel > 1004)
                {
                    sSQLTables = sSQLTables + ",ORG_HIERARCHY,PERSON_INVOLVED,ENTITY  ORG_A";
                }

                if (m_lLowLevel > 1004)
                {
                    sSQLTables = sSQLTables + ",ENTITY  ORG_B";
                }


                sSQLWhere = " WHERE CLAIM.LINE_OF_BUS_CODE = 243" +
                    " AND CLAIM.FILING_STATE_ID = STATES.STATE_ROW_ID" +
                    " AND CLAIM.CLAIM_ID = CLAIMANT.CLAIM_ID" +
                    " AND CLAIMANT.CLAIMANT_EID = PI.ENTITY_ID";



                sHistoryTable = " JURIS_FORMS_HIST";

                if (m_lHighLevel > 1004)
                {
                    sSQLWhere = sSQLWhere + " AND CLAIMANT.CLAIMANT_EID = PERSON_INVOLVED.PI_EID" +
                                            " AND CLAIM.EVENT_ID = PERSON_INVOLVED.EVENT_ID" +
                                            " AND PERSON_INVOLVED.DEPT_ASSIGNED_EID = ORG_HIERARCHY.DEPARTMENT_EID" +
                                            " AND ORG_A.ENTITY_ID = ORG_HIERARCHY." + (sGetOrgHierarchyLevel(m_lHighLevel,p_iClientId)) + "_EID";
                    // " AND ORG_A.ENTITY_ID = ORG_HIERARCHY.CLIENT_EID";
                }

                if (m_lLowLevel > 1004)
                {
                    sSQLWhere = sSQLWhere + " AND ORG_B.ENTITY_ID = ORG_HIERARCHY." + (sGetOrgHierarchyLevel(m_lLowLevel,p_iClientId)) + "_EID";
                    //sSQLWhere = sSQLWhere + " AND ORG_B.ENTITY_ID = ORG_HIERARCHY.CLIENT_EID";
                }

                if (m_lExcludeClosedClaims == 1)
                {
                    sSQLClosedStatus = " SELECT CODES_B.CODE_ID" +
                                       " FROM CODES AS CODES_A,CODES AS CODES_B" +
                                       " WHERE CODES_A.TABLE_ID = " + objCache.GetTableId("STATUS") +
                                       " AND CODES_A.SHORT_CODE = 'C'" +
                                       " AND CODES_A.CODE_ID = CODES_B.RELATED_CODE_ID" +
                                       " AND CODES_B.TABLE_ID = " + objCache.GetTableId("CLAIM_STATUS");


                    //
                    // sSQL = "SELECT * FROM ORG_SECURITY WHERE GROUP_ID > 0 AND DELETED_FLAG =-1 AND GROUP_ENTITIES NOT LIKE '%<ALL>%'";
                    using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQLClosedStatus))
                    {
                        if (objDbReader != null)
                        {
                            sClosedStatus = new StringBuilder();
                            sClosedStatus.Append("(");
                            while (objDbReader.Read())
                            {
                                iCodeId = objDbReader.GetInt32("CODE_ID");
                                sClosedStatus.Append(iCodeId);
                                sClosedStatus.Append(",");


                            }

                            sClosedStatus.Remove(sClosedStatus.Length - 1, 1);
                            sClosedStatus.Append(")");
                        }
                    }
                    sSQLWhere = sSQLWhere + " AND CLAIM_STATUS_CODE NOT IN" + sClosedStatus;
                    //

                    //if (S2) {
                    //    Do While Not DB_EOF(iRS2)
                    //        sClosedStatus = sClosedStatus & sAnyVarToString(vDB_GetData(iRS2, "CODE_ID")) & ","
                    //        DB_MoveNext iRS2
                    //    Loop
                    //    sClosedStatus = Trim(sClosedStatus)
                    //    if( sClosedStatus > "" )
                    //    {
                    //        sClosedStatus = Left(sClosedStatus, Len(sClosedStatus) - 1)
                    //        sSQLWhere = sSQLWhere + " AND CLAIM_STATUS_CODE NOT IN" + 
                    //                    "(" + sClosedStatus + ")";
                    //    }
                    //    subDB_CloseRecordset iRS2, DB_DROP
                    //}
                }

                SQL = sSQLFields + sSQLTables + sSQLWhere;

                sOptionsSQL = ClaimSelectOptionsSQL(sHistoryTable);

                SQL = SQL + sOptionsSQL;
                return SQL;

            }
            catch (Exception exc)
            {
                throw (exc);
            }

            finally
            {
                if (objCache != null)
                    objCache.Dispose();
                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }

        }

        private static string sGetOrgHierarchyLevel(long lLevel, int p_iClientId)
        {

            string sSQL = "";
            string sLevel = string.Empty;
            DbReader objDbReader = null;


            try
            {

                sSQL = "SELECT TABLE_NAME FROM GLOSSARY_TEXT WHERE TABLE_ID = " + lLevel;
                using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        while (objDbReader.Read())
                        {

                            sLevel = objDbReader.GetString("TABLE_NAME");
                        }

                    }

                }



            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintBatchFroi.sGetOrgHierarchyLevel.Err",p_iClientId), p_objEx);
            }
            finally
            {

                if (objDbReader != null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }

            }

            return sLevel;
        }


        private static string ClaimSelectOptionsSQL(string sHistoryTable)
        {
            string SQL = string.Empty;

            switch (sBatchOption)
            {
                case "-1":
                    SQL = SQL + " AND CLAIM.CLAIM_ID NOT IN";
                    SQL = SQL + " (SELECT DISTINCT " + sHistoryTable + ".CLAIM_ID FROM " + sHistoryTable + ")";
                    break;
                case "0":
                    //SQL = SQL + " AND CLAIM.CLAIM_ID NOT IN";//Change by kuladeep for froi issue 4/4/2012 mits:28100
                    SQL = SQL + " AND CLAIM.CLAIM_ID IN";
                    SQL = SQL + " (SELECT DISTINCT " + sHistoryTable + ".CLAIM_ID";
                    SQL = SQL + " FROM " + sHistoryTable;
                    SQL = SQL + " WHERE HOW_PRINTED = 'BATCH')"; 
                    SQL = SQL + " AND CLAIM.CLAIM_ID NOT IN";
                    SQL = SQL + " (SELECT DISTINCT " + sHistoryTable + ".CLAIM_ID";
                    SQL = SQL + " FROM " + sHistoryTable;
                    SQL = SQL + " WHERE HOW_PRINTED = 'BOTH')"; 
                    break;
                case "1":
                    //SQL = SQL + " AND CLAIM.CLAIM_ID NOT IN";//Change by kuladeep for froi issue 4/4/2012 mits:28100
                    SQL = SQL + " AND CLAIM.CLAIM_ID IN";
                    SQL = SQL + " (SELECT DISTINCT " + sHistoryTable + ".CLAIM_ID";
                    SQL = SQL + " FROM " + sHistoryTable;
                    SQL = SQL + " WHERE HOW_PRINTED = 'SINGL')";
                    SQL = SQL + " AND CLAIM.CLAIM_ID NOT IN";
                    SQL = SQL + " (SELECT DISTINCT " + sHistoryTable + ".CLAIM_ID";
                    SQL = SQL + " FROM " + sHistoryTable;
                    SQL = SQL + " WHERE HOW_PRINTED = 'BOTH')";
                    break;
                case "2":
                    //SQL = SQL + " AND CLAIM.CLAIM_ID NOT IN";//Change by kuladeep for froi issue 4/4/2012 mits:28100
                    SQL = SQL + " AND CLAIM.CLAIM_ID IN";
                    SQL = SQL + " (SELECT DISTINCT " + sHistoryTable + ".CLAIM_ID";
                    SQL = SQL + " FROM " + sHistoryTable;
                    SQL = SQL + " WHERE HOW_PRINTED = 'BOTH')";
                    break;

            }
            if (bState)
            {
                SQL = SQL + " AND DATE_OF_CLAIM BETWEEN '" + Conversion.GetDate(sFromDate) + "' AND '" + Conversion.GetDate(sToDate) + "'";
            }
            else if (bState2)
            {
                SQL = SQL + " AND CLAIM.DTTM_RCD_ADDED BETWEEN '" + Conversion.GetDate(sFromDate) + "000000' AND '" + Conversion.GetDate(sToDate) + "240000'";
            }
            else
            {
                SQL = SQL + " AND DATE_OF_CLAIM BETWEEN '" + Conversion.GetDate(sFromDate) + "' AND '" + Conversion.GetDate(sToDate) + "'";
            }

            SQL = SQL + " ORDER BY DATE_OF_CLAIM, CLAIM_NUMBER";
            return SQL;

        }

        /// Name		: Save
        /// 
        /// Author		: Nadim Zafar
        /// Date Created: 22,Feb 2010	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Public Default Save method
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data for saving</param>
        /// <returns>Xmldocument with saved data</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            int iBatchId = 0;
            string sSQL = "";
            int iClaimId = 0;
            string sJuris = string.Empty;
            // string sClaimIds = string.Empty;
            string sSelected = string.Empty;
            string[] arrSelected;
            StringBuilder sClaimIds = null;
            DbReader objDbReader = null;

            try
            {
                //sFromDate = p_objXmlDocument.SelectSingleNode("//FromDate").InnerText;
                //sToDate = p_objXmlDocument.SelectSingleNode("//ToDate").InnerText;
                sSelected = p_objXmlDocument.SelectSingleNode("//SelectedRowId").InnerText;


                sSQL = "SELECT MAX(BATCH_ID) FROM BATCH_PRINT";
                using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                            iBatchId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;
                        else
                            iBatchId = 1;

                    }
                }



                //if (sSelected.Trim() != "")
                //{
                //    sClaimIds = new StringBuilder();
                //    arrSelected = sSelected.Split(' ');
                //    if (arrSelected.Length > 0)
                //    {
                //        for (int i = 0; i < arrSelected.Length; i++)
                //        {
                //            //if ((i % 2) > 0)
                //            //{
                //                iNum = Conversion.ConvertStrToInteger(arrSelected[i].ToString());
                //                sClaimIds.Append(iNum);
                //                sClaimIds.Append(",");
                //            //}
                //        }
                //    }
                //    sClaimIds.Remove(sClaimIds.Length - 1, 1); 
                //}

                if (sSelected.Trim() != "")
                {
                    //sClaimIds = new StringBuilder();
                    arrSelected = sSelected.Split(' ');
                    if (arrSelected.Length > 0)
                    {
                        for (int i = 0; i < arrSelected.Length; i++)
                        {
                            //if ((i % 2) > 0)
                            //{
                            iClaimId = Conversion.ConvertStrToInteger(arrSelected[i].ToString());
                            sSQL = "SELECT STATES.STATE_ID FROM CLAIM,STATES WHERE CLAIM_ID= " + iClaimId + " AND CLAIM.FILING_STATE_ID = STATES.STATE_ROW_ID";
                            using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                            {
                                if (objDbReader != null)
                                {
                                    if (objDbReader.Read())
                                        sJuris = objDbReader.GetString("STATE_ID");
                                    else
                                        sJuris = "";

                                }
                            }
                            sSQL = "INSERT INTO BATCH_PRINT(BATCH_ID,CLAIM_ID,JURISDICTION,PRINTED_STATUS) VALUES (" + iBatchId + "," + iClaimId + ",'" + sJuris + "',0)";
                            m_objDbConnection.ExecuteNonQuery(sSQL);

                        }
                    }

                }



                //try
                //{

                //    sSQL = "INSERT INTO BATCH_PRINT(BATCH_ID,FORM_ID,CLAIM_ID,JURISDICTION) VALUES (" + iBatchId + "," + 0 + ",'" + sClaimIds +"',' '"+  ")";
                //    //RunSql(sSQL, m_objDbConnection);
                //    //p_objConnection.ExecuteNonQuery(p_sSql);  
                //    m_objDbConnection.ExecuteNonQuery(sSQL);
                //}
                //catch { }



                XMLDoc = p_objXmlDocument;
                //Save();
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintBatchFroi.Save.Err",m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Close();
                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();
            }
        }



        /// Name		: Save
        /// 
        /// Author		: Nadim Zafar
        /// Date Created: 22,Feb 2010	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Public Default Save Acord method
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data for saving</param>
        /// <returns>Xmldocument with saved data</returns>
        public XmlDocument SaveAcord(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            int iBatchId = 0;
            string sSQL = "";
            int iClaimId = 0;
            string sJuris = string.Empty;
            // string sClaimIds = string.Empty;
            string sSelected = string.Empty;
            string[] arrSelected;
            string sClaimIds = string.Empty;
           // StringBuilder sClaimIds = null;
            DbReader objDbReader = null;
            int iLen = 0;
            int iForm_id = 0;
            try
            {
                //sFromDate = p_objXmlDocument.SelectSingleNode("//FromDate").InnerText;
                //sToDate = p_objXmlDocument.SelectSingleNode("//ToDate").InnerText;
                sSelected = p_objXmlDocument.SelectSingleNode("//SelectedRowId").InnerText;


                sSQL = "SELECT MAX(BATCH_ID) FROM BATCH_PRINT_ACORD";
                using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                {
                    if (objDbReader != null)
                    {
                        if (objDbReader.Read())
                            iBatchId = Conversion.ConvertStrToInteger(objDbReader[0].ToString()) + 1;
                        else
                            iBatchId = 1;

                    }
                }



                //if (sSelected.Trim() != "")
                //{
                //    sClaimIds = new StringBuilder();
                //    arrSelected = sSelected.Split(' ');
                //    if (arrSelected.Length > 0)
                //    {
                //        for (int i = 0; i < arrSelected.Length; i++)
                //        {
                //            //if ((i % 2) > 0)
                //            //{
                //                iNum = Conversion.ConvertStrToInteger(arrSelected[i].ToString());
                //                sClaimIds.Append(iNum);
                //                sClaimIds.Append(",");
                //            //}
                //        }
                //    }
                //    sClaimIds.Remove(sClaimIds.Length - 1, 1); 
                //}

                if (sSelected.Trim() != "")
                {
                    //sClaimIds = new StringBuilder();
                    arrSelected = sSelected.Split(' ');
                    if (arrSelected.Length > 0)
                    {
                        for (int i = 0; i < arrSelected.Length; i++)
                        {
                           
                             sClaimIds = arrSelected[i].ToString();
                             iLen = sClaimIds.IndexOf('~');
                            iClaimId = Conversion.ConvertStrToInteger(sClaimIds.Substring(0, iLen));
                             iForm_id = Conversion.ConvertStrToInteger(sClaimIds.Substring((iLen+1),(sClaimIds.Length-iLen-1)));

                            //iClaimId = Conversion.ConvertStrToInteger(arrSelected[i].ToString());
                                
                                // sSQL = "SELECT STATES.STATE_ID FROM CLAIM,STATES WHERE CLAIM_ID= " + iClaimId + " AND CLAIM.FILING_STATE_ID = STATES.STATE_ROW_ID";
                                //using (objDbReader = DbFactory.GetDbReader(m_sConnstring, sSQL))
                                //{
                                //    if (objDbReader != null)
                                //    {
                                //        if (objDbReader.Read())
                                //            sJuris = objDbReader.GetString("STATE_ID");
                                //        else
                                //            sJuris = "";

                                //    }
                                //}
                                sSQL = "INSERT INTO BATCH_PRINT_ACORD(BATCH_ID ,CLAIM_ID,FORM_ID,PRINTED_STATUS) VALUES (" + iBatchId + "," + iClaimId +","+iForm_id+ ",0)";
                                m_objDbConnection.ExecuteNonQuery(sSQL);
                            //}

                        }
                    }

                }

                XMLDoc = p_objXmlDocument;
                //Save();
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PrintBatchFroi.Save.Err", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDbReader != null)
                    objDbReader.Close();
                if (m_objDbConnection != null)
                    m_objDbConnection.Dispose();
            }
        }

        #region Common XML functions
        /// <summary>
        /// Initialize the document
        /// </summary>
        /// <param name="p_objDocument">xml Document</param>
        /// <param name="p_objRootNode">Root Node</param>
        /// <param name="p_sRootNodeName">Root Node Name</param>
        public static void StartDocument(ref XmlDocument p_objDocument, ref XmlElement p_objRootNode, string p_sRootNodeName, int p_iClientId)
        {
            try
            {
                p_objDocument = new XmlDocument();
                p_objRootNode = p_objDocument.CreateElement(p_sRootNodeName);
                p_objDocument.AppendChild(p_objRootNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.StartDocument.Error", p_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_sNodeName">Node Name</param>		
        public static void CreateElement(XmlElement p_objParentNode, string p_sNodeName, int p_iClientId)
        {
            XmlElement objChildNode = null;
            try
            {
                objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateElement.Error", p_iClientId), p_objEx);
            }
            finally
            {
                objChildNode = null;
            }

        }

        /// <summary>
        /// Create Element
        /// </summary>
        /// <param name="p_objParentNode">Parent Node</param>
        /// <param name="p_objChildNode">Child Node Name</param>
        /// <param name="p_objChildNode">Child Node</param>
        public static void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode, int p_iClientId)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateElement.Error", p_iClientId), p_objEx);
            }

        }

        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="sText">Text</param>		
        public static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText, int p_iClientId)
        {
            XmlElement objChildNode = null;
            try
            {

                CreateAndSetElement(p_objParentNode, p_sNodeName, sText, ref objChildNode, p_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateAndSetElement.Error", p_iClientId), p_objEx);
            }
            finally
            {
                objChildNode = null;
            }
        }
        /// <summary>
        /// Create Element and set inner text.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="sText">Text</param>
        /// <param name="p_objChildNode">Child Node</param>
        public static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText, ref XmlElement p_objChildNode, int p_iClientId)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode, p_iClientId);
                p_objChildNode.InnerText = sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateAndSetElement.Error", p_iClientId), p_objEx);
            }
        }

        
        #endregion 

    }
}