using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Users Report Access Permissions information.
	/// </summary>
	public class GrantReportAccess
	{
		#region Input/Output Xml
		/*
			<ReportsAccess>
				<Reports>
					<option value="315">07072004 - </option>
				</Reports>
				<Users>
					<option value="2">p (parag sarin)</option>
					<option value="3">t (test test)</option>
					<option value="4">m (manager manager)</option>
				</Users>
			</ReportsAccess>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// Secure DB connection string
		/// </summary>
		private string m_sSecDSN="";

        private int m_iClientId = 0;

		#endregion

		#region Properties
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		/// <summary>
		/// Secure DB connection string
		/// </summary>
		public string SecDSN
		{
			set
			{
				m_sSecDSN=value;
			}	

		}
		#endregion

		#region Constructor
		public GrantReportAccess()
		{
		}
		public GrantReportAccess(string p_sDSN,string p_sSecureDSN, int p_iClientId)
		{
			m_sDSN=p_sDSN;
			m_sSecDSN=p_sSecureDSN;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region Public Functions
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves Users Report Access Permissions information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			ArrayList objQuery=null;
			DbReader objReader=null;
			string sSql="";
			string sQuery="";
			bool bReturnValue=false;
			XmlNode objTempNode=null;
			string[] arrUsers=null;
			string[] arrReports=null;
			try
			{
				objQuery=new ArrayList();
				objTempNode=p_objXmlDocument.SelectSingleNode("/ReportsAccess/Reports");
				if (objTempNode!=null)
				{
					if (objTempNode.InnerText!=null)
					{
						arrReports=objTempNode.InnerText.Split(" ".ToCharArray());
					}
				}
				objTempNode=p_objXmlDocument.SelectSingleNode("/ReportsAccess/Users");
				if (objTempNode!=null)
				{
					if (objTempNode.InnerText!=null)
					{
						arrUsers=objTempNode.InnerText.Split(" ".ToCharArray());
					}
				}
				for (int i=0;i<arrUsers.Length;i++)
				{
					for (int j=0;j<arrReports.Length;j++)
						
					{
						sSql="SELECT USER_ID FROM SM_REPORTS_PERM WHERE USER_ID="+arrUsers[i]+" AND REPORT_ID="+arrReports[j];
						objReader=DbFactory.GetDbReader(m_sDSN,sSql);
						//Umesh MITS_8415
						if(!(objReader.Read()))
						{
                            //MITS 22088 - fixed invalid sql query (INTO was missing)
							sQuery="INSERT INTO SM_REPORTS_PERM (USER_ID, REPORT_ID) VALUES( "+arrUsers[i]+","+arrReports[j]+")";
                            //MITS 22088 - MJP
						}
						else 
						{
							sQuery="UPDATE SM_REPORTS_PERM SET USER_ID="+arrUsers[i]+",REPORT_ID="+arrReports[j]+" WHERE USER_ID="+arrUsers[i]+" AND REPORT_ID="+arrReports[j];
						}
						//Umesh MITS_8415
							objQuery.Add(sQuery);
					}
				}
                UTILITY.Save(objQuery, m_sDSN, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("GrantReportAccess.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objQuery=null;
				objTempNode=null;
				arrUsers=null;
				arrReports=null;
                if(objReader !=null)
                   objReader.Dispose();
			}
			return bReturnValue;
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get()
		{
			DbReader objReader=null;
			string sReportName="";
			string sReportDesc="";
			XmlDocument objDOM=null;
			StringBuilder sbSql=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sLName="";
			string sName="";
			string sFName="";
			SortedList slUsers=null;
			IDictionaryEnumerator objEnum=null;
			try
			{
				sbSql=new StringBuilder();
				sbSql.Append("SELECT SM_REPORTS.REPORT_ID, REPORT_NAME, REPORT_DESC FROM SM_REPORTS ORDER BY REPORT_NAME");
				objDOM=new XmlDocument();
				
				objElemParent = objDOM.CreateElement("ReportsAccess");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("Reports");
				objDOM.FirstChild.AppendChild(objElemChild);
				objReader = DbFactory.GetDbReader(m_sDSN,sbSql.ToString());
				while (objReader.Read())
				{	
					objElemTemp=objDOM.CreateElement("option");
					objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetValue("REPORT_ID")));
					sReportName=Conversion.ConvertObjToStr(objReader.GetValue("REPORT_NAME"));
					sReportDesc=Conversion.ConvertObjToStr(objReader.GetValue("REPORT_DESC"));
					objElemTemp.InnerText=sReportName+" - " +sReportDesc;
					objElemChild.AppendChild(objElemTemp);
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}

                objElemChild = objDOM.CreateElement("Users");
                objDOM.FirstChild.AppendChild(objElemChild);
				sbSql=new StringBuilder();

				sbSql.Append("SELECT DISTINCT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME");
				sbSql.Append(" FROM USER_TABLE,USER_DETAILS_TABLE WHERE USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID");
                //start rsushilaggar 16/06/2010 MITS 11941
                sbSql.Append(" ORDER BY USER_DETAILS_TABLE.LOGIN_NAME");
                //sbSql.Append(" ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
				//slUsers=new SortedList();
				objReader = DbFactory.GetDbReader(m_sSecDSN,sbSql.ToString());
				while (objReader.Read())
				{	
					try
					{
						sLName=Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
						sFName=Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
						sLName=sFName+" "+sLName;
						sName=Conversion.ConvertObjToStr(objReader.GetValue("LOGIN_NAME"));
						sName=sName+" ("+sLName+")";
						//slUsers.Add(Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")),sName);
                        objElemTemp = objDOM.CreateElement("option");
                        objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
                        objElemTemp.InnerText = sName;
                        objElemChild.AppendChild(objElemTemp);
					}
					catch(Exception p_objException)
					{
					}
				}
				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
                //objElemChild=objDOM.CreateElement("Users");
                //objDOM.FirstChild.AppendChild(objElemChild);
                //objEnum=slUsers.GetEnumerator();
                //while (objEnum.MoveNext())
                //{
                //    objElemTemp=objDOM.CreateElement("option");
                //    objElemTemp.SetAttribute("value",objEnum.Key.ToString());
                //    objElemTemp.InnerText=objEnum.Value.ToString();
                //    objElemChild.AppendChild(objElemTemp);
                //}
                //End rsushilaggar
				return objDOM;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("GrantReportAccess.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objReader!=null)
				{
					objReader.Dispose();
				}
				objDOM=null;
				sbSql=null;
				slUsers=null;
				objEnum=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
			}
		}
		#endregion
	}
}
