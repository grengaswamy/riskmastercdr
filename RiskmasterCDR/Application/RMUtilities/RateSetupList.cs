using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Tushar Agarwal
	///Dated   :   Nov,13 2009
	///Purpose :   Rate List implementation form
    /// MITS 18231
	/// </summary>
	public class RateList:UtilitiesUIListBase
	{
		/// <summary>
		/// String array for Discount List field mapping
		/// </summary>

        private bool bISSuccess = false;
        private int m_iClientId = 0;

		private string[,] arrFields = {
			{"CoverageName","CODE_DESC"},
            {"State","STATE_ID"},
			{"EffectiveDate","EFFECTIVE_DATE"},
			{"ExpirationDate","EXPIRATION_DATE"}};

        /// Name		: RateList
		/// Author		: Tushar Agarwal
		/// Date Created: 11/13/2009		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor for Rate List
		/// </summary>
		/// <param name="p_sConnString"></param>
        public RateList(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Tushar Agarwal
		/// Date Created: 11/13/209		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize private variables and properties
		/// </summary>
		new private void Initialize()
		{
            base.TableName = "SYS_POL_EXP_RATES LEFT OUTER JOIN STATES ON SYS_POL_EXP_RATES.STATE = STATES.STATE_ROW_ID LEFT OUTER JOIN CODES_TEXT ON SYS_POL_EXP_RATES.EXPOSURE_ID = CODES_TEXT.CODE_ID ";
            base.KeyField = "EXP_RATE_ROWID";
            base.RadioName = "RateSetupList";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Tushar Agarwal
		/// Date Created: 11/13/2009
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data for the Rate List form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data structure</param>
		/// <returns>Xml document with data and the form structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			string sLOB = string.Empty;
			string sState = string.Empty;
            //Anu Tennyson MITS 18231 2/1/2010 STARTS
            LocalCache objLocalCache = null;
            int iRateMaintenanceType = 0;
            //Anu Tennyson MITS 18231 ENDS

			try
			{
				sLOB = p_objXmlDocument.SelectSingleNode("//LOBCode").InnerText; 
				sState = p_objXmlDocument.SelectSingleNode("//StateCode").InnerText;
                //Anu Tennyson MITS 18231 2/1/2010 STARTS
                objLocalCache = new LocalCache(base.ConnectString,m_iClientId);
                iRateMaintenanceType = objLocalCache.GetCodeId("C", "RATE_MAINTENANCE_TYPE");
                
                //Start:Added by Nitin Goel:Correct retreive for Rate setup list,05/17/2010,MITS#20744
                //base.WhereClause = " (LINE_OF_BUSINESS =" + sLOB + " OR LINE_OF_BUSINESS = 0) AND STATE =" + sState + " AND RATE_MAINTENANCE_TYPE=" + iRateMaintenanceType + "AND  EXP_RATE_ROWID NOT IN (SELECT DISTINCT DISCOUNT_ROWID FROM SYS_POL_SEL_DISC)";
                // base.WhereClause = " (LINE_OF_BUSINESS =" + sLOB + " OR LINE_OF_BUSINESS = 0) AND STATE =" + sState + " AND RATE_MAINTENANCE_TYPE=" + iRateMaintenanceType + "AND  EXP_RATE_ROWID NOT IN (SELECT DISTINCT DISCOUNT_ROWID FROM SYS_POL_SEL_DISC WHERE IS_RATE_SETUP = 1)";
                base.WhereClause = String.Format(" (LINE_OF_BUSINESS = {0} OR LINE_OF_BUSINESS = 0) AND STATE = {1} AND RATE_MAINTENANCE_TYPE = {2} AND  EXP_RATE_ROWID NOT IN (SELECT DISTINCT DISCOUNT_ROWID FROM SYS_POL_SEL_DISC WHERE IS_RATE_SETUP = 1)",sLOB,sState,iRateMaintenanceType);
                //End:Nitin Goel:Correct retreive for Rate setup list,05/17/2010,MITS#20744
                
                //Anu Tennyson MITS 18231 ENDS
				XMLDoc = p_objXmlDocument;
				base.Get();
				return XMLDoc; 
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("RateList.Get.Err", m_iClientId), p_objEx);
			}
            //Anu Tennyson MITS 18231 2/1/2010 STARTS  
            finally
            {
                if (objLocalCache != null)
                {
                    objLocalCache = null;
                }
            }
            //Anu Tennyson MITS 18231 ENDS
		}

	}
}