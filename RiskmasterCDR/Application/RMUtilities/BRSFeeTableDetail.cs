using System;
using System.Xml;
using System.Collections.Generic; 
using System.Data; 
using Riskmaster.Db; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Text; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   02nd,May 2005
	///Purpose :   Shows the BRS Fee Table Detail form
	/// </summary>
	public class BRSFeeTableDetail : UtilitiesUIBase
	{
		/// <summary>
		/// ingenix/Medicode Table Type (CALC_TYPE)
		/// </summary>
		private const int idbUCR94 = 0;

		/// <summary>
		/// idbUCR95
		/// </summary>
		private const int idbUCR95 = 1;

		/// <summary>
		/// idbWC99
		/// </summary>
		private const int idbWC99 = 2;

		/// <summary>
		/// idbOPt
		/// </summary>
		private const int idbOPt = 3;

		/// <summary>
		/// idbHCPCS
		/// </summary>
		private const int idbHCPCS = 4;

		/// <summary>
		/// idbAnes
		/// </summary>
		private const int idbAnes = 7;

		/// <summary>
		/// idbDent
		/// </summary>
		private const int idbDent = 8;

		/// <summary>
		/// Caredata/Medirisk Table Type (CALC_TYPE)
		/// </summary>
		private const int idbOhio96 = 9;	

		/// <summary>
		/// 98,99 Caredata/Medirisk
		/// </summary>
		private const int idbOhio97 = 10;	

		/// <summary>
		/// Mirage custom
		/// </summary>
		private const int idbStAnth = 11;	

		/// <summary>
		/// Basic User Entered Schedule"
		/// </summary>
		private const int idbBasic = 12;	

		/// <summary>
		/// Workers' Compensation 2000" ingenix/Medicode Table Type
		/// </summary>
		private const int idbWC00 = 13;		

		/// <summary>
		/// Extended BRS Schedule
		/// </summary>
		private const int idbExtended = 14; 

		/// <summary>
		/// Free Entry Pharmacy" To allow Pharmacy Fee Entry without a Table
		/// </summary>
		private const int idbFreePharm = 15;

		/// <summary>
		/// Free Entry Hospital" To allow Hospital Fee Entry without a Table
		/// </summary>
		private const int idbFreeHosp = 16; 

		/// <summary>
		/// Generic Free Entry" To allow Fee Entry without a Table
		/// </summary>
		private const int idbFreeGeneric = 17;

		/// <summary>
		/// OWCP Fee schedule
		/// </summary>
        private const int idbOWCP = 19;   //Geeta MITS 17664

        int m_iClientId = 0;//Add by kuladeep for cloud.
		/// <summary>
		/// String Array database field mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "TABLE_ID"},
			{"UserTableName", "USER_TABLE_NAME"},
			{"TableType", "TABLE_TYPE"},
			{"DbType", "CALC_TYPE"},
			{"FeeSchedule", "ODBC_NAME"},
			{"StartDt", "START_DATE"},
			{"EndDt", "END_DATE"},
			{"state", "STATE"},
			{"priority", "PRIORITY"},
			{"UCRPerc", "UCR_PERCENTILES"},
			{"DefPerc", "DEFAULT_PERCENTILE"},
			{"EobFormName", "EOB_FORM"},
			{"EobCode", "EOB_CODE_FILE"},
			{"CalculationType","CALCULATION_TYPE"},
			{"TableId","TABLE_ID"},
			{"hdnTableType","TABLE_TYPE"}
			};

		/// <summary>
		/// String array for UCR
		/// </summary>
		private string[] arrUCR = 
			{
			"Standard(50,60,70,75,80,85,90,95)",
			"Expanded(25,30,40,50,60,70,80,90)",
			"Combined(25,30,40,50,60,70,75,80,85,90,95)"
			};

		/// Name		: BRSFeeTableDetail
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor with Connection String
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public BRSFeeTableDetail(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;//add by kuladeep for Cloud.
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "FEE_TABLES";
			base.KeyField = "TABLE_ID";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get the data along with xml structure for the BRS Fee Table Detail form
		/// </summary>
		/// <param name="p_objXmlDocument">Input xmldocument with xml structure</param>
		/// <returns>Xml document with data and the UI structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			int iFeeTableId=0;
			int iEOBCodeId=0;
			string sValue="";

            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
			//string sSQL="";
            //END PenTesting - srajindersin - 9th Jan 2012

			XmlNode  objNode = null;
			XmlElement objNewElm = null;			
			DbReader objRdr=null;
			LocalCache objCache=null;
			int j = 0;
			try
			{
				XMLDoc = p_objXmlDocument;
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']"); 
				if(objNode!=null)
					iFeeTableId = Conversion.ConvertStrToInteger(objNode.InnerText);  

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DbType']"); 
				if(objNode!=null)
				{
					if((((XmlElement)objNode).GetAttribute("value")!="") ||
						(((XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FeeSchedule']")).GetAttribute("value")!=""))
					{
						FillODBCNames(p_objXmlDocument); 
						FeeScheduleChange(p_objXmlDocument);
						return p_objXmlDocument;
					}
				}
				if(iFeeTableId>0)
					base.Get();

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DbType']"); 
				//************Mohit for Bug No. 000177 ********************************
				//*********************************************************************
				objNewElm = p_objXmlDocument.CreateElement("option");
				objNewElm.SetAttribute("value", "-1"); // Mihika DN. 1844 Corrected the wrongly displayed Database type
				objNewElm.InnerText = " ";
				objNode.AppendChild(objNewElm);
				//*********************************************************************
                foreach (KeyValuePair<int, string> kvp in UTILITY.BRSDatabaseType)
                {
                    objNewElm = p_objXmlDocument.CreateElement("option");
                    objNewElm.SetAttribute("value", kvp.Key.ToString());
                    objNewElm.InnerText = kvp.Value;
                    objNode.AppendChild(objNewElm);
                }
                objCache = new LocalCache(ConnectString, m_iClientId);  
				iEOBCodeId = objCache.GetTableId("EOB_CODES");

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='EobCode']");

                //PenTesting - srajindersin - 9th Jan 2012
                strSQL.Append("SELECT SYSTEM_TABLE_NAME,TABLE_ID FROM GLOSSARY  ");
                strSQL.Append(string.Format(" WHERE RELATED_TABLE_ID = {0}", "~RELATED_TABLE_ID~"));

                dictParams.Add("RELATED_TABLE_ID", iEOBCodeId);

                objRdr = DbFactory.ExecuteReader(base.ConnectString, strSQL.ToString(), dictParams);

				//sSQL = "SELECT SYSTEM_TABLE_NAME,TABLE_ID FROM GLOSSARY WHERE RELATED_TABLE_ID = " + iEOBCodeId;
				//objRdr = DbFactory.GetDbReader(base.ConnectString ,sSQL);
                //END PenTesting - srajindersin - 9th Jan 2012

				while(objRdr.Read())
				{
					objNewElm = XMLDoc.CreateElement("option");
					objNewElm.SetAttribute("value", objRdr.GetInt32("TABLE_ID").ToString());
					objNewElm.InnerText = objRdr.GetString("SYSTEM_TABLE_NAME");
					objNode.AppendChild(objNewElm);
				}
				objRdr.Dispose();

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='UCRPerc']");
				sValue = objNode.InnerText;  

				objNewElm = XMLDoc.CreateElement("option");
				objNewElm.SetAttribute("value", "0");
				objNewElm.InnerText = "";
				objNode.AppendChild(objNewElm);

				for(int i = 0;i< arrUCR.GetLength(0);i++)
				{
					j = i + 1;
					objNewElm = XMLDoc.CreateElement("option");
					objNewElm.SetAttribute("value", j.ToString());
					objNewElm.InnerText = arrUCR[i];
					objNode.AppendChild(objNewElm);
				}
 
				FillODBCNames(p_objXmlDocument);
				return p_objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFeeTableDetail.Get.Err", m_iClientId), p_objEx); 
			}
			finally
			{
               	objNode = null;
				objNewElm = null;			
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
				if(objCache!=null)
				{
					objCache.Dispose();
				}
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data for Fee table detail
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data</param>
		/// <returns>Saved xml document</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			int iFeeTableId = 0;
			int iState=0;
			int iPriority=0;
			int iUCRPerc=0;
			int iDefPerc=0;			
			int iTableType=0;
			int iDbType=0;
			int iCalculationType=0;
			string sFeeSchedule="";
			string sStartDt="";
			string sEndDt="";
			string sUserTableName ="";
			string sEobFormName="";
			string sEobCode="";
			DbConnection objCon=null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			XmlElement objElm=null;
			XmlNode objNode=null;
			DbReader objRdr=null;

            //PenTesting - srajindersin - 9th Jan 2012
            StringBuilder strSQL = new StringBuilder();
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012
			try
			{
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']"); 
				if(objNode!=null)
					iFeeTableId = Conversion.ConvertStrToInteger(objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='UserTableName']"); 
				if(objNode!=null)
					sUserTableName  = objNode.InnerText;

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableType']"); 
				if(objNode!=null)
					iTableType = Conversion.ConvertStrToInteger(((XmlElement)objNode).GetAttribute("value"));

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DbType']"); 
				if(objNode!=null)
					iDbType = Conversion.ConvertStrToInteger(objElm.GetAttribute("value"));

				objElm =  (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FeeSchedule']"); 
				if(objNode!=null)
					sFeeSchedule = objElm.GetAttribute("value"); 

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='StartDt']"); 
				if(objNode!=null)
					sStartDt = Conversion.GetDate (objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='EndDt']"); 
				if(objNode!=null)
					sEndDt = Conversion.GetDate (objNode.InnerText);

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='UCRPerc']"); 
				if(objElm!=null)
					iUCRPerc= Conversion.ConvertStrToInteger(objElm.GetAttribute("value"));

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='DefPerc']"); 
				if(objElm!=null)
					iDefPerc = Conversion.ConvertStrToInteger(objElm.InnerText);   

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='EobFormName']"); 
				if(objNode!=null)
					sEobFormName = objNode.InnerText;

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EobCode']"); 
				if(objNode!=null)
					sEobCode = objElm.GetAttribute("value");

				objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='state']"); 
				if(objNode!=null)
					iState = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='iCalculationType']"); 
				if(objNode!=null)
					iCalculationType =  Conversion.ConvertStrToInteger(objNode.InnerText);

				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='priority']"); 
				if(objNode!=null)
					iPriority =  Conversion.ConvertStrToInteger(objNode.InnerText);

				switch(iCalculationType)
				{
					case 0:
						break;
					case 4:
					switch(iUCRPerc)
					{
						case 15:
							iDefPerc= 98;
							break;
						case 16:
							iDefPerc=99;
							break;
						default:
							iDefPerc= (iUCRPerc * 5) + 25;
							break;
					}
						break;
					default:
						iDefPerc=0;
						break;
				}

				objCon = DbFactory.GetDbConnection(ConnectString);
				objCon.Open();	
				if(iFeeTableId==0)
				{
                    //PenTesting - srajindersin - 9th Jan 2012
                    strSQL.Append("SELECT USER_TABLE_NAME FROM FEE_TABLES ");
                    strSQL.Append(string.Format(" WHERE USER_TABLE_NAME = {0}", "~USER_TABLE_NAME~"));

                    dictParams.Add("USER_TABLE_NAME", sUserTableName);

                    objRdr = DbFactory.ExecuteReader(base.ConnectString, strSQL.ToString(), dictParams);

                    //objRdr = objCon.ExecuteReader("SELECT USER_TABLE_NAME FROM FEE_TABLES WHERE USER_TABLE_NAME='" + sUserTableName + "'");
                    //END PenTesting - srajindersin - 9th Jan 2012
					  
					if(objRdr.Read())
					{
						throw new RMAppException(Globalization.GetString("BRSFeeTableDetail.Save.DupErr",m_iClientId));  
					}
					objRdr.Dispose();
				}
				/*
				 * Date value fix by Tanuj on 13-Jan-2006
				 * */
				if( sEndDt.Trim() == "" )
				{
					sEndDt = "00000000" ;
				}
				if(iFeeTableId==0)
                    iFeeTableId = Utilities.GetNextUID(objCon, "FEE_TABLES", m_iClientId);

				objCommand=objCon.CreateCommand();
				objTran=objCon.BeginTransaction();
				objCommand.Transaction=objTran;
				objCommand.CommandType=CommandType.Text;

                //PenTesting - srajindersin - 9th Jan 2012
                strSQL = new StringBuilder();
                strSQL.Append(string.Format(" DELETE FROM FEE_TABLES WHERE TABLE_ID= {0} ", "~TABLE_ID~"));

                dictParams.Clear();
                dictParams.Add("TABLE_ID", iFeeTableId);

                objCommand.CommandText = strSQL.ToString();
                DbFactory.ExecuteNonQuery(objCommand, dictParams);

                //objCommand.CommandText = "DELETE FROM FEE_TABLES WHERE TABLE_ID=" + iFeeTableId;
                //objCommand.ExecuteNonQuery();

                strSQL = new StringBuilder();
                strSQL.Append("INSERT INTO FEE_TABLES (TABLE_ID,USER_TABLE_NAME,TABLE_TYPE,CALC_TYPE,");
                strSQL.Append(" ODBC_NAME,START_DATE,END_DATE,STATE,PRIORITY,UCR_PERCENTILES,DEFAULT_PERCENTILE,");
                strSQL.Append(" EOB_FORM,EOB_CODE_FILE) ");
                strSQL.Append(string.Format(" VALUES({0}, {1}, {2}, {3},", "~TABLE_ID~", "~USER_TABLE_NAME~", "~TABLE_TYPE~", "~CALC_TYPE~"));
                strSQL.Append(string.Format(" {0}, {1}, {2}, {3}, {4},", "~ODBC_NAME~", "~START_DATE~", "~END_DATE~", "~STATE~","~PRIORITY~"));
                strSQL.Append(string.Format(" {0}, {1}, {2}, {3})", "~UCR_PERCENTILES~", "~DEFAULT_PERCENTILE~", "~EOB_FORM~", "~EOB_CODE_FILE~"));

                dictParams.Clear();
                //dictParams.Add("TABLE_ID", iFeeTableId); //it is already avaiable in command object from previous query
                dictParams.Add("USER_TABLE_NAME", sUserTableName);
                dictParams.Add("TABLE_TYPE", iTableType);
                dictParams.Add("CALC_TYPE", iDbType);
                dictParams.Add("ODBC_NAME", sFeeSchedule);
                dictParams.Add("START_DATE", sStartDt);
                dictParams.Add("END_DATE", sEndDt);
                dictParams.Add("STATE", iState);
                dictParams.Add("PRIORITY", iPriority);
                dictParams.Add("UCR_PERCENTILES", iUCRPerc);
                dictParams.Add("DEFAULT_PERCENTILE", iDefPerc);
                dictParams.Add("EOB_FORM", sEobFormName);
                dictParams.Add("EOB_CODE_FILE", sEobCode);

                objCommand.CommandText = strSQL.ToString();
                DbFactory.ExecuteNonQuery(objCommand, dictParams);

                //objCommand.CommandText = "INSERT INTO FEE_TABLES (TABLE_ID,USER_TABLE_NAME,TABLE_TYPE,CALC_TYPE," + 
                //    "ODBC_NAME,START_DATE,END_DATE,STATE,PRIORITY,UCR_PERCENTILES,DEFAULT_PERCENTILE," + 
                //    "EOB_FORM,EOB_CODE_FILE) VALUES(" + 
                //    iFeeTableId + ",'" + sUserTableName + "'," + iTableType + "," + iDbType + ",'" +
                //    sFeeSchedule + "','" + sStartDt + "','" + sEndDt + "'," + iState + "," + iPriority + "," +
                //    iUCRPerc + "," + iDefPerc + ",'" + sEobFormName + "','" + sEobCode + "')";
                //objCommand.ExecuteNonQuery();
                //END PenTesting - srajindersin - 9th Jan 2012

				objTran.Commit();
				objCon.Dispose(); 
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				if(objTran!=null)
					objTran.Rollback();
                throw new RMAppException(Globalization.GetString("BRSFeeTableDetail.Save.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				if(objTran!=null)
				{
					objTran.Dispose(); 
				}
				if(objCon!=null)
				{
					objCon.Dispose(); 
				}
				objCommand =null;
				objNode = null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
                objElm = null;
                
			}
		}

		/// Name		: FillODBCNames
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fills the ODBC list
		/// </summary>
		/// <returns>filled in combo list</returns>
		public void FillODBCNames(XmlDocument p_objXmlDoc)
		{
			XmlNode objNode=null;
			DbReader objRdr=null;
			string sValue="";
			string sSQL="";
			XmlElement objNewElm=null;
            //PenTesting - srajindersin - 9th Jan 2012
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012

			try
			{
				objNode = p_objXmlDoc.SelectSingleNode("//control[@name='DbType']");  
				sValue = ((XmlElement)objNode).GetAttribute("value");

                objNode = p_objXmlDoc.SelectSingleNode("//control[@name='FeeSchedule']"); 
				objNode.InnerText ="";

                //PenTesting - srajindersin - 9th Jan 2012
                if (sValue == "12")
                {
                    sSQL = "SELECT FEESCHD_NAME FROM BRS_FEESCHD ORDER BY FEESCHD_NAME";
                    objRdr = DbFactory.GetDbReader(base.ConnectString, sSQL);
                }
                else
                {
                    sSQL = string.Format("SELECT FEESCHD_NAME FROM BRS_FEESCHD WHERE CALC_TYPE = {0} ORDER BY FEESCHD_NAME", "~CALC_TYPE~");
                    dictParams.Add("CALC_TYPE", sValue);
                    objRdr = DbFactory.ExecuteReader(base.ConnectString, sSQL, dictParams);
                }

                //if( sValue =="12" )
                //    sSQL = "SELECT FEESCHD_NAME FROM BRS_FEESCHD ORDER BY FEESCHD_NAME";
                //else
                //    sSQL = "SELECT FEESCHD_NAME FROM BRS_FEESCHD WHERE CALC_TYPE = '" + sValue + "' ORDER BY FEESCHD_NAME";

                //objRdr = DbFactory.GetDbReader(base.ConnectString ,sSQL);

                //END PenTesting - srajindersin - 9th Jan 2012
				while(objRdr.Read())
				{
					objNewElm = XMLDoc.CreateElement("option");
					objNewElm.SetAttribute("value", objRdr.GetString(0));
					objNewElm.InnerText = objRdr.GetString(0);
					objNode.AppendChild(objNewElm);
				}
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFeeTableDetail.FillODBCNames.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				objNode=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
			}
		}

		/// Name		: FeeScheduleChange
		/// Author		: Pankaj Chowdhury
		/// Date Created: 07/06/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
			/// </summary>
		/// <returns>Changes Label</returns>
		private void FeeScheduleChange(XmlDocument p_objXmlDoc)
		{
			string sSQL="";
			XmlNode objNod=null;
			string sFeeSched="";
			DbReader objRdr=null;
			XmlElement objFile=null;
            //PenTesting - srajindersin - 9th Jan 2012
            Dictionary<string, object> dictParams = new Dictionary<string, object>();
            //END PenTesting - srajindersin - 9th Jan 2012

			try
			{
				sFeeSched = ((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FeeSchedule']")).GetAttribute("value") ; 
				objNod = p_objXmlDoc.SelectSingleNode("//control//option[@value='" + sFeeSched + "']");
				objFile = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='FileName']");
				if(objNod!=null)
				{
                    //PenTesting - srajindersin - 9th Jan 2012
                    sSQL = string.Format("SELECT DB_PATH FROM BRS_FEESCHD WHERE FEESCHD_NAME = {0}","~FEESCHD_NAME~");

                    dictParams.Add("FEESCHD_NAME", objNod.InnerText);
                    objRdr = DbFactory.ExecuteReader(ConnectString, sSQL, dictParams);

                    //sSQL = "SELECT DB_PATH FROM BRS_FEESCHD WHERE FEESCHD_NAME = '" + objNod.InnerText + "'";
                    //objRdr = DbFactory.GetDbReader(ConnectString, sSQL);
                    //END PenTesting - srajindersin - 9th Jan 2012
					
					if(objRdr.Read())
					{
						objFile.InnerText=objRdr.GetString("DB_PATH");
						objFile.SetAttribute("title",objRdr.GetString("DB_PATH"));
					}
				}
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSFeeTableDetail.FeeScheduleChange.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
				objNod=null;
				objFile=null;
			}
		}
	}
}