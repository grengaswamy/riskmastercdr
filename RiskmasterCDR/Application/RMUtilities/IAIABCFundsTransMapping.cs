using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Funds Transactions Mapping.
	/// </summary>
	public class IAIABCFundsTransMapping:UtilityFormBase
	{
		#region Input/Output Xml
		/*
		<form name="IAIABCFundsMapping" title="IAIABC Funds Mapping" topbuttons="1" supp="">
		<toolbar>
			<button type="save" title="Save" />
		</toolbar>
		<group name="IAIABCFundsMapping" title="IAIABC Funds Mapping">
			<displaycolumn>
			<control name="DescLabel" type="labelonly" title="A benefit can have an unlimited number of Transaction Types.  A Transaction Type should be used only one time.">
			</control>
			</displaycolumn>
			<displaycolumn>
			<control name="IAIABCBenefits" type="combobox" title="IAIABC Benefits" value="">
				<option value=""> </option>
				<option value="4230">010 Fatal</option>
			</control>
			<control name="TransactionTypeBenefits" type="combobox" title="Transaction Type" value="">
				<option value="">
				</option>
				<option value="291">BI Bodily Injury</option>
			</control>
			</displaycolumn>
			<displaycolumn>
			<control name="IAIABCBenefitsList" type="radiolist" radio="0">
				<listhead>
				<rowhead colname="IAIABCBenefitType">IAIABC Benefit Type</rowhead>
				<rowhead colname="IAIABCTransactionType">IAIABC Transaction Type</rowhead>
				<rowhead colname="BenefitsRowId">Row Id</rowhead>
				</listhead>
				<listrow>
				<rowtext type="label" name="IAIABCBenefitType" title="010 Fatal">4230</rowtext>
				<rowtext type="label" name="IAIABCTransactionType" title="BI Bodily Injury">291</rowtext>
				<rowtext type="label" name="BenefitsRowId" title="10" />
				</listrow>
			</control>
			</displaycolumn>
			<displaycolumn>
			<control name="IAIABCPaidToDate" type="combobox" title="IAIABC Paid To Date" value="">
				<option value=""> </option>
				<option value="4241">300 Fureral Expenses</option>
				</control>
			<control name="TransactionTypePaidToDate" type="combobox" title="Transaction Type" 

		value="">
				<option value="">
				</option>
				<option value="291">BI Bodily Injury</option>
				</control>
			</displaycolumn>
			<displaycolumn>
			<control name="IAIABCPaidToDateList" type="radiolist" value="">
				<listhead>
				<rowhead colname="IAIABCPTDType">IAIABC PTD Type</rowhead>
				<rowhead colname="IAIABCPaidToDatTransactionType">IAIABC Transaction 

		Type</rowhead>
				<rowhead colname="PaidToDatRowId">Row Id</rowhead>
				</listhead>
				<listrow>
				<rowtext type="radio" name="radiolist7" value="4242" title=" ">
				</rowtext>
				<rowtext type="label" name="IAIABCPaidToDatTransactionType" title="DB Doctor Bill">2362</rowtext>
				<rowtext type="label" name="PaidToDatRowId" title="14" />
				</listrow>
				</control>
			</displaycolumn>
		</group>
		</form>
		*/
		#endregion

		#region Private Variables
		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields=
		{
			{"IAIABCBenefitType", "DN_CODE_ID"},
			{"IAIABCTransactionType", "TRANS_CODE_ID"},
			{"IAIABCPTDType", "DN_CODE_ID"},
			{"IAIABCPaidToDatTransactionType", "TRANS_CODE_ID"},
			{"PaidToDatRowId", "IA_ROW_ID"},
			{"BenefitsRowId", "IA_ROW_ID"},
			
		};
		/// <summary>
		/// Field Mapping with SQL queries
		/// </summary>
		private string[,] arrChilds = {
			//{"XMLTag", "Query"}
			{"IAIABCBenefitsList","SELECT TRANS_CODE_ID, DN_CODE_ID,IA_ROW_ID FROM IAIABC_FUNDS_MAPPI WHERE DN_NUMBER = '85'"},
			{"IAIABCPaidToDateList","SELECT TRANS_CODE_ID, DN_CODE_ID,IA_ROW_ID FROM IAIABC_FUNDS_MAPPI WHERE DN_NUMBER = '95'"}
									};
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
        private int m_iClientId = 0;//sonali

	#endregion

		#region Constructor
		/// <summary>
		/// Default constructor
		/// </summary>
		/// public IAIABCFundsTransMapping(){}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public IAIABCFundsTransMapping(string p_sConnString, int p_iClientId)//sonali
            : base(p_sConnString, p_iClientId)
		{
			try
			{
				ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
	
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Constructor.Error",m_iClientId),p_objEx);//sonali
			}
	
		}
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public IAIABCFundsTransMapping(string p_sLoginName, string p_sConnectionString, int p_iClientId)//sonali
            : base(p_sConnectionString, p_iClientId) 
		{	
			try
			{
				m_sUserName = p_sLoginName;
				ConnectString = p_sConnectionString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Constructor.Error",m_iClientId),p_objEx);//sonali
			}
	
		}
		#endregion

		#region Private Functions 
		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			try
			{
				TableName = "";
				KeyField = "";
				InitFields(arrFields);
				base.ChildArray = arrChilds;	
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Initialize.Error",m_iClientId),p_objEx);//sonali
			}
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument,UserLogin objUser)
		{
			XmlNodeList objNodLst=null;
			XmlElement objElement=null;
			XmlNode objNode=null;
			string sValue="";
			LocalCache objCache=null;
			string sShortCode="";
			string sShortDesc="";
			string sTransType="";
			string sTableId="";
			string sValue1="";
			string sValue2="";
			string sMastReserType="";
			DbReader objRead=null;
			XmlNodeList objNodTmpLst =null;
			StringBuilder sbSql=null;
			try
			{
				XMLDoc = p_objXmlDocument;
				Get();
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sTransType=objCache.GetTableId("TRANS_TYPES").ToString();
				sMastReserType=objCache.GetTableId("MASTER_RESERVE").ToString();
				
				objCache.Dispose();
				sbSql=new StringBuilder();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TransactionTypeBenefits']");
				sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
				sbSql.Append(" WHERE CODES.TABLE_ID ="+sTransType);
				sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID");
				sbSql.Append(" AND CODES.RELATED_CODE_ID IN");
				sbSql.Append(" (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES");
				sbSql.Append(" WHERE RESERVE_TYPE_CODE = CODE_ID");
				sbSql.Append(" AND RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID ="+sMastReserType+")");
				sbSql.Append(" AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 243)");
				sbSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES_TEXT.LANGUAGE_CODE = 1033"); //Aman ML Change
				sbSql.Append(" ORDER BY CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                //Aman ML Change
                sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "TRANS_TYPES", objUser.objUser.NlsCode);
                //Aman ML Change
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue(0)));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim()+" "
						+Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				sbSql=new StringBuilder();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TransactionTypePaidToDate']");
				sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
				sbSql.Append(" WHERE CODES.TABLE_ID ="+sTransType);
				sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID");
				sbSql.Append(" AND CODES.RELATED_CODE_ID IN");
				sbSql.Append(" (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES");
				sbSql.Append(" WHERE RESERVE_TYPE_CODE = CODE_ID");
				sbSql.Append(" AND RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID ="+sMastReserType+")");
				sbSql.Append(" AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 243)");
				sbSql.Append(" AND CODES.DELETED_FLAG = 0  AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
				sbSql.Append(" ORDER BY CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                //Aman ML Change
                sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "TRANS_TYPES", objUser.objUser.NlsCode);
                //Aman ML Change
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue(0)));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim()+" "
						+Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCBenefits']");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sTableId=objCache.GetTableId("IAIABC_PAY_DN85").ToString();
				objCache.Dispose();
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE FROM CODES");
				sbSql.Append(" WHERE CODES.TABLE_ID ="+sTableId);
				sbSql.Append(" ORDER BY CODES.SHORT_CODE");
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCPaidToDate']");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sTableId=objCache.GetTableId("IAIABC_PTD_DN95").ToString();
				objCache.Dispose();
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE FROM CODES");
				sbSql.Append(" WHERE CODES.TABLE_ID ="+sTableId);
				sbSql.Append(" ORDER BY CODES.SHORT_CODE");
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='TransactionTypeBenefits']"); 
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				if (objNode.FirstChild!=null)
				objNode.InsertBefore((XmlNode)objElement,objNode.FirstChild);

				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='TransactionTypePaidToDate']"); 
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				if (objNode.FirstChild!=null)
				objNode.InsertBefore((XmlNode)objElement,objNode.FirstChild);

				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCBenefits']"); 
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				if (objNode.FirstChild!=null)
				objNode.InsertBefore((XmlNode)objElement,objNode.FirstChild);

				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCPaidToDate']"); 
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				if (objNode.FirstChild!=null)
				objNode.InsertBefore((XmlNode)objElement,objNode.FirstChild);
							
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCBenefits']");  
				objNodLst=objNode.ChildNodes;
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				foreach(XmlNode objTemp in objNodLst)
				{
				
					if (objTemp.NodeType==XmlNodeType.Element)
					{
						sValue=objTemp.Attributes["value"].Value;
						objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
						objTemp.InnerText=sShortCode+" "+sShortDesc;
					}
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCPaidToDate']");  
				objNodLst=objNode.ChildNodes;
				foreach(XmlNode objTemp in objNodLst)
				{
					if (objTemp.NodeType==XmlNodeType.Element)
					{
						sValue=objTemp.Attributes["value"].Value;
						objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
						objTemp.InnerText=sShortCode+" "+sShortDesc;
					}
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCBenefitsList']");  
				objNodLst=objNode.SelectNodes("./listrow/rowtext[@name!='BenefitsRowId']");
				foreach(XmlNode objTemp in objNodLst)
				{
					if (objTemp.NodeType==XmlNodeType.Element)
					{
						sValue=objTemp.Attributes["title"].Value;
						objTemp.InnerText=sValue;
						objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
						objTemp.Attributes["title"].Value=sShortCode+" "+sShortDesc;
					}
				}
				
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCPaidToDateList']");  
				objNodLst=objNode.SelectNodes("./listrow/rowtext[@name!='PaidToDatRowId']");
				foreach(XmlNode objTemp in objNodLst)
				{
					if (objTemp.NodeType==XmlNodeType.Element)
					{
						sValue=objTemp.Attributes["title"].Value;
						objTemp.InnerText=sValue;
						objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
						objTemp.Attributes["title"].Value=sShortCode+" "+sShortDesc;
					}
				}
				objNodLst = p_objXmlDocument.SelectNodes("//control[@name='IAIABCBenefitsList']/listrow");  
				foreach(XmlNode objTemp in objNodLst)
				{
					objNodTmpLst = objTemp.SelectNodes("./rowtext");
					foreach(XmlNode objTmp in objNodTmpLst)
					{
						if (objTmp.NodeType==XmlNodeType.Element)
						{
							if (objTmp.Attributes["name"].Name!="BenefitsRowId")
							{
								sValue1=sValue1+objTmp.InnerText+"|";
							}
						}
					}
					sValue1=sValue1+"**";
				}
				objNode=p_objXmlDocument.SelectSingleNode("//form/group");
				objElement=p_objXmlDocument.CreateElement("BenefitListValues");
				objElement.InnerText=sValue1;
				objNode.AppendChild(objElement);
				objNodLst = p_objXmlDocument.SelectNodes("//control[@name='IAIABCPaidToDateList']/listrow");  
				foreach(XmlNode objTemp in objNodLst)
				{
					objNodTmpLst = objTemp.SelectNodes("./rowtext");
					foreach(XmlNode objTmp in objNodTmpLst)
					{
						if (objTmp.NodeType==XmlNodeType.Element)
						{
							if (objTmp.Attributes["name"].Name!="PaidToDatRowId")
							{
								sValue2=sValue2+objTmp.InnerText+"|";
							}
						}
					}
					sValue2=sValue2+"**";
				}
				objNode=p_objXmlDocument.SelectSingleNode("//form/group");
				objElement=p_objXmlDocument.CreateElement("PaidToDateListValues");
				objElement.InnerText=sValue2;
				objNode.AppendChild(objElement);
				objCache.Dispose(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
					throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Get.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				objNodLst=null;
				objElement=null;
				objNode=null;
				if (objCache != null)
                {
                    objCache.Dispose();
                }
				if (objRead!=null)
				{
					objRead.Dispose();
				}
                objNodTmpLst = null;
                sbSql = null;
				
			}
				return XMLDoc;
		}
		/// Name		: DeletePaidToDateMapping
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes Paid to Date Mapping information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool DeletePaidToDateMapping(XmlDocument p_objXmlDocument)
		{
			ArrayList arrlstSql=null;
			string sSQL="";
			bool bReturnValue=false;
			string sRecord="";
			XmlNode objNode=null;
			string[] arrValues=null;
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCPaidToDateList']"); 
				if (objNode.InnerText!=null)
				{
					arrValues=objNode.InnerText.Split(" ".ToCharArray());
				}
				arrlstSql=new ArrayList();
				if(arrValues!=null)
				{
					for (int i=0;i<arrValues.Length;i++)
					{
						if (!arrValues[i].Trim().Equals(""))
						{
							sSQL="DELETE FROM IAIABC_FUNDS_MAPPI WHERE IA_ROW_ID="+arrValues[i];
							arrlstSql.Add(sSQL);
						}
					}
				}
                UTILITY.Save(arrlstSql, base.ConnectString, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.DeletePaidToDateMapping.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				arrlstSql=null;
				objNode=null;
				arrValues=null;
			}
			return bReturnValue;
		}
		/// Name		: DeleteBenefitsMapping
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes Benefits Mapping information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool DeleteBenefitsMapping(XmlDocument p_objXmlDocument)
		{
			ArrayList arrlstSql=null;
			string sSQL="";
			bool bReturnValue=false;
			string sRecord="";
			XmlNode objNode=null;
			string[] arrValues=null;
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='IAIABCBenefitsList']"); 
				if (objNode.InnerText!=null)
				{
					arrValues=objNode.InnerText.Split(" ".ToCharArray());
				}
				arrlstSql=new ArrayList();
				if (arrValues!=null)
				{
					for (int i=0;i<arrValues.Length;i++)
					{
						if (!arrValues[i].Trim().Equals(""))
						{
							sSQL="DELETE FROM IAIABC_FUNDS_MAPPI WHERE IA_ROW_ID="+arrValues[i];
							arrlstSql.Add(sSQL);
						}
					}
				}
                UTILITY.Save(arrlstSql, base.ConnectString, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.DeleteBenefitsMapping.Error",m_iClientId),p_objEx);//sonail
			}
			finally
			{
				arrlstSql=null;
				objNode=null;
				arrValues=null;
			}
			return bReturnValue;
		}
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//control[@name='"+p_sValue+"']");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}
		/// Name		: SaveBenefits
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves IAIABC Funds Transaction Mapping
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool SaveBenefits(XmlDocument p_objXmlDocument)
		{
			ArrayList objQuery=null;
			string sValue1="";
			string sValue2="";
			string sQuery="";
			string sDate="";
			DbReader objRead=null;
			string sInsertQuery="";
			int iNextId=0;
			bool bReturnValue=false;
			string sUser=m_sUserName;
			try
			{
				XMLDoc = p_objXmlDocument;
				objQuery=new ArrayList();
				sValue1=GetValue(p_objXmlDocument,"IAIABCBenefits");
				sValue2=GetValue(p_objXmlDocument,"TransactionTypeBenefits");
				if (sValue1!="" && sValue2!="")
				{
					sQuery="SELECT IA_ROW_ID FROM IAIABC_FUNDS_MAPPI WHERE DN_NUMBER=85 AND TRANS_CODE_ID='"+sValue1+"' AND DN_CODE_ID='"+sValue2+"'";
					objRead=DbFactory.GetDbReader(base.ConnectString,sQuery);
					if (objRead.Read())
					{
						objRead.Close();
						throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Save.ErrorTransactionTypeBenefits",m_iClientId)); //sonali
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
                    iNextId = Utilities.GetNextUID(base.ConnectString, "IAIABC_FUNDS_MAPPI", m_iClientId);
					sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
					sInsertQuery="INSERT INTO IAIABC_FUNDS_MAPPI (IA_ROW_ID,DN_NUMBER,TRANS_CODE_ID,DN_CODE_ID,DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER) VALUES ("+iNextId.ToString()
						+",85,"+sValue2+","+sValue1+","+sDate+",'"+sUser+"',"+sDate+",'"+sUser+"')";
					objQuery.Add(sInsertQuery);
				}
				base.Save(objQuery);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Save.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				objQuery=null;
				if (objRead!=null)
				{
					objRead.Dispose();
				}
				
			}
			return bReturnValue;
			
		}
		/// Name		: SavePaidToDate
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves IAIABC Funds Transaction Mapping
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool SavePaidToDate(XmlDocument p_objXmlDocument)
		{
			ArrayList objQuery=null;;
			string sValue1="";
			string sValue2="";
			string sQuery="";
			string sDate="";
			DbReader objRead=null;
			string sInsertQuery="";
			int iNextId=0;
			bool bReturnValue=false;
			string sUser=m_sUserName;
			try
			{
				XMLDoc = p_objXmlDocument;
				objQuery=new ArrayList();
				sValue1=GetValue(p_objXmlDocument,"IAIABCPaidToDate");
				sValue2=GetValue(p_objXmlDocument,"TransactionTypePaidToDate");
				if (sValue1!="" && sValue2!="")
				{
					sQuery="SELECT IA_ROW_ID FROM IAIABC_FUNDS_MAPPI WHERE DN_NUMBER=95 AND TRANS_CODE_ID='"+sValue1+"' AND DN_CODE_ID='"+sValue2+"'";
					objRead=DbFactory.GetDbReader(base.ConnectString,sQuery);
					if (objRead.Read())
					{
						objRead.Close();
						throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Save.ErrorTransactionTypePaidToDate",m_iClientId)); //sonali
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
                    iNextId = Utilities.GetNextUID(base.ConnectString, "IAIABC_FUNDS_MAPPI", m_iClientId);
					sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
					sInsertQuery="INSERT INTO IAIABC_FUNDS_MAPPI (IA_ROW_ID,DN_NUMBER,TRANS_CODE_ID,DN_CODE_ID,DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER) VALUES ("+iNextId.ToString()
						+",95,"+sValue2+","+sValue1+","+sDate+",'"+sUser+"',"+sDate+",'"+sUser+"')";
					objQuery.Add(sInsertQuery);
				}
				base.Save(objQuery);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("IAIABCFundsTransMapping.Save.Error",m_iClientId),p_objEx);//sonali
			}
			finally
			{
				objQuery=null;
				if (objRead!=null)
				{
					objRead.Dispose();
				}
				
			}
			return bReturnValue;
			
		}
		#endregion
	}
}
