using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes; 


namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   Supplemental data list Form 
	/// </summary>
	public class SupplementalData:UtilitiesUIListBase 
	{
        int m_iClientId = 0;
		/// <summary>
		/// String array for database field mapping
		/// </summary>
		private string[,] arrFields = {
			{"UserTableName","TABLE_NAME"},
			{"SysTableName","SYSTEM_TABLE_NAME"}
									  };
		/// Name		: SupplementalData
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
		public SupplementalData(string p_sConnString, int p_iClientId):base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialized private variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "GLOSSARY, GLOSSARY_TEXT ";
			base.KeyField = "GLOSSARY.TABLE_ID";
			base.RadioName = "SupplementalList";
			LocalCache objCache = new LocalCache(ConnectString,m_iClientId);
			base.WhereClause = "GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND  GLOSSARY.GLOSSARY_TYPE_CODE = " +
				objCache.GetCodeId("6", "GLOSSARY_TYPES")  + 
				"AND GLOSSARY_TEXT.LANGUAGE_CODE = 1033";
			base.OrderByClause ="GLOSSARY_TEXT.TABLE_NAME";
			objCache.Dispose();
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the xml document structure and data
		/// </summary>
		/// <param name="p_objXmlDocument">Input xmldocument structure</param>
		/// <returns>Output xml document with data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNodeList objNodLst=null;
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				objNodLst = p_objXmlDocument.SelectNodes("//listrow//RowId");
				foreach(XmlNode objNod in objNodLst)
				{
                    //Changed by Abhishek to fix utilities base lst changes
					//objNod.InnerText = objNod.NextSibling.InnerText + "|" + objNod.NextSibling.NextSibling.InnerText;
                    objNod.InnerText = objNod.PreviousSibling.PreviousSibling.InnerText + "|" + objNod.PreviousSibling.InnerText;
				
                }
				return XMLDoc; 
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("SupplementalData.Get.Err", m_iClientId),p_objEx);
			}
			finally
			{
				objNodLst=null;
			}
		}
	}
}