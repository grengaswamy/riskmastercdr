﻿using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Workers Compensation Transactions Mapping.
	/// </summary>
	public class WorkersCompTransMapping:UtilityFormBase
	{
		#region Private variables
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
        //Aman ML Change
        /// <summary>
        /// user's Language Code
        /// </summary>
        private int m_iLangCode = 0;
        private int m_iClientId = 0;
		#endregion
        #region Properties
        //Aman ML Change
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion
        #region Constructor
        /// <summary>
		/// Default Constructor
		/// </summary>
		/// public WorkersCompTransMapping(){}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public WorkersCompTransMapping(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			try
			{
                m_iClientId = p_iClientId;
				ConnectString = p_sConnString;	
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.Constructor.Error", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public WorkersCompTransMapping(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			try
			{
                m_iClientId = p_iClientId;
				m_sUserName = p_sLoginName;
				ConnectString = p_sConnectionString;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.Constructor.Error", m_iClientId), p_objEx);
			}
	
		}
		#endregion

		#region Private Functions
		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			try
			{
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.Initialize.Error", m_iClientId), p_objEx);
			}
		}
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//control[@name='"+p_sValue+"']");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}
		private long GetScreenIDFromWCPBenefit(long p_lTemp,long p_lStateRowID)
		{
			DbReader objRead=null;
			string sQuery="";
			long lValue=-1;
			try
			{
				sQuery="SELECT TAB_INDEX FROM WCP_BENEFIT_LKUP WHERE JURIS_ROW_ID ='"+p_lStateRowID.ToString()+"' AND BENEFIT_LKUP_ID ='"+p_lTemp.ToString()+"'";
				objRead=DbFactory.GetDbReader(base.ConnectString,sQuery);
				if (objRead.Read())
				{
                    lValue = Conversion.ConvertObjToInt64(objRead.GetValue(0), m_iClientId);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.GetScreenIDFromWCPBenefit.Error", m_iClientId), p_objEx);
			}
			finally
			{
				sQuery=null;
				objRead=null;
			}
			return lValue;
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			DbReader objRead=null;
			XmlNode objNode=null;
			XmlElement objElement=null;
			XmlElement objRowTxt=null;
			XmlElement objLstRow=null;
			StringBuilder sbSql=null;
			string sTransType="";
			string sMastReserType="";
			string sStatePostalCode="";
			string sTemp="";
			string sShortCode="";
			string sShortDesc="";
			string sValue1="";
			DbConnection objConn=null;
			LocalCache objCache=null;
			try
			{
				XMLDoc = p_objXmlDocument;
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sTransType=objCache.GetTableId("TRANS_TYPES").ToString();
				sMastReserType=objCache.GetTableId("MASTER_RESERVE").ToString();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='WCJurisdictions']");  	
				sTemp=objNode.InnerText;
				objNode.InnerText="";
				sStatePostalCode=objCache.GetStateCode(Conversion.ConvertStrToInteger(sTemp));
				objCache.Dispose();
				Get();

				sbSql=new StringBuilder();
				sbSql.Append(" SELECT STATE_ROW_ID, STATE_ID, STATE_NAME FROM STATES WHERE STATE_ROW_ID > 0 ORDER BY STATE_ID ASC ");

				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=p_objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("STATE_ROW_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("STATE_ID")+"-"+Conversion.ConvertObjToStr(objRead.GetValue("STATE_NAME")));
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","-1");
				objElement.InnerText="Select Jurisdiction";
				if (objNode.FirstChild!=null)
				objNode=objNode.InsertBefore(objElement,objNode.FirstChild);
				if ((!sTemp.Trim().Equals("")))
				{
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='WCJurisdictions']");  
					objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+sTemp+"']");
					objElement.SetAttribute("selected","1");
					string sWC=objNode.SelectSingleNode("./option[@value='"+sTemp+"']").InnerText;
					sWC=sWC.Split("-".ToCharArray())[0];
					objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='WCJurisdictions']"); 
					objElement.SetAttribute("WCNAME",sWC);
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT BENEFIT_LKUP_ID,JURIS_BENEFIT_DESC FROM WCP_BENEFIT_LKUP, STATES WHERE STATES.STATE_ROW_ID = WCP_BENEFIT_LKUP.JURIS_ROW_ID AND STATES.STATE_ID = '"+sStatePostalCode+"'");
					sbSql.Append(" ORDER BY JURIS_BENEFIT_DESC");
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='WCBenefits']");  	
					objElement=p_objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value","");
					objElement.InnerText="";
					objNode.AppendChild(objElement);
                    using (objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString()))
                    {
                        while (objRead.Read())
                        {
                            objElement = p_objXmlDocument.CreateElement("option");
                            objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue(0)));
                            objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue(1));
                            objNode.AppendChild(objElement);
                        }
                    }//using
					
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='WCTransactionType']");  	
					objElement=p_objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value","");
					objElement.InnerText="";
					objNode.AppendChild(objElement);
					sbSql=new StringBuilder();
					
					if (DbFactory.IsOracleDatabase(base.ConnectString))
					{
                        sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.SHORT_CODE|| ' ' || CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
					}
					else
					{
                        sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC, CODES.SHORT_CODE+' '+CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
					}
					sbSql.Append(" WHERE CODES.TABLE_ID ="+sTransType);
					sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID");
					sbSql.Append(" AND CODES.RELATED_CODE_ID IN");
					sbSql.Append(" (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES");
					sbSql.Append(" WHERE RESERVE_TYPE_CODE = CODE_ID");
					sbSql.Append(" AND RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID ="+sMastReserType+")");
					sbSql.Append(" AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 243)");
					sbSql.Append(" AND CODES.DELETED_FLAG = 0");
                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
					sbSql.Append(" ORDER BY CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                    sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "TRANS_TYPES", this.LanguageCode);
					using (objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString()))
                    {
					    while (objRead.Read())
					    {
						    objElement=p_objXmlDocument.CreateElement("option");
						    objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue(0)));
						    objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue(3));
						    objNode.AppendChild(objElement);
					    }
                    }//using
					
					objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='WCBenefitsList']");  
				
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT WCP_BENEFIT_LKUP.JURIS_BENEFIT_DESC, WCP_BENEFIT_LKUP.BENEFIT_LKUP_ID,WCP_TRANS_TYPES.CODE_ID, ");
					sbSql.Append(" WCP_BENEFIT_LKUP.USE_IN_CALCULATOR,WCP_TRANS_TYPES.ROW_ID FROM WCP_TRANS_TYPES, STATES, WCP_BENEFIT_LKUP WHERE STATES.STATE_ROW_ID = WCP_TRANS_TYPES.STATE_ROW_ID ");
					sbSql.Append(" AND WCP_TRANS_TYPES.BENEFIT_LKUP_ID = WCP_BENEFIT_LKUP.BENEFIT_LKUP_ID AND STATES.STATE_ID = '"+sStatePostalCode+"'");
                    objCache = new LocalCache(base.ConnectString, m_iClientId);
					objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				
					objLstRow = p_objXmlDocument.CreateElement("listhead");

					objRowTxt = p_objXmlDocument.CreateElement("rowhead");
					objRowTxt.SetAttribute("colname" , "WCBenefitType");
					objRowTxt.InnerText="Benefit Type";
					objLstRow.AppendChild(objRowTxt);
					objRowTxt = p_objXmlDocument.CreateElement("rowhead");
					objRowTxt.SetAttribute("colname" , "WCTransactionType");
					objRowTxt.InnerText="Transaction Type";
					objLstRow.AppendChild(objRowTxt);
					objRowTxt = p_objXmlDocument.CreateElement("rowhead");
					objRowTxt.SetAttribute("colname" , "UseCalc");
					objRowTxt.InnerText="Use in Calculator";
					objLstRow.AppendChild(objRowTxt);
					objElement.AppendChild(objLstRow);
					while (objRead.Read())
					{
						objLstRow = p_objXmlDocument.CreateElement("listrow");
						
						objRowTxt = p_objXmlDocument.CreateElement("rowtext");
						objRowTxt.SetAttribute("type" , "label");
						objRowTxt.SetAttribute("name" , "WCBenefitType");
						objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue(0)));
                        objLstRow.AppendChild(objRowTxt);
						sValue1=sValue1+Conversion.ConvertObjToStr(objRead.GetValue(1))+"|";

						sShortCode="";
						sShortDesc="";
						objCache.GetCodeInfo(Conversion.ConvertObjToInt(objRead.GetValue(2), m_iClientId),ref sShortCode,ref sShortDesc,this.LanguageCode); //Aman ML Change

						objRowTxt = p_objXmlDocument.CreateElement("rowtext");
						objRowTxt.SetAttribute("type" , "label");
						objRowTxt.SetAttribute("name" , "WCTransactionType");
						objRowTxt.SetAttribute("title", sShortCode+" "+sShortDesc);
						objLstRow.AppendChild(objRowTxt);
						sValue1=sValue1+Conversion.ConvertObjToStr(objRead.GetValue(2))+"**";
					
						objRowTxt = p_objXmlDocument.CreateElement("rowtext");
						objRowTxt.SetAttribute("type" , "label");
						objRowTxt.SetAttribute("name" , "UseCalc");
						if (Conversion.ConvertObjToStr(objRead.GetValue(3)).Equals("-1"))
						{
							objRowTxt.SetAttribute("title", "Yes");
						}
						else
						{
							objRowTxt.SetAttribute("title", "No");
						}
						objLstRow.AppendChild(objRowTxt);
						/*objRowTxt = p_objXmlDocument.CreateElement("rowtext");
						objRowTxt.SetAttribute("type" , "label");
						objRowTxt.SetAttribute("name" , "RowId");
						objRowTxt.SetAttribute("title", Conversion.ConvertObjToStr(objRead.GetValue("ROW_ID")));
						objLstRow.AppendChild(objRowTxt);*/
						objLstRow.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("ROW_ID")));
						objElement.AppendChild(objLstRow);
					}
					objElement.SetAttribute("value",sValue1);
					objCache.Dispose();
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objRead=null;
				objNode=null;
				objElement=null;
				objRowTxt=null;
				objLstRow=null;
				sbSql=null;
				if (objCache!=null)
				{
					objCache.Dispose();
				}
				if (objConn!=null)
				{
					objConn.Dispose();
				}
				objCache=null;
			}
			return XMLDoc;
		}
		/// Name		: DeleteMapping
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes Mapping 
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool DeleteMapping(XmlDocument p_objXmlDocument)
		{
			ArrayList arrlstSql=null;
			string sSQL="";
			bool bReturnValue=false;
			string sRecord="";
			XmlNode objNode=null;
			string[] arrValues=null;
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//DeleteMapping"); 
				if (objNode.InnerText!=null)
				{
					arrValues=objNode.InnerText.Split(" ".ToCharArray());
				}
				arrlstSql=new ArrayList();
				if (arrValues!=null)
				{
					for (int i=0;i<arrValues.Length;i++)
					{
						if (!arrValues[i].Trim().Equals(""))
						{
							sSQL="DELETE FROM WCP_TRANS_TYPES WHERE ROW_ID="+arrValues[i];
							arrlstSql.Add(sSQL);
						}
					}
				}
                UTILITY.Save(arrlstSql, base.ConnectString, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.DeleteMapping.Error", m_iClientId), p_objEx);
			}
			finally
			{
				arrlstSql=null;
				objNode=null;
				arrValues=null;
			}
			return bReturnValue;
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves IAIABC Funds Transaction Mapping
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			XMLDoc = p_objXmlDocument;
			ArrayList objQuery=null;
			XmlNode objNode=null;
         	string sQuery="";
			string sDate="";
			DbReader objRead=null;
			string sInsertQuery="";
			string sStateRowid="";
			string sBenefitLinkId="";
			string sUtilId="";
			string sCodeid="";
			string sScreenid="";
			int iNextId=0;
			bool bReturnValue=false;
			string sUser=m_sUserName;
			try
			{
				objQuery=new ArrayList();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='WCBenefits']");  
				if (objNode!=null)
				{
					sBenefitLinkId=GetValue(p_objXmlDocument,"WCBenefits");
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='WCJurisdictions']");
				if (objNode!=null)
				{
					sStateRowid=GetValue(p_objXmlDocument,"WCJurisdictions");
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='WCTransactionType']");  
				if (objNode!=null)
				{
					sCodeid=GetValue(p_objXmlDocument,"WCTransactionType");
				}
				
				if (sCodeid!="" && sStateRowid!="")
				{
					sScreenid=Conversion.ConvertObjToStr(GetScreenIDFromWCPBenefit(0,Conversion.ConvertStrToLong(sStateRowid)));
					sUtilId=Conversion.ConvertObjToStr(GetScreenIDFromWCPBenefit(0,Conversion.ConvertStrToLong(sStateRowid)));
					sQuery="SELECT ROW_ID FROM WCP_TRANS_TYPES WHERE CODE_ID='"+sCodeid+"' AND BENEFIT_LKUP_ID='"+sBenefitLinkId+"'";
					objRead=DbFactory.GetDbReader(base.ConnectString,sQuery);
					if (objRead.Read())
					{
						objRead.Close();
                        throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.Save.ErrorTransactionTypeBenefits", m_iClientId)); 
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
                    iNextId = Utilities.GetNextUID(base.ConnectString, "WCP_TRANS_TYPES", m_iClientId);
					sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
					sInsertQuery="INSERT INTO WCP_TRANS_TYPES (ROW_ID,STATE_ROW_ID,BENEFIT_LKUP_ID,UTIL32_CBO_ID,SCREEN_ID,CODE_ID,DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER) VALUES ("+iNextId.ToString()
						+",'"+sStateRowid+"','"+sBenefitLinkId+"','"+sUtilId+"','"+sScreenid+"','"+sCodeid+"',"+sDate+",'"+sUser+"',"+sDate+",'"+sUser+"')";
					objQuery.Add(sInsertQuery);
				}
				base.Save(objQuery);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkersCompTransMapping.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objQuery=null;
				objRead=null;
			}
			return bReturnValue;
		}
		#endregion
	}
}
