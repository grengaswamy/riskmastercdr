﻿
using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Early Pay Discount information.
	/// </summary>
	public class EarlyPayDiscounts:UtilitiesUIBase
	{
        //vsharma205  RMA-11795 starts
         UserLogin m_oUserLogin = null;
        //vsharma205  RMA-11795 ends
		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public EarlyPayDiscounts (string p_sConnString, int p_iClientId)
            : base( p_sConnString, p_iClientId)
		{
			try
			{
              
				ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.Constructor.Error", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public EarlyPayDiscounts(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base( p_sConnectionString, p_iClientId) 
		{
			try
			{
               
				m_sUserName = p_sLoginName;
                m_iClientId = p_iClientId;
				ConnectString = p_sConnectionString;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.Constructor.Error", m_iClientId), p_objEx);
			}
		}
		#endregion

        //vsharma205  RMA-11795 starts 
          /*Desc: Overloaded constrcutor , as "mUserlogin.objUser.NlsCode.ToString()"  in UtilitiesUIBase.cs was null
          Conversion.GetUIDate(sValue,mUserlogin.objUser.NlsCode.ToString(), m_iClientId);*/
        public EarlyPayDiscounts(UserLogin p_oUserLogin, string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_oUserLogin, p_sConnectionString, p_iClientId)
        {
            try
            {
                m_oUserLogin = p_oUserLogin;
                m_sUserName = p_sLoginName;
                m_iClientId = p_iClientId;
                ConnectString = p_sConnectionString;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.Constructor.Error", m_iClientId), p_objEx);
            }
        }
        //vsharma205  RMA-11795 ends

		#region Private Variables
		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields=
			{
			{"RowId","EARLY_DISCOUNT_ROWID"},
			{"UseEarlyPayDiscount", "IN_USE_FLAG"},
			{"Amount", "AMOUNT"},
			{"DollarOrPercent", "DISCOUNT_TYPE"},
			{"LOB", "LINE_OF_BUSINESS"},
			{"DeadlineEligibilDate", "DEADLINE_DATE"},
			{"AddedByUser", "ADDED_BY_USER"},
			{"DttmRcdAdded", "DTTM_RCD_ADDED"},
			{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
			{"UpdatedByUser", "UPDATED_BY_USER"}
			};
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
		/// <summary>
		/// Xml document object
		/// </summary>
		private XmlDocument m_objXMLDocument = null;

        private int m_iClientId = 0;
		#endregion

		#region Private Functions
		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			try
			{
				TableName = "SYS_ERLY_PAY_DSCNT";
				KeyField= "EARLY_DISCOUNT_ROWID";
				InitFields(arrFields);
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.Initialize.Error", m_iClientId), p_objEx);
			}
		}
		private void FillXML()
		{
			string sBillingRuleRowid = "";
			XmlNodeList objNodeList = null;
			string sNodename = string.Empty;
			XmlElement objNode=null;
			try
			{
				objNode=((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']"));
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sBillingRuleRowid = objNode.InnerText;
					}
				}

				sBillingRuleRowid=sBillingRuleRowid.Trim();
				objNodeList=m_objXMLDocument.SelectNodes("//control");
				foreach(XmlElement objXMLElement in objNodeList)
				{
					sNodename = objXMLElement.GetAttribute("name");  
					switch(sNodename)
					{
						case "AddedByUser":
							if(sBillingRuleRowid == "" || sBillingRuleRowid == "-1")
								objXMLElement.InnerText = m_sUserName; 
							break;
						case "DttmRcdAdded":
							if(sBillingRuleRowid == "" || sBillingRuleRowid == "-1")
								objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToLongDateString()); 
							break;
						case "DttmRcdLastUpd":
							objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToLongDateString());
							break;
						case "UpdatedByUser":
							objXMLElement.InnerText = m_sUserName; 
							break;
						case "DeadlineEligibilDate":
							objXMLElement.InnerText = Conversion.GetDate(objXMLElement.InnerText); 
							break;
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.FillXML.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNodeList = null;
				objNode=null;
			}
		}
		#endregion

		/// <summary>
		/// Key field value
		/// </summary>
		new public string KeyFieldValue
		{
			get
			{
				return Conversion.ConvertObjToStr(base.KeyFieldValue);
			}
		}
		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
            //XmlNode objNode = null;
            //string sValue = "";
            //LocalCache objCache = null;
            //string sShortCode = "";
            //string sShortDesc = "";
			try
			{
				XMLDoc = p_objXmlDocument;
				Get();
                // //Commented-No need to set the attribute value for Code Fields.This causes problem in setting Ref attribute for these fields.
                //objCache = new LocalCache(base.ConnectString);
				
                //objNode = p_objXmlDocument.SelectSingleNode("//control[@name='LOB']");			
                //if (objNode.InnerText !=null)
                //{
                //    sValue=	objNode.InnerText;	
                //    ((XmlElement)objNode).SetAttribute("value",sValue);
                //    objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
                //    objNode.InnerText=sShortCode+" "+sShortDesc;
                //}

                //objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DollarOrPercent']");			
                //if (objNode.InnerText !=null)
                //{
                //    sValue=	objNode.InnerText;						
                //    ((XmlElement)objNode).SetAttribute("value",sValue);				
                //    objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
                //    objNode.InnerText=sShortCode+" "+sShortDesc;
                //}
               // //Commented-No need to set the attribute value for Code Fields.This causes problem in setting Ref attribute for these fields.
				return XMLDoc;
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
                //objNode=null;
                //if (objCache!=null)
                //    objCache.Dispose();
				
			}
			
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data to the database
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
			string sLob="";
			string sRowid="";
            string sUseEPD = "";
			DbReader objDBReader=null;
			string sSQL="";

			try
			{
				XMLDoc=p_objXmlDocument;
				m_objXMLDocument=p_objXmlDocument;
				FillXML();
				sLob = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='LOB']")).GetAttribute("codeid");
				sRowid = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;
				//Check if there is already Billing Rule for the same Billing Code, log error
				sSQL = "SELECT EARLY_DISCOUNT_ROWID FROM " +TableName +
					" WHERE IN_USE_FLAG = 1 and LINE_OF_BUSINESS = "+sLob;

				objDBReader = DbFactory.GetDbReader(base.ConnectString,sSQL);
				if(objDBReader.Read())
				{
					if( Conversion.ConvertObjToInt(objDBReader["EARLY_DISCOUNT_ROWID"], m_iClientId) != Conversion.ConvertStrToInteger(sRowid))
                        throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.ValidateXMLData.AlreadyExists", m_iClientId));
				}
				objDBReader.Close();
                sUseEPD = m_objXMLDocument.SelectSingleNode("//control[@name='UseEarlyPayDiscount']").InnerText;
                if (sUseEPD == "")
                    m_objXMLDocument.SelectSingleNode("//control[@name='UseEarlyPayDiscount']").InnerText = "False";
				base.Save();
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objDBReader!=null)
					objDBReader.Dispose();
			}
			
		} 
		/// Name		: Delete
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes the Early Pay Discount information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Delete(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
			try
			{
				XMLDoc=p_objXmlDocument;
				base.Delete();
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("EarlyPayDiscounts.Delete.Error", m_iClientId), p_objEx);
			}
		}
		#endregion

		

	}
}
