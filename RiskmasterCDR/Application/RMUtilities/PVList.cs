using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   18 May 2005
	///Purpose :   This class is responsible for showing the View listing
	/// </summary>
	public class PVList
	{
		#region Private variables
        private string m_netViewFormsConnString = "";
        private string s_DataSourceId = "";
		private string m_sConnectionString="";
        private int m_iClientId = 0; //mbahl3 JIRA [RMACLOUD-123]
		#endregion

		#region Properties
		public string ConnectionString
		{
			set
			{
				m_sConnectionString=value;
			}	

		}

        //mbahl3 JIRA [RMACLOUD-123]
        public int ClientId
        {
            get
            {
                return m_iClientId;
            }
            set
            {
                m_iClientId = value;
            }
        }
        //mbahl3 JIRA [RMACLOUD-123]
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sConnectionString">Connection string</param>
        public PVList(string p_sConnectionString, string p_sDSNId, int p_iClientId) //mbahl3 JIRA [RMACLOUD-123]
		{
			m_sConnectionString = p_sConnectionString;
            m_iClientId = p_iClientId; //mbahl3 JIRA [RMACLOUD-123]
            m_netViewFormsConnString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
            s_DataSourceId = p_sDSNId;
            //m_netViewFormsConnString = "Driver={SQL Native Client};Server=20.198.58.52;Database=RMX_PAGE;UID=sa;PWD=rmserver_123;";
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/18/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get(XmlDocument p_objXMLDocument)
		{
			DbReader objReader=null;
			StringBuilder sbSql=null;
			XmlElement objSelectElement=null;
			XmlElement objRowTxt=null;
			XmlElement objLstRow = null;

			try
			{
				sbSql=new StringBuilder();
				sbSql.Append("SELECT * FROM NET_VIEWS WHERE DATA_SOURCE_ID='"+ s_DataSourceId +"'");
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());

				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='ViewList']"); 
				objLstRow = p_objXMLDocument.CreateElement("listrow");
				
				while (objReader.Read())
				{	
					objRowTxt = p_objXMLDocument.CreateElement("rowtext");
					objRowTxt.SetAttribute("type" , "radio");
					objRowTxt.SetAttribute("value",Conversion.ConvertObjToStr(objReader.GetValue("VIEW_ID")));
					objRowTxt.SetAttribute("title",Conversion.ConvertObjToStr(objReader.GetValue("VIEW_NAME")));
					objRowTxt.SetAttribute("desc",Conversion.ConvertObjToStr(objReader.GetValue("VIEW_DESC")));
					objRowTxt.SetAttribute("name","radiolist");
					objLstRow.AppendChild(objRowTxt);
				}
				objSelectElement.AppendChild((XmlNode)objLstRow); 

				if (!objReader.IsClosed)
				{
					objReader.Close();
				}
				return p_objXMLDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PVList.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{				
                if (objReader != null)
                {
                    if (!objReader.IsClosed)
                    {
                        objReader.Close();
                    }
                    objReader.Dispose();
                }
				p_objXMLDocument=null;
				sbSql=null;
				objSelectElement=null;
				objRowTxt=null;
				objLstRow = null;
			}
		}
		#endregion
	}
}
