using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.AdminTracking;
using Riskmaster.Settings; //Rkatyal4: 7767

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   19 May 2005
	///Purpose :   This class is responsible for Creating, saving the View Definations
	/// </summary>
	public class PVDefination
	{
		#region "Constants"

        //Raman 07/20/2009: removing dependency on Riskmaster.Config

        private static string PV_XMLFILEPATH = UTILITY.GetAppFilesDirectory("PV");
        private static string PV_FORMLISTFILE = "";//rkaur27
        
		
        private const string C_INDENT = "___";

		#endregion

		#region Private variables
        private string m_netViewFormsConnString = "";
        private string s_DataSourceId = "";
		private string m_sConnString = "";
		private string m_sUserId="";
		private string m_sPassword="";
		private string m_sDSNName="";
		private string m_sDSNId="";
		private string m_sSecurityDSN="";
        private int m_iClientId = 0; //mbahl3 JIRA [RMACLOUD-123]
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserId">User ID</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDSNName">DSN Name</param>
		/// <param name="p_sDSNId">DSN Id</param>
		/// <param name="p_sConnString">Connection String</param>
        public PVDefination(string p_sUserId, string p_sPassword, string p_sDSNName, string p_sDSNId, string p_sConnString, int p_iClientId) //mbahl3 JIRA [RMACLOUD-123]
		{
			m_sDSNName = p_sDSNName;
			m_sUserId = p_sUserId;
			m_sPassword = p_sPassword;
			m_sConnString = p_sConnString;
			m_sDSNId = p_sDSNId;
            s_DataSourceId = p_sDSNId;
            m_iClientId = p_iClientId;//mbahl3  JIRA [RMACLOUD-123]
            m_sSecurityDSN = SecurityDatabase.GetSecurityDsn(m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
            m_netViewFormsConnString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId); //mbahl3  JIRA [RMACLOUD-123]
            PV_FORMLISTFILE = UTILITY.GetConfigUtilitySettings(m_sConnString, m_iClientId)["PV_FormListFile"];//rkaur27
		}
		#endregion

		/// Name		: Get
		/// Author		: Anurag Agarwal
		/// Date Created: 05/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <param name="p_objXMLDocument">Blank XML structure for the View UI</param>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
		public XmlDocument Get(XmlDocument p_objXMLDocument)
		{
			StringBuilder sbSql=null;
			DbReader objReader=null;
			XmlElement objSelectElement=null;
			XmlElement objRowTxt=null;
			XmlNodeList objNodeList = null;
			XmlDocument objXMLDocFormList = null;
			int iViewId = 0;
			string sViewName = "";
			Hashtable objHashForms = null;
			Hashtable objHashUsers = null;
            Riskmaster.Settings.SysSettings objSysSettings = null; //Rkatyal4: 7767
            objSysSettings = new Settings.SysSettings(m_sConnString, m_iClientId);//Rkatyal: 7767
			try
			{
				sbSql = new StringBuilder();
				objHashForms = new Hashtable();
				objHashUsers = new Hashtable();

				iViewId = Conversion.ConvertStrToInteger(p_objXMLDocument.SelectSingleNode("//PVList/RowId").InnerText);

				if(iViewId == 0)
					throw new RMAppException(Globalization.GetString("PVDefination.ViewNotFound.Error", m_iClientId)); //mbahl3 JIRA [RMACLOUD-123]

                sbSql.Append(string.Concat("SELECT VIEW_NAME FROM NET_VIEWS WHERE VIEW_ID=" 
                    , iViewId
                    , " AND DATA_SOURCE_ID = '" 
                    , s_DataSourceId 
                    , "'"));
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
				if(objReader != null)
				{
					if(objReader.Read())
						sViewName = objReader["VIEW_NAME"].ToString();
					if(objReader != null)
						objReader.Dispose(); 
				}
				((XmlElement)p_objXMLDocument.SelectSingleNode("//ViewName")).InnerText = "View definition for: " + sViewName;

				//Get form list
				objXMLDocFormList = GetFormList();
                //rkatyal4: 7767 start
                if (objSysSettings.UseEntityRole)
                {
                    XmlNode RemNode = objXMLDocFormList.SelectSingleNode("forms/form[@file='people.xml']");
                    RemNode.ParentNode.RemoveChild(RemNode);

                }
                //rkatyal4: 7767 end

				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//AvailForms"); 
				foreach(XmlElement objADmTEle in objXMLDocFormList.SelectSingleNode("//forms").ChildNodes)
				{	
					if(objADmTEle.Name=="groupstart")
					{
                        objRowTxt = p_objXMLDocument.CreateElement("AvailFormsoption");
						objRowTxt.SetAttribute("value","1");  
						//objRowTxt.SetAttribute("label","1");  
						objRowTxt.InnerText = objADmTEle.GetAttribute("name");
					}
					else if(objADmTEle.Name=="form")
					{
                        objRowTxt = p_objXMLDocument.CreateElement("AvailFormsoption");
						objRowTxt.SetAttribute("value",objADmTEle.GetAttribute("file"));
						objRowTxt.InnerText = (objADmTEle.GetAttribute("toplevel") == "1" ? "*" : C_INDENT) + objADmTEle.GetAttribute("name");
						objHashForms.Add(objADmTEle.GetAttribute("file"),objRowTxt.InnerText);
					}
					objSelectElement.AppendChild((XmlNode)objRowTxt);					
				}

				//Write Users/Groups
				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//AvialUser"); 
                //sbSql.Remove(0,sbSql.Length);
                sbSql.Length = 0;
				sbSql.Append("SELECT * FROM USER_GROUPS ORDER BY GROUP_NAME");
				objReader = DbFactory.GetDbReader(m_sConnString,sbSql.ToString());
				if(objReader != null)
				{
					while(objReader.Read())
					{
                        objRowTxt = p_objXMLDocument.CreateElement("AvialUseroption");
						objRowTxt.SetAttribute("value","g" + objReader["GROUP_ID"].ToString());
						objRowTxt.InnerText = "*" + objReader["GROUP_NAME"].ToString();
						objSelectElement.AppendChild((XmlNode)objRowTxt);
						objHashUsers.Add("g" + objReader["GROUP_ID"].ToString(),objReader["GROUP_NAME"].ToString()); 
					}
					if(objReader != null)
						objReader.Dispose(); 
				}
				
				//Open Security DB
                //sbSql.Remove(0,sbSql.Length);
                sbSql.Length = 0;
				sbSql.Append("SELECT USER_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,");
				sbSql.Append("USER_DETAILS_TABLE.LOGIN_NAME FROM USER_TABLE,USER_DETAILS_TABLE ");
				sbSql.Append(" WHERE USER_TABLE.USER_ID=USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.DSNID=");
				sbSql.Append(m_sDSNId + " ORDER BY USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME");
				objReader = DbFactory.GetDbReader(m_sSecurityDSN,sbSql.ToString());
				if(objReader != null)
				{
					while(objReader.Read())
					{
                        objRowTxt = p_objXMLDocument.CreateElement("AvialUseroption");
						objRowTxt.SetAttribute("value", "u" + objReader["USER_ID"].ToString());
						objRowTxt.InnerText = ((string)(objReader["FIRST_NAME"].ToString()
                            + " " 
                            + objReader["LAST_NAME"].ToString())).Trim() 
                            + " (" 
                            + objReader["LOGIN_NAME"].ToString() 
                            + ")";
						objSelectElement.AppendChild((XmlNode)objRowTxt);						
						objHashUsers.Add("u" + objReader["USER_ID"].ToString(),objRowTxt.InnerText); 
					}
					if(objReader != null)
						objReader.Dispose(); 
				}
				
				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//FormList"); 
                //sbSql.Remove(0,sbSql.Length);
                sbSql.Length = 0;
                sbSql.Append(string.Concat("SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID=" 
                    , iViewId 
                    , " AND DATA_SOURCE_ID = '" 
                    , s_DataSourceId 
                    , "'"));
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
				if(objReader != null)
				{
					while(objReader.Read())
					{
						//Add it to the list only when its a power view form. Not all forms can be edited in PV
						//or when its an Admin Tracking form
						if(PVCreate.IsPvForm(objReader["FORM_NAME"].ToString(),m_sConnString, m_iClientId)||objReader["FORM_NAME"].ToString().IndexOf("admintracking|")>=0)
						{
                            objRowTxt = p_objXMLDocument.CreateElement("FormListoption");
							objRowTxt.SetAttribute("value",objReader["FORM_NAME"].ToString().Replace("|","%"));
                            if (objHashForms.ContainsKey(objReader["FORM_NAME"].ToString().Replace("|", "%")))
                                objRowTxt.InnerText = objHashForms[objReader["FORM_NAME"].ToString().Replace("|", "%")].ToString();
                            else
                                //objRowTxt.InnerText = ""; MITS 25108
                                continue;
							objSelectElement.AppendChild((XmlNode)objRowTxt);
						}
					}
					if(objReader != null)
						objReader.Dispose(); 
				}

				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//UserList"); 
                //sbSql.Remove(0,sbSql.Length);
                sbSql.Length = 0;
                sbSql.Append(string.Concat("SELECT * FROM NET_VIEWS_MEMBERS WHERE NET_VIEWS_MEMBERS.VIEW_ID=" 
                    , iViewId 
                    , " AND DATA_SOURCE_ID = '" 
                    , s_DataSourceId 
                    , "'"));
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
				if(objReader != null)
				{
					while(objReader.Read())
					{
                        objRowTxt = p_objXMLDocument.CreateElement("UserListoption");
				
						if(objReader["ISGROUP"].ToString().Trim() != "0")
						{
							objRowTxt.SetAttribute("value","g" + objReader["MEMBER_ID"].ToString());
							if(objHashUsers.ContainsKey("g" + objReader["MEMBER_ID"].ToString()))
								objRowTxt.InnerText = objHashUsers["g" + objReader["MEMBER_ID"].ToString()].ToString();
							else
								objRowTxt.InnerText = "";
						}
						else
						{
							objRowTxt.SetAttribute("value","u" + objReader["MEMBER_ID"].ToString());
							if(objHashUsers.ContainsKey("u" + objReader["MEMBER_ID"].ToString()))
								objRowTxt.InnerText = objHashUsers["u" + objReader["MEMBER_ID"].ToString()].ToString();
							else
								objRowTxt.InnerText = "";
						}
                        if (objRowTxt.InnerText != string.Empty && objRowTxt.GetAttribute("value") != string.Empty)
                        {
                            objSelectElement.AppendChild((XmlNode)objRowTxt);
                        }
					}
					
				}
				return p_objXMLDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVDefinition.Get.Error", m_iClientId),p_objEx); //mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{
				if(objReader != null)
				{
				  objReader.Dispose();					
				}
				p_objXMLDocument=null;
				objSelectElement=null;
				objRowTxt=null;
				sbSql=null;
				objNodeList = null;
				objXMLDocFormList = null;
				if(objHashForms != null)
				{
					objHashForms.Clear();
					objHashForms = null;
				}
				if(objHashUsers != null)
				{
					objHashUsers.Clear();
					objHashUsers = null;
				}
			}
		}

		/// Name		: Save
		/// Author		: Anurag Agarwal
		/// Date Created: 05/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the view Defination into the database
		/// </summary>
		/// <returns>boolean for success/failure</returns>
		public bool Save(XmlDocument p_objXMLDocument)
		{
			StringBuilder sbSql=null;
			DbReader objReader=null;
			DbConnection objDbConnection = null;
			XmlElement objXMLFormEle = null;
			XmlDocument objXMLFormDoc = null;
			XmlDocument objXMLDocFormList = null;
			string sSelectedForms = "";
			string sSelectedFormCaptions = "";
			string sSelectedUsers = "";
			string sTemp = "";
			int iViewId = 0;
			string [] arrForms;
			string [] arrCaptions;
			string [] arrUsers;
			string sExt = "";
			string sViewXML = "";
            //Added by Shivendu for R5
            string sAspxPageContent = "";
            StringBuilder sViewAspx = new StringBuilder("");
			DbCommand objCmd=null;
			DbParameter objParam=null;
            DbParameter objParamAspx = null;
			ADMForms objADMForms = null;
			DataModelFactory objDmf = null;

            //rsolanki2: extensibility updates 
            string sExtendedFormsList = string.Empty;

			try
			{
				sbSql = new StringBuilder();
                objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
				if(objDbConnection != null)
					objDbConnection.Open();

				iViewId = Conversion.ConvertStrToInteger(p_objXMLDocument.SelectSingleNode("//RowId").InnerText);

				if(iViewId == 0)
					throw new RMAppException(Globalization.GetString("PVDefination.ViewNotFound.Error", m_iClientId)); //mbahl3 JIRA [RMACLOUD-123]

                //rsolanki2: extensibility updates - start 
                //reading the custom view_name into a string
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString,
                    string.Concat("SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 and DATA_SOURCE_ID='"
                        , s_DataSourceId
                        , "'"));

                while (objReader.Read())
                {
                    sbSql.Append(Conversion.ConvertObjToStr(objReader.GetValue("FORM_NAME")));
                    sbSql.Append("|");
                }
                sExtendedFormsList = "|" + sbSql.ToString();
                objReader.Dispose();
                sbSql.Length = 0;
                //rsolanki2:  extensibility updates  - end

				//all members are re-inserted below
                sbSql.Append(string.Concat("DELETE FROM NET_VIEWS_MEMBERS WHERE VIEW_ID="
                    , iViewId 
                    , " AND DATA_SOURCE_ID = '" 
                    , s_DataSourceId 
                    , "'"));
				objDbConnection.ExecuteNonQuery(sbSql.ToString()); 
				
				sSelectedForms = p_objXMLDocument.SelectSingleNode("//SelectedForms").InnerText;
				sSelectedFormCaptions = p_objXMLDocument.SelectSingleNode("//SelectedFormsCaption").InnerText;

				//important: remove the top-level indicator and the indent
				sSelectedFormCaptions = sSelectedFormCaptions.Replace("*","");
				sSelectedFormCaptions = sSelectedFormCaptions.Replace(C_INDENT,"");
			
				sSelectedUsers = p_objXMLDocument.SelectSingleNode("//SelectedUsers").InnerText;

				//delete records in net_view_forms that are not in selectedforms list
                //sbSql.Remove(0,sbSql.Length); 
                sbSql.Length=0;

				if(sSelectedForms.Trim().Length > 0)
				{
					sTemp = " AND FORM_NAME NOT IN ('" + sSelectedForms.Replace("|", "','") ;
					//above leaves an extra ,'' which Oracle does not treat as LIKE condition
					sTemp = sTemp.Replace(",''","");
					sTemp = sTemp.Remove(sTemp.Length -1,1);   
					sSelectedForms = ((string)("|" + sSelectedForms)).Trim(); 
					sSelectedFormCaptions = ((string)("|" + sSelectedFormCaptions)).Trim();
				}
                //Start by Shivendu for MITS 8556
                else
                    sTemp = " AND FORM_NAME NOT IN (";
                //End by Shivendu for MITS 8556
                PVCreate pvc = new PVCreate(m_sUserId, m_sPassword, m_sDSNName, m_netViewFormsConnString, s_DataSourceId, m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
                sbSql.Append(string.Concat("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID=" 
                    , iViewId 
                    , " AND DATA_SOURCE_ID = '" 
                    , s_DataSourceId 
                    , "'"));
                
                if (sTemp != "")
                //Arnab:MITS-11443 - Modified the form_name in case of admin tracking table so that it 
                //does not get deleted if already modified
                {
                    sbSql.Append(sTemp + pvc.DefViewLst() + ")");
                    sbSql.Replace("%", "|");
                }
                //MITS-11443 End

				objDbConnection.ExecuteNonQuery(sbSql.ToString());

                //Update time for views which are not deleted if any view is deleted, done to update cache in case of delete operation
                //sbSql.Remove(0, sbSql.Length);
                sbSql.Length = 0;

                sbSql.Append(string.Concat("UPDATE NET_VIEW_FORMS SET LAST_UPDATED ='" 
                    , System.DateTime.Now.ToString("yyyyMMddHHmmss") 
                    , "' WHERE VIEW_ID=" 
                    , iViewId
                    , " AND DATA_SOURCE_ID = '"
                    , s_DataSourceId 
                    , "' AND FORM_NAME IN ("
                    , pvc.DefViewLst() 
                    , ")"));
                sbSql.Replace("%", "|");
                objDbConnection.ExecuteNonQuery(sbSql.ToString());
                pvc = null;

                //Start by Shivendu for MITS 8556
                if (sSelectedForms.Trim().Length <= 0)
                    sTemp = "";
                //End by Shivendu for MITS 8556
				//remove items from selectedforms list that are already in net_view_forms
                //sbSql.Remove(0,sbSql.Length);
                sbSql.Length = 0;
                sbSql.Append(string.Concat("SELECT FORM_NAME, CAPTION FROM NET_VIEW_FORMS WHERE VIEW_ID =" 
                    , iViewId 
                    , " AND DATA_SOURCE_ID = '" 
                    , s_DataSourceId 
                    , "'"));
				sTemp = sTemp.Replace("NOT",""); 
				if(sTemp!="")
					sbSql.Append(sTemp.Remove(sTemp.Length -1,1)+ ")");
                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
				if(objReader != null)
				{
					while(objReader.Read())
					{
						sSelectedForms = sSelectedForms.Replace("|" 
                            + objReader["FORM_NAME"].ToString() 
                            + "|","||");
						sSelectedFormCaptions = sSelectedFormCaptions.Replace("|" 
                            + objReader["CAPTION"].ToString() 
                            + "|","||");
					}
					if(objReader != null)
						objReader.Close(); 
				}
			
				//trim the first pipe
				if(sSelectedForms.Trim() != "")
					sSelectedForms = sSelectedForms.Substring(1);
				if(sSelectedForms.Trim() != "")
					sSelectedFormCaptions = sSelectedFormCaptions.Substring(1);

				arrForms = sSelectedForms.Split('|');  
				arrCaptions = sSelectedFormCaptions.Split('|');

				//Get form list
				objXMLDocFormList = GetFormList();

                //sbSql.Remove(0,sbSql.Length);
                sbSql.Length = 0;
				if(arrForms.Length > 0)
				{
					for(int i = 0; i < arrForms.Length; i++)
					{                        
                        if (arrForms[i].ToString().Trim() != "")
						{
                            bool bFlag = true;
                            //Arnab:MITS-11443 - Added condition to not to insert default view for admin tracking table if it is already modified
                            #region MITS-11443
                            if (arrForms[i].ToString().Replace("%", "|").Substring(0, 5) == "admin")
                            {
                                //sbSql.Remove(0, sbSql.Length);
                                sbSql.Length = 0;
                                sbSql.Append(string.Concat("SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = " 
                                    , iViewId 
                                    , " AND FORM_NAME = '" 
                                    , arrForms[i].ToString().Replace("%", "|") 
                                    , "'" 
                                    , " AND DATA_SOURCE_ID = '" 
                                    , s_DataSourceId 
                                    , "'"));

                                objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
                                if (objReader != null)
                                {
                                    if (objReader.Read())
                                    {
                                        sViewXML = objReader["VIEW_XML"].ToString();
                                        sAspxPageContent = objReader["PAGE_CONTENT"].ToString();
                                        if (sViewXML != "")
                                            bFlag = false;
                                    }
                                    if (objReader != null)
                                        objReader.Close();
                                }
                            }
                            if (bFlag == true)
                            {
                            #endregion
                                //MITS-11443 End

                                //Replace single quote in the caption name with ''' to avoid insert err
                                arrCaptions[i] = arrCaptions[i].ToString().Replace("'", "''");
                                //sbSql.Remove(0, sbSql.Length);
                                sbSql.Length = 0;
                                objXMLFormEle = (XmlElement)objXMLDocFormList.SelectSingleNode("//form[@file='" + arrForms[i].ToString() + "']");
                                if (objXMLFormEle == null)
                                    throw new RMAppException(Globalization.GetString("PVDefination.Save.FormNotFound", m_iClientId) + arrForms[i].ToString()); //mbahl3 JIRA [RMACLOUD-123]
                                sbSql.Append(string.Concat("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID =" 
                                    , iViewId 
                                    , " AND DATA_SOURCE_ID = "
                                    , s_DataSourceId
                                    , " AND FORM_NAME = '" 
                                    , arrForms[i].ToString().Replace("%", "|")
                                    , "'"));

                                objDbConnection.ExecuteNonQuery(sbSql.ToString());

                                //sbSql.Remove(0, sbSql.Length);
                                sbSql.Length = 0;
                                sbSql.Append("INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,DATA_SOURCE_ID) VALUES(");
                                sbSql.Append(string.Concat(iViewId + ",'" 
                                    , arrForms[i].ToString().Replace("%", "|") 
                                    , "'," 
                                    , objXMLFormEle.GetAttribute("toplevel")));
                                sbSql.Append(string.Concat(",'" 
                                    , arrCaptions[i].ToString() 
                                    , "','" 
                                    , s_DataSourceId 
                                    , "')"));
                                objDbConnection.ExecuteNonQuery(sbSql.ToString());
                            } //MITS-11443 Bracket end
						}
					}
				}
				
				arrUsers = sSelectedUsers.Split('|');
                //sbSql.Remove(0,sbSql.Length);
                sbSql.Length = 0;
				if(arrUsers.Length > 0)
				{
					for(int i = 0; i < arrUsers.Length; i++)
					{
						if(arrUsers[i].ToString().Trim() != "")
						{
                            //sbSql.Remove(0,sbSql.Length);
                            sbSql.Length = 0;
							sbSql.Append("INSERT INTO NET_VIEWS_MEMBERS(VIEW_ID, MEMBER_ID, DATA_SOURCE_ID,ISGROUP) VALUES(");
							if(arrUsers[i].ToString().Substring(0,1) == "g")
                                sbSql.Append(string.Concat(iViewId
                                    , ","
                                    , arrUsers[i].ToString().Substring(1) 
                                    , ",'"
                                    , s_DataSourceId
                                    , "'"
                                    , ",1)")); 
							else
                                sbSql.Append(string.Concat(iViewId 
                                    , ","
                                    , arrUsers[i].ToString().Substring(1) 
                                    , ",'" 
                                    , s_DataSourceId
                                    , "'"
                                    , ",0)")); 
							objDbConnection.ExecuteNonQuery(sbSql.ToString());
						}
					}
				}

				//when user adds a form, automatically add the xml to the database
				//sSelectedForms = sSelectedForms.Replace("|","");
				for(int i = 0; i < arrForms.Length; i++)
				{
					if(arrForms[i].ToString().Trim() != "")
					{
						if(arrForms[i].ToString().Trim().Length > 0)
						{
							sExt = arrForms[i].Substring(arrForms[i].Length - 4);
							sSelectedForms = arrForms[i].Substring(0,arrForms[i].Length - 4).Replace("%","|");   
					
                            //sbSql.Remove(0,sbSql.Length);
                            sbSql.Length = 0;

                            //sbSql.Append(string.Concat("SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = " 

                            //rsolanki2: removing the PAGE_CONTENT from th query below as it is unrequired. afterwards We whould update the sql to  
                            // to have the null chcek on the VIEW_XML done a db level itself.
                            sbSql.Append(string.Concat("SELECT VIEW_XML FROM NET_VIEW_FORMS WHERE VIEW_ID = " 
                                , iViewId 
                                , " AND LOWER(FORM_NAME)='"
                                , (sSelectedForms + sExt).ToLower() 
                                , "'" 
                                , " AND DATA_SOURCE_ID = '"
                                , s_DataSourceId 
                                , "'"));
                            objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());   
							if(objReader != null)
							{
                                if (objReader.Read())
                                {
                                    sViewXML = objReader["VIEW_XML"].ToString();                                    
                                    //sAspxPageContent = objReader["PAGE_CONTENT"].ToString();
                                    sAspxPageContent = string.Empty;
                                }

								if(objReader != null)
									objReader.Close(); 
							}

							//first time or reset; fetch the original xml from file system or admin tracking component
							if(sViewXML == null || sViewXML == "")
							{
								//if adm tracking (over)write the xml file to disk (adm tracking tables are dynamic)
								if(sSelectedForms.StartsWith("admintracking|"))
								{
                                    //Commented by Shivendu for R5
                                    //PVFormEdit pv = new PVFormEdit(m_sUserId, m_sPassword, m_sDSNName, m_sDSNId, m_netViewFormsConnString);
                                    //objXMLFormDoc = pv.GetDefaultAdmView(sSelectedForms);
                                    ////Added by Shivendu for R5
                                    //ComponentPowerViewUpgrade.Classes.PowerViewUpgrade pvUpgrade = new ComponentPowerViewUpgrade.Classes.PowerViewUpgrade();
                                    //pvUpgrade.UpgradeXmlToAspx(false, false, objXMLFormDoc, sSelectedForms + sExt, out sViewAspx);
                                    //sAspxPageContent = sViewAspx.ToString();
                                    objXMLFormDoc = new XmlDocument();
                                    //basic standard xml would be in the table with viewid=0.Not to be taken from disk
                                    //sbSql.Remove(0, sbSql.Length);
                                    sbSql.Length = 0;

                                    //TODO-BSB: MODIFY THIS TO SUCK UP FORM EXTENSION DEFINITION IF AVAILABLE...
                                    //int ViewId = UTILITY.IsExtendedScreen(sSelectedForms) ? Constants.Views.EXTENDED_SCREEN_VIEWID : Constants.Views.BASE_VIEWID;
                                    
                                    sbSql.Append(String.Format("SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='"
                                        + (sSelectedForms + sExt).ToLower()
                                        + "'"
                                        + " AND DATA_SOURCE_ID = '"
                                        + s_DataSourceId
                                        + "'", 0));


                                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
                                    if (objReader != null)
                                    {
                                        if (objReader.Read())
                                        {
                                            sViewXML = objReader["VIEW_XML"].ToString();
                                            sAspxPageContent = objReader["PAGE_CONTENT"].ToString();
                                        }
                                        if (objReader != null)
                                            objReader.Close();
                                    }

                                    objXMLFormDoc.LoadXml(sViewXML);
                                    if (objXMLFormDoc.OuterXml == "")
                                        throw new RMAppException(Globalization.GetString("PVDefination.Save.FormXMLNotFound", m_iClientId) + sSelectedForms + ".xml"); //mbahl3 JIRA [RMACLOUD-123]
								}
								else
								{
									objXMLFormDoc = new XmlDocument();
									//basic standard xml would be in the table with viewid=0.Not to be taken from disk
                                    //sbSql.Remove(0,sbSql.Length);
                                    sbSql.Length = 0;
									
                                    //TODO-BSB: MODIFY THIS TO SUCK UP FORM EXTENSION DEFINITION IF AVAILABLE...
                                    //int ViewId = UTILITY.IsExtendedScreen(sSelectedForms) ? Constants.Views.EXTENDED_SCREEN_VIEWID : Constants.Views.BASE_VIEWID;

                                    //rsolanki2: extensibility updates
                                    //extended page list is already added into sExtendedFormsList 
                                    int ViewId = sExtendedFormsList.ToLower().Contains(string.Concat("|", sSelectedForms.ToLower(), ".xml|"))
                                                    ? Constants.Views.EXTENDED_SCREEN_VIEWID
                                                    : Constants.Views.BASE_VIEWID;
                                    sbSql.Append(String.Format("SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = {0} AND LOWER(FORM_NAME)='"
                                        + (sSelectedForms + sExt).ToLower()
                                        + "'"
                                        + " AND DATA_SOURCE_ID = '"
                                        + s_DataSourceId
                                        + "'", ViewId));

                                    //rsolanki2: extensibility updates 
                                    //sbSql.Append(string.Concat(
                                    //      "SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 AND LOWER(FORM_NAME) ='"
                                    //      , (sSelectedForms + sExt).ToLower()
                                    //      , "' AND DATA_SOURCE_ID='"
                                    //      , s_DataSourceId
                                    //      , "' "
                                    //      , "UNION SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE DATA_SOURCE_ID = '"
                                    //      , s_DataSourceId
                                    //      , "' "
                                    //      , "AND VIEW_ID = 0 AND LOWER(FORM_NAME) ='"
                                    //      , (sSelectedForms + sExt).ToLower()
                                    //      , "' AND"
                                    //      , " FORM_NAME NOT IN "
                                    //      , "(SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 AND DATA_SOURCE_ID='"
                                    //      , s_DataSourceId
                                    //      , "')"
                                    //));


                                    objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());   
									if(objReader != null)
									{
                                        if (objReader.Read())
                                        {
                                            sViewXML = objReader["VIEW_XML"].ToString();
                                            sAspxPageContent = objReader["PAGE_CONTENT"].ToString();
                                        }
										if(objReader != null)
											objReader.Close(); 
									}

									objXMLFormDoc.LoadXml(sViewXML);
									if(objXMLFormDoc.OuterXml == "")
										throw new RMAppException(Globalization.GetString("PVDefination.Save.FormXMLNotFound", m_iClientId) //mbahl3  JIRA [RMACLOUD-123]
                                            + sSelectedForms
                                            + ".xml"); 

									UTILITY.AddSuppDefinition(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objXMLFormDoc, m_iClientId);  //mbahl3 JIRA [RMACLOUD-123]

								}
								//Add view id attrib to the form element 
								((XmlElement)objXMLFormDoc.SelectSingleNode("//form")).SetAttribute("viewid",iViewId.ToString()); 

								//replace with character ' with '' to avoid sql err
								sViewXML = objXMLFormDoc.OuterXml; // no need if parameterized    .Replace("'","''");
                               

                                //sbSql.Remove(0,sbSql.Length);  
                                sbSql.Length = 0;
								objCmd=objDbConnection.CreateCommand();
								objParam=objCmd.CreateParameter();
                                objParamAspx = objCmd.CreateParameter();
								objParam.Direction=ParameterDirection.Input;
                                objParamAspx.Direction = ParameterDirection.Input;
								objParam.Value=sViewXML ;
                                objParamAspx.Value = sAspxPageContent;
								objParam.ParameterName = "XML";
                                objParamAspx.ParameterName = "ASPX";
								objParam.SourceColumn="VIEW_XML";
                                objParamAspx.SourceColumn = "PAGE_CONTENT";
								objCmd.Parameters.Add(objParam);
                                objCmd.Parameters.Add(objParamAspx);

                                sbSql.Append(string.Concat("UPDATE NET_VIEW_FORMS SET VIEW_XML=~XML~,PAGE_CONTENT=~ASPX~,PAGE_NAME = '"
                                    , sSelectedForms.Replace("|", "")
                                    , ".aspx" 
                                    , "',LAST_UPDATED = '"
                                    , System.DateTime.Now.ToString("yyyyMMddHHmmss") 
                                    , "' WHERE VIEW_ID=" 
                                    , iViewId 
                                    , " AND LOWER(FORM_NAME) = '" 
                                    , (sSelectedForms + sExt).ToLower() 
                                    , "'" 
                                    , " AND DATA_SOURCE_ID = '" 
                                    , s_DataSourceId 
                                    , "'"));
								objCmd.CommandText=sbSql.ToString();
								objCmd.ExecuteNonQuery();
                                //Deb ML Changes
                                UpdatePVByLanguage(sSelectedForms, iViewId, sExt, false);
                                //Deb ML Changes
							}
						}
					}
				}

				return true;
			}		
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVDefination.Save.Error",m_iClientId),p_objEx); //mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{
				sbSql=null;
                if(objReader != null)
				{
				    objReader.Dispose();					
				}
                if (objDbConnection != null)
                {
                   objDbConnection.Dispose();
                }
				objXMLFormEle = null;
				objXMLFormDoc = null;
				objXMLDocFormList = null;
				if(objADMForms != null)
					objADMForms.Dispose();
				//objConfig = null;
                objCmd = null;
                objParam = null;
                if(objDmf != null)
                    objDmf.Dispose();
			}
		}
        //Deb ML Changes
        /// <summary>
        /// UpdatePVByLanguage
        /// </summary>
        /// <param name="sSelectedForms"></param>
        /// <param name="iViewId"></param>
        /// <param name="sExt"></param>
        public void UpdatePVByLanguage(string sSelectedForms, int iViewId, string sExt,bool bCloneBaseView)
        {
            XmlDocument objXml= null;
            XmlDocument p_objXmlOut = null;
            PVFormEdit objPVFormEdit = null;
            XmlDocument objDocu = null;
            try
            {
                objXml = new XmlDocument();
                objXml.LoadXml(GetTemplate());
                objXml.SelectSingleNode("//FormName").InnerText = (sSelectedForms + sExt).ToLower();
                objXml.SelectSingleNode("//RowId").InnerText = iViewId.ToString();
                objXml.SelectSingleNode("//Reset").InnerText = bCloneBaseView ? "1" : "0";
                objPVFormEdit = new PVFormEdit(m_sUserId, m_sPassword, m_sDSNName, s_DataSourceId, m_sConnString, m_iClientId);//rkaur27
                p_objXmlOut = objPVFormEdit.GetNew(objXml);
                objDocu = new XmlDocument();
                objDocu.LoadXml(GetTemplate());
                objDocu.SelectSingleNode("//FormName").InnerText = (sSelectedForms + sExt).ToLower();
                objDocu.SelectSingleNode("//RowId").InnerText = iViewId.ToString();
                XmlNodeList fieldNodes = p_objXmlOut.SelectNodes("//FormListoption");
                foreach (XmlElement xE in fieldNodes)
                {
                    if (string.IsNullOrEmpty(objDocu.SelectSingleNode("//Fields").InnerText))
                    {
                        objDocu.SelectSingleNode("//Fields").InnerText = xE.Attributes["value"].Value + "|";
                        objDocu.SelectSingleNode("//Captions").InnerText = xE.InnerText + "|";
                    }
                    else
                    {
                        objDocu.SelectSingleNode("//Fields").InnerText = objDocu.SelectSingleNode("//Fields").InnerText + xE.Attributes["value"].Value;
                        if (!objDocu.SelectSingleNode("//Fields").InnerText.EndsWith("|"))
                        {
                            objDocu.SelectSingleNode("//Fields").InnerText = objDocu.SelectSingleNode("//Fields").InnerText + "|";
                        }
                        objDocu.SelectSingleNode("//Captions").InnerText = objDocu.SelectSingleNode("//Captions").InnerText + xE.InnerText + "|";
                    }
                    objDocu.SelectSingleNode("//Helps").InnerText = objDocu.SelectSingleNode("//Helps").InnerText + "|";
                    objDocu.SelectSingleNode("//ReadOnly").InnerText = objDocu.SelectSingleNode("//ReadOnly").InnerText + "|"; //asharma326 JIRA 7508
                }
                XmlNodeList btnNodes = p_objXmlOut.SelectNodes("//ButtonListoption");
                foreach (XmlElement xE in btnNodes)
                {
                    if (string.IsNullOrEmpty(objDocu.SelectSingleNode("//Buttons").InnerText))
                    {
                        objDocu.SelectSingleNode("//Buttons").InnerText = xE.Attributes["value"].Value + "|";
                        objDocu.SelectSingleNode("//ButtonCaptions").InnerText = xE.InnerText + "|";
                    }
                    else
                    {
                        objDocu.SelectSingleNode("//Buttons").InnerText = objDocu.SelectSingleNode("//Buttons").InnerText + xE.Attributes["value"].Value + "|";
                        objDocu.SelectSingleNode("//ButtonCaptions").InnerText = objDocu.SelectSingleNode("//ButtonCaptions").InnerText + xE.InnerText + "|";
                    }
                }
                XmlNodeList tblNodes = p_objXmlOut.SelectNodes("//ToolBarListoption");
                foreach (XmlElement xE in tblNodes)
                {
                    if (string.IsNullOrEmpty(objDocu.SelectSingleNode("//Toolbar").InnerText))
                    {
                        objDocu.SelectSingleNode("//Toolbar").InnerText = xE.Attributes["value"].Value + "|";
                        objDocu.SelectSingleNode("//ToolbarCaptions").InnerText = xE.InnerText + "|";
                    }
                    else
                    {
                        objDocu.SelectSingleNode("//Toolbar").InnerText = objDocu.SelectSingleNode("//Toolbar").InnerText + xE.Attributes["value"].Value + "|";
                        objDocu.SelectSingleNode("//ToolbarCaptions").InnerText = objDocu.SelectSingleNode("//ToolbarCaptions").InnerText + xE.InnerText + "|";
                    }
                }
                objDocu.SelectSingleNode("//OnlyTopDownLayout").InnerText = "True";
                objDocu.SelectSingleNode("//sysrequired").InnerText = objXml.SelectSingleNode("//sysrequired").InnerText;
                objPVFormEdit.Save(objDocu);
            }
            catch (Exception ee)
            {
            }
            finally
            {
                if (objPVFormEdit != null)
                {
                    objPVFormEdit = null;
                }
                objXml = null;
                p_objXmlOut = null;
                objDocu = null;
            }
        }
        //Deb ML Changes
        /// <summary>
        /// GetMsgTemplate
        /// </summary>
        /// <returns></returns>
        private static string GetTemplate()
        {
            string sXml= @"<form>
                              <group>
                              <sysrequired /> 
                              <CommandButtons /> 
                              <FormFields /> 
                              <FormList /> 
                              <ButtonList /> 
                              <ToolBarButtons /> 
                              <ToolBarList /> 
                              <PVFormEdit>
                              <FormName></FormName> 
                              <Reset>0</Reset> 
                              <Fields /> 
                              <Captions /> 
                              <Helps /> 
                              <ReadOnly/>
                              <Buttons /> 
                              <ButtonCaptions /> 
                              <Toolbar /> 
                              <ToolbarCaptions /> 
                              </PVFormEdit>
                              <PVList>
                              <RowId>0</RowId> 
                              </PVList>
                              <OnlyTopDownLayout>False</OnlyTopDownLayout> 
                              <ReadOnlyForm>False</ReadOnlyForm> 
                              <IsTopDownLayout>False</IsTopDownLayout> 
                              </group>
                           </form>";
            return sXml;
        }

		/// Name		: GetFormList
		/// Author		: Anurag Agarwal
		/// Date Created: 05/19/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrievs the Form list from xml file and ADM form list by calling ADM component
		/// </summary>
		/// <returns>XML Doc containing all forms listing</returns>
		private XmlDocument GetFormList()
		{
			string sFileName = "";
			//RMConfigurator objConfig = null;
			ADMForms objADMForms = null;
			XmlDocument objXMLDocFormList = null;
			XmlElement objSelectElement = null;
			XmlElement objRowTxt = null; 
			DbReader objReader = null;
			DataModelFactory objDmf = null; 
			ADMTableList objAdmLst=null;
			ArrayList arrAdm=null;
			string[] retItem = new string[3];
			try
			{
				//objConfig = new RMConfigurator();
				objXMLDocFormList = new XmlDocument();


                //Raman 07/20/2009: removing dependency on Riskmaster.Config

                //sFileName = objConfig.GetValue(PV_XMLFILEPATH) + @"\" + objConfig.GetValue(PV_FORMLISTFILE);
                sFileName = PV_XMLFILEPATH + @"\" + PV_FORMLISTFILE;
				if(sFileName != "")
				{
					if(System.IO.File.Exists(sFileName))
						objXMLDocFormList.LoadXml(UTILITY.GetXMLFromFile(sFileName));
					else
						throw new RMAppException(Globalization.GetString("PVDefination.GetFormList.FormListNotFound" , m_iClientId)); //mbahl3 JIRA [RMACLOUD-123]
				}
				else
					throw new RMAppException(Globalization.GetString("PVDefination.GetFormList.FormListNotFound", m_iClientId));  //mbahl3 JIRA [RMACLOUD-123]
				
				objSelectElement = (XmlElement)objXMLDocFormList.SelectSingleNode("//forms"); 
//				//TODO: When the formlist file is no more required. Two columns FORM_ID AND PARENT_FORM_ID need 
//				to be added to the NET_VIEW_FORMS table to get it working. No need to pick it from formlist.xml

				//Get the list of forms whose default views have entries in the db
//				objSelectElement = (XmlElement)objXMLDocFormList.CreateElement("forms"); 
//				objXMLDocFormList.AppendChild(objSelectElement);
//				objReader = DbFactory.GetDbReader(m_sConnString,"SELECT CAPTION,FORM_NAME,TOPLEVEL FROM NET_VIEW_FORMS WHERE VIEW_ID = 0");   
//				if(objReader != null)
//				{
//					 while(objReader.Read())
//					{
//						objRowTxt = objXMLDocFormList.CreateElement("form");
//						objRowTxt.SetAttribute("name",Conversion.ConvertObjToStr(objReader["CAPTION"]));
//						objRowTxt.SetAttribute("file",Conversion.ConvertObjToStr(objReader["FORM_NAME"]));
//						objRowTxt.SetAttribute("toplevel",Conversion.ConvertObjToStr(objReader["TOPLEVEL"]));
//						objSelectElement.AppendChild((XmlNode)objRowTxt);
//					}
//					if(objReader != null)
//						objReader.Close(); 
//				}


				//Add the admin tracking tables.
				objDmf = new DataModelFactory(m_sDSNName,m_sUserId,m_sPassword, m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
				objAdmLst= (ADMTableList)objDmf.GetDataModelObject("ADMTableList",false);
				arrAdm = objAdmLst.TableUserNames();
   
				foreach(string[] arrItem in arrAdm)
				{
					objRowTxt = objXMLDocFormList.CreateElement("form");
					objRowTxt.SetAttribute("name",arrItem[1]);
					objRowTxt.SetAttribute("file","admintracking" + "%" + arrItem[0] + ".xml");
					objRowTxt.SetAttribute("toplevel","1");
					objSelectElement.AppendChild((XmlNode)objRowTxt);
				}

				return objXMLDocFormList; 
			}
			finally
			{
				if(objReader!=null)
				{
					objReader.Dispose(); 
				}
				if(objADMForms != null)
					objADMForms.Dispose();
				objXMLDocFormList = null;
				objSelectElement = null;
				objRowTxt = null; 
				if(objDmf!=null)
					objDmf.Dispose();
                //objConfig = null;
                arrAdm = null;
			}
		}
	}
}