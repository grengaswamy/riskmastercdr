using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Navdeep
    ///Dated   :   2nd,May 2008
    ///Purpose :   Safeway - Implements the WorkLossRestrictions form
    /// </summary>
    public class WorkLossRestrictions : UtilitiesUIListBase
    {

        /// <summary>
        /// Database field array
        /// </summary>
        private string[,] arrFields = {
			{"RecordType","WRKLOS_RSTRCT_CODE"},
			{"TransTypeCode","TRANS_TYPE_CODE"},
			{"LOBId","LINE_OF_BUS_CODE"}		
									  };

        private int m_iClientId = 0;
        /// Name		: WorkLossRestrictions
        /// Author		: Navdeep
        /// Date Created: 2nd,May 2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="p_sConnString">Input connection string</param>
        public WorkLossRestrictions(string p_sConnString,int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            m_iClientId = p_iClientId;
            ConnectString = p_sConnString;
            this.Initialize();
        }

        /// Name		: Initialize
        /// Author		: Navdeep
        /// Date Created: 2nd,May 2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Initialize the variables
        /// </summary>
        new private void Initialize()
        {
            base.TableName = "LT_RST_DAYS_MAP";
            base.KeyField = "WRKLOS_ROWID";
            base.RadioName = "WorkLossRestrictionList";
            base.WhereClause = "1=1";
            base.OrderByClause = "WRKLOS_ROWID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        /// Name		: Get
        /// Author		: Navdeep
        /// Date Created: 2nd,May 2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Default get method.Returns the data for the form
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with structure</param>
        /// <returns>Xml structure and output data</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            //rsharma220 MITS 34565 Start
            XmlNodeList objNodLst = null;
            XmlNodeList objNodList = null;
            XmlNode objNode = null;
            SysSettings objSettings = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            XmlElement objElmnt = null;
            int useCarrierClaim = 0;
            try
            {
                objSettings = new SysSettings(ConnectString, m_iClientId);
                useCarrierClaim = objSettings.MultiCovgPerClm;
                XMLDoc = p_objXmlDocument;
                base.Get();
                objNode = p_objXmlDocument.SelectSingleNode("//WorkLossRestrictionList");
                objNodList = objNode.ChildNodes.Item(1).ChildNodes;
                objLstRow = p_objXmlDocument.CreateElement("Settingrow");
                objRowTxt = p_objXmlDocument.CreateElement(objNodList[0].Name);
                objRowTxt.InnerText = (useCarrierClaim == 0) ? "false" : "true";
                objLstRow.AppendChild(objRowTxt);
                objNode.AppendChild((XmlNode)objLstRow);
                //rsharma220 MITS 34565 End
                objNodLst = p_objXmlDocument.GetElementsByTagName("rowtext");
                foreach (XmlElement objElm in objNodLst)
                {
                    objElm.SetAttribute("title", UTILITY.GetCode(objElm.GetAttribute("title"), base.ConnectString, m_iClientId));
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WorkLossRestrictions.Get.Err",m_iClientId), p_objEx);
            }
            finally
            {
                objNodLst = null;
            }
        }
    }
}