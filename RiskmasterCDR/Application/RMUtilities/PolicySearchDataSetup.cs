/**********************************************************************************************
*   Date     |  MITS   | Programmer | Description                                            *
**********************************************************************************************
* 06/04/2014 | 33371   | ajohari2   | Created new class for TPA changes
**********************************************************************************************/

using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;

namespace Riskmaster.Application.RMUtilities
{
    //MITS 33371 ajohari2
    /// <summary>
    ///Author  :   Ankit Johari
    ///Dated   :   7th,May 2014
    ///Purpose :   Policy Search Data Setup List Form 
    /// </summary>
    public class PolicySearchDataSetup : UtilitiesUIBase
    {
        private int m_iClientId = 0;

        /// <summary>
        /// Database fields mapping string array
        /// </summary>
        private string[,] arrFields = {
		    {"RowId","ROW_ID"},
			{"PolSys","POLICY_SYSTEM_ID"},
			{"AgentCode","AGENT_CODE"},
            {"MCO","MCO_CODE"},
            {"PCO","PCO_CODE"},
            {"BusUnit","BUS_UNIT_CODE"},
            {"BusSeg","BUS_SEGMENT_CODE"},
            {"SecGroup","GROUP_ID"}
		};

        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014
        ///Purpose :   Policy Search Data Setup 
        /// <summary>
        /// Default constructor with connection string
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>
        public PolicySearchDataSetup(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014
        ///Purpose :   Policy Search Data Setup 
        /// <summary>
        /// Initialize the variables and properties
        /// </summary>
        new private void Initialize()
        {
            TableName = "TPA_POLICY_PARMS";
            KeyField = "ROW_ID";
            base.InitFields(arrFields);
            base.Initialize();
        }

        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014
        ///Purpose :   Policy Search Data Setup 
        /// <summary>
        /// Get the structure for a new record
        /// </summary>
        /// <param name="p_objXMLDocument">XML Doc</param>
        /// <returns>Xmldocument with structure</returns>
        public XmlDocument New(XmlDocument p_objXMLDocument)
        {
            try
            {
                return p_objXMLDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchDataSetup.New.Err",m_iClientId), p_objEx);
            }
        }

        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014
        ///Purpose :   Policy Search Data Setup 
        /// <summary>
        /// Get the data for a particular id
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>xml data and structure</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm;
            try
            {
                XMLDoc = p_objXmlDocument;
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objElm != null)
                {
                    base.WhereClause = "ROW_ID=" + objElm.InnerText;
                }

                base.Get();
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchDataSetup.Get.Err",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
            }

        }

        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014
        ///Purpose :   DELETE
        /// <summary>
        /// Deletes a particular record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document</param>
        /// <returns>next record if any</returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            XmlElement objElm = null;
            try
            {
                XMLDoc = p_objXmlDocument;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objElm != null)
                {
                    base.WhereClause = "ROW_ID=" + objElm.InnerText;
                }
                base.Delete();
                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchDataSetup.Delete.Err",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
            }
        }

        /// Name   : Save
        ///Author  :   Ankit Johari
        ///Dated   :   7th,May 2014
        /// <summary>
        /// Saves the current record
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data to save it</param>
        /// <returns>Saved data along with its xml structure</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            string sRowId = "";
            int sPolSys = 0;
            string sAgentCode = "";
            string sMCO = "";
            string sPCO = "";
            string sBusUnit = "";
            string sBusSeg = "";
            int sSecGroup = 0;

            string sSQL = "";
            int iCounter = 0;
            XmlElement objElm = null;
            DbConnection objConn = null;
            Riskmaster.Db.DbReader objReader;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objElm != null)
                    sRowId = Convert.ToString(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PolSys']");
                if (objElm != null)
                    sPolSys = Convert.ToInt32(objElm.InnerText);

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='AgentCode']");
                if (objElm != null)
                    sAgentCode = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='MCO']");
                if (objElm != null)
                    sMCO = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PCO']");
                if (objElm != null)
                    sPCO = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BusUnit']");
                if (objElm != null)
                    sBusUnit = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='BusSeg']");
                if (objElm != null)
                    sBusSeg = objElm.InnerText;

                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SecGroup']");
                if (objElm != null)
                    sSecGroup = Convert.ToInt32(objElm.InnerText);

                objConn = DbFactory.GetDbConnection(ConnectString);
                objConn.Open();

                if (sRowId == "")
                {
                    sSQL = "SELECT COUNT(ROW_ID) as count FROM TPA_POLICY_PARMS WHERE POLICY_SYSTEM_ID =" + sPolSys + " AND AGENT_CODE ='" + sAgentCode + "' AND MCO_CODE ='" + sMCO + "' AND PCO_CODE ='" + sPCO + "' AND BUS_UNIT_CODE ='" + sBusUnit + "' AND BUS_SEGMENT_CODE ='" + sBusSeg + "' AND GROUP_ID ='" + sSecGroup + "'";
                    int recordCountOfTPA = objConn.ExecuteInt(sSQL);
                    if (recordCountOfTPA > 0)
                    {
                        iCounter = 1;
                        throw new Exception();
                    }

                    sSQL = "SELECT COUNT(ROW_ID) as count from TPA_POLICY_PARMS WHERE POLICY_SYSTEM_ID =" + sPolSys + " and GROUP_ID ='" + sSecGroup + "'";
                    int recordCount = objConn.ExecuteInt(sSQL);
                    if (recordCount > 10)
                    {
                        iCounter = 2;
                        throw new Exception();
                    }

                    sRowId = Convert.ToString(Utilities.GetNextUID(ConnectString, "TPA_POLICY_PARMS", m_iClientId));
                    if (sRowId == "")
                    {
                        throw new Exception();
                    }

                    sSQL = "INSERT INTO TPA_POLICY_PARMS VALUES(" + sRowId + "," + sSecGroup + ",'" + sAgentCode + "','" + sMCO + "','" + sPCO + "','" + sBusUnit + "','" + sBusSeg + "'," + sPolSys + ")";
                }
                else
                {
                    sSQL = "UPDATE TPA_POLICY_PARMS SET GROUP_ID = " + sSecGroup + " ,AGENT_CODE = '" + sAgentCode + "',MCO_CODE = '" + sMCO + "', PCO_CODE = '" + sPCO + "',BUS_UNIT_CODE = '" + sBusUnit + "' ,BUS_SEGMENT_CODE = '" + sBusSeg + "',POLICY_SYSTEM_ID = '" + sPolSys + "' WHERE  ROW_ID = " + sRowId + "";//JIRA RMA-10571 ajohari2
                }

                objConn.ExecuteNonQuery(sSQL);

                return XMLDoc;
            }
            catch (Exception p_objEx)
            {

                if (iCounter == 1)
                {
                    throw new RMAppException(Globalization.GetString("PolicySearchDataSetup.Record.Exist",m_iClientId), p_objEx);
                }
                else if (iCounter == 2)
                {
                    throw new RMAppException(Globalization.GetString("PolicySearchDataSetup.Record.Limit", m_iClientId), p_objEx);
                }
                else
                {
                    throw new RMAppException(Globalization.GetString("PolicySearchDataSetup.Save.Err", m_iClientId), p_objEx);
                }
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }


        /// <summary>
        /// Get record By GroupID and SystemId
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="PolicySystemId"></param>
        /// <returns></returns>
        public string GetByPolicySystemAndGroupName(int groupId, int PolicySystemId)
        {
            string sSQL = "";

            System.Data.DataSet objDsTPAPolicy = null;
            string sValue = "";

            try
            {

                sSQL = "select * from TPA_POLICY_PARMS where POLICY_SYSTEM_ID = " + PolicySystemId + " and GROUP_ID = " + groupId;

                objDsTPAPolicy = DbFactory.GetDataSet(ConnectString, sSQL,m_iClientId);
                if (objDsTPAPolicy.Tables.Count > 0)
                {
                    if (objDsTPAPolicy.Tables[0].Rows.Count > 0)
                    {
                        int counter = 0;

                        string sMCO_Code = "";
                        string sPCO_Code = "";
                        string sAgent_Code = "";
                        string sBS_Code = "";
                        string sBU_Code = "";

                        while (counter < objDsTPAPolicy.Tables[0].Rows.Count)
                        {
                            sMCO_Code += getValue(Convert.ToString(objDsTPAPolicy.Tables[0].Rows[counter]["MCO_CODE"]), 2, "0");
                            sPCO_Code += getValue(Convert.ToString(objDsTPAPolicy.Tables[0].Rows[counter]["PCO_CODE"]), 2, "0");
                            sAgent_Code += getValue(Convert.ToString(objDsTPAPolicy.Tables[0].Rows[counter]["AGENT_CODE"]), 7, "0");
                            sBS_Code += getValue(Convert.ToString(objDsTPAPolicy.Tables[0].Rows[counter]["BUS_SEGMENT_CODE"]), 8, " ");
                            sBU_Code += getValue(Convert.ToString(objDsTPAPolicy.Tables[0].Rows[counter]["BUS_UNIT_CODE"]), 10, " ");

                            counter++;
                        }

                        sValue = sMCO_Code + "|" + sPCO_Code + "|" + sAgent_Code + "|" + sBS_Code + "|" + sBU_Code + "|";

                    }
                }

            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchDataSetup.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                objDsTPAPolicy = null;
            }

            return sValue;

        }

        /// <summary>
        /// Used to add "0" if not having MaxCount
        /// </summary>
        /// <param name="fieldValue"></param>
        /// <param name="maxCount"></param>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public string getValue(string fieldValue, int maxCount, string Prefix)
        {
            string tempFieldValue = "";
            int lengthOfFieldValue = fieldValue.Length;
            if (lengthOfFieldValue < maxCount)
            {
                int checkCount = 0;
                while (checkCount < (maxCount - lengthOfFieldValue))
                {
                    tempFieldValue += Prefix;
                    checkCount++;
                }
            }
            fieldValue = tempFieldValue + fieldValue;
            return fieldValue;
        }

    }
}