﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author	:   Anurag Agarwal
	///Class	:	ExpConParms
	///Dated	:   07 April 2005
	///Purpose	:   This class is use Add / Modify the Expense Constant data
	/// </summary>
	public class ExpConParms
	{
		/// <summary>
		/// Private variable to store Connection String
		/// </summary>
		private string m_sConnectString = "";

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "";
		
		/// <summary>
		/// Private constant variable to store TableName
		/// </summary>
		private const string TABLENAME = "SYS_POL_EXP_CONST";

        private int m_iClientId = 0;

		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
		public ExpConParms(string p_sLoginName, string p_sConnectionString, int p_iClientId)
		{
			m_sUserName = p_sLoginName;
			m_sConnectString = p_sConnectionString;
            m_iClientId = p_iClientId;
		}


		/// Name		: LoadData
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Loads Expense Constant record on the basis of Expconrowid passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">The Expconrowid is passed in the XML
		///		The sample input XML will be
		///				<Document>
		///					<ExpCon>
		///						<use></use>
		///						<amount></amount>
		///						<effectivedate></effectivedate>
		///						<expirationdate></expirationdate>
		///						<expcontype></expcontype>
		///						<lob></lob>
		///						<state></state>
		///						<expconrowid>4</expconrowid>
		///					</ExpCon>
		///				</Document>
		/// </param>
		/// <returns>The XML with data is returned 
		///		The sample output XML will be 
		///				<Document>
		///					<ExpCon>
		///						<use></use>
		///						<amount></amount>
		///						<effectivedate></effectivedate>
		///						<expirationdate></expirationdate>
		///						<expcontype></expcontype>
		///						<lob></lob>
		///						<state></state>
		///						<expconrowid>4</expconrowid>
		///					</ExpCon>
		///				</Document>
		///</returns>
		public XmlDocument LoadData(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbReader objDbReader = null;
			string sExpConRowId = "";
			XmlElement objExpConXMLEle = null;

			string sValue=string.Empty;
			string sShortCode=string.Empty;
			string sDesc=string.Empty;
			XmlElement objElement=null;
			LocalCache objLocalCache=null;

			try
			{
				objExpConXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/ExpCon");

				sExpConRowId = objExpConXMLEle.GetElementsByTagName("expconrowid").Item(0).InnerText;

				//In case of Edit
				if(sExpConRowId != "")
				{
					sSQL = "SELECT * FROM " + TABLENAME + " WHERE EXP_CON_ROWID= " + sExpConRowId;
					objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
				
					if(objDbReader != null)
						if(objDbReader.Read())
						{
							sValue=objDbReader["IN_USE_FLAG"].ToString();
							if (sValue.Equals("-1") || sValue.Equals("1"))
								objExpConXMLEle.GetElementsByTagName("use").Item(0).InnerText = "True";
							else
								objExpConXMLEle.GetElementsByTagName("use").Item(0).InnerText= "";
							
							objExpConXMLEle.GetElementsByTagName("amount").Item(0).InnerText = objDbReader["AMOUNT"].ToString();
							objExpConXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText =  UTILITY.FormatDate(objDbReader["EFFECTIVE_DATE"].ToString(),false);
							objExpConXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText = UTILITY.FormatDate(objDbReader["EXPIRATION_DATE"].ToString(),false);

                            objLocalCache = new LocalCache(m_sConnectString, m_iClientId);

							objElement=(XmlElement)objExpConXMLEle.GetElementsByTagName("expcontype").Item(0);
							sValue= objDbReader["FLAT_OR_PERCENT"].ToString();
							objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);

							objElement=(XmlElement)objExpConXMLEle.GetElementsByTagName("lob").Item(0);
							sValue= objDbReader["LINE_OF_BUSINESS"].ToString();
							objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);

							objElement=(XmlElement)objExpConXMLEle.GetElementsByTagName("state").Item(0);
							sValue= objDbReader["STATE"].ToString();
							objLocalCache.GetStateInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sDesc);
							objElement.InnerText = sShortCode + " " + sDesc;
							objElement.SetAttribute("codeid",sValue);
						}
					if(objDbReader != null)
						objDbReader.Close();
				}

				return p_objInputXMLDoc;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("ExpConParms.LoadData.Error", m_iClientId),p_objExp);   
			}
            finally
            {
                if (objDbReader != null)
                    objDbReader.Dispose();
                if (objLocalCache != null)
                    objLocalCache.Dispose();
            }

		}


		/// Name		: Save
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves Expense Constant record on the basis of Expconrowid passed
		/// If new record then Expconrowid will be empty string
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<ExpCon>
		///						<use>1</use>
		///						<amount>28</amount>
		///						<effectivedate>20050406</effectivedate>
		///						<expirationdate>20050418</expirationdate>
		///						<expcontype>5237</expcontype>
		///						<lob>5188</lob>
		///						<state>73</state>
		///						<expconrowid>4</expconrowid> OR <expconrowid></expconrowid>(for new record)
		///					</ExpCon>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		public bool Save(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbConnection objCon = null; 
			string sExpConRowId = "";
			ExpenseConstant objExpenseConstant = null;
			bool bIsNew = false;
			XmlElement objExpConXMLEle = null;

			try
			{
				if(Validate(p_objInputXMLDoc)) 
				{
					objExpConXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/ExpCon"); 	

					sExpConRowId = objExpConXMLEle.GetElementsByTagName("expconrowid").Item(0).InnerText;

					objExpenseConstant = new ExpenseConstant();

					if(sExpConRowId == "")
					{
						bIsNew = true;
                        objExpenseConstant.ExpConRowId = Utilities.GetNextUID(m_sConnectString, TABLENAME, m_iClientId);
						objExpenseConstant.AddedByUser = m_sUserName;
                        objExpenseConstant.DttmRcdAdded = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
					}
					else
						objExpenseConstant.ExpConRowId = Conversion.ConvertStrToInteger(sExpConRowId); 

					objExpenseConstant.UpdatedByUser = m_sUserName;
                    objExpenseConstant.DttmRcdLastUpd = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
					
					if (objExpConXMLEle.GetElementsByTagName("use").Item(0).InnerText.ToLower().Trim().Equals("true"))
						objExpenseConstant.Use=1;
					else
						objExpenseConstant.Use=0;

					//objExpenseConstant.Use = Conversion.ConvertStrToInteger(objExpConXMLEle.GetElementsByTagName("use").Item(0).InnerText);
					
					//objExpenseConstant.State = Conversion.ConvertStrToInteger(objExpConXMLEle.GetElementsByTagName("state").Item(0).InnerText);
					objExpenseConstant.State = Conversion.ConvertStrToInteger(objExpConXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value);
					objExpenseConstant.Amount = Conversion.ConvertStrToDouble(objExpConXMLEle.GetElementsByTagName("amount").Item(0).InnerText);
					//objExpenseConstant.ExpConType = Conversion.ConvertStrToInteger(objExpConXMLEle.GetElementsByTagName("expcontype").Item(0).InnerText);
					objExpenseConstant.ExpConType = Conversion.ConvertStrToInteger(objExpConXMLEle.GetElementsByTagName("expcontype").Item(0).Attributes["codeid"].Value);
					objExpenseConstant.EffectiveDate = objExpConXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
					objExpenseConstant.ExpirationDate = objExpConXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;
					//objExpenseConstant.LOB = Conversion.ConvertStrToInteger(objExpConXMLEle.GetElementsByTagName("lob").Item(0).InnerText);
					objExpenseConstant.LOB = Conversion.ConvertStrToInteger(objExpConXMLEle.GetElementsByTagName("lob").Item(0).Attributes["codeid"].Value);

					if(bIsNew)
					{
                        //Parijat : Mits 11446
                        if (objExpenseConstant.ExpirationDate.Trim() == "" || objExpenseConstant.ExpirationDate == "0")
                        {
                            sSQL = "INSERT INTO " + TABLENAME + " (EXP_CON_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," +
                            "UPDATED_BY_USER,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,LINE_OF_BUSINESS," +
                            "IN_USE_FLAG) VALUES(" + objExpenseConstant.ExpConRowId + ",'" + objExpenseConstant.AddedByUser + "'," + objExpenseConstant.DttmRcdAdded + "," +
                            objExpenseConstant.DttmRcdLastUpd + ",'" + objExpenseConstant.UpdatedByUser + "'," +
                            objExpenseConstant.State + "," + objExpenseConstant.Amount + "," + objExpenseConstant.ExpConType + ",'" + UTILITY.FormatDate(objExpenseConstant.EffectiveDate, true) + "',"  +
                            objExpenseConstant.LOB + "," + objExpenseConstant.Use + ")";
                        }
                        else
                        {
                            sSQL = "INSERT INTO " + TABLENAME + " (EXP_CON_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," +
                            "UPDATED_BY_USER,STATE,AMOUNT,FLAT_OR_PERCENT,EFFECTIVE_DATE,EXPIRATION_DATE,LINE_OF_BUSINESS," +
                            "IN_USE_FLAG) VALUES(" + objExpenseConstant.ExpConRowId + ",'" + objExpenseConstant.AddedByUser + "'," + objExpenseConstant.DttmRcdAdded + "," +
                            objExpenseConstant.DttmRcdLastUpd + ",'" + objExpenseConstant.UpdatedByUser + "'," +
                            objExpenseConstant.State + "," + objExpenseConstant.Amount + "," + objExpenseConstant.ExpConType + ",'" + UTILITY.FormatDate(objExpenseConstant.EffectiveDate, true) + "','" +
                            UTILITY.FormatDate(objExpenseConstant.ExpirationDate, true) + "'," + objExpenseConstant.LOB + "," + objExpenseConstant.Use + ")";
                        }
					}
					else
					{
                        //Parijat : Mits 11446
                        if (objExpenseConstant.ExpirationDate.Trim() == "" || objExpenseConstant.ExpirationDate == "0")
                        {
                            sSQL = "UPDATE " + TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objExpenseConstant.DttmRcdLastUpd + "," +
                                "UPDATED_BY_USER='" + objExpenseConstant.UpdatedByUser + "'," + "STATE=" + objExpenseConstant.State +
                                "," + "AMOUNT=" + objExpenseConstant.Amount + "," + "FLAT_OR_PERCENT=" + objExpenseConstant.ExpConType + "," +
                                "EFFECTIVE_DATE='" + UTILITY.FormatDate(objExpenseConstant.EffectiveDate, true) + "'," + "EXPIRATION_DATE= NULL"  + "," +
                                "LINE_OF_BUSINESS=" + objExpenseConstant.LOB + "," + "IN_USE_FLAG=" + objExpenseConstant.Use +
                                " WHERE EXP_CON_ROWID=" + objExpenseConstant.ExpConRowId;
                        }
                        else
                        {
                            sSQL = "UPDATE " + TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objExpenseConstant.DttmRcdLastUpd + "," +
                                "UPDATED_BY_USER='" + objExpenseConstant.UpdatedByUser + "'," + "STATE=" + objExpenseConstant.State +
                                "," + "AMOUNT=" + objExpenseConstant.Amount + "," + "FLAT_OR_PERCENT=" + objExpenseConstant.ExpConType + "," +
                                "EFFECTIVE_DATE='" + UTILITY.FormatDate(objExpenseConstant.EffectiveDate, true) + "'," + "EXPIRATION_DATE='" + UTILITY.FormatDate(objExpenseConstant.ExpirationDate, true) + "'," +
                                "LINE_OF_BUSINESS=" + objExpenseConstant.LOB + "," + "IN_USE_FLAG=" + objExpenseConstant.Use +
                                " WHERE EXP_CON_ROWID=" + objExpenseConstant.ExpConRowId;
                        }
					}

					objCon = DbFactory.GetDbConnection(m_sConnectString);
					objCon.Open();
					objCon.ExecuteNonQuery(sSQL);
					objCon.Dispose(); 

					return true;
				}
				else
					return false;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("ExpConParms.Save.Error", m_iClientId), p_objExp);   
			}
            finally
            {
                if (objCon != null)
                    objCon.Dispose();
            }
        }
	

		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes Expense Constant record
		/// </summary>
		/// <param name="p_iExpConRowId">Expense constant ID</param>
		/// <returns>True for sucess and false for failure</returns>
		internal bool Delete(int p_iExpConRowId)
		{
			string sSQL = "";
			DbConnection objCon = null;

            try
            {
                if (p_iExpConRowId != 0)
                {
                    sSQL = "DELETE FROM " + TABLENAME + " WHERE EXP_CON_ROWID = " + p_iExpConRowId;
                    objCon = DbFactory.GetDbConnection(m_sConnectString);
                    objCon.Open();
                    objCon.ExecuteNonQuery(sSQL);
                    objCon.Dispose();
                }

                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("ExpConParms.Delete.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCon != null) 
                    objCon.Dispose(); 
            }
		}
	

		/// Name		: Validate
		/// Author		: Anurag Agarwal
		/// Date Created	: 7 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Validate Expense Constant data for the values passed
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///				<Document>
		///					<ExpCon>
		///						<use></use>
		///						<amount></amount>
		///						<effectivedate></effectivedate>
		///						<expirationdate></expirationdate>
		///						<expcontype></expcontype>
		///						<lob></lob>
		///						<state></state>
		///						<expconrowid>4</expconrowid>
		///					</ExpCon>
		///				</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		private bool Validate(XmlDocument p_objInputXMLDoc)
		{
			string sExpConRowId = "";
			string sLOB = "";
			string sState = "";
			string sEffectiveDate = "";
			string sExpirationDate = "";
			string sSQL = "";
			DbReader objDBReader = null;
			DbReader objTempDBReader = null;
			bool bIsReaderHasRows = false;
			XmlElement objExpConXMLEle = null;

			try
			{
				objExpConXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document/ExpCon");

				sExpConRowId = objExpConXMLEle.GetElementsByTagName("expconrowid").Item(0).InnerText;
				sLOB = objExpConXMLEle.GetElementsByTagName("lob").Item(0).Attributes["codeid"].Value;
				sState = objExpConXMLEle.GetElementsByTagName("state").Item(0).Attributes["codeid"].Value;
				sEffectiveDate = objExpConXMLEle.GetElementsByTagName("effectivedate").Item(0).InnerText;
				sExpirationDate = objExpConXMLEle.GetElementsByTagName("expirationdate").Item(0).InnerText;

				if(sExpirationDate.Trim() == "")
				{
					sSQL = "SELECT EXP_CON_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + 
						sLOB + " AND STATE = " + sState + " AND EXPIRATION_DATE IS NULL ";
				
					objDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

					while(objDBReader.Read())
					{
						bIsReaderHasRows = true;

						if(objDBReader["EXP_CON_ROWID"].ToString() != sExpConRowId)
                            throw new RMAppException(Globalization.GetString("ExpConParms.Validate.AlreadyExistsWithNoExpDate", m_iClientId));
						else
						{
							sSQL = "SELECT COUNT(EXP_CON_ROWID) FROM " + TABLENAME + " WHERE" + 
								" LINE_OF_BUSINESS = " + sLOB + " AND STATE = " + sState + 
								" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'";

							objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
							if(objTempDBReader.Read())
							{
								if(Conversion.ConvertObjToInt(objTempDBReader.GetValue(0), m_iClientId) > 0)
                                    throw new RMAppException(Globalization.GetString("ExpConParms.Validate.AlreadyExists", m_iClientId));
							}
							objTempDBReader.Close();
						}
					}
					objDBReader.Close();

					if(bIsReaderHasRows == false)
					{
						sSQL = "SELECT EXP_CON_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + sLOB + 
							" AND STATE = " + sState + " AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'";

						objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						while(objTempDBReader.Read())
						{
							if(objTempDBReader["EXP_CON_ROWID"].ToString() != sExpConRowId)
                                throw new RMAppException(Globalization.GetString("ExpConParms.Validate.AlreadyExists", m_iClientId));
						}
						objTempDBReader.Close();
					}
				}
				else
				{
					sSQL = "SELECT EXP_CON_ROWID, EFFECTIVE_DATE FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + 
						sLOB + " AND STATE = " + sState + " AND EXPIRATION_DATE IS NULL ";
				
					objDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

					while(objDBReader.Read())
					{
						bIsReaderHasRows = true;

						if(objDBReader["EXP_CON_ROWID"].ToString() != sExpConRowId)
						{

                            //Mits id: 22839 - Govind - Starts
                            sEffectiveDate = sEffectiveDate.Replace("/", "");

                            string mm = sEffectiveDate.Substring(0, 2);
                            string dd = sEffectiveDate.Substring(2, 2);
                            string yyyy = sEffectiveDate.Substring(4, 4);

                            string datestringconvert = yyyy + mm + dd;
                            sEffectiveDate = datestringconvert.ToString().Trim();

                            //Mits id: 22839 - Govind - End

							if(Conversion.ToDate(sEffectiveDate) >= Conversion.ToDate(objDBReader["EFFECTIVE_DATE"].ToString().Trim()))
                                throw new RMAppException(Globalization.GetString("ExpConParms.Validate.AlreadyExists", m_iClientId));
							else
							{
								sSQL = "SELECT EXP_CON_ROWID FROM " + TABLENAME + " WHERE LINE_OF_BUSINESS = " + sLOB + 
									" AND STATE = " + sState + " AND EXPIRATION_DATE >= '" + 
									Conversion.GetDate(sEffectiveDate) + "' AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

								objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
								while(objTempDBReader.Read())
								{
									if(objTempDBReader["EXP_CON_ROWID"].ToString() != sExpConRowId)
                                        throw new RMAppException(Globalization.GetString("ExpConParms.Validate.AlreadyExists", m_iClientId));
								}
								objTempDBReader.Close();
							}
						}
						else
						{
							sSQL = "SELECT COUNT(EXP_CON_ROWID) FROM " + TABLENAME + " WHERE" + 
								" LINE_OF_BUSINESS = " + sLOB + " AND STATE = " + sState + 
								" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'" + 
								" AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

							objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
							while(objTempDBReader.Read())
							{
								if(Conversion.ConvertObjToInt(objTempDBReader.GetValue(0), m_iClientId) > 0)
                                    throw new RMAppException(Globalization.GetString("ExpConParms.Validate.AlreadyExists", m_iClientId));
							}
							objTempDBReader.Close();
						}
					}
					objDBReader.Close();
 
					if(bIsReaderHasRows == false)
					{
						sSQL = "SELECT EXP_CON_ROWID FROM " + TABLENAME + " WHERE" + 
							" LINE_OF_BUSINESS = " + sLOB + " AND STATE = " + sState + 
							" AND EXPIRATION_DATE >= '" + Conversion.GetDate(sEffectiveDate) + "'" + 
							" AND EFFECTIVE_DATE <= " + Conversion.GetDate(sExpirationDate);

						objTempDBReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						while(objTempDBReader.Read())
						{
							if(objTempDBReader["EXP_CON_ROWID"].ToString() != sExpConRowId)
                                throw new RMAppException(Globalization.GetString("ExpConParms.Validate.AlreadyExists", m_iClientId));
						}
						objTempDBReader.Close();
					}
				}
				return true;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
            finally
            {
                if (objDBReader != null)
                    objDBReader.Dispose();
                if (objTempDBReader != null)
                    objTempDBReader.Dispose();
            }

		}
	}

	/// <summary>
	///Author	:   Anurag Agarwal
	///Class	:	ExpenseConstant
	///Dated	:   06 April 2005
	///Purpose	:   This class works as a container class for "SYS_POL_EXP_CONST" table
	/// </summary>
	public class ExpenseConstant
	{
		public ExpenseConstant()
		{
		}

		/// <summary>
		/// Stores the ExpConRowId value
		/// </summary>
		private int m_iExpConRowId = 0;

		/// <summary>
		/// Stores the Use value
		/// </summary>
		private int m_iUse = 0;

		/// <summary>
		/// Stores the LOB value
		/// </summary>
		private int m_iLOB = 0;

		/// <summary>
		/// Stores the State value
		/// </summary>
		private int m_iState = 0;

		/// <summary>
		/// Stores the Amount value
		/// </summary>
		private double m_dAmount = 0.0;

		/// <summary>
		/// Stores the ExpConType value
		/// </summary>
		private int m_iExpConType = 0;

		/// <summary>
		/// Stores the EffectiveDate value
		/// </summary>
		private string m_sEffectiveDate = string.Empty;

		/// <summary>
		/// Stores the ExpirationDate value
		/// </summary>
		private string m_sExpirationDate = string.Empty;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

		/// <summary>
		/// Read/Write property for ExpConRowId
		/// </summary>
		internal int ExpConRowId
		{
			get
			{
				return m_iExpConRowId;
			}
			set
			{
				m_iExpConRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for Use
		/// </summary>
		internal int Use
		{
			get
			{
				return m_iUse;
			}
			set
			{
				m_iUse = value;
			}
		}

		/// <summary>
		/// Read/Write property for LOB
		/// </summary>
		internal int LOB
		{
			get
			{
				return m_iLOB;
			}
			set
			{
				m_iLOB = value;
			}
		}

		/// <summary>
		/// Read/Write property for State
		/// </summary>
		internal int State
		{
			get
			{
				return m_iState;
			}
			set
			{
				m_iState = value;
			}
		}

		/// <summary>
		/// Read/Write property for Amount
		/// </summary>
		internal double Amount
		{
			get
			{
				return m_dAmount;
			}
			set
			{
				m_dAmount = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpConType
		/// </summary>
		internal int ExpConType
		{
			get
			{
				return m_iExpConType;
			}
			set
			{
				m_iExpConType = value;
			}
		}

		/// <summary>
		/// Read/Write property for EffectiveDate
		/// </summary>
		internal string EffectiveDate
		{
			get
			{
				return m_sEffectiveDate;
			}
			set
			{
				m_sEffectiveDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for ExpirationDate
		/// </summary>
		internal string ExpirationDate
		{
			get
			{
				return m_sExpirationDate;
			}
			set
			{
				m_sExpirationDate = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}
	}
}
