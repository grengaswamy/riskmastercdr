using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Parag Sarin
    ///Dated   :   02 May 2005
    ///Purpose :   This class Creates, Edits, Fetches Funds Transactions Mapping.
    /// </summary>
    public class DCIBenefitCodeMapping : UtilityFormBase
    {
        ///************************************************************** 
        ///* $File		: DCIBenefitCodeMapping.cs 
        ///* $Revision	: 1.0.0.0 
        ///* $Date		: 30-August-2014
        ///* $Author	: Neha Goel/Ankit Gupta
        ///* $Comment	: MITS# 36997 GAP 06 - RMA-5497
        ///* $Source	: 
        ///**************************************************************

        #region Private Variables
        /// <summary>
        /// String Array for Base Class fields
        /// </summary>
        private string[,] arrFields =
		{
			{"DCIBenefitType", "BEN_CODE_ID"},
			{"DCITransactionType", "TRANS_CODE_ID"},
			{"BenefitsRowId", "DCI_BEN_ROW_ID"},
			{"DCIWeeklyAmount", "BEN_WEEK_AMT"}
		};
        /// <summary>
        /// Field Mapping with SQL queries
        /// </summary>
        private string[,] arrChilds = {
			//{"XMLTag", "Query"}
			{"DCIBenefitsList","SELECT TRANS_CODE_ID, BEN_CODE_ID, DCI_BEN_ROW_ID, BEN_WEEK_AMT FROM DCI_BEN_TRANS_MAP"}
                };
        /// <summary>
        /// User name
        /// </summary>
        private string m_sUserName = string.Empty;
        /// <summary>
        /// Connection string
        /// </summary>
        private string m_sDSN = string.Empty;
        private int m_iClientId = 0;
        #endregion

        #region Constructor
        
        //public DCIBenefitCodeMapping() { }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>
        public DCIBenefitCodeMapping(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            try
            {
                ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.Constructor.Error", m_iClientId), p_objEx);
            }

        }
        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="p_sLoginName">Login Name</param>
        /// <param name="p_sConnectionString">Connection String</param>
        public DCIBenefitCodeMapping(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString,p_iClientId)
        {
            try
            {
                m_sUserName = p_sLoginName;
                ConnectString = p_sConnectionString;
                m_iClientId = p_iClientId;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.Constructor.Error", m_iClientId), p_objEx);
            }

        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Initialize the properties
        /// </summary>
        new private void Initialize()
        {
            try
            {
                TableName = string.Empty;
                KeyField = string.Empty;
                InitFields(arrFields);
                base.ChildArray = arrChilds;
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.Initialize.Error", m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Gets the benefit amount by benefit code.
        /// </summary>
        /// <param name="p_sBenefitCode">The P_S benefit code.</param>
        /// <param name="p_bValueExist">if set to <c>true</c> [P_B value exist].</param>
        /// <returns></returns>
        /// <exception cref="Riskmaster.ExceptionTypes.RMAppException"></exception>
        private double GetBenefitAmountByBenefitCode(string p_sBenefitCode , ref bool p_bValueExist)
        {
            StringBuilder sbSql = null;
            double dblBenefitAmount = 0;
            try
            {
                sbSql = new StringBuilder();
                sbSql.Append(" Select BEN_WEEK_AMT From DCI_BEN_TRANS_MAP");
                sbSql.Append(" Where BEN_CODE_ID =" + p_sBenefitCode);
                sbSql.Append(" ORDER BY DCI_BEN_TRANS_MAP.DCI_BEN_ROW_ID DESC");

                using (DbReader objReader = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            p_bValueExist = true;
                            dblBenefitAmount = objReader.GetDouble("BEN_WEEK_AMT");
                        }
                    }
                }
            }
            catch (Exception p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.GetsBenefitAmountBysBenefitCode.Error", m_iClientId), p_objEx);
            }
            finally
            {
                sbSql = null;
            }

            return dblBenefitAmount;
        }

        /// <summary>
        /// Updates all benefit amount for benefit code.
        /// </summary>
        /// <param name="p_sBenefitCode">The P_S benefit code.</param>
        /// <param name="p_lngBenefitAmount">The P_LNG benefit amount.</param>
        /// <returns></returns>
        /// <exception cref="Riskmaster.ExceptionTypes.RMAppException"></exception>
        private bool UpdateAllBenefitAmountForBenefitCode(string p_sBenefitCode, double p_lngBenefitAmount)
        {
            ArrayList arrlstSql = null;
            string sSQL = string.Empty;
            bool bReturnValue = false;
            try
            {
                arrlstSql = new ArrayList();
                sSQL = "UPDATE DCI_BEN_TRANS_MAP SET BEN_WEEK_AMT = " + p_lngBenefitAmount + " WHERE BEN_CODE_ID=" + p_sBenefitCode;
                arrlstSql.Add(sSQL);
                UTILITY.Save(arrlstSql, base.ConnectString, m_iClientId);
                bReturnValue = true;
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.UpdateAllBenefitAmountForBenefitCode.Error", m_iClientId), p_objEx);
            }
            finally
            {
                arrlstSql = null;
            }
            return bReturnValue;
        }

        #endregion

        #region Public Functions
        /// Name		: Get
        /// Author		: Ankit Gupta
        /// Date Created: 30-August-2014
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>Xml containing the data to be populated on screen</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument, UserLogin objUser)
        {
            XmlNodeList objNodLst = null;
            XmlElement objElement = null;
            XmlNode objNode = null;
            string sValue = string.Empty;
            LocalCache objCache = null;
            string sShortCode = string.Empty;
            string sShortDesc = string.Empty;
            string sTableId = string.Empty;
            string sValue1 = string.Empty;
            DbReader objRead = null;
            XmlNodeList objNodTmpLst = null;
            StringBuilder sbSql = null;
            try
            {
                XMLDoc = p_objXmlDocument;
                Get();

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIBenefits']");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                sTableId = objCache.GetTableId("NCCI_BENEFIT_TYPE").ToString();
                //objCache.Dispose();
                sbSql = new StringBuilder();
                sbSql.Append(" SELECT CODES.CODE_ID, CODES.SHORT_CODE FROM CODES");
                sbSql.Append(" WHERE CODES.TABLE_ID =" + sTableId);
                sbSql.Append(" AND CODES.DELETED_FLAG = 0 ORDER BY CODES.SHORT_CODE");
                objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString());
                while (objRead.Read())
                {
                    objElement = XMLDoc.CreateElement("option");
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
                    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")).Trim();
                    objNode.AppendChild(objElement);
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIBenefits']");
                objElement = p_objXmlDocument.CreateElement("option");
                objElement.SetAttribute("value", string.Empty);
                objElement.InnerText = string.Empty;
                if (objNode.FirstChild != null)
                    objNode.InsertBefore((XmlNode)objElement, objNode.FirstChild);


                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIBenefits']");
                objNodLst = objNode.ChildNodes;
                //objCache = new LocalCache(base.ConnectString);
                foreach (XmlNode objTemp in objNodLst)
                {

                    if (objTemp.NodeType == XmlNodeType.Element)
                    {
                        if (objTemp.Attributes["value"].Value != null)
                            sValue = objTemp.Attributes["value"].Value;
                        objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue), ref sShortCode, ref sShortDesc);
                        objTemp.InnerText = sShortCode + " " + sShortDesc;
                    }
                }

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIBenefitsList']");
                objNodLst = objNode.SelectNodes("./listrow/rowtext[@name!='BenefitsRowId']");
                foreach (XmlNode objTemp in objNodLst)
                {
                    if (objTemp.NodeType == XmlNodeType.Element)
                    {
                        if (objTemp.Attributes["title"].Value != null)
                            sValue = objTemp.Attributes["title"].Value;
                        objTemp.InnerText = sValue;
                        if (objTemp.Attributes["name"].Value != null && objTemp.Attributes["name"].Value != "DCIWeeklyAmount")
                        {
                            objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue), ref sShortCode, ref sShortDesc);
                            objTemp.Attributes["title"].Value = sShortCode + " " + sShortDesc;
                        }
                        else
                            objTemp.Attributes["title"].Value = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectString), Convert.ToDouble(sValue), base.ConnectString, m_iClientId);
                    }
                }

                objNodLst = p_objXmlDocument.SelectNodes("//control[@name='DCIBenefitsList']/listrow");
                foreach (XmlNode objTemp in objNodLst)
                {
                    objNodTmpLst = objTemp.SelectNodes("./rowtext");
                    foreach (XmlNode objTmp in objNodTmpLst)
                    {
                        if (objTmp.NodeType == XmlNodeType.Element)
                        {
                            if (objTmp.Attributes["name"].Name != null && objTmp.Attributes["name"].Name != "BenefitsRowId")
                            {
                                if (objTmp.Attributes["name"].Value != null && objTmp.Attributes["name"].Value != "DCIWeeklyAmount")
                                    sValue1 = sValue1 + objTmp.InnerText + "|";
                            }
                        }
                    }
                    sValue1 = sValue1 + "**";
                }
                objNode = p_objXmlDocument.SelectSingleNode("//form/group");
                objElement = p_objXmlDocument.CreateElement("BenefitListValues");
                objElement.InnerText = sValue1;
                objNode.AppendChild(objElement);
                //objCache.Dispose();
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNodLst = null;
                objElement = null;
                objNode = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                if (objRead != null)
                {
                    objRead.Dispose();
                }
                objNodTmpLst = null;
                sbSql = null;

            }
            return XMLDoc;
        }
        /// Name		: DeleteBenefitsMapping
        /// Author		: Ankit Gupta
        /// Date Created: 30-August-2014
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes Benefits Mapping information
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public bool DeleteBenefitsMapping(XmlDocument p_objXmlDocument)
        {
            ArrayList arrlstSql = null;
            string sSQL = string.Empty;
            bool bReturnValue = false;
            string sRecord = string.Empty;
            XmlNode objNode = null;
            string[] arrValues = null;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIBenefitsList']");
                if (objNode.InnerText != null)
                {
                    arrValues = objNode.InnerText.Split(" ".ToCharArray());
                }
                arrlstSql = new ArrayList();
                if (arrValues != null)
                {
                    for (int i = 0; i < arrValues.Length; i++)
                    {
                        if (!arrValues[i].Trim().Equals(string.Empty))
                        {
                            sSQL = "DELETE FROM DCI_BEN_TRANS_MAP WHERE DCI_BEN_ROW_ID=" + arrValues[i];
                            arrlstSql.Add(sSQL);
                        }
                    }
                }
                UTILITY.Save(arrlstSql, base.ConnectString, m_iClientId);
                bReturnValue = true;
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.DeleteBenefitsMapping.Error", m_iClientId), p_objEx);
            }
            finally
            {
                arrlstSql = null;
                objNode = null;
                arrValues = null;
            }
            return bReturnValue;
        }
        private string GetValue(XmlDocument p_objXmlDocument, string p_sValue)
        {
            XmlNode objNode = null;
            objNode = p_objXmlDocument.SelectSingleNode("//control[@name='" + p_sValue + "']");
            if (objNode != null)
            {
                if (objNode.InnerText != null)
                {
                    return objNode.InnerText;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }
        /// Name		: SaveBenefits
        /// Author		: Ankit Gupta
        /// Date Created: 30-August-2014	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Saves DCI Benefit code Mapping
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public bool SaveBenefits(XmlDocument p_objXmlDocument)
        {
            ArrayList objQuery = null;
            string sValue1 = string.Empty;
            string sValue2 = string.Empty;
            double sValue3 = 0;
            string sQuery = string.Empty;
            string sDate = string.Empty;
            DbReader objRead = null;
            string sInsertQuery = string.Empty;
            int iNextId = 0;
            bool bReturnValue = false;
            string sUser = m_sUserName;
            try
            {
                XMLDoc = p_objXmlDocument;
                objQuery = new ArrayList();
                sValue1 = GetValue(p_objXmlDocument, "DCIBenefits");
                sValue2 = GetValue(p_objXmlDocument, "TransactionTypeBenefitID");
                sValue3 = Conversion.ConvertStrToDouble(GetValue(p_objXmlDocument, "BenefitAmount"));
                if (sValue1 != string.Empty && sValue2 != string.Empty)
                {
                    sQuery = "SELECT DCI_BEN_ROW_ID FROM DCI_BEN_TRANS_MAP WHERE TRANS_CODE_ID='" + sValue1 + "' AND BEN_CODE_ID='" + sValue2 + "'";
                    objRead = DbFactory.GetDbReader(base.ConnectString, sQuery);
                    if (objRead.Read())
                    {
                        objRead.Close();
                        throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.Save.ErrorTransactionTypeBenefits", m_iClientId));
                    }
                    if (!objRead.IsClosed)
                    {
                        objRead.Close();
                    }
                    iNextId = Utilities.GetNextUID(base.ConnectString, "DCI_BEN_TRANS_MAP", m_iClientId);
                    sDate = "'" + Conversion.ToDbDateTime(DateTime.Now) + "'";
                    sInsertQuery = "INSERT INTO DCI_BEN_TRANS_MAP (DCI_BEN_ROW_ID,BEN_CODE_ID,TRANS_CODE_ID,BEN_WEEK_AMT,DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER) VALUES (" + iNextId.ToString()
                        + "," + sValue1 + "," + sValue2 + "," + sValue3.ToString() + "," + sDate + ",'" + sUser + "'," + sDate + ",'" + sUser + "')";
                    objQuery.Add(sInsertQuery);
                }
                base.Save(objQuery);
                UpdateAllBenefitAmountForBenefitCode(sValue1, sValue3);
                bReturnValue = true;
            }
            catch (RMAppException p_objEx)
            {
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objQuery = null;
                if (objRead != null)
                {
                    objRead.Dispose();
                }

            }
            return bReturnValue;

        }

        /// <summary>
        /// Gets the benefit amount.
        /// </summary>
        /// <param name="p_objXmlDocument">The p_obj XML document.</param>
        /// <param name="objUser">The object user.</param>
        /// <returns></returns>
        /// <exception cref="Riskmaster.ExceptionTypes.RMAppException"></exception>
        public XmlDocument GetBenefitAmount(XmlDocument p_objXmlDocument, UserLogin objUser) 
        {
            XmlElement objElement = null;
            XmlNode objNode = null;
            string sBenefitCode = string.Empty;
            double dblBenefitAmount = 0;
            bool ValueExist = false;
            try
            {
                XMLDoc = p_objXmlDocument;
                sBenefitCode = GetValue(p_objXmlDocument, "DCIBenefits");
                dblBenefitAmount = GetBenefitAmountByBenefitCode(sBenefitCode, ref ValueExist);
                //((XmlElement)objNode).SetAttribute("name", "value");
                if (ValueExist)
                    p_objXmlDocument.SelectSingleNode("//control[@name='BenefitAmount']").InnerText = CommonFunctions.ConvertByCurrencyCode(CommonFunctions.GetBaseCurrencyCode(base.ConnectString), dblBenefitAmount, base.ConnectString, m_iClientId);//Convert.ToString(dblBenefitAmount);
                else
                    p_objXmlDocument.SelectSingleNode("//control[@name='BenefitAmount']").InnerText = string.Empty;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DCIBenefitCodeMapping.GetBenefitAmount.Error", m_iClientId), p_objEx);
            }
            finally 
            { 
            
            }
            return XMLDoc;
        }
        
        #endregion
    }
}