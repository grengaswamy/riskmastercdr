﻿/**********************************************************************************************
 *   Date     |  MITS   | Programmer | Description                                            *
 **********************************************************************************************
 * 05/23/2014 | 34270  | aahuja21   | Added check memo in payment history gridview
 **********************************************************************************************/
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using System.Collections.Generic;
using System.Text;
using Riskmaster.DataModel;//WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Anshul Verma
    ///Dated   :   19th Nov 2012
    ///Purpose :   This class Create the configurable header details for diary, enhanced notes and payment history page.
    /// </summary>
   public class HeaderConfiguration 
    {
        /// <summary>
        /// Object to store Diary Configurations
        /// </summary>
        private HeaderConfig m_Headerconfig;
       //dvatsa
        private int m_iClientId = 0;
        internal struct HeaderConfig
        {
            //For Diary Configuration
            private bool m_Priority;
            private bool m_Text;
            private bool m_WorkActivity;
            //igupta3 jira 439
            private bool m_NotRoutable;
            private bool m_NotRollable;
            private bool m_Department;
            private bool m_Due;
            private bool m_AttachedRecord;
            private bool m_ClaimStatus;
            private bool m_Claimant;
            private bool m_AssignedUser; 
            private bool m_AssigningUser; 
            private string m_PriorityHeader;
            private string m_TextHeader;
            private string m_WorkActivityHeader;
            //igupta3 jira 439
            private string m_NotRoutableHeader;
            private string m_NotRollableHeader;
            private string m_DepartmentHeader;
            private string m_DueHeader;
            private string m_AttachedRecordHeader;
            private string m_ClaimStatusHeader;
            private string m_ClaimantHeader;
            private string m_AssignedUserHeader; 
            private string m_AssigningUserHeader; 
            //Added by Indu
            private bool m_ParentRecord;
            private string m_ParentRecordHeader;

            internal bool Priority { get { return m_Priority; } set { m_Priority = value; } }
            internal bool Text { get { return m_Text; } set { m_Text = value; } }
            internal bool WorkActivity { get { return m_WorkActivity; } set { m_WorkActivity = value; } }
            //igupta3 jira 439
            internal bool NotRoutable { get { return m_NotRoutable; } set { m_NotRoutable = value; } }
            internal bool NotRollable { get { return m_NotRollable; } set { m_NotRollable = value; } }
            internal bool Department { get { return m_Department; } set { m_Department = value; } }
            internal bool Due { get { return m_Due; } set { m_Due = value; } }
            internal bool AttachedRecord { get { return m_AttachedRecord; } set { m_AttachedRecord = value; } }
            internal bool ClaimStatus { get { return m_ClaimStatus; } set { m_ClaimStatus = value; } }
            internal bool Claimant { get { return m_Claimant; } set { m_Claimant = value; } }
            internal string PriorityHeader { get { return m_PriorityHeader; } set { m_PriorityHeader = value; } }
            internal string TextHeader { get { return m_TextHeader; } set { m_TextHeader = value; } }
            internal string WorkActivityHeader { get { return m_WorkActivityHeader; } set { m_WorkActivityHeader = value; } }
            //igupta3 jira 439
            internal string NotRoutableHeader { get { return m_NotRoutableHeader; } set { m_NotRoutableHeader = value; } }
            internal string NotRollableHeader { get { return m_NotRollableHeader; } set { m_NotRollableHeader = value; } }
            internal string DepartmentHeader { get { return m_DepartmentHeader; } set { m_DepartmentHeader = value; } }
            internal string DueHeader { get { return m_DueHeader; } set { m_DueHeader = value; } }
            internal string AttachedRecordHeader { get { return m_AttachedRecordHeader; } set { m_AttachedRecordHeader = value; } }
            internal string ClaimStatusHeader { get { return m_ClaimStatusHeader; } set { m_ClaimStatusHeader = value; } }
            internal string ClaimantHeader { get { return m_ClaimantHeader; } set { m_ClaimantHeader = value; } }
            internal bool AssignedUser { get { return m_AssignedUser; } set { m_AssignedUser = value; } }
            internal bool AssigningUser { get { return m_AssigningUser; } set { m_AssigningUser = value; } }
            internal string AssignedUserHeader { get { return m_AssignedUserHeader; } set { m_AssignedUserHeader = value; } }
            internal string AssigningUserHeader { get { return m_AssigningUserHeader; } set { m_AssigningUserHeader = value; } }
            //Added by Indu
            internal bool ParentRecord { get { return m_ParentRecord; } set { m_ParentRecord = value; } }
            internal string ParentRecordHeader { get { return m_ParentRecordHeader; } set { m_ParentRecordHeader = value; } }

            //For Progress Notes Configuration
            private bool m_Activity;
            private bool m_NoteType;
            private bool m_NoteText;
            private bool m_AttachedTo;
            private bool m_EnteredBy;
            private bool m_DateCreated;
            private bool m_TimeCreated;
            private bool m_UserType;
            private bool m_Subject;

            private string m_ActivityHeader;
            private string m_NoteTypeHeader;
            private string m_NoteTextHeader;
            private string m_AttachedToHeader;
            private string m_EnteredByHeader;
            private string m_DateCreatedHeader;
            private string m_TimeCreatedHeader;
            private string m_UserTypeHeader;
            private string m_SubjectHeader;

            internal bool Activity { get { return m_Activity; } set { m_Activity = value; } }
            internal bool NoteType { get { return m_NoteType; } set { m_NoteType = value; } }
            internal bool NoteText { get { return m_NoteText; } set { m_NoteText = value; } }
            internal bool AttachedTo { get { return m_AttachedTo; } set { m_AttachedTo = value; } }
            internal bool EnteredBy { get { return m_EnteredBy; } set { m_EnteredBy = value; } }
            internal bool DateCreated { get { return m_DateCreated; } set { m_DateCreated = value; } }
            internal bool TimeCreated { get { return m_TimeCreated; } set { m_TimeCreated = value; } }
            internal bool UserType { get { return m_UserType; } set { m_UserType = value; } }
            internal bool Subject { get { return m_Subject; } set { m_Subject = value; } }

            internal string ActivityHeader { get { return m_ActivityHeader; } set { m_ActivityHeader = value; } }
            internal string NoteTypeHeader { get { return m_NoteTypeHeader; } set { m_NoteTypeHeader = value; } }
            internal string NoteTextHeader { get { return m_NoteTextHeader; } set { m_NoteTextHeader = value; } }
            internal string AttachedToHeader { get { return m_AttachedToHeader; } set { m_AttachedToHeader = value; } }
            internal string EnteredByHeader { get { return m_EnteredByHeader; } set { m_EnteredByHeader = value; } }
            internal string DateCreatedHeader { get { return m_DateCreatedHeader; } set { m_DateCreatedHeader = value; } }
            internal string TimeCreatedHeader { get { return m_TimeCreatedHeader; } set { m_TimeCreatedHeader = value; } }
            internal string UserTypeHeader { get { return m_UserTypeHeader; } set { m_UserTypeHeader = value; } }
            internal string SubjectHeader { get { return m_SubjectHeader; } set { m_SubjectHeader = value; } }

            //For Payment History Configuration      

            private bool m_Control;
            private bool m_Check;
            private bool m_TransDate;
            private bool m_Type;
            private bool m_Cleared;
            private bool m_Void;
            private bool m_StopPay; //added by swati mits # 33431
            private bool m_Status;
            private bool m_Payee;
            private bool m_CheckAmount;
            private bool m_FromDate;
            private bool m_ToDate;
            private bool m_Invoice;
            private bool m_TransactionType;
            private bool m_SplitAmount;
            private bool m_User;
            private bool m_CheckDate;
            private bool m_CombinedPay;
            //Deb
            private bool m_IsFirstFinal;
            private bool m_PolicyName;
            private bool m_CoverageType;
            private bool m_Unit;
            private bool m_LossType;
            private bool m_ManualCheck;
            private bool m_Offset;//igupta3 Mits# 34515
            private bool m_CheckTotal;//Added by Nikhil on 07/29/14.Check Total  in payment history configuration

            private bool m_InvoiceAmount;
            private bool m_CheckMemo;//Ashish Ahuja Mits 34270
            private bool m_HoldReason;//sachin 7810
            private bool m_ReserveType;
            private string m_ControlHeader;
            private string m_CheckHeader;
            private string m_TransDateHeader;
            private string m_TypeHeader;
            private string m_ClearedHeader;
            private string m_VoidHeader;
            private string m_StopPayHeader; //added by swati mits # 33431
            private string m_StatusHeader;
            private string m_PayeeHeader;
            private string m_CheckAmountHeader;
            private string m_FromDateHeader;
            private string m_ToDateHeader;
            private string m_InvoiceHeader;
            private string m_TransactionTypeHeader;
            private string m_SplitAmountHeader;
            private string m_UserHeader;
            private string m_CheckDateHeader;
            private string m_CombinedPayHeader;

            //Deb
            private string m_IsFirstFinalHeader;
            private string m_PolicyNameHeader;
            private string m_CoverageTypeHeader;
            private string m_UnitHeader;
            private string m_LossTypeHeader;
            private string m_ManualCheckHeader;
            private string m_OffsetHeader;//igupta3 Mits# 34515
            private string m_CheckTotalHeader; //Added by Nikhil on 07/29/14.Check Total  in payment history configuration

            private string m_CheckMemoHeader;//Ashish Ahuja Mits 34270
            private string m_HoldReasonHeader;//sachin 7810
            private string m_InvoiceAmountHeader;
            private string m_ReserveTypeHeader;

            internal bool Control { get { return m_Control; } set { m_Control = value; } }
            internal bool Check { get { return m_Check; } set { m_Check = value; } }
            internal bool TransDate { get { return m_TransDate; } set { m_TransDate = value; } }
            internal bool Type { get { return m_Type; } set { m_Type = value; } }
            internal bool Cleared { get { return m_Cleared; } set { m_Cleared = value; } }
            internal bool Void { get { return m_Void; } set { m_Void = value; } }
            //Added by swati agarwal for MITS # 33431
            internal bool StopPay { get { return m_StopPay; } set { m_StopPay = value; } }
            //change end here by swati
            internal bool Status { get { return m_Status; } set { m_Status = value; } }
            internal bool Payee { get { return m_Payee; } set { m_Payee = value; } }
            internal bool CheckAmount { get { return m_CheckAmount; } set { m_CheckAmount = value; } }
            internal bool FromDate { get { return m_FromDate; } set { m_FromDate = value; } }
            internal bool ToDate { get { return m_ToDate; } set { m_ToDate = value; } }
            internal bool Invoice { get { return m_Invoice; } set { m_Invoice = value; } }
            internal bool TransactionType { get { return m_TransactionType; } set { m_TransactionType = value; } }
            internal bool SplitAmount { get { return m_SplitAmount; } set { m_SplitAmount = value; } }
            internal bool User { get { return m_User; } set { m_User = value; } }
            internal bool CheckDate { get { return m_CheckDate; } set { m_CheckDate = value; } }
            internal bool CombinedPay { get { return m_CombinedPay; } set { m_CombinedPay = value; } }

            //Deb
            internal bool IsFirstFinal { get { return m_IsFirstFinal; } set { m_IsFirstFinal = value; } }
            internal bool PolicyName { get { return m_PolicyName; } set { m_PolicyName = value; } }
            internal bool CoverageType { get { return m_CoverageType; } set { m_CoverageType = value; } }
            internal bool Unit { get { return m_Unit; } set { m_Unit = value; } }
            internal bool LossType { get { return m_LossType; } set { m_LossType = value; } }
            internal bool ManualCheck { get { return m_ManualCheck; } set { m_ManualCheck = value; } }
            internal bool Offset { get { return m_Offset; } set { m_Offset = value; } } //igupta3 Mits#34515
            internal bool CheckTotal { get { return m_CheckTotal; } set { m_CheckTotal = value; } } //Added by Nikhil on 07/29/14.Check Total  in payment history configuration

            internal bool InvoiceAmount { get { return m_InvoiceAmount; } set { m_InvoiceAmount = value; } } // Mits 25726
            internal bool CheckMemo { get { return m_CheckMemo; } set { m_CheckMemo = value; } } //Ashish Ahuja Mits 34270
            internal bool HoldReason{ get { return m_HoldReason; } set { m_HoldReason = value; } }//sachin 7810
            internal bool ReserveType { get { return m_ReserveType; } set { m_ReserveType = value; } }
            internal string ControlHeader { get { return m_ControlHeader; } set { m_ControlHeader = value; } }
            internal string CheckHeader { get { return m_CheckHeader; } set { m_CheckHeader = value; } }
            internal string TransDateHeader { get { return m_TransDateHeader; } set { m_TransDateHeader = value; } }
            internal string TypeHeader { get { return m_TypeHeader; } set { m_TypeHeader = value; } }
            internal string ClearedHeader { get { return m_ClearedHeader; } set { m_ClearedHeader = value; } }
            internal string VoidHeader { get { return m_VoidHeader; } set { m_VoidHeader = value; } }
            //Added by swati agarwal for MITS # 33431
            internal string StopPayHeader { get { return m_StopPayHeader; } set { m_StopPayHeader = value; } }
            //change end here by swati
            internal string StatusHeader { get { return m_StatusHeader; } set { m_StatusHeader = value; } }
            internal string PayeeHeader { get { return m_PayeeHeader; } set { m_PayeeHeader = value; } }
            internal string CheckAmountHeader { get { return m_CheckAmountHeader; } set { m_CheckAmountHeader = value; } }
            internal string FromDateHeader { get { return m_FromDateHeader; } set { m_FromDateHeader = value; } }
            internal string ToDateHeader { get { return m_ToDateHeader; } set { m_ToDateHeader = value; } }
            internal string InvoiceHeader { get { return m_InvoiceHeader; } set { m_InvoiceHeader = value; } }
            internal string TransactionTypeHeader { get { return m_TransactionTypeHeader; } set { m_TransactionTypeHeader = value; } }
            internal string SplitAmountHeader { get { return m_SplitAmountHeader; } set { m_SplitAmountHeader = value; } }
            internal string UserHeader { get { return m_UserHeader; } set { m_UserHeader = value; } }
            internal string CheckDateHeader { get { return m_CheckDateHeader; } set { m_CheckDateHeader = value; } }
            internal string CombinedPayHeader { get { return m_CombinedPayHeader; } set { m_CombinedPayHeader = value; } }

            //Deb
            internal string IsFirstFinalHeader { get { return m_IsFirstFinalHeader; } set { m_IsFirstFinalHeader = value; } }
            internal string PolicyNameHeader { get { return m_PolicyNameHeader; } set { m_PolicyNameHeader = value; } }
            internal string CoverageTypeHeader { get { return m_CoverageTypeHeader; } set { m_CoverageTypeHeader = value; } }
            internal string UnitHeader { get { return m_UnitHeader; } set { m_UnitHeader = value; } }
            internal string LossTypeHeader { get { return m_LossTypeHeader; } set { m_LossTypeHeader = value; } }
            internal string ManualCheckHeader { get { return m_ManualCheckHeader; } set { m_ManualCheckHeader = value; } }
            internal string OffsetHeader { get { return m_OffsetHeader; } set { m_OffsetHeader = value; } } // igupta3 Mits# 34515
            internal string CheckTotalHeader { get { return m_CheckTotalHeader; } set { m_CheckTotalHeader = value; } } // Added by Nikhil on 07/29/14.Check Total  in payment history configuration

            internal string InvoiceAmountHeader { get { return m_InvoiceAmountHeader; } set { m_InvoiceAmountHeader = value; } } // Mits 25726
			internal string CheckMemoHeader { get { return m_CheckMemoHeader; } set { m_CheckMemoHeader = value; } } //Ashish Ahuja Mits 34270
            internal string HoldReasonHeader { get { return m_HoldReasonHeader; } set { m_HoldReasonHeader = value; } } //sachin 7810
            internal string ReserveTypeHeader { get { return m_ReserveTypeHeader; } set { m_ReserveTypeHeader = value; } }
        }

       
       string m_sConnectionString = string.Empty;
       int m_iUserId = 0;

       public HeaderConfiguration(string p_sConn,int p_iUserId)
       {
           m_sConnectionString = p_sConn;
           m_iUserId = p_iUserId;
       }
        public XmlDocument GetHeaderConfig(int p_iUserId)
        {
            DbReader objReader = null;
            XmlDocument objOutDoc = null;
            XmlElement objRootNode = null;
            string sSQL = string.Empty;
            string sUserPrefsXML = string.Empty;
            XmlDocument objUserPrefXML = null;
            try
            {
                objOutDoc = new XmlDocument();
                objRootNode = objOutDoc.CreateElement("Config");
                objOutDoc.AppendChild(objRootNode);



                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + p_iUserId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                //For Diary Header Configuration
                XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Priority/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Priority = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Text/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Text = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecord/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AttachedRecord = objNode.Value.ToString() == "1" ? true : false;
                }
                //Added by Indu
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecord/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ParentRecord = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Claimant/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Claimant = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatus/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ClaimStatus = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Department/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Department = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Due/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Due = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivity/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.WorkActivity = objNode.Value.ToString() == "1" ? true : false;
                }
                //igupta3 jira 439
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutable/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NotRoutable = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollable/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NotRollable = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/PriorityHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.PriorityHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/TextHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TextHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecordHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AttachedRecordHeader = objNode.Value.ToString();
                }
                //Added by Indu
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecordHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ParentRecordHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimantHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ClaimantHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatusHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ClaimStatusHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DepartmentHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.DepartmentHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DueHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.DueHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivityHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.WorkActivityHeader = objNode.Value.ToString();
                }
                //igupta3 jira 439
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutableHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NotRoutableHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollableHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NotRollableHeader = objNode.Value.ToString();
                }

                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUser/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AssignedUser = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUser/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AssigningUser = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUserHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AssignedUserHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUserHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AssigningUserHeader = objNode.Value.ToString();
                }
                //asharma326 MITS 34544 
                //Deb
                object oCrClm = DbFactory.ExecuteScalar(m_sConnectionString, "SELECT STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME='MULTI_COVG_CLM'");
                if (Convert.ToString(oCrClm) == "0")
                {
                    this.CreateAndSetElement(objRootNode, "CarrierClaim", Boolean.FalseString);
                }
                else
                {
                    this.CreateAndSetElement(objRootNode, "CarrierClaim", Boolean.TrueString);
                }
                this.CreateAndSetElement(objRootNode, "Priority", m_Headerconfig.Priority.ToString());
                this.CreateAndSetElement(objRootNode, "Text", m_Headerconfig.Text.ToString());
                this.CreateAndSetElement(objRootNode, "WorkActivity", m_Headerconfig.WorkActivity.ToString());
                //igupta3 jira 439
                this.CreateAndSetElement(objRootNode, "NotRoutable", m_Headerconfig.NotRoutable.ToString());
                this.CreateAndSetElement(objRootNode, "NotRollable", m_Headerconfig.NotRollable.ToString());
                this.CreateAndSetElement(objRootNode, "Department", m_Headerconfig.Department.ToString());
                this.CreateAndSetElement(objRootNode, "Due", m_Headerconfig.Due.ToString());
                this.CreateAndSetElement(objRootNode, "AttachedRecord", m_Headerconfig.AttachedRecord.ToString());
                //Added By Indu 
                this.CreateAndSetElement(objRootNode, "ParentRecord", m_Headerconfig.ParentRecord.ToString());
                this.CreateAndSetElement(objRootNode, "Claimant", m_Headerconfig.Claimant.ToString());
                this.CreateAndSetElement(objRootNode, "ClaimStatus", m_Headerconfig.ClaimStatus.ToString());
                this.CreateAndSetElement(objRootNode, "PriorityHeader", m_Headerconfig.PriorityHeader.ToString());
                this.CreateAndSetElement(objRootNode, "TextHeader", m_Headerconfig.TextHeader.ToString());
                this.CreateAndSetElement(objRootNode, "WorkActivityHeader", m_Headerconfig.WorkActivityHeader.ToString());
                //igupta3 jira 439
                this.CreateAndSetElement(objRootNode, "NotRoutableHeader", m_Headerconfig.NotRoutableHeader.ToString());
                this.CreateAndSetElement(objRootNode, "NotRollableHeader", m_Headerconfig.NotRollableHeader.ToString());
                this.CreateAndSetElement(objRootNode, "DepartmentHeader", m_Headerconfig.DepartmentHeader.ToString());
                this.CreateAndSetElement(objRootNode, "DueHeader", m_Headerconfig.DueHeader.ToString());
                this.CreateAndSetElement(objRootNode, "AttachedRecordHeader", m_Headerconfig.AttachedRecordHeader.ToString());
                //Added by Indu
                this.CreateAndSetElement(objRootNode, "ParentRecordHeader", m_Headerconfig.ParentRecordHeader.ToString());
                this.CreateAndSetElement(objRootNode, "ClaimantHeader", m_Headerconfig.ClaimantHeader.ToString());
                this.CreateAndSetElement(objRootNode, "ClaimStatusHeader", m_Headerconfig.ClaimStatusHeader.ToString());
                this.CreateAndSetElement(objRootNode, "AssignedUser", m_Headerconfig.AssignedUser.ToString());
                this.CreateAndSetElement(objRootNode, "AssigningUser", m_Headerconfig.AssigningUser.ToString());
                this.CreateAndSetElement(objRootNode, "AssignedUserHeader", m_Headerconfig.AssignedUserHeader.ToString());
                this.CreateAndSetElement(objRootNode, "AssigningUserHeader", m_Headerconfig.AssigningUserHeader.ToString());

                //For Progress Notes Configuration
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/Activity/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Activity = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteType/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NoteType = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteText/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NoteText = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/AttachedTo/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AttachedTo = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/EnteredBy/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.EnteredBy = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/DateCreated/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.DateCreated = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/TimeCreated/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TimeCreated = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/UserType/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.UserType = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/Subject/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Subject = objNode.Value.ToString() == "1" ? true : false;
                }

                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/ActivityHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ActivityHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteTypeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NoteTypeHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteTextHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.NoteTextHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/AttachedToHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.AttachedToHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/EnteredByHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.EnteredByHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/DateCreatedHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.DateCreatedHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/TimeCreatedHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TimeCreatedHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/UserTypeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.UserTypeHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/SubjectHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.SubjectHeader = objNode.Value.ToString();
                }

                this.CreateAndSetElement(objRootNode, "Activity", m_Headerconfig.Activity.ToString());
                this.CreateAndSetElement(objRootNode, "NoteType", m_Headerconfig.NoteType.ToString());
                this.CreateAndSetElement(objRootNode, "NoteText", m_Headerconfig.NoteText.ToString());
                this.CreateAndSetElement(objRootNode, "AttachedTo", m_Headerconfig.AttachedTo.ToString());
                this.CreateAndSetElement(objRootNode, "EnteredBy", m_Headerconfig.EnteredBy.ToString());
                this.CreateAndSetElement(objRootNode, "DateCreated", m_Headerconfig.DateCreated.ToString());
                this.CreateAndSetElement(objRootNode, "TimeCreated", m_Headerconfig.TimeCreated.ToString());
                this.CreateAndSetElement(objRootNode, "UserType", m_Headerconfig.UserType.ToString());
                this.CreateAndSetElement(objRootNode, "Subject", m_Headerconfig.Subject.ToString());
                this.CreateAndSetElement(objRootNode, "ActivityHeader", m_Headerconfig.ActivityHeader.ToString());
                this.CreateAndSetElement(objRootNode, "NoteTypeHeader", m_Headerconfig.NoteTypeHeader.ToString());
                this.CreateAndSetElement(objRootNode, "NoteTextHeader", m_Headerconfig.NoteTextHeader.ToString());
                this.CreateAndSetElement(objRootNode, "AttachedToHeader", m_Headerconfig.AttachedToHeader.ToString());
                this.CreateAndSetElement(objRootNode, "EnteredByHeader", m_Headerconfig.EnteredByHeader.ToString());
                this.CreateAndSetElement(objRootNode, "DateCreatedHeader", m_Headerconfig.DateCreatedHeader.ToString());
                this.CreateAndSetElement(objRootNode, "TimeCreatedHeader", m_Headerconfig.TimeCreatedHeader.ToString());
                this.CreateAndSetElement(objRootNode, "UserTypeHeader", m_Headerconfig.UserTypeHeader.ToString());
                this.CreateAndSetElement(objRootNode, "SubjectHeader", m_Headerconfig.SubjectHeader.ToString());

                //For Payment History Configuration      
                 
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Control/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Control = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Check/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Check = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransDate/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TransDate = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Type/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Type = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Cleared/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Cleared = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Void/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Void = objNode.Value.ToString() == "1" ? true : false;
                }                
                //Added by swati for MITS # 33431 WWIg Gap 9
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/StopPay/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.StopPay = objNode.Value.ToString() == "1" ? true : false;
                }
                //change end here by swati
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Status/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Status = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Payee/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Payee = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckAmount/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckAmount = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/FromDate/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.FromDate = objNode.Value.ToString() == "1" ? true : false;
                } 
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ToDate/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ToDate = objNode.Value.ToString() == "1" ? true : false;
                } 
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Invoice/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Invoice = objNode.Value.ToString() == "1" ? true : false;
                } 
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransactionType/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TransactionType = objNode.Value.ToString() == "1" ? true : false;
                } 
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/SplitAmount/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.SplitAmount = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/User/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.User = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckDate/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckDate = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CombinedPay/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CombinedPay = objNode.Value.ToString() == "1" ? true : false;
                }

                //Deb
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/IsFirstFinal/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.IsFirstFinal = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/PolicyName/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.PolicyName = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CoverageType/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CoverageType = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Unit/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Unit = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/LossType/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.LossType = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ManualCheck/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ManualCheck = objNode.Value.ToString() == "1" ? true : false;
                }
                //igupta3 Mits#34515
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Offset/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.Offset = objNode.Value.ToString() == "1" ? true : false;
                }
                 //Ashish Ahuja Mits 34270 start
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckMemo/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckMemo = objNode.Value.ToString() == "1" ? true : false;
                }
                //Ashish Ahuja Mits 34270 end

                //sachin  7810 starts
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/HoldReason/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.HoldReason = objNode.Value.ToString() == "1" ? true : false;
                }
                //sachin 7810 ends
                // Mits 25726 start
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/InvoiceAmount/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.InvoiceAmount = objNode.Value.ToString() == "1" ? true : false;
                }
                // Mits 25726 end
				
                //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckTotal/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckTotal = objNode.Value.ToString() == "1" ? true : false;
                }


                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ReserveType/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ReserveType = objNode.Value.ToString() == "1" ? true : false;
                }
                else
                {
                    m_Headerconfig.ReserveType = true; // RMA-13511: for first time loading:aaggarwal29
                }
                
                //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ControlHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ControlHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransDateHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TransDateHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TypeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TypeHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ClearedHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ClearedHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/VoidHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.VoidHeader = objNode.Value.ToString();
                }
                //Added by swati for MITS # 33431 WWIg Gap 9
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/StopPayHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.StopPayHeader = objNode.Value.ToString();
                }
                //change end here by swati
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/StatusHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.StatusHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/PayeeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.PayeeHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckAmountHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckAmountHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/FromDateHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.FromDateHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ToDateHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ToDateHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/InvoiceHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.InvoiceHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransactionTypeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.TransactionTypeHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/SplitAmountHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.SplitAmountHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/UserHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.UserHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckDateHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckDateHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CombinedPayHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CombinedPayHeader = objNode.Value.ToString();
                }
                //Deb
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/IsFirstFinalHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.IsFirstFinalHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/PolicyNameHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.PolicyNameHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CoverageTypeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CoverageTypeHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/UnitHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.UnitHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/LossTypeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.LossTypeHeader = objNode.Value.ToString();
                }
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ManualCheckHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ManualCheckHeader = objNode.Value.ToString();
                }

                //igupta3 Mits#34515
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/OffsetHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.OffsetHeader = objNode.Value.ToString();
                }
				 //Ashish Ahuja Mits 34270 start
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckMemoHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckMemoHeader = objNode.Value.ToString();
                }
                //Ashish Ahuja Mits 34270 end
                //sachin 7810 starts
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/HoldReasonHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.HoldReasonHeader = objNode.Value.ToString();
                }
                //sachin 7810 ends
                //Mits 25726 start
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/InvoiceAmountHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.InvoiceAmountHeader = objNode.Value.ToString();
                }
                // Mits 25726 end

                //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckTotalHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.CheckTotalHeader = objNode.Value.ToString();
                }

                objNode = objNode.SelectSingleNode("//setting/PayHisConfig/ReserveTypeHeader/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_Headerconfig.ReserveTypeHeader = objNode.Value.ToString();
                }
                else
                {
                    m_Headerconfig.ReserveTypeHeader = "Reserve Type";//RMA-13511: for first time user loading:aaggarwal29
                }
                //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                this.CreateAndSetElement(objRootNode, "Control", m_Headerconfig.Control.ToString());
                this.CreateAndSetElement(objRootNode, "Check", m_Headerconfig.Check.ToString());
                this.CreateAndSetElement(objRootNode, "TransDate", m_Headerconfig.TransDate.ToString());
                this.CreateAndSetElement(objRootNode, "Type", m_Headerconfig.Type.ToString());
                this.CreateAndSetElement(objRootNode, "Cleared", m_Headerconfig.Cleared.ToString());
                this.CreateAndSetElement(objRootNode, "Void", m_Headerconfig.Void.ToString());
                //Added by swati MITS # 33431
                this.CreateAndSetElement(objRootNode, "StopPay", m_Headerconfig.StopPay.ToString());
                //change end here by swati
                this.CreateAndSetElement(objRootNode, "Status", m_Headerconfig.Status.ToString());
                this.CreateAndSetElement(objRootNode, "Payee", m_Headerconfig.Payee.ToString());
                this.CreateAndSetElement(objRootNode, "CheckAmount", m_Headerconfig.CheckAmount.ToString());
                this.CreateAndSetElement(objRootNode, "FromDate", m_Headerconfig.FromDate.ToString());
                this.CreateAndSetElement(objRootNode, "ToDate", m_Headerconfig.ToDate.ToString());
                this.CreateAndSetElement(objRootNode, "Invoice", m_Headerconfig.Invoice.ToString());
                this.CreateAndSetElement(objRootNode, "TransactionType", m_Headerconfig.TransactionType.ToString());
                this.CreateAndSetElement(objRootNode, "SplitAmount", m_Headerconfig.SplitAmount.ToString());
                this.CreateAndSetElement(objRootNode, "User", m_Headerconfig.User.ToString());
                this.CreateAndSetElement(objRootNode, "CheckDate", m_Headerconfig.CheckDate.ToString());
                this.CreateAndSetElement(objRootNode, "CombinedPay", m_Headerconfig.CombinedPay.ToString());

                //Deb
                this.CreateAndSetElement(objRootNode, "IsFirstFinal", m_Headerconfig.IsFirstFinal.ToString());
                this.CreateAndSetElement(objRootNode, "PolicyName", m_Headerconfig.PolicyName.ToString());
                this.CreateAndSetElement(objRootNode, "CoverageType", m_Headerconfig.CoverageType.ToString());
                this.CreateAndSetElement(objRootNode, "Unit", m_Headerconfig.Unit.ToString());
                this.CreateAndSetElement(objRootNode, "LossType", m_Headerconfig.LossType.ToString());
                this.CreateAndSetElement(objRootNode, "ManualCheck", m_Headerconfig.ManualCheck.ToString());
                this.CreateAndSetElement(objRootNode, "Offset", m_Headerconfig.Offset.ToString()); //igupta3 Mits#34515
                this.CreateAndSetElement(objRootNode, "CheckTotal", m_Headerconfig.CheckTotal.ToString()); //Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                this.CreateAndSetElement(objRootNode, "CheckMemo", m_Headerconfig.CheckMemo.ToString()); //Ashish Ahuja Mits 34270
                this.CreateAndSetElement(objRootNode, "HoldReason", m_Headerconfig.HoldReason.ToString());//sachin 7810
                this.CreateAndSetElement(objRootNode, "InvoiceAmount", m_Headerconfig.InvoiceAmount.ToString()); // Mits 25726

                this.CreateAndSetElement(objRootNode, "ReserveType", m_Headerconfig.ReserveType.ToString());
                this.CreateAndSetElement(objRootNode, "ControlHeader", m_Headerconfig.ControlHeader.ToString());
                this.CreateAndSetElement(objRootNode, "CheckHeader", m_Headerconfig.CheckHeader.ToString());
                this.CreateAndSetElement(objRootNode, "TransDateHeader", m_Headerconfig.TransDateHeader.ToString());
                this.CreateAndSetElement(objRootNode, "TypeHeader", m_Headerconfig.TypeHeader.ToString());
                this.CreateAndSetElement(objRootNode, "ClearedHeader", m_Headerconfig.ClearedHeader.ToString());
                this.CreateAndSetElement(objRootNode, "VoidHeader", m_Headerconfig.VoidHeader.ToString());
                //Added by swati MITS # 33431
                this.CreateAndSetElement(objRootNode, "StopPayHeader", m_Headerconfig.StopPayHeader.ToString());
                //change end here by swati
                this.CreateAndSetElement(objRootNode, "StatusHeader", m_Headerconfig.StatusHeader.ToString());
                this.CreateAndSetElement(objRootNode, "PayeeHeader", m_Headerconfig.PayeeHeader.ToString());
                this.CreateAndSetElement(objRootNode, "CheckAmountHeader", m_Headerconfig.CheckAmountHeader.ToString());
                this.CreateAndSetElement(objRootNode, "FromDateHeader", m_Headerconfig.FromDateHeader.ToString());
                this.CreateAndSetElement(objRootNode, "ToDateHeader", m_Headerconfig.ToDateHeader.ToString());
                this.CreateAndSetElement(objRootNode, "InvoiceHeader", m_Headerconfig.InvoiceHeader.ToString());
                this.CreateAndSetElement(objRootNode, "TransactionTypeHeader", m_Headerconfig.TransactionTypeHeader.ToString());
                this.CreateAndSetElement(objRootNode, "SplitAmountHeader", m_Headerconfig.SplitAmountHeader.ToString());
                this.CreateAndSetElement(objRootNode, "UserHeader", m_Headerconfig.UserHeader.ToString());
                this.CreateAndSetElement(objRootNode, "CheckDateHeader", m_Headerconfig.CheckDateHeader.ToString());
                this.CreateAndSetElement(objRootNode, "CombinedPayHeader", m_Headerconfig.CombinedPayHeader.ToString());

                //Deb
                this.CreateAndSetElement(objRootNode, "IsFirstFinalHeader", m_Headerconfig.IsFirstFinalHeader.ToString());
                this.CreateAndSetElement(objRootNode, "PolicyNameHeader", m_Headerconfig.PolicyNameHeader.ToString());
                this.CreateAndSetElement(objRootNode, "CoverageTypeHeader", m_Headerconfig.CoverageTypeHeader.ToString());
                this.CreateAndSetElement(objRootNode, "UnitHeader", m_Headerconfig.UnitHeader.ToString());
                this.CreateAndSetElement(objRootNode, "LossTypeHeader", m_Headerconfig.LossTypeHeader.ToString());
                this.CreateAndSetElement(objRootNode, "ManualCheckHeader", m_Headerconfig.ManualCheckHeader.ToString());
                this.CreateAndSetElement(objRootNode, "OffsetHeader", m_Headerconfig.OffsetHeader.ToString()); //igupta3 Mits# 34515
                this.CreateAndSetElement(objRootNode, "CheckTotalHeader", m_Headerconfig.CheckTotalHeader.ToString()); //Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                this.CreateAndSetElement(objRootNode, "CheckMemoHeader", m_Headerconfig.CheckMemoHeader.ToString()); //Ashish Ahuja Mits 34270 
                this.CreateAndSetElement(objRootNode, "HoldReasonHeader", m_Headerconfig.HoldReasonHeader.ToString());//sachin 7810
                this.CreateAndSetElement(objRootNode, "InvoiceAmountHeader", m_Headerconfig.InvoiceAmountHeader.ToString()); // Mits 25726
				this.CreateAndSetElement(objRootNode, "ReserveTypeHeader", m_Headerconfig.ReserveTypeHeader);
                

                
            }
            catch (RMAppException p_objException)
            {
                this.CreateAndSetElement(objRootNode, "ErrorStatus", Boolean.FalseString);//asharma326 MITS 34544 
                return objOutDoc;
             //   throw p_objException;
            }
            catch (Exception p_objException)
            {
                //throw new RMAppException(Globalization.GetString("HeaderConfig.GetUserPreference.Err"), p_objException);
                this.CreateAndSetElement(objRootNode, "ErrorStatus", Boolean.FalseString);//asharma326 MITS 34544 
                return objOutDoc;
            }
            finally
            {
                objRootNode = null;
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }
            }
            return objOutDoc;
        }
        private string GetValue(XmlDocument p_objDocument, string p_sNodeName)
        {
            XmlElement objNode = null;
            string sValue = "";

            try
            {
                objNode = (XmlElement)p_objDocument.SelectSingleNode("//" + p_sNodeName);

                if (objNode != null)
                    sValue = objNode.InnerText;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("HeaderConfig.GetValue.Err",m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
            return (sValue);
        }
        public void SetHeaderConfig(XmlDocument p_objInputDoc)
        {
            XmlDocument objUserPrefXML = null;
            DbReader objReader = null;
            string sUserPrefsXML = "";
            string sSQL = "";

            try
            {
                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + m_iUserId.ToString();
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                //For Diary Configuration
                if (p_objInputDoc.SelectSingleNode("//DiaryConfig/@selected").Value == "1")
                {
                    m_Headerconfig.Priority = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Priority"));
                    m_Headerconfig.Text = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Text"));
                    m_Headerconfig.AttachedRecord = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/AttachedRecord"));
                    //Added by Indu
                    m_Headerconfig.ParentRecord = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/ParentRecord"));
                    m_Headerconfig.Claimant = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Claimant"));
                    m_Headerconfig.ClaimStatus = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/ClaimStatus"));
                    m_Headerconfig.Department = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Department"));
                    m_Headerconfig.Due = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/Due"));
                    m_Headerconfig.WorkActivity = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/WorkActivity"));
                    //igupta3 jira 439
                    m_Headerconfig.NotRoutable = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/NotRoutable"));
                    m_Headerconfig.NotRollable = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/NotRollable"));

                    m_Headerconfig.PriorityHeader = this.GetValue(p_objInputDoc, "DiaryConfig/PriorityHeader").ToString();
                    m_Headerconfig.TextHeader = this.GetValue(p_objInputDoc, "DiaryConfig/TextHeader").ToString();
                    m_Headerconfig.AttachedRecordHeader = this.GetValue(p_objInputDoc, "DiaryConfig/AttachedRecordHeader").ToString();
                    //Added by Indu
                    m_Headerconfig.ParentRecordHeader = this.GetValue(p_objInputDoc, "DiaryConfig/ParentRecordHeader").ToString();
                    m_Headerconfig.ClaimantHeader = this.GetValue(p_objInputDoc, "DiaryConfig/ClaimantHeader").ToString();
                    m_Headerconfig.ClaimStatusHeader = this.GetValue(p_objInputDoc, "DiaryConfig/ClaimStatusHeader").ToString();
                    m_Headerconfig.DepartmentHeader = this.GetValue(p_objInputDoc, "DiaryConfig/DepartmentHeader").ToString();
                    m_Headerconfig.DueHeader = this.GetValue(p_objInputDoc, "DiaryConfig/DueHeader").ToString();
                    m_Headerconfig.WorkActivityHeader = this.GetValue(p_objInputDoc, "DiaryConfig/WorkActivityHeader").ToString();
                    //igupta3 jira 439
                    m_Headerconfig.NotRoutableHeader = this.GetValue(p_objInputDoc, "DiaryConfig/NotRoutableHeader").ToString();
                    m_Headerconfig.NotRollableHeader = this.GetValue(p_objInputDoc, "DiaryConfig/NotRollableHeader").ToString();
                    m_Headerconfig.AssignedUser = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "DiaryConfig/AssignedUser"));
                    m_Headerconfig.AssigningUser = Common.Conversion.ConvertStrToBool((this.GetValue(p_objInputDoc, "DiaryConfig/AssigningUser").ToString()));
                    m_Headerconfig.AssigningUserHeader = this.GetValue(p_objInputDoc, "DiaryConfig/AssigningUserHeader").ToString();
                    m_Headerconfig.AssignedUserHeader = this.GetValue(p_objInputDoc, "DiaryConfig/AssignedUserHeader").ToString();
                }

                //For Progress Notes Configuration
                if (p_objInputDoc.SelectSingleNode("//ProgNotesConfig/@selected").Value == "1")
                {
                    m_Headerconfig.Activity = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/Activity"));
                    m_Headerconfig.NoteType = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/NoteType"));
                    m_Headerconfig.NoteText = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/NoteText"));
                    m_Headerconfig.AttachedTo = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/AttachedTo"));
                    m_Headerconfig.EnteredBy = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/EnteredBy"));
                    m_Headerconfig.DateCreated = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/DateCreated"));
                    m_Headerconfig.TimeCreated = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/TimeCreated"));
                    m_Headerconfig.UserType = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/UserType"));
                    m_Headerconfig.Subject = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "ProgNotesConfig/Subject"));

                    m_Headerconfig.ActivityHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/ActivityHeader").ToString();
                    m_Headerconfig.NoteTypeHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/NoteTypeHeader").ToString();
                    m_Headerconfig.NoteTextHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/NoteTextHeader").ToString();
                    m_Headerconfig.AttachedToHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/AttachedToHeader").ToString();
                    m_Headerconfig.EnteredByHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/EnteredByHeader").ToString();
                    m_Headerconfig.DateCreatedHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/DateCreatedHeader").ToString();
                    m_Headerconfig.TimeCreatedHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/TimeCreatedHeader").ToString();
                    m_Headerconfig.UserTypeHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/UserTypeHeader").ToString();
                    m_Headerconfig.SubjectHeader = this.GetValue(p_objInputDoc, "ProgNotesConfig/SubjectHeader").ToString();
                }
                //For Payment History Configuration

                if (p_objInputDoc.SelectSingleNode("//PayHisConfig/@selected").Value == "1")
                {
                    m_Headerconfig.Control = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Control"));
                    m_Headerconfig.Check = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Check"));
                    m_Headerconfig.TransDate = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/TransDate"));
                    m_Headerconfig.Type = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Type"));
                    m_Headerconfig.Cleared = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Cleared"));
                    m_Headerconfig.Void = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Void"));
                    
                    //Added by swati for MITS # 33431
                    m_Headerconfig.StopPay = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/StopPay"));
                    //change end here by swati

                    m_Headerconfig.Status = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Status"));
                    m_Headerconfig.Payee = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Payee"));
                    m_Headerconfig.CheckAmount = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/CheckAmount"));
                    m_Headerconfig.FromDate = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/FromDate"));
                    m_Headerconfig.ToDate = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/ToDate"));
                    m_Headerconfig.Invoice = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Invoice"));
                    m_Headerconfig.TransactionType = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/TransactionType"));
                    m_Headerconfig.SplitAmount = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/SplitAmount"));
                    m_Headerconfig.User = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/User"));
                    m_Headerconfig.CheckDate = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/CheckDate"));
                    m_Headerconfig.CombinedPay = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/CombinedPay"));

                    //Deb
                    m_Headerconfig.IsFirstFinal = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/IsFirstFinal"));
                    m_Headerconfig.PolicyName = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/PolicyName"));
                    m_Headerconfig.CoverageType = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/CoverageType"));
                    m_Headerconfig.Unit = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Unit"));
                    m_Headerconfig.LossType = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/LossType"));
                    m_Headerconfig.ManualCheck = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/ManualCheck"));
                    m_Headerconfig.Offset = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/Offset")); //igupta3 Mits# 34515
                    m_Headerconfig.CheckMemo = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/CheckMemo")); //Ashish Ahuja Mits 34270
                    m_Headerconfig.HoldReason = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/HoldReason")); //sachin 7810
                    m_Headerconfig.InvoiceAmount = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/InvoiceAmount")); //Mits 25726
                    
                    //Added by Nikhil.
                    m_Headerconfig.CheckTotal = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/CheckTotal")); 
                    m_Headerconfig.ReserveType = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "PayHisConfig/ReserveType"));

                    m_Headerconfig.ControlHeader = this.GetValue(p_objInputDoc, "PayHisConfig/ControlHeader").ToString();
                    m_Headerconfig.CheckHeader = this.GetValue(p_objInputDoc, "PayHisConfig/CheckHeader").ToString();
                    m_Headerconfig.TransDateHeader = this.GetValue(p_objInputDoc, "PayHisConfig/TransDateHeader").ToString();
                    m_Headerconfig.TypeHeader = this.GetValue(p_objInputDoc, "PayHisConfig/TypeHeader");
                    m_Headerconfig.ClearedHeader = this.GetValue(p_objInputDoc, "PayHisConfig/ClearedHeader").ToString();
                    m_Headerconfig.VoidHeader = this.GetValue(p_objInputDoc, "PayHisConfig/VoidHeader").ToString();
                    //Added by swati Agarwal for MITS # 33431
                    m_Headerconfig.StopPayHeader = this.GetValue(p_objInputDoc, "PayHisConfig/StopPayHeader").ToString();
                    //change end here by swati
                    m_Headerconfig.StatusHeader = this.GetValue(p_objInputDoc, "PayHisConfig/StatusHeader").ToString();
                    m_Headerconfig.PayeeHeader = this.GetValue(p_objInputDoc, "PayHisConfig/PayeeHeader").ToString();
                    m_Headerconfig.CheckAmountHeader = this.GetValue(p_objInputDoc, "PayHisConfig/CheckAmountHeader").ToString();
                    m_Headerconfig.FromDateHeader = this.GetValue(p_objInputDoc, "PayHisConfig/FromDateHeader").ToString();
                    m_Headerconfig.ToDateHeader = this.GetValue(p_objInputDoc, "PayHisConfig/ToDateHeader").ToString();
                    m_Headerconfig.InvoiceHeader = this.GetValue(p_objInputDoc, "PayHisConfig/InvoiceHeader").ToString();
                    m_Headerconfig.TransactionTypeHeader = this.GetValue(p_objInputDoc, "PayHisConfig/TransactionTypeHeader").ToString();
                    m_Headerconfig.SplitAmountHeader = this.GetValue(p_objInputDoc, "PayHisConfig/SplitAmountHeader").ToString();
                    m_Headerconfig.UserHeader = this.GetValue(p_objInputDoc, "PayHisConfig/UserHeader").ToString();
                    m_Headerconfig.CheckDateHeader = this.GetValue(p_objInputDoc, "PayHisConfig/CheckDateHeader").ToString();
                    m_Headerconfig.CombinedPayHeader = this.GetValue(p_objInputDoc, "PayHisConfig/CombinedPayHeader").ToString();

                    //Deb
                    m_Headerconfig.IsFirstFinalHeader = this.GetValue(p_objInputDoc, "PayHisConfig/IsFirstFinalHeader").ToString();
                    m_Headerconfig.PolicyNameHeader = this.GetValue(p_objInputDoc, "PayHisConfig/PolicyNameHeader").ToString();
                    m_Headerconfig.UnitHeader = this.GetValue(p_objInputDoc, "PayHisConfig/UnitHeader").ToString();
                    m_Headerconfig.LossTypeHeader = this.GetValue(p_objInputDoc, "PayHisConfig/LossTypeHeader").ToString();
                    m_Headerconfig.CoverageTypeHeader = this.GetValue(p_objInputDoc, "PayHisConfig/CoverageTypeHeader").ToString();
                    m_Headerconfig.ManualCheckHeader = this.GetValue(p_objInputDoc, "PayHisConfig/ManualCheckHeader").ToString();
                    m_Headerconfig.OffsetHeader = this.GetValue(p_objInputDoc, "PayHisConfig/OffsetHeader").ToString();
                    //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    m_Headerconfig.CheckTotalHeader = this.GetValue(p_objInputDoc, "PayHisConfig/CheckTotalHeader").ToString();
                    m_Headerconfig.CheckMemoHeader = this.GetValue(p_objInputDoc, "PayHisConfig/CheckMemoHeader").ToString();
                    m_Headerconfig.HoldReasonHeader = this.GetValue(p_objInputDoc, "PayHisConfig/HoldReasonHeader").ToString();//sachin 7810
                    m_Headerconfig.InvoiceAmountHeader = this.GetValue(p_objInputDoc, "PayHisConfig/InvoiceAmountHeader").ToString();
                    m_Headerconfig.ReserveTypeHeader = this.GetValue(p_objInputDoc, "PayHisConfig/ReserveTypeHeader").ToString();
               }
                CreateUserPreferXML(objUserPrefXML);

                //For Diary Configuration
                XmlNode objNode;
                if (p_objInputDoc.SelectSingleNode("//DiaryConfig/@selected").Value == "1")
                {

                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Priority/@selected");
                    objNode.Value = m_Headerconfig.Priority ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Text/@selected");
                    objNode.Value = m_Headerconfig.Text ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecord/@selected");
                    objNode.Value = m_Headerconfig.AttachedRecord ? "1" : "0";
                    //Added by Indu
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecord/@selected");
                    objNode.Value = m_Headerconfig.ParentRecord ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Claimant/@selected");
                    objNode.Value = m_Headerconfig.Claimant ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatus/@selected");
                    objNode.Value = m_Headerconfig.ClaimStatus ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Department/@selected");
                    objNode.Value = m_Headerconfig.Department ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/Due/@selected");
                    objNode.Value = m_Headerconfig.Due ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivity/@selected");
                    objNode.Value = m_Headerconfig.WorkActivity ? "1" : "0";
                    //igupta3 jira 439
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutable/@selected");
                    objNode.Value = m_Headerconfig.NotRoutable ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollable/@selected");
                    objNode.Value = m_Headerconfig.NotRollable ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/PriorityHeader/@value");
                    objNode.Value = m_Headerconfig.PriorityHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/TextHeader/@value");
                    objNode.Value = m_Headerconfig.TextHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AttachedRecordHeader/@value");
                    objNode.Value = m_Headerconfig.AttachedRecordHeader;
                    //Added by Indu
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ParentRecordHeader/@value");
                    objNode.Value = m_Headerconfig.ParentRecordHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimantHeader/@value");
                    objNode.Value = m_Headerconfig.ClaimantHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/ClaimStatusHeader/@value");
                    objNode.Value = m_Headerconfig.ClaimStatusHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DepartmentHeader/@value");
                    objNode.Value = m_Headerconfig.DepartmentHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DueHeader/@value");
                    objNode.Value = m_Headerconfig.DueHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/WorkActivityHeader/@value");
                    objNode.Value = m_Headerconfig.WorkActivityHeader;
                    //igupta3 jira 439
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRoutableHeader/@value");
                    objNode.Value = m_Headerconfig.NotRoutableHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/NotRollableHeader/@value");
                    objNode.Value = m_Headerconfig.NotRollableHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUser/@selected");
                    objNode.Value = m_Headerconfig.AssignedUser ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUser/@selected");
                    objNode.Value = m_Headerconfig.AssigningUser ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssignedUserHeader/@value");
                    objNode.Value = m_Headerconfig.AssignedUserHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/AssigningUserHeader/@value");
                    objNode.Value = m_Headerconfig.AssigningUserHeader;
                }
                //For Progress Notes Configuration
                if (p_objInputDoc.SelectSingleNode("//ProgNotesConfig/@selected").Value == "1")
                {
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/Activity/@selected");
                    objNode.Value = m_Headerconfig.Activity ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteType/@selected");
                    objNode.Value = m_Headerconfig.NoteType ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteText/@selected");
                    objNode.Value = m_Headerconfig.NoteText ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/AttachedTo/@selected");
                    objNode.Value = m_Headerconfig.AttachedTo ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/EnteredBy/@selected");
                    objNode.Value = m_Headerconfig.EnteredBy ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/DateCreated/@selected");
                    objNode.Value = m_Headerconfig.DateCreated ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/TimeCreated/@selected");
                    objNode.Value = m_Headerconfig.TimeCreated ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/UserType/@selected");
                    objNode.Value = m_Headerconfig.UserType ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/Subject/@selected");
                    objNode.Value = m_Headerconfig.Subject ? "1" : "0";

                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/ActivityHeader/@value");
                    objNode.Value = m_Headerconfig.ActivityHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteTypeHeader/@value");
                    objNode.Value = m_Headerconfig.NoteTypeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/NoteTextHeader/@value");
                    objNode.Value = m_Headerconfig.NoteTextHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/AttachedToHeader/@value");
                    objNode.Value = m_Headerconfig.AttachedToHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/EnteredByHeader/@value");
                    objNode.Value = m_Headerconfig.EnteredByHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/DateCreatedHeader/@value");
                    objNode.Value = m_Headerconfig.DateCreatedHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/TimeCreatedHeader/@value");
                    objNode.Value = m_Headerconfig.TimeCreatedHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/UserTypeHeader/@value");
                    objNode.Value = m_Headerconfig.UserTypeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/ProgNotesConfig/SubjectHeader/@value");
                    objNode.Value = m_Headerconfig.SubjectHeader;
                }
                //For Payment History Configuration
                if (p_objInputDoc.SelectSingleNode("//PayHisConfig/@selected").Value == "1")
                {
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Control/@selected");
                    objNode.Value = m_Headerconfig.Control ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Check/@selected");
                    objNode.Value = m_Headerconfig.Check ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransDate/@selected");
                    objNode.Value = m_Headerconfig.TransDate ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Type/@selected");
                    objNode.Value = m_Headerconfig.Type ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Cleared/@selected");
                    objNode.Value = m_Headerconfig.Cleared ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Void/@selected");
                    objNode.Value = m_Headerconfig.Void ? "1" : "0";
                    //Added by swati for WWIG Gap 9 MITS# 33431
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/StopPay/@selected");
                    objNode.Value = m_Headerconfig.StopPay ? "1" : "0";
                    //change end here by swati
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Status/@selected");
                    objNode.Value = m_Headerconfig.Status ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Payee/@selected");
                    objNode.Value = m_Headerconfig.Payee ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckAmount/@selected");
                    objNode.Value = m_Headerconfig.CheckAmount ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/FromDate/@selected");
                    objNode.Value = m_Headerconfig.FromDate ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ToDate/@selected");
                    objNode.Value = m_Headerconfig.ToDate ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Invoice/@selected");
                    objNode.Value = m_Headerconfig.Invoice ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransactionType/@selected");
                    objNode.Value = m_Headerconfig.TransactionType ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/SplitAmount/@selected");
                    objNode.Value = m_Headerconfig.SplitAmount ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/User/@selected");
                    objNode.Value = m_Headerconfig.User ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckDate/@selected");
                    objNode.Value = m_Headerconfig.CheckDate ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CombinedPay/@selected");
                    objNode.Value = m_Headerconfig.CombinedPay ? "1" : "0";

                    //Deb
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/IsFirstFinal/@selected");
                    objNode.Value = m_Headerconfig.IsFirstFinal ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/PolicyName/@selected");
                    objNode.Value = m_Headerconfig.PolicyName ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CoverageType/@selected");
                    objNode.Value = m_Headerconfig.CoverageType ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Unit/@selected");
                    objNode.Value = m_Headerconfig.Unit ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/LossType/@selected");
                    objNode.Value = m_Headerconfig.LossType ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ManualCheck/@selected");
                    objNode.Value = m_Headerconfig.ManualCheck ? "1" : "0";
                    //igupta3 Mits# 34515
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/Offset/@selected");
                    objNode.Value = m_Headerconfig.Offset ? "1" : "0";

                    //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckTotal/@selected");
                    objNode.Value = m_Headerconfig.CheckTotal ? "1" : "0";
                    //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration

                     //Ashish Ahuja Mits 34270
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckMemo/@selected");
                    objNode.Value = m_Headerconfig.CheckMemo ? "1" : "0";

                    //sachin 7810 starts
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/HoldReason/@selected");
                    objNode.Value = m_Headerconfig.HoldReason ? "1" : "0";
                    //sachin 7810 ends
                    // Mits 25726
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/InvoiceAmount/@selected");
                    objNode.Value = m_Headerconfig.InvoiceAmount ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ReserveType/@selected");
                    objNode.Value = m_Headerconfig.ReserveType ? "1" : "0";

                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ControlHeader/@value");
                    objNode.Value = m_Headerconfig.ControlHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckHeader/@value");
                    objNode.Value = m_Headerconfig.CheckHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransDateHeader/@value");
                    objNode.Value = m_Headerconfig.TransDateHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TypeHeader/@value");
                    objNode.Value = m_Headerconfig.TypeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ClearedHeader/@value");
                    objNode.Value = m_Headerconfig.ClearedHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/VoidHeader/@value");
                    objNode.Value = m_Headerconfig.VoidHeader;
                    //Added by swati for WWIG Gap 9 MITS# 33431
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/StopPayHeader/@value");
                    objNode.Value = m_Headerconfig.StopPayHeader;
                    //change end here by swati
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/StatusHeader/@value");
                    objNode.Value = m_Headerconfig.StatusHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/PayeeHeader/@value");
                    objNode.Value = m_Headerconfig.PayeeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckAmountHeader/@value");
                    objNode.Value = m_Headerconfig.CheckAmountHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/FromDateHeader/@value");
                    objNode.Value = m_Headerconfig.FromDateHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ToDateHeader/@value");
                    objNode.Value = m_Headerconfig.ToDateHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/InvoiceHeader/@value");
                    objNode.Value = m_Headerconfig.InvoiceHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/TransactionTypeHeader/@value");
                    objNode.Value = m_Headerconfig.TransactionTypeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/SplitAmountHeader/@value");
                    objNode.Value = m_Headerconfig.SplitAmountHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/UserHeader/@value");
                    objNode.Value = m_Headerconfig.UserHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckDateHeader/@value");
                    objNode.Value = m_Headerconfig.CheckDateHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CombinedPayHeader/@value");
                    objNode.Value = m_Headerconfig.CombinedPayHeader;

                    //Deb
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/IsFirstFinalHeader/@value");
                    objNode.Value = m_Headerconfig.IsFirstFinalHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/PolicyNameHeader/@value");
                    objNode.Value = m_Headerconfig.PolicyNameHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CoverageTypeHeader/@value");
                    objNode.Value = m_Headerconfig.CoverageTypeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/UnitHeader/@value");
                    objNode.Value = m_Headerconfig.UnitHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/LossTypeHeader/@value");
                    objNode.Value = m_Headerconfig.LossTypeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ManualCheckHeader/@value");
                    objNode.Value = m_Headerconfig.ManualCheckHeader;
                    //igupta3 Mits# 34515
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/OffsetHeader/@value");
                    objNode.Value = m_Headerconfig.OffsetHeader;
                    //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckTotalHeader/@value");
                    objNode.Value = m_Headerconfig.CheckTotalHeader;
                    //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration


					//Ashish Ahuja Mits 34270
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/CheckMemoHeader/@value");
                    objNode.Value = m_Headerconfig.CheckMemoHeader;

                    //sachin 7810 starts
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/HoldReasonHeader/@value");
                    objNode.Value = m_Headerconfig.HoldReasonHeader;
                    //sachin 7810 ends

                    // Mits 25726
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/InvoiceAmountHeader/@value");
                    objNode.Value = m_Headerconfig.InvoiceAmountHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/PayHisConfig/ReserveTypeHeader/@value");
                    objNode.Value = m_Headerconfig.ReserveTypeHeader;
                }
                this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(m_iUserId));
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("HeaderConfig.SaveUserPreference.Err",m_iClientId), p_objException);
            }
            finally
            {
                objUserPrefXML = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                    objReader = null;
                }

            }
        }
        private void SaveUserPrefersXmlToDb(string p_sUserPrefXml, int p_iUserId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            string sSQL = string.Empty;

            try
            {
                //Try to delete the old one.
                sSQL = "DELETE FROM USER_PREF_XML WHERE USER_ID = " + p_iUserId;
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.Close();

                objWriter = DbFactory.GetDbWriter(m_sConnectionString);
                objWriter.Tables.Add("USER_PREF_XML");
                objWriter.Fields.Add("USER_ID", p_iUserId);
                objWriter.Fields.Add("PREF_XML", p_sUserPrefXml);
                objWriter.Execute();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPrefersXmlToDb.Err",m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
            }
        }

        private void CreateUserPreferXML(XmlDocument p_objUserPrefXML)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }

                // Create Config node if Not present.
                //objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/Config");
                //if (objTempNode == null)
                //{
                //    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                //    objTempNode = p_objUserPrefXML.CreateElement("Config");
                //    objParentNode.AppendChild(objTempNode);
                //}

                // Create DiaryConfig node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting");
                    objTempNode = p_objUserPrefXML.CreateElement("DiaryConfig");
                    objParentNode.AppendChild(objTempNode);
                }
                // Create "setting/Config/DiaryConfig/ShowActiveDiaryChecked" node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ShowActiveDiaryChecked");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ShowActiveDiaryChecked");
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }

                //tkr 6/22/2007 add "setting/Config/DiaryConfig/ShowNotes"
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ShowNotes");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ShowNotes");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config - start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/Priority");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Priority");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/Text");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Text");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/WorkActivity");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("WorkActivity");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //igupta3 jira 439
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/NotRoutable");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRoutable");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/NotRollable");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRollable");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/Due");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Due");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/Claimant");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Claimant");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ClaimStatus");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimStatus");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/Department");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Department");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/AttachedRecord");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedRecord");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //Added by Indu
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ParentRecord");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ParentRecord");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/PriorityHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PriorityHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Priority");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/TextHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TextHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Task Name");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/WorkActivityHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("WorkActivityHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Work Activity");
                    objParentNode.AppendChild(objTempNode);
                }
                //igupta3 jira 439
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/NotRoutableHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRoutableHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Not Routable");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/NotRollableHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NotRollableHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Not Rollable");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/DueHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DueHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Due");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ClaimantHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimantHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Claimant");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ClaimStatusHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimStatusHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Claim Status");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/DepartmentHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DepartmentHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Department");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/AttachedRecordHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedRecordHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Attached Record");
                    objParentNode.AppendChild(objTempNode);
                }
                //Added by Indu
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ParentRecordHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ParentRecordHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Parent Record");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ColspanValue");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ColspanValue");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //SAFEWAY: paggarwal2 : 11/08/2008 : Changes for the Diary Header Config - end
                //SAFEWAY: paggarwal2: 04/11/2008: Added for Diary Screens - start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/CalendarView");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CalendarView");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //SAFEWAY: paggarwal2: 04/11/2008: Added for Diary Screens - end

                //Neha Mits 23477 05/04/2011--Saved user sort order for diary listing screen.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/UserSortOrder");

                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserSortOrder");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "0");
                    objParentNode.AppendChild(objTempNode);
                }
                //Neha End Mits 23477
                //Added by Amitosh for Terelik Grid

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/ShowRegarding");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ShowRegarding");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //End Amitosh


                //Added by Amitosh for Diary UI Change
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/AssigningUser");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssignedUser");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/AssigningUser");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssigningUser");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/AssignedUserHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssignedUserHeader");
                    objTempNode.SetAttribute("value", "Assigned User");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/AssigningUserHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AssigningUserHeader");
                    objTempNode.SetAttribute("value", "Assigning User");
                    objParentNode.AppendChild(objTempNode);
                }
                //End Amitosh

                //For Progress Notes Configuration

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting");
                    objTempNode = p_objUserPrefXML.CreateElement("ProgNotesConfig");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/Activity");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Activity");
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteText");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/AttachedTo");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedTo");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/EnteredBy");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("EnteredBy");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/DateCreated");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateCreated");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/TimeCreated");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TimeCreated");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/UserType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/Subject");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Subject");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/ActivityHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ActivityHeader");
                    objTempNode.SetAttribute("value", "Activity");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteTypeHeader");
                    objTempNode.SetAttribute("value", "Note Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/NoteTextHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("NoteTextHeader");
                    objTempNode.SetAttribute("value", "Note Text");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/AttachedToHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("AttachedToHeader");
                    objTempNode.SetAttribute("value", "Attached To");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/EnteredByHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("EnteredByHeader");
                    objTempNode.SetAttribute("value", "Entered By");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/DateCreatedHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateCreatedHeader");
                    objTempNode.SetAttribute("value", "Date Created");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/TimeCreatedHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TimeCreatedHeader");
                    objTempNode.SetAttribute("value", "Time Created");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/UserTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserTypeHeader");
                    objTempNode.SetAttribute("value", "User Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig/SubjectHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/ProgNotesConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SubjectHeader");
                    objTempNode.SetAttribute("value", "Subject");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting");
                    objTempNode = p_objUserPrefXML.CreateElement("PayHisConfig");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Control");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Control");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Check");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Check");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Type");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Type");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Cleared");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Cleared");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Void");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Void");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //added by swati agarwal for wwig gap 9 mits # 33431
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/StopPay");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("StopPay");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //end by swati
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Status");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Status");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Payee");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Payee");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckAmount");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckAmount");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/FromDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("FromDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ToDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ToDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Invoice");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Invoice");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransactionType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransactionType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/SplitAmount");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SplitAmount");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/User");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("User");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckDate");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CombinedPay");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CombinedPay");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                //Deb
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/IsFirstFinal");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("IsFirstFinal");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/PolicyName");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PolicyName");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CoverageType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CoverageType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Unit");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Unit");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/LossType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("LossType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ManualCheck");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ManualCheck");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //igupta3 Mits# 34515
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/Offset");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Offset");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckTotal");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckTotal");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration
				
				
				//Ashish Ahuja Mits 34270 start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckMemo");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckMemo");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //Ashish Ahuja Mits 34270 end

                //sachin 7810 starts
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/HoldReason");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("HoldReason");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                //sachin 7810 ends

                //25726 start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/InvoiceAmount");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("InvoiceAmount");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ReserveType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ReserveType");
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ReserveTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ReserveTypeHeader");
                    objTempNode.SetAttribute("value", "Reserve Type");
                    objParentNode.AppendChild(objTempNode);
                }
                // Mits 25726 end
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ControlHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ControlHeader");
                    objTempNode.SetAttribute("value", "Control #");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckHeader");
                    objTempNode.SetAttribute("value", "Check #");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransDateHeader");
                    objTempNode.SetAttribute("value", "Trans Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TypeHeader");
                    objTempNode.SetAttribute("value", "Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ClearedHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClearedHeader");
                    objTempNode.SetAttribute("value", "Cleared");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/VoidHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("VoidHeader");
                    objTempNode.SetAttribute("value", "Void");
                    objParentNode.AppendChild(objTempNode);
                }
                //added by swati agarwal for wwig gap 9 mits # 33431
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/StopPayHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("StopPayHeader");
                    objTempNode.SetAttribute("value", "StopPay");
                    objParentNode.AppendChild(objTempNode);
                }
                //end by swati
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/StatusHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("StatusHeader");
                    objTempNode.SetAttribute("value", "Status");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/PayeeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PayeeHeader");
                    objTempNode.SetAttribute("value", "Payee");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckAmountHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckAmountHeader");
                    objTempNode.SetAttribute("value", "Check Amount");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/FromDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("FromDateHeader");
                    objTempNode.SetAttribute("value", "From Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ToDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ToDateHeader");
                    objTempNode.SetAttribute("value", "To Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/InvoiceHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("InvoiceHeader");
                    objTempNode.SetAttribute("value", "Invoice #");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/TransactionTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("TransactionTypeHeader");
                    objTempNode.SetAttribute("value", "Transaction Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/SplitAmountHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("SplitAmountHeader");
                    objTempNode.SetAttribute("value", "Split Amount");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/UserHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UserHeader");
                    objTempNode.SetAttribute("value", "User");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckDateHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckDateHeader");
                    objTempNode.SetAttribute("value", "Check Date");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CombinedPayHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CombinedPayHeader");
                    objTempNode.SetAttribute("value", "Combined Pay");
                    objParentNode.AppendChild(objTempNode);
                }

                //Deb
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/IsFirstFinalHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("IsFirstFinalHeader");
                    objTempNode.SetAttribute("value", "IsFirstFinal");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/PolicyNameHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("PolicyNameHeader");
                    objTempNode.SetAttribute("value", "Policy Name");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CoverageTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CoverageTypeHeader");
                    objTempNode.SetAttribute("value", "Coverage Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/UnitHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("UnitHeader");
                    objTempNode.SetAttribute("value", "Unit");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/LossTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("LossTypeHeader");
                    objTempNode.SetAttribute("value", "Loss Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/ManualCheckHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ManualCheckHeader");
                    objTempNode.SetAttribute("value", "Manual Check");
                    objParentNode.AppendChild(objTempNode);
                }
                //igupta3 Mits# 34515
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/OffsetHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("OffsetHeader");
                    objTempNode.SetAttribute("value", "Offset");
                    objParentNode.AppendChild(objTempNode);
                }

                //Added by Nikhil on 07/29/14.Check Total  in payment history configuration
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckTotalHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckTotalHeader");
                    objTempNode.SetAttribute("value", "CheckTotal");
                    objParentNode.AppendChild(objTempNode);
                }
                //End - Added by Nikhil on 07/29/14.Check Total  in payment history configuration

				
				//Ashish Ahuja Mits 34270 start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/CheckMemoHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("CheckMemoHeader");
                    objTempNode.SetAttribute("value", "CheckMemo");
                    objParentNode.AppendChild(objTempNode);
                }
                //Ashish Ahuja Mits 34270 end

                //sachin 7810 starts
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/HoldReasonHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("HoldReasonHeader");
                    objTempNode.SetAttribute("value", "HoldReason");
                    objParentNode.AppendChild(objTempNode);
                }
                //sachin 7810 ends

                // Mits 25726 start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig/InvoiceAmountoHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/PayHisConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("InvoiceAmountHeader");
                    objTempNode.SetAttribute("value", "InvoiceAmount");
                    objParentNode.AppendChild(objTempNode);
                }
                // Mits 25726 end
                //agupta298 - WWIG GAP20 Start
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig/DiarySupplemental");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("setting/DiaryConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DiarySupplemental");
                    objParentNode.AppendChild(objTempNode);
                }
                //agupta298 - WWIG GAP20 End
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("WPA.CreateUserPreferXML.Err",m_iClientId), p_objException);
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }

        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateAndSetElement.Err",m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Create and Set the Text of the New Node.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Inner Text.</param>
        /// <param name="p_objChildNode">Child Node Refrence</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateAndSetElement.Err",m_iClientId), p_objEx);
            }
        }

        ///// <summary>
        ///// Create and Set the Text of the New Node.
        ///// </summary>
        ///// <param name="p_objParentNode">Parent node</param>
        ///// <param name="p_sNodeName">Node Name</param>
        ///// <param name="p_sText">Inner Text.</param>
        ///// <param name="p_objChildNode">Child Node Refrence</param>

        ///// <summary>
        ///// Create new Element in the Xml Document.
        ///// </summary>
        ///// <param name="p_objParentNode">Parent node</param>
        ///// <param name="p_sNodeName">Node Name</param>
        ///// <param name="p_objChildNode">Refrence to the New Node Created.</param>
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("HeaderConfig.CreateElement.Err",m_iClientId), p_objEx);
            }
        }

        //WWIG GAP20A - agupta298 - MITS 36804 - Start - JIRA - 4691
        /// <summary>
        /// Getting Diary Supplemental HeaderConfig from USER_PREF_XML
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public XmlDocument GetDiarySuppHeaderConfig(XmlDocument p_objXMLDocument)
        {
            StringBuilder sbSql = null;
            DbReader objReader = null;
            XmlElement objSelectElement = null;
            XmlElement objListElement = null;
            XmlElement objRowTxt = null;
            string sUserPrefsXML = string.Empty;
            XmlDocument objUserPrefXML = null;
            XmlElement objUserPrefElement = null;
            try
            {

                //Get WPA Diary Supplemental Field Option
                objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//DiarySupplemental/WPADiarySuppFields");
                sbSql = new StringBuilder();
                sbSql.Append("SELECT USER_PROMPT , SYS_FIELD_NAME , FIELD_TYPE , FIELD_ID FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME = 'WPA_DIARY_ENTRY_SUPP' and FIELD_TYPE NOT IN (" + (int)SupplementalFieldTypes.SuppTypePrimaryKey + "," + (int)SupplementalFieldTypes.SuppTypeSecondaryKey + "," + (int)SupplementalFieldTypes.SuppTypeGrid + ",");
                sbSql.Append((int)SupplementalFieldTypes.SuppTypeMultiCode + "," + (int)SupplementalFieldTypes.SuppTypeMultiEntity + "," + (int)SupplementalFieldTypes.SuppTypeMultiState + "," + (int)SupplementalFieldTypes.SuppTypeUserLookup);
                sbSql.Append(") and DELETE_FLAG <> -1 "); 
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());
                if (objReader != null)
                {
                    while (objReader.Read())
                    {
                        objRowTxt = p_objXMLDocument.CreateElement("FormFieldOption");
                        objRowTxt.SetAttribute("value", objReader["SYS_FIELD_NAME"].ToString() + "|" + objReader["FIELD_TYPE"].ToString() + "|" + objReader["FIELD_ID"].ToString());
                        objRowTxt.InnerText = objReader["USER_PROMPT"].ToString();
                        objSelectElement.AppendChild((XmlNode)objRowTxt);
                    }
                    if (objReader != null)
                        objReader.Dispose();
                }

                //Get WPA Diary Supplemental Caption Option
                sbSql = new StringBuilder();
                sbSql.Append("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + m_iUserId);
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                    objUserPrefElement = (XmlElement)objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DiarySupplemental");

                    objListElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//DiarySupplemental/WPADiarySuppFieldsCaption");

                    if (objUserPrefElement != null)
                    {
                        foreach (XmlNode objNode in objUserPrefElement)
                        {
                            if (objNode != null && objNode.Attributes != null)
                            {
                                objRowTxt = p_objXMLDocument.CreateElement("FormListOption");
                                objRowTxt.SetAttribute("value", objNode.Attributes["value"].Value + "|" + objNode.Attributes["type"].Value + "|" + objNode.Attributes["field_id"].Value);
                                objRowTxt.InnerText = objNode.InnerText;
                                objListElement.AppendChild((XmlNode)objRowTxt);
                            }
                        }
                    }
                }

                return p_objXMLDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HeaderConfig.GetDiarySuppHeaderConfig.Error"), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                p_objXMLDocument = null;
                objUserPrefXML = null;
                objSelectElement = null;
                objListElement = null;
                objUserPrefElement = null;
            }
        }

        /// <summary>
        /// Saving the Diary Supplemental Header Config in USER_PREF_XML table.
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <param name="p_objXmlOut"></param>
        /// <param name="p_objErrOut"></param>
        /// <returns></returns>
        public XmlDocument SaveDiarySuppHeaderConfig(XmlDocument p_objXMLDocument)
        {
            StringBuilder sbSql = null;
            DbReader objReader = null;
            XmlElement objListElement = null;
            string sUserPrefsXML = string.Empty;
            XmlDocument objUserPrefXML = null;

            try
            {
                sbSql = new StringBuilder();
                sbSql.Append("SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + m_iUserId);
                objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString());

                if (objReader.Read())
                    sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                objReader.Close();
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();

                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);

                    CreateUserPreferXML(objUserPrefXML);

                    objListElement = (XmlElement)objUserPrefXML.SelectSingleNode("//setting/DiaryConfig/DiarySupplemental");

                    if (objListElement != null)
                    {
                        objListElement.RemoveAll();

                        XmlNodeList tempNodes = p_objXMLDocument.SelectNodes("DiarySupplemental/option");
                        if (tempNodes != null && tempNodes.Count > 0)
                            foreach (XmlNode objNode in tempNodes)
                            {
                                //necessary for crossing XmlDocument contexts
                                XmlNode importNode = objUserPrefXML.ImportNode(objNode, false);
                                importNode.InnerText = objNode.InnerText;
                                objListElement.AppendChild(importNode);
                            }
                    }

                    this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(m_iUserId));
                }
                return p_objXMLDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("HeaderConfig.SaveDiarySuppHeaderConfig.Error"), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                p_objXMLDocument = null;
                objUserPrefXML = null;
                objListElement = null;
            }
        }
        //WWIG GAP20A - agupta298 - MITS 36804 - End - JIRA - 4691
    }
}
