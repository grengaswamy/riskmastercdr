﻿
using System;
using System.Xml;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Neha Sharma
    ///Dated   :   30th May,2011
    ///Purpose :   Contains the methods for Code Relationships.
    /// </summary>
    public class CodeRelationships:UtilitiesBase
    {
        private string m_sDBOUserId = "";
        private int m_iLangCode = 0;
        private int m_iClientId = 0;//Add by kuladeep for Cloud Jira-69
        public CodeRelationships(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		    {
                 
			    ConnectString = p_sConnString;
                m_iClientId = p_iClientId;//Add by kuladeep for Cloud Jira-69
			    this.Initialize(); 
		    }

        public CodeRelationships(string p_sConnString, string p_sUserName, int p_iClientId)
            : base(p_sConnString, p_sUserName, p_iClientId)
            {
                ConnectString = p_sConnString;
                m_iClientId = p_iClientId;//Add by kuladeep for Cloud Jira-69
                m_sDBOUserId = p_sUserName;
                this.Initialize();
            }
        //Aman ML Change
        #region Properties
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion
        //Aman ML Change
        /// <summary>
        /// Gets the data for code Realtionship Load screen
        /// </summary>
        /// <param name="p_objDoc"></param>
        /// <returns>XmlDocument</returns>
        public XmlDocument Get(XmlDocument p_objDoc)
        {

            DbConnection objCon = null;
            LocalCache objCache = null;
            XmlNode objNode = null;
            DbReader objReader = null;
            string sAdjusterList = string.Empty;
            int iTableId = 0;
            int iTableId1 = 0;
            int iTableId2 = 0;
            string sEmailFROIACORD = string.Empty;
            XmlElement objElement = null;
            DataSet objDS = null;
            StringBuilder sbSql = null;
            XmlDocument objXmlDocument = null;
            //int iCLTOLCID =0;
            string sRelationshipId = string.Empty;
            string sSubRelationShipId = string.Empty;
            int isubTableId = 0;
            int iNone=0, iNotDet=0, iDedTypeTable=0;
            try
            {
                using (objCon = DbFactory.GetDbConnection(ConnectString))
                {
                    objCon.Open();
                    objCache = new LocalCache(ConnectString, m_iClientId);
                   
                    iDedTypeTable = objCache.GetTableId("DEDUCTIBLE_TYPE");
                    iNotDet = objCache.GetCodeId("None","DEDUCTIBLE_TYPE");
                    iNone = objCache.GetCodeId("ND","DEDUCTIBLE_TYPE");
                    //added by swati
                    int iStateTableId = objCache.GetTableId("STATES");
                    //change end here by swati

                    objXmlDocument = new XmlDocument();
                    objXmlDocument = p_objDoc;


                    objNode = objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipType']");

                    sRelationshipId = objNode.Attributes["value"].Value;
                            
                    sbSql = new StringBuilder();
                    
                    iTableId = objCache.GetTableId("CODE_REL_TYPE");
                   
                    //sbSql = new StringBuilder();
                    sbSql.Append(" SELECT CODES.CODE_ID,CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                    sbSql.Append(" AND DELETED_FLAG = 0 AND CODES.TABLE_ID =" + iTableId);                   
                    sbSql.Append(" ORDER BY CODES.SHORT_CODE");

                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        objElement = objXmlDocument.CreateElement("option");
                        objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        objElement.InnerText =  Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        objNode.AppendChild(objElement);
                        
                        //if(string.Equals(Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim(),"Claim Type To Loss Component",StringComparison.InvariantCultureIgnoreCase))
                        //iCLTOLCID = Conversion.ConvertObjToInt(dr["CODE_ID"], m_iClientId);
                        //objElement = objXmlDocument.CreateElement("option");
                        //objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"])+"-O");
                        //objElement.InnerText = "Loss Component To Claim Type";
                        //objNode.AppendChild(objElement);
                    }

                    //if(iCLTOLCID >0)
                    //{
                    //objElement = objXmlDocument.CreateElement("option");
                    //    objElement.SetAttribute("value", iCLTOLCID+"-O");
                    //    objElement.InnerText = "Loss Component To Claim Type";
                    //    objNode.AppendChild(objElement);
                    //}
                 

              
                            if( string.Equals(sRelationshipId,""))
                            {
                                 iTableId1 = objCache.GetTableId("CLAIM_TYPE");
                                iTableId2 =  objCache.GetTableId("LOSS_COMPONENT");
                                isubTableId = objCache.GetTableId("CLAIM_AND_LOSS_COMPONENT");
                            }
                            else if(string.Equals(sRelationshipId,objCache.GetCodeId("CTTOLC","CODE_REL_TYPE").ToString(),StringComparison.InvariantCultureIgnoreCase))
                            {
                                 iTableId1 = objCache.GetTableId("CLAIM_TYPE");
                                iTableId2 =  objCache.GetTableId("LOSS_COMPONENT");
                                isubTableId = objCache.GetTableId("CLAIM_AND_LOSS_COMPONENT");
                            }
                            //else if (string.Equals(sRelationshipId, objCache.GetCodeId("CTTOLC", "CODE_REL_TYPE").ToString() + "-O", StringComparison.InvariantCultureIgnoreCase))
                            //{
                            //     iTableId1 = objCache.GetTableId("LOSS_COMPONENT");
                            //    iTableId2 =  objCache.GetTableId("CLAIM_TYPE");
                            //    isubTableId = objCache.GetTableId("CLAIM_LOSS_COMPONENT");
                            //}
                            else if(string.Equals(sRelationshipId,objCache.GetCodeId("POLTOCT", "CODE_REL_TYPE").ToString(),StringComparison.InvariantCultureIgnoreCase))
                            {
                                iTableId1 = objCache.GetTableId("CLAIM_TYPE");
                                iTableId2 = objCache.GetTableId("POLICY_CLAIM_LOB");
                                isubTableId = objCache.GetTableId("POLICY_LOB_AND_CLAIM_TYPE");
                            }
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("POLTOPT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        iTableId1 = objCache.GetTableId("POLICY_CLAIM_LOB");
                        iTableId2 = objCache.GetTableId("POLICY_TYPES");
                        isubTableId = objCache.GetTableId("POLICY_LOB_AND_POLICY_TYPE");
                    }
  //tanwar2 - mits 30910 - start
                                //Changed by Nikhil.Code review changes
                            //else if (string.Equals(sRelationshipId, objCache.GetCodeId("DEDTOCOVT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                            else if (string.Compare(sRelationshipId, objCache.GetCodeId("DEDTOCOVT", "CODE_REL_TYPE").ToString(),true) == 0)
                            {
                                iTableId1 = objCache.GetTableId("DEDUCTIBLE_TYPE");
                                iTableId2 = objCache.GetTableId("COVERAGE_TYPE");
                                isubTableId = objCache.GetTableId("DED_TYPE_AND_COV_TYPE");
                            }
                            //tanwar2 - mits 30910 - end
							//added by swati for AIC Gap 7 MITS # 36929
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("JDTOAIA12", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        iTableId1 = objCache.GetTableId("AIA_CODE_12");
                        iTableId2 = objCache.GetTableId("STATES");                        
                        isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_12");
                    }
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("JDTOAIA34", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        iTableId1 = objCache.GetTableId("AIA_CODE_34");
                        iTableId2 = objCache.GetTableId("STATES");                        
                        isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_34");
                    }
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("JDTOAIA56", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        iTableId1 = objCache.GetTableId("AIA_CODE_56");
                        iTableId2 = objCache.GetTableId("STATES");                        
                        isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_56");
                    }
                    //change end here by swati							
                   
                  //  iTableId = objCache.GetTableId("CLAIM_TYPE");

                            objNode = objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipSubType']");

                            sRelationshipId = objNode.Attributes["value"].Value;

                            sbSql = new StringBuilder();

                           // iTableId = objCache.GetTableId("CODE_REL_TYPE");

                            sbSql = new StringBuilder();
                            sbSql.Append(" SELECT CODES.CODE_ID,CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                            sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                            sbSql.Append(" AND DELETED_FLAG = 0 AND CODES.TABLE_ID =" + isubTableId);                            
                            sbSql.Append(" ORDER BY CODES.SHORT_CODE");
                            objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                            foreach (DataRow dr in objDS.Tables[0].Rows)
                            {
                                objElement = objXmlDocument.CreateElement("option");
                                objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                objElement.InnerText = Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                                objNode.AppendChild(objElement);

                                //if(string.Equals(Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim(),"Claim Type To Loss Component",StringComparison.InvariantCultureIgnoreCase))
                                //iCLTOLCID = Conversion.ConvertObjToInt(dr["CODE_ID"], m_iClientId);
                                //objElement = objXmlDocument.CreateElement("option");
                                //objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"])+"-O");
                                //objElement.InnerText = "Loss Component To Claim Type";
                                //objNode.AppendChild(objElement);
                            }

                            objNode = objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']");
                    sbSql = new StringBuilder();
                    //if-else added by swati
                    // Query modified by rkapoor29
                    if (iTableId1 == iStateTableId)
                    {
                        sbSql.Append("SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0 AND STATE_NAME IS NOT NULL");
                        sbSql.Append(" UNION SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0");
                        sbSql.Append(" AND STATE_ID NOT IN (SELECT STATE_ID FROM STATES)");
                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(),m_iClientId);
                        foreach (DataRow dr in objDS.Tables[0].Rows)
                        {
                            objElement = objXmlDocument.CreateElement("option");
                            objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["STATE_ROW_ID"]));
                            objElement.InnerText = Conversion.ConvertObjToStr(dr["STATE_ID"]).Trim() + " - "
                                + Conversion.ConvertObjToStr(dr["STATE_NAME"]).Trim();
                            objNode.AppendChild(objElement);
                        }
                    }
                    else
                    {
                    sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                    sbSql.Append(" AND DELETED_FLAG = 0 AND CODES.TABLE_ID =" + iTableId1);
                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
                    sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), objCache.GetTableName(iTableId1), this.LanguageCode); //Aman ML Change
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        objElement = objXmlDocument.CreateElement("option");
                        objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        objElement.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        if(iTableId1==iDedTypeTable && (Conversion.ConvertObjToInt(dr["CODE_ID"],m_iClientId) == iNone ||Conversion.ConvertObjToInt(dr["CODE_ID"],m_iClientId)==iNotDet ))
                        {
                            continue; //aaggarwal29: skip adding None and Not Determined short code to the Code relationship screen 
                        }
                        objNode.AppendChild(objElement);
                    }
                    }
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='lstAvailableLossComponents']");
                   // iTableId = objCache.GetTableId("LOSS_COMPONENT");

                    sbSql = new StringBuilder();
                    //if-else added by swati
                    // Query modified by rkapoor29
                    if (iTableId2 == iStateTableId)
                    {
                        sbSql.Append("SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0 AND STATE_NAME IS NOT NULL");
                        sbSql.Append(" UNION SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0");
                        sbSql.Append(" AND STATE_ID NOT IN (SELECT STATE_ID FROM STATES)");
                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(),m_iClientId);
                        foreach (DataRow dr in objDS.Tables[0].Rows)
                        {
                            objElement = objXmlDocument.CreateElement("option");
                            objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["STATE_ROW_ID"]));
                            objElement.InnerText = Conversion.ConvertObjToStr(dr["STATE_ID"]).Trim() + " - "
                                + Conversion.ConvertObjToStr(dr["STATE_NAME"]).Trim();
                            objNode.AppendChild(objElement);
                        }
                    }
                    else
                    {
                    sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                    sbSql.Append(" AND DELETED_FLAG = 0 AND CODES.TABLE_ID =" + iTableId2);
                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
                    sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), objCache.GetTableName(iTableId2), this.LanguageCode); //Aman ML Change
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        objElement = objXmlDocument.CreateElement("option");
                        objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        objElement.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        objNode.AppendChild(objElement);
                    }
                    }
                    objCon.Close();
                }

                return objXmlDocument;
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CodeRelationships.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElement = null;
                objNode = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objCache = null;
                sbSql = null;
               
            }
           
        }

        public XmlDocument GetSubData(XmlDocument p_objDoc)
        {

            DbConnection objCon = null;
            LocalCache objCache = null;
            XmlNode objNode = null;
            string sAdjusterList = string.Empty;
            int iTableId1 = 0;
            int iTableId2 = 0;
            string sEmailFROIACORD = string.Empty;
            XmlElement objElement = null;
            DataSet objDS = null;
            StringBuilder sbSql = null;
            XmlDocument objXmlDocument = null;
            //int iCLTOLCID =0;
            string sRelationshipId = string.Empty;
            string sSubRelationShipId = string.Empty;
            int isubTableId = 0;
            try
            {
                using (objCon = DbFactory.GetDbConnection(ConnectString))
                {

                    objCache = new LocalCache(ConnectString, m_iClientId);
					//added by swati
                    int iStateTableId = objCache.GetTableId("STATES");
                    //change end here by swati

                    objXmlDocument = new XmlDocument();
                    objXmlDocument = p_objDoc;


                    objNode = objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipType']");

                    sRelationshipId = objNode.Attributes["value"].Value;

                   
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipSubType']");

                    sSubRelationShipId = objNode.Attributes["value"].Value;

                    if (string.Equals(sRelationshipId, ""))
                    {
                        iTableId1 = objCache.GetTableId("CLAIM_TYPE");
                        iTableId2 = objCache.GetTableId("LOSS_COMPONENT");
                        isubTableId = objCache.GetTableId("CLAIM_AND_LOSS_COMPONENT");
                    }
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("CTTOLC", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipId, objCache.GetCodeId("CTTOLC", "CLAIM_AND_LOSS_COMPONENT").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("CLAIM_TYPE");
                            iTableId2 = objCache.GetTableId("LOSS_COMPONENT");
                            isubTableId = objCache.GetTableId("CLAIM_AND_LOSS_COMPONENT");
                        }
                        else if (string.Equals(sSubRelationShipId, objCache.GetCodeId("LCTOCT", "CLAIM_AND_LOSS_COMPONENT").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("LOSS_COMPONENT");
                            iTableId2 = objCache.GetTableId("CLAIM_TYPE");
                            isubTableId = objCache.GetTableId("CLAIM_AND_LOSS_COMPONENT");
                        }
                    }
                    
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("POLTOCT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipId, objCache.GetCodeId("POLTOCT", "POLICY_LOB_AND_CLAIM_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("POLICY_CLAIM_LOB");
                            iTableId2 = objCache.GetTableId("CLAIM_TYPE");
                            isubTableId = objCache.GetTableId("POLICY_LOB_AND_CLAIM_TYPE");
                        }
                        else if (string.Equals(sSubRelationShipId, objCache.GetCodeId("CTTOPOL", "POLICY_LOB_AND_CLAIM_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("CLAIM_TYPE");
                            iTableId2 = objCache.GetTableId("POLICY_CLAIM_LOB");
                            isubTableId = objCache.GetTableId("POLICY_LOB_AND_CLAIM_TYPE");
                        }
                    }
                      
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("POLTOPT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipId, objCache.GetCodeId("POLTOPT", "POLICY_LOB_AND_POLICY_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("POLICY_CLAIM_LOB");
                            iTableId2 = objCache.GetTableId("POLICY_TYPES");
                            isubTableId = objCache.GetTableId("POLICY_LOB_AND_POLICY_TYPE");
                        }
                        else if (string.Equals(sSubRelationShipId, objCache.GetCodeId("PTTOPOL", "POLICY_LOB_AND_POLICY_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("POLICY_TYPES");
                            iTableId2 = objCache.GetTableId("POLICY_CLAIM_LOB");
                            isubTableId = objCache.GetTableId("POLICY_LOB_AND_POLICY_TYPE");
                        }
                    }
					//added by swati for AIC Gap 7 MITS # 36929
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("JDTOAIA12", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipId, objCache.GetCodeId("JDTOAIA12", "STATES_AND_AIA_CODE_12").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("STATES");
                            iTableId2 = objCache.GetTableId("AIA_CODE_12");
                            isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_12");
                        }
                        else if (string.Equals(sSubRelationShipId, objCache.GetCodeId("AIA12TOJD", "STATES_AND_AIA_CODE_12").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("AIA_CODE_12");
                            iTableId2 = objCache.GetTableId("STATES");
                            isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_12");
                        }

                    }
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("JDTOAIA34", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipId, objCache.GetCodeId("JDTOAIA34", "STATES_AND_AIA_CODE_34").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("STATES");
                            iTableId2 = objCache.GetTableId("AIA_CODE_34");
                            isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_34");
                        }
                        else if (string.Equals(sSubRelationShipId, objCache.GetCodeId("AIA34TOJD", "STATES_AND_AIA_CODE_34").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("AIA_CODE_34");
                            iTableId2 = objCache.GetTableId("STATES");
                            isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_34");
                        }

                    }
                    else if (string.Equals(sRelationshipId, objCache.GetCodeId("JDTOAIA56", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipId, objCache.GetCodeId("JDTOAIA56", "STATES_AND_AIA_CODE_56").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("STATES");
                            iTableId2 = objCache.GetTableId("AIA_CODE_56");
                            isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_56");
                        }
                        else if (string.Equals(sSubRelationShipId, objCache.GetCodeId("AIA56TOJD", "STATES_AND_AIA_CODE_56").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            iTableId1 = objCache.GetTableId("AIA_CODE_56");
                            iTableId2 = objCache.GetTableId("STATES");
                            isubTableId = objCache.GetTableId("STATES_AND_AIA_CODE_56");
                        }

                    }
                    //change end here by swati
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']");
                    sbSql = new StringBuilder();
                    //if-else added by swati
                    // query modified by rkapoor29
                    if (iTableId1 == iStateTableId)
                    {
                        sbSql.Append("SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0 AND STATE_NAME IS NOT NULL");
                        sbSql.Append(" UNION SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0");
                        sbSql.Append(" AND STATE_ID NOT IN (SELECT STATE_ID FROM STATES)");
                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(),m_iClientId);
                        foreach (DataRow dr in objDS.Tables[0].Rows)
                        {
                            objElement = objXmlDocument.CreateElement("option");
                            objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["STATE_ROW_ID"]));
                            objElement.InnerText = Conversion.ConvertObjToStr(dr["STATE_ID"]).Trim() + " - "
                                + Conversion.ConvertObjToStr(dr["STATE_NAME"]).Trim();
                            objNode.AppendChild(objElement);
                        }
                    }
                    else
                    {
                    sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");   //Aman ML Change
                    sbSql.Append(" AND DELETED_FLAG = 0 AND CODES.TABLE_ID =" + iTableId1);
                    sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), objCache.GetTableName(iTableId1), this.LanguageCode);//Aman ML Change
                    objCon.Open();
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        objElement = objXmlDocument.CreateElement("option");
                        objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        objElement.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        objNode.AppendChild(objElement);
                    }
                    }
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='lstAvailableLossComponents']");
                  

                    sbSql = new StringBuilder();
                    //if-else added by swati
                    // query modified by rkapoor29
                    if (iTableId2 == iStateTableId)
                    {
                        sbSql.Append("SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0 AND STATE_NAME IS NOT NULL");
                        sbSql.Append(" UNION SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE DELETED_FLAG = 0");
                        sbSql.Append(" AND STATE_ID NOT IN (SELECT STATE_ID FROM STATES)");
                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(),m_iClientId);
                        foreach (DataRow dr in objDS.Tables[0].Rows)
                        {
                            objElement = objXmlDocument.CreateElement("option");
                            objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["STATE_ROW_ID"]));
                            objElement.InnerText = Conversion.ConvertObjToStr(dr["STATE_ID"]).Trim() + " - "
                                + Conversion.ConvertObjToStr(dr["STATE_NAME"]).Trim();
                            objNode.AppendChild(objElement);
                        }
                    }
                    else
                    {
                    sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT ");
                    sbSql.Append(" WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID");
                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE = 1033");   //Aman ML Change
                    sbSql.Append(" AND DELETED_FLAG = 0 AND CODES.TABLE_ID =" + iTableId2);
                    sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), objCache.GetTableName(iTableId2), this.LanguageCode);//Aman ML Change
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        objElement = objXmlDocument.CreateElement("option");
                        objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        objElement.InnerText = Conversion.ConvertObjToStr(dr["SHORT_CODE"]).Trim() + " - "
                            + Conversion.ConvertObjToStr(dr["CODE_DESC"]).Trim();
                        objNode.AppendChild(objElement);
                    }
                    }
                    objCon.Close();
                }

                return objXmlDocument;
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CodeRelationships.GetSubData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElement = null;
                objNode = null;
            
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objCache = null;
                sbSql = null;

            }

        }
        /// <summary>
        /// Gets the data on the selection of Code1.
        /// </summary>
        /// <param name="p_objDoc"></param>
        /// <returns></returns>
        public XmlDocument GetRelatedLossComponents(XmlDocument p_objDoc)
        {
            DbConnection objCon = null;
            LocalCache objCache = null;
            XmlNode objNode = null;
            DbReader objReader = null;
            string sSelectedCode = string.Empty;
            int iTableId = 0;
            string sTempVal = string.Empty;
            XmlElement objElement = null;
            DataSet objDS = null;
            StringBuilder sbSql = null;
            XmlDocument objXmlDocument = null;
            string sRelationshipId = null;
            string sRelationshipCode = null;
            string sPageType = null;
            string sSubRelationShipType = null;
            //tanwar2 - mits 30910 - start
            XmlNode xToRemove = null;
            XmlNode xToAdd = null;
            bool bOneToMany = false;
            //tanwar2 - mits 30910 - end
            try
            {
                using (objCon = DbFactory.GetDbConnection(ConnectString))
                {
                    objCon.Open();
                    objCache = new LocalCache(ConnectString, m_iClientId);


                    objXmlDocument = new XmlDocument();
                    objXmlDocument = p_objDoc;

                    sRelationshipCode = objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipType']").InnerText;
                    sSubRelationShipType = objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipSubType']").InnerText;
                   
                    if (string.Equals(sRelationshipCode, "") || string.Equals(sRelationshipCode, objCache.GetCodeId("CTTOLC", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                    if (string.Equals(sSubRelationShipType, objCache.GetCodeId("LCTOCT", "CLAIM_AND_LOSS_COMPONENT").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            sPageType = "O";
                        }
                    }
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("POLTOCT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipType, objCache.GetCodeId("CTTOPOL", "POLICY_LOB_AND_CLAIM_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            sPageType = "O";
                        }
                    }
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("POLTOPT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipType, objCache.GetCodeId("PTTOPOL", "POLICY_LOB_AND_POLICY_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            sPageType = "O";
                        }
                    }
 //tanwar2 - mits 30910 - start
                        //Changed by Nikhil.Code review changes
                    //else if ((string.Equals(sRelationshipCode, objCache.GetCodeId("DEDTOCOVT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase)))
                    else if (string.Compare(sRelationshipCode, objCache.GetCodeId("DEDTOCOVT", "CODE_REL_TYPE").ToString(), true) == 0)
                    {
                        //if (string.Equals(sSubRelationShipType, objCache.GetCodeId("DEDTOCOVT", "DED_TYPE_AND_COV_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        if (string.Compare(sSubRelationShipType, objCache.GetCodeId("DEDTOCOVT", "DED_TYPE_AND_COV_TYPE").ToString(), true) == 0)
                        {
                            sPageType = string.Empty;
                            bOneToMany = true;
                        }
                    }
                    //tanwar2 - mits 30910 - end
					//added by swati for AIC Gap 7 MITS # 36929
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("JDTOAIA12", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipType, objCache.GetCodeId("AIA12TOJD", "STATES_AND_AIA_CODE_12").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            sPageType = "O";
                        }
                    }
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("JDTOAIA34", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipType, objCache.GetCodeId("AIA34TOJD", "STATES_AND_AIA_CODE_34").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            sPageType = "O";
                        }
                    }
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("JDTOAIA56", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.Equals(sSubRelationShipType, objCache.GetCodeId("AIA56TOJD", "STATES_AND_AIA_CODE_56").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            sPageType = "O";
                        }
                    }
                    //change end here by swati

                    sSelectedCode = objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;
                    objXmlDocument.SelectSingleNode("//control[@name='hndPagetype']").RemoveAll();
                    objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").RemoveAll();
                    objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipType']").RemoveAll();
                    objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipSubType']").RemoveAll();
                    objNode = objXmlDocument.SelectSingleNode("//control[@name='txtRelatedComponents']");

                    sbSql = new StringBuilder();
                    if (sPageType == "O")
                    {
                        sbSql.Append(" SELECT CODE1 AS CODE FROM CODE_X_CODE ");
                        sbSql.Append(" WHERE CODE2 = " + sSelectedCode);
                        sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                       sbSql.Append(" AND  DELETED_FLAG = 0 ");
                    }
                    else
                    {
                        sbSql.Append(" SELECT CODE2 AS CODE FROM CODE_X_CODE ");
                        sbSql.Append(" WHERE CODE1 = " + sSelectedCode);
                       // sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                        sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                        sbSql.Append(" AND  DELETED_FLAG = 0 ");
                    }
                    objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                    foreach (DataRow dr in objDS.Tables[0].Rows)
                    {
                        if (sTempVal == "")
                        {
                            sTempVal = dr["CODE"].ToString();
                        }
                        else
                        {
                            sTempVal = sTempVal + "," + dr["CODE"];
                        }
                        
                    }
                    //tanwar2 - mits 30910 - start
                    if (bOneToMany)
                    {
                        //tanwar2 - mits 30910 - start
                        xToRemove = objXmlDocument.SelectSingleNode("//control[@name='txtRemoveComponents']");
                        xToAdd = objXmlDocument.SelectSingleNode("//control[@name='txtAddComponents']");

                        if (xToRemove != null && xToAdd != null)
                        {
                            xToAdd.InnerText = xToRemove.InnerText;
                            xToRemove.InnerText = string.Empty;
                        
                            sbSql.Remove(0, sbSql.Length);
                            if ((string.Compare(sPageType, "O", StringComparison.InvariantCultureIgnoreCase) == 0))
                            {
                                sbSql.Append(" SELECT CODE1 AS CODE FROM CODE_X_CODE ");
                                //Changed by Nikhil.Code review changes
                                //sbSql.Append(" WHERE CODE2 <> " + sSelectedCode);
                                sbSql.Append(" WHERE CODE2 <> ").Append(sSelectedCode);
                            }
                            else
                            {
                                sbSql.Append(" SELECT CODE2 AS CODE FROM CODE_X_CODE ");
                                //Changed by Nikhil.Code review changes
                                //sbSql.Append(" WHERE CODE1 <> " + sSelectedCode);
                                sbSql.Append(" WHERE CODE1 <> ").Append(sSelectedCode);
                            }
                            //Changed by Nikhil.Code review changes
                            //sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                            sbSql.Append(" AND REL_TYPE_CODE =").Append(sRelationshipCode);
                            sbSql.Append(" AND  DELETED_FLAG = 0 ");

                            using (objReader = objCon.ExecuteReader(sbSql.ToString()))
                            {
                                while (objReader.Read())
                                {
                                    //xToRemove.InnerText = (string.IsNullOrEmpty(xToRemove.InnerText) ? objReader["CODE"].ToString()
                                    //    : xToRemove.InnerText + "," + objReader["CODE"].ToString());
                                     xToRemove.InnerText = (string.IsNullOrEmpty(xToRemove.InnerText) ? objReader["CODE"].ToString()
                                        : string.Concat(xToRemove.InnerText , "," , objReader["CODE"].ToString()));
                                }
                            }
                        }
                    }
                    //tanwar2 - mits 30910 - end

                    objNode.InnerText = sTempVal;
                    objCon.Close();
                }

                return objXmlDocument;
            }

            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CodeRelationships.GetRelatedLossComponents.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objElement = null;
                objNode = null;
                if (objReader != null)
                    objReader.Dispose();

                if (objCache != null)
                {
                    objCache.Dispose();
                }
                objCache = null;
                sbSql = null;

            }
        }

        /// <summary>
        /// Saves the relationship Into Database
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            DbConnection objCon = null;
            XmlElement objElm = null;
            int iRowID = 0;
            string sSQL = string.Empty;
            string[] sCodeList;
            string sCode1 = null;
            string sCode2 = null;
            StringBuilder sbSql = null;
            DataSet objDS = null;
            string sUpdateList = null;
            ArrayList sInsertList = new ArrayList();
            bool bIsPresent = true;
            LocalCache objCache;
            string sRelationshipId = null;
            string sRelationshipCode = null;
            string sSubRelationShipType = null;
            try
            {
                using (objCon = DbFactory.GetDbConnection(ConnectString))
                {
                    objCon.Open();
                    objCache = new LocalCache(ConnectString, m_iClientId);

                    XMLDoc = p_objXmlDocument;

                    sRelationshipCode = p_objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipType']").InnerText;
                    sSubRelationShipType = p_objXmlDocument.SelectSingleNode("//control[@name='cboRelationshipSubType']").InnerText;

                    if (sRelationshipCode.EndsWith("-O"))
                    {
                        sRelationshipCode = sRelationshipCode.Remove(sRelationshipCode.Length - 2, 2);
                    }
                  
                    if (string.Equals(sRelationshipCode, "") || string.Equals(sRelationshipCode, objCache.GetCodeId("CTTOLC", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        
                         if (string.IsNullOrEmpty(sSubRelationShipType) ||string.Equals(sSubRelationShipType, objCache.GetCodeId("CTTOLC", "CLAIM_AND_LOSS_COMPONENT").ToString(), StringComparison.InvariantCultureIgnoreCase))
                         {
                             if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                 sCode1 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;
                             
                        }
                        else if (string.Equals(sSubRelationShipType, objCache.GetCodeId("LCTOCT", "CLAIM_AND_LOSS_COMPONENT").ToString(), StringComparison.InvariantCultureIgnoreCase))
                         {
                             if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                 sCode2 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;
                          
                        }
                    }
               
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("POLTOCT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        
                         if (string.IsNullOrEmpty(sSubRelationShipType) ||string.Equals(sSubRelationShipType, objCache.GetCodeId("POLTOCT", "POLICY_LOB_AND_CLAIM_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode1 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;
                          
                        }
                        else if (string.Equals(sSubRelationShipType, objCache.GetCodeId("CTTOPOL", "POLICY_LOB_AND_CLAIM_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode2 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;
                        }
                    }
                          
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("POLTOPT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (string.IsNullOrEmpty(sSubRelationShipType) || string.Equals(sSubRelationShipType, objCache.GetCodeId("POLTOPT", "POLICY_LOB_AND_POLICY_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode1 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;
                        }
                        else if (string.Equals(sSubRelationShipType, objCache.GetCodeId("PTTOPOL", "POLICY_LOB_AND_POLICY_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode2 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;

                    }

                    }
					 //tanwar2 - mits 30910 - start
                        //Changed by Nikhil.Code review change
                    //else if (string.Equals(sRelationshipCode, objCache.GetCodeId("DEDTOCOVT", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    else if (string.Compare(sRelationshipCode, objCache.GetCodeId("DEDTOCOVT", "CODE_REL_TYPE").ToString(), true) == 0)
                    {
                        //Changed by Nikhil.Code review changes
                        //if (string.Equals(sSubRelationShipType, objCache.GetCodeId("DEDTOCOVT", "DED_TYPE_AND_COV_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        if (string.Compare(sSubRelationShipType, objCache.GetCodeId("DEDTOCOVT", "DED_TYPE_AND_COV_TYPE").ToString(), true) == 0)
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode1 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;
                        }
                    }
                    //tanwar2 - mits 30910 - end
					//added by swati for AIC Gap 7 MITS # 36929
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("JDTOAIA12", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {

                        if (string.IsNullOrEmpty(sSubRelationShipType) || string.Equals(sSubRelationShipType, objCache.GetCodeId("JDTOAIA12", "STATES_AND_AIA_CODE_12").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode1 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;

                        }
                        else if (string.Equals(sSubRelationShipType, objCache.GetCodeId("AIA12TOJD", "STATES_AND_AIA_CODE_12").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode2 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;

                        }


                    }
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("JDTOAIA34", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {

                        if (string.IsNullOrEmpty(sSubRelationShipType) || string.Equals(sSubRelationShipType, objCache.GetCodeId("JDTOAIA34", "STATES_AND_AIA_CODE_34").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode1 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;

                        }
                        else if (string.Equals(sSubRelationShipType, objCache.GetCodeId("AIA34TOJD", "STATES_AND_AIA_CODE_34").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode2 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;

                        }


                    }
                    else if (string.Equals(sRelationshipCode, objCache.GetCodeId("JDTOAIA56", "CODE_REL_TYPE").ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {

                        if (string.IsNullOrEmpty(sSubRelationShipType) || string.Equals(sSubRelationShipType, objCache.GetCodeId("JDTOAIA56", "STATES_AND_AIA_CODE_56").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode1 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;

                        }
                        else if (string.Equals(sSubRelationShipType, objCache.GetCodeId("AIA56TOJD", "STATES_AND_AIA_CODE_56").ToString(), StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']") != null)
                                sCode2 = p_objXmlDocument.SelectSingleNode("//control[@name='lstClaimType']").InnerText;

                        }


                    }
                    //change end here by swati
                   
                    objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='txtRelatedComponents']");
                    objElm.InnerText = objElm.InnerText.Trim();
                    sCodeList = objElm.InnerText.Split(',');


                    if (sCode1 != null)
                    {
                        sbSql = new StringBuilder();
                        sbSql.Append(" SELECT CODE2 FROM CODE_X_CODE");
                        sbSql.Append(" WHERE CODE1 =" + sCode1);
                        //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                        sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);


                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);


                        sUpdateList = "";
                        foreach (string sCodeEid in sCodeList)
                        {
                            bIsPresent = true;
                            foreach (DataRow dr in objDS.Tables[0].Rows)
                            {
                                if (dr["CODE2"].ToString() == sCodeEid)
                                {
                                    sUpdateList = sUpdateList + dr["CODE2"] + ",";
                                    bIsPresent = false;
                                    break;
                                }
                            }
                            if (bIsPresent)
                            {
                                sInsertList.Add(sCodeEid);

                            }
                        }
                        sbSql = null;
                        if (sUpdateList.Length > 1)
                        {
                            sbSql = new StringBuilder();
                            sUpdateList = sUpdateList.Remove(sUpdateList.Length - 1);
                            sbSql.Append(" UPDATE CODE_X_CODE SET DELETED_FLAG = -1, UPDATED_BY_USER='" + m_sDBOUserId + "',DTTM_RCD_LAST_UPD=" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            sbSql.Append(" WHERE CODE2 NOT IN (" + sUpdateList + ")");
                            sbSql.Append(" AND CODE1 =" + sCode1);
                            //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                            sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                            objCon.ExecuteNonQuery(sbSql.ToString());
                            sbSql = new StringBuilder();
                            sbSql.Append(" UPDATE CODE_X_CODE SET DELETED_FLAG = 0, UPDATED_BY_USER='" + m_sDBOUserId + "',DTTM_RCD_LAST_UPD=" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            sbSql.Append(" WHERE CODE2 IN (" + sUpdateList + ")");
                            sbSql.Append(" AND CODE1 =" + sCode1);
                            //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                            sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                            objCon.ExecuteNonQuery(sbSql.ToString());
                        }
                        else
                        {
                            sbSql = new StringBuilder();
                            sbSql.Append(" UPDATE CODE_X_CODE SET DELETED_FLAG = -1, UPDATED_BY_USER='" + m_sDBOUserId + "',DTTM_RCD_LAST_UPD=" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            sbSql.Append(" WHERE CODE1 =" + sCode1);
                            //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                            sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                            objCon.ExecuteNonQuery(sbSql.ToString());

                        }

                        sbSql = null;
                        if ((sInsertList != null) && sInsertList.Count > 0)
                        {
                            objCache = new LocalCache(ConnectString, m_iClientId);

                            foreach (string sCodeEid in sInsertList)
                            {
                                if (sCodeEid != "")
                                {
                                    sbSql = new StringBuilder();
                                    iRowID = Utilities.GetNextUID(ConnectString, "CODE_X_CODE", m_iClientId);
                                    sbSql.Append(" INSERT INTO CODE_X_CODE(ROW_ID,CODE1,CODE2,REL_TYPE_CODE,DELETED_FLAG,ADDED_BY_USER,UPDATED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD) VALUES ");
                                    sbSql.Append("(" + iRowID + "," + sCode1 + "," + sCodeEid + "," + sRelationshipCode + ",0,'" + m_sDBOUserId + "',NULL," + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + ",NULL)");
                                    objCon.ExecuteNonQuery(sbSql.ToString());
                                }
                            }

                        }
                    }
                    else if (sCode2 != null)
                    {



                        sbSql = new StringBuilder();
                        sbSql.Append(" SELECT CODE1 FROM CODE_X_CODE");
                        sbSql.Append(" WHERE CODE2 =" + sCode2);
                        //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                        sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);


                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);


                        sUpdateList = "";
                        foreach (string sCodeEid in sCodeList)
                        {
                            bIsPresent = true;
                            foreach (DataRow dr in objDS.Tables[0].Rows)
                            {
                                if (dr["CODE1"].ToString() == sCodeEid)
                                {
                                    sUpdateList = sUpdateList + dr["CODE1"] + ",";
                                    bIsPresent = false;
                                    break;
                                }
                            }
                            if (bIsPresent)
                            {
                                sInsertList.Add(sCodeEid);

                            }
                        }
                        sbSql = null;
                        if ((sUpdateList != null) && sUpdateList.Length > 1)
                        {
                            sbSql = new StringBuilder();
                            sUpdateList = sUpdateList.Remove(sUpdateList.Length - 1);
                            sbSql.Append(" UPDATE CODE_X_CODE SET DELETED_FLAG = -1, UPDATED_BY_USER='" + m_sDBOUserId + "',DTTM_RCD_LAST_UPD=" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            sbSql.Append(" WHERE CODE1 NOT IN (" + sUpdateList + ")");
                            sbSql.Append(" AND CODE2 =" + sCode2);
                            //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                            sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                            objCon.ExecuteNonQuery(sbSql.ToString());
                            sbSql = new StringBuilder();

                            sbSql.Append(" UPDATE CODE_X_CODE SET DELETED_FLAG = 0, UPDATED_BY_USER='" + m_sDBOUserId + "',DTTM_RCD_LAST_UPD=" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            sbSql.Append(" WHERE CODE1 IN (" + sUpdateList + ")");
                            sbSql.Append(" AND CODE2 =" + sCode2);
                            //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                            sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                            objCon.ExecuteNonQuery(sbSql.ToString());
                        }
                        else
                        {
                            sbSql = new StringBuilder();
                            sbSql.Append(" UPDATE CODE_X_CODE SET DELETED_FLAG = -1, UPDATED_BY_USER='" + m_sDBOUserId + "',DTTM_RCD_LAST_UPD=" + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()));
                            sbSql.Append(" WHERE CODE2 =" + sCode2);
                            //sbSql.Append(" AND RELATIONSHIP_ID =" + sRelationshipId);
                            sbSql.Append(" AND REL_TYPE_CODE =" + sRelationshipCode);
                            objCon.ExecuteNonQuery(sbSql.ToString());

                        }
                        sbSql = null;
                        if ((sInsertList != null) && sInsertList.Count > 0)
                        {
                            objCache = new LocalCache(ConnectString, m_iClientId);

                            foreach (string sCodeEid in sInsertList)
                            {
                                if (sCodeEid != "")
                                {
                                    sbSql = new StringBuilder();
                                    iRowID = Utilities.GetNextUID(ConnectString, "CODE_X_CODE", m_iClientId);
                                    sbSql.Append(" INSERT INTO CODE_X_CODE(ROW_ID,CODE1,CODE2,REL_TYPE_CODE,DELETED_FLAG,ADDED_BY_USER,UPDATED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD) VALUES ");
                                    sbSql.Append("(" + iRowID + "," + sCodeEid + "," + sCode2 + "," +  sRelationshipCode + ",0,'" + m_sDBOUserId + "',NULL," + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + ",NULL)");
                                    objCon.ExecuteNonQuery(sbSql.ToString());
                                }
                            }

                        }
                    }
                    objCon.Close();
                }
                return XMLDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CodeRelationships.Save.Error", m_iClientId), p_objEx);
            }
        }
       
    }
}
