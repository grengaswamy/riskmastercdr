﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	/// Summary description for ActivityLogAdaptor.
	/// </summary>
	public class ActivityLog
	{
		private string m_sConnectionString="";
        private int m_iClientID = 0;//sonali
		public ActivityLog(string p_sConnectionString,int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientID = p_iClientId;
          

		}
		private XmlDocument GetData(string p_sDataType)
		{
			XmlDocument objType=null;
			XmlElement objTmpElement=null;
			DbReader objReader=null;
			LocalCache objCache=null;
			string sCode="";
			string sDesc="";
			string sSQL="";
            try
            {
                objType = new XmlDocument();
                objTmpElement = objType.CreateElement(p_sDataType);
                objType.AppendChild(objTmpElement);
                switch (p_sDataType)
                {
                    case "operation":
                        sSQL = "SELECT DISTINCT OPERATION FROM ACTIVITY_LOG";
                        break;
                    case "userlogin":
                        sSQL = "SELECT DISTINCT USER_LOGIN FROM ACTIVITY_LOG";
                        break;
                    case "accesstype":
                        objCache = new LocalCache(m_sConnectionString, m_iClientID);
                        sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID =" + objCache.GetTableId("ACCESS_TYPE_CODE"); ;
                        objCache.Dispose();
                        break;
                }
                objTmpElement = objType.CreateElement("option");
                objTmpElement.InnerText = "";
                objTmpElement.SetAttribute("value", "");
                objType.FirstChild.AppendChild(objTmpElement);
                if (sSQL != "")
                {
                    if (p_sDataType == "accesstype")
                    {
                        objCache = new LocalCache(m_sConnectionString, m_iClientID);
                    }
                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                    while (objReader.Read())
                    {
                        objTmpElement = objType.CreateElement("option");
                        if (p_sDataType != "accesstype")
                        {
                            objTmpElement.InnerText = Conversion.ConvertObjToStr(objReader.GetValue(0));
                            objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                        else
                        {
                            objCache.GetCodeInfo(Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientID), ref sCode, ref sDesc);
                            objTmpElement.InnerText = sCode + " " + sDesc;
                            objTmpElement.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue(0)));
                        }
                        objType.FirstChild.AppendChild(objTmpElement);
                    }
                    objReader.Dispose();
                    if (p_sDataType == "accesstype")
                    {
                        objCache.Dispose();
                    }

                }
                return objType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
               objType = null;
               objTmpElement = null;
               if(objReader != null)
                  objReader.Dispose();
              if (objCache != null)
                  objCache.Dispose();
            }

		}
		public XmlDocument Search(XmlDocument p_objSearchParam)
		{
			XmlElement objElement=null;
			XmlDocument objXmlDocument=null;
			XmlElement objTmpElement=null;
			string sSQL="";
			string sTmpParam="";
			DataSet objDS=null;
            int lRecords = 0;
            int iPageNo = 0;
            int iPageSize = 100;
			try
			{
				objXmlDocument=new XmlDocument();

				objElement=objXmlDocument.CreateElement("SearchResults");
				if (p_objSearchParam.SelectSingleNode("//FirstTime").InnerText!="1")
				{
					sSQL = "SELECT LAST_NAME,FIRST_NAME,OPERATION,USER_LOGIN,ACCESS_TYPE_CODE,DTTM_LOG ";
					sSQL = sSQL + " FROM ACTIVITY_LOG A,ENTITY E WHERE E.ENTITY_ID=A.PATIENT_EID";
					sTmpParam=p_objSearchParam.SelectSingleNode("//PatientLastName").InnerText;
					if (sTmpParam!="")
					{
                        //Parijat:Mits 9710
                        sSQL = sSQL + " AND upper(E.LAST_NAME) like upper('" + sTmpParam.Replace("'", "''") + "%')";
					}
					sTmpParam=p_objSearchParam.SelectSingleNode("//PatientFirstName").InnerText;
					if (sTmpParam!="")
					{
                        //Parijat:Mits 9710
                        sSQL = sSQL + " AND upper(E.FIRST_NAME) like upper('" + sTmpParam.Replace("'", "''") + "%')";
					}
					sTmpParam=p_objSearchParam.SelectSingleNode("//Operation").InnerText;
					if (sTmpParam!="")
					{
						sSQL = sSQL +" AND A.OPERATION = '" + sTmpParam.Replace("'","''")+ "'";
					}
					sTmpParam=p_objSearchParam.SelectSingleNode("//AccessType").InnerText;
					if (sTmpParam!="")
					{
						sSQL = sSQL +" AND A.ACCESS_TYPE_CODE = '" + sTmpParam.Replace("'","''")+ "'";
					}
					sTmpParam=p_objSearchParam.SelectSingleNode("//FromDate").InnerText;
					if (sTmpParam!="")
					{
						sSQL = sSQL +" AND A.DTTM_LOG >= '" + Conversion.GetDate(sTmpParam.Replace("'","''"))+ "'";
					}
					//Parijat
                    //No Need for the check now-- for comparing if FromDate was equal to ToDate.
                    //Since the changes have been done for fetching the record for ToDate date also .
					sTmpParam=p_objSearchParam.SelectSingleNode("//ToDate").InnerText;
					if (sTmpParam!="")
					{
                        //Parijat: MITS 9711,8494
                        //Since data is in the form of 'yyyyMMddHHmmss'in DTTM_LOG
                        // It fails to fetch the data for the 'ToDate' date since the 'HHmmss'part automatically becomes 000000 hence now iots being provided for 'ToDate'
                        sSQL = sSQL + "  AND A.DTTM_LOG <='" + Conversion.GetDate(sTmpParam.Replace("'", "''"))+ "235959" + "'";
					}
					
					sTmpParam=p_objSearchParam.SelectSingleNode("//UserLogin").InnerText;
					if (sTmpParam!="")
					{
						sSQL = sSQL +" AND A.USER_LOGIN='" + sTmpParam.Replace("'","''")+ "'";
					}
                    sSQL = sSQL + "  ORDER BY A.DTTM_LOG DESC";//sonali for JIRA- RMA-6294
                    LocalCache objCache = new LocalCache(m_sConnectionString, m_iClientID);
					objDS=DbFactory.GetDataSet(m_sConnectionString,sSQL,m_iClientID);//sonali
					string sCode="";
					string sCodeDesc="";
                    //start: bkumar33 has changed for 17052  : ybhaskar - Merging the MITS to R5
                   
                    if (p_objSearchParam.SelectSingleNode("//PageNumber").InnerText != "0")
                        iPageNo = Conversion.ConvertStrToInteger(p_objSearchParam.SelectSingleNode("//PageNumber").InnerText);
                    else
                        iPageNo = 1;

                    //iPageSize is fixed to 100. A custom setting can be provided here but will be done only if asked by a client as an enhancement
					//iPageSize = Conversion.ConvertObjToInt(RMConfigurationManager.GetAppSetting("ActivityLogRecords"), m_iClientId);
                    
                    int iTotalRecords = objDS.Tables[0].Rows.Count;
                    
                    int iTotalPages = ((iTotalRecords - 1) / iPageSize) + 1;
                    int iRowStart = (iPageNo - 1) * iPageSize;
                    int iRowEnd = iRowStart + iPageSize;
                    if (iRowEnd > (iTotalRecords - 1))
                    {
                        iRowEnd = iTotalRecords;
                    }

                    objTmpElement = objXmlDocument.CreateElement("search");
                    objTmpElement.SetAttribute("TotalRows", iTotalRecords.ToString());
                    objTmpElement.SetAttribute("PageSize", iPageSize.ToString());
                    objTmpElement.SetAttribute("PageNumber", iPageNo.ToString());
                    objTmpElement.SetAttribute("TotalPages", iTotalPages.ToString());

                    objElement.AppendChild(objTmpElement);

                       foreach (DataRow dr in objDS.Tables[0].Rows)                        
                        {
                            if ((lRecords >= iRowStart) && (lRecords < iRowEnd))
                            {
                                objTmpElement = objXmlDocument.CreateElement("row");
                                objTmpElement.SetAttribute("name", Conversion.ConvertObjToStr(dr["LAST_NAME"]) + ", " + Conversion.ConvertObjToStr(dr["FIRST_NAME"]));
                                objTmpElement.SetAttribute("operation", Conversion.ConvertObjToStr(dr["OPERATION"]));
                                objTmpElement.SetAttribute("login", Conversion.ConvertObjToStr(dr["USER_LOGIN"]));
                                objCache.GetCodeInfo(Conversion.ConvertObjToInt(dr["ACCESS_TYPE_CODE"], m_iClientID), ref sCode, ref sCodeDesc);
                                objTmpElement.SetAttribute("accesstype", sCode + " " + sCodeDesc);
                                objTmpElement.SetAttribute("date", Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(dr["DTTM_LOG"]), "d"));
                                if (Conversion.ConvertObjToStr(dr["DTTM_LOG"]).Length == 14)
                                {
                                    objTmpElement.SetAttribute("time", Conversion.GetDBTimeFormat(Conversion.ConvertObjToStr(dr["DTTM_LOG"]).Substring(8, 6), "t"));
                                }
                                else
                                {
                                    objTmpElement.SetAttribute("time", "");
                                }
                                objElement.AppendChild(objTmpElement);                                
                            }
                            lRecords++;                            
                        }                    
                    objCache.Dispose();
                    //end: bkumar33 has changed for 17052 : ybhaskar Merging the MITS to R5
				}
				objXmlDocument.AppendChild(objElement);
				XmlNode objNode= objXmlDocument.ImportNode((XmlNode)GetData("operation").SelectSingleNode("//operation"),true);
				objXmlDocument.FirstChild.AppendChild(objNode);
				objNode= objXmlDocument.ImportNode((XmlNode)GetData("accesstype").SelectSingleNode("//accesstype"),true);
				objXmlDocument.FirstChild.AppendChild(objNode);
				objNode= objXmlDocument.ImportNode((XmlNode)GetData("userlogin").SelectSingleNode("//userlogin"),true);
				objXmlDocument.FirstChild.AppendChild(objNode);
				return objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("AdjusterTransfer.GetAvailableClaims.Error",m_iClientID),p_objEx);//sonali
			}
			finally
			{
				objXmlDocument=null;
				objElement=null;
				if (objDS!=null)
					objDS.Dispose();
			}
		}
		
	}
}
