using System;
using Riskmaster.Db; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 
using System.Data; 
using System.Text; 
using System.IO; 


namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
    /// Encapsulates standard functions used by the library
	/// </summary>
	public class RMUTIL
	{
        private static int m_iClientId = 0;    //psharma206  
		/// Name		: RMUTIL
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		public RMUTIL(){}

		/// Name		: FixSoundexValues
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Fixes Soundex Values
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public static void FixSoundexValues(string p_sConnString, int p_iClientId)   //psharma206 jira 95
		{
			DbConnection objConn = null;
			DataSet objDs = null;
            m_iClientId = p_iClientId; //psharma206 jira 95



			try
			{
				objConn = DbFactory.GetDbConnection(p_sConnString);
				objConn.Open();
                objDs = DbFactory.GetDataSet(p_sConnString, "SELECT ENTITY_ID, LAST_NAME, LAST_NAME_SOUNDEX FROM ENTITY", m_iClientId);
				foreach(DataRow objRow in objDs.Tables[0].Rows)
				{
					string sSoundex = Utilities.FindSoundexValue(DbRow.GetStringField(objRow,"LAST_NAME"));  
					if(DbRow.GetStringField(objRow , "LAST_NAME_SOUNDEX")!=sSoundex)
					{
						objConn.ExecuteNonQuery("UPDATE ENTITY SET LAST_NAME_SOUNDEX = " + UTILITY.QuoteString(sSoundex) + " WHERE ENTITY_ID = " + DbRow.GetIntField(objRow, "ENTITY_ID"));						
					}
				}
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("RMUTIL.FixSoundexValues.DataErr", m_iClientId), p_objEx); //psharma206 jira 95
			}
			finally
			{
				if(objDs!=null)objDs.Dispose();
				if(objConn!=null)objConn.Dispose();
			}
		}

		/// Name		: RebuildOrgHierarchy
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Rebuild Org Hierarchy
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public static void RebuildOrgHierarchy(string p_sConnString, int p_iClientId)//psharma206 jira 94
		{
			DbConnection objConn = null;
			StringBuilder sbSQL = null;
            m_iClientId = p_iClientId;//psharma206 jira 94
			try
			{
				objConn = DbFactory.GetDbConnection(p_sConnString);
				objConn.Open(); 
				objConn.ExecuteNonQuery("DELETE FROM ORG_HIERARCHY");
				sbSQL = new StringBuilder();
				sbSQL.Append("INSERT INTO ORG_HIERARCHY(DEPARTMENT_EID, FACILITY_EID,LOCATION_EID,DIVISION_EID,REGION_EID,OPERATION_EID,COMPANY_EID,CLIENT_EID)");
				sbSQL.Append(" SELECT DISTINCT DEPARTMENT.ENTITY_ID  DEPARTMENT_EID,");
				sbSQL.Append(" FACILITY.ENTITY_ID  FACILITY_EID, LOCATION.ENTITY_ID  LOCATION_EID,");
				sbSQL.Append(" DIVISION.ENTITY_ID  DIVISION_EID, REGION.ENTITY_ID  REGION_EID,");
				sbSQL.Append(" OPERATION.ENTITY_ID  OPERATION_EID, COMPANY.ENTITY_ID  COMPANY_EID, CLIENTENT.ENTITY_ID  CLIENT_EID");
				
				if (objConn.DatabaseType!= eDatabaseType.DBMS_IS_SQLSRVR && objConn.DatabaseType!= eDatabaseType.DBMS_IS_SYBASE)
					sbSQL.Append(" FROM ENTITY CLIENTENT, ENTITY COMPANY, ENTITY DEPARTMENT, ENTITY DIVISION, ENTITY FACILITY, ENTITY LOCATION, ENTITY OPERATION, ENTITY REGION");
				else
                    sbSQL.Append(" FROM dbo.ENTITY CLIENTENT, dbo.ENTITY COMPANY, dbo.ENTITY DEPARTMENT, dbo.ENTITY DIVISION, dbo.ENTITY FACILITY, dbo.ENTITY LOCATION, dbo.ENTITY OPERATION, dbo.ENTITY REGION");
				sbSQL.Append(" WHERE ");
				sbSQL.Append(" (DEPARTMENT.ENTITY_TABLE_ID = 1012) AND");
				sbSQL.Append(" (DEPARTMENT.PARENT_EID = FACILITY.ENTITY_ID) AND");
				sbSQL.Append(" (FACILITY.PARENT_EID = LOCATION.ENTITY_ID) AND");
				sbSQL.Append(" (LOCATION.PARENT_EID = DIVISION.ENTITY_ID) AND");
				sbSQL.Append(" (DIVISION.PARENT_EID = REGION.ENTITY_ID) AND");
				sbSQL.Append(" (REGION.PARENT_EID = OPERATION.ENTITY_ID) AND");
				sbSQL.Append(" (OPERATION.PARENT_EID = COMPANY.ENTITY_ID) AND");
				sbSQL.Append(" (COMPANY.PARENT_EID = CLIENTENT.ENTITY_ID)");
				
				objConn.ExecuteNonQuery(sbSQL.ToString());
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("RMUTIL.RebuildOrgHierarchy.DataErr", m_iClientId), p_objEx); //psharma206 jira 94
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
				sbSQL = null;
			}
		}

		/// Name		: RebuildGlossValues
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Rebuild Glossary Values
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
		/// <param name="p_sFileName">File Name</param>
        public static void RebuildGlossValues(string p_sConnString, int p_iClientId)  //psharma206 jira 93
		{
			long lMaxId = 0;
			long lNextUid = 0;
			string sTableName = "";
			string sFieldName = "";
			char[] arrDelimit = ",".ToCharArray();
			String[] sarrColumn=null;
			DbConnection objConn = null;
			StreamReader objSRdr = null;
			DbReader objDbRdr = null;
			string sFileName = "";
            int count = 0; // mkaran2 - MITS 32876
            m_iClientId = p_iClientId;//psharma206 jira 93

			try
			{
				sFileName = Path.Combine(UTILITY.GetAppFilesDirectory("RMGloss"),  UTILITY.GetConfigUtilitySettings(string.Empty, 0)["RMGlossFile"]);//rkaur27  
				
				objConn = DbFactory.GetDbConnection(p_sConnString);
				objConn.Open();
				if (File.Exists(sFileName))
				{
					using (objSRdr = new StreamReader(sFileName)) 
					{
						string sLine = "";
						while ((sLine = objSRdr.ReadLine())!= null)
						{
							sarrColumn = sLine.Split(arrDelimit);
							sTableName = sarrColumn[0].Trim();
							sFieldName = sarrColumn[1].Trim();
							if (sTableName != "" && sFieldName != "")
							{
                                //lMaxId = Conversion.ConvertObjToInt64(objConn.ExecuteScalar(
                                //           String.Format("SELECT MAX({0}) FROM {1}", sFieldName, sTableName))) + 1;
                                //if (lMaxId > 0)
                                //{
                                //    lNextUid = Conversion.ConvertObjToInt64(objConn.ExecuteScalar(
                                //        String.Format("SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '{0}'", sTableName)));
                                //    if (lNextUid < lMaxId)
                                //    {
                                //        objConn.ExecuteNonQuery("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " +
                                //            lMaxId + " WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'");
                                //    }
                                //}

                                // mkaran2 - MITS 32876 : start
                                count = Convert.ToInt32(objConn.ExecuteScalar(
                                    string.Format("SELECT Count(NEXT_UNIQUE_ID) FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '{0}'",sTableName)));
                                if (count > 0)
                                {
                                    lMaxId = Conversion.ConvertObjToInt64(objConn.ExecuteScalar(
                                            String.Format("SELECT MAX({0}) FROM {1}", sFieldName, sTableName)), m_iClientId) + 1;
                                    if (lMaxId > 0)
                                    {
                                        lNextUid = Conversion.ConvertObjToInt64(objConn.ExecuteScalar(
                                            String.Format("SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME = '{0}'", sTableName)), m_iClientId);
                                        if (lNextUid < lMaxId)
                                        {
                                            objConn.ExecuteNonQuery("UPDATE GLOSSARY SET NEXT_UNIQUE_ID = " +
                                               lMaxId + " WHERE SYSTEM_TABLE_NAME = '" + sTableName + "'");
                                        }
                                    }
                                } // mkaran2 - MITS 32876 : end
							}
						}
					}
				}
				else
					throw new RMAppException("RMUTIL.RebuildGlossValues.FileMissing");
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("RMUTIL.RebuildGlossValues.DataErr", m_iClientId), p_objEx); //psharma206 jira 93
			}
			finally
			{
				if(objDbRdr!=null)objDbRdr.Dispose();
				if(objConn!=null)objConn.Dispose();
				sarrColumn=null; 
				arrDelimit = null;
				objSRdr = null;
			}            
		}
	}
}