﻿using System;
using System.Data; 
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.Text;
using Riskmaster.Settings;
using System.Collections.Specialized; 

namespace Riskmaster.Application.RMUtilities
{

    /// <summary>
    /// This enum identifies the Location from where the Dll is called. If it is called from PowerViewUpgrade then it is all, if from Utility
    /// then Supp or Juris depending upon the Utility Screen
    /// </summary>
    public enum PowerViewMode
    {
        ALL,
        SUPP,
        JURIS
    }

    public enum UpgradeSection
    {
        ALL,
        ONLYSUPP,
        ONLYJURIS
    }

	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   19th,Apr 2005
	///Purpose :   Common functions
	/// </summary>
	public class UTILITY
	{
		/// <summary>
		/// dictionary object to specify BRS fee schedule type explicitly instead of implicitly based
        /// on the string array index. 
		/// </summary>
        private static Dictionary<int, string> dicBRSDatabaseType = null;
   
		/// Name		: UTILITY
		/// Author		: Pankaj
		/// Date Created: 04/19/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		internal UTILITY(){}

        

		#region "Properties"

		/// <summary>
		/// BRSDatabaseType
		/// </summary>
		internal static Dictionary<int, string> BRSDatabaseType
		{
			get
            {
                if(dicBRSDatabaseType == null)
                {
                    dicBRSDatabaseType = new Dictionary<int, string>();
                    dicBRSDatabaseType.Add(0,"UCR 1994 (old)");
                    dicBRSDatabaseType.Add(1,"UCR 1995 to Present");
                    dicBRSDatabaseType.Add(2,"Workers' Compensation 1999 (old)");
                    dicBRSDatabaseType.Add(3,"Outpatient Style");
                    dicBRSDatabaseType.Add(4,"HCPCS Style");
                    dicBRSDatabaseType.Add(7,"Anesthesia");
                    dicBRSDatabaseType.Add(8,"Dental UCR-1995 Style");
                    dicBRSDatabaseType.Add(9,"Ohio Workers' Comp 1996 (old)");
                    dicBRSDatabaseType.Add(10,"Ohio Wrks' Comp 1997");
                    dicBRSDatabaseType.Add(11,"St. Anthony for Nevada");
                    dicBRSDatabaseType.Add(12,"Basic User Entered Schedule");
                    dicBRSDatabaseType.Add(13,"Workers' Compensation 2000");
                    dicBRSDatabaseType.Add(14,"Extended BRS Schedule");
                    dicBRSDatabaseType.Add(15,"Free Entry Pharmacy *");
                    dicBRSDatabaseType.Add(16,"Free Entry Hospital *");
                    dicBRSDatabaseType.Add(17,"Generic Free Entry *");
                    dicBRSDatabaseType.Add(18, "Free Entry Dental *");
                    dicBRSDatabaseType.Add(19,"OWCP Fee Schedule");
                    dicBRSDatabaseType.Add(20,"Florida Only Hospital");
                }

                return dicBRSDatabaseType;
            }
		}
		#endregion

		/// Name		: QuoteString
		/// Author		: Pankaj
		/// Date Created: 04/19/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// QuoteString
		/// </summary>
		/// <param name="p_sArg">String Argument</param>
		/// <returns>String value</returns>
		internal static string QuoteString(string p_sArg)
		{
			if (p_sArg==null || p_sArg == "")
				return "NULL";
			else
				return "'" +  p_sArg + "'";
		}

		/// Name		: GetCode
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the Code
		/// </summary>
		/// <param name="p_sCodeId">Code Id</param>
		/// <param name="p_sConString">Connection String</param>
		/// <returns>Code String</returns>
		internal static string GetCode(string p_sCodeId, string p_sConString,int p_iClientId)
		{
			LocalCache  objCache = null;
			string sShortCode = "";
			string sCodeDesc = ""; 
			try
			{
				int iCodeId = Conversion.ConvertStrToInteger(p_sCodeId);
                objCache = new LocalCache(p_sConString,p_iClientId);
				if(iCodeId != 0)
				{
					objCache.GetCodeInfo(iCodeId ,ref sShortCode,ref sCodeDesc);
				}
				objCache.Dispose();
				return sShortCode + " " + sCodeDesc;
			}
			finally
			{
				if(objCache!=null)
				{
					objCache.Dispose();
					objCache=null;
				}
			}
		}
         //25/12/2007 Abhishek, MITS 11097 - START
        /// <summary>
        /// Gets the Code for entity.. as entity is handled differently and Code details should be retrieved from glossary table
        /// </summary>
        /// <param name="p_sCodeId">Code Id</param>
        /// <param name="p_sConString">Connection String</param>
        /// <returns>Code String</returns>
        internal static string GetEntityCode(string p_sCodeId, string p_sConString,int p_iClientId)
        {
            LocalCache objCache = null;
            string sShortCode = "";
            string sCodeDesc = "";
            try
            {
                int iCodeId = Conversion.ConvertStrToInteger(p_sCodeId);
                objCache = new LocalCache(p_sConString, p_iClientId);
                if (iCodeId != 0)
                {
                    objCache.GetCodeInfoForEntity(iCodeId, ref sShortCode, ref sCodeDesc);
                }
                objCache.Dispose();
                return sShortCode + " " + sCodeDesc;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }
        //25/12/2007 Abhishek, MITS 11097 - END
		
		/// Name		: GetOrg
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Get Organisation
		/// </summary>
		/// <param name="p_sOrgId">Org Id</param>
		/// <param name="p_sConString">Connection String</param>
		/// <returns>Organisation Name</returns>
		internal static string GetOrg(string p_sOrgId, string p_sConString,int p_iClientId)
		{
			LocalCache  objCache = null;
			string sAbbr = "";
			string sDesc = ""; 
			try
			{
				int iOrgId = Conversion.ConvertStrToInteger(p_sOrgId);
				objCache = new LocalCache(p_sConString,p_iClientId);
				if(iOrgId != 0)
				{
					objCache.GetOrgInfo(iOrgId ,ref sAbbr,ref sDesc);
				}
				objCache.Dispose();
				return sAbbr + " " + sDesc;
			}
			finally
			{
				if(objCache != null)
				{
					objCache.Dispose();
					objCache=null;
				}
			}
		}

		/// Name		: GetState
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the State Details
		/// </summary>
		/// <param name="p_sStateId">State Id</param>
		/// <param name="p_sConString">Connection String</param>
		/// <returns>State Name</returns>
		internal static string GetState(string p_sStateId, string p_sConString,int p_iClientId)
		{
			LocalCache  objCache = null;
			string sCode = "";
			string sDesc = ""; 
			try
			{
				int iStateId = Conversion.ConvertStrToInteger(p_sStateId);
				objCache = new LocalCache(p_sConString,p_iClientId);
				if(iStateId != 0)
				{
					objCache.GetStateInfo(iStateId ,ref sCode,ref sDesc);
				}
				objCache.Dispose();
				return sCode + " " + sDesc;
			}
			finally
			{
				if(objCache != null)
				{
					objCache.Dispose();
					objCache=null;
				}
			}
		}

		/// Name		: GetNewElement
		/// Author		: Anurag Agarwal
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Creats a new XMLElement with the nodename passed as parameter
		/// </summary>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>new XMLElement</returns>
		internal static XmlElement GetNewElement(string p_sNodeName)
		{
			XmlDocument objXDoc;
			XmlElement objXMLElement;
			objXDoc = new XmlDocument();
			objXMLElement = objXDoc.CreateElement(p_sNodeName); 
			objXDoc = null; 
			return objXMLElement;
		}

		/// Name		: GetNewEleWithValue
		/// Author		: Anurag Agarwal
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Creats a new XMLElement with the nodename passed as parameter. 
		/// Also sets the Innertext to the new XMLElement
		/// </summary>
		/// <param name="p_sNewNodeName">New Node to be Created</param>
		/// <param name="p_sNodeValue">Value for the node</param>
		/// <returns>New XML Element with Innertext set</returns>
		internal static XmlElement GetNewEleWithValue(string p_sNewNodeName, string p_sNodeValue)
		{
			XmlDocument objXDoc;
			XmlElement objXMLElement;
			objXDoc = new XmlDocument();
			objXMLElement = objXDoc.CreateElement(p_sNewNodeName); 
			objXMLElement.InnerText = p_sNodeValue;  
			objXDoc = null;
			return objXMLElement;
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Save
		/// </summary>
		/// <param name="p_arrlstSql">Sql array</param>
		/// <param name="p_sDSN">dsn name</param>
		internal static void Save(ArrayList p_arrlstSql,string p_sDSN, int p_iClientId)
		{
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			try
			{
				if(p_arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(p_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < p_arrlstSql.Count; i++)
					{
						objCommand.CommandText=p_arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
					objTran=null;
				}
			}
			catch(RMAppException p_objEx)
			{

				//srajindersin MITS 28176
				if (objTran != null)
					objTran.Rollback();
				
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{

				//srajindersin MITS 28176
				if (objTran != null)
					objTran.Rollback();

                throw new RMAppException(Globalization.GetString("UTILITY.Save.Error", p_iClientId), p_objEx);
			}

			finally
			{
				if (objConn!=null)
				{
					objConn.Close();
				}
				objConn=null;
                if (objTran != null)
                    objTran.Dispose();
				objCommand=null;
			}
		}
        //anandpraka2 : Added for the MITS 26942
        internal static void Save(ArrayList p_arrlstSql, string p_sDSN, Dictionary<string, string> dictParams, int p_iClientId)
        {
            DbConnection objConn = null;
            DbTransaction objTran = null;
            DbCommand objCommand = null;
           
            try
            {
                if (p_arrlstSql.Count > 0)
                {
                    objConn = DbFactory.GetDbConnection(p_sDSN);
                    objConn.Open();
                    objCommand = objConn.CreateCommand();
                    objTran = objConn.BeginTransaction();
                    objCommand.Transaction = objTran;
                    objCommand.CommandType = CommandType.Text;
                    for (int i = 0; i < p_arrlstSql.Count; i++)
                    {
                      objCommand.CommandText = p_arrlstSql[i].ToString();
                      DbFactory.ExecuteNonQuery(objCommand, dictParams);
                      objCommand.Parameters.Clear(); //Aman MITS 27339
                    } 
                    
                    objTran.Commit();
                    objConn.Dispose();
                    objCommand = null;
                    objTran = null;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UTILITY.Save.Error", p_iClientId), p_objEx);
            }
            finally
            {
                
                if (objConn != null)
                {
                    objConn.Close();
                }
                objConn = null;
                if (objTran != null)
                    objTran.Dispose();
                objCommand = null;
            }
        }
        //anandpraka2 : End additions for the MITS 26942
	
		/// Name			: GetCodeList
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 3 Dec 2004		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets Code List
		/// </summary>
		/// <param name="p_sTableId">Table Id</param>
		/// <param name="p_sConnectString">Connection String</param>
		/// <returns>Dataset for Code List</returns>
        internal static DataSet GetCodeList(string p_sTableId, string p_sConnectString, int p_iClientId)
		{
			DataSet objDs=null;
			StringBuilder objSql=null;
			try
			{
				objSql=new StringBuilder();
				objSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT ");
				objSql.Append(" WHERE CODES.TABLE_ID ="+p_sTableId);
				objSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
				objSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 ");
				objSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ");
                objDs = DbFactory.GetDataSet(p_sConnectString, objSql.ToString(), p_iClientId);
				return objDs;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("UTILITY.GetCodeList.Error", p_iClientId), p_objEx);
			}
			finally
			{
				objSql=null;
				objDs=null;
			}
			
		}
		/// Name		: LoadListWithCodes
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 8 Apr 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Loads a combo xml element with the code and code text
		/// </summary>
		/// <param name="p_sConStr">Connection String</param>
		/// <param name="p_objElm">Xml Element</param>
		/// <param name="p_iTableId">Table Id</param>
		internal static void LoadListWithCodes(string p_sConStr, XmlDocument p_objXmlDoc, string p_sControlName , int p_iTableId, int p_iClientId)
		{			
			string sSQL="";
			DbReader objRdr = null;
			XmlElement objElm = null;
			XmlElement objChld = null;
            SysSettings objSysSettings = null;
            LocalCache objCache = null;
			try
			{
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='" + p_sControlName +"']");
				if(objElm ==null)
					return;
                
                
                sSQL = "SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC," +
                    "CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT WHERE CODES.TABLE_ID = " +
                    p_iTableId + " AND CODES.CODE_ID = CODES_TEXT.CODE_ID" +
                    " AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 AND CODES.CODE_ID IN ( ";
                
                //START rsushilaggar MITS 25137 Date 06/14/2011
                objCache = new LocalCache(p_sConStr,p_iClientId);
                objSysSettings = new SysSettings(p_sConStr, p_iClientId);
                if (objSysSettings.UseDILOB)
                {
                    sSQL = sSQL + objCache.GetCodeId("DI", "LINE_OF_BUSINESS") + ",";
                }
                if (objSysSettings.UseGCLOB)
                {
                    sSQL = sSQL + objCache.GetCodeId("GC", "LINE_OF_BUSINESS") + ",";
                }
                if (objSysSettings.UsePCLOB)
                {
                    sSQL = sSQL + objCache.GetCodeId("PC", "LINE_OF_BUSINESS") + ",";
                }
                if (objSysSettings.UseVALOB)
                {
                    sSQL = sSQL + objCache.GetCodeId("VA", "LINE_OF_BUSINESS") + ",";
                }
                if (objSysSettings.UseWCLOB)
                {
                    sSQL = sSQL + objCache.GetCodeId("WC", "LINE_OF_BUSINESS") + ",";
                }
                sSQL = sSQL + " 0) ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC";
                //END rsushilaggar MITS 25137 Date 06/14/2011              

                

				objRdr = DbFactory.GetDbReader(p_sConStr, sSQL);
				while(objRdr.Read())
				{
					objChld = p_objXmlDoc.CreateElement("option");
					objChld.SetAttribute("value", objRdr.GetInt32(0).ToString());
					objChld.InnerText = objRdr.GetString(1) + " - " + objRdr.GetString(2);
					objElm.AppendChild(objChld);
				}
			}
			finally
			{
				if (objRdr != null)
				{
					objRdr.Dispose();
					objRdr=null;
				}
				objElm = null;
				objChld = null;
			}
		}

		/// Name		: AddCode
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 8 Apr 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Adds code to the table
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
		/// <param name="p_sShortCode">Short Code</param>
		/// <param name="p_iNLSCd">NLS Code</param>
		/// <param name="p_sDesc">Description</param>
		/// <param name="p_sTblName">Table Name</param>
		/// <param name="p_iPrntTblId">Table Id</param>
		/// <returns>Code Id</returns>
        internal static int AddCode(string p_sConnStr, string p_sShortCode, int p_iNLSCd, string p_sDesc, string p_sTblName, int p_iPrntTblId, int p_iClientId)
		{
			int iCodeId=0;
			int iTableId=0;
			string sSQL="";
			bool bJustAddDesc=false;
			DbReader objRdr=null;
			LocalCache objCache=null;
			DbConnection objCon = null;
			DbTransaction objTran=null;
			DbCommand objCmd=null;

			try
			{
				objCache = new LocalCache(p_sConnStr,p_iClientId);
				//get table id of code table to add to
				iTableId = objCache.GetTableId(p_sTblName); 

				objCon =  DbFactory.GetDbConnection(p_sConnStr);
				objCon.Open(); 
				objCmd=objCon.CreateCommand();
				objTran=objCon.BeginTransaction();
				objCmd.Transaction=objTran;

				if(iTableId ==0)
                    throw new RMAppException(Globalization.GetString("UTILITY.AddCode.InvalidName", p_iClientId));

				if(p_sTblName.ToUpper()=="STATES")
				{
					// see if this code already exists
					sSQL = "SELECT STATE_ROW_ID FROM STATES WHERE STATE_ID = '" + p_sShortCode + "'";
					objRdr = objCon.ExecuteReader(sSQL);
					if(objRdr.Read())
					{
						return objRdr.GetInt32("STATE_ROW_ID");
					}
					objRdr.Dispose();

					//get a unique id for the state
                    iCodeId = Utilities.GetNextUID(p_sConnStr, "STATES", p_iClientId);
					if(iCodeId ==0)
                        throw new RMAppException(Globalization.GetString("UTILITY.AddCode.InvalidState", p_iClientId));

					//insert the values into the codes text table
					objCmd.CommandText  = "INSERT INTO STATES (STATE_ROW_ID, STATE_ID, STATE_NAME, DELETED_FLAG)" + 
						" VALUES (" + iCodeId  + ",'" +  "','" + p_sShortCode + "','" +  p_sDesc +  "', 0)";
					objCmd.ExecuteNonQuery();			

				}
				else
				{
					sSQL = "SELECT CODE_ID FROM CODES WHERE TABLE_ID = "  + iTableId  + " AND SHORT_CODE='" + p_sShortCode + "'";
					objRdr = DbFactory.GetDbReader(p_sConnStr, sSQL);

					if(objRdr.Read())
					{
						iCodeId = objRdr.GetInt32("CODE_ID"); 						
						objRdr.Dispose();
						sSQL= "SELECT CODE_ID FROM CODES_TEXT WHERE CODE_ID = " + iCodeId;
						objRdr= DbFactory.GetDbReader(p_sConnStr, sSQL);
						if(objRdr.Read())
						{
							return iCodeId;
						}
						else if(iCodeId ==0)
                            throw new RMAppException(Globalization.GetString("UTILITY.AddCode.InvalidCode", p_iClientId));
						else
							bJustAddDesc=true;
					}
				}
				if (!bJustAddDesc)
				{
                    iCodeId = Utilities.GetNextUID(p_sConnStr, "CODES", p_iClientId);
					if(iCodeId==0)
                        throw new RMAppException(Globalization.GetString("UTILITY.AddCode.CodeAssign", p_iClientId));

					objCmd.CommandText = "INSERT INTO CODES (CODE_ID,TABLE_ID,SHORT_CODE,RELATED_CODE_ID,DELETED_FLAG,LINE_OF_BUS_CODE)" + 
						" VALUES (" + iCodeId  +  "," + iTableId +  "," + "'" + p_sShortCode  + "'," + p_iPrntTblId + ",0,0)";
					objCmd.ExecuteNonQuery();  
				}

				objCmd.CommandText = "INSERT INTO CODES_TEXT (CODE_ID, LANGUAGE_CODE, SHORT_CODE, CODE_DESC)" +
					" VALUES (" + iCodeId  + "," + p_iNLSCd  + "," + 
					"'" + p_sShortCode + "','" + p_sDesc + "')";
				objCmd.ExecuteNonQuery();
				objTran.Commit(); 
				return iCodeId; 
			}
			catch(RMAppException p_objEx)
			{
				if(objTran!=null)
					objTran.Rollback(); 
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				if(objTran!=null)
					objTran.Rollback();
                throw new RMAppException(Globalization.GetString("UTILITY.AddCode.DataErr", p_iClientId), p_objEx);
			}
			finally
			{
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr=null;
				}
				if (objCon!=null)
				{
					objCon.Close();
					objCon=null;
				}
				if(objCache != null)
				{
					objCache.Dispose();
					objCache=null;
				}
				if (objTran!=null)
                    objTran.Dispose();
				objCmd=null;
				objCache=null;
			}
		}

        /// <summary>
        /// Determines whether the user is a member of a specified SQL
        /// Server role
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strRoleName">string containing the name of the role to verify membership</param>
        /// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal static bool IsSQLRoleMember(string strConnString, string strRoleName)
        {
            string SQL_ROLE_NAME = "sp_helprolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsDBO = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;
            //nadim
            const string SECURITYADMIN_ROLE = "securityadmin";
            const string SYSADMIN_ROLE = "sysadmin";
            //nadim

            //Parses out the User ID and Pwd from the database connection string
            UTILITY.GetUIdPwdFromConString(strConnString, ref strUserName, ref strPassword);

            //Get a DataReader object
            using (DbReader rdr = DbFactory.GetDbReader(strConnString, SQL_ROLE_NAME))
            {
                //Loop through all of the records
                while (rdr.Read())
                {
                    string strMemberName = rdr[MEMBER_NAME_COLUMN].ToString();

                    //in case of sa user , member name will be dbo
                    if (strUserName.ToLower() == "sa" && strMemberName.ToLower() == "dbo")
                    {
                        blnIsDBO = true;
                        break;
                    }
                    else
                    {
                        //Check if the member name matches
                        if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                        {
                            blnIsDBO = true;
                            break;
                        }//if
                    }//if
                }//while
            }//using
            //Nadim-To check if the user is having security admin or sysadmin permission-start
            if (blnIsDBO)
            {
                if (UTILITY.IsSQLServerRoleMember(strConnString, SYSADMIN_ROLE))
                {
                    blnIsDBO = true;                    
                }
                else if (UTILITY.IsSQLServerRoleMember(strConnString, SECURITYADMIN_ROLE))
                {
                     blnIsDBO = true;          
                }
                 else
                  {
                    blnIsDBO = false;
                   }
            }
            //Nadim-To check if the user is having security admin or sysadmin permission-end
            return blnIsDBO;
        }//method: IsSQLRoleMember()

        /// <summary>
        /// Determines whether the user is a member of a specified SQL
        /// Server server-level role
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strRoleName">string containing the name of the role to verify membership</param>
        /// <returns>boolean indicating whether or not the specified user is a role member</returns>
        internal static bool IsSQLServerRoleMember(string strConnString, string strRoleName)
        {
            string SQL_ROLE_NAME = "sp_helpsrvrolemember '" + strRoleName + "'";
            const string MEMBER_NAME_COLUMN = "MemberName";
            bool blnIsSysAdmin = false;
            string strUserName = string.Empty;
            string strPassword = string.Empty;

            //Parses out the User ID and Pwd from the database connection string
            UTILITY.GetUIdPwdFromConString(strConnString, ref strUserName, ref strPassword);

            //Get a DataReader object
            using (DbReader rdr = DbFactory.GetDbReader(strConnString, SQL_ROLE_NAME))
            {
                //Loop through all of the records
                while (rdr.Read())
                {
                    string strMemberName = rdr[MEMBER_NAME_COLUMN].ToString();

                    //Check if the member name matches
                    //Nadim removing Case sensitivity-start
                    if (strUserName.ToLower().Equals(strMemberName.ToLower()))
                    {
                        blnIsSysAdmin = true;
                        break;
                    }//if
                    //Nadim removing Case sensitivity-end
                }//while
            }//using

            return blnIsSysAdmin;
        }//method: IsSQLServerRoleMember()

        /// <summary>
        /// Determines whether or not the specified SQL Server platform
        /// is later than SQL Server 2000
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <returns>boolean indicating whether or not the database is a version
        /// later than SQL Server 2000</returns>
        internal static bool IsSQLServer2000(string strConnString)
        {
            double dblSQLVersion = 0.0;
            bool blnIsSQLServer2000 = false;
            const double SQL_DB_VERSION = 9.0;
            string strSQL = "SELECT SERVERPROPERTY('productversion') AS SQLVERSION";

            //create a database connection
            using (DbConnection dbConn = DbFactory.GetDbConnection(strConnString))
            {
                //Open the database connection
                dbConn.Open();

                DbCommand dbCmd = dbConn.CreateCommand();

                //Specify the SQL statement to execute against the database
                dbCmd.CommandText = strSQL;

                bool blnParsed = Double.TryParse(dbCmd.ExecuteScalar().ToString(), out dblSQLVersion);

                //If the current version is less than SQL 2005/2008
                if (blnParsed && (dblSQLVersion < SQL_DB_VERSION))
                {
                    blnIsSQLServer2000 = true;
                }//if
                
            }//using

            return blnIsSQLServer2000;
        }//method: IsSQLServer2000()

        /// <summary>
        /// Creates the appropriate string to determine how to create a login
        /// account in SQL server dependent on the version of SQL Server
        /// the application is currently using
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strUserID">string containing the user id to add to SQL Server</param>
        /// <returns>string containing the appropriate SQL statement based on the SQL server platform</returns>
        internal static string AddSQLLogin(string strConnString, string strUserID, string strPassword, string strDBName)
        {
            StringBuilder strAddLogin = new StringBuilder();
            const string SEPARATOR = ",";

            if (IsSQLServer2000(strConnString))
            {
                strAddLogin.Append("sp_addlogin ");
                strAddLogin.Append("'" + strUserID + "'");
                strAddLogin.Append(SEPARATOR);
                strAddLogin.Append("'" + strPassword + "'");
                strAddLogin.Append(SEPARATOR);
                strAddLogin.Append("'" + strDBName + "'");
            }//if
            else
            {
                strAddLogin.Append("CREATE LOGIN ");
                strAddLogin.Append(strUserID);
                strAddLogin.Append(" WITH PASSWORD =");
                strAddLogin.Append("'" + strPassword + "'");
            }//else

            return strAddLogin.ToString();
        }//method: AddSQLLogin()


        /// <summary>
        /// Creates the appropriate string to determine how to create a user
        /// account in SQL server dependent on the version of SQL Server
        /// the application is currently using
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strUserID">string containing the user id to add to SQL Server</param>
        /// <returns>string containing the appropriate SQL statement based on the SQL server platform</returns>
        internal static string AddSQLUser(string strConnString, string strUserID)
        {
            StringBuilder strAddUser = new StringBuilder();

            if (IsSQLServer2000(strConnString))
            {
                strAddUser.Append("sp_adduser ");
                strAddUser.Append(strUserID);
            }//if
            else
            {
                strAddUser.Append("CREATE USER ");
                strAddUser.Append(strUserID);
            }//else

            return strAddUser.ToString();
        }//method: AddSQLUser()

        //Changed by Nadim for BES(4/15/2009)-start

        /// <summary>
        /// In Sql Server 2005 and 2008 the Objects are associated with schema. So creating the Schema if it is 2005 or 08
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strUserID">string containing the user id to add to SQL Server</param>
        /// <returns>string containing the appropriate SQL statement for creation of schema based on the SQL server platform</returns>
        internal static string AddSQLSchema(string strConnString, string strUserID)
        {
            StringBuilder strAddSchema = new StringBuilder();

            if (IsSQLServer2000(strConnString))
            {
                strAddSchema.Append(" ");
            }
            else
            {
                strAddSchema.Append("CREATE SCHEMA ");
                strAddSchema.Append(strUserID);
                strAddSchema.Append(" ");
                strAddSchema.Append("AUTHORIZATION ");
                strAddSchema.Append(strUserID);

            }
            return strAddSchema.ToString();
        }
        //Changed by Nadim for BES-End
        //Changed by Nadim for BES(4/15/2009)-start   
       
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strUserID">string containing the user id to add to SQL Server</param>
        /// <returns>string containing the appropriate SQL statement for Adding schema </returns>
        internal static string AddSQLSchema2User(string strConnString, string strUserID)
        {
            StringBuilder strAddSchema2User = new StringBuilder();

            strAddSchema2User.Append("ALTER USER ");
            strAddSchema2User.Append(strUserID);
            strAddSchema2User.Append(" WITH DEFAULT_SCHEMA = ");
            strAddSchema2User.Append(strUserID);

            return strAddSchema2User.ToString();
        }
        //Changed By Nadim for BES-End
        /// <summary>
        /// Creates the appropriate string to determine how to execute as a 
        /// specified user account in SQL Server
        /// dependent on the version of SQL Server
        /// the application is currently using
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <param name="strUserID">string containing the user id to add to SQL Server</param>
        /// <returns>string containing the appropriate SQL statement based on the SQL server platform</returns>
        internal static string ExecuteAsSQLUser(string strConnString, string strUserID)
        {
            StringBuilder strExecuteAsUser = new StringBuilder();

            if (IsSQLServer2000(strConnString))
            {
                strExecuteAsUser.Append("SETUSER ");
                strExecuteAsUser.Append("'" + strUserID + "'");
            }//if
            else
            {
                //Changed by nadim for BES(4/15/2009)-start
               // strExecuteAsUser.Append("EXECUTE AS ");
                strExecuteAsUser.Append("EXECUTE AS USER=");
                //Changed by nadim for BES(4/15/2009)-End
                strExecuteAsUser.Append("'" + strUserID + "'");
            }//else

            return strExecuteAsUser.ToString();
        }//method: ExecuteAsSQLUser()

        /// <summary>
        /// Creates the appropriate string to revert permissions
        /// previously used for impersonating a particular user account in SQL Server        
        /// dependent on the version of SQL Server
        /// the application is currently using
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <returns>string containing the appropriate SQL statement based on the SQL server platform</returns>
        internal static string RevertSQLUser(string strConnString)
        {
            string strRevertUser = string.Empty;

            if (IsSQLServer2000(strConnString))
            {
                strRevertUser = "SETUSER";
            }//if
            else
            {
                strRevertUser = "REVERT";
            }//else

            return strRevertUser;
        }//method: RevertSQLUser()


		/// Name		: GetUIdPwdFromConString
		/// Author		:  Anurag
		/// Date Created: 04/19/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get UID and PWD
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
		/// <param name="p_sUId">User Id</param>
		/// <param name="p_sPwd">Password</param>
		internal static void GetUIdPwdFromConString(string p_sConnString, ref string p_sUId, ref string p_sPwd)
		{
			string sConnect = "";
			int iPos1 = 0;
			int iPos2 = 0;

			if(p_sConnString == "")
			{
				p_sUId = "";
				p_sPwd = "";
			}
			else
			{
				sConnect = p_sConnString;
				iPos1 = sConnect.IndexOf("UID=");
				iPos2 = sConnect.IndexOf(";",iPos1);
				p_sUId = sConnect.Substring(iPos1 + 4, iPos2 - iPos1 - 4);
				iPos1 = sConnect.IndexOf("PWD=");
				iPos2 = sConnect.IndexOf(";",iPos1);
				if(iPos2 == -1)
					iPos2 = sConnect.Length + 1; 
				p_sPwd = sConnect.Substring(iPos1 + 4, iPos2 - iPos1 - 4);
			}
		}

		/// Name		: GetXMLFromFile
		/// Author		: Anurag Agarwal
		/// Date Created: 05/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the XML from the file specified
		/// </summary>
		/// <param name="p_sFileName">File name</param>
		/// <returns>XML as string</returns>
		internal static string GetXMLFromFile(string p_sFileName)
		{
			string strContent;

			System.IO.StreamReader objReader = System.IO.File.OpenText(p_sFileName);
			strContent = objReader.ReadToEnd();
			objReader.Close();

			return strContent;
		}
		/// Name		: IsExtendedScreen
		/// Author		: Brian Battah
		/// Date Created: 05/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Determines whether the screen identified by screen name is
        /// currently configured with a CustomScreen in the Extensibility
        /// section of Riskmaster.config
		/// </summary>
		/// <returns>XmlDocument containing form detials</returns>
        internal static bool IsExtendedScreen(string p_sScreenName)
        {
            string sScreenName;
            if (p_sScreenName.IndexOf('|') != -1)
            {
                sScreenName = p_sScreenName;
            }
            else
            {
                sScreenName = System.IO.Path.GetFileNameWithoutExtension(p_sScreenName);
            }
            //TODO: Determine how Screen Extensions were previously managed
            return false;
        }
		/// Name		: AddSuppDefinition
		/// Author		: Anurag Agarwal
		/// Date Created: 05/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Adds the suppliments information
		/// </summary>
		/// <returns>XmlDocument containing form detials</returns>
		public static void AddSuppDefinition(string p_sUserId, string p_sPassword, string p_sConnString, string p_sDSNName, XmlDocument p_objXMLDocument, int p_iClientId) //mbahl3 JIRA [RMACLOUD-123]
		{
			string sTableName = "";
			DataModelFactory objDataModelFactory = null;
			SupplementalObject objSupp = null;
			XmlElement objSuppGrp = null;
			XmlDocument objSuppDoc= null;
			XmlElement objSec =null;
            string sLOBForPowerView = "";
			try
			{
				sTableName = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("supp");
                sLOBForPowerView = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("name");
                
                if(sTableName.Trim() == "")
					return; 

				objSuppDoc = new XmlDocument();
                objDataModelFactory = new DataModelFactory(p_sDSNName, p_sUserId, p_sPassword, p_iClientId);//mbahl3 JIRA [RMACLOUD-123]

				foreach(XmlNode objSection in p_objXMLDocument.SelectNodes("//section"))
				{
					switch(((XmlElement)objSection).GetAttribute("name"))
					{
						case "suppdata":
							objSupp = (Supplementals)objDataModelFactory.GetDataModelObject("Supplementals",false);
                            objSupp.LOBForPowerView = sLOBForPowerView;
							objSupp.TableName= sTableName; 
							break;
						case "jurisdata":
                            //Geeta 05/07/07 : Mits 9310 
							objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals",false);
//							objSupp.TableName= sTableName;
							break;
						case "acorddata":
							objSupp = (Acord)objDataModelFactory.GetDataModelObject("Acord",false);
							objSupp.TableName="CLAIM_ACCORD_SUPP";
							break;
					}
					if(objSupp!=null)
					{
						objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);  
						objSuppGrp = p_objXMLDocument.CreateElement("group");
						foreach(XmlAttribute  attr in objSuppDoc.SelectSingleNode("//group").Attributes)
						{
                            //Geeta 05/07/07 : Mits 9310 
                            if (attr.Value == "jurisgroup")
                            {
                                attr.Value = "pvjurisgroup";  
                            }  

                            objSuppGrp.SetAttribute(attr.Name,attr.Value);  
						}
                        if (sTableName == "FUNDS_TRANS_SPLIT_SUPP" && !string.IsNullOrEmpty(objSuppDoc.SelectSingleNode("//group").InnerXml))
                        {
                            string sTempInnerXML = objSuppDoc.SelectSingleNode("//group").InnerXml;
                            sTempInnerXML = sTempInnerXML.Replace("/Instance/*/Supplementals", "/option/Supplementals");
                            //rkulavil : RMA-10021 starts
                            //int iPos2 = 1;
                            //int iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                            //do
                            //{
                            //    iPos2 = sTempInnerXML.IndexOf('"', iPos);
                            //    string sTemp = sTempInnerXML.Substring(iPos, (iPos2 - iPos));
                            //    string sTemp2 = sTemp;
                            //    sTemp = sTemp.ToUpper();
                            //    sTempInnerXML = sTempInnerXML.Replace(sTemp2, sTemp);
                            //    iPos = iPos2;
                            //    iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                            //} while (iPos > 0);
                            //sTempInnerXML = sTempInnerXML.Replace("SUPP_", "");
                            //rkulavil : RMA-10021 ends

                            objSuppGrp.InnerXml = sTempInnerXML;
                        }
                        else
                        {
                            objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                        }
						p_objXMLDocument.SelectSingleNode("//form").InsertAfter(objSuppGrp, p_objXMLDocument.SelectSingleNode("//form/group[last()]"));
					}
				}
			}		
			finally
			{
				if(objSupp != null)
					objSupp.Dispose();
				if(objDataModelFactory != null)
					objDataModelFactory.Dispose();
				objSuppDoc=null;
				objSuppGrp = null;
				objSec =null;
			}
		}

        /// Name		: AddSuppDefinition
        /// Author		: Naresh Chandra Padhy
        /// Date Created: 05/20/2005		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Adds the supplements information without invoking the Scripts
        /// </summary>
        /// <returns>XmlDocument containing form details</returns> 
        public static void AddSuppDefinitionIgnoreScripts(string p_sUserId, string p_sPassword, string p_sConnString, string p_sDSNName, XmlDocument p_objXMLDocument, int p_iClientId)
        {
            string sTableName = "";
            DataModelFactory objDataModelFactory = null;
            SupplementalObject objSupp = null;
            XmlElement objSuppGrp = null;
            XmlDocument objSuppDoc = null;
            XmlElement objSec = null;
            string sLOBForPowerView = "";
            try
            {
                sTableName = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("supp");
                sLOBForPowerView = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("name");

                if (sTableName.Trim() == "")
                    return;

                objSuppDoc = new XmlDocument();
                objDataModelFactory = DataModelFactory.CreateDataModelFactory(p_sConnString, p_iClientId);

                foreach (XmlNode objSection in p_objXMLDocument.SelectNodes("//section"))
                {
                    switch (((XmlElement)objSection).GetAttribute("name"))
                    {
                        case "suppdata":
                            objSupp = (Supplementals)objDataModelFactory.GetDataModelObject("Supplementals", false);
                            objSupp.PowerViewUpgrade = true;
                            objSupp.LOBForPowerView = sLOBForPowerView;
                            objSupp.TableName = sTableName;
                            break;
                        case "jurisdata":
                            //Geeta 05/07/07 : Mits 9310 
                            objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals", false);
                            //							objSupp.TableName= sTableName;
                            break;
                        case "acorddata":
                            objSupp = (Acord)objDataModelFactory.GetDataModelObject("Acord", false);
                            objSupp.PowerViewUpgrade = true;
                            objSupp.TableName = "CLAIM_ACCORD_SUPP";
                            break;
                    }
                    if (objSupp != null)
                    {
                        objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);
                        objSuppGrp = p_objXMLDocument.CreateElement("group");
                        foreach (XmlAttribute attr in objSuppDoc.SelectSingleNode("//group").Attributes)
                        {
                            //Geeta 05/07/07 : Mits 9310 
                            if (attr.Value == "jurisgroup")
                            {
                                attr.Value = "pvjurisgroup";
                            }

                            objSuppGrp.SetAttribute(attr.Name, attr.Value);
                        }
                        
                        if (sTableName == "FUNDS_TRANS_SPLIT_SUPP" && !string.IsNullOrEmpty(objSuppDoc.SelectSingleNode("//group").InnerXml))
                        {
                            string sTempInnerXML = objSuppDoc.SelectSingleNode("//group").InnerXml;
                            sTempInnerXML = sTempInnerXML.Replace("/Instance/*/Supplementals", "/option/Supplementals");
                            //rkulavil : RMA-10021 starts
                            //int iPos2 = 1;
                            //int iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                            //do
                            //{
                            //    iPos2 = sTempInnerXML.IndexOf('"', iPos);
                            //    string sTemp = sTempInnerXML.Substring(iPos, (iPos2 - iPos));
                            //    string sTemp2 = sTemp;
                            //    sTemp = sTemp.ToUpper();
                            //    sTempInnerXML = sTempInnerXML.Replace(sTemp2, sTemp);
                            //    iPos = iPos2;
                            //    iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                            //} while (iPos > 0);
                            //sTempInnerXML = sTempInnerXML.Replace("SUPP_", "");
                            //rkulavil : RMA-10021 ends

                            objSuppGrp.InnerXml = sTempInnerXML;
                        }
                        else
                        {
                            objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                        }

                        p_objXMLDocument.SelectSingleNode("//form").InsertAfter(objSuppGrp, p_objXMLDocument.SelectSingleNode("//form/group[last()]"));
                    }
                }
            }
            finally
            {
                if (objSupp != null)
                    objSupp.Dispose();
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                objSuppDoc = null;
                objSuppGrp = null;
                objSec = null;
            }
        }
        /// Name		: AddSuppDefinitionPowerViewUpgrade
        /// Author		: 
        /// Date Created: 08/08/08		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Adds the suppliments information to the xml from net_view_forms
        /// </summary>
        /// <returns>XmlDocument containing form detials</returns>
        public static void AddSuppDefinitionPowerViewUpgrade(string p_sUserId, string p_sPassword, string p_sConnString, string p_sDSNName, XmlDocument p_objXMLDocument, bool bAdminTableList, out ArrayList retAdminTables, XmlDocument p_objXMLJuris, PowerViewMode CurrentMode, string sJurisTableName, UpgradeSection CurrentUpgrade, int p_iClientId)  //mbahl3 jira [RMACLOUD-127] 
        {
            DataModelFactory objDataModelFactory = null;
            SupplementalObject objSupp = null;
            XmlElement objSuppGrp = null;
            XmlDocument objSuppDoc = null;
            ADMTableList objList = null;
            ADMTable objAdmin = null;
            string sTableName = string.Empty, sLOBForPowerView = string.Empty;
            try
            {
                retAdminTables = null;
                objSuppDoc = new XmlDocument();
                //Here DSN is databasename, so commenting single parameter function CreateDataModelFactory
                objDataModelFactory = new DataModelFactory(p_sDSNName, p_sUserId, p_sPassword,p_iClientId);  //mbahl3 jira [RMACLOUD-127] 
                //objDataModelFactory = DataModelFactory.CreateDataModelFactory(p_sDSNName);
                if (bAdminTableList)
                {

                    objList = objDataModelFactory.GetDataModelObject("ADMTableList", false) as ADMTableList;
                    retAdminTables = objList.TableUserNamesForPV();  //averma62 MITS - 27770
                    return;
                }

                // Naresh: We do not want to execute this if the function is called from Jurisdictional
                // As we do not have to update the claimwc.xml we do not have p_objXMLDocument in that case
                if (CurrentMode != PowerViewMode.JURIS)
                {
                    sTableName = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("supp");
                    sLOBForPowerView = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("name");
                
                    if (sTableName == null)
                        return;
                    if (sTableName.Trim() == "")
                        return;



                    foreach (XmlNode objSection in p_objXMLDocument.SelectNodes("//section"))
                    {
                        switch (((XmlElement)objSection).GetAttribute("name"))
                        {

                            case "suppdata":
                                
                                    objSupp = (Supplementals)objDataModelFactory.GetDataModelObject("Supplementals", false);
                                    //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                    objSupp.PowerViewUpgrade = true;
                                    if ((CurrentMode == PowerViewMode.ALL || CurrentMode == PowerViewMode.SUPP) && (CurrentUpgrade == UpgradeSection.ALL || CurrentUpgrade == UpgradeSection.ONLYSUPP))
                                    {
                                        objSupp.LOBForPowerView = sLOBForPowerView;
                                        objSupp.TableName = sTableName;
                                    }

                                break;
                            case "jurisdata":

                                
                                    //Geeta 05/07/07 : Mits 9310 
                                    objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals", false);
                                    //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                    objSupp.PowerViewUpgrade = true;
                                    if (CurrentMode == PowerViewMode.ALL && (CurrentUpgrade == UpgradeSection.ALL || CurrentUpgrade == UpgradeSection.ONLYJURIS))
                                    {
                                        GetJurisData(p_sUserId, p_sPassword, p_sConnString, p_sDSNName, p_objXMLJuris, "", p_iClientId);
                                    }
                                break;
                            case "acorddata":
                                objSupp = (Acord)objDataModelFactory.GetDataModelObject("Acord", false);
                                //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                objSupp.PowerViewUpgrade = true;
                                objSupp.TableName = "CLAIM_ACCORD_SUPP";

                                break;
                            //Start by Shivendu to add admin table info to admintracking.xml at runtime
                            case "admdata":
                                objAdmin = (ADMTable)objDataModelFactory.GetDataModelObject("ADMTable", false);
                                //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                objAdmin.PowerViewUpgrade = true;
                                objAdmin.TableName = sTableName;
                                break;

                            //End by Shivendu to add admin table info to admintracking.xml at runtime
                        }
                        if (objAdmin != null)
                        {
                            objSuppDoc.LoadXml(objAdmin.ViewXml.OuterXml);
                            objSuppGrp = p_objXMLDocument.CreateElement("group");
                            foreach (XmlAttribute attr in objSuppDoc.SelectSingleNode("//group").Attributes)
                            {
                                //Geeta 05/07/07 : Mits 9310 
                                if (attr.Value == "jurisgroup")
                                {
                                    attr.Value = "pvjurisgroup";
                                }

                                objSuppGrp.SetAttribute(attr.Name, attr.Value);
                            }
                            objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                            p_objXMLDocument.SelectSingleNode("//form").InsertAfter(objSuppGrp, p_objXMLDocument.SelectSingleNode("//form/group[last()]"));
                        }
                        if (objSupp != null)
                        {
                            objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);
                            objSuppGrp = p_objXMLDocument.CreateElement("group");
                            foreach (XmlAttribute attr in objSuppDoc.SelectSingleNode("//group").Attributes)
                            {
                                //Geeta 05/07/07 : Mits 9310 
                                if (attr.Value == "jurisgroup")
                                {
                                    attr.Value = "pvjurisgroup";
                                }

                                objSuppGrp.SetAttribute(attr.Name, attr.Value);
                            }
                            if (sTableName == "FUNDS_TRANS_SPLIT_SUPP" && ! string.IsNullOrEmpty(objSuppDoc.SelectSingleNode("//group").InnerXml))
                            {
                                string sTempInnerXML = objSuppDoc.SelectSingleNode("//group").InnerXml;
                                sTempInnerXML = sTempInnerXML.Replace("/Instance/*/Supplementals", "/option/Supplementals");
                                 //rkulavil : RMA-10021 starts
								//int iPos2 = 1;
                                //int iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                                //do
                                //{
                                //    iPos2 = sTempInnerXML.IndexOf('"', iPos);
                                //    string sTemp = sTempInnerXML.Substring(iPos, (iPos2 - iPos));
                                //    string sTemp2 = sTemp;
                                //    sTemp = sTemp.ToUpper();
                                //    sTempInnerXML = sTempInnerXML.Replace(sTemp2, sTemp);
                                //    iPos = iPos2;
                                //    iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                                //} while (iPos > 0);
                                //sTempInnerXML = sTempInnerXML.Replace("SUPP_", "");
								 //rkulavil : RMA-10021 ends
								 
                                objSuppGrp.InnerXml = sTempInnerXML;
                            }
                            else
                            {
                                objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                            }
                            p_objXMLDocument.SelectSingleNode("//form").InsertBefore(objSuppGrp, p_objXMLDocument.SelectSingleNode("//form/section[last()]"));
                        }
                    }
                }
                else
                {
                    GetJurisData(p_sUserId, p_sPassword, p_sConnString, p_sDSNName, p_objXMLJuris, sJurisTableName, p_iClientId);
                }
            }
            finally
            {
                if (objSupp != null)
                    objSupp.Dispose();
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                objSuppDoc = null;
                objSuppGrp = null;
                if (objList != null)
                    objList.Dispose();
            }
        }

        /// <summary>
        /// Retrives the Data for Jurisdictions
        /// </summary>
        /// <param name="p_sUserId">The Current User Id</param>
        /// <param name="p_sPassword">Password for that User</param>
        /// <param name="p_sConnString">Connection String pointing to Page DB</param>
        /// <param name="p_sDSNName">Name of the DSN</param>
        /// <param name="p_objXMLJuris">The Juris Data will be Appended to this XmlDocument</param>
        /// <param name="sSuppTableName">If the Function is called from PowerviewUpgrade then this is blank, in which case it retrives Juris data
        /// for all the state. But if it is called from Utility, then it contains the name of the Juris for which upgrade needs to run.
        /// </param>
        private static void GetJurisData(string p_sUserId, string p_sPassword, string p_sConnString, string p_sDSNName, XmlDocument p_objXMLJuris, string sSuppTableName, int p_iClientId)
        {
            string sTableName = "";
            DataModelFactory objDataModelFactory = null;
            SupplementalObject objSupp = null;
            XmlDocument objSuppDoc = null;
            string sSql = string.Empty;
            string sStateShortName = string.Empty;
            int iStateId = 0;
            DataSet objJurisData = null;
            XmlElement xmJuris = null;
            XmlElement objSuppGrp = null;
            LocalCache objCache = null;
            try
            {
                objSuppDoc = new XmlDocument();
                //Here DSN is databasename, so commenting single parameter function CreateDataModelFactory
                objDataModelFactory = new DataModelFactory(p_sDSNName, p_sUserId, p_sPassword, p_iClientId);
                //objDataModelFactory = DataModelFactory.CreateDataModelFactory(p_sDSNName);

                // Get the List of JURIS Table 
                
                sSql = String.Format("SELECT DISTINCT SUPP_TABLE_NAME FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME LIKE 'WC%SUPP' AND SUPP_TABLE_NAME LIKE '{0}%'", sSuppTableName);
                objJurisData = DbFactory.GetDataSet(objDataModelFactory.Context.DbConn.ConnectionString, sSql, p_iClientId);
                if (objJurisData != null)
                {
                    objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals", false);
                    objSupp.PowerViewUpgrade = true;
                    xmJuris = p_objXMLJuris.CreateElement("JurisData");
                    p_objXMLJuris.AppendChild(xmJuris);
                    for(int i = 0; i< objJurisData.Tables[0].Rows.Count; i++)
                    {

                        sTableName = objJurisData.Tables[0].Rows[i]["SUPP_TABLE_NAME"].ToString();
                        objSupp.TableName = sTableName;
                        if (objSupp != null)
                        {
                            objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);
                            objSuppGrp = xmJuris.OwnerDocument.CreateElement("group");
                            sStateShortName = sTableName.Replace("WC_", "").Replace("_SUPP", "");
                            objCache = new LocalCache(objDataModelFactory.Context.DbConn.ConnectionString,p_iClientId);
                            iStateId = objCache.GetStateRowID(sStateShortName);
                            objSuppGrp.SetAttribute("StateId", iStateId.ToString());
                            objSuppGrp.SetAttribute("name", "jurisgroup");
                            objSuppGrp.SetAttribute("title", "Jurisdictionals");

                            xmJuris.AppendChild(objSuppGrp);
                            objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;  
                        }
                    }
                }

            }
            finally
            {
                if (objSupp != null)
                    objSupp.Dispose();
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                objSuppDoc = null;
                objSuppGrp = null;
                xmJuris = null;
            }
        }

		/// Name		: IsFieldExists
		/// Author		: Anurag Agarwal
		/// Date Created	: 19 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Checks for the input field existance in the passed list of columns
		/// </summary>
		/// <param name="p_objDBReader">Data Reader object containing the recordset</param>
		/// <param name="p_sFieldName">Field Name to be check</param>
		/// <returns>Flag saying that whether field exist or not</returns>
        internal static bool IsFieldExists(DbReader p_objDBReader, string p_sFieldName, int p_iClientId)
		{
			DataTable objDataTable=null;
            try
            {
                if (p_objDBReader != null)
                {
                    objDataTable = p_objDBReader.GetSchemaTable();
                    if (objDataTable != null)
                    {
                        foreach (DataColumn objDataColumn in objDataTable.Columns)
                        {
                            if (objDataColumn.ColumnName == p_sFieldName)
                                return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UTILITY.IsFieldExists.CompareError", p_iClientId), p_objEx);
            }
            finally
            {
                if (objDataTable != null)
                    objDataTable.Dispose();
            }
		}

        internal static bool IsFieldExists(DataSet p_objDataSet, string p_sFieldName, int p_iClientId)
        {
            DataTable objDataTable=null;
            try
            {
                if (p_objDataSet.Tables[0] != null)
                {
                    objDataTable = p_objDataSet.Tables[0]; 
                    if (objDataTable != null)
                    {
                        foreach (DataColumn objDataColumn in objDataTable.Columns)
                        {
                            if (objDataColumn.ColumnName == p_sFieldName)
                                return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("UTILITY.IsFieldExists.CompareError", p_iClientId), p_objEx);
            }
            finally
            {
                if (objDataTable != null)
                    objDataTable.Dispose();
            }
        }
		/// Name		: GetUser
		/// Author		: Anurag Agarwal
		/// Date Created	: 27 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// This function gets the user name from loginname.
		/// </summary>
		/// <param name="p_sUserId">User ID</param>
		/// <param name="p_sDsnId">DSN ID</param>
		/// <returns>User Details</returns>
		internal static string GetUser(int p_sUserId, string p_sDsnId, string p_sSecurityDSN, int p_iClientId)
		{
			
			DbReader objRdr = null;
			string sLastName = string.Empty;
			string sFirstName = string.Empty;
			string sSQL = string.Empty;
			string sUser = "";
 
			try
			{
				sSQL = "SELECT USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME,USER_DETAILS_TABLE.LOGIN_NAME FROM " + 
					"USER_DETAILS_TABLE,USER_TABLE WHERE USER_DETAILS_TABLE.DSNID = " + p_sDsnId + " AND " + 
					"USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.USER_ID =" + p_sUserId;
				objRdr=DbFactory.GetDbReader(p_sSecurityDSN ,sSQL);
				if (objRdr!=null)
				{
					if(objRdr.Read())
					{
						sLastName = objRdr["LAST_NAME"].ToString();
						sFirstName = objRdr["FIRST_NAME"].ToString();
						sUser = sLastName;
						if(sFirstName != "")
						{	
							if(sLastName != "")
								sUser += ", ";
							sUser +=  sFirstName;
						}

						sUser += "  (Login Name: " + objRdr["LOGIN_NAME"].ToString() + ")";
					}
					objRdr.Close();
				}

				return sUser.Trim();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("UTILITY.GetUser.ErrorGet", p_iClientId), p_objEx);				
			}
			finally
			{
				if(objRdr != null)
					objRdr.Dispose();
			}
		}

        /// <summary>
        /// Gets the NameValueCollection of all Utility configuration settings
        /// </summary>
        /// <returns>NameValueCollection containing all of the available RISKMASTER Utility
        /// configuration settings</returns>
        internal static NameValueCollection GetConfigUtilitySettings(string p_sConnStr, int p_iClientId)
        {
            return RMConfigurationManager.GetNameValueSectionSettings("RMUtilities",p_sConnStr, p_iClientId);
        } // method: GetConfigUtilitySettings

        /// <summary>
        /// Gets the specified AppFiles subdirectory
        /// </summary>
        /// <param name="strAppFilesDirectory">string containing the name of the AppFiles subdirectory</param>
        /// <returns>string containing the path to the AppFiles directory</returns>
        internal static string GetAppFilesDirectory(string strAppFilesDirectory)
        {
            return RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, strAppFilesDirectory);
        } // method: GetAppFilesDirectory



		/// Name		: GetClientInfo
		/// Author		: Anurag Agarwal
		/// Date Created	: 30 May 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Returns the Client Info from Sys Parms table
		/// </summary>
		/// <param name="p_sConnectionString">Connection String</param>
		/// <returns>Client Info</returns>
		internal static string GetClientInfo(string p_sConnectionString, int p_iClientId)
		{
			SysSettings objSysSetting = null;
			string sClientInfo = "";
			string sCRLF = @"\r\n";

            objSysSetting = new SysSettings(p_sConnectionString, p_iClientId);
			sClientInfo = objSysSetting.ClientName + sCRLF;
			sClientInfo += objSysSetting.Addr1 + sCRLF;
			sClientInfo += objSysSetting.Addr2 + sCRLF;
            //sClientInfo += objSysSetting.Addr3 + sCRLF; // JIRA 6420 syadav55
            //sClientInfo += objSysSetting.Addr4 + sCRLF; // JIRA 6420 syadav55
			sClientInfo += objSysSetting.City + ", " + objSysSetting.State + " " + objSysSetting.ZipCode + sCRLF;
			sClientInfo += objSysSetting.PhoneNumber;
 
			return sClientInfo;
		}

		internal static string FormatDate(string p_sDate, bool p_bIsSave)
		{
			if(p_sDate.Trim() == "" || p_sDate == "0")
				return "";
			if(p_bIsSave)
			{
				return Conversion.GetDate(p_sDate);
			}
			else
			{
				return Conversion.ToDate(Conversion.GetDate(p_sDate)).ToShortDateString();
			}
		}

		/// Name		: CamelCase
		/// Author		: Pankaj Chowdhury
		/// Date Created	: 28 June 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Returns Camel Case String
		/// </summary>
		/// <param name="p_sInpStr">Input String</param>
		/// <returns>Camel Case string</returns>
		internal static string CamelCase(string p_sInpStr)
		{
			return p_sInpStr.Substring(0,1).ToUpper() + p_sInpStr.Substring(1,p_sInpStr.Length-1).ToLower();      
		}
        /// <summary>
        /// Parijat:Boeing - Broker Support
        /// Checks if the user has Enhanced BES turned on for SQL Server
        /// </summary>
        /// <param name="strConnString">string containing the database connection string</param>
        /// <returns>boolean indicating whether or not the user has turned on Enhanced BES</returns>
        /// <remarks>In order to turn on Enhanced BES for SQL Server, you will need to execute the 
        /// following code on the RISKMASTER database:
        /// ALTER TABLE SYS_PARMS ADD ENH_BES int NULL
        /// UPDATE SYS_PARMS
        /// SET ENH_BES = 1
        /// </remarks>
        internal static bool IsEnhancedBES(string strConnString)
        {
            //Nadim --return  ENH_BES setting
            string sSQL= "SELECT * FROM SYS_PARMS";
            int intENH_BES = 0;
            bool blnIsEnhancedBES = false;
         
            using (DbReader dbRdr = DbFactory.GetDbReader(strConnString, sSQL))
            {
                //Get the schema information from the query
                //DataTable dtSchema = dbRdr.GetSchemaTable();
                //Nadim
                //Verify that the columns exists for Enhanced BES
                //if (dtSchema.Columns.Contains(ENHANCED_BES))
                //{
                //    Int32.TryParse(dbRdr[ENHANCED_BES].ToString(), out intENH_BES);

                //    //If Enhanced BES has been turned on
                //    if (intENH_BES != 0)
                //    {
                //        blnIsEnhancedBES = true;
                //    }//if
                //}//if
                if (dbRdr != null)
                {
                    if (dbRdr.Read())
                    {
                        intENH_BES = dbRdr.GetInt32("ENH_BES");
                        if (intENH_BES == 1)
                            blnIsEnhancedBES = true;
                        else
                            blnIsEnhancedBES = false;
                    }
                }
                //Nadim --return  ENH_BES setting
            }//using

            return blnIsEnhancedBES;
        }//method: IsEnhancedBES()

        /// Name		: GetEmpDetails
        /// Author		: Animesh Sahai
        /// Date Created: 11/24/2009		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************

        /// <summary>
        /// Returns the name of the employee in the format "Lastname, Firstname"
        /// </summary>
        /// <param name="p_sCodeId">Entity Id</param>
        /// <param name="p_sConString">Connection string</param>
        /// <returns>Employee name</returns>
        internal static string GetEmpDetails(string p_sCodeId, string p_sConString, int p_iClientId)
        {
            string sSQL = string.Empty;
            string strEmpDetails = string.Empty;
            DbReader objReader = null;
            try
            {
                sSQL = "SELECT LAST_NAME,FIRST_NAME FROM ENTITY " +
                        "WHERE ENTITY_ID =" + p_sCodeId;
                objReader = DbFactory.GetDbReader(p_sConString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        strEmpDetails = Conversion.ConvertObjToStr(objReader["LAST_NAME"]);
                        if ((Conversion.ConvertObjToStr(objReader["LAST_NAME"])!= "") && (Conversion.ConvertObjToStr(objReader["FIRST_NAME"])!= ""))
                        {
                            strEmpDetails = strEmpDetails + ", ";
                        }
                        strEmpDetails = (strEmpDetails + Conversion.ConvertObjToStr(objReader["FIRST_NAME"])).Trim();
                    }
                }
                else
                {
                    strEmpDetails = "";
                }

                objReader.Close();
                return strEmpDetails;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjusterGetError", p_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
        }

        /// Name		: GetOrgName
        /// Author		: Animesh sahai
        /// Date Created	: 14 Jan 2008		
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// Get Organisation
        /// </summary>
        /// <param name="p_sOrgId">Org Id</param>
        /// <param name="p_sConString">Connection String</param>
        /// <returns>Organisation Name without abbrevation</returns>
        internal static string GetOrgName(string p_sOrgId, string p_sConString,int p_iClientId)
        {
            LocalCache objCache = null;
            string sAbbr = "";
            string sDesc = "";
            try
            {
                int iOrgId = Conversion.ConvertStrToInteger(p_sOrgId);
                objCache = new LocalCache(p_sConString,p_iClientId);
                if (iOrgId != 0)
                {
                    objCache.GetOrgInfo(iOrgId, ref sAbbr, ref sDesc);
                }
                objCache.Dispose();
                return sDesc;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
            }
        }

        /// Name		: GetXmlMultiCodeInfo
        /// Author		: Nima Norouzi
        /// Date Created: May 2010		
        /// ************************************************************
        /// <summary>
        /// Accepts multi codes as sValue and adds code descriptions(based on type attribute in objXmlElement) as sub-XmlElements to objXmlElement. objXmlElement can be then bound to our multicode user control.
        /// </summary>
        /// <param name="objXmlElement">ref xmlElement to which sub-xmlElements are added</param>
        /// <param name="objXmlDoc">ref xmlDocument of the current context</param>
        /// <param name="sValue">Containing multi codes separated by space</param>
        internal static void GetXmlMultiCodeInfo(ref XmlElement objXmlElement, ref XmlDocument objXmlDoc, string sValue, ref LocalCache objLocalCache, int p_iClientId)
        {
            StringBuilder serializedStr = new StringBuilder();
            XmlElement objTempXmlElement = null;
            string[] splitItems = null;
            string sLookupType = string.Empty;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            if (!string.IsNullOrEmpty(sValue))
            {
                sLookupType = objXmlElement.GetAttribute("lookuptype");
                objXmlElement.SetAttribute("codeid", sValue);
                sValue = sValue.Trim(); // Added by bsharma33 for MITS 28245
                splitItems = sValue.Split((char)32);
                for (int i = 0; i < splitItems.Length; i++)
                {
                    if (serializedStr.Length > 0)
                    {
                        serializedStr.Remove(0, serializedStr.Length);
                    }
                    SwitchLookupTypeMultiCode(sLookupType, Riskmaster.Common.Conversion.ConvertObjToInt(splitItems[i], p_iClientId), ref sShortCode, ref sDesc, ref objLocalCache);
                    if (sDesc == string.Empty)
                    {
                        serializedStr.Append(sShortCode);
                    }
                    else
                    {
                        serializedStr.Append(String.Format("{0} {1}", sShortCode, sDesc));
                    }

                    objTempXmlElement = objXmlDoc.CreateElement("Types");
                    //objTempXmlElement.SetAttribute("name", sShortCode);
                    objTempXmlElement.SetAttribute("value", splitItems[i]);
                    objTempXmlElement.InnerText = serializedStr.ToString();

                    objXmlElement.AppendChild(objTempXmlElement);

                }
            }
        }

        /// Name		: GetMultiCodeInfo
        /// Author		: Nima Norouzi
        /// Date Created: May 2010		
        /// ************************************************************
        /// <summary>
        /// Accepts multi codes as sValue and returns code descriptions(based on sType) separetad by comma.
        /// </summary>
        /// <param name="sType">The type, code descriptions are retrieved based on</param>
        /// <param name="sValue">Containing multi codes separated by space</param>
        /// <returns>returns code descriptions(based on sType) separetad by comma</returns>
        internal static string GetMultiCodeInfo(string sType, string sValue, ref LocalCache objLocalCache, int p_iClientId)
        {
            StringBuilder serializedStr = new StringBuilder();
            string[] splitItems = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            if (!string.IsNullOrEmpty(sValue))
            {
                sValue = sValue.Trim(); // Added by bsharma33 for MITS 28245
                splitItems = sValue.Split((char)32);
                for (int i = 0; i < splitItems.Length; i++)
                {
                    SwitchLookupTypeMultiCode(sType, Riskmaster.Common.Conversion.ConvertObjToInt(splitItems[i], p_iClientId), ref sShortCode, ref sDesc, ref objLocalCache);
                    if (sDesc == string.Empty)
                    {
                        sShortCode = sShortCode.Replace(" ", "_"); //in case of orgh we replace spaces with under lines so it does not get wrapped in the grid and it will be readable.
                    }
                    if (serializedStr.Length > 0)
                    {
                        serializedStr.Append(String.Format(", {0}", sShortCode));
                    }
                    else
                    {
                        serializedStr.Append(String.Format("{0}", sShortCode));
                    }
                }
            }
            return serializedStr.ToString();
        }

        internal static void SwitchLookupTypeMultiCode(string sLookupType, int iCode, ref string sShortCode, ref string sDesc, ref LocalCache objLocalCache)
        {
            switch (sLookupType.ToUpper())
            {
                case "LINE_OF_BUSINESS":
                case "CLAIM_TYPE":
                    objLocalCache.GetCodeInfo(iCode, ref sShortCode, ref sDesc);
                    break;
                case "STATES":
                    objLocalCache.GetStateInfo(iCode, ref sShortCode, ref sDesc);
                    break;
                case "ORGH":
                    objLocalCache.GetOrgInfo(iCode, ref sShortCode, ref sDesc);
                    sShortCode = sDesc;
                    sDesc = string.Empty;
                    break;
                default:
                    sShortCode = string.Empty;
                    sDesc = string.Empty;
                    break;
            }
        }

	}


	/// <summary>
	/// Class is used to work with DataRows
	/// </summary>
	internal class DbRow
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		internal DbRow(){}

		/// Name		: GetStringField
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the String value of the column in the Data Row
		/// </summary>
		/// <param name="p_objRow">Data Row</param>
		/// <param name="p_sColumnName">Column Name to be extracted</param>
		/// <returns>String representation of the Column</returns>
		internal static string GetStringField(DataRow p_objRow, string p_sColumnName)
		{
			if (p_objRow[p_sColumnName].ToString()=="")
				return "";
			else
				return Convert.ToString(p_objRow[p_sColumnName]) ; 
		}

		/// Name		: GetIntField
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the Integer value of the column in the Data Row
		/// </summary>
		/// <param name="p_objRow">Data Row</param>
		/// <param name="p_sColumnName">Column Name to be extracted</param>
		/// <returns>Numeric representation of the Column</returns>
		internal static int GetIntField(DataRow p_objRow, string p_sColumnName)
		{
			if (p_objRow[p_sColumnName].ToString()=="" )
				return 0;
			else
				return Convert.ToInt32(p_objRow[p_sColumnName]); 
		}

		/// Name		: GetLngField
		/// Author		: Pankaj Chowdhury
		/// Date Created: 11/26/2004		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Returns the Long value of the column in the Data Row
		/// </summary>
		/// <param name="p_objRow">Data Row</param>
		/// <param name="p_sColumnName">Column Name to be extracted</param>
		/// <returns>Numeric representation of the Column</returns>
		internal static long GetLngField(DataRow p_objRow, string p_sColumnName)
		{
			if (p_objRow[p_sColumnName].ToString()=="" )
				return 0;
			else
				return Convert.ToInt64(p_objRow[p_sColumnName]); 
		}
	}
}
