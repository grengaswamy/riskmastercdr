using System;
using System.Data;
using System.Xml;
using Riskmaster.Db;
using System.Text;
using System.Globalization;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	/// Summary description for JurisPreparerInfo.
	/// </summary>
	public class JurisPreparerInfo
	{
        private int m_iClientId = 0;
		public JurisPreparerInfo(string userId,string connectionString, int p_iClientId)
		{
			m_userId=userId;
			m_connectionString=connectionString;
            m_iClientId = p_iClientId;
			//
			// TODO: Add constructor logic here
			//
		}
		private enum JurisOptions
		{
			iNoFieldData,iNoJurisOptions,iJurisOptions,iNoRecord //14/3 added for no records found case
		}

		private string m_userId;
		private string m_connectionString;
		private int iSkipPrompt;

		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			
			try
			{
				XmlDocument xml = GetUserPref();
				XmlNode node = xml.SelectSingleNode("//JurisOptions");
				if( node != null)
				{
					p_objXmlDocument.SelectSingleNode("//JurisPreparer/preparerPhone").InnerText	=	node.Attributes.GetNamedItem("phone").InnerText;
					p_objXmlDocument.SelectSingleNode("//JurisPreparer/preparerName").InnerText		=	node.Attributes.GetNamedItem("name").InnerText;
					p_objXmlDocument.SelectSingleNode("//JurisPreparer/preparerTitle").InnerText	=	node.Attributes.GetNamedItem("title").InnerText;
					p_objXmlDocument.SelectSingleNode("//JurisPreparer/rememberInfo").InnerText		=	node.Attributes.GetNamedItem("skipprompt").InnerText;
				}
				return p_objXmlDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("JurisPreparerInfo.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				
			}
		}

		private XmlDocument GetUserPref()
		{
			XmlDocument xml  = new XmlDocument();
			XmlNode node  =null;
			try
			{
				string sTemp;
				iSkipPrompt = 0;
				string sName = string.Empty;
				string sTitle = string.Empty;
				string sPhone = string.Empty;
				string sXMLText = string.Empty;
				switch (XMLExists(ref sXMLText))
				{
					case JurisOptions.iNoFieldData:
					{
						break;
					}
					case JurisOptions.iJurisOptions:
					{
						xml = new XmlDocument();
						xml.LoadXml( sXMLText );
						node = xml.SelectSingleNode("//JurisOptions");
						sTemp =	node.Attributes.GetNamedItem("skipprompt").InnerText.Trim();
						if( sTemp == "")
						{
							iSkipPrompt = -2;
						}
						else
						{
							iSkipPrompt = Conversion.ConvertStrToInteger( node.Attributes.GetNamedItem("skipprompt").InnerText);
						}
						break;
					}
					case JurisOptions.iNoJurisOptions:
					{
						break;
					}
					default:
					{
						break;
					}	
				}
				return xml;
			
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("JurisPreparerInfo.Get.Error", m_iClientId),p_objEx);
			}
			finally
			{
				xml  =null;
				node  =null;
			}
		}

		private JurisPreparerInfo.JurisOptions XMLExists(ref string sXMLText)
		{
			string sSQL;
			sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + m_userId;
			DbReader objReader=null;
			bool bRecords = false;
			try
			{
				objReader = DbFactory.GetDbReader(m_connectionString,sSQL);				
				while( objReader.Read() )
				{
					bRecords = true;
					sXMLText += objReader.GetValue(0).ToString();
				}
				
				if (!bRecords) return JurisOptions.iNoRecord;

				if ( sXMLText == "" )
				{
					return JurisOptions.iNoFieldData;
				}
							
				if ( sXMLText.ToUpper().IndexOf("JurisOptions".ToUpper())!=-1)
				{
					return JurisOptions.iJurisOptions;
				}
				else
					return JurisOptions.iNoJurisOptions;
			}
			catch( RMAppException p_objException )
			{
				throw p_objException;
			}
			catch(Exception p_objException)
			{			
				throw p_objException;
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Dispose();
                }
			}
		}
	
		public bool Save(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			string sName = string.Empty;
			string sTitle = string.Empty;
			string sPhone = string.Empty;
			string sRememberInfo = string.Empty;
			string sXMLText = string.Empty;
			
			string sSQL="";
			DbConnection objConn = null;																				
			DbCommand objCommand=null;
            DbParameter objParam = null;                    //Umesh
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//JurisPreparer/preparerName"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sName=objNode.InnerText;
						//sName=sName.Replace("'","''");  ////MITS_8257  by Umesh
				}
				objNode=p_objXmlDocument.SelectSingleNode("//JurisPreparer/preparerTitle"); 
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sTitle=objNode.InnerText;
						//sTitle=sTitle.Replace("'","''");  //MITS_8257  by Umesh
				}
				objNode = p_objXmlDocument.SelectSingleNode("//JurisPreparer/preparerPhone");  
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sPhone=objNode.InnerText;
				}
				objNode = p_objXmlDocument.SelectSingleNode("//JurisPreparer/rememberInfo");  
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
						sRememberInfo=objNode.InnerText;
				}
				XmlDocument xml = new XmlDocument();
                objConn = DbFactory.GetDbConnection(m_connectionString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                // insert/update using SQL command  using parameter : Umesh
                objParam = objCommand.CreateParameter();
                objParam.Direction = ParameterDirection.Input;
                objParam.ParameterName = "XML";
                objParam.SourceColumn = "PREF_XML";
				string sXml=string.Empty;
				switch ( XMLExists(ref sXml))
				{
					//3/14 sumit ideal case
					case JurisOptions.iJurisOptions:
					{
						xml.LoadXml(sXml);
						XmlElement objElem = (XmlElement) xml.SelectSingleNode("//JurisOptions");
						objElem.Attributes.GetNamedItem("phone").InnerText=	sPhone;
						objElem.Attributes.GetNamedItem("name").InnerText=	sName;
						objElem.Attributes.GetNamedItem("title").InnerText=	sTitle;
						objElem.Attributes.GetNamedItem("skipprompt").InnerText = sRememberInfo;
						//sSQL = "UPDATE USER_PREF_XML SET PREF_XML="+"'" + xml.OuterXml + "'" +" WHERE USER_ID = " + m_userId;
                        objParam.Value = xml.OuterXml;
                        sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID = " + m_userId;
						break;
					}
					case JurisOptions.iNoJurisOptions:
					{
						//3/14 sumit we dont have options but we have other things
						xml.LoadXml(sXml);
						XmlElement objElem=xml.CreateElement("JurisOptions");							
						objElem.Attributes.Append(xml.CreateAttribute("phone"));
						objElem.Attributes.Append(xml.CreateAttribute("name"));
						objElem.Attributes.Append(xml.CreateAttribute("title"));
						objElem.Attributes.Append(xml.CreateAttribute("skipprompt"));
						objElem.Attributes.GetNamedItem("phone").InnerText=	sPhone;
						objElem.Attributes.GetNamedItem("name").InnerText=	sName;
						objElem.Attributes.GetNamedItem("title").InnerText=	sTitle;
						objElem.Attributes.GetNamedItem("skipprompt").InnerText = sRememberInfo;
						xml.SelectSingleNode("//setting").AppendChild(objElem);							
						//sSQL = "UPDATE USER_PREF_XML SET PREF_XML="+ "'"+ xml.OuterXml + "'" +" WHERE USER_ID = " + m_userId;
                        objParam.Value = xml.OuterXml;
                        sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID = " + m_userId;
						break;
					}
					case JurisOptions.iNoFieldData:
					{
						//3/14 sumit - we have a blank record for this user 
						sXml = CreateUserPrefXmlForJurisPreparer(sPhone,sRememberInfo,sTitle,sName).OuterXml;
						//sSQL = "UPDATE USER_PREF_XML SET PREF_XML="+ "'"+ sXml + "'"+"WHERE USER_ID = " + m_userId;
                        objParam.Value = sXml;
                        sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~  WHERE USER_ID = " + m_userId;
						break;
					}
					case JurisOptions.iNoRecord:
					{
						//3/14 sumit - we have no record for this user at all so its an insert case
						sXml = CreateUserPrefXmlForJurisPreparer(sPhone,sRememberInfo,sTitle,sName).OuterXml;
						//sSQL = "INSERT INTO USER_PREF_XML(PREF_XML,USER_ID) VALUES(" + "'"+ sXml  + "',"+ m_userId +")";
                        objParam.Value = sXml;
                        sSQL = "INSERT INTO USER_PREF_XML(PREF_XML,USER_ID) VALUES( ~XML~," + m_userId + ")";
						break;
					}
					
				}
			
                //objConn = DbFactory.GetDbConnection(m_connectionString);
                //objConn.Open();
                //objCommand=objConn.CreateCommand();
                
                //PJS MITS 10924, 12-10-07 - added parameter to command object
                objCommand.Parameters.Add(objParam);
                objCommand.CommandText=sSQL;
				objCommand.ExecuteNonQuery();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("JurisPreparerInfo.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
                if(objConn != null)
				    objConn.Dispose(); 
				objCommand=null;
                objParam = null;  
			}
			return true;

		}

		/// <summary>
		/// Used to create the user pref xml for juris preparer
		/// </summary>
		/// <param name="sPhone">phone num of preparer</param>
		/// <param name="sRememberInfo">Checkbox on UI for prompt</param>
		/// <param name="sTitle">title of preparer</param>
		/// <param name="sName">name of preparer</param>
		/// <returns>Xmldocument containing the structure for preparer</returns>
		private XmlDocument CreateUserPrefXmlForJurisPreparer(string sPhone,string sRememberInfo,string sTitle,string sName)
		{
			XmlDocument xml = new XmlDocument();
			XmlNode rootNode = xml.CreateElement("setting");
			XmlNode childNode = xml.CreateElement("JurisOptions");
			childNode.Attributes.Append(xml.CreateAttribute("phone"));
			childNode.Attributes.Append(xml.CreateAttribute("name"));
			childNode.Attributes.Append(xml.CreateAttribute("title"));
			childNode.Attributes.Append(xml.CreateAttribute("skipprompt"));						
			childNode.Attributes.GetNamedItem("phone").InnerText=	sPhone;
			childNode.Attributes.GetNamedItem("name").InnerText=	sName;
			childNode.Attributes.GetNamedItem("title").InnerText=	sTitle;
			childNode.Attributes.GetNamedItem("skipprompt").InnerText = sRememberInfo;
			rootNode.AppendChild(childNode);
			xml.InnerXml = rootNode.OuterXml;
			return xml;
		}

	}
	
}
