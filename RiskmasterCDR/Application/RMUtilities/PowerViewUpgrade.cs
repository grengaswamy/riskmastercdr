﻿using System;
using System.Xml;
using System.Text;
using System.Xml.Xsl;
using System.IO;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;
using System.Collections;

namespace Riskmaster.Application.RMUtilities
{
    #region PowerViewUpgrade XSL transformations
    public partial class PowerViewUpgrade
    {
        private string XslFileTab = @"form2Tab.xslt";
        private string XslFileTopDown = @"form2.xslt";
        private string XslFileTabReadOnly = @"form2Tabreadonly.xslt";
        private string XslFileTopDownReadOnly = @"form2readonly.xslt";
        private string XslControls = @"controls.xsl";

        //rsolanki2: PvUpgrade performance updates
        private XslCompiledTransform XctFileTab = new XslCompiledTransform();
        private XslCompiledTransform XctFileTopDown = new XslCompiledTransform();
        private XslCompiledTransform XctFileTabReadOnly = new XslCompiledTransform();
        private XslCompiledTransform XctFileTopDownReadOnly = new XslCompiledTransform();
        private XslCompiledTransform XctControls = new XslCompiledTransform();

        private string GroupAssocMapping = @"RMSUPPGA.XML";
        private string sXslBasePath = string.Empty;
        private string m_ViewConnString = string.Empty;//Parijat : conn string for fetching up of jurisdictional data

        private const char m_GAFieldValueDelimiter = ':'; 
        private const char m_GAServerControlNameDelimiter = ',';
        private const string m_GAFieldDelimiter = ";";
        private int m_iClientId = 0;//mbahl3 JIRA [RMACLOUD-123]

        public PowerViewUpgrade(int p_iClientId) //mbahl3  JIRA [RMACLOUD-123]
        {
            sXslBasePath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "PowerViewUpgrade" );
            m_iClientId = p_iClientId; //mbahl3  JIRA [RMACLOUD-123]
            if (! string.IsNullOrEmpty(sXslBasePath))
            {
                //rsolanki2: PvUpgrade performance updates
                XslFileTab = Path.Combine(sXslBasePath, XslFileTab);
                if (!File.Exists(XslFileTab))
                {
                    throw new FileNotFoundException("Xsl file Not found : " + XslFileTab);
                }

                XslFileTopDown = Path.Combine(sXslBasePath, XslFileTopDown);
                if (!File.Exists(XslFileTopDown))
                {
                    throw new FileNotFoundException("Xsl file Not found : " + XslFileTopDown);
                }

                XslFileTabReadOnly = Path.Combine(sXslBasePath, XslFileTabReadOnly);
                if (!File.Exists(XslFileTabReadOnly))
                {
                    throw new FileNotFoundException("Xsl file Not found : " + XslFileTabReadOnly);
                }

                XslFileTopDownReadOnly = Path.Combine(sXslBasePath, XslFileTopDownReadOnly);
                if (!File.Exists(XslFileTopDownReadOnly))
                {
                    throw new FileNotFoundException("Xsl file Not found : " + XslFileTopDownReadOnly);
                }

                XslControls = Path.Combine(sXslBasePath, XslControls);
                if (!File.Exists(XslControls))
                {
                    throw new FileNotFoundException("Xsl file Not found : " + XslControls);
                }

                XctFileTab.Load(XslFileTab);
                XctFileTopDown.Load(XslFileTopDown);
                XctFileTabReadOnly.Load(XslFileTabReadOnly);
                XctFileTopDownReadOnly.Load(XslFileTopDownReadOnly);
                XctControls.Load(XslControls);
            }

            m_ViewConnString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId);//mbahl3   JIRA [RMACLOUD-123]    //Parijat : conn string for fetching up of jurisdictional data

        }

        public bool UpgradeXmlToAspx(bool bReadOnly, bool bTopDown, XmlDocument viewXml, string pageName, out StringBuilder convertedAspx)
        {
            //XslCompiledTransform xsl = new XslCompiledTransform();
            XslTransform xsl = new XslTransform(); //MITS 27898
            XsltArgumentList xslArg = new XsltArgumentList();
            StringWriter sw = new StringWriter();
            string[] aspxPageName = new string[2];
            string sHtml = "";
            try
            {
                XmlDocument objDom = viewXml;
                XmlNode objNode = objDom.SelectSingleNode("//form");
                if (objNode.Attributes["formtype"] != null)
                {
                    if (objNode.Attributes["formtype"].Value.ToLower() == "gridpopup")
                    {
                        if (!bTopDown && !bReadOnly)
                        {
                            //xsl.Load(XslFileTab);
                            xslArg.AddParam("divClass", "", "full");
                        }
                        else if (!bTopDown && bReadOnly)
                        {
                            //xsl.Load(XslFileTabReadOnly);
                            xslArg.AddParam("divClass", "", "full");
                        }
                        else if (bTopDown && !bReadOnly)
                        {
                            //xsl.Load(XslFileTopDown);
                            xslArg.AddParam("divClass", "", "full");
                        }
                        else if (bTopDown && bReadOnly)
                        {
                            //xsl.Load(XslFileTopDownReadOnly);
                            xslArg.AddParam("divClass", "", "full");
                        }
                    }
                }
                // load xslt to do transformation
                else if (pageName.IndexOf("sentinel") > -1 && !bReadOnly)
                {
                    //xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "sentinel");
                }
                else if (pageName.IndexOf("sentinel") > -1 && bReadOnly)
                {
                    //xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "sentinel");
                }
                // load xslt to do transformation

                // load xslt to do transformation
                else if (pageName.IndexOf("admintracking") > -1 && !bReadOnly)
                {
                    //xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "full");
                }
                else if (pageName.IndexOf("admintracking") > -1 && bReadOnly)
                {
                    //xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "full");
                }
                // load xslt to do transformation
                else if (bTopDown && !bReadOnly)
                {
                    //xsl.Load(XslFileTopDown);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && !bReadOnly)
                {
                    //xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "half");

                }
                else if (bTopDown && bReadOnly)
                {
                    //xsl.Load(XslFileTopDownReadOnly);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && bReadOnly)
                {
                    //xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "half");

                }

                // Call the function to remove the special characters from the XML
                ProcessXml(ref viewXml);

                // Get tab names and put into a hidden field
                GetTabNames(ref viewXml);

                // replace group association control field with actual field name
                GetGroupAssocFieldNames(ref viewXml);

                // get transformed results
                if (!bTopDown && !bReadOnly)
                {
                    XctFileTab.Transform(viewXml, xslArg, sw);
                }
                else if (!bTopDown && bReadOnly)
                {                   
                    XctFileTabReadOnly.Transform(viewXml, xslArg, sw);
                }
                else if (bTopDown && !bReadOnly)
                {   
                    XctFileTopDown.Transform(viewXml, xslArg, sw);
                }
                else if (bTopDown && bReadOnly)
                { 
                    XctFileTopDownReadOnly.Transform(viewXml, xslArg, sw);
                }
                aspxPageName = pageName.Split('.');
                convertedAspx = new StringBuilder("");
                convertedAspx.Append("<%@ Page Language=\"C#\" AutoEventWireup=\"true\" CodeBehind=\"");
                if (pageName.IndexOf("admintracking") > -1)
                {
                    convertedAspx.Append("admintracking");
                }
                else
                {
                    convertedAspx.Append(aspxPageName[0]);
                }
                convertedAspx.Append(".aspx.cs");
                //convertedAspx.Append("\" Inherits=\"Power_Editor.WebForm1\" %>");
                if (pageName.IndexOf("admintracking") > -1)
                {
                    convertedAspx.Append("\"  Inherits=\"Riskmaster.UI.FDM.Admintracking" 
                        + "\" ValidateRequest=\"false\" %>");
                }
                else
                {
                    convertedAspx.Append("\"  Inherits=\"Riskmaster.UI.FDM." 
                        + aspxPageName[0].Substring(0, 1).ToUpper() 
                        + aspxPageName[0].Substring(1) 
                        + "\" ValidateRequest=\"false\" %>");
                }
                //convertedAspx.Append("<%@ Register assembly=\"AjaxControlToolkit\" namespace=\"AjaxControlToolkit\" tagprefix=\"cc1\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/CodeLookUp.ascx\" tagname=\"CodeLookUp\" tagprefix=\"uc\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/UserControlDataGrid.ascx\" tagname=\"UserControlDataGrid\" tagprefix=\"dg\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/MultiCodeLookup.ascx\" tagname=\"MultiCode\" tagprefix=\"uc\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/PleaseWaitDialog.ascx\" tagname=\"PleaseWaitDialog\" tagprefix=\"uc\" %>");
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/SystemUsers.ascx\" tagname=\"SystemUsers\" tagprefix=\"cul\" %>");
                convertedAspx.Append("<%@ Register Assembly=\"MultiCurrencyCustomControl\" Namespace=\"MultiCurrencyCustomControl\"  TagPrefix=\"mc\" %>");//Deb
                convertedAspx.Append("<%@ Register src=\"~/UI/Shared/Controls/MultiSystemUsers.ascx\" tagname=\"MultiSystemUsers\" tagprefix=\"msu\" %>"); //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                convertedAspx.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                sHtml = sw.ToString().Replace("&amp;", "&");
                //Raman: 08/13/2009: If top down layout is selected then we can make all control classes to be completerow wherever it is half
                //MITS 17476 and 17479
                if (bTopDown)
                {
                    sHtml = sHtml.Replace("class=\"half\"", "class=\"completerow\"");
                }
                convertedAspx.Append(sHtml);
                // free up the memory of objects that are not used anymore
                sw.Close();
                return true;

            }

            catch (FileLoadException e)
            {
                throw new FileLoadException("A File Load exception has occured in upgrading the screen " + pageName + e.Message, e);

            }

            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("File not found has occured in upgrading the screen " + pageName + e.Message, e);

            }
            catch (IOException e)
            {
                throw new IOException("An IO exception has occured in upgrading the screen " + pageName + e.Message, e);

            }
            catch (Exception e)
            {
                throw new Exception("An exception has occured in upgrading the screen " + pageName + e.Message, e);

            }
            finally
            {
                //if (xsl != null)
                //{
                //    xsl = null;
                //}
                if (sw != null)
                {
                    sw.Dispose();
                }
            }
        }
        /// <summary>
        /// Function for converting any xml tag in form of Aspx controls
        /// created by- Parijat, for dynamic Controls.
        /// </summary>
        /// <param name="bReadOnly">If the converted should be in form of read only.</param>
        /// <param name="bTopDown">If it has to be in form of top down layout.</param>
        /// <param name="viewXml">Xml document which needs to be converted.</param>
        /// <param name="divName">Any name Of div tag which is being converted</param>
        /// <param name="convertedAspx">The converted result in the form of ASPX controls.</param>
        /// <returns>bool</returns>
        public bool UpgradeXmlTagToAspxForm(bool bReadOnly, bool bTopDown, XmlDocument viewXml, string divName, out StringBuilder convertedAspx)
        {
            //XslCompiledTransform xsl = new XslCompiledTransform();
            XsltArgumentList xslArg = new XsltArgumentList();
            StringWriter sw = new StringWriter();
            string[] aspxPageName = new string[2];
            string sHtml = "";
            try
            {
                // load xslt to do transformation
                if (bTopDown && !bReadOnly)
                {
                    //xsl.Load(XslFileTopDown);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && !bReadOnly)
                {
                    //xsl.Load(XslFileTab);
                    xslArg.AddParam("divClass", "", "half");

                }
                else if (bTopDown && bReadOnly)
                {
                    //xsl.Load(XslFileTopDownReadOnly);
                    xslArg.AddParam("divClass", "", "full");

                }
                else if (!bTopDown && bReadOnly)
                {
                    //xsl.Load(XslFileTabReadOnly);
                    xslArg.AddParam("divClass", "", "half");

                }

                // Call the function to remove the special characters from the XML
                ProcessXml(ref viewXml);

                // get transformed results
                //xsl.Transform(viewXml, xslArg, sw);


                //rsolanki2 : PvUpgrade performace updates.
                if (!bTopDown && !bReadOnly)
                {
                    XctFileTab.Transform(viewXml, xslArg, sw);
                }
                else if (!bTopDown && bReadOnly)
                {
                    XctFileTabReadOnly.Transform(viewXml, xslArg, sw);
                }
                else if (bTopDown && !bReadOnly)
                {
                    XctFileTopDown.Transform(viewXml, xslArg, sw);
                }
                else if (bTopDown && bReadOnly)
                {
                    XctFileTopDownReadOnly.Transform(viewXml, xslArg, sw);
                }
                convertedAspx = new StringBuilder("");
                sHtml = sw.ToString().Replace("&amp;", "&");
                convertedAspx.Append(sHtml);
                // free up the memory of objects that are not used anymore
                sw.Close();
                return true;

            }

            catch (FileLoadException e)
            {
                throw new FileLoadException("A File Load exception has occured in upgrading the division " + divName + e.Message, e);

            }

            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("File not found has occured in upgrading the division " + divName + e.Message, e);

            }
            catch (IOException e)
            {
                throw new IOException("An IO exception has occured in upgrading the division " + divName + e.Message, e);

            }
            catch (Exception e)
            {
                throw new Exception("An exception has occured in upgrading the division " + divName + e.Message, e);

            }
            finally
            {
                //if (xsl != null)
                //{
                //    xsl = null;
                //}
                if (sw != null)
                {
                    sw.Dispose();
                }
            }
        }
        /// <summary>
        /// Function for fetching of Jurisdictional data in form of Aspx stored in the database
        /// created by- Parijat, for dynamic Controls. --Jurisdictional
        /// </summary>
        /// <param name="stateID"></param>
        /// <param name="jurisdictionalAspx"></param>
        /// <returns></returns>
        //Raman 12/31/2010 : Making this method as static so as to avoid constructor being called as it loads XSL's
        //and has been identified as a performance bottleneck
        public static bool FetchJurisdictionalData(int stateID, int p_iDsnid ,  out string jurisdictionalAspx, int p_iClientId)
        {
            StringBuilder sbSql = null;
            DbReader objReader = null;
            //Raman 12/31/2010 : R7 Performance Improvement
            string sViewConnString = String.Empty;
            try
            {
                sbSql = new StringBuilder();
                //Raman 12/31/2010 : R7 Performance Improvement
                sViewConnString = RMConfigurationManager.GetConnectionString("ViewDataSource", p_iClientId);
                jurisdictionalAspx = String.Empty;
                sbSql.Append("SELECT PAGE_CONTENT FROM JURIS_VIEWS WHERE STATE_ID =" + stateID.ToString() + " AND DATA_SOURCE_ID = " + p_iDsnid.ToString());
                using (objReader = DbFactory.GetDbReader(sViewConnString, sbSql.ToString()))
                {
                    if (objReader != null)
                    {
                        if (objReader.Read())
                        {
                            jurisdictionalAspx = objReader["PAGE_CONTENT"].ToString();
                        }
                    }
                }

                return true;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException("An error occured while fetching jurisdictional data.", p_objEx);
            }
            finally
            {

                sbSql = null;
                if (objReader != null)
                    objReader.Dispose();
            }
        }

        public bool UpgradeXmlTagToAspxFormJuris(bool bReadOnly, bool bTopDown, XmlDocument viewXml, string divName, out StringBuilder convertedAspx)
        {
            //Mits 16129 :Asif Start
            string strJuris = string.Empty;
            string strFinal = string.Empty;
            XmlDocument xmldoc = viewXml;
            XmlNodeList xnodelist = xmldoc.SelectNodes("//group/displaycolumn/control[@required='yes']");
            foreach (XmlNode xnode in xnodelist)
            {
                 strJuris = xnode.SelectSingleNode("@name").Value;
                if (xnode.Attributes["type"].Value == "code")
                {
                    strJuris = strJuris + "_codelookup";
                }
                strFinal = strFinal + "|" + strJuris;
            }
            if (strFinal != "")
            {
                strFinal = strFinal.Remove(0, 1);
            }
            XmlElement objElemTemp = null;
            objElemTemp = xmldoc.CreateElement("internal");
            objElemTemp.SetAttribute("type", "hidden");
            objElemTemp.SetAttribute("name", "sysRequied_juris");
            objElemTemp.SetAttribute("value",strFinal);
            xmldoc.FirstChild.AppendChild(objElemTemp);
            //Mits 16129 :Asif End
            //XslCompiledTransform xsl = new XslCompiledTransform();
            XsltArgumentList xslArg = new XsltArgumentList();
            StringWriter sw = new StringWriter();
            string[] aspxPageName = new string[2];
            string sHtml = "";

            try
            {
                // load xslt to do transformation

                //xsl.Load(sXslBasePath + "/controls.xsl");
                xslArg.AddParam("divClass", "", "completrow");

                // Call the function to remove the special characters from the XML
                ProcessXml(ref viewXml);

                // get transformed results
                //xsl.Transform(viewXml, xslArg, sw);
                
                //rsolanki2: PvUpgrade performance updates.
                XctControls.Transform(viewXml, xslArg, sw);
                convertedAspx = new StringBuilder("");
                sHtml = sw.ToString().Replace("&amp;", "&");
                convertedAspx.Append(sHtml);
                // free up the memory of objects that are not used anymore
                sw.Close();
                return true;
            }
            catch (FileLoadException e)
            {
                throw new FileLoadException("A File Load exception has occured in upgrading the division " + divName + e.Message, e);
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("File not found during has occured in upgrading the division " + divName + e.Message, e);
            }
            catch (IOException e)
            {
                throw new IOException("An IO exception has occured in upgrading the division " + divName + e.Message, e);
            }
            catch (Exception e)
            {
                throw new Exception("An exception has occured in upgrading the division " + divName + e.Message, e);
            }
            finally
            {
                //if (xsl != null)
                //{
                //    xsl = null;
                //}
                if (sw != null)
                {
                    sw.Dispose();
                }
            }
        }
    }//partial class PowerViewUpgrade 
    #endregion

    #region PowerViewUpgrade View Definition Upgrades
    public partial class PowerViewUpgrade
    {
        /// Name		: AddSuppDefinitionPowerViewUpgrade
        /// Author		: 
        /// Date Created: 08/08/08		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Adds the suppliments information to the xml from net_view_forms
        /// </summary>
        /// <returns>XmlDocument containing form detials</returns>
        public static void UpgradeSupplementalFields(string p_sConnString, XmlDocument p_objXMLDocument, XmlDocument p_objXMLJuris, PowerViewMode CurrentMode, string sJurisTableName, UpgradeSection CurrentUpgrade, int p_iClientId)
        {
            DataModelFactory objDataModelFactory = null;
            SupplementalObject objSupp = null;
            XmlElement objSuppGrp = null;
            XmlDocument objSuppDoc = null;
            ADMTableList objList = null;
            ADMTable objAdmin = null;
            string sTableName = string.Empty, sLOBForPowerView = string.Empty;
            try
            {
                objSuppDoc = new XmlDocument();
                objDataModelFactory = DataModelFactory.CreateDataModelFactory(p_sConnString, p_iClientId);

                // Naresh: We do not want to execute this if the function is called from Jurisdictional
                // As we do not have to update the claimwc.xml we do not have p_objXMLDocument in that case
                if (CurrentMode != PowerViewMode.JURIS)
                {
                    sTableName = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("supp");
                    sLOBForPowerView = ((XmlElement)p_objXMLDocument.GetElementsByTagName("form")[0]).GetAttribute("name");

                    if (sTableName == null)
                        return;
                    if (sTableName.Trim() == "")
                        return;



                    foreach (XmlNode objSection in p_objXMLDocument.SelectNodes("//section"))
                    {
                        switch (((XmlElement)objSection).GetAttribute("name"))
                        {

                            case "suppdata":

                                objSupp = (Supplementals)objDataModelFactory.GetDataModelObject("Supplementals", false);
                                //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                objSupp.PowerViewUpgrade = true;
                                if ((CurrentMode == PowerViewMode.ALL || CurrentMode == PowerViewMode.SUPP) && (CurrentUpgrade == UpgradeSection.ALL || CurrentUpgrade == UpgradeSection.ONLYSUPP))
                                {
                                    objSupp.LOBForPowerView = sLOBForPowerView;
                                    objSupp.TableName = sTableName;
                                }

                                break;
                            case "jurisdata":


                                //Geeta 05/07/07 : Mits 9310 
                                objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals", false);
                                //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                objSupp.PowerViewUpgrade = true;
                                if (CurrentMode == PowerViewMode.ALL && (CurrentUpgrade == UpgradeSection.ALL || CurrentUpgrade == UpgradeSection.ONLYJURIS))
                                {
                                    UpgradeJurisdictionalData(p_sConnString, p_objXMLJuris, "", p_iClientId);
                                }
                                break;
                            case "acorddata":
                                objSupp = (Acord)objDataModelFactory.GetDataModelObject("Acord", false);
                                //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                objSupp.PowerViewUpgrade = true;
                                objSupp.TableName = "CLAIM_ACCORD_SUPP";

                                break;
                            //Start by Shivendu to add admin table info to admintracking.xml at runtime
                            case "admdata":
                                objAdmin = (ADMTable)objDataModelFactory.GetDataModelObject("ADMTable", false);
                                //Added by Shivendu to avoid scripting processing when LoadTableDefinition is called from PowerviewUpgrade tool
                                objAdmin.PowerViewUpgrade = true;
                                objAdmin.TableName = sTableName;
                                break;

                            //End by Shivendu to add admin table info to admintracking.xml at runtime
                        }
                        if (objAdmin != null)
                        {
                            objSuppDoc.LoadXml(objAdmin.ViewXml.OuterXml);
                            objSuppGrp = p_objXMLDocument.CreateElement("group");
                            foreach (XmlAttribute attr in objSuppDoc.SelectSingleNode("//group").Attributes)
                            {
                                //Geeta 05/07/07 : Mits 9310 
                                if (attr.Value == "jurisgroup")
                                {
                                    attr.Value = "pvjurisgroup";
                                }

                                objSuppGrp.SetAttribute(attr.Name, attr.Value);
                            }
                            objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                            p_objXMLDocument.SelectSingleNode("//form").InsertAfter(objSuppGrp, p_objXMLDocument.SelectSingleNode("//form/group[last()]"));
                        }
                        if (objSupp != null)
                        {
                            objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);
                            objSuppGrp = p_objXMLDocument.CreateElement("group");
                            foreach (XmlAttribute attr in objSuppDoc.SelectSingleNode("//group").Attributes)
                            {
                                //Geeta 05/07/07 : Mits 9310 
                                if (attr.Value == "jurisgroup")
                                {
                                    attr.Value = "pvjurisgroup";
                                }

                                objSuppGrp.SetAttribute(attr.Name, attr.Value);
                            }
                            if (sTableName == "FUNDS_TRANS_SPLIT_SUPP" && !string.IsNullOrEmpty(objSuppDoc.SelectSingleNode("//group").InnerXml))
                            {
                                string sTempInnerXML = objSuppDoc.SelectSingleNode("//group").InnerXml;
                                sTempInnerXML = sTempInnerXML.Replace("/Instance/*/Supplementals", "/option/Supplementals");
                                
                                //rkulavil : RMA-10021 starts
                                //int iPos2 = 1;
                                //int iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                                //do
                                //{
                                //    iPos2 = sTempInnerXML.IndexOf('"', iPos);
                                //    string sTemp = sTempInnerXML.Substring(iPos, (iPos2 - iPos));
                                //    string sTemp2 = sTemp;
                                //    sTemp = sTemp.ToUpper();
                                //    sTempInnerXML = sTempInnerXML.Replace(sTemp2, sTemp);
                                //    iPos = iPos2;
                                //    iPos = sTempInnerXML.IndexOf("supp_", iPos2);
                                //} while (iPos > 0);
                                //sTempInnerXML = sTempInnerXML.Replace("SUPP_", "");
                                //rkulavil : RMA-10021 ends

                                objSuppGrp.InnerXml = sTempInnerXML;
                            }
                            else
                            {
                                objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                            }
                            p_objXMLDocument.SelectSingleNode("//form").InsertBefore(objSuppGrp, p_objXMLDocument.SelectSingleNode("//form/section[last()]"));
                        }
                    }
                }
                else
                {
                    UpgradeJurisdictionalData(p_sConnString, p_objXMLJuris, sJurisTableName, p_iClientId);
                }
            }
            finally
            {
                if (objSupp != null)
                    objSupp.Dispose();
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                objSuppDoc = null;
                objSuppGrp = null;
                if (objList != null)
                    objList.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ArrayList GetAdminTables(string p_sConnString, int p_iClientId)
        {
            ArrayList retAdminTables = new ArrayList();
            DataModelFactory objDataModelFactory = DataModelFactory.CreateDataModelFactory(p_sConnString, p_iClientId);

            ADMTableList objList = objDataModelFactory.GetDataModelObject("ADMTableList", false) as ADMTableList;
            retAdminTables = objList.TableUserNames();

            //Clean up
            objDataModelFactory.Dispose();

            return retAdminTables;
        } // method: GetAdminTables


        /// <summary>
        /// Retrives the Data for Jurisdictions
        /// </summary>
        /// <param name="p_sConnString">Connection String pointing to Page DB</param>
        /// <param name="p_objXMLJuris">The Juris Data will be Appended to this XmlDocument</param>
        /// <param name="sSuppTableName">If the Function is called from PowerviewUpgrade then this is blank, in which case it retrives Juris data
        /// for all the state. But if it is called from Utility, then it contains the name of the Juris for which upgrade needs to run.
        /// </param>
        private static void UpgradeJurisdictionalData(string p_sConnString, XmlDocument p_objXMLJuris, string sSuppTableName, int p_iClientId)
        {
            string sTableName = "";
            DataModelFactory objDataModelFactory = null;
            SupplementalObject objSupp = null;
            XmlDocument objSuppDoc = null;
            string sSql = string.Empty;
            string sStateShortName = string.Empty;
            int iStateId = 0;
            DataSet objJurisData = null;
            XmlElement xmJuris = null;
            XmlElement objSuppGrp = null;
            LocalCache objCache = null;
            try
            {
                objSuppDoc = new XmlDocument();
                objDataModelFactory = DataModelFactory.CreateDataModelFactory(p_sConnString, p_iClientId);

                // Get the List of JURIS Table 

                sSql = String.Format("SELECT DISTINCT SUPP_TABLE_NAME FROM SUPP_DICTIONARY WHERE SUPP_TABLE_NAME LIKE 'WC%SUPP' AND SUPP_TABLE_NAME LIKE '{0}%'", sSuppTableName);
                objJurisData = DbFactory.GetDataSet(objDataModelFactory.Context.DbConn.ConnectionString, sSql, p_iClientId);
                if (objJurisData != null)
                {
                    objSupp = (Jurisdictionals)objDataModelFactory.GetDataModelObject("Jurisdictionals", false);
                    objSupp.PowerViewUpgrade = true;
                    xmJuris = p_objXMLJuris.CreateElement("JurisData");
                    p_objXMLJuris.AppendChild(xmJuris);
                    for (int i = 0; i < objJurisData.Tables[0].Rows.Count; i++)
                    {

                        sTableName = objJurisData.Tables[0].Rows[i]["SUPP_TABLE_NAME"].ToString();
                        objSupp.TableName = sTableName;
                        if (objSupp != null)
                        {
                            objSuppDoc.LoadXml(objSupp.ViewXml.OuterXml);
                            objSuppGrp = xmJuris.OwnerDocument.CreateElement("group");
                            sStateShortName = sTableName.Replace("WC_", "").Replace("_SUPP", "");
                            objCache = new LocalCache(objDataModelFactory.Context.DbConn.ConnectionString,p_iClientId);
                            iStateId = objCache.GetStateRowID(sStateShortName);
                            objSuppGrp.SetAttribute("StateShortName", sStateShortName);
                            objSuppGrp.SetAttribute("StateId", iStateId.ToString());
                            objSuppGrp.SetAttribute("name", "jurisgroup");
                            objSuppGrp.SetAttribute("title", "Jurisdictionals");

                            xmJuris.AppendChild(objSuppGrp);
                            objSuppGrp.InnerXml = objSuppDoc.SelectSingleNode("//group").InnerXml;
                        }
                    }
                }

            }
            finally
            {
                if (objSupp != null)
                    objSupp.Dispose();
                if (objDataModelFactory != null)
                    objDataModelFactory.Dispose();
                objSuppDoc = null;
                objSuppGrp = null;
                xmJuris = null;
            }
        }

        /// <summary>
        /// This function removes the Special Characters (#,@ etc) from the name attribute of the Controls 
        /// </summary>
        /// <param name="p_objInputXml">The Xml From which the special characters has to be removed</param>
        private void ProcessXml(ref XmlDocument p_objInputXml)
        {
            string[] arrSpecialChar = new string[] { "#", "@" };

            string sSpecialCharCondition = string.Empty;
            string sReplaceChar = string.Empty;
            bool bFirstTime = true;
            foreach (string strChar in arrSpecialChar)
            {
                if (bFirstTime)
                {
                    sSpecialCharCondition += " and";
                }
                else
                {
                    sSpecialCharCondition += " or";
                }
                sSpecialCharCondition += " contains(.,'" + strChar + "')";
                sReplaceChar += "(" + strChar + ")|";
                bFirstTime = false;
            }

            if (sReplaceChar.Length > 1)
                sReplaceChar = sReplaceChar.Substring(0, sReplaceChar.Length - 1);
            XmlNodeList objControlNameColl = p_objInputXml.SelectNodes("//control/@name[contains(.,'supp_')" + sSpecialCharCondition + "]");

            foreach (XmlNode objControl in objControlNameColl)
            {
                objControl.InnerText = System.Text.RegularExpressions.Regex.Replace(objControl.InnerText, sReplaceChar, "_");
            }
        }

        /// <summary>
        /// get all the tab names (group element name) and put into a hidden field.
        /// It can speed up the page load and avoid searching for tab names in the 
        /// Javascript code.
        /// </summary>
        /// <param name="p_objInputXml">View Xml Document</param>
        private void GetTabNames(ref XmlDocument p_objInputXml)
        {
            //Get all the tabs in a page
            XmlNode oTabNames = p_objInputXml.SelectSingleNode("//internal[@name='TabNameList']");
            if (oTabNames != null)
            {
                StringBuilder oSBGroupNames = new StringBuilder();
                XmlNodeList oGroupList = p_objInputXml.SelectNodes("//group");
                foreach (XmlNode oGroup in oGroupList)
                {
                    if (oGroup.Attributes["name"] != null)
                    {
                        if (oSBGroupNames.Length > 0)
                            oSBGroupNames.Append("|");
                        oSBGroupNames.Append("TABS" + oGroup.Attributes["name"].Value);
                    }
                }
                oTabNames.Attributes["value"].Value = oSBGroupNames.ToString();
            }
        }

        /// <summary>
        /// 1. Get the group association supp field names and put to a hidden field
        /// 2. Replace control field with actual control name
        /// </summary>
        /// <param name="p_objInputXml"></param>
        private void GetGroupAssocFieldNames(ref XmlDocument p_objInputXml)
        {
            //Get the form name
            XmlNode oFormNode = p_objInputXml.SelectSingleNode("//form");
            if (oFormNode == null)
                return;

            //If there is no supp table, return
            if (oFormNode.Attributes["supp"] == null)
                return;

            string sSuppTableName = oFormNode.Attributes["supp"].Value;

            //Load the field mapping from XML
            string sGAMappingFilePath = RMConfigurator.CombineFilePath(RMConfigurator.AppFilesPath, "TableMaintenance");
            
            XmlNode oGAFieldNames = p_objInputXml.SelectSingleNode("//internal[@name='GroupAssocFieldList']");
            if (oGAFieldNames != null)
            {
                XElement oSuppGA = XElement.Load(sGAMappingFilePath + "\\" + GroupAssocMapping);
                XElement oFormGA = oSuppGA.XPathSelectElement(string.Format("//page[@supptablename='{0}']",sSuppTableName));
                if (oFormGA == null)
                    return;

                StringBuilder oSBGroupAssocNames = new StringBuilder();
                XmlNodeList oGroupAssocNodeList = p_objInputXml.SelectNodes("//control[@GroupAssoc and starts-with(@name, 'supp_')]");
                foreach (XmlNode oGAField in oGroupAssocNodeList)
                {
                    if (oGAField.Attributes["GroupAssoc"] != null)
                    {
                        string sGroupAssoc = oGAField.Attributes["GroupAssoc"].Value;
                        if (string.IsNullOrEmpty(sGroupAssoc))
                            continue;

                        if (oSBGroupAssocNames.Length > 0)
                            oSBGroupAssocNames.Append("|");
                        oSBGroupAssocNames.Append(oGAField.Attributes["name"].Value);

                        //replace attribute GroupAssoc with the control name
                        string[] arGA = sGroupAssoc.Split(m_GAFieldValueDelimiter);
                        string sFieldName = arGA[0];
                        //MITS 19778 Need to specify the whole path to uniquely find the element
                        XElement oFieldElement = oFormGA.XPathSelectElement(string.Format("//page[@supptablename='{0}']/field[@name='{1}']",sSuppTableName, sFieldName));
                        if (oFieldElement != null)
                        {
                            //For some special case MITS 19433, one supp field could be controlled by 
                            //multiple fields
                            string sServerControlNames = oFieldElement.Attribute("servercontrolname").Value;
                            string[] arServerControlNames = sServerControlNames.Split(m_GAServerControlNameDelimiter);
                            string sGroupAssocValues = string.Empty;
                            foreach (string sServerControlName in arServerControlNames)
                            {
                                if (!string.IsNullOrEmpty(sGroupAssocValues))
                                    sGroupAssocValues += m_GAFieldDelimiter;
                                sGroupAssocValues += sServerControlName + m_GAFieldValueDelimiter + arGA[1];
                            }
                            oGAField.Attributes["GroupAssoc"].Value = sGroupAssocValues;
                        }
                    }
                }
                oGAFieldNames.Attributes["value"].Value = oSBGroupAssocNames.ToString();
            }
        }

    }//partial class PowerViewUpgrade 
    #endregion
}//namespace
