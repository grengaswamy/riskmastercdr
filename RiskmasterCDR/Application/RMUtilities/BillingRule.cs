﻿
using System;
using System.Xml; 
using Riskmaster.Db;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 
using Riskmaster.Security;


namespace Riskmaster.Application.RMUtilities
{
	///<summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits Billing rule information.
	/// </summary>
	public class BillingRule:UtilitiesUIBase 
	{
		
		#region Private Variables
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName = "";
		/// <summary>
		///  Fields mapping with Database fields
		/// </summary>
		private string[,] arrFields = {
		{"RowId", "BILLING_RULE_ROWID"},
		{"AddedByUser", "ADDED_BY_USER"},
		{"DttmRcdAdded", "DTTM_RCD_ADDED"},
		{"DttmRcdLastUpd", "DTTM_RCD_LAST_UPD"},
		{"UpdatedByUser", "UPDATED_BY_USER"},
		{"InUseFlag", "IN_USE_FLAG"},
		{"BillingRuleCode", "BILLING_RULE_CODE"},
		{"NoticeOffsetInd", "NOTICE_OFFSET_IND"},
		{"FirstNoticeDays", "FIRST_NOTICE_DAYS"},
		{"CancelPendDiary", "CANCEL_PEND_DIARY"},
		{"SecondNoticeDays", "SECOND_NOTICE_DAYS"},
		{"CancelDiary", "CANCEL_DIARY"},
		{"ThirdNoticeDays", "THIRD_NOTICE_DAYS"},
		{"FourthNoticeDays", "FOURTH_NOTICE_DAYS"},
		{"OutstandBalIndic", "OUTSTAND_BAL_INDIC"},
		{"NonPayInd", "NON_PAY_IND"},
		{"MaxNumRebills", "MAX_NUM_REBILLS"},
		{"DueDays", "DUE_DAYS"} 
									  };	
		
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sConnectionString = "";
		/// <summary>
		/// Xml document object
		/// </summary>
		private XmlDocument m_objXMLDocument = null;
		
		/// <summary>
		/// Key field value
		/// </summary>
		public string KeyFieldValue
		{
			get
			{
				return Conversion.ConvertObjToStr(base.KeyFieldValue);
			}
		}
		#endregion

        private int m_iClientId = 0;

		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public BillingRule(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			try
			{
				ConnectString = p_sConnString;
				m_sConnectionString = p_sConnString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRule.Constructor.Error", m_iClientId), p_objEx);
			}

		}
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public BillingRule(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{
			try
			{
				m_sUserName = p_sLoginName;
				ConnectString = p_sConnectionString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRule.Constructor.Error", m_iClientId), p_objEx);
			}
		}
		#endregion
		
		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
            //XmlNode objNode = null;
            //string sValue = "";
			try
			{
                XMLDoc = p_objXmlDocument;
                base.Get();

                // //Commented-No need to set the attribute value for Code Fields.This causes problem in setting Ref attribute for these fields.
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='NoticeOffsetInd']");
                //if (objNode!=null)
                //{
                //    if (objNode.InnerText!=null)
                //    {
                //        sValue=objNode.InnerText;
                //        ((XmlElement)objNode).SetAttribute("value",sValue);
                //        objNode.InnerText=UTILITY.GetCode(objNode.InnerText,base.ConnectString);
                //    }
                //}
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='CancelPendDiary']");
                //if (objNode!=null)
                //{
                //    if (objNode.InnerText!=null)
                //    {
                //        sValue=objNode.InnerText;
                //        ((XmlElement)objNode).SetAttribute("value",sValue);
                //        objNode.InnerText=UTILITY.GetCode(objNode.InnerText,base.ConnectString);
                //    }
                //}
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='CancelDiary']");
                //if (objNode!=null)
                //{
                //    if (objNode.InnerText!=null)
                //    {
                //        sValue=objNode.InnerText;
                //        ((XmlElement)objNode).SetAttribute("value",sValue);
                //        objNode.InnerText=UTILITY.GetCode(objNode.InnerText,base.ConnectString);
                //    }
                //}
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='OutstandBalIndic']");
                //if (objNode!=null)
                //{
                //    if (objNode.InnerText!=null)
                //    {
                //        sValue=objNode.InnerText;
                //        ((XmlElement)objNode).SetAttribute("value",sValue);
                //        objNode.InnerText=UTILITY.GetCode(objNode.InnerText,base.ConnectString);
                //    }
                //}
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='NonPayInd']");
                //if (objNode!=null)
                //{
                //    if (objNode.InnerText!=null)
                //    {
                //        sValue=objNode.InnerText;
                //        ((XmlElement)objNode).SetAttribute("value",sValue);
                //        objNode.InnerText=UTILITY.GetCode(objNode.InnerText,base.ConnectString);
                //    }
                //}
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='BillingRuleCode']");
                //if (objNode!=null)
                //{
                //    if (objNode.InnerText!=null)
                //    {
                //        sValue=objNode.InnerText;
                //        ((XmlElement)objNode).SetAttribute("value",sValue);
                //        objNode.InnerText=UTILITY.GetCode(objNode.InnerText,base.ConnectString);
                //    }
                //}
                // //Commented-No need to set the attribute value for Code Fields.This causes problem in setting Ref attribute for these fields.
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRule.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				//objNode=null;
			}
			return XMLDoc; 
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data to the database
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
            string sUseBR = "";
			try
			{
                XMLDoc = p_objXmlDocument;
				m_objXMLDocument = p_objXmlDocument;
				if(ValidateXMLData()) 
				{
					FillXML();
                    sUseBR = m_objXMLDocument.SelectSingleNode("//control[@name='InUseFlag']").InnerText;
                    if (sUseBR == "")
                        m_objXMLDocument.SelectSingleNode("//control[@name='InUseFlag']").InnerText = "False";

					base.Save();
				}
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRule.Save.Error", m_iClientId), p_objEx);
			}
			return bReturnValue;
		}
		#endregion

		#region Private Functions
		new private void Initialize()
		{
			try
			{
				TableName = "SYS_BILL_BLNG_RULE";
				KeyField = "BILLING_RULE_ROWID";
				base.InitFields(arrFields);
				base.Initialize();
			}
		
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRule.Initialize.Error", m_iClientId), p_objEx);
			}
		}
		private void FillXML()
		{
			string sBillingRuleRowid = "";
			XmlNodeList objNodeList = null;
			string sNodename = string.Empty;
			XmlElement objNode=null;
			try
			{
				objNode=((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']"));
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sBillingRuleRowid = objNode.InnerText;
					}
				}
				sBillingRuleRowid=sBillingRuleRowid.Trim();
				sBillingRuleRowid=sBillingRuleRowid.Trim();
				objNodeList=m_objXMLDocument.SelectNodes("//control");
				foreach(XmlElement objXMLElement in objNodeList)
				{
					sNodename = objXMLElement.GetAttribute("name");  
					switch(sNodename)
					{
						case "AddedByUser":
							if(sBillingRuleRowid == "" || sBillingRuleRowid == "-1")
								objXMLElement.InnerText = m_sUserName; 
							break;
						case "DttmRcdAdded":
							if(sBillingRuleRowid == "" || sBillingRuleRowid == "-1")
								objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToLongDateString()); 
							break;
						case "DttmRcdLastUpd":
							objXMLElement.InnerText = Conversion.GetDateTime(DateTime.Now.ToLongDateString());
							break;
						case "UpdatedByUser":
							objXMLElement.InnerText = m_sUserName; 
							break;
					}
				}
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRule.FillXML.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNodeList = null;
				objNode=null;
			}
		}

		private bool ValidateXMLData()
		{
			string sBillingRuleRowid = "";
			string sBillingRuleCode = "";
			string sInUseFlag = "";
			string sSQL = "";
			DbReader objDBReader = null;
			XmlElement objElement=null;
			try
			{
				sBillingRuleRowid = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='RowId']")).InnerText;
				objElement=((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='BillingRuleCode']"));
				if (objElement!=null)
				{
					if (objElement.Attributes["codeid"]!=null)
					{
						sBillingRuleCode =objElement.Attributes["codeid"].Value;
					}
					else
					{
						sBillingRuleCode="";
					}
				}
				else
				{
					sBillingRuleCode="";
				}
				
				sInUseFlag = ((XmlElement)m_objXMLDocument.SelectSingleNode("//control[@name='InUseFlag']")).InnerText;
				
				if(sInUseFlag != "True")
				{
					//Check if the Billing Rule is used by an active pay plan
					sSQL = "SELECT count(BILLING_RULE_ROWID) FROM SYS_BILL_PAY_PLAN WHERE" + 
							" IN_USE_FLAG = 1 AND BILLING_RULE_ROWID = " + sBillingRuleRowid;

						objDBReader = DbFactory.GetDbReader(base.ConnectString,sSQL);
						if(objDBReader.Read())
						{
                            if (Conversion.ConvertObjToInt(objDBReader[0], m_iClientId) > 0)
                                throw new RMAppException(Globalization.GetString("BillingRule.ValidateXMLData.AlreadyUsed", m_iClientId));
						}
						objDBReader.Close();
				}

				//Check if there is already Billing Rule for the same Billing Code, log error
				sSQL = "SELECT BILLING_RULE_ROWID FROM SYS_BILL_BLNG_RULE " + 
					"WHERE BILLING_RULE_CODE = " + sBillingRuleCode; 

				objDBReader = DbFactory.GetDbReader(base.ConnectString,sSQL);
				if(objDBReader.Read())
				{
					if( Conversion.ConvertObjToInt(objDBReader["BILLING_RULE_ROWID"], m_iClientId) != Conversion.ConvertStrToInteger(sBillingRuleRowid))
                        throw new RMAppException(Globalization.GetString("BillingRule.ValidateXMLData.AlreadyExists", m_iClientId));
				}
				objDBReader.Close();

				return true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BillingRule.ValidateXMLData.Error", m_iClientId), p_objEx);
			}
			finally
			{
                if (objDBReader != null)
                {
                    objDBReader.Close();
                    objDBReader.Dispose();
                }
                objElement = null;
				
			}
		}
		/// Name		: Delete
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes the Billing Rule information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Delete(XmlDocument p_objXmlDocument)
		{
			bool bReturnValue=false;
			string sSQL="";
			XmlNode objNode=null;
			DbReader objReader=null;
			try
			{
				XMLDoc=p_objXmlDocument;
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
				if (objNode!=null)
				{
					if (!objNode.InnerText.Trim().Equals(""))
					{
						sSQL = "SELECT BILLING_RULE_ROWID FROM SYS_BILL_PAY_PLAN WHERE IN_USE_FLAG <> 0 AND BILLING_RULE_ROWID = " +objNode.InnerText;
						objReader=DbFactory.GetDbReader(base.ConnectString,sSQL);
						if (objReader.Read())
						{
							throw new RMAppException(Globalization.GetString("BRPayPlan.Delete.UsedByActivePayPlan", m_iClientId));
						}
						objReader.Close();
					}
				}
				base.Delete();
				bReturnValue=true;
				return bReturnValue;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("BRPayPlan.Delete.Error", m_iClientId),p_objEx);
			}
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                objNode = null;

            }
		}
		#endregion
	}
}
