﻿using System;
using System.IO;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using System.Collections;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    /// Author  :   Amandepp Kaur
    /// Dated   :   09th August 2011
    /// Purpose :   Implements the Diary Jobs
    /// </summary>
    public class MobileUpload
    {
        /// <summary>
        /// Private Connection String variable
        /// </summary>
        private string m_sConnStr = "";

        /// <summary>
        /// Private string for User
        /// </summary>
        private string m_sUser = "";

        /// <summary>
        /// Private string for Password
        /// </summary>
        private string m_sPwd = "";

        /// <summary>
        /// Private string for Dsn
        /// </summary>
        private string m_sDsn = "";

        /// <summary>
        /// Database type
        /// </summary>
        private int m_iDbType = 0;

        private int m_iClientId=0;
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="p_sConnStr">Connection String</param>
        public MobileUpload(string p_sUser, string p_sPwd, string p_sDsn, int p_iClientId)
        {
            m_sUser = p_sUser;
            m_sPwd = p_sPwd;
            m_sDsn = p_sDsn;
            m_iClientId = p_iClientId;
            UserLogin objUserLogin = new UserLogin(p_sUser, p_sPwd, p_sDsn, m_iClientId);
            m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;
            m_iDbType = (int)objUserLogin.objRiskmasterDatabase.DbType;
            objUserLogin = null;
        }

        /// Name		 :  GetStatusofDiary
        /// Author		 :  Amandeep Kaur
        /// Date Created :  08/09/2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlDoc">Input Xml Document</param>

        public XmlDocument GetStatusofDiary(XmlDocument p_objXmlDoc)
        {
            int iStatus;
            XmlNode objTempNode = null;
            XmlDocument objXmlOut = null;
            XmlElement objXmlEle = null;
            //XmlNode objNode = null;
            XmlElement objParentNode = null;
            try
            {
                objXmlOut = new XmlDocument();
                iStatus = GetStatus(p_objXmlDoc);
                objTempNode = p_objXmlDoc.SelectSingleNode("//EntryId");
                objParentNode = objXmlOut.CreateElement("DiaryStatus");

                objXmlEle = objXmlOut.CreateElement("EntryId");
                objXmlEle.InnerText = objTempNode.InnerText;
                objParentNode.AppendChild(objXmlEle);

                objXmlEle = objXmlOut.CreateElement("Status");

                switch (iStatus)
                {
                    case -1:                                      //diary status is open                        
                        objXmlEle.InnerText = "Open";
                        break;

                    case 0:                                       //diary status is closed                       
                        objXmlEle.InnerText = "Closed";
                        break;

                    case -2:                                      //diary status is deleted                                       
                        objXmlEle.InnerText = "Deleted";
                        break;

                    default:
                        throw new RMAppException(Globalization.GetString("MobileUpload.GetStatusofDiary.Error", m_iClientId));

                }

                objParentNode.AppendChild(objXmlEle);
                objXmlOut.AppendChild(objParentNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("MobileUpload.GetStatusofDiary.Error", m_iClientId), p_objEx);
            }
            return objXmlOut;
        }

        /// Name		 :  GetStatus
        /// Author		 :  Amandeep Kaur
        /// Date Created :  08/09/2011		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlDoc">Input Xml Document</param>
        public int GetStatus(XmlDocument p_objXmlDoc)
        {
            string sSQL = string.Empty;
            XmlElement objTempNode = null;
            int iStatus = -3;
            int iDiaryStatus = 0;
            try
            {
                objTempNode = (XmlElement)p_objXmlDoc.SelectSingleNode("//EntryId");
                if (objTempNode == null)
                {
                    throw new RMAppException(Globalization.GetString("MobileUpload.GetStatus.Error", m_iClientId));
                }

                sSQL = "SELECT STATUS_OPEN FROM WPA_DIARY_ENTRY where ENTRY_ID = '" + objTempNode.InnerText + "' AND ASSIGNED_USER = '" + m_sUser + "'";

                using (DbReader objReader = DbFactory.GetDbReader(m_sConnStr, sSQL))
                {
                    if (objReader.Read())
                    {
                        iDiaryStatus = Conversion.ConvertObjToInt(objReader.GetValue("STATUS_OPEN"), m_iClientId);
                        if (iDiaryStatus != 0)
                        {
                            iStatus = -1;
                        }
                        else
                        {
                            iStatus = 0;
                        }
                    }
                    else
                    {
                        iStatus = -2;
                    }
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("MobileUpload.GetStatus.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objTempNode = null;
            }
            return iStatus;
        }
    }
}
