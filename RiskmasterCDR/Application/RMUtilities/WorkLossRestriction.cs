using System;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   Raman
    ///Dated   :   03 Aug 2009
    ///Purpose :   R6 Create Work Loss/Restrictions Mapping Storage
    /// </summary>
    public class WorkLossRestriction : UtilitiesUIBase
    {
        #region Private variables
        /// <summary>
        /// Fields mapping with database fields
        /// </summary>
        private string[,] arrFields = {
			{"RowId", "WRKLOS_ROWID"},
			{"RecordType","WRKLOS_RSTRCT_CODE"},
			{"TransTypeCode","TRANS_TYPE_CODE"},
			{"LOBId","LINE_OF_BUS_CODE"}	
									  };


        private int m_iClientId = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>
        public WorkLossRestriction(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            try
            {
                m_iClientId = p_iClientId;
                ConnectString = p_sConnString;
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WorkLossRestriction.Constructor.Error", m_iClientId), p_objEx);
            }
        }
        #endregion

        #region Private Functions
        new private void Initialize()
        {
            try
            {
                TableName = "LT_RST_DAYS_MAP";
                KeyField = "WRKLOS_ROWID";
                base.InitFields(arrFields);
                base.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WorkLossRestriction.Initialize.Error", m_iClientId), p_objEx);
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Gets the xml structure for the form
        /// </summary>
        /// <param name="p_objXMLDocument">Input xml document</param>
        /// <returns>xml structure</returns>
        public XmlDocument New(XmlDocument p_objXMLDocument)
        {
            try
            {
                return p_objXMLDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WorkLossRestriction.New.Error", m_iClientId), p_objEx);
            }
        }
        /// Name		: Get
        /// Author		: Navdeep
        /// Date Created: 02 May 2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>Xml containing the data to be populated on screen</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            try
            {
                XMLDoc = p_objXmlDocument;

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objNode.InnerText != null && !objNode.InnerText.Equals(""))
                    base.Get();
              
              
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WorkLossRestriction.Get.Err", m_iClientId), p_objEx);
            }
            finally
            {
                			objNode = null;
            }
        }

        /// Name		: Save
        /// Author		: Navdeep
        /// Date Created: 02 May 2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the data to the database
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public bool Save(XmlDocument p_objXmlDocument)
        {
            bool bReturnValue = false;
            try
            {
                XMLDoc = p_objXmlDocument;
              
                base.Save();
                bReturnValue = true;
                return bReturnValue;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WorkLossRestriction.Save.Error", m_iClientId), p_objEx);
            }

        }
        /// Name		: Delete
        /// Author		: Navdeep
        /// Date Created: 02 May 2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes Mapping information
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public bool Delete(XmlDocument p_objXmlDocument)
        {
            bool bReturnValue = false;
            XMLDoc = p_objXmlDocument;
            try
            {
                base.Delete();
                bReturnValue = true;
                return bReturnValue;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("WorkLossRestriction.Delete.Error", m_iClientId), p_objEx);
            }
        }
        #endregion

    }
}
