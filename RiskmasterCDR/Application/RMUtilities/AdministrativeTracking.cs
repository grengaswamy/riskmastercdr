using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes; 


namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   20th,Apr 2005
	///Purpose :   Implements Admin Tracking List form
	/// </summary>
	public class AdministrativeTracking:UtilitiesUIListBase 
	{
		/// <summary>
		/// Database table fields mapping
		/// </summary>
		private string[,] arrFields = {
			{"UserTableName","TABLE_NAME"},
			{"SysTableName","SYSTEM_TABLE_NAME"}
									  };
        private int m_iClientId = 0;

		/// Name		: AdministrativeTracking
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor with connection string parameter
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
		public AdministrativeTracking(string p_sConnString, int p_iClientId):base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
			this.Initialize();
            m_iClientId = p_iClientId; 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initializing the variables
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "GLOSSARY, GLOSSARY_TEXT ";
			base.KeyField = "GLOSSARY.TABLE_ID";
			base.RadioName = "AdminTrackingList";
			LocalCache objCache = new LocalCache(ConnectString,m_iClientId);			
			base.WhereClause = "GLOSSARY.TABLE_ID = GLOSSARY_TEXT.TABLE_ID AND  GLOSSARY.GLOSSARY_TYPE_CODE = " +
				objCache.GetCodeId("8", "GLOSSARY_TYPES")  + 
				"AND GLOSSARY_TEXT.LANGUAGE_CODE = 1033";
			base.OrderByClause ="GLOSSARY_TEXT.TABLE_NAME";
			objCache.Dispose();
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Public Get method gets the data in xml format
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with xml structure</param>
		/// <returns>Xml data for Admin Tracking tables</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNodeList objNodLst=null;
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				objNodLst = p_objXmlDocument.SelectNodes("//listrow//RowId");
				foreach(XmlNode objNod in objNodLst)
				{
                    //Changed by Abhishek to fix utilities base lst changes
					//objNod.InnerText = objNod.NextSibling.InnerText + "|" + objNod.NextSibling.NextSibling.InnerText;
                    objNod.InnerText = objNod.PreviousSibling.PreviousSibling.InnerText + "|" + objNod.PreviousSibling.InnerText;
				
                }
				return XMLDoc; 
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("AdministrativeTracking.Get.Err",m_iClientId),p_objEx);
			}
			finally
			{
				objNodLst=null;
			}
		}
	}
}