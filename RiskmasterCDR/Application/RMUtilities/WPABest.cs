using System;
using System.IO;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using System.Collections;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   06th,Jun 2005
	///Purpose :   Implements the WPA Best Wizard Form
	/// </summary>
	public class WPABest
	{
		/// <summary>
		/// Private Connection String variable
		/// </summary>
		private string m_sConnStr="";

		/// <summary>
		/// Private string for User
		/// </summary>
		private string m_sUser="";

		/// <summary>
		/// Private string for Password
		/// </summary>
		private string m_sPwd="";

		/// <summary>
		/// Private string for Dsn
		/// </summary>
		private string m_sDsn="";

		/// <summary>
		/// Private variable for LOB
		/// </summary>
		private int m_iLOB=0;

		/// <summary>
		/// private variable for Role
		/// </summary>
		private int m_iRole=0;

		/// <summary>
		/// Private variable for BP Id
		/// </summary>
		private int m_iBPID=0;

		/// <summary>
		/// Private variable for Topic
		/// </summary>
		private int m_iTopic=0;	
		
		/// <summary>
		/// Private boolean for LOB
		/// </summary>
		private bool m_bLOB=false;

		/// <summary>
		/// Private boolean for Role
		/// </summary>
		private bool m_bRole=false;

		/// <summary>
		/// Private boolean for Topic
		/// </summary>
		private bool m_bTopic=false;
		
		/// <summary>
		/// BP Name
		/// </summary>	
		string m_sBPName="";

		/// <summary>
		/// Best Definition Object
		/// </summary>
		private BestDefinition objBestDef;

		/// <summary>
		/// Info Definition Object
		/// </summary>
		private InfoDefinition objInfoDef;

		/// <summary>
		/// Info Setting Object
		/// </summary>
		private InfoSetting objInfoSet;

		/// <summary>
		/// Available Auto definitions
		/// </summary>
		private int m_iAvailableDefs=0;

		/// <summary>
		/// Database type
		/// </summary>
		private int m_iDbType=0;

        private int m_iClientId = 0;

		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
        public WPABest(string p_sUser, string p_sPwd, string p_sDsn, int p_iClientId)
		{
			m_sUser=p_sUser;
			m_sPwd=p_sPwd;
			m_sDsn=p_sDsn;
            m_iClientId = p_iClientId;
            UserLogin objUserLogin = new UserLogin(p_sUser, p_sPwd, p_sDsn, m_iClientId);
			m_sConnStr = objUserLogin.objRiskmasterDatabase.ConnectionString;
			m_iDbType = (int)objUserLogin.objRiskmasterDatabase.DbType;
			objUserLogin=null;
			objBestDef = new BestDefinition(); 
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 06/06/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get method returns the data for WPA Best Wizard.
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDoc)
		{			
			try
			{	
				GetBestPractices(p_objXmlDoc);
				p_objXmlDoc.SelectSingleNode("//control[@name='AvlDefsCount']").InnerText =m_iAvailableDefs.ToString(); 
				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPABest.Get.GetErr", m_iClientId), p_objEx);
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 06/06/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data for WPA Best Wizard.
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument Save(XmlDocument p_objXmlDoc)
		{
			string sData="";
			string sTemp="";
			string sTmp="";
			string sSQL="";
			int iPtr=0;
			int iRowId=0;
			DbConnection objConn = null;
			DbCommand objCommand = null;
			DbTransaction objTran = null;
			BestDefinition objBestDef;
			InfoDefinition objInfoDef;
			DbReader objRdr=null;

			string sSelectedDef=string.Empty;
			string[] sarrSelDef;

			try
			{	
				sSelectedDef=((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SelDefIdList']")).InnerText; 
				if (sSelectedDef.Trim()!=string.Empty)
				{
					sarrSelDef=sSelectedDef.Split('|');
					for(int i=0;i<sarrSelDef.Length;i++)
					{
						objBestDef=WPAAUTO.GetBestDef(Conversion.ConvertStrToInteger(sarrSelDef[i]),m_iClientId, m_sConnStr);//rkaur27
						objInfoDef=LoadInfoDefObj(objBestDef.DEF_ID);
						objRdr=DbFactory.GetDbReader(m_sConnStr,"SELECT * FROM WPA_AUTO_SET WHERE BEST_PRACT_ID = " + objBestDef.ID);
						if(objRdr.Read())
						{
                            throw new RMAppException(Globalization.GetString("WPABest.Save.Err", m_iClientId));
						} 
						objRdr.Dispose();

						for(int iCtr=0;iCtr<=objBestDef.NumFilters -1;iCtr++)
						{
							if(objBestDef.FilterSet[iCtr].Data.ToString().IndexOf(",*")>-1)
							{
								sData="";
								sTemp=objBestDef.FilterSet[iCtr].Data.ToString().Substring(1,objBestDef.FilterSet[iCtr].Data.ToString().Length-1);
								while (sTemp !="")
								{
									iPtr = sTemp.IndexOf(",");
									sTmp = sTemp.Substring(0,iPtr);
									sTemp = sTemp.Substring(iPtr,sTemp.Length-iPtr-1);
									if(sTmp.Substring(0,1)=="*")
									{
										if(sTmp=="*")
											sSQL= objInfoDef.FilterDef[GetFilterIndex(objInfoDef,objBestDef.FilterSet[iCtr].Number)].SQLFill;
										else
										{
											sTmp=sTmp.Substring(1,sTmp.Length-1);
											sData = sData +  "," + sTmp;
											sSQL = objInfoDef.FilterDef[GetFilterIndex(objInfoDef, objBestDef.FilterSet[iCtr].Number)].SQLFill + " AND CODES.RELATED_CODE_ID = " + sTmp;
										}
										objRdr=DbFactory.GetDbReader(m_sConnStr,sSQL);
										while(objRdr.Read())
										{
											sData = sData + "," + objRdr.GetInt32("CODE_ID");
										}
										objRdr.Dispose(); 
									}
									else
										sData= sData + "," + sTmp;
								}
								objBestDef.FilterSet[iCtr].Data = sData + ",";
							}
						}
                        iRowId = Utilities.GetNextUID(m_sConnStr, "WPA_AUTO_SET", m_iClientId);
						objConn = DbFactory.GetDbConnection(m_sConnStr);
						objConn.Open();
						objCommand=objConn.CreateCommand();
						objTran=objConn.BeginTransaction();
						objCommand.Transaction=objTran;
						objCommand.CommandText = "DELETE FROM WPA_AUTO_SET WHERE AUTO_ROW_ID = " + iRowId;
						objCommand.ExecuteNonQuery();

						objCommand.CommandText = "INSERT INTO WPA_AUTO_SET(AUTO_ROW_ID, AUTO_NAME, DEF_NAME, DEF_INDEX," + 
							"SEND_CREATED, SEND_UPDATED, SEND_ADJUSTER, NOTIFICATION_DAYS, ROUTE_DAYS, PRIORITY, TASK_ESTIMATE," + 
							"TASK_EST_TYPE, TIME_BILLABLE_FLAG, EXPORT_SCHEDULE, INSTRUCTIONS, PROCESS_DATE, BEST_PRACT_ID, DUE_DAYS, "+ 
							"SEND_SUPPFIELD, SEND_SUPPFIELDNAME) VALUES(" + 
							iRowId + ",'" + objBestDef.Name  + "','" + objInfoDef.Name    + "'," + objBestDef.DEF_ID   + ",0,0,0,0,0," + 
							+ objBestDef.Priority  + "," + objBestDef.TaskEst+ ",1," +
							objBestDef.Billable  + ",0,'" + objBestDef.Instructions  + "',''," + objBestDef.ID + "," + objInfoSet.iDueDays +  ",0"
							+ ",'')" ;
						objCommand.ExecuteNonQuery();

						objCommand.CommandText = "DELETE FROM WPA_AUTO_FILTER WHERE AUTO_ID = " + iRowId;
						objCommand.ExecuteNonQuery();
						for(int iCtr=0; iCtr<=objBestDef.NumFilters-1;iCtr++)
						{
							objCommand.CommandText = "INSERT INTO WPA_AUTO_FILTER(AUTO_ID,FILTER_INDEX,FILTER_DATA) VALUES(" +
								iRowId  + "," +  objBestDef.FilterSet[iCtr].Number + ",'" + objBestDef.FilterSet[iCtr].Data + "')";
							objCommand.ExecuteNonQuery();
						}
					}
					objTran.Commit();
				}
				return p_objXmlDoc;
			}

			catch(RMAppException p_objEx)
			{
				if(objTran!=null)
					objTran.Rollback(); 
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
				if(objTran!=null)
					objTran.Rollback();
                throw new RMAppException(Globalization.GetString("WPABest.Save.Err", m_iClientId), p_objEx);
			}
			finally
			{
//				objPreDefLst=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr=null;
				}
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null; 
				}
                if (objTran != null)
                    objTran.Dispose();
				objCommand= null;
			}
		}

		/// Name		: GetBestPractices
		/// Author		: Pankaj Chowdhury
		/// Date Created: 06/06/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// GetBestPractices
		/// </summary>
		/// <param name="p_objXmlDoc">Input Xml Document</param>
		private void GetBestPractices(XmlDocument p_objXmlDoc)
		{
			int iPtr=0;			
			string sValue="";
			string sKeyword="";
			string sFileName="";
			string sLine = "";
			StreamReader objSRdr=null;
			try
			{
                sFileName = String.Format(@"{0}\{1}", UTILITY.GetAppFilesDirectory("WPA"), UTILITY.GetConfigUtilitySettings(m_sConnStr, m_iClientId)["WPABestDefFile"]);//rkaur27
				if (File.Exists(sFileName))
				{
					using (objSRdr = new StreamReader(sFileName)) 
					{
						sLine = "";
						while ((sLine = objSRdr.ReadLine())!= null)
						{
							if(sLine.Trim()!="")
							{
								if(sLine.Substring(0,1)=="[")
								{
									sKeyword = sLine.Substring(1,sLine.Length-2);									
									m_sBPName = sKeyword;
									CheckBP(p_objXmlDoc,m_sBPName);
									m_iBPID=0;
									m_iRole=0;
									m_iLOB=0;
									m_iTopic=0;
								}
								else
								{
									iPtr = sLine.IndexOf("=");
									if((sLine.Substring(0,2)=="//")||(iPtr<0))
										continue;
									sKeyword= sLine.Trim().ToUpper().Substring(0,iPtr -1).Trim() ;
									sValue=sLine.Substring(iPtr+1,sLine.Length - iPtr-1).Trim() ; 
									switch(sKeyword)
									{
										case "ID":
											m_iBPID=Conversion.ConvertStrToInteger(sValue);
											break;
										case "ROLES":
											m_iRole=Conversion.ConvertStrToInteger(sValue);
											break;
										case "LOB":
											m_iLOB=Conversion.ConvertStrToInteger(sValue);
											break;
										case "TOPICS":
											m_iTopic=Conversion.ConvertStrToInteger(sValue);
											break;
									}
								}
							}
						}
					}
				}
				
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPABest.GetBestPractices.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if(objSRdr != null)
				{
					objSRdr.Close();
					objSRdr=null;
				}
			}
		}

		/// Name		: CheckBP
		/// Author		: Pankaj Chowdhury
		/// Date Created: 06/06/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// CheckBP
		/// </summary>
		/// <param name="p_objXmlDoc">Input Xml Document</param>
		private void CheckBP(XmlDocument p_objXmlDoc, string p_sName)
		{
			XmlElement objNewElm=null;
			XmlNode objPreDef=null;

			string sSelectedRole=string.Empty;
			string sSelectedLOB=string.Empty;
			string sSelectedTopics=string.Empty;

			try
			{
				if(p_sName!="")
				{
					m_bRole=false;
					m_bLOB=false;
					m_bTopic=false;

					objPreDef = p_objXmlDoc.SelectSingleNode("//control[@name='PreDef']"); 

					sSelectedRole=((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SelRolesIdList']")).InnerText; 
					if (sSelectedRole.Trim() != string.Empty)
					{
						if (CheckWhetherExists(sSelectedRole, m_iRole))
							m_bRole=true;
						else
							m_bRole=false;
					}
					else
						m_bRole=true;

					sSelectedLOB=((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SelLobIdList']")).InnerText; 
					if ((sSelectedLOB.Trim() == string.Empty) || (m_iLOB == 0))
						m_bLOB=true;
					else
					{
						if (CheckWhetherExists(sSelectedLOB,m_iLOB))
							m_bLOB=true;
						else
							m_bLOB=false;
					}

					sSelectedTopics=((XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='SelTopicsIdList']")).InnerText; 
					if (sSelectedTopics.Trim() != string.Empty)
					{
						sSelectedTopics="|" + sSelectedTopics + "|";
						if (sSelectedTopics.IndexOf("|" + m_iTopic.ToString() + "|") != -1)
							m_bTopic=true;
						else
							m_bTopic=false;
					}
					else
						m_bTopic=true;

					if(m_bRole && m_bLOB && m_bTopic)
					{
						objNewElm = p_objXmlDoc.CreateElement("option");
						objNewElm.SetAttribute("value",m_iBPID.ToString());
						objNewElm.InnerText = p_sName; 
						objPreDef.AppendChild(objNewElm);
						m_iAvailableDefs++;
					}
				}
			}
			finally
			{
				objNewElm=null;
			}
		}


		private bool CheckWhetherExists(string p_sSelectedValues, int p_iExistingValue)
		{
			int[] iarrExistingValues=new int[5]{0,0,0,0,0};
			string[] sarrSelectedValues;
			int iCounter=0;
			int iExistingValue=p_iExistingValue;

			sarrSelectedValues=p_sSelectedValues.Split('|');
	
			if (iExistingValue >= 16)
			{
				iExistingValue=iExistingValue-16;
				iarrExistingValues[iCounter]=16;
				iCounter++;
			}
			if (iExistingValue >= 8)
			{
				iExistingValue=iExistingValue-8;
				iarrExistingValues[iCounter]=8;
				iCounter++;
			}
			if (iExistingValue >= 4)
			{
				iExistingValue=iExistingValue-4;
				iarrExistingValues[iCounter]=4;
				iCounter++;
			}
			if (iExistingValue >= 2)
			{
				iExistingValue=iExistingValue-2;
				iarrExistingValues[iCounter]=2;
				iCounter++;
			}
			if (iExistingValue >= 1)
			{
				iExistingValue=iExistingValue-1;
				iarrExistingValues[iCounter]=1;
				iCounter++;
			}
			for (int i=0;i<sarrSelectedValues.Length;i++)
			{
				for (int j=0;j<iCounter;j++)
				{
					if (iarrExistingValues[j] == Conversion.ConvertStrToInteger(sarrSelectedValues[i]))
						return true;
				}
			}
			return false;
		}
	

		/// Name			: LoadInfoDefObj
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 16 May 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Load the Info Definition data
		/// </summary>
		/// <param name="p_ID">row id</param>
		/// <returns>xml document with data</returns>
		private InfoDefinition LoadInfoDefObj(int p_ID)
		{
			int iPtr = 0;
			int iInfoNum = 0;
			int iFilterNum = 0;
			int iNumFilters = 0;
			int iDbType =0;
			bool bEnd = false;
            //Start-Mridul Bansal. 01/19/10. MITS#18229. It is not being used below
			//bool bUseEnhPol=false;
            //End-Mridul Bansal. 01/19/10. MITS#18229
			string sFileName="";
			string sKeyWord="";
			string sValue = "";
			string sTmp ="";
			StreamReader objSRdr = null;
			Riskmaster.Settings.SysSettings objSys = null;
			InfoDefinition objInfoDef;

			try
			{
				objInfoDef = new InfoDefinition();
                objSys = new Riskmaster.Settings.SysSettings(m_sConnStr, m_iClientId);  
                //Start-Mridul Bansal. 01/19/10. MITS#18229. It is not being used below.
				//bUseEnhPol = Conversion.ConvertLongToBool(objSys.UseEnhPolFlag);
                //End-Mridul Bansal. 01/19/10. MITS#18229
				objSys = null;
                sFileName = String.Format(@"{0}\{1}", UTILITY.GetAppFilesDirectory("WPA"), UTILITY.GetConfigUtilitySettings(m_sConnStr, m_iClientId)["WPAInfoDevFile"]);//rkaur27
				objInfoDef.FilterDef = new FilterDefinition[50];

				if (File.Exists(sFileName))
				{
					using (objSRdr = new StreamReader(sFileName)) 
					{
						string sLine = "";
						while ((sLine = objSRdr.ReadLine())!= null)
						{
							sLine = sLine.Trim();
							if(sLine!="")
							{
								if (sLine.Substring(0,1)=="[")
								{
									sKeyWord = sLine.Substring(1,sLine.Length - 2);
									if(sKeyWord.ToUpper()=="FILTER")
									{
										iFilterNum +=1;
										iNumFilters +=1; 
										objInfoDef.NumFilters++; 
									}
									else
									{
										if (bEnd)
											break;
										iFilterNum = -1;
										iNumFilters = 0;
										objInfoDef.NumFilters=0; 
										iInfoNum  += 1;
										objInfoDef.Name = sKeyWord; 
									}
								}
								else
								{
									iPtr =  sLine.IndexOf("=");
									if(sLine.Substring(0,2)=="//" || iPtr == 0 )
										iPtr  = 1;
									sKeyWord = sLine.Substring(0, iPtr) ;
									sValue = sLine.Substring(iPtr+1,sLine.Length-iPtr-1).Trim();
									switch(sKeyWord.ToUpper()) 
									{
										case "NAME":
											objInfoDef.FilterDef[iFilterNum].Name  = sValue;
											break;
										case "ID":
											if (iFilterNum == -1)
											{
												if (p_ID == Conversion.ConvertStrToInteger(sValue))
													bEnd = true;
												objInfoDef.ID  =  Conversion.ConvertStrToInteger(sValue);
											}
											else
												objInfoDef.FilterDef[iFilterNum].ID = Conversion.ConvertStrToInteger(sValue);
											break;
										case "ATTACH_TABLE":
											objInfoDef.AttachTable = sKeyWord;
											break;
										case "ATTACH_COLUMN":
											objInfoDef.AttachCol =  sKeyWord;
											break;
										case "ATTACH_DESC":
											objInfoDef.AttachDesc = sKeyWord;
											break;
										case "TMPSQL":
											if(sValue.Substring(0,1)=="[")
											{
												iPtr = sValue.IndexOf("]");
												sTmp = sValue.Substring(2,iPtr-2);
												sValue = sValue.Substring(0, iPtr+1);
												switch(sTmp.ToUpper())
												{
													case "SQLSERVER":
														iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
														break;
													case "SYBASE":
														iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
														break;
													case "INFORMIX":
														iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
														break;
													case "ORACLE":
														iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
														break;
													case "DB2":
														iDbType = (int)eDatabaseType.DBMS_IS_DB2;
														break;
													default:
														iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
														break;
												}
												if (iDbType == m_iDbType )
													objInfoDef.tmpSQL = sValue; 
											}
											else
												objInfoDef.tmpSQL= sValue;
											break;
										case "SQLFILL":	
											if(sValue.Substring(0,1)=="[")
											{
												iPtr = sValue.IndexOf("]");
												sTmp = sValue.Substring(2,iPtr-2);
												sValue = sValue.Substring(0, iPtr+1);
												switch(sTmp.ToUpper())
												{
													case "SQLSERVER":
														iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
														break;
													case "SYBASE":
														iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
														break;
													case "INFORMIX":
														iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
														break;
													case "ORACLE":
														iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
														break;
													case "DB2":
														iDbType = (int)eDatabaseType.DBMS_IS_DB2;
														break;
													default:
														iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
														break;
												}
												if (iDbType == m_iDbType )
													objInfoDef.FilterDef[iFilterNum].SQLFill = sValue; 
											}
											else
												objInfoDef.FilterDef[iFilterNum].SQLFill = sValue;
											break;
										case "SQLFROM":
											if(sValue.Substring(0,1)=="[")
											{
												iPtr = sValue.IndexOf("]");
												sTmp = sValue.Substring(2,iPtr-2);
												sValue = sValue.Substring(0, iPtr+1);
												switch(sTmp.ToUpper())
												{
													case "SQLSERVER":
														iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
														break;
													case "SYBASE":
														iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
														break;
													case "INFORMIX":
														iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
														break;
													case "ORACLE":
														iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
														break;
													case "DB2":
														iDbType = (int)eDatabaseType.DBMS_IS_DB2;
														break;
													default:
														iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
														break;
												}
												if (iDbType == m_iDbType )
													objInfoDef.FilterDef[iFilterNum].SQLFrom = sValue; 
											}
											else
												objInfoDef.FilterDef[iFilterNum].SQLFrom = sValue;
											break;

										case "SQLWHERE":
											if(sValue.Substring(0,1)=="[")
											{
												iPtr = sValue.IndexOf("]");
												sTmp = sValue.Substring(2,iPtr-2);
												sValue = sValue.Substring(0, iPtr+1);
												switch(sTmp.ToUpper())
												{
													case "SQLSERVER":
														iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
														break;
													case "SYBASE":
														iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
														break;
													case "INFORMIX":
														iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
														break;
													case "ORACLE":
														iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
														break;
													case "DB2":
														iDbType = (int)eDatabaseType.DBMS_IS_DB2;
														break;
													default:
														iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
														break;
												}
												if (iDbType == m_iDbType )
													objInfoDef.FilterDef[iFilterNum].SQLWhere = sValue; 
											}
											else
												objInfoDef.FilterDef[iFilterNum].SQLWhere = sValue;
											break;
										case "SQL":
											if(sValue.Substring(0,1)=="[")
											{
												iPtr = sValue.IndexOf("]");
												sTmp = sValue.Substring(2,iPtr-2);
												sValue = sValue.Substring(0, iPtr+1);
												switch(sTmp.ToUpper())
												{
													case "SQLSERVER":
														iDbType = (int)eDatabaseType.DBMS_IS_SQLSRVR;
														break;
													case "SYBASE":
														iDbType = (int)eDatabaseType.DBMS_IS_SYBASE;
														break;
													case "INFORMIX":
														iDbType = (int)eDatabaseType.DBMS_IS_INFORMIX;
														break;
													case "ORACLE":
														iDbType = (int)eDatabaseType.DBMS_IS_ORACLE;
														break;
													case "DB2":
														iDbType = (int)eDatabaseType.DBMS_IS_DB2;
														break;
													default:
														iDbType = (int)eDatabaseType.DBMS_IS_ACCESS;
														break;
												}
												if (iDbType == m_iDbType )
													objInfoDef.SQL  = sValue; 
											}
											else
												objInfoDef.SQL = sValue;
											break;
										case "TYPE":
										switch(sValue.ToUpper())
										{
											case "CHECKBOX":
												objInfoDef.FilterDef[iFilterNum].FilterType = 1;
												break;
											case "LIST":
												objInfoDef.FilterDef[iFilterNum].FilterType = 2;
												break;
											case "TOPX":
												objInfoDef.FilterDef[iFilterNum].FilterType = 3;
												break;
											case "COMBO":
												objInfoDef.FilterDef[iFilterNum].FilterType = 4;
												break;
											case "DURATION":
												objInfoDef.FilterDef[iFilterNum].FilterType = 5;
												break;
											default:
												objInfoDef.FilterDef[iFilterNum].FilterType = 0;
												break;
										}
											break;
										case "MIN":
											// MIN and DEFAULT have the same logic
										switch (sValue)
										{
											case "CURRENT_YEAR":
												objInfoDef.FilterDef[iFilterNum].FilterMin = System.DateTime.Now.Year;
												break;
											default:
												objInfoDef.FilterDef[iFilterNum].FilterMin =Conversion.ConvertStrToLong(sValue);
												break;
										}
											break;
										case "DEFAULT":
										switch (sValue)
										{
											case "CURRENT_YEAR":
												objInfoDef.FilterDef[iFilterNum].DefValue  = System.DateTime.Now.Year.ToString() ;
												break;
											default:
												objInfoDef.FilterDef[iFilterNum].DefValue =sValue;
												break;
										}
											break;
										case "MAX":
											objInfoDef.FilterDef[iFilterNum].FilterMax = Conversion.ConvertStrToLong(sValue);
											break;
										case "TABLE":
											objInfoDef.FilterDef[iFilterNum].table = sValue;
											break;
									}
								}
							}						
						}
					}
					if(p_ID>objInfoDef.ID)
						objInfoDef.Name = "";
				}
				return objInfoDef;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAAutoDiarySetup.LoadInfoDef.Err", m_iClientId), p_objEx);  
			}
			finally
			{			
                if (objSRdr != null)
                    objSRdr.Dispose();
				objSys = null;
			}
		}


		/// <summary>
		/// Function GetFilterIndex
		/// </summary>
		/// <param name="p_iIndex">Index</param>
		/// <returns>Selected Index</returns>
		private int GetFilterIndex(InfoDefinition p_objInfoDef, int p_iIndex)
		{
			for(int iCtr=0 ; iCtr<=p_objInfoDef.NumFilters -1;iCtr++)
			{
				if(p_iIndex==p_objInfoDef.FilterDef[iCtr].ID)
					return iCtr;
			}
			return 0;
		}
	}
}