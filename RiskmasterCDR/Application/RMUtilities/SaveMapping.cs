﻿using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   23rd,Feb 2005
	///Purpose :   Tax Mapping implementation form
	/// </summary>	
	public class SaveMapping
	{
		/// <summary>
		/// DSN
		/// </summary>
		private string m_sDSN="";

        private int m_iClientId = 0;

		/// <summary>
		/// DSN
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnectString">Connection String</param>
        public SaveMapping(string p_sConnectString, int p_iClientId)
		{
			m_sDSN=p_sConnectString;
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Fetching the code lis from the database
		/// </summary>
		/// <param name="p_sTableId">Table ID</param>
		/// <param name="p_sConnectString">Connection String</param>
		/// <returns>DataSet containing the List</returns>
		internal DataSet GetCodeList(string p_sTableId,string p_sConnectString)
		{
			DataSet objDs=null;
			StringBuilder objSql=null;
			
			try
			{
				objSql=new StringBuilder();
				objSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT ");
				objSql.Append(" WHERE CODES.TABLE_ID ="+p_sTableId);
				objSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
				objSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 ");
				objSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ");
				objDs=DbFactory.GetDataSet(p_sConnectString,objSql.ToString(),m_iClientId);
				return objDs;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("SaveMapping.GetCodeList.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				if(objDs != null)
					objDs.Dispose(); 
				objSql = null;
			}
		}

		/// <summary>
		/// Fetches the Tax Mapping Data
		/// </summary>
		/// <returns>XML Doc with data</returns>
		public XmlDocument GetTaxMapValues()
		{
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			string sTaxName="";
			XmlElement objElemTemp=null;
			DbReader objReader=null;
			string sStateId="";
			StringBuilder objSql=null;
			string sFname="";
			string sLname="";
			string sEname="";
			LocalCache objCache=null;

			try
			{
				objSql=new StringBuilder();
				objSql.Append(" SELECT * FROM TAX_MAPPING");
				objReader = DbFactory.GetDbReader(m_sDSN,objSql.ToString());
				objDOM =new XmlDocument();
				objElemParent = objDOM.CreateElement("Document");
				objDOM.AppendChild(objElemParent);
				objElemChild=objDOM.CreateElement("TaxMapValues");
				objDOM.FirstChild.AppendChild(objElemChild);
				while (objReader.Read())
				{	
					objCache=new LocalCache(m_sDSN,m_iClientId);
                    objCache.GetEntityInfo(Conversion.ConvertObjToInt64(objReader.GetValue("TAX_EID"), m_iClientId), ref sFname, ref sLname);
					sEname=sLname;
					if (!sFname.Trim().Equals(""))
					{
						sEname+=", "+sFname;
					}
					sStateId=Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID"));
					if (sStateId=="-1")
					{
						sTaxName=Conversion.ConvertObjToStr(objReader.GetValue("TAX_NAME")).Trim().ToUpper();
						if (sTaxName.Equals("FEDERAL")
							||sTaxName.Equals("SOCIAL_SECURITY")
							||sTaxName.Equals("MEDICARE")
							)
						{
							objElemTemp=objDOM.CreateElement("FederalTaxes");
							objElemTemp.SetAttribute("value",sTaxName);
							objElemTemp.SetAttribute("stateId","");
							objElemTemp.SetAttribute("payee",sEname);
							objElemTemp.SetAttribute("transTypeid","");
							objElemTemp.SetAttribute("payeeid",Conversion.ConvertObjToStr(objReader.GetValue("TAX_EID")));
							objElemTemp.SetAttribute("transType",Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE_ID")));
							objElemTemp.SetAttribute("taxPercent",Conversion.ConvertObjToStr(objReader.GetValue("TAX_PERCENTAGE")));
							objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
						}
					}
					else
					{
						objElemTemp=objDOM.CreateElement("StateTax");
						objElemTemp.SetAttribute("value","STATE");
						objElemTemp.SetAttribute("payee",sEname);
						objElemTemp.SetAttribute("payeeid",Conversion.ConvertObjToStr(objReader.GetValue("TAX_EID")));
						objElemTemp.SetAttribute("transTypeid",Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE_ID")));
						objElemTemp.SetAttribute("transType",objCache.GetCodeDesc(Conversion.ConvertObjToInt(objReader.GetValue("TRANS_TYPE_CODE_ID"), m_iClientId)));
						objElemTemp.SetAttribute("taxPercent",Conversion.ConvertObjToStr(objReader.GetValue("TAX_PERCENTAGE")));
						objElemTemp.InnerText=Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID"))
							+"-"+Conversion.ConvertObjToStr(objReader.GetValue("TAX_EID"))
							+"-"+Conversion.ConvertObjToStr(objReader.GetValue("TRANS_TYPE_CODE_ID"));
						objElemTemp.SetAttribute("stateId",Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID")));
						objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
					}
					sEname="";
				}
				return objDOM;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("SaveMapping.GetTaxMapValues.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				objDOM=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
				if(objReader != null)
					objReader.Dispose();
				objSql=null;
				if(objCache!=null)
					objCache.Dispose();
			}
		}

		/// <summary>
		/// Saves the Tax Mapping Data into the database
		/// </summary>
		/// <param name="p_sXml">XML with data</param>
		/// <returns>boolean for success/failure</returns>
		public bool SaveMappingInfo(string p_sXml)
		{
			string sSql=null;
			ArrayList arrlstSql=null;
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;
			XmlNodeList objNodeList=null;
			XmlDocument objDOM=null;

			try
			{
				arrlstSql=new ArrayList();
				sSql=" DELETE FROM TAX_MAPPING WHERE TAX_EID > 0";
				arrlstSql.Add(sSql);
				objDOM=new XmlDocument();
				objDOM.LoadXml(p_sXml);
				objNodeList = objDOM.SelectNodes("//Document/TaxMapValues/FederalTaxes");
				foreach (XmlNode objTmpNode in objNodeList)
				{
					sSql = " INSERT INTO TAX_MAPPING (TAX_NAME, TAX_EID, TRANS_TYPE_CODE_ID, TAX_PERCENTAGE, STATE_ID) VALUES ('"
						+objTmpNode.Attributes["value"].Value+"','"+objTmpNode.Attributes["payeeid"].Value+"','"
						+objTmpNode.Attributes["transType"].Value+"','"+objTmpNode.Attributes["taxPercent"].Value+"','-1')";
					arrlstSql.Add(sSql);
				}
				objNodeList = objDOM.SelectNodes("//Document/TaxMapValues/StateTax");
				foreach (XmlNode objTmpNode in objNodeList)
				{
					sSql = " INSERT INTO TAX_MAPPING (TAX_NAME, TAX_EID, TRANS_TYPE_CODE_ID, TAX_PERCENTAGE, STATE_ID) VALUES ('"
						+objTmpNode.Attributes["value"].Value+"','"+objTmpNode.Attributes["payeeid"].Value+"','"
						+objTmpNode.Attributes["transTypeid"].Value+"','"+objTmpNode.Attributes["taxPercent"].Value+"','"+objTmpNode.InnerText.Split('-')[0]+"')";
					arrlstSql.Add(sSql);
				}
				if(arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sDSN);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < arrlstSql.Count; i++)
					{
						objCommand.CommandText=arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
					objConn.Dispose(); 
					objCommand=null;
					objTran=null;
				}
				return true;	
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("SaveMapping.SaveMappingInfo.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				sSql=null;
				arrlstSql=null;
				if(objConn != null)
					objConn.Dispose();
				if(objTran!=null)
					objTran.Dispose();
				if(objCommand!=null)
					objCommand = null;
				objNodeList=null;
				objDOM=null;
			}
		}

		/// <summary>
		/// Fetches the Transaction Types data
		/// </summary>
		/// <returns>XML Doc with data</returns>
		public XmlDocument GetTransTypes()
		{
			DataSet objDs=null;
			XmlDocument objDOM=null;
			XmlElement objElemParent=null;
			XmlElement objElemChild=null;
			XmlElement objElemTemp=null;
			string sTransType="";
			LocalCache objCache=null;

			try
			{
                objCache = new LocalCache(m_sDSN, m_iClientId);
				sTransType=Conversion.ConvertObjToStr(objCache.GetTableId("TRANS_TYPES"));
				objCache.Dispose();
				objDs=GetCodeList(sTransType,m_sDSN);
				
				if (objDs.Tables[0]!=null)
				{
				
					objDOM =new XmlDocument();
					objElemParent = objDOM.CreateElement("Document");
					objDOM.AppendChild(objElemParent);
					objElemChild=objDOM.CreateElement("TransactionTypes");
					objDOM.FirstChild.AppendChild(objElemChild);
					foreach (DataRow dr in objDs.Tables[0].Rows)
					{
						objElemTemp=objDOM.CreateElement("Transaction");
						objElemTemp.SetAttribute("value",Conversion.ConvertObjToStr(dr["CODE_ID"]));
						objElemTemp.InnerText=Conversion.ConvertObjToStr(dr["SHORT_CODE"]+" - "+Conversion.ConvertObjToStr(dr["CODE_DESC"]));
						objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
					}
				
				}//end if
				return objDOM;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
                throw new RMAppException(Globalization.GetString("SaveMapping.GetTransTypes.Error", m_iClientId), p_objExp);   
			}
			finally
			{
				if(objDs!=null)
					objDs.Dispose();
				objDOM=null;
				objElemParent=null;
				objElemChild=null;
				objElemTemp=null;
				if(objCache!=null)
					objCache.Dispose();
			}
		}
	}
}
