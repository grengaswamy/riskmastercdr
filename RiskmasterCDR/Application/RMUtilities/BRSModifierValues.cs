using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   02nd,May 2005
	///Purpose :   Shows the Modifier Values List form
	/// </summary>
	public class BRSModifierValues:UtilitiesUIListBase 
	{
        private int m_iClientId = 0;
		/// <summary>
		/// Private string variable for database field mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "MODIFIER_ROW_ID"},
			{"ModifierCode","MODIFIER_CODE"},
			{"StartCPT","START_CPT"},
			{"EndCPT","END_CPT"},
			{"ModValue","MODIFIER_VALUE"},
			{"MaxRLV","MAX_RLV"}
			};

		/// Name		: BRSModifierValues
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default constructor with connection string
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        public BRSModifierValues(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
            m_iClientId = p_iClientId;
			base.ConnectString = p_sConnString;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "BRS_MOD_VALUES";
			base.KeyField = "MODIFIER_ROW_ID";
			base.RadioName = "BRSModifierValuesList";			
			base.WhereClause = "";
			base.OrderByClause = "";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieve the data for Modifier Values list
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with xml structure</param>
		/// <returns>List data in the xml document format</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
            XmlNodeList objNodeLst = null;			
			XmlNode objNode = null;
			try
			{
				XMLDoc = p_objXmlDocument;
				objNode = p_objXmlDocument.SelectSingleNode("//tableid");
				string sTableId = objNode.InnerText;
                //Changed by bsharma33 for Pan Testing MITS 26941
				//base.WhereClause  = "FEETABLE_ID =" + sTableId;
                base.WhereClause = string.Format("FEETABLE_ID ={0}", "~sTableId~");
                base.dictParams.Add("sTableId", sTableId);
				base.Get();
                base.dictParams.Clear();
                //End changes by bsharma33 for Pan Testing MITS 26941
				objNodeLst = p_objXmlDocument.SelectNodes("//listrow/ModifierCode");
				foreach(XmlElement objElm in objNodeLst)
				{
                    objElm.InnerText = UTILITY.GetCode(objElm.InnerText, base.ConnectString, m_iClientId);    
				}
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSModifierValues.Get.Err", m_iClientId), p_objEx);
			}
			finally
			{
				objNodeLst = null;			
				objNode = null;
			}
		}
	}
}