﻿using System;
using System.Xml;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Data;
using System.Globalization;
using Riskmaster.Common;
using System.Collections.Generic;


namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    /// Deb Jena
    /// ML Implementation
    /// </summary>
    public class MultiLanguage
    {
        #region Private Functions
        private string m_sUserName = string.Empty;

        /// <summary>
        /// Connection String
        /// </summary>
        private string m_sConnectString = string.Empty;

        /// <summary>
        /// Private variable for Client Id
        /// </summary>
        private int m_iClientId = 0;
        #endregion Private Functions
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sLoginName"></param>
        /// <param name="p_sConnectString"></param>
        public MultiLanguage(string p_sLoginName, string p_sConnectString, int p_iClientId)
        {
            m_sUserName = p_sLoginName;
            m_sConnectString = p_sConnectString;
            m_iClientId = p_iClientId;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_sConnectString"></param>
        public MultiLanguage(string p_sConnectString, int p_iClientId)
        {
            m_sConnectString = p_sConnectString;
            m_iClientId = p_iClientId;
        }
        /// <summary>
        /// Get the languages that are supported
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument GetLanguages(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;
            DbReader objRead = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            XmlElement objRowHeader = null;
            string sPageID = string.Empty;
            string sLangId = string.Empty;
            try
            {
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PageId");
                if (objElement != null)
                    sPageID = objElement.InnerText;

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//LangId");
                if (objElement != null)
                    sLangId = objElement.InnerText;

                sSQL = "SELECT * FROM SYS_LANGUAGES ORDER BY LANGUAGE_NAME";
                objRead = DbFactory.GetDbReader(RMConfigurationSettings.GetSecurityDSN(m_iClientId), sSQL);
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//MultiLanguageSetup/listhead");

                objRowHeader = p_objXmlDocument.CreateElement("RowId");
                objRowHeader.InnerText = "Row Id";
                objElement.AppendChild(objRowHeader);

                objRowHeader = p_objXmlDocument.CreateElement("LanguageName");
                // objRowHeader.InnerText = "Language Name";
                objRowHeader.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrLangName", 0, sPageID, m_iClientId), sLangId);
                objElement.AppendChild(objRowHeader);

                objRowHeader = p_objXmlDocument.CreateElement("Suppported");
                //rsharma220 MITS 33737 : Corrected the spelling of supported
                // objRowHeader.InnerText = "Supported";
                objRowHeader.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrSupported", 0, sPageID, m_iClientId), sLangId);
                objElement.AppendChild(objRowHeader);

                /*Start Modified By ttumula2 on 16/Dec/2014(RMA-6033) */
                objRowHeader = p_objXmlDocument.CreateElement("FormatDate");
                //objRowHeader.InnerText = "Date Format";
                objRowHeader.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("gvHdrDateFormat", 0, sPageID, m_iClientId), sLangId);
                objElement.AppendChild(objRowHeader);
                /*End Modified By ttumula2 on 16/Dec/2014(RMA-6033) */

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//MultiLanguageSetup");

                while (objRead.Read())
                {
                    objLstRow = p_objXmlDocument.CreateElement("listrow");

                    //we will use Conversion.ConvertObjToStr insteaof Cpnversion.CastToType as want the output as string and CastToType function accepts string as input
                    objRowTxt = p_objXmlDocument.CreateElement("RowId");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("LANG_ID"));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("LanguageName");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("LANGUAGE_NAME"));
                    objLstRow.AppendChild(objRowTxt);

                    objRowTxt = p_objXmlDocument.CreateElement("Suppported");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SUPPORTED_FLAG"));
                    if (objRowTxt.InnerText == "-1")
                        //objRowTxt.InnerText = "Yes";
                        objRowTxt.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("lblYes", 0, sPageID, m_iClientId), sLangId);
                    else
                        //objRowTxt.InnerText = "No";
                        objRowTxt.InnerText = CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("lblNo", 0, sPageID, m_iClientId), sLangId);
                    objLstRow.AppendChild(objRowTxt);

                    /*Start Modified By ttumula2 on 16/Dec/2014(RMA-6033) */
                    objRowTxt = p_objXmlDocument.CreateElement("FormatDate");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("DATEFORMAT"));
                    objLstRow.AppendChild(objRowTxt);
                    /*End Modified By ttumula2 on 16/Dec/2014(RMA-6033) */

                    objElement.AppendChild(objLstRow);
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                //throw new RMAppException(Globalization.GetString("MultiLanguage.GetLanguage.Error", m_iClientId), p_objEx);
                // throw new Exception(CommonFunctions.FilterBusinessMessage(Globalization.GetString("MultiLanguage.GetLanguage.Error", m_iClientId), sLangId), p_objEx);
                throw new Exception(Globalization.GetString("MultiLanguage.GetLanguage.Error", m_iClientId, sLangId), p_objEx); //Add by ttumula2 on jan 13 2015 for 6033

            }
            finally
            {
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                objRead = null;
            }
        }
        /// <summary>
        /// GetLanguageFormats
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument GetLanguageFormats(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;
            DbReader objRead = null;
            XmlElement objLstRow = null;
            XmlElement objRowTxt = null;
            XmlElement objRowHeader = null;
            string sCountryRowId = string.Empty;
            try
            {
                sSQL = "select CODE_ID,SHORT_CODE,CODE_DESC from CODES_TEXT where CODE_ID in (select CODE_ID from CODES where TABLE_ID=(select TABLE_ID from GLOSSARY where SYSTEM_TABLE_NAME='COUNTRY') and deleted_flag='0')";
                
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//RegionalLanguageSetup/listhead");

                objRowHeader = p_objXmlDocument.CreateElement("RowId");
                objRowHeader.InnerText = "Row Id";
                objElement.AppendChild(objRowHeader);

                objRowHeader = p_objXmlDocument.CreateElement("CountryName");
                objRowHeader.InnerText = "Country Name";
                objElement.AppendChild(objRowHeader);

                objRowHeader = p_objXmlDocument.CreateElement("ZipCodeFormat");
                objRowHeader.InnerText = "ZipCode Format";
                objElement.AppendChild(objRowHeader);

                objRowHeader = p_objXmlDocument.CreateElement("PhoneFormat");
                objRowHeader.InnerText = "Phone Format";
                objElement.AppendChild(objRowHeader);
                              
                objRowHeader = p_objXmlDocument.CreateElement("SSNFormat");
                objRowHeader.InnerText = "SSN Format";
                objElement.AppendChild(objRowHeader);
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//RegionalLanguageSetup");

                using (objRead = DbFactory.GetDbReader(m_sConnectString, sSQL))
                {
                    while (objRead.Read())
                    {
                        sCountryRowId = Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID"));
                        objLstRow = p_objXmlDocument.CreateElement("listrow");

                        objRowTxt = p_objXmlDocument.CreateElement("RowId");
                        objRowTxt.InnerText = sCountryRowId;
                        objLstRow.AppendChild(objRowTxt);

                        objRowTxt = p_objXmlDocument.CreateElement("CountryName");
                        objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")) + " " + Conversion.ConvertObjToStr(objRead.GetValue("CODE_DESC"));
                        objLstRow.AppendChild(objRowTxt);

                        objRowTxt = p_objXmlDocument.CreateElement("ZipCodeFormat");
                        objRowTxt.InnerText = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectString, "SELECT ZIPCODE_FORMATDESC FROM ZIPCODE_FORMAT WHERE COUNTRY_ID=" + sCountryRowId));
                        if (objRowTxt.InnerText == "0")  //rsharma220 MITS 35423
                        {
                            objRowTxt.InnerText = " ";
                        }
                        objLstRow.AppendChild(objRowTxt);

                        objRowTxt = p_objXmlDocument.CreateElement("PhoneFormat");
                        objRowTxt.InnerText = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(m_sConnectString, "SELECT PHONE_FORMATDESC FROM PHONE_FORMAT WHERE COUNTRY_ID=" + sCountryRowId));
                        if (objRowTxt.InnerText == "0")  //rsharma220 MITS 35423
                        {
                            objRowTxt.InnerText = " ";
                        }
                        objLstRow.AppendChild(objRowTxt);
                        objRowTxt = p_objXmlDocument.CreateElement("SSNFormat");
                        string sSql = string.Format("SELECT SSN_FORMATDESC FROM SSN_FORMAT WHERE COUNTRY_ID='{0}'", sCountryRowId);
                        string sSSNDesc = string.Empty;
                        using (DbReader dbr = DbFactory.ExecuteReader(m_sConnectString, sSql))
                        {
                            while (dbr.Read())
                            {
                                if (sSSNDesc == string.Empty)
                                    sSSNDesc = dbr.GetString("SSN_FORMATDESC");
                                else
                                    sSSNDesc = sSSNDesc + " , " + dbr.GetString("SSN_FORMATDESC");
                            }
                            objRowTxt.InnerText = sSSNDesc;
                            objLstRow.AppendChild(objRowTxt);
                        }
                        if (objRowTxt.InnerText == "0")
                        {
                            objRowTxt.InnerText = " ";
                        }

                        objElement.AppendChild(objLstRow);
                    }
                }
            }
            catch (Exception ee)
            {

            }
            return p_objXmlDocument;
        }
        /// <summary>
        /// GetFormatLists
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument GetFormatLists(XmlDocument p_objXmlDocument)
        {
            string sSQL = string.Empty;
            DbReader objRdr = null;
            XmlElement objElm = null;
            XmlElement objNewElm = null;
            XmlElement objElement = null;
            int isSSNNumeric;
            string sCountryId = string.Empty;
            string sValue = string.Empty;
            objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
            if (objElement != null)
            {
                if (objElement.InnerText != null)
                {
                    if (objElement.InnerText.Trim() != "")
                    {
                        sCountryId = objElement.InnerText;
                        sSQL = "select CODE_ID,SHORT_CODE,CODE_DESC from CODES_TEXT where CODE_ID=" + sCountryId;
                        using (objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL))
                        {
                            while (objRdr.Read())
                            {
                                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Country']");
                                objElement.InnerText = Conversion.ConvertObjToStr(objRdr.GetValue("SHORT_CODE")) + " " + Conversion.ConvertObjToStr(objRdr.GetValue("CODE_DESC")); ;
                            }
                        }
                        sSQL = "SELECT ZIPCODE_FORMAT_DESC FROM ZIPCODE_FORMAT_LIST";
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ZipCodeFormats']");
                        objElm.InnerText = string.Empty;
                        if (objElm != null)
                        {
                            objNewElm = p_objXmlDocument.CreateElement("option");
                            objElm.AppendChild(objNewElm);
                            objNewElm = null;
                            objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objRdr.Read())
                            {
                                objNewElm = p_objXmlDocument.CreateElement("option");
                                objNewElm.SetAttribute("value", objRdr.GetString(0));
                                objNewElm.InnerText = objRdr.GetString(0);
                                objElm.AppendChild(objNewElm);
                                objNewElm = null;
                            }
                        }
                        objElm.Attributes["value"].Value =Convert.ToString(DbFactory.ExecuteScalar(m_sConnectString, "SELECT ZIPCODE_FORMATDESC FROM ZIPCODE_FORMAT WHERE COUNTRY_ID=" + sCountryId));
                        sSQL = "SELECT PHONE_FORMAT_DESC FROM PHONE_FORMAT_LIST";
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PhoneFormats']");
                        objElm.InnerText = string.Empty;
                        if (objElm != null)
                        {
                            objNewElm = p_objXmlDocument.CreateElement("option");
                            objElm.AppendChild(objNewElm);
                            objNewElm = null;
                            objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objRdr.Read())
                            {
                                objNewElm = p_objXmlDocument.CreateElement("option");
                                objNewElm.SetAttribute("value", objRdr.GetString(0));
                                objNewElm.InnerText = objRdr.GetString(0);
                                objElm.AppendChild(objNewElm);
                                objNewElm = null;
                            }
                        }
                        objElm.Attributes["value"].Value = Convert.ToString(DbFactory.ExecuteScalar(m_sConnectString, "SELECT PHONE_FORMATDESC FROM PHONE_FORMAT WHERE COUNTRY_ID=" + sCountryId));
                        sSQL = "SELECT SSN_FORMAT_DESC  FROM SSN_FORMAT_LIST";
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SSNFormats']");
                        objElm.InnerText = string.Empty;
                        if (objElm != null)
                        {
                            objNewElm = p_objXmlDocument.CreateElement("option");
                            objElm.AppendChild(objNewElm);
                            objNewElm = null;
                            objRdr = DbFactory.GetDbReader(m_sConnectString, sSQL);
                            while (objRdr.Read())
                            {
                                objNewElm = p_objXmlDocument.CreateElement("option");
                                objNewElm.SetAttribute("value", objRdr.GetString(0));
                                objNewElm.InnerText = objRdr.GetString(0);
                                objElm.AppendChild(objNewElm);
                                objNewElm = null;
                            }
                        }
                        objRdr = null;
                        objRdr = DbFactory.ExecuteReader(m_sConnectString, "SELECT SSN_FORMATDESC,SSN_ISNUMERIC FROM SSN_FORMAT WHERE COUNTRY_ID=" + sCountryId);
                        while (objRdr.Read())
                        {
                            if (string.IsNullOrEmpty(sValue))
                            {
                                sValue = objRdr.GetString("SSN_FORMATDESC");
                            }
                            else
                            {
                                sValue = sValue + "," + objRdr.GetString("SSN_FORMATDESC");
                            }
                            isSSNNumeric = Convert.ToInt16(objRdr.GetValue("SSN_ISNUMERIC"));
                            if (isSSNNumeric == 0)
                            {
                                objElm.Attributes["isnumeric"].Value = "false";
                            }
                            else
                                objElm.Attributes["isnumeric"].Value = "true";
                        }
                        objElm.Attributes["value"].Value = sValue;
                    }
                }
            }
            return p_objXmlDocument;
        }
        /// <summary>
        /// Get Multi langage selected
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument GetSelectedMultiLanguageInfo(XmlDocument p_objXmlDocument)
        {
            XmlElement objElement = null;
            string sSQL = string.Empty;
            DbReader objRead = null;
            XmlElement objElm = null; // Add By ttumula2 on 17 Dec 2014(RMA-6033)
            XmlElement objNewElm = null; // Add By ttumula2 on 17 Dec 2014(RMA-6033)
            string sLangId = string.Empty;// Add By ttumula2 on 17 Dec 2014(RMA-6033)
            string DateFormat = string.Empty; // Add By ttumula2 on 17 Dec 2014(RMA-6033)
            string sPageID = string.Empty;



            try
            {
                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//PageId");
                if (objElement != null)
                    sPageID = objElement.InnerText;

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//LangId");
                if (objElement != null)
                    sLangId = objElement.InnerText;

                objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objElement != null)
                {
                    if (objElement.InnerText != null)
                    {
                        if (objElement.InnerText.Trim() != "")
                        {
                            sLangId = objElement.InnerText;

                            /*Start Modified By ttumula2 on 16/Dec/2014(RMA-6033)*/

                            //sSQL = "SELECT LANGUAGE_NAME,SUPPORTED_FLAG FROM SYS_LANGUAGES WHERE LANG_ID=" + objElement.InnerText;
                            sSQL = "SELECT LANGUAGE_NAME,SUPPORTED_FLAG,DATEFORMAT FROM SYS_LANGUAGES WHERE LANG_ID=" + objElement.InnerText;

                            /*End Modified By ttumula2 on 16/Dec/2014(RMA-6033)*/
                            using (objRead = DbFactory.GetDbReader(RMConfigurationSettings.GetSecurityDSN(m_iClientId), sSQL))
                            {
                                while (objRead.Read())
                                {
                                    objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='LanguageName']");
                                    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("LANGUAGE_NAME"));


                                    objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Supported']");
                                    objElement.Attributes["value"].Value = Conversion.ConvertObjToStr(objRead.GetValue("SUPPORTED_FLAG"));
                                    //objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SUPPORTED_FLAG"));

                                    /*Start Modified By ttumula2 on 16/Dec/2014(RMA-6033)*/
                                    objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FormatDate']");
                                    // objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("DATEFORMAT")); // Comment By ttumula2 on 17 Dec 2014(RMA-6033)
                                    DateFormat = Conversion.ConvertObjToStr(objRead.GetValue("DATEFORMAT")); // Add By ttumula2 on 17 Dec 2014(RMA-6033)
                                    /*End Modified By ttumula2 on 16/Dec/2014(RMA-6033)*/
                                }
                            }
                            /*Start Modified By ttumula2 on 17/Dec/2014(RMA-6033)*/
                            sSQL = "SELECT DATE_FORMAT_DESC FROM DATE_FORMAT_LIST";
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FormatDate']");
                            objElm.InnerText = string.Empty;
                            if (objElm != null)
                            {
                                objNewElm = p_objXmlDocument.CreateElement("option");
                                objElm.AppendChild(objNewElm);
                                objNewElm = null;
                                objRead = DbFactory.GetDbReader(RMConfigurationSettings.GetSecurityDSN(m_iClientId), sSQL);
                                while (objRead.Read())
                                {
                                    objNewElm = p_objXmlDocument.CreateElement("option");
                                    objNewElm.SetAttribute("value", objRead.GetString(0));
                                    objNewElm.InnerText = objRead.GetString(0);
                                    objElm.AppendChild(objNewElm);
                                    objNewElm = null;
                                }
                            }
                            objElm.Attributes["value"].Value = DateFormat;
                            /*End Modified By ttumula2 on 17/Dec/2014(RMA-6033)*/
                        }
                    }
                }
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                // throw new RMAppException(Globalization.GetString("MultiLanguage.GetSelectedMultiLanguageInfo.Error", m_iClientId), p_objEx);
                //throw new Exception(CommonFunctions.FilterBusinessMessage(Globalization.GetString("MultiLanguage.GetSelectedMultiLanguageInfo.Error", m_iClientId), sLangId), p_objEx);
                throw new Exception(Globalization.GetString("MultiLanguage.GetSelectedMultiLanguageInfo.Error", m_iClientId, sLangId), p_objEx);  //Add by ttumula2 on jan 13 2015 for 6033

            }
            finally
            {
                if (objRead != null)
                {
                    objRead = null;
                }
            }
        }
        /// <summary>
        /// Update the Language
        /// </summary>
        /// <param name="p_objXmlDocument"></param>
        /// <returns></returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
            int isNumeric;
            string sRowId = string.Empty;
            string sSuppFlag = string.Empty;
            string sDateFormat = string.Empty; // Add By ttumula2 on 16 Dec 2014(RMA-6033)
            string[] sDataSeparator = { "|^|" };
            string sSQL = string.Empty;
            XmlElement objElm = null;
            DbWriter dbr = null;
            DbReader objRead = null;
            string splitValue = string.Empty;
            string[] sValue = { };
            string sTemp = string.Empty;
            Riskmaster.Db.DbCommand objCommand = null;
            string sLangId = string.Empty;
            string sPageID = string.Empty;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//form");
                if (objElm != null)
                {
                    if (objElm.Attributes["name"].Value != "RegionalLanguageSetup")
                    {
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                        if (objElm != null)
                            sRowId = objElm.InnerText;

                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Supported']");
                        if (objElm != null)
                            sSuppFlag = objElm.GetAttribute("value");

                        /* Start Modified By ttumula2 on 16 Dec 2014(RMA-6033)*/

                        //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FormatDate']");
                        //if (objElm != null)
                        //sDateFormat = objElm.InnerText;

                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='FormatDate']");
                        if (objElm != null) // Add By ttumula2 on 17 Dec 2014(RMA-6033)
                        {
                            sDateFormat = objElm.GetAttribute("value");
                        }


                        /* End Modified By ttumula2 on 16 Dec 2014(RMA-6033)*/

                        if (!string.IsNullOrEmpty(sRowId))
                        {
                            dbr = DbFactory.GetDbWriter(RMConfigurationSettings.GetSecurityDSN(m_iClientId));
                            dbr.Tables.Add("SYS_LANGUAGES");
                            dbr.Where.Add(string.Format("LANG_ID={0}", sRowId));
                            dbr.Fields.Add("SUPPORTED_FLAG", sSuppFlag);
                            dbr.Fields.Add("DATEFORMAT", sDateFormat); // Add By ttumula2 on 16 Dec 2014(RMA-6033)
                            dbr.Execute();
                        }
                    }
                    else
                    {
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//PageId");
                        if (objElm != null)
                            sPageID = objElm.InnerText;
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//LangId");
                        if (objElm != null)
                            sLangId = objElm.InnerText;
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                        if (objElm != null)
                            sRowId = objElm.InnerText;
                        if (!string.IsNullOrEmpty(sRowId))
                        {
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='PhoneFormats']");
                            if (objElm != null)
                            {
                                sTemp = objElm.GetAttribute("value");
                            }
                            //rsharma220 MITS 35423
                            sSQL = "SELECT * FROM PHONE_FORMAT WHERE COUNTRY_ID=" + sRowId;
                            objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);

                            dbr = DbFactory.GetDbWriter(m_sConnectString);
                            dbr.Tables.Add("PHONE_FORMAT");

                            if (objRead.Read())
                            {
                                dbr.Where.Add(string.Format("COUNTRY_ID={0}", sRowId));
                            }
                            else
                            {
                                dbr.Fields.Add("COUNTRY_ID", sRowId);
                            }
                            dbr.Fields.Add("PHONE_FORMATDESC", sTemp);
                            dbr.Execute();
                            dbr.Reset(true);

                            //caggarwal4 RMA-6039 

                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='SSNFormats']");
                            if (objElm != null)
                            {
                                splitValue = objElm.GetAttribute("value");
                                sValue = splitValue.Split(sDataSeparator, StringSplitOptions.None);
                            }
                            string isNum = objElm.GetAttribute("isnumeric");
                            if (isNum == "False")
                                isNumeric = 0;
                            else
                                isNumeric = -1;
                            DbFactory.ExecuteNonQuery(m_sConnectString, String.Format("DELETE FROM SSN_FORMAT WHERE COUNTRY_ID='{0}'", sRowId));
                            dbr = DbFactory.GetDbWriter(m_sConnectString);
                            foreach (string str in sValue)
                            {
                                dbr.Tables.Add("SSN_FORMAT");
                                if (!string.IsNullOrEmpty(str))
                                {
                                    dbr.Fields.Add("COUNTRY_ID", sRowId);
                                    dbr.Fields.Add("SSN_FORMATDESC", str);
                                    dbr.Fields.Add("SSN_ISNUMERIC", isNumeric);
                                    dbr.Execute();
                                    dbr.Reset(true);
                                }
                            }
                            dbr.Reset(true);
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ZipCodeFormats']");
                            if (objElm != null)
                            {
                                sTemp = objElm.GetAttribute("value");
                            }
                            //rsharma220 MITS 35423
                            sSQL = "SELECT * FROM ZIPCODE_FORMAT WHERE COUNTRY_ID=" + sRowId;
                            objRead = DbFactory.GetDbReader(m_sConnectString, sSQL);

                            dbr.Tables.Add("ZIPCODE_FORMAT");
                            if (objRead.Read())
                            {
                                dbr.Where.Add(string.Format("COUNTRY_ID={0}", sRowId));
                            }
                            else
                            {
                                dbr.Fields.Add("COUNTRY_ID", sRowId);
                            }
                            dbr.Fields.Add("ZIPCODE_FORMATDESC", sTemp);
                            dbr.Execute();
                            
                        }
                    }
                }
                return p_objXmlDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                // throw new RMAppException(Globalization.GetString("MultiLanguage.Save.Error", m_iClientId), p_objEx);
                // throw new Exception(CommonFunctions.FilterBusinessMessage(Globalization.GetString("MultiLanguage.Save.Error", m_iClientId), sLangId), p_objEx);
                throw new Exception(Globalization.GetString("MultiLanguage.Save.Error", m_iClientId, sLangId), p_objEx); //Add by ttumula2 on jan 13 2015 for 6033


            }
            finally
            {
                objElm = null;
                if (dbr != null)
                    dbr = null;
                if (objRead != null)
                {
                    objRead = null;
                }
            }
        }

    }
}
