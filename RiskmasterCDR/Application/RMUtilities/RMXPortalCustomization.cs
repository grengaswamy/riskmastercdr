﻿

using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.Application;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    ///Author  :   
    ///Dated   :   
    ///Purpose :   This class deals with adding users via the Customized User list
    /// </summary>
    public class RMXPortalCustomization 
    {
        #region Private variables
        /// <summary>
        /// Connection string
        /// </summary>
        private string m_sDSN = "";
        /// <summary>
        /// Secure DB Connection string
        /// </summary>
        private string m_sSecDSN = "";
        /// <summary>
        /// DSN id
        /// </summary>
        private string m_sDsnId = "";
        /// <summary>
        /// DB Reader for retriving group names
        /// </summary>
        DbReader m_objReader = null;
        /// <summary>
        /// User Login
        /// </summary>
        private int m_iUserId = 0;

        private int m_ClientId = 0;

        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public RMXPortalCustomization() { }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>

        public RMXPortalCustomization(string p_sDSN, string p_sDsnId, string p_sSecureDSN, int p_iUserId,int p_iClientId)
        {
            m_sDSN = p_sDSN;
            m_sDsnId = p_sDsnId;
            m_sSecDSN = p_sSecureDSN;
            m_iUserId = p_iUserId;
            m_ClientId = p_iClientId;
        }
        #endregion


        #region Public Functions

        /// Name		: GetUserList
        /// Author		: Rahul Solanki
        /// Date Created: 3/14/2008		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************   
        public DataSet GetUserList()
        {
            
            StringBuilder sbSql = null;
            DbConnection objRMConn;
            DbReader objReader = null;           
            string sSQL = "";
            Hashtable hstModuleGroups = new Hashtable();
            int iGrpId = 0, i=0, j=0, iUserId =0;
            string sGrpName = string.Empty;
            IDictionaryEnumerator ideModuleGroup ;
            DataSet objDsUsers;
            DataSet objDsGroups;

            try
            {
                objRMConn = DbFactory.GetDbConnection(m_sDSN);

                sbSql = new StringBuilder();
                sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_DETAILS_TABLE.LOGIN_NAME FROM USER_DETAILS_TABLE, USER_TABLE WHERE DSNID=");                
                sbSql.Append(m_sDsnId);
                sbSql.Append(" AND USER_TABLE.USER_ID <> 0 AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ORDER BY USER_DETAILS_TABLE.LOGIN_NAME");

                objDsUsers = Riskmaster.Db.DbFactory.GetDataSet(m_sSecDSN, sbSql.ToString(), m_ClientId);
                objDsUsers.Tables[0].TableName = "Users";
                int iUserCount =  objDsUsers.Tables[0].Rows.Count ;
               
                //ojDataSet.WriteXml(@"c:\dataset.xml");

               
                objDsUsers.Tables[0].Columns.Add("GROUP_NAME");
               


                objRMConn.Open();                
                sSQL = "SELECT GROUP_ID, GROUP_NAME FROM USER_GROUPS ORDER BY GROUP_NAME";
                objReader = objRMConn.ExecuteReader(sSQL);

                while (objReader.Read())
                {                    
                    iGrpId =objReader.GetInt("GROUP_ID");
                    sGrpName = objReader.GetString("GROUP_NAME");
                    if (iGrpId != 0 && iGrpId != 0 && sGrpName.Length >0 )
                    {
                        hstModuleGroups.Add(iGrpId, sGrpName);
                    }                    
                }
                objReader.Close();

                ideModuleGroup = hstModuleGroups.GetEnumerator();
                while (ideModuleGroup.MoveNext())
                {
                    sSQL = "SELECT USER_ID FROM USER_MEMBERSHIP WHERE GROUP_ID=" + ideModuleGroup.Key.ToString();
                    string sGroupName = ideModuleGroup.Value.ToString();
                    objDsGroups = new DataSet();
                    objDsGroups = Riskmaster.Db.DbFactory.GetDataSet(m_sDSN, sSQL.ToString(), m_ClientId);
                    
                    int iGroupsCount = objDsGroups.Tables[0].Rows.Count;
                    if ( iGroupsCount  > 0)
                    {                        
                        for (i = 0; i < iGroupsCount; i++ )
                        {
                            iUserId = Convert.ToInt32 (objDsGroups.Tables[0].Rows[i]["USER_ID"]);
                            for (j = 0; j < iUserCount; j++)
                            {
                                if (Convert.ToInt32(objDsUsers.Tables[0].Rows[j]["USER_ID"]) == iUserId)                           
                                 {
                                    objDsUsers.Tables[0].Rows[j]["GROUP_NAME"] =  sGroupName;
                                 }
                            }
                        }
                    }
                    objDsGroups.Dispose();                   
                }                

            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CustomizeUserList.Get.Error",m_ClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }                                
            }
            return objDsUsers;
            //return p_objXmlDocument;
        }


        private string GetGroupType(int p_iUserId)
        {
            string sSQL = "";
            string sGroupName = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT GROUP_NAME FROM USER_GROUPS,USER_MEMBERSHIP WHERE USER_MEMBERSHIP.GROUP_ID= USER_GROUPS.GROUP_ID AND USER_MEMBERSHIP.USER_ID =" + p_iUserId;

                // m_objReader = DbFactory.GetDbReader(m_sDSN, sSQL);
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL);


                if (objReader.Read())
                {
                    sGroupName = Conversion.ConvertObjToStr(objReader.GetValue(0));
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return sGroupName;

        }
        private void logDebug(string sAdaptorMethod, string p_sXmlSring)
        {
            XmlDocument xmlRequest = new XmlDocument();
            xmlRequest.Load(new StringReader("<debug></debug>"));
            XmlElement elem = xmlRequest.CreateElement("text");
            elem.InnerText = p_sXmlSring;
            xmlRequest.DocumentElement.AppendChild(elem);

            // First, log the error to the error log

            // Add overall result status
            string sCall = sAdaptorMethod;
            sCall += " (Debug logging)";

            // Log the overall call failure and the input envelope (only if call failed)

            LogItem oLogItem = new LogItem();
            oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
            oLogItem.Category = "CommonWebServiceLog";
            oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
            oLogItem.Message = sAdaptorMethod + " call debugging.";
            if (xmlRequest != null)
                try
                {
                    oLogItem.RMParamList.Add("XML Input Envelope", xmlRequest.OuterXml);
                }
                catch (Exception)
                {

                    oLogItem.RMParamList.Add("XML Input Envelope", "xmlRequest contents could not be logged - likely too large for available memory.");
                }
            Log.Write(oLogItem,m_ClientId);
        }
        #endregion



    }
}
