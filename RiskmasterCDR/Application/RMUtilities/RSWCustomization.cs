﻿using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.Security;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;


namespace Riskmaster.Application.RMUtilities
{ /// <summary>
    
    
    ////// </summary>
    public class RSWCustomization
    {
        #region Variable Declaration


        private string m_sDSN = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
      //  private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
      //  private DataModelFactory m_objDataModelFactory = null;

        //  private string m_sFormName = "";

        //skhare7:MITS 22119
        StringBuilder sbSQL = null;
        string sCurrenDate = string.Empty;//variable for date
        int iMonthNow = 0;				//current month
        int iDay = 0;						//current day
        StringBuilder sSQL = null;

        private int m_iClientId = 0;
        //End
        /// <summary>
        /// Language code of the user logged in
        /// </summary>
        private int m_iLangCode = 0; //Aman MITS 31626
        #endregion
        //Aman MITS 31626
        #region Properties
        
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }

        #endregion
        //Aman MITS 31626
        #region Constructor
        public RSWCustomization(string p_sConnString, int p_iClientId)
        {
            m_iClientId = p_iClientId;
            m_sDSN = p_sConnString;
           
        }
        #endregion
        private string[,] arrFields =
        {
         {"LOB_CODE","LOB_CODE"}
        };
  
        private XmlDocument GetReserveType(string p_sLobCode, string p_sClaimCode, bool bReserveByClaim)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemTemp = null;
            StringBuilder sSQL = null;
            DbReader objReader = null;
            try
            {
                objDOM = new XmlDocument();
              objElemParent = objDOM.CreateElement("Reserves");
             objDOM.AppendChild(objElemParent);
             sSQL = new StringBuilder();
             //Aman MITS 31626 -- made the query ML Specific --start
                if (bReserveByClaim)
                {   
                   
                    sSQL.Append("SELECT SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_CLM_TYPE_RES, CODES_TEXT, CODES ");
                    sSQL.Append(" WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sSQL.Append(" CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = " + p_sLobCode + " AND SYS_CLM_TYPE_RES.CLAIM_TYPE_CODE=" + p_sClaimCode + "");
                    sSQL.Append(" ORDER BY CODES_TEXT.CODE_DESC");

                }
                else
                {   
                   
                    sSQL.Append("SELECT SYS_LOB_RESERVES.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_LOB_RESERVES, CODES_TEXT, CODES ");
                    sSQL.Append(" WHERE SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND");
                    sSQL.Append(" CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = " + p_sLobCode + "");
                    sSQL.Append("  ORDER BY CODES_TEXT.CODE_DESC");
                }
                sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "RESERVE_TYPE", this.LanguageCode);
                //Aman MITS 31626 --end
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("Types");
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")));
                   
                    //objElemTemp.InnerText =  objReader.GetString("CODE_DESC");
                    objElemTemp.InnerText = objReader.GetString(2); //Aman MITS 31626
                    objDOM.FirstChild.AppendChild(objElemTemp);
                   
                    //strCodeId.Append(strCodeId);
                    //strCodeId.Append(" ");
                    //strCodeId.Append(Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")));
                  //  strCodeId = strCodeId + " " + Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE"));

                }

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
           
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemTemp = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                sSQL = null;
                
            }
        }
        public XmlDocument GetTranstype(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemTemp = null;
           
             XmlNode objNode = null;
            string p_iReservetypeCode = "";
            DbReader objReader = null;
            sSQL = new StringBuilder();
            sCurrenDate = DateTime.Now.Year.ToString();

            //for the date string
            //skhare7:MITS 22119
            if (iMonthNow < 10)
                sCurrenDate = sCurrenDate + "0" + iMonthNow.ToString();
            else
                sCurrenDate = sCurrenDate + iMonthNow.ToString();

            if (iDay < 10)
                sCurrenDate = sCurrenDate + "0" + iDay.ToString();
            else
                sCurrenDate = sCurrenDate + iDay.ToString();
            //skhare7:MITS 22119
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode(".//TransReserveType");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                        p_iReservetypeCode = objNode.InnerText;
                }
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("TransTypes");
                objDOM.AppendChild(objElemParent);
                //skhare7:MITS 22119
                sSQL.Append("SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId from CODES,CODES_TEXT WHERE   CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =" + p_iReservetypeCode + " ");
                sSQL.Append(" AND  CODES.DELETED_FLAG<>-1  AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");
                //skhare7:MITS 22119
             //   sSQL = "SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId from CODES,CODES_TEXT WHERE   CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =" + p_iReservetypeCode + "  " +
             //  " AND  CODES.DELETED_FLAG<>-1";               
                sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "TRANS_TYPES", this.LanguageCode);
                objReader = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("Types");
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("TransId")));
                 
                    objElemTemp.InnerText = objReader.GetString("TransDesc");
                    objDOM.FirstChild.AppendChild(objElemTemp);
             
                }
         
                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemTemp = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                sbSQL = null;

            }
        }
        public XmlDocument GetReserveTypeCustomized(XmlDocument p_objXmlDocument, string p_sClaimCode, bool bReserveByClaim, string p_sLobCode)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemTemp = null;
            XmlElement objElemTemp1 = null;
            XmlElement objElemTemp2 = null;
          //  string sSQL = "";
            DbReader objReader = null;
            DbReader objReader1 = null;
            DbReader objReader2 = null;
            string sReserveTypeCode = null;
        
            sCurrenDate = DateTime.Now.Year.ToString();

            //for the date string
            //skhare7:MITS 22119
            if (iMonthNow < 10)
                sCurrenDate = sCurrenDate + "0" + iMonthNow.ToString();
            else
                sCurrenDate = sCurrenDate + iMonthNow.ToString();

            if (iDay < 10)
                sCurrenDate = sCurrenDate + "0" + iDay.ToString();
            else
                sCurrenDate = sCurrenDate + iDay.ToString();
            //skhare7:MITS 22119
            try
            {
               XmlNode objNode=p_objXmlDocument.SelectSingleNode("//Reserves");

               if (objNode != null)
               {
                  
                       foreach (XmlElement objElement in objNode)
                       {
                           if (objElement.Attributes["value"].Value != null)
                           {

                                sReserveTypeCode = sReserveTypeCode + "," + objElement.Attributes["value"].Value;

                           }
                      
                   }
                   if(sReserveTypeCode!=null)
                       sReserveTypeCode = sReserveTypeCode.Remove(0, 1);
               }
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("ReservesNew");
                objDOM.AppendChild(objElemParent);

                if (bReserveByClaim)
                {

                    if (sReserveTypeCode != null)
                    {
                        sSQL = new StringBuilder();
                        sSQL.Append("SELECT RSW_RES_CUSTOMIZATION.RSW_CUTOM_ID,RSW_RES_CUSTOMIZATION.CLAIM_TYPE_CODE,RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM RSW_RES_CUSTOMIZATION, CODES_TEXT, CODES ");
                        sSQL.Append(" WHERE RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND ");
                        sSQL.Append(" CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND RSW_RES_CUSTOMIZATION.LOB_CODE = " + p_sLobCode + "");
                        sSQL.Append(" AND  RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE IN ( " + sReserveTypeCode + ") AND RSW_RES_CUSTOMIZATION.CLAIM_TYPE_CODE= " + p_sClaimCode + "  ORDER BY CODES_TEXT.CODE_DESC");

                        sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "RESERVE_TYPE", this.LanguageCode); //Aman MITS 31626
                        objReader1 = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                        //if (objReader1.Read())
                        //{
                        sSQL = null;
                        while (objReader1.Read())
                        {
                            objElemTemp1 = objDOM.CreateElement("ReserveCustomized");
                            //Aman MITS 31626
                            //objElemTemp1.SetAttribute("value", Conversion.ConvertObjToStr(objReader1.GetValue("RESERVE_TYPE_CODE")));
                            objElemTemp1.SetAttribute("value", Conversion.ConvertObjToStr(objReader1.GetValue(2)));
                            //objElemTemp1.SetAttribute("Name", Conversion.ConvertObjToStr(objReader1.GetValue("CODE_DESC")));
                            objElemTemp1.SetAttribute("Name", Conversion.ConvertObjToStr(objReader1.GetValue(4)));
                            
                            objDOM.FirstChild.AppendChild(objElemTemp1);

                            //strCodeId.Append(strCodeId);
                            //strCodeId.Append(" ");
                            //strCodeId.Append(Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")));
                           // strCodeId = strCodeId + " " + Conversion.ConvertObjToStr(objReader1.GetValue("RESERVE_TYPE_CODE"));
                            sSQL = new StringBuilder();
                            //skhare7:MITS 22119
                            //Aman MITS 31626  made the query ML Specific 
                            sSQL.Append("SELECT distinct CODE_DESC TransDesc,RSW_CUSTOM_TRANSTYPE.TRANS_TYPE_CODE TransId ,RSW_RES_CUSTOMIZATION.LOB_CODE LOBCODE from CODES,CODES_TEXT ,RSW_RES_CUSTOMIZATION,RSW_CUSTOM_TRANSTYPE ");
                            sSQL.Append(" WHERE CODES_TEXT.CODE_ID = CODES.CODE_ID AND  RSW_CUSTOM_TRANSTYPE.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND CODES.RELATED_CODE_ID =RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE AND");
                            sSQL.Append(" RSW_RES_CUSTOMIZATION.RSW_CUTOM_ID=RSW_CUSTOM_TRANSTYPE.RSW_CUTOM_ID AND RSW_RES_CUSTOMIZATION.RSW_CUTOM_ID =" + Conversion.ConvertObjToInt(objReader1.GetValue("RSW_CUTOM_ID"), m_iClientId) + " AND CODES.DELETED_FLAG<>-1");
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");
                            //skhare7:MITS 22119

                            sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "TRANS_TYPES", this.LanguageCode); //Aman MITS 31626
                            objReader = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                         
                            sSQL = null;
                          
                            while (objReader.Read())
                            {

                                objElemTemp = objDOM.CreateElement("TransCustomTypes");
                                objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("TransId")));

                                objElemTemp.InnerText = objReader.GetString("TransDesc");
                                objElemTemp1.AppendChild(objElemTemp);
                            }
                            //skhare7:MITS 22119
                            //Aman MITS 31626  made the query ML Specific 
                            sSQL = new StringBuilder();
                            sSQL.Append("SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId from CODES,CODES_TEXT WHERE   CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =" + Conversion.ConvertObjToInt(objReader1.GetValue("RESERVE_TYPE_CODE"), m_iClientId) + " AND ");
                            sSQL.Append("  CODES.CODE_ID NOT IN (SELECT TRANS_TYPE_CODE FROM  RSW_CUSTOM_TRANSTYPE WHERE RSW_CUTOM_ID=" + Conversion.ConvertObjToInt(objReader1.GetValue("RSW_CUTOM_ID"), m_iClientId) + " ) AND  CODES.DELETED_FLAG<>-1 AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");

                            sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "TRANS_TYPES", this.LanguageCode); //Aman MITS 31626
                            objReader2 = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                            sSQL = null;
                            //skhare7:MITS 22119
                            while (objReader2.Read())
                            {

                                objElemTemp2 = objDOM.CreateElement("TransactionTypes");
                                objElemTemp2.SetAttribute("value", Conversion.ConvertObjToStr(objReader2.GetValue("TransId")));

                                objElemTemp2.InnerText = objReader2.GetString("TransDesc");
                                objElemTemp1.AppendChild(objElemTemp2);

                            }
                        }

                    }

                }
                else
                {
                    if (sReserveTypeCode != null)
                    {
                        sSQL = new StringBuilder();
                        sSQL.Append("SELECT RSW_RES_CUSTOMIZATION.RSW_CUTOM_ID,RSW_RES_CUSTOMIZATION.CLAIM_TYPE_CODE,RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE,CODES_TEXT.SHORT_CODE, CODES_TEXT.CODE_DESC FROM RSW_RES_CUSTOMIZATION, CODES_TEXT, CODES ");
                        sSQL.Append(" WHERE RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND ");
                        sSQL.Append(" CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND RSW_RES_CUSTOMIZATION.LOB_CODE = " + p_sLobCode + "");
                        sSQL.Append(" AND  RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE IN ( " + sReserveTypeCode + ") AND RSW_RES_CUSTOMIZATION.CLAIM_TYPE_CODE=0  ORDER BY CODES_TEXT.CODE_DESC");

                        sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "RESERVE_TYPE", this.LanguageCode); //Aman MITS 31626
                        objReader1 = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                        sSQL = null;
                        //if (objReader1.Read())
                        //{
                        //Aman MITS 31626 used the index numbers instead of column names
                        while (objReader1.Read())
                        {
                            objElemTemp1 = objDOM.CreateElement("ReserveCustomized");

                            //objElemTemp1.SetAttribute("value", Conversion.ConvertObjToStr(objReader1.GetValue("RESERVE_TYPE_CODE")));
                            objElemTemp1.SetAttribute("value", Conversion.ConvertObjToStr(objReader1.GetValue(2)));
                           // objElemTemp1.SetAttribute("Name", Conversion.ConvertObjToStr(objReader1.GetValue("CODE_DESC")));
                            objElemTemp1.SetAttribute("Name", Conversion.ConvertObjToStr(objReader1.GetValue(4)));
                           
                            objDOM.FirstChild.AppendChild(objElemTemp1);

                            //strCodeId.Append(strCodeId);
                            //strCodeId.Append(" ");
                            //strCodeId.Append(Conversion.ConvertObjToStr(objReader.GetValue("RESERVE_TYPE_CODE")));
                            ////strCodeId = strCodeId + " " + Conversion.ConvertObjToStr(objReader1.GetValue("RESERVE_TYPE_CODE"));
                           
                            //skhare7:MITS 22119
                            sSQL = new StringBuilder();
                            //Aman MITS 31626  made the query ML Specific 
                            sSQL.Append("SELECT distinct CODE_DESC TransDesc,RSW_CUSTOM_TRANSTYPE.TRANS_TYPE_CODE TransId ,RSW_RES_CUSTOMIZATION.LOB_CODE LOBCODE from CODES,CODES_TEXT ,RSW_RES_CUSTOMIZATION,RSW_CUSTOM_TRANSTYPE ");
                            sSQL.Append(" WHERE   RSW_CUSTOM_TRANSTYPE.TRANS_TYPE_CODE = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =RSW_RES_CUSTOMIZATION.RESERVE_TYPE_CODE AND CODES_TEXT.LANGUAGE_CODE = 1033 AND ");
                            sSQL.Append(" RSW_RES_CUSTOMIZATION.RSW_CUTOM_ID=RSW_CUSTOM_TRANSTYPE.RSW_CUTOM_ID   AND CODES_TEXT.CODE_ID=CODES.code_id  AND RSW_RES_CUSTOMIZATION.RSW_CUTOM_ID =" + Conversion.ConvertObjToInt(objReader1.GetValue("RSW_CUTOM_ID"), m_iClientId) + " AND CODES.DELETED_FLAG<>-1");
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");
                            //skhare7:MITS 22119
                            sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "TRANS_TYPES", this.LanguageCode); //Aman MITS 31626
                            objReader = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                         
                            sSQL = null;
                            while (objReader.Read())
                            {

                                objElemTemp = objDOM.CreateElement("TransCustomTypes");
                                objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("TransId")));

                                objElemTemp.InnerText = objReader.GetString("TransDesc");
                                objElemTemp1.AppendChild(objElemTemp);
                            }
                            //skhare7:MITS 22119
                            sSQL = new StringBuilder();
                            sSQL.Append("SELECT CODE_DESC TransDesc,CODES.CODE_ID TransId from CODES,CODES_TEXT WHERE   CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES.RELATED_CODE_ID =" + Conversion.ConvertObjToInt(objReader1.GetValue("RESERVE_TYPE_CODE"), m_iClientId) + " AND ");
                            sSQL.Append(" CODES.CODE_ID NOT IN (SELECT TRANS_TYPE_CODE FROM  RSW_CUSTOM_TRANSTYPE WHERE RSW_CUTOM_ID=" + Conversion.ConvertObjToInt(objReader1.GetValue("RSW_CUTOM_ID"), m_iClientId) + " ) AND  CODES.DELETED_FLAG<>-1 AND CODES_TEXT.LANGUAGE_CODE = 1033 ");
                            sSQL.Append(" AND ((CODES.TRIGGER_DATE_FIELD IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE IS NULL) ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE IS NULL AND CODES.EFF_END_DATE>='" + sCurrenDate + "') ");
                            sSQL.Append(" OR (CODES.TRIGGER_DATE_FIELD='SYSTEM_DATE' AND CODES.EFF_START_DATE<='" + sCurrenDate + "' AND CODES.EFF_END_DATE>='" + sCurrenDate + "')) ");

                            sSQL = CommonFunctions.PrepareMultilingualQuery(sSQL.ToString(), "TRANS_TYPES", this.LanguageCode); //Aman MITS 31626
                            objReader2 = DbFactory.GetDbReader(m_sDSN, sSQL.ToString());
                            //skhare7:MITS 22119
                            sSQL = null;
                            while (objReader2.Read())
                            {

                                objElemTemp2 = objDOM.CreateElement("TransactionTypes");
                                objElemTemp2.SetAttribute("value", Conversion.ConvertObjToStr(objReader2.GetValue("TransId")));

                                objElemTemp2.InnerText = objReader2.GetString("TransDesc");
                                objElemTemp1.AppendChild(objElemTemp2);

                            }
                         }

                    }
            
                }

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
          
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemTemp = null;
                objElemTemp1 = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objReader1 != null)
                {
                    objReader1.Dispose();
                }
                if (objReader2 != null)
                {
                    objReader2.Dispose();
                }
                sSQL = null;

            }
        }
     
        public bool Save(XmlDocument p_objXmlDocument)
        {
           
            XmlNode objNode = null;
            XmlNode objNode1 = null;
       
            bool bReturn = false;
            StringBuilder sbSQL;
            LocalCache objCache = null;
            string sLOBCode = string.Empty;
            string sClaimCode = string.Empty;
            string sSysSetCustom = string.Empty;
            string sDesc = string.Empty;
            DbConnection objCon = null;
            DbCommand objCmd = null;
        
            int  iRSWCustomId=0;
            int iRSWTransId = 0;
            DbReader objReader = null;

            try
            {
                sbSQL = new StringBuilder();
                objCon = DbFactory.GetDbConnection(m_sDSN);
                XmlNodeList nodes = p_objXmlDocument.SelectNodes("//ReserveCustomized");
                objNode = p_objXmlDocument.SelectSingleNode(".//LineOfBusiness");
                if (objNode != null)
                {
                    foreach (XmlElement objElement in objNode)
                    {
                        if (objElement.Attributes["selected"] != null)
                        {
                            if (objElement.Attributes["selected"].Value == "1")
                            {
                                sLOBCode = objElement.Attributes["value"].Value;
                            
                            }
                        }
                           
                    }
                }
                objNode = p_objXmlDocument.SelectSingleNode(".//ClaimTypes");
                objNode1 = p_objXmlDocument.SelectSingleNode("//ResClaimType");

                if(objNode1!=null)
                {
                    if (objNode1.InnerText != null)
                    {
                        if (objNode1.InnerText == "-1")
                        {
                            if (objNode != null)
                            {
                                foreach (XmlElement objElement in objNode)
                                {
                                    if (objElement.Attributes["selected"] != null)
                                    {
                                        if (objElement.Attributes["selected"].Value == "1")
                                        {
                                            sClaimCode = objElement.Attributes["value"].Value;

                                        }
                                    }

                                }
                            }
                        }
                        else if (objNode1.InnerText == "0")
                        {

                            sClaimCode = "0";
                        }
                           
                    }
                }
                if(sClaimCode=="")
                    sClaimCode = "0";
                objNode = p_objXmlDocument.SelectSingleNode("//SysSetting");
                if(objNode!=null)

                { 
                      if(objNode.InnerText!=null)
                      { 
                          
                          sSysSetCustom =objNode.InnerText;
                      }
                }
                if(sSysSetCustom=="")
                    objNode = p_objXmlDocument.SelectSingleNode("//SysSetCustom");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {

                        sSysSetCustom = objNode.InnerText;
                    }
                }

                if (sClaimCode == "0")
                {

                    sbSQL.Append("DELETE FROM RSW_RES_CUSTOMIZATION WHERE  RESERVE_CLAIM_TYPE=-1 ");
                    sbSQL.Append("AND LOB_CODE=" + sLOBCode + " ");

                    objCon.Open();
                    objCmd = objCon.CreateCommand();

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();

                    sbSQL.Append("DELETE FROM RSW_CUSTOM_TRANSTYPE WHERE RSW_CUTOM_ID IN (SELECT RSW_CUTOM_ID FROM RSW_RES_CUSTOMIZATION WHERE  RESERVE_CLAIM_TYPE=-1) ");
                 

                  //  objCon.Open();
                    objCmd = objCon.CreateCommand();

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();


                    objCon.Close();

                }

                sbSQL.Append("SELECT RSW_CUTOM_ID, RESERVE_TYPE_CODE,LOB_CODE FROM RSW_RES_CUSTOMIZATION WHERE ");
                sbSQL.Append(" LOB_CODE=" + sLOBCode + "  AND CLAIM_TYPE_CODE=" + sClaimCode + "");

                objReader = DbFactory.GetDbReader(m_sDSN, sbSQL.ToString());
                sbSQL.Remove(0, sbSQL.Length);
                objCon.Open();
              


                while (objReader.Read())
                {
                    // objReader.Close();
                    sbSQL.Append("DELETE FROM RSW_RES_CUSTOMIZATION WHERE  CLAIM_TYPE_CODE=" + sClaimCode + " ");
                    sbSQL.Append("AND LOB_CODE=" + sLOBCode + " ");
                  
                 
                    objCmd = objCon.CreateCommand();

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();

                    sbSQL.Append(" DELETE FROM RSW_CUSTOM_TRANSTYPE WHERE RSW_CUTOM_ID="+Conversion.ConvertObjToInt(objReader.GetValue("RSW_CUTOM_ID"), m_iClientId)+"");

                    objCmd = objCon.CreateCommand();

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();
                  //  iRSWCustomId = Conversion.ConvertObjToInt(objReader.GetValue("RSW_CUTOM_ID"), m_iClientId);
                }

                if (nodes.Count > 0)
                {
                    foreach (XmlNode node in nodes)
                    {

                        iRSWCustomId = Utilities.GetNextUID(m_sDSN, "RSW_RES_CUSTOMIZATION", m_iClientId);
                        sbSQL.Append(" INSERT INTO RSW_RES_CUSTOMIZATION (RSW_CUTOM_ID,SYS_CLAIM_SET, RESERVE_CLAIM_TYPE,CLAIM_TYPE_CODE,LOB_CODE,RESERVE_TYPE_CODE,LAST_UPDATED_ON )");
                        sbSQL.Append(" VALUES(" + iRSWCustomId + "," + sSysSetCustom + "," + objNode1.InnerText + "," + sClaimCode + "," + sLOBCode + "," + node.Attributes["value"].Value + "," + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + ")");
                        // objCon.Open();
                        objCmd = objCon.CreateCommand();

                        objCmd.CommandText = sbSQL.ToString();
                        sbSQL.Remove(0, sbSQL.Length);
                        objCmd.ExecuteNonQuery();

                        foreach (XmlElement XMLele in node)
                        {
                            if (XMLele.LocalName == "TransCustomTypes")
                            {
                                iRSWTransId = Utilities.GetNextUID(m_sDSN, "RSW_CUSTOM_TRANSTYPE", m_iClientId);
                                sbSQL.Append(" INSERT INTO RSW_CUSTOM_TRANSTYPE (RSW_CUTOM_ID, RES_TRANS_ID,TRANS_TYPE_CODE,LAST_UPDATED_ON) ");
                                sbSQL.Append(" VALUES(" + iRSWCustomId + "," + iRSWTransId + "," + Conversion.ConvertObjToInt(XMLele.Attributes["value"].Value.ToString(), m_iClientId) + "," + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + ")");

                                objCmd = objCon.CreateCommand();

                                objCmd.CommandText = sbSQL.ToString();
                                sbSQL.Remove(0, sbSQL.Length);
                                objCmd.ExecuteNonQuery();
                            }

                        }

                    }
                }
                else
                {

                    iRSWCustomId = Utilities.GetNextUID(m_sDSN, "RSW_RES_CUSTOMIZATION", m_iClientId);
                    sbSQL.Append(" INSERT INTO RSW_RES_CUSTOMIZATION (RSW_CUTOM_ID,SYS_CLAIM_SET, RESERVE_CLAIM_TYPE,CLAIM_TYPE_CODE,LOB_CODE,RESERVE_TYPE_CODE,LAST_UPDATED_ON )");
                    sbSQL.Append(" VALUES(" + iRSWCustomId + "," + sSysSetCustom + "," + objNode1.InnerText + "," + sClaimCode + "," + sLOBCode + ","+ 0 +"," + Riskmaster.Common.Conversion.GetDateTime(DateTime.Now.ToString()) + ")");
                    // objCon.Open();
                    objCmd = objCon.CreateCommand();

                    objCmd.CommandText = sbSQL.ToString();
                    sbSQL.Remove(0, sbSQL.Length);
                    objCmd.ExecuteNonQuery();
                
                }
                objCon.Close();
                bReturn = true;

            }
            catch (Exception ex)
            {
                throw ex;
               
            }
            finally
               {
                if (objCache != null)
                {
                    objCache.Dispose();
                    objCache = null;
                }
                if (objCon != null)
                {
                    objCon.Close();
                    objCon = null;
                }
                objCmd = null;
                sbSQL = null;
            }
            return bReturn;
            
        }
        public XmlDocument GetLOB(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTempTypes = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            DbReader objReaderClaim = null;
          //  string sClaimChecked = "";
            DbReader objReaderLOB = null;
            StringBuilder objSql = null;
            XmlNode objNode = null;
            LocalCache objCache = null;
            int iTableId = 0;
            int iClaimId = 0;
            string sLOB = "";
            string sCLaimTypeCode = "";
            string sResByClaimTypeValue = "";
            string sResByClaimType = "";
            DataSet objDs = null;
            XmlDocument objDocTemp = null;
            XmlDocument objDocTrans= null;
            string sTemp = "";
            string sLOBSelect = "";
            string sLobCode = "";
       //     StringBuilder strTemp = null;
            string sSysSetCustom = "";
            int iCount = 0;
            XmlElement objElem = null;
            try
            {
                objNode = p_objXmlDocument.SelectSingleNode(".//LineOfBusiness");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (objNode.InnerText != "")
                            sLobCode = objNode.InnerText;
                    }
                }

                objNode = p_objXmlDocument.SelectSingleNode(".//SysSetCustom");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (objNode.InnerText != "")
                            sSysSetCustom = objNode.InnerText;
                    }
                }
                objNode = p_objXmlDocument.SelectSingleNode("//LOBSelect");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (objNode.InnerText != "")
                            sLOBSelect = objNode.InnerText;
                    }
                }
                objNode = p_objXmlDocument.SelectSingleNode("//ClaimId");
                if (objNode != null)
                {
                    if (objNode.InnerText != null)
                    {
                        if (objNode.InnerText != "")
                            iClaimId =Convert.ToInt32(objNode.InnerText);
                    }
                }

                objSql = new StringBuilder();


                if (iClaimId != 0)
                {
                    objSql.Append(" SELECT LINE_OF_BUS_CODE,CLAIM_TYPE_CODE FROM CLAIM ");
                    objSql.Append(" WHERE CLAIM_ID=" + iClaimId + " ");

                    //  objDs = DbFactory.GetDataSet(m_sDSN, objSql.ToString());

                    objReaderLOB = DbFactory.GetDbReader(m_sDSN, objSql.ToString());

                    if (objReaderLOB.Read())
                    {
                        sLOB = Conversion.ConvertObjToStr(objReaderLOB.GetValue("LINE_OF_BUS_CODE"));
                        sCLaimTypeCode = Conversion.ConvertObjToStr(objReaderLOB.GetValue("CLAIM_TYPE_CODE"));
                    }
                }
                objDOM = new XmlDocument();
                objElem = objDOM.CreateElement("LOB");
                objDOM.AppendChild(objElem);

                objElemParent = objDOM.CreateElement("LOBParms");
                //objDOM.AppendChild(objElemParent);
                objDOM.FirstChild.AppendChild(objElemParent);

                objElemChild = objDOM.CreateElement("Options");


                //objDOM.FirstChild.AppendChild(objElemChild);

                objDOM.FirstChild.FirstChild.AppendChild(objElemChild);
                objCache = new LocalCache(m_sDSN, m_iClientId);
                iTableId = objCache.GetTableId("LINE_OF_BUSINESS");
                objCache.Dispose();
                objDs = UTILITY.GetCodeList(iTableId.ToString(), m_sDSN, m_iClientId);
                objElemTemp = objDOM.CreateElement("LineOfBusiness");
                if (objDs.Tables[0] != null)
                {
                    foreach (DataRow dr in objDs.Tables[0].Rows)
                    {
                        objElemTempTypes = objDOM.CreateElement("Types");
                        objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                        objElemTempTypes.InnerText =  Conversion.ConvertObjToStr(dr["CODE_DESC"]);
                        objElemTemp.AppendChild(objElemTempTypes);
                        if (sLOB != "")
                        {

                            if (sLobCode.Trim().Equals(""))
                                sLobCode =sLOB;

                            if (sLobCode.Trim().Equals(Conversion.ConvertObjToStr(dr["CODE_ID"])))
                            {
                                objElemTempTypes.SetAttribute("selected", "1");
                            }
                        }
                        else
                        {

                            if (sLobCode.Trim().Equals(""))
                                sLobCode = Conversion.ConvertObjToStr(dr["CODE_ID"]);

                            if (sLobCode.Trim().Equals(Conversion.ConvertObjToStr(dr["CODE_ID"])))
                            {
                                objElemTempTypes.SetAttribute("selected", "1");
                            }
                        }
                        iCount++;
                    }

                }
                //objDOM.FirstChild.AppendChild(objElemTemp);//asif
                objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
               

                if (!sLobCode.Equals(""))
                {
                   
                    objSql = new StringBuilder();

                    objSql.Append(" SELECT  * FROM RSW_RES_CUSTOMIZATION ");
                    objSql.Append(" WHERE RSW_CUTOM_ID =(Select MAX(RSW_CUTOM_ID) from RSW_RES_CUSTOMIZATION WHERE LOB_CODE=" + sLobCode + " )ORDER BY LAST_UPDATED_ON DESC");
                 //   objSql.Append(" SELECT  * FROM RSW_RES_CUSTOMIZATION ");
                  //  objSql.Append(" WHERE  ROWNUM < 2 and LOB_CODE=" + sLobCode + " ORDER BY LAST_UPDATED_ON DESC");
                   
                   
                  //  objDs = DbFactory.GetDataSet(m_sDSN, objSql.ToString());

                    objReader = DbFactory.GetDbReader(m_sDSN, objSql.ToString());
                    objSql = new StringBuilder();
                    objSql.Append(string.Format(" SELECT RES_BY_CLM_TYPE FROM {0} WHERE LINE_OF_BUS_CODE = {1}", "SYS_PARMS_LOB", sLobCode));

                    objReaderClaim = DbFactory.GetDbReader(m_sDSN, objSql.ToString());
                       

                    if (objReader.Read())
                    {
                        objElemParent = objDOM.CreateElement("ReserveOptions");
                        //objDOM.FirstChild.AppendChild(objElemParent);//asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                       
                        string sRSWTemp = string.Empty;


                        objElemTemp = objDOM.CreateElement("RSWCLaimTypes");
                        //string strTempAutoAdjustTypes = string.Empty;

                        objDs = DbFactory.GetDataSet(m_sDSN, "SELECT CLAIM_TYPE_CODE FROM SYS_LOB_RSW_CLAIM_TYPE WHERE LINE_OF_BUS_CODE=" + sLobCode, m_iClientId);
                        if (objDs.Tables[0] != null)
                        {
                            foreach (DataRow dr in objDs.Tables[0].Rows)
                            {
                                objElemTempTypes = objDOM.CreateElement("Types");
                                objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]));
                                sRSWTemp = sRSWTemp + " " + Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]);
                                objElemTempTypes.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]), m_sDSN, m_iClientId);
                                objElemTemp.AppendChild(objElemTempTypes);
                                //strTempAutoAdjustTypes = strTempAutoAdjustTypes + " " + Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]);//Asif
                            }
                        }

                        objElemTemp.SetAttribute("codeid", sRSWTemp);//123
                        objElemParent.AppendChild(objElemTemp);

                        objElemTemp = objDOM.CreateElement("SysSetCustom");
                        if (sSysSetCustom != "")
                            objElemTemp.InnerText = sSysSetCustom;
                        else
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("SYS_CLAIM_SET"));
                       
                        objElemParent.AppendChild(objElemTemp);


                        objElemTemp = objDOM.CreateElement("ResClaimType");
                        if (objReaderClaim.Read())
                        {
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReaderClaim.GetValue("RES_BY_CLM_TYPE"));
                            sResByClaimType = objElemTemp.InnerText;
                        }
                        // objElemTemp.InnerText = sClaimChecked;
                       // sResByClaimType = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_TYPE_CODE"));
                        sResByClaimTypeValue = Conversion.ConvertObjToStr(objReader.GetValue("CLAIM_TYPE_CODE"));

                        objElemParent.AppendChild(objElemTemp);

                        objCache = new LocalCache(m_sDSN, m_iClientId);
                        iTableId = objCache.GetTableId("CLAIM_TYPE");
                        objCache.Dispose();
                        objSql = new StringBuilder();
                        objSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT");
                        objSql.Append(" WHERE CODES.TABLE_ID =" + iTableId.ToString());
                        objSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND ");
                        objSql.Append(" (CODES.LINE_OF_BUS_CODE = " + sLobCode);
                        objSql.Append(" OR CODES.LINE_OF_BUS_CODE = 0 ) AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0");
                        //objSql.Append(" OR CODES.LINE_OF_BUS_CODE = 0 OR CODES.LINE_OF_BUS_CODE IS NULL) AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0");
                        objSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC");

                        objSql = CommonFunctions.PrepareMultilingualQuery(objSql.ToString(), "CLAIM_TYPE", this.LanguageCode); //Aman MITS 31626
                        objDs = DbFactory.GetDataSet(m_sDSN, objSql.ToString(), m_iClientId);

                        //Aman MITS 31626 used the indexes instead of Column Names
                        objNode = p_objXmlDocument.SelectSingleNode(".//ClaimTypes");
                        if (objNode != null)
                        {
                            if (objNode.InnerText != null)
                            {
                                if (objNode.InnerText != "")
                                    sTemp = objNode.InnerText;
                            }
                           
                        }

                        objElemTemp = objDOM.CreateElement("ClaimTypes");
                        if (objDs.Tables[0] != null)
                        {
                            for (int i = 0; i < objDs.Tables[0].Rows.Count; i++)
                            {
                                DataRow dr = objDs.Tables[0].Rows[i];
                             //if (sLOBSelect != "SelectLOB" )
                             // {
                                if ((i == 0) && (sTemp == ""))
                                {
                                    if (sCLaimTypeCode == "")

                                    { 
                                        //sTemp = Conversion.ConvertObjToStr(dr["CODE_ID"]); 
                                        sTemp = Conversion.ConvertObjToStr(dr[0]); 
                                    }
                                    else
                                    {
                                        sTemp = sCLaimTypeCode;
                                    }
                                }
                                
                    
                                objElemTempTypes = objDOM.CreateElement("Types");
                                //objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr[0]));
                                //TR2647 Raman Bhatia - Claim Type passed by UI is not getting considered
                                if (sTemp.Trim().Equals(Conversion.ConvertObjToStr(dr[0])))
                                {
                                    objElemTempTypes.SetAttribute("selected", "1");
                                }
                                //objElemTempTypes.InnerText = Conversion.ConvertObjToStr(dr["CODE_DESC"]);
                                objElemTempTypes.InnerText = Conversion.ConvertObjToStr(dr[2]);
                                objElemTemp.AppendChild(objElemTempTypes);
                            }

                        }//end if

                        objElemParent.AppendChild(objElemTemp);
                        objElemTemp = objDOM.CreateElement("ReserveTypes");

                     
                        if (sResByClaimType.Trim().Equals("-1"))
                        {

                            objDocTemp = GetReserveType(sLobCode, sTemp, true);

                        }
                        else
                        {

                            objDocTemp = GetReserveType(sLobCode, sTemp, false);

                        }
                        objNode = objDOM.ImportNode(objDocTemp.DocumentElement, true);
                       
                        //Asif Start

                        //   objElemTemp.SetAttribute("codeid", strTemp);

                        //Asif End
                        objElemTemp.AppendChild(objNode);
                        objElemParent.AppendChild(objElemTemp);
                        //objDOM.FirstChild.FirstChild.AppendChild(objElemParent);asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

                        if (sResByClaimType.Trim().Equals("-1"))
                        {
                            objDocTrans = GetReserveTypeCustomized(objDOM, sTemp, true, sLobCode);
                        }
                        else
                        {
                            objDocTrans = GetReserveTypeCustomized(objDOM, sTemp, false, sLobCode);
                        }

                        objNode = objDOM.ImportNode(objDocTrans.DocumentElement, true);

                        objElemTemp.AppendChild(objNode);
                        objElemParent.AppendChild(objElemTemp);
                        //objDOM.FirstChild.FirstChild.AppendChild(objElemParent);asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                        sResByClaimType = "";
                       // sClaimChecked = "";

                    }
                    else
                    {

                        objElemParent = objDOM.CreateElement("ReserveOptions");
                        //objDOM.FirstChild.AppendChild(objElemParent);//asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                        string sRSWTemp = string.Empty;


                        objElemTemp = objDOM.CreateElement("RSWCLaimTypes");
                        //string strTempAutoAdjustTypes = string.Empty;

                        objDs = DbFactory.GetDataSet(m_sDSN, "SELECT CLAIM_TYPE_CODE FROM SYS_LOB_RSW_CLAIM_TYPE WHERE LINE_OF_BUS_CODE=" + sLobCode, m_iClientId);
                        if (objDs.Tables[0] != null)
                        {
                            foreach (DataRow dr in objDs.Tables[0].Rows)
                            {
                                objElemTempTypes = objDOM.CreateElement("Types");
                                objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]));
                                sRSWTemp = sRSWTemp + " " + Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]);
                                objElemTempTypes.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(dr["CLAIM_TYPE_CODE"]), m_sDSN, m_iClientId);
                                objElemTemp.AppendChild(objElemTempTypes);
                                //strTempAutoAdjustTypes = strTempAutoAdjustTypes + " " + Conversion.ConvertObjToStr(dr["RES_TYPE_CODE"]);//Asif
                            }
                        }

                        objElemTemp.SetAttribute("codeid", sRSWTemp);//123
                        objElemParent.AppendChild(objElemTemp);


                        objElemTemp = objDOM.CreateElement("RSWCLaimStatus");
                        sRSWTemp = string.Empty;

                        objDs = DbFactory.GetDataSet(m_sDSN, "SELECT CLAIM_STATUS_CODE FROM SYS_LOB_RSW_CLAIM_STATUS WHERE LINE_OF_BUS_CODE=" + sLobCode, m_iClientId);
                        if (objDs.Tables[0] != null)
                        {
                            foreach (DataRow dr in objDs.Tables[0].Rows)
                            {
                                objElemTempTypes = objDOM.CreateElement("Types");
                                objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CLAIM_STATUS_CODE"]));
                                sRSWTemp = sRSWTemp + " " + Conversion.ConvertObjToStr(dr["CLAIM_STATUS_CODE"]);
                                objElemTempTypes.InnerText = UTILITY.GetCode(Conversion.ConvertObjToStr(dr["CLAIM_STATUS_CODE"]), m_sDSN, m_iClientId);
                                objElemTemp.AppendChild(objElemTempTypes);
                            }
                        }

                        objElemTemp.SetAttribute("codeid", sRSWTemp);//123
                        objElemParent.AppendChild(objElemTemp);

                     
                        objElemTemp = objDOM.CreateElement("SysSetCustom");
                        if (sSysSetCustom != "")
                            objElemTemp.InnerText = sSysSetCustom;
                        else
                            objElemTemp.InnerText = "0";

                        objElemParent.AppendChild(objElemTemp);


                        objElemTemp = objDOM.CreateElement("ResClaimType");
                        if (objReaderClaim.Read())
                        {
                            objElemTemp.InnerText = Conversion.ConvertObjToStr(objReaderClaim.GetValue("RES_BY_CLM_TYPE"));
                            sResByClaimType = objElemTemp.InnerText;
                        }
                     
                        objElemParent.AppendChild(objElemTemp);

                        objCache = new LocalCache(m_sDSN, m_iClientId);
                        iTableId = objCache.GetTableId("CLAIM_TYPE");
                        objCache.Dispose();
                        objSql = new StringBuilder();
                        objSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC,CODES.LINE_OF_BUS_CODE FROM CODES,CODES_TEXT");
                        objSql.Append(" WHERE CODES.TABLE_ID =" + iTableId.ToString());
                        objSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID AND CODES_TEXT.LANGUAGE_CODE = 1033 AND ");
                        objSql.Append(" (CODES.LINE_OF_BUS_CODE = " + sLobCode);
                        objSql.Append(" OR CODES.LINE_OF_BUS_CODE = 0 ) AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0");
                        objSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC");
                        objDs = DbFactory.GetDataSet(m_sDSN, objSql.ToString(), m_iClientId);

                        objSql = CommonFunctions.PrepareMultilingualQuery(objSql.ToString(), "CLAIM_TYPE", this.LanguageCode); //Aman MITS 31626
                        objNode = p_objXmlDocument.SelectSingleNode(".//ClaimTypes");
                        if (objNode != null)
                        {
                            if (objNode.InnerText != null)
                            {
                                if (objNode.InnerText != "")
                                    sTemp = objNode.InnerText;
                            }
                        }
                        //Aman MITS 31626 used the indexes instead of Column Names
                        objElemTemp = objDOM.CreateElement("ClaimTypes");
                        if (objDs.Tables[0] != null)
                        {
                            for (int i = 0; i < objDs.Tables[0].Rows.Count; i++)
                            {
                                DataRow dr = objDs.Tables[0].Rows[i];

                                if ((i == 0) && (sTemp == ""))
                                {
                                    if (sCLaimTypeCode == "")
                                    {
                                        //sTemp = Conversion.ConvertObjToStr(dr["CODE_ID"]);
                                        sTemp = Conversion.ConvertObjToStr(dr[0]);
                                    }
                                    else
                                    {
                                        sTemp = sCLaimTypeCode;
                                    }
                                }
                                objElemTempTypes = objDOM.CreateElement("Types");
                                //objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr["CODE_ID"]));
                                objElemTempTypes.SetAttribute("value", Conversion.ConvertObjToStr(dr[0]));
                               
                                if (sTemp.Trim().Equals(Conversion.ConvertObjToStr(dr[0])))
                                {
                                    objElemTempTypes.SetAttribute("selected", "1");
                                }
                                //objElemTempTypes.InnerText =  Conversion.ConvertObjToStr(dr["CODE_DESC"]);
                                objElemTempTypes.InnerText = Conversion.ConvertObjToStr(dr[2]);
                                objElemTemp.AppendChild(objElemTempTypes);
                            }

                        }//end if

                        objElemParent.AppendChild(objElemTemp);
                        objElemTemp = objDOM.CreateElement("ReserveTypes");
                       // strTemp = new StringBuilder();
                        //string strTemp = string.Empty;//asif
                        if (sResByClaimType.Trim().Equals("-1"))
                        {

                            objDocTemp = GetReserveType(sLobCode, sTemp, true);

                        }
                        else
                        {

                            objDocTemp = GetReserveType(sLobCode, sTemp, false);
                        }

                        objNode = objDOM.ImportNode(objDocTemp.DocumentElement, true);
                     
                        objElemTemp.AppendChild(objNode);
                        objElemParent.AppendChild(objElemTemp);
                        //objDOM.FirstChild.FirstChild.AppendChild(objElemParent);asif
                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);

                        objDOM.FirstChild.FirstChild.FirstChild.AppendChild(objElemParent);
                    }
                }//new
                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
          
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTempTypes = null;
                objElemTemp = null;
                objSql = null;
                objNode = null;
                if (objCache != null)
                    objCache.Dispose();
              
                objDs = null;
                objDocTemp = null;
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objReaderClaim != null)
                {
                    objReaderClaim.Dispose();
                }
               
            }

        }
       
    }
   	
}
