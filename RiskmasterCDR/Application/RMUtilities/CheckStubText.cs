using System;
using System.Collections;
using System.Xml;
using Riskmaster.Common;
using System.Linq;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using System.Text;
using System.Xml.Linq;

namespace Riskmaster.Application.RMUtilities
{
    /// <summary>
    /// Date		: 04/07/2010
    /// Author	    : Michael Capps
    ///Purpose      : This class Creates, Edits, Fetches Claim Type information.
    /// </summary>
    public class CheckStubText : UtilitiesUIBase
    {
        #region Private variables
        /// <summary>
        /// Fields mapping with database fields
        /// </summary>
        private string[,] arrFields = {
			{"RowId", "CHKSTBTXT_ROW_ID"},
			{"StateId","STATE_ID"},
			{"TransTypeCode","TRANS_TYPE_CODE"},
			{"CheckStubText","CHECK_STUB_TEXT"}	
									  };

        //		private XmlDocument m_objXMLDocument = null;
        
        #endregion

        private int m_iClientId = 0;//Add by kuladeep for Jira-63
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string</param>
        public CheckStubText(string p_sConnString, int p_iClientId)//Add by kuladeep for Jira-63
            : base(p_sConnString, p_iClientId)
        {
            try
            {
                ConnectString = p_sConnString;
                m_iClientId = p_iClientId;//Add by kuladeep for Jira-63
                this.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckStubText.Constructor.Error", m_iClientId), p_objEx);//Add by kuladeep for Jira-63
            }
        }
        #endregion

        #region Private Functions
        new private void Initialize()
        {
            try
            {
                TableName = "CHK_STUB_TEXT_MAP";
                KeyField = "CHKSTBTXT_ROW_ID";
                base.InitFields(arrFields);
                base.Initialize();
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckStubText.Initialize.Error", m_iClientId), p_objEx);//Add by kuladeep for Jira-63
            }
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Gets the xml structure for the form
        /// </summary>
        /// <param name="p_objXMLDocument">Input xml document</param>
        /// <returns>xml structure</returns>
       /* public XmlDocument New(XmlDocument p_objXMLDocument)
        {
            try
            {
                return p_objXMLDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckStubText.New.Error"), p_objEx);
            }
        }*/
        /// Name		: Get
        /// Date		: 04/07/2010
        /// Author	    : Michael Capps		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>Xml containing the data to be populated on screen</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            try
            {
                XMLDoc = p_objXmlDocument;

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                if (objNode.InnerText != null && !objNode.InnerText.Equals(""))
                    base.Get();
                //				objNodLst = p_objXmlDocument.GetElementsByTagName("rowtext");
                //				foreach(XmlElement objElm in objNodLst)
                //				{
                //					objElm.SetAttribute("title", UTILITY.GetCode(objElm.GetAttribute("title"),base.ConnectString)); 
                //				}
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckStubText.Get.Err", m_iClientId), p_objEx);//Add by kuladeep for Jira-63
            }
            finally
            {
               			objNode = null;
            }
        }

        /// Name		: Save
        /// Date		: 04/07/2010
        /// Author	    : Michael Capps		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Saves the data to the database
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public XmlDocument Save(XmlDocument p_objXmlDocument)
        {
           // bool bReturnValue = false;
            XmlNodeList objXmlNodeList;
            DbConnection objConn = null;
            XmlDocument p_objXmlOut;
            int iNextRowId = 0;
            //Ankit Sharma:Changes for Pen Testing
            StringBuilder sbSQL = new StringBuilder();
            DbWriter writer = null;
            //Ankit Sharma:End changes
            try
            {
                //Start rsushilaggar Modified the code to add and edit check stub text
                objXmlNodeList = p_objXmlDocument.GetElementsByTagName("control");
                //XmlElement element = objXmlNodeList
                XmlNode objStateNode = p_objXmlDocument.SelectSingleNode("//control[@name='States']");
                string s_State = objStateNode.Attributes["codeid"].Value;
                string[] s_StateList = s_State.Split(' ');

                XmlNode objTransTypeNode = p_objXmlDocument.SelectSingleNode("//control[@name='TransTypeCode']");
                string s_TransTypeCode = objTransTypeNode.Attributes["codeid"].Value;
                string[] s_TransTypeCodeList = s_TransTypeCode.Split(' ');

                XmlNode objCheckStubNode = p_objXmlDocument.SelectSingleNode("//control[@name='CheckStubText']");
                string s_CheckStubText = objCheckStubNode.InnerText;

                XmlNode objRowIdNode = p_objXmlDocument.SelectSingleNode("//control[@name='RowId']");
                string sRowId = objRowIdNode.InnerText;

                foreach (string State in s_StateList)
                {
                    foreach (string TransType in s_TransTypeCodeList)
                    {
                        string s_sql = string.Empty;
                        if (String.IsNullOrEmpty(sRowId))
                        {
                            iNextRowId = Riskmaster.Common.Utilities.GetNextUID(this.ConnectString, "CHK_STUB_TEXT_MAP", m_iClientId);
                            //aanandpraka2:Changes for Pen testing-02/Jan/2012
                            //s_sql = "INSERT INTO CHK_STUB_TEXT_MAP (CHKSTBTXT_ROW_ID,STATE_ID,TRANS_TYPE_CODE,CHECK_STUB_TEXT) values(" + iNextRowId + "," + (!string.IsNullOrEmpty(State) ? State : "0") + "," + (!string.IsNullOrEmpty(TransType) ? TransType : "0") + ",'" + s_CheckStubText + "')";
                            
                            writer = DbFactory.GetDbWriter(this.ConnectString);
                            writer.Tables.Add("CHK_STUB_TEXT_MAP");
                            writer.Fields.Add("CHKSTBTXT_ROW_ID", iNextRowId);
                            writer.Fields.Add("STATE_ID", (!string.IsNullOrEmpty(State) ? Conversion.ConvertStrToInteger(State) : 0));
                            writer.Fields.Add("TRANS_TYPE_CODE", (!string.IsNullOrEmpty(TransType) ? Conversion.ConvertStrToInteger(TransType) : 0));
                            writer.Fields.Add("CHECK_STUB_TEXT", s_CheckStubText);
                            
                            writer.Execute();
                            //aanandpraka2:End Changes
                        }
                        else
                        {
                            //aanandpraka2:Changes for Pen testing-02/Jan/2012
                            //s_sql = "UPDATE CHK_STUB_TEXT_MAP SET STATE_ID =" + (!string.IsNullOrEmpty(State) ? State : "0") + ",TRANS_TYPE_CODE=" + (!string.IsNullOrEmpty(TransType) ? TransType : "0") + ",CHECK_STUB_TEXT='" + s_CheckStubText + "' Where CHKSTBTXT_ROW_ID=" + sRowId;
                            writer = DbFactory.GetDbWriter(this.ConnectString);

                            writer.Tables.Add("CHK_STUB_TEXT_MAP");
                            writer.Fields.Add("STATE_ID", (!string.IsNullOrEmpty(State) ? Conversion.ConvertStrToInteger(State) : 0));
                            writer.Fields.Add("TRANS_TYPE_CODE", (!string.IsNullOrEmpty(TransType) ? Conversion.ConvertStrToInteger(TransType) : 0));
                            writer.Fields.Add("CHECK_STUB_TEXT", s_CheckStubText);
                            writer.Where.Add(" CHKSTBTXT_ROW_ID="+ sRowId);

                            writer.Execute();
                        }
                        
                        //objConn = DbFactory.GetDbConnection(this.ConnectString);
                        //objConn.Open();
                        //objConn.ExecuteNonQuery(s_sql);
                        //objConn.Dispose(); 
                        //aanandpraka2:End Changes
                    }
                }
                //End rsushilaggar
                //base.Save();


                //bReturnValue = true;
                //return bReturnValue;
                //rsushilaggar: Get the latest check stub text from database after Adding new /Updating
                CheckStubTexts objCheckStubtext = null; //Application layer component	
                XmlDocument p_objXmlIn = GetXmlTemplate();
                objCheckStubtext = new CheckStubTexts(base.ConnectString, m_iClientId);
                p_objXmlOut = objCheckStubtext.Get(p_objXmlIn);
                return p_objXmlOut;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckStubText.Save.Error", m_iClientId), p_objEx);//Add by kuladeep for Jira-63
            }

        }
        /// Name		: Delete
        /// Date		: 04/07/2010
        /// Author	    : Michael Capps		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Deletes the Claim type inaanaation
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>True/False</returns>
        public XmlDocument Delete(XmlDocument p_objXmlDocument)
        {
            XMLDoc = p_objXmlDocument;
            XmlDocument p_objXmlOut;
            try
            {
                base.Delete();
                //rsushilaggar: Get the latest check stub text from database after Deletion
                CheckStubTexts objCheckStubtext = null; //Application layer component	
                XmlDocument p_objXmlIn = GetXmlTemplate();
                objCheckStubtext = new CheckStubTexts(base.ConnectString, m_iClientId);
                p_objXmlOut = objCheckStubtext.Get(p_objXmlIn);

                return p_objXmlOut;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckStubText.Delete.Error", m_iClientId), p_objEx);//Add by kuladeep for Jira-63
            }
        }
        #endregion
        /// <summary>
        /// Author Rahul Aggarwal
        /// Generate the default template to get Check stub Text
        /// </summary>
        /// <param name="selectedRowId"></param>
        /// <returns></returns>
        private XmlDocument GetXmlTemplate()
        {
            //XElement XmlTemplate = null;
            StringBuilder sXml = new StringBuilder("<CheckStubTextMappingList><listhead>");
            sXml = sXml.Append("<StateId>State Id</StateId><TransTypeCode type='code'>Transaction Type</TransTypeCode><CheckStubText>Check Stub Text</CheckStubText><RowId>RowId");
            sXml = sXml.Append("</RowId>");
            sXml = sXml.Append("</listhead></CheckStubTextMappingList>");
            //XElement oElement = XElement.Parse(sXml.ToString());
            XmlDocument document = new XmlDocument();
            document.LoadXml(sXml.ToString());
            return document;
        }

    }
}

