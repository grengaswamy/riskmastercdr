using System;
using Riskmaster.Db;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches Billing options information.
	/// </summary>
	public class BillingOptions:UtilityFormBase
	{
		#region Private Variables
		/// <summary>
		/// Fields mapping with Database fields
		/// </summary>
		private string[,] arrFields = {
			{"SBBR_InUse", "IN_USE_FLAG"},
			{"SBBR_BillingRuleCode","BILLING_RULE_CODE"},
			{"SBBR_BillingRuleName","BILLING_RULE_CODE"},
			{"SBBR_Notice1Days","FIRST_NOTICE_DAYS"},
			{"SBBR_Notice2Days","SECOND_NOTICE_DAYS"},
			{"SBBR_Notice3Days","THIRD_NOTICE_DAYS"},
			{"SBBR_Notice4Days","FOURTH_NOTICE_DAYS"},
			{"SEPD_InUse","IN_USE_FLAG"},
			{"SEPD_LOB","LINE_OF_BUSINESS"},
			{"SEPD_Amount","AMOUNT"},
			{"SEPD_DollarOrPercent","DISCOUNT_TYPE"},
			{"SEPD_EligibilityDate","DEADLINE_DATE"},
			{"SBPP_InUse","IN_USE_FLAG"},
			{"SBPP_PayPlanCode", "PAY_PLAN_CODE"},
			{"SBPP_BillingRuleCode","BILLING_RULE_ROWID"},
			{"SBPP_LOB", "LINE_OF_BUSINESS"},
			{"SBPP_State","STATE"},
			{"SBPP_InstallGenType", "INSTALL_GEN_TYPE"},
			{"SBPP_Frequency","FIXED_FREQUENCY"},
			{"SBPP_PayPlanId","PAY_PLAN_ROWID"},
			{"SBPP_ErlyPayDscntId","EARLY_DISCOUNT_ROWID"},
			{"SBPP_BlngRuleId","BILLING_RULE_ROWID"}
									  };
		/// <summary>
		/// Field mapping SQL queries
		/// </summary>
		//{"XMLTag", "Table Name", "Key Field", "Value Field", "Where Clause"}
		private string[,] arrChilds = {
			{"SysBillBlngRule" , "SELECT BILLING_RULE_ROWID,IN_USE_FLAG,BILLING_RULE_CODE,FIRST_NOTICE_DAYS,SECOND_NOTICE_DAYS,THIRD_NOTICE_DAYS,FOURTH_NOTICE_DAYS FROM SYS_BILL_BLNG_RULE"},
			{"SysBillPayPlan" , "SELECT PAY_PLAN_ROWID,IN_USE_FLAG,PAY_PLAN_CODE,BILLING_RULE_ROWID,LINE_OF_BUSINESS,STATE,INSTALL_GEN_TYPE,FIXED_FREQUENCY FROM SYS_BILL_PAY_PLAN"},
			{"SysErlyPayDscnt" , "SELECT EARLY_DISCOUNT_ROWID,IN_USE_FLAG,LINE_OF_BUSINESS,AMOUNT,DISCOUNT_TYPE,DEADLINE_DATE FROM SYS_ERLY_PAY_DSCNT"}
									  };

        int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public BillingOptions(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			try
			{
				ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("BillingOptions.Constructor.Error", m_iClientId),p_objEx);
			}
		}
		#endregion

		#region Private Functions
		new private void Initialize()
		{
			try
			{
				base.InitFields(arrFields);
				base.ChildArray = arrChilds;
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("BillingOptions.Initialize.Error", m_iClientId),p_objEx);
			}
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			LocalCache objCache=null;
			XmlNodeList objNodLst = null;
			XMLDoc = p_objXmlDocument;
			XmlNode objNode=null;
			XmlNode objTmpNode=null;
			string sSql="";
			DbReader objRead=null;
			string sValue="";
			string sShortCode="";
			string sShortDesc="";
			XmlElement objParent=null;
			XmlElement objElement=null;
			StringBuilder sbSql=null;
			XmlElement objLstRow=null;
			XmlElement objRowTxt=null;
			DbReader objReadTemp=null;
			Double dblTotal=0;
			string sDP="";
			try
			{
				base.Get();
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/group[1]");  
				objElement = (XmlElement)p_objXmlDocument.CreateElement("BillingRules");  
				objParent.AppendChild(objElement);
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT BILLING_RULE_ROWID,IN_USE_FLAG,BILLING_RULE_CODE,FIRST_NOTICE_DAYS,SECOND_NOTICE_DAYS,THIRD_NOTICE_DAYS,FOURTH_NOTICE_DAYS FROM SYS_BILL_BLNG_RULE ");
			
		
				objLstRow = p_objXmlDocument.CreateElement("listhead");

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "InUse");
				objRowTxt.SetAttribute("width" , "7");
				objRowTxt.InnerText="In Use";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "BRCode");
				objRowTxt.InnerText="Billing Rule Code";
				objRowTxt.SetAttribute("width" , "18");
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "BRName");
				objRowTxt.InnerText="Billing Rule Name";
				objRowTxt.SetAttribute("width" , "15");
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Notice1Days");
				objRowTxt.SetAttribute("width" , "Notice1Days");
				objRowTxt.InnerText="Notice 1 Days";
				objRowTxt.SetAttribute("width" , "13");
				objLstRow.AppendChild(objRowTxt);
				
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Notice2Days");
				objRowTxt.InnerText="Notice 2 Days";
				objRowTxt.SetAttribute("width" , "13");
				objLstRow.AppendChild(objRowTxt);
				
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Notice3Days");
				objRowTxt.InnerText="Notice 3 Days";
				objRowTxt.SetAttribute("width" , "13");
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Notice4Days");
				objRowTxt.InnerText="Notice 4 Days";
				objRowTxt.SetAttribute("width" , "13");
				objLstRow.AppendChild(objRowTxt);
			
				objElement.AppendChild(objLstRow);
				
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objLstRow = p_objXmlDocument.CreateElement("BillingRule");
					
					objRowTxt = p_objXmlDocument.CreateElement("InUse");
					if (Conversion.ConvertObjToStr(objRead.GetValue("IN_USE_FLAG")).Equals("0"))
					{
						objRowTxt.InnerText="N";
					}
					else
					{
						objRowTxt.InnerText="Y";
					}
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("BRCode");
					objRowTxt.InnerText=objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("BILLING_RULE_CODE"))));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("BRName");
					objRowTxt.InnerText=objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("BILLING_RULE_CODE"))));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("Notice1Days");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("FIRST_NOTICE_DAYS"));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("Notice2Days");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("SECOND_NOTICE_DAYS"));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("Notice3Days");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("THIRD_NOTICE_DAYS"));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("Notice4Days");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("FOURTH_NOTICE_DAYS"));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("BRRowId");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("BILLING_RULE_ROWID"));
					objLstRow.AppendChild(objRowTxt);

					objElement.AppendChild(objLstRow);
				
				}
				objElement = (XmlElement)p_objXmlDocument.CreateElement("PayPlans");  
				objParent.AppendChild(objElement);
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT PAY_PLAN_ROWID,IN_USE_FLAG,PAY_PLAN_CODE,BILLING_RULE_ROWID,LINE_OF_BUSINESS,STATE,INSTALL_GEN_TYPE,FIXED_FREQUENCY FROM SYS_BILL_PAY_PLAN ");
			
		
				objLstRow = p_objXmlDocument.CreateElement("listhead");

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "InUse");
				objRowTxt.InnerText="In Use";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "PPCode");
				objRowTxt.InnerText="Pay Plan Code";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "BRCode");
				objRowTxt.InnerText="Billing Rule Code";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "LOB");
				objRowTxt.InnerText="LOB";
				objRowTxt.SetAttribute("width" , "10");
				objLstRow.AppendChild(objRowTxt);
				
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "State");
				objRowTxt.InnerText="State";
				objRowTxt.SetAttribute("width" , "10");
				objLstRow.AppendChild(objRowTxt);
				
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "InstallType");
				objRowTxt.InnerText="Install Gen. Type";
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Frequency");
				objRowTxt.InnerText="Frequency";
				objLstRow.AppendChild(objRowTxt);
			
				objElement.AppendChild(objLstRow);

				if( objRead != null )
					objRead.Close();
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objLstRow = p_objXmlDocument.CreateElement("PayPlan");
					
					objRowTxt = p_objXmlDocument.CreateElement("InUse");
					if (Conversion.ConvertObjToStr(objRead.GetValue("IN_USE_FLAG")).Equals("0"))
					{
						objRowTxt.InnerText="N";
					}
					else
					{
						objRowTxt.InnerText="Y";
					}
					
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("PPCode");
					objRowTxt.InnerText=objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("PAY_PLAN_CODE"))));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("BRCode");
					sSql="SELECT BILLING_RULE_CODE FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID ="+Conversion.ConvertObjToStr(objRead.GetValue("BILLING_RULE_ROWID"));
					objReadTemp=DbFactory.GetDbReader(base.ConnectString,sSql);
					if (objReadTemp.Read())
					{																										
						sValue=Conversion.ConvertObjToStr(objReadTemp.GetValue(0));
						objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
						objRowTxt.InnerText=sShortCode;
					}
					if (objReadTemp!=null)
						objReadTemp.Close();
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("LOB");
					objRowTxt.InnerText=objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("LINE_OF_BUSINESS"))));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("State");
					objRowTxt.InnerText=objCache.GetStateCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("STATE"))));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("InstallType");
					objRowTxt.InnerText=objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("INSTALL_GEN_TYPE"))));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("Frequency");
					objRowTxt.InnerText=objCache.GetCodeDesc(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("FIXED_FREQUENCY"))));
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("PPRowId");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("PAY_PLAN_ROWID"));
					objLstRow.AppendChild(objRowTxt);

					objElement.AppendChild(objLstRow);
				
				}

				objElement = (XmlElement)p_objXmlDocument.CreateElement("EarlyPayDiscounts");  
				objParent.AppendChild(objElement);
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT EARLY_DISCOUNT_ROWID,IN_USE_FLAG,LINE_OF_BUSINESS,AMOUNT,DISCOUNT_TYPE,DEADLINE_DATE FROM SYS_ERLY_PAY_DSCNT ");
			
		
				objLstRow = p_objXmlDocument.CreateElement("listhead");

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "InUse");
				objRowTxt.InnerText="In Use";
				objRowTxt.SetAttribute("width" , "10");
				objLstRow.AppendChild(objRowTxt);

		
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "LOB");
				objRowTxt.InnerText="LOB";
				objRowTxt.SetAttribute("width" , "20");
				objLstRow.AppendChild(objRowTxt);
				
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "Amount");
				objRowTxt.InnerText="Amount";
				objRowTxt.SetAttribute("width" , "25");
				objLstRow.AppendChild(objRowTxt);
				
				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "DollarPercent");
				objRowTxt.InnerText="Dollar or Percent";
				objRowTxt.SetAttribute("width" , "25");
				objLstRow.AppendChild(objRowTxt);

				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				objRowTxt.SetAttribute("colname" , "EligibilityDate");
				
				objRowTxt.InnerText="Eligibility Date";
				objRowTxt.SetAttribute("width" , "35");
				objLstRow.AppendChild(objRowTxt);
			
				objElement.AppendChild(objLstRow);

				if( objRead != null )
					objRead.Close();
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objLstRow = p_objXmlDocument.CreateElement("EarlyPayDiscount");
					
					objRowTxt = p_objXmlDocument.CreateElement("InUse");
					if (Conversion.ConvertObjToStr(objRead.GetValue("IN_USE_FLAG")).Equals("0"))
					{
						objRowTxt.InnerText="N";
					}
					else
					{
						objRowTxt.InnerText="Y";
					}
					objLstRow.AppendChild(objRowTxt);

										
					objRowTxt = p_objXmlDocument.CreateElement("LOB");
					objRowTxt.InnerText=objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("LINE_OF_BUSINESS"))));
					objLstRow.AppendChild(objRowTxt);
					
					sDP=objCache.GetShortCode(Conversion.ConvertStrToInteger(Conversion.ConvertObjToStr(objRead.GetValue("DISCOUNT_TYPE"))));
					
					objRowTxt = p_objXmlDocument.CreateElement("Amount");
					if (sDP.ToLower().Equals("d"))
					{
                        dblTotal = Conversion.ConvertObjToDouble(objRead.GetValue("AMOUNT"), m_iClientId);
						objRowTxt.InnerText=Conversion.ConvertObjToStr(string.Format( "{0:C}" , dblTotal));
					}
					else
						objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("AMOUNT"))+" %";
					objLstRow.AppendChild(objRowTxt);
				
					objRowTxt = p_objXmlDocument.CreateElement("DollarPercent");
					objRowTxt.InnerText=sDP;
					objLstRow.AppendChild(objRowTxt);

					objRowTxt = p_objXmlDocument.CreateElement("EligibilityDate");
					objRowTxt.InnerText=Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(objRead.GetValue("DEADLINE_DATE")),"d");
					objLstRow.AppendChild(objRowTxt);
					
					objRowTxt = p_objXmlDocument.CreateElement("EPRowId");
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("EARLY_DISCOUNT_ROWID"));
					objLstRow.AppendChild(objRowTxt);

					objElement.AppendChild(objLstRow);
				
				}

				//				objNodLst=objNode.SelectNodes(".//listrow");
				//				foreach(XmlNode objTemp in objNodLst)
				//				{
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBBR_InUse']");
				//					if (!objTmpNode.Attributes["title"].Value.Trim().Equals("0"))
				//					{
				//						objTmpNode.Attributes["title"].Value="Y";
				//					}
				//					else
				//					{
				//						objTmpNode.Attributes["title"].Value="N";
				//					}
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBBR_BillingRuleCode']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode.Attributes["title"].Value=objCache.GetShortCode(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBBR_BillingRuleName']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode.Attributes["title"].Value=objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//				}
				//				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SysBillPayPlan']"); 
				//				objNodLst=objNode.SelectNodes(".//listrow");
				//				foreach(XmlNode objTemp in objNodLst)
				//				{
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBPP_InUse']");
				//					if (!objTmpNode.Attributes["title"].Value.Trim().Equals("0"))
				//					{
				//						objTmpNode.Attributes["title"].Value="Y";
				//					}
				//					else
				//					{
				//						objTmpNode.Attributes["title"].Value="N";
				//					}
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBPP_PayPlanCode']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode.Attributes["title"].Value=objCache.GetShortCode(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBPP_LOB']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode.Attributes["title"].Value=objCache.GetShortCode(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBPP_BillingRuleCode']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					sSql="SELECT BILLING_RULE_CODE FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID ="+objTmpNode.Attributes["title"].Value;
				//					objRead=DbFactory.GetDbReader(base.ConnectString,sSql);
				//					if (objRead.Read())
				//					{																										
				//						sValue=Conversion.ConvertObjToStr(objRead.GetValue(0));
				//						objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue),ref sShortCode,ref sShortDesc);
				//						objTmpNode.Attributes["title"].Value=sShortCode;
				//					}
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBPP_State']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode.Attributes["title"].Value=objCache.GetStateCode(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBPP_InstallGenType']");
				//					objTmpNode.Attributes["title"].Value=objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SBPP_Frequency']");
				//					objTmpNode.Attributes["title"].Value=objCache.GetCodeDesc(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//
				//				}
				//				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SysErlyPayDscnt']"); 
				//				objNodLst=objNode.SelectNodes(".//listrow");
				//				foreach(XmlNode objTemp in objNodLst)
				//				{
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SEPD_InUse']");
				//					if (!objTmpNode.Attributes["title"].Value.Trim().Equals("0"))
				//					{
				//						objTmpNode.Attributes["title"].Value="Y";
				//					}
				//					else
				//					{
				//						objTmpNode.Attributes["title"].Value="N";
				//					}
				//
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SEPD_LOB']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode.Attributes["title"].Value=objCache.GetShortCode(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SEPD_DollarOrPercent']");
				//					((XmlElement)objTmpNode).SetAttribute("value",objTmpNode.Attributes["title"].Value);
				//					objTmpNode.Attributes["title"].Value=objCache.GetShortCode(Conversion.ConvertStrToInteger(objTmpNode.Attributes["title"].Value));
				//					objTmpNode=objTemp.SelectSingleNode(".//rowtext[@name='SEPD_EligibilityDate']");
				//					if (!objTmpNode.Attributes["title"].Value.Equals(""))
				//					{
				//						objTmpNode.Attributes["title"].Value=Conversion.GetDBDateFormat(objTmpNode.Attributes["title"].Value,"d");
				//					}
				//				}
				objCache.Dispose();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("BillingOptions.Get.Error", m_iClientId),p_objEx);
			}
			finally
			{
				if (objCache!=null)
					objCache.Dispose();
				objCache=null;
				objNodLst = null;
				objNode=null;
				objTmpNode=null;
				if (objRead!=null)
				{
					objRead.Close();
                    objRead.Dispose();
				}

                if (objReadTemp != null)
                {
                    objReadTemp.Close();
                    objReadTemp.Dispose();
                }
				objRead=null;

			}
			return p_objXmlDocument; 
		}
		#endregion
	}
}