using System;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Xml;
using System.Text;
using System.Web;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System.Collections;
using System.Xml.Linq;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
    /// Author		: Nima Norouzi
    /// Date Created: July 2011		
    /// ************************************************************
    /// Purpose : Implements the Customization features for Auto Assign Adjuster Setup Screen
	/// </summary>
	public class AutoAssignAdjusterCustomization
	{
		/// <summary>
		/// Connection string variable
		/// </summary>
        string m_connectionString = "";

        private Hashtable CustomOptions;

        private int m_iClientId = 0;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string parameter</param>
        public AutoAssignAdjusterCustomization(string p_sConnString, int p_iClientId)
        {
            CustomOptions = new Hashtable();
            m_connectionString = p_sConnString;
            CustomOptions.Add("AAA_LOB_FLG", "LOB");
            CustomOptions.Add("AAA_DEP_FLG", "Departments");
            CustomOptions.Add("AAA_CLT_FLG", "ClaimType");
            CustomOptions.Add("AAA_JUR_FLG", "Jurisdiction");
            CustomOptions.Add("AAA_FWI_FLG", "ForceWorkItems");
            CustomOptions.Add("AAA_WRI_FLG", "WorkItems");
            CustomOptions.Add("AAA_SWI_FLG", "SkipWorkItems");
            CustomOptions.Add("AAA_AVL_FLG", "CurrentlyAvailable");
            CustomOptions.Add("AAA_SSC_FLG", "SeparateSameCriteria");
            m_iClientId = p_iClientId;
        }

		/// <summary>
        /// Gets the Auto Assign Adjuster Customization data.
		/// </summary>
        public XDocument GetCustomization(XDocument XDoc)
		{
            DbConnection objCon = null;
            XElement XElm = null;
            String sSQL = String.Empty;
            try
            {
                sSQL = "SELECT PARM_NAME, STR_PARM_VALUE FROM PARMS_NAME_VALUE WHERE PARM_NAME IN ('DUMMY_ITEM'";
                foreach (DictionaryEntry entry in CustomOptions)
                {
                    sSQL += ", '" + entry.Key.ToString() + "'";
                }
                sSQL += ")";
                using (objCon = DbFactory.GetDbConnection(m_connectionString))
                {
                    objCon.Open();
                    using (Riskmaster.Db.DbReader rdr = objCon.ExecuteReader(sSQL))
                    {
                        while (rdr.Read())
                        {
                            XElm = XDoc.Root.Element(CustomOptions[rdr["PARM_NAME"]].ToString());
                            if (XElm != null)
                            {
                                if (rdr["STR_PARM_VALUE"].ToString() == (-1).ToString() || rdr["STR_PARM_VALUE"].ToString() == (0).ToString())
                                {
                                    XElm.Value = rdr["STR_PARM_VALUE"].ToString();
                                }
                            }
                        }
                   }

                }
                return XDoc;
            }
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AutoAssignAdjusterCustomization.GetCustomization.Err", m_iClientId), p_objEx); 
			}
			finally
			{
			}
		}

		/// <summary>
		/// Saves the Auto Assign Adjuster Customization to the database
		/// </summary>
		public void SaveCustomization(XDocument XDoc)
		{
            DbConnection objCon = null;
            XElement XElm = null;
            String sSQL = String.Empty;
            String sInClause = String.Empty;
			try
			{
                using (objCon = DbFactory.GetDbConnection(m_connectionString))
                {
                    objCon.Open();
                    sSQL = "UPDATE PARMS_NAME_VALUE SET STR_PARM_VALUE = CASE PARM_NAME";
                    foreach (DictionaryEntry entry in CustomOptions)
                    {
                        XElm = XDoc.Root.Element(entry.Value.ToString());

                        if (XElm != null)
                        {
                            if (XElm.Value == (-1).ToString() || XElm.Value == (0).ToString())
                            {
                                sSQL += " WHEN '" + entry.Key.ToString() + "' THEN '" + XElm.Value + "'";
                                if (sInClause == String.Empty)
                                {
                                    sInClause = "('" + entry.Key.ToString() + "'";
                                }
                                else
                                {
                                    sInClause += ",'" + entry.Key.ToString() + "'";
                                }
                            }
                        }
                    }
                    if (sInClause != String.Empty)
                    {
                        sSQL = sSQL + " END WHERE PARM_NAME IN " + sInClause + ")";
                        objCon.ExecuteNonQuery(sSQL); //update a column (for multiple records) using one query!
                    }

                }
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("AutoAssignAdjusterCustomization.SaveCustomization.Err", m_iClientId), p_objEx); 
			}
		}
	}
}
