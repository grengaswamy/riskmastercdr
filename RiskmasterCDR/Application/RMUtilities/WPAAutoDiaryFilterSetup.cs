using System.Linq;
using System;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;   
using System.Data; 
using Riskmaster.Common; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   06,Jul 2005
	///Purpose :   WPA Auto Filter Setup form
	/// </summary>
	public class WPAAutoDiaryFilterSetup
	{
		/// <summary>
		/// Private connection string variable
		/// </summary>
		private string m_sConnStr="";
        private int m_iClientId=0;
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConStr">Connection String</param>
        public WPAAutoDiaryFilterSetup(string p_sConStr, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sConnStr=p_sConStr;
		}


		/// Name			: Get
		/// Author			: Pankaj Chowdhury
		/// Date Created	: 06 Jul 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Gets the xml structure with data for the first step of the wizard
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document structure</param>
		/// <returns>Xml document with data and its structure for UI</returns>
		public XmlDocument Get(XmlDocument p_objXmlDoc) 
		{	
			string sId="";
			string sSQL="";
			string sFilterType="";
			string sDefVal="";
			string sData="";
			DbReader objRdr=null;
			XmlElement objElm=null;
			DataTable objTbl=null;
			XmlElement objSelLst=null;
			XmlElement objAvlLst=null;
			XmlElement objNewElm=null;
            var SelectedNodeSql = string.Empty;
            string templateId = string.Empty;
			try
			{
                objAvlLst = (XmlElement)p_objXmlDoc.SelectSingleNode("//AvlBodyParts"); 

				objSelLst = (XmlElement)p_objXmlDoc.SelectSingleNode("//SelBodyParts"); 

                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//id"); 
				if(objElm!=null)
					sId= objElm.InnerText;
                objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//templateid");
                if (objElm != null)
                    templateId = objElm.InnerText;
                //asharma326 MITS 35669 
                if (WPAAutoDiarySetup.xFilters != null)
                {
                    //var SelectedNode =(xElementFilters.Elements("AvlFilters").Where(n=>n.Attribute("id").Value==sId).Select(n=>n.Value)).FirstOrDefault();
                    //SelectedNodeSql = (from node in xElementFilters.Descendants("level")
                    //                   where node.Attribute("id").Value == sId
                    //                   //where (node.Elements("level")).Where(n=>n.Attribute("id").Value==sId)
                    //                   select node.Attribute("SQLFill").Value).FirstOrDefault();
                    if (WPAAutoDiarySetup.xFilters.ContainsKey(templateId) && WPAAutoDiarySetup.xFilters[templateId].ContainsKey(sId))
                    {
                        SelectedNodeSql = WPAAutoDiarySetup.xFilters[templateId][sId];
                    }
                }
                //if (WPAAutoDiarySetup.xElementFilters != null)
                //{
                //    //var SelectedNode =(xElementFilters.Elements("AvlFilters").Where(n=>n.Attribute("id").Value==sId).Select(n=>n.Value)).FirstOrDefault();
                //    SelectedNodeSql = (from node in WPAAutoDiarySetup.xElementFilters.Descendants("level")
                //                       where node.Attribute("id").Value == sId
                //                       //where (node.Elements("level")).Where(n=>n.Attribute("id").Value==sId)
                //                       select node.Attribute("SQLFill").Value).FirstOrDefault();
                //}
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//SQLFill");
                if (objElm != null) { sSQL = SelectedNodeSql; }
					//sSQL= objElm.InnerText; 
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//FilterType");
				if(objElm!=null)
					sFilterType= objElm.InnerText; 
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//DefValue");
				if(objElm!=null)
					sDefVal= objElm.InnerText; 
				objElm = (XmlElement)p_objXmlDoc.SelectSingleNode("//data");
				if(objElm!=null)
					sData= objElm.InnerText;

                //if (sSQL.ToUpper().IndexOf("IN") > -1)
                //{
                //    int index = sSQL.LastIndexOf("IN");
                //    string sTemp = sSQL.Substring(index + 2);
                //    if (!(sTemp.IndexOf("(") > -1 && sTemp.IndexOf(")") > -1))
                //        sSQL = sSQL.Replace(sTemp, "(" + sTemp + ")");
                //}

				objRdr=DbFactory.GetDbReader(m_sConnStr,sSQL);
				objTbl = objRdr.GetSchemaTable(); 
				while(objRdr.Read())
				{
					string sName="";
					for(int iCtr=0;iCtr<objRdr.FieldCount-1;iCtr++)  
					{
						sName+=objRdr.GetString(iCtr);
					}

                    //rupal:r8 auto diary setup enh, if condition changed
                    //if (sData.IndexOf("," + objRdr.GetInt32(objRdr.FieldCount - 1).ToString() + ",") != -1) 
                    if(sData.IndexOf("," + objRdr[objRdr.FieldCount-1].ToString() +  ",") != -1) 
					{
						objNewElm = p_objXmlDoc.CreateElement("option");
                        //rupal:r8 auto diary setup enh
						//objNewElm.SetAttribute("value",objRdr.GetInt32(objRdr.FieldCount-1).ToString());
                        objNewElm.SetAttribute("value", objRdr[objRdr.FieldCount - 1].ToString());

						objNewElm.InnerText = sName;
						objSelLst.AppendChild(objNewElm);
					}
					else
					{
						objNewElm = p_objXmlDoc.CreateElement("option");
                        
                        //rupal:r8 auto diary setup enh
                        //objNewElm.SetAttribute("value",objRdr.GetInt32(objRdr.FieldCount-1).ToString());
                        objNewElm.SetAttribute("value", objRdr[objRdr.FieldCount - 1].ToString());

						objNewElm.InnerText = sName;
						objAvlLst.AppendChild(objNewElm);
					}
				}
				return p_objXmlDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WPAAutoDiaryFilterSetup.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objElm=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr=null;
				}
                if (objTbl != null)
				{
                    objTbl.Dispose();
				}
			}
		}
	}
}

