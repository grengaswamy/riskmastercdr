﻿using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
   public class PolicyCodeMapping
    {
        string m_sConnString = string.Empty;
        private int m_iClientId = 0;

        public PolicyCodeMapping(string p_sConnectString)
		{
			m_sConnString=p_sConnectString;
		}

        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objReader = null;
            String sSql = null;
            string sPolicysystemTypeCode = string.Empty;
            string sValue = string.Empty;
            try
            {
                //rupal:start                
                sPolicysystemTypeCode = p_objXmlDocument.SelectSingleNode("PolicySystemTypeCode").InnerText;
                //sSql = " SELECT TABLE_NAME,GLOSSARY_TEXT.TABLE_ID FROM GLOSSARY,GLOSSARY_TEXT WHERE GLOSSARY.GLOSSARY_TYPE_CODE IN (2,3) AND GLOSSARY.TABLE_ID=GLOSSARY_TEXT.TABLE_ID ORDER BY TABLE_NAME";
                sSql = " SELECT RMX_TABLE_ID,GLOSSARY_TEXT.TABLE_NAME, INCLUDE_CLAIM_TYPE FROM PS_MAP_TABLES,GLOSSARY_TEXT WHERE PS_MAP_TABLES.RMX_TABLE_ID=GLOSSARY_TEXT.TABLE_ID AND POLICY_SYSTEM_TYPE_ID = " + sPolicysystemTypeCode;
                //rupal:end
                objReader = DbFactory.GetDbReader(m_sConnString, sSql);
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("PolicyCodeMapping");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("CodeTypes");
                while (objReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("option");
                    //rupal:start
                    //objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("TABLE_ID")));
                    sValue = Conversion.ConvertObjToStr(objReader.GetValue("RMX_TABLE_ID")) + "|" + Conversion.ConvertObjToStr(objReader.GetValue("INCLUDE_CLAIM_TYPE"));
                    //objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objReader.GetValue("RMX_TABLE_ID")));                    
                    objElemTemp.SetAttribute("value", sValue);                    
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objReader.GetValue("TABLE_NAME"));
                    //rupal:end
                    objElemChild.AppendChild(objElemTemp);

                }

                objDOM.FirstChild.AppendChild(objElemChild);
                objReader.Close();

                return objDOM;
            }
            catch (RMAppException p_objEx)
            {
                
                throw new RMAppException(Globalization.GetString("PolicyCodeMapping.Get.Error",m_iClientId), p_objEx);
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
                
            }
            finally
            {
                objDOM = null;
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                if (!objReader.IsClosed)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objReader = null;
            }

        }
        public XmlDocument GetCodeDetails(XmlDocument p_objXmlDocument)
        {
            XmlNode objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            DbReader objDbReader = null;
            String sSql = null;
            string sTableName = string.Empty;
            string sCodeType = string.Empty;
            string sMultipleRelationship = string.Empty;//rupal
            string sPolicySystemTypeId = string.Empty;//rupal
            string sPSTableID = string.Empty;//rupal
            string sTableNmsuffix =string.Empty, sTableNm = string.Empty, sPolicySystemID = string.Empty;     //aaggarwal29: Code mapping change
            string sMappingTablePrefix = string.Empty; // aaggarwal29: Code mapping change
            string sIncludeClaimType = string.Empty;
            LocalCache objcache = null;
            try
            {
                objElemParent = p_objXmlDocument.SelectSingleNode("PolicyCodeMapping");
                sCodeType= p_objXmlDocument.SelectSingleNode("//PolicyCodeMapping/SelectedCodeType").InnerText;

                //rupal:start
                /*
                sSql = "SELECT SYSTEM_TABLE_NAME FROM GLOSSARY WHERE TABLE_ID =" + sCodeType;
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objDbReader.Read())
                {
                    sTableName = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                }
                objDbReader.Close();
                */
                sPolicySystemTypeId = p_objXmlDocument.SelectSingleNode("//PolicyCodeMapping/PolicySystemTypeCode").InnerText;
                //aaggarwal29: Code mapping change start
                sPolicySystemID = p_objXmlDocument.SelectSingleNode("//PolicyCodeMapping/PolicySystemID").InnerText;

                sSql = "SELECT MULTIPLE_RELATIONSHIP, SYSTEM_TABLE_NAME, INCLUDE_CLAIM_TYPE FROM PS_MAP_TABLES, GLOSSARY WHERE POLICY_SYSTEM_TYPE_ID= " + sPolicySystemTypeId + " AND RMX_TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = " + sCodeType;
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objDbReader.Read())
                {
                    sMultipleRelationship = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                    sTableNmsuffix = Conversion.ConvertObjToStr(objDbReader.GetValue(1));
                    sIncludeClaimType = Conversion.ConvertObjToStr(objDbReader.GetValue(2));
                }
                objDbReader.Close();

                sSql = "SELECT TABLE_PREFIX FROM POLICY_X_WEB WHERE POLICY_SYSTEM_ID =" + sPolicySystemID;
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objDbReader.Read())
                {
                   
                    sMappingTablePrefix = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                }

                if (Conversion.ConvertStrToInteger(sMultipleRelationship) == 1)
                {
                    
                    
                    sTableNm = sMappingTablePrefix.Trim() + "_" + sTableNmsuffix.Trim() + "(M)";
                }
                else
                {
                    
                    
                    sTableNm = sMappingTablePrefix.Trim() + "_" + sTableNmsuffix.Trim();
                }
                sSql = "SELECT TABLE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME ='" + sTableNm + "'";
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                if (objDbReader.Read())
                {
                    sPSTableID = Conversion.ConvertObjToStr(objDbReader.GetValue(0));
                }
                if (sPSTableID == string.Empty)
                    sPSTableID = "0";
                //aaggarwal29: Code mapping change end
                //rupal:end
                //rupal:start
                objcache = new LocalCache(m_sConnString, m_iClientId);
                int iStateTableId = objcache.GetTableId("STATES");
                int iPolicyStatusTableId = objcache.GetTableId("POLICY_STATUS");
                //get table id for STATES
                //sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT RM_CODE_ID FROM POLICY_CODE_MAPPING) AND GLOSSARY.SYSTEM_TABLE_NAME='" + sTableName + "'AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID";                
                if (sCodeType == iStateTableId.ToString())//if state mapping is required
                {
                    // aaggarwal29: Code mapping changes start
                    
                    if (DbFactory.IsOracleDatabase(m_sConnString))
                    {
                        sSql = "SELECT STATE_NAME ||' (' || STATE_ID ||')' CODE_DESC, STATE_ROW_ID CODE_ID,STATE_ID SHORT_CODE FROM STATES WHERE STATE_ROW_ID NOT IN (SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID = " + sPolicySystemID + " AND RMX_TABLE_ID=" + iStateTableId + ") AND STATE_NAME IS NOT NULL AND DELETED_FLAG <> -1 ORDER BY STATE_NAME";
                        if (sIncludeClaimType == "1")
                        {
                            sSql = "SELECT STATE_NAME ||' (' || STATE_ID ||')' CODE_DESC, STATE_ROW_ID CODE_ID,STATE_ID SHORT_CODE FROM STATES WHERE STATE_NAME IS NOT NULL AND DELETED_FLAG <> -1 ORDER BY STATE_NAME";
                        }

                    }
                    else
                    {
                        sSql = "SELECT STATE_NAME +' (' + STATE_ID +')' CODE_DESC, STATE_ROW_ID CODE_ID,STATE_ID  SHORT_CODE FROM STATES WHERE STATE_ROW_ID NOT IN (SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID = " + sPolicySystemID + " AND RMX_TABLE_ID=" + iStateTableId + ") AND STATE_NAME IS NOT NULL AND DELETED_FLAG <> -1 ORDER BY STATE_NAME";
                        if (sIncludeClaimType == "1")
                        {
                            sSql = "SELECT STATE_NAME +' (' + STATE_ID +')' CODE_DESC, STATE_ROW_ID CODE_ID,STATE_ID SHORT_CODE FROM STATES WHERE STATE_NAME IS NOT NULL AND DELETED_FLAG <> -1 ORDER BY STATE_NAME";
                        }
                        //aaggarwal29: Code mapping changes end
                    }
                }
                //aaggarwal29: MITS 33734: Going forward, RM Codes for PolicyStatus table type will be available for binding , even if they are already mapped to PT Codes
                    // thus allowing scenario like for RM(In Effect)-Pt(Verified); RM(In Effect)-Pt(Pending). Assuming  Multiple relationship will never be set On for PolicyStatus.
                else if (sCodeType == iPolicyStatusTableId.ToString())
                {
                    sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE  GLOSSARY.TABLE_ID = "+sCodeType+" AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ";
                }
                else
                {
                    if (sIncludeClaimType == "0")
                    {
                        // aaggarwal29: Code mapping changes start
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT RMX_CODE_ID FROM POLICY_CODE_MAPPING WHERE POLICY_SYSTEM_ID = " + sPolicySystemID + ") AND GLOSSARY.TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                        //aaggarwal29: Code mapping changes end
                    }
                    else if (sIncludeClaimType == "1")
                    {
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sCodeType + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                    }
                }
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                objElemChild = p_objXmlDocument.CreateElement("RMCodes");
                while (objDbReader.Read())
                {
                    objElemTemp = p_objXmlDocument.CreateElement("option");
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_ID")));
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_DESC")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("SHORT_CODE"));
                    objElemChild.AppendChild(objElemTemp);
                    

                }
               p_objXmlDocument.FirstChild.AppendChild(objElemChild);

               objElemChild = null;
                objDbReader.Close();
                //rupal:start
                //sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT POL_SYS_ID FROM POLICY_CODE_MAPPING) AND  GLOSSARY.SYSTEM_TABLE_NAME='" + "PS_" + sTableName + "'AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID";
                sSql = string.Empty;
                if (sMultipleRelationship == "0")
                {
                    if (sCodeType == iStateTableId.ToString())//if state mapping is required
                    {
                        if (DbFactory.IsOracleDatabase(m_sConnString))
                        {
                            sSql = "SELECT CODE_DESC ||' (' || CODES_TEXT.SHORT_CODE || ')' CODE_DESC ,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT PS_CODE_ID FROM POLICY_CODE_MAPPING) AND  GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC ";
                            if (sIncludeClaimType == "1") //overwrite existing query based on include_claim_type clause
                            {
                                sSql = "SELECT CODE_DESC ||' (' || CODES_TEXT.SHORT_CODE || ')' CODE_DESC ,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC ";
                            }
                        }
                        else
                        {
                            sSql = "SELECT CODE_DESC +' (' + CODES_TEXT.SHORT_CODE + ')' CODE_DESC ,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT PS_CODE_ID FROM POLICY_CODE_MAPPING) AND  GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC ";
                            if (sIncludeClaimType == "1") //overwrite existing query based on include_claim_type clause
                            {
                                sSql = "SELECT CODE_DESC +' (' + CODES_TEXT.SHORT_CODE + ')' CODE_DESC ,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC ";
                            }
                        }
                    }
                    else
                    {
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE CODES_TEXT.CODE_ID NOT IN (SELECT PS_CODE_ID FROM POLICY_CODE_MAPPING) AND  GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC";
                        if (sIncludeClaimType == "1") //overwrite existing query based on include_claim_type clause
                        {
                            sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID= " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC";
                        }
                    }
                }
                else
                {
                    if (sCodeType == iStateTableId.ToString())//if state mapping is required
                    {
                        if (DbFactory.IsOracleDatabase(m_sConnString))
                        {
                            sSql = "SELECT CODE_DESC ||' (' || CODES_TEXT.SHORT_CODE || ')' CODE_DESC ,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC ";
                        }
                        else
                        {
                            sSql = "SELECT CODE_DESC +' (' + CODES_TEXT.SHORT_CODE + ')' CODE_DESC ,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC";
                        }
                    }
                    else
                    {
                        sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID,CODES_TEXT.SHORT_CODE FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID= " + sPSTableID + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG<> -1 ORDER BY CODE_DESC";
                    }
                }
                //rupal:end
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
               objElemChild = p_objXmlDocument.CreateElement("PolSysCodes");
                while (objDbReader.Read())
                {
                    objElemTemp = p_objXmlDocument.CreateElement("option");
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_ID")));
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_DESC")) + " -  " + Conversion.ConvertObjToStr(objDbReader.GetValue("SHORT_CODE"));
                    objElemChild.AppendChild(objElemTemp);
                }

                p_objXmlDocument.FirstChild.AppendChild(objElemChild);


                objDbReader.Close();
                //rupal:start
                /*
                sSql = "SELECT POLICY_CODE_MAPPING.ROW_ID,RM_CODE_ID,POL_SYS_ID,RM.CODE_DESC RM_CODE,PS.CODE_DESC POL_SYS FROM POLICY_CODE_MAPPING,CODES_TEXT RM,CODES_TEXT PS WHERE CODE_TYPE = " + sCodeType;
                sSql = sSql + " AND RM.CODE_ID = RM_CODE_ID AND PS.CODE_ID=POL_SYS_ID";
                 */
                if (sCodeType == iStateTableId.ToString())//if state mapping is required
                {
                    if (DbFactory.IsOracleDatabase(m_sConnString))
                    {
                        sSql = "SELECT POLICY_CODE_MAPPING.ROW_ID,RMX_CODE_ID,PS_CODE_ID,STATES.STATE_NAME || ' (' || STATE_ID || ')' RM_CODE, PS.CODE_DESC || ' (' || PS.SHORT_CODE || ')' POL_SYS, CL.CODE_DESC CL_DESC, CLAIM_TYPE_CODE FROM POLICY_CODE_MAPPING,STATES,CODES_TEXT PS,CODES_TEXT CL WHERE RMX_TABLE_ID = " + sCodeType;
                        sSql = sSql + " AND STATES.STATE_ROW_ID = RMX_CODE_ID AND PS.CODE_ID=PS_CODE_ID AND CL.CODE_ID = CLAIM_TYPE_CODE";
                        sSql = sSql + " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID =" + sPolicySystemID;         // aaggarwal29: Code mapping change
                    }
                    else
                    {
                        sSql = "SELECT POLICY_CODE_MAPPING.ROW_ID,RMX_CODE_ID,PS_CODE_ID,STATES.STATE_NAME + ' (' + STATE_ID + ')' RM_CODE,PS.CODE_DESC + ' (' + PS.SHORT_CODE + ')' POL_SYS, CL.CODE_DESC CL_DESC, CLAIM_TYPE_CODE FROM POLICY_CODE_MAPPING,STATES,CODES_TEXT PS, CODES_TEXT CL WHERE RMX_TABLE_ID = " + sCodeType;
                        sSql = sSql + " AND STATES.STATE_ROW_ID = RMX_CODE_ID AND PS.CODE_ID=PS_CODE_ID AND CL.CODE_ID = CLAIM_TYPE_CODE";
                        sSql = sSql + " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID =" + sPolicySystemID;         // aaggarwal29: Code mapping change
                    }
                }
                else
                {
                     // changes by dbisht6 for mits 34930 starts             
                    //RM_SHORT_CODE AND PS_SHORT_CODE COLUMNS ARE ADDED IN THE QUERY TO RETRIEVE THE SHORT CODES ALONG WITH DESCPRIPTION
                    sSql = "SELECT POLICY_CODE_MAPPING.ROW_ID,RMX_CODE_ID,PS_CODE_ID,RM.CODE_DESC RM_CODE,PS.CODE_DESC POL_SYS, CL.CODE_DESC CL_DESC, CLAIM_TYPE_CODE,RM.SHORT_CODE RM_SHORT_CODE,PS.SHORT_CODE PS_SHORT_CODE FROM POLICY_CODE_MAPPING,CODES_TEXT RM,CODES_TEXT PS, CODES_TEXT CL WHERE RMX_TABLE_ID = " + sCodeType;
                    sSql = sSql + " AND RM.CODE_ID = RMX_CODE_ID AND PS.CODE_ID=PS_CODE_ID AND CL.CODE_ID = CLAIM_TYPE_CODE";
                    sSql = sSql + " AND POLICY_CODE_MAPPING.POLICY_SYSTEM_ID =" + sPolicySystemID;         // aaggarwal29: Code mapping change
                }
                //rupal:end
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                string sKey = string.Empty;
                while (objDbReader.Read())
                {
                    objElemTemp = p_objXmlDocument.CreateElement("MappedCodes");
                    if (sCodeType == iStateTableId.ToString())
                    {
                        objElemTemp.SetAttribute("RMCode", Conversion.ConvertObjToStr(objDbReader.GetValue("RM_CODE")));
                        objElemTemp.SetAttribute("PolSysCode", Conversion.ConvertObjToStr(objDbReader.GetValue("POL_SYS")));
                    }
                    else
                    {
                        objElemTemp.SetAttribute("RMCode", Conversion.ConvertObjToStr(objDbReader.GetValue("RM_SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objDbReader.GetValue("RM_CODE")));
                        objElemTemp.SetAttribute("PolSysCode", Conversion.ConvertObjToStr(objDbReader.GetValue("PS_SHORT_CODE")) + " - " + Conversion.ConvertObjToStr(objDbReader.GetValue("POL_SYS")));
                    }
                        // dbisht6 end
                    objElemTemp.SetAttribute("RowId", Conversion.ConvertObjToStr(objDbReader.GetValue("ROW_ID")));
                    objElemTemp.SetAttribute("ClaimCd", Conversion.ConvertObjToStr(objDbReader.GetValue("CL_DESC")));
                    sKey = Conversion.ConvertObjToStr(objDbReader.GetValue("RMX_CODE_ID")) + Conversion.ConvertObjToStr(objDbReader.GetValue("CLAIM_TYPE_CODE"));
                    objElemTemp.SetAttribute("Key", sKey);
                    p_objXmlDocument.FirstChild.AppendChild(objElemTemp);
                }


                return p_objXmlDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicyCodeMapping.Get.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objElemParent = null;
                objElemChild = null;
                objElemTemp = null;
                //tanwar2 - !objDbReader.IsClosed => This is wrong.
                //We should check for null and then dispose.
                //It is responsibility of close/dispose method to skip dispose if reader is closed.
                //if (!objDbReader.IsClosed)
                if(objDbReader!=null)
                {
                    objDbReader.Close();
                    objDbReader.Dispose();
                }
                if (objcache != null)
                    objcache.Dispose();
            }

        }


        public bool SavePolicyCodeInfo(XmlDocument p_objInputXMLDoc)
        {
            string sSQL = "";
            DbConnection objCon = null;
            XmlElement objPolicySysXMLEle = null;
            string sRMCodeId = string.Empty;
            string sPolSysCodeId = string.Empty;
            string sCodeType = string.Empty;
            string sPolicySystemId = string.Empty;
            string sClaimType = string.Empty;
            try
            {
                objPolicySysXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//PolicyCodeMapping/AddCode");
                sRMCodeId = objPolicySysXMLEle.GetElementsByTagName("RMCode").Item(0).InnerText;
                sPolSysCodeId = objPolicySysXMLEle.GetElementsByTagName("PolSysCode").Item(0).InnerText;
                sPolicySystemId = objPolicySysXMLEle.GetElementsByTagName("PolicySystemId").Item(0).InnerText;
                sCodeType = objPolicySysXMLEle.GetElementsByTagName("CodeType").Item(0).InnerText;
                //RUPAL:START
                sPolicySystemId = objPolicySysXMLEle.GetElementsByTagName("PolicySystemId").Item(0).InnerText;
                sClaimType = objPolicySysXMLEle.GetElementsByTagName("ClaimTypeCd").Item(0).InnerText;
                //sSQL = "INSERT INTO POLICY_CODE_MAPPING(ROW_ID,RM_CODE_ID,POL_SYS_ID,CODE_TYPE)" +
                //            "VALUES(" + Utilities.GetNextUID(m_sConnString, "POLICY_CODE_MAPPING") + "," + sRMCodeId + "," + sPolSysCodeId + "," +
                //            sCodeType+")";

                //sSQL = "INSERT INTO POLICY_CODE_MAPPING(ROW_ID,RMX_CODE_ID,PS_CODE_ID,POLICY_SYSTEM_ID,RMX_TABLE_ID)" +
                //            "VALUES(" + Utilities.GetNextUID(m_sConnString, "POLICY_CODE_MAPPING") + "," + sRMCodeId + "," + sPolSysCodeId + "," + sPolicySystemId + "," +
                //            sCodeType + ")";

                sSQL = "INSERT INTO POLICY_CODE_MAPPING(ROW_ID,RMX_CODE_ID,PS_CODE_ID,POLICY_SYSTEM_ID,RMX_TABLE_ID, CLAIM_TYPE_CODE)" +
                            "VALUES(" + Utilities.GetNextUID(m_sConnString, "POLICY_CODE_MAPPING", m_iClientId) + "," + sRMCodeId + "," + sPolSysCodeId + "," + sPolicySystemId + "," +
                            sCodeType + ","+sClaimType+")";


                //RUPAL:END
                objCon = DbFactory.GetDbConnection(m_sConnString);
                objCon.Open();
                objCon.ExecuteNonQuery(sSQL);
                return true;
            }
            catch (RMAppException p_objRMExp)
            {
                throw new RMAppException(Globalization.GetString("PolicyCodeMapping.Save.Error",m_iClientId), p_objRMExp);
                
            }
            catch (Exception p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                if (objCon != null)
                    objCon.Dispose();

                if (objPolicySysXMLEle != null)
                    objPolicySysXMLEle = null;

            }
        }
        public XmlDocument DeleteCodeMapping(XmlDocument p_objXmlDocument)
        {
            string sRowId = string.Empty;
            XmlElement objElm = null;
            DbConnection objConn = null;
            string sSQL = string.Empty;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//DeletedCode");
                if (objElm != null)
                    sRowId = objElm.InnerText;

                objConn = DbFactory.GetDbConnection(m_sConnString);
                objConn.Open();

                if (sRowId != "")
                {
                    sSQL = "DELETE FROM POLICY_CODE_MAPPING  WHERE ROW_ID IN(" + sRowId+")";
                }
                objConn.ExecuteNonQuery(sSQL);
                return p_objXmlDocument;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicyCodeMapping.Delete.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objElm = null;
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
            }
        }

        public XmlDocument GetClaimTypes(XmlDocument p_objInputXmlDoc)
        {
            string sTableId = string.Empty;
            LocalCache objCache = new LocalCache(m_sConnString, m_iClientId);
            string sSql;
            DbReader objDbReader = null;
            XmlDocument objDOM;
            XmlNode objElemParent, objElemChild ;
            XmlElement objElemTemp;
            string sPolicysystemTypeCode =string.Empty;

            try
            {
                if (p_objInputXmlDoc.SelectSingleNode("//PolicySystemTypeCode") != null)
                {
                    sPolicysystemTypeCode = p_objInputXmlDoc.SelectSingleNode("//PolicySystemTypeCode").InnerText;
                }

                sTableId = objCache.GetTableId("CLAIM_TYPE").ToString();

                sSql = "SELECT CODE_DESC,CODES_TEXT.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY WHERE GLOSSARY.TABLE_ID = " + sTableId + " AND GLOSSARY.TABLE_ID = CODES.TABLE_ID AND CODES.CODE_ID= CODES_TEXT.CODE_ID AND CODES.DELETED_FLAG <> -1 ORDER BY CODE_DESC ";
                objDbReader = DbFactory.GetDbReader(m_sConnString, sSql);
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("PolicyCodeMapping");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("RMClaimTypes");
                while (objDbReader.Read())
                {
                    objElemTemp = objDOM.CreateElement("option");
                    objElemTemp.SetAttribute("value", Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_ID")));
                    objElemTemp.InnerText = Conversion.ConvertObjToStr(objDbReader.GetValue("CODE_DESC"));
                    objElemChild.AppendChild(objElemTemp);
                }
                objDOM.FirstChild.AppendChild(objElemChild);
                objDbReader.Close();
                return objDOM;
            }
            catch (Exception p_objExp)
            {
                throw p_objExp;
            }
            finally
            {
                if (objCache != null)
                {
                    objCache = null;
                }
                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader = null;
                }
            }
        }

    }
}
