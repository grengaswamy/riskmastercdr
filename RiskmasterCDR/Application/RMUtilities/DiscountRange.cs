using System;
using System.Xml;
using Riskmaster.ExceptionTypes; 
using Riskmaster.Common; 


namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   Container class for discount range
	/// </summary>
	public class DiscountRange
	{
		public DiscountRange()
		{
		}

		/// <summary>
		/// Stores the DiscountRangeRowId value
		/// </summary>
		private int m_iDiscountRangeRowId = 0;

		/// <summary>
		/// Stores the DiscountRowid value
		/// </summary>
		private int m_iDiscountRowid = 0;

		/// <summary>
		/// Stores the Amount value
		/// </summary>
		private double m_dAmount = 0.0;

		/// <summary>
		/// Stores the BeginningRange value
		/// </summary>
		private double m_dBeginningRange = 0.0;

		/// <summary>
		/// Stores the EndRange value
		/// </summary>
		private double m_dEndRange = 0.0;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the NewRecord value
		/// </summary>
		private bool m_bNewRecord = false;

		/// <summary>
		/// Stores the pParent value
		/// </summary>
		private DiscountRangeList m_objParent = null;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

		/// <summary>
		/// Read/Write property for DiscountRangeRowId
		/// </summary>
		internal int DiscountRangeRowId
		{
			get
			{
				return m_iDiscountRangeRowId;
			}
			set
			{
				m_iDiscountRangeRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for DiscountRowid
		/// </summary>
		internal int DiscountRowid
		{
			get
			{
				return m_iDiscountRowid;
			}
			set
			{
				m_iDiscountRowid = value;
			}
		}

		/// <summary>
		/// Read/Write property for Amount
		/// </summary>
		internal double Amount
		{
			get
			{
				return m_dAmount;
			}
			set
			{
				m_dAmount = value;
			}
		}

		/// <summary>
		/// Read/Write property for BeginningRange
		/// </summary>
		internal double BeginningRange
		{
			get
			{
				return m_dBeginningRange;
			}
			set
			{
				m_dBeginningRange = value;
			}
		}

		/// <summary>
		/// Read/Write property for EndRange
		/// </summary>
		internal double EndRange
		{
			get
			{
				return m_dEndRange;
			}
			set
			{
				m_dEndRange = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for NewRecord
		/// </summary>
		internal bool NewRecord
		{
			get
			{
				return m_bNewRecord;
			}
			set
			{
				m_bNewRecord = value;
			}
		}

		/// <summary>
		/// Read/Write property for pParent
		/// </summary>
		internal DiscountRangeList Parent
		{
			//IMPORTANT DO NO SET PARENT PROPERTY FROM PARENT'S OBJECT INITIALIZE EVENT !!!!!!
			get
			{
				return m_objParent ;
			}
			set
			{
				m_objParent = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}

		public void Clear()
		{
			m_sAddedByUser = "";
			m_bDataChanged = false;
			m_sDttmRcdLastUpd = "";
			m_sDttmRcdLastUpd = "";
			m_sUpdatedByUser = "";
			m_dAmount = 0;
			m_dBeginningRange = 0;
			m_iDiscountRangeRowId = 0;
			m_iDiscountRowid = 0;
			m_dEndRange = 0;
			m_bNewRecord = false;
		}
	}
}
