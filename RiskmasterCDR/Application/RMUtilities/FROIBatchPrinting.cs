using System;
using System.Xml; 
using Riskmaster.Common; 
using Riskmaster.ExceptionTypes; 
using Riskmaster.Db;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   28th,Apr 2005
	///Purpose :   Implements FROI Batch Printing Options form
	/// </summary>
	public class FROIBatchPrinting : UtilitiesBase 
	{
		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "ROW_ID"},
			{"OrgHighLevel", "FROI_BAT_HIGH_LVL"},
			{"OrgLowLevel", "FROI_BAT_LOW_LVL"},
			{"ExcludeMaster", "FROI_BAT_CLD_CLM"}
		};
		
		/// <summary>
		/// String array for combo values
		/// </summary>
		private string[,] arrChilds = {
			{"OrgHighLevel", "TABLE_ID", "SYSTEM_TABLE_NAME", "GLOSSARY" , "TABLE_ID BETWEEN 1005 AND 1012 OR TABLE_ID=0"},
			{"OrgLowLevel",  "TABLE_ID", "SYSTEM_TABLE_NAME", "GLOSSARY" , "TABLE_ID BETWEEN 1005 AND 1012 OR TABLE_ID=0"}
									  };

        private int m_iClientId = 0;
		/// Name		: FROIBatchPrinting
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor with Connection String
		/// </summary>
		/// <param name="p_sConnString">Connection String parameter</param>
        public FROIBatchPrinting(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

        //gagnihotri MITS 11995 Changes made for Audit table
        public FROIBatchPrinting(string p_sConnString, string p_sUserName, int p_iClientId)
            : base(p_sConnString, p_sUserName, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the properties and variables
		/// </summary>
		new private void Initialize()
		{
			TableName = "SYS_PARMS";
			KeyField = "ROW_ID";
			InitFields(arrFields);
			base.ChildArray = arrChilds;			
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Public default get method
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Xmldocument with data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			XmlNode objHighNode=null;
			try
			{
				objHighNode = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='OrgHighLevel']");
				if(objHighNode!=null)
					if(((XmlElement)objHighNode).GetAttribute("value")!="")
					{
						//This is the case of Change of High Level Combo
						return LoadLowLevel(p_objXmlDocument,((XmlElement)objHighNode).GetAttribute("value"));
					}
				XMLDoc = p_objXmlDocument;
				Get();
				return XMLDoc;	
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FROIBatchPrinting.Get.Err", m_iClientId),p_objEx);
			}
			finally
			{
				objHighNode=null;
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/28/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Public Default Get method
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data for saving</param>
		/// <returns>Xmldocument with saved data</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				Save();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("FROIBatchPrinting.Save.Err", m_iClientId), p_objEx);
			}
		}

		/// Name		: LoadLowLevel
		/// Author		: Pankaj Chowdhury
		/// Date Created: 06/27/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Public default get method
		/// </summary>
		/// <param name="p_objXmlDoc">Org Level Id</param>
		/// <param name="p_sHighLevel">Org Level</param>
		/// <returns>Xmldocument with data</returns>
		private XmlDocument LoadLowLevel(XmlDocument p_objXmlDoc, string p_sHighLevel)
		{
			XmlElement objLowNode=null;
			DbReader objRdr=null;
			XmlElement objNewElm=null;
			int iHighLevel=0;
			try
			{
				iHighLevel= Conversion.ConvertStrToInteger(p_sHighLevel)+1;  
				objLowNode = (XmlElement)p_objXmlDoc.SelectSingleNode("//control[@name='OrgLowLevel']");
				objLowNode.InnerXml="";

				objNewElm=p_objXmlDoc.CreateElement("option");
				objNewElm.SetAttribute("value","0");
				objNewElm.InnerText = "None";
				objLowNode.AppendChild(objNewElm); 

				if(iHighLevel==1)
				{
					return p_objXmlDoc;
				}
				objRdr= DbFactory.GetDbReader(ConnectString,"SELECT TABLE_ID,SYSTEM_TABLE_NAME FROM GLOSSARY WHERE " + 
					"TABLE_ID BETWEEN " + iHighLevel + " AND 1012");
				while(objRdr.Read())
				{
					objNewElm=p_objXmlDoc.CreateElement("option");
					objNewElm.SetAttribute("value",objRdr.GetInt32("TABLE_ID").ToString());
					objNewElm.InnerText = UTILITY.CamelCase(objRdr.GetString("SYSTEM_TABLE_NAME"));
					objLowNode.AppendChild(objNewElm); 
				}
				return p_objXmlDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("FROIBatchPrinting.GetLowLevel.Err", m_iClientId),p_objEx);
			}
			finally
			{
				objNewElm=null;
				objLowNode=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
					objRdr=null;
				}				
			}
		}
	}
}