using System;
using System.Collections ;
using Riskmaster.Common; 
using Riskmaster.Db;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21st,Mar 2005
	///Purpose :   Discount Range List implementation form
	/// </summary>
	public class DiscountRangeList : CollectionBase
	{
		private string m_sConnectString = "";
		private int m_iDiscountRowid = 0;
		private bool m_bDataChanged = false;
        private int m_iClientId = 0;

		/// <summary>
		/// Stores the pParent value
		/// </summary>
		private Discount m_objParent = null;

		private const string TABLE_NAME = "SYS_POL_DISC_RANGE";

        public DiscountRangeList(string p_sConnectString, int p_iClientId)
		{
			m_sConnectString = p_sConnectString;
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Read property for Count
		/// </summary>
		internal int Count
		{
			get
			{
				return List.Count;
			}
		}

		/// <summary>
		/// Read property for Count
		/// </summary>
		internal int DiscountRowid
		{
			get
			{
				return m_iDiscountRowid;
			}
			set
			{
				m_iDiscountRowid = value;
				for(int i = 0; i < List.Count; i++)
				{
					((DiscountRange)List[i]).DiscountRowid = m_iDiscountRowid;
				}
			}
		}

		/// <summary>
		/// Read/Write property for pParent
		/// </summary>
		internal Discount Parent
		{
			//IMPORTANT DO NO SET PARENT PROPERTY FROM PARENT'S OBJECT INITIALIZE EVENT !!!!!!
			get
			{
				return m_objParent ;
			}
			set
			{
				m_objParent = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				if(m_bDataChanged)
					return m_bDataChanged;
 
				for(int i = 0; i < List.Count; i++)
				{
					if(((DiscountRange)List[i]).DataChanged == true)
					{
						m_bDataChanged = true;
						break;
					}

				}
				return m_bDataChanged;
			}
			set
			{
				for(int i = 0; i < List.Count; i++)
				{
					((DiscountRange)List[i]).DataChanged = value;
				}
				m_bDataChanged = value;
			}
		}

		/// <summary>
		/// Add an item in the list.
		/// </summary>
		/// <param name="p_objDiscountRange">Financial Summary Item</param>
		/// <returns>Position on which the item is inserted.</returns>
		internal int Add(DiscountRange p_objDiscountRange)
		{ 
			if(p_objDiscountRange.DiscountRangeRowId == 0)
			{
                p_objDiscountRange.DiscountRangeRowId = Common.Utilities.GetNextUID(m_sConnectString, TABLE_NAME, m_iClientId);
				p_objDiscountRange.NewRecord = true;
			}

			p_objDiscountRange.Parent = this;
			m_bDataChanged = true;
			return List.Add( p_objDiscountRange );			
		}		


		/// <summary>
		/// Remove the item form the list.
		/// </summary>
		/// <param name="p_objDiscountRange">Financial Summary Item</param>
		internal void Remove( DiscountRange p_objDiscountRange )
		{
			List.Remove( p_objDiscountRange );
		} 

		/// <summary>
		/// Check the existence of the item in the list.
		/// </summary>
		/// <param name="p_objDiscountRange">Financial Summary Item</param>
		/// <returns>Return the status of the item. "True" if item is in the list.</returns>
		internal bool Contains( DiscountRange p_objDiscountRange )
		{
			return List.Contains( p_objDiscountRange );
		}
		/// <summary>
		/// Get the index of the item in the list.
		/// </summary>
		/// <param name="p_objDiscountRange">Financial Summary Item</param>
		/// <returns>Return the integer index.</returns>
		internal int IndexOf( DiscountRange p_objDiscountRange )
		{
			return List.IndexOf( p_objDiscountRange );
		}

		/// <summary>
		/// Indexing for DiscountRange
		/// </summary>
		internal DiscountRange this[int p_iIndex]
		{
			get { return ( DiscountRange )List[p_iIndex]; }
			set { List[p_iIndex] = value; }
		}

		internal void LoadData()
		{

			DbReader objDbReader = null;
			DiscountRange objDiscountRange = null;

			try
			{
				//pmahli 10/3/2007 Sorted Disocunt Range list
                string sSQL = "SELECT * FROM " + TABLE_NAME + " WHERE DISCOUNT_ROWID=" + m_iDiscountRowid + "ORDER BY DISC_RANGE_ROWID";

				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						objDiscountRange = new DiscountRange();

						objDiscountRange.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objDiscountRange.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objDiscountRange.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objDiscountRange.DttmRcdLastUpd  = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objDiscountRange.Amount = Conversion.ConvertStrToDouble(objDbReader["AMOUNT"].ToString());
						objDiscountRange.BeginningRange = Conversion.ConvertStrToDouble(objDbReader["BEGINNING_RANGE"].ToString());
						objDiscountRange.DiscountRangeRowId = Conversion.ConvertStrToInteger(objDbReader["DISC_RANGE_ROWID"].ToString());
						objDiscountRange.DiscountRowid = Conversion.ConvertStrToInteger(objDbReader["DISCOUNT_ROWID"].ToString());
						objDiscountRange.EndRange = Conversion.ConvertStrToDouble(objDbReader["END_RANGE"].ToString());

						objDiscountRange.DataChanged = false;
						List.Add(objDiscountRange);
					}
				}

				m_bDataChanged = false;
			}
			finally
			{
				if(objDbReader != null)
					objDbReader.Dispose();
				if(objDiscountRange != null)
					objDiscountRange = null;
			}
		}

	}
}
