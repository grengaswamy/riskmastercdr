﻿using System;
using System.Xml;
using System.Data;
using System.Collections;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;




namespace Riskmaster.Application.RMUtilities
{
    public class AutoFroiAcord: UtilitiesBase 
    {

            /// <summary>
            /// Two dimensional array for the fields mapping
            /// </summary>
            private string[,] arrFields = {			    
			    {"RowId","ROW_ID"},						
			    {"Adjusters","ADJUSTERS"},									
                {"EmailFrom","EMAILFROIACORDFROM"},	
	        };

            int m_iClientId = 0;
            public AutoFroiAcord(string p_sConnString, int p_iClientId):base(p_sConnString, p_iClientId)
		    {
			    ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
			    this.Initialize(); 
		    }

            public AutoFroiAcord(string p_sConnString, string p_sUserName, int p_iClientId)
                : base(p_sConnString, p_sUserName, p_iClientId)
            {
                ConnectString = p_sConnString;
				m_iClientId = p_iClientId;
                this.Initialize();
            }

            /// <summary>
            /// Initialize the variables
            /// </summary>
            new private void Initialize()
            {
                TableName = "EMAIL_FROI_ACORD";
                KeyField = "ROW_ID";
                base.InitFields(arrFields);
                base.Initialize();
            }

            /// <summary>
            /// Get method returns the data
            /// </summary>
            /// <param name="p_objXmlDocument">Input xml structure document</param>
            /// <returns>Xml Document with Data</returns>
            /// <summary>            
            /// <param name="p_objXmlDocument">Input xml document with form structure</param>
            /// <returns>Xml document with data and structure</returns>            

            public XmlDocument Get(XmlDocument p_objDoc)
            {

                XmlElement objElm = null;              
                DbConnection objCon = null;              
                LocalCache objCache = null;
                XmlNode objNode = null;
                XmlNode objIncNode = null;
                DbReader objReader = null;                
                string sAdjusterList = string.Empty;                
                int iTableId = 0 ;
                string sEmailFROIACORD = string.Empty;
                XmlElement objElement=null;
                XmlElement objIncElement = null;			    
			    DataSet objDS=null;			    
			    StringBuilder sbSql=null;			    
			    XmlDocument objXmlDocument=null;

                try
                {
                    using (objCon = DbFactory.GetDbConnection(ConnectString))
                    {
                        objCon.Open();
                        objCache = new LocalCache(ConnectString,m_iClientId);
                        iTableId = objCache.GetTableId("ADJUSTERS");

                        objXmlDocument = new XmlDocument();
                        sbSql = new StringBuilder();
                        sbSql.Append("<Adjuster>");
                        sbSql.Append("<control name='ExcAdjusters' type='combobox' title='Excluded Adjusters:'> </control>");
                        sbSql.Append("<control name='IncAdjusters' type='combobox' title='Included Adjusters:'> </control>");
                        sbSql.Append("<control name='EmailFrom' type='text' title='Sender Email Address'/>");

                        //Devender Singh : March 30, 2010 : - Start
                        //MITS 20166 
                        //Added following check boxes to capture the flag for sending extra fields data
                        //in the body of the Email for ACORD and FROI
                        sbSql.Append("<control name='chkEmailAcord' type='checkbox' title='chk Email Acord'/>");
                        sbSql.Append("<control name='chkEmailFroi' type='checkbox' title='chk Email Froi'/>");
                        //Devender Singh : March 30, 2010 : - Start

                        sbSql.Append("</Adjuster>");
                        objXmlDocument.LoadXml(sbSql.ToString());

                        //sEmailFROIACORD = objCon.ExecuteString("SELECT EMAILFROIACORDFROM FROM EMAIL_FROI_ACORD");
                        //objNode = objXmlDocument.SelectSingleNode("//control[@name='EmailFrom']");
                        //if (objNode != null)
                        //{
                        //    objNode.InnerText = sEmailFROIACORD;
                        //}


                        // Dsingh50 ( march 30 2010 ) :- start custom Email fields update for Chubb 


                        //sEmailFROIACORD = 



                        //Devender Singh : March 30, 2010 :
                        //MITS 20166 
                        objDS = DbFactory.GetDataSet(ConnectString,
                            "SELECT EMAILFROIACORDFROM, USE_CUS_FLD_FROI_FLAG, USE_CUS_FLD_ACORD_FLAG FROM EMAIL_FROI_ACORD", m_iClientId);


                        //Commented the following lines : start

                        //DbReader objFroiReader;
                        //objFroiReader = objCon.ExecuteReader
                        //    ("SELECT EMAILFROIACORDFROM, IsCustomFieldFroiEnabled, IsCustomFieldAcordEnabled FROM EMAIL_FROI_ACORD");

                        //Devender Singh : March 30, 2010 
                        //MITS 20166 
                        //Commented the following lines : End

                        objNode = objXmlDocument.SelectSingleNode("//control[@name='EmailFrom']");
                        if (objNode != null)
                        {
                            if (objDS.Tables[0].Rows.Count > 0)
                            {
                                objNode.InnerText = objDS.Tables[0].Rows[0]["EMAILFROIACORDFROM"].ToString();
                            }
                            else
                            {
                                objNode.InnerText = "0";
                            }
                        }


                        //Devender Singh : March 30, 2010 : - Start
                        //MITS 20166 
                        //Reading and setting the values into the check boxes
                        objNode = objXmlDocument.SelectSingleNode("//control[@name='chkEmailFroi']");
                        if (objNode != null)
                        {
                            //rsolanki2: adding null check + refactoring 
                            if (objDS.Tables[0].Rows.Count > 0)
                            {
                                objNode.InnerText = (objDS.Tables[0].Rows[0]["USE_CUS_FLD_FROI_FLAG"].ToString() == "-1") ? "True" : "False";
                            }
                            else
                            {
                                objNode.InnerText = "False";
                            }

                            //if (objNode.InnerText == "-1")
                            //{
                            //    objNode.InnerText = "True";
                            //}
                            //else
                            //{
                            //    objNode.InnerText = "False";
                            //}
                        }

                        objNode = objXmlDocument.SelectSingleNode("//control[@name='chkEmailAcord']");
                        if (objNode != null)
                        {
                            //rsolanki2: adding null check + refactoring 
                            if (objDS.Tables[0].Rows.Count > 0)
                            {
                                objNode.InnerText = (objDS.Tables[0].Rows[0]["USE_CUS_FLD_ACORD_FLAG"].ToString() == "-1") ? "True" : "False";
                            }
                            else
                            {
                                objNode.InnerText = "False";
                            }

                            //objNode.InnerText = objDS.Tables[0].Rows[0]["USE_CUS_FLD_ACORD_FLAG"].ToString();
                            //if (objNode.InnerText == "-1")
                            //{
                            //    objNode.InnerText = "True";
                            //}
                            //else
                            //{
                            //    objNode.InnerText = "False";
                            //}
                        }

                        //Devender Singh : March 30, 2010 : - End
                        //MITS 20166 
                        //Reading and setting the values into the check boxes


                        objNode = objXmlDocument.SelectSingleNode("//control[@name='ExcAdjusters']");
                        objIncNode = objXmlDocument.SelectSingleNode("//control[@name='IncAdjusters']");


                        //cOMMENTED TO Fetch Adjusters from other table
                        //sAdjusterList = objCon.ExecuteString("SELECT ADJUSTERS FROM EMAIL_FROI_ACORD");

                        sbSql = new StringBuilder();
                        sbSql.Append("SELECT ADJUSTER_EID FROM ADJUSTER_FROI_ACORD,EMAIL_FROI_ACORD WHERE ADJUSTER_FROI_ACORD.ROW_ID = EMAIL_FROI_ACORD.ROW_ID");
                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                        foreach (DataRow dr in objDS.Tables[0].Rows)
                        {
                            sAdjusterList = sAdjusterList + "," + Conversion.ConvertObjToStr(dr["ADJUSTER_EID"]) + ",";
                        }
                        sbSql = new StringBuilder();

                        //Geeta : Mits 18443 for Auto Froi Acord
                        sbSql.Append("SELECT FIRST_NAME, LAST_NAME, ENTITY_ID FROM ENTITY WHERE ENTITY_TABLE_ID = " + iTableId + "AND DELETED_FLAG <> -1" + " ORDER BY LAST_NAME"); //Added deleted flag condition for Mits 18951
                        objDS = DbFactory.GetDataSet(ConnectString, sbSql.ToString(), m_iClientId);
                        foreach (DataRow dr in objDS.Tables[0].Rows)
                        {
                            if (sAdjusterList.Contains("," + (Conversion.ConvertObjToStr(dr["ENTITY_ID"])) + ","))
                            {
                                objIncElement = objXmlDocument.CreateElement("option");
                                objIncElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["ENTITY_ID"]));
                                objIncElement.InnerText = Conversion.ConvertObjToStr(dr["LAST_NAME"]) + "," + Conversion.ConvertObjToStr(dr["FIRST_NAME"]);
                                objIncNode.AppendChild(objIncElement);
                            }
                            else
                            {
                                objElement = objXmlDocument.CreateElement("option");
                                objElement.SetAttribute("value", Conversion.ConvertObjToStr(dr["ENTITY_ID"]));
                                objElement.InnerText = Conversion.ConvertObjToStr(dr["LAST_NAME"]) + "," + Conversion.ConvertObjToStr(dr["FIRST_NAME"]);
                                objNode.AppendChild(objElement);
                            }
                        }
                        objCon.Close();
                    }

                    return objXmlDocument;
                }
                catch (Exception p_objEx)
                {
                    throw new RMAppException(Globalization.GetString("AutoFroiAcord.Get.Error", m_iClientId), p_objEx);
                }
                finally
                {                    
                    if (objCache != null)
                        objCache.Dispose();                    
                    if (objReader != null)
                    {
                        objReader.Dispose();
                    }

                }
            }           

            /// Name		: Save
            /// Author		: Navdeep            
            ///************************************************************
            /// Amendment History
            ///************************************************************
            ///     Amendment   *    Author
            ///************************************************************
            /// <summary>
            /// Saves the xml document with data and the form structure
            /// </summary>
            /// <param name="p_objXmlDocument">Input xml document with data</param>
            /// <returns>Saved data</returns>
            public XmlDocument Save(XmlDocument p_objXmlDocument)
            {
                DbConnection objCon = null;
                XmlElement objElm = null;
                //Devender Singh : March 30, 2010 Commented the following lines
                //MITS 20166 
                //For providing two extra check boxes for Acord and Froi setting
                //These settings are for sending few fields data in the body of the 
                //the Email that is generated
                //start Devender
                XmlElement objElmAcord = null;
                XmlElement objElmFroi = null;
                //End Devender
                int iRowID = 0;
                string sSQL = string.Empty;
                string[] sAdjusterList;

                //Added by bsharma33 for PenTesting MITS 26932 
                System.Collections.Generic.Dictionary<string, string> dictParams = new System.Collections.Generic.Dictionary<string, string>();
                StringBuilder sbSQL = null;
                //End Changes by bsharma33 for PenTesting MITS 26932                              
                                                                                
                try
                {
                    
                    XMLDoc = p_objXmlDocument;
                    //Changed by bsharma33 for PenTesting MITS 26932
                    sbSQL = new StringBuilder();
                    
                    using (objCon = DbFactory.GetDbConnection(ConnectString))
                    {

                        objCon.Open();
                        
                        iRowID = Riskmaster.Common.Conversion.ConvertObjToInt(objCon.ExecuteScalar("SELECT ROW_ID FROM EMAIL_FROI_ACORD"), m_iClientId);
                   //Ended changes by bsharma33 for PenTesting MITS 26932
                        if (iRowID == 0) //new record
                        {
                            //Devender Singh : March 30, 2010  Start
                            //MITS 20166 
                            //For providing two extra check boxes for Acord and Froi setting
                            //These settings are for sending few fields data in the body of the 
                            //the Email that is generated
                            
                            //Commented the following limes : Start 
                            

                            ////Insert FROMEmail Address into EMAIL_FROI_ACORD table 
                            //objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EmailFrom']");
                            //if (objElm != null)
                            //    sSQL = "INSERT INTO EMAIL_FROI_ACORD (ROW_ID,EMAILFROIACORDFROM,ADJUSTERS) VALUES(1,'" + objElm.InnerText + "','";
                            //sSQL = sSQL + "1')";
                            //objCon.ExecuteNonQuery(sSQL);

                            //Commented the following limes : End 

                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EmailFrom']");
                            objElmAcord = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='chkEmailAcord']");
                            objElmFroi = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='chkEmailFroi']");

                            if (objElm != null && objElmAcord != null && objElmFroi != null)
                            {
                                 if (objElmAcord.InnerText.Equals("True", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    objElmAcord.InnerText="-1";
                                }
                                else
                                {
                                    objElmAcord.InnerText = "0";
                                }

                                if (objElmFroi.InnerText.Equals("True", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    objElmFroi.InnerText = "-1";
                                }
                                else
                                {
                                    objElmFroi.InnerText = "0";
                                }


                                //Changed by bsharma33 MITS 26932
                                //sSQL = "INSERT INTO EMAIL_FROI_ACORD (ROW_ID,EMAILFROIACORDFROM,ADJUSTERS,USE_CUS_FLD_ACORD_FLAG,USE_CUS_FLD_FROI_FLAG) VALUES(1,'" + objElm.InnerText + "','";
                                //sSQL = sSQL + "1'," + objElmAcord.InnerText + "," + objElmFroi.InnerText + ")";
                                sbSQL.Append(string.Format("INSERT INTO EMAIL_FROI_ACORD (ROW_ID,EMAILFROIACORDFROM,ADJUSTERS,USE_CUS_FLD_ACORD_FLAG,USE_CUS_FLD_FROI_FLAG) VALUES(1,{0},'1',{1},{2})", "~EMAIL_FROI_ACORD_FROM~", "~USE_CUS_FLD_ACORD_FLAG~", "~USE_CUS_FLD_FROI_FLAG~"));
                                dictParams.Add("EMAIL_FROI_ACORD_FROM", objElm.InnerText);
                                dictParams.Add("USE_CUS_FLD_ACORD_FLAG", objElmAcord.InnerText);
                                dictParams.Add("USE_CUS_FLD_FROI_FLAG", objElmFroi.InnerText);

                                DbFactory.ExecuteNonQuery(ConnectString, sbSQL.ToString(), dictParams);
                                
                                //objCon.ExecuteNonQuery(sSQL);
                                //End changes bsharma33 MITS 26932
                            }

                            //Devender Singh : March 30, 2010  End
                            //MITS 20166 
                            //For providing two extra check boxes for Acord and Froi setting
                            //These settings are for sending few fields data in the body of the 
                            //the Email that is generated


                        }
                        else //Update the existing record
                        {
                            //Changed by bsharma33 for PenTesting MITS 26932
                            //sSQL = "UPDATE EMAIL_FROI_ACORD SET EMAILFROIACORDFROM = '";
                        
                            sbSQL.Append(string.Format("UPDATE EMAIL_FROI_ACORD SET EMAILFROIACORDFROM ={0}", "~EMAIL_FROI_ACORD_FROM~"));
                            //End Changes by bsharma33 for PenTesting MITS 26932
                            objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='EmailFrom']");
                            
                            //Devender Singh : March 30, 2010 Start
                            //MITS 20166 
                            //For providing two extra check boxes for Acord and Froi setting
                            //These settings are for sending few fields data in the body of the 
                            //the Email that is generated

                            //Commented the following lines : Start
                            //if (objElm != null)
                            //{
                            //    objCon.ExecuteNonQuery(sSQL + objElm.InnerText + "', ADJUSTERS = '1'");
                            //}
                            //Commented the following lines : End
                           
                            if (objElm != null)
                            {
                                //Changed by bsharma33 for PenTesting MITS 26932
                                dictParams.Add("EMAIL_FROI_ACORD_FROM", objElm.InnerText);
                                //sSQL  = sSQL +  objElm.InnerText + "', ADJUSTERS = '1'";
                                sbSQL.Append(", ADJUSTERS='1' ");
                                //End changes by bsharma33 for PenTesting MITS 26932
                                
                            }
                            objElmAcord = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='chkEmailAcord']");
                            if (objElmAcord != null)
                            {
                                if (objElmAcord.InnerText.Equals("True", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    //Changed by bsharma33 for PenTesting MITS 26932
                                    //sSQL = sSQL + ",USE_CUS_FLD_ACORD_FLAG=-1";
                                    sbSQL.Append(",USE_CUS_FLD_ACORD_FLAG=-1");
                                    //End changes by bsharma33 for PenTesting MITS 26932
                                }
                                else
                                {
                                    //Changed by bsharma33 for PenTesting MITS 26932
                                    //sSQL = sSQL + ",USE_CUS_FLD_ACORD_FLAG=0";
                                    sbSQL.Append(",USE_CUS_FLD_ACORD_FLAG=0");
                                    //end changes by bsharma33 for PenTesting MITS 26932
                                }
                            }
                            objElmFroi = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='chkEmailFroi']");
                            if (objElmFroi != null)
                            {
                                if (objElmFroi.InnerText.Equals("True", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    //Changed by bsharma33 for PenTesting MITS 26932
                                    //sSQL = sSQL + ",USE_CUS_FLD_FROI_FLAG=-1";
                                    sbSQL.Append(",USE_CUS_FLD_FROI_FLAG=-1");
                                    //End changes by bsharma33 for PenTesting MITS 26932

                                }
                                else
                                {
                                    //Changed by bsharma33 for PenTesting MITS 26932
                                    //sSQL = sSQL + ",USE_CUS_FLD_FROI_FLAG=0";
                                    sbSQL.Append(",USE_CUS_FLD_FROI_FLAG=0");
                                    //Ehanged by bsharma33 for PenTesting MITS 26932
                                }
                            }
                            if (objElm != null && objElmAcord != null && objElmFroi != null)
                            {
                                //Changed by bsharma33 for PenTesting MITS 26932
                                DbFactory.ExecuteNonQuery(objCon.ConnectionString, sbSQL.ToString(), dictParams);
                                //objCon.ExecuteNonQuery(sSQL);
                                //End changes by bsharma33 for PenTesting MITS 26932
                            }

                            //Devender Singh : March 30, 2010 End
                            //MITS 20166 
                            //For providing two extra check boxes for Acord and Froi setting
                            //These settings are for sending few fields data in the body of the 
                            //the Email that is generated


                           

                        }
                        //Added by bsharma33 MITS 26932, clearing Insert/Update command from sbSQL and clearing the parameters from dictParams.
                        sbSQL.Remove(0, sbSQL.Length);
                        dictParams.Clear();
                        //End Additions by bsharma33 MITS 26932

                        //ADDING The adjuster EIDs value in table "ADJUSTER_FROI_ACORD"
                        objCon.ExecuteNonQuery("DELETE FROM ADJUSTER_FROI_ACORD");
                        objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='IncAdjusters']");
                        if (objElm != null)
                        {
                            objElm.InnerText = objElm.InnerText.Trim();
                            sAdjusterList = objElm.InnerText.Split(' ');
                            //Added by bsharma33 MITS 26932
                            if (sAdjusterList.Length > 0)
                            {
                                sbSQL.Append(string.Format("INSERT INTO ADJUSTER_FROI_ACORD VALUES(1, {0})", "~ADJUSTER_EID~"));
                                dictParams.Add("ADJUSTER_EID", "0");
                                
                            }
                            //End addition by bsharma33 MITS 26932
                            foreach (string sAdjusterEid in sAdjusterList)
                            {
                                if (!string.IsNullOrEmpty(sAdjusterEid))
                                {
                                    //Changed by bsharma33 for PenTesting MITS 26932
                                    //sSQL = "INSERT INTO ADJUSTER_FROI_ACORD VALUES(1, " + sAdjusterEid + " )";
                                    
                                   dictParams["ADJUSTER_EID"] = sAdjusterEid;
                                   DbFactory.ExecuteNonQuery(objCon.ConnectionString, sbSQL.ToString(), dictParams);
                                  //objCon.ExecuteNonQuery(sSQL);
                                   //End changes by bsharma33 for PenTesting MITS 26932
                                }
                            }
                        }

                    }
                        return XMLDoc;
                }
                catch (Exception p_objEx)
                {
                    throw new RMAppException(Globalization.GetString("AutoFroiAcord.Save.Error", m_iClientId), p_objEx);
                }
            }
    }
}
