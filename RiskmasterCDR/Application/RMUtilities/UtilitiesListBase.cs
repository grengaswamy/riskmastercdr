using System;
using System.Xml; 
using Riskmaster.Db; 
using System.Data;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag, Pankaj
	///Dated   :   1st,Mar 2005
	///Purpose :   Base Class for List Type Forms
	/// </summary>
	public class UtilitiesListBase
	{
		/// <summary>
		/// Key Field
		/// </summary>
		private string m_sKeyField="";

		/// <summary>
		/// Key Field Value
		/// </summary>
		private string m_sKeyFieldValue="";

		/// <summary>
		/// Table Name
		/// </summary>
		private string m_sTableName="";

		/// <summary>
		/// Radio Name
		/// </summary>
		private string m_sRadioName="";

		/// <summary>
		/// Where Clause
		/// </summary>
		private string m_sWhereClause="";

		/// <summary>
		/// Order By Clause
		/// </summary>
		private string m_sOrderByClause="";

		/// <summary>
		/// Xml Document
		/// </summary>
		private XmlDocument m_objXmlDoc=null;

		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnString="";

		/// <summary>
		/// Internal Datafield List variable
		/// </summary>
		internal DataFieldList m_FieldList=new DataFieldList();

		/// <summary>
		/// Child array variable
		/// </summary>
		protected string[,] m_arrChild =  new string[10,5];

        private int m_iClientId = 0;//Add by kuladeep for review defect.

		#region "Properties"

		/// <summary>
		/// TableName
		/// </summary>
		protected string TableName
		{
			get{return m_sTableName;}
			set{m_sTableName = value;}
		}

		/// <summary>
		/// KeyField
		/// </summary>
		protected string KeyField
		{
			get{return m_sKeyField;}
			set{m_sKeyField = value;}
		}

		/// <summary>
		/// KeyFieldValue
		/// </summary>
		protected string KeyFieldValue
		{
			get{return m_sKeyFieldValue;}
			set{m_sKeyFieldValue = value;}
		}

		/// <summary>
		/// RadioName
		/// </summary>
		protected string RadioName
		{
			get{return m_sRadioName;}
			set{m_sRadioName = value;}
		}

		/// <summary>
		/// WhereClause
		/// </summary>
		protected string WhereClause
		{
			get{return m_sWhereClause;}
			set{m_sWhereClause = value;}
		}

		/// <summary>
		/// OrderByClause
		/// </summary>
		protected string OrderByClause
		{
			get{return m_sOrderByClause;}
			set{m_sOrderByClause = value;}
		}

		/// <summary>
		/// XMLDoc
		/// </summary>
		protected XmlDocument XMLDoc
		{
			get{return m_objXmlDoc;}
			set{m_objXmlDoc = value;}
		}

		/// <summary>
		/// ConnectString
		/// </summary>
		internal string ConnectString
		{
			get{return m_sConnString;}
			set{m_sConnString = value;}
		}

		/// <summary>
		/// Child Array
		/// </summary>
		protected string[,] ChildArray
		{
			set{m_arrChild = value;}
		}

		#endregion

		/// Name		: UtilitiesListBase
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        internal UtilitiesListBase(string p_sConnString, int p_iClientId)//Add by kuladeep for review defect.
		{
            m_iClientId = p_iClientId;
			Initialize();
		}

		/// Name		: Initialize
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize
		/// </summary>
		protected void Initialize(){}

		/// Name		: InitFields
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Add all obect Fields into the Field Collection from our Field List.
		/// </summary>
		/// <param name="p_sFields">Fields Collection</param>
		protected virtual void InitFields(string[,] p_sFields)
		{ 
			for(int i =0; i< p_sFields.GetLength(0);i++)
			{
				this.m_FieldList[p_sFields[i,0]] = p_sFields[i,1];
			}
		}

		/// Name		: FieldsToSelect
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// FieldsToSelect
		/// </summary>
		/// <returns>string list</returns>
		private string FieldsToSelect()
		{
			string sFieldsToSelect="";

			foreach(string key in m_FieldList.Keys)
			{
				if( sFieldsToSelect=="")
                    sFieldsToSelect = m_FieldList[key].ToString() ;
				else
					sFieldsToSelect += "," + m_FieldList[key].ToString() ;
			}
			return sFieldsToSelect;
		}

		/// Name		: Get
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Base Get Implementation
		/// </summary>
		protected virtual void Get()
		{
			DataSet objDs = null;
			XmlElement objLstRow = null;
			XmlElement objRowTxt = null;
			XmlNode objNode = null;
			XmlNodeList objNodLst = null;
			XmlElement objElm =null;
			string sColumnName="";
			string sSQL = "";
			
			try
			{
				sSQL = String.Format("SELECT {0},{1} FROM {2}",m_sKeyField, FieldsToSelect() , m_sTableName);
				if (m_sWhereClause !=null && m_sWhereClause !="")
					sSQL += " WHERE " + m_sWhereClause;
				if (m_sOrderByClause !=null && m_sOrderByClause !="")
					sSQL += " ORDER BY " + m_sOrderByClause;
                objDs = DbFactory.GetDataSet(m_sConnString, sSQL,m_iClientId); //Add by kuladeep for review defect.

				objNode = m_objXmlDoc.SelectSingleNode("//" + m_sRadioName); 
				objNodLst  =m_objXmlDoc.SelectSingleNode("//listhead").ChildNodes; 
			
				foreach(DataRow objRow in objDs.Tables[0].Rows )
				{
					objLstRow = m_objXmlDoc.CreateElement("listrow");
					if( m_sKeyField.IndexOf('.') > 0)  
						sColumnName = (m_sKeyField.ToString().Split('.'))[1].ToString();
					else
						sColumnName = m_sKeyField.ToString();

					objRowTxt = m_objXmlDoc.CreateElement("RowId");
					objRowTxt.InnerText=objRow[sColumnName].ToString();
					objLstRow.AppendChild(objRowTxt);
					for(int i = 1; i< objNodLst.Count;i++)  
					{
						objElm = (XmlElement)objNodLst[i]; 
						objRowTxt = m_objXmlDoc.CreateElement(objNodLst[i].Name);
						if(objNodLst[i].Attributes["type"] != null)
						{
							if (objNodLst[i].Attributes["type"].Value.ToLower()=="date")
							{
								objRowTxt.InnerText=objRow[m_FieldList[objElm.Name].ToString()].ToString();
								objRowTxt.InnerText=UTILITY.FormatDate(objRowTxt.InnerText,false);
							}
						}
						else
							objRowTxt.InnerText=objRow[m_FieldList[objElm.Name].ToString()].ToString();
						objLstRow.AppendChild(objRowTxt);
					}
					objNode.AppendChild((XmlNode)objLstRow);
				}
				objDs.Dispose();
			}
			finally
			{
				if (objDs != null)
				{
					objDs.Dispose();
					objDs=null;
				}
				objLstRow = null;
				objRowTxt = null;
				objNode = null;
				objNodLst = null;
				objElm =null;
			}
		}

		/// Name		: Delete
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete method
		/// </summary>
		/// <returns>boolean success</returns>
		protected virtual bool Delete()
		{
			DbConnection objConn = null;
			string sSQL = "";
			try
			{
				sSQL = String.Format("DELETE FROM {0} WHERE {1} = {2}",m_sTableName,m_sKeyField,m_sKeyFieldValue);
				if (m_sWhereClause!="")
					sSQL += " AND " + m_sWhereClause;
				objConn = DbFactory.GetDbConnection(m_sConnString);
				objConn.Open(); 
				objConn.ExecuteNonQuery(sSQL); 
				objConn.Dispose();
				return true;
			}
			finally
			{
				if(objConn != null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}
	}	
}