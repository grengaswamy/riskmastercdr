using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Application.AdminTracking; 
using Riskmaster.DataModel;
using Riskmaster.Settings; //Rkatyal4: 7767

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   2 May 2005
	///Purpose :   This class is responsible for Creating, Deleting & saving the Views
	/// </summary>
	public class PVCreate
	{
	
		#region Private variables
        private string m_netViewFormsConnString = "";
        private string s_DataSourceId = "";
		private string m_sConnString = "";
		private string m_sUserId="";
		private string m_sPassword="";
		private string m_sDSNName="";
        private string sLangSelected = "1033";///Deb ML Changes
        private int  m_iClientId = 0;//mbahl3 JIRA [RMACLOUD-123]
        //Raman 07/20/2009: removing dependency on Riskmaster.Config

        private static string PV_XMLFILEPATH = UTILITY.GetAppFilesDirectory("PV");
        private static string PV_FORMLISTFILE = "";//rkaur27
        
		
        
        private static XmlDocument m_objXMLDocFormList = null;
        private static string m_sDefVwLst = string.Empty;
		const string XHTML_NAMESPACE="http://www.w3.org/1999/xhtml";
		#endregion

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserId">User Id</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDSNName">DSN Name</param>
		/// <param name="p_sConnString">Connection String</param>
        public PVCreate(string p_sUserId, string p_sPassword, string p_sDSNName, string p_sConnString, string p_sDataSourceId, int p_iClientId) //mbahl3 JIRA [RMACLOUD-123]
		{
			m_sDSNName = p_sDSNName;
			m_sUserId = p_sUserId;
			m_sPassword = p_sPassword;
			m_sConnString = p_sConnString;
            s_DataSourceId = p_sDataSourceId;
            m_iClientId = p_iClientId; //mbahl3 JIRA [RMACLOUD-123]
            m_netViewFormsConnString = RMConfigurationManager.GetConnectionString("ViewDataSource", m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
            PV_FORMLISTFILE = UTILITY.GetConfigUtilitySettings(m_sConnString, m_iClientId)["PV_FormListFile"];//rkaur27
		}

		#endregion

		#region Public Functions
		/// Name		: Get
		/// Author		: Anurag Agarwal
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <param name="p_objXMLDocument">Blank XML structure for the View UI</param>
		/// <returns>XmlDocument containing data to be shown on screen</returns>
        public XmlDocument Get(XmlDocument p_objXMLDocument) 
		{
			StringBuilder sbSql=null;
			DbReader objReader=null;
			XmlElement objSelectElement=null;
			XmlElement objRowTxt=null;
			ADMForms objADMForms = null;
			XmlDocument objXMLDocADMTables = null; 
			XmlNodeList objNodeList = null;
			int iViewId = 0;
			string sHomePage = "";
            //Deb Multi Language
            XmlElement objLangElm = null;
            XmlElement objNewElm = null;
            string sLangSelected = "1033";
            Riskmaster.Settings.SysSettings objSysSettings = null; //Rkatyal4: 7767
            objSysSettings = new Settings.SysSettings(m_sConnString, m_iClientId);//Rkatyal: 7767
            //Deb Multi Language
			try
			{
                //rkatyal4: 7767 start
                if (objSysSettings.UseEntityRole)
                {
                    string[] nodes = { "employee", "driver", "patient", "people", "physician", "staff" };
                    foreach (string node in nodes)
                    {
                        XmlNode RemNode = p_objXMLDocument.SelectSingleNode("//option[@homeSysName='" + node +"']");
                        RemNode.ParentNode.RemoveChild(RemNode);
                    }
                }
                //rkatyal4: 7767 end
				sbSql = new StringBuilder();

				iViewId = Conversion.ConvertStrToInteger(p_objXMLDocument.SelectSingleNode("//PVList/RowId").InnerText);
				if(iViewId != 0)
				{
					sbSql.Append("SELECT * FROM NET_VIEWS WHERE VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID= '" + s_DataSourceId+ "'");
                    using (objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString()))
                    {
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                p_objXMLDocument.SelectSingleNode("//PVCreate/ViewName").InnerText = objReader["VIEW_NAME"].ToString();
                                p_objXMLDocument.SelectSingleNode("//PVCreate/ViewDesc").InnerText = objReader["VIEW_DESC"].ToString();
                                sHomePage = objReader["HOME_PAGE"].ToString();
                                p_objXMLDocument.SelectSingleNode("//PVCreate/HomePageURL").InnerText = objReader["HOME_PAGE"].ToString();
                                sLangSelected = objReader["LANGUAGE_CODE"].ToString();
                            }                     
                        }
                    }
				}
				else
				{
                    p_objXMLDocument.SelectSingleNode("//PVCreate/ViewName").InnerText = "";
                    p_objXMLDocument.SelectSingleNode("//PVCreate/ViewDesc").InnerText = "";
                    p_objXMLDocument.SelectSingleNode("//PVCreate/HomePageURL").InnerText = "";							
				}

				objADMForms = new ADMForms(m_sDSNName, m_sUserId, m_sPassword, m_iClientId); //mbahl3 JIRA [RMACLOUD-123]
				objXMLDocADMTables = objADMForms.GetADMTables();
				objSelectElement = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='HomePage']"); 
				objNodeList = objXMLDocADMTables.GetElementsByTagName("AdmTrackingTable"); 

				foreach(XmlElement objADmTEle in objNodeList)
				{	
					objRowTxt = p_objXMLDocument.CreateElement("option");
					objRowTxt.SetAttribute("pagemenu","Admin");		
                    //Geeta 11/17/2006 : Modified for fixing mits no.8087
                    //changed by Nitin on 105/19/2009 
                    objRowTxt.SetAttribute("value", "UI/FDM/admintrackinglist.aspx?SysViewType=controlsonly");
                    objRowTxt.SetAttribute("homeSysName", "admintrackinglist|" + objADmTEle.SelectSingleNode("TableName").InnerText);
					objRowTxt.InnerText = objADmTEle.SelectSingleNode("TableName").InnerText;
					objSelectElement.AppendChild((XmlNode)objRowTxt);
				}
				
				if(iViewId != 0)
				{
					objNodeList = objSelectElement.GetElementsByTagName("option");
					foreach(XmlElement objADmTEle in objNodeList)
					{
						if(objADmTEle.GetAttribute("value").Trim() == sHomePage.Trim())
						{
							objADmTEle.SetAttribute("selected","1"); 
						}
					}
				}
                //Deb Multi Language
                if(sbSql.Length != 0)
                 sbSql = sbSql.Remove(0, sbSql.Length);
                sbSql = sbSql.Append("SELECT LANG_ID,LANGUAGE_NAME FROM SYS_LANGUAGES WHERE SUPPORTED_FLAG = -1 ORDER BY LANGUAGE_NAME");
                objLangElm = (XmlElement)p_objXMLDocument.SelectSingleNode("//control[@name='language']");
                objLangElm.Attributes["value"].Value = sLangSelected;
                if (objLangElm != null)
                {
                    using (objReader = DbFactory.GetDbReader(SecurityDatabase.GetSecurityDsn(m_iClientId), sbSql.ToString())) //mbahl3 JIRA [RMACLOUD-123]
                    {
                        while (objReader.Read())
                        {
                            objNewElm = p_objXMLDocument.CreateElement("option");
                            objNewElm.SetAttribute("value", objReader.GetInt32("LANG_ID").ToString());
                            objNewElm.InnerText = objReader.GetString("LANGUAGE_NAME");
                            objLangElm.AppendChild(objNewElm);
                            objNewElm = null;
                        }
                    }
                }
                //Deb Multi Language
				return p_objXMLDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVCreate.Get.Error",m_iClientId),p_objEx); //mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{				
				p_objXMLDocument=null;
				objSelectElement=null;
				objRowTxt=null;
				if(objADMForms != null)
					objADMForms.Dispose();
				objXMLDocADMTables = null;
				objNodeList = null;
                sbSql = null;
                if(objReader!=null)
                    objReader.Dispose();

			}
		}
		/// Name		: Save
		/// Author		: Anurag Agarwal
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the selected View information into the database
		/// </summary>
		/// <param name="p_objXMLDocument">XmlDocument containing Updated or new view info</param>
		/// <returns>boolean for success/failure</returns>
		public bool Save(XmlDocument p_objXMLDocument)
		{
			DbConnection objDbConnection=null;
			StringBuilder sbSql=null;
			int iViewId = 0;
			string sViewName = "";
			string sViewDesc = "";
			string sHomePage = "";
			string sHomePageURL = "";
			string sPageMenu = "";
            bool bCloneBaseView = false;
			try
			{
				sbSql=new StringBuilder();  
				iViewId = Conversion.ConvertStrToInteger(p_objXMLDocument.SelectSingleNode("//PVList/RowId").InnerText);
                sViewName = System.Web.HttpUtility.HtmlDecode(p_objXMLDocument.SelectSingleNode("//PVCreate/ViewName").InnerText);   //Aman MITS 27767                                      
				sViewDesc = p_objXMLDocument.SelectSingleNode("//PVCreate/ViewDesc").InnerText;
				sHomePage = p_objXMLDocument.SelectSingleNode("//PVCreate/HomePage").InnerText;
				sHomePageURL = p_objXMLDocument.SelectSingleNode("//PVCreate/HomePageURL").InnerText;
				sPageMenu = p_objXMLDocument.SelectSingleNode("//PVCreate/PageMenu").InnerText;
                bCloneBaseView = Common.Conversion.ConvertStrToBool(p_objXMLDocument.SelectSingleNode("//PVCreate/CloneBaseView").InnerText);
                sLangSelected = p_objXMLDocument.SelectSingleNode("//control[@name='HomePage']").Attributes["value"].Value; //Deb ML changes
				if(iViewId == 0)
				{
					//MITS-7687,changed by Shruti on 29th Aug 2006 -- starts
					string sQuery = "SELECT VIEW_NAME FROM NET_VIEWS WHERE VIEW_NAME = "+ Utilities.FormatSqlFieldValue(sViewName)+ " AND DATA_SOURCE_ID='" + s_DataSourceId + "'" ;
					
                    objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
					if(objDbConnection != null)
						objDbConnection.Open();
					Object objDuplicate =  objDbConnection.ExecuteScalar(sQuery);					
					

					if(objDuplicate == null)
					{
					//MITS-7687,changed by Shruti on 29th Aug 2006 -- ends
                        iViewId = Utilities.GetNextUID(m_sConnString, "NET_VIEWS", m_iClientId);
                        sbSql.Append("INSERT INTO NET_VIEWS(VIEW_ID,VIEW_NAME,VIEW_DESC,HOME_PAGE,DATA_SOURCE_ID,LANGUAGE_CODE) ");//Deb ML changes
						sbSql.Append("VALUES(" + iViewId + "," + Utilities.FormatSqlFieldValue(sViewName) + ",");
                        sbSql.Append(Utilities.FormatSqlFieldValue(sViewDesc) + "," + Utilities.FormatSqlFieldValue(sHomePageURL) + ",'" + s_DataSourceId + "'," + sLangSelected + ")");//Deb ML changes
						//Insert default views for non-pv forms
						InsertDefaultViews(iViewId);
					//MITS-7687,changed by Shruti on 29th Aug 2006 -- starts
					}
					else
					{
						throw new RMAppException("You cannot create duplicate Power Views.");
					}
					//MITS-7687,changed by Shruti on 29th Aug 2006 -- ends
				}
				else
				{
                    //Start -Added by Abhishek for duplicate power view check
                    string sQuery = "SELECT * FROM NET_VIEWS WHERE VIEW_ID != '" + iViewId + "' AND VIEW_NAME = " + Utilities.FormatSqlFieldValue(sViewName) + " AND DATA_SOURCE_ID='" + s_DataSourceId + "'";
                    objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
                    if (objDbConnection != null)
                        objDbConnection.Open();
                    Object objDuplicate = objDbConnection.ExecuteScalar(sQuery);

                    if (objDuplicate == null)
                    {
                        sbSql.Append("UPDATE NET_VIEWS SET VIEW_NAME=" + Utilities.FormatSqlFieldValue(sViewName) + ", VIEW_DESC=");
                        sbSql.Append(Utilities.FormatSqlFieldValue(sViewDesc) + ", HOME_PAGE=" + Utilities.FormatSqlFieldValue(sHomePageURL));
                        sbSql.Append(", PAGE_MENU=" + Utilities.FormatSqlFieldValue(sPageMenu) + ", LANGUAGE_CODE=" + sLangSelected + " WHERE VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID = '" + s_DataSourceId + "'");//Deb ML changes

                        //MITS-7687,changed by Shruti on 29th Aug 2006 -- starts
                        objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
                        if (objDbConnection != null)
                            objDbConnection.Open();
                    }
                    else
                    {
                        throw new RMAppException("You cannot create duplicate Power Views.");
                    }
                    //End - Added by Abhishek for duplicate power view check
                    				
					//MITS-7687,changed by Shruti on 29th Aug 2006 -- ends
				}
				objDbConnection.ExecuteNonQuery(sbSql.ToString());
				objDbConnection.Close();

                if (bCloneBaseView == true)
                    CloneBaseView(iViewId);

				return true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVCreate.Save.Error", m_iClientId),p_objEx); // mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{
				if (objDbConnection != null)
				{
					objDbConnection.Dispose();
				}
				p_objXMLDocument=null;
				sbSql=null;
			}
		}

        /// <summary>
        /// Clones all base views for the newly created power view
        /// </summary>
        /// <param name="p_iViewId"></param>
        private void CloneBaseView(int p_iViewId)
        {
            XmlDocument objXMLDocFormList = null;
            XmlDocument objXMLFormDoc = null;
            XmlNodeList objXmlNodeList = null;
            XmlElement objElement = null;
            StringBuilder sbSql = null;
            DbConnection objDbConnection = null;
            ArrayList arrForms = null;
            DbCommand objCmd = null;
            DbParameter objParam = null;
            DbParameter objParamAspx = null;
            DbReader objReader = null;            
            string sViewXML =  string.Empty;
            string sFormCaption = string.Empty;
            string sFormName = string.Empty;
            //Added by Shivendu for R5
            string sAspxPageContent = "";
            StringBuilder sViewAspx = new StringBuilder("");


            try
            {
                arrForms = new ArrayList();
                sbSql = new StringBuilder();
                objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
                if (objDbConnection != null)
                    objDbConnection.Open();

                objXMLDocFormList = GetFormList();
                objXmlNodeList = objXMLDocFormList.SelectNodes("//form");

                foreach(XmlNode objNode in objXmlNodeList)
                {                 
                    objElement = (XmlElement)objNode;
                    sFormCaption = objElement.GetAttribute("file").Replace("%", "|");
                    sFormCaption = sFormCaption.Replace("'","''");
                    sFormName = objElement.GetAttribute("name").Replace("'", "''");
                    arrForms.Add(sFormCaption);
                    sbSql.Remove(0, sbSql.Length);
                    sbSql.Append("INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,DATA_SOURCE_ID) VALUES(");
                    sbSql.Append(p_iViewId + ",'" + sFormCaption + "'," +
                                 objElement.GetAttribute("toplevel") + ",'" + sFormName + "','" + s_DataSourceId + "')");
                    objDbConnection.ExecuteNonQuery(sbSql.ToString());       
                }

                for (int i = 0; i < arrForms.Count; i++)
                {
                    if (((string)arrForms[i]).StartsWith("admintracking|"))
                    {
                        //Commented by Shivendu for R5
                        //PVFormEdit pv = new PVFormEdit(m_sUserId, m_sPassword, m_sDSNName, m_sDSNName, m_netViewFormsConnString);
                        //objXMLFormDoc = pv.GetDefaultAdmView((string)arrForms[i]);
                        ////Added by Shivendu for R5
                        //ComponentPowerViewUpgrade.Classes.PowerViewUpgrade pvUpgrade = new ComponentPowerViewUpgrade.Classes.PowerViewUpgrade();
                        //pvUpgrade.UpgradeXmlToAspx(false, false, objXMLFormDoc, arrForms[i].ToString(), out sViewAspx);
                        //sAspxPageContent = sViewAspx.ToString();
                        objXMLFormDoc = new XmlDocument();

                        sbSql.Remove(0, sbSql.Length);

                        sbSql.Append("SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND LOWER(FORM_NAME)='");
                        sbSql.Append(arrForms[i].ToString().ToLower() );
                        sbSql.Append("' AND DATA_SOURCE_ID = '");
                        sbSql.Append(s_DataSourceId );
                        sbSql.Append("'");
                        objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                sViewXML = objReader["VIEW_XML"].ToString();
                                sAspxPageContent = objReader["PAGE_CONTENT"].ToString();
                            }
                            if (objReader != null)
                                objReader.Close();
                        }

                        objXMLFormDoc.LoadXml(sViewXML);
                        if (objXMLFormDoc.OuterXml == "")
                            throw new RMAppException(Globalization.GetString("PVDefination.Save.FormXMLNotFound", m_iClientId) + arrForms[i]); //mbahl3 JIRA [RMACLOUD-123]
                                               
                    }

                    else
                    {
                        objXMLFormDoc = new XmlDocument();

                        //sbSql.Remove(0, sbSql.Length);

                        //rsolanki2: extensibility updates 

                        sbSql.Length = 0;
                        sbSql.Append( string.Concat(
                            "SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 AND LOWER(FORM_NAME) ='"
                            , arrForms[i].ToString().ToLower() 
                            ,"' AND DATA_SOURCE_ID='"
                            , s_DataSourceId
                            , "' "
                            , "UNION ALL SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE DATA_SOURCE_ID = '"
                            , s_DataSourceId
                            , "' "
                            , "AND VIEW_ID = 0 AND LOWER(FORM_NAME) ='"
                            , arrForms[i].ToString().ToLower() 
                            , "' AND"
                            , " FORM_NAME NOT IN "
                            , "(SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 AND DATA_SOURCE_ID='"
                            , s_DataSourceId
                            , "')"
                        ));

                        //sbSql.Append("SELECT VIEW_XML,PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID = 0 AND LOWER(FORM_NAME)='" 
                        //    + arrForms[i].ToString().ToLower() 
                        //    + "' AND DATA_SOURCE_ID = '"
                        //    + s_DataSourceId 
                        //    + "'");
                        objReader = DbFactory.GetDbReader(m_netViewFormsConnString, sbSql.ToString());
                        if (objReader != null)
                        {
                            if (objReader.Read())
                            {
                                sViewXML = objReader["VIEW_XML"].ToString();
                                sAspxPageContent = objReader["PAGE_CONTENT"].ToString();
                            }
                            if (objReader != null)
                                objReader.Close();
                        }

                        objXMLFormDoc.LoadXml(sViewXML);
                        if (objXMLFormDoc.OuterXml == "")
                            throw new RMAppException(Globalization.GetString("PVDefination.Save.FormXMLNotFound", m_iClientId) + arrForms[i]); //mbahl3  JIRA [RMACLOUD-123]

                        UTILITY.AddSuppDefinition(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objXMLFormDoc,m_iClientId); //mbahl3 JIRA [RMACLOUD-123]

                    }
                    //Deb ML Changes
                    if (sLangSelected == "1033" || ((string)arrForms[i]).StartsWith("admintracking|"))
                    {
                        //Add view id attrib to the form element 
                        ((XmlElement)objXMLFormDoc.SelectSingleNode("//form")).SetAttribute("viewid", p_iViewId.ToString());
                        sViewXML = objXMLFormDoc.OuterXml; // no need if parameterized    .Replace("'","''");
                        sbSql.Remove(0, sbSql.Length);
                        objCmd = objDbConnection.CreateCommand();
                        objParam = objCmd.CreateParameter();
                        objParamAspx = objCmd.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParamAspx.Direction = ParameterDirection.Input;
                        objParamAspx.Value = sAspxPageContent;
                        objParam.Value = sViewXML;
                        objParam.ParameterName = "XML";
                        objParamAspx.ParameterName = "ASPX";
                        objParam.SourceColumn = "VIEW_XML";
                        objParamAspx.SourceColumn = "PAGE_CONTENT";
                        objCmd.Parameters.Add(objParam);
                        objCmd.Parameters.Add(objParamAspx);
                        //ybhaskar : MITS 17115 : There is no pipe in the aspx page name .
                        string strAspxPageName = arrForms[i].ToString().Replace("|", "");
                        //ybhaskar : END
                        sbSql.Append("UPDATE NET_VIEW_FORMS SET VIEW_XML=~XML~,PAGE_CONTENT=~ASPX~,PAGE_NAME = '"
                            + strAspxPageName.Replace("xml", "aspx")
                            + "',LAST_UPDATED = '"
                            + System.DateTime.Now.ToString("yyyyMMddHHmmss")
                            + "' WHERE VIEW_ID="
                            + p_iViewId
                            + " AND LOWER(FORM_NAME) = '"
                            + arrForms[i].ToString().ToLower()
                            + "'"
                            + " AND DATA_SOURCE_ID = ' "
                            + s_DataSourceId
                            + "'");
                        objCmd.CommandText = sbSql.ToString();
                        objCmd.ExecuteNonQuery();
                    }
                    else
                    {
                        PVDefination objDef = new PVDefination(m_sUserId, m_sPassword, m_sDSNName, s_DataSourceId, m_sConnString, m_iClientId);//rkaur27
                        objDef.UpdatePVByLanguage(arrForms[i].ToString().ToLower().Substring(0, arrForms[i].ToString().ToLower().IndexOf(".xml")), p_iViewId, ".xml",true);
                    }
                    //Deb ML Changes
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
              throw new RMAppException(Globalization.GetString("PVCreate.Clone.Error", m_iClientId), p_objEx); //mbahl3 JIRA [RMACLOUD-123]
            }
            finally
            {
                if(objDbConnection != null)
                    objDbConnection.Close();
                if (objReader != null)
                    objReader.Close();             
                objXMLDocFormList = null;
                objXMLFormDoc = null;
                objXmlNodeList = null;
                objElement = null;
            }            
        }



        /// <summary>
        /// Retrievs the Form list from xml file and ADM form list by calling ADM component
        /// </summary>
        /// <returns>XML Doc containing all forms listing</returns>
        private XmlDocument GetFormList()
        {
            string sFileName = "";
            //RMConfigurator objConfig = null;
            ADMForms objADMForms = null;
            XmlDocument objXMLDocFormList = null;
            XmlElement objSelectElement = null;
            XmlElement objRowTxt = null;
            DbReader objReader = null;
            DataModelFactory objDmf = null;
            ADMTableList objAdmLst = null;
            ArrayList arrAdm = null;
            string[] retItem = new string[3];
            try
            {
                //objConfig = new RMConfigurator();
                objXMLDocFormList = new XmlDocument();

                //Raman 07/20/2009: removing dependency on Riskmaster.Config

                //sFileName = objConfig.GetValue(PV_XMLFILEPATH) + @"\" + objConfig.GetValue(PV_FORMLISTFILE);
                sFileName = PV_XMLFILEPATH + @"\" + PV_FORMLISTFILE;
                if (sFileName != "")
                {
                    if (System.IO.File.Exists(sFileName))
                        objXMLDocFormList.LoadXml(UTILITY.GetXMLFromFile(sFileName));
                    else
                        throw new RMAppException(Globalization.GetString("PVDefination.GetFormList.FormListNotFound", m_iClientId)); //mbahl3 JIRA [RMACLOUD-123]
                }
                else
                    throw new RMAppException(Globalization.GetString("PVDefination.GetFormList.FormListNotFound", m_iClientId)); //mbahl3 JIRA [RMACLOUD-123]

                objSelectElement = (XmlElement)objXMLDocFormList.SelectSingleNode("//forms");
        
                //Add the admin tracking tables.
                objDmf = new DataModelFactory(m_sDSNName, m_sUserId, m_sPassword, m_iClientId);//mbahl3  JIRA [RMACLOUD-123]
                objAdmLst = (ADMTableList)objDmf.GetDataModelObject("ADMTableList", false);
                arrAdm = objAdmLst.TableUserNames();

                foreach (string[] arrItem in arrAdm)
                {
                    objRowTxt = objXMLDocFormList.CreateElement("form");
                    objRowTxt.SetAttribute("name", arrItem[1]);
                    objRowTxt.SetAttribute("file", "admintracking" + "%" + arrItem[0] + ".xml");
                    objRowTxt.SetAttribute("toplevel", "1");
                    objSelectElement.AppendChild((XmlNode)objRowTxt);
                }

                return objXMLDocFormList;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objADMForms != null)
                    objADMForms.Dispose();
                objXMLDocFormList = null;
                objSelectElement = null;
                objRowTxt = null;
                if (objDmf != null)
                    objDmf.Dispose();
            }
        }
                

		
		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <param name="p_objXMLDocument">XML Document containing the View Id to be deleted</param>
		/// <returns>boolean for success/failure</returns>
		public bool Delete(XmlDocument p_objXMLDocument)
		{
			DbConnection objDbConnection=null;
			StringBuilder sbSql=null;
			int iViewId = 0;

			try
			{
				sbSql = new StringBuilder();

				iViewId = Conversion.ConvertStrToInteger(p_objXMLDocument.SelectSingleNode("//PVList/RowId").InnerText);
				if(iViewId != 0)
				{
					objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
					if(objDbConnection != null)
						objDbConnection.Open();
					
					sbSql.Append("DELETE FROM NET_VIEWS WHERE VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID = '" + s_DataSourceId + "'");
					objDbConnection.ExecuteNonQuery(sbSql.ToString());
					
					sbSql.Remove(0,sbSql.Length);
                    sbSql.Append("DELETE FROM NET_VIEW_FORMS WHERE VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID = '" + s_DataSourceId + "'");
					objDbConnection.ExecuteNonQuery(sbSql.ToString());

					sbSql.Remove(0,sbSql.Length);
                    sbSql.Append("DELETE FROM NET_VIEWS_MEMBERS WHERE VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID = '" + s_DataSourceId + "'");
					objDbConnection.ExecuteNonQuery(sbSql.ToString());
				}
				else
					return false;

				return true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVCreate.Delete.Error", m_iClientId),p_objEx); //mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{
                if (objDbConnection != null)
                {
                    objDbConnection.Dispose();
                }
				p_objXMLDocument=null;
				sbSql=null;
			}
		}

		/// Name		: Clone
		/// Author		: Anurag Agarwal
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Retrieves data to load on to the screen
		/// </summary>
		/// <param name="p_objXMLDocument">XML Document containing the View Id to be cloned</param>
		/// <returns>boolean for success/failure</returns>
		public bool Clone(XmlDocument p_objXMLDocument)
		{
			DbConnection objDbConnection=null;
			StringBuilder sbSql=null;
			DbReader objDbReader = null;
            //Added by Shivendu for MITS 8180----Start-----
            DbCommand objCmd = null;
            DbParameter objParam = null;
            DbParameter objParamAspx = null;
            string sSql = "";
            //Added by Shivendu for MITS 8180----End-------
			int iViewId = 0;
			int iNewViewId = 0;
			string sViewName = "";
			string sViewXML = "";
            //Added by Shivendu for R5
            StringBuilder sViewAspx = new StringBuilder("");
			XmlDocument objXml=null;
			XmlNamespaceManager nsNamespace = null;
			try
			{
				sbSql = new StringBuilder();
				objXml = new XmlDocument(); 

				iViewId = Conversion.ConvertStrToInteger(p_objXMLDocument.SelectSingleNode("//PVList/RowId").InnerText);
				sViewName = p_objXMLDocument.SelectSingleNode("//PVList/PVName").InnerText;
				if(sViewName.Trim() == "")
					sViewName = "Copy of View " + iViewId;
				else
					sViewName = "Copy of " + sViewName;

				if(iViewId != 0)
				{
                    iNewViewId = Utilities.GetNextUID(m_sConnString, "NET_VIEWS", m_iClientId);

                    objDbConnection = DbFactory.GetDbConnection(m_netViewFormsConnString);
					if(objDbConnection != null)
						objDbConnection.Open();
					
					sbSql.Append("INSERT INTO NET_VIEWS(VIEW_ID,VIEW_NAME,VIEW_DESC,HOME_PAGE, DATA_SOURCE_ID) SELECT ");
                   //Parijat: changes done for issue 9008 -where apostrophe was handled in the querry
                    sbSql.Append(iNewViewId + "," + Utilities.FormatSqlFieldValue(sViewName) + ",VIEW_DESC,HOME_PAGE, DATA_SOURCE_ID FROM NET_VIEWS ");
					sbSql.Append("WHERE NET_VIEWS.VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID = ' " + s_DataSourceId + "'");
					objDbConnection.ExecuteNonQuery(sbSql.ToString());
					
					sbSql.Remove(0,sbSql.Length);
                    sbSql.Append("INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,DATA_SOURCE_ID) SELECT ");
                    sbSql.Append(iNewViewId + ",FORM_NAME, TOPLEVEL,CAPTION,DATA_SOURCE_ID FROM NET_VIEW_FORMS WHERE VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID = ' " + s_DataSourceId + "'");
					objDbConnection.ExecuteNonQuery(sbSql.ToString());

					//grab the xml to clone from old view id
					sbSql.Remove(0,sbSql.Length);
                    sbSql.Append("SELECT FORM_NAME, VIEW_XML, PAGE_CONTENT FROM NET_VIEW_FORMS WHERE VIEW_ID=" + iViewId + " AND DATA_SOURCE_ID = ' " + s_DataSourceId + "'");
					objDbReader = DbFactory.GetDbReader(m_netViewFormsConnString,sbSql.ToString());
					if(objDbReader != null)
					{
                        

						while(objDbReader.Read())
						{
							
							sViewXML = objDbReader["VIEW_XML"].ToString(); 
							//insert xml into new view id
							objXml.LoadXml(sViewXML);
							XmlNode objNod = objXml.SelectSingleNode("//form");
							if(objNod==null)
							{
								nsNamespace = new XmlNamespaceManager(objXml.NameTable);
								nsNamespace.AddNamespace("xhtml", XHTML_NAMESPACE);
								//Try the xhtml:body for the other list kind of forms
								objNod = objXml.SelectSingleNode("//xhtml:body",nsNamespace);
							}
							if(objNod!=null)
								((XmlElement)objNod).SetAttribute("viewid",iNewViewId.ToString()); 
							
                            //Four lines below are commented by Shivendu for MITS 8180
                            //sbSql.Remove(0, sbSql.Length);
                            //sViewXML = Common.Utilities.FormatSqlFieldValue(objXml.OuterXml); 
							//sbSql.Append("UPDATE NET_VIEW_FORMS SET VIEW_XML=" + sViewXML + " WHERE VIEW_ID=" + iNewViewId + " AND FORM_NAME='" + objDbReader["FORM_NAME"].ToString() + "'" );
                            //objDbConnection.ExecuteNonQuery(sbSql.ToString());

                            //Added by Shivendu for R5
                            //ComponentPowerViewUpgrade.Classes.PowerViewUpgrade pvUpgrade = new ComponentPowerViewUpgrade.Classes.PowerViewUpgrade();
                            //pvUpgrade.UpgradeXmlToAspx(false, false, objXml, objDbReader["FORM_NAME"].ToString(), out sViewAspx);
                            //Added by Shivendu for MITS 8180----Start-----
                            objCmd = objDbConnection.CreateCommand();
                            objParam = objCmd.CreateParameter();
                            objParamAspx = objCmd.CreateParameter();
                            objParam.Direction = ParameterDirection.Input;
                            objParamAspx.Direction = ParameterDirection.Input;
                            objParam.Value = objXml.OuterXml;
                            objParamAspx.Value = objDbReader["PAGE_CONTENT"].ToString(); ;
                            objParam.ParameterName = "XML";
                            objParamAspx.ParameterName = "ASPX";
                            objParam.SourceColumn = "VIEW_XML";
                            objParamAspx.SourceColumn = "PAGE_CONTENT";
                            objCmd.Parameters.Add(objParam);
                            objCmd.Parameters.Add(objParamAspx);
                            //MITS 20277.Added Replace("|","") in form name as the admin form name was getting stored with a |.
                            sSql = "UPDATE NET_VIEW_FORMS SET VIEW_XML= ~XML~,PAGE_CONTENT=~ASPX~,PAGE_NAME = '" 
                                + objDbReader["FORM_NAME"].ToString().Replace("xml", "aspx").Replace("|","").ToLower() 
                                + "',LAST_UPDATED = '" 
                                + System.DateTime.Now.ToString("yyyyMMddHHmmss")
                                + "' WHERE VIEW_ID=" 
                                + iNewViewId 
                                + " AND FORM_NAME='" 
                                + objDbReader["FORM_NAME"].ToString() 
                                + "'" 
                                + " AND DATA_SOURCE_ID = ' " 
                                + s_DataSourceId 
                                + "'";
                            objCmd.CommandText = sSql;
                            objCmd.ExecuteNonQuery();
                            //Added by Shivendu for MITS 8180----End-------
                            

						}
						if(objDbReader != null)
							objDbReader.Dispose();
                        
					}
					
					sbSql.Remove(0,sbSql.Length);  
					sbSql.Append("INSERT INTO NET_VIEWS_MEMBERS(VIEW_ID, MEMBER_ID, ISGROUP, DATA_SOURCE_ID) SELECT ");
                    sbSql.Append(iNewViewId + ",MEMBER_ID,ISGROUP, DATA_SOURCE_ID FROM NET_VIEWS_MEMBERS WHERE VIEW_ID=" 
                        + iViewId 
                        + " AND DATA_SOURCE_ID = ' " 
                        + s_DataSourceId 
                        + "'");
					objDbConnection.ExecuteNonQuery(sbSql.ToString());
				}
				else
					return false;

				return true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("PVCreate.Clone.Error", m_iClientId),p_objEx); //mbahl3 JIRA [RMACLOUD-123]
			}
			finally
			{
                if (objDbReader != null)
                {
                   objDbReader.Dispose();
                }
                if (objDbConnection != null)
                {
                   objDbConnection.Dispose();
                }                
				p_objXMLDocument=null;
				sbSql=null;
				objXml=null;
                objCmd = null;
                objParam = null;
                nsNamespace = null;
			}
		}


		/// <summary>
		/// Inserts the default views with the newly created view. These are the views which cannot be edited 
		/// using powerviews. So they need to be inserted by default when the view is created first time.
		/// </summary>
		private void InsertDefaultViews(int p_iView)
		{
			string sXml="";
            StringBuilder sViewAspx = new StringBuilder("");
			DbConnection objCon=null;
			XmlNode objNod=null;
			DataSet objDs=null;
			string sSql="";
			DbCommand objCmd=null;
			DbParameter objParam=null;
            DbParameter objParamAspx = null;
			XmlNamespaceManager nsNamespace = null;
			XmlDocument objView=new XmlDocument();
            XmlDocument objTempXmlDoc = new XmlDocument();

            try
            {
                //rsolanki2: extensibility updates. need to pick up view_ids with -1 ( in place of the base view ids ) where exist
                objDs = DbFactory.GetDataSet(m_netViewFormsConnString 
                    ,string.Concat(
                        "SELECT * FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 AND DATA_SOURCE_ID='"
                        , s_DataSourceId
                        , "' "
                        ,"UNION ALL SELECT * FROM NET_VIEW_FORMS WHERE DATA_SOURCE_ID = '"
                        , s_DataSourceId
                        , "' "
                        ,"AND VIEW_ID = 0 AND FORM_NAME NOT IN "
                        ,"(SELECT FORM_NAME FROM NET_VIEW_FORMS WHERE VIEW_ID = -1 AND DATA_SOURCE_ID='"
                        , s_DataSourceId
                        , "')"
                    ), m_iClientId); 

                //  "SELECT * FROM NET_VIEW_FORMS  WHERE VIEW_ID=0" 
                //, " AND DATA_SOURCE_ID = ' " 
                //, s_DataSourceId 
                //, "'"
                objCon = DbFactory.GetDbConnection(m_netViewFormsConnString);
                objCon.Open();
                foreach (DataRow oRow in objDs.Tables[0].Rows)
                {
                    if (!IsPvForm(Conversion.ConvertObjToStr(oRow["FORM_NAME"]),m_sConnString, m_iClientId))
                    {
                        objView.LoadXml(Conversion.ConvertObjToStr(oRow["VIEW_XML"]));
                        objNod = objView.SelectSingleNode("//form");
                        if (objNod == null)
                        {
                            nsNamespace = new XmlNamespaceManager(objView.NameTable);
                            nsNamespace.AddNamespace("xhtml", XHTML_NAMESPACE);
                            //Try the xhtml:body for the other list kind of forms
                            objNod = objView.SelectSingleNode("//xhtml:body", nsNamespace);
                        }
                        if (objNod != null)
                            ((XmlElement)objNod).SetAttribute("viewid", p_iView.ToString());

                        //single quote creating problems
                        //sXml = objView.OuterXml;
                        //sXml = sXml.Replace("'","''");
                        //objView.LoadXml(sXml); 
                        objCmd = objCon.CreateCommand();
                        objParam = objCmd.CreateParameter();
                        objParamAspx = objCmd.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParamAspx.Direction = ParameterDirection.Input;
                        objParam.Value = objView.OuterXml;
                        //Added by Shivendu for R5
                        //ComponentPowerViewUpgrade.Classes.PowerViewUpgrade pvUpgrade = new ComponentPowerViewUpgrade.Classes.PowerViewUpgrade();
                        //pvUpgrade.UpgradeXmlToAspx(false, false, objView, Conversion.ConvertObjToStr(oRow["FORM_NAME"]), out sViewAspx);
                        objParamAspx.Value = Conversion.ConvertObjToStr(oRow["PAGE_CONTENT"]);
                        objParam.ParameterName = "XML";
                        objParamAspx.ParameterName = "ASPX";
                        objParam.SourceColumn = "VIEW_XML";
                        objParamAspx.SourceColumn = "PAGE_CONTENT";
                        objCmd.Parameters.Add(objParam);
                        objCmd.Parameters.Add(objParamAspx);

                        //Geeta 11/16/07 : Modified for Mits 10927
                        if (!(objView.OuterXml.Contains("stylesheet")))
                        {
                            objTempXmlDoc.LoadXml(objView.OuterXml);
                            UTILITY.AddSuppDefinition(m_sUserId, m_sPassword, m_sConnString, m_sDSNName, objTempXmlDoc, m_iClientId);    //mbahl3 JIRA [RMACLOUD-123]                        
                            objParam.Value = objTempXmlDoc.OuterXml;
                            //Added by Shivendu for R5

                            //pvUpgrade.UpgradeXmlToAspx(false, false, objTempXmlDoc, Conversion.ConvertObjToStr(oRow["FORM_NAME"]), out sViewAspx);

                            //objParamAspx.Value = sViewAspx.ToString();
                        }
                       
                        //Is missing from the form list. Need to be inserted as default in the views table
                        sSql = "INSERT INTO NET_VIEW_FORMS(VIEW_ID,FORM_NAME,TOPLEVEL,CAPTION,VIEW_XML,PAGE_CONTENT,PAGE_NAME,LAST_UPDATED,DATA_SOURCE_ID) VALUES (" 
                            + p_iView 
                            + ",'" 
                            + Conversion.ConvertObjToStr(oRow["FORM_NAME"]) +
                            "'," 
                            + Conversion.ConvertObjToStr(oRow["TOPLEVEL"]) 
                            + ",'" 
                            + Conversion.ConvertObjToStr(oRow["CAPTION"]) 
                            + "'," +
                            "~XML~,~ASPX~,'" + Conversion.ConvertObjToStr(oRow["FORM_NAME"]).Replace("xml", "aspx") 
                            + "','" 
                            + System.DateTime.Now.ToString("yyyyMMddHHmmss") 
                            +"','"
                            + s_DataSourceId
                            +"')";
                        objCmd.CommandText = sSql;
                        objCmd.ExecuteNonQuery();
                    }
                }
            }
            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                }
                if (objDs != null)
                    objDs.Dispose();

                objCmd = null;
                objParam = null;
                objView = null;
                objTempXmlDoc = null;
            }
		}


		internal static bool IsPvForm(string p_sForm, string p_sConnString, int p_iClientId)
		{	
			bool bPvForm = false;
            if (m_objXMLDocFormList == null || m_objXMLDocFormList.InnerXml == "")
			{
				string sFileName = "";
				//RMConfigurator objConfig = new RMConfigurator();
				m_objXMLDocFormList = new XmlDocument();

                //Raman 07/20/2009: removing dependency on Riskmaster.Config

                //sFileName = objConfig.GetValue(PV_XMLFILEPATH) + @"\" + objConfig.GetValue(PV_FORMLISTFILE);
                string sFormListFile = UTILITY.GetConfigUtilitySettings(p_sConnString, p_iClientId)["PV_FormListFile"];
                sFileName = PV_XMLFILEPATH + @"\" + sFormListFile;
				if(sFileName != "")
				{
					if(System.IO.File.Exists(sFileName))
						m_objXMLDocFormList.Load(sFileName);
					else
                        throw new RMAppException(Globalization.GetString("PVDefination.GetFormList.FormListNotFound", p_iClientId));
				}
				else
                    throw new RMAppException(Globalization.GetString("PVDefination.GetFormList.FormListNotFound", p_iClientId));
			}

            if (m_objXMLDocFormList.SelectSingleNode("//form[@file='" + p_sForm + "']") != null || p_sForm.IndexOf("admintracking")>=0)
				bPvForm=true;

			return bPvForm;
		}

		internal string DefViewLst()
		{
			DbConnection objCon=null;
			DataSet objDs=null;
            try
            {
                if (m_sDefVwLst == "")
                {
                    objCon = DbFactory.GetDbConnection(m_netViewFormsConnString);
                    objCon.Open();

                    //rsolanki2: performance updates 
                    objDs = DbFactory.GetDataSet(m_netViewFormsConnString
                        , string.Concat("SELECT FORM_NAME FROM NET_VIEW_FORMS  WHERE VIEW_ID=0" 
                            , " AND DATA_SOURCE_ID = ' " 
                            , s_DataSourceId
                            , "'"), m_iClientId);
                    foreach (DataRow oRow in objDs.Tables[0].Rows)
                    {
                        if (!IsPvForm(Conversion.ConvertObjToStr(oRow["FORM_NAME"]),m_sConnString, m_iClientId))
                        {
                            if (m_sDefVwLst != "")
                                m_sDefVwLst += ",";
                            m_sDefVwLst += "'" + Conversion.ConvertObjToStr(oRow["FORM_NAME"]) + "'";
                        }
                    }                 
                }
                return m_sDefVwLst; 
            }

            finally
            {
                if (objCon != null)
                {
                    objCon.Dispose();
                }
                if (objDs != null)
                {
                    objDs.Dispose();
                }
            }			
		}

		#endregion
	}
}
