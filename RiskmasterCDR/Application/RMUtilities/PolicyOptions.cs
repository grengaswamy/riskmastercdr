﻿
using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using System.Globalization;
using System.Threading;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21 Mar 2005
	///Purpose :   This class Fetches PolicyOptions Data.
	/// </summary>
	public class PolicyOptions
	{
		/// <summary>
		/// Root tag
		/// </summary>
		private const string ROOT_TAG = "<Document></Document>";

		/// <summary>
		/// Private constant variable to store TableName
		/// </summary>
		private const string TABLENAME = "SYS_POL_OPTIONS";

		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnectString = "";

        /// <summary>
        /// Conversion successful 
        /// Added by Tushar MITS 18231
        /// </summary>
        private bool blnSuccess = false;

        //Anu Tennyson for MITS 18229 STARTS 2/2/2010
        /// <summary>
        /// Name of the Code Table for finding the code id 
        /// </summary>
        private const string CODE_TABLENAME = "RATE_MAINTENANCE_TYPE";
        //Anu Tennyson for MITS 18229 ENDS
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "";

        private int m_iClientId = 0;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnectString"></param>
		public PolicyOptions(string p_sLoginName,string p_sConnectString, int p_iClientId)
		{
			m_sUserName = p_sLoginName;
			m_sConnectString = p_sConnectString;
            m_iClientId = p_iClientId;
		}

		#region "LoadData"
		#region "Output XML"
		///	<Document>
		///		<DiscountsList>
		///			<ListHeader>
		///				<discountname>Discount Name</discountname>
		///				<lob>GL</lob>
		///				<state>AC</state>
		///				<discounttype>F</discounttype>
		///				<amount>$34.00</amount>
		///				<effectivedate>3/5/2005 12:00:00 AM</effectivedate>
		///				<expirationdate>3/8/2005 12:00:00 AM</expirationdate>
		///				<discountrowid>1</discountrowid>
		///				<tag>AnuragTest,5188,73,34,5237,3/5/2005 12:00:00 AM,3/8/2005 12:00:00 AM,1</tag>
		///			</ListHeader>
		///			<option>
		///				<discountname>asa</discountname>
		///				<lob>GL</lob>
		///				<state>AC</state>
		///				<discounttype>F</discounttype>
		///				<amount>$0.00</amount>
		///				<effectivedate>3/16/2005 12:00:00 AM</effectivedate>
		///				<expirationdate>1/1/0001 12:00:00 AM</expirationdate>
		///				<discountrowid>2</discountrowid>
		///				<tag>asa,5188,73,0,5237,3/16/2005 12:00:00 AM,1/1/0001 12:00:00 AM,2</tag>
		///			</option>
		///		</DiscountsList>
		///		<PolOptRowId></PolOptRowId>
		///		<RoundAmounts>0</RoundAmounts>
		///		<UseYear>1</UseYear>
		///		<Placement enabled="1">5239</Placement>
		///		<ExpConstList>
		///			<option>
		///				<use>Y</use>
		///				<lob>GL</lob>
		///				<state>AC</state>
		///				<expcontype>F</expcontype>
		///				<amount>$23.00</amount>
		///				<effectivedate>4/6/2005 12:00:00 AM</effectivedate>
		///				<expirationdate>4/18/2005 12:00:00 AM</expirationdate>
		///				<expconrowid>1</expconrowid>
		///				<tag>True,5188,73,23,5237,4/6/2005 12:00:00 AM,4/18/2005 12:00:00 AM,1</tag>
		///			</option>
		///		</ExpConstList>
		///		<TierList>
		///			<option>
		///				<use>Y</use>
		///				<tiername>Modified Premium</tiername>
		///				<lob>\" \"</lob>
		///				<state></state>
		///				<effectivedate>1/1/0001 12:00:00 AM</effectivedate>
		///				<expirationdate>1/1/0001 12:00:00 AM</expirationdate>
		///				<applicablelevel></applicablelevel>
		///				<disctierrowid>1</disctierrowid>
		///				<tag>True,0,0,1/1/0001 12:00:00 AM,1/1/0001 12:00:00 AM,,1</tag>
		///			</option>
		///			<option>
		///				<use>Y</use>
		///				<tiername>ffsd</tiername>
		///				<lob>WC</lob>
		///				<state>AR</state>
		///				<effectivedate>3/16/2005 12:00:00 AM</effectivedate>
		///				<expirationdate>4/20/2005 12:00:00 AM</expirationdate>
		///				<applicablelevel>Modified Premium</applicablelevel>
		///				<disctierrowid>5</disctierrowid>
		///				<tag>True,5187,5,3/16/2005 12:00:00 AM,4/20/2005 12:00:00 AM,Modified Premium,5</tag>
		///			</option>
		///			<option>
		///				<use>N</use>
		///				<tiername>zxc</tiername>
		///				<lob>GL</lob>
		///				<state>AZ</state>
		///				<effectivedate>4/22/2005 12:00:00 AM</effectivedate>
		///				<expirationdate>4/28/2005 12:00:00 AM</expirationdate>
		///				<applicablelevel>Modified Premium</applicablelevel>
		///				<disctierrowid>6</disctierrowid>
		///				<tag>False,5188,4,4/22/2005 12:00:00 AM,4/28/2005 12:00:00 AM,Modified Premium,6</tag>
		///			</option>
		///		</TierList>
		///		<ThresholdsList>
		///			<option>
		///				<use>Y</use>
		///				<lob>WC</lob>
		///				<state>AL</state>
		///				<amount>$23.00</amount>
		///				<effectivedate>3/2/2005 12:00:00 AM</effectivedate>
		///				<expirationdate>3/3/2005 12:00:00 AM</expirationdate>
		///				<thresholdrowid>1</thresholdrowid>
		///				<tag>True,5187,1,23,3/2/2005 12:00:00 AM,3/3/2005 12:00:00 AM,1</tag>
		///			</option>
		///		</ThresholdsList>
		///		<RenewalInfoList>
		///			<option>
		///				<lob>GL</lob>
		///				<state>AK</state>
		///				<termlength>$1.00</termlength>
		///				<termcode>D</termcode>
		///				<renewalinforowid>1</renewalinforowid>
		///				<tag>5188,2,1,5220,1</tag>
		///			</option>
		///		</RenewalInfoList>
		///		<DiscountsSetUpList>
		///			<option>
		///				<use></use>
		///				<discountname></discountname>
		///				<lob></lob>
		///				<state></state>
		///				<applicablelevel></applicablelevel>
		///				<useredit></useredit>
		///				<seldiscrowid></seldiscrowid>
		///				<tag></tag>
		///			</option>
		///		</DiscountsSetUpList>
		///		<ExpRatesList>
		///			<option>
		///				<exposurecode></exposurecode>
		///				<exposuredescription></exposuredescription>
		///				<state></state>
		///				<flatorpercent></flatorpercent>
		///				<amount></amount>
		///				<fixedorprorate></fixedorprorate>
		///				<baserate></baserate>
		///				<effectivedate></effectivedate>
		///				<expirationdate></expirationdate>
		///				<expraterowid></expraterowid>
		///				<tag></tag>
		///			</option>
		///		</ExpRatesList>
		///	</Document>
		#endregion
		/// <summary>
		/// Loads data for Polcyoptions
		/// </summary>
		/// <returns>XML Doc with data</returns>
		public XmlDocument LoadData()
		{
			DbReader objDbReader = null;
			DbReader objTempDbReader = null;
			Discount objDiscount = null;
			PolOptions objPolOptions = null;
			ExpenseConstant objExpenseConstant = null;

            //npadhy RMSC retrofit Start 
            Taxes objTaxes = null;
            //npadhy RMSC retrofit Ends

			DiscountTier objDiscountTier = null;
			Threshold objThreshold = null;
			RenewalInformation objRenewalInformation = null;
			SelectedDiscount objSelectedDiscount = null;
			ExposureRate objExposureRate = null;

			ArrayList objArrayList = null;
			XmlDocument objXMLDocument = null;
			XmlElement objXMLChildElement = null;
			XmlElement objXMLElement = null;
			LocalCache objLocalCache = null;
            string sApplicableLevel = string.Empty;
            string sDiscountName = string.Empty;

			string sShortCode=string.Empty;
			string sDesc=string.Empty;
            int iCounter = 0;
            //Anu Tennyson for MITS 18229 STARTS 2/2/2010
            int iRateMaintType = 0;
            //Anu Tennyson for MITS 18229 ENDS

            //Start: Sumit-08/13/2010 - MITS# 20815
            string sAssocDiscounts = string.Empty;
            //End:Sumit

            //Start: Sumit-11/09/2010 - MITS# 22877
            string sAssocCoverages = string.Empty;
            //End:Sumit

            int iNoOfRecords = 0;
			try
			{
                //Manish for multi currency
                string culture = CommonFunctions.GetCulture(0, CommonFunctions.NavFormType.None, m_sConnectString);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture, false);

				//load XML root Tag
                objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
				objXMLDocument = new XmlDocument();
				objXMLDocument.LoadXml(ROOT_TAG);

				//load Discount List
				#region
                iCounter = 0;
                string sSQL = "SELECT * FROM SYS_POL_DISCOUNTS ORDER BY DISCOUNT_ROWID DESC";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

				objArrayList = new ArrayList();
                iNoOfRecords = 0;
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (DiscountCurrentPage - 1) * DiscountPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= DiscountCurrentPage * DiscountPageSize)
                            break;
						//end rsushilaggar
                        objDiscount = new Discount(m_iClientId);

						objDiscount.DiscountName = objDbReader["DISCOUNT_NAME"].ToString();
						objDiscount.LOB = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUSINESS"].ToString());
						objDiscount.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objDiscount.Amount  = Conversion.ConvertStrToDouble(objDbReader["AMOUNT"].ToString());
						objDiscount.DiscountType = Conversion.ConvertStrToInteger(objDbReader["FLAT_OR_PERCENT"].ToString());
						objDiscount.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objDiscount.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objDiscount.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
						objDiscount.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
						objDiscount.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
						objDiscount.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
                        //Uncommented By abansal23 for RowId
						objDiscount.DiscountRowId = Conversion.ConvertStrToInteger(objDbReader["DISCOUNT_ROWID"].ToString());

						objArrayList.Add(objDiscount);
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}	

				//load DiscountsSetUp list
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
			
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("DiscountsList"),false);
				objXMLElement.AppendChild(objXMLChildElement);
				objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("DiscountsList");	
				
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"),false);
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discountname","Discount/Surcharge Name"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob","LOB"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state","State"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discounttype","Flat or Percent"),true));
                //Divya MITS 9230 change Amount to Amount/Rate 04/24/2007
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount","Amount/Rate"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate","Effective Date"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate","Expiration Date"),true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discountrowid","Discount Row Id"),true));				
				objXMLElement.AppendChild(objXMLChildElement);
	
				for(int i = 0; i < objArrayList.Count; i++)
				{
					objDiscount = (Discount)objArrayList[i]; 

					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/DiscountsList/option[" + (++iCounter).ToString() + "]");
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discountname",objDiscount.DiscountName),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob",objLocalCache.GetShortCode(objDiscount.LOB)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objDiscount.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discounttype",objLocalCache.GetShortCode(objDiscount.DiscountType)),true));
					if(objXMLChildElement.GetElementsByTagName("discounttype").Item(0).InnerText == "F")
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount",string.Format("{0:C}",objDiscount.Amount)),true));
					else
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", objDiscount.Amount.ToString() + "%"), true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate",UTILITY.FormatDate(objDiscount.EffectiveDate,false)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate",UTILITY.FormatDate(objDiscount.ExpirationDate,false)),true));					
					string sTag = objDiscount.DiscountName + "," + objDiscount.LOB + "," + objDiscount.State +
						"," + objDiscount.Amount + "," + objDiscount.DiscountType + "," + UTILITY.FormatDate(objDiscount.EffectiveDate,false) + 
						"," + UTILITY.FormatDate(objDiscount.ExpirationDate,false) + "," + objDiscount.DiscountRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discountrowid", objDiscount.DiscountRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);

				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/DiscountsList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("discountname"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("discounttype"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("discountrowid"), false));
                objXMLElement.AppendChild(objXMLChildElement);
				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_DISCOUNTS";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                int totalRows = 0;
                int totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / DiscountPageSize;
                    if ((totalRows % DiscountPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("Document"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("DiscountsList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/DiscountsList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End rsushilaggar
				#endregion

				//load pol options info
				#region
				sSQL = "SELECT * FROM SYS_POL_OPTIONS";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

				if(objDbReader != null)
				{
					objPolOptions = new PolOptions();
				
					if(objDbReader.Read())
					{
						objPolOptions.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objPolOptions.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objPolOptions.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objPolOptions.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objPolOptions.PolOptRowId = Conversion.ConvertStrToInteger(objDbReader["POL_OPT_ROWID"].ToString());
                        objPolOptions.RoundAmounts = Conversion.ConvertObjToBool(objDbReader["ROUND_AMTS_FLAG"].ToString(), m_iClientId);
                        objPolOptions.UseYear = Conversion.ConvertObjToBool(objDbReader["USE_YEAR_FLAG"], m_iClientId);
                        objPolOptions.Placement = Conversion.ConvertStrToInteger(objDbReader["PLACEMENT"].ToString());
                        objPolOptions.AuditCancelledPolicy = Conversion.ConvertObjToBool(objDbReader["AUDIT_CANCELLED_POLICY"], m_iClientId);
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}
			
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
				objXMLElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("RoundAmounts",(objPolOptions.RoundAmounts == true ? "True" : "").ToString()),true));
				objXMLElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("UseYear",(objPolOptions.UseYear == true ? "True" : "").ToString()),true));
                objXMLElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("AuditPolicy", (objPolOptions.AuditCancelledPolicy== true ? "True" : "").ToString()), true));
				objXMLElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("Placement",objPolOptions.Placement.ToString()),true));
				objXMLElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("PolOptRowId",objPolOptions.PolOptRowId.ToString()),true));
			
//				if(objPolOptions.UseYear)
//				{
					objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("Placement");
					objXMLElement.SetAttribute("enabled","1");  
					objXMLElement.SetAttribute("codeid",objXMLElement.InnerText);
					objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objXMLElement.InnerText),ref sShortCode,ref sDesc);
					objXMLElement.InnerText= sShortCode + " " + sDesc;
//				}
				#endregion

				//initialize expense constant list view
				#region
                iCounter = 0;
                sSQL = "SELECT * FROM SYS_POL_EXP_CONST ORDER BY EXP_CON_ROWID DESC";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

				objArrayList = null;
				objArrayList = new ArrayList();
                iNoOfRecords = 0;
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < ( ExpConstCurrentPage - 1) * ExpConstPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= ExpConstCurrentPage * ExpConstPageSize)
                            break;
						//End rsushilaggar
						objExpenseConstant = new ExpenseConstant();

						objExpenseConstant.Use = Conversion.ConvertObjToInt(objDbReader["IN_USE_FLAG"], m_iClientId);
						objExpenseConstant.LOB = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUSINESS"].ToString());
						objExpenseConstant.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objExpenseConstant.Amount  = Conversion.ConvertStrToDouble(objDbReader["AMOUNT"].ToString());
						objExpenseConstant.ExpConType = Conversion.ConvertStrToInteger(objDbReader["FLAT_OR_PERCENT"].ToString());
						objExpenseConstant.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objExpenseConstant.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objExpenseConstant.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
						objExpenseConstant.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
						objExpenseConstant.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
						objExpenseConstant.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objExpenseConstant.ExpConRowId = Conversion.ConvertStrToInteger(objDbReader["EXP_CON_ROWID"].ToString());//akashyap3

						objArrayList.Add(objExpenseConstant);
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}	
			
				//load list
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
			
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("ExpConstList"),false);
				objXMLElement.AppendChild(objXMLChildElement);
				objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("ExpConstList");	

				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"),false);
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use","In Use"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob","LOB"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state","State"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expcontype","Flat or Percent"),true));
                //Divya MITS 9230 change Amount to Amount/Rate 04/24/2007
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount","Amount/Rate"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate","Effective Date"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate","Expiration Date"),true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expconrowid","Exp Con Row Id"),true));				
				objXMLElement.AppendChild(objXMLChildElement);

				for(int i = 0; i < objArrayList.Count; i++)
				{
					objExpenseConstant = (ExpenseConstant)objArrayList[i]; 
					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/ExpConstList/option[" + (++iCounter).ToString() + "]");
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use",(objExpenseConstant.Use != 0 ? "Y" : "N")),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob",objLocalCache.GetShortCode(objExpenseConstant.LOB)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objExpenseConstant.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expcontype",objLocalCache.GetShortCode(objExpenseConstant.ExpConType)),true));
					if(objXMLChildElement.GetElementsByTagName("expcontype").Item(0).InnerText == "F")
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount",string.Format("{0:C}",objExpenseConstant.Amount)),true));
					else
						objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount",objExpenseConstant.Amount.ToString() + "%"),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate",UTILITY.FormatDate(objExpenseConstant.EffectiveDate,false)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate",UTILITY.FormatDate(objExpenseConstant.ExpirationDate,false)),true));
					string sTag = objExpenseConstant.Use + "," + objExpenseConstant.LOB + "," + objExpenseConstant.State + "," + 
						objExpenseConstant.Amount + "," + objExpenseConstant.ExpConType + "," + UTILITY.FormatDate(objExpenseConstant.EffectiveDate,false) + 
						"," + UTILITY.FormatDate(objExpenseConstant.ExpirationDate,false) + "," + objExpenseConstant.ExpConRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expconrowid", objExpenseConstant.ExpConRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/ExpConstList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("use"),false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expcontype"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expconrowid"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLElement.AppendChild(objXMLChildElement);

				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_EXP_CONST";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / ExpConstPageSize;
                    if ((totalRows % ExpConstPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("ExpConstList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/ExpConstList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End rsushilaggar
				#endregion
                

				//initialize discount tier list view
				#region
                iCounter = 0;
                sSQL = "SELECT * FROM SYS_POL_DISC_TIER ORDER BY DISC_TIER_ROWID DESC";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
                iNoOfRecords = 0;
				objArrayList = null;
				objArrayList = new ArrayList();

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (TierCurrentPage - 1) * TierPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= TierCurrentPage * TierPageSize)
                            break;
						//End rsushilaggar
						objDiscountTier = new DiscountTier();

                        objDiscountTier.Use = Conversion.ConvertObjToBool(objDbReader["IN_USE_FLAG"], m_iClientId);
						objDiscountTier.LOB = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUSINESS"].ToString());
						objDiscountTier.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objDiscountTier.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objDiscountTier.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objDiscountTier.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
						objDiscountTier.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
						objDiscountTier.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
						objDiscountTier.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objDiscountTier.DiscTierRowId = Conversion.ConvertStrToInteger(objDbReader["DISC_TIER_ROWID"].ToString());
						objDiscountTier.TierName = objDbReader["TIER_NAME"].ToString();
						objDiscountTier.ParentLevel = objDbReader["PARENT_LEVEL"].ToString();

						objArrayList.Add(objDiscountTier);
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}	
			
				//load list view
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
			
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("TierList"),false);
				objXMLElement.AppendChild(objXMLChildElement);
				objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("TierList");	

				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"),false);
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use","In Use"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tiername","Tier Name"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob","LOB"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state","State"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate","Effective Date"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate","Expiration Date"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("applicablelevel","Applicable Level"),true));
				//objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("disctierrowid","Disc Tier Row Id"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag","Tag"),true));
				objXMLElement.AppendChild(objXMLChildElement);

				for(int i = 0; i < objArrayList.Count; i++)
				{
					objDiscountTier = (DiscountTier)objArrayList[i]; 

					if(objDiscountTier.ParentLevel != "")
					{
						sSQL = "SELECT TIER_NAME FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + objDiscountTier.ParentLevel;
						objTempDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						if(objTempDbReader != null)
						{
							if(objTempDbReader.Read())
								sApplicableLevel = objTempDbReader["TIER_NAME"].ToString();
                            //abansal23 on 06/03/2009 for MITS 15214 Starts
                            else
                                sApplicableLevel = string.Empty ;
                            //abansal23 on 06/03/2009 for MITS 15214 Ends
                            if (objTempDbReader != null)
                                objTempDbReader.Close();                        
						}
					}

					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/TierList/option[" + (++iCounter).ToString() + "]");
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use",(objDiscountTier.Use == true ? "Y" : "N")),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tiername",objDiscountTier.TierName),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob",objLocalCache.GetShortCode(objDiscountTier.LOB)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objDiscountTier.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate",UTILITY.FormatDate(objDiscountTier.EffectiveDate,false)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate",UTILITY.FormatDate(objDiscountTier.ExpirationDate,false)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("applicablelevel",sApplicableLevel),true));
					
					string sTag = objDiscountTier.Use + "," + objDiscountTier.LOB + "," + objDiscountTier.State + "," + 
						UTILITY.FormatDate(objDiscountTier.EffectiveDate,false) + "," + UTILITY.FormatDate(objDiscountTier.ExpirationDate,false) + 
						"," + sApplicableLevel + "," + objDiscountTier.DiscTierRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("disctierrowid", objDiscountTier.DiscTierRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/TierList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("use"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tiername"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("applicablelevel"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("disctierrowid"), false));
                objXMLElement.AppendChild(objXMLChildElement);
				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_DISC_TIER ";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / TierPageSize;
                    if ((totalRows % TierPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("TierList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/TierList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End rsushilaggar
				#endregion

				//initialize threshold list view
				#region
                iCounter = 0;
                sSQL = "SELECT * FROM SYS_POL_BILL_THRSH ORDER BY THRESHOLD_ROWID DESC";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
                iNoOfRecords = 0;
				objArrayList = null;
				objArrayList = new ArrayList();

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (MinBillCurrentPage - 1) * MinBillPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= MinBillCurrentPage * MinBillPageSize)
                            break;
						//End rsushilaggar
						objThreshold = new Threshold();

                        objThreshold.Use = Conversion.ConvertObjToBool(objDbReader["IN_USE_FLAG"], m_iClientId);
						objThreshold.LOB = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUSINESS"].ToString());
						objThreshold.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objThreshold.Amount  = Conversion.ConvertStrToDouble(objDbReader["AMOUNT"].ToString());
						objThreshold.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objThreshold.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objThreshold.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
						objThreshold.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
						objThreshold.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
						objThreshold.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objThreshold.ThresholdRowId = Conversion.ConvertStrToInteger(objDbReader["THRESHOLD_ROWID"].ToString());

						objArrayList.Add(objThreshold);
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}	
			
				//load list
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
			
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("ThresholdsList"),false);
				objXMLElement.AppendChild(objXMLChildElement);
				objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("ThresholdsList");	

				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"),false);
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use","In Use"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob","LOB"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state","State"),true));
                //Divya MITS 9230 change Amount to Amount/Rate 04/24/2007
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount","Amount/Rate"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate","Effective Date"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate","Expiration Date"),true));
				//objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("thresholdrowid","Threshold Row Id"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag","Tag"),true));
				objXMLElement.AppendChild(objXMLChildElement);

				for(int i = 0; i < objArrayList.Count; i++)
				{
					objThreshold = (Threshold)objArrayList[i]; 

					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/ThresholdsList/option[" + (++iCounter).ToString() + "]");
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use",(objThreshold.Use == true ? "Y" : "N")),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob",objLocalCache.GetShortCode(objThreshold.LOB)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objThreshold.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount",string.Format("{0:C}",objThreshold.Amount)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate",UTILITY.FormatDate(objThreshold.EffectiveDate,false)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate",UTILITY.FormatDate(objThreshold.ExpirationDate,false)),true));
					string sTag = objThreshold.Use + "," + objThreshold.LOB + "," + objThreshold.State + "," + 
						objThreshold.Amount + "," + UTILITY.FormatDate(objThreshold.EffectiveDate,false) + 
						"," + UTILITY.FormatDate(objThreshold.ExpirationDate,false) + "," + objThreshold.ThresholdRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("thresholdrowid", objThreshold.ThresholdRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/ThresholdsList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("use"), false));                
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("thresholdrowid"), false));             
                objXMLElement.AppendChild(objXMLChildElement);
				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_BILL_THRSH";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                 totalRows = 0;
                 totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / MinBillPageSize;
                    if ((totalRows % MinBillPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("ThresholdsList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/ThresholdsList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End rsushilaggar
				#endregion

                #region 
                //Initialise  Tax list 
                iCounter = 0;
                sSQL = "SELECT * FROM SYS_POL_TAX ORDER BY TAX_ROWID DESC";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                iNoOfRecords = 0;
                objArrayList = null;
                objArrayList = new ArrayList();

                if (objDbReader != null)
                {
                    while (objDbReader.Read())
                    {
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (PolTaxCurrentPage - 1) * PolTaxPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= PolTaxCurrentPage * PolTaxPageSize)
                            break;
						//End rsushilaggar
                        objTaxes = new Taxes();

                        objTaxes.Use = Conversion.ConvertObjToInt(objDbReader["IN_USE_FLAG"], m_iClientId);
                        objTaxes.LOB = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUSINESS"].ToString());
                        objTaxes.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
                        objTaxes.Amount = Conversion.ConvertStrToDouble(objDbReader["AMOUNT"].ToString());
                        objTaxes.TaxType = Conversion.ConvertStrToInteger(objDbReader["FLAT_OR_PERCENT"].ToString());
                        objTaxes.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
                        objTaxes.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
                        objTaxes.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
                        objTaxes.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
                        objTaxes.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
                        objTaxes.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
                        objTaxes.TaxRowId = Conversion.ConvertStrToInteger(objDbReader["TAX_ROWID"].ToString());

                        objArrayList.Add(objTaxes);
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }

                //load list
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("PolicyTaxList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("PolicyTaxList");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"), false);
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use", "In Use"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob", "LOB"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state", "State"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("taxtype", "Flat or Percent"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", "Amount/Rate"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate", "Effective Date"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate", "Expiration Date"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("PolicyTaxrowid", "Tax Row Id"), true));
                objXMLElement.AppendChild(objXMLChildElement);

                for (int i = 0; i < objArrayList.Count; i++)
                {
                    objTaxes = (Taxes)objArrayList[i];
                    objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/ExpConstList/option[" + (++iCounter).ToString() + "]");
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use", (objTaxes.Use != 0 ? "Y" : "N")), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob", objLocalCache.GetShortCode(objTaxes.LOB)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state", objLocalCache.GetStateCode(objTaxes.State)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("taxtype", objLocalCache.GetShortCode(objTaxes.TaxType)), true));
                    if (objXMLChildElement.GetElementsByTagName("taxtype").Item(0).InnerText == "F")
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", string.Format("{0:C}", objTaxes.Amount)), true));
                    else
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", objTaxes.Amount.ToString() + "%"), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate", UTILITY.FormatDate(objTaxes.EffectiveDate, false)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate", UTILITY.FormatDate(objTaxes.ExpirationDate, false)), true));
                    string sTag = objTaxes.Use + "," + objTaxes.LOB + "," + objTaxes.State + "," +
                        objTaxes.Amount + "," + objTaxes.TaxType + "," + UTILITY.FormatDate(objTaxes.EffectiveDate, false) +
                        "," + UTILITY.FormatDate(objTaxes.ExpirationDate, false) + "," + objTaxes.TaxRowId;
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("PolicyTaxrowid", objTaxes.TaxRowId.ToString()), true));
                    objXMLElement.AppendChild(objXMLChildElement);
                }
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/PolicyTaxList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("use"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("taxtype"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("PolicyTaxrowid"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLElement.AppendChild(objXMLChildElement);
				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_TAX ";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / PolTaxPageSize;
                    if ((totalRows % PolTaxPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("PolicyTaxList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/PolicyTaxList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End rsushilaggar
                #endregion

				//initialize renewal info list view
				#region
                iCounter = 0;
                sSQL = "SELECT * FROM SYS_POL_RNWL_INFO ORDER BY RENEWAL_INFO_ROWID DESC";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);

				objArrayList = null;
				objArrayList = new ArrayList();
                iNoOfRecords = 0;
				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (TermCurrentPage - 1) * TermPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= TermCurrentPage * TermPageSize)
                            break;
						//Ens rushilaggar
						objRenewalInformation = new RenewalInformation();

						objRenewalInformation.LOB = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUSINESS"].ToString());
						objRenewalInformation.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objRenewalInformation.Amount  = Conversion.ConvertStrToDouble(objDbReader["AMOUNT"].ToString());
						objRenewalInformation.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objRenewalInformation.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objRenewalInformation.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
						objRenewalInformation.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objRenewalInformation.RenewalInfoRowId = Conversion.ConvertStrToInteger(objDbReader["RENEWAL_INFO_ROWID"].ToString());
						objRenewalInformation.TermCode = Conversion.ConvertStrToInteger(objDbReader["TERM_CODE"].ToString());

						objArrayList.Add(objRenewalInformation);

                        

                        iNoOfRecords++;
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}	
			
				//load list
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
			
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("RenewalInfoList"),false);
				objXMLElement.AppendChild(objXMLChildElement);
				objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("RenewalInfoList");	

				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"),false);
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob","LOB"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state","State"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("termlength","Term Length"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("termcode","Term Code"),true));
				//objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("renewalinforowid","Renewal Info Row Id"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag","Tag"),true));
				objXMLElement.AppendChild(objXMLChildElement);

				for(int i = 0; i < objArrayList.Count; i++)
				{
					objRenewalInformation = (RenewalInformation)objArrayList[i]; 

					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/RenewalInfoList/option[" + (++iCounter).ToString() + "]");  
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob",objLocalCache.GetShortCode(objRenewalInformation.LOB)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objRenewalInformation.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("termlength",objRenewalInformation.Amount.ToString()),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("termcode",objLocalCache.GetShortCode(objRenewalInformation.TermCode)),true));
					string sTag = objRenewalInformation.LOB + "," + objRenewalInformation.State + "," + objRenewalInformation.Amount
						+ "," + objRenewalInformation.TermCode + "," +  objRenewalInformation.RenewalInfoRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("renewalinforowid", objRenewalInformation.RenewalInfoRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/RenewalInfoList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("termlength"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("termcode"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("renewalinforowid"), false));
                objXMLElement.AppendChild(objXMLChildElement);
                // rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_RNWL_INFO";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                 totalRows = 0;
                 totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / TermPageSize;
                    if ((totalRows % TermPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                 objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("RenewalInfoList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/RenewalInfoList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End rsushilaggar
				#endregion

				//initialize Rate Set Up List view
                //Added by Tushar MITS 18231
				#region  Rate Up List
                iCounter = 0;
                sSQL = "SELECT * FROM SYS_POL_SEL_DISC WHERE IS_RATE_SETUP=1  ORDER BY SEL_DISC_ROWID DESC";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
                iNoOfRecords = 0;
				objArrayList = null;
				objArrayList = new ArrayList();

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (RateCurrentPage - 1) * RatePageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= RateCurrentPage * RatePageSize)
                            break;
						// End rsushilaggar 


						objSelectedDiscount = new SelectedDiscount();

                        objSelectedDiscount.Use = Conversion.ConvertObjToBool(objDbReader["IN_USE_FLAG"], m_iClientId);
						objSelectedDiscount.LOB = Conversion.ConvertStrToInteger(objDbReader["LINE_OF_BUSINESS"].ToString());
						objSelectedDiscount.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objSelectedDiscount.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objSelectedDiscount.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objSelectedDiscount.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
						objSelectedDiscount.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objSelectedDiscount.SelDiscRowId = Conversion.ConvertStrToInteger(objDbReader["SEL_DISC_ROWID"].ToString());
						objSelectedDiscount.DiscountRowid = Conversion.ConvertStrToInteger(objDbReader["DISCOUNT_ROWID"].ToString());
						objSelectedDiscount.ParentLevel = objDbReader["PARENT_LEVEL"].ToString();
                        objSelectedDiscount.Override = Conversion.ConvertObjToBool(objDbReader["OVERRIDE_FLAG"], m_iClientId);

						objArrayList.Add(objSelectedDiscount);
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}	
			
				//load list
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
			
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("RateSetUpList"),false);
				objXMLElement.AppendChild(objXMLChildElement);
				objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("RateSetUpList");	

				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"),false);
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use","In Use"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("coveragename","Coverage Name"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob","LOB"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state","State"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("applicablelevel","Applicable Level"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("useredit","User Edit"),true));
				//objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("seldiscrowid","Sel Disc Row Id"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag","Tag"),true));
				objXMLElement.AppendChild(objXMLChildElement);

				for(int i = 0; i < objArrayList.Count; i++)
				{
					objSelectedDiscount = (SelectedDiscount)objArrayList[i]; 
				
					sApplicableLevel = "";
					if(objSelectedDiscount.ParentLevel != "")
					{
						sSQL = "SELECT TIER_NAME FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + objSelectedDiscount.ParentLevel;
						objTempDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						if(objTempDbReader != null)
						{
							if(objTempDbReader.Read())
								sApplicableLevel = objTempDbReader["TIER_NAME"].ToString();
							if(objTempDbReader != null)
								objTempDbReader.Close();
						}
					}

					if(objSelectedDiscount.DiscountRowid != 0)
					{
                        sSQL = "SELECT CODE_DESC AS COVERAGE_NAME FROM SYS_POL_EXP_RATES,CODES_TEXT WHERE SYS_POL_EXP_RATES.EXPOSURE_ID =CODES_TEXT.CODE_ID AND SYS_POL_EXP_RATES.EXP_RATE_ROWID = " + objSelectedDiscount.DiscountRowid;
						objTempDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						if(objTempDbReader != null)
						{
							if(objTempDbReader.Read())
                                sDiscountName = objTempDbReader["COVERAGE_NAME"].ToString();
							if(objTempDbReader != null)
								objTempDbReader.Close();
						}
					}

					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/RateSetUpList/option[" + (++iCounter).ToString() + "]");                    
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use",(objSelectedDiscount.Use == true ? "Y" : "N")),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("coveragename", sDiscountName), true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob",objLocalCache.GetShortCode(objSelectedDiscount.LOB)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objSelectedDiscount.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("applicablelevel",sApplicableLevel),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("useredit",(objSelectedDiscount.Override == true ? "Y" : "N")),true));
					string sTag = objSelectedDiscount.Use + "," + sDiscountName + "," + objSelectedDiscount.LOB + "," + objSelectedDiscount.State + 
						"," + sApplicableLevel	+ "," + objSelectedDiscount.Override + "," +  objSelectedDiscount.SelDiscRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("selraterowid", objSelectedDiscount.SelDiscRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);

                    //Start: Sumit-11/09/2010 - MITS# 22877
                    if (String.IsNullOrEmpty(sAssocCoverages))
                    {
                        sAssocCoverages = objSelectedDiscount.DiscountRowid.ToString();
                    }
                    else
                    {
                        sAssocCoverages += "|" + objSelectedDiscount.DiscountRowid.ToString();
                    }
                    //End:Sumit
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/RateSetUpList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("use"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("coveragename"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("applicablelevel"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("useredit"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("selraterowid"), false));
                objXMLElement.AppendChild(objXMLChildElement);

                //Start: Sumit-11/09/2010 - MITS# 22877
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
                objXMLElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("AssocCoverages", sAssocCoverages), true));
                //End:Sumit

				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_SEL_DISC WHERE IS_RATE_SETUP=1 ";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / RatePageSize;
                    if ((totalRows % RatePageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("RateSetUpList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/RateSetUpList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End rsushilaggar
				#endregion


                //initialize Discounts Set Up List view
                #region
                iCounter = 0;
                //Condition added by Tushar for MITS 18231
                sSQL = "SELECT * FROM SYS_POL_SEL_DISC WHERE IS_RATE_SETUP=0 OR IS_RATE_SETUP IS NULL ORDER BY SEL_DISC_ROWID DESC";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                iNoOfRecords = 0;
                objArrayList = null;
                objArrayList = new ArrayList();

                if (objDbReader != null)
                {
                    while (objDbReader.Read())
                    {
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (DisTierCurrentPage - 1) * DisTierPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= DisTierCurrentPage * DisTierPageSize)
                            break;
						// End rsushilaggar 

                        objSelectedDiscount = new SelectedDiscount();

                        objSelectedDiscount.Use = Conversion.ConvertStrToBool(objDbReader["IN_USE_FLAG"].ToString());
                        objSelectedDiscount.LOB = Conversion.CastToType<int>(objDbReader["LINE_OF_BUSINESS"].ToString(), out blnSuccess);
                        objSelectedDiscount.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
                        objSelectedDiscount.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
                        objSelectedDiscount.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
                        objSelectedDiscount.State = Conversion.CastToType<int>(objDbReader["STATE"].ToString(), out blnSuccess);
                        objSelectedDiscount.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
                        objSelectedDiscount.SelDiscRowId = Conversion.CastToType<int> (objDbReader["SEL_DISC_ROWID"].ToString(), out blnSuccess);
                        objSelectedDiscount.DiscountRowid = Conversion.CastToType<int> (objDbReader["DISCOUNT_ROWID"].ToString(), out blnSuccess);
                        objSelectedDiscount.ParentLevel = objDbReader["PARENT_LEVEL"].ToString();
                        objSelectedDiscount.Override = Conversion.ConvertStrToBool(objDbReader["OVERRIDE_FLAG"].ToString());

                        objArrayList.Add(objSelectedDiscount);
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }

                //load list
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("DiscountsSetUpList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("DiscountsSetUpList");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"), false);
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use", "In Use"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discountname", "Discount/Surcharge Name"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob", "LOB"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state", "State"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("applicablelevel", "Applicable Level"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("useredit", "User Edit"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
                objXMLElement.AppendChild(objXMLChildElement);

                for (int i = 0; i < objArrayList.Count; i++)
                {
                    objSelectedDiscount = (SelectedDiscount)objArrayList[i];

                    sApplicableLevel = string.Empty ;
                    if (!string.IsNullOrEmpty(   objSelectedDiscount.ParentLevel ))
                    {
                        sSQL = "SELECT TIER_NAME FROM SYS_POL_DISC_TIER WHERE DISC_TIER_ROWID = " + objSelectedDiscount.ParentLevel;
                        objTempDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                        if (objTempDbReader != null)
                        {
                            if (objTempDbReader.Read())
                                sApplicableLevel = objTempDbReader["TIER_NAME"].ToString();
                            if (objTempDbReader != null)
                                objTempDbReader.Close();
                        }
                    }
                    sDiscountName = string.Empty;
					if(objSelectedDiscount.DiscountRowid != 0)
					{
						sSQL = "SELECT DISCOUNT_NAME FROM SYS_POL_DISCOUNTS WHERE DISCOUNT_ROWID = " + objSelectedDiscount.DiscountRowid;
						objTempDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
						if(objTempDbReader != null)
						{
							if(objTempDbReader.Read())
								sDiscountName = objTempDbReader["DISCOUNT_NAME"].ToString();
							if(objTempDbReader != null)
								objTempDbReader.Close();
						}
					}

					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/DiscountsSetUpList/option[" + (++iCounter).ToString() + "]");                    
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("use",(objSelectedDiscount.Use == true ? "Y" : "N")),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("discountname",sDiscountName),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lob",objLocalCache.GetShortCode(objSelectedDiscount.LOB)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objSelectedDiscount.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("applicablelevel",sApplicableLevel),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("useredit",(objSelectedDiscount.Override == true ? "Y" : "N")),true));
					string sTag = objSelectedDiscount.Use + "," + sDiscountName + "," + objSelectedDiscount.LOB + "," + objSelectedDiscount.State + 
						"," + sApplicableLevel	+ "," + objSelectedDiscount.Override + "," +  objSelectedDiscount.SelDiscRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("seldiscrowid", objSelectedDiscount.SelDiscRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);

                    //Start: Sumit-08/13/2010 - MITS# 20815
                    if (String.IsNullOrEmpty(sAssocDiscounts))
                    {
                        sAssocDiscounts = objSelectedDiscount.DiscountRowid.ToString();
                    }
                    else
                    {
                        sAssocDiscounts += "|" + objSelectedDiscount.DiscountRowid.ToString();
                    }
                    //End:Sumit
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/DiscountsSetUpList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("use"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("discountname"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lob"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("applicablelevel"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("useredit"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("seldiscrowid"), false));
                objXMLElement.AppendChild(objXMLChildElement);

                //Start: Sumit-08/13/2010 - MITS# 20815
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
                objXMLElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("AssocDiscounts", sAssocDiscounts), true));
                //End:Sumit
				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_SEL_DISC WHERE IS_RATE_SETUP=0 OR IS_RATE_SETUP IS NULL ";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / DisTierPageSize;
                    if ((totalRows % DisTierPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("DiscountsSetUpList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/DiscountsSetUpList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				//End - rsushilaggar
				#endregion

				//initialize exp rates list view
				#region
                iCounter = 0;
                objLocalCache = new LocalCache(m_sConnectString, m_iClientId);
                iRateMaintType = objLocalCache.GetCodeId("E", CODE_TABLENAME);
                sSQL = "SELECT * FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE= " + iRateMaintType + "ORDER BY EXP_RATE_ROWID DESC";
				objDbReader = DbFactory.GetDbReader(m_sConnectString,sSQL);
                iNoOfRecords = 0;
				objArrayList = null;
				objArrayList = new ArrayList();

				if(objDbReader != null)
				{
					while(objDbReader.Read())
					{
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (ExpRatesCurrentPage - 1) * ExpRatesPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= ExpRatesCurrentPage * ExpRatesPageSize)
                            break;
						//End rsushilaggar
                        objExposureRate = new ExposureRate(m_iClientId);

						objExposureRate.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
						objExposureRate.Amount  = Conversion.ConvertStrToDouble(objDbReader["AMOUNT"].ToString());
						objExposureRate.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
						objExposureRate.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
						objExposureRate.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
						objExposureRate.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
						objExposureRate.State = Conversion.ConvertStrToInteger(objDbReader["STATE"].ToString());
						objExposureRate.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
						objExposureRate.ExpRateRowId = Conversion.ConvertStrToInteger(objDbReader["EXP_RATE_ROWID"].ToString());
						objExposureRate.ExposureId = Conversion.ConvertStrToInteger(objDbReader["EXPOSURE_ID"].ToString());
						objExposureRate.BaseRate  = Conversion.ConvertStrToDouble(objDbReader["BASE_RATE"].ToString());
						objExposureRate.ExpRateType = Conversion.ConvertStrToInteger(objDbReader["FLAT_OR_PERCENT"].ToString());
						objExposureRate.FixedOrProRate = Conversion.ConvertStrToInteger(objDbReader["FIXED_OR_PRORATE"].ToString());
                        objExposureRate.OrgHierarchy = Conversion.CastToType<int>(objDbReader["ORG_HIERARCHY"].ToString(), out blnSuccess);//Added bY Tushar MITS 18231
                        objExposureRate.LineOFBusiness = Conversion.CastToType<int>(objDbReader["LINE_OF_BUSINESS"].ToString(), out blnSuccess);//Added By tushar MITS 18231

						objArrayList.Add(objExposureRate);
					}
					if(objDbReader != null)
						objDbReader.Close(); 
				}	
			
				//load list
				objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");
			
				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("ExpRatesList"),false);
				objXMLElement.AppendChild(objXMLChildElement);
				objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("ExpRatesList");	

				objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"),false);
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lobcode", "Line of Business"), true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposurecode","Exposure Code"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposuredescription","Exposure Description"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state","State"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("flatorpercent","Flat or Percent"),true));
                //Divya MITS 9230 change Amount to Amount/Rate 04/24/2007
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount","Amount/Rate"),true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("fixedorprorate","Fixed or Pro-Rata"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("baserate","Base Rate"),true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("orghierarchy", "Org. Hierarchy"), true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate","Effective Date"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate","Expiration Date"),true));
				//objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expraterowid","Exp Rate Row Id"),true));
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag","Tag"),true));
				objXMLElement.AppendChild(objXMLChildElement);

				for(int i = 0; i < objArrayList.Count; i++)
				{
					objExposureRate = (ExposureRate)objArrayList[i]; 

					objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"),false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/ExpRatesList/option[" + (++iCounter).ToString() + "]");
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lobcode", objLocalCache.GetShortCode(objExposureRate.LineOFBusiness)), true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposurecode",objLocalCache.GetShortCode(objExposureRate.ExposureId)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposuredescription",objLocalCache.GetCodeDesc(objExposureRate.ExposureId)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state",objLocalCache.GetStateCode(objExposureRate.State)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("flatorpercent",objLocalCache.GetShortCode(objExposureRate.ExpRateType)),true));
                    if (objXMLChildElement.GetElementsByTagName("flatorpercent").Item(0).InnerText == "F")
                   
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", string.Format("{0:C}", objExposureRate.Amount)), true));
                    else
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", objExposureRate.Amount.ToString() + "%"), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("fixedorprorate", objLocalCache.GetShortCode(objExposureRate.FixedOrProRate)), true));

                    //Added bY Tushar MITS 18231
                    objLocalCache.GetOrgInfo(objExposureRate.OrgHierarchy, ref   sShortCode, ref sDesc); 
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("orghierarchy", sShortCode + ' '+ sDesc ), true));
                    //End
                    //Divya 04/25/2007 MITS 9236 -start
                    //Mukul(6/20/04) Commented as base rate is always in percentage
                    //if (objXMLChildElement.GetElementsByTagName("flatorpercent").Item(0).InnerText == "F")
                    //    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("baserate", string.Format("{0:C}", objExposureRate.BaseRate)), true));
                    //else
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("baserate", objExposureRate.BaseRate.ToString() + "%"), true));
			        //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("baserate",string.Format("{0:C}",objExposureRate.BaseRate) + "%"),true));
                    //Divya 04/25/2007 MITS 9236 -end
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate",UTILITY.FormatDate(objExposureRate.EffectiveDate,false)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate",UTILITY.FormatDate(objExposureRate.ExpirationDate,false)),true));
					string sTag = objLocalCache.GetShortCode(objExposureRate.LineOFBusiness) +","+   objExposureRate.ExposureId + "," + objExposureRate.State + "," + objExposureRate.Amount + 
						"," + objExposureRate.ExpRateType + "," + objExposureRate.FixedOrProRate + "," + objExposureRate.BaseRate +  
						"," + UTILITY.FormatDate(objExposureRate.EffectiveDate,false) + "," + UTILITY.FormatDate(objExposureRate.ExpirationDate,false) + "," + objExposureRate.ExpRateRowId;
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expraterowid", objExposureRate.ExpRateRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/ExpRatesList/option[" + (++iCounter).ToString() + "]");

                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lobcode"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("exposurecode"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("exposuredescription"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("flatorpercent"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("fixedorprorate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("orghierarchy"), false));//Added bY Tushar MITS 18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("baserate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expraterowid"), false));
                objXMLElement.AppendChild(objXMLChildElement);
				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE= " + iRateMaintType;
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / ExpRatesPageSize;
                    if ((totalRows % ExpRatesPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("ExpRatesList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/ExpRatesList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				// End rsushilaggar 
				#endregion
                //Added By Tushar MITS 18231
                //initialize Coverage list view
                #region Coverage List
                iCounter = 0;
                iRateMaintType = objLocalCache.GetCodeId("C", CODE_TABLENAME);
                sSQL = "SELECT * FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE= " + iRateMaintType + "ORDER BY EXP_RATE_ROWID DESC";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                iNoOfRecords = 0;
                objArrayList = null;
                objArrayList = new ArrayList();

                if (objDbReader != null)
                {
                    while (objDbReader.Read())
                    {
						// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (CoverageCurrentPage - 1) * CoveragePageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= CoverageCurrentPage * CoveragePageSize)
                            break;
						//End rsushilaggar
                        objExposureRate = new ExposureRate(m_iClientId);

                        objExposureRate.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
                        objExposureRate.Amount = Conversion.CastToType<double>(objDbReader["AMOUNT"].ToString(),out blnSuccess );
                        objExposureRate.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
                        objExposureRate.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
                        objExposureRate.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
                        objExposureRate.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
                        objExposureRate.State = Conversion.CastToType<int>(objDbReader["STATE"].ToString(), out blnSuccess);
                        objExposureRate.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
                        objExposureRate.ExpRateRowId = Conversion.CastToType<int>(objDbReader["EXP_RATE_ROWID"].ToString(), out blnSuccess);
                        objExposureRate.ExposureId = Conversion.CastToType<int>(objDbReader["EXPOSURE_ID"].ToString(), out blnSuccess);
                        objExposureRate.BaseRate = Conversion.CastToType<double>(objDbReader["BASE_RATE"].ToString(), out blnSuccess);
                        objExposureRate.ExpRateType = Conversion.CastToType<int>(objDbReader["FLAT_OR_PERCENT"].ToString(), out blnSuccess);
                        objExposureRate.FixedOrProRate = Conversion.CastToType<int>(objDbReader["FIXED_OR_PRORATE"].ToString(), out blnSuccess);
                        objExposureRate.LineOFBusiness = Conversion.CastToType<int>(objDbReader["LINE_OF_BUSINESS"].ToString(), out blnSuccess);//Added By tushar MITS 18231
                        objExposureRate.OrgHierarchy = Conversion.CastToType<int>(objDbReader["ORG_HIERARCHY"].ToString(), out blnSuccess);//Added bY Tushar MITS 18231

                        objArrayList.Add(objExposureRate);
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }

                //load list
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("CovRatesList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("CovRatesList");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"), false);
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lobcode", "Line of Business"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("coveragecode", "Coverage Code"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("coveragedescription", "Coverage Description"), true));
                // Start:Added by Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", "Amount"), true));
                //End:Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state", "State"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("flatorpercent", "Flat or Percent"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("fixedorprorate", "Fixed or Pro-Rata"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("orghierarchy", "Org. Hierarchy"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate", "Effective Date"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate", "Expiration Date"), true));
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expraterowid","Exp Rate Row Id"),true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
                objXMLElement.AppendChild(objXMLChildElement);

                for (int i = 0; i < objArrayList.Count; i++)
                {
                    objExposureRate = (ExposureRate)objArrayList[i];

                    objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/CovRatesList/option[" + (++iCounter).ToString() + "]");
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lobcode", objLocalCache.GetShortCode(objExposureRate.LineOFBusiness)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("coveragecode", objLocalCache.GetShortCode(objExposureRate.ExposureId)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("coveragedescription", objLocalCache.GetCodeDesc(objExposureRate.ExposureId)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state", objLocalCache.GetStateCode(objExposureRate.State)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("flatorpercent", objLocalCache.GetShortCode(objExposureRate.ExpRateType)), true));
                    if (objXMLChildElement.GetElementsByTagName("flatorpercent").Item(0).InnerText == "F")

                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", string.Format("{0:C}", objExposureRate.Amount)), true));
                    else
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", objExposureRate.Amount.ToString() + "%"), true));

                    //Added bY Tushar MITS 18231
                    objLocalCache.GetOrgInfo(objExposureRate.OrgHierarchy, ref   sShortCode, ref sDesc);
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("orghierarchy", sShortCode + ' ' + sDesc), true));
                    //End

                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("fixedorprorate", objLocalCache.GetShortCode(objExposureRate.FixedOrProRate)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate", UTILITY.FormatDate(objExposureRate.EffectiveDate, false)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate", UTILITY.FormatDate(objExposureRate.ExpirationDate, false)), true));
                    string sTag = objLocalCache.GetShortCode(objExposureRate.LineOFBusiness) + "," + objExposureRate.ExposureId + "," + objExposureRate.State + "," + objExposureRate.Amount +
                        "," + objExposureRate.ExpRateType + "," + objExposureRate.FixedOrProRate + "," + objExposureRate.LineOFBusiness +
                        "," + UTILITY.FormatDate(objExposureRate.EffectiveDate, false) + "," + UTILITY.FormatDate(objExposureRate.ExpirationDate, false) + "," + objExposureRate.ExpRateRowId;
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expraterowid", objExposureRate.ExpRateRowId.ToString()), true));
                    objXMLElement.AppendChild(objXMLChildElement);
                }
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/CovRatesList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lobcode"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("coveragecode"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("coveragedescription"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                // Start:Added by Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                //End:Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("flatorpercent"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("fixedorprorate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("orghierarchy"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expraterowid"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("orghierarchy"), false));//Added bY Tushar MITS 18231

                objXMLElement.AppendChild(objXMLChildElement);
				// rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE= " + iRateMaintType;
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / CoveragePageSize;
                    if ((totalRows % CoveragePageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("CovRatesList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/CovRatesList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
				// End rsushilaggar 
                #endregion Coverage List

                //initialize UAR list view
                #region
                iCounter = 0;
                iRateMaintType = objLocalCache.GetCodeId("U", CODE_TABLENAME);
                sSQL = "SELECT * FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE= " + iRateMaintType + "ORDER BY EXP_RATE_ROWID DESC";
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                iNoOfRecords = 0;
                objArrayList = null;
                objArrayList = new ArrayList();

                if (objDbReader != null)
                {
                    while (objDbReader.Read())
                    {
                        // rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                        if (iNoOfRecords < (UARCurrentPage - 1) * UARPageSize)
                        {
                            iNoOfRecords++;
                            continue;
                        }

                        if (iNoOfRecords >= UARCurrentPage * UARPageSize)
                            break;
						//End rsushilaggar
                        objExposureRate = new ExposureRate(m_iClientId);

                        objExposureRate.AddedByUser = objDbReader["ADDED_BY_USER"].ToString();
                        objExposureRate.Amount = Conversion.CastToType<double >(objDbReader["AMOUNT"].ToString(), out blnSuccess);
                        objExposureRate.DttmRcdAdded = objDbReader["DTTM_RCD_ADDED"].ToString();
                        objExposureRate.DttmRcdLastUpd = objDbReader["DTTM_RCD_LAST_UPD"].ToString();
                        objExposureRate.EffectiveDate = objDbReader["EFFECTIVE_DATE"].ToString();
                        objExposureRate.ExpirationDate = objDbReader["EXPIRATION_DATE"].ToString();
                        objExposureRate.State = Conversion.CastToType<int>(objDbReader["STATE"].ToString(), out blnSuccess);
                        objExposureRate.UpdatedByUser = objDbReader["UPDATED_BY_USER"].ToString();
                        objExposureRate.ExpRateRowId = Conversion.CastToType<int>(objDbReader["EXP_RATE_ROWID"].ToString(), out blnSuccess);
                        objExposureRate.ExposureId = Conversion.CastToType<int>(objDbReader["EXPOSURE_ID"].ToString(), out blnSuccess);
                        objExposureRate.BaseRate = Conversion.CastToType<double>(objDbReader["BASE_RATE"].ToString(), out blnSuccess);
                        objExposureRate.ExpRateType = Conversion.CastToType<int>(objDbReader["FLAT_OR_PERCENT"].ToString(), out blnSuccess);
                        objExposureRate.FixedOrProRate = Conversion.CastToType<int>(objDbReader["FIXED_OR_PRORATE"].ToString(), out blnSuccess);
                        objExposureRate.LineOFBusiness = Conversion.CastToType<int>(objDbReader["LINE_OF_BUSINESS"].ToString(), out blnSuccess);
                        objExposureRate.OrgHierarchy = Conversion.CastToType<int>(objDbReader["ORG_HIERARCHY"].ToString(), out blnSuccess);//Added bY Tushar MITS 18231

                        objArrayList.Add(objExposureRate);
                    }
                    if (objDbReader != null)
                        objDbReader.Close();
                }

                //load list
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("UARRatesList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLElement.SelectSingleNode("UARRatesList");

                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("listhead"), false);
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lobcode", "Line of Business"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposurecode", "Code"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposuredescription", "Description"), true));
				// Start:Added by Nitin Goel,02/19/2010,MITS#18231
				objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", "Amount"), true));
				//End:Nitin Goel,02/19/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state", "State"), true));
				// Start:Commented by Nitin Goel,02/11/2010,MITS#18231
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("orghierarchy", "Org. Hierarchy"), true));
				//objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", "Amount"), true));
				//End:Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("flatorpercent", "Flat or Percent"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("fixedorprorate", "Fixed or Pro-Rata"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("baserate", "Base Rate"), true));
                // Start:Added by Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("orghierarchy", "Org. Hierarchy"), true));
                //End:Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate", "Effective Date"), true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate", "Expiration Date"), true));
                //objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expraterowid","Exp Rate Row Id"),true));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", "Tag"), true));
                objXMLElement.AppendChild(objXMLChildElement);

                for (int i = 0; i < objArrayList.Count; i++)
                {
                    objExposureRate = (ExposureRate)objArrayList[i];

                    objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                    objXMLChildElement.SetAttribute("ref", "/Document/Document/UARRatesList/option[" + (++iCounter).ToString() + "]");
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("lobcode", objLocalCache.GetShortCode(objExposureRate.LineOFBusiness)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposurecode", objLocalCache.GetShortCode(objExposureRate.ExposureId)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("exposuredescription", objLocalCache.GetCodeDesc(objExposureRate.ExposureId)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("state", objLocalCache.GetStateCode(objExposureRate.State)), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("flatorpercent", objLocalCache.GetShortCode(objExposureRate.ExpRateType)), true));
                    if (objXMLChildElement.GetElementsByTagName("flatorpercent").Item(0).InnerText == "F")

                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", string.Format("{0:C}", objExposureRate.Amount)), true));
                    else
                        objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("amount", objExposureRate.Amount.ToString() + "%"), true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("fixedorprorate", objLocalCache.GetShortCode(objExposureRate.FixedOrProRate)), true));
                    //Added bY Tushar MITS 18231
                    objLocalCache.GetOrgInfo(objExposureRate.OrgHierarchy, ref   sShortCode, ref sDesc);
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("orghierarchy", sShortCode + ' ' + sDesc), true));
                    //End
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("baserate", objExposureRate.BaseRate.ToString() + "%"), true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("effectivedate",UTILITY.FormatDate(objExposureRate.EffectiveDate,false)),true));
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expirationdate",UTILITY.FormatDate(objExposureRate.ExpirationDate,false)),true));
                 //Start: Neha Suresh Jain, 04/02/2010, LOB added in sTag.
                    //string sTag = objExposureRate.ExposureId + "," + objExposureRate.State + "," + objExposureRate.Amount +
                    //   "," + objExposureRate.ExpRateType + "," + objExposureRate.FixedOrProRate + "," + objExposureRate.BaseRate +
                    //   "," + UTILITY.FormatDate(objExposureRate.EffectiveDate, false) + "," + UTILITY.FormatDate(objExposureRate.ExpirationDate, false) + "," + objExposureRate.ExpRateRowId;
                    string sTag = objLocalCache.GetShortCode(objExposureRate.LineOFBusiness) + "," + objExposureRate.ExposureId + "," + objExposureRate.State + "," + objExposureRate.Amount + 
						"," + objExposureRate.ExpRateType + "," + objExposureRate.FixedOrProRate + "," + objExposureRate.BaseRate +  
						"," + UTILITY.FormatDate(objExposureRate.EffectiveDate,false) + "," + UTILITY.FormatDate(objExposureRate.ExpirationDate,false) + "," + objExposureRate.ExpRateRowId;
                    //End: Neha Suresh Jain, 04/02/2010
					objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("tag", sTag),true));
                    objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("expraterowid", objExposureRate.ExpRateRowId.ToString()), true));
					objXMLElement.AppendChild(objXMLChildElement);
				}
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("option"), false);
                objXMLChildElement.SetAttribute("ref", "/Document/Document/ExpRatesList/option[" + (++iCounter).ToString() + "]");
                objXMLChildElement.SetAttribute("type", "new");
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("lobcode"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("exposurecode"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("exposuredescription"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("state"), false));
                // Start:Added by Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("amount"), false));
                //End:Nitin Goel,02/11/2010,MITS#18231
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("flatorpercent"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("fixedorprorate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("baserate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("effectivedate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expirationdate"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("tag"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("expraterowid"), false));
                objXMLChildElement.AppendChild(objXMLDocument.ImportNode(UTILITY.GetNewElement("orghierarchy"), false));//Added bY Tushar MITS 18231
                objXMLElement.AppendChild(objXMLChildElement);
                
                // rsushilaggar - Added pagination on the Policy setup grid MITS 22371
                sSQL = "SELECT count(*) Count FROM SYS_POL_EXP_RATES WHERE RATE_MAINTENANCE_TYPE= " + iRateMaintType;
                objDbReader = DbFactory.GetDbReader(m_sConnectString, sSQL);
                totalRows = 0;
                totalPages = 0;
                if (objDbReader.Read())
                {
                    totalRows = Conversion.ConvertStrToInteger(objDbReader["Count"].ToString());
                    totalPages = totalRows / UARCurrentPage;
                    if ((totalRows % UARPageSize) > 0)
                    {
                        totalPages = totalPages + 1;
                    }
                }
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewElement("UARRatesList"), false);
                objXMLElement.AppendChild(objXMLChildElement);
                objXMLElement = (XmlElement)objXMLDocument.SelectSingleNode("Document/Document/UARRatesList");
                objXMLChildElement = (XmlElement)objXMLDocument.ImportNode(UTILITY.GetNewEleWithValue("hdTotalPages", totalPages.ToString()), true);
                objXMLElement.AppendChild(objXMLChildElement);
                //end rsushilaggar
				#endregion
                //End
				return objXMLDocument;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("PolicyOptions.LoadData.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				if(objDbReader != null)
					objDbReader.Dispose();
				if(objTempDbReader != null)
					objTempDbReader.Dispose(); 
				objDiscount = null;
				objPolOptions = null;
				objExpenseConstant = null;
				objDiscountTier = null;
				objThreshold = null;
				objRenewalInformation = null;
				objSelectedDiscount = null;
				objExposureRate = null;

				objArrayList = null;
				objXMLDocument = null;
				objXMLChildElement = null;
				objXMLElement = null;
				if(objLocalCache != null)
					objLocalCache.Dispose();
			}
		}
		#endregion	

		#region "Save"
		/// Name		: Save
		/// Author		: Anurag Agarwal
		/// Date Created	: 6 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Saves Policy Options Record
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///		<Document>
		///			<PolOptRowId></PolOptRowId>
		///			<RoundAmounts>0</RoundAmounts>
		///			<UseYear>1</UseYear>
		///			<Placement>5239</Placement>
		///		</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		public bool Save(XmlDocument p_objInputXMLDoc)
		{
			string sSQL = "";
			DbConnection objCon = null; 
			string sPolOptRowId = "";
			PolOptions objPolOptions = null;
			bool bIsNew = false;
			XmlElement objPolOptXMLEle = null;

			try
			{
				objPolOptXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document"); 	

				sPolOptRowId = objPolOptXMLEle.GetElementsByTagName("PolOptRowId").Item(0).InnerText;

				objPolOptions = new PolOptions();

				if(sPolOptRowId == "0")
				{
					bIsNew = true;
                    objPolOptions.PolOptRowId = Utilities.GetNextUID(m_sConnectString, TABLENAME, m_iClientId);
					objPolOptions.AddedByUser = m_sUserName;
                    objPolOptions.DttmRcdAdded = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
				}
				else
					objPolOptions.PolOptRowId = Conversion.ConvertStrToInteger(sPolOptRowId);

				objPolOptions.UpdatedByUser = m_sUserName;
                objPolOptions.DttmRcdLastUpd = Conversion.GetDateTime(DateTime.Now.ToString("yyyyMMddHHmmss"));  
				if (objPolOptXMLEle.GetElementsByTagName("RoundAmounts").Item(0).InnerText == "True")
					objPolOptions.RoundAmounts=true;
				else
					objPolOptions.RoundAmounts=false;

				if (objPolOptXMLEle.GetElementsByTagName("UseYear").Item(0).InnerText == "True")
					objPolOptions.UseYear = true;
				else
					objPolOptions.UseYear = false;

                //rsushilaggar MITS 21495
                if (objPolOptXMLEle.GetElementsByTagName("AuditPolicy").Item(0).InnerText == "True")
                    objPolOptions.AuditCancelledPolicy = true;
                else
                    objPolOptions.AuditCancelledPolicy = false;



				objPolOptions.Placement = Conversion.ConvertStrToInteger(objPolOptXMLEle.GetElementsByTagName("Placement").Item(0).Attributes["codeid"].Value);

				if(bIsNew)
				{
					sSQL = "INSERT INTO " + TABLENAME + " (POL_OPT_ROWID,ADDED_BY_USER,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD," +
                        "UPDATED_BY_USER,ROUND_AMTS_FLAG,USE_YEAR_FLAG,PLACEMENT,AUDIT_CANCELLED_POLICY) VALUES(" + objPolOptions.PolOptRowId + ",'" + objPolOptions.AddedByUser + "'," + objPolOptions.DttmRcdAdded + "," + 
						objPolOptions.DttmRcdLastUpd + ",'" + objPolOptions.UpdatedByUser + "'," +
                        (objPolOptions.RoundAmounts == true ? 1 : 0) + "," + (objPolOptions.UseYear == true ? 1 : 0) + ",'" + objPolOptions.Placement + "'," + (objPolOptions.AuditCancelledPolicy == true ? 1 : 0) + ")";

				}
				else
				{
					sSQL = "UPDATE " + TABLENAME + " SET " + "DTTM_RCD_LAST_UPD=" + objPolOptions.DttmRcdLastUpd + "," + 
						"UPDATED_BY_USER='" + objPolOptions.UpdatedByUser + "'," + "ROUND_AMTS_FLAG=" + (objPolOptions.RoundAmounts == true ? 1 : 0) + "," + 
						"USE_YEAR_FLAG=" + (objPolOptions.UseYear == true ? 1 : 0) + "," + "PLACEMENT='" + objPolOptions.Placement + "',AUDIT_CANCELLED_POLICY="+ (objPolOptions.AuditCancelledPolicy == true ? 1 : 0)+" WHERE POL_OPT_ROWID=" + objPolOptions.PolOptRowId; 
				}

				objCon = DbFactory.GetDbConnection(m_sConnectString);
				objCon.Open();
				objCon.ExecuteNonQuery(sSQL);

				return true;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("PolicyOptions.Save.Error", m_iClientId),p_objExp);   
			}
			finally
			{
				if(objCon != null)
					objCon.Dispose();

				if(objPolOptions != null)
					objPolOptions = null;
	
				if(objPolOptXMLEle != null)
					objPolOptXMLEle = null;
			}
		}


		/// Name		: Delete
		/// Author		: Anurag Agarwal
		/// Date Created	: 6 April 2005		
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///				   *               *               *
		/// ************************************************************
		/// <summary>
		/// Deletes the Lists records
		/// </summary>
		/// <param name="p_objInputXMLDoc">
		///		The sample input XML will be
		///		<Document>
		///			<DeletionListName></DeletionListName>
		///			<DeletionId></DeletionId>
		///		</Document>
		/// </param>
		/// <returns>True for sucess and false for failure</returns>
		public bool Delete(XmlDocument p_objInputXMLDoc)
		{
			string sListName = "";
			int iListRowId = 0;
			XmlElement objPolOptXMLEle = null;

			DiscountList objDiscountList = null;
			ExpConParms objExpConParms = null;
            TaxParms objTaxParms = null; //npadhy RMSC retrofit Start 
			DiscTier objDiscTier = null;
  			MinBillThreshold objMinBillThreshold = null;
			RenewalInfo objRenewalInfo = null;
			SelectedDisc objSelectedDisc = null;
			ExpRateParms objExpRateParms = null;
            //Tushar:MITS 18231
            int iISRateSetup = 0;

			try
			{
				objPolOptXMLEle = (XmlElement)p_objInputXMLDoc.SelectSingleNode("//Document"); 	

				sListName = objPolOptXMLEle.GetElementsByTagName("DeletionListName").Item(0).InnerText;
				iListRowId = Conversion.ConvertStrToInteger(objPolOptXMLEle.GetElementsByTagName("DeletionId").Item(0).InnerText);
                iISRateSetup = Conversion.ConvertStrToInteger(objPolOptXMLEle.GetElementsByTagName("ISRateSetUp").Item(0).InnerText);

				switch(sListName.ToLower())
				{
					case "discountslist":
						objDiscountList = new DiscountList(m_sConnectString, m_iClientId);
						objDiscountList.Delete(iListRowId);
						break;
					case "expconstlist":
                        objExpConParms = new ExpConParms(m_sUserName, m_sConnectString, m_iClientId);
						objExpConParms.Delete(iListRowId);  
						break;
					case "tierlist":
                        objDiscTier = new DiscTier(m_sConnectString, m_iClientId);
						objDiscTier.Delete(iListRowId);  
						break;
					case "thresholdslist":
                        objMinBillThreshold = new MinBillThreshold(m_sConnectString, m_iClientId);
						objMinBillThreshold.Delete(iListRowId);
						break;
					case "renewalinfolist":
                        objRenewalInfo = new RenewalInfo(m_sConnectString, m_iClientId);
						objRenewalInfo.Delete(iListRowId);
						break;
					case "discountssetuplist":
                        objSelectedDisc = new SelectedDisc(m_sConnectString, m_iClientId);
                        objSelectedDisc.Delete(iListRowId, iISRateSetup);//Added By Tushar MITS 18231
						break;
                    case "ratesetuplist":
                        objSelectedDisc = new SelectedDisc(m_sConnectString, m_iClientId);
                        objSelectedDisc.Delete(iListRowId, iISRateSetup);//Added By Tushar MITS 18231
						break;
					case "exprateslist":
                        objExpRateParms = new ExpRateParms(m_sUserName, m_sConnectString, m_iClientId);
						objExpRateParms.Delete(iListRowId);
						break;
                    //npadhy RMSC retrofit Start 
                    case "policytaxlist":
                        objTaxParms = new TaxParms(m_sUserName, m_sConnectString, m_iClientId);
                        objTaxParms.Delete(iListRowId);
                        break;
                    //npadhy RMSC retrofit Ends
                    case "covrateslist":
                        objExpRateParms = new ExpRateParms(m_sUserName, m_sConnectString, m_iClientId);
                        objExpRateParms.Delete(iListRowId);
                        break;
				}

				return true;
			}
			catch(RMAppException p_objRMExp)
			{
				throw p_objRMExp;
			}
			catch(Exception p_objExp)
			{
				throw new RMAppException(Globalization.GetString("PolicyOptions.Delete.Error", m_iClientId),p_objExp);   
			}
			finally
			{
				if(objPolOptXMLEle != null)
					objPolOptXMLEle = null;
				if(objDiscountList != null)
					objDiscountList = null; 
				if(objExpConParms != null)
					objExpConParms = null;
				if(objDiscTier != null)
					objDiscTier = null;
				if(objMinBillThreshold != null)
					objMinBillThreshold = null;
				if(objRenewalInfo != null)
					objRenewalInfo = null;
				if(objSelectedDisc != null)
					objSelectedDisc = null;
				if(objExpRateParms != null)
					objExpRateParms = null;
			}
		}

		#endregion
        #region rsushilaggar - Added pagination on the Policy setup grid MITS 22371
        /// <summary>
        /// Stores the PageSize of the Term Info Grid
        /// </summary>
        private int m_iTermPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Term Info Grid
        /// </summary>
        private int m_iTermCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Expense Constant Grid
        /// </summary>
        private int m_iExpConstPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Min Bill Threshold Grid
        /// </summary>
        private int m_iExpConstCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Min Bill Threshold Grid
        /// </summary>
        private int m_iMinBillPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Expense Constant Grid
        /// </summary>
        private int m_iMinBillCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Policy Tax Grid
        /// </summary>
        private int m_iPolTaxPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Policy Tax Info Grid
        /// </summary>
        private int m_iPolTaxCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Discount Surcharge Grid
        /// </summary>
        private int m_iDiscountPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Discount Surcharge  Grid
        /// </summary>
        private int m_iDiscountCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Exposure Rate Grid
        /// </summary>
        private int m_iExpRatesPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Exposure Rate Grid
        /// </summary>
        private int m_iExpRatesCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Coverages Grid
        /// </summary>
        private int m_iCoveragePageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Coverages Grid
        /// </summary>
        private int m_iCoverageCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Unit at Risk Grid
        /// </summary>
        private int m_iUARPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Unit at Risk Grid
        /// </summary>
        private int m_iUARCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Tier Grid
        /// </summary>
        private int m_iTierPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Tier Grid
        /// </summary>
        private int m_iTierCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Discount Tier mapping Grid
        /// </summary>
        private int m_iDisTierPageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Discount Tier mapping Grid
        /// </summary>
        private int m_iDisTierCurrentPage = 0;

        /// <summary>
        /// Stores the PageSize of the Rate Grid
        /// </summary>
        private int m_iRatePageSize = 0;

        /// <summary>
        /// Stores the Current Page of the Rate Grid
        /// </summary>
        private int m_iRateCurrentPage = 0;


        //term Info setup

        public int TermPageSize
        {
            get { return m_iTermPageSize; }
            set { m_iTermPageSize = value; }
        }

        public int TermCurrentPage
        {
            get { return m_iTermCurrentPage; }
            set { m_iTermCurrentPage = value; }
        }

        //expense Constant Setup
        public int ExpConstPageSize
        {
            get { return m_iExpConstPageSize; }
            set { m_iExpConstPageSize = value; }
        }

        public int ExpConstCurrentPage
        {
            get { return m_iExpConstCurrentPage; }
            set { m_iExpConstCurrentPage = value; }
        }

        //Minimum Bill Threshhold setup
        public int MinBillPageSize
        {
            get { return m_iMinBillPageSize; }
            set { m_iMinBillPageSize = value; }
        }

        public int MinBillCurrentPage
        {
            get { return m_iMinBillCurrentPage; }
            set { m_iMinBillCurrentPage = value; }
        }

        //Policy Tax Setup
        public int PolTaxPageSize
        {
            get { return m_iPolTaxPageSize; }
            set { m_iPolTaxPageSize = value; }
        }

        public int PolTaxCurrentPage
        {
            get { return m_iPolTaxCurrentPage; }
            set { m_iPolTaxCurrentPage = value; }
        }

        
        //Discount setup
        public int DiscountPageSize
        {
            get { return m_iDiscountPageSize; }
            set { m_iDiscountPageSize = value; }
        }

        public int DiscountCurrentPage
        {
            get { return m_iDiscountCurrentPage; }
            set { m_iDiscountCurrentPage = value; }
        }

        //Coverage Setup
        public int CoveragePageSize
        {
            get { return m_iCoveragePageSize; }
            set { m_iCoveragePageSize = value; }
        }

        public int CoverageCurrentPage
        {
            get { return m_iCoverageCurrentPage; }
            set { m_iCoverageCurrentPage = value; }
        }

        //Unit At risk setup
        public int UARPageSize
        {
            get { return m_iUARPageSize; }
            set { m_iUARPageSize = value; }
        }

        public int UARCurrentPage
        {
            get { return m_iUARCurrentPage; }
            set { m_iUARCurrentPage = value; }
        }

        //Tier setup
        public int TierPageSize
        {
            get { return m_iTierPageSize; }
            set { m_iTierPageSize = value; }
        }

        public int TierCurrentPage
        {
            get { return m_iTierCurrentPage; }
            set { m_iTierCurrentPage = value; }
        }

        //Exposure Rate setup
        public int ExpRatesPageSize
        {
            get { return m_iExpRatesPageSize; }
            set { m_iExpRatesPageSize = value; }
        }

        public int ExpRatesCurrentPage
        {
            get { return m_iExpRatesCurrentPage; }
            set { m_iExpRatesCurrentPage = value; }
        }

        //Discount Tier Setup
        public int DisTierPageSize
        {
            get { return m_iDisTierPageSize; }
            set { m_iDisTierPageSize = value; }
        }

        public int DisTierCurrentPage
        {
            get { return m_iDisTierCurrentPage; }
            set { m_iDisTierCurrentPage = value; }
        }

        //Rate Setup
        public int RatePageSize
        {
            get { return m_iRatePageSize; }
            set { m_iRatePageSize = value; }
        }

        public int RateCurrentPage
        {
            get { return m_iRateCurrentPage; }
            set { m_iRateCurrentPage = value; }
        }

        #endregion




	}

	/// <summary>
	/// Summary description for PolicyOptions.
	/// </summary>
	public class PolOptions
	{
		/// <summary>
		/// Default Construtor
		/// </summary>
		public PolOptions()
		{
		}

		/// <summary>
		/// Stores the PolOptRowId value
		/// </summary>
		private int m_iPolOptRowId = 0;

		/// <summary>
		/// Stores the RoundAmounts value
		/// </summary>
		private bool m_bRoundAmounts = false;

		/// <summary>
		/// Stores the UseYear value
		/// </summary>
		private bool m_bUseYear = false;

		/// <summary>
		/// Stores the Placement value
		/// </summary>
		private int m_iPlacement = 0;

		/// <summary>
		/// Stores the AddedByUser value
		/// </summary>
		private string m_sAddedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdAdded value
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty;

		/// <summary>
		/// Stores the UpdatedByUser value
		/// </summary>
		private string m_sUpdatedByUser = string.Empty;

		/// <summary>
		/// Stores the DttmRcdLastUpd value
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty;

		/// <summary>
		/// Stores the DataChanged value
		/// </summary>
		private bool m_bDataChanged = false;

        //rsushilaggar MITS 21495
        /// <summary>
        /// Stores the AuditCancelledPolicy value
        /// </summary>
        private bool m_bAuditCancelledPolicy = false;



		/// <summary>
		/// Read/Write property for PolOptRowId
		/// </summary>
		public int PolOptRowId
		{
			get
			{
				return m_iPolOptRowId;
			}
			set
			{
				m_iPolOptRowId = value;
			}
		}

		/// <summary>
		/// Read/Write property for RoundAmounts
		/// </summary>
		internal bool RoundAmounts
		{
			get
			{
				return m_bRoundAmounts;
			}
			set
			{
				m_bRoundAmounts = value;
			}
		}

		/// <summary>
		/// Read/Write property for UseYear
		/// </summary>
		internal bool UseYear
		{
			get
			{
				return m_bUseYear;
			}
			set
			{
				m_bUseYear = value;
			}
		}

		/// <summary>
		/// Read/Write property for Placement
		/// </summary>
		internal int Placement
		{
			get
			{
				return m_iPlacement;
			}
			set
			{
				m_iPlacement = value;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser
		/// </summary>
		internal string AddedByUser
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdAdded
		/// </summary>
		internal string DttmRcdAdded
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser
		/// </summary>
		internal string UpdatedByUser
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value;
			}
		}

		/// <summary>
		/// Read/Write property for DttmRcdLastUpd
		/// </summary>
		internal string DttmRcdLastUpd
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value;
			}
		}

		/// <summary>
		/// Read/Write property for DataChanged
		/// </summary>
		internal bool DataChanged
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value;
			}
		}

        //rsushilaggar MITS 21495
        /// <summary>
        /// Read/Write property for AuditCancelledPolicy
        /// </summary>
        internal bool AuditCancelledPolicy
        {
            get
            {
                return m_bAuditCancelledPolicy;
            }
            set
            {
                m_bAuditCancelledPolicy = value;
            }
        }
	}
}
