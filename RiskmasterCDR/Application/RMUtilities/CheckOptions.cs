using System;
using System.Data;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
using Riskmaster.Settings;
//Payal: RMA-8169 --Starts
using System.Linq;
using System.Data.SqlClient;
//Payal: RMA-8169 --Ends

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches checks options.
	/// </summary>
	public class CheckOptions:UtilityFormBase
	{
		
		#region Private Functions
       
		new private void Initialize()
		{
			TableName = "CHECK_OPTIONS";
			KeyField = "";
			try
			{
				base.InitFields(arrFields);
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("CheckOptions.Initialize.Error", m_iClientId),p_objEx);
			}
		}
					
		#endregion

		#region Private variables
		/// <summary>
		/// Fields mapping with database fields
		/// </summary>
		private string[,] arrFields = {
										  //Check Options Setup Table
			{"RollUpChecks","ROLLUP_CHECKS"},								
			{"AllowPostChecks","ALLOW_POST_DATE"},
			{"PrintEOBChecks","PRT_EOB_WITH_CHKS"},
			{"MaxAutoChecksBatch","MAX_AUTO_CHECKS"},
			{"PrntCheckStubEOB","PRINT_STUB_DETAIL"},								
			{"DupPayCheck","DUPLICATE_FLAG"},
			{"ProhibitDupSaveCheck","DO_NOT_SAVE_DUPS"},
			{"PayLimitExceed","SH_PAYMENT"},
			{"PayDetailLimitExceed","SH_PAY_DET"},
			{"PayNotExist","SH_NEW_PAYEE"},
            {"PerClaimPayLimitsExceeded","PER_CLAIM_PAY_LIMITS_EXCEEDED"},
			{"DiaryToManager","SH_DIARY"},
			{"QueuePayment","QUEUED_PAYMENTS"},
			{"UseSupChain","USER_SPECIFIC_FLAG"},
			{"DaysApproval","DAYS_FOR_APPROVAL"},
			{"SpecTranTypeCodes","SH_TRANS_TYPES"},
            	{"SpecTranTypeCodes","SH_TRANS_TYPES"},
                	{"PrintClmtEOB","PRT_CLMNT_EOB"},
                    	{"PrintPayeeEOB","PRT_PAYEE_EOB"},
            {"EOBDescNextLine", "EOB_DESC_NEXT"},
			{"DirectToPrinter","DIRECT_TO_PRINTER"},
            {"PrntAddlCheckDetailsSeparate","PRT_ADDL_CHECK_SEP"},
            //Add by kuladeep for mits:33322 Start
            {"UseHoldReserve","USE_HOLD_RESERVES"},
            //Add by kuladeep for mits:33322 End
            {"UseHoldIncurredLimits","USE_HOLD_INCURRED_LIMITS"},//Jira 6385- Incurred Limit
            //MGaba2:R6 Reserve WorkSheet related settings in Payment Parameter:Start
            {"UseSupAppReserves","USE_SUP_APP_RESERVES"},
			{"DaysApprovalReserves","DAYS_APP_RESERVES"},
            {"HoursApprovalReserves","HOURS_APP_RESERVES"},
			{"NotifySupReserves","NOTIFY_SUP_RESERVES"},
			{"UseCurrAdjReserves","USE_CUR_ADJ_RESERVES"},
            //MGaba2:R6 Reserve WorkSheet related settings in Payment Parameter:End
            //smahajan6: Safeway: Payment Supervisory Approval: Start
            {"HoursApproval","HOURS_FOR_APPROVAL"},
            {"PmtNotfyImmSup","PMT_NOTFY_IMM_SUP"},
            //smahajan6: Safeway: Payment Supervisory Approval: End
            //Start: rsushilaggar 22-Mar-2010 MITS-19624 MITS 20153
            {"DisableEmailNotifyForSuperVsr","DISABLE_EMAIL_NOTIFY_FOR_SUPV"},
            {"DisableDiaryNotifyForSuperVsr","DISABLE_DIARY_NOTIFY_FOR_SUPV"},
            {"DisableEmailNotifyForPayment","DISABLE_EMAIL_NOTIFY_FOR_PYMT"},
            //End : rsushilaggar 22-Mar-2010
            {"DisableDiaryNotifyForUnapprovedEntity","DISABLE_DIARY_NOTIFY_APPROVAL"},//sachin 7810
            //zmohammad MITs 21109 Start
            {"EnableCRDiary","ENABLE_DIARY_NOTIFY_FOR_CR"},
            {"EnableCREmail","ENABLE_EMAIL_NOTIFY_FOR_CR"},
            //zmohammad MITs 21109 End
            //Start: MITS 25716 mcapps2 Print EOB Detail on the check stub
            {"PrintEOBStub","PRINT_EOB_STUB"}, 
            //End: MITS 25716 mcapps2 Print EOB Detail on the check stub
            //Debabrata Biswas: MITS # Date 04/26/2010
            {"LSSPaymentsOnHOLD","LSS_PAYMENT_HOLD"},
            //rsushilaggar: Vendor's Supervisor Approval 05/05/2010 MITS 20606
            //Debabrata Biswas: MITS # Date 04/26/2010
            {"UseSupAppSearch","USE_SUP_APPROVAL_SEARCH"},
            //Debabrata Biswas: MITS # Date 05/06/2010
            {"EnableVoidReasonComment","ENABLE_VOID_REASON"},
            {"CreateUpdateEntity","UPDATE_LSS_ENTITY"},
            //rsushilaggar: Vendor's Supervisor Approval 05/05/2010 MITS 20606
            {"LssCollectionOnHold","LSS_COL_ON_HOLD"},
            //rsushilaggar: Collections from LSS 06/02/2010 MITS 20938
            {"UseSupAppPayments","USE_SUP_APP_PAYMENTS"},
            //Start Ashutosh for Schedule Date settings
            {"AutomaticChecks","SCH_DATE_AUTOCHECK"},
            {"NOCPayments","SCH_DATE_NONOCC_PMT"},
            {"CombPayments","SCH_DATE_COMBINED_PMT"},
             {"checkSchedule","WEEKEND_SCHEDULE"},
            //Start: rsushilaggar 23-NOV-2011 MITS-26332
            {"SupervisoryApproval","USE_SUP_APP_PAYMENTS"},
            {"AccessGrpApprove","ALLOW_SUP_GRP_APPROVE"},
            {"AccessGrpApproveReserve","ALLOW_SUP_GRP_APPROVE_RSV"},
            //End : rsushilaggar 23-NOV-2011
             
            //skhare7 R8 enhancement
            {"MaxPayees","MAX_PAYEES"},
             //skhare7 R8  
             //Ankit Start : Financial Enhancement - Prefix and Suffix Changes
            {"IncludePrefix","INCLUDE_PREFIX"},
            {"IncludeSuffix","INCLUDE_SUFFIX"},
            //Ankit End
            //Deb
            {"LocalPrinter","LOCAL_PRINTER"},
            //Deb
            // npadhy JIRA 6418 Default Distribution Type
            {"DefaultDistributionType","DEF_DSTRBN_TYPE_CODE"}
									  };
		private string[,] arrChilds = null;
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN="";
		/// <summary>
		/// User id
		/// </summary>
		private string m_sUserId="";
		/// <summary>
		/// DSN id
		/// </summary>
		private string m_sDsnId="";
		/// <summary>
		/// Security database DSN
		/// </summary>
		private string m_sSecureDSN="";
        //Added by bsharma33 for Regions Pen Testing MITS 26943
        /// <summary>
        /// Parameter Listing for queries
        /// </summary>
        System.Collections.Generic.Dictionary<string, string> dictParams = new System.Collections.Generic.Dictionary<string, string>();
        //End addiitons by bsharma33 for Regions Pen Testing MITS 26943

        private int m_iClientId = 0; //rkaur27
		#endregion

		#region Properties
		/// <summary>
		/// DSN id
		/// </summary>
		public string DSNID
		{
			set
			{
				m_sDsnId=value;
			}	

		}
		/// <summary>
		/// Connection string
		/// </summary>
		public string DSN
		{
			set
			{
				m_sDSN=value;
			}	

		}
		/// <summary>
		/// Security DSN
		/// </summary>
		public string SecureDSN
		{
			set
			{
				m_sSecureDSN=value;
			}	

		}
		/// <summary>
		/// User id
		/// </summary>
		public string UserId
		{
			set
			{
				m_sUserId=value;
			}	

		}
		#endregion
		
		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
        public CheckOptions(int p_iClientId) : base(p_iClientId) { }
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sDsnId">Dsn id</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public CheckOptions(string p_sDsnId, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) //rkaur27
		{
			try
			{
				ConnectString = p_sConnectionString;
				m_sDSN=p_sConnectionString;
				m_sDsnId=p_sDsnId;
                m_iClientId = p_iClientId; //rkaur27
				m_sSecureDSN=SecurityDatabase.GetSecurityDsn(m_iClientId);//rkaur27
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("CheckOptions.Constructor.Error", m_iClientId),p_objEx);
			}
		}
		#endregion
		
		#region Public Functions
   
        public XmlDocument GetDummyData(XmlDocument p_objXmlDocument)
        {

            XMLDoc = p_objXmlDocument;
             return XMLDoc;
        }
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			string sDsnId=m_sDsnId;
			string sSecureDSN=m_sSecureDSN;
            string sCheckPrintMapping = string.Empty; // npadhy JIRA 6418
            string sDefDstrbnTypeScode = string.Empty;
            string sDefDstrbnTypeDesc = string.Empty;
			XmlElement objElement=null;
			XmlElement objParent=null;
			string sValues="";
			XmlNode objNode=null;
			DbReader objRead=null;
			int iDupNoDays=0;
            int iDirectToPrinter = 0;
            int iPrintClmtEOB = 0;
            int iPrintPayeeEOB = 0;
            int iEOBDescNext = 0;
            int iCheckPaperBin = 0;//Asif
            int iEobPaperBin = 0;//Asif
            string sPrinterName = string.Empty;//Asif
          
			int iCheckToFile=0;
			int iDupPayCriteria=0;
			StringBuilder sbSql=null;
			LocalCache objCache=null;
			XmlElement objRowTxt=null;
			XmlElement objLstRow=null;
			XMLDoc = p_objXmlDocument;
            XmlAttribute Xattribute=null;//Asif
			int iDupFlag=0;
			string sLOB="";
            string sDistributionType = "";
			// npadhy JIRA 6418 Starts
            string sEFTShortCode = "EFT";
            string sManualShortCode = "MAL";
			// npadhy JIRA 6418 Starts
			int iTmp=0;
            //smahajan6: Safeway: Payment Supervisory Approval: Start
            int iDaysApproval = 0;
            int iHoursApproval = 0;
            //smahajan6: Safeway: Payment Supervisory Approval: End
            //MGaba2:R6:Approval settings related to Reserve WorkSheet:Start
            int iDaysAppReserves = 0;
            int iHoursAppReserves = 0;
            //MGaba2:R6:End
            //Start: rsushilaggar 22-Mar-2010 MITS-19624 MITS-20153
            int iDisableEmailNotifyForSuperVsr = 0;
            int iDisableDiaryNotifyForSuperVsr = 0;
            int iDisableEmailNotifyForPayment = 0;
            //Start: rsushilaggar 11/23/2011 MITS-26332
            int iSupervisoryApproval = 0;
            int iAccessGrpApprove = 0;
            int iAccessGrpApproveReserve = 0;
            //End: rsushilaggar 
            int iDisableDiaryNotifyForUnapprovedEntity = 0;            //sachin 7810
            int iEnableCRDiary = 0;
            int iEnableCREmail = 0;

            //Start Ashutosh these variables are included for schedule date functionality
                int iSCH_DATE_AUTOCHECK=0;
                int iSCH_DATE_NONOCC_PMT=0;
                int iSCH_DATE_COMBINED_PMT=0;
                string WEEKEND_SCHEDULE = "";
            //END 
            //skhare7 R8 combined Payment
                int iMaxPayees = 0;
           //Add by kuladeep for  mits:33322
           int iDisableHoldForReserve = 0;
           int iDisableHoldForIncurredLimit = 0;////Jira 6385- Incurred Limit
           int iDefDstrbnType = 0;
           bool bIsSuccess = false; 
			try
			{
				Get();
				//objNode=p_objXmlDocument.SelectSingleNode("//internal[@name='LOB']");Asif  
                objNode = p_objXmlDocument.SelectSingleNode("//LOB");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sLOB=objCache.GetTableId("LINE_OF_BUSINESS").ToString();
				objCache.Dispose();
				sbSql=new StringBuilder();
                //Changed by bsharma33  for Regions Pen Testing  MITS 26943
				sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
				sbSql.Append(string.Format(" WHERE CODES.TABLE_ID ={0}","~sLOB~"));
				sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
				sbSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 ");
				sbSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ");
                dictParams.Add("sLOB",sLOB);
               
				//objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
                objRead = DbFactory.ExecuteReader(base.ConnectString, sbSql.ToString(), dictParams);
                dictParams.Clear(); 
                //End changes by bshrma33 for Regions Pen Testing MITS 26943
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")).Trim()+" "
						+Conversion.ConvertObjToStr(objRead.GetValue("CODE_DESC")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
                // npadhy JIRA 6418 Starts
                //sharishkumar Jira 6418 starts
				
                sbSql = new StringBuilder();
                // npadhy 6418 - Populate the Distribution Type. The Distribution Tyeo for which we already have the Mapping, we do not show them again.
                // Also, we do not need any mapping for EFT and Manual. So removed those from the Query .
                objNode = XMLDoc.SelectSingleNode("//control[@name='lstDistributionType']"); 
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                sDistributionType = objCache.GetTableId("DISTRIBUTION_TYPE").ToString();
                objCache.Dispose();
                sbSql = new StringBuilder();
                
                sbSql.Append("SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES INNER JOIN CODES_TEXT ");
                sbSql.Append("ON CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                sbSql.AppendFormat(" WHERE CODES.TABLE_ID ={0}", "~sDistributionType~");
                // We do not need to provide configuration for EFT and Manual
                sbSql.AppendFormat(" AND CODES.SHORT_CODE NOT IN ({0},{1})", "~sEFTShortCode~", "~sManualShortCode~");
                sbSql.Append(" AND CODES.CODE_ID NOT IN (SELECT DSTRBN_TYPE_CODE FROM CHECK_PRINT_OPTIONS)");
                sbSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0");
                sbSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ");
                dictParams.Add("sDistributionType", sDistributionType);
                dictParams.Add("sEFTShortCode", sEFTShortCode);
                dictParams.Add("sManualShortCode", sManualShortCode);

                objRead = DbFactory.ExecuteReader(base.ConnectString, sbSql.ToString(), dictParams);
                dictParams.Clear();

                while (objRead.Read())
                {
                    objElement = XMLDoc.CreateElement("option");
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
                    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("CODE_DESC")).Trim();
                    objNode.AppendChild(objElement);
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }

                sbSql = new StringBuilder();
                objNode = XMLDoc.SelectSingleNode("//control[@name='lstPrintDistribution']");
                sbSql = new StringBuilder();

                // npadhy JIRA 6418 - Get the available Print Options and its mapping from the CHECK_PRINT_OPTIONS table
                sbSql.Append("SELECT DSTRBN_TYPE_CODE, CT.CODE_DESC DSTRBN_TYPE_NAME, PRINT_OPTION,");
                sbSql.Append("CASE ");
	            sbSql.Append("WHEN PRINT_OPTION = 0 THEN 'To Printer Only' ");
	            sbSql.Append("WHEN PRINT_OPTION = 1 THEN 'To File Only' ");
	            sbSql.Append("WHEN PRINT_OPTION = 2 THEN 'To Printer and File' ");
	            sbSql.Append("WHEN PRINT_OPTION = 3 THEN 'To Custom File Only' ");
	            sbSql.Append("WHEN PRINT_OPTION = 4 THEN 'To Printer and Custom File Only' ");
                sbSql.Append("END PRINT_OPTION_NAME ");
                sbSql.Append("From CHECK_PRINT_OPTIONS CPO INNER JOIN CODES_TEXT CT ");
                sbSql.Append("ON CPO.DSTRBN_TYPE_CODE = CT.CODE_ID");

                objRead = DbFactory.ExecuteReader(base.ConnectString, sbSql.ToString(), dictParams);
                dictParams.Clear();

                while (objRead.Read())
                {
                    objElement = XMLDoc.CreateElement("option");
                    sCheckPrintMapping += Conversion.ConvertObjToStr(objRead.GetValue("DSTRBN_TYPE_CODE")) + "|" + Conversion.ConvertObjToStr(objRead.GetValue("PRINT_OPTION")) + ",";
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue("DSTRBN_TYPE_CODE")) + "|" + Conversion.ConvertObjToStr(objRead.GetValue("PRINT_OPTION")));
                    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("DSTRBN_TYPE_NAME")) + "|" + Conversion.ConvertObjToStr(objRead.GetValue("PRINT_OPTION_NAME"));
                    objNode.AppendChild(objElement);
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }

                objNode = XMLDoc.SelectSingleNode("//control[@name='txtMapping']");
                objNode.InnerText = sCheckPrintMapping;

                //sharishkumar Jira 6418 ends
                sbSql = new StringBuilder();
				//smahajan6: Safeway: Payment Supervisory Approval: Start
                //sbSql.Append(" SELECT DUP_NUM_DAYS,CHECK_TO_FILE,DUP_PAY_CRITERIA,DUPLICATE_FLAG,DAYS_FOR_APPROVAL,HOURS_FOR_APPROVAL FROM CHECK_OPTIONS ");
                //sbSql.Append(" SELECT DUP_NUM_DAYS,CHECK_PAPER_BIN,EOB_PAPER_BIN,CHECK_TO_FILE,DUP_PAY_CRITERIA,DUPLICATE_FLAG FROM CHECK_OPTIONS ");//Asif
                //MGaba2:R6 :Added for Approval settings related to Reserve WorkSheet                
                //sbSql.Append(" SELECT DUP_NUM_DAYS,CHECK_PAPER_BIN,EOB_PAPER_BIN,PRINTER_NAME,CHECK_TO_FILE,DUP_PAY_CRITERIA,DUPLICATE_FLAG,DIRECT_TO_PRINTER,PRT_PAYEE_EOB,PRT_CLMNT_EOB,DAYS_FOR_APPROVAL,HOURS_FOR_APPROVAL FROM CHECK_OPTIONS ");
                //MCIC Batch Printing Filter MITS 20050 Date: 04/22/2010. Added PRE_CHK_ORG_LEVEL to the query
                //zmohammad MITs 21109
                ////Jira 6385- Incurred Limit Start
                //sbSql.Append(" SELECT MAX_PAYEES, DUP_NUM_DAYS,CHECK_PAPER_BIN,EOB_PAPER_BIN,PRINTER_NAME,CHECK_TO_FILE,DUP_PAY_CRITERIA,DUPLICATE_FLAG,DIRECT_TO_PRINTER,PRT_PAYEE_EOB,PRT_CLMNT_EOB,DAYS_FOR_APPROVAL,HOURS_FOR_APPROVAL,DAYS_APP_RESERVES,HOURS_APP_RESERVES,DISABLE_EMAIL_NOTIFY_FOR_SUPV,DISABLE_DIARY_NOTIFY_FOR_SUPV,DISABLE_EMAIL_NOTIFY_FOR_PYMT,PRE_CHK_ORG_LEVEL,SCH_DATE_AUTOCHECK,SCH_DATE_NONOCC_PMT,SCH_DATE_COMBINED_PMT,WEEKEND_SCHEDULE,USE_SUP_APP_PAYMENTS,ALLOW_SUP_GRP_APPROVE,ALLOW_SUP_GRP_APPROVE_RSV,USE_HOLD_RESERVES,EOB_DESC_NEXT,ENABLE_EMAIL_NOTIFY_FOR_CR,ENABLE_DIARY_NOTIFY_FOR_CR  FROM CHECK_OPTIONS ");
                sbSql.Append(" SELECT MAX_PAYEES, DUP_NUM_DAYS,CHECK_PAPER_BIN,EOB_PAPER_BIN,PRINTER_NAME,CHECK_TO_FILE,DUP_PAY_CRITERIA,DUPLICATE_FLAG,DIRECT_TO_PRINTER,PRT_PAYEE_EOB,PRT_CLMNT_EOB,DAYS_FOR_APPROVAL,HOURS_FOR_APPROVAL,DAYS_APP_RESERVES,HOURS_APP_RESERVES,DISABLE_EMAIL_NOTIFY_FOR_SUPV,DISABLE_DIARY_NOTIFY_FOR_SUPV,DISABLE_EMAIL_NOTIFY_FOR_PYMT,PRE_CHK_ORG_LEVEL,SCH_DATE_AUTOCHECK,SCH_DATE_NONOCC_PMT,SCH_DATE_COMBINED_PMT,WEEKEND_SCHEDULE,USE_SUP_APP_PAYMENTS,ALLOW_SUP_GRP_APPROVE,ALLOW_SUP_GRP_APPROVE_RSV,USE_HOLD_RESERVES,EOB_DESC_NEXT,ENABLE_EMAIL_NOTIFY_FOR_CR,ENABLE_DIARY_NOTIFY_FOR_CR,USE_HOLD_INCURRED_LIMITS, DEF_DSTRBN_TYPE_CODE,DISABLE_DIARY_NOTIFY_APPROVAL  FROM CHECK_OPTIONS ");//sachin 7810
                ////Jira 6385- Incurred Limit End
                //rsushilaggar: 22 Mar 2010 MITS-19624 MITS-20153
                //MGaba2:R6:End
                //smahajan6: Safeway: Payment Supervisory Approval: End
                

				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				if (objRead.Read())
				{
                    iDupNoDays = Conversion.ConvertObjToInt(objRead.GetValue("DUP_NUM_DAYS"), m_iClientId);
                    //Changed by Gagan for MITS 11461 : Start
                    iCheckPaperBin = Conversion.ConvertObjToInt(objRead.GetValue("CHECK_PAPER_BIN"), m_iClientId);
                    iEobPaperBin = Conversion.ConvertObjToInt(objRead.GetValue("EOB_PAPER_BIN"), m_iClientId);
                    sPrinterName = Conversion.ConvertObjToStr(objRead.GetValue("PRINTER_NAME"));
                    iPrintClmtEOB = Conversion.ConvertObjToInt(objRead.GetValue("PRT_CLMNT_EOB"), m_iClientId);
                    iDirectToPrinter = Conversion.ConvertObjToInt(objRead.GetValue("DIRECT_TO_PRINTER"), m_iClientId);
					iEOBDescNext = Conversion.ConvertObjToInt(objRead.GetValue("EOB_DESC_NEXT"), m_iClientId);
                    iPrintPayeeEOB = Conversion.ConvertObjToInt(objRead.GetValue("PRT_PAYEE_EOB"), m_iClientId);
                    //Changed by Gagan for MITS 11461 : End
                    iCheckToFile = Conversion.ConvertObjToInt(objRead.GetValue("CHECK_TO_FILE"), m_iClientId);
                    iDupPayCriteria = Conversion.ConvertObjToInt(objRead.GetValue("DUP_PAY_CRITERIA"), m_iClientId);
                    iDupFlag = Conversion.ConvertObjToInt(objRead.GetValue("DUPLICATE_FLAG"), m_iClientId);
                    //smahajan6: Safeway: Payment Supervisory Approval: Start
                    iHoursApproval = Conversion.ConvertObjToInt(objRead.GetValue("HOURS_FOR_APPROVAL"), m_iClientId);
                    iDaysApproval = Conversion.ConvertObjToInt(objRead.GetValue("DAYS_FOR_APPROVAL"), m_iClientId);
                    //smahajan6: Safeway: Payment Supervisory Approval: End
                    //MGaba2:R6:Approval settings related to Reserve WorkSheet:Start
                    iDaysAppReserves = Conversion.ConvertObjToInt(objRead.GetValue("DAYS_APP_RESERVES"), m_iClientId);
                    iHoursAppReserves = Conversion.ConvertObjToInt(objRead.GetValue("HOURS_APP_RESERVES"), m_iClientId);
                    //MGaba2:R6:End
                    //Start: rsushilaggar : 22-Mar-2010 MITS-19624 MITS-20153
                    iDisableEmailNotifyForSuperVsr = Conversion.ConvertObjToInt(objRead.GetValue("DISABLE_EMAIL_NOTIFY_FOR_SUPV"), m_iClientId);
                    iDisableDiaryNotifyForSuperVsr = Conversion.ConvertObjToInt(objRead.GetValue("DISABLE_DIARY_NOTIFY_FOR_SUPV"), m_iClientId);
                    iDisableEmailNotifyForPayment = Conversion.ConvertObjToInt(objRead.GetValue("DISABLE_EMAIL_NOTIFY_FOR_PYMT"), m_iClientId);
                    //End: rsushilaggar
                    iDisableDiaryNotifyForUnapprovedEntity = Conversion.ConvertObjToInt(objRead.GetValue("DISABLE_DIARY_NOTIFY_APPROVAL"), m_iClientId);//sachin 7810
                    //Start: rsushilaggar : 23-Nov-2011 MITS-26332
                    iSupervisoryApproval = Conversion.ConvertObjToInt(objRead.GetValue("USE_SUP_APP_PAYMENTS"), m_iClientId);
                    iAccessGrpApprove = Conversion.ConvertObjToInt(objRead.GetValue("ALLOW_SUP_GRP_APPROVE"), m_iClientId);
                    iAccessGrpApproveReserve = Conversion.ConvertObjToInt(objRead.GetValue("ALLOW_SUP_GRP_APPROVE_RSV"), m_iClientId);
                    //End: rsushilaggar

                    //zmohammad MITs 21109
                    iEnableCRDiary = Conversion.ConvertObjToInt(objRead.GetValue("ENABLE_DIARY_NOTIFY_FOR_CR"), m_iClientId);
                    iEnableCREmail = Conversion.ConvertObjToInt(objRead.GetValue("ENABLE_EMAIL_NOTIFY_FOR_CR"), m_iClientId);

                    //Start Ashutosh
                    iSCH_DATE_AUTOCHECK = Conversion.ConvertObjToInt(objRead.GetValue("SCH_DATE_AUTOCHECK"), m_iClientId);
                    iSCH_DATE_NONOCC_PMT = Conversion.ConvertObjToInt(objRead.GetValue("SCH_DATE_NONOCC_PMT"), m_iClientId);
                    iSCH_DATE_COMBINED_PMT = Conversion.ConvertObjToInt(objRead.GetValue("SCH_DATE_COMBINED_PMT"), m_iClientId);
                     WEEKEND_SCHEDULE = Conversion.ConvertObjToStr(objRead.GetValue("WEEKEND_SCHEDULE"));

                    //End 
                    //skhare7 r8 Combined Payment
                     iMaxPayees = Conversion.ConvertObjToInt(objRead.GetValue("MAX_PAYEES"), m_iClientId);
                    //Add by kuladeep for mits:33322
                    
                     iDisableHoldForReserve = Conversion.CastToType<int>(Convert.ToString((objRead.GetValue("USE_HOLD_RESERVES"))), out bIsSuccess);
                     iDisableHoldForIncurredLimit = Conversion.CastToType<int>(Convert.ToString((objRead.GetValue("USE_HOLD_INCURRED_LIMITS"))), out bIsSuccess);////Jira 6385- Incurred Limit
                     iDefDstrbnType = Conversion.CastToType<int>(Convert.ToString((objRead.GetValue("DEF_DSTRBN_TYPE_CODE"))), out bIsSuccess);////Jira 6418 - Distribution Type
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
                //START ASHUTOSH for Schedule Date
                   objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AutomaticChecks']");
                   if (iSCH_DATE_AUTOCHECK == 1 || iSCH_DATE_AUTOCHECK == -1)
                   {
                       objNode.InnerText = "True";
                   }
                   else 
                   {
                       objNode.InnerText = "False";
                   }
                   objNode = p_objXmlDocument.SelectSingleNode("//control[@name='NOCPayments']");
                   if (iSCH_DATE_NONOCC_PMT == 1 || iSCH_DATE_NONOCC_PMT == -1)
                   {
                       objNode.InnerText = "True";
                   }
                   else
                   {
                       objNode.InnerText = "False";
                   }
                   objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CombPayments']");
                   if (iSCH_DATE_COMBINED_PMT == 1 || iSCH_DATE_COMBINED_PMT == -1)
                   {
                       objNode.InnerText = "True";
                   }
                   else
                   {
                       objNode.InnerText = "False";
                   }

                   objNode = p_objXmlDocument.SelectSingleNode("//group/checkSchedule");
                    if (WEEKEND_SCHEDULE != "")
                    {
                        objNode.InnerText = WEEKEND_SCHEDULE;
                    }
             
                //END
                //smahajan6: Safeway: Payment Supervisory Approval: Start
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TimeInterval']");
                if (iHoursApproval != 0)
                {
                    objElement = (XmlElement)objNode.SelectSingleNode("./option[@value='1']");
                    objElement.SetAttribute("selected", "1");
                    p_objXmlDocument.SelectSingleNode("//control[@name='DaysApproval']").InnerText = iHoursApproval.ToString();
                }
                else
                {
                    objElement = (XmlElement)objNode.SelectSingleNode("./option[@value='0']");
                    objElement.SetAttribute("selected", "1");
                    p_objXmlDocument.SelectSingleNode("//control[@name='DaysApproval']").InnerText = iDaysApproval.ToString();
                }
                //smahajan6: Safeway: Payment Supervisory Approval: End

                //MGaba2:R6:Approval settings related to Reserve WorkSheet:Start
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TimeIntervalAppRes']");
                if (iHoursAppReserves != 0)
                {
                    objElement = (XmlElement)objNode.SelectSingleNode("./option[@value='1']");
                    objElement.SetAttribute("selected", "1");
                    p_objXmlDocument.SelectSingleNode("//control[@name='DaysApprovalReserves']").InnerText = iHoursAppReserves.ToString();
                }
                else
                {
                    objElement = (XmlElement)objNode.SelectSingleNode("./option[@value='0']");
                    objElement.SetAttribute("selected", "1");
                    p_objXmlDocument.SelectSingleNode("//control[@name='DaysApprovalReserves']").InnerText = iDaysAppReserves.ToString();
                }
                //MGaba2:R6:End

                //Start: rsushilaggar 22-Mar-2010 MITS-19624 MITS-20153
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DisableEmailNotifyForPayment']");
                if (iDisableEmailNotifyForPayment == -1 || iDisableEmailNotifyForPayment == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
               
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DisableEmailNotifyForSuperVsr']");
                if (iDisableEmailNotifyForSuperVsr == -1 || iDisableEmailNotifyForSuperVsr == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DisableDiaryNotifyForSuperVsr']");
                if (iDisableDiaryNotifyForSuperVsr == -1 || iDisableDiaryNotifyForSuperVsr == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                //End: rsushilaggar 22-Mar-2010

                //sachin 7810
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DisableDiaryNotifyForUnapprovedEntity']");
                if (iDisableDiaryNotifyForUnapprovedEntity == -1 || iDisableDiaryNotifyForUnapprovedEntity == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                //sachin 7810

                //zmohammad MITs 21109
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='EnableCRDiary']");
                if (iEnableCRDiary == -1 || iEnableCRDiary == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='EnableCREmail']");
                if (iEnableCREmail == -1 || iEnableCREmail == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }

                //Add by kuladeep for mits:33322 Start
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='UseHoldReserve']");
                if (iDisableHoldForReserve == -1 || iDisableHoldForReserve == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                //Add by kuladeep for mits:33322 End

                ////Jira 6385- Incurred Limit Start
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='UseHoldIncurredLimits']");
                if (iDisableHoldForIncurredLimit == -1 || iDisableHoldForIncurredLimit == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                ////Jira 6385- Incurred Limit End

                //Start: rsushilaggar 23-Nov-2011 MITS-26332
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='SupervisoryApproval']");
                if (iSupervisoryApproval == -1 || iSupervisoryApproval == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AccessGrpApprove']");
                if (iAccessGrpApprove == -1 || iAccessGrpApprove == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AccessGrpApproveReserve']");
                if (iAccessGrpApproveReserve == -1 || iAccessGrpApproveReserve == 1)
                {
                    objNode.InnerText = "True";
                }
                else
                {
                    objNode.InnerText = "";
                }
                //Changed by Gagan for MITS 11461 : Start
				//Start Change code by parag sep 05
                //if (iCheckPaperBin==0)
                //    iCheckPaperBin=1;
                //if (iEobPaperBin==0)
                //    iEobPaperBin=1;
				//End Change code by parag sep 05
                //Changed by Gagan for MITS 11461 : End
                //Asif Added Code for Print Settings Start
                objNode = p_objXmlDocument.SelectSingleNode("//PrinterName");
                objNode.InnerText = sPrinterName;
                objNode = p_objXmlDocument.SelectSingleNode("//PaperBinForChecks");
                objNode.InnerText =iCheckPaperBin.ToString();
                objNode = p_objXmlDocument.SelectSingleNode("//PaperBinForEobs");
                objNode.InnerText = iEobPaperBin.ToString();
                //Asif Added Code for Print Settings End

				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='DupPayCheck']");  
				if (iDupFlag==-1 || iDupFlag==1)
				{
					objNode.InnerText="True";
				}
				else
				{
					objNode.InnerText="";
				}
                

                //Changed by Gagan for MITS 11461 : Start
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='PaperBinChecks']");  
                //objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+
                //    iCheckPaperBin.ToString()+"']");
                //objElement.SetAttribute("selected","1");

                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='PaperBinEOB']");  
                //objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+
                //    iEobPaperBin.ToString()+"']");
                //objElement.SetAttribute("selected","1");
                //Changed by Gagan for MITS 11461 : End

                // npadhy Jira 6418 - Comment the Existing control of Print to File Option.
                //objNode=p_objXmlDocument.SelectSingleNode("//control[@name='PrntChecktoFile']");  
                //objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+
                //    iCheckToFile.ToString()+"']");
                //objElement.SetAttribute("selected","1");
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DefaultDistributionType']");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                objCache.GetCodeInfo(iDefDstrbnType, ref sDefDstrbnTypeScode, ref sDefDstrbnTypeDesc);
                objCache.Dispose();
                objNode.Attributes["codeid"].Value = iDefDstrbnType.ToString();
                objNode.InnerText = String.Format("{0} {1}", sDefDstrbnTypeScode, sDefDstrbnTypeDesc);
                

				sbSql=new StringBuilder();
				//objNode=XMLDoc.SelectSingleNode("//internal[@name='UserNotify']"); Asif
                objNode = XMLDoc.SelectSingleNode("//form/UserNotify");
				sbSql=new StringBuilder();
                //Changed by bsharma33  for Regions Pen Testing  MITS 26943
                dictParams.Clear();
				sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ");
				sbSql.Append(" FROM USER_DETAILS_TABLE,USER_TABLE ");
				sbSql.Append(string.Format(" WHERE USER_DETAILS_TABLE.DSNID = {0}","~sDsnId~"));
				sbSql.Append(" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ");
                dictParams.Add("sDsnId", sDsnId);
				//objRead=DbFactory.GetDbReader(sSecureDSN,sbSql.ToString());
                objRead = DbFactory.ExecuteReader(sSecureDSN, sbSql.ToString(), dictParams);
                dictParams.Clear();
                //End changes by bsharma33  for Regions Pen Testing  MITS 26943
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("LAST_NAME")).Trim()+", "
						+Conversion.ConvertObjToStr(objRead.GetValue("FIRST_NAME")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
			
					
				objNode=XMLDoc.SelectSingleNode("//control[@name='TransCodes']");
                objNode.Attributes.RemoveAt(3);
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT TRANS_TYPE_CODE FROM SUP_HOLD_TRANS ");
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
                objCache = new LocalCache(base.ConnectString, m_iClientId);
                string strTransCodes = string.Empty;//Asif
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("Item");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("TRANS_TYPE_CODE")));
                    strTransCodes = strTransCodes + " " + Conversion.ConvertObjToStr(objRead.GetValue("TRANS_TYPE_CODE"));//Asif

               
					objElement.InnerText=UTILITY.GetCode(Conversion.ConvertObjToStr(objRead.GetValue("TRANS_TYPE_CODE")),base.ConnectString,m_iClientId);
					objNode.AppendChild(objElement);
				}

                Xattribute = XMLDoc.CreateAttribute("codeid");//Asif
                Xattribute.InnerText = strTransCodes;//Asif
                objNode.Attributes.Append(Xattribute);//Asif


				objCache.Dispose();
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				
				//objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/group[1]");Asif
                objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/DupNoOfDays");  
				//objElement = (XmlElement)p_objXmlDocument.CreateElement("DupNoOfDays");Asif  
				//objElement.InnerText=iDupNoDays.ToString();Asif
                objParent.InnerText = iDupNoDays.ToString();
				//objParent.AppendChild(objElement);Asif
             
				objNode=XMLDoc.SelectSingleNode("//control[@name='DupCriteria']"); 
                // akaushik5 Chaged For MITS 37111 Starts
                //SortedList objList = new SortedList();
                //objList.Add(0, "0 - No Additional Criteria");
                //if (iDupNoDays > 0)
                //    objList.Add(1, "1 - Transaction Date within " + iDupNoDays.ToString() + " Days");
                //else
                //    objList.Add(1, "1 - Transaction Date within X Days");
                //objList.Add(2, "2 - Payment Total");
                //objList.Add(3, "3 - From and To Date Range");
                //objList.Add(4, "4 - Split Amount and Transaction Type");
                //if (iDupNoDays > 0)
                //{
                //    objList.Add(5, "5 - Payment Total and Trans Date within " + iDupNoDays.ToString() + " Days");
                //    objList.Add(6, "6 - Split Amt, Trans Type, & Trans Date within " + iDupNoDays.ToString() + " Days");
                //}
                //else
                //{
                //    objList.Add(5, "5 - Payment Total and Trans Date within X Days");
                //    objList.Add(6, "6 - Split Amt, Trans Type, & Trans Date within X Days");
                //}
                //objList.Add(7, "7 - Claims with same claimant SSN & From - To Date Range");
                ////Geeta 01/29/07 : MITS 10485
                //if (iDupNoDays > 0)
                //{
                //    objList.Add(8, "8 - Payment Total, Invoice Number, and Trans Date within " + iDupNoDays.ToString() + " Days");
                //    objList.Add(12, "12 - Payment Total, Invoice Number, Split Amt and Trans Date within " + iDupNoDays.ToString() + " Days");
                //}
                //else
                //{
                //    objList.Add(8, "8 - Payment Total, Invoice Number, and Trans Date within X Days");
                //    objList.Add(12, "12 - Payment Total, Invoice Number, Split Amt and Trans Date within X Days");
                //}
                //objList.Add(9, "9 - Invoice Number per Payee");
                //objList.Add(10, "10 - Claims with same Claimant SSN, Amount, From - To Date Range, and Invoice Number");
                //objList.Add(11, "11 - Claims with same (Payee Name or SSN) and (Overlapping Date Range Or Invoice #.)");
                //objList.Add(13, "13 - Claims with same Payee SSN, Amount, From - To Date Range, and Invoice Number");
                //objList.Add(14, "14 - Current Claim with same Claimant SSN, Amount, From - To Date Range, and Invoice Number");
                //objList.Add(15, "15 - Current Claim with same (Payee Name or SSN) and (Overlapping Date Range Or Invoice #.)");
                SortedList objList = CheckOptions.GetDuplicateCriteriaList(iDupNoDays);
                // akaushik5 Chaged For MITS 37111 Ends

				IDictionaryEnumerator objEnum=objList.GetEnumerator();
				while (objEnum.MoveNext())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",objEnum.Key.ToString());
					objElement.InnerText=objEnum.Value.ToString();
					objNode.AppendChild(objElement);
				}

				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='DupCriteria']");  
				objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+
					iDupPayCriteria.ToString()+"']");
				objElement.SetAttribute("selected","1");
			
				//objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/group[1]"); Asif
                objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/NotifyUsers"); 
                
				//objElement = (XmlElement)p_objXmlDocument.CreateElement("NotifyUsers");
                //Asif Checked For PaymentNotification node
                XmlElement xPaymentNotification =(XmlElement) p_objXmlDocument.SelectSingleNode("//form/NotifyUsers/PaymentNotification");
                if (xPaymentNotification != null)
                {
                    //p_objXmlDocument.RemoveChild(xPaymentNotification);
                   xPaymentNotification.RemoveAll();
               
                }

                objElement = (XmlElement)p_objXmlDocument.CreateElement("PaymentNotification");
				objParent.AppendChild(objElement);
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT * FROM PAY_NOTIFY_USERS ");
			
		
				objLstRow = p_objXmlDocument.CreateElement("listhead");

				//objRowTxt = p_objXmlDocument.CreateElement("rowhead");
                objRowTxt = p_objXmlDocument.CreateElement("UNotify");
				objRowTxt.SetAttribute("colname" , "UNotify");
				objRowTxt.InnerText="User to Notify";
				objLstRow.AppendChild(objRowTxt);

				//objRowTxt = p_objXmlDocument.CreateElement("rowhead");
                objRowTxt = p_objXmlDocument.CreateElement("LBusiness");
				objRowTxt.SetAttribute("colname" , "LBusiness");
				objRowTxt.InnerText="Line of Business";
				objLstRow.AppendChild(objRowTxt);

				//objRowTxt = p_objXmlDocument.CreateElement("rowhead");
                objRowTxt = p_objXmlDocument.CreateElement("NPeriod");
				objRowTxt.SetAttribute("colname" , "NPeriod");
				objRowTxt.InnerText="Notify Period";
				objLstRow.AppendChild(objRowTxt);

				//objRowTxt = p_objXmlDocument.CreateElement("rowhead");
                objRowTxt = p_objXmlDocument.CreateElement("DCriteria");
				objRowTxt.SetAttribute("colname" , "DCriteria");
				objRowTxt.InnerText="Date Criteria";
				objLstRow.AppendChild(objRowTxt);
			
							
			
				objElement.AppendChild(objLstRow);

                objCache = new LocalCache(base.ConnectString, m_iClientId);
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objLstRow = p_objXmlDocument.CreateElement("User");
					
					objRowTxt = p_objXmlDocument.CreateElement("UNotify");
                        //objNode=p_objXmlDocument.SelectSingleNode("//internal[@name='UserNotify']");  Asif
                        objNode = p_objXmlDocument.SelectSingleNode("//form/UserNotify");
					objNode=objNode.SelectSingleNode("./option[@value='"+
						Conversion.ConvertObjToStr(objRead.GetValue("USER_ID"))+"']");
					if (objNode!=null)
					{
						objRowTxt.InnerText=objNode.InnerText;
					}
					else
					{
						objRowTxt.InnerText="";
					}
					objLstRow.AppendChild(objRowTxt);
				
				
					sValues=sValues+Conversion.ConvertObjToStr(objRead.GetValue("USER_ID"))+"|";
					objRowTxt = p_objXmlDocument.CreateElement("LBusiness");
					objRowTxt.InnerText=objCache.GetShortCode(Conversion.ConvertObjToInt(
                        objRead.GetValue("LINE_OF_BUS_CODE"), m_iClientId)) + " - " +
						objCache.GetCodeDesc(Conversion.ConvertObjToInt(
                        objRead.GetValue("LINE_OF_BUS_CODE"), m_iClientId));
					objLstRow.AppendChild(objRowTxt);
					sValues=sValues+Conversion.ConvertObjToStr(objRead.GetValue("LINE_OF_BUS_CODE"))+"|";
                    iTmp = Conversion.ConvertObjToInt(objRead.GetValue("NOTIFY_TYPE"), m_iClientId) % 10;	
					objRowTxt = p_objXmlDocument.CreateElement("NPeriod");
                        //objNode=p_objXmlDocument.SelectSingleNode("//internal[@name='NotifyPeriod']"); Asif
                        objNode = p_objXmlDocument.SelectSingleNode("//form/NotifyPeriod");
					objNode=objNode.SelectSingleNode("./option[@value='"+
						iTmp.ToString()+"']");
					if (objNode!=null)
					{
						objRowTxt.InnerText=objNode.InnerText;
					}
					else
					{
						objRowTxt.InnerText="";
					}
				
					objLstRow.AppendChild(objRowTxt);
					sValues=sValues+Conversion.ConvertObjToStr(iTmp.ToString())+"|";
					objRowTxt = p_objXmlDocument.CreateElement("DCriteria");
                    if (Conversion.ConvertObjToInt(objRead.GetValue("NOTIFY_TYPE"), m_iClientId) >= 30)
					{
						objRowTxt.InnerText="by end of month";
						sValues=sValues+"30|";
					}
                    else if (Conversion.ConvertObjToInt(objRead.GetValue("NOTIFY_TYPE"), m_iClientId) >= 20)
					{
						objRowTxt.InnerText="by end of week";
						sValues=sValues+"20|";
					}
					else
					{
                           // objRowTxt.InnerText = "<= current date";//Asif
						objRowTxt.InnerText="less/equal current date";
						sValues=sValues+"10|";
					}
				
					objLstRow.AppendChild(objRowTxt);
					

					objRowTxt = p_objXmlDocument.CreateElement("RecordId");
					
					objRowTxt.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("RECORD_ID"));
				
					sValues=sValues+objRowTxt.InnerText+"**";
					objLstRow.AppendChild(objRowTxt);

					objElement.AppendChild(objLstRow);
				}
                //Asif Start
                //For Grid
                objLstRow = p_objXmlDocument.CreateElement("User");
                objLstRow.SetAttribute("type", "new");
                objRowTxt = p_objXmlDocument.CreateElement("UNotify");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);

                objRowTxt = p_objXmlDocument.CreateElement("LBusiness");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);

                objRowTxt = p_objXmlDocument.CreateElement("NPeriod");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);

                objRowTxt = p_objXmlDocument.CreateElement("DCriteria");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);

                objRowTxt = p_objXmlDocument.CreateElement("RecordId");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);


                objElement.AppendChild(objLstRow);
                //Asif End


				objCache.Dispose();

				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				//objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/group[1]"); Asif  
                objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/PaymentNotifyValues"); 
				//objElement = (XmlElement)p_objXmlDocument.CreateElement("PaymentNotifyValues");  //Asif
				//objElement.InnerText=sValues.ToString();//Asif
                objParent.InnerText =sValues.ToString();
				//objParent.AppendChild(objElement);//Asif
				//new code
				//				objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='NotifyList']");  
				//				
				//				sbSql=new StringBuilder();
				//				sbSql.Append(" SELECT * FROM PAY_NOTIFY_USERS ");
				//			
				//		
				//				objLstRow = p_objXmlDocument.CreateElement("listhead");
				//
				//				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				//				objRowTxt.SetAttribute("colname" , "UNotify");
				//				objRowTxt.InnerText="User to Notify";
				//				objLstRow.AppendChild(objRowTxt);
				//
				//				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				//				objRowTxt.SetAttribute("colname" , "LBusiness");
				//				objRowTxt.InnerText="Line of Business";
				//				objLstRow.AppendChild(objRowTxt);
				//
				//				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				//				objRowTxt.SetAttribute("colname" , "NPeriod");
				//				objRowTxt.InnerText="Notify Period";
				//
				//				objRowTxt = p_objXmlDocument.CreateElement("rowhead");
				//				objRowTxt.SetAttribute("colname" , "DCriteria");
				//				objRowTxt.InnerText="Date Criteria";
				//				objLstRow.AppendChild(objRowTxt);
				//			
				//				objRowTxt = p_objXmlDocument.CreateElement("RecordId");
				//				objRowTxt.SetAttribute("colname" , "RecordId");
				//				objRowTxt.InnerText="RecordId";
				//				objLstRow.AppendChild(objRowTxt);
				//			
				//				objElement.AppendChild(objLstRow);
				//				objCache=new LocalCache(base.ConnectString);
				//				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				//				while (objRead.Read())
				//				{
				//					objLstRow = p_objXmlDocument.CreateElement("listrow");
				//
				//					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
				//					objRowTxt.SetAttribute("type" , "label");
				//					objRowTxt.SetAttribute("name" , "UNotify");
				//					objNode=p_objXmlDocument.SelectSingleNode("//control[@name='UserNotify']");  
				//					objNode=objNode.SelectSingleNode("./option[@value='"+
				//						Conversion.ConvertObjToStr(objRead.GetValue("USER_ID"))+"']");
				//					if (objNode!=null)
				//					{
				//						objRowTxt.SetAttribute("title", objNode.InnerText);
				//					}
				//					else
				//					{
				//						objRowTxt.SetAttribute("title", "");
				//					}
				//					objLstRow.AppendChild(objRowTxt);
				//				
				//				
				//				
				//					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
				//					objRowTxt.SetAttribute("type" , "label");
				//					objRowTxt.SetAttribute("name" , "LBusiness");
				//					objRowTxt.SetAttribute("title", objCache.GetShortCode(Conversion.ConvertObjToInt(
				//						objRead.GetValue("LINE_OF_BUS_CODE")))+" - "+
				//						objCache.GetCodeDesc(Conversion.ConvertObjToInt(
				//						objRead.GetValue("LINE_OF_BUS_CODE"))));
				//					objLstRow.AppendChild(objRowTxt);
				//
				//					iTmp = Conversion.ConvertObjToInt(objRead.GetValue("NOTIFY_TYPE")) % 10;	
				//					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
				//					objRowTxt.SetAttribute("type" , "label");
				//					objRowTxt.SetAttribute("name" , "NPeriod");
				//					objNode=p_objXmlDocument.SelectSingleNode("//control[@name='NotifyPeriod']");  
				//					objNode=objNode.SelectSingleNode("./option[@value='"+
				//						iTmp.ToString()+"']");
				//					if (objNode!=null)
				//					{
				//						objRowTxt.SetAttribute("title", objNode.InnerText);
				//					}
				//					else
				//					{
				//						objRowTxt.SetAttribute("title", "");
				//					}
				//				
				//					objLstRow.AppendChild(objRowTxt);
				//					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
				//					objRowTxt.SetAttribute("type" , "label");
				//					objRowTxt.SetAttribute("name" , "DCriteria");
				//					if (Conversion.ConvertObjToInt(objRead.GetValue("NOTIFY_TYPE")) >=30)
				//					{
				//						objRowTxt.SetAttribute("title","by end of month");
				//					}
				//					else if (Conversion.ConvertObjToInt(objRead.GetValue("NOTIFY_TYPE")) >=20)
				//					{
				//						objRowTxt.SetAttribute("title","by end of week");
				//					}
				//					else
				//					{
				//						objRowTxt.SetAttribute("title","<= current date");
				//					}
				//				
				//					objLstRow.AppendChild(objRowTxt);
				//
				//					objRowTxt = p_objXmlDocument.CreateElement("rowtext");
				//					objRowTxt.SetAttribute("type" , "label");
				//					objRowTxt.SetAttribute("name" , "RecordId");
				//					objRowTxt.SetAttribute("title",Conversion.ConvertObjToStr(objRead.GetValue("RECORD_ID")));
				//				
				//					objLstRow.AppendChild(objRowTxt);
				//
				//					objElement.AppendChild(objLstRow);
                //start : rsushilaggar 06/02/2010 MITS 20073,20036,20938
                SysSettings settings = new SysSettings(base.ConnectString, m_iClientId);
                if (settings != null && !settings.RMXLSSEnable)
                {
                    objElement = (XmlElement)XMLDoc.SelectSingleNode("//control[@name='CreateUpdateEntity']");
                    if (objElement != null)
                    {
                        //objElement.SetAttribute("disabled", "true");
                        objElement.InnerText = "";
                    }
                    objElement = (XmlElement)XMLDoc.SelectSingleNode("//control[@name='LSSPaymentsOnHOLD']");
                    if (objElement != null)
                    {
                        //objElement.SetAttribute("disabled", "true");
                        objElement.InnerText = "";
                    }
                    objElement = (XmlElement)XMLDoc.SelectSingleNode("//control[@name='LssCollectionOnHold']");
                    if (objElement != null)
                    {
                        //objElement.SetAttribute("disabled", "true");
                        objElement.InnerText = "";
                    }
                    //XMLDoc.AppendChild(
                    XmlElement element = p_objXmlDocument.CreateElement("DisableControls");
                    XmlElement element1 = p_objXmlDocument.CreateElement("Item");
                    element1.InnerText = "CreateUpdateEntity";
                    element.AppendChild(element1);
                    XmlElement element2 = p_objXmlDocument.CreateElement("Item");
                    element2.InnerText = "LSSPaymentsOnHOLD";
                    element.AppendChild(element2);
                    XmlElement element3 = p_objXmlDocument.CreateElement("Item");
                    element3.InnerText = "LssCollectionOnHold";
                    element.AppendChild(element3);

                    objElement = (XmlElement)XMLDoc.SelectSingleNode("//form");
                    if(objElement != null)
                    objElement.AppendChild(element);
                   
                }
                //R8 Enhancement for ScheduleDate
                //================================================================================
                objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/NotifySchedule");

                //objElement = (XmlElement)p_objXmlDocument.CreateElement("NotifyUsers");
                //Asif Checked For PaymentNotification node
                XmlElement xScheduleDate = (XmlElement)p_objXmlDocument.SelectSingleNode("//form/NotifySchedule/ScheduleDate");
                if (xScheduleDate != null)
                {
                    //p_objXmlDocument.RemoveChild(xPaymentNotification);
                    xScheduleDate.RemoveAll();

                }

                objElement = (XmlElement)p_objXmlDocument.CreateElement("ScheduleDate");
                objParent.AppendChild(objElement);
                sbSql = new StringBuilder();
                sbSql.Append(" SELECT * FROM SCHEDULED_DATES ");


                objLstRow = p_objXmlDocument.CreateElement("listhead");

                
                objRowTxt = p_objXmlDocument.CreateElement("Date");
                objRowTxt.SetAttribute("colname", "Date");
                objRowTxt.InnerText = "Date";
                objLstRow.AppendChild(objRowTxt);

                
                objRowTxt = p_objXmlDocument.CreateElement("Description");
                objRowTxt.SetAttribute("colname", "Description");
                objRowTxt.InnerText = "Description";
                objLstRow.AppendChild(objRowTxt);

            

                



                objElement.AppendChild(objLstRow);

                objCache = new LocalCache(base.ConnectString, m_iClientId);
                objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString());

                while (objRead.Read())
                {

                    objLstRow = p_objXmlDocument.CreateElement("Schedule1");
                    sValues = Conversion.ConvertObjToStr(objRead.GetValue("Schedule_Date")) ;
                    objRowTxt = p_objXmlDocument.CreateElement("Date");
                    objRowTxt.InnerText =  Conversion.GetDBDateFormat(sValues,"d");
                    objLstRow.AppendChild(objRowTxt);

                    sValues = Conversion.ConvertObjToStr(objRead.GetValue("Schedule_Description")) ;

                    objRowTxt = p_objXmlDocument.CreateElement("Description");
                    objRowTxt.InnerText = sValues;
                    objLstRow.AppendChild(objRowTxt);
                    objRowTxt = p_objXmlDocument.CreateElement("ScheduleId");
                    objRowTxt.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SCHEDULE_ROW_ID"));
                    //sValues = sValues + objRowTxt.InnerText + "**";
                    objLstRow.AppendChild(objRowTxt);
                    //objElement.AppendChild(objLstRow);

                    objElement.AppendChild(objLstRow);
                }
              
                ////For Grid
                objLstRow = p_objXmlDocument.CreateElement("Schedule1");
                objLstRow.SetAttribute("type", "new");

                objRowTxt = p_objXmlDocument.CreateElement("Date");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);

                objRowTxt = p_objXmlDocument.CreateElement("Description");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);

                objRowTxt = p_objXmlDocument.CreateElement("ScheduleId");
                objRowTxt.InnerText = "";
                objLstRow.AppendChild(objRowTxt);


                objElement.AppendChild(objLstRow);
                //Asif End


                objCache.Dispose();

                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                objParent = (XmlElement)p_objXmlDocument.SelectSingleNode("//ScheduleNotifyValues");
                objParent.InnerText = sValues.ToString();
                //end: rsushilaggar
				return XMLDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("CheckOptions.Get.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objElement=null;
				objNode=null;
				if (objRead!=null)
				{
					objRead.Dispose();
				}
				sbSql=null;
				if (objCache!=null)
				{
					objCache.Dispose();
				}
				objRowTxt=null;
				objLstRow=null;
			}

			
		}

        /// <summary>
        /// Gets the duplicate criteria list.
        /// </summary>
        /// <param name="iDupNoDays">The i dup no days.</param>
        /// <returns></returns>
        private static SortedList GetDuplicateCriteriaList(int iDupNoDays)
        {
            SortedList objList = new SortedList();
            string numberOfDays = iDupNoDays > 0 ? iDupNoDays.ToString() : "X";
            objList.Add(0, "0 - No Additional Criteria");
            objList.Add(1, string.Format("1 - Transaction Date within {0} Days", numberOfDays));
            objList.Add(2, "2 - Payment Total");
            objList.Add(3, "3 - From and To Date Range");
            objList.Add(4, "4 - Split Amount and Transaction Type");
            objList.Add(5, string.Format("5 - Payment Total and Trans Date within {0} Days", numberOfDays));
            objList.Add(6, string.Format("6 - Split Amt, Trans Type, & Trans Date within {0} Days", numberOfDays));
            objList.Add(7, "7 - Claims with same claimant SSN & From - To Date Range");
            objList.Add(8, string.Format("8 - Payment Total, Invoice Number, and Trans Date within {0} Days", numberOfDays));
            objList.Add(9, "9 - Invoice Number per Payee");
            objList.Add(10, "10 - Claims with same Claimant SSN, Amount, From - To Date Range, and Invoice Number");
            objList.Add(11, "11 - Claims with same (Payee Name or SSN) and (Overlapping Date Range Or Invoice #.)");
            objList.Add(12, string.Format("12 - Payment Total, Invoice Number, Split Amt and Trans Date within {0} Days", numberOfDays));
            objList.Add(13, "13 - Claims with same Payee SSN, Amount, From - To Date Range, and Invoice Number");
            objList.Add(14, "14 - Current Claim with same Claimant SSN, Amount, From - To Date Range, and Invoice Number");
            objList.Add(15, "15 - Current Claim with same (Payee Name or SSN) and (Overlapping Date Range Or Invoice #.)");
            return objList;
        }

		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument GetAddNotifyValues(XmlDocument p_objXmlDocument)
		{
			string sDsnId=m_sDsnId;
			string sSecureDSN=m_sSecureDSN;
			XmlElement objElement=null;
            //XmlElement objParent=null;
            //string sValues="";
			XmlNode objNode=null;
			DbReader objRead=null;
            //int iDupNoDays=0;
            //int iCheckPaperBin=0;
            //int iEobPaperBin=0;
            //int iCheckToFile=0;
            //int iDupPayCriteria=0;
			StringBuilder sbSql=null;
			LocalCache objCache=null;
            //XmlElement objRowTxt=null;
            //XmlElement objLstRow=null;
			XMLDoc = p_objXmlDocument;
            //int iDupFlag=0;
			string sLOB="";
            //int iTmp=0;
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='LOB']");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sLOB=objCache.GetTableId("LINE_OF_BUSINESS").ToString();
				objCache.Dispose();
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
				sbSql.Append(" WHERE CODES.TABLE_ID ="+sLOB);
				sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
				sbSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 ");
				sbSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ");
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")).Trim()+" "
						+Conversion.ConvertObjToStr(objRead.GetValue("CODE_DESC")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				sbSql=new StringBuilder();
				objNode=XMLDoc.SelectSingleNode("//control[@name='UserNotify']"); 
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ");
				sbSql.Append(" FROM USER_DETAILS_TABLE,USER_TABLE ");
				sbSql.Append(" WHERE USER_DETAILS_TABLE.DSNID = "+sDsnId);
				sbSql.Append(" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ");
				objRead=DbFactory.GetDbReader(sSecureDSN,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("LAST_NAME")).Trim()+", "
						+Conversion.ConvertObjToStr(objRead.GetValue("FIRST_NAME")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				return XMLDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("CheckOptions.Get.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objElement=null;
				objNode=null;
				if (objRead!=null)
				{
					objRead.Dispose();
				}
				sbSql=null;
				if (objCache!=null)
				{
					objCache.Dispose();
				}
                //objRowTxt=null;
                //objLstRow=null;
			}

			
		}
        /// Name		: GetDuplicateCriteria
        /// Author		: Nanda kumar
        /// Date Created: 05/23/2014
		/// MITS 		: 32087  		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Get Duplicate criteria information
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Duplicate criteria information</param>
        /// <returns>True/False</returns>
        public XmlDocument GetDuplicateCriteria(XmlDocument p_objXmlDocument)
        {
            XmlNode objNode = null;
            int iDupCriteria = 0;
            string sSql = "";
            DbReader objRead = null;
            string sValues = "";
            try
            {
                sSql = "SELECT DUP_NUM_DAYS, DUP_PAY_CRITERIA FROM CHECK_OPTIONS";
                objRead = DbFactory.GetDbReader(base.ConnectString, sSql.ToString());
                if (objRead.Read())
                {
                    sValues = objRead.GetValue("DUP_NUM_DAYS").ToString();
                    objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DupNoOfDays']");
                    if (objNode != null)
                    {
                        objNode.InnerText = sValues;
                    }
                    sValues = objRead.GetValue("DUP_PAY_CRITERIA").ToString();
                    objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DupCriteria']");
                    if (objNode != null)
                    {
                        objNode.InnerText = sValues;
                    }
                }
                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
                return p_objXmlDocument;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckOptions.SetDuplicateCriteria.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
        }
		/// Name		: SetDuplicateCriteria
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Updates Duplicate criteria information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Duplicate criteria information</param>
		/// <returns>True/False</returns>
		public XmlDocument SetDuplicateCriteria(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			ArrayList alSql=null;
			int iDupCriteria=0;
			string sSql="";
			SortedList objList=null;
			IDictionaryEnumerator objEnum=null;
			int iDupNoDays=0;
			XmlDocument objDoc=null;
			XmlElement objElement=null;
			XmlElement objTemp=null;
			try
			{
				alSql=new ArrayList();
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='DupCriteria']"); 
				iDupCriteria=Conversion.ConvertStrToInteger(objNode.InnerText);
				objNode=p_objXmlDocument.SelectSingleNode("//DupNoOfDays"); 
				iDupNoDays=Conversion.ConvertStrToInteger(objNode.InnerText);
				sSql="UPDATE CHECK_OPTIONS SET DUP_NUM_DAYS='"+iDupNoDays.ToString()+"',DUP_PAY_CRITERIA='"+iDupCriteria.ToString()+"'";
				alSql.Add(sSql);
				//UTILITY.Save(alSql,m_sDSN);
				objDoc=new XmlDocument();
				objElement=objDoc.CreateElement("DupCriteria");
				objDoc.AppendChild(objElement);
                // akaushik5 Changed for MITS 37111 Starts
                //objList=new SortedList();
                //objList.Add(0,"0 - No Additional Criteria");
                //if (iDupNoDays > 0)
                //    objList.Add(1,"1 - Transaction Date within "+iDupNoDays.ToString()+ " Days");
                //else
                //    objList.Add(1,"1 - Transaction Date within X Days");
                //objList.Add(2,"2 - Payment Total");
                //objList.Add(3,"3 - From and To Date Range");
                //objList.Add(4,"4 - Split Amount and Transaction Type");
                //if (iDupNoDays > 0)
                //{
                //    objList.Add(5,"5 - Payment Total and Trans Date within "+iDupNoDays.ToString()+ " Days");
                //    objList.Add(6,"6 - Split Amt, Trans Type, & Trans Date within "+iDupNoDays.ToString()+ " Days");
                //}
                //else
                //{
                //    objList.Add(5,"5 - Payment Total and Trans Date within X Days");
                //    objList.Add(6,"6 - Split Amt, Trans Type, & Trans Date within X Days");
                //}
                //objList.Add(7,"7 - Claims with same claimant SSN & From - To Date Range");
                //if (iDupNoDays > 0)
                //{
                //    objList.Add(8,"8 - Payment Total, Invoice Number, and Trans Date within "+iDupNoDays.ToString()+ " Days");
                //}
                //else
                //{
                //    objList.Add(8,"8 - Payment Total, Invoice Number, and Trans Date within X Days");
                //}
                //objList.Add(9,"9 - Invoice Number per Payee");
                //objList.Add(10,"10 - Claims with same Claimant SSN, Amount, From - To Date Range, and Invoice Number");
                //objList.Add(11,"11 - Claims with same (Payee Name or SSN) and (Overlapping Date Range Or Invoice #.)");
                //objList.Add(13, "13 - Claims with same Payee SSN, Amount, From - To Date Range, and Invoice Number");
                //objList.Add(14, "14 - Current Claim with same Claimant SSN, Amount, From - To Date Range, and Invoice Number");
                //objList.Add(15, "15 - Current Claim with same (Payee Name or SSN) and (Overlapping Date Range Or Invoice #.)");
                objList = CheckOptions.GetDuplicateCriteriaList(iDupNoDays);
                // akaushik5 Changed for MITS 37111 Ends

				objEnum=objList.GetEnumerator();
				while (objEnum.MoveNext())
				{
					objTemp=objDoc.CreateElement("option");
					objTemp.SetAttribute("value",objEnum.Key.ToString());
					objTemp.InnerText=objEnum.Value.ToString();
					objElement.AppendChild(objTemp);
				}
				return objDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("CheckOptions.SetDuplicateCriteria.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objNode=null;
				objDoc=null;
				objTemp=null;
				objElement=null;
				alSql=null;
			}
			
		}
		/// <summary>
		/// Deletes Payment notification information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing records to be deleted</param>
		/// <returns>True/False</returns>
		public XmlDocument DeletePaymentNotification(XmlDocument p_objXmlDocument)
		{
			ArrayList arrlstSql=null;
			string sSQL="";
			//bool bReturnValue=false;
			string sRecord="";
			string sReplace="";
			//string sReplacedValue="";
			XmlNode objNode=null;
			string[] arrRecord=null;
			string[] arrValue=null;
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='NotifyList']"); 
				sRecord=objNode.InnerText;
				// Neelima Jan 2006
				// Defect No 001488 fixed
				sSQL="DELETE FROM PAY_NOTIFY_USERS WHERE RECORD_ID="+sRecord;
				arrlstSql=new ArrayList();
				arrlstSql.Add(sSQL);
                UTILITY.Save(arrlstSql, m_sDSN, m_iClientId);
				//				bReturnValue=true;

				objNode=p_objXmlDocument.SelectSingleNode("//PaymentNotifyValues");
				arrRecord=objNode.InnerText.Split("**".ToCharArray());
				for(int i=0;i<arrRecord.Length;i++)
				{
					if (!arrRecord[i].Trim().Equals(""))
					{
						sReplace="**"+arrRecord[i];
						arrValue=arrRecord[i].Split("|".ToCharArray());
						if (arrValue[4].Equals(sRecord))
						{
							objNode.InnerText=objNode.InnerText.Replace(sReplace,"");
							break;
						}
					}

				}
				return p_objXmlDocument;
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("CheckOptions.DeletePaymentNotification.Error", m_iClientId),p_objEx);
			}
			finally
			{
				arrlstSql=null;
				objNode=null;
			}
			

		}
		/// <summary>
		/// Add Payment notification information 
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing data to add</param>
		/// <returns>True/False</returns>
		public XmlDocument AddPaymentNotification(XmlDocument p_objXmlDocument)
		{
			string sDsnId=m_sDsnId;
			int iTmp=0;
			string sValue="";
			string sUserId="";
			string sLobId="";
			string sSecureDSN=m_sSecureDSN;
			int iNotifyType=0;
			int iDateCriteria=0;
			XmlElement objRowTxt=null;
			XmlElement objLstRow=null;
			XmlElement objElement=null;
			XmlElement objValues=null;
			XmlNode objNode=null;
			XmlDocument objDOC=null;
			StringBuilder sbSql=null;
			DbReader objRead=null;
			string sName="";
			string sRecordId="";
			int iRecordId=0;
			LocalCache objCache=null;
			try
			{
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='LOB']"); 
				sLobId=objNode.InnerText;
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='UserNotify']"); 
				sUserId=objNode.InnerText;
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='NPeriod']"); 
				iNotifyType=Conversion.ConvertStrToInteger(objNode.InnerText);
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='DateCriteria']"); 
				iDateCriteria=Conversion.ConvertStrToInteger(objNode.InnerText);
				//iNotifyType=iNotifyType+iDateCriteria;
				sValue=p_objXmlDocument.SelectSingleNode("//PaymentNotifyValues").InnerText;
				objDOC=new XmlDocument();
				sRecordId=p_objXmlDocument.SelectSingleNode("//RecordId").InnerText;
				iRecordId=Conversion.ConvertStrToInteger(sRecordId)-1;
				/*iRecordId=Utilities.GetNextUID(m_sDSN,"PAY_NOTIFY_USERS");
				sSQL="INSERT INTO PAY_NOTIFY_USERS(RECORD_ID,USER_ID,LINE_OF_BUS_CODE,NOTIFY_TYPE) VALUES('"
					+iRecordId.ToString()+"','"+sUserId+"','"+sLobId+"','"+iNotifyType.ToString()+"')";
				arrlstSql=new ArrayList();
				arrlstSql.Add(sSQL);
				UTILITY.Save(arrlstSql,m_sDSN);
				*/
                objCache = new LocalCache(m_sDSN, m_iClientId);

				objElement = (XmlElement)objDOC.CreateElement("Notify");  
				objDOC.AppendChild(objElement);
				objElement = (XmlElement)objDOC.CreateElement("NotifyUsers");  
				objDOC.FirstChild.AppendChild(objElement);
				objValues = objDOC.CreateElement("Values");
				objLstRow = objDOC.CreateElement("User");
				objRowTxt = objDOC.CreateElement("UNotify");
				sbSql=new StringBuilder();

				sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ");
				sbSql.Append(" FROM USER_DETAILS_TABLE,USER_TABLE ");
				sbSql.Append(" WHERE USER_DETAILS_TABLE.DSNID = "+sDsnId);
				sbSql.Append(" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID AND USER_DETAILS_TABLE.USER_ID="+sUserId+"");
				objRead=DbFactory.GetDbReader(sSecureDSN,sbSql.ToString());
				while (objRead.Read())
				{
				
					sName=Conversion.ConvertObjToStr(objRead.GetValue("LAST_NAME")).Trim()+", "
						+Conversion.ConvertObjToStr(objRead.GetValue("FIRST_NAME")).Trim();
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objRowTxt.SetAttribute("value",sUserId);
				
				objRowTxt.InnerText=sName;
				
				objLstRow.AppendChild(objRowTxt);
				
				sValue=sValue+Conversion.ConvertObjToStr(sUserId)+"|";
				//sValues=sValues+Conversion.ConvertObjToStr(sUserId)+"|";
				objRowTxt = objDOC.CreateElement("LBusiness");
				objRowTxt.InnerText=objCache.GetShortCode(Conversion.ConvertObjToInt(
                    sLobId, m_iClientId)) + " - " +
					objCache.GetCodeDesc(Conversion.ConvertObjToInt(
                    sLobId, m_iClientId));
				objLstRow.AppendChild(objRowTxt);
				sValue=sValue+sLobId+"|";
				//sValues=sValues+Conversion.ConvertObjToStr(objRead.GetValue("LINE_OF_BUS_CODE"))+"|";
				//iTmp = Conversion.ConvertObjToInt(iNotifyType) % 10;	
                iTmp = Conversion.ConvertObjToInt(iNotifyType, m_iClientId);
				objRowTxt = objDOC.CreateElement("NPeriod");
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='NotifyPeriod']");  
				objNode=objNode.SelectSingleNode("./option[@value='"+
					iTmp.ToString()+"']");
				if (objNode!=null)
				{
					objRowTxt.InnerText=objNode.InnerText;
				}
				else
				{
					objRowTxt.InnerText="";
				}
				
				objLstRow.AppendChild(objRowTxt);
				//sValues=sValues+Conversion.ConvertObjToStr(iTmp.ToString())+"|";
				sValue=sValue+Conversion.ConvertObjToStr(iTmp.ToString())+"|";
				objRowTxt = objDOC.CreateElement("DCriteria");
				if (iDateCriteria >=30)
				{
					objRowTxt.InnerText="by end of month";
				}
				else if (iDateCriteria >=20)
				{
					objRowTxt.InnerText="by end of week";
				}
				else
				{
                    //objRowTxt.InnerText="<= current date";
                    objRowTxt.InnerText = "less/equal current date";
				}
				
				objLstRow.AppendChild(objRowTxt);
				sValue=sValue+Conversion.ConvertObjToStr(iDateCriteria)+"|";
				sValue=sValue+iRecordId.ToString()+"**";
				
				objRowTxt = objDOC.CreateElement("RecordId");
					
				objRowTxt.InnerText="0";
				
				objLstRow.AppendChild(objRowTxt);

				objElement.AppendChild(objLstRow);
				objValues.InnerText=sValue;
				objDOC.FirstChild.AppendChild(objValues);
				
				return objDOC;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("CheckOptions.AddPaymentNotification.Error", m_iClientId),p_objEx);
			}
			finally
			{
                if (objRead != null)
                {
                    objRead.Dispose();
                }
                sbSql = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }

				objDOC=null;
				
				objNode=null;
			}
			
		}

		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the data to the database
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			XmlNodeList objXmlNodeList = null;
			XMLDoc=p_objXmlDocument;
			ArrayList alQuery=null;
			XmlNode objNode=null;
			string sSql = string.Empty;
			string sFieldNames = string.Empty;
			string sFieldValues = string.Empty; 
			string[] arrValues=null;
			string[] arrRecord=null;
			string sUserId="";
			string sLobId="";
			int iNotifyType=0;
			int iRecordId=0;
			int iDupNoDays=0;
			int iDupCriteria=0;
            string sDeletedIds=string.Empty;
            string sPrinterName = string.Empty; //For r5 printer
            int iPaperBinForChecks=0;//For r5 printer
            int iPaperBinForEobs=0;//For R5 Printer
            //Added by bsharma33 Pen Testing Regions MITS 26943
			// npadhy JIRA 6418 - Save the Print Options and Distribution Type Mapping
            string sDistributionType = string.Empty;
            string sPrintOptions = string.Empty;
            DbConnection objConn = null;
            DbTransaction objTran = null;
            DbCommand objCommand = null;
            //Payal:RMA-8169 --Starts
            string[] deletedIds = null;
            string[] paramNames = null;
            string inClause = string.Empty;
            
            //Payal:RMA-8169 --Ends
            //End changes by bsharma33 Pen Testing MITS 26943
			try
			{
                //Added by bsharma33 Pen Testing Regions MITS 26943
                objConn = DbFactory.GetDbConnection(base.ConnectString);
                objConn.Open();
                objCommand = objConn.CreateCommand();
                objTran = objConn.BeginTransaction();
                objCommand.Transaction = objTran;
                objCommand.CommandType = CommandType.Text;
                dictParams.Clear();//In case parameters are added
                //End changes by bsharma33 Pen Testing MITS 26943
                //Asif Added for Deletion start
                
                sDeletedIds = XMLDoc.SelectSingleNode("//form/DeletedIds").InnerText;
                ArrayList arrlstSql=null;
                string sSQL="";
                if (sDeletedIds != "")
                {
                    //Changed by bsharma33 for Regions Pen Testing MITS 26943
                    //Payal:RMA-8169 :Starts
                    //sSQL = string.Format("DELETE FROM PAY_NOTIFY_USERS WHERE RECORD_ID IN ({0})", "~sDeletedIds~"); 
                   sSQL = "DELETE FROM PAY_NOTIFY_USERS WHERE RECORD_ID IN ({0})";
                   //dictParams.Add("sDeletedIds", sDeletedIds); 
                   deletedIds = sDeletedIds.Split(',');
                   paramNames = deletedIds.Select(
                       (s, i) => "~DeletedIds~" + i.ToString()
                   ).ToArray();
                   inClause = string.Join(",", paramNames);
                    using (SqlCommand cmd = new SqlCommand(string.Format(sSQL, inClause)))
                    {
                        for (int i = 0; i < paramNames.Length; i++)
                        {
                            dictParams.Add(paramNames[i], deletedIds[i]);
                        }
                        arrlstSql = new ArrayList();
                        arrlstSql.Add(cmd.CommandText);
                        UTILITY.Save(arrlstSql, m_sDSN, dictParams, m_iClientId);
                    }
                    // arrlstSql = new ArrayList(); 
                    //  arrlstSql.Add(sSQL); 
                    //UTILITY.Save(arrlstSql, m_sDSN);
                    // UTILITY.Save(arrlstSql, m_sDSN,dictParams, m_iClientId); 
                    //Payal:RMA-8169 :Ends
                    dictParams.Clear();
                    //End changes by bsharma33 for Regions Pen Testing MITS 26943
                    sDeletedIds = string.Empty;
                }
				//objXmlNodeList = XMLDoc.SelectNodes("//CheckOptions/control");Asif
                objXmlNodeList = XMLDoc.SelectNodes("//form/control");
				foreach(XmlElement objXmlElement in objXmlNodeList)
				{
                    // npadhy JIRA 6418 Changed If condition to switch, added case for Code Lookup
                    switch(objXmlElement.GetAttribute("type").ToLower())
                    {
                        case "checkbox":
					    {
						    if (objXmlElement.InnerText!=null)
						    {
							    if (objXmlElement.InnerText.Equals("True"))
							    {
								    objXmlElement.InnerText="-1";
							    }
							    else
							    {
								    objXmlElement.InnerText="0";
							    }
						    }
						    else
						    {
							    objXmlElement.InnerText="0";
						    }
						    if(m_FieldList[objXmlElement.GetAttribute("name")] != null)
						    {
                                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
							    //sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + objXmlElement.InnerText + "'");
                                sSql += String.Format(" {0} = {1},", this.m_FieldList[objXmlElement.GetAttribute("name")], "~" + this.m_FieldList[objXmlElement.GetAttribute("name")].ToString() + "~");
                                dictParams.Add(this.m_FieldList[objXmlElement.GetAttribute("name")].ToString(), objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : objXmlElement.InnerText);
                                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
						    }
                            break;
					    }
                        case "code":
                        if (m_FieldList[objXmlElement.GetAttribute("name")] != null)
                        {
                            sSql += String.Format(" {0} = {1},", this.m_FieldList[objXmlElement.GetAttribute("name")], "~" + this.m_FieldList[objXmlElement.GetAttribute("name")].ToString() + "~");
                            dictParams.Add(this.m_FieldList[objXmlElement.GetAttribute("name")].ToString(), objXmlElement.GetAttribute("codeid") == string.Empty ? "0" : objXmlElement.GetAttribute("codeid"));
                        }
                        break;
                    }

				}
				if(sSql != string.Empty)
				{
					sSql = sSql.Substring(0,sSql.Length - 1);
					sSql = String.Format("UPDATE {0} SET {1} ",TableName,sSql);
				}
                //Changed by bsharma33 for Regions Pen Testing MITS 26943
				//alQuery=new ArrayList();
                //alQuery.Add(sSql);
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes by bsharma33 for Regions Pen Testing MITS 26943
                
                //objNode=XMLDoc.SelectSingleNode("//CheckOptions/control[@name='MaxAutoChecksBatch']");
                objNode = XMLDoc.SelectSingleNode("//form/control[@name='MaxAutoChecksBatch']"); 
				if (objNode.InnerText.Trim()=="")
				{
                    //MGaba2:MITS 18929:Changing the default value to 1
                    //objNode.InnerText="0";
                    objNode.InnerText = "1";
				}
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
				//sSql="UPDATE CHECK_OPTIONS SET MAX_AUTO_CHECKS='"+objNode.InnerText+"';";

                sSql = string.Format("UPDATE CHECK_OPTIONS SET MAX_AUTO_CHECKS={0}", "~MAX_AUTO_CHECKS~");
                dictParams.Add("MAX_AUTO_CHECKS", objNode.InnerText);
				objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear(); 
                //Start Ashutosh Schedule Date
                objNode = XMLDoc.SelectSingleNode("//group/checkSchedule");
                //sSql = "UPDATE CHECK_OPTIONS SET WEEKEND_SCHEDULE='" + objNode.InnerText + "';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET WEEKEND_SCHEDULE={0}", "~WEEKEND_SCHEDULE~");
                dictParams.Add("WEEKEND_SCHEDULE", objNode.InnerText);
               // alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
				
				//objNode=XMLDoc.SelectSingleNode("//CheckOptions/control[@name='DaysApproval']");Asif
                objNode = XMLDoc.SelectSingleNode("//form/control[@name='DaysApproval']"); 
				if (objNode.InnerText.Trim()=="")
				{
					objNode.InnerText="0";
				}
                //smahajan6: Safeway: Payment Supervisory Approval: Start
                //sSql = "UPDATE CHECK_OPTIONS SET DAYS_FOR_APPROVAL='" + objNode.InnerText + "'";
                //alQuery.Add(sSql);

                if (XMLDoc.SelectSingleNode("//form/control[@name='TimeInterval']").InnerText == "0")
                {
                    //Changed by bsharma33 for Regions Pen Testing MITS 26943
                    //sSql = "UPDATE CHECK_OPTIONS SET DAYS_FOR_APPROVAL='" + objNode.InnerText + "'"
                       // + " , HOURS_FOR_APPROVAL='0';";
                    sSql = string.Format("UPDATE CHECK_OPTIONS SET DAYS_FOR_APPROVAL={0} , HOURS_FOR_APPROVAL=0", "~DAYS_FOR_APPROVAL~");
                    dictParams.Add("DAYS_FOR_APPROVAL", objNode.InnerText);
                    //End changes by bsharma33 for Regions Pen Testing MITS 26943
                }
                else
                {
                    //Changed by bsharma33 for Regions Pen Testing MITS 26943
                   // sSql = "UPDATE CHECK_OPTIONS SET DAYS_FOR_APPROVAL='0'"
                       // + " , HOURS_FOR_APPROVAL='" + objNode.InnerText + "';";
                    sSql = string.Format("UPDATE CHECK_OPTIONS SET DAYS_FOR_APPROVAL=0 , HOURS_FOR_APPROVAL={0}", "~HOURS_FOR_APPROVAL~");
                    dictParams.Add("HOURS_FOR_APPROVAL", objNode.InnerText);
                   
                }
				//alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear(); 
                //End changes by bsharma33 for Regions Pen Testing MITS 26943
                //objNode = XMLDoc.SelectSingleNode("//form/control[@name='PmtNotfyImmSup']");
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943 -- Commented as the field is being updated twice.
                //sSql = "UPDATE CHECK_OPTIONS SET PMT_NOTFY_IMM_SUP=" + objNode.InnerText +";";
                //sSql = string.Format("UPDATE CHECK_OPTIONS SET PMT_NOTFY_IMM_SUP={0};","~PMT_NOTFY_IMM_SUP~");
                //dictParams.Add("PMT_NOTFY_IMM_SUP", objNode.InnerText);
                // alQuery.Add(sSql); 
                //objCommand.CommandText = sSql;
                //DbFactory.ExecuteNonQuery(objCommand, dictParams);
                //dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //smahajan6: Safeway: Payment Supervisory Approval: End

                //Changed by Gagan for MITS 11461 : Start
                //objNode=XMLDoc.SelectSingleNode("//CheckOptions/control[@name='PaperBinChecks']"); 
                //sSql="UPDATE CHECK_OPTIONS SET CHECK_PAPER_BIN='"+objNode.InnerText+"'";
                //alQuery.Add(sSql);

                //objNode=XMLDoc.SelectSingleNode("//CheckOptions/control[@name='PaperBinEOB']"); 
                //sSql="UPDATE CHECK_OPTIONS SET EOB_PAPER_BIN='"+objNode.InnerText+"'";
                //alQuery.Add(sSql);
                //Changed by Gagan for MITS 11461 : End

                // npadhy JIRA 6418 - Commenting the save of existing Print Option
				//objNode=XMLDoc.SelectSingleNode("//CheckOptions/control[@name='PrntChecktoFile']"); Asif
                //objNode = XMLDoc.SelectSingleNode("//form/control[@name='PrntChecktoFile']"); //Added By Asif
                // //Changed by bsharma33 for Regions Pen Testing MITS 26943
                ////sSql="UPDATE CHECK_OPTIONS SET CHECK_TO_FILE='"+objNode.InnerText+"';";
                //sSql = string.Format("UPDATE CHECK_OPTIONS SET CHECK_TO_FILE={0}","~CHECK_TO_FILE~");
                //dictParams.Add("CHECK_TO_FILE", objNode.InnerText);
				//alQuery.Add(sSql); 
                //objCommand.CommandText = sSql;
                //DbFactory.ExecuteNonQuery(objCommand, dictParams);
                //objCommand.Parameters.Clear();
                //dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943

				//objNode=XMLDoc.SelectSingleNode("//CheckOptions/DuplicatePaymentCheck");Asif
                objNode = XMLDoc.SelectSingleNode("//form/DuplicatePaymentCheck");
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
				//sSql="UPDATE CHECK_OPTIONS SET DUP_PAY_CRITERIA='"+objNode.InnerText+"';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET DUP_PAY_CRITERIA={0}", "~DUP_PAY_CHECK~");
                dictParams.Add("DUP_PAY_CHECK", objNode.InnerText);
				//alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //Debabrata Biswas: MCIC Batch Printing Filter MITS 20050 Date: 04/22/2010
                objNode = XMLDoc.SelectSingleNode("//form/control[@name='PreCheckOrgLvl']");
                //iPreCheckOrgLvl = Conversion.ConvertStrToInteger(objNode.InnerText);
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
                //sSql = "UPDATE CHECK_OPTIONS SET PRE_CHK_ORG_LEVEL='" + Conversion.ConvertStrToInteger(objNode.InnerText) + "';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET PRE_CHK_ORG_LEVEL={0}", "~PRE_CHK_ORG_LEVEL~");
                dictParams.Add("PRE_CHK_ORG_LEVEL", objNode.InnerText);
                //alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //Debabrata Biswas: End
				
				//objNode = XMLDoc.SelectSingleNode("//CheckOptions/TransValues");
                objNode = XMLDoc.SelectSingleNode("//form/TransValues");
				if (objNode!=null)
				{
					if (objNode.InnerText!=null)
					{
						sSql = "DELETE FROM SUP_HOLD_TRANS";
                        //Changed by bsharma33 for Regions Pen Testing MITS 26943
						//alQuery.Add(sSql); 
                        objCommand.CommandText = sSql;
                        DbFactory.ExecuteNonQuery(objCommand, dictParams);
                        objCommand.Parameters.Clear();
                        dictParams.Clear();
                        //End changes by bsharma33 for Regions Pen Testing MITS 26943
						arrValues=objNode.InnerText.Split(new char [] {','});
                        
						foreach(string strValue in arrValues)
						{
							if (!strValue.Trim().Equals(""))
							{
                                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
								//sSql = "INSERT INTO SUP_HOLD_TRANS(TRANS_TYPE_CODE) VALUES (" +strValue+ ");";
                                sSql = string.Format("INSERT INTO SUP_HOLD_TRANS(TRANS_TYPE_CODE) VALUES ({0})", "~TRANS_TYPE_CODE~");
                                dictParams.Add("TRANS_TYPE_CODE" , strValue);
                                
								//alQuery.Add(sSql); 
                                objCommand.CommandText = sSql;
                                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                                objCommand.Parameters.Clear();
                                dictParams.Clear();
                                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
							}
						}
					}
				}
				sSql="DELETE FROM PAY_NOTIFY_USERS";
                //Changed by bsharma33 for Regions Pen Testing MITS 26943
				//alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes by bsharma33 for Regions Pen Testing MITS 26943
				objNode=p_objXmlDocument.SelectSingleNode("//PaymentNotifyValues");
				arrRecord=objNode.InnerText.Split("**".ToCharArray());
                
				for(int i=0;i<arrRecord.Length;i++)
				{
					if (!arrRecord[i].Trim().Equals(""))
					{
						arrValues=arrRecord[i].Split(new char [] {'|'});
						sUserId=arrValues[0];
						sLobId=arrValues[1];
						iNotifyType=Conversion.ConvertStrToInteger(arrValues[2])+Conversion.ConvertStrToInteger(arrValues[3]);
                        iRecordId = Utilities.GetNextUID(m_sDSN, "PAY_NOTIFY_USERS", m_iClientId);
                         //Changed by bsharma33 for Regions Pen Testing MITS 26943
						//sSql="INSERT INTO PAY_NOTIFY_USERS(RECORD_ID,USER_ID,LINE_OF_BUS_CODE,NOTIFY_TYPE) VALUES('"
							//+iRecordId.ToString()+"','"+sUserId+"','"+sLobId+"','"+iNotifyType.ToString()+"');";
                        sSql = string.Format("INSERT INTO PAY_NOTIFY_USERS(RECORD_ID,USER_ID,LINE_OF_BUS_CODE,NOTIFY_TYPE) VALUES({0},{1},{2},{3})", "~RECORD_ID~", "~USER_ID~", "~LINE_OF_BUS_CODE~", "~NOTIFY_TYPE~");
                        dictParams.Add("RECORD_ID", iRecordId.ToString());
                        dictParams.Add("USER_ID", sUserId);
                        dictParams.Add("LINE_OF_BUS_CODE", sLobId);
                        dictParams.Add("NOTIFY_TYPE", iNotifyType.ToString());
                         //alQuery.Add(sSql); 
                        objCommand.CommandText = sSql;
                        DbFactory.ExecuteNonQuery(objCommand, dictParams);
                        objCommand.Parameters.Clear();
                        dictParams.Clear();
                        //End changes  by bsharma33 for Regions Pen Testing MITS 26943
					}
				}
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='DupCriteria']"); 
				iDupCriteria=Conversion.ConvertStrToInteger(objNode.InnerText);
				objNode=p_objXmlDocument.SelectSingleNode("//DupNoOfDays"); 
				iDupNoDays=Conversion.ConvertStrToInteger(objNode.InnerText);
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
				//sSql="UPDATE CHECK_OPTIONS SET DUP_NUM_DAYS='"+iDupNoDays.ToString()+"',DUP_PAY_CRITERIA='"+iDupCriteria.ToString()+"';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET DUP_NUM_DAYS={0},DUP_PAY_CRITERIA={1}", "~DUP_NUM_DAYS~", "~DUP_PAY_CRITERIA~");
                dictParams.Add("DUP_NUM_DAYS", iDupNoDays.ToString());
                dictParams.Add("DUP_PAY_CRITERIA", iDupCriteria.ToString());
				//alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //MGaba2:R6:Reserve WorkSheet related settings in Payment Parameter:Start
                objNode = XMLDoc.SelectSingleNode("//control[@name='DaysApprovalReserves']");
                if (objNode.InnerText.Trim() == "")
                {
                    objNode.InnerText = "0";
                }
                //sSql = "UPDATE CHECK_OPTIONS SET DAYS_APP_RESERVES='" + objNode.InnerText + "'";
                //alQuery.Add(sSql);
                if (XMLDoc.SelectSingleNode("//form/control[@name='TimeIntervalAppRes']").InnerText == "0")
                {
                     //Changed by bsharma33 for Regions Pen Testing MITS 26943
                    //sSql = "UPDATE CHECK_OPTIONS SET DAYS_APP_RESERVES='" + objNode.InnerText + "'"
                      //  + " , HOURS_APP_RESERVES='0';";
                    sSql = string.Format("UPDATE CHECK_OPTIONS SET DAYS_APP_RESERVES={0} , HOURS_APP_RESERVES=0", "~DAYS_APP_RESERVES~");
                    dictParams.Add("DAYS_APP_RESERVES", objNode.InnerText);
                    //End changes  by bsharma33 for Regions Pen Testing MITS 26943

                }
                else
                {
                     //Changed by bsharma33 for Regions Pen Testing MITS 26943
                    //sSql = "UPDATE CHECK_OPTIONS SET DAYS_APP_RESERVES='0'"
                      //  + " , HOURS_APP_RESERVES='" + objNode.InnerText + "';";
                    sSql = string.Format("UPDATE CHECK_OPTIONS SET DAYS_APP_RESERVES=0, HOURS_APP_RESERVES={0}", "~HOURS_APP_RESERVES~");
                    dictParams.Add("HOURS_APP_RESERVES", objNode.InnerText);
                    
                }
               // alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //MGaba2:R6:Reserve WorkSheet related settings in Payment Parameter:End
                //New Functionality Added For Printer in R5 Start
                objNode = p_objXmlDocument.SelectSingleNode("//PrinterName"); 
                sPrinterName=objNode.InnerText;
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
                //sSql = "UPDATE CHECK_OPTIONS SET PRINTER_NAME = '" + sPrinterName + "';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET PRINTER_NAME = {0}", "~PRINTER_NAME~");
                dictParams.Add("PRINTER_NAME", sPrinterName);
                //alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //For paper bin for checks
                objNode = p_objXmlDocument.SelectSingleNode("//PaperBinForChecks");
                iPaperBinForChecks = Conversion.ConvertStrToInteger(objNode.InnerText);
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
                //sSql ="UPDATE CHECK_OPTIONS SET CHECK_PAPER_BIN ="+iPaperBinForChecks+";";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET CHECK_PAPER_BIN ={0}", "~CHECK_PAPER_BIN~");
                dictParams.Add("CHECK_PAPER_BIN", iPaperBinForChecks.ToString());
                //alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //For PaperBinFor Eobs
                objNode = p_objXmlDocument.SelectSingleNode("//PaperBinForEobs");
                iPaperBinForEobs = Conversion.ConvertStrToInteger(objNode.InnerText);
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
                //sSql = "UPDATE CHECK_OPTIONS SET EOB_PAPER_BIN =" + iPaperBinForEobs + ";";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET EOB_PAPER_BIN ={0}", "~EOB_PAPER_BIN~");
                dictParams.Add("EOB_PAPER_BIN", iPaperBinForEobs.ToString());
                //alQuery.Add(sSql); 
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943
                //New Functionality Added For Printer in R5 End
                //skhare7 R8 enhancement
                objNode = XMLDoc.SelectSingleNode("//form/control[@name='MaxPayees']");
                if (objNode.InnerText.Trim() == "")
                {
                   
                    objNode.InnerText = "1";
                }
                 //Changed by bsharma33 for Regions Pen Testing MITS 26943
                //sSql = "UPDATE CHECK_OPTIONS SET MAX_PAYEES='" + objNode.InnerText + "';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET MAX_PAYEES={0}", "~MAX_PAYEES~");
                dictParams.Add("MAX_PAYEES", objNode.InnerText);
               // alQuery.Add(sSql);
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();

                //if (XMLDoc.SelectSingleNode("//form/control[@name='SupervisoryApproval']").InnerText != "-1")//Added By Asif
                //{
                //    sSql = "UPDATE CHECK_OPTIONS SET ALLOW_SUP_GRP_APPROVE='0'";
                //    alQuery.Add(sSql);
                //}

                objNode = XMLDoc.SelectSingleNode("//form/control[@name='AccessGrpApprove']");
                if (objNode.InnerText.Trim() == "")
                {

                    objNode.InnerText = "1";
                }
                //Changed by bsharma33 for Regions Pen Testing MITS 26943
                //sSql = "UPDATE CHECK_OPTIONS SET MAX_PAYEES='" + objNode.InnerText + "';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET ALLOW_SUP_GRP_APPROVE={0}", "~ALLOW_SUP_GRP_APPROVE~");
                dictParams.Add("ALLOW_SUP_GRP_APPROVE", objNode.InnerText);
                // alQuery.Add(sSql);
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();

                //if (XMLDoc.SelectSingleNode("//form/control[@name='UseSupAppReserves']").InnerText != "-1") //Added By Asif
                //{
                //    sSql = "UPDATE CHECK_OPTIONS SET ALLOW_SUP_GRP_APPROVE_RSV='0'";
                //    alQuery.Add(sSql);                
                //}

                objNode = XMLDoc.SelectSingleNode("//form/control[@name='AccessGrpApproveReserve']");
                if (objNode.InnerText.Trim() == "")
                {

                    objNode.InnerText = "1";
                }
                //Changed by bsharma33 for Regions Pen Testing MITS 26943
                //sSql = "UPDATE CHECK_OPTIONS SET MAX_PAYEES='" + objNode.InnerText + "';";
                sSql = string.Format("UPDATE CHECK_OPTIONS SET ALLOW_SUP_GRP_APPROVE_RSV={0}", "~ALLOW_SUP_GRP_APPROVE_RSV~");
                dictParams.Add("ALLOW_SUP_GRP_APPROVE_RSV", objNode.InnerText);
                // alQuery.Add(sSql);
                objCommand.CommandText = sSql;
                DbFactory.ExecuteNonQuery(objCommand, dictParams);
                objCommand.Parameters.Clear();
                dictParams.Clear();
                    //skhare7 R8 
                   // base.Save(alQuery,dictParams);
                //End changes  by bsharma33 for Regions Pen Testing MITS 26943

                // npadhy JIRA 6418 Save the mapping between Distribution Type and Print Options
                objNode = p_objXmlDocument.SelectSingleNode("//form/control[@name='txtMapping']");
                arrRecord = objNode.InnerText.Split(",".ToCharArray());
                // Delete all values from Table and then Insert all
                sSql = string.Format("DELETE FROM CHECK_PRINT_OPTIONS");
                DbWriter objWriter = null;
                DbFactory.ExecuteNonQuery(objCommand.Connection.ConnectionString, sSql);
                for (int i = 0; i < arrRecord.Length; i++)
                {
                    if (!arrRecord[i].Trim().Equals(""))
                    {
                        arrValues = arrRecord[i].Split(new char[] { '|' });
                        sDistributionType = arrValues[0];
                        sPrintOptions = arrValues[1];
                        objWriter = DbFactory.GetDbWriter(objCommand.Connection.ConnectionString);
                        objWriter.Tables.Add("CHECK_PRINT_OPTIONS");
                        objWriter.Fields.Add("DSTRBN_TYPE_CODE", sDistributionType);
                        objWriter.Fields.Add("PRINT_OPTION", sPrintOptions);

                        if (DbFactory.GetDatabaseType(objCommand.Connection.ConnectionString) == eDatabaseType.DBMS_IS_ORACLE)
                        {
                            int iRowId = Convert.ToInt32(DbFactory.ExecuteScalar(objCommand.Connection.ConnectionString, "SELECT SEQ_CHECK_PRINT_OPTIONS_ROW_ID.NEXTVAL FROM DUAL"));
                            objWriter.Fields.Add("ROW_ID", iRowId);
                        }
                        objWriter.Execute();
                    }
                }
                // npadhy JIRA 6418
                objTran.Commit();
				return XMLDoc;
			}
			catch(RMAppException p_objEx)
            {  //Added by bsharma33 for Regions Pen Testing MITS 26943
                objTran.Rollback();
                //End changes by bsharma33 for Regions Pen Testing MITS 26943
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                //Added by bsharma33 for Regions Pen Testing MITS 26943
                objTran.Rollback();
                //Changed by bsharma33 for Regions Pen Testing MITS 26943
				throw new RMAppException(Globalization.GetString("CheckOptions.Save.Error", m_iClientId),p_objEx);
			}
			finally
			{
				objXmlNodeList = null;
				alQuery=null;
				objNode=null;
				arrValues=null;
                //Added  by bsharma33 for Regions Pen Testing MITS 26943
                dictParams.Clear();
                if (objConn != null)
                    objConn.Dispose();
                objCommand = null;
                objTran = null;
                //End additions  by bsharma33 for Regions Pen Testing MITS 26943
			}
		}

        //Start Addedy By Ashutosh
        /// Name		: GetScheduleDates
        /// Author		: Ashutosh
        /// 
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
        /// <returns>Xml containing the data to be populated on screen</returns>
        public XmlDocument GetScheduleDates(XmlDocument p_objXmlDocument)
        {
            string sDsnId = m_sDsnId;
            string sSecureDSN = m_sSecureDSN;
            XmlElement objElm;//Ashutosh
            string strRowID = string.Empty;//Ashutosh
            //XmlElement objElement = null;
            //XmlElement objParent = null;
            string sValues = "";
            XmlNode objNode = null;
            DbReader objRead = null;
            //int iDupNoDays = 0;
            //int iCheckPaperBin = 0;
            //int iEobPaperBin = 0;
            //int iCheckToFile = 0;
            //int iDupPayCriteria = 0;
            StringBuilder sbSql = null;
            LocalCache objCache = null;
            //XmlElement objRowTxt = null;
            //XmlElement objLstRow = null;
            XMLDoc = p_objXmlDocument;
            int iDupFlag = 0;
            string sLOB = "";
            int iTmp = 0;
            try
            {
                objElm = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='RowID']");//Ashutosh
                if (objElm != null)
                    strRowID = objElm.InnerText;
                if (strRowID != null && strRowID !="")
                {
                    sbSql = new StringBuilder();
                    sbSql.Append(" SELECT * FROM SCHEDULED_DATES where SCHEDULE_ROW_ID=" + strRowID);
                    objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString());
                    if (objRead.Read())
                    {
                        sValues = objRead.GetValue("SCHEDULE_DATE").ToString();

                        objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ScheduleDate']");
                        if (objNode != null)
                        {
                            objNode.InnerText = Conversion.GetDBDateFormat(sValues, "d");
                        }
                        sValues = objRead.GetValue("SCHEDULE_DESCRIPTION").ToString();
                        objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Description']");
                        if (objNode != null)
                        {
                            objNode.InnerText = sValues;
                        }
                    }
                    if (!objRead.IsClosed)
                       {
                            objRead.Close();
                       }

                }
              
                //objCache = new LocalCache(base.ConnectString);
                //sLOB = objCache.GetTableId("LINE_OF_BUSINESS").ToString();
                //objCache.Dispose();
                
                //sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
                //sbSql.Append(" WHERE CODES.TABLE_ID =" + sLOB);
                //sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID ");
                //sbSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES.CODE_ID <> 0 ");
                //sbSql.Append(" ORDER BY CODES.SHORT_CODE,CODES_TEXT.CODE_DESC ");
                //objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString());
                //while (objRead.Read())
                //{
                //    objElement = XMLDoc.CreateElement("option");
                //    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue("CODE_ID")));
                //    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("SHORT_CODE")).Trim() + " "
                //        + Conversion.ConvertObjToStr(objRead.GetValue("CODE_DESC")).Trim();
                //    objNode.AppendChild(objElement);
                //}
                //if (!objRead.IsClosed)
                //{
                //    objRead.Close();
                //}
                //sbSql = new StringBuilder();
                //objNode = XMLDoc.SelectSingleNode("//control[@name='UserNotify']");
                //sbSql = new StringBuilder();
                //sbSql.Append(" SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ");
                //sbSql.Append(" FROM USER_DETAILS_TABLE,USER_TABLE ");
                //sbSql.Append(" WHERE USER_DETAILS_TABLE.DSNID = " + sDsnId);
                //sbSql.Append(" AND USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ");
                //objRead = DbFactory.GetDbReader(sSecureDSN, sbSql.ToString());
                //while (objRead.Read())
                //{
                //    objElement = XMLDoc.CreateElement("option");
                //    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue("USER_ID")));
                //    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("LAST_NAME")).Trim() + ", "
                //        + Conversion.ConvertObjToStr(objRead.GetValue("FIRST_NAME")).Trim();
                //    objNode.AppendChild(objElement);
                //}
                //if (!objRead.IsClosed)
                //{
                //    objRead.Close();
                //}
                return XMLDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckOptions.Get.Error", m_iClientId), p_objEx);
            }
            finally
            {
                //objElement = null;
                objNode = null;
                if (objRead != null)
                {
                    objRead.Dispose();
                }
                sbSql = null;
                if (objCache != null)
                {
                    objCache.Dispose();
                }
                //objRowTxt = null;
                //objLstRow = null;
            }


        }

        //End
		#endregion

        //Start Addedy By Ashutosh
        /// Name		: SaveSchedule
        /// Author		: Ashutosh
        /// 
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlIn">Xml containing the Parameters for the function</param>
        /// <param name="UserId" >It will current user login NAme</param>
        /// <returns>Xml containing the data to be populated on screen</returns>
        public void SaveSchedule(XmlDocument p_objXmlIn,string  UserId)
        {
                   
            XMLDoc = p_objXmlIn;
            ArrayList alQuery=null;
			XmlNode objNode=null;
			string sSql = string.Empty;
            Int32 iRecCount = 0;
            Int32 iNextScheduleId = 0;
            string sUserid = UserId;
            string sAddedOn = "";
            string sUpdatedOn = "";
            string sScheduleDes = string.Empty;
            string sScheduleDate = string.Empty;
            Int32 iScheduleId = 0;
            try
            {
                
                //this will Save the scheduleDeate  and ScheduleDeascription
                objNode = XMLDoc.SelectSingleNode("//Document/ScheduleDate/ScheduleDate");
                 sScheduleDate = objNode.InnerText;
                objNode = XMLDoc.SelectSingleNode("//Document/ScheduleDate/ScheduleDescription");
                 sScheduleDes = objNode.InnerText.Trim();
                 objNode = XMLDoc.SelectSingleNode("//Document/ScheduleDate/ScheduleId");
                 iScheduleId =Convert.ToInt32( objNode.InnerText);
                //We are saving the date in yyymmdd format so creating the date in desired format.
                sAddedOn = Conversion.ToDbDateTime(System.DateTime.Now);
                sScheduleDate = Conversion.ToDbDateTime(Convert.ToDateTime(sScheduleDate));
                sUpdatedOn = sAddedOn;
                sScheduleDate = sScheduleDate.Substring(0, 8);


                sSql = "SELECT NEXT_UNIQUE_ID FROM GLOSSARY WHERE SYSTEM_TABLE_NAME ='SCHEDULED_DATES'";

                iNextScheduleId = Convert.ToInt32(DbFactory.ExecuteScalar(base.ConnectString, sSql.ToString()));

                //Check for Duplicacy
                if (iScheduleId != -1)
                {
                    sSql = "SELECT COUNT(SCHEDULE_ROW_ID) FROM SCHEDULED_DATES WHERE SCHEDULE_ROW_ID=" + iScheduleId;
                }
                else
                {
                    sSql = "SELECT COUNT(SCHEDULE_ROW_ID) FROM SCHEDULED_DATES WHERE SCHEDULE_DATE='" + sScheduleDate + "' AND SCHEDULE_DESCRIPTION='" + sScheduleDes + "'";
                }
             
                iRecCount = Convert.ToInt32(DbFactory.ExecuteScalar(base.ConnectString, sSql.ToString()));

                if (iRecCount > 0)
                {
                    sSql = "UPDATE SCHEDULED_DATES SET SCHEDULE_DESCRIPTION='" + sScheduleDes + "',";
                    sSql = sSql + " SCHEDULE_DATE='" + sScheduleDate + "',";
                    sSql = sSql + "UPDATED_BY_USER='" + sUserid + "',";
                    sSql = sSql + "DTTM_RCD_LAST_UPD='" + sUpdatedOn + "'";

                    if (iScheduleId != -1)
                    {
                        sSql = sSql + " WHERE SCHEDULE_ROW_ID=" + iScheduleId;
                    }
                    else
                    {
                        sSql = sSql + " WHERE SCHEDULE_DATE='" + sScheduleDate + "'";
                    }
                    DbFactory.ExecuteNonQuery(base.ConnectString, sSql);
                }
                else
                {
                    sSql = "INSERT INTO SCHEDULED_DATES(SCHEDULE_ROW_ID,SCHEDULE_DATE,SCHEDULE_DESCRIPTION,DTTM_RCD_ADDED,DTTM_RCD_LAST_UPD,ADDED_BY_USER,UPDATED_BY_USER)";
                    sSql = sSql + "values (" + iNextScheduleId + ",'" + sScheduleDate + "','" + sScheduleDes + "','" + sAddedOn + "',' ','" + sUserid + "',' ')";

                    alQuery = new ArrayList();
                    alQuery.Add(sSql);
                    sSql = "UPDATE GLOSSARY SET NEXT_UNIQUE_ID=" + (iNextScheduleId + 1) + " WHERE SYSTEM_TABLE_NAME ='SCHEDULED_DATES'";
                    alQuery.Add(sSql);
                    base.Save(alQuery);
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckOptions.SaveSchedule.Error", m_iClientId), p_objEx);
            }
            finally
            {
             
                alQuery = null;
                objNode = null;
             
            }

            
          
          
        }

        //Start Addedy By Ashutosh
        /// Name		: DeleteSchedule
        /// Author		: Ashutosh
        /// 
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Function Gets data to populate Screen
        /// </summary>
        /// <param name="p_objXmlIn">Xml containing the Parameters for the function</param>
        
        public void DeleteSchedule(XmlDocument p_objXmlIn)
        {
            XMLDoc = p_objXmlIn;

            string sSchedueId = string.Empty;
            string sSql = string.Empty;

            try
            {
                sSchedueId = XMLDoc.SelectSingleNode("//SetCriteria/ScheduleId").InnerText;
                sSql = "DELETE FROM SCHEDULED_DATES WHERE SCHEDULE_ROW_ID=" + sSchedueId;
                DbFactory.ExecuteNonQuery(base.ConnectString, sSql);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("CheckOptions.DeleteSchedule.Error", m_iClientId), p_objEx);
            }
            finally
            {
                XMLDoc = null;
            }

            

        }
    }
}
