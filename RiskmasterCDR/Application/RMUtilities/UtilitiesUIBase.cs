using System;
using System.Xml; 
using Riskmaster.DataModel; 
using System.Collections; 
using Riskmaster.Db; 
using Riskmaster.Common; 
using System.Data;
using System.Text;
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag, Pankaj
	///Dated   :   1st,Mar 2005
	///Purpose :   Base Class form
	/// </summary>
	public class UtilitiesUIBase // INavigation //, IPersistence, IDisposable
	{	
		/// <summary>
		/// Key Field
		/// </summary>
		private string m_sKeyField="";

		/// <summary>
		/// Key Field Value=""
		/// </summary>
		private long m_lKeyFieldValue=0;

		/// <summary>
		/// String Key field value
		/// </summary>
		private string m_sKeyFieldValue="";

		/// <summary>
		/// Table Name
		/// </summary>
		private string m_sTableName="";

		/// <summary>
		/// Input Xml document
		/// </summary>
		private XmlDocument m_objXmlDoc=null;		

		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnString="";

		/// <summary>
		/// Key Tag
		/// </summary>
		private string m_sKeyTag="";

		/// <summary>
		/// Where Clause for SQL query
		/// </summary>
		private string m_sWhereClause="";
		
		/// <summary>
		/// Move First Constant
		/// </summary>
		internal const int MOVE_FIRST = 1;

		/// <summary>
		/// Move Last Constant
		/// </summary>
		internal const int MOVE_LAST= 2;

		/// <summary>
		/// Move Next Constant
		/// </summary>
		internal const int MOVE_NEXT= 3;

		/// <summary>
		/// Move Previous Constant
		/// </summary>
		internal const int MOVE_PREVIOUS = 4;		

		/// <summary>
		/// Datafield List
		/// </summary>
		internal DataFieldList m_FieldList=new DataFieldList(); 

		/// <summary>
		/// Child array variable
		/// </summary>
		protected string[,] m_arrChild =  new string[10,5];
		
		/// <summary>
		/// Property map collection variable
		/// </summary>
		internal Hashtable m_PropertyMap = new Hashtable();

        private string m_sUserlogin = "";

        UserLogin mUserlogin = null;//vkumar258 RMA-6037

       // private int m_iClientId = 0;
        //aanandpraka2:Changes for Pen Testing
        /// <summary>
        /// Dictionary variable for storing parameter collection
        /// </summary>
        protected System.Collections.Generic.Dictionary<string, object> dictParams = new System.Collections.Generic.Dictionary<string, object>();
        //aanandpraka2:End Changes

        private int m_iClientId = 0;
		#region "Properties"
		
		/// <summary>
		/// TableName Property
		/// </summary>
		protected string TableName
		{
			get{return m_sTableName;}
			set{m_sTableName = value;}
		}

		/// <summary>
		/// KeyField Property
		/// </summary>
		protected string KeyField
		{
			get{return m_sKeyField;}
			set{m_sKeyField = value;}
		}

		/// <summary>
		/// WhereClause Property
		/// </summary>
		protected string WhereClause
		{
			get{return m_sWhereClause;}
			set{m_sWhereClause = value;}
		}

		/// <summary>
		/// Xml document Property
		/// </summary>
		protected XmlDocument XMLDoc
		{
			get{return m_objXmlDoc;}
			set{m_objXmlDoc = value;}
		}

		/// <summary>
		/// Connection String Property
		/// </summary>
		internal string ConnectString
		{
			get{return m_sConnString;}
			set{m_sConnString = value;}
		}

		/// <summary>
		/// Child array property
		/// </summary>
		protected string[,] ChildArray
		{
			set{m_arrChild = value;}
		}

		/// <summary>
		/// Key field value
		/// </summary>
		protected long KeyFieldValue
		{
			get
			{
				return m_lKeyFieldValue;
			}
		}

		#endregion

        internal UtilitiesUIBase(UserLogin oUserlogin, string p_sConnString, int p_iClientId = 0)// vkumar258 - RMA-6037
        {
            mUserlogin = oUserlogin;
            Initialize();
        }
		/// Name		: UtilitiesUIBase
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>	
        internal UtilitiesUIBase(string p_sConnString, int p_iClientId)
		{
			Initialize();
		}

		/// Name		: UtilitiesUIBase
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDatabaseName">Dsn Name</param>
        internal UtilitiesUIBase(string p_sUserName, string p_sPassword, string p_sDatabaseName, int p_iClientId)
		{
			Initialize();
		}
		
		/// Name		: Initialize
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Blank Implementation
		/// </summary>
		protected void Initialize(){}

		/// Name		: InitFields
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Add all obect Fields into the Field Collection from our Field List.
		/// </summary>
		/// <param name="p_sFields">Fields List</param>
		protected virtual void InitFields(string[,] p_sFields)
		{ 
			for(int i =0; i< p_sFields.GetLength(0);i++)
			{
				if(m_sKeyField==p_sFields[i,1])
					m_sKeyTag = p_sFields[i,0];
				this.m_FieldList[p_sFields[i,0]] = p_sFields[i,1];
			}			
		}

	
		/// <summary>
		/// E V E N T S 
		/// </summary>
		public event InitObjEventHandler InitObj;


		/// Name		: MoveFirst
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveFirst
		/// </summary>
		protected virtual void MoveFirst()
		{
			MoveAndLoad(MOVE_FIRST);
		}

		/// Name		: MoveLast
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveLast
		/// </summary>
		protected virtual void MoveLast()
		{
			MoveAndLoad(MOVE_LAST);
		}

		/// Name		: MoveNext
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveNext
		/// </summary>
		protected virtual void MoveNext()
		{
			m_lKeyFieldValue = GetKeyFieldValue();
			MoveAndLoad(MOVE_NEXT);
		}

		/// Name		: MovePrevious
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MovePrevious
		/// </summary>
		protected virtual void MovePrevious()
		{
			m_lKeyFieldValue = GetKeyFieldValue();
			MoveAndLoad(MOVE_PREVIOUS);
		}

		/// Name		: MoveTo
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveTo
		/// </summary>
		/// <param name="keyValue">Key Value</param>
		protected virtual void MoveTo( params int[] keyValue){}

		/// Name		: MoveAndLoad
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// MoveAndLoad
		/// </summary>
		/// <param name="moveDirection">Move Direction</param>
		protected void MoveAndLoad(int moveDirection)
		{
			DbReader rdr= null;
			string sSQL="";
			int lNewID=0;
			DbConnection objConn = null;
            //aanandpraka2:Changes for Pen testing-03-Jan-12            
            StringBuilder sbSql = new StringBuilder();
            //aanandpraka2:End Changes
			try
			{
				switch (moveDirection)
				{
					case MOVE_FIRST:
                        //aanandpraka2:Changes for Pen testing-03-Jan-12 
						//sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>0",this.m_sKeyField,this.m_sTableName, this.m_sKeyField);
                        sbSql.Append(String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>0", this.m_sKeyField, this.m_sTableName, this.m_sKeyField)); 
						break;
					case MOVE_LAST:
						//sSQL = String.Format("SELECT MAX({0}) FROM {1}",this.m_sKeyField,this.m_sTableName);
                        sbSql.Append(String.Format("SELECT MAX({0}) FROM {1} ", this.m_sKeyField, this.m_sTableName));
						break;
					case MOVE_NEXT:
						//sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
                        sbSql.Append(String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}", this.m_sKeyField, this.m_sTableName, this.m_sKeyField, "~m_lKeyFieldValue~"));

                        this.dictParams.Add("m_lKeyFieldValue", m_lKeyFieldValue);                        
						break;
					case MOVE_PREVIOUS:
                        if (m_lKeyFieldValue == 0)
                        {
                            //sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
                            sbSql.Append(String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}", this.m_sKeyField, this.m_sTableName, this.m_sKeyField, "~m_lKeyFieldValue~"));
                            this.dictParams.Add("m_lKeyFieldValue", m_lKeyFieldValue);                            
                        }
                        else
                        {
                            //sSQL = String.Format("SELECT MAX({0}) FROM {1} WHERE {2}<{3}", this.m_sKeyField, this.m_sTableName, this.m_sKeyField, m_lKeyFieldValue);
                            sbSql.Append(String.Format("SELECT MAX({0}) FROM {1} WHERE {2}<{3}", this.m_sKeyField, this.m_sTableName, this.m_sKeyField, "~m_lKeyFieldValue~"));
                            this.dictParams.Add("m_lKeyFieldValue", m_lKeyFieldValue);
                            //aanandpraka2:End Changes
                        }
						break;
				}
                //aanandpraka2:Changes for Pen testing-03-Jan-12 
                objConn = DbFactory.GetDbConnection(m_sConnString);
				//objConn.Open(); 
				//rdr = objConn.ExecuteReader(sSQL);
                rdr = DbFactory.ExecuteReader(objConn.ConnectionString, sbSql.ToString(), this.dictParams);
                //aanandpraka2:End Changes
				///Fetch the RowId
				if (rdr.Read())
					lNewID = rdr.GetInt(0);
				rdr.Close();
				//rdr = null;
				objConn.Dispose(); 
				
				//Only if New Unique ID is not 0 and New Unique ID different than old one
				if (lNewID != 0 && lNewID != m_lKeyFieldValue)
				{
					m_lKeyFieldValue = lNewID;
				}
				Get();
                this.dictParams.Clear();//aanandpraka2:Pen Testing-Clear the dictionary parameters
			}
			finally
			{
                if (rdr != null)
                {
                    rdr.Close();
                    rdr.Dispose();
                }                
			}
		}

		/// Name		: Save
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save method
		/// </summary>
		protected virtual void Save()
		{
			XmlNodeList objXmlNodeList = null;
            //Commented by bsharma33 Regions Pan Testing MITS : 26942, not in use
			//string sSql = string.Empty;
			//string sFieldNames = string.Empty;
			//string sFieldValues = string.Empty; 
            //End changes by bsharma33 Regions Pan Testing MITS : 26942, not in use
			DbConnection objConn = null;
			string sControlType="";
            //Added by bsharma33 for regions Pen Testing MITS 26942
            DbWriter writer = null;
            string sFieldName, sFieldValue;
            //end changes by bsharma33 for regions Pen Testing MITS 26942
			try
			{
				if(!ValidateXml())
					throw new Exception("Unable to validate the Input XML"); 
			
				objXmlNodeList = m_objXmlDoc.GetElementsByTagName("control");
			
				m_lKeyFieldValue = GetKeyFieldValue();
                //Added by bsharma33 for regions Pen Testing MITS 26942
                objConn = DbFactory.GetDbConnection(m_sConnString);
                writer = DbFactory.GetDbWriter(objConn);
                writer.Tables.Add(this.m_sTableName);
                //End changes by bsharma33 for regions Pen Testing MITS 26942
				if(m_lKeyFieldValue >0)
				{
                                     
					foreach(XmlElement objXmlElement in objXmlNodeList)
					{
						if(m_FieldList[objXmlElement.GetAttribute("name")] != null)
						{
							if(m_FieldList[objXmlElement.GetAttribute("name")].ToString() != m_sKeyField)
							{
								sControlType= objXmlElement.GetAttribute("type").Trim().ToLower();
								switch(sControlType)
								{
									case "radio":
									case "combobox":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.GetAttribute("value").ToUpper() == "NULL"? "null" : "'" + objXmlElement.GetAttribute("value") + "'");
                                        sFieldValue = objXmlElement.GetAttribute("value").ToUpper() == "NULL" ? "null" : objXmlElement.GetAttribute("value");
                                        //Endchanges by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "checkbox":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.InnerText.ToLower() == "true"? "1" : "0");
                                        sFieldValue = objXmlElement.InnerText.ToLower() == "true" ? "1" : "0";
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
										break;
									case "date":
                                        //Parijat : Mits 11446
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], (objXmlElement.InnerText.ToUpper() == "NULL" ||objXmlElement.InnerText.ToUpper() == "")? "null" : "'" + UTILITY.FormatDate(objXmlElement.InnerText,true) + "'");
                                        sFieldValue = (objXmlElement.InnerText.ToUpper() == "NULL" || objXmlElement.InnerText.ToUpper() == "") ? null : UTILITY.FormatDate(objXmlElement.InnerText, true);
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "time":
                                        //Changed by bsharma33 Regions Pan Testing MITS : 26942
										//sSql += String.Format(" {0} = '{1}'," , this.m_FieldList[objXmlElement.GetAttribute("name")], Conversion.GetTime(objXmlElement.InnerText));
										sFieldValue = Conversion.GetTime(objXmlElement.InnerText);
                                        //End changes by bsharma33 Regions Pan Testing MITS : 26942
                                        break;
									case "code":
                                     
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.GetAttribute("codeid"));   
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")],  objXmlElement.GetAttribute("codeid").ToUpper() == "NULL"? "null" : "'" +  objXmlElement.GetAttribute("codeid") + "'");
										sFieldValue = objXmlElement.GetAttribute("codeid").ToUpper() == "NULL"? "null" : objXmlElement.GetAttribute("codeid");
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "orgh":
                                        //Changed by bsharma33 Regions Pan Testing MITS : 26942
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.GetAttribute("codeid"));
										sFieldValue = objXmlElement.GetAttribute("codeid");
                                        //End changes by bsharma33 Regions Pan Testing MITS : 26942
                                        break;
									default:
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sSql += String.Format(" {0} = {1}," , this.m_FieldList[objXmlElement.GetAttribute("name")], objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + objXmlElement.InnerText + "'");
										sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : objXmlElement.InnerText;
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;

								}
                                //Added by bsharma33 for regions Pen Testing MITS 26942
                                sFieldName = this.m_FieldList[objXmlElement.GetAttribute("name")].ToString();
                                writer.Fields.Add(sFieldName, sFieldValue);
                                //End changes by bsharma33 for regions Pen Testing MITS 26942
                               
							}
						}
					}
                    //Changed by bsharma33 for regions Pen Testing MITS 26942
					//if(sSql != string.Empty)
					//{
						//sSql = sSql.Substring(0,sSql.Length - 1);
						//sSql = String.Format("UPDATE {0} SET {1} WHERE {2} = {3}",this.m_sTableName,sSql,this.m_sKeyField,m_lKeyFieldValue);
                        WhereClause = string.Format("{0}={1}",this.m_sKeyField,m_lKeyFieldValue);
                                                
						if(m_sWhereClause!="")
						{
                            WhereClause = WhereClause + " AND "+m_sWhereClause;
                            //if( sSql.IndexOf("WHERE")>0)
                            //    sSql = sSql + " AND " +  m_sWhereClause;
                            //else
                            //    sSql = " WHERE " + m_sWhereClause;
						}
                        writer.Where.Add(WhereClause);
					//}
                    //End Changes by bsharma33 for regions Pen Testing MITS 26942
				}
				else
				{
                    m_lKeyFieldValue = Riskmaster.Common.Utilities.GetNextUID(m_sConnString, m_sTableName, m_iClientId);

					foreach(XmlElement objXmlElement in objXmlNodeList)
					{	
						if(m_FieldList[objXmlElement.GetAttribute("name")] != null)
						{
							if(m_FieldList[objXmlElement.GetAttribute("name")].ToString() == m_sKeyField)
							{
                                //Changed by bsharma33 for regions Pen Testing MITS 26942
								//sFieldValues += String.Format("{0},", m_lKeyFieldValue);
								//sFieldNames += String.Format("{0},", this.m_FieldList[objXmlElement.GetAttribute("name")]);
                                sFieldName = Convert.ToString(this.m_FieldList[objXmlElement.GetAttribute("name")]);
                                sFieldValue =  Convert.ToString(m_lKeyFieldValue);
                                writer.Fields.Add(sFieldName, sFieldValue);
                                //End changes by bsharma33 for regions Pen Testing MITS 26942

							}
							else if(objXmlElement.InnerText.Trim() != string.Empty)
							{
								sControlType = objXmlElement.GetAttribute("type").Trim().ToLower();
								switch(sControlType)
								{
									case "radio":
									case "combobox":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sFieldValues += String.Format("{0},", objXmlElement.GetAttribute("value").ToUpper() == "NULL"? "null" : "'" + objXmlElement.GetAttribute("value") + "'");	
										 sFieldValue =  Convert.ToString(m_lKeyFieldValue);
                                         //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "checkbox":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//if (objXmlElement.InnerText.ToLower().Trim().Equals("true"))
										//	sFieldValues += String.Format("{0},","1");
										//else
										//	sFieldValues += String.Format("{0},","0");
                                        if (objXmlElement.InnerText.ToLower().Trim().Equals("true"))
                                            sFieldValue = "1";
                                        else
                                            sFieldValue = "0";
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "date":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sFieldValues += String.Format("{0},", objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + UTILITY.FormatDate(objXmlElement.InnerText,true) + "'");
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : UTILITY.FormatDate(objXmlElement.InnerText, true);
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "time":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sFieldValues += String.Format("{0},",objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + Conversion.GetTime(objXmlElement.InnerText) + "'");
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : Conversion.GetTime(objXmlElement.InnerText);
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "code":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//String.Format("{0},",objXmlElement.InnerText.ToUpper() == "NULL"? "null" :  objXmlElement.GetAttribute("codeid") );
										//sFieldValues += String.Format("{0},", objXmlElement.GetAttribute("codeid").ToUpper() == "NULL"? "null" : "'" + objXmlElement.GetAttribute("codeid") + "'");
                                        sFieldValue = objXmlElement.GetAttribute("codeid").ToUpper() == "NULL" ? "null" : objXmlElement.GetAttribute("codeid");
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									case "orgh":
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sFieldValues += String.Format("{0},",objXmlElement.InnerText.ToUpper() == "NULL"? "null" :  objXmlElement.GetAttribute("codeid") );
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" : objXmlElement.GetAttribute("codeid");
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
									default:
                                        //Changed by bsharma33 for regions Pen Testing MITS 26942
										//sFieldValues += String.Format("{0},", objXmlElement.InnerText.ToUpper() == "NULL"? "null" : "'" + objXmlElement.InnerText + "'");
                                        sFieldValue = objXmlElement.InnerText.ToUpper() == "NULL" ? "null" :  objXmlElement.InnerText;
                                        //End changes by bsharma33 for regions Pen Testing MITS 26942
                                        break;
								}
                                //Changed by bsharma33 for regions Pen Testing MITS 26942
								//sFieldNames += String.Format("{0},", this.m_FieldList[objXmlElement.GetAttribute("name")]);
                                sFieldName = this.m_FieldList[objXmlElement.GetAttribute("name")].ToString();
                                writer.Fields.Add(sFieldName, sFieldValue);
                                //End changes by bsharma33 for regions Pen Testing MITS 26942
							}
						}
					}
                    //Changed by bsharma33 for regions Pen Testing MITS 26942
					//if(sFieldNames != string.Empty && sFieldValues != string.Empty)
					//{
						//sFieldNames = sFieldNames.Substring(0,sFieldNames.Length - 1);
						//sFieldValues = sFieldValues.Substring(0,sFieldValues.Length - 1);
						//sSql = String.Format("INSERT INTO {0} ({1}) VALUES ({2})",this.m_sTableName,sFieldNames,sFieldValues);

					//}
				}			
				//objConn = DbFactory.GetDbConnection(m_sConnString);
				//objConn.Open(); 
				//objConn.ExecuteNonQuery(sSql);
                writer.Execute();
                //End changes by bsharma33 for regions Pen Testing MITS 26942
				 
			}
			finally
			{
				objXmlNodeList = null;
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}
		
		/// Name		: Get
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Base Get Implementation
		/// </summary>
		protected virtual void Get()
		{
            Get(false);
		}

        /// <summary>
        /// For Oracle database, the value has to be inside single quota if it's a number
        /// </summary>
        /// <param name="bIsString">
        ///     database column data type. true: the value needs to be inside single quota
        ///     false: determined if it can be converted to number
        /// </param>
        protected virtual void Get(bool bIsString)
        {
            string sSQL = "";
            DbConnection objConn = null;
            DbReader objDbReader = null;
            XmlNodeList objNodeList = null;
            string sValue = "";
            LocalCache objLocalCache = null;
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sControlType = "";
            XmlElement multiItemElement = null;
            //aanandpraka2:Changes for Pen Testing 4-Jan-12
            StringBuilder sbSql = new StringBuilder();
            //aanandpraka2:End Changes

            try
            {
                if (bIsString)
                {
                    GetStringKeyFieldValue();
                    //aanandpraka2:Changes start for Pen Testing-4-Jan-12
                    //sSQL = String.Format("SELECT * FROM {0} WHERE {1} = '{2}'", m_sTableName, m_sKeyField, m_sKeyFieldValue);
                    sbSql.Append(string.Format("SELECT * FROM {0} WHERE {1} = {2}", m_sTableName, m_sKeyField, "~m_sKeyFieldValue~"));

                    this.dictParams.Add("m_sKeyFieldValue",m_sKeyFieldValue);
                    //aanandpraka2:End Changes
                }
                else
                {
                    if (m_lKeyFieldValue == 0)
                        m_lKeyFieldValue = GetKeyFieldValue();
                    if (m_lKeyFieldValue == 0)
                    {
                        //For situation where the key field is a string column
                        if (m_sKeyFieldValue != "" && (Conversion.ConvertStrToInteger(m_sKeyFieldValue) == 0))
                        {
                            //sSQL = String.Format("SELECT * FROM {0} WHERE {1} = '{2}'", m_sTableName, m_sKeyField, m_sKeyFieldValue);
                            sbSql.Append(string.Format("SELECT * FROM {0} WHERE {1} = {2}", m_sTableName, m_sKeyField, "~m_sKeyFieldValue~"));
                            this.dictParams.Add("m_sKeyFieldValue", m_sKeyFieldValue);
                        }
                        else
                        {
                            //sSQL = String.Format("SELECT * FROM {0}", m_sTableName);
                            sbSql.Append(string.Format("SELECT * FROM {0}", m_sTableName));
                        }
                    }
                    else
                    {
                        //sSQL = String.Format("SELECT * FROM {0} WHERE {1} = {2}", m_sTableName, m_sKeyField, m_lKeyFieldValue);
                        sbSql.Append(string.Format("SELECT * FROM {0} WHERE {1} = {2}", m_sTableName, m_sKeyField, "~m_lKeyFieldValue~"));
                        this.dictParams.Add("m_lKeyFieldValue", m_lKeyFieldValue);
                    }
                }

                if (m_sWhereClause != "")
                {
                    if (sSQL.IndexOf("WHERE") > 0)
                    {
                        sSQL = sSQL + " AND " + m_sWhereClause;
                    }
                    else
                    {
                        sSQL = sSQL + " WHERE " + m_sWhereClause;
                    }
                }

                //aanandpraka2:Changes for Pen Testing-4-Jan-12
                //objConn = DbFactory.GetDbConnection(m_sConnString);
                //objConn.Open();
                //objDbReader = objConn.ExecuteReader(sSQL);
                objDbReader = DbFactory.ExecuteReader(m_sConnString, sbSql.ToString(), this.dictParams);
                this.dictParams.Clear();
                //aanandpraka2:End Changes

                objLocalCache = new LocalCache(m_sConnString, m_iClientId);

                if (objDbReader != null)
                {
                    if (objDbReader.Read())
                    {
                        foreach (string sName in m_FieldList.Keys)
                        {
                            sValue = objDbReader[m_FieldList[sName].ToString()].ToString();
                            objNodeList = m_objXmlDoc.GetElementsByTagName("control");
                            foreach (XmlElement objXmlElement in objNodeList)
                            {
                                if (objXmlElement.GetAttribute("name") == sName)
                                {
                                    sControlType = objXmlElement.GetAttribute("type").ToLower();
                                    switch (sControlType)
                                    {
                                        case "checkbox":
                                            if (sValue.Equals("-1") || sValue.Equals("1"))
                                                objXmlElement.InnerText = "True";
                                            else
                                                objXmlElement.InnerText = "";
                                            break;
                                        case "code":
                                            objXmlElement.SetAttribute("codeid", sValue);
                                            if (objXmlElement.GetAttribute("codetable") != null)
                                            {
                                                if (objXmlElement.GetAttribute("codetable").Trim().ToUpper() == "STATES")
                                                    objXmlElement.InnerText = UTILITY.GetState(sValue, m_sConnString, m_iClientId);
                                                else
                                                    objXmlElement.InnerText = UTILITY.GetCode(sValue, m_sConnString, m_iClientId);
                                            }
                                            break;
                                        case "state":
                                            objXmlElement.SetAttribute("codeid", sValue);
                                            objXmlElement.InnerText = UTILITY.GetState(sValue, m_sConnString, m_iClientId);
                                            break;
                                        case "date":
                                            objXmlElement.InnerText = Conversion.GetUIDate(sValue,mUserlogin.objUser.NlsCode.ToString(), m_iClientId);//vkumar258 RMA-6037
                                            break;
                                        //Radio button and combo are same except appearance
                                        case "radio":
                                        case "combobox":
                                            objXmlElement.SetAttribute("value", sValue);
                                            break;
                                        case "time":
                                            objXmlElement.InnerText = Conversion.GetDBTimeFormat(sValue, "t");
                                            break;
                                        case "orgh":
                                            objXmlElement.SetAttribute("codeid", sValue);
                                            objXmlElement.InnerText = UTILITY.GetOrg(sValue, m_sConnString, m_iClientId);
                                            break;
                                        case "orglist":
                                            XmlElement objNew = null;
                                            objXmlElement.InnerXml = "";
                                            objXmlElement.SetAttribute("orgid", sValue);
                                            objNew = m_objXmlDoc.CreateElement("option");
                                            objNew.SetAttribute("value", sValue);
                                            objNew.InnerText = UTILITY.GetOrg(sValue, m_sConnString, m_iClientId);
                                            objXmlElement.AppendChild(objNew);
                                            break;
                                        case "multicode":
                                            multiItemElement = objXmlElement;
                                            UTILITY.GetXmlMultiCodeInfo(ref multiItemElement, ref m_objXmlDoc, sValue, ref objLocalCache, m_iClientId);
                                            break;
                                        default:
                                            objXmlElement.InnerText = sValue;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    objDbReader.Close();
                }
                //objConn.Dispose();//aanandpraka2
                LoadChildXml();
            }
            finally
            {
                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader = null;
                }
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
                objNodeList = null;
            }
        }
		
		/// Name		: Delete
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Delete method
		/// </summary>
		/// <returns>boolean success</returns>
		protected virtual bool Delete()
		{
			string sSQL="";
			DbConnection objConn = null;
            //aanandpraka2:Changes for Pen Testing-03-Jan-12
            StringBuilder sbSQL = new StringBuilder();
           //aanandpraka2:End Changes	

			try
			{
				m_lKeyFieldValue = GetKeyFieldValue();
                //aanandpraka2:Changes for Pen Testing-03-Jan-12
				//sSQL= String.Format("DELETE FROM {0} WHERE {1} = {2}",m_sTableName,m_sKeyField,m_lKeyFieldValue);
                //if(m_sWhereClause!="")
                //    sSQL= sSQL+ " AND " + m_sWhereClause;
                //
                //objConn.Open(); 
                //objConn.ExecuteNonQuery(sSQL); 
                //objConn.Dispose();

                sbSQL.Append(string.Format("DELETE FROM {0} WHERE {1} = {2}", m_sTableName, m_sKeyField, "~m_lKeyFieldValue~"));
                this.dictParams.Add("m_lKeyFieldValue", m_lKeyFieldValue);
                //objConn = DbFactory.GetDbConnection(m_sConnString);
                DbFactory.ExecuteScalar(m_sConnString, sbSQL.ToString(), this.dictParams);
                this.dictParams.Clear();
				return true;
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Dispose();
					objConn=null;
				}
                dictParams.Clear();//aanandpraka2:Changes for Pen Testing
			}
		}

		/// Name		: ValidateXml
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Validate the input xml for consistency
		/// </summary>
		/// <returns>boolean success</returns>
		protected virtual bool ValidateXml()
		{
			XmlNodeList objNodeList = null;
			bool bFound = false;
			try
			{
				foreach(string sName in m_FieldList.Keys)
				{
					objNodeList = m_objXmlDoc.GetElementsByTagName("control");
				
					foreach(XmlElement objXmlElement in objNodeList)
					{
						if(objXmlElement.GetAttribute("name") == sName)
						{
							bFound = true;
							break;
						}
					}
					if(bFound == false)
						return false;
					bFound = false;
				}
				return true;
			}
			finally
			{
				objNodeList=null;
			}
		}

		/// Name		: GetKeyFieldValue
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the key field value from the xml
		/// </summary>
		/// <returns>key field value</returns>
		private long GetKeyFieldValue()
		{
			XmlNodeList objXmlNodeList;
			long lKeyFieldValue = 0;

			try
			{
				objXmlNodeList = m_objXmlDoc.GetElementsByTagName("control");
				foreach(XmlElement objXmlElement in objXmlNodeList)
				{
					if(objXmlElement.GetAttribute("name") == m_sKeyTag)
					{
						if(objXmlElement.GetAttribute("type")=="code")
						{
							lKeyFieldValue = Conversion.ConvertStrToLong(objXmlElement.GetAttribute("codeid"));
							m_sKeyFieldValue = objXmlElement.GetAttribute("codeid");
						}
						else
						{
							lKeyFieldValue = Conversion.ConvertStrToLong(objXmlElement.InnerText); 
							m_sKeyFieldValue = objXmlElement.InnerText;
						}
						break;
					}
				}

				return lKeyFieldValue; 
			}
			finally
			{
				objXmlNodeList=null;
			}
		}

        /// <summary>
        /// Get string value of key field
        /// </summary>
        /// <returns></returns>
        private string GetStringKeyFieldValue()
        {
            string sKeyFieldValue = string.Empty;

            XmlElement objXmlElement = (XmlElement)m_objXmlDoc.SelectSingleNode(string.Format("//control[@name='{0}']", m_sKeyTag));
            if (objXmlElement != null)
            {
                if (objXmlElement.GetAttribute("type") == "code")
                {
                    sKeyFieldValue = objXmlElement.GetAttribute("codeid");
                    m_sKeyFieldValue = objXmlElement.GetAttribute("codeid");
                }
                else
                {
                    sKeyFieldValue = objXmlElement.InnerText;
                    m_sKeyFieldValue = objXmlElement.InnerText;
                }
            }
            return sKeyFieldValue;
        }

		/// Name		: LoadChildXml
		/// Author		: Anurag, Pankaj
		/// Date Created: 03/01/2005
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Populates the combo and other lists
		/// </summary>
		private void LoadChildXml()
		{ 
			XmlNodeList objNodeList = null;
			XmlElement objNewElm = null;
			DataSet objDs = null;
			string sSQL = "";

			try
			{
				for(int i =0; i< m_arrChild.GetLength(0);i++)
				{
					objNodeList = m_objXmlDoc.GetElementsByTagName("control");

					foreach(XmlElement objElm in objNodeList)
					{
						if(objElm.GetAttribute("name") == m_arrChild[i,0])
						{
							//string sValue = objElm.InnerText;
							objElm.SetAttribute("value", objElm.InnerText);
							objElm.InnerText =""; 
							sSQL = String.Format("SELECT {0} , {1} FROM {2} WHERE {3}",m_arrChild[i,1],m_arrChild[i,2],m_arrChild[i,3],m_arrChild[i,4]);
							objDs = DbFactory.GetDataSet(m_sConnString, sSQL, m_iClientId);
							foreach(DataRow objRow in objDs.Tables[0].Rows)
							{
								string sDesc = DbRow.GetStringField(objRow,m_arrChild[i,2]).ToLower();
								long lValue = DbRow.GetLngField(objRow,m_arrChild[i,1]);
								//if (lValue == 0)
								//	sDesc ="None";							
								objNewElm = XMLDoc.CreateElement("option");
								objNewElm.SetAttribute("value", lValue.ToString());
								objNewElm.InnerText = sDesc;
								//if ( sValue == lValue.ToString())
								//	objNewElm.SetAttribute("selected", "1");
								objElm.AppendChild(objNewElm);
							}
						}
					}
				}
			}
			finally
			{
				objNodeList = null;
				objNewElm = null;
				if(objDs!=null)
				{
					objDs.Dispose();
					objDs=null;
				}
			}
		}
	}
}