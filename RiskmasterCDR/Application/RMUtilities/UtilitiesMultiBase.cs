using System;
using System.Xml;
using System.Collections; 
using Riskmaster.Db;
using Riskmaster.Common;
using System.Data;
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag, Pankaj
	///Dated   :   1st,Mar 2005
	///Purpose :   Base Class for multiple List Type Forms
	/// </summary>
	public class UtilitiesMultiBase
	{
		private int m_iDsnId;

		/// <summary>
		/// Key Field
		/// </summary>
		internal string m_sKeyField;

		/// <summary>
		/// Table Name
		/// </summary>
		private string m_sTableName;

		/// <summary>
		/// XML Document
		/// </summary>
		private XmlDocument m_objXmlDoc;		

		/// <summary>
		/// Connection String
		/// </summary>
		private string m_sConnString;

		/// <summary>
		/// Child Connection string
		/// </summary>
		private string m_sChildConnString;

		/// <summary>
		/// Key tag
		/// </summary>
		private string m_sKeyTag;

        /// <summary>
        /// gagnihotri MITS 11995 Changes made for Audit table
        /// User Login Name
        /// </summary>
        private string m_sUserName;

		/// <summary>
		/// Internal Datafield List variable
		/// </summary>
		internal DataFieldList m_FieldList=new DataFieldList(); 

		/// <summary>
		/// Internal Key Datafield List variable
		/// </summary>
		internal DataFieldList m_KeyFieldList=new DataFieldList(); 

		/// <summary>
		/// Child array variable
		/// </summary>
		protected string[,] m_arrChild =  new string[10,5];

		/// <summary>
		/// Hash table collection
		/// </summary>
		internal Hashtable m_PropertyMap = new Hashtable();

        private int m_iClientId = 0;

		#region "Properties"

		protected int DsnId
		{
			get{return m_iDsnId;}
			set{m_iDsnId = value;}
		}

		/// <summary>
		/// Table name
		/// </summary>
		protected string TableName
		{
			get{return m_sTableName;}
			set{m_sTableName = value;}
		}

		/// <summary>
		/// Key field
		/// </summary>
		protected string KeyField
		{
			get{return m_sKeyField;}
			set{m_sKeyField = value;}
		}

		/// <summary>
		/// XMl Doc
		/// </summary>
		protected XmlDocument XMLDoc
		{
			get{return m_objXmlDoc;}
			set{m_objXmlDoc = value;}
		}

		/// <summary>
		/// Connection String
		/// </summary>
		internal string ConnectString
		{
			get{return m_sConnString;}
			set{m_sConnString = value;}
		}

		/// <summary>
		/// Child Connection String
		/// </summary>
		internal string ChildConnectString
		{
			get{return m_sChildConnString;}
			set{m_sChildConnString = value;}
		}

		/// <summary>
		/// Child Array
		/// </summary>
		protected string[,] ChildArray
		{
			set{m_arrChild = value;}
		}

		#endregion

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString"></param>
		internal UtilitiesMultiBase(string p_sConnString, int p_iClientId)//: base(true,p_objCtx)
		{
            m_iClientId = p_iClientId;
			Initialize();
		}

		/// <summary>
		/// Over Loaded Constructor
		/// </summary>
		/// <param name="p_sUserName">User name</param>
		/// <param name="p_sPassword">password</param>
		/// <param name="p_sDatabaseName">Database Name</param>
        internal UtilitiesMultiBase(string p_sUserName, string p_sPassword, string p_sDatabaseName, int p_iClientId)//: base(true,p_objCtx)
		{
            m_iClientId = p_iClientId;
			Initialize();
		}

        //gagnihotri MITS 11995 Changes made for Audit table
        internal UtilitiesMultiBase(string p_sConnString, string p_sUserName, int p_iClientId)//: base(true,p_objCtx)
        {
            m_iClientId = p_iClientId;
            m_sUserName = p_sUserName;
            Initialize();
        }

		/// <summary>
		/// Initialize
		/// </summary>
		protected void Initialize()
		{
		}

		/// <summary>
		/// Add all obect Fields into the Field Collection from our Field List.
		/// </summary>
		/// <param name="p_sFields">array of fields</param>
		protected virtual void InitFields(string[,] p_sFields)
		{ 
			for(int i =0; i< p_sFields.GetLength(0);i++)
			{
				if(m_sKeyField==p_sFields[i,1])
					m_sKeyTag = p_sFields[i,0];
				this.m_FieldList[p_sFields[i,0]] = p_sFields[i,1];
			}			
		}

		/// <summary>
		/// Add all object Key Fields into the Field Collection from our Field List.
		/// </summary>
		/// <param name="p_sFields"></param>
		protected virtual void InitKeyFields(string[,] p_sFields)
		{ 
			for(int i =0; i< p_sFields.GetLength(0);i++)
			{
				this.m_KeyFieldList[p_sFields[i,0]] = p_sFields[i,1];
			}			
		}

		/// <summary>
		/// Base Save implementation
		/// </summary>
		/// <param name="p_bIsDelete">boolean for whether deletion should be performed before saving</param>
		protected virtual void Save(bool p_bIsDelete)
		{
			XmlNodeList objXmlNodeList = null;
			DbConnection objConn = null;
			DataFieldList objXMLEleList = new DataFieldList();
            //Added by bsharma33 for Region Pan testing MITS 26941
            DbWriter writer = null;
            System.Collections.Generic.Dictionary<string, string> dictParams = new System.Collections.Generic.Dictionary<string, string>();
            //End additions by bsharma33 for Region Pan testing MITS 26941
			string sSQL="";
			
			try
			{
				if(!ValidateXml())
					throw new Exception("Unable to validate the Input XML"); 
			
				objXmlNodeList = m_objXmlDoc.GetElementsByTagName("control");

				foreach(XmlElement objElm in objXmlNodeList )
				{
					if (objElm.GetAttribute("type").ToLower() =="code")
                        objXMLEleList[objElm.GetAttribute("name")] = Conversion.ConvertStrToInteger(objElm.GetAttribute("codeid"));   //Aman MITS 27418
					else
						objXMLEleList[objElm.GetAttribute("name")] = objElm.InnerText;   
				}

                objConn = DbFactory.GetDbConnection(m_sConnString);
                //changed by bsharma33 for Region Pan testing  MITS 26941
                //objConn.Open(); 
                 writer = DbFactory.GetDbWriter(objConn);
                //end changes by bsharma33 MITS 26941
				foreach(string sName in m_FieldList.Keys)
				{   //Commented and changed by bsharma33, not in use.
					//string sFieldNames = "";
					//string sFieldValues = "";
                    //writer.Tables.Add(this.m_sTableName);  //Aman MITS 27418 --Table Name was being added again and again in the insert query
                    //end changes
					if(m_FieldList[sName].ToString() == m_sKeyField)
					{
						if(objXMLEleList[sName].ToString() != "")
						{                            
							if(p_bIsDelete)
							{
                                //Changed by bsharma33 for Region Pan testing  MITS 26941
                                sSQL = string.Format("DELETE FROM {0} WHERE {1} = {2}", m_sTableName, m_sKeyField, "~m_sKeyFieldValue~");
                                dictParams.Add("m_sKeyFieldValue",objXMLEleList[sName].ToString());
                                DbFactory.ExecuteNonQuery(m_sConnString, sSQL, dictParams);
                                dictParams.Clear();
								//objConn.ExecuteNonQuery(sSQL);
                                //End changes by bsharma33 for Region Pan testing  MITS 26941
								string[] arrColumnValues = m_KeyFieldList[objXMLEleList[sName].ToString()].ToString().Split(',');

								for(int i = 0; i < arrColumnValues.Length; i++)
								{
                                    //Changed by bsharma33 for Region Pan testing  MITS 26941
									//sFieldNames += m_FieldList[arrColumnValues[i].ToString()].ToString() + ",";
									//sFieldValues += objXMLEleList[arrColumnValues[i].ToString()].ToString() + ",";
                                    writer.Fields.Add(m_FieldList[arrColumnValues[i].ToString()].ToString(), objXMLEleList[arrColumnValues[i].ToString()].ToString());
                                    //End changes by bsharma33 for Region Pan testing  MITS 26941
								}
                                //Changed by bsharma33 for Region Pan testing  MITS 26941
								//if(sFieldNames != string.Empty && sFieldValues != string.Empty)
                                if(writer.Fields.Count>0)
								{
                                    //gagnihotri MITS 11995 Changes made for Audit table
                                    if (m_sUserName != "")
                                    {
                                        //sFieldNames += "UPDATED_BY_USER,DTTM_RCD_LAST_UPD,";
                                        //sFieldValues += "'" + m_sUserName + "'," + Conversion.ToDbDateTime(DateTime.Now) + ",";
                                        writer.Fields.Add("UPDATED_BY_USER", m_sUserName);
                                        writer.Fields.Add("DTTM_RCD_LAST_UPD", Conversion.ToDbDateTime(DateTime.Now));
                                    }
									//sFieldNames = sFieldNames.Substring(0,sFieldNames.Length - 1);
									//sFieldValues = sFieldValues.Substring(0,sFieldValues.Length - 1);
									//sSQL = string.Format("INSERT INTO {0} ({1}) VALUES ({2})",m_sTableName,sFieldNames,sFieldValues);
									//try
									//{
										//objConn.ExecuteNonQuery(sSQL);
                                       
									//}
									//catch{}
								}
                                //End changes by bsharma33 for Region Pan testing  MITS 26941
							}
							else
							{
								string[] arrColumnValues = m_KeyFieldList[objXMLEleList[sName].ToString()].ToString().Split(',');

								for(int i = 0; i < arrColumnValues.Length; i++)
								{
                                    //Changed by bsharma33 for Region Pan testing  MITS 26941
									//sFieldNames += m_FieldList[arrColumnValues[i].ToString()].ToString() + " = '" + objXMLEleList[arrColumnValues[i].ToString()].ToString() + "',";
                                    writer.Fields.Add(m_FieldList[arrColumnValues[i].ToString()].ToString(), objXMLEleList[arrColumnValues[i].ToString()].ToString());
                                    //End changes by bsharma33 for Region Pan testing  MITS 26941
								}
								//if(sFieldNames != string.Empty)
                                if(writer.Fields.Count>0)
								{
                                    //changed by bsharma33 for Region Pan testing  MITS 26941
									//sFieldNames = sFieldNames.Substring(0,sFieldNames.Length - 1);
									//sSQL = string.Format("UPDATE {0} SET {1} WHERE {2} = '{3}'",m_sTableName,sFieldNames,m_sKeyField,objXMLEleList[sName].ToString());
                                    writer.Where.Add(string.Format(" {0} = '{1}'",  m_sKeyField, objXMLEleList[sName].ToString()));  //Aman added the single quotes in the column name for medical info setup
                                    //objConn.ExecuteNonQuery(sSQL);
                                    //End changes by bsharma33 for Region Pan testing  MITS 26941
								}
							
							}
                            //Changed by bsharma33 for Region Pan testing  MITS 26941
                            try
                            {
                                writer.Tables.Add(this.m_sTableName);   //Aman MITS 27418
                                writer.Execute();
                                writer.Reset(true);
                            }
                            catch
                            {
                                writer.Reset(true);   //Aman MITS 27418
                            }
                            //End changes by bsharma33 for Region Pan testing  MITS 26941
						}
					}
				}
			}
			finally
			{

				if(objConn != null)
				  objConn.Dispose();
                
                writer = null;//Added by bsharma33 for Region Pan testing  MITS 26941
				objXmlNodeList = null;
				objXMLEleList = null;
			}
		}
		
		/// <summary>
		/// base get implementation
		/// </summary>
		protected virtual void Get()
		{
			DataSet objDataSet = null;
			XmlNodeList objNodeList = null;
			string sSQL = "";

			try
			{
				sSQL = String.Format("SELECT * FROM {0}",m_sTableName);

				objDataSet = DbFactory.GetDataSet(m_sConnString,sSQL, m_iClientId);

				foreach(DataRow objDataRow in objDataSet.Tables[0].Rows)
				{

					string sColumnNames = "";
					if(m_KeyFieldList[objDataRow[m_sKeyField].ToString()] != null)
						sColumnNames = m_KeyFieldList[objDataRow[m_sKeyField].ToString()].ToString();
					else
						sColumnNames = "0";
					if( sColumnNames != null && sColumnNames != "0")
					{
						string[] arrColumns = sColumnNames.Split(',');
						for(int i = 0; i< arrColumns.Length ; i++) 
						{
							objNodeList = m_objXmlDoc.GetElementsByTagName("control");
							foreach(XmlElement objXmlElement in objNodeList)
							{
								if(objXmlElement.GetAttribute("name") == arrColumns[i].ToString().Trim())
								{	
									if (objXmlElement.GetAttribute("type")=="code")
									{
                                        objXmlElement.InnerText = UTILITY.GetUser(Conversion.ConvertStrToInteger(objDataRow[m_FieldList[arrColumns[i].ToString()].ToString()].ToString()), m_iDsnId.ToString(), SecurityDatabase.GetSecurityDsn(m_iClientId), m_iClientId);//sShortCode + " " + sShortDesc;
										objXmlElement.SetAttribute("codeid",objDataRow[m_FieldList[arrColumns[i].ToString()].ToString()].ToString());
									}
									else
										objXmlElement.InnerText = objDataRow[m_FieldList[arrColumns[i].ToString()].ToString()].ToString();
								}
							}
						}
					}
				}
				if(m_sChildConnString != "")
					LoadChildXml(m_sChildConnString); 
			}
			finally
			{
				if(objDataSet != null)
					objDataSet.Dispose();
			}
		}
		
		/// <summary>
		/// validates the input xml
		/// </summary>
		/// <returns>boolean for success/failure</returns>
		protected virtual bool ValidateXml()
		{
			XmlNodeList objNodeList = null;
			bool bFound = false;

			try
			{
				foreach(string sName in m_FieldList.Keys)
				{
					objNodeList = m_objXmlDoc.GetElementsByTagName("control");
				
					foreach(XmlElement objXmlElement in objNodeList)
					{
						if(objXmlElement.GetAttribute("name") == sName)
						{
							bFound = true;
							break;
						}
					}

					if(bFound == false)
						return false;

					bFound = false;
				}
				return true;
			}
			finally
			{
				if(objNodeList != null)
					objNodeList = null;
			}
		}

		/// <summary>
		/// Loads XML for combobox from the child array
		/// </summary>
		/// <param name="p_sConString">Connection string</param>
		private void LoadChildXml(string p_sConString)
		{ 
			XmlNodeList objNodeList = null;
			XmlElement objNewElm = null;
			DataSet objDs = null;
			string sSQL = "";
			string sColumnName = "";

			try
			{
				for(int i =0; i< m_arrChild.GetLength(0);i++)
				{
					objNodeList = m_objXmlDoc.GetElementsByTagName("control");

					foreach(XmlElement objElm in objNodeList)
					{
						if(objElm.GetAttribute("name") == m_arrChild[i,0])
						{
							string sValue = objElm.InnerText;
							objElm.InnerText =""; 
							sSQL = String.Format("SELECT {0} , {1} FROM {2} WHERE {3}",m_arrChild[i,1],m_arrChild[i,5],m_arrChild[i,3],m_arrChild[i,4]);
                            objDs = DbFactory.GetDataSet(p_sConString, sSQL, m_iClientId);

							objNewElm = XMLDoc.CreateElement("option");
							objNewElm.SetAttribute("value", "0");
							objNewElm.InnerText = "";
							objElm.AppendChild(objNewElm);

							foreach(DataRow objRow in objDs.Tables[0].Rows)
							{
								if( m_arrChild[i,2].ToString().IndexOf('.') > 0)  
									sColumnName = (m_arrChild[i,2].Split('.'))[1].ToString();
								else
									sColumnName = m_arrChild[i,2].ToString();
								string sDesc = DbRow.GetStringField(objRow,sColumnName).ToLower();
								if( m_arrChild[i,1].ToString().IndexOf('.') > 0)  
									sColumnName = (m_arrChild[i,1].Split('.'))[1].ToString();
								else
									sColumnName = m_arrChild[i,1].ToString();
								long lValue = DbRow.GetLngField(objRow,sColumnName);
								if (lValue == 0)
									sDesc ="None";							
								objNewElm = XMLDoc.CreateElement("option");
								objNewElm.SetAttribute("value", lValue.ToString());
								objNewElm.InnerText = sDesc;
								if ( sValue == lValue.ToString())
								{
									objNewElm.SetAttribute("selected", "1");
									objNewElm.SetAttribute("codeid",lValue.ToString());
								}
								objElm.AppendChild(objNewElm);
							}
						}
					}
				}
			}
			finally
			{
				objNodeList = null;
				objNewElm = null;
				if(objDs != null)
					objDs.Dispose();
			}
		}
	}
}
