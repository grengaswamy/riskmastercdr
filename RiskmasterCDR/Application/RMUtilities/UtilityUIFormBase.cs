using System;
using System.Xml;
using Riskmaster.Db; 
using System.Data;
using System.Collections;
using Riskmaster.DataModel;
using Riskmaster.Common; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   20th,May 2005
	///Purpose :   Base Class
	/// </summary>
	public class UtilityUIFormBase
	{
		/// <summary>
		/// Key field variable
		/// </summary>
		private string m_sKeyField="";

		/// <summary>
		/// Key field value
		/// </summary>
		private long m_lKeyFieldValue=0;

		/// <summary>
		/// Table Name
		/// </summary>
		private string m_sTableName="";

		/// <summary>
		/// Input xml document
		/// </summary>
		private XmlDocument m_objXmlDoc=null;

		/// <summary>
		/// Connection String variable
		/// </summary>
		private string m_sConnString;

        //Aman ML Change
        /// <summary>
        /// user's Language Code
        /// </summary>
        private int m_iLangCode = 0;
		/// <summary>
		/// Key Tag
		/// </summary>
		private string m_sKeyTag;

		/// <summary>
		/// Child SQL String
		/// </summary>
		private string m_sChildSql;

		/// <summary>
		/// Move First
		/// </summary>
		internal const int MOVE_FIRST = 1;

		/// <summary>
		/// Move Last
		/// </summary>
		internal const int MOVE_LAST= 2;

		/// <summary>
		/// Move Next
		/// </summary>
		internal const int MOVE_NEXT= 3;

		/// <summary>
		/// Move Previous
		/// </summary>
		internal const int MOVE_PREVIOUS = 4;		

		/// <summary>
		/// Data Field List
		/// </summary>
		internal DataFieldList m_FieldList=new DataFieldList(); 

		/// <summary>
		/// Child Array
		/// </summary>
		protected string[,] m_arrChild =  new string[10,5];
		
		/// <summary>
		/// Property Map collection
		/// </summary>
		internal Hashtable m_PropertyMap = new Hashtable();

        private int m_iClientId = 0;//Add by kuladeep for Review defect.

		#region "Properties"

		/// <summary>
		/// Table Name
		/// </summary>
		protected string TableName
		{
			get{return m_sTableName;}
			set{m_sTableName = value;}
		}

		/// <summary>
		/// Key Field
		/// </summary>
		protected string KeyField
		{
			get{return m_sKeyField;}
			set{m_sKeyField = value;}
		}

		/// <summary>
		/// Child SQL
		/// </summary>
		protected string ChildSql
		{
			get{return m_sChildSql;}
			set{m_sChildSql = value;}
		}

		/// <summary>
		/// Input xml document
		/// </summary>
		protected XmlDocument XMLDoc
		{
			get{return m_objXmlDoc;}
			set{m_objXmlDoc = value;}
		}

		/// <summary>
		/// Connection String
		/// </summary>
		internal string ConnectString
		{
			get{return m_sConnString;}
			set{m_sConnString = value;}
		}

		/// <summary>
		/// Child Array
		/// </summary>
		protected string[,] ChildArray
		{
			set{m_arrChild = value;}
		}

        
        //Aman ML Change
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        
		#endregion

		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        internal UtilityUIFormBase(string p_sConnString, int p_iClientId)
		{
			Initialize();
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Constructor
		/// </summary>
        internal UtilityUIFormBase(int p_iClientId)
		{
			Initialize();
            m_iClientId = p_iClientId;
		}

		/// <summary>
		/// Utilities Base Form
		/// </summary>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDatabaseName">Database Name</param>
        internal UtilityUIFormBase(string p_sUserName, string p_sPassword, string p_sDatabaseName, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			Initialize();
		}
		
		/// <summary>
		/// Initialize
		/// </summary>
		protected void Initialize(){}

		/// <summary>
		/// Initialize the field array. Add all obect Fields into the Field Collection from our Field List.
		/// </summary>
		/// <param name="p_sFields"></param>
		protected virtual void InitFields(string[,] p_sFields)
		{ 
			for(int i =0; i< p_sFields.GetLength(0);i++)
			{
				if(m_sKeyField==p_sFields[i,1])
					m_sKeyTag = p_sFields[i,0];
				this.m_FieldList[p_sFields[i,0]] = p_sFields[i,1];
			}			
		}
	
		/// <summary>
		/// MoveFirst
		/// </summary>
		protected virtual void MoveFirst()
		{
			MoveAndLoad(MOVE_FIRST);
		}

		/// <summary>
		/// MoveLast
		/// </summary>
		protected virtual void MoveLast()
		{
			MoveAndLoad(MOVE_LAST);
		}

		/// <summary>
		/// MoveNext
		/// </summary>
		protected virtual void MoveNext()
		{
			m_lKeyFieldValue = GetKeyFieldValue();
			MoveAndLoad(MOVE_NEXT);
		}

		/// <summary>
		/// MovePrevious
		/// </summary>
		protected virtual void MovePrevious()
		{
			m_lKeyFieldValue = GetKeyFieldValue();
			MoveAndLoad(MOVE_PREVIOUS);
		}

		/// <summary>
		/// MoveTo
		/// </summary>
		/// <param name="keyValue">Key Value</param>
		protected virtual void MoveTo( params int[] keyValue){}

		/// <summary>
		/// Loads the data.
		/// </summary>
		/// <param name="moveDirection">Move Direction</param>
		protected void MoveAndLoad(int moveDirection)
		{
			DbReader rdr= null;
			string sSQL="";
			int lNewID=0;
			DbConnection objConn = null;

			try
			{				
				switch (moveDirection)
				{
					case MOVE_FIRST:
						sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>0",this.m_sKeyField,this.m_sTableName, this.m_sKeyField);
						break;
					case MOVE_LAST:
						sSQL = String.Format("SELECT MAX({0}) FROM {1}",this.m_sKeyField,this.m_sTableName);
						break;
					case MOVE_NEXT:
						sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
						break;
					case MOVE_PREVIOUS:
						if(m_lKeyFieldValue==0)
							sSQL = String.Format("SELECT MIN({0}) FROM {1} WHERE {2}>{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
						else
							sSQL = String.Format("SELECT MAX({0}) FROM {1} WHERE {2}<{3}",this.m_sKeyField,this.m_sTableName, this.m_sKeyField,m_lKeyFieldValue);
						break;
				}

				objConn = DbFactory.GetDbConnection(m_sConnString);
				objConn.Open(); 
				rdr = objConn.ExecuteReader(sSQL);

				///Fetch the RowId
				if (rdr.Read())
					lNewID = rdr.GetInt(0);
				rdr.Close();
				rdr = null;
				objConn.Dispose(); 
				
				//Only if New Unique ID is not 0 and New Unique ID different than old one
				if (lNewID != 0 && lNewID != m_lKeyFieldValue)
				{
					m_lKeyFieldValue = lNewID;
				}
				Get();
			}
			catch(Exception e)
			{
				throw e;
			}
			finally
			{
				if(rdr !=null)
					rdr.Close();
			}
		}

		/// <summary>
		/// Save the data
		/// </summary>
		/// <param name="p_arrlstSql">SQL string array</param>
		protected virtual void Save(ArrayList p_arrlstSql)
		{
			DbConnection objConn = null;
			DbTransaction objTran=null;
			DbCommand objCommand=null;

			try
			{

				if(p_arrlstSql.Count > 0)
				{
					objConn = DbFactory.GetDbConnection(m_sConnString);
					objConn.Open();
					objCommand=objConn.CreateCommand();
					objTran=objConn.BeginTransaction();
					objCommand.Transaction=objTran;
					objCommand.CommandType=CommandType.Text;
					for(int i = 0; i < p_arrlstSql.Count; i++)
					{
						objCommand.CommandText=p_arrlstSql[i].ToString();
						objCommand.ExecuteNonQuery();
					}
					objTran.Commit();
				}
			}
			finally
			{
				if(objConn!=null)
				{
					objConn.Dispose(); 
					objConn = null;
				}
				objCommand=null;
                if (objTran != null)
                    objTran.Dispose();
			}
		}

		/// <summary>
		/// Get the data
		/// </summary>
		protected virtual void Get()
		{
			DbConnection objConn = null;
			DbReader objDbReader = null;
			XmlNodeList objNodeList = null;
			string sSQL = "";
			try
			{

				if(m_lKeyFieldValue == 0)
					m_lKeyFieldValue = GetKeyFieldValue();
			
				if(m_sTableName != "" && m_sTableName != null) //In case no fields present apart from listbox or combo
				{
					if(m_lKeyFieldValue==0)
						sSQL = String.Format("SELECT * FROM {0}", m_sTableName);
					else
						sSQL = String.Format("SELECT * FROM {0} WHERE {1} = {2}",m_sTableName,m_sKeyField,m_lKeyFieldValue);

					objConn = DbFactory.GetDbConnection(m_sConnString);
					objConn.Open(); 
					objDbReader = objConn.ExecuteReader(sSQL); 

					if (objDbReader != null)
					{
						if(objDbReader.Read())
						{
							foreach(string sName in m_FieldList.Keys)
							{
								objNodeList = m_objXmlDoc.GetElementsByTagName("control");
								foreach(XmlElement objXmlElement in objNodeList)
								{
									if(objXmlElement.GetAttribute("name") == sName)
									{
										objXmlElement.InnerText = objDbReader[m_FieldList[sName].ToString()].ToString();
										if(objXmlElement.GetAttribute("type").ToLower() == "checkbox")
										{
											if (objXmlElement.InnerText.Equals("-1") || objXmlElement.InnerText.Equals("1"))
												objXmlElement.InnerText = "True";
											else
												objXmlElement.InnerText = "";
										}
										break;
									}
								}
							}
						}
						objDbReader.Close();
					}

					objConn.Dispose(); 
				}

				LoadChildXml(); 
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Dispose();
					objConn=null;
				}
				if (objDbReader != null)
				{
					objDbReader.Dispose();
					objDbReader = null;
				}
				objNodeList = null;
			}
		}
		
		/// <summary>
		/// Delete Method
		/// </summary>
		/// <returns>Boolean success</returns>
		protected virtual bool Delete()
		{
			DbConnection objConn = null;

			try
			{
				m_lKeyFieldValue = GetKeyFieldValue();
		
				objConn = DbFactory.GetDbConnection(m_sConnString);
				objConn.Open(); 
				objConn.ExecuteNonQuery(String.Format("DELETE FROM {0} WHERE {1} = {2}",m_sTableName,m_sKeyField,m_lKeyFieldValue)); 
				objConn.Dispose();

				return true;
			}
			finally
			{
				if( objConn != null)
				{
					objConn.Dispose();
					objConn=null;
				}
			}
		}

		/// <summary>
		/// Validate the Input xml
		/// </summary>
		/// <returns></returns>
		protected virtual bool ValidateXml()
		{
			XmlNodeList objNodeList = null;
			bool bFound = false;

			foreach(string sName in m_FieldList.Keys)
			{
				objNodeList = m_objXmlDoc.GetElementsByTagName("control");
				
				foreach(XmlElement objXmlElement in objNodeList)
				{
					if(objXmlElement.GetAttribute("name") == sName)
					{
						bFound = true;
						break;
					}
				}

				if(bFound == false)
					return false;

				bFound = false;
			}
			return true;
		}

		/// <summary>
		/// Get the Key field Value
		/// </summary>
		/// <returns>key field value</returns>
		private long GetKeyFieldValue()
		{
			XmlNodeList objXmlNodeList=null;
			long lKeyFieldValue = 0;

			objXmlNodeList = m_objXmlDoc.GetElementsByTagName("control");
			foreach(XmlElement objXmlElement in objXmlNodeList)
			{
				if(objXmlElement.GetAttribute("name") == m_sKeyTag)
				{
					lKeyFieldValue = Conversion.ConvertStrToLong(objXmlElement.InnerText); 
					break;
				}
			}
			return lKeyFieldValue; 
		}

		/// <summary>
		/// LoadChildXml
		/// </summary>
		private void LoadChildXml()
		{ 
			XmlNodeList objNodeList = null;
			XmlElement objNewElm = null;
			DataSet objDs = null;
			XmlNodeList objNodLst = null;
			XmlElement objLstRow = null;
			XmlElement objRowTxt = null;
			XmlElement objChildElm =null;
			int iCount = 0;
			string sSQL = "";
			LocalCache objLocalCache = null;
			string sShortCode = "";
			string sCodeDesc = "";

			try
			{
				for(int i =0; i< m_arrChild.GetLength(0);i++)
				{
					objNodeList = m_objXmlDoc.GetElementsByTagName("control");
					foreach(XmlElement objElm in objNodeList)
					{
						if(objElm.GetAttribute("name") == m_arrChild[i,0])
						{
							if(objElm.GetAttribute("type") == "combobox")
							{
								string sValue = objElm.InnerText;
								objElm.InnerText =""; 
								sSQL = m_arrChild[i,1].ToString();  
								objDs = DbFactory.GetDataSet(m_sConnString, sSQL,m_iClientId);//Add by kuladeep for review defect.
								foreach(DataRow objRow in objDs.Tables[0].Rows)
								{
									string sDesc = objRow[1].ToString(); 
									long lValue = Conversion.ConvertStrToLong(objRow[0].ToString());
									if (lValue == 0)
										sDesc ="None";							
									objNewElm = XMLDoc.CreateElement("option");
									objNewElm.SetAttribute("value", lValue.ToString());
									objNewElm.InnerText = sDesc;
									if ( sValue == lValue.ToString())
										objNewElm.SetAttribute("selected", "1");
									objElm.AppendChild(objNewElm);
								}

								if(objElm.GetAttribute("value") != null)
									objElm.SetAttribute("value",sValue);  
							}
							else if(objElm.GetAttribute("type") == "radiolist")
							{							
								sSQL = m_arrChild[i,1].ToString();
                                objDs = DbFactory.GetDataSet(m_sConnString, sSQL, m_iClientId); //Add by kuladeep for review defect.

								objNodLst  = objElm.GetElementsByTagName("rowhead");  
								foreach(DataRow objRow in objDs.Tables[0].Rows )
								{
									objLstRow = m_objXmlDoc.CreateElement("listrow");

									for(int j = 0 ; j< objNodLst.Count;j++)  
									{
											objChildElm = (XmlElement)objNodLst[j]; 
											objRowTxt = m_objXmlDoc.CreateElement("rowtext");
											objRowTxt.SetAttribute("type" , "label");
											objRowTxt.SetAttribute("name" , objChildElm.GetAttribute("colname"));
											objRowTxt.SetAttribute("title", objRow[m_FieldList[objChildElm.GetAttribute("colname")].ToString()].ToString());
											objRowTxt.SetAttribute("value", objRow[m_FieldList[objChildElm.GetAttribute("colname")].ToString()].ToString());
											objLstRow.AppendChild(objRowTxt);
									}
									objElm.AppendChild((XmlNode)objLstRow);
								}
							}
							else if(objElm.GetAttribute("type") == "codelist")
							{							
								iCount = 0;

                                objLocalCache = new LocalCache(m_sConnString, m_iClientId);

								sSQL = m_arrChild[i,1].ToString();
                                objDs = DbFactory.GetDataSet(m_sConnString, sSQL, m_iClientId);
					
								foreach(DataRow objRow in objDs.Tables[0].Rows )
								{
                                    //Start averma62 - MITS - 27437
                                    if (objRow[iCount].ToString() == "0")
                                        continue;
                                    //End averma62 - MITS - 27437
									objRowTxt = m_objXmlDoc.CreateElement("Item");
									objRowTxt.InnerText = objRow[iCount].ToString();
									objRowTxt.SetAttribute("value", objRowTxt.InnerText);
									objLocalCache.GetCodeInfo(Conversion.ConvertStrToInteger(objRowTxt.InnerText),ref sShortCode,ref sCodeDesc,this.LanguageCode);  //Aman ML Change
									objRowTxt.InnerText = sShortCode + " " + sCodeDesc;
									objElm.AppendChild(objRowTxt);
								}
							}
						}
					}
				}
			}
			finally
			{
				objNodeList = null;
				objNewElm = null;
				if (objDs != null)
				{
					objDs.Dispose();
					objDs=null;
				}
                if (objLocalCache != null)
                    objLocalCache.Dispose();
			}
		}
	}
}
