using System;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Xml;
using System.Text;
using System.Web;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
/* vsharma205 starts- Jira 7767*/
using Riskmaster.Settings;
using Riskmaster.DataModel;
using System.Xml.Linq;
using System.Linq;
/* vsharma205 ends- Jira 7767*/

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   20th,Apr 2005
	///Purpose :   Implements the Admin Customization for Riskmaster
	///				A sample setting xml is attached to this project RMAdminSettings.xml
	/// </summary>
	public class RMAdminSettings
	{
		/// <summary>
		/// Connection string variable
		/// </summary>
		string m_sConnStr="";
        

        //Changed by Gagan for MITS 10376 : Start
        string m_connectionString = "";
        //Changed by Gagan for MITS 10376 : End
        //dvatsa
        private int m_iClientId = 0;
        NameValueCollection m_RMUtilSettings = null;

		/// Name		: RMAdminSettings
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************		
		/// <param name="p_sConnString">Connection string parameter</param>
        //public RMAdminSettings(string p_sConnString, int p_iClientId)
        //{
        //    m_sConnStr=p_sConnString;
        //    m_iClientId = p_iClientId;
        //}

        //Changed by Gagan for MITS 10376 : Start

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="p_sConnString">Connection string parameter</param>
        /// <param name="p_sConnString">RiskMaster Database Connection string parameter</param>
        public RMAdminSettings(string p_sConnString, string p_sConnectionString, int p_iClientId)
        {
            m_sConnStr = p_sConnString;
            m_connectionString = p_sConnectionString;
            m_iClientId = p_iClientId;
            m_RMUtilSettings = RMConfigurationManager.GetNameValueSectionSettings("RMUtilities", m_connectionString, m_iClientId);
        }

        //Changed by Gagan for MITS 10376 : End

		/// Name		: GetAdminConfig
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Admin Configuration data.
		/// </summary>
		/// <param name="p_sFileName">File name to be retrieved</param>
		/// <returns>Xml document for Admin Configuration</returns>
		public XmlDocument GetAdminConfig(string p_sFileName)
		{
			DbReader objRdr=null;
			XmlDocument objXml=null;
			try
			{
				objXml = new XmlDocument(); 
				objRdr=DbFactory.GetDbReader(m_sConnStr,
					"SELECT CONTENT FROM CUSTOMIZE WHERE FILENAME='" + p_sFileName + "'");
				if(objRdr.Read())
				{
                    //srajindersin - Pentesting - Cross site Scripting - MITS 27656
                    //srajindersin -  MITS 28501
                    //objXml.LoadXml(CommonFunctions.HTMLCustomEncode(objRdr.GetString("CONTENT")));
                    objXml.LoadXml(objRdr.GetString("CONTENT"));
                    //END srajindersin - Pentesting - Cross site Scripting - MITS 27656
					
					//objRdr.Dispose(); 
				}
                return objXml;
			}
			catch(Exception p_objEx)
			{ //dvatsa
				throw new RMAppException(Globalization.GetString("RMAdminSettings.GetAdminConfig.Err",m_iClientId),p_objEx); 
			}
			finally
			{
				objXml=null;
				if(objRdr!=null)
				{
					objRdr.Dispose();
				}
			}
		}

		/// Name		: SaveAdminConfig
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the Admin Configuration to the database
		/// </summary>
		/// <param name="p_objXmlDoc">Input xml document with data</param>
		public void SaveAdminConfig(XmlDocument p_objXmlDoc)
		{
			string sFileName="";
            string sXml = null;
			DbWriter objWriter=null;
            XmlDocument objXmlDocument = null;
			try
			{
				sFileName = p_objXmlDoc.DocumentElement.Name;
				objWriter = DbFactory.GetDbWriter(m_sConnStr);
                objXmlDocument = new XmlDocument();
                switch (sFileName)
                {
                    case "customize_captions":
                    case "customize_settings":
                    case "customize_reports":
                    case "customize_search":
                    case "customize_custom":
                        if (p_objXmlDoc.SelectSingleNode("//"+sFileName) != null)
                        {
                            sXml = "<" + sFileName + ">" + p_objXmlDoc.SelectSingleNode("//" + sFileName).InnerXml.ToString() + "</" + sFileName + ">";
                            objXmlDocument.LoadXml(sXml);
                        }
                        break;
                    
                    case "customize_images":
                        if (p_objXmlDoc.SelectSingleNode("//RMAdminSettings") != null)
                            objXmlDocument.InnerXml = "<RMAdminSettings>" + p_objXmlDoc.SelectSingleNode("//RMAdminSettings").InnerXml + "</RMAdminSettings>";
                        break;
                    default:
                        objXmlDocument.InnerXml = p_objXmlDoc.InnerXml;
                        break;
                }
                
				objWriter.Tables.Add("CUSTOMIZE");
                objWriter.Fields.Add("CONTENT", objXmlDocument.InnerXml);
				objWriter.Where.Add("FILENAME='" + sFileName + "'");
				objWriter.Execute();
                //Changed by Gagan for MITS 10376 : Start
                UpdateLocalCache(p_objXmlDoc);
                //Changed by Gagan for MITS 10376 : End

                //Added Rakhi for Mits 17929:MDI Menu not getting updated with the changes saved within Customizations.
                if (HttpRuntime.Cache.Get(sFileName)!=null)
                {
                    HttpRuntime.Cache.Remove(sFileName);
                }
                //Added Rakhi for Mits 17929:MDI Menu not getting updated with the changes saved within Customizations.
			}
			catch(Exception p_objEx)
			{
                //dvatsa
                throw new RMAppException(Globalization.GetString("RMAdminSettings.SaveAdminConfig.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				objWriter=null;
                objXmlDocument = null;
			}
		}

        //Changed by Gagan for MITS 10376 : Start

        /// <summary>
        /// Update Local Cache with new width and height
        /// for controls
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        public void UpdateLocalCache(XmlDocument p_objXmlDoc)
        {
            int iTextMLCols = 0;
            int iTextMLRows = 0;
            int iFreeCodeCols = 0;
            int iFreeCodeRows = 0;
            int iReadOnlyMemoCols = 0;
            int iReadOnlyMemoRows = 0;
            int iMemoCols = 0;
            int iMemoRows = 0;
            LocalCache objLocalCache = null;          
                       
            try
            {             
                SetRowCol(p_objXmlDoc, ref iTextMLCols, ref iTextMLRows, "//TextML/");
                SetRowCol(p_objXmlDoc, ref iFreeCodeCols, ref iFreeCodeRows, "//FreeCode/");
                SetRowCol(p_objXmlDoc, ref iReadOnlyMemoCols, ref iReadOnlyMemoRows, "//ReadOnlyMemo/");
                SetRowCol(p_objXmlDoc, ref iMemoCols, ref iMemoRows, "//Memo/");

                objLocalCache = new LocalCache(m_connectionString, m_iClientId);
                objLocalCache.UpdateWidthHeight(iTextMLCols, iTextMLRows, iFreeCodeCols, iFreeCodeRows, iReadOnlyMemoCols, iReadOnlyMemoRows, iMemoCols, iMemoRows);
                
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("RMAdminSettings.SaveAdminConfig.Err", m_iClientId), p_objEx); 
            }
            finally
            {
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                    objLocalCache = null;
                }
            }
        }

        /// <summary>
        /// Sets control variables for columns and Rows
        /// </summary>
        /// <param name="p_objXmlDoc">xml containing data</param>
        /// <param name="iCol">variable that will contain column</param>
        /// <param name="iRow"></param>
        /// <param name="strToken"></param>
        public void SetRowCol(XmlDocument p_objXmlDoc,ref int iCol, ref int iRow, string strToken)
        {
            try
            {
                
                // || removed and && condition added by Shivendu for MITS 11929
                if (p_objXmlDoc.SelectSingleNode(strToken + "Width") != null && p_objXmlDoc.SelectSingleNode(strToken + "Width").InnerText != "" )
                {
                    iCol = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Width").InnerText);
                }
                // if here added by SHivendu for MITS 11929
                else if (p_objXmlDoc.SelectSingleNode(strToken + "Width") != null && p_objXmlDoc.SelectSingleNode(strToken + "Width").InnerText == "")
                {
                    //if added by SHivendu for MITS 11929
                    if(p_objXmlDoc.SelectSingleNode(strToken + "Width").Attributes["default"]!=null)
                    iCol = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Width").Attributes["default"].Value);
                }
                // || removed and && condition added by Shivendu for MITS 11929
                if (p_objXmlDoc.SelectSingleNode(strToken + "Height") != null && p_objXmlDoc.SelectSingleNode(strToken + "Height").InnerText != "" )
                {
                    iRow = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Height").InnerText);
                }
                // if here added by SHivendu for MITS 11929
                else if (p_objXmlDoc.SelectSingleNode(strToken + "Height") != null && p_objXmlDoc.SelectSingleNode(strToken + "Height").InnerText == "")
                {
                    //if added by SHivendu for MITS 11929
                    if (p_objXmlDoc.SelectSingleNode(strToken + "Height").Attributes["default"] != null)
                    iRow = int.Parse(p_objXmlDoc.SelectSingleNode(strToken + "Height").Attributes["default"].Value);
                }
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("RMAdminSettings.SaveAdminConfig.Err", m_iClientId), p_objEx); 
            }
            finally
            {
            }
        }

        //Changed by Gagan for MITS 10376 : End

		/// Name		: GetImage
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the Admin Image
		/// </summary>
		/// <param name="p_sFile">File name of the image to be retrieved</param>
		/// <returns>Image binary string</returns>
		public string GetImage(string p_sFile)
		{
			Riskmaster.Application.ZipUtil.ZipUtility objZip = null; 
			MemoryStream objMem = null;
			DbReader objRdr = null;

			try
			{
                objZip = new ZipUtil.ZipUtility(m_iClientId);
				objRdr = DbFactory.GetDbReader(m_sConnStr,"SELECT CONTENT FROM CUSTOMIZE WHERE FILENAME='" + p_sFile + "'");
				if(objRdr.Read())
				{
					string sContent= objRdr.GetString("CONTENT");
					//objMem = objZip.UnZIPBufferToMemoryStream(ref sContent);
					//return Convert.ToBase64String(objMem.ToArray()); 	
					return sContent;
				}			
				else
					return string.Empty;
			}
			catch(Exception p_objEx)
			{
                //dvatsa
                throw new RMAppException(Globalization.GetString("RMAdminSettings.GetImage.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				if (objZip!=null)
				{
					objZip.Dispose();
				}
				if (objRdr!=null)
				{
                    objRdr.Dispose();
				}
                if (objMem != null)
				    objMem.Dispose();
			}
		}

		/// Name		: SetImage
		/// Author		: Pankaj Chowdhury
		/// Date Created: 04/20/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the Admin image to the database
		/// </summary>
		/// <param name="p_sFile">Image file name</param>
		public void SetImage(string p_sFile, string p_sFileData)
		{
			string sFolder="";
			DbWriter objWriter=null;
			DbReader objReader=null;
			string sFileName=string.Empty;

			try
			{
				sFileName=p_sFile.Substring(p_sFile.LastIndexOf("\\")+1, p_sFile.Length - p_sFile.LastIndexOf("\\")-1);

                sFolder = m_RMUtilSettings["RMUtilImagesPath"];

				objWriter = DbFactory.GetDbWriter(m_sConnStr);
				objWriter.Tables.Add("CUSTOMIZE");
				objWriter.Fields.Add("ID",GetNextUID(m_sConnStr));
				objWriter.Fields.Add("FOLDER",sFolder);
				objWriter.Fields.Add("TYPE",0);
				objWriter.Fields.Add("IS_BINARY",1);
				objWriter.Fields.Add("CONTENT",p_sFileData);

				objReader=DbFactory.GetDbReader(m_sConnStr,"SELECT * FROM CUSTOMIZE WHERE FILENAME='" + p_sFile + "'");
				if (objReader.Read())
					objWriter.Where.Add("FILENAME='" + sFileName + "'");
				else
					objWriter.Fields.Add("FILENAME",sFileName);
                if(objReader!=null)
				    objReader.Close();

				objWriter.Execute();
			}
			catch(Exception p_objEx)
			{
                //dvatsa
                throw new RMAppException(Globalization.GetString("RMAdminSettings.SetImage.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				objWriter=null;
                if (objReader != null)
                    objReader.Dispose();
			}
		}

		public XmlDocument GetCustomFiles()
		{
			DbReader objRdr=null;
			
			XmlDocument objXml=null;
			XmlElement objElem=null;
			XmlElement objRootElem=null;

			try
			{
				objXml = new XmlDocument(); 

				objRootElem=objXml.CreateElement("Files");
				objXml.AppendChild(objRootElem);

				objRdr=DbFactory.GetDbReader(m_sConnStr,"SELECT FILENAME FROM CUSTOMIZE WHERE TYPE=0 AND IS_BINARY=0");
				while(objRdr.Read())
				{
					objElem=objXml.CreateElement("File");
					objElem.InnerText=objRdr.GetString("FILENAME");
					objRootElem.AppendChild(objElem);
				}
               
				return objXml;
			}
			catch(Exception p_objEx)
			{
                //dvatsa
                throw new RMAppException(Globalization.GetString("RMAdminSettings.GetCustomFiles.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				objXml=null;
				objRootElem=null;
				objElem=null;
				if(objRdr!=null)
				{
                    objRdr.Dispose();
				}
			}
		}

		public void SetCustomFile(string p_sFile, string p_sFileData)
		{
			DbWriter objWriter=null;
			DbReader objReader=null;
			string sFileName=string.Empty;
			
			try
			{
				if (p_sFile.IndexOf("\\") > -1)
					sFileName=p_sFile.Substring(p_sFile.LastIndexOf("\\")+1, p_sFile.Length - p_sFile.LastIndexOf("\\")-1);
				else
					sFileName=p_sFile;

				//remove extension
				sFileName=sFileName.Substring(0,sFileName.IndexOf("."));

				//get string back from base 64 encoded data
				ASCIIEncoding objEncoding = new ASCIIEncoding( );
				string sFileData = objEncoding.GetString(Convert.FromBase64String(p_sFileData));

				objWriter = DbFactory.GetDbWriter(m_sConnStr);
				objWriter.Tables.Add("CUSTOMIZE");
				objWriter.Fields.Add("FOLDER",Convert.DBNull);
				objWriter.Fields.Add("TYPE",0);
				objWriter.Fields.Add("IS_BINARY",0);
				objWriter.Fields.Add("CONTENT",sFileData);

				objReader=DbFactory.GetDbReader(m_sConnStr,"SELECT * FROM CUSTOMIZE WHERE FILENAME='" + sFileName + "'");
				if (objReader.Read())
				{
					objWriter.Fields.Add("ID",objReader.GetInt32("ID").ToString());
					objWriter.Where.Add("FILENAME='" + sFileName + "'");
				}
				else
				{
					objWriter.Fields.Add("ID",GetNextUID(m_sConnStr));
					objWriter.Fields.Add("FILENAME",sFileName);
				}
                if(objReader!=null)
				objReader.Close();

				objWriter.Execute();
			}
			catch(Exception p_objEx)
			{
                //dvatsa
                throw new RMAppException(Globalization.GetString("RMAdminSettings.SetCustomFile.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				objWriter=null;
                if (objReader != null)
                    objReader.Dispose();
			}
		}

		private int GetNextUID(string p_sConString)
		{
			DbConnection objCn = null;
			DbReader objDbReader = null;
			int iTableID=0;

			try
			{
				objCn = Db.DbFactory.GetDbConnection(p_sConString);
				objCn.Open(); 
					
				//Get Current Id.
				objDbReader = objCn.ExecuteReader("SELECT MAX(ID) ID FROM CUSTOMIZE");
				if(objDbReader.Read())
				{	
					iTableID = Conversion.ConvertStrToInteger(objDbReader["ID"].ToString());
				}
				if(objDbReader != null)
                    objDbReader.Close();

				return ++iTableID;
			}
			finally
			{
				if(objCn != null)
				{
					objCn.Dispose();
				}
                if (objDbReader != null)
                    objDbReader.Dispose();
			}
		}
	}
}
