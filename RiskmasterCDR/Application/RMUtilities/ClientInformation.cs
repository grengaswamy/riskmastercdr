﻿
using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.DataModel; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using System.Text;
using Riskmaster.Security;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   21st,Mar 2005
	///Purpose :   Client Information Details form
	/// </summary>
	public class ClientInformation:UtilitiesUIBase 
	{
        
		/// <summary>
		/// String array for database fields mapping
		/// </summary>
		private string[,] arrFields = {
			//First tab - Client Information
			{"RowId", "ROW_ID"},
			{"ClientName", "CLIENT_NAME"},
			{"Essp", "ESSP"},
			{"Address1", "ADDR1"},
			{"Address2", "ADDR2"},
            //{"Address3", "ADDR3"}, // JIRA 6420 syadav55
            //{"Address4", "ADDR4"}, // JIRA 6420 syadav55
			{"City", "CITY"},
			{"State", "STATE"},
			{"Zip", "ZIP_CODE"},
			{"Product", "DTG_PRODUCT"},
			{"Printer", "PRINTER"},
            //sgoel6 Medicare 05/13/2009
            {"taxid","TAX_ID"},
			{"Hardware", "HARDWARE"},
			{"InstallationDate", "DATE_INSTALLED"},
			{"Phone", "PHONE_NUMBER"},
			{"Fax", "FAX_NUMBER"},
			//{"Modem", "MODEM_PHONE"}, Deprecated. Not required as per QA comments. Pankaj -08/29/05
			{"ClientStatus", "CLIENT_STATUS_CODE"},
			{"Version", "DTG_VERSION"},
			{"Support", "DTG_SUPPORT_PHONE"},
			{"DistributionMedia","DISTRIBUTION_MEDIA"},
			
			//Second Tab - Contact Information
			{"PrimaryContact","PRIMARY_CONTACT"},
			{"PrimaryTitle","PRIMARY_TITLE"},
			{"SecondaryContact","SECONDARY_CONTACT"},
			{"SecondaryTitle","SECONDARY_TITLE"},
			{"DataEntryContact","DATA_ENTRY_CONTACT"},
			{"DataEntryTitle","DATA_ENTRY_TITLE"},
			{"ReportingContact","REPORTING_CONTACT"},
			{"ReportingTitle","REPORTING_TITLE"},
			{"DeptMgrContact","DEPT_MGR_CONTACT"},
			{"DeptMgrTitle","DEPT_MGR_TITLE"},
			{"AccountingContact","ACCOUNTING_CONTACT"},
			{"AccountingTitle","ACCOUNTING_TITLE"},

            //sgoel6 Medicare 05/13/2009| Third Tab MMSEA Medicare
            {"DoingBusinessAs","DOING_BUS_AS"},
            {"LegalName","LEGAL_NAME"}            
								  };

        
        
		/// Name		: ClientInformation
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor with Connection String
		/// </summary>
		/// <param name="p_sConnString">Connection String</param>
        /// 
        int m_iClientId = 0;//rkaur27
        //vkumar258 - RMA-6037
        private UserLogin m_UserLogin = null;
        private string m_sUserName = "";
        private string m_sConnectString = "";
        //vkumar258 - RMA-6037 -End

        public ClientInformation(string p_sConnString, int p_iClientID = 0)//rkaur27
            : base(p_sConnString, p_iClientID)
		{
            ConnectString = p_sConnString;
            m_iClientId = p_iClientID;//rkaur27
			this.Initialize(); 
		}
        public ClientInformation(UserLogin oUserLogin, string p_sConnString, int p_iClientID = 0)
            : base(oUserLogin, p_sConnString, p_iClientID)//vkumar258
        {
            m_UserLogin = oUserLogin;
            ConnectString = p_sConnString;
            m_iClientId = p_iClientID;
            this.Initialize(); 

        }
		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize Private variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "SYS_PARMS";
			base.KeyField = "ROW_ID";
			base.InitFields(arrFields);
			base.Initialize();
		}
	
		/// Name		: New
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the xml structure for a new record
		/// </summary>
		/// <param name="p_objXMLDocument">XML Doc</param>
		/// <returns>Xmldocument with structure</returns>
		public XmlDocument New(XmlDocument p_objXMLDocument)
		{
			try
			{
				return p_objXMLDocument; 
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.New.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the data along with the XML Structure
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with xml structure</param>
		/// <returns>xml document with data and the xml layout structure</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument, string p_userlgin, string p_dbname, string p_password)
		{
            //sgoel6 Medicare 05/16/2009
            //ClientInfoMMSEA objClientInfoMMSEA=new ClientInfoMMSEA(base.ConnectString);
            XmlDocument XMLClientInfoMMSEADataDoc = null;
            XmlDocument XMLRef = new XmlDocument(); 
			try
			{
				XMLDoc = p_objXmlDocument;
                base.Get();
                XMLRef = XMLDoc;
                XMLClientInfoMMSEADataDoc=GetClientInfoMMSEAData(p_userlgin, p_dbname, p_password, ref XMLRef);
                return XMLRef; 
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.Get.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: Delete
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes a particular record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Next record after the deleted record</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Delete();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.Delete.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the xml data to the database
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data to be saved</param>
		/// <returns>Saved data with xml structure</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Save();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.Save.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: MoveFirst
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Move to the first record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Xml document with the data</returns>
		public XmlDocument MoveFirst(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.MoveFirst();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.MoveFirst.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: MoveLast
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Move to the Last Record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Xml document with the data</returns>
		public XmlDocument MoveLast(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.MoveLast();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.MoveLast.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: MovePrevious
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Move to the Previous Record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Xml document with the data</returns>
		public XmlDocument MovePrevious(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.MovePrevious();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.MovePrevious.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: MoveNext
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Move to the Next Record
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document</param>
		/// <returns>Xml document with the data</returns>
		public XmlDocument MoveNext(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.MoveNext();
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("ClientInformation.MoveNext.Err", m_iClientId),p_objEx);
			}
		}
        public XmlDocument GetClientInfoMMSEAData(string p_userlgin,string p_dbname,string p_password, ref XmlDocument XMLDoc)
        {
            CMMSEAXData objEntityMmseaRowId = null;
            XmlElement objXMLElem = null;
            XmlDocument objOutXML = null;
            //RMConfigurator objConfig = null;
            XmlElement objRoot = null;
            //int iEntityEid = 0;
            //Entity objEntity = null;
            DataModelFactory m_objDMF = null;
            DbReader drEntXMMSEA = null;
            string sSQL = string.Empty;
            //InitializeDataModel();
            m_objDMF = new DataModelFactory(p_dbname,p_userlgin,p_password, m_iClientId);
            //sgoel6 Medicare Filing Schedule changes
            string sScheduleCode = string.Empty;
            string sCodeType = "";
            string sCodeDesc = "";
                
            //objEntityMmseaRowId = (CMMSEAXData)m_objDMF.GetDataModelObject("CMMSEAXData", false);
            //objEntity = (Entity)m_objDMF.GetDataModelObject("Entity", false);
            try
            {
                //objEntityMmseaRowId.MoveTo(p_iEntityMmseaRowId);
                //iEntityEid = objEntityMmseaRowId.EntityEID;
                //objEntity.MoveTo(-2);
              
                objRoot = XMLDoc.CreateElement("CMMSEAXData");
                XMLDoc.DocumentElement.AppendChild(objRoot);
                

                XmlElement objListHead = null;
                XmlElement objListOption = null;

                objListHead = XMLDoc.CreateElement("listhead");
                StringBuilder sNode = new StringBuilder("<LobCode>Line of Business</LobCode>");
                sNode.Append("<SecondLobCode>Second Line Of Business</SecondLobCode>");
                sNode.Append("<ReporterId>Reporter ID</ReporterId>");
                sNode.Append("<SiteId>Office Code/Site ID</SiteId>");
                sNode.Append("<ScheduleCode>Medicare Filing Schedule</ScheduleCode>");

                objListHead.InnerXml = sNode.ToString();
                objRoot.AppendChild(objListHead);

                sSQL = "SELECT * FROM ENTITY_X_MMSEA WHERE ENTITY_ID=-2";
                
                drEntXMMSEA = m_objDMF.Context.DbConn.ExecuteReader(sSQL);

                
                while (drEntXMMSEA.Read())
                {
                    //sgoel6 Medicare Filing Schedule
                    m_objDMF.Context.LocalCache.GetCodeInfo(Conversion.ConvertObjToInt(drEntXMMSEA["REPORT_SCHED_CODE"], m_iClientId), ref sCodeType, ref sCodeDesc);
                    sScheduleCode = sCodeType + " " + sCodeDesc;

                    objListOption = XMLDoc.CreateElement("option");
                    //sNode = "<LobCode>";
                    //sNode = sNode + objCMMSEAXData.LineOfBusCode.ToString() + "</LobCode>";
                    //sNode = sNode + "<ReporterId>" + dr.MMSEAReporterID.ToString() + "</ReporterId>";
                    //sNode = sNode + "<SiteId>" + objCMMSEAXData.MMSEAOfficeSiteID.ToString() + "</SiteId>";
                    //sNode = sNode + "<EntMMSEARowID>" + objCMMSEAXData.EntMMSEARowID.ToString() + "</EntMMSEARowID>";
                    //sNode = sNode + "<EntityEID>" + objCMMSEAXData.EntityEID.ToString() + "</EntityEID>";
                    //sNode = sNode + "<AddedByUser>" + objCMMSEAXData.AddedByUser.ToString() + "</AddedByUser>";
                    //sNode = sNode + "<DttmRcdLastUpd>" + objCMMSEAXData.DttmRcdLastUpd.ToString() + "</DttmRcdLastUpd>";
                    //sNode = sNode + "<DttmRcdAdded>" + objCMMSEAXData.DttmRcdAdded.ToString() + "</DttmRcdAdded>";
                    //sNode = sNode + "<UpdatedByUser>" + objCMMSEAXData.UpdatedByUser.ToString() + "</UpdatedByUser>";
                    sNode.Length = 0;
                    sNode.Append("<LobCode>");
                    sNode.Append(GetCodeInfo(Conversion.ConvertObjToInt(drEntXMMSEA["LINE_OF_BUS_CODE"], m_iClientId)));
                    sNode.Append("</LobCode>");
                    sNode.AppendFormat("<SecondLobCode>{0}</SecondLobCode>", GetCodeInfo(Conversion.ConvertObjToInt(drEntXMMSEA["SECOND_LOB_CODE"], m_iClientId))); //Mits 20818
                    sNode.AppendFormat("<ReporterId>{0}</ReporterId>",Conversion.ConvertObjToStr(drEntXMMSEA["MMSEA_REPRTER_TEXT"]));
                    sNode.AppendFormat("<SiteId>{0}</SiteId>",Conversion.ConvertObjToStr(drEntXMMSEA["MMSEA_OFE_STE_TEXT"]));
                    sNode.AppendFormat("<ScheduleCode>{0}</ScheduleCode>",sScheduleCode);
                    sNode.AppendFormat("<EntMMSEARowID>{0}</EntMMSEARowID>",Conversion.ConvertObjToStr(drEntXMMSEA["ENT_MMSEA_ROW_ID"]));
                    sNode.AppendFormat("<EntityEID>{0}</EntityEID>",Conversion.ConvertObjToStr(drEntXMMSEA["ENTITY_ID"]));
                    sNode.AppendFormat("<AddedByUser>{0}</AddedByUser>",Conversion.ConvertObjToStr(drEntXMMSEA["ADDED_BY_USER"]));
                    sNode.AppendFormat("<DttmRcdLastUpd>{0}</DttmRcdLastUpd>", Conversion.ConvertObjToStr(drEntXMMSEA["DTTM_RCD_LAST_UPD"]));
                    sNode.AppendFormat("<DttmRcdAdded>{0}</DttmRcdAdded>", Conversion.ConvertObjToStr(drEntXMMSEA["DTTM_RCD_ADDED"]));

                    //vkumar258 - RMA-6037 starts
                   // sNode.AppendFormat("<DttmRcdLastUpd>{0}</DttmRcdLastUpd>",(Conversion.ConvertObjToStr(drEntXMMSEA["DTTM_RCD_LAST_UPD"])));
                   // sNode.AppendFormat("<DttmRcdAdded>{0}</DttmRcdAdded>",(Conversion.ConvertObjToStr(drEntXMMSEA["DTTM_RCD_ADDED"])));

                    sNode.AppendFormat("<DttmRcdLastUpd>{0}</DttmRcdLastUpd>",Common.Conversion.GetUIDate(drEntXMMSEA["DTTM_RCD_LAST_UPD"].ToString(),m_UserLogin.objUser.NlsCode.ToString(),m_iClientId));
                    sNode.AppendFormat("<DttmRcdAdded>{0}</DttmRcdAdded>", Common.Conversion.GetUIDate(drEntXMMSEA["DTTM_RCD_ADDED"].ToString(),m_UserLogin.objUser.NlsCode.ToString(),m_iClientId));
                    //vkumar258 RMA-6037 End

                    sNode.AppendFormat("<UpdatedByUser>{0}</UpdatedByUser>", Conversion.ConvertObjToStr(drEntXMMSEA["UPDATED_BY_USER"]));

                    objListOption.InnerXml = sNode.ToString();
                    objRoot.AppendChild(objListOption);
                   
                }
                if (drEntXMMSEA != null)
                {
                    drEntXMMSEA.Dispose();
                    drEntXMMSEA.Close();
                }

                //Inserting blank row for grid
                objListOption = XMLDoc.CreateElement("option");
                XmlAttribute objXmlAttribute = XMLDoc.CreateAttribute("type");
                objXmlAttribute.InnerText = "new";
                objListOption.Attributes.Append(objXmlAttribute);

                sNode.Length = 0;
                sNode.Append("<LobCode></LobCode>");
                sNode.Append("<SecondLobCode></SecondLobCode>");//Mits 20818
                sNode.Append("<ReporterId></ReporterId>");
                sNode.Append("<SiteId></SiteId>");
                sNode.Append("<ScheduleCode></ScheduleCode>");
                sNode.Append("<EntMMSEARowID></EntMMSEARowID>");
                sNode.Append("<EntityEID></EntityEID>");
                sNode.Append("<AddedByUser></AddedByUser>");
                sNode.Append("<DttmRcdLastUpd></DttmRcdLastUpd>");
                sNode.Append("<DttmRcdAdded></DttmRcdAdded>");
                sNode.Append("<UpdatedByUser></UpdatedByUser>");
                objListOption.InnerXml = sNode.ToString();
                objRoot.AppendChild(objListOption);

                XMLDoc.DocumentElement.AppendChild(objRoot);
            }
            catch (RecordNotFoundException p_objException)
            {
                throw new InvalidValueException("CMMSEAXData.GetClientInfoMMSEAData.EntityMMSEARowIdNotFound", p_objException);
            }
            return XMLDoc;
        }
        private string GetCodeInfo(int sLobCode)
        {
            switch (sLobCode)
            {
                case 0: //Mits 20818
                    return "";
                case 241:
                    return "GC  General Claims";
                case 242:
                    return "VA  Vehicle Accident Claims";
                case 243:
                    return "WC  Workers' Compensation";
                default:
                    return "All";

            }
        }
	}
}
