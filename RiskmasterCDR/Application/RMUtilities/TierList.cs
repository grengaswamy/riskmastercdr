using System;
using System.Xml; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Anurag Agarwal
	///Dated   :   21st,Mar 2005
	///Purpose :   Tier List implementation form
	/// </summary>	
    public class TierList : UtilitiesUIListBase
    {
        private int m_iClientId = 0;

        /// <summary>
        /// String array for Base Class field mapping
        /// </summary>
        private string[,] arrFields = {
			{"LevelName","TIER_NAME"},
			{"State","STATE_ID"},
			{"EffectiveDate","EFFECTIVE_DATE"},
			{"ExpirationDate","EXPIRATION_DATE"},
            {"ParentLevel","PARENT_LEVEL"}
									  };

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="p_sConnString">Connection String</param>
        public TierList(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
        {
            ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
            this.Initialize();
        }

        /// <summary>
        /// Initializes the  fields
        /// </summary>
        new private void Initialize()
        {
            LocalCache objCache = null;

            try
            {
                base.TableName = "SYS_POL_DISC_TIER LEFT OUTER JOIN STATES ON SYS_POL_DISC_TIER.STATE = STATES.STATE_ROW_ID  ";
                base.KeyField = "DISC_TIER_ROWID";
                base.RadioName = "TierList";
                objCache = new LocalCache(ConnectString,m_iClientId);
                base.WhereClause = "TIER_NAME = 'Modified Premium'";
                base.InitFields(arrFields);
                base.Initialize();
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TierList.Initialize.Error", m_iClientId), p_objExp);
            }
            finally
            {
                if (objCache != null)
                    objCache.Dispose();
            }
        }

        /// <summary>
        /// Get the data for the TierList form
        /// </summary>
        /// <param name="p_objXmlDocument">Input xml document with data structure</param>
        /// <returns>Xml document with data and the form structure</returns>
        public XmlDocument Get(XmlDocument p_objXmlDocument)
        {
            string sLOB = "";
            string sState = "";
            string sWhere = "";
            string sParent = string.Empty;
            XmlElement xTempElement = null;
            XmlNode xNode = null;
            XmlNode xElementTierList = null;
            XmlNodeList xNodeList = null;
            try
            {
                XMLDoc = p_objXmlDocument;
                sLOB = p_objXmlDocument.SelectSingleNode("//LOBCode").InnerText;
                sState = p_objXmlDocument.SelectSingleNode("//StateCode").InnerText;
                //rsushilaggar MITS 25274 date 06/24/2011-
                sParent = p_objXmlDocument.SelectSingleNode("//ParentCode").InnerText;
                if (sParent == "RATESETUP")
                {
                    sWhere = "TIER_NAME in ('Modified Premium')" + " OR LINE_OF_BUSINESS =" + sLOB + " AND IN_USE_FLAG <> 0";
                }
                else
                {
                    sWhere = "TIER_NAME in ('Modified Premium','Manual Premium')" + " OR LINE_OF_BUSINESS =" + sLOB + " AND IN_USE_FLAG <> 0";
                }
                if (sState != "")
                    sWhere += " AND (STATE = " + sState + " OR STATE = 0)";
                else
                    sWhere += " AND STATE = 0";
                base.WhereClause = sWhere;
                base.Get();
                //rsushilaggar filter out the manual premium tier list from the tier list for Rate setup grid
                if (sParent == "RATESETUP")
                {
                    xNode = XMLDoc.SelectSingleNode("//listrow[LevelName = 'Modified Premium']");
                    
                    xTempElement = XMLDoc.CreateElement("listrow");
                    xTempElement.AppendChild(xNode);

                    GetTierList(XMLDoc, xNode, xTempElement);
                    xElementTierList = XMLDoc.SelectSingleNode("//TierList");
                    xNodeList = XMLDoc.SelectNodes("//listrow");
                    foreach (XmlNode node in xNodeList)
                    {
                        xElementTierList.RemoveChild(node);
                    }
                    xNodeList = xTempElement.SelectNodes("//listrow");
                    foreach (XmlNode node in xNodeList)
                    {
                        xElementTierList.AppendChild(node);
                    }
                    
                }
                return XMLDoc;
            }
            catch (RMAppException p_objRMExp)
            {
                throw p_objRMExp;
            }
            catch (Exception p_objExp)
            {
                throw new RMAppException(Globalization.GetString("TierList.Get.Error", m_iClientId), p_objExp);
            }
        }

        /// <summary>
        /// Author - Rahul Aggarwal
        /// Write this function to get the modified premium tier list
        /// </summary>
        /// <param name="p_xDocument"></param>
        /// <param name="p_xNode"></param>
        /// <param name="p_xElement"></param>
        private void GetTierList(XmlDocument p_xDocument, XmlNode p_xNode, XmlElement p_xElement)
        {

            XmlNodeList xNodeList = p_xDocument.SelectNodes("//listrow[ParentLevel = '" + p_xNode.SelectSingleNode(@"RowId").InnerText + "']");
            if (xNodeList != null && xNodeList.Count > 0)
            {
                foreach (XmlNode xNode in xNodeList)
                {
                    p_xElement.AppendChild(xNode);
                    GetTierList(p_xDocument, xNode, p_xElement);
                }
            }
      
        }
    }
		
	
}
