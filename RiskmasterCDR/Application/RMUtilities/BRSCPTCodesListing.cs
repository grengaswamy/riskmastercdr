using System;
using System.Xml;
using Riskmaster.Db; 
using Riskmaster.ExceptionTypes;
using Riskmaster.Common; 

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   04th,May 2005
	///Purpose :   CPT Codes Listing Form 
	/// </summary>
	public class BRSCPTCodesListing:UtilitiesUIListBase  
	{
		/// <summary>
		/// String array for database field mapping
		/// </summary>
		private string[,] arrFields = {
			{"RowId", "CPT"},
			{"CPT","CPT"},
			{"Desc","CPT_DESC"},
			{"Amount","AMOUNT"}
			};

        private int m_iClientId = 0;
		/// Name		: BRSCPTCodesListing
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="p_sConnString">Connection String Parameter</param>
        public BRSCPTCodesListing(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Initialize private variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "WRCMP_CPT";
			base.KeyField = "CPT";
			base.RadioName = "BRSCPTCodesListingList";			
			base.WhereClause = "1=1";
			base.OrderByClause = "";
			base.InitFields(arrFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Gets the CPT Codes List
		/// </summary>
		/// <param name="p_objXmlDocument">Input Xml document for structure</param>
		/// <returns>Xml document with data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{	
			XmlNode objNode = null;
			XmlNodeList objNodLst=null;
			try
			{
				XMLDoc = p_objXmlDocument;			
				objNode = p_objXmlDocument.SelectSingleNode("//tableid");
				string sTableId = objNode.InnerText; 
				base.WhereClause  = "TABLE_ID =" + sTableId;
				base.Get();
				objNodLst = p_objXmlDocument.SelectNodes("//listrow//RowId");
				foreach(XmlNode objNod in objNodLst)
				{
					objNod.InnerText = objNod.InnerText + "|" + sTableId;
				}
				p_objXmlDocument.SelectSingleNode("//RecordCount").InnerText=objNodLst.Count.ToString() ;
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSCPTCodesListing.Get.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				objNode = null;
                objNodLst = null;
			}
		}

		/// Name		: Delete
		/// Author		: Pankaj Chowdhury
		/// Date Created: 05/04/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************	
		/// <summary>
		/// Deletes a particular record for CPT Codes Listing
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with xml structure</param>
		/// <returns>Returns the next record if any</returns>
		public XmlDocument Delete(XmlDocument p_objXmlDocument)
		{	
			string sSQL="";
			string sCPT="";
			string sTableId="";
			DbConnection objCon=null;
			XmlNode objNode = null;        
			try
			{
				XMLDoc = p_objXmlDocument;			
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TableId']");
				if(objNode!=null)
					sTableId = objNode.InnerText ;
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='CPTCode']");
				if(objNode!=null)
					sCPT = objNode.InnerText ;
				sSQL = "DELETE FROM WRCMP_CPT WHERE TABLE_ID =" + sTableId + " AND CPT = '" + sCPT + "'";			
				objCon = DbFactory.GetDbConnection(ConnectString);
				objCon.Open(); 
				objCon.ExecuteNonQuery(sSQL);
				return p_objXmlDocument;
			}
			catch(Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("BRSCPTCodesListing.Delete.Err", m_iClientId), p_objEx); 
			}
			finally
			{
				if(objCon!=null)
				{
					objCon.Dispose();
					
				}
				objNode = null;
			}
		}
	}
}