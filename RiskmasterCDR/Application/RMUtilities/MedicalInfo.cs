using System;
using System.Xml; 
using Riskmaster.Db; 
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;  

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   21 Mar 2005
	///Purpose :   Medical Info Setup form
	/// </summary>
	public class MedicalInfo: UtilitiesMultiBase  
	{
		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields = {
			{"WcMdaDsnName", "PARM_NAME"},
			{"DsnName", "STR_PARM_VALUE"},
			{"WcMdaUserId", "PARM_NAME"},
			{"UserId", "STR_PARM_VALUE"},
			{"WcMdaPswd", "PARM_NAME"},
			{"Password", "STR_PARM_VALUE"},
			{"WcMdaHelpFile", "PARM_NAME"},
			{"HelpFile", "STR_PARM_VALUE"}
		  };

        private int m_iClientId = 0;
		/// <summary>
		/// string array for key fields mapping
		/// </summary>
		private string[,] arrKeyFields = {										 
			{"WC_MDA_DSN_NAME", "DsnName"},
			{"WC_MDA_USER_ID", "UserId" },
			{"WC_MDA_PSWD", "Password"},
			{"WC_MDA_HELP_FILE", "HelpFile"}
										 };

		/// Name		: MedicalInfo
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Default Constructor with Connection String
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
		public MedicalInfo(string p_sConnString, int p_iClientId): base(p_sConnString, p_iClientId)
		{
			ConnectString = p_sConnString;
            m_iClientId = p_iClientId;
			this.Initialize(); 
		}

		/// Name		: Initialize
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Initialize the private variables and properties
		/// </summary>
		new private void Initialize()
		{
			base.TableName = "PARMS_NAME_VALUE";
			base.KeyField = "PARM_NAME";
			base.InitFields(arrFields);
			base.InitKeyFields( arrKeyFields);
			base.Initialize();
		}

		/// Name		: Get
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Gets the data for the Medical Info Setup
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with form structure</param>
		/// <returns>Xml document with data and structure</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Get();
				return p_objXmlDocument;
			}		
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("MedicalInfo.Get.Err", m_iClientId),p_objEx);
			}
		}

		/// Name		: Save
		/// Author		: Pankaj Chowdhury
		/// Date Created: 03/21/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Saves the xml document with data and the form structure
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml document with data</param>
		/// <returns>Saved data</returns>
		public XmlDocument Save(XmlDocument p_objXmlDocument)
		{
			try
			{
				XMLDoc = p_objXmlDocument;
				base.Save(false); 
				return XMLDoc;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("MedicalInfo.Save.Err", m_iClientId),p_objEx);
			}
		}
	}
}
