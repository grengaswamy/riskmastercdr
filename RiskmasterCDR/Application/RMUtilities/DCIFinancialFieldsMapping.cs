using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.RMUtilities
{
    ///************************************************************** 
    ///* $File		: DCIFinancialFieldsMapping.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 30-August-2014
    ///* $Author	: Neha Goel/Ankit Gupta
    ///* $Comment	: MITS# 36997 GAP 06 - RMA-5497
    ///* $Source	: 
    ///**************************************************************
	public class DCIFinancialFieldsMapping:UtilityFormBase
	{

		#region Private Variables
		/// <summary>
		/// String Array for Base Class fields
		/// </summary>
		private string[,] arrFields=
		{
			{"DCIFieldId", "DCI_FIELD_ID"},
            {"DCIReserveType", "RES_CODE_ID"},
			{"DCITransactionType", "TRANS_CODE_ID"},		
			{"DCIFieldsRowId", "DCIFIN_ROW_ID"},
			
		};
		/// <summary>
		/// Field Mapping with SQL queries
		/// </summary>
        private string[,] arrChilds = {
			//{"XMLTag", "Query"}
			{"DCIFieldsList","SELECT DCIFIN_ROW_ID, DCI_FIELD_ID,RES_CODE_ID,TRANS_CODE_ID FROM DCI_FINFLDS_TRANS_MAP"},			
				                                      };
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName=string.Empty;
		/// <summary>
		/// Connection string
		/// </summary>
		private string m_sDSN=string.Empty;
        private int m_iClientId = 0;
	#endregion

		#region Constructor
		
        //public DCIFinancialFieldsMapping(){}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="p_sConnString">Connection string</param>
        public DCIFinancialFieldsMapping(string p_sConnString, int p_iClientId)
            : base(p_sConnString, p_iClientId)
		{
			try
			{
				ConnectString = p_sConnString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DCIFinancialFieldsMapping.Constructor.Error", m_iClientId), p_objEx);
			}
	
		}
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public DCIFinancialFieldsMapping(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString,p_iClientId) 
		{	
			try
			{
				m_sUserName = p_sLoginName;
				ConnectString = p_sConnectionString;
                m_iClientId = p_iClientId;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DCIFinancialFieldsMapping.Constructor.Error", m_iClientId), p_objEx);
			}
	
		}
		#endregion

		#region Private Functions 
		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			try
			{
				TableName = string.Empty;
                KeyField = string.Empty;
				InitFields(arrFields);
				base.ChildArray = arrChilds;	
			}
			catch(RMAppException p_objEx)
			{
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DCIFinancialFieldsMapping.Initialize.Error", m_iClientId), p_objEx);
			}
		}
		#endregion

		#region Public Functions
		/// Name		: Get
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument,UserLogin objUser)
		{
			XmlNodeList objNodLst=null;
			XmlElement objElement=null;
			XmlNode objNode=null;
			string sValue=string.Empty;
			LocalCache objCache=null;
			string sShortCode=string.Empty;
			string sShortDesc=string.Empty;
			string sTransType=string.Empty;
			string sTableId=string.Empty;
			string sValue1=string.Empty;
			string sValue2=string.Empty;
			string sMastReserType=string.Empty;
            string sReserveType = string.Empty;
            Int32 iReserveByClaimType = 0;
            Int32 iTransByReserveType = 0;
			DbReader objRead=null;
			XmlNodeList objNodTmpLst =null;
			StringBuilder sbSql=null;
			try
			{
				XMLDoc = p_objXmlDocument;
				Get();
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sTransType=objCache.GetTableId("TRANS_TYPES").ToString();
                sReserveType = objCache.GetTableId("RESERVE_TYPE").ToString();
				sMastReserType=objCache.GetTableId("MASTER_RESERVE").ToString();
				
                //Get Reserves
				//objCache.Dispose();
				sbSql=new StringBuilder();
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ReserveType']");
                if (objNode != null && !string.IsNullOrEmpty(objNode.InnerText))
                {
                    iTransByReserveType = Convert.ToInt32(objNode.InnerText);
                }
                //sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
                //sbSql.Append(" WHERE CODES.TABLE_ID =" + sReserveType);
                //sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID");
                //sbSql.Append(" AND CODES.RELATED_CODE_ID IN");
                //sbSql.Append(" (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES");
                //sbSql.Append(" WHERE RESERVE_TYPE_CODE = CODE_ID");
                //sbSql.Append(" AND RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID ="+sMastReserType+")");
                //sbSql.Append(" AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 243)");
                //sbSql.Append(" AND CODES.DELETED_FLAG = 0 AND CODES_TEXT.LANGUAGE_CODE = 1033"); //Aman ML Change
                //sbSql.Append(" ORDER BY CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                //ML Change
                //sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "RESERVE_TYPE", objUser.objUser.NlsCode);
                //ML Change
                sbSql.Append("select RES_BY_CLM_TYPE from SYS_PARMS_LOB where LINE_OF_BUS_CODE = 243 ");
                iReserveByClaimType = Conversion.ConvertObjToInt(DbFactory.ExecuteScalar(base.ConnectString, sbSql.ToString()), m_iClientId);
                sbSql = new StringBuilder();
                if (iReserveByClaimType == -1)
                {   sbSql.Append("SELECT DISTINCT(SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE),CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_CLM_TYPE_RES, CODES_TEXT, CODES ");
                    sbSql.Append("WHERE SYS_CLM_TYPE_RES.RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSql.Append("CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND CODES_TEXT.LANGUAGE_CODE = 1033 AND SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = 243"); 
                    sbSql.Append(" ORDER BY CODES_TEXT.CODE_DESC");


                }
                else if (iReserveByClaimType == 0)
                {
                    sbSql.Append("SELECT DISTINCT(SYS_LOB_RESERVES.RESERVE_TYPE_CODE),CODES.SHORT_CODE, CODES_TEXT.CODE_DESC FROM SYS_LOB_RESERVES, CODES_TEXT, CODES ");
                    sbSql.Append("WHERE SYS_LOB_RESERVES.RESERVE_TYPE_CODE = CODES.CODE_ID AND ");
                    sbSql.Append("CODES_TEXT.CODE_ID = CODES.CODE_ID  AND CODES.DELETED_FLAG <> -1 AND CODES_TEXT.LANGUAGE_CODE = 1033 AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 243");
                    sbSql.Append("ORDER BY CODES.SHORT_CODE, CODES_TEXT.CODE_DESC");
                }
                
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue(0)));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim()+" "
						+Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}

                //Get Transactions
                if (iTransByReserveType > 0)
                {
                    sbSql = new StringBuilder();
                    objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TransactionType']");
                    sbSql.Append(" SELECT CODES.CODE_ID,CODES.SHORT_CODE,CODES_TEXT.CODE_DESC FROM CODES,CODES_TEXT ");
                    sbSql.Append(" WHERE CODES.TABLE_ID =" + sTransType);
                    sbSql.Append(" AND CODES.CODE_ID = CODES_TEXT.CODE_ID");
                    sbSql.Append(" AND CODES.RELATED_CODE_ID IN");
                    if (iTransByReserveType > 0)
                    {
                        sbSql.Append("(" + iTransByReserveType + ")");
                    }
                    //else if (iReserveByClaimType == -1)
                    //{
                    //    sbSql.Append(" (SELECT RESERVE_TYPE_CODE FROM SYS_CLM_TYPE_RES,CODES");
                    //    sbSql.Append(" WHERE RESERVE_TYPE_CODE = CODE_ID");
                    //    sbSql.Append(" AND RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID =" + sMastReserType + ")");
                    //    sbSql.Append(" AND SYS_CLM_TYPE_RES.LINE_OF_BUS_CODE = 243)");
                    //}
                    //else if (iReserveByClaimType == 0)
                    //{
                    //    sbSql.Append(" (SELECT RESERVE_TYPE_CODE FROM SYS_LOB_RESERVES,CODES");
                    //    sbSql.Append(" WHERE RESERVE_TYPE_CODE = CODE_ID");
                    //    sbSql.Append(" AND RELATED_CODE_ID IN (SELECT CODE_ID FROM CODES WHERE TABLE_ID =" + sMastReserType + ")");
                    //    sbSql.Append(" AND SYS_LOB_RESERVES.LINE_OF_BUS_CODE = 243)");
                    //}
                    sbSql.Append(" AND CODES.DELETED_FLAG = 0  AND CODES_TEXT.LANGUAGE_CODE = 1033");  //Aman ML Change
                    sbSql.Append(" ORDER BY CODES_TEXT.CODE_DESC");
                    //ML Change
                    sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), "TRANS_TYPES", objUser.objUser.NlsCode);
                    //ML Change
                    objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString());
                    while (objRead.Read())
                    {
                        objElement = XMLDoc.CreateElement("option");
                        objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue(0)));
                        objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue(1)).Trim() + " "
                            + Conversion.ConvertObjToStr(objRead.GetValue(2)).Trim();
                        objNode.AppendChild(objElement);
                    }
                    if (!objRead.IsClosed)
                    {
                        objRead.Close();
                    }
                }

                //Get DCI Fields
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIFields']");				
				sbSql=new StringBuilder();
                sbSql.Append(" SELECT DCI_ROW_ID, DCI_FIELDS FROM DCI_FIN_FIELDS");
                sbSql.Append(" ORDER BY DCI_FIELDS");
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=XMLDoc.CreateElement("option");
                    objElement.SetAttribute("value", Conversion.ConvertObjToStr(objRead.GetValue("DCI_ROW_ID")));
                    objElement.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("DCI_FIELDS")).Trim();
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
                
                //Add to xml
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ReserveType']"); 
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","0");
				objElement.InnerText=string.Empty;
				if (objNode.FirstChild!=null)
				objNode.InsertBefore((XmlNode)objElement,objNode.FirstChild);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='TransactionType']"); 
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","0");
				objElement.InnerText=string.Empty;
				if (objNode.FirstChild!=null)
				objNode.InsertBefore((XmlNode)objElement,objNode.FirstChild);

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIFields']"); 
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","0");
				objElement.InnerText=string.Empty;
				if (objNode.FirstChild!=null)
				objNode.InsertBefore((XmlNode)objElement,objNode.FirstChild);
                                

                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIFieldsList']");
                objNodLst = objNode.SelectNodes("./listrow/rowtext[@name!='DCIFieldsRowId']");
                //objCache = new LocalCache(base.ConnectString);
				foreach(XmlNode objTemp in objNodLst)
				{
					if (objTemp.NodeType==XmlNodeType.Element)
					{
                        if (objTemp.Attributes["title"].Value != null)
                            sValue=objTemp.Attributes["title"].Value;
						objTemp.InnerText=sValue;
                        if (objTemp.Attributes["name"].Value != null && objTemp.Attributes["name"].Value.Equals("DCIFieldId", StringComparison.OrdinalIgnoreCase))
                        {
                            sbSql = new StringBuilder();
                            sbSql.Append("select DCI_FIELDS from DCI_FIN_FIELDS where DCI_ROW_ID =" + sValue);
                            objTemp.Attributes["title"].Value = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(base.ConnectString, sbSql.ToString()));
                        }
                        else if (!string.IsNullOrEmpty(sValue))
                        {
                            objCache.GetCodeInfo(Conversion.ConvertStrToInteger(sValue), ref sShortCode, ref sShortDesc);
                            objTemp.Attributes["title"].Value = sShortCode + " " + sShortDesc;
                        }
						
					}
				}
                //objCache.Dispose();


                objNodLst = p_objXmlDocument.SelectNodes("//control[@name='DCIFieldsList']/listrow");  
				foreach(XmlNode objTemp in objNodLst)
				{
					objNodTmpLst = objTemp.SelectNodes("./rowtext");
					foreach(XmlNode objTmp in objNodTmpLst)
					{
						if (objTmp.NodeType==XmlNodeType.Element)
						{
                            if (objTmp.Attributes["name"].Name != null && objTmp.Attributes["name"].Name != "DCIFieldsRowId")
							{
								sValue1=sValue1+objTmp.InnerText+"|";
							}
						}
					}
					sValue1=sValue1+"**";
				}
                objNode = p_objXmlDocument.SelectSingleNode("//form/group");
                if (objNode != null)
                {
                    objElement = p_objXmlDocument.CreateElement("DCIFieldsListValues");
                    objElement.InnerText = sValue1;
                    objNode.AppendChild(objElement); 
                }
                //objCache.Dispose(); 
			}
			catch(RMAppException p_objEx)
			{
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DCIFinancialFieldsMapping.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNodLst=null;
				objElement=null;
				objNode=null;
				if (objCache != null)
                {
                    objCache.Dispose();
                }
				if (objRead!=null)
				{
					objRead.Dispose();
				}
                objNodTmpLst = null;
                sbSql = null;
				
			}
				return XMLDoc;
		}
	
	
		/// Name		: Delete
		/// Deletes Benefits Mapping information
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Delete(XmlDocument p_objXmlDocument)
		{
			ArrayList arrlstSql=null;
			string sSQL=string.Empty;
			bool bReturnValue=false;
			string sRecord=string.Empty;
			XmlNode objNode=null;
			string[] arrValues=null;
			try
			{
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='DCIFieldsList']"); 
				if (objNode.InnerText!=null)
				{
					arrValues=objNode.InnerText.Split(" ".ToCharArray());
				}
				arrlstSql=new ArrayList();
				if (arrValues!=null)
				{
					for (int i=0;i<arrValues.Length;i++)
					{
                        if (!arrValues[i].Trim().Equals(string.Empty))
						{
                            sSQL = "DELETE FROM DCI_FINFLDS_TRANS_MAP WHERE DCIFIN_ROW_ID=" + arrValues[i];
							arrlstSql.Add(sSQL);
						}
					}
				}
                UTILITY.Save(arrlstSql, base.ConnectString, m_iClientId);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DCIFinancialFieldsMapping.Delete.Error", m_iClientId), p_objEx);
			}
			finally
			{
				arrlstSql=null;
				objNode=null;
				arrValues=null;
			}
			return bReturnValue;
		}
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//control[@name='"+p_sValue+"']");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
                    return string.Empty;
				}
			}
			else
			{
                return string.Empty;
			}
		}
		/// Name		: Save		
		/// <summary>
		/// Function Saves IAIABC Funds Transaction Mapping
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			ArrayList objQuery=null;
            string sValue1 = string.Empty;
            string sValue2 = string.Empty;
            string sValue3 = string.Empty;
            string sQuery = string.Empty;
            string sDate = string.Empty;
			DbReader objRead=null;
            string sInsertQuery = string.Empty;
            string sUpdateQuery = string.Empty;
			int iNextId=0;
			bool bReturnValue=false;
            bool IsUpdate = false;
			string sUser=m_sUserName;
			try
			{
				XMLDoc = p_objXmlDocument;
				objQuery=new ArrayList();
                sValue1 = GetValue(p_objXmlDocument, "DCIFields");
                sValue2 = GetValue(p_objXmlDocument, "ReserveTypeID");
                sValue3 = GetValue(p_objXmlDocument, "TransactionTypeID");
                if (sValue1 != string.Empty && sValue2 != string.Empty)
				{
                    sQuery = "SELECT DCIFIN_ROW_ID FROM DCI_FINFLDS_TRANS_MAP WHERE DCI_FIELD_ID=" + Convert.ToInt32(sValue1) + " AND RES_CODE_ID=" + Convert.ToInt32(sValue2) + " AND TRANS_CODE_ID=" + sValue3;
					objRead=DbFactory.GetDbReader(base.ConnectString,sQuery);
					if (objRead.Read())
					{
						objRead.Close();
                        throw new RMAppException(Globalization.GetString("DCIFinancialFieldsMapping.Save.ErrorTransactionTypeBenefits", m_iClientId)); 
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}

                    if (Convert.ToInt32(sValue3) == 0)
                    {
                        sQuery = "SELECT DCIFIN_ROW_ID FROM DCI_FINFLDS_TRANS_MAP WHERE DCI_FIELD_ID=" + Convert.ToInt32(sValue1) + " AND RES_CODE_ID=" + Convert.ToInt32(sValue2);
                        objRead = DbFactory.GetDbReader(base.ConnectString, sQuery);
                        if (objRead.Read())
                        {
                            objRead.Close();
                            throw new RMAppException("Transaction Type is required for this mapping.");
                        }
                        if (!objRead.IsClosed)
                        {
                            objRead.Close();
                        }
                    }
                    else if (Convert.ToInt32(sValue3) > 0)
                    {
                        sQuery = "SELECT DCIFIN_ROW_ID FROM DCI_FINFLDS_TRANS_MAP WHERE DCI_FIELD_ID=" + Convert.ToInt32(sValue1) + " AND RES_CODE_ID=" + Convert.ToInt32(sValue2) + " AND TRANS_CODE_ID = 0";
                        objRead = DbFactory.GetDbReader(base.ConnectString, sQuery);
                        if (objRead.Read())
                        {
                            objRead.Close();
                            IsUpdate = true;
                        }
                        if (!objRead.IsClosed)
                        {
                            objRead.Close();
                        }
                    }

                    sDate = "'" + Conversion.ToDbDateTime(DateTime.Now) + "'";

                    if (!IsUpdate)
                    {
                        iNextId = Utilities.GetNextUID(base.ConnectString, "DCI_FINFLDS_TRANS_MAP", m_iClientId);
                        sInsertQuery = "INSERT INTO DCI_FINFLDS_TRANS_MAP (DCIFIN_ROW_ID,DCI_FIELD_ID,RES_CODE_ID,TRANS_CODE_ID,DTTM_RCD_ADDED,ADDED_BY_USER,DTTM_RCD_LAST_UPD,UPDATED_BY_USER) VALUES (" + iNextId.ToString()
                            + "," + Convert.ToInt32(sValue1) + "," + Convert.ToInt32(sValue2) + "," + Convert.ToInt32(sValue3) + "," + sDate + ",'" + sUser + "'," + sDate + ",'" + sUser + "')";
                        objQuery.Add(sInsertQuery);
                    }
                    else 
                    {
                        sUpdateQuery = "UPDATE DCI_FINFLDS_TRANS_MAP SET TRANS_CODE_ID = " + Convert.ToInt32(sValue3) + ", DTTM_RCD_LAST_UPD = " + sDate + ", UPDATED_BY_USER = '" + sUser + "' WHERE DCI_FIELD_ID = " + Convert.ToInt32(sValue1) + " AND RES_CODE_ID = " + Convert.ToInt32(sValue2);
                        objQuery.Add(sUpdateQuery);
                    }
				}
				base.Save(objQuery);
				bReturnValue=true;
			}
			catch(RMAppException p_objEx)
			{
                Log.Write(p_objEx.Message + " Stack Trace: " + p_objEx.StackTrace, Log.LOG_CATEGORY_DEFAULT, m_iClientId);
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("DCIFinancialFieldsMapping.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objQuery=null;
				if (objRead!=null)
				{
					objRead.Dispose();
				}
				
			}
			return bReturnValue;
			
		}
	
	
		#endregion
    }
}
