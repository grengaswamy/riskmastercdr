using System;
using System.Collections;
using System.Xml;
using System.Text;
using Riskmaster.Common; 
using Riskmaster.Db;
using Riskmaster.Security;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Parag Sarin
	///Dated   :   02 May 2005
	///Purpose :   This class Creates, Edits, Fetches WorkCompAgentServiceArea information.
	/// </summary>
	public class WorkCompAgentServiceArea:UtilityFormBase
	{
		#region Input/Output Xml
		/*<form name="WorkCompAgentServiceArea" title="Agent Service Area" topbuttons="1" supp="">
		  <toolbar>
			<button type="save" title="Save" />
		  </toolbar>
		  <group name="WorkCompAgentServiceArea" title="Agent Service Area">
			<displaycolumn>
			  <control name="Jurisdictions" type="combobox" title="Jurisdiction:" onchange="Navigate('7');" value="7^CA^CA-California">
				<option value="">
				</option>
				<option value="75^AA^AA-UP">AA-UP</option>
				 </control>
			  <control name="BusinessName" type="text" title="Business Name:" value="PARAG SARIN">FRESNO I&amp;A UNIT          </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="OfficeFunction" type="combobox" title="Function:" onchange="Navigate('7');" value="2926">2926<option value=""></option><option value="2926" selected="1">I &amp; A  Information and Assistance Unit</option><option value="2927">VocReh  Rehabilitation Unit</option></control>
			  <control name="Address1" type="text" title="Address:" value="2550 MARIPOSA STREET, STE. 4078">2550 MARIPOSA STREET, STE. 4078   </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="Agents" type="combobox" title="Agency:" onchange="Navigate('7');" value="">1<option value=""></option><option value="21">ANAHEIM I&amp;A UNIT         </option><option value="18">BAKERSFIELD I&amp;A UNIT     </option><option value="1" selected="1">FRESNO I&amp;A UNIT          </option><option value="22">GROVER BEACH I&amp;A UNIT    </option><option value="2">LONG BEACH I&amp;A UNIT      </option><option value="3">LOS ANGELES I&amp;A UNIT     </option><option value="15">NORWALK I &amp; A UNIT       </option><option value="4">OAKLAND I&amp;A UNIT         </option><option value="5">POMONA I&amp;A UNIT          </option><option value="19">REDDING I&amp;A UNIT         </option><option value="23">RIVERSIDE I&amp;A UNIT       </option><option value="6">SACRAMENTO I&amp;A UNIT      </option><option value="16">SALINAS I&amp;A UNIT         </option><option value="7">SAN BERNARDINO I&amp;A UNIT  </option><option value="8">SAN DIEGO I&amp;A UNIT       </option><option value="9">SAN FRANCISCO I&amp;A UNIT   </option><option value="10">SAN JOSE I&amp;A UNIT        </option><option value="11">SANTA ANA I&amp;A UNIT       </option><option value="20">SANTA BARBARA I&amp;A UNIT   </option><option value="14">SANTA MONICA I&amp;A UNIT    </option><option value="17">STOCKTON I&amp;A UNIT        </option><option value="12">VAN NUYS I&amp;A UNIT        </option><option value="13">VENTURA I&amp;A UNIT         </option><option value="24">WALNUT CREEK I&amp;A UNIT    </option></control>
			  <control name="Address2" type="text" title="" value="">
			  </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="Contact" type="text" title="Contact:" value="STEPHEN WEBSTER">STEPHEN WEBSTER </control>
			  <control name="City" type="text" title="City:" value="FRESNO         ">FRESNO         </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="ContactPhone" type="text" title="Phone:" value="(209) 445-5355">(209) 445-5355</control>
			  <control name="State" type="text" title="Postal Code:">CA-California</control>
			</displaycolumn>
			<displaycolumn>
			  <control name="PostCode" type="text" title="Postal Code:" value="93721">93721     </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="FindZip" type="text" title="Zip:" value="911111">
			  </control>
			  <control name="Find" type="button" title="Find">
			  </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="AgentsByZip" type="combobox" title="" value="">
			  </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="AddAgent" type="button" title="Add New">
			  </control>
			  <control name="DeleteAgent" type="button" title="Delete">
			  </control>
			  <control name="SaveAgent" type="button" title="Save">
			  </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="ServiceAreaTableName" type="combobox" title="Service Area Postal Code" size="3" value="10080">
				<option value="93301">93301</option>
      
			  </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="JurisdictionalTableName" type="combobox" title="JurisdictionalPostal Code" size="3" value="">
				<option value="44446">44446</option>
				</control>
			</displaycolumn>
			<displaycolumn>
			  <control name="JurisCodeAdd" type="text" title="New Jurisdictional Postal Codes" value="10080">10080</control>
      
			  <control name="AddJurisCode" type="button" title="Add">
			  </control>
			</displaycolumn>
			<displaycolumn>
			  <control name="JurisCodDelete" type="text" title="Delete Jurisdictional Postal Codes" value="10080">
			  </control>
			  <control name="DeleteJurisCode" type="button" title="Delete">
			  </control>
			</displaycolumn>
		  </group>
		</form>
		*/
		#endregion

		#region Private variables
		/// <summary>
		/// User name
		/// </summary>
		private string m_sUserName="";
        //Aman ML Change
        /// <summary>
        /// user's Language Code
        /// </summary>
        private int m_iLangCode = 0;
        private int m_iClientId = 0;
		#endregion

		#region Constructor
		/// <summary>
		/// Overloaded constructor
		/// </summary>
		/// <param name="p_sLoginName">Login Name</param>
		/// <param name="p_sConnectionString">Connection String</param>
        public WorkCompAgentServiceArea(string p_sLoginName, string p_sConnectionString, int p_iClientId)
            : base(p_sConnectionString, p_iClientId) 
		{	
			try
			{
                m_iClientId = p_iClientId;
				m_sUserName =	p_sLoginName;
				ConnectString = p_sConnectionString;
				this.Initialize(); 
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.Constructor.Error", m_iClientId), p_objEx);
			}
	
		}
		#endregion
        #region Properties
        //Aman ML Change
        public int LanguageCode
        {
            get
            {
                return m_iLangCode;
            }
            set
            {
                m_iLangCode = value;
            }
        }
        #endregion
		#region Private Functions
		/// <summary>
		/// Initialize the properties
		/// </summary>
		new private void Initialize()
		{
			try
			{
				base.Initialize();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.Initialize.Error", m_iClientId), p_objEx);
			}
		}
		private string GetValue(XmlDocument p_objXmlDocument,string p_sValue)
		{
			XmlNode objNode=null;
			objNode=p_objXmlDocument.SelectSingleNode("//control[@name='"+p_sValue+"']");
			if (objNode!=null)
			{
				if (objNode.InnerText!=null)
				{
					return objNode.InnerText;
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}
		#endregion

		#region Public Functions
		/// Name		: AddJurisPostalCodes
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************		
        /// Ref ID   MITS    Date Amended   *   Amendment   *                              Author
        ///************************************************************
        /// [100]   31748    6-6-2013         avoid inserting record with empty string     kartheek
		///************************************************************
		/// <summary>
		/// Adds Jurisdictional PostalCodes
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		public void AddJurisPostalCodes(XmlDocument p_objXmlDocument)
		{
            DbReader objRead = null;
			XmlNode objNode=null;
			StringBuilder sbSql=null;
			string sDate="";
			string sTemp="";
			string sTransId="";
			string sRowId="";
			string sCodeAdd="";
            int iFound = 0;
			ArrayList alSQL=null;
			LocalCache objCache=null;
			try
			{
				sbSql=new StringBuilder();
				alSQL=new ArrayList();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='JurisCodeAdd']");
				sCodeAdd=GetValue(p_objXmlDocument,"JurisCodeAdd");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				sTemp=GetValue(p_objXmlDocument,"Jurisdictions");
				if (!sTemp.Equals(""))
				{
                    objCache = new LocalCache(base.ConnectString, m_iClientId);
					sTransId=Conversion.ConvertObjToStr(objCache.GetTableId(sTemp.Split('^')[1]+"_AGENCY_FUNCTION"));
					sRowId=sTemp.Split('^')[0];
					objCache.Dispose();
				}
			
				sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";

                sbSql = new StringBuilder();
                sbSql.Append("SELECT POSTAL_CODE FROM POSTAL_CODES WHERE JURIS_ROW_ID ='" + sRowId + "' AND POSTAL_CODE ='" + sCodeAdd + "'");
                objRead = DbFactory.GetDbReader(base.ConnectString, sbSql.ToString());
                //[100] start
                if (objRead.Read() && !string.IsNullOrEmpty(sCodeAdd))//END [100]
                {
                    iFound = 1;
                    sbSql = new StringBuilder();
                    sbSql.Append("UPDATE POSTAL_CODES SET DTTM_RCD_LAST_UPD=" + sDate);
                    sbSql.Append(",UPDATED_BY_USER='" + m_sUserName + "'");
                    sbSql.Append(",DELETED_FLAG=0");
                    sbSql.Append(" WHERE JURIS_ROW_ID=" + sRowId);
                    sbSql.Append(" AND POSTAL_CODE ='" + sCodeAdd + "'");
                }
                //[100] start
                if (iFound == 0 && !string.IsNullOrEmpty(sCodeAdd))//END [100]
                {

                    sbSql = new StringBuilder();
                    sbSql.Append("INSERT INTO POSTAL_CODES(JURIS_ROW_ID, POSTAL_CODE,DTTM_RCD_LAST_UPD, UPDATED_BY_USER, DTTM_RCD_ADDED,ADDED_BY_USER, DELETED_FLAG) VALUES (");
                    sbSql.Append("'" + sRowId + "','" + sCodeAdd + "'," + sDate + ",'" + m_sUserName + "'," + sDate + ",'" + m_sUserName + "',0)");
                }
				alSQL.Add(sbSql.ToString());
                UTILITY.Save(alSQL, base.ConnectString, m_iClientId);


                if (!objRead.IsClosed)
                {
                    objRead.Close();
                }
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.AddJurisPostalCodes.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
				sbSql=null;
                if (objCache != null)
                    objCache.Dispose();
			}
		}
		/// Name		: DeleteJurisPostalCodes
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes Jurisdictional PostalCodes
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		public void DeleteJurisPostalCodes(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			StringBuilder sbSql=null;
			string sDate="";
			string sTemp="";
			string sTransId="";
			string sCodeDelete="";
			string sRowId="";
			ArrayList alSQL=null;
			LocalCache objCache=null;
			try
			{
				sbSql=new StringBuilder();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='JurisCodDelete']");
				sCodeDelete=GetValue(p_objXmlDocument,"JurisCodDelete");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
				sTemp=GetValue(p_objXmlDocument,"Jurisdictions");
				if (!sTemp.Equals(""))
				{
                    objCache = new LocalCache(base.ConnectString, m_iClientId);
					sTransId=Conversion.ConvertObjToStr(objCache.GetTableId(sTemp.Split('^')[1]+"_AGENCY_FUNCTION"));
					sRowId=sTemp.Split('^')[0];
					objCache.Dispose();
				}
			
				sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
				sbSql.Append("UPDATE POSTAL_CODES SET DTTM_RCD_LAST_UPD="+sDate);
				sbSql.Append(",UPDATED_BY_USER='"+m_sUserName+"'");
				sbSql.Append(",DELETED_FLAG=-1");
				sbSql.Append(" WHERE JURIS_ROW_ID="+sRowId);
				sbSql.Append(" AND POSTAL_CODE ='"+sCodeDelete+"'");
				
				alSQL=new ArrayList();
				alSQL.Add(sbSql.ToString());
                UTILITY.Save(alSQL, base.ConnectString, m_iClientId);
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.DeleteJurisPostalCodes.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
				sbSql=null;
				alSQL=null;
				if (objCache!=null)
					objCache.Dispose();				
			}
		}
		/// Name		: SaveZips
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Save All Zip Codes for Agency and state
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		public void SaveZips(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			StringBuilder sbSql=null;
			string sOfficeId="";
			string sAgentsId="";
			string sDate="";
			string sTemp="";
			string sTransId="";
			string sRowId="";
			string sCodeAdd="";
			ArrayList alSQL=null;
			LocalCache objCache=null;
			string [] arrIds=null;
			try
			{
				sbSql=new StringBuilder();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
				sTemp=GetValue(p_objXmlDocument,"Jurisdictions");
				if (!sTemp.Equals(""))
				{
                    objCache = new LocalCache(base.ConnectString, m_iClientId);
					sTransId=Conversion.ConvertObjToStr(objCache.GetTableId(sTemp.Split('^')[1]+"_AGENCY_FUNCTION"));
					sRowId=sTemp.Split('^')[0];
					objCache.Dispose();
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='OfficeFunction']");
				sOfficeId=GetValue(p_objXmlDocument,"OfficeFunction");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Agents']");
				sAgentsId=GetValue(p_objXmlDocument,"Agents");
				sbSql.Append("DELETE WCP_AGNCY_SERV_ZIP ");
				sbSql.Append(" WHERE STATE_ROW_ID="+sRowId);
				sbSql.Append(" AND FUNCTION_CODE_ID ="+sOfficeId);
				sbSql.Append(" AND AGENCY_ID ="+sAgentsId);
				alSQL=new ArrayList();
				alSQL.Add(sbSql.ToString());
				
				objNode=p_objXmlDocument.SelectSingleNode("//control[@name='ServiceAreaTableName']");
				
				if (objNode.InnerText !="")
				{
					arrIds=GetValue(p_objXmlDocument,"ServiceAreaTableName").Split(new char[]{' '});
					for (int i=0;i<arrIds.Length;i++)
					{
						sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
						sbSql=new StringBuilder();
						sCodeAdd=arrIds[i];
						sbSql.Append("INSERT INTO WCP_AGNCY_SERV_ZIP(STATE_ROW_ID, ZIP_CODE,AGENCY_ID,FUNCTION_CODE_ID,DTTM_RCD_LAST_UPD, UPDATED_BY_USER,DTTM_RCD_ADDED, ADDED_BY_USER, DELETED_FLAG) VALUES (");
						sbSql.Append("'"+sRowId+"','"+sCodeAdd+"','"+sAgentsId+"','"+sOfficeId+"',"+sDate+",'"+m_sUserName+"',"+sDate+",'"+m_sUserName+"',0)");
						alSQL.Add(sbSql.ToString());					
					}
				}
                UTILITY.Save(alSQL, base.ConnectString, m_iClientId);
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.SaveZips.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
				sbSql=null;
                if (objCache != null)
                    objCache.Dispose();
			}
		}
		/// Name		: DeleteAgency
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Deletes Agency
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		public void DeleteAgency(XmlDocument p_objXmlDocument)
		{
			XmlNode objNode=null;
			StringBuilder sbSql=null;
			string sOfficeId="";
			string sAgentsId="";
			string sDate="";
			string sTemp="";
			string sTransId="";
			string sRowId="";
			ArrayList alSQL=null;
			LocalCache objCache=null;
			try
			{
				sbSql=new StringBuilder();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
				sTemp=GetValue(p_objXmlDocument,"Jurisdictions");
				if (!sTemp.Equals(""))
				{
                    objCache = new LocalCache(base.ConnectString, m_iClientId);
					sTransId=Conversion.ConvertObjToStr(objCache.GetTableId(sTemp.Split('^')[1]+"_AGENCY_FUNCTION"));
					sRowId=sTemp.Split('^')[0];
					objCache.Dispose();
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='OfficeFunction']");
				sOfficeId=GetValue(p_objXmlDocument,"OfficeFunction");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Agents']");
				sAgentsId=GetValue(p_objXmlDocument,"Agents");
				sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
				sbSql.Append("UPDATE WCP_STATE_AGENCY SET DTTM_RCD_LAST_UPD="+sDate);
				sbSql.Append(",UPDATED_BY_USER='"+m_sUserName+"'");
				sbSql.Append(",DELETED_FLAG=-1");
				sbSql.Append(" WHERE STATE_ROW_ID="+sRowId);
				sbSql.Append(" AND FUNCTION_CODE_ID ="+sOfficeId);
				sbSql.Append(" AND AGENCY_ID ="+sAgentsId);
				alSQL=new ArrayList();
				alSQL.Add(sbSql.ToString());
                UTILITY.Save(alSQL, base.ConnectString, m_iClientId);
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.DeleteAgency.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objNode=null;
				alSQL=null;
				sbSql=null;
				if (objCache!=null)
					objCache.Dispose();
				objCache=null;
			}
		}
		/// Name		: FindZip
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		
		/// <summary>
		/// Find Zip code
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing zip code data</returns>
		public XmlDocument FindZip(XmlDocument p_objXmlDocument)
		{
			DbReader objRead=null;
			XmlNode objNode=null;
			StringBuilder sbSql=null;
			XmlElement objElement=null;
			XmlDocument objDoc=null;
			string sTemp="";
			int iFound=0;
			try
			{
				sbSql=new StringBuilder();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='FindZip']");
				sTemp=GetValue(p_objXmlDocument,"FindZip");
				sTemp=sTemp.Trim();
				if (sTemp.Length >5)
				{
					sTemp=sTemp.Substring(0,5);
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='AgentsByZip']");
				sbSql.Append(" SELECT DISTINCT LAST_NAME FROM WCP_AGNCY_SERV_ZIP,WCP_STATE_AGENCY ");
				sbSql.Append(" WHERE WCP_AGNCY_SERV_ZIP.AGENCY_ID = WCP_STATE_AGENCY.AGENCY_ID ");
				sbSql.Append(" AND WCP_AGNCY_SERV_ZIP.ZIP_CODE = '"+sTemp+"'");
				sbSql.Append(" ORDER BY LAST_NAME");
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=p_objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue(0)));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue(0));
					objNode.AppendChild(objElement);
					iFound=1;
				}
				
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				if (iFound==0)
				{
					objElement=p_objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value","");
					objElement.InnerText="Not Found";
					objNode.AppendChild(objElement);
				}
				objDoc=Get(p_objXmlDocument);
				if (objDoc!=null)
				{
					objNode=objDoc.ImportNode(objNode,true);
					if (objDoc.FirstChild==null)
					{
						objDoc.AppendChild(objNode);
					}
					else
					{
						objDoc.FirstChild.AppendChild(objNode);
					}
				}
				return objDoc;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.FindZip.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				objDoc=null;
				objRead=null;
				objElement=null;
				objRead=null;
				objNode=null;
				sbSql=null;
			
			}
		}
		/// Name		: Get
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Gets data to populate Screen
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>Xml containing the data to be populated on screen</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			DbReader objRead=null;
			XmlNode objNode=null;
			XmlNodeList objNodeList=null;
			XmlElement objElement=null;
			XmlElement objLstRow=null;
			StringBuilder sbSql=null;
			string sTransId="";
			string sRowId="";
			string sTemp="";
			string sOffice="";
			string sAgents="";
			int iInLoop=0;
			LocalCache objCache=null;
			SortedList slPostalCodes=null;
			try
			{
				XMLDoc = p_objXmlDocument;
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
				sTemp=GetValue(p_objXmlDocument,"Jurisdictions");
                objCache = new LocalCache(base.ConnectString, m_iClientId);
				objNode.InnerText="";
				if (!sTemp.Equals(""))
				{
					//objCache = new LocalCache(base.ConnectString);
					sTransId=Conversion.ConvertObjToStr(objCache.GetTableId(sTemp.Split('^')[1]+"_AGENCY_FUNCTION"));
					sRowId=sTemp.Split('^')[0];
					//objCache.Dispose();
					if (Conversion.ConvertStrToInteger(sTransId) <=0)
					{
						objElement =(XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
                        string sError = Globalization.GetString("WorkCompAgentServiceArea.NotSupported.Error", m_iClientId);
						sError=sError.Replace("%%",sTemp.Split('^')[2]);
						objElement.SetAttribute("NotSupported",sError);
					}
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='OfficeFunction']");
				sOffice=GetValue(p_objXmlDocument,"OfficeFunction");
				objNode.InnerText="";
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Agents']");
				sAgents=GetValue(p_objXmlDocument,"Agents");
				objNode.InnerText="";
				Get();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
				sbSql=new StringBuilder();
				sbSql.Append(" SELECT STATE_ROW_ID,STATE_ID,STATE_NAME FROM STATES WHERE STATE_ROW_ID > 0 ORDER BY STATE_ID ASC ");
				objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
				while (objRead.Read())
				{
					objElement=p_objXmlDocument.CreateElement("option");
					objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("STATE_ROW_ID")));
					objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("STATE_ID")+"-"+Conversion.ConvertObjToStr(objRead.GetValue("STATE_NAME")));
					objNode.AppendChild(objElement);
				}
				if (!objRead.IsClosed)
				{
					objRead.Close();
				}
				
				objNodeList = objNode.SelectNodes("//option");
				foreach(XmlNode objTmpNode in objNodeList)
				{
					objTmpNode.Attributes["value"].Value=objTmpNode.Attributes["value"].Value+"^"+objTmpNode.InnerText.Split('-')[0]+"^"+objTmpNode.InnerText;
				}
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				if (objNode.FirstChild!=null)
					objNode=objNode.InsertBefore(objElement,objNode.FirstChild);
				if ((!sTransId.Trim().Equals("")))
				{
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");  
					objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+sTemp+"']");
					objElement.SetAttribute("selected","1");
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='OfficeFunction']");  	
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				iInLoop=0;
				objNode.AppendChild(objElement);
				if (!sTransId.Trim().Equals(""))
				{
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT DISTINCT CODES.CODE_ID, CODES.SHORT_CODE, ");
					sbSql.Append(" CODES_TEXT.CODE_DESC FROM CODES, CODES_TEXT WHERE CODES.CODE_ID = CODES_TEXT.CODE_ID  ");
					sbSql.Append(" AND CODES.TABLE_ID = "+sTransId);
                    sbSql.Append(" AND CODES_TEXT.LANGUAGE_CODE =1033"); //Aman ML Change--Start
                    sbSql = CommonFunctions.PrepareMultilingualQuery(sbSql.ToString(), objCache.GetTableName(Conversion.ConvertStrToInteger(sTransId)), this.LanguageCode);  //Aman ML Change--Start
					objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
					while (objRead.Read())
					{
						objElement=p_objXmlDocument.CreateElement("option");
						objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue(0)));
						objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue(1)+"  "+Conversion.ConvertObjToStr(objRead.GetValue(2)));
						objNode.AppendChild(objElement);
						iInLoop=1;
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
					if (iInLoop==0)
					{
						//throw exception
						//There are no Office Functions defined for this jurisdiction.
                        objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='NoOfficeSelected']");
                        objElement.SetAttribute("NoOffice", Globalization.GetString("WorkCompAgentServiceArea.NoOfficeFunction.Error", m_iClientId));
					}
				}

				
				if ((!sOffice.Trim().Equals("")))
				{
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='OfficeFunction']");  
					objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+sOffice+"']");
					objElement.SetAttribute("selected","1");
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Agents']");  	
				objElement=p_objXmlDocument.CreateElement("option");
				objElement.SetAttribute("value","");
				objElement.InnerText="";
				objNode.AppendChild(objElement);
				if (!sOffice.Trim().Equals(""))
				{
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT DISTINCT AGENCY_ID ,LAST_NAME FROM WCP_STATE_AGENCY  ");
					sbSql.Append(" WHERE STATE_ROW_ID = "+sRowId+" AND FUNCTION_CODE_ID = "+sOffice);
					sbSql.Append(" AND DELETED_FLAG = 0 ORDER BY LAST_NAME ");
					objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
					while (objRead.Read())
					{
						objElement=p_objXmlDocument.CreateElement("option");
						objElement.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue(0)));
						objElement.InnerText=Conversion.ConvertObjToStr(objRead.GetValue(1));
						objNode.AppendChild(objElement);
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
				}
				if ((!sAgents.Trim().Equals("")))
				{
					objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Agents']");  
					objElement=(XmlElement)objNode.SelectSingleNode("./option[@value='"+sAgents+"']");
					objElement.SetAttribute("selected","1");
				}
				if (!sAgents.Trim().Equals(""))
				{
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT DISTINCT * FROM WCP_STATE_AGENCY WHERE STATE_ROW_ID =  "+sRowId);
					sbSql.Append(" AND FUNCTION_CODE_ID = "+sOffice);
					sbSql.Append(" AND AGENCY_ID = "+sAgents);
					objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
					if (objRead.Read())
					{
						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='BusinessName']"); 
						objNode.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("LAST_NAME"));
						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address1']"); 
						objNode.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("ADDR1"));
						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address2']"); 
						objNode.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("ADDR2"));

                        // JIRA 6420 syadav55 starts
                        objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address3']"); 
                        objNode.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("ADDR3"));
                        objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address4']");
                        objNode.InnerText = Conversion.ConvertObjToStr(objRead.GetValue("ADDR4"));
                        // JIRA 6420 syadav55 ends

						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='City']"); 
						objNode.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("CITY"));
						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='PostCode']"); 
						objNode.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("ZIP_CODE"));
						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Contact']"); 
						objNode.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("CONTACT"));
						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ContactPhone']"); 
						objNode.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("PHONE"));
						objNode = p_objXmlDocument.SelectSingleNode("//control[@name='State']"); 
						objNode.InnerText=sTemp.Split('^')[2];
						
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
						
					slPostalCodes=new SortedList();
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT DISTINCT * FROM POSTAL_CODES ");
					sbSql.Append(" WHERE JURIS_ROW_ID = "+sRowId);
					sbSql.Append(" AND DELETED_FLAG = 0");
					sbSql.Append(" ORDER BY POSTAL_CODE");
					objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
					while (objRead.Read())
					{
						try
						{
							slPostalCodes.Add(Conversion.ConvertObjToStr(objRead.GetValue("POSTAL_CODE")),Conversion.ConvertObjToStr(objRead.GetValue("POSTAL_CODE")));
						}
						catch
						{
							continue;
						}
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
					objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='ServiceAreaTableName']");  
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT * FROM WCP_AGNCY_SERV_ZIP WHERE STATE_ROW_ID =  "+sRowId);
					sbSql.Append(" AND FUNCTION_CODE_ID = "+sOffice);
					sbSql.Append(" AND AGENCY_ID = "+sAgents);
					sbSql.Append(" ORDER BY ZIP_CODE ASC");
					objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
					while (objRead.Read())
					{
						objLstRow=p_objXmlDocument.CreateElement("option");
						objLstRow.SetAttribute("value",Conversion.ConvertObjToStr(objRead.GetValue("ZIP_CODE")));
						objLstRow.InnerText=Conversion.ConvertObjToStr(objRead.GetValue("ZIP_CODE"));
						objElement.AppendChild(objLstRow);
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
					sbSql=new StringBuilder();
					sbSql.Append(" SELECT ZIP_CODE FROM WCP_AGNCY_SERV_ZIP WHERE STATE_ROW_ID =  "+sRowId);
					sbSql.Append(" AND FUNCTION_CODE_ID = "+sOffice);
					sbSql.Append(" ORDER BY ZIP_CODE ASC");
					objRead=DbFactory.GetDbReader(base.ConnectString,sbSql.ToString());
					while (objRead.Read())
					{
						try
						{
							slPostalCodes.Remove(Conversion.ConvertObjToStr(objRead.GetValue("ZIP_CODE")));
						}
						catch
						{
						}
					}
					if (!objRead.IsClosed)
					{
						objRead.Close();
					}
					objElement = (XmlElement)p_objXmlDocument.SelectSingleNode("//control[@name='JurisdictionalTableName']");  
					sbSql=new StringBuilder();
					if (!sTemp.Trim().Equals(""))
					{
						IDictionaryEnumerator objEnum=slPostalCodes.GetEnumerator();
						while (objEnum.MoveNext())
						{
							objLstRow = p_objXmlDocument.CreateElement("option");
							objLstRow.InnerText=Conversion.ConvertObjToStr(objEnum.Value.ToString());
							objLstRow.SetAttribute("value",objEnum.Value.ToString());
							objElement.AppendChild(objLstRow);
						}
					}

				}
				
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.Get.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if (objRead!=null)
				{
					objRead.Close();
				}
				objRead=null;
				objNode=null;
				objNodeList=null;
				objElement=null;
				objLstRow=null;
				sbSql=null;
				if (objCache!=null)
					objCache.Dispose();
				objCache=null;
				slPostalCodes=null;
			}
			return XMLDoc;
		}
		/// Name		: Save
		/// Author		: Parag Sarin
		/// Date Created: 05/02/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Function Saves Worker Compensation Agent Service Area data
		/// </summary>
		/// <param name="p_objXmlDocument">Xml containing the Parameters for the function</param>
		/// <returns>True/False</returns>
		public bool Save(XmlDocument p_objXmlDocument)
		{
			XMLDoc = p_objXmlDocument;
			ArrayList objQuery=null;
			XmlNode objNode=null;
			string sDate="";
			StringBuilder sbSql=null;
			string sLastName="";
			string sAgentsId="";
			string sAddr1="";
			string sAddr2="";
            string sAddr3 = ""; // JIRA 6420 syadav55
            string sAddr4 = ""; // JIRA 6420 syadav55
			string sCity="";
			string sZip="";
			string sPhone="";
			string sContact="";
			string sOfficeId="";
			string sTemp="";
			string sTransId="";
			string sRowId="";
			LocalCache objCache=null;
			int iNextId=0;
			int iAddOrEdit=0;
			string sUser=m_sUserName;
			string sValue="";
			bool bReturn=false;
			try
			{
				objQuery=new ArrayList();
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Jurisdictions']");
				sTemp=GetValue(p_objXmlDocument,"Jurisdictions");
				if (!sTemp.Equals(""))
				{
                    objCache = new LocalCache(base.ConnectString, m_iClientId);
					sTransId=Conversion.ConvertObjToStr(objCache.GetTableId(sTemp.Split('^')[1]+"_AGENCY_FUNCTION"));
					sRowId=sTemp.Split('^')[0];
					objCache.Dispose();
				}
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='OfficeFunction']");
				sOfficeId=GetValue(p_objXmlDocument,"OfficeFunction");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Agents']");
				sValue=GetValue(p_objXmlDocument,"Agents");
				if (sValue!=null)
				{
					if (sValue.Trim().Equals(""))
					{
						iAddOrEdit=0;
					}
					else
					{
						iAddOrEdit=1;
					}
				}
				else
				{
					iAddOrEdit=0;
				}
					
				sAgentsId=sValue;
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='BusinessName']");
				sLastName=GetValue(p_objXmlDocument,"BusinessName");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='ContactPhone']");
				sPhone=GetValue(p_objXmlDocument,"ContactPhone");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Contact']");
				sContact=GetValue(p_objXmlDocument,"Contact");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='PostCode']");
				sZip=GetValue(p_objXmlDocument,"PostCode");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='City']");
				sCity=GetValue(p_objXmlDocument,"City");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address1']");
				sAddr1=GetValue(p_objXmlDocument,"Address1");
				objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address2']");
				sAddr2=GetValue(p_objXmlDocument,"Address2");

                // JIRA 6420 syadav55 starts
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address3']");
                sAddr3 = GetValue(p_objXmlDocument, "Address3");
                objNode = p_objXmlDocument.SelectSingleNode("//control[@name='Address4']");
                sAddr4 = GetValue(p_objXmlDocument, "Address4");
                // JIRA 6420 syadav55 ends

				if (iAddOrEdit==0)
				{
					sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
                    iNextId = Utilities.GetNextUID(base.ConnectString, "WCP_STATE_AGENCY", m_iClientId);
					sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
					sbSql=new StringBuilder();
                    //Error found in the below query while doing conne leaks changes.pawan
                    sbSql.Append("INSERT INTO WCP_STATE_AGENCY(AGENCY_ID, STATE_ROW_ID, FUNCTION_CODE_ID, LAST_NAME, ADDR1, ADDR2, ADDR3, ADDR4, CITY, ZIP_CODE, PHONE, CONTACT, DTTM_RCD_ADDED, UPDATED_BY_USER,DTTM_RCD_LAST_UPD , ADDED_BY_USER, DELETED_FLAG) VALUES (" + iNextId.ToString());
                    sbSql.Append(",'" + sRowId + "','" + sOfficeId + "','" + sLastName + "','" + sAddr1 + "','" + sAddr2 + "','" + sAddr3 + "','" + sAddr4 + "','" + sCity + "','" + sZip + "','" + sPhone + "','" + sContact + "'," + sDate + ",'" + sUser + "'," + sDate + ",'" + sUser + "',0)");
					objQuery.Add(sbSql.ToString());
				}
				else
				{
					sDate="'"+Conversion.ToDbDateTime(DateTime.Now)+"'";
					sbSql=new StringBuilder();
					sbSql.Append("UPDATE WCP_STATE_AGENCY SET LAST_NAME='"+sLastName+"',ADDR1='"+sAddr1+"',ADDR2='"+sAddr2+"',ADDR3='"+sAddr3+"',ADDR4='"+sAddr4);
					sbSql.Append("',CITY='"+sCity+"',ZIP_CODE='"+sZip+"',CONTACT='"+sContact+"',PHONE='"+sPhone+"'");
					sbSql.Append(",DTTM_RCD_LAST_UPD="+sDate);
					sbSql.Append(",UPDATED_BY_USER='"+sUser+"'");
					sbSql.Append(" WHERE STATE_ROW_ID="+sRowId);
					sbSql.Append(" AND FUNCTION_CODE_ID ="+sOfficeId);
					sbSql.Append(" AND AGENCY_ID ="+sAgentsId);
					objQuery.Add(sbSql.ToString());
				}
				base.Save(objQuery);
				bReturn=true;
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx;
			}
			catch (Exception p_objEx)
			{
                throw new RMAppException(Globalization.GetString("WorkCompAgentServiceArea.Save.Error", m_iClientId), p_objEx);
			}
			finally
			{
				objQuery=null;
				objNode=null;
				sDate="";
				sbSql=null;
				if (objCache!=null)
					objCache.Dispose();
				objCache=null;
			}
			return bReturn;
		}
		#endregion
	}
}
