using System;
using System.Xml;
using Riskmaster.Common;  
 
namespace Riskmaster.Application.RMUtilities
{
	/// <summary>
	///Author  :   Pankaj Chowdhury
	///Dated   :   21 Mar 2005
	///Purpose :   This class Fetches Activities List.
	/// </summary>
	public class ActivityAdd
	{
		/// <summary>
		/// Connection String
		/// </summary>
		string m_sConnStr = "";

        int m_iClientId = 0;
		/// <summary>
		/// Default Constructor
		/// </summary>
		/// <param name="p_sConnStr">Connection String</param>
		public ActivityAdd(string p_sConnStr, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_sConnStr = p_sConnStr;
		}

		/// <summary>
		/// Get method returns the data for Avail Activities
		/// </summary>
		/// <param name="p_objXmlDocument">Input xml structure document</param>
		/// <returns>Xml Document with Data</returns>
		public XmlDocument Get(XmlDocument p_objXmlDocument)
		{
			LocalCache objCache=null;
			
			try
			{
				objCache = new LocalCache(m_sConnStr,m_iClientId);
				UTILITY.LoadListWithCodes(m_sConnStr,p_objXmlDocument,"ActAvail",objCache.GetTableId("WPA_ACTIVITIES"), m_iClientId);  
				objCache.Dispose();

				return p_objXmlDocument;
			}
			finally
			{
				if(objCache != null)
					objCache.Dispose();
			}
		}
	}
}
