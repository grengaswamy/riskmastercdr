using System;
using System.Collections.Generic;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Text;
using Riskmaster.ExceptionTypes;
using Riskmaster.DataModel;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /**************************************************************
    * $File		: GenerateNextInstallment.cs
    * $Revision	: 1.0.0.0
    * $Date		: 
    * $Author		: Gagan Bhatnagar
    * $Comment		:  
    * $Source		:  	
   **************************************************************/
    public class GenerateNextInstallment : IDisposable
    {
        #region Constructor

        /// <summary>
        /// Constructor, initializes the variables to the default value.
        /// </summary>
        /// <param name="p_sDsnName">DSN Name</param>
        /// <param name="p_sUserName">User Name</param>
        /// <param name="p_sPassword">Password</param>
        public GenerateNextInstallment(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
            try
            {
                m_iClientId = p_iClientId;
                m_sUserName = p_sUserName;
                m_sPassword = p_sPassword;
                m_sDsnName = p_sDsnName;
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            }
            catch (Exception exc)
            {
                throw exc;
            }
		}

		#endregion 

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
        }

        #region "Member Variables"
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = string.Empty;
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = string.Empty;
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>		
        private string m_sDsnName = string.Empty;
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;

        private int m_iClientId = 0;

        # endregion



        #region Public Methods

        /// <summary>
        /// Generates next installment
        /// </summary>
        public void GenerateInstallment()
        {
            BillingMaster objBillingMaster = null;
            BillingManager objBillingManager = null;
            WpaDiaryEntry objWpaDiaryEntry = null;
            BillXInstlmnt objBillXInstlmnt = null;
            Db.DbReader objReader = null;
            ArrayList objDiaries = new ArrayList();
            string strConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            string sTime = String.Empty;
            string ssql = String.Empty;
            DateTime objStart = DateTime.MinValue;
            DateTime objEnd = DateTime.MinValue; ;
            TimeSpan t = TimeSpan.MinValue;            

            try
            {
                //read to find all the installment diaries with today's date or before
                ssql = "SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE ATTACH_TABLE = 'BILL_X_INSTLMNT'" +
                       " AND COMPLETE_DATE <= '" + DateTime.Now.ToString("yyyyMMdd") +
                       "' AND STATUS_OPEN <>0 AND DIARY_VOID = 0 AND DIARY_DELETED = 0 ORDER BY COMPLETE_DATE";

                objReader = DbFactory.GetDbReader(strConnectionString, ssql);

                while (objReader.Read())
                {
                    objDiaries.Add(objReader.GetInt(0));
                }

                objBillingMaster = new BillingMaster(m_objDataModelFactory, m_iClientId);

                foreach (object objlngEntryId in objDiaries)
                {
                    objWpaDiaryEntry = (WpaDiaryEntry)m_objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);
                    objWpaDiaryEntry.MoveTo((int)objlngEntryId);
                    objBillingMaster.Diaries.Add(objWpaDiaryEntry.EntryId, objWpaDiaryEntry);

                    try
                    {

                        //retrieve installment
                        objBillXInstlmnt = (BillXInstlmnt)m_objDataModelFactory.GetDataModelObject("BillXInstlmnt", false);

                        objBillXInstlmnt.MoveTo(objWpaDiaryEntry.AttachRecordid);
                        objBillingMaster.Installments.Add(objBillXInstlmnt);

                        //generate the next installment/schedule diaries
                       
                        objStart = DateTime.Now;
                        sTime = objStart.TimeOfDay.ToString().Substring(0, 2)
                                       + objStart.TimeOfDay.ToString().Substring(3, 2)
                                       + objStart.TimeOfDay.ToString().Substring(6, 2);
                        objWpaDiaryEntry.TeStartTime = sTime;
                        objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);

                        
                        objBillingManager.CreateChargeItemInstallment(objBillingMaster, objBillXInstlmnt);

                        Console.WriteLine("0 ^*^*^ Installmnt {0} of Amount $ {1} has been generated for Policy Id {2}",objBillXInstlmnt.InstallmentNum ,objBillXInstlmnt.Amount,objBillXInstlmnt.PolicyId);
                        
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("1001 ^*^*^ {0} ", ex.Message);
                        Log.Write(ex.Message, "CommonWebServiceLog", m_iClientId);   
                        //TO DO
                        //log and continue
                        //Log.Write(ex.Message, "Default");
                  
                    }

                    objEnd = DateTime.Now;
                    sTime = objEnd.TimeOfDay.ToString().Substring(0, 2)
                                    + objEnd.TimeOfDay.ToString().Substring(3, 2)
                                    + objEnd.TimeOfDay.ToString().Substring(6, 2);
                    
                    t = objEnd.Subtract(objStart);
                    
                    objWpaDiaryEntry.TeEndTime = sTime;
                    objWpaDiaryEntry.TeTotalHours = t.TotalHours;
                    //objWpaDiaryEntry.TeTotalHours = int.Parse(objWpaDiaryEntry.TeEndTime) - int.Parse(objWpaDiaryEntry.TeStartTime);

                    objWpaDiaryEntry.StatusOpen = false;
                    objBillingMaster.Save();
                }
                
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objWpaDiaryEntry != null)
                {
                    objWpaDiaryEntry.Dispose();
                }
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                }
                if (objBillXInstlmnt != null)
                {
                    objBillXInstlmnt.Dispose();
                }
                objDiaries = null;
            }
        }

        #endregion
    }
}
