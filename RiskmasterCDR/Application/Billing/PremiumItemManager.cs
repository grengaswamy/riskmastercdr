using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;

using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /**************************************************************
     * $File		: PremiumItemManager.cs
     * $Revision	: 1.0.0.0
     * $Date		: 
     * $Author		: Nitesh Deedwania
     * $Comment		:  
     * $Source		:  	
    **************************************************************/
    public class PremiumItemManager : IDisposable
    {
        #region Member Variables
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Private variable to store the instance of Local Cache object
        /// </summary>
        private LocalCache m_objLocalCache = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;
        private int m_iClientId = 0;

        private const string PREMIUM_NODE_NAME = "PremiumItem";
        private const string PREMIUM_DUE_DATE = "DueDate";
        private const string PREMIUM_PAYMENT_METHOD = "PaymentMethod";
        private const string PREMIUM_AMOUNT = "Amount";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor, initializes the variables to the default value .
        /// </summary>
        /// <param name="p_objDataModelFactory">DataModelFactory Object</param>
        public PremiumItemManager(DataModelFactory p_objDataModelFactory, int p_iClientId)
        {
            m_objDataModelFactory = p_objDataModelFactory;
            m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString;
            m_iClientId = p_iClientId;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
        }
        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sDsnName">DsnName</param>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>	
        public PremiumItemManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
        {

            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
        }
        #endregion

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }

        public XmlDocument GetPremiumItemData(int p_iBillItemRowId)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootNode = null;

            try
            {
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                if (p_iBillItemRowId != 0)
                    objBillXBillItem.MoveTo(p_iBillItemRowId);
                else
                    throw new RMAppException("PremiumItemManager.GetPremiumItemData.NoBillingSelected");

                Function.StartDocument(ref m_objDocument, ref objRootNode, PREMIUM_NODE_NAME, m_iClientId);
                this.CreatePremiumItemXml(objRootNode, objBillXBillItem);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PremiumItemManager.GetPremiumItemData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objRootNode = null;
            }
            return (m_objDocument);
        }

        private void CreatePremiumItemXml(XmlElement p_objParentNode, BillXBillItem p_objBillXBillItem)
        {
            try
            {
                Function.CreateNodeWithCodeValues(p_objParentNode, PREMIUM_PAYMENT_METHOD, p_objBillXBillItem.BillXPremItem.PremiumItemType, m_sConnectionString, m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PREMIUM_AMOUNT, string.Format("{0:C}", Math.Abs(p_objBillXBillItem.Amount) * -1), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PREMIUM_DUE_DATE, Conversion.GetDBDateFormat(p_objBillXBillItem.DueDate, "d"), m_iClientId);
                Function.SetBillItemXml(p_objBillXBillItem, p_objParentNode, m_sConnectionString, m_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PremiumItemManager.CreatePremiumItemXml.Error", m_iClientId), p_objEx);
            }
        }


     

    }
}
