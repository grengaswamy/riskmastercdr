using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Collections;

using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
    /**************************************************************
     * $File		: AdjustmentManager.cs
     * $Revision	: 1.0.0.0
     * $Date		: 
     * $Author		: Vaibhav/Nitesh Deedwania
     * $Comment		:  
     * $Source		:  	
    **************************************************************/
    public class AdjustmentManager : IDisposable
    {

        #region Member Variables
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
        /// <summary>
        /// Represents the connection string for the underlying Riskmaster Database.
        /// </summary>
        private string m_sConnectionString = string.Empty;
        /// <summary>
        /// Private variable to store the instance of Local Cache object
        /// </summary>
        private LocalCache m_objLocalCache = null;
        /// <summary>
        /// Private variable to store the instance of XmlDocument object
        /// </summary>
        private XmlDocument m_objDocument = null;

        private int m_iClientId = 0;

        private const string ADJUSTMENT_NODE_NAME = "Adjustments";
        private const string ADJUSTMENT_TYPE = "AdjustmentType";
        private const string ADJUSTMENT_AMOUNT = "Amount";
        private const string ADJUSTMENT_DISABLEALL = "DisableControls";


        #endregion

        #region Constructor

        public AdjustmentManager(DataModelFactory p_objDataModelFactory, int p_iClientId)
        {
            m_objDataModelFactory = p_objDataModelFactory;
            m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString;
            m_iClientId = p_iClientId;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
        }

        public AdjustmentManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModel.DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
        }

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }

        #endregion
        public XmlDocument GetNewAdjustmentData(int p_PolicyId, int p_iTermNumber, string p_sPolicyNumber)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootNode = null;

            try
            {
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                if (p_PolicyId != 0)
                {
                    objBillXBillItem.PolicyId = p_PolicyId;
                    objBillXBillItem.PolicyNumber = p_sPolicyNumber;
                    objBillXBillItem.TermNumber = p_iTermNumber;
                    objBillXBillItem.BillingItemType = m_objLocalCache.GetCodeId("A", "BILLING_ITEM_TYPES");
                }
                else
                    throw new RMAppException("AdjustmentManager.GetNewAdjustmentData.NoPolicySelect");

                Function.StartDocument(ref m_objDocument, ref objRootNode, ADJUSTMENT_NODE_NAME, m_iClientId);
                this.CreateAdjustmentItemXml(objRootNode, objBillXBillItem);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjustmentManager.GetNewAdjustmentData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objRootNode = null;
            }
            return (m_objDocument);
        }
        public XmlDocument GetAdjustmentData(int p_iBillItemRowId)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootNode = null;

            try
            {
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                if (p_iBillItemRowId != 0)
                    objBillXBillItem.MoveTo(p_iBillItemRowId);
                else
                    throw new RMAppException("AdjustmentManager.GetAdjustmentData.NoBillingSelected");

                Function.StartDocument(ref m_objDocument, ref objRootNode, ADJUSTMENT_NODE_NAME, m_iClientId);
                this.CreateAdjustmentItemXml(objRootNode, objBillXBillItem);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjustmentManager.GetAdjustmentData.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                objRootNode = null;
            }
            return (m_objDocument);
        }


        private void CreateAdjustmentItemXml(XmlElement p_objParentNode, BillXBillItem p_objBillXBillItem)
        {
            XmlElement objNew = null;
            XmlDocument objXML = p_objParentNode.OwnerDocument;
            XmlElement xmlOption = null;
            XmlAttribute xmlOptionAttrib = null;
            XmlCDataSection objCData = null;

            //Manish Multicurrency
            string sShortCode = string.Empty;
            string sDesc = string.Empty;
            string sCurrency = string.Empty;
            LocalCache objCache = null;
            objCache = new LocalCache(m_sConnectionString, m_iClientId);

            objCache.GetCodeInfo(CommonFunctions.GetBaseCurrencyCode(m_sConnectionString), ref sShortCode, ref sDesc);
            sCurrency = sDesc.Split('|')[1];

            try
            {
                //Function.CreateNodeWithCodeValues(p_objParentNode, ADJUSTMENT_TYPE, p_objBillXBillItem.BillXAdjstmnt.AdjustmentType, m_sConnectionString);
                Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_TYPE, Conversion.ConvertObjToStr(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) + "," + m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType), m_iClientId);
                Function.CreateAndSetElement(p_objParentNode, "BaseCurrencyType", sCurrency, m_iClientId);
                if (m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) == "WO" ||
                    m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) == "UP" ||
                    m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) == "OP")
                    Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, p_objBillXBillItem.Amount.ToString(), m_iClientId);
                else
                {
                    if (p_objBillXBillItem.Amount < 0)
                    {
                        if (m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) == "DV" ||
                            m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) == "ED")
                            //Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, string.Format("{0:C}", p_objBillXBillItem.Amount)); Changes made for R5 : csingh7
                            Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, Convert.ToString(p_objBillXBillItem.Amount), m_iClientId);
                        else
                            //Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, string.Format("{0:C}", p_objBillXBillItem.Amount * -1));Changes made for R5 : csingh7
                            Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, Convert.ToString(p_objBillXBillItem.Amount * -1), m_iClientId);
                    }
                    else
                    {
                        if (m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) == "DV" ||
                            m_objLocalCache.GetShortCode(p_objBillXBillItem.BillXAdjstmnt.AdjustmentType) == "ED")
                            //Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, string.Format("{0:C}", p_objBillXBillItem.Amount * -1));Changes made for R5 : csingh7
                            Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, Convert.ToString(p_objBillXBillItem.Amount * -1), m_iClientId);
                        else
                            //Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, string.Format("{0:C}", p_objBillXBillItem.Amount));Changes made for R5 : csingh7
                            Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_AMOUNT, Convert.ToString(p_objBillXBillItem.Amount), m_iClientId);
                    }
                }
                if ((p_objBillXBillItem.Status != m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT") || p_objBillXBillItem.BillXAdjstmnt.AdjustmentType == m_objLocalCache.GetCodeId("OP", "ADJUSTMENT_TYPES") || p_objBillXBillItem.BillXAdjstmnt.AdjustmentType == m_objLocalCache.GetCodeId("UP", "ADJUSTMENT_TYPES") || p_objBillXBillItem.BillXAdjstmnt.AdjustmentType == m_objLocalCache.GetCodeId("ED", "ADJUSTMENT_TYPES")) &&  p_objBillXBillItem.Status != 0)
                {
                    //Function.CreateAndSetElement(p_objParentNode, ADJUSTMENT_DISABLEALL, "True");
                    XmlElement objTempElement = p_objParentNode.OwnerDocument.CreateElement("DisableControls");

                    XmlElement objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "AdjustmentType";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Amount";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "InvoiceNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "NoticeNumber";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Comments";
                    objTempElement.AppendChild(objChildElement);

                    objChildElement = p_objParentNode.OwnerDocument.CreateElement("Item");
                    objChildElement.InnerText = "Ok";
                    objTempElement.AppendChild(objChildElement);

                    p_objParentNode.AppendChild(objTempElement);

                }
                string sCodevalues = Function.GetCodes("ADJUSTMENT_TYPES", m_sConnectionString, m_iClientId);
                //rupal:start,mits 27977
                //string[] sCodeArr = sCodevalues.Split('^');
                string[] sCodeArr = sCodevalues.Split(Function.NewCodeSeperator, StringSplitOptions.RemoveEmptyEntries);
                //rupal:end

                objNew = objXML.CreateElement("CodeList");

                xmlOption = objXML.CreateElement("option");
                xmlOptionAttrib = objXML.CreateAttribute("value");
                xmlOption.Attributes.Append(xmlOptionAttrib);
                objNew.AppendChild(xmlOption);
                xmlOptionAttrib = null;
                xmlOption = null;

                for (int i = 0; i < sCodeArr.Length; i++)
                {
                    //rupal:start,mits 27977
                    //string[] sCodes = sCodeArr[i].Split('~');
                    string[] sCodes = sCodeArr[i].Split(Function.CodeIdSeperator, StringSplitOptions.RemoveEmptyEntries);
                    //rupal:end
                    xmlOption = objXML.CreateElement("option");
                    objCData = objXML.CreateCDataSection(sCodes[1]);
                    xmlOption.AppendChild(objCData);
                    xmlOptionAttrib = objXML.CreateAttribute("value");

                    xmlOptionAttrib.Value = sCodes[0];

                    xmlOption.Attributes.Append(xmlOptionAttrib);
                    objNew.AppendChild(xmlOption);
                }

                objXML.DocumentElement.AppendChild(objNew);

                Function.SetBillItemXml(p_objBillXBillItem, p_objParentNode, m_sConnectionString, m_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjustmentManager.CreateAdjustmentItemXml.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNew = null;
                objXML = null;
                xmlOption = null;
                xmlOptionAttrib = null;
                objCData = null;
            }
        }


        public void SaveAdjustment(XmlDocument p_objInputDoc, int p_iBillItemRowId)
        {
            BillXBillItem objBillXBillItem = null;
            //policy Billing 
            double dblAmount = 0.0;
            string sDateEntered = string.Empty;
            string sDateReconciled = string.Empty;
            int iEntryBatchNumber = 0;
            int iEntryBatchType = 0;
            int iFinalBatchNumber = 0;
            int iFinalBatchType = 0;
            int iInvoiceNumber = 0;
            int iNoticeNumber = 0;
            int iBillingItemType = 0;
            int iStatus = 0;
            int iTermNumber = 0;
            string sComment = string.Empty;
            int iAdjusmenttype = 0;
            string sPolicyNumber = string.Empty;
            int iPolicyId = 0;
            try
            {
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                Function.GetBillItemXml(p_objInputDoc, ref sPolicyNumber, ref iPolicyId, ref sDateReconciled, ref iEntryBatchNumber, ref iEntryBatchType, ref iFinalBatchNumber, ref iFinalBatchType, ref iInvoiceNumber, ref iNoticeNumber, ref iStatus, ref iTermNumber, ref sComment, m_sConnectionString, m_iClientId);
                //Function.GetBillItemXml(p_objInputDoc, sDateEntered, sDateReconciled, iEntryBatchNumber, iEntryBatchType, iFinalBatchNumber, iFinalBatchType, iInvoiceNumber, iNoticeNumber, iStatus, iTermNumber, sComment);

                iBillingItemType = m_objLocalCache.GetCodeId("A", "BILLING_ITEM_TYPES");
                //iAdjusmenttype = Conversion.ConvertStrToInteger(Function.GetCodeValue(p_objInputDoc, ADJUSTMENT_TYPE));
                iAdjusmenttype = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, ADJUSTMENT_TYPE, m_iClientId).Substring(0, Function.GetValue(p_objInputDoc, ADJUSTMENT_TYPE, m_iClientId).Length - 3));
                if (m_objLocalCache.GetShortCode(iAdjusmenttype) == "WO")
                {
                    if (Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId).StartsWith(@"("))
                    {
                        dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId).Replace("(", "").Replace(")", "")) * -1;
                    }
                    else
                    {
                        dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId));
                    }
                }
                else
                {
                    if (Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId).StartsWith(@"("))
                    {
                        if (m_objLocalCache.GetShortCode(iAdjusmenttype) == "DV" ||
                            m_objLocalCache.GetShortCode(iAdjusmenttype) == "ED")
                            dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId).Replace("(", "").Replace(")", "")) * -1;
                        else
                            dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId).Replace("(", "").Replace(")", "")) * -1;
                    }
                    else
                    {
                        if (m_objLocalCache.GetShortCode(iAdjusmenttype) == "DV" || m_objLocalCache.GetShortCode(iAdjusmenttype) == "ED")
                        {
                            if (Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId)) < 0)
                                dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId));
                            else
                                dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId)) * -1;
                        }
                        else
                            dblAmount = Conversion.ConvertStrToDouble(Function.GetValue(p_objInputDoc, ADJUSTMENT_AMOUNT, m_iClientId));
                    }
                }
                //Save Edited Data else save new record
                if (p_iBillItemRowId != 0)
                    objBillXBillItem.MoveTo(p_iBillItemRowId);

                objBillXBillItem.Amount = dblAmount;
                objBillXBillItem.DateEntered = Conversion.GetDate(DateTime.Now.ToString("d"));
                objBillXBillItem.DateReconciled = sDateReconciled;
                objBillXBillItem.EntryBatchNum = iEntryBatchNumber;
                objBillXBillItem.EntryBatchType = iEntryBatchType;
                objBillXBillItem.FinalBatchNum = iFinalBatchNumber;
                objBillXBillItem.FinalBatchType = iFinalBatchType;
                objBillXBillItem.InvoiceId = iInvoiceNumber;
                objBillXBillItem.NoticeId = iNoticeNumber;
                objBillXBillItem.PolicyId = iPolicyId;
                objBillXBillItem.PolicyNumber = sPolicyNumber;
                objBillXBillItem.BillingItemType = iBillingItemType;
                objBillXBillItem.Status = iStatus;
                objBillXBillItem.TermNumber = iTermNumber;
                objBillXBillItem.Comments = sComment;


                objBillXBillItem.BillXAdjstmnt.AdjustmentType = iAdjusmenttype;
                objBillXBillItem.Save();
          
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjustmentManager.SaveAdjustment.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }

            }

        }
    }
}
