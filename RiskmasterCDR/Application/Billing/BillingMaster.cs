using System;
using System.Collections ;
using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.DataModel ;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: BillingMaster.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Vaibav kaushik/Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class BillingMaster : IDisposable
	{
		#region "Member Variables"
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty;
		/// <summary>
		/// Variable to store Account Instance.
		/// </summary>
		private BillXAccount m_objBillXAccount = null ; 
		/// <summary>
		/// Policies Collection.
		/// </summary>
		private Hashtable m_objPolicies = null ;
        private Hashtable m_objPoliciesTmp = null;
		/// <summary>
		/// Billing Items Collection.
		/// </summary>
		private Hashtable m_objBillingItems = null ;
        private Hashtable m_objBillingItemsTmp = null;
		/// <summary>
		/// Deleted Billing Items Collection.
		/// </summary>
		private ArrayList m_arrlstDeletedBillingItems = null ;
		/// <summary>
		/// Installments Collection. ( Need an indexed Based Search ).
		/// </summary>
		private ArrayList m_arrlstInstallments = null ; 
		/// <summary>
		/// Deleted Installments Collection. 
		/// </summary>
		private ArrayList m_arrlstDeletedInstallments = null ; 
		/// <summary>
		/// Diaries Collection.
		/// </summary>
		private Hashtable m_objDiaries = null ;
        private Hashtable m_objDiariesTmp = null;
		/// <summary>
		/// Delete Diaries Collection.
		/// </summary>
		private Hashtable m_objDiariesToDelete = null ;
        private Hashtable m_objDiariesToDeleteTmp = null;
		/// <summary>
		/// Batches Collection.
		/// </summary>
		private Hashtable m_objBatches = null ;
        private Hashtable m_objBatchesTmp = null;
		/// <summary>
		/// Deleted Batches Collection.
		/// </summary>
		private ArrayList m_arrlstDeletedBatches = null ; 		
		/// <summary>
		/// Invoices Collection.
		/// </summary>
		private Hashtable m_objInvoices = null ;
        private Hashtable m_objInvoicesTmp = null;
		/// <summary>
		/// Notices Collection.
		/// </summary>
		private Hashtable m_objNotices = null ;
        private Hashtable m_objNoticesTmp = null;
        private int m_iClientId = 0;
		#endregion 

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value.
		/// </summary>
		/// <param name="p_objDataModelFactory">Data Model Factory Object.</param>
        public BillingMaster(DataModelFactory p_objDataModelFactory, int p_iClientId)
		{
			m_objDataModelFactory = p_objDataModelFactory ;
            m_iClientId = p_iClientId;
			m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString ;

			m_objBillXAccount = ( BillXAccount ) m_objDataModelFactory.GetDataModelObject( "BillXAccount" , false );
			
			m_objPolicies = new Hashtable() ;
            m_objPoliciesTmp = new Hashtable();
            m_objBillingItemsTmp = new Hashtable();
			m_objBillingItems = new Hashtable() ;
            m_objBillingItemsTmp =new Hashtable() ;
			m_arrlstDeletedBillingItems = new ArrayList();
			m_arrlstInstallments = new ArrayList() ;
			m_arrlstDeletedInstallments = new ArrayList();
			m_objDiaries = new Hashtable() ;
            m_objDiariesTmp = new Hashtable();
			m_objDiariesToDelete = new Hashtable() ;
            m_objDiariesToDeleteTmp = new Hashtable();
			m_objBatches = new Hashtable() ;
            m_objBatchesTmp = new Hashtable();
			m_arrlstDeletedBatches = new ArrayList();
			m_objInvoices = new Hashtable() ;
            m_objInvoicesTmp = new Hashtable();
			m_objNotices = new Hashtable() ;
            m_objNoticesTmp = new Hashtable();	
			
		}
		#endregion 

        public void Dispose()
        {
            if (m_objBillXAccount != null)
            {
                m_objBillXAccount.Dispose();
            }
            m_objPolicies = null;
            m_objPoliciesTmp = null;
            m_objBillingItemsTmp = null;
            m_objBillingItems = null;
            m_objBillingItemsTmp = null;
            m_arrlstDeletedBillingItems = null;
            m_arrlstInstallments = null;
            m_arrlstDeletedInstallments = null;
            m_objDiaries = null;
            m_objDiariesTmp = null;
            m_objDiariesToDelete = null;
            m_objDiariesToDeleteTmp = null;
            m_objBatches = null;
            m_objBatchesTmp = null;
            m_arrlstDeletedBatches = null;
            m_objInvoices = null;
            m_objInvoicesTmp = null;
            m_objNotices = null;
            m_objNoticesTmp = null;
        }

		public BillXAccount Account 
		{
			get
			{
				return m_objBillXAccount;
			}
			set
			{
				m_objBillXAccount = value ;
			}
		}
		public Hashtable Policies 
		{
			get
			{				
				return m_objPolicies;
			}			
		}
		public Hashtable BillingItems 
		{
			get
			{				
				return m_objBillingItems;
			}			
		}
        public ArrayList Installments 
		{
			get
			{				
				return m_arrlstInstallments;
			}			
		}
		public Hashtable Diaries 
		{
			get
			{				
				return m_objDiaries;
			}			
		}
		public Hashtable Batches 
		{
			get
			{				
				return m_objBatches;
			}			
		}
		public Hashtable DiariesToDelete 
		{
			get
			{				
				return m_objDiariesToDelete;
			}			
		}
		public Hashtable Invoices 
		{
			get
			{				
				return m_objInvoices;
			}			
		}
		public Hashtable Notices 
		{
			get
			{				
				return m_objNotices;
			}			
		}

        // Start Naresh 6/18/2007 Added another Function for Save to be called from Enhanced Policy
        /// <summary>
        /// Saves policy,Account,Billing items, installments, diaries, batches, invoices, notices in a single transaction.
        /// </summary>
        public void Save(bool p_bSavePolicy)
        {
            DbTransaction objTrans = null;
            DbConnection objConn = null;
            PolicyEnh objPolicy = null;
            BillXBillItem objBillItem = null;
            BillXInstlmnt objInstallment = null;
            WpaDiaryEntry objDiary = null;
            BillXBatch objBatch = null;
            BillXInvoice objInvoice = null;
            BillXNotice objNotice = null;

            int iIndex = 0;

            try
            {
                objTrans = m_objDataModelFactory.Context.DbConn.BeginTransaction();
                m_objDataModelFactory.Context.DbTrans = objTrans;

                // Save policy

                m_objPoliciesTmp.Clear();
                foreach (int objKey in m_objPolicies.Keys)
                {
                    objPolicy = (PolicyEnh)m_objPolicies[objKey];

                    // npadhy 24/05/07 Commented the Code for Explicitly Saving the BillXAccount, 
                    // BillXInstallment and BillXItem. As these are children of PolicyEnh their objects
                    // are added in the object of the PolicyEnh

                    // Save Account
                    if (objPolicy.PolicyId == m_objBillXAccount.PolicyId)
                        objPolicy.BillXAccountList.Add(m_objBillXAccount);

                    // Save Installments
                    for (iIndex = 0; iIndex < m_arrlstInstallments.Count; iIndex++)
                    {
                        objInstallment = (BillXInstlmnt)m_arrlstInstallments[iIndex];

                        if (m_arrlstDeletedInstallments.Contains(objInstallment.InstallmentRowid))
                            objInstallment.Delete();
                        else
                        {
                            if (objPolicy.PolicyId == objInstallment.PolicyId)
                                objPolicy.BillXInstlmntList.Add(objInstallment);
                        }
                    }

                    // Save Billing items
                    m_objBillingItemsTmp.Clear();
                    foreach (int objKeyBill in m_objBillingItems.Keys)
                    {
                        objBillItem = (BillXBillItem)m_objBillingItems[objKeyBill];

                        if (m_arrlstDeletedBillingItems.Contains(objBillItem.BillingItemRowid))
                            objBillItem.Delete();
                        else
                        {
                            //objBillItem.Save();
                            objPolicy.BillXBillItemList.Add(objBillItem);
                            m_objBillingItemsTmp.Add(objBillItem.BillingItemRowid, objBillItem);
                        }

                    }

                    m_objBillingItems.Clear();
                    foreach (int objKeyBill in m_objBillingItemsTmp.Keys)
                    {
                        objBillItem = (BillXBillItem)m_objBillingItemsTmp[objKeyBill];
                        m_objBillingItems.Add(objBillItem.BillingItemRowid, objBillItem);
                    }

                    // Save the Policy Object, thereby saving the Children
                    objPolicy.Save();
                    m_objPoliciesTmp.Add(objPolicy.PolicyId, objPolicy);
                }

                m_objPolicies.Clear();
                foreach (int objKeyParent in m_objPoliciesTmp.Keys)
                {
                    objPolicy = (PolicyEnh)m_objPoliciesTmp[objKeyParent];
                    m_objPolicies.Add(objPolicy.PolicyId, objPolicy);
                    // npadhy 24/05/07 Commented the Code for Explicitly Saving the BillXAccount, 
                    // BillXInstallment and BillXItem. As these are children of PolicyEnh their objects
                    // are added in the objevt of the PolicyEnh

                    // Save diaries
                    m_objDiariesTmp.Clear();
                    foreach (int objKey in m_objDiaries.Keys)
                    {
                        objDiary = (WpaDiaryEntry)m_objDiaries[objKey];
                        if (objDiary.AttachRecordid == 0)
                        {
                            objDiary.AttachRecordid = objPolicy.PolicyId;
                        }
                        objDiary.Save();
                        m_objDiariesTmp.Add(objDiary.EntryId, objDiary);
                    }


                    m_objDiaries.Clear();
                    foreach (int objKey in m_objDiariesTmp.Keys)
                    {
                        objDiary = (WpaDiaryEntry)m_objDiariesTmp[objKey];
                        m_objDiaries.Add(objDiary.EntryId, objDiary);
                    }

                    // Delete diaries as needed
                    foreach (int objKey in m_objDiariesToDelete.Keys)
                    {
                        objDiary = (WpaDiaryEntry)m_objDiariesToDelete[objKey];
                        objDiary.Delete();
                    }
                    // Save batches
                    m_objBatchesTmp.Clear();
                    foreach (int objKey in m_objBatches.Keys)
                    {
                        objBatch = (BillXBatch)m_objBatches[objKey];

                        if (m_arrlstDeletedBatches.Contains(objBatch.BatchRowid))
                            objBatch.Delete();
                        else
                        {
                            if (objBatch.PolicyId == 0)
                            {
                                objBatch.PolicyId = objPolicy.PolicyId;
                            }
                            objBatch.Save();
                            m_objBatchesTmp.Add(objBatch.BatchRowid, objBatch);
                        }
                    }

                    m_objBatches.Clear();
                    foreach (int objKey in m_objBatchesTmp.Keys)
                    {
                        objBatch = (BillXBatch)m_objBatchesTmp[objKey];
                        m_objBatches.Add(objBatch.BatchRowid, objBatch);
                    }


                    // Save invoices
                    m_objInvoicesTmp.Clear();
                    foreach (int objKey in m_objInvoices.Keys)
                    {
                        objInvoice = (BillXInvoice)m_objInvoices[objKey];
                        if (objInvoice.PolicyId == 0)
                        {
                            objInvoice.PolicyId = objPolicy.PolicyId;
                        }
                        objInvoice.Save();
                        m_objInvoicesTmp.Add(objInvoice.InvoiceRowid, objInvoice);
                    }

                    m_objInvoices.Clear();
                    foreach (int objKey in m_objInvoicesTmp.Keys)
                    {
                        objInvoice = (BillXInvoice)m_objInvoicesTmp[objKey];
                        m_objInvoices.Add(objInvoice.InvoiceRowid, objInvoice);
                    }

                    // Save notices
                    m_objNoticesTmp.Clear();
                    foreach (int objKey in m_objNotices.Keys)
                    {
                        objNotice = (BillXNotice)m_objNotices[objKey];
                        if (objNotice.PolicyId == 0)
                        {
                            objNotice.PolicyId = objPolicy.PolicyId;
                        }
                        objNotice.Save();
                        m_objNoticesTmp.Add(objNotice.NoticeRowid, objNotice);
                    }

                    m_objNotices.Clear();
                    foreach (int objKey in m_objNoticesTmp.Keys)
                    {
                        objNotice = (BillXNotice)m_objNoticesTmp[objKey];
                        m_objNotices.Add(objNotice.NoticeRowid, objNotice);
                    }
                }

                objTrans.Commit();
            }
            catch (RMAppException p_objEx)
            {
                if (objTrans != null)
                    objTrans.Rollback();
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                if (objTrans != null)
                    objTrans.Rollback();
                throw new RMAppException(Globalization.GetString("BillingMaster.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objTrans != null)
                {
                    objTrans.Dispose();
                    objTrans = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                    objConn = null;
                }
                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                    objPolicy = null;
                }
                if (objBillItem != null)
                {
                    objBillItem.Dispose();
                    objBillItem = null;
                }
                if (objInstallment != null)
                {
                    objInstallment.Dispose();
                    objInstallment = null;
                }
                if (objDiary != null)
                {
                    objDiary.Dispose();
                    objDiary = null;
                }
                if (objBatch != null)
                {
                    objBatch.Dispose();
                    objBatch = null;
                }
                if (objInvoice != null)
                {
                    objInvoice.Dispose();
                    objInvoice = null;
                }
                if (objNotice != null)
                {
                    objNotice.Dispose();
                    objNotice = null;
                }
            }
        }
        // End Naresh 6/18/2007 Added another Function for Save to be called from Enhanced Policy

        // Start Naresh 6/18/2007 Rollbacked the Changes for Saving Enhanced Policy Object
        /// <summary>
        /// Saves policy,Account,Billing items, installments, diaries, batches, invoices, notices in a single transaction.
        /// </summary>
        public void Save()
        {
            DbTransaction objTrans = null;
            DbConnection objConn = null;

            PolicyEnh objPolicy = null;
            BillXBillItem objBillItem = null;
            BillXInstlmnt objInstallment = null;
            WpaDiaryEntry objDiary = null;
            BillXBatch objBatch = null;
            BillXInvoice objInvoice = null;
            BillXNotice objNotice = null;

            int iIndex = 0;

            try
            {
                objTrans = m_objDataModelFactory.Context.DbConn.BeginTransaction();
                m_objDataModelFactory.Context.DbTrans = objTrans;

                // Save policy
                m_objPoliciesTmp.Clear();
                foreach (int objKey in m_objPolicies.Keys)
                {
                    objPolicy = (PolicyEnh)m_objPolicies[objKey];
                    objPolicy.Save();
                    m_objPoliciesTmp.Add(objPolicy.PolicyId, objPolicy);
                }

                m_objPolicies.Clear();
                foreach (int objKey in m_objPoliciesTmp.Keys)
                {
                    objPolicy = (PolicyEnh)m_objPoliciesTmp[objKey];
                    m_objPolicies.Add(objPolicy.PolicyId, objPolicy);
                }

                // Save Account
                m_objBillXAccount.Save();

                // Save Billing items
                m_objBillingItemsTmp.Clear();
                foreach (int objKey in m_objBillingItems.Keys)
                {
                    objBillItem = (BillXBillItem)m_objBillingItems[objKey];

                    if (m_arrlstDeletedBillingItems.Contains(objBillItem.BillingItemRowid))
                        objBillItem.Delete();
                    else
                    {
                        objBillItem.Save();
                        m_objBillingItemsTmp.Add(objBillItem.BillingItemRowid, objBillItem);
                    }

                }

                m_objBillingItems.Clear();
                foreach (int objKey in m_objBillingItemsTmp.Keys)
                {
                    objBillItem = (BillXBillItem)m_objBillingItemsTmp[objKey];
                    m_objBillingItems.Add(objBillItem.BillingItemRowid, objBillItem);
                }



                // save installments
                for (iIndex = 0; iIndex < m_arrlstInstallments.Count; iIndex++)
                {
                    objInstallment = (BillXInstlmnt)m_arrlstInstallments[iIndex];

                    if (m_arrlstDeletedInstallments.Contains(objInstallment.InstallmentRowid))
                        objInstallment.Delete();
                    else
                        objInstallment.Save();
                }

                // Save diaries
                m_objDiariesTmp.Clear();
                foreach (int objKey in m_objDiaries.Keys)
                {
                    objDiary = (WpaDiaryEntry)m_objDiaries[objKey];
                    objDiary.Save();
                    m_objDiariesTmp.Add(objDiary.EntryId, objDiary);
                }


                m_objDiaries.Clear();
                foreach (int objKey in m_objDiariesTmp.Keys)
                {
                    objDiary = (WpaDiaryEntry)m_objDiariesTmp[objKey];
                    m_objDiaries.Add(objDiary.EntryId, objDiary);
                }

                // Delete diaries as needed
                foreach (int objKey in m_objDiariesToDelete.Keys)
                {
                    objDiary = (WpaDiaryEntry)m_objDiariesToDelete[objKey];
                    objDiary.Delete();
                }
                // Save batches
                m_objBatchesTmp.Clear();
                foreach (int objKey in m_objBatches.Keys)
                {
                    objBatch = (BillXBatch)m_objBatches[objKey];

                    if (m_arrlstDeletedBatches.Contains(objBatch.BatchRowid))
                        objBatch.Delete();
                    else
                    {
                        objBatch.Save();
                        m_objBatchesTmp.Add(objBatch.BatchRowid, objBatch);
                    }
                }

                m_objBatches.Clear();
                foreach (int objKey in m_objBatchesTmp.Keys)
                {
                    objBatch = (BillXBatch)m_objBatchesTmp[objKey];
                    m_objBatches.Add(objBatch.BatchRowid, objBatch);
                }


                // Save invoices
                m_objInvoicesTmp.Clear();
                foreach (int objKey in m_objInvoices.Keys)
                {
                    objInvoice = (BillXInvoice)m_objInvoices[objKey];
                    objInvoice.Save();
                    m_objInvoicesTmp.Add(objInvoice.InvoiceRowid, objInvoice);
                }

                m_objInvoices.Clear();
                foreach (int objKey in m_objInvoicesTmp.Keys)
                {
                    objInvoice = (BillXInvoice)m_objInvoicesTmp[objKey];
                    m_objInvoices.Add(objInvoice.InvoiceRowid, objInvoice);
                }

                // Save notices
                m_objNoticesTmp.Clear();
                foreach (int objKey in m_objNotices.Keys)
                {
                    objNotice = (BillXNotice)m_objNotices[objKey];
                    objNotice.Save();
                    m_objNoticesTmp.Add(objNotice.NoticeRowid, objNotice);
                }

                m_objNotices.Clear();
                foreach (int objKey in m_objNoticesTmp.Keys)
                {
                    objNotice = (BillXNotice)m_objNoticesTmp[objKey];
                    m_objNotices.Add(objNotice.NoticeRowid, objNotice);
                }

                objTrans.Commit();
            }
            catch (RMAppException p_objEx)
            {
                if (objTrans != null)
                    objTrans.Rollback();
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                if (objTrans != null)
                    objTrans.Rollback();
                throw new RMAppException(Globalization.GetString("BillingMaster.Save.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objTrans != null)
                {
                    objTrans.Dispose();
                    objTrans = null;
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                    objConn = null;
                }
                if (objPolicy != null)
                {
                    objPolicy.Dispose();
                    objPolicy = null;
                }
                if (objBillItem != null)
                {
                    objBillItem.Dispose();
                    objBillItem = null;
                }
                if (objInstallment != null)
                {
                    objInstallment.Dispose();
                    objInstallment = null;
                }
                if (objDiary != null)
                {
                    objDiary.Dispose();
                    objDiary = null;
                }
                if (objBatch != null)
                {
                    objBatch.Dispose();
                    objBatch = null;
                }
                if (objInvoice != null)
                {
                    objInvoice.Dispose();
                    objInvoice = null;
                }
                if (objNotice != null)
                {
                    objNotice.Dispose();
                    objNotice = null;
                }
            }
        }
        // End Naresh 6/18/2007 Rollbacked the Changes for Saving Enhanced Policy Object

        /// <summary>
        /// Deletes the billing Item.
        /// </summary>
        /// <param name="p_iBillingItemRowid">Billing Item Rowid</param>
		public void DeleteBillingItem( int p_iBillingItemRowid )
		{
			if( !m_arrlstDeletedBillingItems.Contains( p_iBillingItemRowid ) )
				m_arrlstDeletedBillingItems.Add( p_iBillingItemRowid );
		}

        /// <summary>
        /// Checks if the billing item is deleted or not.
        /// </summary>
        /// <param name="p_iBillingItemRowid"></param>
        /// <returns>true/false</returns>
		public bool IsDeletedBillItem( int p_iBillingItemRowid )
		{
			return( m_arrlstDeletedBillingItems.Contains( p_iBillingItemRowid ) );
		}

        /// <summary>
        /// Deletes the installment
        /// </summary>
        /// <param name="p_iInstallmentRowid">Installment Rowid</param>
		public void DeleteInstallment( int p_iInstallmentRowid )
		{
			if( !m_arrlstDeletedInstallments.Contains( p_iInstallmentRowid ) )
				m_arrlstDeletedInstallments.Add( p_iInstallmentRowid );
		}

        /// <summary>
        /// Checks if the installment is deleted or not.
        /// </summary>
        /// <param name="p_iInstallmentRowid">Installment's rowid</param>
        /// <returns>true/false</returns>
		public bool IsDeletedInstallment( int p_iInstallmentRowid )
		{
			return( m_arrlstDeletedInstallments.Contains( p_iInstallmentRowid ) );
		}
        /// <summary>
        /// Deletes the batch
        /// </summary>
        /// <param name="p_iBatchRowid">Batch Rowid</param>
		public void DeleteBatch( int p_iBatchRowid )
		{
			if( !m_arrlstDeletedBatches.Contains( p_iBatchRowid ) )
				m_arrlstDeletedBatches.Add( p_iBatchRowid );
		}

        /// <summary>
        /// Checks if the batch is deleted or not.
        /// </summary>
        /// <param name="p_iBatchRowid">Batch Rowid</param>
        /// <returns>true/false</returns>
		public bool IsDeletedBatche( int p_iBatchRowid )
		{
			return( m_arrlstDeletedBatches.Contains( p_iBatchRowid ) );
		}
	}
}
