using System;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: BillingRule.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	/// <summary>
	/// TODO
	/// </summary>
	public class BillingRule
	{
		#region Member Variables
		/// <summary>
		/// Private variable for DttmRcdLastUpd.
		/// </summary>
		private string m_sDttmRcdLastUpd = string.Empty ;
		/// <summary>
		/// Private variable for UpdatedByUser.
		/// </summary>
		private string m_sUpdatedByUser = string.Empty ;
		/// <summary>
		/// Private variable for DttmRcdAdded.
		/// </summary>
		private string m_sDttmRcdAdded = string.Empty ;
		/// <summary>
		/// Private variable for AddedByUser.
		/// </summary>
		private string m_sAddedByUser = string.Empty ;
		/// <summary>
		/// Private variable for FirstNoticeDays.
		/// </summary>
		private string m_sFirstNoticeDays = string.Empty ;
		/// <summary>
		/// Private variable for SecondNoticeDays.
		/// </summary>
		private string m_sSecondNoticeDays = string.Empty ;
		/// <summary>
		/// Private variable for ThirdNoticeDays.
		/// </summary>
		private string m_sThirdNoticeDays = string.Empty ;
		/// <summary>
		/// Private variable for FourthNoticeDays.
		/// </summary>
		private string m_sFourthNoticeDays = string.Empty ;
		/// <summary>
		/// Private variable forNoticeOffsetInd.
		/// </summary>
		private int m_iNoticeOffsetInd = 0 ;
		/// <summary>
		/// Private variable for BillingRuleRowid.
		/// </summary>
		private int m_iBillingRuleRowid = 0 ;
		/// <summary>
		/// Private variable for BillingRuleCode.
		/// </summary>
		private int m_iBillingRuleCode = 0 ;
		/// <summary>
		/// Private variable for MaxNumRebills.
		/// </summary>
		private int m_iMaxNumRebills = 0 ;
		/// <summary>
		/// Private variable for DueDays.
		/// </summary>
		private int m_iDueDays = 0 ;
		/// <summary>
		/// Private variable for OutstandBalIndic.
		/// </summary>
		private int m_iOutstandBalIndic = 0 ;
		/// <summary>
		/// Private variable for InUseFlag.
		/// </summary>
		private int m_iInUseFlag = 0 ;
		/// <summary>
		/// Private variable for NonPayInd.
		/// </summary>
		private int m_iNonPayInd = 0 ;
		/// <summary>
		/// Private variable for CancelPendDiary.
		/// </summary>
		private int m_iCancelPendDiary = 0 ;
		/// <summary>
		/// Private variable for CancelDiary.
		/// </summary>
		private int m_iCancelDiary = 0 ;
		/// <summary>
		/// Private variable for NewRecord.
		/// </summary>
		private bool m_bNewRecord = false ;
		/// <summary>
		/// Private variable for DataChanged.
		/// </summary>
		private bool m_bDataChanged = false ;
		#endregion 

		#region Member Properties
		/// <summary>
		/// Read/Write property for DttmRcdLastUpd.
		/// </summary>
		public string DttmRcdLastUpd 
		{
			get
			{
				return m_sDttmRcdLastUpd;
			}
			set
			{
				m_sDttmRcdLastUpd = value ;
			}
		}

		/// <summary>
		/// Read/Write property for UpdatedByUser.
		/// </summary>
		public string UpdatedByUser 
		{
			get
			{
				return m_sUpdatedByUser;
			}
			set
			{
				m_sUpdatedByUser = value ;
			}
		}
		/// <summary>
		/// Read/Write property for DttmRcdAdded.
		/// </summary>
		public string DttmRcdAdded 
		{
			get
			{
				return m_sDttmRcdAdded;
			}
			set
			{
				m_sDttmRcdAdded = value ;
			}
		}

		/// <summary>
		/// Read/Write property for AddedByUser.
		/// </summary>
		public string AddedByUser 
		{
			get
			{
				return m_sAddedByUser;
			}
			set
			{
				m_sAddedByUser = value ;
			}
		}

		/// <summary>
		/// Read/Write property for FirstNoticeDays.
		/// </summary>
		public string FirstNoticeDays 
		{
			get
			{
				return m_sFirstNoticeDays;
			}
			set
			{
				m_sFirstNoticeDays = value ;
			}
		}

		/// <summary>
		/// Read/Write property for SecondNoticeDays.
		/// </summary>
		public string SecondNoticeDays 
		{
			get
			{
				return m_sSecondNoticeDays;
			}
			set
			{
				m_sSecondNoticeDays = value ;
			}
		}

		/// <summary>
		/// Read/Write property for ThirdNoticeDays.
		/// </summary>
		public string ThirdNoticeDays 
		{
			get
			{
				return m_sThirdNoticeDays;
			}
			set
			{
				m_sThirdNoticeDays = value ;
			}
		}

		/// <summary>
		/// Read/Write property for FourthNoticeDays.
		/// </summary>
		public string FourthNoticeDays 
		{
			get
			{
				return m_sFourthNoticeDays;
			}
			set
			{
				m_sFourthNoticeDays = value ;
			}
		}

		/// <summary>
		/// Read/Write property for NoticeOffsetInd.
		/// </summary>
		public int NoticeOffsetInd 
		{
			get
			{
				return m_iNoticeOffsetInd;
			}
			set
			{
				m_iNoticeOffsetInd = value ;
			}
		}

		/// <summary>
		/// Read/Write property for BillingRuleRowid.
		/// </summary>
		public int BillingRuleRowid 
		{
			get
			{
				return m_iBillingRuleRowid;
			}
			set
			{
				m_iBillingRuleRowid = value ;
			}
		}

		/// <summary>
		/// Read/Write property for BillingRuleCode.
		/// </summary>
		public int BillingRuleCode 
		{
			get
			{
				return m_iBillingRuleCode;
			}
			set
			{
				m_iBillingRuleCode = value ;
			}
		}
		/// <summary>
		/// Read/Write property for MaxNumRebills.
		/// </summary>
		public int MaxNumRebills 
		{
			get
			{
				return m_iMaxNumRebills;
			}
			set
			{
				m_iMaxNumRebills = value ;
			}
		}

		/// <summary>
		/// Read/Write property for DueDays.
		/// </summary>
		public int DueDays 
		{
			get
			{
				return m_iDueDays;
			}
			set
			{
				m_iDueDays = value ;
			}
		}

		/// <summary>
		/// Read/Write property for OutstandBalIndic.
		/// </summary>
		public int OutstandBalIndic 
		{
			get
			{
				return m_iOutstandBalIndic;
			}
			set
			{
				m_iOutstandBalIndic = value ;
			}
		}
		/// <summary>
		/// Read/Write property for InUseFlag.
		/// </summary>
		public int InUseFlag 
		{
			get
			{
				return m_iInUseFlag;
			}
			set
			{
				m_iInUseFlag = value ;
			}
		}
		/// <summary>
		/// Read/Write property for NonPayInd.
		/// </summary>
		public int NonPayInd 
		{
			get
			{
				return m_iNonPayInd;
			}
			set
			{
				m_iNonPayInd = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CancelPendDiary.
		/// </summary>
		public int CancelPendDiary 
		{
			get
			{
				return m_iCancelPendDiary;
			}
			set
			{
				m_iCancelPendDiary = value ;
			}
		}
		/// <summary>
		/// Read/Write property for CancelDiary.
		/// </summary>
		public int CancelDiary 
		{
			get
			{
				return m_iCancelDiary;
			}
			set
			{
				m_iCancelDiary = value ;
			}
		}
		/// <summary>
		/// Read/Write property for NewRecord.
		/// </summary>
		public bool NewRecord 
		{
			get
			{
				return m_bNewRecord;
			}
			set
			{
				m_bNewRecord = value ;
			}
		}
		/// <summary>
		/// Read/Write property for DataChanged.
		/// </summary>
		public bool DataChanged 
		{
			get
			{
				return m_bDataChanged;
			}
			set
			{
				m_bDataChanged = value ;
			}
		}
		#endregion 

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value.
		/// </summary>
		public BillingRule()
		{			
		}
		#endregion 
		
	}
}
