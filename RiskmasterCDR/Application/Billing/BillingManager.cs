﻿
using System;
using System.IO ;
using System.Xml ;
using System.Text ;
using System.Collections ;

using Riskmaster.Common ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.DataModel ;
using Riskmaster.Application.PrintChecks;
using Riskmaster.Security;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: BillingManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Vaibhav/Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class BillingManager : IDisposable
	{		 
		#region "Member Variables"
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty;
		/// <summary>
		/// Private variable to store the instance of Local Cache object
		/// </summary>
		private LocalCache m_objLocalCache = null ;
        private int m_iClientId = 0;
        private string m_sLangcode = string.Empty;//vkumar258 ML Changes
        UserLogin m_oUserLogin = null;
		#endregion
		
		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value.
		/// </summary>
		/// <param name="p_objDataModelFactory">Data Model Factory Object.</param>
        public BillingManager(DataModelFactory p_objDataModelFactory, int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_objDataModelFactory = p_objDataModelFactory ;
			m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString ;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
            // pending - insure that DataModelFactory object, passed to all instantiated Billingmanager objects, are created with ClientID first
            m_iClientId = p_objDataModelFactory.Context.ClientId;
		}
        public BillingManager(string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)
		{
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword,m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
           
		}

        public BillingManager(UserLogin p_oUserLogin, string p_sUserName, string p_sPassword, string p_sDsnName, int p_iClientId)//vkumar258 ML Changes
        {
            m_oUserLogin = p_oUserLogin;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModelFactory(p_sDsnName, p_sUserName, p_sPassword, m_iClientId);
            m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

        }

		#endregion

        public void Dispose()
        {
            //if (m_objDataModelFactory != null)
            //{
            //    m_objDataModelFactory.Dispose();
            //}
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }
	
		#region Reconcilation Methods
	    /// <summary>
	    /// Checks whether the check is paid in full or not.
	    /// </summary>
	    /// <param name="p_arrlstBillingItems">List of Billing Items</param>
	    /// <param name="p_arrlstAccount">List of Accounts</param>
	    /// <param name="p_arrlstInstallment">List of installments</param>
	    /// <param name="p_objBillingMaster">Object of BillingMaster Class</param>
	    /// <param name="p_iBatchId">Batch Id</param>
	    /// <returns>A bool value</returns>
		public bool CheckPaidInFull( ArrayList p_arrlstBillingItems , ArrayList p_arrlstAccount , ArrayList p_arrlstInstallment , BillingMaster p_objBillingMaster , int p_iBatchId )
		{
			BillXBillItem objBillXBillItem = null ;
			BillXAccount objBillXAccount = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			
			int iIndex = 0 ;
			int iIndexAccount = 0 ;
			int iTermNumber = 0 ;
			double dblTotalCredits = 0.0 ;
			string sPolicyStatus = string.Empty ;
			bool bReturnValue = false ;
			bool bAccountFound = false ;
			bool bInstallPremiumItemPresent = false ;
			bool bSecondInstallmentNotGenerated = false ;
			
			try
			{
				for( iIndex = 0 ; iIndex < p_arrlstBillingItems.Count ; iIndex++ )
				{
					objBillXBillItem = ( BillXBillItem ) p_arrlstBillingItems[ iIndex ] ;
					if( m_objLocalCache.GetShortCode( objBillXBillItem.Status ) == "SR" && m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P" && m_objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "IN" )
					{
						bInstallPremiumItemPresent = true ;
						break ;
					}
				}

				if( !bInstallPremiumItemPresent )
					return( false ) ;

				for( iIndex = 0 ; iIndex < p_arrlstBillingItems.Count ; iIndex++ )
				{
					objBillXBillItem = ( BillXBillItem ) p_arrlstBillingItems[ iIndex ] ;
					if( objBillXBillItem.FinalBatchNum == p_iBatchId )
					{
						// Verify that the current term is on a pay plan
						for( iIndexAccount = 0 ; iIndexAccount < p_arrlstAccount.Count ; iIndexAccount++ )
						{
							objBillXAccount = ( BillXAccount ) p_arrlstAccount[ iIndexAccount ] ;
							if( objBillXAccount.TermNumber == objBillXBillItem.TermNumber && objBillXAccount.PayPlanRowid != 0 )
							{
								// IF THE POLICY IS CANCELLED, DO NOT DO THE CHECK
                                sPolicyStatus = m_objLocalCache.GetShortCode(Function.GetPolicyStatus(objBillXAccount.PolicyId, m_sConnectionString, m_iClientId));
								if( sPolicyStatus == "C" || sPolicyStatus == "CF" || sPolicyStatus == "CPR" )
									return( false );
								p_objBillingMaster.Account = objBillXAccount ;
								bAccountFound = true ;
								iTermNumber = objBillXAccount.TermNumber ;
								break ;
							}
						}
						break;
					}
				}

				if( !bAccountFound )
					return( false );

				// Verify only the first installment has been generated
				for( iIndex = 0 ; iIndex < p_arrlstInstallment.Count ; iIndex++ )
				{
					objBillXInstlmnt = ( BillXInstlmnt ) p_arrlstInstallment[iIndex] ; 
					if( objBillXInstlmnt.TermNumber == iTermNumber && objBillXInstlmnt.InstallmentNum == 2 && objBillXInstlmnt.GeneratedFlag == false )
					{
						bSecondInstallmentNotGenerated = true ;
						break ;
					}
				}

				if( !bSecondInstallmentNotGenerated )
					return( false );

				// Sum the charges and credits on the policy
				for( iIndex = 0 ; iIndex < p_arrlstBillingItems.Count ; iIndex++ )
				{
					objBillXBillItem = ( BillXBillItem ) p_arrlstBillingItems[ iIndex ] ;
					if( objBillXBillItem.TermNumber == iTermNumber && m_objLocalCache.GetShortCode( objBillXBillItem.Status) == "SR" && objBillXBillItem.Amount < 0 )
                        //Changed for MITS 9775 by Gagan : Start
                        dblTotalCredits += objBillXBillItem.Amount;
                        //Changed for MITS 9775 by Gagan : End
				}
		
				if( dblTotalCredits < 0 )
				{
					if( Math.Abs( dblTotalCredits ) >= p_objBillingMaster.Account.AmountDue )
						bReturnValue = true ;
				}
				return( bReturnValue );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.CheckPaidInFull.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
				if( objBillXAccount != null )
				{
					objBillXAccount.Dispose();
					objBillXAccount = null ;
				}
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}				
			}
		}
        /// <summary>
        /// Cancels pay plan if premium paid in full.
        /// </summary>
        /// <param name="p_arrlstBillingItems">List of billing items</param>
        /// <param name="p_arrlstInstallment">List of installments</param>
        /// <param name="p_objBillingMaster">Object of BillingMaster Class</param>
        /// <param name="p_iBatchId">Batch Id</param>
        public void CancelPayPlanPaid( ArrayList p_arrlstBillingItems , ArrayList p_arrlstInstallment , BillingMaster p_objBillingMaster , int p_iBatchId )
		{
			BillXBillItem objBillXBillItem = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			PolicyEnh objPolicyEnh = null ;
			DbReader objReader = null ;
			WpaDiaryEntry objWpaDiaryEntry = null ;
			DiaryManager objDiaryManager = null ;
			
			int iInstallRowid = 0 ;
			int iIndex = 0 ;
			int iIndexInstall = 0 ;
			int iTransactionId = 0 ;
			bool bInvoiceNotPrinted = false ;
			string sSQL = string.Empty ;

			try
			{
				objDiaryManager = new DiaryManager( m_objDataModelFactory,m_iClientId );
				
				for( iIndexInstall = 0 ; iIndexInstall < p_arrlstInstallment.Count ; iIndexInstall++ )
				{
					objBillXInstlmnt = ( BillXInstlmnt ) p_arrlstInstallment[iIndexInstall] ; 
					if( objBillXInstlmnt.TermNumber == p_objBillingMaster.Account.TermNumber && objBillXInstlmnt.InstallmentNum == 2 && objBillXInstlmnt.GeneratedFlag == false )
					{
						iInstallRowid = objBillXInstlmnt.InstallmentRowid ;
						break ;
					}
				}

				// Find the first installment premium item
				for( iIndex = 0 ; iIndex < p_arrlstBillingItems.Count ; iIndex++ )
				{
					objBillXBillItem = ( BillXBillItem ) p_arrlstBillingItems[ iIndex ] ;
					if( objBillXBillItem.TermNumber == p_objBillingMaster.Account.TermNumber && m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P" )
					{
						if( m_objLocalCache.GetShortCode( objBillXBillItem.BillXPremItem.PremiumItemType ) == "IN" && m_objLocalCache.GetShortCode( objBillXBillItem.Status ) == "SR" )
						{
							p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
							p_objBillingMaster.DeleteBillingItem( objBillXBillItem.BillingItemRowid );
							p_objBillingMaster.Account.PayPlanRowid = 0 ;
							objPolicyEnh = ( PolicyEnh ) m_objDataModelFactory.GetDataModelObject( "PolicyEnh" , false );
							objPolicyEnh.MoveTo( p_objBillingMaster.Account.PolicyId );
                            //Changed for MITS 9775 by Gagan : Start
                            p_objBillingMaster.Policies.Add(objPolicyEnh.PolicyId, objPolicyEnh);
                            //Changed for MITS 9775 by Gagan : End
							
							// Grab all the installments and delete them
							for( iIndexInstall = 0 ; iIndexInstall < p_arrlstInstallment.Count ; iIndexInstall++ )
							{
								objBillXInstlmnt = ( BillXInstlmnt ) p_arrlstInstallment[iIndexInstall] ; 
								if( objBillXInstlmnt.TermNumber == p_objBillingMaster.Account.TermNumber )
								{
									p_objBillingMaster.Installments.Add( objBillXInstlmnt );
									p_objBillingMaster.DeleteInstallment( objBillXInstlmnt.InstallmentRowid ); 
								}
							}

							// Find the associated installment diary and mark for delete
							sSQL = "SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE ATTACH_RECORDID = " + iInstallRowid + " AND ATTACH_TABLE = 'BILL_X_INSTLMNT' " ;
							objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
							if( objReader.Read() )
							{
								objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
								objWpaDiaryEntry.MoveTo( Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId ) );
								p_objBillingMaster.DiariesToDelete.Add( objWpaDiaryEntry.EntryId , objWpaDiaryEntry ); 
							}
							objReader.Close();	

							// If the invoice hasn't printed related to this one, then delete and reschedule
							sSQL = " SELECT ENTRY_ID FROM WPA_DIARY_ENTRY WHERE ATTACH_RECORDID = " + objBillXBillItem.PolicyId + " AND ATT_SEC_REC_ID = " + objBillXBillItem.BillingItemRowid
								+	" AND ENTRY_NAME = 'Print Invoice' AND STATUS_OPEN <>0 AND DIARY_DELETED =0 AND DIARY_VOID=0" ;
							objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
							if( objReader.Read() )
							{
								bInvoiceNotPrinted = true ;
								objWpaDiaryEntry = ( WpaDiaryEntry ) m_objDataModelFactory.GetDataModelObject( "WpaDiaryEntry" , false );
								objWpaDiaryEntry.MoveTo( Common.Conversion.ConvertObjToInt( objReader.GetValue( "ENTRY_ID" ), m_iClientId ) );
								p_objBillingMaster.DiariesToDelete.Add( objWpaDiaryEntry.EntryId , objWpaDiaryEntry ); 
							}
							objReader.Close();	

							objDiaryManager.DeleteCancelDiaries( objBillXBillItem.PolicyId , objBillXBillItem.BillingItemRowid, p_objBillingMaster );
							objDiaryManager.DeleteCancelPendingDiaries( objBillXBillItem.PolicyId , objBillXBillItem.BillingItemRowid, p_objBillingMaster );
							objDiaryManager.DeleteNoticeDiaries( objBillXBillItem.PolicyId , objBillXBillItem.BillingItemRowid, p_objBillingMaster );

							foreach( PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList )
							{
								if( objPolicyXTransEnh.TermNumber == p_objBillingMaster.Account.TermNumber && ( m_objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) == "NB" || m_objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) == "RN" ) )
								{
									iTransactionId = objPolicyXTransEnh.TransactionId ;
									this.CreateChargeItemPolicy( p_objBillingMaster, iTransactionId , m_objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) , 0.0 , true );
									break;
								}
							}

							if( bInvoiceNotPrinted )
							{
								objDiaryManager.ScheduleInvoiceDiaries( null , p_objBillingMaster , true );
							}
						}
					}
				}

				foreach( int iKey in p_objBillingMaster.BillingItems.Keys )
				{
					objBillXBillItem = ( BillXBillItem ) p_objBillingMaster.BillingItems[iKey] ;					
					if( m_objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P"  && ( ! p_objBillingMaster.IsDeletedBillItem( objBillXBillItem.BillingItemRowid ) ) )					
					{
						objBillXBillItem.FinalBatchNum = p_iBatchId ;
						objBillXBillItem.FinalBatchType = m_objLocalCache.GetCodeId( "R", "BATCH_TYPES" ) ;
						objBillXBillItem.Status = m_objLocalCache.GetCodeId( "SR", "BILLING_ITEM_STAT" );
					}
				}	

				p_objBillingMaster.Save();
                //Changed for MITS 9775 by Gagan : Start
                //Clearing Billing Items as they are added in Reconcile
                p_objBillingMaster.BillingItems.Clear();
                //Changed for MITS 9775 by Gagan : End
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.CancelPayPlanPaid.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}
                if (objPolicyEnh != null)
                {
                    objPolicyEnh.Dispose();
                    objPolicyEnh = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objWpaDiaryEntry != null)
                {
                    objWpaDiaryEntry.Dispose();
                }
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                }
			}	
		}
        # region Post Disbursement Print
        /// Name		: PostPrintDisb
        /// Author		: Shruti Choudhary
        /// Date Created: 07/02/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Updates the check printed status for disbursement from billing screen.
        /// </summary>
        /// <param name="p_objInputDoc"></param>
        public XmlDocument PostPrintDisb(XmlDocument p_objInputDoc)
        {
            string sSQL = string.Empty;
            CheckSetup objCheckSetup = null;
            DbReader objReader = null;
            DbConnection objDbConnection = null;
            XmlDocument objXmlOut = new XmlDocument();
            int iTransId = 0;
            int iSelBillingItem = 0;
            string sSource = string.Empty;
            int iPrinted = 0;
            //int transNum = -1;
            long transNum = -1;   //pmittal5 Mits 14500 03/02/09 - TRANS_NUMBER is of long type 
            string sDtofCheck = string.Empty;
            try
            {

                iTransId = Conversion.ConvertStrToInteger(p_objInputDoc.SelectSingleNode("//TransId").InnerText);
                iSelBillingItem = Conversion.ConvertStrToInteger(p_objInputDoc.SelectSingleNode("//SelDisbursementRowID").InnerText);

                using (objCheckSetup = new CheckSetup(m_objDataModelFactory.Context.RMUser.objRiskmasterDatabase.DataSourceName, m_objDataModelFactory.Context.RMUser.LoginName, m_objDataModelFactory.Context.RMUser.Password, m_objDataModelFactory.Context.RMUser.UserId, m_objDataModelFactory.Context.RMUser.GroupId, m_iClientId))
                {
                    objXmlOut = objCheckSetup.UpdateStatusForPrintedChecks(p_objInputDoc);
                }

                sSQL = "SELECT TRANS_NUMBER, DATE_OF_CHECK, STATUS_CODE FROM FUNDS WHERE TRANS_ID = " + iTransId.ToString();
                objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if (Conversion.ConvertObjToInt(objReader.GetValue("STATUS_CODE"), m_iClientId) == 1054)
                        {
                            iPrinted = -1;
                        }
                        //pmittal5 Mits 14500 03/02/09 - TRANS_NUMBER is of long type 
                        //transNum = Conversion.ConvertObjToInt(objReader.GetValue("TRANS_NUMBER"), m_iClientId);
                        transNum = Conversion.ConvertObjToInt64(objReader.GetValue("TRANS_NUMBER"), m_iClientId);
                        sDtofCheck =Conversion.GetUIDate(objReader.GetString("DATE_OF_CHECK"),m_oUserLogin.objUser.NlsCode.ToString(),m_iClientId);//vkumar258 ML Changes
                    }
                    objReader.Close();
                    objDbConnection.Close();
                }

                sSQL = "UPDATE BILL_X_DSBRSMNT SET FUND_TRAN_ID=" + iTransId.ToString() + ", CHECK_NUMBER=" + transNum.ToString() + ", DATE_PRINTED=" + Utilities.FormatSqlFieldValue(sDtofCheck) + ", PRINTED_FLAG=" + iPrinted.ToString() + " WHERE DISBURSEMENT_ROWID=" + iSelBillingItem.ToString();
                objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();

                objDbConnection.ExecuteNonQuery(sSQL);
                objDbConnection.Close();

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BillingManager.PostPrintDisb.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }
                if (objCheckSetup != null)
                {
                    objCheckSetup.Dispose();
                }
            }
            return objXmlOut;
        }
        # endregion

        # region PrintDisbursement
        /// Name		: PrintDisbursement
        /// Author		: Shruti Choudhary
        /// Date Created: 07/02/2007		
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// Sends the required data to the ui to be displayed on print check dialog.
        /// </summary>
        /// <param name="BillingItemID"></param>
        /// <returns>An xml document containg the data required to be printed on the print dialog.</returns>
        public XmlDocument PrintDisbursement(string BillingItemID)
        {
            BillXBillItem objBillXBillItem = null;
            XmlElement objRootElement = null;
            XmlElement objChildElement = null;
            XmlDocument objXmlDom = null;
            string sSQL = string.Empty;
            int iBillItemRowId;
            Funds objFunds = null;
            FundsTransSplit objTS = null;
            DbReader objReader = null;
            DbConnection objDbConnection = null;

            string sFName = string.Empty;
            string sLName = string.Empty;
            string sAddr1 = string.Empty;
            string sAddr2 = string.Empty;
            string sCity = string.Empty;
            int iStateID = 0;
            string sZip = string.Empty;
            string sAccountName = string.Empty;

            try
            {
                iBillItemRowId = Conversion.ConvertObjToInt(BillingItemID, m_iClientId);
                objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                objBillXBillItem.MoveTo(iBillItemRowId);
                if (objBillXBillItem.BillXDsbrsmnt.PrintedFlag)
                {
                    throw new RMAppException(Globalization.GetString("BillingManager.PrintDisbursement.AlreadyBeenPrinted", m_iClientId));
                }

                sSQL = "SELECT * FROM ENTITY WHERE ENTITY_ID = " + objBillXBillItem.BillXDsbrsmnt.Payee;
                objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sFName = objReader.GetString("FIRST_NAME");
                        sLName = objReader.GetString("LAST_NAME");
                        sAddr1 = objReader.GetString("ADDR1");
                        sAddr2 = objReader.GetString("ADDR2");
                        sCity = objReader.GetString("CITY");
                        iStateID = Conversion.ConvertObjToInt(objReader.GetValue("STATE_ID"), m_iClientId);
                        sZip = objReader.GetString("ZIP_CODE");
                    }
                    objReader.Close();
                    objDbConnection.Close();
                }

                objFunds = m_objDataModelFactory.GetDataModelObject("Funds", false) as Funds;
                objFunds.ClaimId = 0;
                objFunds.UnitId = 0;
                objFunds.FirstName = sFName;
                objFunds.LastName = sLName;
                objFunds.Addr1 = sAddr1;
                objFunds.Addr2 = sAddr2;
                objFunds.City = sCity;
                objFunds.StateId = iStateID;
                objFunds.ZipCode = sZip;
                objFunds.CheckMemo = "Disbursement Check";
                objFunds.PayeeEid = objBillXBillItem.BillXDsbrsmnt.Payee;
                objFunds.PayeeTypeCode = m_objLocalCache.GetCodeId("O", "PAYEE_TYPE");
                objFunds.EnclosureFlag = false;
                objFunds.SettlementFlag = false;
                objFunds.VoucherFlag = false;
                //objFunds.TransId = objBillXBillItem.BillXDsbrsmnt.FundTranId;
                //objFunds.CtlNumber = 
                objFunds.Amount = objBillXBillItem.Amount;
                objFunds.AccountId = objBillXBillItem.BillXDsbrsmnt.BankAcctCode;
                objFunds.SubAccountId = objBillXBillItem.BillXDsbrsmnt.SubAcctCode;
                objFunds.DateOfCheck = Conversion.ToDbDate(DateTime.Now);
                objFunds.TransDate = Conversion.ToDbDate(DateTime.Now);
                objFunds.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
                objFunds.AddedByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                objFunds.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                objFunds.UpdatedByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                objFunds.AutoCheckFlag = false;
                objFunds.PrecheckFlag = false;
                objFunds.VoidFlag = false;
                objFunds.ClearedFlag = false;
                objFunds.PaymentFlag = true;
                objFunds.CollectionFlag = false;
                objFunds.StatusCode = m_objLocalCache.GetCodeId("R", "CHECK_STATUS");

                objTS = objFunds.TransSplitList.AddNew();

                objTS.TransTypeCode = objBillXBillItem.BillXDsbrsmnt.TransactionType;
                objTS.ReserveTypeCode = 0;
                objTS.Amount = objBillXBillItem.Amount;
                objTS.DttmRcdAdded = Conversion.ToDbDateTime(DateTime.Now);
                objTS.AddedByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                objTS.DttmRcdLastUpd = Conversion.ToDbDateTime(DateTime.Now);
                objTS.UpdatedByUser = m_objDataModelFactory.Context.RMUser.LoginName;
                objTS.FromDate = objBillXBillItem.DateReconciled;
                objTS.ToDate = objBillXBillItem.DateReconciled;
                objTS.SumAmount = objBillXBillItem.Amount;
                objTS.InvoiceAmount = objBillXBillItem.Amount;

                objFunds.Save();

                sSQL = "SELECT ACCOUNT_NAME FROM ACCOUNT WHERE ACCOUNT_ID = " + objBillXBillItem.BillXDsbrsmnt.BankAcctCode;
                objDbConnection = DbFactory.GetDbConnection(m_sConnectionString);
                if (objDbConnection != null)
                    objDbConnection.Open();

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        sAccountName = objReader.GetString("ACCOUNT_NAME");
                    }
                    objReader.Close();
                    objDbConnection.Close();
                }

                objXmlDom = new XmlDocument();
                objRootElement = objXmlDom.CreateElement("CheckDetails");
                objChildElement = objXmlDom.CreateElement("TransID");
                objChildElement.InnerText = objFunds.TransId.ToString();
                objRootElement.AppendChild(objChildElement);
                objChildElement = objXmlDom.CreateElement("Amount");
                objChildElement.InnerText = objBillXBillItem.Amount.ToString();
                objRootElement.AppendChild(objChildElement);
                objChildElement = objXmlDom.CreateElement("Account");
                objChildElement.InnerText = sAccountName;
                objRootElement.AppendChild(objChildElement);
                objChildElement = objXmlDom.CreateElement("Source");
                objChildElement.InnerText = "Disbursement";
                objRootElement.AppendChild(objChildElement);
                objChildElement = objXmlDom.CreateElement("SelBillingRowID");
                objChildElement.InnerText = objBillXBillItem.BillXDsbrsmnt.DisbursementRowid.ToString();
                objRootElement.AppendChild(objChildElement);
                objXmlDom.AppendChild(objRootElement);

                return objXmlDom;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BillingManager.PrintDisbursement.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objXmlDom = null;
                objChildElement = null;
                objRootElement = null;
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objDbConnection != null)
                {
                    objDbConnection.Close();
                    objDbConnection.Dispose();
                }
                if (objFunds != null)
                {
                    objFunds.Dispose();
                }
                if (objTS != null)
                {
                    objTS.Dispose();
                }
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                }
            }


        }
        # endregion

        /// <summary>
        /// Gets the reconcilliation details.
        /// </summary>
        /// <param name="p_arrlstBillingItemIDs">List of Billing Items</param>
        /// <param name="bIsCheckPaidInFull">True/ False whether customer has paid premium in full</param>
        /// 
        /// <returns>Batch total amount</returns>                
        public string GetReconciliationDetails(string[]  p_arrlstBillingItemIDs, ref bool bIsCheckPaidInFull)
        {
            ArrayList arrlstBillItems = new ArrayList();
            ArrayList arrlstSelectedBillItems = new ArrayList();
            BillXBillItem objBillXBillItem = null;
            BillXBatch objBatch = null;
            //Changed for MITS 9775 by Gagan : Start
            BatchManager objBatchManager = new BatchManager(m_objDataModelFactory, m_iClientId);
            //Changed for MITS 9775 by Gagan : End
            string sSQL = string.Empty;
            int iBatchNumber=0;
            int iPolicyId = 0;
            int iBillItemRowId;
            int iCount = 0;
            string sBatchTotal=string.Empty;
            string sBillingItemStatus= string.Empty;
            try
            {
                foreach (string sBillingRowID in p_arrlstBillingItemIDs)
                {
                    iBillItemRowId = Conversion.ConvertObjToInt(sBillingRowID, m_iClientId);
                    objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                    objBillXBillItem.MoveTo(iBillItemRowId);
                    arrlstSelectedBillItems.Add(objBillXBillItem);
                    iBatchNumber = objBillXBillItem.FinalBatchNum;
                    iPolicyId = objBillXBillItem.PolicyId;
                    objBillXBillItem = null;
                }
                sSQL = "Select * from BILL_X_BILL_ITEM where POLICY_ID = " + iPolicyId;
                FillBillingItemLoop(arrlstBillItems, sSQL, true);

                foreach (BillXBillItem objbillItem in arrlstBillItems)
                {
                    sBillingItemStatus = m_objLocalCache.GetShortCode(objbillItem.Status);
                    if (sBillingItemStatus == "SR")
                    {
                        iCount++;
                    }
                }
                if (iCount == 1)
                {
                    throw new RMAppException(Globalization.GetString("BillingManager.GetReconciliationDetails.JustOneToReconcile", m_iClientId));
                }
                foreach (BillXBillItem objbillItem in arrlstSelectedBillItems)
                {
                    sBillingItemStatus = m_objLocalCache.GetShortCode(objbillItem.Status);
                    if (sBillingItemStatus == "OK")
                        throw new RMAppException(Globalization.GetString("BillingManager.GetReconciliationDetails.NotSelected", m_iClientId));
                    if (sBillingItemStatus == "RC")
                        throw new RMAppException(Globalization.GetString("BillingManager.GetReconciliationDetails.AlreadyReconciled", m_iClientId));
                }


                objBatch = (BillXBatch)m_objDataModelFactory.GetDataModelObject("BillXBatch", false);
                sSQL = "SELECT * FROM BILL_X_BATCH WHERE BATCH_ROWID=" + iBatchNumber;
                FillBatch(objBatch, sSQL);

                sBatchTotal = Conversion.ConvertObjToStr(objBatch.BatchAmount);

                //Changed for MITS 9775 by Gagan : Start               
                //Checks whether premium has been paid in full
                bIsCheckPaidInFull = objBatchManager.IsCheckPaidInFull(iBatchNumber);
                //Changed for MITS 9775 by Gagan : End
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BillingManager.GetReconciliationDetails.Error", m_iClientId), p_objEx);
            }
            finally
            {
                arrlstBillItems = null;
                arrlstSelectedBillItems = null;
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                }
                if (objBatch != null)
                {
                    objBatch.Dispose();
                }
                if (objBatchManager != null)
                {
                    objBatchManager.Dispose();
                }
            }
            return sBatchTotal;
        }
        /// <summary>
        /// Reconciles the selected billing items.
        /// </summary>
        /// <param name="p_arrlstBillingItems">List of billing items</param>
        /// <param name="p_iType">Reconcilliation type</param>
        /// <param name="p_objBillingMaster">Object of BillingMaster Class</param>
		public void Reconcile( ArrayList p_arrlstBillingItems , int p_iType , BillingMaster p_objBillingMaster )
		{
			BillXBatch objBillXBatch = null ;
			BillXBillItem objBillXBillItem = null ;
			DiaryManager objDiaryManager = null ;

			double dblBatchTotal = 0.0 ;
			string sPolicyNumber = string.Empty ;
			int iTermNumber = 0 ;

			try
			{
                objDiaryManager = new DiaryManager(m_objDataModelFactory, m_iClientId);

				// Getting the first Item in the HashCollection.
				// The collection will have only one item. 
				foreach( int iKey in p_objBillingMaster.Batches.Keys )
				{
					objBillXBatch = ( BillXBatch ) p_objBillingMaster.Batches[iKey] ;
					break ;
				}
				
				// Loop through all transactions in the batch and mark them as reconciled
				foreach( BillXBillItem objBillXBillItemTemp in p_arrlstBillingItems )
				{
					if( objBillXBillItemTemp.FinalBatchNum == objBillXBatch.BatchRowid && m_objLocalCache.GetShortCode( objBillXBillItemTemp.Status ) == "SR" )
					{
						objBillXBillItemTemp.Status = m_objLocalCache.GetCodeId( "RC" , "BILLING_ITEM_STAT" );
						objBillXBillItemTemp.DateReconciled = Conversion.GetDate( DateTime.Now.ToString( "d" ) ); 
						dblBatchTotal += objBillXBillItemTemp.Amount ;
						sPolicyNumber = objBillXBillItemTemp.PolicyNumber ;
						iTermNumber = objBillXBillItemTemp.TermNumber ;
						p_objBillingMaster.BillingItems.Add( objBillXBillItemTemp.BillingItemRowid , objBillXBillItemTemp );
						
						// DELETE ANY CANCELLATION PROCESSING
						if( m_objLocalCache.GetShortCode( objBillXBillItemTemp.BillingItemType ) == "P" )
						{
							objDiaryManager.DeleteCancelDiaries( objBillXBillItemTemp.PolicyId, objBillXBillItemTemp.BillingItemRowid, p_objBillingMaster );
							objDiaryManager.DeleteCancelPendingDiaries( objBillXBillItemTemp.PolicyId, objBillXBillItemTemp.BillingItemRowid, p_objBillingMaster );
							objDiaryManager.DeleteNoticeDiaries( objBillXBillItemTemp.PolicyId, objBillXBillItemTemp.BillingItemRowid, p_objBillingMaster );
							objDiaryManager.DeleteCancelRebillDiaries( objBillXBillItemTemp.PolicyId, objBillXBillItemTemp.BillingItemRowid, p_objBillingMaster );
							objDiaryManager.DeleteUnpaidBalanceDiary( objBillXBillItemTemp.PolicyId, objBillXBillItemTemp.BillingItemRowid, p_objBillingMaster );
						}
					}
				}

				objBillXBatch.BatchAmount = dblBatchTotal ;

				if( objBillXBatch.BatchAmount != 0 )
				{
					switch( m_objLocalCache.GetShortCode( p_iType ) )
					{
							// Reconcile with balance
						case "B" : 
							if( objBillXBatch.BatchAmount > 0 )
							{
								// Create underpayment txn
								objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
								this.CreateAdjustment( objBillXBillItem, objBillXBatch , true , m_objLocalCache.GetCodeId( "UP", "ADJUSTMENT_TYPES") , sPolicyNumber, iTermNumber );
								objBillXBillItem.Status = m_objLocalCache.GetCodeId( "OK", "BILLING_ITEM_STAT" ) ;
                                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
								p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
								objBillXBatch.BatchCount++ ;
								
								// DO SCHEDULING FOR THIS ITEM
								// create contra item
								objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
								this.CreateAdjustment( objBillXBillItem, objBillXBatch , false , m_objLocalCache.GetCodeId( "BI", "ADJUSTMENT_TYPES") , sPolicyNumber, iTermNumber );
								objBillXBillItem.FinalBatchNum = objBillXBatch.BatchRowid ;
								objBillXBillItem.FinalBatchType = objBillXBatch.BatchType ;
								objBillXBillItem.Status = m_objLocalCache.GetCodeId( "RC", "BILLING_ITEM_STAT" ) ;
								objBillXBillItem.DateReconciled = Conversion.GetDate( DateTime.Now.ToString( "d" ) );
                                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
								p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
								objBillXBatch.BatchCount++ ;
							}
							else
							{
								// Create overpayment txn
								objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
								this.CreateAdjustment( objBillXBillItem, objBillXBatch , true , m_objLocalCache.GetCodeId( "OP", "ADJUSTMENT_TYPES") , sPolicyNumber, iTermNumber );
								objBillXBillItem.Status = m_objLocalCache.GetCodeId( "OK", "BILLING_ITEM_STAT" ) ;
                                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
								p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
								objBillXBatch.BatchCount++ ;
								
								// DO SCHEDULING FOR THIS ITEM
								// create contra item
								objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
								this.CreateAdjustment( objBillXBillItem, objBillXBatch , false , m_objLocalCache.GetCodeId( "BI", "ADJUSTMENT_TYPES") , sPolicyNumber, iTermNumber );
								objBillXBillItem.FinalBatchNum = objBillXBatch.BatchRowid ;
								objBillXBillItem.FinalBatchType = objBillXBatch.BatchType ;
								objBillXBillItem.Status = m_objLocalCache.GetCodeId( "RC", "BILLING_ITEM_STAT" ) ;
								objBillXBillItem.DateReconciled = Conversion.GetDate( DateTime.Now.ToString( "d" ) );
                                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
								p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
								objBillXBatch.BatchCount++ ;
							}
							break;
							// reconcile with write-off
						case "W" :
							if( objBillXBatch.BatchAmount < 0 )
							{
								// Create + writeoff txn
								objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
								this.CreateAdjustment( objBillXBillItem, objBillXBatch , false , m_objLocalCache.GetCodeId( "WO", "ADJUSTMENT_TYPES") , sPolicyNumber, iTermNumber );
								objBillXBillItem.Status = m_objLocalCache.GetCodeId( "RC", "BILLING_ITEM_STAT" ) ;
								objBillXBillItem.DateReconciled = Conversion.GetDate( DateTime.Now.ToString( "d" ) );
								objBillXBillItem.FinalBatchNum = objBillXBatch.BatchRowid ;
								objBillXBillItem.FinalBatchType = objBillXBatch.BatchType ;
                                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
								p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
								objBillXBatch.BatchCount++ ;
								objBillXBatch.BatchAmount += objBillXBillItem.Amount ;
							}
							else
							{
								// Create - writeoff txn
								objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
								this.CreateAdjustment( objBillXBillItem, objBillXBatch , false , m_objLocalCache.GetCodeId( "WO", "ADJUSTMENT_TYPES") , sPolicyNumber, iTermNumber );
								objBillXBillItem.Status = m_objLocalCache.GetCodeId( "RC", "BILLING_ITEM_STAT" ) ;
								objBillXBillItem.DateReconciled = Conversion.GetDate( DateTime.Now.ToString( "d" ) );
								objBillXBillItem.FinalBatchNum = objBillXBatch.BatchRowid ;
								objBillXBillItem.FinalBatchType = objBillXBatch.BatchType ;
                                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
								p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid , objBillXBillItem );
								objBillXBatch.BatchCount++ ;
								objBillXBatch.BatchAmount += objBillXBillItem.Amount ;
							}
							break;		
					}
				}

				objBillXBatch.DateClosed = Conversion.GetDate( DateTime.Now.ToString( "d" ) );
				objBillXBatch.Status = m_objLocalCache.GetCodeId( "C", "BATCH_STATUS" );				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.Reconcile.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBatch != null )
				{
					objBillXBatch.Dispose();
					objBillXBatch = null ;
				}
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                }
			}
		}
		/// <summary>
		/// Creates an Adjustment Billing Item.
		/// </summary>
        /// <param name="p_objBillXBillItem">Object of BillXBillItem Class</param>
        /// <param name="p_objBillXBatch">Object of BillXBatch Class</param>
		/// <param name="p_bKeepSign">Positive or Negative</param>
        /// <param name="p_iType">AdjustmentType</param>
		/// <param name="p_sPolicyNumber">Policy Number</param>
		/// <param name="p_iTermNumber">Term Number</param>
        private void CreateAdjustment( BillXBillItem  p_objBillXBillItem, BillXBatch p_objBillXBatch,  bool p_bKeepSign , int p_iType, string p_sPolicyNumber, int p_iTermNumber )
		{
			try
			{
				if( p_bKeepSign )
					p_objBillXBillItem.Amount = p_objBillXBatch.BatchAmount ;
				else
					p_objBillXBillItem.Amount = - p_objBillXBatch.BatchAmount ;

				p_objBillXBillItem.BillingItemType = m_objLocalCache.GetCodeId( "A", "BILLING_ITEM_TYPES" );
				p_objBillXBillItem.DateEntered = Conversion.GetDate( DateTime.Now.ToString("d") ); 
				p_objBillXBillItem.EntryBatchNum = p_objBillXBatch.BatchRowid ;
				p_objBillXBillItem.EntryBatchType = p_objBillXBatch.BatchType ;
				p_objBillXBillItem.PolicyId = p_objBillXBatch.PolicyId ;
				p_objBillXBillItem.PolicyNumber = p_sPolicyNumber ;
				p_objBillXBillItem.BillXAdjstmnt.AdjustmentType = p_iType ;
				p_objBillXBillItem.TermNumber = p_iTermNumber ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.CreateAdjustment.Error", m_iClientId), p_objEx);				
			}			
		}

		#endregion 

        /// <summary>
        /// Delete the selected billing item
        /// </summary>
        /// <param name="p_arrlstBillingItemIDs">List of Billing Items</param>
        public void DeleteBillingItem(string[] p_arrlstBillingItemIDs)
        { 

            ArrayList arrlstSelectedBillItems = new ArrayList();
            BillXBillItem objBillXBillItem = null;
            DbReader objReader = null;
            DiaryManager objDiaryManager = new DiaryManager(m_objDataModelFactory, m_iClientId);
            int iBillItemRowId;
            string sBillingItemStatus= string.Empty;
            string sBillingItemType=string.Empty;
            bool IsReceiptPresent = false;
            string sSql=string.Empty;
            int iPolicyId = 0;
            int iTermNumber=0;
            string sAdjType = string.Empty;
            bool bEPDError = false;
            try
            {
                foreach (string sBillingRowID in p_arrlstBillingItemIDs)
                {
                    iBillItemRowId = Conversion.ConvertObjToInt(sBillingRowID, m_iClientId);
                    objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                    objBillXBillItem.MoveTo(iBillItemRowId);
                    iPolicyId = objBillXBillItem.PolicyId;
                    iTermNumber = objBillXBillItem.TermNumber;
                    arrlstSelectedBillItems.Add(objBillXBillItem);
                    objBillXBillItem = null;
                }
                foreach (BillXBillItem objbillItem in arrlstSelectedBillItems)
                {

                    sBillingItemType = m_objLocalCache.GetShortCode(objbillItem.BillingItemType);
                    if (sBillingItemType == "P")
                        throw new RMAppException(Globalization.GetString("BillingManager.DeleteBillingItem.CanNotDeletePremium", m_iClientId));
                    if (sBillingItemType == "A")
                    {
                        sAdjType = m_objLocalCache.GetShortCode(objbillItem.BillXAdjstmnt.AdjustmentType);
                        if (sAdjType == "OP" || sAdjType == "UP" || sAdjType == "BI")
                            throw new RMAppException(Globalization.GetString("BillingManager.DeleteBillingItem.CanNotDeleteAdjustment", m_iClientId));
                    }
                    if (sBillingItemType == "R")
                        IsReceiptPresent = true;

                }

                if (IsReceiptPresent)
                {
                    sSql = "SELECT count(A.BILLING_ITEM_ROWID) FROM BILL_X_BILL_ITEM A, BILL_X_ADJSTMNT B";
                    sSql = sSql + " WHERE A.BILLING_ITEM_ROWID = B.BILLING_ITEM_ROWID AND A.STATUS = ";
                    sSql = sSql + m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT");
                    sSql = sSql + " And B.ADJUSTMENT_TYPE = " + m_objLocalCache.GetCodeId("ED", "ADJUSTMENT_TYPES"); ;
                    sSql = sSql + " And A.Policy_ID = " + iPolicyId.ToString();

                    objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                    if (objReader.Read())
                        if (Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId) > 0)
                            bEPDError = true;
                            //throw new RMAppException(Globalization.GetString("BillingManager.DeleteBillingItem.CanNotDeleteReceiptEarlyPay"));
                }

                foreach (BillXBillItem objbillItem in arrlstSelectedBillItems)
                {

                    objbillItem.Delete();
                    objDiaryManager.ScheduleCreditDiaries(iPolicyId, iTermNumber);
                }

                if(bEPDError)
                    throw new RMAppException(Globalization.GetString("BillingManager.DeleteBillingItem.CanNotDeleteReceiptEarlyPay", m_iClientId));
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BillingManager.DeleteBillingItem.Error", m_iClientId), p_objEx);
            }
            finally
            {
                arrlstSelectedBillItems = null;
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                    objReader = null;
                }
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                }
            }

        }
		public void CreateChargeItemPolicy( BillingMaster p_objBillingMaster, int p_iTransNumber, string p_sType )
		{
			this.CreateChargeItemPolicy( p_objBillingMaster , p_iTransNumber , p_sType , 0.0 , false );
		}
				
		public void CreateChargeItemPolicy( BillingMaster p_objBillingMaster, int p_iTransNumber, string p_sType , double p_dblAmount )
		{
			this.CreateChargeItemPolicy( p_objBillingMaster , p_iTransNumber , p_sType , p_dblAmount , false );
		}
		
        /// <summary>
        /// Creates a Charge Item Policy
        /// </summary>
        /// <param name="p_objBillingMaster">Object of BillingMaster</param>
        /// <param name="p_iTransNumber">Transaction Number</param>
        /// <param name="p_sType"></param>
        /// <param name="p_dblAmount">Amount</param>
        /// <param name="p_bNoCancelDiaries">Whether to cancel the Diaries</param>
		public void CreateChargeItemPolicy( BillingMaster p_objBillingMaster, int p_iTransNumber, string p_sType, double p_dblAmount, bool p_bNoCancelDiaries )
		{
			BillingRule objBillingRule = null ;
			BillXBillItem objBillXBillItem = null ;
			PolicyEnh objPolicyEnh = null ;		
			DiaryManager objDiaryManager = null ;

			string sSQL = string.Empty ;			
            //npadhy RMSC retrofit Starts
            BillXBillItem objbiTax; 
            string seff=string.Empty;  
            string sexp =string.Empty;  
            double dTax =0;
            int lTranType=0;
            DbReader objReader = null;
            PolicyXTransEnh objPolicyXTransEnhTemp = null; ;
            //npadhy RMSC retrofit Ends
			
			try
			{
				objDiaryManager = new DiaryManager( m_objDataModelFactory,m_iClientId );
				objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
			
				// Look at billing rule
				sSQL = " SELECT * FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID = " + p_objBillingMaster.Account.BillingRuleRowid ;
				objBillingRule = new BillingRule();
				this.FillBillingRule( objBillingRule , sSQL );

				objBillXBillItem.BillingItemType = m_objLocalCache.GetCodeId( "P" , "BILLING_ITEM_TYPES" );

				switch( p_sType )
				{
					case "NB" :
						objBillXBillItem.BillXPremItem.PremiumItemType = m_objLocalCache.GetCodeId( "FB", "PREMIUM_ITEM_TYPES" );
						break;
					case "EN" :
						objBillXBillItem.BillXPremItem.PremiumItemType = m_objLocalCache.GetCodeId( "PC", "PREMIUM_ITEM_TYPES" );
						break;
					case "AU" :
						objBillXBillItem.BillXPremItem.PremiumItemType = m_objLocalCache.GetCodeId( "AP", "PREMIUM_ITEM_TYPES" );
						break ;
					case "CF" :
					case "CPR" :
						objBillXBillItem.BillXPremItem.PremiumItemType = m_objLocalCache.GetCodeId( "CP", "PREMIUM_ITEM_TYPES" );
						break ;
					case "RL" :
					case "RNL" :
						objBillXBillItem.BillXPremItem.PremiumItemType = m_objLocalCache.GetCodeId( "RP", "PREMIUM_ITEM_TYPES" ) ;
						break ;												
				}

				foreach( int iKey in p_objBillingMaster.Policies.Keys )
				{
					objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
					if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
					{
						objBillXBillItem.PolicyId = objPolicyEnh.PolicyId ;
						foreach( PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList )
						{
                            //npadhy RMSC retrofit Starts
                            objPolicyXTransEnhTemp=objPolicyXTransEnh;  
							//npadhy RMSC retrofit Ends
							if( objPolicyXTransEnh.TransactionId == p_iTransNumber )
							{
								if( m_objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) == "NB" 
									|| m_objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) == "RN"
									|| m_objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) == "EN"
									|| m_objLocalCache.GetShortCode( objPolicyXTransEnh.TransactionType ) == "AU" )
									objBillXBillItem.Amount = objPolicyXTransEnh.ChgBilledPremium ;
								else
									objBillXBillItem.Amount = p_dblAmount  ;

								objBillXBillItem.BillXPremItem.PolicyTranId = objPolicyXTransEnh.TransactionId ;
                                //npadhy RMSC retrofit Starts Get Transaction Type
                                lTranType = objPolicyXTransEnh.TransactionType;
                                 //npadhy RMSC retrofit Ends
								foreach( PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList )
								{
									if( objPolicyXTermEnh.TermNumber == objPolicyXTransEnh.TermNumber && objPolicyXTermEnh.SequenceAlpha == objPolicyXTransEnh.TermSeqAlpha )
									{
										objBillXBillItem.PolicyNumber = objPolicyXTermEnh.PolicyNumber ;
										objBillXBillItem.TermNumber = objPolicyXTermEnh.TermNumber ;
                                        seff=objPolicyXTermEnh.EffectiveDate;
                                        sexp=objPolicyXTermEnh.ExpirationDate;
                                        sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")" 
                                                + "AND EFFECTIVE_DATE <= '" + seff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + seff + "') AND IN_USE_FLAG <> 0" ;

                                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                                        if (objReader.Read())
                                        {
                                            p_objBillingMaster.Account.UseTax = true;
                                            p_objBillingMaster.Account.TaxRate = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                                            p_objBillingMaster.Account.TaxType = Conversion.ConvertObjToInt(objReader.GetValue("FLAT_OR_PERCENT"), m_iClientId);     
                                        }
										break;
									}
								}
								break ;
							}
						}
						objBillXBillItem.Status = m_objLocalCache.GetCodeId( "OK", "BILLING_ITEM_STAT" );
						objBillXBillItem.DateEntered = Conversion.GetDate( DateTime.Now.ToString("d") ); // TODO
						if( objBillXBillItem.Amount > 0 )
                            objBillXBillItem.DueDate = Conversion.GetDate(Conversion.ToDate(Conversion.GetDBDateFormat(objBillXBillItem.DateEntered, "yyyyMMdd")).AddDays(objBillingRule.DueDays).ToString("d"));// TODO
						break;
					}
				}

                //npadhy RMSC retrofit Starts
                //Subtract Tax from Amount
                switch (this.m_objLocalCache.GetShortCode(lTranType))
                {
                    case "CPR":
                    case "CF":
                        if (p_objBillingMaster.Account.UseTax)
                        {
                            if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                            {
                                p_objBillingMaster.Account.TaxAmount = (objBillXBillItem.Amount) * (p_objBillingMaster.Account.TaxRate / 100);
                                if (p_objBillingMaster.Account.TaxAmount > 0)
                                    p_objBillingMaster.Account.TaxAmount = Math.Round(p_objBillingMaster.Account.TaxAmount, 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                p_objBillingMaster.Account.TaxAmount = 0;
                            }
                        }
                        break;
                    case "AU":
                    case "EN":
                    case "RN":
                        if (p_objBillingMaster.Account.UseTax)
                        {
                            objBillXBillItem.Amount = objPolicyXTransEnhTemp.TransactionPrem;
                            if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                                dTax = objBillXBillItem.Amount * (p_objBillingMaster.Account.TaxRate / 100);
                            else
                                dTax = objBillXBillItem.Amount * p_objBillingMaster.Account.TaxRate;
                        }
                        else
                        {
                            objBillXBillItem.Amount = objPolicyXTransEnhTemp.TransactionPrem;   
                        }
                        break;
                    default:
                        if (p_objBillingMaster.Account.UseTax)
                        {
                            dTax = p_objBillingMaster.Account.TaxAmount;
                            objBillXBillItem.Amount = objBillXBillItem.Amount - dTax;    
                        }
                        break;
                }
                 //npadhy RMSC retrofit Ends
                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
				p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid, objBillXBillItem );

                //npadhy RMSC retrofit Starts
                if (p_objBillingMaster.Account.UseTax)
                {
                    if ((this.m_objLocalCache.GetShortCode(lTranType) == "AU" || this.m_objLocalCache.GetShortCode(lTranType) == "RN" || this.m_objLocalCache.GetShortCode(lTranType) == "EN") && (dTax != 0))
                    {
                        this.CreateChargeItemTax(objBillXBillItem, p_objBillingMaster, objBillXBillItem.BillXPremItem.PolicyTranId, dTax);         
                    }
                    else if(p_objBillingMaster.Account.TaxAmount !=0)
                    {
                        this.CreateChargeItemTax(objBillXBillItem, p_objBillingMaster, objBillXBillItem.BillXPremItem.PolicyTranId, p_objBillingMaster.Account.TaxAmount);        
                    }
                }
                //npadhy RMSC retrofit Ends

				if( !p_bNoCancelDiaries && objBillXBillItem.Amount > 0  && p_sType != "AU" && p_sType != "CF" && p_sType != "CPR" ) 
				{
					objDiaryManager.ScheduleCancelPendingDiary( objBillXBillItem , objBillingRule , p_objBillingMaster );
					objDiaryManager.ScheduleCancelDiary( objBillXBillItem , objBillingRule , p_objBillingMaster );
					objDiaryManager.ScheduleNoticeDiaries( objBillXBillItem , objBillingRule , p_objBillingMaster );
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.CreateChargeItemPolicy.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                }
				objBillingRule = null ;				
			}
		}
        
        /// <summary>
        /// Creates Charge Item Installments.
        /// </summary>
        /// <param name="p_objBillingMaster">Object of BillingMaster</param>
        /// <param name="p_objBillXInstlmnt">List of installemnts for a given policy</param>
		public void CreateChargeItemInstallment( BillingMaster p_objBillingMaster , BillXInstlmnt p_objBillXInstlmnt )
		{
			BillXBillItem objBillXBillItem = null ;
			BillingRule objBillingRule = null ;
			DbReader objReader = null ;
			DiaryManager objDiaryManager = null ;

			string sSQL = string.Empty ;
			int iBrRowId = 0 ;

			try
			{
				objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
				objBillXBillItem.BillingItemType = m_objLocalCache.GetCodeId( "P" , "BILLING_ITEM_TYPES" );

                objDiaryManager = new DiaryManager(m_objDataModelFactory, m_iClientId);

				// Create the charge item based on the installment record
				objBillXBillItem.BillXPremItem.PremiumItemType = m_objLocalCache.GetCodeId( "IN" , "PREMIUM_ITEM_TYPES" );
				objBillXBillItem.BillXPremItem.PolicyTranId = p_objBillXInstlmnt.PolicyTranId ;

				if( p_objBillingMaster.Account.BillAccountRowid == 0 )
				{
					sSQL = " SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID=" + p_objBillXInstlmnt.PolicyId + " AND TERM_NUMBER=" + p_objBillXInstlmnt.TermNumber ;
					this.FillAccount( p_objBillingMaster.Account , sSQL );
				}

				objBillXBillItem.Amount = p_objBillXInstlmnt.Amount ;
				objBillXBillItem.PolicyId = p_objBillXInstlmnt.PolicyId ;
				objBillXBillItem.PolicyNumber = p_objBillingMaster.Account.PolicyNumber ;
				objBillXBillItem.Status = m_objLocalCache.GetCodeId( "OK" , "BILLING_ITEM_STAT" ) ;
				objBillXBillItem.DateEntered = p_objBillXInstlmnt.InstallmentDate ;
				objBillXBillItem.TermNumber = p_objBillXInstlmnt.TermNumber ;
                objBillXBillItem.DueDate = p_objBillXInstlmnt.InstallDueDate;

                objBillXBillItem.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
				p_objBillingMaster.BillingItems.Add( objBillXBillItem.BillingItemRowid, objBillXBillItem );

                //npadhy RMSC retrofit Starts
                if (p_objBillingMaster.Account.UseTax)
                {
                    this.CreateChargeItemTax(objBillXBillItem, p_objBillingMaster, p_objBillXInstlmnt.PolicyTranId, p_objBillXInstlmnt.TaxAmount);
                }                
                 //npadhy RMSC retrofit Ends

				if( p_objBillingMaster.Account.BillingRuleRowid != 0 )
				{
                    iBrRowId = p_objBillingMaster.Account.BillingRuleRowid;
				}
				else
				{
					sSQL = " SELECT BILLING_RULE_ROWID FROM BILL_X_ACCOUNT WHERE POLICY_ID = " + objBillXBillItem.PolicyId ;
					objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
					if( objReader.Read() )
					{
						iBrRowId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_RULE_ROWID" ), m_iClientId ); ;
					}
					objReader.Close();
				}

				objBillingRule = new BillingRule();
				sSQL = " SELECT * FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID = " +iBrRowId ;				
				this.FillBillingRule( objBillingRule, sSQL );

				p_objBillXInstlmnt.GeneratedFlag = true ;
				objDiaryManager.ScheduleInstallmentDiary( p_objBillingMaster, p_objBillXInstlmnt.PolicyId, p_objBillXInstlmnt.InstallmentNum );
				objDiaryManager.ScheduleInvoiceDiaries( p_objBillXInstlmnt, p_objBillingMaster );
				objDiaryManager.ScheduleCancelPendingDiary(  objBillXBillItem, objBillingRule, p_objBillingMaster );
				objDiaryManager.ScheduleCancelDiary( objBillXBillItem, objBillingRule, p_objBillingMaster );
				objDiaryManager.ScheduleNoticeDiaries( objBillXBillItem, objBillingRule, p_objBillingMaster );
												
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.CreateChargeItemInstallment.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXBillItem != null )
				{
					objBillXBillItem.Dispose();
					objBillXBillItem = null ;
				}
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                }
				objBillingRule = null ;			
			}
		}
		
		public void AdjustInstallments( BillingMaster p_objBillingMaster, int p_iTransNumber, string p_sType )
		{			
			PayPlan objPayPlan = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			PolicyEnh objPolicyEnh = null ; 
			DiaryManager objDiaryManager = null ;
			DbReader objReader = null;
            
            //npadhy RMSC retrofit Starts
            double dTaxRate=0;
            double dTranTax=0 ;
            double dInstalTax=0 ;
            double dDividedTax=0;
            int lInstlCount=0;
            double dCheckValue=0 ;
            double dTotalTax=0;
            double dTaxDiff=0;
            double dGeneratedTax=0;
            PolicyXTransEnh objPolicyXTransEnhTemp=null;
            //npadhy RMSC retrofit ends

			string sSQL = string.Empty ;
			int iIndex = 0 ;
			int iMinInstallNumber = 0 ;
			double dblAmount = 0.0 ;
			double dblDividedAmount = 0.0 ;


			try
			{
                objDiaryManager = new DiaryManager(m_objDataModelFactory, m_iClientId);

				objPayPlan = new PayPlan();
				sSQL = " SELECT * FROM SYS_BILL_PAY_PLAN WHERE PAY_PLAN_ROWID = " + p_objBillingMaster.Account.PayPlanRowid ;
				this.FillPayPlan( objPayPlan , sSQL );

				switch( m_objLocalCache.GetShortCode( objPayPlan.ExtraAmtsIndic ) )
				{
					case "I" :
						this.CreateChargeItemPolicy( p_objBillingMaster , p_iTransNumber , p_sType );
						objDiaryManager.ScheduleInvoiceDiaries( p_objBillingMaster );
						break ;
					case "N" :
						objBillXInstlmnt = ( BillXInstlmnt ) m_objDataModelFactory.GetDataModelObject( "BillXInstlmnt" , false );
						sSQL = " SELECT MIN(INSTALLMENT_NUM) FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + 
							p_objBillingMaster.Account.PolicyId + " AND BILL_ACCOUNT_ROWID = " + 
							p_objBillingMaster.Account.BillAccountRowid + " AND GENERATED_FLAG = 0" ;

						objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
						if( objReader.Read() )
						{
							iMinInstallNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );				
						}
						else
						{
							this.CreateChargeItemPolicy( p_objBillingMaster, p_iTransNumber , p_sType );
							objDiaryManager.ScheduleInvoiceDiaries( p_objBillingMaster );
							return ;
						}

                        objReader.Close();	

                        //Changed by Gagan for mits 16239
                        //Case where there is no min installment num i.e. iMinInstallNumber = 0
                        //ie. no ungenerated installment, then Bill Immediately
                        if (iMinInstallNumber == 0)
                        {
                            this.CreateChargeItemPolicy(p_objBillingMaster, p_iTransNumber, p_sType);
                            objDiaryManager.ScheduleInvoiceDiaries(p_objBillingMaster);
                            return;
                        }
						

						sSQL = " SELECT * FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_objBillingMaster.Account.PolicyId + " AND BILL_ACCOUNT_ROWID = " + p_objBillingMaster.Account.BillAccountRowid + " AND INSTALLMENT_NUM = " + iMinInstallNumber ;
						this.FillInstallment( objBillXInstlmnt , sSQL );
						
						foreach( int iKey in p_objBillingMaster.Policies.Keys )
						{
							objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
							if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
							{
								foreach( PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList )
								{
									if( objPolicyXTransEnh.TransactionId == p_iTransNumber )
									{
                                        //npadhy RMSC retrofit Starts
                                        // Earlier the Amount was calculated as Change in Billed Premium. But with Tax around 
                                        // Transaction Tax needs to subtracted from it.
										//objBillXInstlmnt.Amount += objPolicyXTransEnh.ChgBilledPremium ;
                                        dTranTax = objPolicyXTransEnh.TranTax;
                                        dTaxRate = objPolicyXTransEnh.TaxRate;
                                        objBillXInstlmnt.Amount += (objPolicyXTransEnh.ChgBilledPremium - dTranTax);
                                        //npadhy RMSC retrofit Ends
										// this will change the transaction associated
										objBillXInstlmnt.PolicyTranId = p_iTransNumber ;
										break ;
									}
								}	
								break ;		
							}
						}
                        //npadhy RMSC retrofit Starts 
                        // Only the Tax of Percent Type is adjusted and not the Flat Tax
                        if (p_objBillingMaster.Account.UseTax)
                        {
                            if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                            {
                                objBillXInstlmnt.TaxAmount = Math.Round(Math.Abs(objBillXInstlmnt.Amount) * (p_objBillingMaster.Account.TaxRate / 100), 2, MidpointRounding.AwayFromZero);    
                                if (objBillXInstlmnt.Amount < 0 )
                                    objBillXInstlmnt.TaxAmount = -(objBillXInstlmnt.TaxAmount);  
                            }
                        }
                        //npadhy RMSC retrofit ends

						p_objBillingMaster.Installments.Add( objBillXInstlmnt );
						break ;
					case "S" :
						sSQL = " SELECT * FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_objBillingMaster.Account.PolicyId + 
								" AND BILL_ACCOUNT_ROWID = " + p_objBillingMaster.Account.BillAccountRowid + 
								" AND GENERATED_FLAG = 0 ORDER BY INSTALLMENT_NUM" ;

						this.FillInstallmentLoop( p_objBillingMaster, sSQL );

						if( p_objBillingMaster.Installments.Count == 0 )
						{
							this.CreateChargeItemPolicy( p_objBillingMaster, p_iTransNumber , p_sType );
							objDiaryManager.ScheduleInvoiceDiaries( p_objBillingMaster );
							return ;
						}
						
						foreach( int iKey in p_objBillingMaster.Policies.Keys )
						{
							objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
							if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
							{
								foreach( PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList )
								{
                                    objPolicyXTransEnhTemp=objPolicyXTransEnh;  
									if( objPolicyXTransEnh.TransactionId == p_iTransNumber )
									{
                                        //Changed by Gagan for mits 18446: Start

										//dblAmount = objPolicyXTransEnh.ChgBilledPremium ;
                                        dblAmount = objPolicyXTransEnh.TransactionPrem;
                                        //npadhy RMSC retrofit Starts
                                        //dTranTax = objPolicyXTransEnh.TranTax;
                                        //dTaxRate = objPolicyXTransEnh.TaxRate;
                                        //dCheckValue = dblAmount - dTranTax;
                                        //dblAmount includes only changed premium
                                        //dblAmount = dCheckValue;
                                        //npadhy RMSC retrofit Ends
                                        //Changed by Gagan for mits 18446: End

										break ;
									}
								}	
								break ;		
							}
						}
                        //Add by kuladeep for MITS:25093--Start
                        lInstlCount = p_objBillingMaster.Installments.Count;
                        //Add by kuladeep for MITS:25093--End
						dblDividedAmount = dblAmount / p_objBillingMaster.Installments.Count ;

                        if (Function.GetRoundAmountFlag(m_sConnectionString, m_iClientId))
                            dblDividedAmount = Math.Round(dblDividedAmount, 0, MidpointRounding.AwayFromZero);
						else
                            dblDividedAmount = Math.Round(dblDividedAmount, 2, MidpointRounding.AwayFromZero);

						for( iIndex = 0 ; iIndex < p_objBillingMaster.Installments.Count ; iIndex++ )
						{
							objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[iIndex] ;
							
                            objBillXInstlmnt.PolicyTranId = p_iTransNumber ;
                            //npadhy RMSC retrofit Starts
                            //objBillXInstlmnt.Amount += dblDividedAmount ; 
                            if (Math.Abs(dblDividedAmount) > Math.Abs(dblAmount))
                            {
                                objBillXInstlmnt.Amount += dblAmount;
                            }
                            else
                            {
                                objBillXInstlmnt.Amount += dblDividedAmount; 
                            }
                            //npadhy RMSC retrofit Ends
							dblAmount -= dblDividedAmount ;
                            
                            //npadhy RMSC retrofit Starts
                            if (p_objBillingMaster.Account.UseTax)
                            {
                                if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                                {
                                    objBillXInstlmnt.TaxAmount = Math.Round(objBillXInstlmnt.Amount * (p_objBillingMaster.Account.TaxRate / 100), 2, MidpointRounding.AwayFromZero);
                                    dTotalTax += objBillXInstlmnt.TaxAmount;
                                }
                            }
                            else
                            {
                                objBillXInstlmnt.TaxAmount = Math.Round((p_objBillingMaster.Account.TaxRate / lInstlCount), 2, MidpointRounding.AwayFromZero);    
                            }
                            //npadhy RMSC retrofit Ends
						}

						if( dblAmount > 0 )
						{
							//  TODO
							objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[ p_objBillingMaster.Installments.Count -1 ] ;
							objBillXInstlmnt.Amount += dblAmount ; 
						}

                        //npadhy RMSC retrofit Starts
                        sSQL = "SELECT TAX_AMOUNT FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_objBillingMaster.Account.PolicyId + " AND BILL_ACCOUNT_ROWID = " + p_objBillingMaster.Account.BillAccountRowid + " AND GENERATED_FLAG <> 0 ORDER BY INSTALLMENT_NUM";
                        objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                        while (objReader.Read())
                        {
                            dGeneratedTax += Conversion.ConvertObjToDouble(objReader.GetValue("TAX_AMOUNT"), m_iClientId);       
                        }
                        objReader.Close();
                        dTaxDiff = objPolicyXTransEnhTemp.TaxAmount - dTotalTax - dGeneratedTax;
                        if (dTaxDiff != 0)
                        {
                            objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[p_objBillingMaster.Installments.Count - 1];
                            objBillXInstlmnt.TaxAmount += dTaxDiff; 
                        }
                        //npadhy RMSC retrofit ends

						break ;
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.AdjustInstallments.Error", m_iClientId), p_objEx);
			}
			finally
			{
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
				objPayPlan = null ;
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                }
                //npadhy RMSC retrofit Starts
                if (objPolicyXTransEnhTemp != null)
                {
                    objPolicyXTransEnhTemp.Dispose();
                    objPolicyXTransEnhTemp = null;
                }
                //npadhy RMSC retrofit Ends
			}

		}
		
		#region Fill Methods for Notice, Invoice, Account, BillItem, Premium, Disbursement, Adjustment and Billing Rule, Installment, PayPlan
		
		public void FillNotice( BillXNotice p_objBillXNotice , string p_sSQL )
		{
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{
                    p_objBillXNotice.MoveTo(Common.Conversion.ConvertObjToInt(objReader.GetValue("NOTICE_ROWID"), m_iClientId));
		
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillNotice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Dispose();
					objReader = null ;
				}
			}
		}


		public void FillInvoice( BillXInvoice p_objBillXInvoice , string p_sSQL )
		{
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{   
                    p_objBillXInvoice.MoveTo(Common.Conversion.ConvertObjToInt( objReader.GetValue( "INVOICE_ROWID" ), m_iClientId ));
		
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillInvoice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Dispose();
					objReader = null ;
				}
			}
		}


		public void FillAccount( BillXAccount p_objBillXAccount , string p_sSQL )
		{
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{
                    p_objBillXAccount.MoveTo(Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILL_ACCOUNT_ROWID" ), m_iClientId) );
			
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillAccount.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
		}


		public void FillPayPlan( PayPlan p_objPayPlan , string p_sSQL )
		{
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{

					p_objPayPlan.AddedByUser = objReader.GetString( "ADDED_BY_USER" );
					p_objPayPlan.BillingRuleRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_RULE_ROWID" ), m_iClientId );											
					p_objPayPlan.DttmRcdAdded = objReader.GetString( "DTTM_RCD_ADDED" );
					p_objPayPlan.DttmRcdLastUpd = objReader.GetString( "DTTM_RCD_LAST_UPD" );
					p_objPayPlan.ExtraAmtsIndic = Common.Conversion.ConvertObjToInt( objReader.GetValue( "EXTRA_AMTS_INDIC" ), m_iClientId );											
					p_objPayPlan.FixedDueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FIXED_DUE_DAYS" ), m_iClientId );											
					p_objPayPlan.FixedFrequency = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FIXED_FREQUENCY" ), m_iClientId );											
					p_objPayPlan.FixedGenDay = Common.Conversion.ConvertObjToInt( objReader.GetValue( "FIXED_GEN_DAY" ), m_iClientId );											
					p_objPayPlan.InstallGenType = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL_GEN_TYPE" ), m_iClientId );											
					p_objPayPlan.Install1Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL1_AMOUNT" ) ));
					p_objPayPlan.Install1Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL1_TYPE" ), m_iClientId );
					p_objPayPlan.Install1LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL1_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install1DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL1_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install2Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL2_AMOUNT" ) ));
					p_objPayPlan.Install2Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL2_TYPE" ), m_iClientId );
					p_objPayPlan.Install2LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL2_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install2DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL2_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install3Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL3_AMOUNT" ) ));
					p_objPayPlan.Install3Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL3_TYPE" ), m_iClientId );
					p_objPayPlan.Install3LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL3_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install3DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL3_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install4Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL4_AMOUNT" ) ));
					p_objPayPlan.Install4Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL4_TYPE" ), m_iClientId );
					p_objPayPlan.Install4LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL4_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install4DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL4_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install5Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL5_AMOUNT" ) ));
					p_objPayPlan.Install5Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL5_TYPE" ), m_iClientId );
					p_objPayPlan.Install5LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL5_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install5DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL5_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install6Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL6_AMOUNT" ) ));
					p_objPayPlan.Install6Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL6_TYPE" ), m_iClientId );
					p_objPayPlan.Install6LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL6_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install6DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL6_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install7Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL7_AMOUNT" ) ));
					p_objPayPlan.Install7Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL7_TYPE" ), m_iClientId );
					p_objPayPlan.Install7LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL7_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install7DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL7_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install8Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL8_AMOUNT" ) ));
					p_objPayPlan.Install8Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL8_TYPE" ), m_iClientId );
					p_objPayPlan.Install8LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL8_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install8DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL8_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install9Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL9_AMOUNT" ) ));
					p_objPayPlan.Install9Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL9_TYPE" ), m_iClientId );
					p_objPayPlan.Install9LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL9_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install9DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL9_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install10Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL10_AMOUNT" ) ));
					p_objPayPlan.Install10Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL10_TYPE" ), m_iClientId );
					p_objPayPlan.Install10LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL10_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install10DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL10_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install11Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL11_AMOUNT" ) ));
					p_objPayPlan.Install11Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL11_TYPE" ), m_iClientId );
					p_objPayPlan.Install11LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL11_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install11DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL11_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.Install12Amount = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "INSTALL12_AMOUNT" ) ));
					p_objPayPlan.Install12Type = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL12_TYPE" ), m_iClientId );
					p_objPayPlan.Install12LagDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL12_LAG_DAYS" ), m_iClientId );
					p_objPayPlan.Install12DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALL12_DUE_DAYS" ), m_iClientId );
					p_objPayPlan.InUseFlag = Common.Conversion.ConvertObjToInt( objReader.GetValue( "IN_USE_FLAG" ), m_iClientId );
					p_objPayPlan.LateStartIndic = Common.Conversion.ConvertObjToInt( objReader.GetValue( "LATE_START_INDIC" ), m_iClientId );
					p_objPayPlan.LineOfBusiness = Common.Conversion.ConvertObjToInt( objReader.GetValue( "LINE_OF_BUSINESS" ), m_iClientId );
					p_objPayPlan.OffsetIndic = Common.Conversion.ConvertObjToInt( objReader.GetValue( "OFFSET_INDIC" ), m_iClientId );
					p_objPayPlan.PayPlanCode = Common.Conversion.ConvertObjToInt( objReader.GetValue( "PAY_PLAN_CODE" ), m_iClientId );
					p_objPayPlan.PayPlanRowid =Common.Conversion.ConvertObjToInt( objReader.GetValue( "PAY_PLAN_ROWID" ), m_iClientId );
					p_objPayPlan.State = Common.Conversion.ConvertObjToInt( objReader.GetValue( "STATE" ), m_iClientId );
					p_objPayPlan.UpdatedByUser = objReader.GetString( "UPDATED_BY_USER" );

                    //npadhy RMSC retrofit Starts
                    p_objPayPlan.FixedGenDate = Conversion.ConvertObjToStr(objReader.GetValue("FIXED_GEN_DATE"));
                    p_objPayPlan.FixedDueDate = Conversion.ConvertObjToStr(objReader.GetValue("FIXED_DUE_DATE"));
                    p_objPayPlan.Install1GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL1_GEN_DATE"));
                    p_objPayPlan.Install1DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL1_DUE_DATE"));
                    p_objPayPlan.Install2GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL2_GEN_DATE"));
                    p_objPayPlan.Install2DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL2_DUE_DATE"));
                    p_objPayPlan.Install3GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL3_GEN_DATE"));
                    p_objPayPlan.Install3DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL3_DUE_DATE"));
                    p_objPayPlan.Install4GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL4_GEN_DATE"));
                    p_objPayPlan.Install4DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL4_DUE_DATE"));
                    p_objPayPlan.Install5GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL5_GEN_DATE"));
                    p_objPayPlan.Install5DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL5_DUE_DATE"));
                    p_objPayPlan.Install6GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL6_GEN_DATE"));
                    p_objPayPlan.Install6DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL6_DUE_DATE"));
                    p_objPayPlan.Install7GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL7_GEN_DATE"));
                    p_objPayPlan.Install7DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL7_DUE_DATE"));
                    p_objPayPlan.Install8GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL8_GEN_DATE"));
                    p_objPayPlan.Install8DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL8_DUE_DATE"));
                    p_objPayPlan.Install9GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL9_GEN_DATE"));
                    p_objPayPlan.Install9DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL9_DUE_DATE"));
                    p_objPayPlan.Install10GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL10_GEN_DATE"));
                    p_objPayPlan.Install10DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL10_DUE_DATE"));
                    p_objPayPlan.Install11GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL11_GEN_DATE"));
                    p_objPayPlan.Install11DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL11_DUE_DATE"));
                    p_objPayPlan.Install12GenDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL12_GEN_DATE"));
                    p_objPayPlan.Install12DueDate = Conversion.ConvertObjToStr(objReader.GetValue("INSTALL12_DUE_DATE"));
                    //npadhy RMSC retrofit Ends
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillPayPlan.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}				
			}
		}


		public void FillBillingItem( BillXBillItem p_objBillXBillItem , string p_sSQL )
		{
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{
                    p_objBillXBillItem.MoveTo(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId));

				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillBillItem.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}				
			}
		}


		public void FillBillingRule( BillingRule p_objBillingRule , string p_sSQL )
		{
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{
					p_objBillingRule.AddedByUser = objReader.GetString( "ADDED_BY_USER" );
				
					p_objBillingRule.BillingRuleCode = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_RULE_CODE" ), m_iClientId );							
					p_objBillingRule.BillingRuleRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_RULE_ROWID" ), m_iClientId );											
					p_objBillingRule.DttmRcdAdded = objReader.GetString( "DTTM_RCD_ADDED" );
					p_objBillingRule.DttmRcdLastUpd = objReader.GetString( "DTTM_RCD_LAST_UPD" );
					p_objBillingRule.UpdatedByUser = objReader.GetString( "UPDATED_BY_USER" );	
					p_objBillingRule.FirstNoticeDays = objReader.GetString( "FIRST_NOTICE_DAYS" );
					p_objBillingRule.SecondNoticeDays = objReader.GetString( "SECOND_NOTICE_DAYS" );
					p_objBillingRule.ThirdNoticeDays = objReader.GetString( "THIRD_NOTICE_DAYS" );
					p_objBillingRule.FourthNoticeDays = objReader.GetString( "FOURTH_NOTICE_DAYS" );
					p_objBillingRule.NoticeOffsetInd = Common.Conversion.ConvertObjToInt( objReader.GetValue( "NOTICE_OFFSET_IND" ), m_iClientId );
					p_objBillingRule.MaxNumRebills = Common.Conversion.ConvertObjToInt( objReader.GetValue( "MAX_NUM_REBILLS" ), m_iClientId );
					p_objBillingRule.DueDays = Common.Conversion.ConvertObjToInt( objReader.GetValue( "DUE_DAYS" ), m_iClientId );
					p_objBillingRule.OutstandBalIndic = Common.Conversion.ConvertObjToInt( objReader.GetValue( "OUTSTAND_BAL_INDIC" ), m_iClientId );
					p_objBillingRule.InUseFlag = Common.Conversion.ConvertObjToInt( objReader.GetValue( "IN_USE_FLAG" ), m_iClientId );
					p_objBillingRule.NonPayInd = Common.Conversion.ConvertObjToInt( objReader.GetValue( "NON_PAY_IND" ), m_iClientId );
					p_objBillingRule.CancelPendDiary = Common.Conversion.ConvertObjToInt( objReader.GetValue( "CANCEL_PEND_DIARY" ), m_iClientId );
					p_objBillingRule.CancelDiary = Common.Conversion.ConvertObjToInt( objReader.GetValue( "CANCEL_DIARY" ), m_iClientId );						
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillBillingRule.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}				
			}
		}


		public void FillInstallment( BillXInstlmnt p_objBillXInstlmnt , string p_sSQL )
		{
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{
                    p_objBillXInstlmnt.MoveTo( Common.Conversion.ConvertObjToInt(objReader.GetValue("INSTALLMENT_ROWID"), m_iClientId));
				
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillInstallment.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Dispose();
					objReader = null ;
				}				
			}
		}
		

		public void FillInstallmentLoop( BillingMaster p_objBillingMaster, string p_sSQL )
		{
			BillXInstlmnt objBillXInstlmnt = null ;
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				while( objReader.Read() )
				{
					objBillXInstlmnt = ( BillXInstlmnt ) m_objDataModelFactory.GetDataModelObject( "BillXInstlmnt" , false );
                    objBillXInstlmnt.MoveTo(Common.Conversion.ConvertObjToInt(objReader.GetValue("INSTALLMENT_ROWID"), m_iClientId));

					// Installment is an Array List.
					p_objBillingMaster.Installments.Add( objBillXInstlmnt );
					objBillXInstlmnt = null ;
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillInstallmentLoop.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Dispose();
					objReader = null ;
				}	
                if (objBillXInstlmnt != null)
                {
                    objBillXInstlmnt.Dispose();
                }
			}
		}
		

		public void FillBillingItemLoop( ArrayList p_arrlstBillingItems, string p_sSQL , bool p_bRetrieveSubs )
		{
			DbReader objReader = null ;
			BillXBillItem objBillXBillItem = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				while( objReader.Read() )
				{
					objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
                    objBillXBillItem.MoveTo(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId));
					p_arrlstBillingItems.Add( objBillXBillItem );
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillBilltemLoop.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                }
			}
		}


		public bool FillBatch( BillXBatch p_objBillXBatch , string p_sSQL )
		{
			DbReader objReader = null ;
			bool bReturnValue = false ;

			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				if( objReader.Read() )
				{
                    p_objBillXBatch.MoveTo(Common.Conversion.ConvertObjToInt(objReader.GetValue("BATCH_ROWID"), m_iClientId));
					bReturnValue = true ;
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillBatch.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( bReturnValue );
		}


		public void FillBatchLoop( ArrayList p_arrlstBatch, string p_sSQL )
		{
			DbReader objReader = null ;
			BillXBatch objBillXBatch = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				while( objReader.Read() )
				{
					objBillXBatch = ( BillXBatch ) m_objDataModelFactory.GetDataModelObject( "BillXBatch" , false );
                    objBillXBatch.MoveTo(Common.Conversion.ConvertObjToInt( objReader.GetValue( "BATCH_ROWID" ), m_iClientId ));
					p_arrlstBatch.Add( objBillXBatch );
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillBatchLoop.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
                if (objBillXBatch != null)
                {
                    objBillXBatch.Dispose();    
                }
			}
		}


		public void FillBillAccount( ArrayList p_arrlstAccountItems, string p_sSQL )
		{
			BillXAccount objBillXAccount = null ;
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				while( objReader.Read() )
				{
					objBillXAccount = ( BillXAccount ) m_objDataModelFactory.GetDataModelObject( "BillXAccount" , false );
                    objBillXAccount.MoveTo(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILL_ACCOUNT_ROWID"), m_iClientId));
					p_arrlstAccountItems.Add( objBillXAccount );
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillbillAccouunt.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
				if( objBillXAccount != null )
				{
					objBillXAccount.Dispose();
					objBillXAccount = null ;
				}
			}
		}


		public void FillBillInstall( ArrayList p_arrlstInstallItems, string p_sSQL )
		{
			BillXInstlmnt objBillXInstlmnt = null ;
			DbReader objReader = null ;
			
			try
			{
				objReader = DbFactory.GetDbReader( m_sConnectionString , p_sSQL );
				while( objReader.Read() )
				{
					objBillXInstlmnt = ( BillXInstlmnt ) m_objDataModelFactory.GetDataModelObject( "BillXInstlmnt" , false );
                    objBillXInstlmnt.MoveTo(Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALLMENT_ROWID" ), m_iClientId ));

					p_arrlstInstallItems.Add( objBillXInstlmnt );
				}
				objReader.Close();
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("BillingManager.FillbillInstall.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}	
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}
			}
		}

        public void CreateChargeItemTax(BillXBillItem objBillXBillItem, BillingMaster p_objBillingMaster, int lPolicyTranID, double dTaxAmount)
        {
            int iPremType;
            string sSQL = string.Empty;
            DbReader objReader = null;
            int lBillingItemType;
            BillXBillItem objBillItemTax=null;
            try
            {
                objBillItemTax = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                objBillItemTax.BillingItemType = this.m_objLocalCache.GetCodeId("TAX", "BILLING_ITEM_TYPES");
                objBillItemTax.BillXPremItem.PremiumItemType = this.m_objLocalCache.GetCodeId("TAX", "PREMIUM_ITEM_TYPES");
                objBillItemTax.BillXPremItem.PolicyTranId = lPolicyTranID;
                if (p_objBillingMaster.Account.BillAccountRowid == 0)
                {
                    sSQL = "SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID= " + objBillXBillItem.PolicyId + " AND TERM_NUMBER= " + objBillXBillItem.TermNumber;
                    this.FillAccount(p_objBillingMaster.Account, sSQL);
                }
                objBillItemTax.Amount = dTaxAmount;
                objBillItemTax.PolicyId = objBillXBillItem.PolicyId;
                objBillItemTax.PolicyNumber = p_objBillingMaster.Account.PolicyNumber;
                objBillItemTax.Status = this.m_objLocalCache.GetCodeId("OK", "BILLING_ITEM_STAT");
                objBillItemTax.DateEntered = objBillXBillItem.DateEntered;
                objBillItemTax.DueDate = objBillXBillItem.DueDate;
                objBillItemTax.TermNumber = objBillXBillItem.TermNumber;
                if (p_objBillingMaster.Account.UseTax)
                {
                    objBillItemTax.BillingItemRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_BILL_ITEM", m_iClientId);
                    p_objBillingMaster.BillingItems.Add(objBillItemTax.BillingItemRowid, objBillItemTax);
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("BillingManager.CreateChargeItemTax.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objBillItemTax != null)
                {
                    objBillItemTax.Dispose();
                    objBillItemTax = null;
                }
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
        }

		#endregion 
	}
}

