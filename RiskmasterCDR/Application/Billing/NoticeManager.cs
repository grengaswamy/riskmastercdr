﻿
using System;
using System.IO ;
using System.Xml ;
using System.Text ;
using System.Collections;

using Riskmaster.Common ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.DataModel ;
using Riskmaster.Application.DocumentManagement ;
using Riskmaster.Application.FileStorage ;
using Riskmaster.Security ;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Export.Pdf;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: NoticeManager.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class NoticeManager : IDisposable
	{
		#region "Member Variables"
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = string.Empty;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = string.Empty;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>		
		private string m_sDsnName = string.Empty;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty;
		/// <summary>
		/// Represents the save path for Printed Invoices PDF .
		/// </summary>
		private string m_sPdfSavePath = string.Empty ;
        /// <summary>
        /// Represents the dummy Active Report used to merge invoices
        /// </summary>
        public Test objActiveReport;

        private int m_iClientId = 0;

		#endregion
		
		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value.
		/// </summary>
		/// <param name="p_sDsnName">DSN Name</param>
		/// <param name="p_sUserName">User Name</param>
		/// <param name="p_sPassword">Password</param>
        public NoticeManager(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{		
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
            m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);	
			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            m_sPdfSavePath = Function.GetSavePath();
            objActiveReport = new Riskmaster.Application.EnhancePolicy.Billing.Test();
            objActiveReport.Run();
		}
		#endregion

        public void Dispose()
        {
            if (m_objDataModelFactory != null)
            {
                m_objDataModelFactory.Dispose();
            }
            if (objActiveReport != null)
            {
                objActiveReport.Dispose();
            }
        }

        # region  Export Notice

        /// <summary>
        ///Exports Notices to PDF
        /// </summary>
        public void ExportNotice()
        {
            PdfExport objPdfExport = new PdfExport();
            try
            {
                //Removing first page from the dummy Active report used for merging notices
                objActiveReport.Document.Pages.Remove(objActiveReport.Document.Pages[0]);
                objPdfExport.Export(objActiveReport.Document, m_sPdfSavePath + "\\Notices - "
                                    + Common.Conversion.ToDbDate(DateTime.Now) + " - "
                                    + DateTime.Now.TimeOfDay.ToString().Substring(0, 2)
                                    + DateTime.Now.TimeOfDay.ToString().Substring(3, 2)
                                    + DateTime.Now.TimeOfDay.ToString().Substring(6, 2)
                                    + " - " + m_sDsnName
                                    + ".pdf");

            }
            finally
            {
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                }
            }
        }

        # endregion

		#region GL First, GL Other, WC Full Pay, WC Audit, WC Pay Plan First, WC Pay Plan Other :: Methods To Print Report
				
		public void PrintGLNotice( int p_iNoticeId, BillingMaster p_objBillingMaster)
		{
			GLNotice objGLNotice = null ;
			PdfExport objPdfExport = null ;

			string sPdfFileName = string.Empty ;

			try
			{
				// Create Instance Of the Report Class.
				objGLNotice = new GLNotice( m_objDataModelFactory,m_iClientId ); 
				
				// Gather Data.					
				objGLNotice.GatherData( p_iNoticeId , p_objBillingMaster );

				// Run the report.
				objGLNotice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objGLNotice.Document.Pages);
				
				// PDF File Name.
				//sPdfFileName = "GLNotice" + objGLNotice.Notice.BillDate + "PolicyID" + objGLNotice.Notice.PolicyId + ".pdf" ;
				//p_sPDFSaveFilePath = m_sPdfSavePath +  sPdfFileName ;

				// Export the report to the PDF file.
				//objPdfExport = new PdfExport();
				//objPdfExport.Export( objGLNotice.Document, p_sPDFSaveFilePath );
				
				// Attach the PDF to the Invoice.		
                //objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser);
                //objDocManager.ConnectionString = m_sConnectionString;
                //objDocManager.UserLoginName = m_sUserName;
                //objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
                //objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
                //objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
                //sbDocumentXml = new StringBuilder();			
                //sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
                //sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
                //sbDocumentXml.Append( "<Category></Category><Name>GL Notice</Name><Class></Class><Subject>Notice</Subject>" );
                //sbDocumentXml.Append( "<Type></Type>" );
                //sbDocumentXml.Append( "<Notes>Policy Id: " + objGLNotice.Notice.PolicyId.ToString() + "</Notes>" );
                //sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
                //sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
                //sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
                //sbDocumentXml.Append( "<Keywords></Keywords>" );
                //sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
                //sbDocumentXml.Append( "<AttachRecordId>" + objGLNotice.Notice.PolicyId.ToString() + "</AttachRecordId>" );
                //sbDocumentXml.Append( "</Document></data>" ); 						 	
                //objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );
                
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("NoticeManager.PrintGLNotice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objGLNotice != null)
                {
                    objGLNotice.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}

			}
		}

        //Start:Added by Nitin Goel:To generate Invoice for vehicle and property ,06/10/2010
        /// <summary>
        /// To Print Notice for AL
        /// </summary>
        /// <param name="p_iNoticeId">Notice Id</param>
        /// <param name="p_objBillingMaster">Object of Billing Master</param>
        public void PrintALNotice(int p_iNoticeId, BillingMaster p_objBillingMaster)
        {
            ALNotice objALNotice = null;
            PdfExport objPdfExport = null;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objALNotice = new ALNotice(m_objDataModelFactory,m_iClientId);
                // Gather Data.					
                objALNotice.GatherData(p_iNoticeId, p_objBillingMaster);

                // Run the report.
                objALNotice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objALNotice.Document.Pages);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NoticeManager.PrintALNotice.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objALNotice != null)
                {
                    objALNotice.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }

            }
        }

       /// <summary>
        /// To Print Notice for PC
       /// </summary>
        /// <param name="p_iNoticeId">Notice Id</param>
        /// <param name="p_objBillingMaster">Object of Billing Master</param>
        public void PrintPCNotice(int p_iNoticeId, BillingMaster p_objBillingMaster)
        {
            PCNotice objPCNotice = null;
            PdfExport objPdfExport = null;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objPCNotice = new PCNotice(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objPCNotice.GatherData(p_iNoticeId, p_objBillingMaster);

                // Run the report.
                objPCNotice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objPCNotice.Document.Pages);

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NoticeManager.PrintPCNotice.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objPCNotice != null)
                {
                    objPCNotice.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }

            }
        }
        //End:Nitin Goel:To generate Invoice for vehicle and property,06/10/2010




		private void PrintWCNotice( int p_iNoticeId, BillingMaster p_objBillingMaster )
		{
			WCNotice objWCNotice = null ;
			PdfExport objPdfExport = null ;

			string sPdfFileName = string.Empty ;

			try
			{
				// Create Instance Of the Report Class.
				objWCNotice = new WCNotice( m_objDataModelFactory,m_iClientId ); 
				
				// Gather Data.					
				objWCNotice.GatherData( p_iNoticeId , p_objBillingMaster );

				// Run the report.
				objWCNotice.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objWCNotice.Document.Pages);
				
                //// PDF File Name.
                //sPdfFileName = "WCNotice" + objWCNotice.Notice.BillDate + "PolicyID" + objWCNotice.Notice.PolicyId + ".pdf" ;
                //p_sPDFSaveFilePath = m_sPdfSavePath +  sPdfFileName ;

                //// Export the report to the PDF file.
                //objPdfExport = new PdfExport();
                //objPdfExport.Export( objWCNotice.Document, p_sPDFSaveFilePath );
				
                //// Attach the PDF to the Invoice.				
                //objDocManager = new DocumentManager(m_objDataModelFactory.Context.RMUser );
                //objDocManager.ConnectionString = m_sConnectionString;
                //objDocManager.UserLoginName = m_sUserName;
                //objDocManager.DocumentStorageType = StorageType.FileSystemStorage ;
                //objDocManager.DestinationStoragePath = m_objDataModelFactory.Context.RMUser.DocumentPath ; 
                //objAttachDocStream = Function.CreateStream( p_sPDFSaveFilePath );
											
                //sbDocumentXml = new StringBuilder();			
                //sbDocumentXml.Append( "<data><Document><DocumentId></DocumentId><FolderId></FolderId>" );
                //sbDocumentXml.Append( "<CreateDate>"+ Common.Conversion.ToDbDate( DateTime.Now ) + "</CreateDate>" );
                //sbDocumentXml.Append( "<Category></Category><Name>WC Notice</Name><Class></Class><Subject>Notice</Subject>" );
                //sbDocumentXml.Append( "<Type></Type>" );
                //sbDocumentXml.Append( "<Notes>Policy Id: " + objWCNotice.Notice.PolicyId.ToString() + "</Notes>" );
                //sbDocumentXml.Append( "<UserLoginName>" + m_sUserName + "</UserLoginName>" );
                //sbDocumentXml.Append( "<FileName>" + sPdfFileName + "</FileName>" );  
                //sbDocumentXml.Append( "<FilePath>" + m_objDataModelFactory.Context.RMUser.DocumentPath + "</FilePath>");
                //sbDocumentXml.Append( "<Keywords></Keywords>" );
                //sbDocumentXml.Append( "<AttachTable>POLICY_ENH</AttachTable>" );
                //sbDocumentXml.Append( "<AttachRecordId>" + objWCNotice.Notice.PolicyId.ToString() + "</AttachRecordId>" );
                //sbDocumentXml.Append( "</Document></data>" ); 						 	
                //objDocManager.AddDocument( sbDocumentXml.ToString() , objAttachDocStream ,0, out lDocumentId );

			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("NoticeManager.PrintWCNotice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objWCNotice != null)
                {
                    objWCNotice.Dispose();
                }
				if( objPdfExport != null )
				{
					objPdfExport.Dispose();
					objPdfExport = null ;
				}

			}
		}


        //npadhy RMSC retrofit Starts
        private void PrintWCNoticeTax(int p_iNoticeId, BillingMaster p_objBillingMaster)
        {
            WCNoticeTax objWCNoticeTax = null;
            PdfExport objPdfExport = null;
            bool blnReturn=true;

            string sPdfFileName = string.Empty;

            try
            {
                // Create Instance Of the Report Class.
                objWCNoticeTax = new WCNoticeTax(m_objDataModelFactory,m_iClientId);

                // Gather Data.					
                objWCNoticeTax.GatherData(p_iNoticeId, p_objBillingMaster, ref blnReturn);

                // Run the report.
                objWCNoticeTax.Run();

                //Add generated report to dummy report
                objActiveReport.Document.Pages.AddRange(objWCNoticeTax.Document.Pages);
                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("NoticeManager.PrintWCNoticeTax.Error", m_iClientId), p_objEx);
            }
            finally
            {
                if (objWCNoticeTax != null)
                {
                    objWCNoticeTax.Dispose();
                }
                if (objPdfExport != null)
                {
                    objPdfExport.Dispose();
                    objPdfExport = null;
                }

            }
        }

        //npadhy RMSC retrofit Ends
		
		#endregion 

		#region Print Notice
		public void PrintNotice( int p_iNoticeId, BillingMaster p_objBillingMaster)
		{
			BillXNotice objBillXNotice = null ;
			BillXAccount objBillXAccount = null ;
			PolicyEnh objPolicyEnh = null ;
			LocalCache objLocalCache = null ;
			
			string sPolicyType = string.Empty ;
			bool bNoticeFound = false ;

            //Changed by Gagan for MITS 18853 : start
            string seff = string.Empty;
            string sexp = string.Empty;
            string sSQL = string.Empty;

            DbReader objReader = null;
            //Changed by Gagan for MITS 18853 : end


			try
			{
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);

				objBillXNotice = (BillXNotice) p_objBillingMaster.Notices[ p_iNoticeId ] ;
				if( objBillXNotice != null )
				{
					objBillXAccount = ( BillXAccount ) m_objDataModelFactory.GetDataModelObject( "BillXAccount" , false );
					//objBillXAccount.BillAccountRowid = objBillXNotice.BillAccountRowid ;
                    objBillXAccount.MoveTo(objBillXNotice.BillAccountRowid);

					objPolicyEnh = (PolicyEnh) p_objBillingMaster.Policies[ objBillXNotice.PolicyId ];
					if( objPolicyEnh != null )
					{
						sPolicyType = objLocalCache.GetShortCode( objPolicyEnh.PolicyType );
						bNoticeFound = true ;
					}
				}

				if( !bNoticeFound )
				{
					objBillXNotice = ( BillXNotice ) m_objDataModelFactory.GetDataModelObject( "BillXNotice" , false );
                    objBillXNotice.MoveTo(p_iNoticeId);
					//objBillXNotice.NoticeRowid = p_iNoticeId ;
					//objBillXNotice.Refresh();
					p_objBillingMaster.Invoices.Add( p_iNoticeId , objBillXNotice );

					objBillXAccount = ( BillXAccount ) m_objDataModelFactory.GetDataModelObject( "BillXAccount" , false );
					//objBillXAccount.BillAccountRowid = objBillXNotice.BillAccountRowid ;
                    objBillXAccount.MoveTo(objBillXNotice.BillAccountRowid);

					objPolicyEnh = ( PolicyEnh ) m_objDataModelFactory.GetDataModelObject( "PolicyEnh" , false );
                    objPolicyEnh.MoveTo(objBillXNotice.PolicyId);
                    //objPolicyEnh.PolicyId = objBillXNotice.PolicyId ;
					//objPolicyEnh.Refresh();				
					p_objBillingMaster.Policies.Add( objBillXNotice.PolicyId , objPolicyEnh );

					sPolicyType = objLocalCache.GetShortCode( objPolicyEnh.PolicyType );
				}


                //changed by gagan for mits 18853 : Start
                foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                {
                    foreach (PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList)
                    {
                        if (objPolicyXTermEnh.TermNumber == objPolicyXTransEnh.TermNumber && objPolicyXTermEnh.SequenceAlpha == objPolicyXTransEnh.TermSeqAlpha)
                        {
                            seff = objPolicyXTermEnh.EffectiveDate;
                            sexp = objPolicyXTermEnh.ExpirationDate;
                            sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE=" + objPolicyEnh.State + ")"
                                    + "AND EFFECTIVE_DATE <= '" + seff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + seff + "') AND IN_USE_FLAG <> 0";

                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objReader.Read())
                            {
                                p_objBillingMaster.Account.UseTax = true;
                                p_objBillingMaster.Account.TaxRate = Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                                p_objBillingMaster.Account.TaxType = Conversion.ConvertObjToInt(objReader.GetValue("FLAT_OR_PERCENT"), m_iClientId);
                            }
                            break;
                        }
                    }
                }

                if (objReader != null)
                    objReader.Close();

                //changed by gagan for mits 18853 : End


				//objBillXAccount.Refresh();

				if( sPolicyType == "GL" )
					this.PrintGLNotice( p_iNoticeId , p_objBillingMaster);
                //Start:Added by Nitin Goel:To generate Invoice for vehicle and property ,06/10/2010
                else if (sPolicyType == "AL")
                    {
                         this.PrintALNotice(p_iNoticeId, p_objBillingMaster);
                    }
                else if (sPolicyType == "PC")
                    {
                        this.PrintPCNotice(p_iNoticeId, p_objBillingMaster);
                    }
                //End:Nitin Goel:To generate Invoice for vehicle and property,06/10/2010
                else

                    //npadhy RMSC retrofit Starts
                    // We will Print the Invoices corresponding to Tax if UseTax is True.
                    // Otherwise we will print the Legacy Invoice
                    if (p_objBillingMaster.Account.UseTax)
                    {
                        
                        this.PrintWCNoticeTax(p_iNoticeId, p_objBillingMaster); //Animesh Modified RMSC  
                    }
                    else
                    {
                        this.PrintWCNotice(p_iNoticeId, p_objBillingMaster);
                    }
                    //npadhy RMSC retrofit Ends

                Console.WriteLine("0 ^*^*^ Notice Generated for Notice Id {0} and Policy Id {1}", p_iNoticeId , objBillXNotice.PolicyId);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("NoticeManager.PrintNotice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objBillXNotice != null )
				{
					objBillXNotice.Dispose();
					objBillXNotice = null ;
				}
				if( objBillXAccount != null )
				{
					objBillXAccount.Dispose();
					objBillXAccount = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
                if (objReader != null)
                {
                    objReader.Close();
                }

			}
		}


        /// <summary>
        /// Generates single batch pdf for Notices
        /// </summary>
        public void PrintNotice()
        {
            bool bPrinting = false;
            string ssql = String.Empty;

            BillingMaster objBillingMaster = null;            
            WpaDiaryEntry objWpaDiaryEntry = null;
            Db.DbReader objReader = null;
            Db.DbReader objTempReader = null;
            ArrayList objDiaries = new ArrayList();
            ArrayList objNoticesToPrint = new ArrayList();
            bool bError = false;

            string strConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;

            try
            {

                ssql = "SELECT ENTRY_ID , ATTACH_RECORDID FROM WPA_DIARY_ENTRY WHERE ENTRY_NAME = 'Print Notice'"
                        + " AND COMPLETE_DATE <= '" + DateTime.Now.ToString("yyyyMMdd")
                        + "' AND STATUS_OPEN <>0 AND DIARY_VOID = 0 AND DIARY_DELETED = 0 "
                        + " ORDER BY COMPLETE_DATE";

                objReader = DbFactory.GetDbReader(strConnectionString, ssql);

                while (objReader.Read())
                {
                            ssql = "SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + objReader.GetInt(1);

                            objTempReader = DbFactory.GetDbReader(strConnectionString, ssql);

                            if (objTempReader.Read())
                            {
                                objDiaries.Add(objReader.GetInt(0));
                            }
                            else
                            {
                                objWpaDiaryEntry = (WpaDiaryEntry)m_objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);
                                objWpaDiaryEntry.MoveTo(objReader.GetInt(0));
                                objWpaDiaryEntry.StatusOpen = false;
                                objWpaDiaryEntry.Save();
                            }
                            objTempReader.Close();
                        }

                objBillingMaster = new BillingMaster(m_objDataModelFactory,m_iClientId);
                //objNoticeManager = new NoticeManager(base.userLogin.objRiskmasterDatabase.DataSourceName, base.userLogin.LoginName, base.userLogin.Password);

                foreach (object objlngEntryId in objDiaries)
                {
                    bError = false;

                    objWpaDiaryEntry = (WpaDiaryEntry)m_objDataModelFactory.GetDataModelObject("WpaDiaryEntry", false);
                    objWpaDiaryEntry.MoveTo((int)objlngEntryId);
                    objBillingMaster.Diaries.Add(objWpaDiaryEntry.EntryId, objWpaDiaryEntry);
                    //objNoticeManager.CreateNotice(objWpaDiaryEntry, objBillingMaster);
                    try
                    {
                        this.CreateNotice(objWpaDiaryEntry, objBillingMaster);
                    }
                    catch (Exception ex)
                    {
                        bError = true;
                        Console.WriteLine("1001 ^*^*^ {0} ", ex.Message);
                        Log.Write(ex.Message, "CommonWebServiceLog", m_iClientId);                        
                        //TO DO
                        // Log.Write(ex.Message, "Default");
                    }
                    objWpaDiaryEntry.StatusOpen = false;
                    objBillingMaster.Save();

                    if (!bError)
                    {
                        foreach (int objKey in objBillingMaster.Notices.Keys)
                        {
                            if (!objNoticesToPrint.Contains(((BillXNotice)objBillingMaster.Notices
                                [objKey]).NoticeRowid))    //csingh7 06/04/2010 : MITS 18321 
                            {        
                            objNoticesToPrint.Add(((BillXNotice)objBillingMaster.Notices
                                [objKey]).NoticeRowid);
                            break;
                            }         
                        }
                    }
                }

                //Check how many notices need to be printed
                if (objNoticesToPrint.Count == 0)
                {
                    Console.WriteLine("0 ^*^*^ There were no notices to be printed");
                    return;
                }

                //Generate/Print report for each notice           
                foreach (object objNoticeRowId in objNoticesToPrint)
                {
                    bPrinting = true;
                    //objNoticeManager.PrintNotice((int)objNoticeRowId, objBillingMaster);
                    try
                    {
                        this.PrintNotice((int)objNoticeRowId, objBillingMaster);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("1001 ^*^*^ {0} ", ex.Message);
                        Log.Write(ex.Message, "CommonWebServiceLog", m_iClientId);    
                        //TO DO
                        //Log.Write(ex.Message, "Default");
                    }
                }

                //Export notices to a single batch pdf
                //objNoticeManager.ExportNotice();
                this.ExportNotice();

            

            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objWpaDiaryEntry != null)
                {
                    objWpaDiaryEntry.Dispose();
                }
                if (objBillingMaster != null)
                {
                    objBillingMaster.Dispose();
                }
                //objNoticeManager = null;

                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                objDiaries = null;
                objNoticesToPrint = null;
            }
        }

		#endregion

		#region Create Invoice
		public void CreateNotice( WpaDiaryEntry p_objDiary , BillingMaster p_objBillingMaster )
		{
			BillingManager objBillingManager = null ;
			BillXNotice objLastNotice = null ;
			BillXInvoice objLastInvoice = null ;
			BillXNotice objNewNotice = null ;
			BillXAccount objAccount = null ;
			PolicyEnh objPolicyEnh = null ;
			BillXBillItem objBillXBillItem = null ;
			DbReader objReader = null ;
			DbReader objReaderNext = null ;
			LocalCache objLocalCache = null ;
			StringBuilder sbSQL = null ;
			BillXNoticeDet objBillXNoticeDet = null ;
			
			string sSQL = string.Empty ;
			string sBillItemType = string.Empty ;
			string sAdjustmentType = string.Empty ;
			string sLastDate = string.Empty ;
			string sDueDate = string.Empty ;         

			int iPolicyId = 0 ;
			int iBIRowId = 0 ;
			int iTermNumber = 0 ;
			int iLastInvoiceRowid = 0 ;
			int iLastNoticeRowid = 0 ;
			bool bHoldAmt = false ;
			double dblHoldAmt = 0.0 ;
			double dblTotalCredits = 0.0 ;
			double dblTotalOutstandingInvoiceBalance = 0.0  ;
			double dblCurrentCharges = 0.0 ;
			double dblPolicyTotal = 0.0 ;

            //Changed by Gagan for mits 12463: Start
 
            double dBilledPremium = 0;            
            double dPreviousBalance = 0;
            string sDateEntered = "";
            //long lBillingItemId = 0;       Commented by csingh7 : MITS 19364
            int iBillingItemId = 0;
            long lNotId = 0;
            ArrayList collBatchId = new ArrayList();
            
            string sStatus = "";
            long lEntryBatchNum = 0;
            long lFinalBatchNum = 0;
            bool bFound = false;
            double dCredits =0;
            //Dim v As Variant

            //Changed by Gagan for mits 12463: End
            //npadhy RMSC retrofit Starts
            double dTotalStateTax = 0;
            BillingRule  objBillingRule = null;
            bool bNoticeForNewInstlmnt;
            //npadhy RMSC retrofit Ends

            // MITS 19364 : Start
            int iCancelledTaxBillingItemRowId = 0;
            int iMaxTransId = 0;
            bool bCancel = false;
            // MITS 19364 : End

			try
			{
                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
				objBillingManager = new BillingManager( m_objDataModelFactory ,m_iClientId);

				objLastNotice = ( BillXNotice ) m_objDataModelFactory.GetDataModelObject( "BillXNotice" , false );
				objLastInvoice = ( BillXInvoice ) m_objDataModelFactory.GetDataModelObject( "BillXInvoice" , false );
				objNewNotice = ( BillXNotice ) m_objDataModelFactory.GetDataModelObject( "BillXNotice" , false );
				objAccount = ( BillXAccount ) m_objDataModelFactory.GetDataModelObject( "BillXAccount" , false );
				objPolicyEnh = ( PolicyEnh ) m_objDataModelFactory.GetDataModelObject( "PolicyEnh" , false );
			
				iPolicyId = p_objDiary.AttachRecordid ;
				iBIRowId = p_objDiary.AttSecRecId ;
                
                // csingh7 : MITS 19351 commented
                //if( p_objDiary.Regarding.Length > 1 && Common.Conversion.IsNumeric( p_objDiary.Regarding.Substring( p_objDiary.Regarding.Length - 2 ) ) )
                //{
                //    iTermNumber = Common.Conversion.ConvertStrToInteger( p_objDiary.Regarding.Substring( p_objDiary.Regarding.Length - 2 ));
                //}
                //else
                //{
                //    sSQL = " SELECT TERM_NUMBER FROM BILL_X_BILL_ITEM WHERE BILLING_ITEM_ROWID=" + iBIRowId ;
                //    objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                //    if( objReader.Read() )
                //    {
                //        iTermNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TERM_NUMBER" ), m_iClientId );
                //    }
                //    objReader.Close();
                //}

                // csingh7 : MITS 19351 Added
                sSQL = " SELECT TERM_NUMBER FROM BILL_X_BILL_ITEM WHERE BILLING_ITEM_ROWID=" + iBIRowId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader.Read())
                {
                    iTermNumber = Common.Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), m_iClientId);
                }
                objReader.Close();

                if (iTermNumber == 0)   // if term number is not available from Bill_X_Bill_Item ,it will be extracted from 'Regarding' field. 
                {
                    if (p_objDiary.Regarding.Length > 1)
                    {
                        if (p_objDiary.Regarding.Contains("Term Number") && Common.Conversion.IsNumeric(p_objDiary.Regarding.Substring(p_objDiary.Regarding.Length - 2)))
                        {
                            iTermNumber = Common.Conversion.ConvertStrToInteger(p_objDiary.Regarding.Substring(p_objDiary.Regarding.Length - 2));
                        }
                        else    // Regarding field length is 70 , if term number not found even in 'Regarding' field , throw new exception.
                        {
                            throw new RMAppException(Globalization.GetString("NoticeManager.CreateNotice.InvalidTermNumber",m_iClientId));//psharma206  
                        }
                    }
                    else
                    {
                        throw new RMAppException(Globalization.GetString("NoticeManager.CreateNotice.InvalidTermNumber", m_iClientId));//psharma206 
                    }
                }
                // csingh7 : MITS 19351 End

                objNewNotice.NoticeRowid = Common.Utilities.GetNextUID(m_sConnectionString, "BILL_X_NOTICE", m_iClientId);
				p_objBillingMaster.Notices.Add( objNewNotice.NoticeRowid , objNewNotice );

				objNewNotice.PolicyId = iPolicyId ;
				objNewNotice.TermNumber = iTermNumber ;
				objNewNotice.BillDate = Common.Conversion.GetDate( DateTime.Now.ToString("d") );
			
				// Retrieve the billing address entity id
                objPolicyEnh.MoveTo(iPolicyId);
				//objPolicyEnh.PolicyId = iPolicyId ;
				//objPolicyEnh.Refresh();                
                if (!p_objBillingMaster.Policies.ContainsKey(objPolicyEnh.PolicyId))
				    p_objBillingMaster.Policies.Add( objPolicyEnh.PolicyId , objPolicyEnh );

				foreach( PolicyXBillEnh objPolicyXBillEnh in objPolicyEnh.PolicyXBillEnhList )
				{
					if( objPolicyXBillEnh.TermNumber == iTermNumber )
					{
						if( objPolicyXBillEnh.BillToOverride )
							objNewNotice.HierarchyLevel = objPolicyXBillEnh.NewBillToEid ;
						else
							objNewNotice.HierarchyLevel = objPolicyXBillEnh.BillToEid;
						break;
					}
				}
            // MITS 19364 : Start

                foreach (PolicyXTransEnh objPolicyXTransEnhd in objPolicyEnh.PolicyXTransEnhList)
                {
                    if (iMaxTransId < objPolicyXTransEnhd.TransactionId)
                        iMaxTransId = objPolicyXTransEnhd.TransactionId;
                }
                PolicyXTransEnh objPolicyTrans = (PolicyXTransEnh)m_objDataModelFactory.GetDataModelObject("PolicyXTransEnh", false); ;
                objPolicyTrans.MoveTo(iMaxTransId);
                if (objLocalCache.GetShortCode(objPolicyTrans.TransactionType) == "CF" || objLocalCache.GetShortCode(objPolicyTrans.TransactionType) == "CPR")
                {
                    bCancel = true;
                }
            // MITS 19364 : End
				// Find the last invoice sent
				sSQL = " SELECT MAX(INVOICE_ROWID) FROM BILL_X_INVOICE WHERE POLICY_ID = " + iPolicyId + 
					" AND TERM_NUMBER = " + iTermNumber ;

				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					iLastInvoiceRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );
				}
				objReader.Close();

				if( iLastInvoiceRowid != 0 )
				{
					sSQL = "SELECT * FROM BILL_X_INVOICE WHERE INVOICE_ROWID = " + iLastInvoiceRowid ;
					objBillingManager.FillInvoice( objLastInvoice , sSQL );
				}
			
				// Find the last notice sent
				sSQL = " SELECT MAX(NOTICE_ROWID) FROM BILL_X_NOTICE WHERE POLICY_ID = " 
					+ iPolicyId + " AND TERM_NUMBER = " + iTermNumber ;
			
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					iLastNoticeRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );
				}
				objReader.Close();

				if( iLastNoticeRowid != 0 )
				{
					sSQL = " SELECT * FROM BILL_X_NOTICE WHERE NOTICE_ROWID = " + iLastNoticeRowid ;
					objBillingManager.FillNotice( objLastNotice , sSQL );
                    //***************************************
                    //npadhy RMSC retrofit Starts
                    sSQL = "SELECT BILLING_ITEM_ROWID FROM BILL_X_BILL_ITEM WHERE NOTICE_ID = " + iLastNoticeRowid ;
                    objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
                    while (objReader.Read())
                    {
                        if (iBIRowId == Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId))
                        {
                            bNoticeForNewInstlmnt = false;
                            break;
                        }
                        else
                        {
                            bNoticeForNewInstlmnt = true;  
                        }
                    }
                    objReader.Close();  
				}

				// Compare the dates of the notice and invoice to see which to use
                //if( objLastNotice.NoticeRowid == 0 || Conversion.ConvertStrToInteger( objLastInvoice.DttmRcdAdded ) > Conversion.ConvertStrToInteger( objLastNotice.DttmRcdAdded ) )
                //    objNewNotice.PrevInvoiceRowid = objLastInvoice.InvoiceRowid ;
                //else
                //    objNewNotice.PrevNoticeRowid = objLastNotice.NoticeRowid ;

                if (objLastNotice.NoticeRowid != 0)
                {
                    objNewNotice.PrevNoticeRowid = objLastNotice.NoticeRowid;   
                }
                if (objLastInvoice.InvoiceRowid != 0)
                {
                    objNewNotice.PrevInvoiceRowid = objLastInvoice.InvoiceRowid;   
                }
                //npadhy RMSC retrofit Ends

				// Get the bill account record
				sSQL = " SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER = " + iTermNumber ;
				objBillingManager.FillAccount( objAccount , sSQL );
		    
				objNewNotice.BillAccountRowid = objAccount.BillAccountRowid ;

                //Changed by Gagan for mits 12463: Start

                //// Find the total of the credits
                //sbSQL = new StringBuilder();
                //sbSQL.Append( " SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId + " AND TERM_NUMBER=" + iTermNumber );
                //sbSQL.Append( " AND INVOICE_ID=0 AND NOTICE_ID=0" );
                //sbSQL.Append( " AND BILLING_ITEM_TYPE <>" + objLocalCache.GetCodeId( "P", "BILLING_ITEM_TYPES") );
                //sbSQL.Append( " AND BILLING_ITEM_TYPE <>" + objLocalCache.GetCodeId( "D", "BILLING_ITEM_TYPES") );
                //sbSQL.Append( " AND AMOUNT <0" );

                //objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
                //while( objReader.Read() )
                //{
                //    sBillItemType = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_TYPE" ), m_iClientId ) );
                //    if( sBillItemType == "A" )
                //    {
                //        bHoldAmt = false ;
                //        sSQL = "SELECT * FROM BILL_X_ADJSTMNT WHERE BILLING_ITEM_ROWID=" 
                //            + Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
					
                //        objReaderNext = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                //        if( objReaderNext.Read() )
                //        {
                //            sAdjustmentType = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReaderNext.GetValue("ADJUSTMENT_TYPE"), m_iClientId));
                //            if( sAdjustmentType != "OP" && sAdjustmentType != "BI" )
                //            {
                //                if( sAdjustmentType == "DV" )
                //                {
                //                    bHoldAmt = true ;
                //                    dblHoldAmt = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //                }
                //                else
                //                {
                //                    dblTotalCredits += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //                    objBillXNoticeDet = ( BillXNoticeDet ) m_objDataModelFactory.GetDataModelObject( "BillXNoticeDet" , false );
                //                    objBillXNoticeDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //                    objNewNotice.BillXNoticeDetList.Add( objBillXNoticeDet );
                //                }
                //            }
                //        }
                //        objReaderNext.Close();

                //        if( bHoldAmt )
                //        {
                //            sSQL = " SELECT * FROM BILL_X_INVOICE_DET WHERE BILL_ITEM_ID=" 
                //                + Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
						
                //            objReaderNext = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                //            if( objReaderNext.Read() )
                //            {
                //                dblHoldAmt = 0.0 ;
                //            }
                //            else
                //            {
                //                objBillXNoticeDet = ( BillXNoticeDet ) m_objDataModelFactory.GetDataModelObject( "BillXNoticeDet" , false );
                //                objBillXNoticeDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //                objNewNotice.BillXNoticeDetList.Add( objBillXNoticeDet );
                //            }
                //            objReaderNext.Close();
                //        }
                //        dblTotalCredits += dblHoldAmt ;
                //        dblHoldAmt = 0.0 ;
                //    }
                //    else
                //    {
                //        dblTotalCredits += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //        objBillXNoticeDet = ( BillXNoticeDet ) m_objDataModelFactory.GetDataModelObject( "BillXNoticeDet" , false );
                //        objBillXNoticeDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //        objNewNotice.BillXNoticeDetList.Add( objBillXNoticeDet );
                //    }
                //}
                //objReader.Close();

                //if( objNewNotice.PrevInvoiceRowid != 0 )
                //{
                //    dblTotalOutstandingInvoiceBalance = objLastInvoice.Amount + dblTotalCredits ;
                //    objNewNotice.PreviousBalance = objLastInvoice.Amount ;
                //}
                //else
                //{
                //    if( objNewNotice.PrevNoticeRowid != 0 )
                //    {
                //        dblTotalOutstandingInvoiceBalance = objLastNotice.Amount + dblTotalCredits ;
                //        objNewNotice.PreviousBalance = objLastNotice.Amount ;
                //    }
                //}
			
                //// Find the current charges
                //sbSQL.Remove(0,sbSQL.Length);
                //sbSQL.Append( " SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId );
                //sbSQL.Append( " AND TERM_NUMBER = " + iTermNumber );
                //sbSQL.Append( " AND (AMOUNT > 0 OR BILLING_ITEM_TYPE=" + objLocalCache.GetCodeId("P", "BILLING_ITEM_TYPES") + ")" );
                //sbSQL.Append( " AND INVOICE_ID =0 AND NOTICE_ID=0" );

                //objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
                //while( objReader.Read() )
                //{
                //    sBillItemType = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_TYPE" ), m_iClientId ) );
                //    if( sBillItemType == "A" )
                //    {
                //        bHoldAmt = false ;
                //        sSQL = "SELECT * FROM BILL_X_ADJSTMNT WHERE BILLING_ITEM_ROWID=" 
                //            + Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
					
                //        objReaderNext = DbFactory.GetDbReader( m_sConnectionString , sSQL );
                //        if( objReaderNext.Read() )
                //        {
                //            sAdjustmentType = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReaderNext.GetValue( "ADJUSTMENT_TYPE" ), m_iClientId ) );
                //            if( sAdjustmentType != "UP" && sAdjustmentType != "BI" )
                //            {
                //                dblCurrentCharges += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //                objBillXNoticeDet = ( BillXNoticeDet ) m_objDataModelFactory.GetDataModelObject( "BillXNoticeDet" , false );
                //                objBillXNoticeDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //                objNewNotice.BillXNoticeDetList.Add( objBillXNoticeDet );							
                //            }
                //        }
                //        objReaderNext.Close();					
                //    }
                //    else
                //    {
                //        dblCurrentCharges += Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT" ) ));
                //        objBillXNoticeDet = ( BillXNoticeDet ) m_objDataModelFactory.GetDataModelObject( "BillXNoticeDet" , false );
                //        objBillXNoticeDet.BillItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_ROWID" ), m_iClientId );
                //        objNewNotice.BillXNoticeDetList.Add( objBillXNoticeDet );
                //    }
                //}
                //objReader.Close();

                //if( objNewNotice.PrevInvoiceRowid == 0 && objNewNotice.PrevNoticeRowid == 0 )
                //    dblPolicyTotal = dblCurrentCharges + dblTotalCredits ;
                //else
                //    dblPolicyTotal = dblCurrentCharges + dblTotalOutstandingInvoiceBalance ;


                //Moving RMSC changes to Base
                sbSQL = new StringBuilder();
                sbSQL.Remove(0,sbSQL.Length);
                sbSQL.Append("SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " +   iPolicyId.ToString()  + " AND TERM_NUMBER=" + iTermNumber.ToString());
                sbSQL.Append(" AND BILLING_ITEM_TYPE = " + objLocalCache.GetCodeId("P", "BILLING_ITEM_TYPES"));
                sbSQL.Append(" ORDER BY DATE_ENTERED ASC");

                string sMaxDate  = "";
                objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );
                while( objReader.Read() )
                {            
                    sMaxDate = Common.Conversion.ConvertObjToStr( objReader.GetValue( "DATE_ENTERED" ) );
                    
                }
                
                objReader.Close();               
    
                //Find the final batch numbers for the latest premium
                sbSQL.Remove(0,sbSQL.Length);
                sbSQL.Append("SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId.ToString() + " AND TERM_NUMBER=" + iTermNumber.ToString()); 
                sbSQL.Append(" ORDER BY BILLING_ITEM_ROWID");               
    
                objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );       

                while (objReader.Read())
                {
                    sBillItemType = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_ITEM_TYPE" ), m_iClientId));
                    
                    sDateEntered = Common.Conversion.ConvertObjToStr( objReader.GetValue("DATE_ENTERED"));
            // MITS 19364 : Start

                    //lBillingItemId = Common.Conversion.ConvertObjToInt64( objReader.GetValue("BILLING_ITEM_ROWID")); changed by csingh7 : MITS 19364
                    iBillingItemId = Common.Conversion.ConvertObjToInt( objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId); 
            // MITS 19364 : End


                    sStatus = objLocalCache.GetShortCode( Common.Conversion.ConvertObjToInt(objReader.GetValue( "STATUS" ), m_iClientId));

                    lEntryBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("ENTRY_BATCH_NUM"), m_iClientId);

                    lFinalBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("FINAL_BATCH_NUM"), m_iClientId);

                    lNotId = Common.Conversion.ConvertObjToInt64(objReader.GetValue("NOTICE_ID"), m_iClientId);
                    
                    if (sBillItemType == "P" && sDateEntered == sMaxDate) 
                    {
                        if(sStatus == "RC")
                        {
                            bFound = false;
                            foreach(object v in collBatchId) 
                            {
                                if(lFinalBatchNum == Convert.ToInt64(v))
                                    {                               
                                    bFound = true;
                                }                                
                            }                            

                            if(bFound == false)
                            {
                                collBatchId.Add(lFinalBatchNum);
                            }                            
                        }
                    }
//npadhy RMSC retrofit Starts
                    else if (sBillItemType == "TAX" && sDateEntered == sMaxDate)
                    {
                        if (sStatus == "RC")
                        {
                            bFound = false;
                            foreach (object v in collBatchId)
                            {
                                if (lFinalBatchNum == Convert.ToInt64(v))
                                {
                                    bFound = true;  
                                }
                            }
                            if (bFound == false)
                            {
                                collBatchId.Add(lFinalBatchNum);
                            }
                        }
                    }
                }
 //npadhy RMSC retrofit Ends
                objReader.Close();
                sbSQL.Remove(0, sbSQL.Length);
                sbSQL.Append("SELECT * FROM BILL_X_BILL_ITEM WHERE POLICY_ID = " + iPolicyId.ToString() + " AND TERM_NUMBER=" + iTermNumber.ToString());
                sbSQL.Append(" ORDER BY BILLING_ITEM_ROWID");    
    
                dBilledPremium = 0;                
                dPreviousBalance = 0;
 //npadhy RMSC retrofit Starts
                dTotalStateTax = 0;
//npadhy RMSC retrofit Ends

                objReader = DbFactory.GetDbReader( m_sConnectionString , sbSQL.ToString() );

                while (objReader.Read())
                {
                    sBillItemType = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_TYPE"), m_iClientId));
                    
                    sDateEntered = Common.Conversion.ConvertObjToStr( objReader.GetValue("DATE_ENTERED"));

            // MITS 19364 : Start

                    //lBillingItemId = Common.Conversion.ConvertObjToInt64( objReader.GetValue("BILLING_ITEM_ROWID")); changed by csingh7 : MITS 19364
                    iBillingItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId); 
            // MITS 19364 : End

                    sStatus = objLocalCache.GetShortCode(Common.Conversion.ConvertObjToInt(objReader.GetValue("STATUS"), m_iClientId));

                    lEntryBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("ENTRY_BATCH_NUM"), m_iClientId);

                    lFinalBatchNum = Common.Conversion.ConvertObjToInt64(objReader.GetValue("FINAL_BATCH_NUM"), m_iClientId);

                    lNotId = Common.Conversion.ConvertObjToInt64(objReader.GetValue("NOTICE_ID"), m_iClientId);


            // MITS 19364 : Start

                    if (bCancel)
                    {
                        iBillingItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId);
                        objBillXBillItem = (BillXBillItem)m_objDataModelFactory.GetDataModelObject("BillXBillItem", false);
                        objBillXBillItem.MoveTo(iBillingItemId);
                        
                        if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "P" && objLocalCache.GetShortCode(objBillXBillItem.BillXPremItem.PremiumItemType) == "CP")
                        {
                            dBilledPremium = dBilledPremium + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            iCancelledTaxBillingItemRowId = iBillingItemId + 1;  // for calculating cancelled premium tax                         
                        }
                        else if (objLocalCache.GetShortCode(objBillXBillItem.BillingItemType) == "TAX" && iBillingItemId == iCancelledTaxBillingItemRowId)
                        {
                            dTotalStateTax = dTotalStateTax + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        else if ((sBillItemType == "P" || sBillItemType == "TAX") && sStatus != "RC")
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        else if (sBillItemType == "A" && lEntryBatchNum != 0 && sStatus != "RC")
                        {
                            bFound = false;
                            foreach (object v in collBatchId)
                            {
                                if (lFinalBatchNum == Convert.ToInt64(v))
                                {
                                    bFound = true;
                                }
                            }

                            if (bFound == false)
                            {
                                dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            }
                        }
                        //Any unreconciled adjustment that has been manually entered in the system
                        //by the user needs to be reflected as previous balance        
                        else if (sBillItemType == "A" && lEntryBatchNum == 0 && sStatus != "RC")
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }

            // MITS 19364 : End


                    }
                    else
                    {
                        //Latest Premium Items with Date Entered among the latest generated premiums
                        // and (whose invoice hasnt been generated or have billing item rowid same as
                        //billing item for which this installment diary was generated)       
                        if (sBillItemType == "P" && sDateEntered == sMaxDate)
                        {
                            dBilledPremium = dBilledPremium + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        //npadhy RMSC retrofit Starts
                        //Latest Tax Items with Date Entered among the latest generated taxes
                        //and (whose invoice hasnt been generated or have billing item rowid same as
                        //billing item for which this installment diary was generated)      
                        else if (sBillItemType == "TAX" && sDateEntered == sMaxDate)
                        {
                            dTotalStateTax = dTotalStateTax + Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }
                        //npadhy RMSC retrofit Ends

                         //Any premium item that hasn't been reconciled yet comes as a previous balance
                        //npadhy RMSC retrofit Starts
                        else if ((sBillItemType == "P" || sBillItemType == "TAX") && sStatus != "RC")  //Animesh Modified RMSC 
                        //npadhy RMSC retrofit Ends
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }


                        //do not include adjustments in Previous Balance generated as a result of
                        //reconciliation of premium items shown in billed Premium. Include those which
                        //are generated as a result of previous reconciliation
                        else if (sBillItemType == "A" && lEntryBatchNum != 0 && sStatus != "RC")
                        {
                            bFound = false;
                            foreach (object v in collBatchId)
                            {
                                if (lFinalBatchNum == Convert.ToInt64(v))
                                {
                                    bFound = true;
                                }
                            }

                            if (bFound == false)
                            {
                                dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            }
                        }


                        //Any unreconciled adjustment that has been manually entered in the system
                        //by the user needs to be reflected as previous balance        
                        else if (sBillItemType == "A" && lEntryBatchNum == 0 && sStatus != "RC")
                        {
                            dPreviousBalance = dPreviousBalance + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                        }

                        //Calculating Credits
                        //Credits include all reconciled items which are in the latest batch containing current premium or tax items
                        //Adjustments - if entered manually
                        //Adjustments - generated as a result of previous reconciliation
                        //Reciepts
                        //Disbursements
                        //'Note: System generated Adjustments of type Balance Item (where entry batch num = final batch num) should not
                        //be included in the credits.

    //npadhy RMSC retrofit Starts
                        else if (sStatus == "RC" && (sBillItemType != "P" || (sBillItemType == "P" && sDateEntered != sMaxDate)) && (sBillItemType != "TAX" || (sBillItemType == "TAX" && sDateEntered != sMaxDate)) && lEntryBatchNum != lFinalBatchNum)
                        {

                            bFound = false;
                            foreach (object v in collBatchId)
                            {
                                if (lFinalBatchNum == Convert.ToInt64(v))
                                {
                                    bFound = true;
                                }
                            }

                            if (bFound == true)
                            {
                                dCredits = dCredits + Common.Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                            }
                        }
                        //npadhy RMSC retrofit Ends
                    }
                    objBillXNoticeDet = ( BillXNoticeDet) m_objDataModelFactory.GetDataModelObject( "BillXNoticeDet" , false );
                    objBillXNoticeDet.BillItemId = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILLING_ITEM_ROWID"), m_iClientId);
                    objNewNotice.BillXNoticeDetList.Add(objBillXNoticeDet);      
    
                }
                objReader.Close();
                
			
				// Find all the billing items that are on this invoice
				foreach( BillXNoticeDet objBillXNoticeDetTemp in objNewNotice.BillXNoticeDetList )
				{
					objBillXBillItem = ( BillXBillItem ) m_objDataModelFactory.GetDataModelObject( "BillXBillItem" , false );
					sSQL = " SELECT * FROM BILL_X_BILL_ITEM WHERE BILLING_ITEM_ROWID=" + objBillXNoticeDetTemp.BillItemId ;
					objBillingManager.FillBillingItem( objBillXBillItem , sSQL );

					if( objLocalCache.GetShortCode( objBillXBillItem.BillingItemType ) == "P" )
					{
						if( sLastDate == "" || Common.Conversion.ConvertStrToInteger( objBillXBillItem.DateEntered ) > Common.Conversion.ConvertStrToInteger( sLastDate ) )
						{
							sDueDate = objBillXBillItem.DueDate ;
							sLastDate = objBillXBillItem.DueDate ;
						}
					}
					objBillXBillItem.NoticeId = objNewNotice.NoticeRowid ;
                    //Deb MITS 31622 
                    if (!p_objBillingMaster.BillingItems.ContainsKey(objBillXNoticeDetTemp.BillItemId))
                    {
                        p_objBillingMaster.BillingItems.Add(objBillXNoticeDetTemp.BillItemId, objBillXBillItem);
                    }
                    //Deb MITS 31622
				}
	
				if( sDueDate == "" )
				{
					sDueDate = Common.Conversion.GetDate( DateTime.Now.ToString("d") );
				}
                //npadhy RMSC retrofit Starts 
                objBillingRule = new BillingRule();  
                sSQL="SELECT * FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID = " + objAccount.BillingRuleRowid ;
                objBillingManager.FillBillingRule(objBillingRule, sSQL);
                objNewNotice.PreviousBalance = dPreviousBalance;
                objNewNotice.Payments = dCredits;
                objNewNotice.CurrentCharges = dBilledPremium;
                objNewNotice.CurrentTaxCharges = dTotalStateTax; //Animesh Inserted RMSC   
                objNewNotice.OutstandingBalance = dPreviousBalance;
                objNewNotice.Amount = Math.Round(dPreviousBalance, 2, MidpointRounding.AwayFromZero) + Math.Round(dBilledPremium, 2, MidpointRounding.AwayFromZero)
                    + Math.Round(dCredits, 2, MidpointRounding.AwayFromZero) + Math.Round(dTotalStateTax, 2, MidpointRounding.AwayFromZero);


				//objNewNotice.DueDate = sDueDate ; //Animesh Commented RMSC 
                objNewNotice.DueDate = Conversion.GetDate(Conversion.ToDate(p_objDiary.CompleteDate).AddDays(objBillingRule.DueDays).ToString("d"));  //RMSC Animesh Inserted 
				//objNewNotice.Amount = dblPolicyTotal ;
				//objNewNotice.Payments = dblTotalCredits ;
				//objNewNotice.OutstandingBalance = dblTotalOutstandingInvoiceBalance ;
				//objNewNotice.CurrentCharges = dblCurrentCharges ;
				objNewNotice.PolicyNumber = objAccount.PolicyNumber ;
//npadhy RMSC retrofit Ends

                //Changed by Gagan for mits 12463: End
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("NoticeManager.CreateNotice.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objLastNotice != null )
				{
					objLastInvoice.Dispose();
					objLastInvoice = null ;
				}
				if( objLastInvoice != null )
				{
					objLastInvoice.Dispose();
					objLastInvoice = null ;
				}
				if( objNewNotice != null )
				{
					objNewNotice.Dispose();
					objNewNotice = null ;
				}
				if( objAccount != null )
				{
					objAccount.Dispose();
					objAccount = null ;
				}
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
				if( objReaderNext != null )
				{
					objReaderNext.Close();
					objReaderNext.Dispose();
					objReaderNext = null ;
				}
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
				if( objBillXNoticeDet != null )
				{
					objBillXNoticeDet.Dispose();
					objBillXNoticeDet = null ;
				}
                if (objBillXBillItem != null)
                {
                    objBillXBillItem.Dispose();
                    objBillXBillItem = null;
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
//npadhy RMSC retrofit Starts
                if (objBillingRule != null)
                {
                    objBillingRule = null; 
                }
//npadhy RMSC retrofit Ends
				sbSQL = null ;
			}
		}

		#endregion 
	}
}
