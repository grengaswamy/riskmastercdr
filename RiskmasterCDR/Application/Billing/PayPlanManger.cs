using System;
using System.IO ;
using System.Xml ;
using System.Text ;
using System.Collections ;
using Riskmaster.Common ;
using Riskmaster.Db ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.DataModel ;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: PayPlanManger.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Vaibhav/Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class PayPlanManger : IDisposable
	{		
		#region Member Variables
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty ;	
		/// <summary>
		/// Private variable to store the instance of Local Cache object
		/// </summary>
		private LocalCache m_objLocalCache = null ;
        private int m_iClientId = 0;
		#endregion 		

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value, and Initialize the Report.
		/// </summary>
		/// <param name="p_objDataModelFactory">DataModelFactory Object</param>
		public PayPlanManger( DataModelFactory p_objDataModelFactory,int p_iClientId )
		{
			m_objDataModelFactory = p_objDataModelFactory ;
            m_iClientId = p_iClientId;
			m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString ;
            m_objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
		}
		#endregion 

        public void Dispose()
        {
            if (m_objLocalCache != null)
            {
                m_objLocalCache.Dispose();
            }
        }
		public void CreatePayPlanForPolicy( BillingMaster p_objBillingMaster, int p_iTransNumber , string p_sType )
		{
			PolicyEnh objPolicyEnh = null ;
			PayPlan objPayPlan = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			BillXInstlmnt objBillXInstlmntTemp = null ;
			ArrayList arrlistInstallments = null ;
			DiaryManager objDiaryManager = null ;
			BillingManager objBillingManager = null ;


			bool bRound = false ;
			string sSQL = string.Empty ;
			double dblPremium = 0.0 ;
			double dblRemaining = 0.0 ;
			int iInstallCount = 0 ;
			int iInstallCountTemp = 0 ;
			double dblInstallAmount = 0.0 ;
			int iInstallType = 0 ;
			int iInstallLagDays = 0 ;
			int iInstallDueDays = 0 ;
            //npadhy RMSC retrofit Starts
            string strGenDate = string.Empty;
            string strDueDate = string.Empty; 
            //npadhy RMSC retrofit Ends
			bool bSkipAll = false ;
			string sInstallDate = string.Empty ;
			string sOffsetDate = string.Empty ;
			int iInstallDay = 0 ;
			int iFixedDay = 0 ;
            //Changed  by Gagan for mits 16513 : Start
            string sPolicyExpirationDate = "";
            //Changed  by Gagan for mits 16513 : End
            //npadhy RMSC retrofit Starts
            string strComparisonDate = string.Empty;
            string strDateToUse = string.Empty; 
            double dInstallTaxAmt;
            double dTaxAmount;
            double dTaxRate;
            string sTaxType=string.Empty;
            long lInstlCount;
            string seff=string.Empty;  
            string sexp=string.Empty;
            DbReader objReader = null;
            
             //npadhy RMSC retrofit Ends

			try
			{
                objDiaryManager = new DiaryManager(m_objDataModelFactory, m_iClientId);
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);

                bRound = Function.GetRoundAmountFlag(m_sConnectionString, m_iClientId);
                //npadhy RMSC retrofit Starts
                dInstallTaxAmt = 0;
                dTaxAmount = 0;
                dTaxRate = 0;
                //npadhy RMSC retrofit Ends
				
				if( p_sType == "NB" || p_sType == "RN" )
				{
					foreach( int iKey in p_objBillingMaster.Policies.Keys )
					{
						objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
						if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
						{
							sSQL = " SELECT * FROM SYS_BILL_PAY_PLAN WHERE PAY_PLAN_ROWID=" + p_objBillingMaster.Account.PayPlanRowid
								+	" AND LINE_OF_BUSINESS=" + objPolicyEnh.PolicyType + " AND STATE=" + objPolicyEnh.State ;
							
							objPayPlan = new PayPlan();
							objBillingManager.FillPayPlan( objPayPlan , sSQL );

                            //Changed  by Gagan for mits 16513 : Start

                            //Finding policy expiration date
                            PolicyXTransEnh objPolicyXTransEnhTmp= null;
                            foreach (PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList)
                            {
                                objPolicyXTransEnhTmp = objPolicyXTransEnh;
                                //npadhy RMSC retrofit Starts

                                if (objPolicyXTransEnh.TermNumber == p_objBillingMaster.Account.TermNumber)
                                {
                                    seff = objPolicyXTransEnh.EffectiveDate;
                                    sexp = objPolicyXTransEnh.ExpirationDate;   
                                }
                                //npadhy RMSC retrofit Ends
                            }

                            //Changed by Gagan for MITS 20765 : Start
                            //if(objPolicyXTransEnhTmp != null)
                            //    sPolicyExpirationDate = objPolicyXTransEnhTmp.ExpirationDate;
                            sPolicyExpirationDate = sexp;
                            //Changed by Gagan for MITS 20765 : End

                            //Changed  by Gagan for mits 16513 : End
                            
                            //npadhy RMSC retrofit Starts
                            //sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS= " + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE= "
                            //       + objPolicyEnh.State + ") AND EFFECTIVE_DATE <= '" + seff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + seff + "') AND IN_USE_FLAG = 1";
                            sSQL = "SELECT * FROM SYS_POL_TAX WHERE (LINE_OF_BUSINESS=0 OR LINE_OF_BUSINESS= " + objPolicyEnh.PolicyType + ") AND (STATE=0 OR STATE= "
                            + objPolicyEnh.State + ") AND EFFECTIVE_DATE <= '" + seff + "' AND (EXPIRATION_DATE IS NULL OR EXPIRATION_DATE >= '" + seff + "') AND IN_USE_FLAG <>0";//Change by Manika "=1" to "<>0" for mits:26051
                            objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                            if (objReader.Read())
                            {
                                p_objBillingMaster.Account.UseTax = true;    
                            }
                            objReader.Close();  
                            //npadhy RMSC retrofit Ends

							break;
						}
					}
					//npadhy RMSC retrofit Starts
					//Need to subtract the tax from the total billed premium amount
                    //total billed premium includes the tax amount
                    //dblPremium = dblRemaining = p_objBillingMaster.Account.AmountDue;
                    if (p_objBillingMaster.Account.UseTax)
                    {
                        dTaxAmount = p_objBillingMaster.Account.TaxAmount;    
                        dblPremium = dblRemaining = p_objBillingMaster.Account.AmountDue - dTaxAmount;      
                    }
                    else
                    {
                        dblPremium = dblRemaining = p_objBillingMaster.Account.AmountDue;
                    }
                    //npadhy RMSC retrofit Ends
					if( dblPremium == 0.0 )
						return ;
					
					
					arrlistInstallments = new ArrayList();
					// Create the installment records
					while( dblRemaining != 0.0 )
					{
						iInstallCount++ ;
						switch( iInstallCount )
						{
							#region Get Install Amount, Type, LagDays and DueDays
							case 1 :
								dblInstallAmount = objPayPlan.Install1Amount ;
								iInstallType = objPayPlan.Install1Type ;
								iInstallLagDays = objPayPlan.Install1LagDays ;
								iInstallDueDays = objPayPlan.Install1DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install1GenDate;
                                strDueDate = objPayPlan.Install1DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 2 :
								dblInstallAmount = objPayPlan.Install2Amount ;
								iInstallType = objPayPlan.Install2Type ;
								iInstallLagDays = objPayPlan.Install2LagDays ;
								iInstallDueDays = objPayPlan.Install2DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install2GenDate; 
                                strDueDate = objPayPlan.Install2DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 3 :
								dblInstallAmount = objPayPlan.Install3Amount ;
								iInstallType = objPayPlan.Install3Type ;
								iInstallLagDays = objPayPlan.Install3LagDays ;
								iInstallDueDays = objPayPlan.Install3DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install3GenDate; 
                                strDueDate = objPayPlan.Install3DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 4 :
								dblInstallAmount = objPayPlan.Install4Amount ;
								iInstallType = objPayPlan.Install4Type ;
								iInstallLagDays = objPayPlan.Install4LagDays ;
								iInstallDueDays = objPayPlan.Install4DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install4GenDate;
                                strDueDate = objPayPlan.Install4DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 5 :
								dblInstallAmount = objPayPlan.Install5Amount ;
								iInstallType = objPayPlan.Install5Type ;
								iInstallLagDays = objPayPlan.Install5LagDays ;
								iInstallDueDays = objPayPlan.Install5DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install5GenDate;
                                strDueDate = objPayPlan.Install5DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 6 :
								dblInstallAmount = objPayPlan.Install6Amount ;
								iInstallType = objPayPlan.Install6Type ;
								iInstallLagDays = objPayPlan.Install6LagDays ;
								iInstallDueDays = objPayPlan.Install6DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install6GenDate;
                                strDueDate = objPayPlan.Install6DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 7 :
								dblInstallAmount = objPayPlan.Install7Amount ;
								iInstallType = objPayPlan.Install7Type ;
								iInstallLagDays = objPayPlan.Install7LagDays ;
								iInstallDueDays = objPayPlan.Install7DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install7GenDate;
                                strDueDate = objPayPlan.Install7DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 8 :
								dblInstallAmount = objPayPlan.Install8Amount ;
								iInstallType = objPayPlan.Install8Type ;
								iInstallLagDays = objPayPlan.Install8LagDays ;
								iInstallDueDays = objPayPlan.Install8DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install8GenDate;
                                strDueDate = objPayPlan.Install8DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 9 :
								dblInstallAmount = objPayPlan.Install9Amount ;
								iInstallType = objPayPlan.Install9Type ;
								iInstallLagDays = objPayPlan.Install9LagDays ;
								iInstallDueDays = objPayPlan.Install9DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install9GenDate;
                                strDueDate = objPayPlan.Install9DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 10 :
								dblInstallAmount = objPayPlan.Install10Amount ;
								iInstallType = objPayPlan.Install10Type ;
								iInstallLagDays = objPayPlan.Install10LagDays ;
								iInstallDueDays = objPayPlan.Install10DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install10GenDate;
                                strDueDate = objPayPlan.Install10DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 11 :
								dblInstallAmount = objPayPlan.Install11Amount ;
								iInstallType = objPayPlan.Install11Type ;
								iInstallLagDays = objPayPlan.Install11LagDays ;
								iInstallDueDays = objPayPlan.Install11DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install11GenDate;
                                strDueDate = objPayPlan.Install11DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
							case 12 :
								dblInstallAmount = objPayPlan.Install12Amount ;
								iInstallType = objPayPlan.Install12Type ;
								iInstallLagDays = objPayPlan.Install12LagDays ;
								iInstallDueDays = objPayPlan.Install12DueDays ;
                                //npadhy RMSC retrofit Starts
                                strGenDate = objPayPlan.Install12GenDate;
                                strDueDate = objPayPlan.Install12DueDate;
                                //npadhy RMSC retrofit Ends
								break ;
								#endregion 
						}
						if( dblRemaining != 0 && dblInstallAmount == 0 )
						{
							// Apply leftover to last installment
							if( dblRemaining > 0.001 )
							{
                                // Start Naresh This is Copied from RMworld. There was a Problem with existing Code 
                                // In Rmworld we used to pick the data from Collection by using iInstallCount-1 
                                // Here it should be iInstallCount-2
								objBillXInstlmnt = (BillXInstlmnt)arrlistInstallments[ iInstallCount-2 ];
								objBillXInstlmnt.Amount += dblRemaining ;
								dblRemaining = 0.0 ;
							}
							else
							{
								objBillXInstlmnt = null ;
								bSkipAll = true ;
							}
						}
						else
						{
							objBillXInstlmnt = ( BillXInstlmnt ) m_objDataModelFactory.GetDataModelObject( "BillXInstlmnt" , false );						
							if( m_objLocalCache.GetShortCode( iInstallType ) == "D" )
							{
								if( dblRemaining > dblInstallAmount )
									objBillXInstlmnt.Amount = dblInstallAmount ;
								else
									objBillXInstlmnt.Amount = dblRemaining ;
							}
							else
							{
								// For percentage
								if( bRound )
                                    objBillXInstlmnt.Amount = Math.Round((dblPremium * (dblInstallAmount / 100)), 0, MidpointRounding.AwayFromZero);
								else
                                    objBillXInstlmnt.Amount = Math.Round((dblPremium * (dblInstallAmount / 100)), 2, MidpointRounding.AwayFromZero);
							}
							if( objBillXInstlmnt.Amount > dblRemaining )
							{
								objBillXInstlmnt.Amount = dblRemaining ;
								dblRemaining = 0 ;
							}
							else
							{
                                dblRemaining = Math.Round((dblRemaining - objBillXInstlmnt.Amount), 2, MidpointRounding.AwayFromZero);
							}

							objBillXInstlmnt.InstallmentNum = iInstallCount ;
							objBillXInstlmnt.BillAccountRowid = p_objBillingMaster.Account.BillAccountRowid ;
							objBillXInstlmnt.PolicyId = p_objBillingMaster.Account.PolicyId ;
							objBillXInstlmnt.PolicyTranId = p_iTransNumber ; 
							objBillXInstlmnt.TermNumber = p_objBillingMaster.Account.TermNumber ;

							// Set installment date and due date
							if( iInstallCount == 1 )
							{
								if( m_objLocalCache.GetShortCode( objPayPlan.OffsetIndic ) == "T" )
								{
									foreach( int iKey in p_objBillingMaster.Policies.Keys )
									{
										objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
										if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
										{
											foreach( PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList )
											{
												if( objPolicyXTermEnh.TransactionId == p_iTransNumber )
												{
													sInstallDate = objPolicyXTermEnh.EffectiveDate ;
                                                    //npadhy RMSC retrofit Starts
                                                    if (Conversion.ConvertObjToInt(Conversion.GetDate(DateTime.Now.ToString()), m_iClientId) > Conversion.ConvertObjToInt(sInstallDate, m_iClientId))
                                                    {
                                                        strComparisonDate = Conversion.GetDate(DateTime.Now.ToString());
                                                    }
                                                    else
                                                    {
                                                        strComparisonDate = sInstallDate;  
                                                    }
                                                    //npadhy RMSC retrofit Ends
													break ;
												}
											}											
										}
									}
								}
								else
								{
									sInstallDate = Conversion.GetDate( DateTime.Now.ToString() ); 
//npadhy RMSC retrofit Starts
                                    strComparisonDate = Conversion.GetDate(DateTime.Now.ToString());
//npadhy RMSC retrofit Ends
								}
								sOffsetDate = sInstallDate ;
							}

							// Determine the actual install date
                            if (m_objLocalCache.GetShortCode(objPayPlan.InstallGenType) == "F")
                            {
                                // Fixed
                                if (iInstallCount == 1)
                                {
                                    //npadhy RMSC retrofit Starts
                                    if (objPayPlan.FixedGenDate.Trim() == "")
                                    {
                                        iInstallDay = Conversion.ConvertStrToInteger(sInstallDate.Substring(6, 2));
                                        iFixedDay = Conversion.ConvertStrToInteger(m_objLocalCache.GetShortCode(objPayPlan.FixedGenDay));

                                        if (iInstallDay != iFixedDay)
                                            sInstallDate = Function.AddDays(sInstallDate, iFixedDay - iInstallDay);
                                    }
                                    else
                                    {
                                        if (Conversion.ConvertObjToInt(sInstallDate, m_iClientId) < Conversion.ConvertObjToInt(objPayPlan.FixedGenDate, m_iClientId))
                                        {
                                            sInstallDate = objPayPlan.FixedGenDate;
                                        }
                                    }
                                    //npadhy RMSC retrofit Ends
                                }

                                objBillXInstlmnt.InstallmentDate = sInstallDate;
                                //npadhy RMSC retrofit Starts
                                if (objPayPlan.FixedGenDate.Trim() == "")
                                {
                                    objBillXInstlmnt.InstallDueDate = Function.AddDays(sInstallDate, objPayPlan.FixedDueDays);
                                }
                                else
                                {
                                    if (Conversion.ConvertObjToInt(sInstallDate, m_iClientId) < Conversion.ConvertObjToInt(objPayPlan.FixedDueDate, m_iClientId))
                                    {
                                        objBillXInstlmnt.InstallDueDate = objPayPlan.FixedDueDate;
                                    }
                                    else
                                    {
                                        objBillXInstlmnt.InstallDueDate = sInstallDate;
                                    }
                                }
                                
                                if (objPayPlan.FixedGenDate.Trim() == "")
                                //npadhy RMSC retrofit Ends
                                {
                                    switch (m_objLocalCache.GetShortCode(objPayPlan.FixedFrequency))
                                    {
                                        case "M":
                                            sInstallDate = Function.AddMonths(sInstallDate, 1);
                                            break;
                                        case "Q":
                                            sInstallDate = Function.AddMonths(sInstallDate, 3);
                                            break;
                                        case "B":
                                            sInstallDate = Function.AddMonths(sInstallDate, 6);
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                //npadhy RMSC retrofit Starts
                                // Custom 
                                if (objPayPlan.Install1GenDate.Trim() == "") 
                                {
                                    sInstallDate = Function.AddDays(sInstallDate, iInstallLagDays);
                                    objBillXInstlmnt.InstallmentDate = sInstallDate;
                                    objBillXInstlmnt.InstallDueDate = Function.AddDays(objBillXInstlmnt.InstallmentDate, iInstallDueDays);
                                }
                                else
                                {
                                    sInstallDate = strGenDate;
                                    objBillXInstlmnt.InstallmentDate = sInstallDate;
                                    objBillXInstlmnt.InstallDueDate = strDueDate;
                                }
                                //npadhy RMSC retrofit Ends
                            }
							arrlistInstallments.Add( objBillXInstlmnt );
						}
						if( bSkipAll )
							break ;
					}// while end
					
                    //npadhy RMSC retrofit Starts
                    if (objPayPlan.Install1GenDate.Trim()  == "" && objPayPlan.FixedGenDate.Trim()== "")
                    {
                        strDateToUse = Conversion.GetDate( DateTime.Now.ToString() );
                    }
                    else
                    {
                      strDateToUse = strComparisonDate;
                    }
                    //npadhy RMSC retrofit Ends
					for( iInstallCount = 0 ; iInstallCount < arrlistInstallments.Count ; iInstallCount++ )
					{
						objBillXInstlmnt = ( BillXInstlmnt ) arrlistInstallments[ iInstallCount ] ;
                        //npadhy RMSC retrofit Starts
						//if( Conversion.ConvertStrToInteger( objBillXInstlmnt.InstallmentDate ) < Conversion.ConvertStrToInteger( Conversion.GetDate( DateTime.Now.ToString() ) ) ) 
                        if (Conversion.ConvertStrToInteger(objBillXInstlmnt.InstallmentDate) < Conversion.ConvertObjToInt(strDateToUse, m_iClientId)) 
                        //npadhy RMSC retrofit Ends
						{
							if( m_objLocalCache.GetShortCode( objPayPlan.LateStartIndic ) == "B" )
							{
								objBillXInstlmnt.InstallmentDate = Conversion.GetDate( DateTime.Now.ToString() ) ; 
								if( m_objLocalCache.GetShortCode( objPayPlan.InstallGenType ) == "F" )
								{
                                    //npadhy RMSC retrofit Starts
                                    if (objPayPlan.FixedGenDate.Trim() == "") 
                                    {
                                        objBillXInstlmnt.InstallDueDate = Function.AddDays(objBillXInstlmnt.InstallmentDate, objPayPlan.FixedDueDays);
                                    }
                                    else 
                                    {
                                        objBillXInstlmnt.InstallDueDate = objPayPlan.FixedDueDate;    
                                    }
                                    //npadhy RMSC retrofit Ends
								}
								else
								{
                                    switch (iInstallCount + 1)
									{
										case 1 :
											iInstallDueDays = objPayPlan.Install1DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install1DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 2 :
											iInstallDueDays = objPayPlan.Install2DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install2DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 3 :
											iInstallDueDays = objPayPlan.Install3DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install3DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 4 :
											iInstallDueDays = objPayPlan.Install4DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install4DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 5 :
											iInstallDueDays = objPayPlan.Install5DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install5DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 6 :
											iInstallDueDays = objPayPlan.Install6DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install6DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 7 :
											iInstallDueDays = objPayPlan.Install7DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install7DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 8 :
											iInstallDueDays = objPayPlan.Install8DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install8DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 9 :
											iInstallDueDays = objPayPlan.Install9DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install9DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 10 :
											iInstallDueDays = objPayPlan.Install10DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install10DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 11 :
											iInstallDueDays = objPayPlan.Install11DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install11DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
										case 12 :
											iInstallDueDays = objPayPlan.Install12DueDays ;
                                            //npadhy RMSC retrofit Starts
                                            strDueDate = objPayPlan.Install12DueDate;
                                            //npadhy RMSC retrofit Ends
											break;
									}
                                    //npadhy RMSC retrofit Starts
                                    if (objPayPlan.Install1GenDate.Trim() == "") 
                                    {
                                        objBillXInstlmnt.InstallDueDate = Function.AddDays(objBillXInstlmnt.InstallmentDate, iInstallDueDays);
                                    }
                                    else 
                                    {
                                        objBillXInstlmnt.InstallDueDate = strDateToUse; 
                                    }
                                    //npadhy RMSC retrofit Ends
								}
							}
							else
							{
								for( iInstallCountTemp = 0 ; iInstallCountTemp < arrlistInstallments.Count ; iInstallCountTemp++ )
								{
									objBillXInstlmntTemp = ( BillXInstlmnt ) arrlistInstallments[ iInstallCountTemp ] ;
                                    //npadhy RMSC retrofit Starts
                                    //if (objBillXInstlmntTemp.InstallmentDate != "" && Conversion.ConvertStrToInteger(objBillXInstlmntTemp.InstallmentDate) >= Conversion.ConvertStrToInteger(Conversion.GetDate(DateTime.Now.ToString()))) //Animesh Modifed the condition RMSC 
                                    if (objBillXInstlmntTemp.InstallmentDate != "" && Conversion.ConvertStrToInteger(objBillXInstlmntTemp.InstallmentDate) >= Conversion.ConvertObjToInt(strDateToUse, m_iClientId))
                                    //npadhy RMSC retrofit Ends
									{
										objBillXInstlmnt.InstallmentDate = objBillXInstlmntTemp.InstallmentDate ;
										objBillXInstlmnt.InstallDueDate = objBillXInstlmntTemp.InstallDueDate ;
										break ;
									}
								}
							}
						}
					}


                    //Changed  by Gagan for mits 16513 : Start
                    //Finding all installments that are greater than policy expiration date 
                    double dReadjustmentAmount = 0;
                    ArrayList arrlstRemove = new ArrayList();
                    bool bFound = false;
                    if (sPolicyExpirationDate != "")
                    {
                        foreach (BillXInstlmnt objBillXInstallment in arrlistInstallments)
                        {
                            if (Conversion.ConvertStrToInteger(objBillXInstallment.InstallmentDate) > Conversion.ConvertStrToInteger(sPolicyExpirationDate))
                            {
                                dReadjustmentAmount += objBillXInstallment.Amount;
                                arrlstRemove.Add(objBillXInstallment);
                                bFound = true;
                            }
                        }


                        if (bFound)
                        {
                            //Remove post-dated installments
                            foreach (object objBillXInstallment in arrlstRemove)
                            {
                                arrlistInstallments.Remove(objBillXInstallment);
                            }

                            //Adding left over amount to the last installment that has install date less than policy expiration date
                            if(arrlistInstallments.Count > 0)
                                ((BillXInstlmnt)(arrlistInstallments[arrlistInstallments.Count - 1])).Amount += dReadjustmentAmount;
                        }

                    }

                    //npadhy RMSC retrofit Starts 
                    //Caliculate Installment Tax
                    int iInsllmentCount;
                    double dGenTax = 0 ;
                    iInsllmentCount = arrlistInstallments.Count;
                    if (p_objBillingMaster.Account.UseTax)
                    {
                        foreach (BillXInstlmnt objInstllment in arrlistInstallments)
                        {
                            if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                            {
                                objInstllment.TaxAmount = Math.Round(objInstllment.Amount * (p_objBillingMaster.Account.TaxRate / 100), 2, MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                objInstllment.TaxAmount = Math.Round(p_objBillingMaster.Account.TaxRate / iInsllmentCount, 2, MidpointRounding.AwayFromZero);    
                            }
                            dGenTax += objInstllment.TaxAmount;  
                        }
                        if (iInsllmentCount > 0)
                        {
                            ((BillXInstlmnt)(arrlistInstallments[arrlistInstallments.Count - 1])).TaxAmount += (dTaxAmount - dGenTax);  
                        }
                                               
                    }
                    //npadhy RMSC retrofit Ends

					for( iInstallCount = 0 ; iInstallCount < arrlistInstallments.Count ; iInstallCount++ )
					{
						objBillXInstlmnt = ( BillXInstlmnt ) arrlistInstallments[iInstallCount];
                        objBillXInstlmnt.InstallmentRowid = Utilities.GetNextUID(m_sConnectionString, "BILL_X_INSTLMNT", m_iClientId);
						p_objBillingMaster.Installments.Add( objBillXInstlmnt );
					}

                    //Changed  by Gagan for mits 16513 : End

					objDiaryManager.ScheduleInstallmentDiary( p_objBillingMaster );
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PayPlanManager.CreatePayPlanForPolicy.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
				if( objBillXInstlmnt != null )
				{
					objBillXInstlmnt.Dispose();
					objBillXInstlmnt = null ;
				}
				if( objBillXInstlmntTemp != null )
				{
					objBillXInstlmntTemp.Dispose();
					objBillXInstlmntTemp = null ;
				}
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                    objDiaryManager = null;
                }
				objPayPlan = null ;
				arrlistInstallments = null ;
			}
		}


		public void ReactivatePayPlan( BillingMaster p_objBillingMaster, int p_iTransNumber, string p_sType, double p_dblAmount )
		{
			PolicyEnh objPolicyEnh = null ;
			BillXInstlmnt objBillXInstlmnt = null ;
			PayPlan objPayPlan = null ;
			DiaryManager objDiaryManager = null ;
			BillingManager objBillingManager = null ;

			string sExpDate = string.Empty ;
			string sInstallDate = string.Empty ;
			string sSQL = string.Empty ;

			int iInstallDay = 0 ;
			double dblRemaining = 0.0 ;
			bool bRound = false ;
			int iDueDays = 0 ;
			int iIndex = 0 ;
			int iIndexJ = 0 ;
			int iLagDays = 0 ;
			int iFixedDay = 0 ;
			double dblInstallAmt = 0.0 ;

            //npadhy RMSC retrofit Starts
            int iInstlCount;
            double dTax;
            string sGenerationDate = string.Empty;
            string sDueDate= string.Empty;  
            string sTxnDate =string.Empty;  
            int iCounter;
            BillXInstlmnt objBillXInstlmnt2 = null ;
            BillingRule objBillRule=null;
            double dCheckValue;
            //npadhy RMSC retrofit Ends
			
			try
			{
                //RMSC
                dTax = 0;
                dCheckValue = 0;
                iInstlCount = 0;
                iCounter = 0; 
                //RMSC
                objBillingManager = new BillingManager(m_objDataModelFactory, m_iClientId);
                objDiaryManager = new DiaryManager(m_objDataModelFactory, m_iClientId);

                bRound = Function.GetRoundAmountFlag(m_sConnectionString, m_iClientId);
				foreach( int iKey in p_objBillingMaster.Policies.Keys )
				{
					objPolicyEnh = ( PolicyEnh ) p_objBillingMaster.Policies[iKey] ;
					if( m_objLocalCache.GetShortCode( objPolicyEnh.PolicyIndicator ) == "P" )
					{
						foreach( PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList )
						{
							if( objPolicyXTermEnh.TransactionId == p_iTransNumber )
							{
								sExpDate = objPolicyXTermEnh.ExpirationDate ;
								break ;
							}
						}	
						foreach( PolicyXTransEnh objPolicyXTransEnh in objPolicyEnh.PolicyXTransEnhList )
						{
							if( objPolicyXTransEnh.TransactionId == p_iTransNumber )
							{
								sInstallDate = objPolicyXTransEnh.EffectiveDate ;
								break ;
							}
						}
						break ;		
					}
				}
			
				sSQL = " SELECT * FROM BILL_X_INSTLMNT WHERE BILL_ACCOUNT_ROWID =" + p_objBillingMaster.Account.BillAccountRowid 
					+ " AND GENERATED_FLAG = 0 ORDER BY INSTALLMENT_NUM" ;
				objBillingManager.FillInstallmentLoop( p_objBillingMaster, sSQL );
			
				objPayPlan = new PayPlan();
				sSQL = " SELECT * FROM SYS_BILL_PAY_PLAN WHERE PAY_PLAN_ROWID = " + p_objBillingMaster.Account.PayPlanRowid ;
				objBillingManager.FillPayPlan( objPayPlan , sSQL );

				dblRemaining = p_dblAmount ;
				if( dblRemaining == 0 )
					return ;

				// If all installments have gone out, bill in full.
				if( p_objBillingMaster.Installments.Count == 0 )
				{
					objBillingManager.CreateChargeItemPolicy( p_objBillingMaster, p_iTransNumber, p_sType , p_dblAmount );
					return ;
				}

				// If there is only one installment left, schedule it.
				if( p_objBillingMaster.Installments.Count == 1 )
				{
					objBillXInstlmnt = ( BillXInstlmnt ) p_objBillingMaster.Installments[0] ;
                    //npadhy RMSC retrofit Starts
                    if (p_objBillingMaster.Account.UseTax == true)
                    {
                        dTax = p_objBillingMaster.Account.TaxAmount;
                        p_dblAmount -= dTax; 
                    }
                    //npadhy RMSC retrofit Ends
					objBillXInstlmnt.Amount = p_dblAmount ;

                    // npadhy Start MITS 19377 The installment Date in database is of 8 characters and meant to store only date and not time.
                    // So in DataModel, it uses the function Conversion.GetDate to parse it. If there is time in the Date as well, 
                    // while parsing it returns blank. So changing the function to GetDate
					objBillXInstlmnt.InstallmentDate = Conversion.GetDate( DateTime.Now.ToString("d") ); // TODO
                    // npadhy End MITS 19377 The installment Date in database is of 8 characters and meant to store only date and not time.
                    // So in DataModel, it uses the function Conversion.GetDate to parse it. If there is time in the Date as well, 
                    // while parsing it returns blank. So changing the function to GetDate

					iDueDays = this.GetDueDays( objPayPlan , objBillXInstlmnt.InstallmentNum );
					objBillXInstlmnt.InstallDueDate = Function.AddDays( objBillXInstlmnt.InstallmentDate , iDueDays );
                    //npadhy RMSC retrofit Starts
                    if (p_objBillingMaster.Account.UseTax == true)
                    {
                        if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                        {
                            objBillXInstlmnt.TaxAmount = Math.Round(objBillXInstlmnt.Amount * (p_objBillingMaster.Account.TaxRate / 100), 2, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            objBillXInstlmnt.TaxAmount = Math.Round(p_objBillingMaster.Account.TaxRate, 2, MidpointRounding.AwayFromZero);    
                        }
                    }
                    //npadhy RMSC retrofit Ends
					objBillingManager.CreateChargeItemInstallment( p_objBillingMaster , objBillXInstlmnt );
					return ;
				}
			
				// Otherwise, schedule as many as we have time for... do the dates first, then the amounts
			
				for( iIndex = 0 ; iIndex < p_objBillingMaster.Installments.Count ; iIndex++ )
				{
					objBillXInstlmnt = ( BillXInstlmnt ) p_objBillingMaster.Installments[iIndex] ;
                    //npadhy RMSC retrofit Starts
                    iCounter = iIndex;  
                    //npadhy RMSC retrofit Ends
					if( iIndex == 0 )
					{
						objBillXInstlmnt.InstallmentDate = sInstallDate ;
						iDueDays = this.GetDueDays( objPayPlan , objBillXInstlmnt.InstallmentNum );
                        //npadhy RMSC retrofit Starts
                        if (iDueDays != 0)
                        {
                            objBillXInstlmnt.InstallDueDate = Function.AddDays(objBillXInstlmnt.InstallmentDate, iDueDays);
                        }
                        else
                        {
                            sSQL = "SELECT * FROM SYS_BILL_BLNG_RULE WHERE BILLING_RULE_ROWID = " + p_objBillingMaster.Account.BillingRuleRowid;
                            objBillRule = new BillingRule();
                            objBillingManager.FillBillingRule(objBillRule, sSQL);
                            objBillXInstlmnt.InstallDueDate = Function.AddDays(objBillXInstlmnt.InstallmentDate, objBillRule.DueDays);    
                        }
                        //npadhy RMSC retrofit Ends
					}
					else
					{
						if( m_objLocalCache.GetShortCode( objPayPlan.InstallGenType ) == "F" )
						{
							iFixedDay = Conversion.ConvertStrToInteger( m_objLocalCache.GetShortCode( objPayPlan.FixedGenDay ) );
							if( sInstallDate.Length >= 8 )
								iInstallDay = Conversion.ConvertStrToInteger(sInstallDate.Substring( 6 , 2 )); //TODO										
							// TODO
							if( iInstallDay != iFixedDay )
								sInstallDate = Function.AddDays( sInstallDate , iFixedDay - iInstallDay );									
						
							switch( m_objLocalCache.GetShortCode( objPayPlan.FixedFrequency ) )
							{
								case "M" :
									sInstallDate = Function.AddMonths( sInstallDate , 1 ); // TODO
									break;
								case "Q" :
									sInstallDate = Function.AddMonths( sInstallDate , 3 );
									break;
								case "B" :
									sInstallDate = Function.AddMonths( sInstallDate , 6 );
									break;
								case "A" :
									sInstallDate = Function.AddMonths( sInstallDate , 12 );
									break;
							}
							objBillXInstlmnt.InstallmentDate = sInstallDate ; // TODO
							objBillXInstlmnt.InstallDueDate = Function.AddDays( sInstallDate , objPayPlan.FixedDueDays );
							if( Conversion.ConvertStrToInteger( objBillXInstlmnt.InstallDueDate ) >= Conversion.ConvertStrToInteger( sExpDate ) )
							{
								for( iIndexJ = iIndex ; iIndexJ < p_objBillingMaster.Installments.Count ; iIndexJ++ )
								{
                                    objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[iIndexJ];
                                    p_objBillingMaster.DeleteInstallment(objBillXInstlmnt.InstallmentRowid);
								}
								break ;
							}
						}
						else
						{
							// Custom 
							iLagDays = this.GetLagDays( objPayPlan, objBillXInstlmnt.InstallmentNum ) ;
							iDueDays = this.GetDueDays( objPayPlan, objBillXInstlmnt.InstallmentNum ) ;
                            //npadhy RMSC retrofit Starts
                            sGenerationDate = this.GetGenerationDate(objPayPlan, objBillXInstlmnt.InstallmentNum);
                            sDueDate = this.GetDueDate(objPayPlan, objBillXInstlmnt.InstallmentNum);
                            if (iLagDays != 0)
                            {
                                sInstallDate = Function.AddDays(sInstallDate, iLagDays);
                                objBillXInstlmnt.InstallmentDate = sInstallDate; // TODO
                                objBillXInstlmnt.InstallDueDate = Function.AddDays(objBillXInstlmnt.InstallmentDate, iDueDays);
                            }
                            else if (sGenerationDate.Trim() != "")  
                            {
                                sTxnDate = sInstallDate;
                                if (Conversion.ConvertObjToInt(sGenerationDate, m_iClientId) >= Conversion.ConvertObjToInt(sTxnDate, m_iClientId))
                                {
                                    objBillXInstlmnt.InstallmentDate = sGenerationDate;
                                    objBillXInstlmnt.InstallDueDate = sDueDate;
                                }
                                else
                                {
                                    iCounter += 1;
                                    while (iCounter < p_objBillingMaster.Installments.Count)
                                    {
                                        objBillXInstlmnt2 = (BillXInstlmnt)p_objBillingMaster.Installments[iCounter];
                                        sGenerationDate = this.GetGenerationDate(objPayPlan, objBillXInstlmnt2.InstallmentNum);
                                        sDueDate = this.GetDueDate(objPayPlan,objBillXInstlmnt2.InstallmentNum);
                                        if (Conversion.ConvertObjToInt(sGenerationDate, m_iClientId) >= Conversion.ConvertObjToInt(sTxnDate, m_iClientId))
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            iCounter += 1; 
                                        }
                                        objBillXInstlmnt2=null; 
                                    }
                                    if (Conversion.ConvertObjToInt(sGenerationDate, m_iClientId) >= Conversion.ConvertObjToInt(sTxnDate, m_iClientId))
                                    {
                                        objBillXInstlmnt.InstallmentDate = sGenerationDate;
                                        objBillXInstlmnt.InstallDueDate = sDueDate;
                                    }
                                    else
                                    {
                                        objBillXInstlmnt.InstallmentDate = sTxnDate ;
                                        objBillXInstlmnt.InstallDueDate = sTxnDate;
                                    }
                                }
                            }
                            //npadhy RMSC retrofit Ends	
						}
					}
				}
			   
                //npadhy RMSC retrofit Starts
                //check to make sure the due dates don't fall outside the term exp date
                for (iIndex = 0; iIndex < p_objBillingMaster.Installments.Count; iIndex++)
                {
                    objBillXInstlmnt2 = (BillXInstlmnt)p_objBillingMaster.Installments[iIndex];
                    if (Conversion.ConvertObjToInt(objBillXInstlmnt2.InstallDueDate, m_iClientId) >= Conversion.ConvertObjToInt(sExpDate, m_iClientId))
                    {
                        p_objBillingMaster.DeleteInstallment(objBillXInstlmnt2.InstallmentRowid);    
                    }
                }
                //npadhy RMSC retrofit Ends
                 iIndexJ = 0;
                // Now go back and fill in the amounts  TODO TODO TODO Set iIndexJ
                 for (iIndex = 0; iIndex < p_objBillingMaster.Installments.Count; iIndex++)
                 {
                     objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[iIndex];
                     if (!p_objBillingMaster.IsDeletedInstallment(objBillXInstlmnt.InstallmentRowid))
                     {
                         iIndexJ++;
                     }
                 }

                //npadhy RMSC retrofit Starts
                 //subtract tax from amount
                 if (p_objBillingMaster.Account.UseTax)
                 {
                     dTax = p_objBillingMaster.Account.TaxAmount;
                     p_dblAmount -= dTax;
                     dblRemaining = p_dblAmount;
                     dCheckValue = p_dblAmount;  
                 }
                //npadhy RMSC retrofit Ends
    			
				if( bRound )
                    dblInstallAmt = Math.Round((p_dblAmount / iIndexJ), 0, MidpointRounding.AwayFromZero);
				else
                    dblInstallAmt = Math.Round((p_dblAmount / iIndexJ), 2, MidpointRounding.AwayFromZero);
			
				for( iIndex = 0 ; iIndex < iIndexJ ; iIndex++ )
				{
					objBillXInstlmnt = ( BillXInstlmnt ) p_objBillingMaster.Installments[iIndex] ;
					if( dblInstallAmt > dblRemaining )
					{
						objBillXInstlmnt.Amount = dblRemaining ;
						if( bRound )
                            dblRemaining = Math.Round((dblRemaining - dblInstallAmt), 0, MidpointRounding.AwayFromZero);
						else
                            dblRemaining = Math.Round((dblRemaining - dblInstallAmt), 2, MidpointRounding.AwayFromZero);
					}
					else
					{
						objBillXInstlmnt.Amount = dblInstallAmt ;
						if( bRound )
                            dblRemaining = Math.Round((dblRemaining - dblInstallAmt), 0, MidpointRounding.AwayFromZero);
						else
                            dblRemaining = Math.Round((dblRemaining - dblInstallAmt), 2, MidpointRounding.AwayFromZero);
					}
				}
                //***************************
                //if( dblRemaining > 0 )
                //{
                //    objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[iIndexJ - 1];
                //    objBillXInstlmnt.Amount += dblRemaining ;
                //}
                //***************************
                //npadhy RMSC retrofit Starts
                double dCurrentAmt= 0;
                double dPaidAmt = 0;
                double dTotalAmt = 0;
                double dTotalTaxToPay = 0;
                double dCurrentTax = 0;
                double dPaidTax = 0;
                double dTotalTax= 0;
                double dTaxDiff = 0;
                DbReader objReader = null;
                if (p_objBillingMaster.Account.UseTax)
                {
                    for (iIndex = 0; iIndex < p_objBillingMaster.Installments.Count; iIndex++)
                    {
                        objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[iIndex];
                        if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                        {
                            objBillXInstlmnt.TaxAmount = Math.Round(objBillXInstlmnt.Amount * (p_objBillingMaster.Account.TaxRate / 100), 2, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            objBillXInstlmnt.TaxAmount = Math.Round(p_objBillingMaster.Account.TaxRate / p_objBillingMaster.Installments.Count, 2, MidpointRounding.AwayFromZero);     
                        }
                        dCurrentAmt += objBillXInstlmnt.Amount;
                        dCurrentTax += objBillXInstlmnt.TaxAmount;   
                    }
                }

                if ((dCheckValue >= 0 && dblRemaining >= 0) || (dCheckValue < 0 && dblRemaining < 0))
                {
                    objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[iIndexJ - 1];
                    objBillXInstlmnt.Amount += dblRemaining ;
                    //recalculated tax for increased installment amount  - start
                    if (p_objBillingMaster.Account.UseTax)
                    {
                        if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                        {
                            objBillXInstlmnt.TaxAmount = Math.Round(objBillXInstlmnt.Amount * (p_objBillingMaster.Account.TaxRate / 100), 2, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            objBillXInstlmnt.TaxAmount = Math.Round(p_objBillingMaster.Account.TaxRate / p_objBillingMaster.Installments.Count, 2, MidpointRounding.AwayFromZero);
                        }
                    }
                    //recalculated tax for increased installment amount  - End
                }

                sSQL = "SELECT AMOUNT,TAX_AMOUNT FROM BILL_X_INSTLMNT WHERE POLICY_ID = " + p_objBillingMaster.Account.PolicyId + " AND BILL_ACCOUNT_ROWID = " + p_objBillingMaster.Account.BillAccountRowid + " AND GENERATED_FLAG <> 0 ORDER BY INSTALLMENT_NUM";
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                while (objReader.Read())
                {
                    dPaidAmt += Conversion.ConvertObjToDouble(objReader.GetValue("AMOUNT"), m_iClientId);
                    dPaidTax += Conversion.ConvertObjToDouble(objReader.GetValue("TAX_AMOUNT"), m_iClientId);
                }
                objReader.Close();

                dTotalAmt = dCurrentAmt + dPaidAmt;
                dTotalTax = dCurrentTax + dPaidTax;

                if (p_objBillingMaster.Account.UseTax)
                {
                    if (this.m_objLocalCache.GetShortCode(p_objBillingMaster.Account.TaxType) == "P")
                    {
                        dTotalTaxToPay = Math.Round(dTotalAmt * (p_objBillingMaster.Account.TaxRate / 100), 2, MidpointRounding.AwayFromZero);
                        dTaxDiff = dTotalTaxToPay - dTotalTax;
                        objBillXInstlmnt = (BillXInstlmnt)p_objBillingMaster.Installments[iIndexJ - 1];
                        objBillXInstlmnt.TaxAmount = objBillXInstlmnt.TaxAmount + dTaxDiff;    
                    }
                }
                //npadhy RMSC retrofit Ends
			
				objDiaryManager.ScheduleInstallmentDiary( p_objBillingMaster );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PayPlanManager.ReactivatePayPlan.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
                //if( objPolicyEnh != null )
                //{
                //    objPolicyEnh.Dispose();
                //    objPolicyEnh = null ;
                //}			
                if (objBillXInstlmnt != null)
                {
                    objBillXInstlmnt.Dispose();
                    objBillXInstlmnt = null;
                }
                if (objDiaryManager != null)
                {
                    objDiaryManager.Dispose();
                    objDiaryManager = null;
                }
                if (objBillingManager != null)
                {
                    objBillingManager.Dispose();
                    objBillingManager = null;
                }

				objPayPlan = null ;
			}
		}


		public int GetDueDays( PayPlan p_objPayPlan, int p_iInstallmentNum )
		{
			int iReturnValue = 0 ;
			
			try
			{
				if( m_objLocalCache.GetShortCode( p_objPayPlan.InstallGenType ) == "C" )
				{
					switch( p_iInstallmentNum )
					{
						case 1 :
							iReturnValue = p_objPayPlan.Install1DueDays ;
							break;
						case 2 :
							iReturnValue = p_objPayPlan.Install2DueDays ;
							break;
						case 3 :
							iReturnValue = p_objPayPlan.Install3DueDays ;
							break;
						case 4 :
							iReturnValue = p_objPayPlan.Install4DueDays ;
							break;
						case 5 :
							iReturnValue = p_objPayPlan.Install5DueDays ;
							break;
						case 6 :
							iReturnValue = p_objPayPlan.Install6DueDays ;
							break;
						case 7 :
							iReturnValue = p_objPayPlan.Install7DueDays ;
							break;
						case 8 :
							iReturnValue = p_objPayPlan.Install8DueDays ;
							break;
						case 9 :
							iReturnValue = p_objPayPlan.Install9DueDays ;
							break;
						case 10 :
							iReturnValue = p_objPayPlan.Install10DueDays ;
							break;
						case 11 :
							iReturnValue = p_objPayPlan.Install11DueDays ;
							break;
						case 12 :
							iReturnValue = p_objPayPlan.Install12DueDays ;
							break;
					}
				}
				else
				{
					iReturnValue = p_objPayPlan.FixedDueDays ;
				}
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PayPlanManager.GetDueDays.Error", m_iClientId), p_objEx);				
			}
			return( iReturnValue );
		}

        //npadhy RMSC retrofit Starts
        public string GetGenerationDate (PayPlan p_objPayPlan,int p_iInstallmentNum)
        {
            string strReturnDate = string.Empty;
            try
            {
                switch (p_iInstallmentNum)
                {
                    case 1:
                        strReturnDate = p_objPayPlan.Install1GenDate;
                        break;
                    case 2:
                        strReturnDate = p_objPayPlan.Install2GenDate;
                        break;
                    case 3:
                        strReturnDate = p_objPayPlan.Install3GenDate;
                        break;
                    case 4:
                        strReturnDate = p_objPayPlan.Install4GenDate;
                        break;
                    case 5:
                        strReturnDate = p_objPayPlan.Install5GenDate;
                        break;
                    case 6:
                        strReturnDate = p_objPayPlan.Install6GenDate;
                        break;
                    case 7:
                        strReturnDate = p_objPayPlan.Install7GenDate;
                        break;
                    case 8:
                        strReturnDate = p_objPayPlan.Install8GenDate;
                        break;
                    case 9:
                        strReturnDate = p_objPayPlan.Install9GenDate;
                        break;
                    case 10:
                        strReturnDate = p_objPayPlan.Install10GenDate;
                        break;
                    case 11:
                        strReturnDate = p_objPayPlan.Install11GenDate;
                        break;
                    case 12:
                        strReturnDate = p_objPayPlan.Install12GenDate;
                        break;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PayPlanManager.GetGenerationDate.Error", m_iClientId), p_objEx);
            }
            return (strReturnDate);
        }

        public string GetDueDate(PayPlan p_objPayPlan, int p_iInstallmentNum)
        {
            string strReturnDate = string.Empty;
            try
            {
                switch (p_iInstallmentNum)
                {
                    case 1:
                        strReturnDate = p_objPayPlan.Install1DueDate ;
                        break;
                    case 2:
                        strReturnDate = p_objPayPlan.Install2DueDate;
                        break;
                    case 3:
                        strReturnDate = p_objPayPlan.Install3DueDate;
                        break;
                    case 4:
                        strReturnDate = p_objPayPlan.Install4DueDate;
                        break;
                    case 5:
                        strReturnDate = p_objPayPlan.Install5DueDate;
                        break;
                    case 6:
                        strReturnDate = p_objPayPlan.Install6DueDate;
                        break;
                    case 7:
                        strReturnDate = p_objPayPlan.Install7DueDate;
                        break;
                    case 8:
                        strReturnDate = p_objPayPlan.Install8DueDate;
                        break;
                    case 9:
                        strReturnDate = p_objPayPlan.Install9DueDate;
                        break;
                    case 10:
                        strReturnDate = p_objPayPlan.Install10DueDate;
                        break;
                    case 11:
                        strReturnDate = p_objPayPlan.Install11DueDate;
                        break;
                    case 12:
                        strReturnDate = p_objPayPlan.Install12DueDate;
                        break;
                }
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PayPlanManager.GetGenerationDate.Error", m_iClientId), p_objEx);
            }
            return (strReturnDate);
        }
        //npadhy RMSC retrofit Ends

		public int GetLagDays( PayPlan p_objPayPlan, int p_iInstallmentNum )
		{
			int iReturnValue = 0 ;
			
			try
			{
				switch( p_iInstallmentNum )
				{
					case 1 :
						iReturnValue = p_objPayPlan.Install1LagDays ;
						break;
					case 2 :
						iReturnValue = p_objPayPlan.Install2LagDays ;
						break;
					case 3 :
						iReturnValue = p_objPayPlan.Install3LagDays ;
						break;
					case 4 :
						iReturnValue = p_objPayPlan.Install4LagDays ;
						break;
					case 5 :
						iReturnValue = p_objPayPlan.Install5LagDays ;
						break;
					case 6 :
						iReturnValue = p_objPayPlan.Install6LagDays ;
						break;
					case 7 :
						iReturnValue = p_objPayPlan.Install7LagDays ;
						break;
					case 8 :
						iReturnValue = p_objPayPlan.Install8LagDays ;
						break;
					case 9 :
						iReturnValue = p_objPayPlan.Install9LagDays ;
						break;
					case 10 :
						iReturnValue = p_objPayPlan.Install10LagDays ;
						break;
					case 11 :
						iReturnValue = p_objPayPlan.Install11LagDays ;
						break;
					case 12 :
						iReturnValue = p_objPayPlan.Install12LagDays ;
						break;
				}
				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PayPlanManager.GetLagDays.Error", m_iClientId), p_objEx);				
			}
			return( iReturnValue );
		}

	}
}
