﻿
using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Application.ReportInterfaces;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/**************************************************************
	 * $File		: WCPPFirstInvoice.cs
	 * $Revision	: 1.0.0.0
	 * $Date		: 
	 * $Author		: Nitesh Deedwania
	 * $Comment		:  
	 * $Source		:  	
	**************************************************************/
	public class WCPPFirstInvoice : DataDynamics.ActiveReports.ActiveReport3
	{
		#region Member Variables
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = string.Empty ;
		/// <summary>
		/// Private variable to store Insured Entity
		/// </summary>		
		private Entity m_objInsuredEntity = null ;
		/// <summary>
		/// Private variable to store Insurer Entity
		/// </summary>
		private Entity m_objInsurerEntity = null ;
		/// <summary>
		/// Private variable to store Invoice
		/// </summary>
		private BillXInvoice m_objBillXInvoice = null ; 
		/// <summary>
		/// Private variable to store Account
		/// </summary>
		private BillXAccount m_objBillXAccount = null ; 
		/// <summary>
		/// Private variable to store Effective Date
		/// </summary>		
		private string m_sEffDate = string.Empty ;
		/// <summary>
		/// Private variable to store Exp. Date
		/// </summary>
		private string m_sExpDate = string.Empty ;
		/// <summary>
		/// Private variable to store Total Dividend Credit
		/// </summary>
		private double m_dblTotalDividendCredit = 0.0 ;
		/// <summary>
		/// Private variable to store Install Count
		/// </summary>
		private int m_iInstallCount = 0 ;
		/// <summary>
		/// Private variable to store Installs Due
		/// </summary>
		private string m_sInstallsDue = string.Empty ;
		/// <summary>
		/// Private variable to store Installs Due Second
		/// </summary>
		private string m_sInstallsDueSecond = string.Empty ;
		/// <summary>
		/// Private variable to store Fetch Count
		/// </summary>
		private int m_iFetchCount = 0 ;
        private string m_sPolicy_Name = string.Empty; // csingh7  : MITS 12456 
        private int m_iClientId = 0;
		#endregion 		

		#region Constructor
		/// <summary>
		/// Constructor, initializes the variables to the default value, and Initialize the Report.
		/// </summary>
		/// <param name="p_objDataModelFactory">DataModelFactory Object</param>
		public WCPPFirstInvoice( DataModelFactory p_objDataModelFactory,int p_iClientId)
		{
            m_iClientId = p_iClientId;
			m_objDataModelFactory = p_objDataModelFactory ;
			m_sConnectionString = p_objDataModelFactory.Context.DbConn.ConnectionString ;
			
			InitializeComponent();
		}
		#endregion 

		#region Properties
		/// <summary>
		/// Read propertie for the Invoice collection.
		/// </summary>
		internal BillXInvoice Invoice 
		{
			get
			{				
				return m_objBillXInvoice;
			}			
		}

		#endregion 

		#region DataInitialize, FetchData, ReportStart Functions bind to respective events of the report.
		/// <summary>
		/// This method would invoke the method of base class to initialize the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void PolicyBilling_DataInitialize( object p_objsender, System.EventArgs p_objeArgs )
		{ 
			try
			{
				m_iFetchCount = 0 ;

				Fields.Add("INSURED_NAME");
				Fields.Add("INSURED_ATTN");
				Fields.Add("INSURED_ADDR1");
				Fields.Add("INSURED_ADDR2");
				Fields.Add("INSURED_CITY");
				Fields.Add("INSURED_STATE");
				Fields.Add("INSURED_ZIPCODE");
				Fields.Add("INSURER_NAME");
				Fields.Add("INSURER_ADDR1");
				Fields.Add("INSURER_ADDR2");
				Fields.Add("INSURER_ADDR3");
                Fields.Add("POLICY_NAME");  //csingh7  : MITS 12456
				Fields.Add("DATE");
				Fields.Add("INVOICE_NUMBER");
				Fields.Add("DUE_DATE");
				Fields.Add("TERM_DATES");
				Fields.Add("TOTAL_BILLED_PREM");
				Fields.Add("TOTAL_DIVIDEND_CREDIT");
				Fields.Add("NET_PREMIUM");					
				Fields.Add("AMOUNT");
				Fields.Add("NUM_INSTALLS");
				Fields.Add("DUE_DATES1");
				Fields.Add("DUE_DATES2");												
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
			}
		}
		/// <summary>
		/// This method would invoke the method of base class to fill data in the fields of the report.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event.</param>
		private void PolicyBilling_FetchData( object p_objsender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs p_objeArgs)
		{
			LocalCache objLocalCache = null ;
            DateTime objDate;

			string sMonth = string.Empty ;
			string sDay = string.Empty ;
			string sYear = string.Empty ;
			
			try
			{
				m_iFetchCount++ ;

				if( m_iFetchCount >=2 )
				{
					p_objeArgs.EOF = true;
					return;
				}
				else
					p_objeArgs.EOF = false;

                objLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                m_objInsuredEntity.MoveTo(m_objBillXInvoice.HierarchyLevel);        // csingh7 : R5 changes MITS 16417
				// Supply data
				Fields["INSURED_NAME"].Value = m_objInsuredEntity.LastName ;
				Fields["INSURED_ATTN"].Value = m_objInsuredEntity.Contact ;
				Fields["INSURED_ADDR1"].Value = m_objInsuredEntity.Addr1 ;
				Fields["INSURED_ADDR2"].Value = m_objInsuredEntity.Addr2 ;
				Fields["INSURED_CITY"].Value = m_objInsuredEntity.City ;
				Fields["INSURED_STATE"].Value = objLocalCache.GetStateCode( m_objInsuredEntity.StateId );
				Fields["INSURED_ZIPCODE"].Value = m_objInsuredEntity.ZipCode ;
		    
				Fields["INSURER_NAME"].Value = m_objInsurerEntity.LastName ;
				Fields["INSURER_ADDR1"].Value = m_objInsurerEntity.Addr1 ;
				Fields["INSURER_ADDR2"].Value = m_objInsurerEntity.Addr2 ;
				Fields["INSURER_ADDR3"].Value = m_objInsurerEntity.City 
					+ ", " + objLocalCache.GetStateCode( m_objInsurerEntity.StateId ) 
					+ "   " + m_objInsurerEntity.ZipCode ;
                Fields["POLICY_NAME"].Value = m_sPolicy_Name;   //csingh7  : MITS 12456

                sMonth = m_objBillXInvoice.BillDate.Substring(4, 2);
                sDay = m_objBillXInvoice.BillDate.Substring(6, 2);
                sYear = m_objBillXInvoice.BillDate.Substring(0, 4);

                objDate = new DateTime(Conversion.ConvertStrToInteger(sYear),
                                                Conversion.ConvertStrToInteger(sMonth),
                                                Conversion.ConvertStrToInteger(sDay));



                Fields["DATE"].Value = objDate.ToString("MMMM") + " " + sDay + ", " + sYear;   
				Fields["INVOICE_NUMBER"].Value = m_objBillXInvoice.InvoiceRowid ;
				Fields["DUE_DATE"].Value = Common.Conversion.GetDBDateFormat( m_objBillXInvoice.DueDate , "d" ) ;
				Fields["TERM_DATES"].Value = m_sEffDate + " - " + m_sExpDate ;

				Fields["TOTAL_BILLED_PREM"].Value = string.Format("{0:C}" , m_objBillXAccount.AmountDue ) ;
				Fields["TOTAL_DIVIDEND_CREDIT"].Value = string.Format("{0:C}" , m_dblTotalDividendCredit ) ;
				Fields["NET_PREMIUM"].Value = string.Format("{0:C}" , m_objBillXAccount.AmountDue - m_dblTotalDividendCredit ) ;
				Fields["AMOUNT"].Value = string.Format("{0:C}" , m_objBillXInvoice.Amount ) ;
				Fields["NUM_INSTALLS"].Value = m_iInstallCount ;
				Fields["DUE_DATES1"].Value = m_sInstallsDue ;
				Fields["DUE_DATES2"].Value = m_sInstallsDueSecond ;				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("PolicyBilling_FetchData", m_iClientId), p_objEx);
			}
			finally
			{
				if( objLocalCache != null )
				{
					objLocalCache.Dispose();
					objLocalCache = null ;
				}
			}
		}

		/// <summary>
		/// This method will do the page setting for the EOB Report to be printed.
		/// </summary>
		/// <param name="p_objsender">Sender of the event.</param>
		/// <param name="p_objeArgs">Information about the event</param>
		private void PolicyBilling_ReportStart( object p_objsender,  System.EventArgs p_objeArgs)
		{
			try
			{
				this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("WCPPFirstInvoice.PolicyBilling_ReportStart.Error", m_iClientId), p_objEx);				
			}
		}

		#endregion 

		#region GatherData Method 
		/// <summary>
		/// This method will collect the data for the report.
		/// </summary>
		/// <param name="p_iInvoiceId">Invoice Id</param>
		/// <param name="p_objBillingMaster">Billing Master Object</param>
		public void GatherData( int p_iInvoiceId, BillingMaster p_objBillingMaster )
		{
			PolicyEnh objPolicyEnh = null ;
			DbReader objReader = null ;			
							
			string sSQL = string.Empty ;
			int iInstallCountLocal = 0 ;

			try
			{
				m_objInsuredEntity = ( Entity ) m_objDataModelFactory.GetDataModelObject( "Entity" , false );
			
				m_objBillXInvoice = (BillXInvoice) p_objBillingMaster.Invoices[ p_iInvoiceId ] ;
				if( m_objBillXInvoice != null )
				{
					//m_objInsuredEntity.EntityId = m_objBillXInvoice.HierarchyLevel ;
                    m_objInsuredEntity.MoveTo(m_objBillXInvoice.HierarchyLevel);
					objPolicyEnh = (PolicyEnh) p_objBillingMaster.Policies[ m_objBillXInvoice.PolicyId ];
					if( objPolicyEnh != null )
					{
                        m_sPolicy_Name = objPolicyEnh.PolicyName;   //csingh7  : MITS 12456
						m_objInsurerEntity = objPolicyEnh.InsurerEntity ;
						foreach( PolicyXTermEnh objPolicyXTermEnh in objPolicyEnh.PolicyXTermEnhList )
						{
							if( objPolicyXTermEnh.TermNumber == m_objBillXInvoice.TermNumber )
							{
								m_sEffDate = Common.Conversion.GetDBDateFormat( objPolicyXTermEnh.EffectiveDate , "d" ); 
								m_sExpDate = Common.Conversion.GetDBDateFormat( objPolicyXTermEnh.ExpirationDate , "d" );
								break;
							}
						}
						try
						{
							m_dblTotalDividendCredit = (double)objPolicyEnh.Supplementals["Total Dividend Credit"].Value ;
						}
						catch
						{
							m_dblTotalDividendCredit = 0 ;
						}
					}
				}
				
				// Retrieve the account record
                m_objBillXAccount = (BillXAccount)m_objDataModelFactory.GetDataModelObject("BillXAccount", false);
				sSQL = " SELECT * FROM BILL_X_ACCOUNT WHERE POLICY_ID =" + m_objBillXInvoice.PolicyId + " AND TERM_NUMBER=" + m_objBillXInvoice.TermNumber ;
				objReader = DbFactory.GetDbReader( m_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					m_objBillXAccount.AddedByUser = objReader.GetString( "ADDED_BY_USER" );
					m_objBillXAccount.AmountDue = Common.Conversion.ConvertStrToDouble( Common.Conversion.ConvertObjToStr(objReader.GetValue( "AMOUNT_DUE" ) ));
					m_objBillXAccount.BillAccountRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILL_ACCOUNT_ROWID" ), m_iClientId );
					m_objBillXAccount.BillingRuleRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( "BILLING_RULE_ROWID" ), m_iClientId );
					m_objBillXAccount.DttmRcdAdded = objReader.GetString( "DTTM_RCD_ADDED" );
					m_objBillXAccount.DttmRcdLastUpd = objReader.GetString( "DTTM_RCD_LAST_UPD" );
					m_objBillXAccount.PayPlanRowid = Common.Conversion.ConvertObjToInt( objReader.GetValue( "PAY_PLAN_ROWID" ), m_iClientId );
					m_objBillXAccount.PolicyId = Common.Conversion.ConvertObjToInt( objReader.GetValue( "POLICY_ID" ), m_iClientId );
					m_objBillXAccount.PolicyNumber = objReader.GetString( "POLICY_NUMBER" );
					m_objBillXAccount.TermNumber = Common.Conversion.ConvertObjToInt( objReader.GetValue( "TERM_NUMBER" ), m_iClientId );
					m_objBillXAccount.UpdatedByUser = objReader.GetString( "UPDATED_BY_USER" );				
				}
				objReader.Close();

				// Retrieve the address
				m_objInsuredEntity.Refresh();

				// Count the number of installments
				sSQL = " SELECT COUNT(*) FROM BILL_X_INSTLMNT WHERE POLICY_ID=" + m_objBillXInvoice.PolicyId + " AND TERM_NUMBER=" + m_objBillXInvoice.TermNumber ;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				if( objReader.Read() ) 
					m_iInstallCount = Common.Conversion.ConvertObjToInt( objReader.GetValue( 0 ), m_iClientId );
				objReader.Close();

				// Now retrieve all the due dates for the installments
				sSQL = " SELECT * FROM BILL_X_INSTLMNT WHERE POLICY_ID=" + m_objBillXInvoice.PolicyId + " AND TERM_NUMBER=" + m_objBillXInvoice.TermNumber + " ORDER BY INSTALLMENT_NUM" ;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
				while( objReader.Read() )
				{
					if( Common.Conversion.ConvertObjToInt( objReader.GetValue( "INSTALLMENT_NUM" ), m_iClientId ) == 1 )
					{
						m_sInstallsDue = Conversion.GetDBDateFormat( objReader.GetString( "INSTALL_DUE_DATE" ) , "d" );
						iInstallCountLocal++ ;
					}
					else
					{
						if( iInstallCountLocal > 5 )
							m_sInstallsDueSecond += ", " + Conversion.GetDBDateFormat( objReader.GetString( "INSTALL_DUE_DATE" ) , "d" );
						else
							m_sInstallsDue += ", " + Conversion.GetDBDateFormat( objReader.GetString( "INSTALL_DUE_DATE" ) , "d" );
					}
				}
				
				// Close the parentheses for the due dates
				if( m_sInstallsDueSecond != "" )
					m_sInstallsDueSecond += ")." ;
				else
					m_sInstallsDue += ")." ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("WCPPFirstInvoice.GatherData.Error", m_iClientId), p_objEx);				
			}
			finally
			{
				if( objPolicyEnh != null )
				{
					objPolicyEnh.Dispose();
					objPolicyEnh = null ;
				}
				if( objReader != null )
				{
					objReader.Dispose() ;
					objReader = null ;
				}				
			}
		}

		#endregion 

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.TextBox Field28 = null;
		private DataDynamics.ActiveReports.TextBox Field29 = null;
		private DataDynamics.ActiveReports.TextBox Field30 = null;
		private DataDynamics.ActiveReports.TextBox Field33 = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.TextBox Field34 = null;
		private DataDynamics.ActiveReports.TextBox Field53 = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.Label Label2 = null;
		private DataDynamics.ActiveReports.Label Label4 = null;
		private DataDynamics.ActiveReports.Label Label5 = null;
		private DataDynamics.ActiveReports.TextBox Field42 = null;
		private DataDynamics.ActiveReports.TextBox Field43 = null;
		private DataDynamics.ActiveReports.TextBox Field44 = null;
		private DataDynamics.ActiveReports.Shape Shape1 = null;
		private DataDynamics.ActiveReports.Label Label6 = null;
		private DataDynamics.ActiveReports.Label Label7 = null;
		private DataDynamics.ActiveReports.TextBox Field46 = null;
		private DataDynamics.ActiveReports.Label Label8 = null;
		private DataDynamics.ActiveReports.TextBox Field47 = null;
		private DataDynamics.ActiveReports.Label Label9 = null;
		private DataDynamics.ActiveReports.TextBox Field48 = null;
		private DataDynamics.ActiveReports.Label Label23 = null;
		private DataDynamics.ActiveReports.Shape Shape2 = null;
		private DataDynamics.ActiveReports.Label Label31 = null;
		private DataDynamics.ActiveReports.Label Label32 = null;
		private DataDynamics.ActiveReports.TextBox Field49 = null;
		private DataDynamics.ActiveReports.Label Label33 = null;
		private DataDynamics.ActiveReports.Label Label34 = null;
		private DataDynamics.ActiveReports.TextBox Field50 = null;
		private DataDynamics.ActiveReports.Label Label35 = null;
		private DataDynamics.ActiveReports.TextBox Field51 = null;
		private DataDynamics.ActiveReports.TextBox Field52 = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.TextBox Field1 = null;
		private DataDynamics.ActiveReports.TextBox Field2 = null;
		private DataDynamics.ActiveReports.TextBox Field3 = null;
		private DataDynamics.ActiveReports.TextBox Field4 = null;
		private DataDynamics.ActiveReports.TextBox Field10 = null;
        private DataDynamics.ActiveReports.TextBox Field11 = null;
		private DataDynamics.ActiveReports.TextBox Field41 = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WCPPFirstInvoice));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.Label2 = new DataDynamics.ActiveReports.Label();
            this.Label4 = new DataDynamics.ActiveReports.Label();
            this.Label5 = new DataDynamics.ActiveReports.Label();
            this.Field42 = new DataDynamics.ActiveReports.TextBox();
            this.Field43 = new DataDynamics.ActiveReports.TextBox();
            this.Field44 = new DataDynamics.ActiveReports.TextBox();
            this.Shape1 = new DataDynamics.ActiveReports.Shape();
            this.Label6 = new DataDynamics.ActiveReports.Label();
            this.Label7 = new DataDynamics.ActiveReports.Label();
            this.Field46 = new DataDynamics.ActiveReports.TextBox();
            this.Label8 = new DataDynamics.ActiveReports.Label();
            this.Field47 = new DataDynamics.ActiveReports.TextBox();
            this.Label9 = new DataDynamics.ActiveReports.Label();
            this.Field48 = new DataDynamics.ActiveReports.TextBox();
            this.Label23 = new DataDynamics.ActiveReports.Label();
            this.Shape2 = new DataDynamics.ActiveReports.Shape();
            this.Label31 = new DataDynamics.ActiveReports.Label();
            this.Label32 = new DataDynamics.ActiveReports.Label();
            this.Field49 = new DataDynamics.ActiveReports.TextBox();
            this.Label33 = new DataDynamics.ActiveReports.Label();
            this.Label34 = new DataDynamics.ActiveReports.Label();
            this.Field50 = new DataDynamics.ActiveReports.TextBox();
            this.Label35 = new DataDynamics.ActiveReports.Label();
            this.Field51 = new DataDynamics.ActiveReports.TextBox();
            this.Field52 = new DataDynamics.ActiveReports.TextBox();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.Field28 = new DataDynamics.ActiveReports.TextBox();
            this.Field29 = new DataDynamics.ActiveReports.TextBox();
            this.Field30 = new DataDynamics.ActiveReports.TextBox();
            this.Field33 = new DataDynamics.ActiveReports.TextBox();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.Field34 = new DataDynamics.ActiveReports.TextBox();
            this.Field53 = new DataDynamics.ActiveReports.TextBox();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.Field1 = new DataDynamics.ActiveReports.TextBox();
            this.Field2 = new DataDynamics.ActiveReports.TextBox();
            this.Field3 = new DataDynamics.ActiveReports.TextBox();
            this.Field4 = new DataDynamics.ActiveReports.TextBox();
            this.Field10 = new DataDynamics.ActiveReports.TextBox();
            this.Field11 = new DataDynamics.ActiveReports.TextBox();
            this.Field41 = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Label2,
            this.Label4,
            this.Label5,
            this.Field42,
            this.Field43,
            this.Field44,
            this.Shape1,
            this.Label6,
            this.Label7,
            this.Field46,
            this.Label8,
            this.Field47,
            this.Label9,
            this.Field48,
            this.Label23,
            this.Shape2,
            this.Label31,
            this.Label32,
            this.Field49,
            this.Label33,
            this.Label34,
            this.Field50,
            this.Label35,
            this.Field51,
            this.Field52});
            this.Detail.Height = 6.270833F;
            this.Detail.Name = "Detail";
            // 
            // Label2
            // 
            this.Label2.Border.BottomColor = System.Drawing.Color.Black;
            this.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.LeftColor = System.Drawing.Color.Black;
            this.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.RightColor = System.Drawing.Color.Black;
            this.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.TopColor = System.Drawing.Color.Black;
            this.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = "";
            this.Label2.Left = 0.125F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label2.Text = "Invoice Number:";
            this.Label2.Top = 0.5F;
            this.Label2.Width = 1.375F;
            // 
            // Label4
            // 
            this.Label4.Border.BottomColor = System.Drawing.Color.Black;
            this.Label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Border.LeftColor = System.Drawing.Color.Black;
            this.Label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Border.RightColor = System.Drawing.Color.Black;
            this.Label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Border.TopColor = System.Drawing.Color.Black;
            this.Label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = "";
            this.Label4.Left = 0.8125F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label4.Text = "Workers\' Compensation Coverage:";
            this.Label4.Top = 1.0625F;
            this.Label4.Width = 2.6875F;
            // 
            // Label5
            // 
            this.Label5.Border.BottomColor = System.Drawing.Color.Black;
            this.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Border.LeftColor = System.Drawing.Color.Black;
            this.Label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Border.RightColor = System.Drawing.Color.Black;
            this.Label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Border.TopColor = System.Drawing.Color.Black;
            this.Label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = "";
            this.Label5.Left = 4F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label5.Text = "Due Date:";
            this.Label5.Top = 0.5F;
            this.Label5.Width = 0.875F;
            // 
            // Field42
            // 
            this.Field42.Border.BottomColor = System.Drawing.Color.Black;
            this.Field42.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.Border.LeftColor = System.Drawing.Color.Black;
            this.Field42.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.Border.RightColor = System.Drawing.Color.Black;
            this.Field42.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.Border.TopColor = System.Drawing.Color.Black;
            this.Field42.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field42.DataField = "DUE_DATE";
            this.Field42.Height = 0.1875F;
            this.Field42.Left = 4.875F;
            this.Field42.Name = "Field42";
            this.Field42.Style = "ddo-char-set: 0; text-align: left; font-size: 12pt; font-family: Arial; ";
            this.Field42.Text = "DUE_DATE";
            this.Field42.Top = 0.5F;
            this.Field42.Width = 1.375F;
            // 
            // Field43
            // 
            this.Field43.Border.BottomColor = System.Drawing.Color.Black;
            this.Field43.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.Border.LeftColor = System.Drawing.Color.Black;
            this.Field43.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.Border.RightColor = System.Drawing.Color.Black;
            this.Field43.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.Border.TopColor = System.Drawing.Color.Black;
            this.Field43.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field43.DataField = "TERM_DATES";
            this.Field43.Height = 0.1875F;
            this.Field43.Left = 3.5F;
            this.Field43.Name = "Field43";
            this.Field43.Style = "ddo-char-set: 0; text-align: left; font-size: 12pt; font-family: Arial; ";
            this.Field43.Text = "TERM_DATES";
            this.Field43.Top = 1.0625F;
            this.Field43.Width = 2F;
            // 
            // Field44
            // 
            this.Field44.Border.BottomColor = System.Drawing.Color.Black;
            this.Field44.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.Border.LeftColor = System.Drawing.Color.Black;
            this.Field44.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.Border.RightColor = System.Drawing.Color.Black;
            this.Field44.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.Border.TopColor = System.Drawing.Color.Black;
            this.Field44.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field44.DataField = "INVOICE_NUMBER";
            this.Field44.Height = 0.1875F;
            this.Field44.Left = 1.5F;
            this.Field44.Name = "Field44";
            this.Field44.Style = "ddo-char-set: 0; text-align: left; font-size: 12pt; font-family: Arial; ";
            this.Field44.Text = "INVOICE_NUMBER";
            this.Field44.Top = 0.5F;
            this.Field44.Width = 1.375F;
            // 
            // Shape1
            // 
            this.Shape1.Border.BottomColor = System.Drawing.Color.Black;
            this.Shape1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape1.Border.LeftColor = System.Drawing.Color.Black;
            this.Shape1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape1.Border.RightColor = System.Drawing.Color.Black;
            this.Shape1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape1.Border.TopColor = System.Drawing.Color.Black;
            this.Shape1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape1.Height = 1.75F;
            this.Shape1.Left = 0.1875F;
            this.Shape1.Name = "Shape1";
            this.Shape1.RoundingRadius = 9.999999F;
            this.Shape1.Top = 1.5625F;
            this.Shape1.Width = 6.125F;
            // 
            // Label6
            // 
            this.Label6.Border.BottomColor = System.Drawing.Color.Black;
            this.Label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Border.LeftColor = System.Drawing.Color.Black;
            this.Label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Border.RightColor = System.Drawing.Color.Black;
            this.Label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Border.TopColor = System.Drawing.Color.Black;
            this.Label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = "";
            this.Label6.Left = 0.3125F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-weight: bold; font-size: 12pt; font-family: Arial; ";
            this.Label6.Text = "Full Payment Option";
            this.Label6.Top = 1.6875F;
            this.Label6.Width = 1.6875F;
            // 
            // Label7
            // 
            this.Label7.Border.BottomColor = System.Drawing.Color.Black;
            this.Label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Border.LeftColor = System.Drawing.Color.Black;
            this.Label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Border.RightColor = System.Drawing.Color.Black;
            this.Label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Border.TopColor = System.Drawing.Color.Black;
            this.Label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = "";
            this.Label7.Left = 1.625F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label7.Text = "Total Billed Premium";
            this.Label7.Top = 2.1875F;
            this.Label7.Width = 1.625F;
            // 
            // Field46
            // 
            this.Field46.Border.BottomColor = System.Drawing.Color.Black;
            this.Field46.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.Border.LeftColor = System.Drawing.Color.Black;
            this.Field46.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.Border.RightColor = System.Drawing.Color.Black;
            this.Field46.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.Border.TopColor = System.Drawing.Color.Black;
            this.Field46.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field46.DataField = "TOTAL_BILLED_PREM";
            this.Field46.Height = 0.1875F;
            this.Field46.Left = 4F;
            this.Field46.Name = "Field46";
            this.Field46.Style = "ddo-char-set: 0; text-align: right; font-size: 12pt; font-family: Arial; ";
            this.Field46.Text = "TOTAL_BILLED_PREM";
            this.Field46.Top = 2.1875F;
            this.Field46.Width = 1.375F;
            // 
            // Label8
            // 
            this.Label8.Border.BottomColor = System.Drawing.Color.Black;
            this.Label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Border.LeftColor = System.Drawing.Color.Black;
            this.Label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Border.RightColor = System.Drawing.Color.Black;
            this.Label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Border.TopColor = System.Drawing.Color.Black;
            this.Label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = "";
            this.Label8.Left = 1.625F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 12pt; font-family: Arial; ";
            this.Label8.Text = "Total Dividend Credit";
            this.Label8.Top = 2.5625F;
            this.Label8.Width = 1.6875F;
            // 
            // Field47
            // 
            this.Field47.Border.BottomColor = System.Drawing.Color.Black;
            this.Field47.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.Border.LeftColor = System.Drawing.Color.Black;
            this.Field47.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.Border.RightColor = System.Drawing.Color.Black;
            this.Field47.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.Border.TopColor = System.Drawing.Color.Black;
            this.Field47.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field47.DataField = "TOTAL_DIVIDEND_CREDIT";
            this.Field47.Height = 0.1875F;
            this.Field47.Left = 4F;
            this.Field47.Name = "Field47";
            this.Field47.Style = "ddo-char-set: 0; text-align: right; font-size: 12pt; font-family: Arial; ";
            this.Field47.Text = "TOTAL_DIVIDENT_CREDIT";
            this.Field47.Top = 2.5625F;
            this.Field47.Width = 1.375F;
            // 
            // Label9
            // 
            this.Label9.Border.BottomColor = System.Drawing.Color.Black;
            this.Label9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Border.LeftColor = System.Drawing.Color.Black;
            this.Label9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Border.RightColor = System.Drawing.Color.Black;
            this.Label9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Border.TopColor = System.Drawing.Color.Black;
            this.Label9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = "";
            this.Label9.Left = 1.625F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold; font-size: 12pt; font-family: Arial; ";
            this.Label9.Text = "Total Annual Premium";
            this.Label9.Top = 2.9375F;
            this.Label9.Width = 1.75F;
            // 
            // Field48
            // 
            this.Field48.Border.BottomColor = System.Drawing.Color.Black;
            this.Field48.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field48.Border.LeftColor = System.Drawing.Color.Black;
            this.Field48.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field48.Border.RightColor = System.Drawing.Color.Black;
            this.Field48.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field48.Border.TopColor = System.Drawing.Color.Black;
            this.Field48.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field48.DataField = "NET_PREMIUM";
            this.Field48.Height = 0.1875F;
            this.Field48.Left = 4F;
            this.Field48.Name = "Field48";
            this.Field48.Style = "ddo-char-set: 0; text-align: right; font-weight: bold; font-size: 12pt; font-fami" +
                "ly: Arial; ";
            this.Field48.Text = "NET_PREMIUM";
            this.Field48.Top = 2.9375F;
            this.Field48.Width = 1.375F;
            // 
            // Label23
            // 
            this.Label23.Border.BottomColor = System.Drawing.Color.Black;
            this.Label23.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Border.LeftColor = System.Drawing.Color.Black;
            this.Label23.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Border.RightColor = System.Drawing.Color.Black;
            this.Label23.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Border.TopColor = System.Drawing.Color.Black;
            this.Label23.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label23.Height = 0.1875F;
            this.Label23.HyperLink = "";
            this.Label23.Left = 0.125F;
            this.Label23.Name = "Label23";
            this.Label23.Style = "font-size: 10pt; ";
            this.Label23.Text = "To insure proper handling, please return a copy of this invoice with your check.";
            this.Label23.Top = 5.9375F;
            this.Label23.Width = 5.3125F;
            // 
            // Shape2
            // 
            this.Shape2.Border.BottomColor = System.Drawing.Color.Black;
            this.Shape2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape2.Border.LeftColor = System.Drawing.Color.Black;
            this.Shape2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape2.Border.RightColor = System.Drawing.Color.Black;
            this.Shape2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape2.Border.TopColor = System.Drawing.Color.Black;
            this.Shape2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Shape2.Height = 1F;
            this.Shape2.Left = 0.1875F;
            this.Shape2.Name = "Shape2";
            this.Shape2.RoundingRadius = 9.999999F;
            this.Shape2.Top = 4.875F;
            this.Shape2.Width = 6.125F;
            // 
            // Label31
            // 
            this.Label31.Border.BottomColor = System.Drawing.Color.Black;
            this.Label31.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label31.Border.LeftColor = System.Drawing.Color.Black;
            this.Label31.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label31.Border.RightColor = System.Drawing.Color.Black;
            this.Label31.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label31.Border.TopColor = System.Drawing.Color.Black;
            this.Label31.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label31.Height = 0.1875F;
            this.Label31.HyperLink = "";
            this.Label31.Left = 0.3125F;
            this.Label31.Name = "Label31";
            this.Label31.Style = "font-weight: bold; font-size: 12pt; font-family: Arial; ";
            this.Label31.Text = "Payment Plan Option";
            this.Label31.Top = 5F;
            this.Label31.Width = 1.75F;
            // 
            // Label32
            // 
            this.Label32.Border.BottomColor = System.Drawing.Color.Black;
            this.Label32.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label32.Border.LeftColor = System.Drawing.Color.Black;
            this.Label32.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label32.Border.RightColor = System.Drawing.Color.Black;
            this.Label32.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label32.Border.TopColor = System.Drawing.Color.Black;
            this.Label32.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label32.Height = 0.1875F;
            this.Label32.HyperLink = "";
            this.Label32.Left = 1.625F;
            this.Label32.Name = "Label32";
            this.Label32.Style = "font-weight: bold; font-size: 12pt; font-family: Arial; ";
            this.Label32.Text = "Installment Due";
            this.Label32.Top = 5.4375F;
            this.Label32.Width = 1.625F;
            // 
            // Field49
            // 
            this.Field49.Border.BottomColor = System.Drawing.Color.Black;
            this.Field49.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field49.Border.LeftColor = System.Drawing.Color.Black;
            this.Field49.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field49.Border.RightColor = System.Drawing.Color.Black;
            this.Field49.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field49.Border.TopColor = System.Drawing.Color.Black;
            this.Field49.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field49.DataField = "AMOUNT";
            this.Field49.Height = 0.1875F;
            this.Field49.Left = 4F;
            this.Field49.Name = "Field49";
            this.Field49.Style = "ddo-char-set: 0; text-align: right; font-weight: bold; font-size: 12pt; font-fami" +
                "ly: Arial; ";
            this.Field49.Text = "AMOUNT";
            this.Field49.Top = 5.4375F;
            this.Field49.Width = 1.375F;
            // 
            // Label33
            // 
            this.Label33.Border.BottomColor = System.Drawing.Color.Black;
            this.Label33.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label33.Border.LeftColor = System.Drawing.Color.Black;
            this.Label33.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label33.Border.RightColor = System.Drawing.Color.Black;
            this.Label33.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label33.Border.TopColor = System.Drawing.Color.Black;
            this.Label33.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label33.Height = 0.25F;
            this.Label33.HyperLink = "";
            this.Label33.Left = 0.4375F;
            this.Label33.Name = "Label33";
            this.Label33.Style = "text-align: center; font-weight: bold; font-size: 14.5pt; ";
            this.Label33.Text = "***Qualifies for Optional Payment Plan***";
            this.Label33.Top = 3.5F;
            this.Label33.Width = 5.5625F;
            // 
            // Label34
            // 
            this.Label34.Border.BottomColor = System.Drawing.Color.Black;
            this.Label34.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label34.Border.LeftColor = System.Drawing.Color.Black;
            this.Label34.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label34.Border.RightColor = System.Drawing.Color.Black;
            this.Label34.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label34.Border.TopColor = System.Drawing.Color.Black;
            this.Label34.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label34.Height = 0.25F;
            this.Label34.HyperLink = "";
            this.Label34.Left = 0.1875F;
            this.Label34.Name = "Label34";
            this.Label34.Style = "text-align: left; font-weight: bold; font-size: 14.5pt; ";
            this.Label34.Text = "You may choose to pay the Total Annual  Premium in";
            this.Label34.Top = 3.9375F;
            this.Label34.Width = 5.125F;
            // 
            // Field50
            // 
            this.Field50.Border.BottomColor = System.Drawing.Color.Black;
            this.Field50.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field50.Border.LeftColor = System.Drawing.Color.Black;
            this.Field50.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field50.Border.RightColor = System.Drawing.Color.Black;
            this.Field50.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field50.Border.TopColor = System.Drawing.Color.Black;
            this.Field50.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field50.DataField = "NUM_INSTALLS";
            this.Field50.Height = 0.25F;
            this.Field50.Left = 5.3125F;
            this.Field50.Name = "Field50";
            this.Field50.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 14.5pt; font-fam" +
                "ily: Arial; ";
            this.Field50.Text = "NUM_INSTALLS";
            this.Field50.Top = 3.9375F;
            this.Field50.Width = 0.5625F;
            // 
            // Label35
            // 
            this.Label35.Border.BottomColor = System.Drawing.Color.Black;
            this.Label35.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label35.Border.LeftColor = System.Drawing.Color.Black;
            this.Label35.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label35.Border.RightColor = System.Drawing.Color.Black;
            this.Label35.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label35.Border.TopColor = System.Drawing.Color.Black;
            this.Label35.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label35.Height = 0.25F;
            this.Label35.HyperLink = "";
            this.Label35.Left = 0.1875F;
            this.Label35.Name = "Label35";
            this.Label35.Style = "text-align: left; font-weight: bold; font-size: 14.5pt; ";
            this.Label35.Text = "installments (due";
            this.Label35.Top = 4.25F;
            this.Label35.Width = 1.6875F;
            // 
            // Field51
            // 
            this.Field51.Border.BottomColor = System.Drawing.Color.Black;
            this.Field51.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field51.Border.LeftColor = System.Drawing.Color.Black;
            this.Field51.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field51.Border.RightColor = System.Drawing.Color.Black;
            this.Field51.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field51.Border.TopColor = System.Drawing.Color.Black;
            this.Field51.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field51.DataField = "DUE_DATES1";
            this.Field51.Height = 0.25F;
            this.Field51.Left = 1.875F;
            this.Field51.Name = "Field51";
            this.Field51.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 14.5pt; font-fam" +
                "ily: Arial; ";
            this.Field51.Text = "DUE_DATES1";
            this.Field51.Top = 4.25F;
            this.Field51.Width = 4.4375F;
            // 
            // Field52
            // 
            this.Field52.Border.BottomColor = System.Drawing.Color.Black;
            this.Field52.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field52.Border.LeftColor = System.Drawing.Color.Black;
            this.Field52.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field52.Border.RightColor = System.Drawing.Color.Black;
            this.Field52.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field52.Border.TopColor = System.Drawing.Color.Black;
            this.Field52.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field52.DataField = "DUE_DATES2";
            this.Field52.Height = 0.25F;
            this.Field52.Left = 0.1875F;
            this.Field52.Name = "Field52";
            this.Field52.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 14.5pt; font-fam" +
                "ily: Arial; ";
            this.Field52.Text = "DUE_DATES2";
            this.Field52.Top = 4.5625F;
            this.Field52.Width = 6.125F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Field28,
            this.Field29,
            this.Field30,
            this.Field33,
            this.Label1,
            this.Field34,
            this.Field53});
            this.PageHeader.Height = 1.790972F;
            this.PageHeader.Name = "PageHeader";
            // 
            // Field28
            // 
            this.Field28.Border.BottomColor = System.Drawing.Color.Black;
            this.Field28.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.Border.LeftColor = System.Drawing.Color.Black;
            this.Field28.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.Border.RightColor = System.Drawing.Color.Black;
            this.Field28.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.Border.TopColor = System.Drawing.Color.Black;
            this.Field28.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field28.DataField = "INSURER_NAME";
            this.Field28.Height = 0.1875F;
            this.Field28.Left = 0.9375F;
            this.Field28.Name = "Field28";
            this.Field28.Style = "ddo-char-set: 178; text-align: center; font-weight: bold; font-size: 12pt; font-f" +
                "amily: Arial; ";
            this.Field28.Text = "INSURER_NAME";
            this.Field28.Top = 0.375F;
            this.Field28.Width = 4.5625F;
            // 
            // Field29
            // 
            this.Field29.Border.BottomColor = System.Drawing.Color.Black;
            this.Field29.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.Border.LeftColor = System.Drawing.Color.Black;
            this.Field29.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.Border.RightColor = System.Drawing.Color.Black;
            this.Field29.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.Border.TopColor = System.Drawing.Color.Black;
            this.Field29.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field29.DataField = "INSURER_ADDR1";
            this.Field29.Height = 0.1875F;
            this.Field29.Left = 0.9375F;
            this.Field29.Name = "Field29";
            this.Field29.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field29.Text = "INSURER_ADDR1";
            this.Field29.Top = 0.5625F;
            this.Field29.Width = 4.5625F;
            // 
            // Field30
            // 
            this.Field30.Border.BottomColor = System.Drawing.Color.Black;
            this.Field30.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.Border.LeftColor = System.Drawing.Color.Black;
            this.Field30.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.Border.RightColor = System.Drawing.Color.Black;
            this.Field30.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.Border.TopColor = System.Drawing.Color.Black;
            this.Field30.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field30.DataField = "INSURER_ADDR3";
            this.Field30.Height = 0.1875F;
            this.Field30.Left = 0.9375F;
            this.Field30.Name = "Field30";
            this.Field30.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field30.Text = "INSURER_ADDR3";
            this.Field30.Top = 0.9375F;
            this.Field30.Width = 4.5625F;
            // 
            // Field33
            // 
            this.Field33.Border.BottomColor = System.Drawing.Color.Black;
            this.Field33.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.Border.LeftColor = System.Drawing.Color.Black;
            this.Field33.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.Border.RightColor = System.Drawing.Color.Black;
            this.Field33.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.Border.TopColor = System.Drawing.Color.Black;
            this.Field33.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field33.DataField = "INSURER_ADDR2";
            this.Field33.Height = 0.1875F;
            this.Field33.Left = 0.9375F;
            this.Field33.Name = "Field33";
            this.Field33.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field33.Text = "INSURER_ADDR2";
            this.Field33.Top = 0.75F;
            this.Field33.Width = 4.5625F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.3125F;
            this.Label1.HyperLink = "";
            this.Label1.Left = 0.9375F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "text-align: center; font-weight: bold; font-size: 18pt; ";
            this.Label1.Text = "INVOICE";
            this.Label1.Top = 0F;
            this.Label1.Width = 4.5625F;
            // 
            // Field34
            // 
            this.Field34.Border.BottomColor = System.Drawing.Color.Black;
            this.Field34.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.Border.LeftColor = System.Drawing.Color.Black;
            this.Field34.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.Border.RightColor = System.Drawing.Color.Black;
            this.Field34.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.Border.TopColor = System.Drawing.Color.Black;
            this.Field34.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field34.DataField = "DATE";
            this.Field34.Height = 0.188F;
            this.Field34.Left = 2.1875F;
            this.Field34.Name = "Field34";
            this.Field34.Style = "ddo-char-set: 178; text-align: center; font-size: 12pt; font-family: Arial; ";
            this.Field34.Text = "DATE";
            this.Field34.Top = 1.1875F;
            this.Field34.Width = 2F;
            // 
            // Field53
            // 
            this.Field53.Border.BottomColor = System.Drawing.Color.Black;
            this.Field53.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field53.Border.LeftColor = System.Drawing.Color.Black;
            this.Field53.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field53.Border.RightColor = System.Drawing.Color.Black;
            this.Field53.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field53.Border.TopColor = System.Drawing.Color.Black;
            this.Field53.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field53.CountNullValues = true;
            this.Field53.DataField = "POLICY_NAME";
            this.Field53.Height = 0.1875F;
            this.Field53.Left = 0.875F;
            this.Field53.Name = "Field53";
            this.Field53.Style = "ddo-char-set: 178; text-align: center; font-weight: bold; font-size: 12pt; font-f" +
                "amily: Arial; ";
            this.Field53.Text = "POLICY_NAME";
            this.Field53.Top = 1.4375F;
            this.Field53.Width = 4.5625F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field10,
            this.Field11,
            this.Field41});
            this.PageFooter.Height = 1.180556F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Field1
            // 
            this.Field1.Border.BottomColor = System.Drawing.Color.Black;
            this.Field1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.Border.LeftColor = System.Drawing.Color.Black;
            this.Field1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.Border.RightColor = System.Drawing.Color.Black;
            this.Field1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.Border.TopColor = System.Drawing.Color.Black;
            this.Field1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field1.DataField = "INSURED_NAME";
            this.Field1.Height = 0.125F;
            this.Field1.Left = 0.25F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "ddo-char-set: 0; font-weight: bold; font-size: 8.5pt; font-family: Microsoft Sans" +
                " Serif; ";
            this.Field1.Text = "INSURED_NAME";
            this.Field1.Top = 0.125F;
            this.Field1.Width = 3.0625F;
            // 
            // Field2
            // 
            this.Field2.Border.BottomColor = System.Drawing.Color.Black;
            this.Field2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.Border.LeftColor = System.Drawing.Color.Black;
            this.Field2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.Border.RightColor = System.Drawing.Color.Black;
            this.Field2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.Border.TopColor = System.Drawing.Color.Black;
            this.Field2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field2.DataField = "INSURED_ATTN";
            this.Field2.Height = 0.125F;
            this.Field2.Left = 0.25F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field2.Text = "INSURED_ATTN";
            this.Field2.Top = 0.25F;
            this.Field2.Width = 3.0625F;
            // 
            // Field3
            // 
            this.Field3.Border.BottomColor = System.Drawing.Color.Black;
            this.Field3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.Border.LeftColor = System.Drawing.Color.Black;
            this.Field3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.Border.RightColor = System.Drawing.Color.Black;
            this.Field3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.Border.TopColor = System.Drawing.Color.Black;
            this.Field3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field3.DataField = "INSURED_ADDR1";
            this.Field3.Height = 0.125F;
            this.Field3.Left = 0.25F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field3.Text = "INSURED_ADDR1";
            this.Field3.Top = 0.375F;
            this.Field3.Width = 3.0625F;
            // 
            // Field4
            // 
            this.Field4.Border.BottomColor = System.Drawing.Color.Black;
            this.Field4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.Border.LeftColor = System.Drawing.Color.Black;
            this.Field4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.Border.RightColor = System.Drawing.Color.Black;
            this.Field4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.Border.TopColor = System.Drawing.Color.Black;
            this.Field4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field4.DataField = "INSURED_CITY";
            this.Field4.Height = 0.125F;
            this.Field4.Left = 0.25F;
            this.Field4.Name = "Field4";
            this.Field4.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field4.Text = "INSURED_CITY";
            this.Field4.Top = 0.625F;
            this.Field4.Width = 1.25F;
            // 
            // Field10
            // 
            this.Field10.Border.BottomColor = System.Drawing.Color.Black;
            this.Field10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.Border.LeftColor = System.Drawing.Color.Black;
            this.Field10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.Border.RightColor = System.Drawing.Color.Black;
            this.Field10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.Border.TopColor = System.Drawing.Color.Black;
            this.Field10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field10.DataField = "INSURED_STATE";
            this.Field10.Height = 0.125F;
            this.Field10.Left = 1.563F;
            this.Field10.Name = "Field10";
            this.Field10.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field10.Text = "INSURED_STATE";
            this.Field10.Top = 0.625F;
            this.Field10.Width = 0.375F;
            // 
            // Field11
            // 
            this.Field11.Border.BottomColor = System.Drawing.Color.Black;
            this.Field11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.Border.LeftColor = System.Drawing.Color.Black;
            this.Field11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.Border.RightColor = System.Drawing.Color.Black;
            this.Field11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.Border.TopColor = System.Drawing.Color.Black;
            this.Field11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field11.DataField = "INSURED_ZIPCODE";
            this.Field11.Height = 0.125F;
            this.Field11.Left = 2F;
            this.Field11.Name = "Field11";
            this.Field11.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field11.Text = "INSURED_ZIPCODE";
            this.Field11.Top = 0.625F;
            this.Field11.Width = 0.875F;
            // 
            // Field41
            // 
            this.Field41.Border.BottomColor = System.Drawing.Color.Black;
            this.Field41.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.Border.LeftColor = System.Drawing.Color.Black;
            this.Field41.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.Border.RightColor = System.Drawing.Color.Black;
            this.Field41.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.Border.TopColor = System.Drawing.Color.Black;
            this.Field41.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Field41.DataField = "INSURED_ADDR2";
            this.Field41.Height = 0.125F;
            this.Field41.Left = 0.25F;
            this.Field41.Name = "Field41";
            this.Field41.Style = "ddo-char-set: 0; font-size: 8.5pt; font-family: Microsoft Sans Serif; ";
            this.Field41.Text = "INSURED_ADDR2";
            this.Field41.Top = 0.5F;
            this.Field41.Width = 3.0625F;
            // 
            // WCPPFirstInvoice
            // 
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.PolicyBilling_FetchData);
            this.ReportStart += new System.EventHandler(this.PolicyBilling_ReportStart);
            this.DataInitialize += new System.EventHandler(this.PolicyBilling_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		 }

		#endregion
	}
}
