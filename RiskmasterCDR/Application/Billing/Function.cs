﻿
using System;
using System.IO; 
using System.Xml ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Common ;
using Riskmaster.Db ;
using Riskmaster.DataModel ;
using System.Collections ;

namespace Riskmaster.Application.EnhancePolicy.Billing
{
	/// <summary>
	/// Summary description for Functions.
	/// </summary>
	public class Function
	{	
		
		/// <summary>
		/// Creates a memory stream of given file
		/// </summary>
		/// <param name="p_sFilePath">Complete File Path</param>
		/// <returns>Memory stream of the given file</returns>
        //rupal:start, mits 27977
        private static string[] sArr = new string[] { "~|*$" };
        private static string[] sArr1 = new string[] { "^|*$" };
        public static string[] CodeIdSeperator
        {
            get
            {
                return sArr;
            }
        }
        public static string[] NewCodeSeperator
        {
            get
            {
                return sArr1;
            }
        }
        //rupal:end

		public static MemoryStream CreateStream( string p_sFilePath,int p_iClientId )
		{
			MemoryStream objMemoryStream = null;
			FileStream objFileStream = null;
			BinaryReader objBinaryReader = null ;
			Byte[] arrByte = null;

			try
			{
				if( !File.Exists( p_sFilePath ) )
					throw new FileNotFoundException(Globalization.GetString("Function.FileNotFound.Error",p_iClientId));//psharma206
				objFileStream = new FileStream( p_sFilePath , FileMode.Open , FileAccess.Read );
				objMemoryStream = new MemoryStream( (int)objFileStream.Length );
				objBinaryReader = new BinaryReader( objFileStream );
				arrByte = objBinaryReader.ReadBytes( (int)objFileStream.Length );
				objMemoryStream.Write( arrByte , 0 , (int)objFileStream.Length );
				return( objMemoryStream );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.CreateStream.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objFileStream != null )
				{
					objFileStream.Close();
					objFileStream.Dispose();
				}
				if( objBinaryReader != null )
				{
					objBinaryReader.Close();
					objBinaryReader = null ;
				}
				arrByte = null;
				if( objMemoryStream != null )
				{
					objMemoryStream.Close();
					objMemoryStream.Dispose();
				}
			}			
		}


		#region Get Configuration File Options
		/// <summary>
		/// Gets the save path for Enhanced Policy Billing files
		/// </summary>
		/// <returns></returns>
		public static string GetSavePath()
		{
            return RMConfigurator.CombineFilePath(RMConfigurator.UserDataPath, "Billing");
		}

		#endregion

		#region Date functions
		public static string AddDays( string p_sDate , int p_iNumberOfDays )
		{
			// TODO
//			If Mid$(sDiaryDate, 2, 1) = "/" Or Mid$(sDiaryDate, 3, 1) = "/" Then
//				sDiaryDate = DateAdd("d", BR.DueDays, sDiaryDate)
//			Else
//				sDiaryDate = DateAdd("d", BR.DueDays, vMakeDateUseful(sDiaryDate))
//			End If

			return ( Conversion.GetDate( Conversion.ToDate( Conversion.GetDBDateFormat( p_sDate , "yyyyMMdd" ) ).AddDays( p_iNumberOfDays ).ToString("d" )) );
		}

		public static string AddMonths( string p_sDate , int p_iNumberOfMonths )
		{
			// TODO
			//			If Mid$(sDiaryDate, 2, 1) = "/" Or Mid$(sDiaryDate, 3, 1) = "/" Then
			//				sDiaryDate = DateAdd("d", BR.DueDays, sDiaryDate)
			//			Else
			//				sDiaryDate = DateAdd("d", BR.DueDays, vMakeDateUseful(sDiaryDate))
			//			End If

            return (Conversion.GetDate(Conversion.ToDate(Conversion.GetDBDateFormat(p_sDate, "yyyyMMdd")).AddMonths(p_iNumberOfMonths).ToString("d")));
		}

		#endregion 

		#region Policy Information function
		public static string GetPolicyName( int p_iPolicyId , string p_sConnectionString,int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			string sPolicyName = string.Empty ;

			try
			{
				sSQL = " SELECT POLICY_NAME FROM POLICY_ENH WHERE POLICY_ID = " + p_iPolicyId ;

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					sPolicyName = objReader.GetString( "POLICY_NAME" );				
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetPolicyName.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( sPolicyName );
		}

        public static int GetPolicyStatus(int p_iPolicyId, string p_sConnectionString, int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			int iPolStatus = 0 ;

			try
			{
				sSQL = " SELECT POLICY_STATUS_CODE FROM POLICY_ENH WHERE POLICY_ID = " + p_iPolicyId ;

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
                    iPolStatus = Common.Conversion.ConvertObjToInt(objReader.GetValue("POLICY_STATUS_CODE"), p_iClientId);
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetPolicyStatus.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( iPolStatus );
		}


        public static int GetPolicyStatus(int p_iPolicyId, int p_iTermNumber, int p_iSequenceNumber, string p_sConnectionString, int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			int iPolStatus = 0 ;

			try
			{
				sSQL = "SELECT POLICY_STATUS FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + p_iPolicyId + " and TERM_NUMBER = " + p_iTermNumber + " and  SEQUENCE_ALPHA > " + p_iSequenceNumber ;

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
                    iPolStatus = Common.Conversion.ConvertObjToInt(objReader.GetValue("POLICY_STATUS"), p_iClientId);
				}
				else
				{
					objReader.Close();
					sSQL = "SELECT POLICY_STATUS FROM POLICY_X_TRANS_ENH WHERE POLICY_ID = " + p_iPolicyId + " and TERM_NUMBER = " + p_iTermNumber + " and  SEQUENCE_ALPHA = " + p_iSequenceNumber ;

					objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
					if( objReader.Read() )
					{
                        iPolStatus = Common.Conversion.ConvertObjToInt(objReader.GetValue("POLICY_STATUS"), p_iClientId);
					}
					objReader.Close();
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetPolicyStatus.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( iPolStatus );
		}



        public static bool GetRoundAmountFlag(string p_sConnectionString, int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			bool bRoundAmountFlag = false ;

			try
			{
				sSQL = " SELECT * FROM SYS_POL_OPTIONS " ;

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					bRoundAmountFlag = objReader.GetBoolean( "ROUND_AMTS_FLAG" );				
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetRoundAmountFlag.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( bRoundAmountFlag );
		}


		public static int GetMaxTermNumber( int p_iPolicyId , string p_sConnectionString ,int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			int iMaxTermNumber = 0 ;

			try
			{
                // Naresh Do Not Bill Changes
                // sSQL = "SELECT MAX(TERM_NUMBER) FROM  POLICY_X_TERM_ENH WHERE POLICY_ID = " + p_iPolicyId ;
                sSQL = "SELECT MAX(TERM_NUMBER) FROM POLICY_X_TERM_ENH WHERE TERM_NUMBER IN (SELECT MAX(TERM_NUMBER) FROM POLICY_X_BILL_ENH WHERE DONOTBILL =0 and POLICY_ID= " + p_iPolicyId + ")";

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
                    iMaxTermNumber = Common.Conversion.ConvertObjToInt(objReader.GetValue(0), p_iClientId);		
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetMaxTermNumber.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( iMaxTermNumber );
		}


        public static int GetMaxSequenceNumber(int p_iPolicyId, int p_iTermNumber, string p_sConnectionString, int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			int iMaxSequenceNumber = 0 ;

			try
			{
				sSQL = "SELECT MAX(SEQUENCE_ALPHA) FROM POLICY_X_TERM_ENH where POLICY_ID = " + p_iPolicyId + " and TERM_NUMBER = " + p_iTermNumber ;

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
                    iMaxSequenceNumber = Common.Conversion.ConvertObjToInt(objReader.GetValue(0), p_iClientId);		
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetMaxSequenceNumber.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( iMaxSequenceNumber );
		}


		public static string GetPolicyNumber( int p_iPolicyId , int p_iMaxTermNumber , int p_iSequenceNumber, ref int p_iTermNumber, string p_sConnectionString,int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			string sPolicyNumber = string.Empty ;

			try
			{
				sSQL = "SELECT POLICY_NUMBER, TERM_NUMBER FROM POLICY_X_TERM_ENH where POLICY_ID = " + p_iPolicyId + " and TERM_NUMBER = " + p_iMaxTermNumber + " and  SEQUENCE_ALPHA = " + p_iSequenceNumber ;

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					sPolicyNumber = objReader.GetString( "POLICY_NUMBER" ) ;
                    p_iTermNumber = Common.Conversion.ConvertObjToInt(objReader.GetValue("TERM_NUMBER"), p_iClientId);
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetPolicyName.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( sPolicyNumber );
		}


        public static int GetBillToPayeeEid(int p_iPolicyId, int p_iTermNumber, string p_sConnectionString, int p_iClientId)
		{
			DbReader objReader = null ;
			string sSQL = string.Empty ;
			int iPayeeEid = 0 ;

			try
			{
				sSQL = "SELECT BILL_TO_OVERRIDE, NEW_BILL_TO_EID, BILL_TO_EID FROM POLICY_X_BILL_ENH WHERE POLICY_ID =" + p_iPolicyId + " and TERM_NUMBER = " + p_iTermNumber ;

				objReader = DbFactory.GetDbReader( p_sConnectionString , sSQL );
				if( objReader.Read() )
				{
					// TODO
					if( objReader.GetBoolean( "BILL_TO_OVERRIDE" ) )
                        iPayeeEid = Common.Conversion.ConvertObjToInt(objReader.GetValue("NEW_BILL_TO_EID"), p_iClientId);
					else
                        iPayeeEid = Common.Conversion.ConvertObjToInt(objReader.GetValue("BILL_TO_EID"), p_iClientId);
				}
				objReader.Close();	
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetBillToPayeeEid.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				if( objReader != null )
				{
					objReader.Close();
					objReader.Dispose();
					objReader = null ;
				}
			}
			return( iPayeeEid );
		}


		
		#endregion 
        #region Entity Name
        /// <summary>
        /// Gets Entity Name
        /// </summary>
        /// <param name="p_lEntityId">Entity Id</param>
        /// <returns>Entity first name & last name</returns>
        public static string GetEntityName(int p_lEntityId, string p_sConnectionString, int p_iClientId)
        {
            string sSQL = "";
            string sLN = "";
            string sFN = "";
            string sRetVal = "";
            DbReader objReader = null;

            try
            {
                sSQL = "SELECT FIRST_NAME,LAST_NAME FROM ENTITY WHERE ENTITY_ID = " + p_lEntityId;
                objReader = DbFactory.GetDbReader(p_sConnectionString, sSQL);

                if (objReader.Read())
                {
                    sFN = objReader.GetString("FIRST_NAME");
                    sLN = objReader.GetString("LAST_NAME");
                    if (sFN == "" && sLN == "")
                    {
                        sRetVal = "";
                    }
                    else if (sFN == "")
                    {
                        sRetVal = sLN;
                    }
                    else
                    {
                        sRetVal = sLN + ", " + sFN;
                    }
                }
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Function.GetEntityName.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
            }
            return (sRetVal);
        }
        #endregion
		#region Common XML functions
		/// <summary>
		/// Initialize the document
		/// </summary>
		/// <param name="p_objDocument">xml Document</param>
		/// <param name="p_objRootNode">Root Node</param>
		/// <param name="p_sRootNodeName">Root Node Name</param>
        public static void StartDocument(ref XmlDocument p_objDocument, ref XmlElement p_objRootNode, string p_sRootNodeName, int p_iClientId)
		{
			try
			{
				p_objDocument = new XmlDocument();
				p_objRootNode = p_objDocument.CreateElement( p_sRootNodeName );
				p_objDocument.AppendChild( p_objRootNode );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.StartDocument.Error", p_iClientId), p_objEx);				
			}
		}

		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_sNodeName">Node Name</param>		
        public static void CreateElement(XmlElement p_objParentNode, string p_sNodeName, int p_iClientId)
		{
			XmlElement objChildNode = null ;
			try
			{
				objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( objChildNode );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.CreateElement.Error", p_iClientId), p_objEx);
			}
			finally
			{
				objChildNode = null ;
			}

		}	
	
		/// <summary>
		/// Create Element
		/// </summary>
		/// <param name="p_objParentNode">Parent Node</param>
		/// <param name="p_objChildNode">Child Node Name</param>
		/// <param name="p_objChildNode">Child Node</param>
        public static void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode, int p_iClientId)
		{
			try
			{
				p_objChildNode = p_objParentNode.OwnerDocument.CreateElement( p_sNodeName ) ;
				p_objParentNode.AppendChild( p_objChildNode );
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.CreateElement.Error", p_iClientId), p_objEx);
			}

		}
		
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="sText">Text</param>		
        public static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText, int p_iClientId)
		{
            XmlElement objChildNode = null;
            try
            {

                Function.CreateAndSetElement(p_objParentNode, p_sNodeName, sText, ref objChildNode, p_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateAndSetElement.Error", p_iClientId), p_objEx);
            }
            finally
            {
                objChildNode = null;
            }
		}
		/// <summary>
		/// Create Element and set inner text.
		/// </summary>
		/// <param name="p_objParentNode">Parent node</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <param name="sText">Text</param>
		/// <param name="p_objChildNode">Child Node</param>
        public static void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string sText, ref XmlElement p_objChildNode, int p_iClientId)
		{
			try
			{
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode, p_iClientId);
				p_objChildNode.InnerText = sText ;			
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.CreateAndSetElement.Error", p_iClientId), p_objEx);
			}
		}

		/// Name		: GetValue
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get text value of the node.
		/// </summary>
		/// <param name="p_objDocument">Input XML document.</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>Text value of the node</returns>
        public static string GetValue(XmlDocument p_objDocument, string p_sNodeName, int p_iClientId)
		{
			XmlElement objNode = null ;
			string sValue = "" ;

			try
			{
				objNode = ( XmlElement ) p_objDocument.SelectSingleNode( "//" + p_sNodeName );

				if( objNode != null )
					sValue = objNode.InnerText ;
			}
			catch(Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetValue.Error", p_iClientId), p_objEx);
			}
			finally
			{
				objNode = null ;
			}
			return( sValue );

		}
        public static string GetCodeValue(XmlDocument p_objDocument, string p_sNodeName, int p_iClientId)
        {
            XmlElement objNode = null;
            string sValue = "";

            try
            {
                objNode = (XmlElement)p_objDocument.SelectSingleNode("//" + p_sNodeName);

                if (objNode != null)
                    sValue = objNode.Attributes[PolBillingConstants.CODE_ID_ATTRIBUTE_NAME].Value;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.GetCodeValue.Error", p_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
            return (sValue);

        }
		/// Name		: GetList
		/// Author		: Vaibhav Kaushik
		/// Date Created: 01/05/2005		
		///************************************************************
		/// Amendment History
		///************************************************************
		/// Date Amended   *   Amendment   *    Author
		///************************************************************
		/// <summary>
		/// Get list form XML document.
		/// </summary>
		/// <param name="p_objDocument">Input XML document</param>
		/// <param name="p_sNodeName">Node Name</param>
		/// <returns>Array List</returns>
        public static ArrayList GetList(XmlDocument p_objDocument, string p_sNodeName, int p_iClientId)
		{
			XmlNodeList objNodeList = null ;
			ArrayList arrlstReturn = null ;
			string sValue = "" ;

			try
			{
				arrlstReturn = new ArrayList();

				objNodeList = p_objDocument.SelectNodes( "//" + p_sNodeName );
				foreach( XmlNode objTempNode in objNodeList )
				{
					sValue = ((XmlElement)objTempNode).InnerText ;
					arrlstReturn.Add( sValue );
				}
				return( arrlstReturn );
			}
			catch(Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetList.Error", p_iClientId), p_objEx);
			}
			finally
			{
				objNodeList = null ;
				arrlstReturn = null ;
			}			
		}		
		#endregion 

        # region PaymentMethod

        public static string GetCodes(string sTableName, string p_sConnectionString, int p_iClientId)
        {
            string sSQL = "";
            string sRetVal = "";
            DbReader objReader = null;
            //Changed by Shruti, to make it compatible with oracle.
            LocalCache objLocalCache = null;
            int iCodeId = 0;
            try
            {
                objLocalCache = new LocalCache(p_sConnectionString, p_iClientId);
                sSQL = "SELECT CODES.CODE_ID FROM CODES,CODES_TEXT,GLOSSARY" + //, CODES.SHORT_CODE, CODES.SHORT_CODE + ' ' + CODES_TEXT.CODE_DESC AS CODE_DESC FROM CODES,CODES_TEXT,GLOSSARY " +
                    " WHERE GLOSSARY.SYSTEM_TABLE_NAME='" + sTableName + "'" +
                    " AND GLOSSARY.TABLE_ID=CODES.TABLE_ID" +
                    " AND CODES.CODE_ID=CODES_TEXT.CODE_ID" +
                    " AND CODES_TEXT.LANGUAGE_CODE=1033" +
                    " AND (CODES.DELETED_FLAG = 0 OR CODES.DELETED_FLAG IS NULL)" +
                    " ORDER BY CODES.SHORT_CODE ASC";

                objReader = DbFactory.GetDbReader(p_sConnectionString, sSQL);

                while(objReader.Read())
                {
                    iCodeId = Conversion.ConvertObjToInt(objReader.GetValue("CODE_ID"), p_iClientId);
                    if (sTableName == "ADJUSTMENT_TYPES")
                    {
                        if (sRetVal.Trim() == string.Empty)
                        {
                            //rupal:mits 27977
                            //sRetVal = iCodeId.ToString() + "," + objLocalCache.GetShortCode(iCodeId) + "~" + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                            sRetVal = iCodeId.ToString() + "," + objLocalCache.GetShortCode(iCodeId) +CodeIdSeperator[0] + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                        }
                        else
                        {
                            //rupal:mits 27977
                            //sRetVal = sRetVal + "^" + iCodeId.ToString() + "," + objLocalCache.GetShortCode(iCodeId) + "~" + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                            sRetVal = sRetVal + NewCodeSeperator[0] + iCodeId.ToString() + "," + objLocalCache.GetShortCode(iCodeId) + CodeIdSeperator[0] + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                        }
                    }
                    else
                    {
                        if (sRetVal.Trim() == string.Empty)
                        {
                            //sRetVal = iCodeId.ToString() + "~" + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                            //rupal:mits 27977
                            sRetVal = iCodeId.ToString() + CodeIdSeperator[0] + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                        }
                        else
                        {
                            //sRetVal = sRetVal + "^" + iCodeId.ToString() + "~" + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                            sRetVal = sRetVal + NewCodeSeperator[0] + iCodeId.ToString() + CodeIdSeperator[0] + objLocalCache.GetShortCode(iCodeId) + " " + objLocalCache.GetCodeDesc(iCodeId);
                        }
                    }
                }

                objReader.Close();

            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("Function.GetEntityName.Error", p_iClientId), p_objException);
            }
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
            }
            return (sRetVal);
        }
        # endregion


        public static void GetBillItemXml(XmlDocument p_objInputDoc, ref string p_sPolicyNumber, ref int p_iPolicyId, ref string p_sDateReconciled, ref int p_iEntryBatchNumber, ref int p_iEntryBatchType, ref int p_iFinalBatchNumber, ref int p_iFinalBatchType, ref int p_iInvoiceNumber, ref int p_iNoticeNumber, ref int p_iStatus, ref int p_iTermNumber, ref string p_sComment, string p_sConnectionString, int p_iClientId)
		{		
			LocalCache objLocalCache = null;

			try
			{
                objLocalCache = new LocalCache(p_sConnectionString, p_iClientId);

                p_sPolicyNumber = Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_POLICY_NUMBER, p_iClientId );
                p_iPolicyId = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_POLICY_ID, p_iClientId));
                p_sDateReconciled = Conversion.GetDBDateFormat(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_DATE_RECONCILED, p_iClientId), "d");
                p_iEntryBatchNumber = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_ENTRY_BATCH_NUMBER, p_iClientId));
                p_iEntryBatchType = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_ENTRY_BATCH_TYPE, p_iClientId));
                p_iFinalBatchNumber = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_FINAL_BATCH_NUMBER, p_iClientId));
                p_iFinalBatchType = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_FINAL_BATCH_TYPE, p_iClientId));
                p_iInvoiceNumber = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_INVOICE_NUMBER, p_iClientId));
                p_iNoticeNumber = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_NOTICE_NUMBER, p_iClientId));
				p_iStatus = objLocalCache.GetCodeId( "OK" , "BILLING_ITEM_STAT" );
                p_iTermNumber = Conversion.ConvertStrToInteger(Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_TERM_NUMBER, p_iClientId));
                p_sComment = Function.GetValue(p_objInputDoc, PolBillingConstants.BILL_ITEM_COMMENT, p_iClientId);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.GetBillItemXml.Error", p_iClientId), p_objEx);				
			} 			
			finally
			{
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
			}
		}


        public static void SetBillItemXml(BillXBillItem p_objBillXBillItem, XmlElement p_objParentNode, string p_sConnectionString, int p_iClientId)
        {
            LocalCache objLocalCache = null;

            try
            {
                objLocalCache = new LocalCache(p_sConnectionString,p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_ROW_ID, p_objBillXBillItem.BillingItemRowid.ToString(), p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_POLICY_NAME, Function.GetPolicyName(p_objBillXBillItem.PolicyId, p_sConnectionString, p_iClientId), p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_POLICY_NUMBER, p_objBillXBillItem.PolicyNumber, p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_DATE_ENTERED, Conversion.GetDBDateFormat(p_objBillXBillItem.DateEntered, "d"), p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_INVOICE_NUMBER, p_objBillXBillItem.InvoiceId.ToString(), p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_NOTICE_NUMBER, p_objBillXBillItem.NoticeId.ToString(), p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_DATE_RECONCILED, Conversion.GetDBDateFormat(p_objBillXBillItem.DateReconciled, "d"), p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_TERM_NUMBER, p_objBillXBillItem.TermNumber.ToString(), p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_ENTRY_BATCH_NUMBER, p_objBillXBillItem.EntryBatchNum.ToString(), p_iClientId);
                Function.CreateNodeWithCodeValues(p_objParentNode, PolBillingConstants.BILL_ITEM_ENTRY_BATCH_TYPE, p_objBillXBillItem.EntryBatchType, p_sConnectionString, p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_FINAL_BATCH_NUMBER, p_objBillXBillItem.FinalBatchNum.ToString(), p_iClientId);
                Function.CreateNodeWithCodeValues(p_objParentNode, PolBillingConstants.BILL_ITEM_FINAL_BATCH_TYPE, p_objBillXBillItem.FinalBatchType, p_sConnectionString, p_iClientId);
                Function.CreateNodeWithCodeValues(p_objParentNode, PolBillingConstants.BILL_ITEM_STATUS, p_objBillXBillItem.Status, p_sConnectionString, p_iClientId);
                Function.CreateAndSetElement(p_objParentNode, PolBillingConstants.BILL_ITEM_COMMENT, p_objBillXBillItem.Comments, p_iClientId);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.SetBillItemXml.Error", p_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
            }
        }


        public static void CreateNodeWithCodeValues(XmlElement p_objParentNode, string p_sNodeName, int p_iCodeId, string p_sConnectionString, int p_iClientId)
		{
			XmlElement objTempNode = null ;
			LocalCache objLocalCache = null ;
			try
			{
				objLocalCache = new LocalCache( p_sConnectionString,p_iClientId );
                Function.CreateElement(p_objParentNode, p_sNodeName, ref objTempNode, p_iClientId);
				objTempNode.SetAttribute( PolBillingConstants.CODE_ID_ATTRIBUTE_NAME , p_iCodeId.ToString() );
				objTempNode.SetAttribute( PolBillingConstants.SHORT_CODE_ATTRIBUTE_NAME , objLocalCache.GetShortCode( p_iCodeId ) );
				objTempNode.SetAttribute( PolBillingConstants.CODE_DESC_ATTRIBUTE_NAME , objLocalCache.GetCodeDesc( p_iCodeId ) );
                objTempNode.InnerText=objLocalCache.GetShortCode( p_iCodeId )+" "+ objLocalCache.GetCodeDesc( p_iCodeId );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("Function.CreateNodeWithCodeValues.Error", p_iClientId), p_objEx);				
			}
			finally
			{
				objTempNode = null;
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
			}

		}
        public static void CreateNodeWithCodeValues(XmlElement p_objParentNode, string p_sNodeName, int p_iCodeId, string p_sConnectionString, ref XmlElement p_objChildNode, int p_iClientId)
        {
            LocalCache objLocalCache = null;
            try
            {
                objLocalCache = new LocalCache(p_sConnectionString,p_iClientId);
                Function.CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode, p_iClientId);
                p_objChildNode.SetAttribute(PolBillingConstants.CODE_ID_ATTRIBUTE_NAME, p_iCodeId.ToString());
                p_objChildNode.SetAttribute(PolBillingConstants.SHORT_CODE_ATTRIBUTE_NAME, objLocalCache.GetShortCode(p_iCodeId));
                p_objChildNode.SetAttribute(PolBillingConstants.CODE_DESC_ATTRIBUTE_NAME, objLocalCache.GetCodeDesc(p_iCodeId));
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Function.CreateNodeWithCodeValues.Error", p_iClientId), p_objEx);
            }
            finally
            {
                if (objLocalCache != null)
                {
                    objLocalCache.Dispose();
                }
            }

        }
	}
}
