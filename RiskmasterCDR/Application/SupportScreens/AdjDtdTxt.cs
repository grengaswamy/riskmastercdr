using System;
using System.Collections;
using System.Xml;

using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.SupportScreens
{
    ///************************************************************** 
    ///* $File				: AdjDtdTxt.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 23-Dec-2005
    ///* $Author			: Mihika Agrawal
    ///***************************************************************	
    /// <summary>	
    ///	This class lists the Adjuster Dated Text Information for an Adjuster
    /// </summary>
    public class AdjDtdTxt
    {
        #region Constructor
        // Default Constructor
        public AdjDtdTxt()
        {

        }

        /// <summary>
        ///	Constructor, initializes the variables to the default value
        /// </summary>
        /// <param name="p_sUserName">UserName</param>
        /// <param name="p_sPassword">Password</param>
        /// <param name="p_sDsnName">DsnName</param>
        public AdjDtdTxt(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
        }
        #endregion

        #region Variable Declaration
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        // private string m_sConnectionString = "" ;
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        // private DataModelFactory m_objDataModelFactory = null ;

        //Ash - cloud, config settings moved to DB
        //Hashtable m_MessageSettings = RMConfigurationSettings.GetRMMessages();
        private Hashtable m_MessageSettings = null;
        private int m_iClientId = 0;
        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <returns></returns>
        public XmlDocument GetAdjDatedText(XmlDocument p_objXmlIn)
        {
            #region local variables
            XmlDocument objXmlOut = null;
            XmlElement objRootElm = null;
            XmlElement objRowElm = null;
            XmlElement objElm = null;
            //Anurag 06/01/2006 - TR#1331 Added for format Dated Text
            XmlElement objInnerRowElm = null;

            ClaimAdjuster objClaimAdj = null;
            //nadim for 13515
            string sPrimaryClaimant = string.Empty;
            int iClaimID = 0;
            string sCodedesc = string.Empty;
            //nadim for 13515

            string sSQL = string.Empty;
            DbReader objDbReader = null;

            int iAdjRowId = 0;
            string sFilterMethod = string.Empty;
            string sBeginDate = string.Empty;
            string sEndDate = string.Empty;
            string sFilterByType = string.Empty;
            string sCodeIds = "";
            //Anurag 06/01/2006 - TR#1331 Added for format Dated Text
            string strDatedText = string.Empty;
            string[] strArrDatedText = null;
            DataModelFactory objDataModelFactory = null;
            //string sConnectionString = null;
            #endregion

            try
            {
                iAdjRowId = Conversion.ConvertStrToInteger(p_objXmlIn.SelectSingleNode("//adjrowid").InnerText);

                if (p_objXmlIn.SelectSingleNode("//FilterMethod") != null)
                    sFilterMethod = p_objXmlIn.SelectSingleNode("//FilterMethod").InnerText;
                if (sFilterMethod == "")
                    sFilterMethod = "1";

                if (sFilterMethod == "2")
                {
                    sBeginDate = Conversion.GetDate(p_objXmlIn.SelectSingleNode("//BeginDate").InnerText);
                    sEndDate = Conversion.GetDate(p_objXmlIn.SelectSingleNode("//EndDate").InnerText);
                }

                sFilterByType = p_objXmlIn.SelectSingleNode("//FilterType").InnerText;
                if (sFilterByType == "1")
                {
                    sCodeIds = p_objXmlIn.SelectSingleNode("//FilterTypeList").InnerText.Trim();
                    sCodeIds = sCodeIds.Replace(" ", ",");
                }
                //Code Commented by nadim for 13515-start

                //sSQL = "SELECT * FROM ADJUST_DATED_TEXT WHERE ADJ_ROW_ID = " + iAdjRowId;
                //if (sFilterMethod == "2")
                //    sSQL = sSQL + " AND DATE_ENTERED BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'";
                //if (sFilterByType == "1")
                //    sSQL = sSQL + " AND TEXT_TYPE_CODE IN (" + sCodeIds + ")";
                //Anurag 06/01/2006 - TR#1331 Added for getting data in Order
                //Nadim 13515-end

                //Query written by nadim for 13515 to sort according to short code of codes_text table-start
                sSQL = "SELECT ADJUST_DATED_TEXT.*, CODES_TEXT.SHORT_CODE FROM ADJUST_DATED_TEXT,CODES_TEXT WHERE ADJ_ROW_ID =" + iAdjRowId + " AND CODES_TEXT.CODE_ID IN (SELECT TEXT_TYPE_CODE  FROM ADJUST_DATED_TEXT WHERE ADJ_ROW_ID =" + iAdjRowId + ") AND TEXT_TYPE_CODE=CODES_TEXT.CODE_ID";
                if (sFilterMethod == "2")
                    sSQL = sSQL + " AND DATE_ENTERED BETWEEN '" + sBeginDate + "' AND '" + sEndDate + "'";
                if (sFilterByType == "1")
                {
                    sSQL = sSQL + " AND TEXT_TYPE_CODE IN (" + sCodeIds + ") AND CODES_TEXT.CODE_ID IN (" + sCodeIds + ")";
                    sSQL = sSQL + "  ORDER BY CODES_TEXT.SHORT_CODE,DATE_ENTERED DESC,TIME_ENTERED DESC";
                }
                else
                    sSQL = sSQL + "  ORDER BY DATE_ENTERED DESC,TIME_ENTERED DESC";
                //Code commented by nadim for 13515-start
                //sSQL += "  ORDER BY DATE_ENTERED DESC,TIME_ENTERED DESC";
                //nadim for 13515-end           

                //nadim for 13515-end
                this.Initialize();

                objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);

                objDbReader = DbFactory.GetDbReader(objDataModelFactory.Context.DbConn.ConnectionString, sSQL);

                objClaimAdj = (DataModel.ClaimAdjuster)objDataModelFactory.GetDataModelObject("ClaimAdjuster", false);
                objClaimAdj.MoveTo(iAdjRowId);

                objXmlOut = new XmlDocument();
                objRootElm = objXmlOut.CreateElement("AdjusterDatedText");
                objXmlOut.AppendChild(objRootElm);
                //Code commented by nadim nadim for 13515
                //objElm = objXmlOut.CreateElement("ClaimNumber");
                //objElm.InnerText = p_objXmlIn.SelectSingleNode("//ClaimNumber").InnerText;
                //objRootElm.AppendChild(objElm);
                //Code commented by nadim nadim for 13515
                //Code Added by nadim nadim for 13515 to get primary claimant only -start
                Claim objclaim = (DataModel.Claim)objDataModelFactory.GetDataModelObject("Claim", false);
                iClaimID = objClaimAdj.ClaimId;
                objclaim.MoveTo(iClaimID);

                objElm = objXmlOut.CreateElement("ClaimNumber");
                objElm.InnerText = objclaim.ClaimNumber;
                objRootElm.AppendChild(objElm);

                objElm = objXmlOut.CreateElement("PrimaryClaimantName");
                //MGaba2:MITS 24273:For Property claim Claimant is not getting printed on the print adjuster dated text
                if (objclaim.LineOfBusCode == 241 || objclaim.LineOfBusCode == 242 || objclaim.LineOfBusCode == 845)
                {
                    sPrimaryClaimant = GetPrimaryClaimantOnly(iClaimID);
                }
                else if (objclaim.LineOfBusCode == 243 || objclaim.LineOfBusCode == 844)
                {
                    if (objclaim.PrimaryClaimant != null)
                    {
                        sPrimaryClaimant = objclaim.PrimaryClaimant.ClaimantEntity.GetLastFirstName();
                    }
                    else
                    {
                        sPrimaryClaimant = objclaim.PrimaryPiEmployee.PiEntity.LastName + ',' + objclaim.PrimaryPiEmployee.PiEntity.FirstName;
                    }
                }
                if (sPrimaryClaimant.EndsWith(","))
                {
                    sPrimaryClaimant = sPrimaryClaimant.Remove(sPrimaryClaimant.LastIndexOf(','));
                }

                objElm.InnerText = sPrimaryClaimant;
                objRootElm.AppendChild(objElm);
                if (objclaim != null)
                {
                    objclaim.Dispose();
                    objclaim = null;
                }
                //nadim for 13515-end

                while (objDbReader.Read())
                {
                    objRowElm = objXmlOut.CreateElement("Row");

                    // Adjuster Name
                    objElm = objXmlOut.CreateElement("AdjusterName");
                    objElm.InnerText = objClaimAdj.AdjusterEntity.GetLastFirstName();
                    objRowElm.AppendChild(objElm);

                    // Text Type
                    objElm = objXmlOut.CreateElement("TextType");
                    objElm.InnerText = objDataModelFactory.Context.LocalCache.GetCodeDesc(objDbReader.GetInt32("TEXT_TYPE_CODE"));
                    objRowElm.AppendChild(objElm);

                    // Added By User
                    objElm = objXmlOut.CreateElement("AddedByUser");
                    objElm.InnerText = objDbReader.GetString("ENTERED_BY_USER");
                    objRowElm.AppendChild(objElm);

                    // Date Time Entered
                    objElm = objXmlOut.CreateElement("DateTimeEntered");
                    objElm.InnerText = Conversion.GetDBDateFormat(objDbReader.GetString("DATE_ENTERED"), "d") +
                        " " + Conversion.GetDBTimeFormat(objDbReader.GetString("TIME_ENTERED"), "t");
                    objRowElm.AppendChild(objElm);

                    // Dated Text
                    //Anurag 06/01/2006 - TR#1331 Modified to format Dated Text
                    objElm = objXmlOut.CreateElement("DatedText");
                    //Start by Shivendu for MITS 15409
                    if (objDbReader["DATED_TEXT_HTMLCOMMENTS"] != null)
                    {
                        strDatedText = objDbReader["DATED_TEXT_HTMLCOMMENTS"].ToString();
                    }
                    if (string.IsNullOrEmpty(strDatedText))
                    {
                        strDatedText = objDbReader["DATED_TEXT"].ToString();
                    }
                    //End by Shivendu for MITS 15409

                    strArrDatedText = strDatedText.Split('\n');
                    for (int i = 0; i < strArrDatedText.Length; i++)
                    {
                        if (i == 0)
                        {
                            objInnerRowElm = objXmlOut.CreateElement("FirstInnerRow");
                            objInnerRowElm.InnerText = strArrDatedText[i].ToString();
                            objElm.AppendChild(objInnerRowElm);
                        }
                        else
                        {
                            objInnerRowElm = objXmlOut.CreateElement("InnerRow");
                            objInnerRowElm.InnerText = strArrDatedText[i].ToString();
                            objElm.AppendChild(objInnerRowElm);
                        }
                    }
                    objRowElm.AppendChild(objElm);
                    objRootElm.AppendChild(objRowElm);
                }

                // Ash - cloud, load config settings from db
                m_MessageSettings = RMConfigurationSettings.GetRMMessages(objDataModelFactory.Context.DbConn.ConnectionString, m_iClientId);

                // Proprietary Information
                objElm = objXmlOut.CreateElement("ProprietaryInfo");
                objElm.InnerText = m_MessageSettings["DEF_RMPROPRIETARY"].ToString();
                objRootElm.AppendChild(objElm);

                // RM Name
                objElm = objXmlOut.CreateElement("RMName");
                objElm.InnerText = m_MessageSettings["DEF_NAME"].ToString();
                objRootElm.AppendChild(objElm);

                // Copyright Info
                objElm = objXmlOut.CreateElement("Copyright");
                objElm.InnerText = m_MessageSettings["DEF_RMCOPYRIGHT"].ToString();
                objRootElm.AppendChild(objElm);

                // Rights Reserved
                objElm = objXmlOut.CreateElement("RightsReserved");
                objElm.InnerText = m_MessageSettings["DEF_RMRIGHTSRESERVED"].ToString();
                objRootElm.AppendChild(objElm);

                //Anurag 06/01/2006 - TR#1331 Added for getting date of reporting
                objElm = objXmlOut.CreateElement("ReportDate");
                objElm.InnerText = DateTime.Now.ToShortDateString();
                objRootElm.AppendChild(objElm);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("AdjDtdTxt.GetAdjDatedText.Error",m_iClientId), p_objEx);
            }
            finally
            {
                if (objDataModelFactory != null)
                {
                    objDataModelFactory.UnInitialize();
                    objDataModelFactory = null;
                }

                if (objClaimAdj != null)
                {
                    objClaimAdj.Dispose();
                    objClaimAdj = null;
                }
                if (objDbReader != null)
                {
                    objDbReader.Dispose();
                    objDbReader = null;
                }
            }
            return objXmlOut;
        }
        #endregion

        #region Private Methods

        //New method added by Nadim for 13515 to get PrimaryClaimant only-start
        /// Name		: GetPrimaryClaimantOnly
        /// Author		: Nadim Zafar
        /// Date Created	: 8 Jan 2008
        /// ************************************************************
        private string GetPrimaryClaimantOnly(int iClaimID)
        {
            string sPclaimantOnly = string.Empty;
            string sSqlClaimant = string.Empty;
            DbReader objReaderClaimant = null;
            DataModelFactory objDataModelFactoryClaimant = null;


            try
            {
                if (iClaimID == 0)
                    return (sPclaimantOnly);
                sSqlClaimant = "SELECT ENTITY.LAST_NAME, ENTITY.FIRST_NAME, PRIMARY_CLMNT_FLAG FROM CLAIMANT, ENTITY WHERE CLAIMANT.CLAIM_ID = " + iClaimID + " AND CLAIMANT.PRIMARY_CLMNT_FLAG =-1 AND NOT CLAIMANT.PRIMARY_CLMNT_FLAG IS  NULL AND CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID";
                objDataModelFactoryClaimant = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
                objReaderClaimant = DbFactory.GetDbReader(objDataModelFactoryClaimant.Context.DbConn.ConnectionString, sSqlClaimant);

                if (objReaderClaimant != null)
                {
                    while (objReaderClaimant.Read())
                    {
                        sPclaimantOnly = objReaderClaimant.GetValue("LAST_NAME") + "," + objReaderClaimant.GetValue("FIRST_NAME");
                    }
                    objReaderClaimant.Close();
                }
            }

            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            finally
            {
                if (objDataModelFactoryClaimant != null)
                {
                    objDataModelFactoryClaimant.UnInitialize();
                    objDataModelFactoryClaimant = null;
                }

                if (objReaderClaimant != null)
                {
                    objReaderClaimant.Dispose();
                    objReaderClaimant = null;
                }

            }

            return sPclaimantOnly;


        }

        //Method added by Nadim for 13515 to get primary claimant only -end

        /// <summary>
        /// Initialize objects
        /// </summary>
        private void Initialize()
        {
            //			try
            //			{
            //				m_objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword );	
            //				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;				
            //			}
            //			catch(DataModelException p_objEx)
            //			{
            //				throw p_objEx ;
            //			}
            //			catch( RMAppException p_objEx )
            //			{
            //				throw p_objEx ;
            //			}
            //			catch( Exception p_objEx )
            //			{
            //				throw new RMAppException(Globalization.GetString("AdjDtdTxt.Initialize.Error") , p_objEx );				
            //			}			
        }

        #endregion
    }
}
