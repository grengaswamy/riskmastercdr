﻿using System;
using System.Xml;
using System.Text;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Collections.Generic;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.SupportScreens
{
    /// <summary>
    /// Class to get and set Policy System download search configuration settings. 
    /// </summary>
    /// Pradyumna 3/29/2014 - MITS 35885
    public class PolicySearchConfig
    {
         #region Constructor
        // Default Constructor
        public PolicySearchConfig()
        {
        }

        public PolicySearchConfig(string connectionString, int dsnid, int p_iClientId)//rkaur27
        {
            DbConnection objConn;
            m_ConnectionString = connectionString;
            m_DsnId = dsnid;
            m_iClientId = p_iClientId;//rkaur27
            //Added By Neha Mits 23633
            objConn = DbFactory.GetDbConnection(m_ConnectionString);
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            objConn.Close();
        }
        #endregion

        #region Variable Declaration
        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private int m_DsnId = 0;
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sDBType = string.Empty;
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_ConnectionString = string.Empty;
        /// <summary>
        /// Object to store Policy Search Screen Fields configuration
        /// </summary>
        private PolicySrchConfig m_PolicySearchConfig;

        private int m_iClientId = 0;//rkaur27

        internal struct PolicySrchConfig
        {
            private bool m_SysName;
            private bool m_PolSym;
            private bool m_Module;
            private bool m_Lob;
            private bool m_PolNum;
            private bool m_PolState;
            private bool m_LocCmpny;
            private bool m_LossDate;
            private bool m_AgntNum;
            private bool m_InsrdZip;
            private bool m_CustNum;
            private bool m_MstrCmpny;
            private bool m_InsrdCity;
            private bool m_InsrdSSN;
            private bool m_GrpNum;
            private bool m_InsrdLastName;
            private bool m_InsrdFirstName;
            private bool m_InsrdName;

            internal bool SysName { get { return m_SysName; } set { m_SysName = value; } }
            internal bool PolSym { get { return m_PolSym; } set { m_PolSym = value; } }
            internal bool Module { get { return m_Module; } set { m_Module = value; } }
            internal bool Lob { get { return m_Lob; } set { m_Lob = value; } }
            internal bool PolNum { get { return m_PolNum; } set { m_PolNum = value; } }
            internal bool PolState { get { return m_PolState; } set { m_PolState = value; } }
            internal bool LocCmpny { get { return m_LocCmpny; } set { m_LocCmpny = value; } }
            internal bool LossDate { get { return m_LossDate; } set { m_LossDate = value; } }
            internal bool AgntNum { get { return m_AgntNum; } set { m_AgntNum = value; } }
            internal bool InsrdZip { get { return m_InsrdZip; } set { m_InsrdZip = value; } }
            internal bool CustNum { get { return m_CustNum; } set { m_CustNum = value; } }
            internal bool MstrCmpny { get { return m_MstrCmpny; } set { m_MstrCmpny = value; } }
            internal bool InsrdCity { get { return m_InsrdCity; } set { m_InsrdCity = value; } }
            internal bool InsrdSSN { get { return m_InsrdSSN; } set { m_InsrdSSN = value; } }
            internal bool GrpNum { get { return m_GrpNum; } set { m_GrpNum = value; } }
            internal bool InsrdLastName { get { return m_InsrdLastName; } set { m_InsrdLastName = value; } }
            internal bool InsrdFirstName { get { return m_InsrdFirstName; } set { m_InsrdFirstName = value; } }
            internal bool InsrdName { get { return m_InsrdName; } set { m_InsrdName = value; } }

            private string m_SysNameText;
            private string m_PolSymText;
            private string m_ModuleText;
            private string m_LobText;
            private string m_PolNumText;
            private string m_PolStateText;
            private string m_LocCmpnyText;
            private string m_LossDateText;
            private string m_AgntNumText;
            private string m_InsrdZipText;
            private string m_CustNumText;
            private string m_MstrCmpnyText;
            private string m_InsrdCityText;
            private string m_InsrdSSNText;
            private string m_GrpNumText;
            private string m_InsrdLastNameText;
            private string m_InsrdFirstNameText;
            private string m_InsrdNameNameText;

            internal string SysNameText { get { return m_SysNameText; } set { m_SysNameText = value; } }
            internal string PolSymText { get { return m_PolSymText; } set { m_PolSymText = value; } }
            internal string ModuleText { get { return m_ModuleText; } set { m_ModuleText = value; } }
            internal string LobText { get { return m_LobText; } set { m_LobText = value; } }
            internal string PolNumText { get { return m_PolNumText; } set { m_PolNumText = value; } }
            internal string PolStateText { get { return m_PolStateText; } set { m_PolStateText = value; } }
            internal string LocCmpnyText { get { return m_LocCmpnyText; } set { m_LocCmpnyText = value; } }
            internal string LossDateText { get { return m_LossDateText; } set { m_LossDateText = value; } }
            internal string AgntNumText { get { return m_AgntNumText; } set { m_AgntNumText = value; } }
            internal string InsrdZipText { get { return m_InsrdZipText; } set { m_InsrdZipText = value; } }
            internal string CustNumText { get { return m_CustNumText; } set { m_CustNumText = value; } }
            internal string MstrCmpnyText { get { return m_MstrCmpnyText; } set { m_MstrCmpnyText = value; } }
            internal string InsrdCityText { get { return m_InsrdCityText; } set { m_InsrdCityText = value; } }
            internal string InsrdSSNText { get { return m_InsrdSSNText; } set { m_InsrdSSNText = value; } }
            internal string GrpNumText { get { return m_GrpNumText; } set { m_GrpNumText = value; } }
            internal string InsrdLastNameText { get { return m_InsrdLastNameText; } set { m_InsrdLastNameText = value; } }
            internal string InsrdFirstNameText { get { return m_InsrdFirstNameText; } set { m_InsrdFirstNameText = value; } }
            internal string InsrdNameText { get { return m_InsrdNameNameText; } set { m_InsrdNameNameText = value; } }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets Policy search config settings
        /// </summary>
        /// <param name="p_iDsnID"></param>
        /// <returns></returns>
        public XmlDocument GetPolicySearchConfig(int p_iDsnID)
        {
            DbReader objReader = null;
            XmlDocument objRetDoc = null;
            XmlDocument objPrevDoc = null;
            XmlNode objNode = null;
            XmlElement objRootNode = null;
            string sConfigXml = string.Empty;
            string sSQL = string.Empty;
            bool bExists = false;

            try
            {
                objRetDoc = new XmlDocument();

                sSQL = "SELECT CONFIG_XML FROM POLICY_SRCH_CONFIG WHERE DSN_ID = " + p_iDsnID;

                using (objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL))
                {
                    if (objReader.Read())
                        sConfigXml = Convert.ToString(objReader.GetValue("CONFIG_XML"));
                }

                sConfigXml = sConfigXml.Trim();
                objPrevDoc = new XmlDocument();

                if (sConfigXml != string.Empty)
                {
                    objPrevDoc.LoadXml(sConfigXml);
                    bExists = true;
                }

                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/SysName/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.SysName = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/PolSym/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.PolSym = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/Module/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.Module = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/Lob/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.Lob = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/PolNum/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.PolNum = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/PolState/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.PolState = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/LocCmpny/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.LocCmpny = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/LossDate/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.LossDate = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/AgntNum/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.AgntNum = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdZip/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdZip = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/CustNum/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.CustNum = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/MstrCmpny/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.MstrCmpny = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdCity/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdCity = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdSSN/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdSSN = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/GrpNum/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.GrpNum = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdLastName/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdLastName = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdFirstName/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdFirstName = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdName/@selected");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdName = objNode.Value.ToString() == "1" ? true : false;
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/SysNameText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.SysNameText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/PolSymText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.PolSymText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/ModuleText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.ModuleText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/LobText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.LobText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/PolNumText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.PolNumText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/PolStateText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.PolStateText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/LocCmpnyText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.LocCmpnyText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/LossDateText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.LossDateText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/AgntNumText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.AgntNumText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdZipText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdZipText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/CustNumText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.CustNumText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/MstrCmpnyText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.MstrCmpnyText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdCityText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdCityText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdSSNText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdSSNText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/GrpNumText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.GrpNumText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdLastNameText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdLastNameText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdFirstNameText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdFirstNameText = objNode.Value.ToString();
                }
                objNode = objPrevDoc.SelectSingleNode("//setting/PolicySearchConfig/InsrdNameText/@value");
                if (objNode != null && objNode.Value != null)
                {
                    m_PolicySearchConfig.InsrdNameText = objNode.Value.ToString();
                }

                if (bExists)
                {
                    objRootNode = objRetDoc.CreateElement("PolicySearchConfig");
                    objRetDoc.AppendChild(objRootNode);
                    this.CreateAndSetElement(objRootNode, "SysName", m_PolicySearchConfig.SysName.ToString());
                    this.CreateAndSetElement(objRootNode, "PolSym", m_PolicySearchConfig.PolSym.ToString());
                    this.CreateAndSetElement(objRootNode, "Module", m_PolicySearchConfig.Module.ToString());
                    this.CreateAndSetElement(objRootNode, "Lob", m_PolicySearchConfig.Lob.ToString());
                    this.CreateAndSetElement(objRootNode, "PolNum", m_PolicySearchConfig.PolNum.ToString());
                    this.CreateAndSetElement(objRootNode, "PolState", m_PolicySearchConfig.PolState.ToString());
                    this.CreateAndSetElement(objRootNode, "LocCmpny", m_PolicySearchConfig.LocCmpny.ToString());
                    this.CreateAndSetElement(objRootNode, "LossDate", m_PolicySearchConfig.LossDate.ToString());
                    this.CreateAndSetElement(objRootNode, "AgntNum", m_PolicySearchConfig.AgntNum.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdZip", m_PolicySearchConfig.InsrdZip.ToString());
                    this.CreateAndSetElement(objRootNode, "CustNum", m_PolicySearchConfig.CustNum.ToString());
                    this.CreateAndSetElement(objRootNode, "MstrCmpny", m_PolicySearchConfig.MstrCmpny.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdCity", m_PolicySearchConfig.InsrdCity.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdSSN", m_PolicySearchConfig.InsrdSSN.ToString());
                    this.CreateAndSetElement(objRootNode, "GrpNum", m_PolicySearchConfig.GrpNum.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdLastName", m_PolicySearchConfig.InsrdLastName.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdFirstName", m_PolicySearchConfig.InsrdFirstName.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdName", m_PolicySearchConfig.InsrdName.ToString());
                    this.CreateAndSetElement(objRootNode, "SysNameText", m_PolicySearchConfig.SysNameText.ToString());
                    this.CreateAndSetElement(objRootNode, "PolSymText", m_PolicySearchConfig.PolSymText.ToString());
                    this.CreateAndSetElement(objRootNode, "ModuleText", m_PolicySearchConfig.ModuleText.ToString());
                    this.CreateAndSetElement(objRootNode, "LobText", m_PolicySearchConfig.LobText.ToString());
                    this.CreateAndSetElement(objRootNode, "PolNumText", m_PolicySearchConfig.PolNumText.ToString());
                    this.CreateAndSetElement(objRootNode, "PolStateText", m_PolicySearchConfig.PolStateText.ToString());
                    this.CreateAndSetElement(objRootNode, "LocCmpnyText", m_PolicySearchConfig.LocCmpnyText.ToString());
                    this.CreateAndSetElement(objRootNode, "LossDateText", m_PolicySearchConfig.LossDateText.ToString());
                    this.CreateAndSetElement(objRootNode, "AgntNumText", m_PolicySearchConfig.AgntNumText.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdZipText", m_PolicySearchConfig.InsrdZipText.ToString());
                    this.CreateAndSetElement(objRootNode, "CustNumText", m_PolicySearchConfig.CustNumText.ToString());
                    this.CreateAndSetElement(objRootNode, "MstrCmpnyText", m_PolicySearchConfig.MstrCmpnyText.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdCityText", m_PolicySearchConfig.InsrdCityText.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdSSNText", m_PolicySearchConfig.InsrdSSNText.ToString());
                    this.CreateAndSetElement(objRootNode, "GrpNumText", m_PolicySearchConfig.GrpNumText.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdLastNameText", m_PolicySearchConfig.InsrdLastNameText.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdFirstNameText", m_PolicySearchConfig.InsrdFirstNameText.ToString());
                    this.CreateAndSetElement(objRootNode, "InsrdNameText", m_PolicySearchConfig.InsrdNameText.ToString());
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchConfig.GetPolicySearchConfig.Err", m_iClientId), p_objException);//rkaur27
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
                objNode = null;
                objRootNode = null;
            }
            return objRetDoc;
        }

        /// <summary>
        /// Saves the configuration settings of Policy Search screen
        /// </summary>
        /// <param name="p_objInputDoc"></param>
        public void SavePolicySrchConfig(XmlDocument p_objInputDoc)
        {
            XmlDocument objPrevXml = null;
            XmlNode objNode = null;
            DbReader objReader = null;
            bool bSuccess = false;
            string sPrevXml = string.Empty;
            string sSql = string.Empty;

            try
            {
                m_PolicySearchConfig.SysName = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/SysName"), out bSuccess);
                m_PolicySearchConfig.PolSym = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/PolSym"), out bSuccess);
                m_PolicySearchConfig.Module = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/Module"), out bSuccess);
                m_PolicySearchConfig.Lob = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/Lob"), out bSuccess);
                m_PolicySearchConfig.PolNum = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/PolNum"), out bSuccess);
                m_PolicySearchConfig.PolState = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/PolState"), out bSuccess);
                m_PolicySearchConfig.LocCmpny = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/LocCmpny"), out bSuccess);
                m_PolicySearchConfig.LossDate = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/LossDate"), out bSuccess);
                m_PolicySearchConfig.AgntNum = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/AgntNum"), out bSuccess);
                m_PolicySearchConfig.InsrdZip = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdZip"), out bSuccess);
                m_PolicySearchConfig.CustNum = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/CustNum"), out bSuccess);
                m_PolicySearchConfig.MstrCmpny = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/MstrCmpny"), out bSuccess);
                m_PolicySearchConfig.InsrdCity = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdCity"), out bSuccess);
                m_PolicySearchConfig.InsrdSSN = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdSSN"), out bSuccess);
                m_PolicySearchConfig.GrpNum = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/GrpNum"), out bSuccess);
                m_PolicySearchConfig.InsrdLastName = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdLastName"), out bSuccess);
                m_PolicySearchConfig.InsrdFirstName = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdFirstName"), out bSuccess);
                m_PolicySearchConfig.InsrdName = Conversion.CastToType<bool>(this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdName"), out bSuccess);
                m_PolicySearchConfig.SysNameText = this.GetValue(p_objInputDoc, "PolicySearchConfig/SysNameText");
                m_PolicySearchConfig.PolSymText = this.GetValue(p_objInputDoc, "PolicySearchConfig/PolSymText");
                m_PolicySearchConfig.ModuleText = this.GetValue(p_objInputDoc, "PolicySearchConfig/ModuleText");
                m_PolicySearchConfig.LobText = this.GetValue(p_objInputDoc, "PolicySearchConfig/LobText");
                m_PolicySearchConfig.PolNumText = this.GetValue(p_objInputDoc, "PolicySearchConfig/PolNumText");
                m_PolicySearchConfig.PolStateText = this.GetValue(p_objInputDoc, "PolicySearchConfig/PolStateText");
                m_PolicySearchConfig.LocCmpnyText = this.GetValue(p_objInputDoc, "PolicySearchConfig/LocCmpnyText");
                m_PolicySearchConfig.LossDateText = this.GetValue(p_objInputDoc, "PolicySearchConfig/LossDateText");
                m_PolicySearchConfig.AgntNumText = this.GetValue(p_objInputDoc, "PolicySearchConfig/AgntNumText");
                m_PolicySearchConfig.InsrdZipText = this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdZipText");
                m_PolicySearchConfig.CustNumText = this.GetValue(p_objInputDoc, "PolicySearchConfig/CustNumText");
                m_PolicySearchConfig.MstrCmpnyText = this.GetValue(p_objInputDoc, "PolicySearchConfig/MstrCmpnyText");
                m_PolicySearchConfig.InsrdCityText = this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdCityText");
                m_PolicySearchConfig.InsrdSSNText = this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdSSNText");
                m_PolicySearchConfig.GrpNumText = this.GetValue(p_objInputDoc, "PolicySearchConfig/GrpNumText");
                m_PolicySearchConfig.InsrdLastNameText = this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdLastNameText");
                m_PolicySearchConfig.InsrdFirstNameText = this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdFirstNameText");
                m_PolicySearchConfig.InsrdNameText = this.GetValue(p_objInputDoc, "PolicySearchConfig/InsrdNameText");

                sSql = "SELECT CONFIG_XML FROM POLICY_SRCH_CONFIG WHERE DSN_ID = " + m_DsnId;
                using (objReader = DbFactory.GetDbReader(m_ConnectionString, sSql))
                {
                    if (objReader.Read())
                    {
                        sPrevXml = Convert.ToString(objReader.GetValue("CONFIG_XML"));
                    }
                }
                sPrevXml = sPrevXml.Trim();
                objPrevXml = new XmlDocument();

                if (sPrevXml != string.Empty)
                {
                    objPrevXml.LoadXml(sPrevXml);
                }

                CreateDefaultXML(objPrevXml);

                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/SysName/@selected");
                objNode.Value = m_PolicySearchConfig.SysName ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/PolSym/@selected");
                objNode.Value = m_PolicySearchConfig.PolSym ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/Module/@selected");
                objNode.Value = m_PolicySearchConfig.Module ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/Lob/@selected");
                objNode.Value = m_PolicySearchConfig.Lob ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/PolNum/@selected");
                objNode.Value = m_PolicySearchConfig.PolNum ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/PolState/@selected");
                objNode.Value = m_PolicySearchConfig.PolState ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/LocCmpny/@selected");
                objNode.Value = m_PolicySearchConfig.LocCmpny ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/LossDate/@selected");
                objNode.Value = m_PolicySearchConfig.LossDate ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/AgntNum/@selected");
                objNode.Value = m_PolicySearchConfig.AgntNum ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdZip/@selected");
                objNode.Value = m_PolicySearchConfig.InsrdZip ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/CustNum/@selected");
                objNode.Value = m_PolicySearchConfig.CustNum ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/MstrCmpny/@selected");
                objNode.Value = m_PolicySearchConfig.MstrCmpny ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdCity/@selected");
                objNode.Value = m_PolicySearchConfig.InsrdCity ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdSSN/@selected");
                objNode.Value = m_PolicySearchConfig.InsrdSSN ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/GrpNum/@selected");
                objNode.Value = m_PolicySearchConfig.GrpNum ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdLastName/@selected");
                objNode.Value = m_PolicySearchConfig.InsrdLastName ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdFirstName/@selected");
                objNode.Value = m_PolicySearchConfig.InsrdFirstName ? "1" : "0";
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdName/@selected");
                objNode.Value = m_PolicySearchConfig.InsrdName ? "1" : "0";

                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/SysNameText/@value");
                objNode.Value = m_PolicySearchConfig.SysNameText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/PolSymText/@value");
                objNode.Value = m_PolicySearchConfig.PolSymText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/ModuleText/@value");
                objNode.Value = m_PolicySearchConfig.ModuleText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/LobText/@value");
                objNode.Value = m_PolicySearchConfig.LobText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/PolNumText/@value");
                objNode.Value = m_PolicySearchConfig.PolNumText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/PolStateText/@value");
                objNode.Value = m_PolicySearchConfig.PolStateText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/LocCmpnyText/@value");
                objNode.Value = m_PolicySearchConfig.LocCmpnyText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/LossDateText/@value");
                objNode.Value = m_PolicySearchConfig.LossDateText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/AgntNumText/@value");
                objNode.Value = m_PolicySearchConfig.AgntNumText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdZipText/@value");
                objNode.Value = m_PolicySearchConfig.InsrdZipText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/CustNumText/@value");
                objNode.Value = m_PolicySearchConfig.CustNumText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/MstrCmpnyText/@value");
                objNode.Value = m_PolicySearchConfig.MstrCmpnyText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdCityText/@value");
                objNode.Value = m_PolicySearchConfig.InsrdCityText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdSSNText/@value");
                objNode.Value = m_PolicySearchConfig.InsrdSSNText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/GrpNumText/@value");
                objNode.Value = m_PolicySearchConfig.GrpNumText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdLastNameText/@value");
                objNode.Value = m_PolicySearchConfig.InsrdLastNameText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdFirstNameText/@value");
                objNode.Value = m_PolicySearchConfig.InsrdFirstNameText;
                objNode = objPrevXml.SelectSingleNode("//setting/PolicySearchConfig/InsrdNameText/@value");
                objNode.Value = m_PolicySearchConfig.InsrdNameText;

                this.SaveXmlToDb(objPrevXml.OuterXml, Convert.ToInt32(m_DsnId));
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchConfig.SavePolicySrchConfig.Err", m_iClientId), p_objException);//rkaur27
            }
            finally
            {
                objPrevXml = null;
                objNode = null;
                if (objReader != null)
                    objReader.Dispose();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Create and Set the Text of the New Node.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Inner Text.</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchConfig.CreateAndSetElement.Err", m_iClientId), p_objEx);//rkaur27
            }
        }

        /// <summary>
        /// Create and Set the Text of the New Node.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Inner Text.</param>
        /// <param name="p_objChildNode">Child Node Refrence</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchConfig.CreateAndSetElement.Err", m_iClientId), p_objEx);//rkaur27
            }
        }

        ///// <summary>
        ///// Create new Element in the Xml Document.
        ///// </summary>
        ///// <param name="p_objParentNode">Parent node</param>
        ///// <param name="p_sNodeName">Node Name</param>
        ///// <param name="p_objChildNode">Refrence to the New Node Created.</param>
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchConfig.CreateElement.Err", m_iClientId), p_objEx);//rkaur27
            }
        }

        /// <summary>
        /// Get Value of the Node.
        /// </summary>
        /// <param name="p_objDocument">Input document.</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <returns>Node Inner Text.</returns>
        private string GetValue(XmlDocument p_objDocument, string p_sNodeName)
        {
            XmlElement objNode = null;
            string sValue = "";

            try
            {
                objNode = (XmlElement)p_objDocument.SelectSingleNode("//" + p_sNodeName);

                if (objNode != null)
                    sValue = objNode.InnerText;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetValue.Err", m_iClientId), p_objEx);//rkaur27
            }
            finally
            {
                objNode = null;
            }
            return (sValue);
        }

        /// <summary>
        /// Creates default XML template for Policy search default configuration values
        /// </summary>
        /// <param name="p_objPrevXML"></param>
        private void CreateDefaultXML(XmlDocument p_objPrevXML)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            try
            {
                if (p_objPrevXML == null)
                {
                    p_objPrevXML = new XmlDocument();
                }
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objPrevXML.CreateElement("setting");
                    p_objPrevXML.AppendChild(objTempNode);
                }

                // Create PolicySearchConfig node if Not present.
                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("/setting");
                    objTempNode = p_objPrevXML.CreateElement("PolicySearchConfig");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/SysName");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("SysName");
                    //SysName is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/PolSym");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("PolSym");
                    //SysName is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/Module");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("Module");
                    //Module is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                
                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/Lob");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("Lob");
                    //Lob is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/PolNum");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("PolNum");
                    //PolicyNumber is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/PolState");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("PolState");
                    //PolicyState is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/LocCmpny");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("LocCmpny");
                    //LocationCompany is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/LossDate");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("LossDate");
                    //Lossdate is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/AgntNum");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("AgntNum");
                    //AgentNumber is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdZip");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdZip");
                    //InsuredZip is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/CustNum");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("CustNum");
                    //CustomerNumber is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/MstrCmpny");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("MstrCmpny");
                    //MasterCompany is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdCity");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdCity");
                    //InsuredCity is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdSSN");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdSSN");
                    //InsuredSSN is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/GrpNum");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("GrpNum");
                    //GroupNumber is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdLastName");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdLastName");
                    //InsuredLastName is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdFirstName");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdFirstName");
                    //InsuredFirstname is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdName");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdName");
                    //InsuredName is False by default
                    objTempNode.SetAttribute("selected", "0");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/SysNameText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("SysNameText");
                    objTempNode.SetAttribute("value", "System Name:");
                    objParentNode.AppendChild(objTempNode);
                }


                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/PolSymText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("PolSymText");
                    objTempNode.SetAttribute("value", "Policy Symbol:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/ModuleText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("ModuleText");
                    objTempNode.SetAttribute("value", "Module:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/LobText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("LobText");
                    objTempNode.SetAttribute("value", "Line Of Business:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/PolNumText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("PolNumText");
                    objTempNode.SetAttribute("value", "Policy Number:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/PolStateText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("PolStateText");
                    objTempNode.SetAttribute("value", "Policy State:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/LocCmpnyText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("LocCmpnyText");
                    objTempNode.SetAttribute("value", "Location Company:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/LossDateText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("LossDateText");
                    objTempNode.SetAttribute("value", "Date of Loss:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/AgntNumText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("AgntNumText");
                    objTempNode.SetAttribute("value", "Agent Number:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdZipText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdZipText");
                    objTempNode.SetAttribute("value", "Insured Zip:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/CustNumText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("CustNumText");
                    objTempNode.SetAttribute("value", "Customer Number:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/MstrCmpnyText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("MstrCmpnyText");
                    objTempNode.SetAttribute("value", "Master Company:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdCityText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdCityText");
                    objTempNode.SetAttribute("value", "Insured City:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdSSNText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdSSNText");
                    objTempNode.SetAttribute("value", "Insured SSN:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/GrpNumText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("GrpNumText");
                    objTempNode.SetAttribute("value", "Group Number:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdLastNameText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdLastNameText");
                    objTempNode.SetAttribute("value", "Insured Last Name:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdFirstNameText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdFirstNameText");
                    objTempNode.SetAttribute("value", "Insured First Name:");
                    objParentNode.AppendChild(objTempNode);
                }

                objTempNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig/InsrdNameText");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objPrevXML.SelectSingleNode("//PolicySearchConfig");
                    objTempNode = p_objPrevXML.CreateElement("InsrdNameText");
                    objTempNode.SetAttribute("value", "Insured Name:");
                    objParentNode.AppendChild(objTempNode);
                }
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchConfig.CreateDefaultXML.Err", m_iClientId), p_objException);//rkaur27
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }

        /// <summary>
        /// Save Policy search configuration xml to database
        /// </summary>
        /// <param name="p_sConfigXml"></param>
        /// <param name="p_iDsnId"></param>
        private void SaveXmlToDb(string p_sConfigXml, int p_iDsnId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            string sSQL = string.Empty;

            try
            {
                //Try to delete the old one.
                sSQL = "DELETE FROM POLICY_SRCH_CONFIG WHERE DSN_ID = " + p_iDsnId;
                objConn = DbFactory.GetDbConnection(m_ConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.Close();

                objWriter = DbFactory.GetDbWriter(m_ConnectionString);
                objWriter.Tables.Add("POLICY_SRCH_CONFIG");
                objWriter.Fields.Add("DSN_ID", p_iDsnId);
                objWriter.Fields.Add("CONFIG_XML", p_sConfigXml);
                objWriter.Execute();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("PolicySearchConfig.SaveXmlToDb.Err", m_iClientId), p_objException);//rkaur27
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
            }
        }

        #endregion
    }
}
