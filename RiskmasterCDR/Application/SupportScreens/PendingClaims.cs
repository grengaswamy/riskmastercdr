﻿/**********************************************************************************************
 *   date     |  jira     | programmer  | description                                            *
 **********************************************************************************************
 * 03/20/2015 | rma-8135  | gsinghal7   | created initial version to get pending claims
 **********************************************************************************************/

using System.Linq;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Xml;
using Riskmaster.Settings;
using Newtonsoft.Json;

namespace Riskmaster.Application.SupportScreens
{
    public class PendingClaims
    {
        string m_ConnectionString = string.Empty;
        int m_UserId = 0;
        string m_LoginName = "";
        string m_GridId = "";
        int m_DsnId = 0;

        private int m_iClientId = 0;
        private static string m_sDBType = "";
        private static Dictionary<string, string> m_UserIdList = new Dictionary<string, string>();
        private int m_iLangCode = 0;

        #region Constructor

        public PendingClaims(string connectionString, int userId, string loginName, int p_iClientId, int languageCode)
        {
            DbConnection objConn;
            m_ConnectionString = connectionString;
            m_UserId = userId;
            m_LoginName = loginName;
            if (m_iLangCode == 0)
                m_iLangCode = languageCode;

            //dvatsa
            m_iClientId = p_iClientId;
            //Added By Neha Mits 23633
            objConn = DbFactory.GetDbConnection(m_ConnectionString);
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            objConn.Close();
        }

        #endregion

        #region public methods

        /// <summary>
        /// Provides Pending Claims
        /// </summary>
        /// <param name="p_objXmlDoc"></param>
        /// <returns></returns>
        public XmlDocument GetPendingClaimRecrods(XmlDocument p_objXmlDoc, int dsnId, string gridId)
        {
            string recordType = string.Empty;
            m_DsnId = dsnId;
            m_GridId = gridId;
            //  XmlNodeList recentRecordsList = null;
            XmlDocument p_objXmlOut = null;

            try
            {
                p_objXmlOut = GetPendingClaims(p_objXmlDoc);

                return p_objXmlOut;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("PendingClaims.GetPendingClaims.Error", m_iClientId), p_objEx);
            }

        }


        /// <summary>
        /// Provides all reportess to bind them to Dropdown List
        /// </summary>
        /// <param name="p_objXmlIn"></param>
        /// <returns></returns>
        public XmlDocument GetDDItems(XmlDocument p_objXmlIn)
        {
            try
            {
                XmlDocument objXMLDoc = new XmlDocument();
                XmlElement objdisplaycolumn = objXMLDoc.CreateElement("displaycolumn");
                objXMLDoc.AppendChild(objdisplaycolumn);
                XmlElement objcontrol = objXMLDoc.CreateElement("control");
                XmlElement objOption = null;
                Dictionary<string, object> dicGridData = new Dictionary<string, object>();
                List<Dictionary<string, object>> lstDictData = new List<Dictionary<string, object>>();

                objdisplaycolumn.AppendChild(objcontrol);
                GetAllEmployees(m_UserId);
                if (m_UserIdList.Count > 0)
                {
                    foreach (var item in m_UserIdList.Keys)
                    {
                        dicGridData = new Dictionary<string, object>();
                        dicGridData.Add("userId", Convert.ToString(item));
                        dicGridData.Add("userName", Convert.ToString(m_UserIdList[item]));
                        lstDictData.Add(dicGridData);
                    }
                }
                objcontrol.InnerText = Conversion.CreateJSONStringFromDictionary(lstDictData);
                return objXMLDoc;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("PendingClaims.GetDDist.Error", m_iClientId), p_objEx);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Provides Pending Claims
        /// </summary>
        /// <param name="p_inobjXmlDoc"></param>
        /// <returns></returns>
        private XmlDocument GetPendingClaims(XmlDocument p_inobjXmlDoc)
        {
            StringBuilder strSql = new StringBuilder();
            List<int> claimidList = new List<int>();
            XmlElement objPendingClaims = null;
            XmlDocument p_objXmlDoc = null;
            XmlDocument userPrefXML = new XmlDocument(); ;
            string s_Userlst = string.Empty;
            string l_RM_USER_ID = string.Empty;
            LocalCache objCache = null;
            List<Dictionary<string, object>> finalListData = new List<Dictionary<string, object>>();
            string strJsonResponse = "";
            Dictionary<string, object> dicGridData = new Dictionary<string, object>();
            List<Dictionary<string, object>> l_ListDictData = new List<Dictionary<string, object>>();
            string sUserPrefsXML = string.Empty;
            string sDbDateValue = string.Empty;
            string sUiDate = string.Empty;
            int TotalCount = 0;


            try
            {
                objCache = new LocalCache(m_ConnectionString, m_iClientId);
                p_objXmlDoc = new XmlDocument();
                bool argShowAll = Convert.ToBoolean(p_inobjXmlDoc.SelectSingleNode("//showAll").InnerText);
                int argReporteeId = Convert.ToInt32(p_inobjXmlDoc.SelectSingleNode("//userId").InnerText);

                objPendingClaims = p_objXmlDoc.CreateElement("pendingclaims");
                p_objXmlDoc.AppendChild(objPendingClaims);
                XmlElement objDataElement = null;
                CreateElement(p_objXmlDoc.DocumentElement, "Data", ref objDataElement);

                #region Prepare Json for UserPref and add to XMl doc
                string strJsonUserPref = "";
                strJsonUserPref = GetOrCreateUserPreference(m_UserId, "PendingClaims.aspx", m_DsnId, m_GridId);

                if (argReporteeId == -1)
                {
                    //get value of show all checkbox from user pref for the first time only
                    Dictionary<string, object> l_dict = new Dictionary<string, object>();
                    JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(strJsonUserPref)), l_dict);
                    Dictionary<string, object> l_dict1 = new Dictionary<string, object>();
                    JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(l_dict["AdditionalUserPref"].ToString())), l_dict1);
                    argShowAll = Convert.ToBoolean(l_dict1["showAll"]);

                }

                XmlElement objUserPrefXmlElement = p_objXmlDoc.CreateElement("UserPref");
                objUserPrefXmlElement.InnerText = strJsonUserPref;
                objPendingClaims.AppendChild(objUserPrefXmlElement);
                #endregion

                //Add hidden column Claim Id and header template in column definition
                //ModifyResultXML(p_objXmlDoc, userPrefXML);

                string strshowAll = String.Empty;
                string strJoin = String.Empty;

                //case when All is selected from Dropdown
                if (argReporteeId == -999)//-999 represent All data
                {
                    l_RM_USER_ID = string.Join(",", m_UserIdList.Where(x => x.Key != argReporteeId.ToString()).Select(x => x.Key));
                }

                //get rm_user_id based on value selected from drop down list
                else if (argReporteeId == -1 || argReporteeId == m_UserId)//-1 represent logged in user
                {
                    l_RM_USER_ID = Convert.ToString(m_UserId);
                }
                else
                {
                    l_RM_USER_ID = Convert.ToString(argReporteeId);
                }

                //if check box is not selected and clause to select only primary adjuster
                if (argShowAll == false)
                {
                    strshowAll = " AND CA.CURRENT_ADJ_FLAG!=0 ";
                }

                string strname = String.Empty;
                #region commented code to get comma separated values from row
                //                if (string.Equals(m_sDBType, Constants.DB_ORACLE.ToUpper(), StringComparison.OrdinalIgnoreCase))
                //                {
                //                    strname = @"(SELECT LISTAGG ( (COALESCE(E4.FIRST_NAME,'') ||' ' || COALESCE(E4.LAST_NAME,'') ) ,',') WITHIN GROUP (ORDER BY E4.FIRST_NAME) FROM ENTITY E4 LEFT JOIN CLAIMANT CL1 ON 
                //CL1.CLAIMANT_EID= E4.ENTITY_ID WHERE CL1.CLAIM_ID=CLAIM.CLAIM_ID) AS CLAIMANT ,
                //(SELECT LISTAGG ( (COALESCE(E4.FIRST_NAME,'') ||' ' || COALESCE(E4.LAST_NAME,'') ) ,',') WITHIN GROUP (ORDER BY E4.FIRST_NAME) FROM  ENTITY E4 LEFT JOIN POLICY_X_INSURED PL1 ON PL1.INSURED_EID= E4.ENTITY_ID 
                //WHERE PL1.POLICY_ID=CLAIM.PRIMARY_POLICY_ID) AS INSURED";
                //                }
                //                else
                //                {
                //                    strname =
                //                      @"(STUFF((SELECT ',' + (ISNULL(E4.FIRST_NAME,'')+' '+ISNULL(E4.LAST_NAME,''))
                //FROM ENTITY E4 LEFT JOIN CLAIMANT CL1 ON CL1.CLAIMANT_EID= E4.ENTITY_ID WHERE CL1.CLAIM_ID=CLAIM.CLAIM_ID FOR XML PATH(''), root('CLAIMANTNAME'), type 
                //     ).value('/CLAIMANTNAME[1]','varchar(max)'),1,2,'')) CLAIMANT , 
                //(STUFF((SELECT ', ' + (ISNULL(E4.FIRST_NAME,'')+' '+ISNULL(E4.LAST_NAME,'')) FROM ENTITY E4 LEFT JOIN POLICY_X_INSURED PL1 ON PL1.INSURED_EID= E4.ENTITY_ID 
                //WHERE PL1.POLICY_ID=CLAIM.PRIMARY_POLICY_ID FOR XML PATH(''), root('INSUREDNAME'), type 
                //     ).value('/INSUREDNAME[1]','varchar(max)'),1,2,'')) INSURED ";

                //                }
                #endregion

                //create separator based on db type oracle/Sql server
                string strSeparator = String.Empty;
                if (string.Equals(m_sDBType, Constants.DB_ORACLE.ToUpper(), StringComparison.OrdinalIgnoreCase))
                {
                    strSeparator = "||' ' || ";
                }
                else
                {
                    strSeparator = "+' ' + ";
                }
                string sTempTable4ClaimType = CommonFunctions.CreateTempTable4MultiLingual(m_ConnectionString, "CLAIM_TYPE", "CLAIM_TYPE", m_iLangCode, m_iClientId);
                string sTempTable4CLaimStatus = CommonFunctions.CreateTempTable4MultiLingual(m_ConnectionString, "CLAIM_STATUS", "CLAIM_STATUS", m_iLangCode, m_iClientId);

                strSql = new StringBuilder();
                
                strSql.AppendFormat(@"SELECT CLAIM_ID,RESERVE_CLAIMID, CLAIM_NUMBER,
                                        CASE WHEN AddOrSubtractRecRsvToIncurred = - 1 THEN Coalesce(NonRecoveryNetIncurred,0) + Coalesce(RecoveryNetIncurred,0)
		                                    Else
			                                    Coalesce(NonRecoveryNetIncurred,0) - Coalesce(RecoveryNetIncurred,0) 
	                                        END NET_INCURRED, 
                                        CASE WHEN MIN(CLAIMANT)=' ' OR MIN(CLAIMANT) IS NULL THEN '_' ELSE MIN(CLAIMANT) END CLAIMANT,
                                        CASE WHEN MIN(INSURED)=' ' OR MIN(INSURED) IS NULL THEN '_' ELSE MIN(INSURED) END INSURED,
                                        LTRIM(MIN(POLICY_NAME)) POLICY_NAME,LOB,EVENT_NUMBER,DATE_OF_EVENT,DATE_OF_CLAIM,
                                        CLAIM_TYPE,CLAIM_STATUS ,MY_ROLE
										FROM (
                                        SELECT DISTINCT CLAIM.CLAIM_ID,RC.CLAIM_ID RESERVE_CLAIMID, CLAIM.CLAIM_NUMBER,
                                       LTRIM( COALESCE(E1.FIRST_NAME,'')" + strSeparator + @"COALESCE(E1.LAST_NAME,'')) CLAIMANT,
                                        LTRIM(COALESCE(E2.FIRST_NAME,'')" + strSeparator + @"COALESCE(E2.LAST_NAME,'') ) INSURED,
                                        (
	                                        SELECT		SUM(COALESCE(INCURRED_AMOUNT,0))
	                                        FROM		RESERVE_CURRENT NonRecoveryRsv INNER JOIN CODES CHILD 
					                                        ON NonRecoveryRsv.RESERVE_TYPE_CODE = CHILD.CODE_ID 
				                                        INNER JOIN CODES PARENT 
					                                        ON CHILD.RELATED_CODE_ID = PARENT.CODE_ID 
	                                        WHERE		NonRecoveryRsv.CLAIM_ID = Claim.Claim_id
				                                        AND PARENT.SHORT_CODE <> 'R'
	                                        GROUP BY	NonRecoveryRsv.CLAIM_ID
                                        ) NonRecoveryNetIncurred,
                                        (
	                                        SELECT		SUM(COALESCE(INCURRED_AMOUNT,0))
	                                        FROM		RESERVE_CURRENT RecoveryRsv INNER JOIN CODES CHILD 
					                                        ON RecoveryRsv.RESERVE_TYPE_CODE = CHILD.CODE_ID 
				                                        INNER JOIN CODES PARENT 
					                                        ON CHILD.RELATED_CODE_ID = PARENT.CODE_ID 
	                                        WHERE		RecoveryRsv.CLAIM_ID = Claim.Claim_id
				                                        AND PARENT.SHORT_CODE = 'R'
	                                        GROUP BY	RecoveryRsv.CLAIM_ID
                                        ) RecoveryNetIncurred,
                                        (
	                                        SELECT ADD_RCV_RSV_TO_INC_AMT FROM SYS_PARMS_LOB WHERE SYS_PARMS_LOB.LINE_OF_BUS_CODE = CLAIM.LINE_OF_BUS_CODE
                                        ) AddOrSubtractRecRsvToIncurred,
                                        COALESCE(POLICY.POLICY_NAME,'_') POLICY_NAME,COALESCE(LOBCodesText.CODE_DESC,'_') LOB,
                                        CLAIM.EVENT_NUMBER,EVENT.DATE_OF_EVENT,CLAIM.DATE_OF_CLAIM,
                                        COALESCE(ClaimType.CODE_DESC,'_') AS CLAIM_TYPE,ClaimStatus.CODE_DESC AS CLAIM_STATUS ,CA.CURRENT_ADJ_FLAG AS MY_ROLE 
                                        FROM CLAIM  
                                        LEFT JOIN CLAIMANT ON CLAIMANT.CLAIM_ID= CLAIM.CLAIM_ID 
                                        LEFT JOIN CLAIM_ADJUSTER CA ON CA.CLAIM_ID=CLAIM.CLAIM_ID 
                                        LEFT JOIN ENTITY E1 ON E1.ENTITY_ID=CLAIMANT.CLAIMANT_EID
                                        LEFT JOIN POLICY_X_INSURED ON POLICY_X_INSURED.POLICY_ID=CLAIM.PRIMARY_POLICY_ID
                                        LEFT JOIN ENTITY E2 ON E2.ENTITY_ID=POLICY_X_INSURED.INSURED_EID
                                        LEFT JOIN RESERVE_CURRENT ON RESERVE_CURRENT.CLAIM_ID=CLAIM.CLAIM_ID
                                        LEFT JOIN POLICY ON POLICY.POLICY_ID=CLAIM.PRIMARY_POLICY_ID
                                        LEFT JOIN EVENT ON EVENT.EVENT_ID=CLAIM.EVENT_ID
                                        LEFT JOIN " + sTempTable4ClaimType + " ClaimType ON ClaimType.CODE_ID=CLAIM.CLAIM_TYPE_CODE " +
                                        "LEFT JOIN " + sTempTable4CLaimStatus + " ClaimStatus ON ClaimStatus.CODE_ID=CLAIM.CLAIM_STATUS_CODE " +
                                        @"LEFT JOIN CODES LOBCodes ON LOBCodes.CODE_ID = CLAIM.LINE_OF_BUS_CODE
                                        LEFT JOIN CODES_TEXT LOBCodesText ON LOBCodes.CODE_ID = LOBCodesText.CODE_ID
                                        LEFT JOIN ENTITY E3 ON E3.ENTITY_ID = CA.ADJUSTER_EID
                                        LEFT JOIN RESERVE_CURRENT RC ON CLAIM.CLAIM_ID=RC.CLAIM_ID AND RC.ASSIGNADJ_EID=CA.ADJUSTER_EID
                                        WHERE  E3.RM_USER_ID in (" + l_RM_USER_ID + ") " + strshowAll +
                                        @" AND ClaimStatus.RELATED_CODE_ID = " + objCache.GetCodeId("O", "STATUS") +
                                        @" GROUP BY CLAIM.CLAIM_ID,RC.CLAIM_ID, CLAIM.CLAIM_NUMBER,E1.LAST_NAME,E1.FIRST_NAME,E2.FIRST_NAME,E2.LAST_NAME,
                                        POLICY.POLICY_NAME,LOBCodesText.CODE_DESC ,CLAIM.EVENT_NUMBER,EVENT.DATE_OF_EVENT,CLAIM.DATE_OF_CLAIM, ClaimType.CODE_DESC  ,ClaimStatus.CODE_DESC,
                                        CA.CURRENT_ADJ_FLAG ,CLAIM.PRIMARY_POLICY_ID,CLAIM.LINE_OF_BUS_CODE
                                        ) DATA_VIEW
                                        GROUP BY CLAIM_ID,RESERVE_CLAIMID, CLAIM_NUMBER,
                                        NonRecoveryNetIncurred, RecoveryNetIncurred,
                                        POLICY_NAME,LOB,EVENT_NUMBER,DATE_OF_EVENT,DATE_OF_CLAIM,
                                        CLAIM_TYPE,CLAIM_STATUS ,MY_ROLE, AddOrSubtractRecRsvToIncurred ORDER BY 1
                                        ");

                
                //Read data and append to XML 
                using (DbReader objReader = DbFactory.GetDbReader(m_ConnectionString, strSql.ToString()))
                {
                    while (objReader.Read())
                    {
                        dicGridData = new Dictionary<string, object>();
                        //set adjuster as per flag received
                        if (argShowAll == false)//check box false; only primary adjuster
                        {
                            dicGridData.Add("my_role",  Globalization.GetString("PendingClaims.PrimaryAdjuster", m_iClientId));
                        }
                        else
                        {//if show all, check box is checked there will be below four roles
                            if (Convert.ToInt32(objReader.GetValue("MY_ROLE")) != 0 && (String.IsNullOrEmpty(Convert.ToString(objReader.GetValue("RESERVE_CLAIMID"))) || Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_CLAIMID"), m_iClientId) == 0))
                            {
                                dicGridData.Add("my_role", "1");//Primary  Adjuster
                            }
                            else if (Convert.ToInt32(objReader.GetValue("MY_ROLE")) != 0 && !(String.IsNullOrEmpty(Convert.ToString(objReader.GetValue("RESERVE_CLAIMID"))) || Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_CLAIMID"), m_iClientId) == 0))
                            {
                                dicGridData.Add("my_role", "2");//Primary and Reserve Adjuster
                            }
                            if (Convert.ToInt32(objReader.GetValue("MY_ROLE")) == 0 && (String.IsNullOrEmpty(Convert.ToString(objReader.GetValue("RESERVE_CLAIMID"))) || Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_CLAIMID"), m_iClientId) == 0))
                            {
                                dicGridData.Add("my_role", "3");//Claim Adjuster
                            }
                            else if (Convert.ToInt32(objReader.GetValue("MY_ROLE")) == 0 && !(String.IsNullOrEmpty(Convert.ToString(objReader.GetValue("RESERVE_CLAIMID"))) || Conversion.ConvertObjToInt(objReader.GetValue("RESERVE_CLAIMID"), m_iClientId) == 0))
                            {
                                dicGridData.Add("my_role", "4");//Claim and Reserve Adjuster
                            }
                        }
                        dicGridData.Add("claim_id", Convert.ToString(objReader.GetValue("CLAIM_ID")));
                        dicGridData.Add("claim_number", Convert.ToString(objReader.GetValue("CLAIM_NUMBER")));
                        dicGridData.Add("claimant", Convert.ToString(objReader.GetValue("CLAIMANT")));
                        dicGridData.Add("insured", Convert.ToString(objReader.GetValue("INSURED")));

                        // dicGridData.Add("net_incurred", (Convert.ToInt32(objReader.GetValue("NET_INCURRED"))).ToString("C"));
                        int iBaseCurCode = CommonFunctions.GetBaseCurrencyCode(m_ConnectionString);
                        double dNetIncurred = Conversion.ConvertObjToDouble(objReader.GetValue("net_incurred"), m_iClientId);
                        dicGridData.Add("net_incurred", CommonFunctions.ConvertByCurrencyCode(iBaseCurCode, dNetIncurred, m_ConnectionString, m_iClientId));
                        //rkotak:starts, custom sorting to sort on underlying data and not on the data that is displayed to user                       
                        GridCommonFunctions.GetSortColumnForCurrencyField(ref dicGridData, "net_incurred", dNetIncurred);

                        dicGridData.Add("policy_name", Convert.ToString(objReader.GetValue("POLICY_NAME")));
                        dicGridData.Add("lob", Convert.ToString(objReader.GetValue("LOB")));
                        dicGridData.Add("event_number", Convert.ToString(objReader.GetValue("EVENT_NUMBER")));
                        
                        //  dicGridData.Add("date_of_event", Conversion.GetDBDateFormat(objReader.GetValue("DATE_OF_EVENT").ToString(), "d"));
                        dicGridData.Add("date_of_event", Conversion.GetUIDate(objReader.GetString("DATE_OF_EVENT"), m_iLangCode.ToString(), m_iClientId));
                        GridCommonFunctions.GetSortColumnForDateField(ref dicGridData, "date_of_event", objReader.GetString("DATE_OF_EVENT"));
                        
                        // dicGridData.Add("date_of_claim", Conversion.GetDBDateFormat(objReader.GetValue("DATE_OF_CLAIM").ToString(), "d"));
                        dicGridData.Add("date_of_claim", Conversion.GetUIDate(objReader.GetString("DATE_OF_CLAIM"), m_iLangCode.ToString(), m_iClientId));
                        GridCommonFunctions.GetSortColumnForDateField(ref dicGridData, "date_of_claim", objReader.GetString("DATE_OF_CLAIM"));

                        dicGridData.Add("claim_type", Convert.ToString(objReader.GetValue("CLAIM_TYPE")));
                        dicGridData.Add("claim_status", Convert.ToString(objReader.GetValue("CLAIM_STATUS")));

                        //append child to parent node                        
                        l_ListDictData.Add(dicGridData);

                    }
                    //extract unique row for multiple row in case of all data is selected
                    if (argReporteeId == -999)
                    {
                        foreach (Dictionary<string, object> item in l_ListDictData)
                        {
                            if (finalListData.Find(d => d["claim_id"].ToString() == item["claim_id"].ToString()) == null)
                            {
                                //get all items with same claim id from the list
                                List<Dictionary<string, object>> l_Bunch = l_ListDictData.FindAll(x => Convert.ToString(x["claim_id"]) == Convert.ToString(item["claim_id"]));

                                // if more than one claim than get row with minimum my role
                                if (l_Bunch.Count > 1)
                                {
                                    finalListData.Add(l_Bunch.Find(d => d["my_role"] == l_Bunch.Min(p => p["my_role"])));
                                }
                                else
                                {
                                    finalListData.Add(item);
                                }
                            }
                        }
                        AddRole(ref finalListData);
                        //cretae JSON string from data
                        strJsonResponse = Conversion.CreateJSONStringFromDictionary(finalListData);
                        TotalCount = finalListData.Count;
                    }
                    else
                    {
                        AddRole(ref l_ListDictData);
                        //cretae JSON string from data
                        strJsonResponse = Conversion.CreateJSONStringFromDictionary(l_ListDictData);
                        TotalCount = l_ListDictData.Count;
                    }

                    //Add JSON response to XML node
                    objDataElement.InnerText = strJsonResponse;
                    objPendingClaims.AppendChild(objDataElement);
                }



                #region Prepare Json string for additional data and add to XMl doc
                XmlElement objAdditionalDataXmlElement = p_objXmlDoc.CreateElement("AdditionalData");
                strJsonResponse = string.Empty;
                dicGridData = new Dictionary<string, object>();
                l_ListDictData.Clear();
                dicGridData.Add("TotalCount", TotalCount.ToString());
                l_ListDictData.Add(dicGridData);
                strJsonResponse = Conversion.CreateJSONStringFromDictionary(l_ListDictData);
                objAdditionalDataXmlElement.InnerText = strJsonResponse;
                objPendingClaims.AppendChild(objAdditionalDataXmlElement);
                #endregion

                return p_objXmlDoc;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PendingClaims.GetPendingClaims.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objCache.Dispose();
                if (strSql != null)
                    strSql = null;
                if (p_objXmlDoc != null)
                    p_objXmlDoc = null;
            }
            //return p_objXmlDoc;
        }

        /// <summary>
        /// Resolve my role in the list
        /// </summary>
        /// <param name="argListData"></param>
        private void AddRole(ref List<Dictionary<string, object>> argListData)
        {
            foreach (var item in argListData)
            {
                if (item["my_role"].ToString() == "1")
                    item["my_role"] = Globalization.GetString("PendingClaims.PrimaryAdjuster", m_iClientId); //"Primary Adjuster"; 
                if (item["my_role"].ToString() == "2")
                    item["my_role"] = Globalization.GetString("PendingClaims.PrimaryAndReserveAdjuster", m_iClientId); //"Primary and Reserve Adjuster";
                if (item["my_role"].ToString() == "3")
                    item["my_role"] = Globalization.GetString("PendingClaims.ClaimAdjuster", m_iClientId); //"Claim Adjuster";
                if (item["my_role"].ToString() == "4")
                    item["my_role"] = Globalization.GetString("PendingClaims.ClaimAndReserveAdjuster", m_iClientId); //"Claim and Reserve Adjuster";
            }

        }

        /// <summary>
        /// asharma326 Getting all levels of employees.
        /// </summary>
        /// <param name="m_UserId"></param>
        private void GetAllEmployees(int m_UserId)
        {
            //get securityDB type
            try
            {
                m_UserIdList = new Dictionary<string, string>();
                DbConnection objConn = DbFactory.GetDbConnection(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId));
                objConn.Open();
                string s_SecurityDBType = objConn.DatabaseType.ToString().ToUpper();
                objConn.Close();
                //no need to check whether logged in user is adjuster or not.jira 11744
                //check if logged user is an adjuster
//                StringBuilder sbSql = new StringBuilder();
//                sbSql.AppendFormat(@"SELECT DISTINCT ENTITY.RM_USER_ID,CLAIM_ADJUSTER.ADJUSTER_EID,DELETED_FLAG FROM 
//                                ENTITY  LEFT JOIN CLAIM_ADJUSTER  ON ENTITY.ENTITY_ID=CLAIM_ADJUSTER.ADJUSTER_EID 
//                                WHERE ENTITY.DELETED_FLAG=0  AND ENTITY.RM_USER_ID = " + m_UserId );
               
//                using (DbReader objReader = DbFactory.GetDbReader(m_ConnectionString, sbSql.ToString()))
//                {
//                    if (!objReader.Read())
//                        return;
//                }


                StringBuilder sbSql = new StringBuilder(); ;
                if (string.Equals(s_SecurityDBType, Constants.DB_ORACLE.ToUpper(), StringComparison.OrdinalIgnoreCase))
                {
                    sbSql.AppendFormat(@"SELECT USER_ID EMP_ID, FIRST_NAME EMP_FNAME,LAST_NAME EMP_LNAME, MANAGER_ID MGR_ID,LEVEL EMPLEVEL
	                            FROM USER_TABLE 
                                START WITH USER_TABLE.USER_ID=" + m_UserId + @"  
                                CONNECT BY NOCYCLE PRIOR USER_ID = MANAGER_ID");
                }
                else
                {

                    sbSql.AppendFormat(@"WITH EMPLOYEELIST AS 
	                            (
		                        SELECT 
                                '/'+CAST( MGR.USER_ID AS VARCHAR(MAX)) AS RELPATH,
			                     MGR.USER_ID EMP_ID, 
			                     MGR.FIRST_NAME EMP_FNAME,
			                     MGR.LAST_NAME EMP_LNAME, 
			                     MGR.MANAGER_ID AS 'MGR_ID',
			                     1 AS EMPLEVEL
		                         FROM USER_TABLE MGR 
		                         WHERE MGR.USER_ID=" + m_UserId + @"
		                         UNION ALL 
		                         SELECT
				                        EL.RELPATH +'/'+ CAST( UT1.USER_ID AS VARCHAR(MAX)) AS RELPATH, 
				                        UT1.USER_ID EMP_ID, 
				                        UT1.FIRST_NAME EMP_FNAME,
				                        UT1.LAST_NAME EMP_LNAME, 
				                        UT1.MANAGER_ID AS 'MGR_ID', 
				                        EL.EMPLEVEL + 1 AS EMPLEVEL
		                         FROM USER_TABLE UT1 INNER JOIN EMPLOYEELIST EL
		                         ON UT1.MANAGER_ID= EL.EMP_ID
		                         WHERE CHARINDEX('/'+CAST( UT1.USER_ID AS VARCHAR(MAX))+'/',RELPATH) = 0
	                            )
                                SELECT * FROM EMPLOYEELIST
                                ");
                }


                using (DataSet ds = DbFactory.GetDataSet(Riskmaster.Security.SecurityDatabase.GetSecurityDsn(m_iClientId), sbSql.ToString(), m_iClientId))
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        if (!m_UserIdList.ContainsKey(Convert.ToString(row["EMP_ID"])))
                        {
                            m_UserIdList.Add(Convert.ToString(row["EMP_ID"]), Convert.ToString(row["EMP_FNAME"])
                                                      + " " + Convert.ToString(row["EMP_LNAME"]));
                        }

                    }
                }
//                //Get all user who are also an adjuster
//                sbSql = new StringBuilder();
//                sbSql.AppendFormat(@"SELECT DISTINCT ENTITY.RM_USER_ID,CLAIM_ADJUSTER.ADJUSTER_EID FROM 
//                                ENTITY  LEFT JOIN CLAIM_ADJUSTER  ON ENTITY.ENTITY_ID=CLAIM_ADJUSTER.ADJUSTER_EID 
//                                WHERE ENTITY.DELETED_FLAG=0 AND ENTITY.RM_USER_ID IN (" + string.Join(",", m_UserIdList.Select(x => x.Key)) + @")"
//                                );

//                List<string> l_AdjList = new List<string>();
//                using (DbReader objReader = DbFactory.GetDbReader(m_ConnectionString, sbSql.ToString()))
//                {
//                    while (objReader.Read())
//                    {
//                        l_AdjList.Add(objReader.GetValue("RM_USER_ID").ToString());
//                    }
//                }
                //filter m_UserIdList with logged in user + subordiates who are only adjusters. exclude rest users.
                //modify user list to contain only adjusters
                //if (l_AdjList.Count > 0)
                //{
                //m_UserIdList = m_UserIdList.Where(a => a.Key == Convert.ToString(m_UserId)|| l_AdjList.Contains(a.Key)).ToDictionary(x => x.Key, x => x.Value);//.Where(x => l_AdjList.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
                //}
                //if (!m_UserIdList.ContainsKey(m_UserId.ToString()))
                //{

                //}
                //Add "All" fied to list
                if (m_UserIdList.Count >1)
                {
                    if (!m_UserIdList.ContainsKey("-999"))
                    {
                        m_UserIdList.Add("-999", "All");
                    }
                }
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("PendingClaims.GetDDist.Error", m_iClientId), p_objEx);
            }

        }
                 
        /// <summary>
        /// this function will create user pref for the grid if it does not exist in the database
        /// </summary>
        /// 
        private string GetOrCreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            bool IsUserPrefExist = false;
            try
            {
                Riskmaster.Common.GridCommonFunctions.GetUserHeaderAndPreference(ref sJsonUserPref, p_iUserID, m_iClientId, p_sPageName, p_iDSNId.ToString(), p_sGridId);
                if (String.IsNullOrEmpty(sJsonUserPref))
                    IsUserPrefExist = false;
                else
                    IsUserPrefExist = true;

                if (!IsUserPrefExist)
                {
                    return CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
                }
                else //set cell template on first column to make it hyper link.
                {
                    //get value of show all checkbox from user pref for the first time only
                    Dictionary<string, object> l_dict = new Dictionary<string, object>();
                    JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(sJsonUserPref)), l_dict);

                    //string strCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><a onclick=parent.MDIShowScreen(\'{{row.getProperty(\'claim_id\')}}\',\'claim\');return false; href=\"#\">{{row.getProperty(col.field)}}</a></div>";
                    string strHyperLinkCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=parent.MDIShowScreen(\'{{row.getProperty(\'claim_id\')}}\',\'claim\');return false; href=\"#\">{{row.getProperty(col.field)}}</a></div>";            ////rma-10914
                    List<Dictionary<string, object>> l_dict2 = new List<Dictionary<string, object>>();
                    JsonConvert.PopulateObject(Convert.ToString(JsonConvert.DeserializeObject(l_dict["colDef"].ToString())), l_dict2);
                    l_dict2.FirstOrDefault(m => Convert.ToBoolean(m["visible"]) == true)["cellTemplate"] = strHyperLinkCellTemplate;//JIRA 11487 Column ordering 
                    //l_dict2[0]["cellTemplate"] = strHyperLinkCellTemplate;
                    l_dict["colDef"] = l_dict2;
                    sJsonUserPref = JsonConvert.SerializeObject(l_dict); //Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
                }
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("PendingClaims.GetOrCreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }

        /// <summary>
        /// This function is used to Create default User Pref when it does not exist in database
        /// </summary>
        /// <param name="p_iUserID"></param>
        /// <param name="p_sPageName"></param>
        /// <param name="p_iDSNId"></param>
        /// <param name="p_sGridId"></param>
        /// <returns></returns>
        private string CreateUserPreference(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            try
            {
                //if there are multiple grids on the page, each grid can have different user pref.                
                if (p_sGridId == "gridPenClaims")
                    sJsonUserPref = CreateUserPreferenceForPenClaims(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("PendingClaims.CreateUserPreference.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;

        }

        private string CreateUserPreferenceForPenClaims(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = string.Empty;
            SysSettings objSysSettings;
            List<GridColumnDefHelper> lstColDef;
            GridPreference oGridPref;
            Dictionary<string, object> dicAdditionalUserPref = new Dictionary<string, object>();
            string sPageID = "0";
            try
            {   //add hyper link to first column
              // string strCellTemplate =        "<div class=\"ngCellText\" ng-class=\"col.colIndex()\"><a onclick=parent.MDIShowScreen(\'{{row.getProperty(\'claim_id\')}}\',\'claim\');return false; href=\"#\">{{row.getProperty(col.field)}}</a></div>";
                string strHyperLinkCellTemplate = "<div class=\"ngCellText\" ng-class=\"col.colIndex()\" title=\"{{COL_FIELD}}\" ><a onclick=parent.MDIShowScreen(\'{{row.getProperty(\'claim_id\')}}\',\'claim\');return false; href=\"#\">{{row.getProperty(col.field)}}</a></div>";            ////rma-10914
                objSysSettings = new SysSettings(m_ConnectionString, m_iClientId);
                sPageID = Globalization.GetPageId(p_sPageName, m_iClientId);
                lstColDef = new List<GridColumnDefHelper>();
                
                //By Default 5 visible columns
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "claim_number", p_sdisplayName: getMLHeaderText("lblClaimNumber", sPageID), p_sCellTemplate: strHyperLinkCellTemplate));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "claimant", p_sdisplayName: getMLHeaderText("lblClaimant", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "insured", p_sdisplayName: getMLHeaderText("lblInsured", sPageID)));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "net_incurred", p_sdisplayName: getMLHeaderText("lblNetIncurred", sPageID), p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.NumericSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "net_incurred.DbValue", p_sdisplayName: "net_incurred.DbValue", p_bvisible: false,  p_bAlwaysInvisibleOnColumnMenu: true));//rkotak:custom sorting
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "policy_name", p_sdisplayName: getMLHeaderText("lblPolicyName", sPageID)));

                //By Default invisible columns
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "event_number", p_sdisplayName: getMLHeaderText("lblEventNumber", sPageID), p_bvisible: false));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "date_of_event", p_sdisplayName: getMLHeaderText("lblDateOfEvent", sPageID), p_bvisible: false, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "date_of_event.DbValue", p_sdisplayName: "date_of_event.DbValue", p_bvisible: false,  p_bAlwaysInvisibleOnColumnMenu: true));//rkotak:custom sorting
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "date_of_claim", p_sdisplayName: getMLHeaderText("lblDateOfClaim", sPageID), p_bvisible: false, p_Enum_ClientSideSortFunctions: GridCommonFunctions.ClientSideSortFunctions.DateTimeSortingFunction));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "date_of_claim.DbValue", p_sdisplayName: "date_of_claim.DbValue", p_bvisible: false,  p_bAlwaysInvisibleOnColumnMenu: true));//rkotak:custom sorting
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "claim_type", p_sdisplayName: getMLHeaderText("lblClaimType", sPageID), p_bvisible: false));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "claim_status", p_sdisplayName: getMLHeaderText("lblClaimStatus", sPageID), p_bvisible: false));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "lob", p_sdisplayName: getMLHeaderText("lblLOB", sPageID), p_bvisible: false));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "my_role", p_sdisplayName: getMLHeaderText("lblMyRole", sPageID), p_bvisible: false));
                lstColDef.Add(new GridColumnDefHelper(p_sfield: "claim_id.DbValue", p_sdisplayName: "claim_id.DbValue", p_bvisible: false, p_bAlwaysInvisibleOnColumnMenu: true));//rkotak:custom sorting
                oGridPref = new GridPreference();
                oGridPref.colDef = lstColDef;
                oGridPref.PageZise = "";
                oGridPref.SortColumn = new string[1] { "claim_id.DbValue" };
                oGridPref.SortDirection = new string[1] { "asc" };
                dicAdditionalUserPref["showAll"] = false;
                oGridPref.AdditionalUserPref = dicAdditionalUserPref;
                sJsonUserPref = Riskmaster.Common.GridCommonFunctions.CreateUserPrefJSON(oGridPref);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("PendingClaims.CreateUserPreferenceForPenClaims.Error", m_iClientId), objException);
            }
            finally
            {
                objSysSettings = null;
                lstColDef = null;
                oGridPref = null;
            }
            return sJsonUserPref;
        }

        /// <summary>
        /// This function is used to restore default user pref
        /// </summary>
        /// <param name="p_iUserID"></param>
        /// <param name="p_sPageName"></param>
        /// <param name="p_iDSNId"></param>
        /// <param name="p_sGridId"></param>
        /// <returns></returns>
       public string RestoreDefaultUserPref(int p_iUserID, string p_sPageName, int p_iDSNId, string p_sGridId)
        {
            string sJsonUserPref = "";
            try
            {
                if (Riskmaster.Common.GridCommonFunctions.DeleteUserPreference(p_iUserID, m_iClientId, p_sPageName, p_iDSNId, p_sGridId))
                    sJsonUserPref = CreateUserPreference(p_iUserID, p_sPageName, p_iDSNId, p_sGridId);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw new RMAppException(Globalization.GetString("PendingClaims.RestoreDefaultUserPref.Error", m_iClientId), objException);
            }
            finally
            {
            }
            return sJsonUserPref;
        }

        /// <summary>
        /// Provides Multilingual values
        /// </summary>
        /// <param name="key"></param>
        /// <param name="sPageID"></param>
        /// <returns></returns>
        private string getMLHeaderText(string key, string sPageID)
        {
            string sUserLangCode = "0";
            try
            {
                sUserLangCode = m_iLangCode.ToString();
                if (sUserLangCode == "0")
                {
                    //set base lang code as user lang code if it is not defined
                    sUserLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
                }
                return CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString(key, 0, sPageID, m_iClientId), sUserLangCode);
            }
            catch (PermissionViolationException objException)
            {
                throw objException;
            }
            catch (RMAppException objException)
            {
                throw objException;
            }
            catch (Exception objException)
            {
                throw objException;
            }
        }

        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
            p_objParentNode.AppendChild(p_objChildNode);
        }

        #endregion
    }
}