﻿
using System;
using System.Xml;
using System.Collections;
using Riskmaster.Common;
using System.IO;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;
using C1.C1PrintDocument;
using System.Drawing;

namespace Riskmaster.Application.SupportScreens
{
	///************************************************************** 
	///* $File				: EntityExposure.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 05-Oct-2005
	///* $Author			: Nikhil Garg
	///***************************************************************	
	/// <summary>	
	///	This class Rolls up the Exposure Records and Prints the RollUp Log Report	
	/// </summary>
    public class EntityExposure : IDisposable
	{
		#region "Constructor"
		/// <summary>
		/// Default constructor.
		/// </summary>
		public EntityExposure()
		{
		}
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public EntityExposure(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
		}
        private bool _isDisposed = false;
		~EntityExposure()
		{
            //Added by Shivendu
            Dispose();
		}
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;

                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                //Added by Shivendu
                if (m_objCache != null)
                {
                    m_objCache.Dispose();
                    m_objCache = null;
                }

                GC.SuppressFinalize(this);
            }
        }

		#endregion

		#region "Structure"
		internal struct TYPE_EXPOSURE_LOG
		{
			public bool Status;
			public string TABLE_NAME;
			public string ENTITY_NAME;
			public string START_DATE;
			public string END_DATE;
			public int NO_ENTITY;
			public int NO_TABLE;
		}
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable to store the Local Cache
		/// </summary>
		private LocalCache m_objCache = null;
		/// <summary>
		/// Arraylist used for producing Log Report
		/// </summary>
		private ArrayList m_arrLog=new ArrayList();

        private int m_iClientId = 0;
		#endregion

		#region "Public Functions"

        /// Name		: RollUp
        /// Author		: Nikhil Kumar Garg
        /// Date Created: 05-Oct-2005
        ///*************************************************************************
        /// Amendment History
        ///*************************************************************************
        /// Date Amended   *   Amendment   *								Author
        ///*************************************************************************
        /// <summary>
        /// Rolls up the Exposure Information and send back the Exposure Roll up Log Report
        /// </summary>
        /// <param name="p_objErrOut">Errors Collection</param>
        /// <param name="p_sPdfDocPath">PDF Document path</param>
		/// <param name="p_iCompleted">No. of records rolled up successfully</param>
        /// <param name="p_iFailed">No. of records where rolling up failed</param>
        /// <returns>true/false. whether successful or not</returns>
		public bool RollUp(ref string p_sPdfDocPath, ref int p_iCompleted, ref int p_iFailed)//Shruti for 11658
		{
			string sSQL=string.Empty;
			int iTotalFailed=0;
			int iTotalGood=0;
			int iCurrentEntityTable=0;
			int iLevel=0;
			int iFailedTableCount = 0;
			int iTableCount = 0;
			string sTableName=string.Empty;
			string sAlias=" ";
			int iCount=0;
			string sEntityName=string.Empty;
			int iEntityID=0;
			string sStartDate=string.Empty;
			string sEndDate=string.Empty;
			int iResult=0;
			bool bRecordsFound=false;
			XmlDocument objXML=new XmlDocument();

			this.Initialize();

			DbConnection objConn=null;
			DbReader objReader=null;

			sSQL = "DELETE FROM ENTITY_EXPOSURE WHERE USER_GENERATD_FLAG = 0";
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);			
				objConn.Open();
				objConn.ExecuteNonQuery(sSQL);
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				//todo:entry in global.resx
				throw new RMAppException(Globalization.GetString("EntityExposure.RollUp.Error", m_iClientId), p_objEx);//rkaur27
			}
			finally
			{
				objConn.Close();
				objConn.Dispose();
			}

			iCurrentEntityTable=m_objCache.GetTableId("DEPARTMENT");

			for (iLevel=2;iLevel<=8;iLevel++)
			{
				iFailedTableCount=0;
				iTableCount=0;

				// Get next level up in org. hierarchy
				iCurrentEntityTable = GetParentTableID(iCurrentEntityTable);

				sTableName = m_objCache.GetTableName(iCurrentEntityTable);

				sSQL = "SELECT PARENT.ENTITY_ID, PARENT.LAST_NAME, START_DATE, END_DATE,";
				sSQL = sSQL + "   SUM(NO_OF_EMPLOYEES) " + sAlias + " NOE, SUM(NO_OF_WORK_HOURS) " + sAlias + " NOWH, SUM(PAYROLL_AMOUNT) " + sAlias + " PA,";
				sSQL = sSQL + "   SUM(ASSET_VALUE) " + sAlias + " AV, SUM(SQUARE_FOOTAGE) " + sAlias + " SF, SUM(VEHICLE_COUNT) " + sAlias + " VC,";
				sSQL = sSQL + "   SUM(TOTAL_REVENUE) " + sAlias + " TR, SUM(OTHER_BASE) " + sAlias + " OB, SUM(RISK_MGMT_OVERHEAD) " + sAlias + " RMO";
				sSQL = sSQL + " FROM ENTITY, ENTITY_EXPOSURE, ENTITY " + sAlias + " PARENT";
				sSQL = sSQL + " WHERE ENTITY.ENTITY_ID = ENTITY_EXPOSURE.ENTITY_ID";

				sSQL = sSQL + "   AND ENTITY.PARENT_EID = PARENT.ENTITY_ID";
				sSQL = sSQL + "   AND PARENT.ENTITY_TABLE_ID = " + iCurrentEntityTable;
				sSQL = sSQL + " GROUP BY PARENT.ENTITY_ID, PARENT.LAST_NAME, START_DATE, END_DATE";
				sSQL = sSQL + " ORDER BY PARENT.ENTITY_ID, PARENT.LAST_NAME, START_DATE, END_DATE";

				objReader=DbFactory.GetDbReader(m_sConnectionString, sSQL);

				iCount=1;
				//Loop for all distinct entities on this level
				while(objReader.Read())
				{
					bRecordsFound=true;
					sEntityName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));

					//Get key info from user entered rollup record
                    iEntityID = Conversion.ConvertObjToInt(objReader.GetValue("ENTITY_ID"), m_iClientId);
					sStartDate = Conversion.ConvertObjToStr(objReader.GetValue("START_DATE"));
					sEndDate = Conversion.ConvertObjToStr(objReader.GetValue("END_DATE"));

					if (ExposureDatesOK(iEntityID, -1, sStartDate, sEndDate))
					{
						iTableCount = iTableCount + 1;

						sSQL = "INSERT INTO ENTITY_EXPOSURE(EXPOSURE_ROW_ID,ENTITY_ID,START_DATE,END_DATE,NO_OF_EMPLOYEES,";
						sSQL = sSQL + "NO_OF_WORK_HOURS,PAYROLL_AMOUNT,ASSET_VALUE,SQUARE_FOOTAGE,VEHICLE_COUNT,TOTAL_REVENUE,";
						sSQL = sSQL + "OTHER_BASE,RISK_MGMT_OVERHEAD,USER_GENERATD_FLAG) VALUES (";
                        sSQL = sSQL + Utilities.GetNextUID(m_sConnectionString, "ENTITY_EXPOSURE", m_iClientId) + "," + iEntityID + ",";
						sSQL = sSQL + "'" + sStartDate + "'" + "," + "'" + sEndDate + "'" + ",";
                        sSQL = sSQL + Conversion.ConvertObjToInt(objReader.GetValue("NOE"), m_iClientId).ToString() + "," + Conversion.ConvertObjToInt(objReader.GetValue("NOWH"), m_iClientId).ToString() + ",";
                        sSQL = sSQL + Conversion.ConvertObjToDouble(objReader.GetValue("PA"), m_iClientId).ToString() + "," + Conversion.ConvertObjToDouble(objReader.GetValue("AV"), m_iClientId).ToString() + ",";
                        sSQL = sSQL + Conversion.ConvertObjToInt(objReader.GetValue("SF"), m_iClientId).ToString() + "," + Conversion.ConvertObjToInt(objReader.GetValue("VC"), m_iClientId).ToString() + ",";
                        sSQL = sSQL + Conversion.ConvertObjToDouble(objReader.GetValue("TR"), m_iClientId).ToString() + "," + Conversion.ConvertObjToDouble(objReader.GetValue("OB"), m_iClientId).ToString() + ",";
                        sSQL = sSQL + Conversion.ConvertObjToDouble(objReader.GetValue("RMO"), m_iClientId).ToString() + ",0)";

						objConn = DbFactory.GetDbConnection(m_sConnectionString);			
						objConn.Open();
						iResult=objConn.ExecuteNonQuery(sSQL);
						objConn.Close();
						objConn.Dispose();
					}
					else
					{
						iFailedTableCount = iFailedTableCount + 1;
						AddExposureLog(false, sTableName, sEntityName, Conversion.GetDBDateFormat(sStartDate, "d"), Conversion.GetDBDateFormat(sEndDate, "d"), 0, 0);
					}
					iCount = iCount + 1;
				}
				objReader.Close();
				objReader.Dispose();
				if (bRecordsFound)
					AddExposureLog(true, sTableName, "", "", "", iTableCount, 0);
				else
					AddExposureLog(false, sTableName, "", "", "", 0, 0);

				iTotalFailed = iTotalFailed + iFailedTableCount;
				iTotalGood = iTotalGood + iTableCount;
			}

			p_sPdfDocPath=PrintRollUpReport(m_arrLog);
            //Shruti for 11658
            p_iCompleted = iTotalGood;
            p_iFailed = iTotalFailed;
			//add popup message for display
			//p_objErrOut.Add(Globalization.GetString("EntityExposure.RollUpComplete"),
				//String.Format(Globalization.GetString("EntityExposure.RollUp.ExposureRollUpComplete"), iTotalGood,iTotalFailed),
				//BusinessAdaptorErrorType.PopupMessage);
            //Shruti for 11658 ends

			return true;
		}

		#endregion

		#region "Private Functions"
		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword , m_iClientId);//rkaur27	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_objCache=new LocalCache(m_sConnectionString,m_iClientId);
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EntityExposure.Initialize.Error", m_iClientId) , p_objEx );//rkaur27				
			}			
		}

		/// <summary>
		/// Whether Exposure Dates are in the range or not
		/// </summary>
		private bool ExposureDatesOK(int p_iEntityID, int p_iExposureID, string p_sStartDate, string p_sEndDate)
		{
			bool bReturnValue=true;
			string sSQLTmp=string.Empty;
			DbReader objReader=null;
			int iResult=0;

			if (p_iEntityID != 0 &&  p_iExposureID != 0)
			{
				sSQLTmp = "SELECT COUNT(*) FROM ENTITY_EXPOSURE WHERE ENTITY_ID = " + p_iEntityID.ToString();
				sSQLTmp = sSQLTmp + " AND EXPOSURE_ROW_ID <> " + p_iExposureID.ToString();
				sSQLTmp = sSQLTmp + " AND (START_DATE BETWEEN '" + p_sStartDate + "' AND '" + p_sEndDate + "'";
				sSQLTmp = sSQLTmp + " OR END_DATE BETWEEN '" + p_sStartDate + "' AND '" + p_sEndDate + "')";

				try
				{
					objReader=DbFactory.GetDbReader(m_sConnectionString, sSQLTmp);

					if (objReader.Read())
						iResult=Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);

					if (iResult>0)
						bReturnValue=false;
				}
				catch(RMAppException p_objEx)
				{
					throw p_objEx ;
				}
				catch(Exception p_objEx)
				{
					throw new RMAppException(Globalization.GetString("EntityExposure.ExposureDatesOK.Error", m_iClientId), p_objEx);//rkaur27
				}
				finally
				{
					//Modified By SHivendu
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
				}
			}
			return bReturnValue;
		}

		/// <summary>
		/// Gets Parent Table Id for the Org Hierarchy Level
		/// </summary>
		private int GetParentTableID(int p_iCurrentEntityTable)
		{
			DbReader objReader=null;
			string sSQL=string.Empty;
			int iParentTableId=0;

			if (p_iCurrentEntityTable != 0)
			{
				sSQL = "SELECT RELATED_TABLE_ID FROM GLOSSARY WHERE TABLE_ID = " + p_iCurrentEntityTable;		

				try
				{
					objReader=DbFactory.GetDbReader(m_sConnectionString, sSQL);
					if(objReader.Read())
						iParentTableId=Conversion.ConvertObjToInt(objReader.GetValue("RELATED_TABLE_ID"), m_iClientId);
				}
				catch(RMAppException p_objEx)
				{
					throw p_objEx ;
				}
				catch(Exception p_objEx)
				{
					throw new RMAppException(Globalization.GetString("EntityExposure.GetParentTableID.Error", m_iClientId), p_objEx);//rkaur27
				}
				finally
				{
                    //Modified By SHivendu
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
				}
			}
			return iParentTableId;        
		}

		/// <summary>
		/// Add an Entry to Exposure Log Array used for generating report
		/// </summary>
		private void AddExposureLog(bool p_bStatus, string p_sTN, string p_sEN, string p_sSD, string p_sED, int p_iTC, int p_iEC)
		{
			TYPE_EXPOSURE_LOG objLog=new TYPE_EXPOSURE_LOG();
			objLog.Status = p_bStatus;
			objLog.TABLE_NAME = p_sTN;
			objLog.ENTITY_NAME = p_sEN;
			objLog.START_DATE = p_sSD;
			objLog.END_DATE = p_sED;
			objLog.NO_TABLE = p_iTC;
			objLog.NO_ENTITY = p_iEC;
			m_arrLog.Add(objLog);
		}
		/// <summary>
		/// Generates a unique file Name 
		/// </summary>
		private string GetFileName(string p_sFileName, string p_sFileExt)
		{
			string sFileName=string.Empty;	//for holding file name

			sFileName=p_sFileName + "_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + 
				DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() +
				DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "." + p_sFileExt;

			return sFileName;
		}

		#endregion

		#region Print Roll Up Log
		/// <summary>
		/// Event when a new page is started in the report
		/// </summary>
		private void NewPageRollUp( C1PrintDocument objPrintDoc , NewPageStartedEventArgs p_objArgs )
		{			
			RenderTable objHeaderTable = null ;
			RenderTable objFooterTable = null ;
			string sGapString = "                                                                  ";

			try
			{
				#region Print Header
				objHeaderTable = new RenderTable( objPrintDoc );	
				objHeaderTable.Style.Borders.AllEmpty = false;
				objHeaderTable.Style.Borders.Left = new LineDef( Color.Black , 0 );
				objHeaderTable.Style.Borders.Right = new LineDef( Color.Black , 0 );
				objHeaderTable.Style.Borders.Bottom = new LineDef( Color.Black , 0 );
				objHeaderTable.Style.Borders.Top = new LineDef( Color.Black , 0 );

				objHeaderTable.StyleTableCell.BorderTableHorz.Empty = true;
				objHeaderTable.StyleTableCell.BorderTableVert.Empty = true;							
				objHeaderTable.StyleTableCell.BorderTableHorz.WidthPt = 0 ;
				objHeaderTable.StyleTableCell.BorderTableVert.WidthPt = 0 ;

				objHeaderTable.Columns.AddSome(1);
				objHeaderTable.Body.Rows.AddSome(3);
				objHeaderTable.Columns[0].StyleTableCell.TextAlignHorz = AlignHorzEnum.Center;				
				objHeaderTable.Columns[0].Width = 11000 ;
				
			
				objHeaderTable.Body.Cell( 0, 0).StyleTableCell.Font = new Font( "Arial" , 10 ,FontStyle.Bold );									
				objHeaderTable.Body.Cell( 0, 0).RenderText.Text = "Page " + objPrintDoc.PageCount + sGapString + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
				
				objHeaderTable.Body.Cell( 1, 0).StyleTableCell.Font = new Font( "Arial" , 10 ,FontStyle.Bold );									
				objHeaderTable.Body.Cell( 1, 0).RenderText.Text = "Exposure Roll Up Log" ;

				if( objPrintDoc.CurrentPage == 1 )
					objHeaderTable.Body.Cell( 2, 0).RenderText.Text = "Successfull Roll Log" ;
				else
					objHeaderTable.Body.Cell( 2, 0).RenderText.Text = "Failed Roll Log" ;
			
				objPrintDoc.PageHeader.RenderObject = objHeaderTable;		
				#endregion 

				#region Print Footer
				objFooterTable = new RenderTable( objPrintDoc );	
				objFooterTable.Style.Borders.AllEmpty = false;
				objFooterTable.Style.Borders.Left = new LineDef( Color.Black , 0 );
				objFooterTable.Style.Borders.Right = new LineDef( Color.Black , 0 );
				objFooterTable.Style.Borders.Bottom = new LineDef( Color.Black , 0 );
				objFooterTable.Style.Borders.Top = new LineDef( Color.Black , 0 );

				objFooterTable.StyleTableCell.BorderTableHorz.Empty = true;
				objFooterTable.StyleTableCell.BorderTableVert.Empty = true;							
				objFooterTable.StyleTableCell.BorderTableHorz.WidthPt = 0 ;
				objFooterTable.StyleTableCell.BorderTableVert.WidthPt = 0 ;

				objFooterTable.Columns.AddSome(1);
				objFooterTable.Body.Rows.AddSome(1);
				objFooterTable.Columns[0].StyleTableCell.TextAlignHorz = AlignHorzEnum.Center;				
				objFooterTable.Columns[0].Width = 11000 ;
				
			
				objFooterTable.Body.Cell( 0, 0).StyleTableCell.Font = new Font( "Arial" , 10 ,FontStyle.Bold );									
				if( objPrintDoc.CurrentPage == 1 )
					objFooterTable.Body.Cell( 0, 0).RenderText.Text = " [Continued]";
				else
					objFooterTable.Body.Cell( 0, 0).RenderText.Text = "[End Of Report]";
				
			
				objPrintDoc.PageFooter.RenderObject = objFooterTable;		
				#endregion 
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EntityExposure.NewPageRollUp.Error", m_iClientId) , p_objEx );//rkaur27				
			}
			finally
			{
				objHeaderTable = null ;
				objFooterTable = null ;
			}

		}

		/// <summary>
		/// Prints the Rollup Log Report
		/// </summary>
		private string PrintRollUpReport(ArrayList p_arrlstSuccessRollUp)
		{
			TYPE_EXPOSURE_LOG structRollup ;
			C1PrintDocument objPrintDoc = null ;
			RenderTable objTable = null ;
			TableColumn objTableColumn = null ;

            string sRollUpPDFPath = string.Empty;
			int iIndex = 0 ;
			int iRowIndex = 0 ;

			try
			{
                sRollUpPDFPath = RMConfigurator.TempPath;
								

				#region Initialize document and set the layout properties.
				objPrintDoc = new C1PrintDocument();
				objPrintDoc.DefaultUnit = UnitTypeEnum.Twip ;
				objPrintDoc.PageSettings.Margins.Top = 45 ;
				objPrintDoc.PageSettings.Margins.Left = 50 ;
				objPrintDoc.PageSettings.Margins.Bottom = 40 ;
				objPrintDoc.PageSettings.Margins.Right = 50 ;	
				objPrintDoc.PageSettings.Landscape = false ;
				objPrintDoc.NewPageStarted += new NewPageStartedEventHandler( this.NewPageRollUp );			
				objPrintDoc.StartDoc();

				objTable = new RenderTable( objPrintDoc );
				objPrintDoc.Style.Borders.AllEmpty = true;
				objTable.Body.StyleTableCell.Font = new Font( "Courier New" , 10 );
			
				objTable.Columns.AddSome( 2 );
				objTableColumn = new TableColumn( objPrintDoc );
				objTableColumn.Width = 2700 ;
				objTable.Columns[0] = objTableColumn ;
				objTableColumn.Width = 8100 ;
				objTable.Columns[1] = objTableColumn ;								 					
			
				objTable.Body.AutoHeight = true ;

				objTable.StyleTableCell.Borders.Bottom = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Top = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Left = new LineDef( Color.Black , 0 );
				objTable.StyleTableCell.Borders.Right = new LineDef( Color.Black , 0 );
					
				objTable.Style.Borders.Left = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Right = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Bottom = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Top = new LineDef( Color.Black , 0 );
			
				objTable.StyleTableCell.Padding.Right = 0 ;
				objTable.StyleTableCell.Padding.Top = 0 ;
				objTable.StyleTableCell.Padding.Bottom = 0 ;			

				objTable.StyleTableCell.BorderTableHorz.Empty = false;
				objTable.StyleTableCell.BorderTableVert.Empty = false;	
		
				objTable.StyleTableCell.BorderTableHorz.WidthPt = 0 ;
				objTable.StyleTableCell.BorderTableVert.WidthPt = 0 ;
				
				
				#endregion 											
			
				#region Print Success RollUp

				objTable = new RenderTable( objPrintDoc );
				objPrintDoc.Style.Borders.AllEmpty = true;
				objTable.Body.StyleTableCell.Font = new Font( "Courier New" , 10 );
			
				objTable.Columns.AddSome( 2 );
				objTableColumn = new TableColumn( objPrintDoc );
				objTableColumn.Width = 2700 ;
				objTable.Columns[0] = objTableColumn ;
				objTableColumn.Width = 8100 ;
				objTable.Columns[1] = objTableColumn ;								 					
			
				objTable.Body.AutoHeight = true ;

				objTable.StyleTableCell.Borders.Bottom = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Top = new LineDef( Color.Black , 0 );			
				objTable.StyleTableCell.Borders.Left = new LineDef( Color.Black , 0 );
				objTable.StyleTableCell.Borders.Right = new LineDef( Color.Black , 0 );
					
				objTable.Style.Borders.Left = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Right = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Bottom = new LineDef( Color.Black , 0 );
				objTable.Style.Borders.Top = new LineDef( Color.Black , 0 );
			
				objTable.StyleTableCell.Padding.Right = 0 ;
				objTable.StyleTableCell.Padding.Top = 0 ;
				objTable.StyleTableCell.Padding.Bottom = 0 ;			

				objTable.StyleTableCell.BorderTableHorz.Empty = false;
				objTable.StyleTableCell.BorderTableVert.Empty = false;	
		
				objTable.StyleTableCell.BorderTableHorz.WidthPt = 0 ;

				objTable.StyleTableCell.BorderTableVert.WidthPt = 0 ;
				objTable.Body.Rows.AddSome( 1 );	
				objTable.Body.Cell( 0 , 0 ).RenderText.Text = "Table Name";
				objTable.Body.Cell( 0 , 1 ).RenderText.Text = "Total Count";

				objTable.Body.Rows.AddSome( 1 );

				iRowIndex = 1 ;
				objTable.Body.Cell( iRowIndex , 0 ).RenderText.Text = " ";
				objTable.Body.Cell( iRowIndex , 1 ).RenderText.Text = " ";

				// Set cell text.
				for( iIndex = 0 ; iIndex < p_arrlstSuccessRollUp.Count ; iIndex++ )
				{
					structRollup = ( TYPE_EXPOSURE_LOG ) p_arrlstSuccessRollUp[iIndex] ;
					if( structRollup.Status )
					{
						objTable.Body.Rows.AddSome( 1 );					
						iRowIndex++ ;
						objTable.Body.Cell( iRowIndex , 0 ).RenderText.Text = structRollup.TABLE_NAME ;
						objTable.Body.Cell( iRowIndex , 1 ).RenderText.Text = structRollup.NO_TABLE.ToString();
					}
					
				}	
				objPrintDoc.RenderBlock( objTable );
				#endregion 				

				#region Print Fail Rollup
		
				if( iRowIndex == 8 )
				{
					objTable = new RenderTable( objPrintDoc );
					objPrintDoc.Style.Borders.AllEmpty = true;
					objTable.Body.StyleTableCell.Font = new Font( "Courier New" , 10 );
			
					objTable.Columns.AddSome( 4 );
					objTableColumn = new TableColumn( objPrintDoc );
					objTableColumn.Width = 2700 ;
					objTable.Columns[0] = objTableColumn ;
					objTableColumn.Width = 4000 ;
					objTable.Columns[1] = objTableColumn ;	
					objTableColumn.Width = 2700 ;
					objTable.Columns[2] = objTableColumn ;
					objTableColumn.Width = 2700 ;
					objTable.Columns[3] = objTableColumn ;
							
					objTable.Body.AutoHeight = true ;

					objTable.StyleTableCell.Borders.Bottom = new LineDef( Color.Black , 0 );			
					objTable.StyleTableCell.Borders.Top = new LineDef( Color.Black , 0 );			
					objTable.StyleTableCell.Borders.Left = new LineDef( Color.Black , 0 );
					objTable.StyleTableCell.Borders.Right = new LineDef( Color.Black , 0 );
					
					objTable.Style.Borders.Left = new LineDef( Color.Black , 0 );
					objTable.Style.Borders.Right = new LineDef( Color.Black , 0 );
					objTable.Style.Borders.Bottom = new LineDef( Color.Black , 0 );
					objTable.Style.Borders.Top = new LineDef( Color.Black , 0 );
			
					objTable.StyleTableCell.Padding.Right = 0 ;
					objTable.StyleTableCell.Padding.Top = 0 ;
					objTable.StyleTableCell.Padding.Bottom = 0 ;			

					objTable.StyleTableCell.BorderTableHorz.Empty = false;
					objTable.StyleTableCell.BorderTableVert.Empty = false;	
		
					objTable.StyleTableCell.BorderTableHorz.WidthPt = 0 ;
					objTable.StyleTableCell.BorderTableVert.WidthPt = 0 ;
					objTable.Body.Rows.AddSome( 1 );	
					objTable.Body.Cell( 0 , 0 ).RenderText.Text = "Table Name";
					objTable.Body.Cell( 0 , 1 ).RenderText.Text = "Entity Name";
					objTable.Body.Cell( 0 , 2 ).RenderText.Text = "Start Date";
					objTable.Body.Cell( 0 , 3 ).RenderText.Text = "End Date";
					objPrintDoc.NewPage();

					objTable.Body.Rows.AddSome( 1 );

					iRowIndex = 1 ;
					objTable.Body.Cell( iRowIndex , 0 ).RenderText.Text = " ";
					objTable.Body.Cell( iRowIndex , 1 ).RenderText.Text = " ";

					for( iIndex = 0 ; iIndex < p_arrlstSuccessRollUp.Count ; iIndex++ )
					{
						structRollup = ( TYPE_EXPOSURE_LOG ) p_arrlstSuccessRollUp[iIndex] ;
						if( !structRollup.Status )
						{
							objTable.Body.Rows.AddSome( 1 );					
							iRowIndex++ ;
							objTable.Body.Cell( iRowIndex , 0 ).RenderText.Text = structRollup.TABLE_NAME ;
							objTable.Body.Cell( iRowIndex , 1 ).RenderText.Text = structRollup.ENTITY_NAME;
							objTable.Body.Cell( iRowIndex , 2 ).RenderText.Text = structRollup.START_DATE;
							objTable.Body.Cell( iRowIndex , 3 ).RenderText.Text = structRollup.END_DATE;
						}
					
					}
					objPrintDoc.RenderBlock( objTable );
				}
				#endregion 
				
				// End doc.
				objPrintDoc.EndDoc();
			
				// Save the PDF.
				sRollUpPDFPath += GetFileName("RollUpReport","pdf");		// + Conversion.ToDbDate(DateTime.Now) + AppDomain.GetCurrentThreadId() + ".pdf" ;
				objPrintDoc.ExportToPDF( sRollUpPDFPath ,false );
				return( sRollUpPDFPath );
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EntityExposure.PrintRollUpReport.Error", m_iClientId) , p_objEx );//rkaur27				
			}
			finally
			{
				if( objPrintDoc != null )
				{
					objPrintDoc.Dispose();
					objPrintDoc = null ;
				}
				if( objTable != null )
				{
					objTable.Dispose();
					objTable = null ;
				}
				if( objTableColumn != null )
				{
					objTableColumn.Dispose();
					objTableColumn = null ;
				}								 				
			}
		}
		#endregion 
	}
}
