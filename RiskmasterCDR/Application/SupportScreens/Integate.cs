﻿using System;
using System.Collections;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.SupportScreens
{
    public class Integate
	{
        private int m_iClientId = 0;
        public Integate( int p_iClientId)
		{
            m_iClientId = p_iClientId;
		}
        public XmlDocument GetActualPageNameWithParams(XmlDocument requestParams, string sessionid)
		{
            Hashtable request = new Hashtable();
            XmlDocument objXml = new XmlDocument();
            int id = 0;
            string sFDMScreenName = string.Empty;
            bool bIsFDMScreen = false;
            try
            {
                string servername = RMConfigurationManager.GetAppSetting("CompatibilityWebServer");
                string querystring = "";
                foreach (XmlNode nd in requestParams.SelectNodes("//parameters/parameter"))
                {
                    string name = nd.SelectSingleNode("name").InnerText;
                    string value = nd.SelectSingleNode("value").InnerText;
                    if (name.Trim().ToLower() == "screen")
                    {
                        id = Conversion.ConvertStrToInteger(value);
                        requestParams.SelectSingleNode("//parameters").RemoveChild(nd);
                    }
                    if (name.Trim().ToLower() == "pg")
                    {
                        requestParams.SelectSingleNode("//parameters").RemoveChild(nd);
                    }
                    if (name.Trim().ToLower() == "fdmscreen")
                    {
                        bIsFDMScreen = true;
                        sFDMScreenName = value;
                    }
                }
                XmlElement el = requestParams.CreateElement("parameter");
                if(!bIsFDMScreen)
                {
                    string param = String.Format("<name>sesid</name><value>{0}</value>", sessionid);
                    el.InnerXml = param;
                    requestParams.SelectSingleNode("//parameters").AppendChild(el);

                    switch (id)
                    {
                        case 1:
                            servername += "Document.mvc/Index";
                            break;
                        case 2:
                            servername += "Document.mvc/Download";
                            break;
                        case 200:
                            servername += "SecurityManagement.mvc/Index";
                            break;
                        default:
                            break;
                    }

                }
                else
                {
                    string param = String.Format("<name>SessionID</name><value>{0}</value>", sessionid);
                    el.InnerXml = param;
                    requestParams.SelectSingleNode("//parameters").AppendChild(el);

                    switch (sFDMScreenName.ToLower())
                    {
                        case "event":
                            servername += "Event.aspx";
                            break;
                        case "vehicle":
                            servername += "vehicle.aspx";
                            break;
                        default:
                            break;
                    }
                }
                servername = servername + querystring;
                el = objXml.CreateElement("Integrate");
                XmlElement actualel = objXml.CreateElement("PageToRender");
                actualel.InnerText = servername;
                el.AppendChild(actualel);
                XmlElement actualquerystrings= objXml.CreateElement("Querystring");
                actualquerystrings.InnerXml = requestParams.SelectSingleNode("//parameters").OuterXml;
                el.AppendChild(actualquerystrings);
                objXml.AppendChild(el);        
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Integate.GetActualPageNameWithParams.Error", m_iClientId), p_objEx);//rkaur27
            }
            return objXml;
        }
    }
}
