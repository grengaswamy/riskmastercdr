using System;
using System.Data;
using System.Xml;
using System.Collections;
using Riskmaster.Common;
using System.IO;
using Riskmaster.ExceptionTypes;
using Riskmaster.Db;
using Riskmaster.DataModel;

namespace Riskmaster.Application.SupportScreens
{
	///************************************************************** 
	///* $File				: EntitySSNCheck.cs 
	///* $Revision			: 1.0.0.0 
	///* $Creation Date		: 02-Jan-2006
	///* $Author			: Nikhil Garg
	///***************************************************************	
	/// <summary>	
	///	This class checks for duplicate SSN while saving the Entity Record
	/// </summary>
    public class EntitySSNCheck : IDisposable
	{
		#region "Constructor"
		/// <summary>
		/// Default constructor.
		/// </summary>
		public EntitySSNCheck()
		{
		}
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public EntitySSNCheck(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId,int p_iClientId)
        {
            m_sUserName = p_sUserName;
            m_sPassword = p_sPassword;
            m_sDsnName = p_sDsnName;
            m_iUserId = p_iUserId;
            m_iClientId = p_iClientId;
            this.Initialize();
        }
        bool _isDisposed = false;
		~EntitySSNCheck()
		{
            Dispose();
		}
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;

                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                //Added by Shivendu
                if (m_objCache != null)
                {
                    m_objCache.Dispose();
                    m_objCache = null;
                }

                GC.SuppressFinalize(this);
            }
        }
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
		/// <summary>
		/// Private variable to store the Local Cache
		/// </summary>
		private LocalCache m_objCache = null;
		/// <summary>
		/// Private variable to store user id
		/// </summary>
		private int m_iUserId=0;
        private int m_iClientId = 0;
		#endregion

		#region "Private Functions"
		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory( m_sDsnName , m_sUserName , m_sPassword,m_iClientId );	
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_objCache=new LocalCache(m_sConnectionString,m_iClientId);
			}
			catch(DataModelException p_objEx)
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("EntitySSNCheck.Initialize.Error", m_iClientId) , p_objEx );//rkaur27				
			}			
		}


		private bool CheckUserOptionForIgnore()
		{
			string sSQL=string.Empty;
			DbReader objReader=null;
			string sXml=string.Empty;
			XmlDocument objPrefXml=new XmlDocument();
			XmlElement objElem=null;
			bool bIgnore=false;
            //try added by Shivendu
            try
            {
                sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + m_iUserId;
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);

                if (objReader != null)
                {
                    if (objReader.Read())
                    {
                        if ((Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != null) &&
                            (Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != ""))
                        {
                            sXml = Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                            objReader.Close();
                            objPrefXml.LoadXml(sXml);
                            objElem = (XmlElement)objPrefXml.SelectSingleNode("//IgnoreSSNChecking");

                            if (objElem != null)
                            {
                                if (objElem.InnerText == "1")
                                    bIgnore = true;
                            }
                        }
                    }
                }
                return bIgnore;
            }
            //Added by Shivendu.
            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();

                }
            }
		}

		#endregion

		#region "Public Functions"

		/// <summary>
		///		This function checks for duplicate SSN while saving the Entity Record
		/// </summary>
		/// <param name="p_sSSNId">SSN ID to check for duplicate</param>
		/// <returns>XML Document containing list of matching entities</returns>
		public XmlDocument CheckSSN(string p_sSSNId, string p_sEntityId)
		{
			string sSQL=string.Empty;			//SQL string 
			XmlDocument objDuplicate=null;		//XML Document for returing t Webservice 
			XmlElement objDocumentElement=null;	//Document element
			XmlElement objElem=null;			//Element for adding matching entities
			XmlCDataSection objCData=null;		//CData Element for adding Entity Name
			bool bFound=false;					//whether matching entity found or not
			DbReader objReader=null;			//for looking into the database
			
			objDuplicate = new XmlDocument();
			objDocumentElement=objDuplicate.CreateElement("Entities");
			objDuplicate.AppendChild(objDocumentElement);

			if (CheckUserOptionForIgnore())
			{
				objDocumentElement.SetAttribute("dupexists","0");
				return objDuplicate;
			}
			else
			{
                //pmittal5 01/04/10 Mits 18399 - While checking for Duplicate Tax Id, entities marked as Deleted should not be picked up. 
                //sSQL = "SELECT LAST_NAME, FIRST_NAME, ENTITY_ID FROM ENTITY WHERE TAX_ID='" 
                //    + p_sSSNId + "' AND ENTITY_ID <> " + p_sEntityId;
                sSQL = "SELECT LAST_NAME, FIRST_NAME, ENTITY_ID FROM ENTITY WHERE TAX_ID='"
                    + p_sSSNId + "' AND ENTITY_ID <> " + p_sEntityId + " AND DELETED_FLAG = 0 ";
				try
				{
					objReader=DbFactory.GetDbReader(m_sConnectionString, sSQL);
					while (objReader.Read())
					{
						bFound=true;
						objElem=objDuplicate.CreateElement("Entity");
						objElem.SetAttribute("id",Conversion.ConvertObjToStr(objReader.GetValue("ENTITY_ID")));
						if (Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"))!=string.Empty)
							objCData=objDuplicate.CreateCDataSection(Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")) + "," +
								Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME")));
						else
							objCData=objDuplicate.CreateCDataSection(Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME")));

						objElem.AppendChild(objCData);
						objDocumentElement.AppendChild(objElem);
					}
					objReader.Close();

					if (bFound)
						objDocumentElement.SetAttribute("dupexists","1");
					else
						objDocumentElement.SetAttribute("dupexists","0");

					return objDuplicate;
				}
				catch(RMAppException p_objEx)
				{
					throw p_objEx ;
				}
				catch(Exception p_objEx)
				{
					throw new RMAppException(Globalization.GetString("EntitySSNCheck.CheckSSN.Error", m_iClientId), p_objEx);//rkaur27
				}
				finally
				{
                    if (objReader != null)
                    {
                        objReader.Close();
                        objReader.Dispose();
                    }
					objCData=null;
					objElem=null;
					objDocumentElement=null;
					objDuplicate=null;
				}
			}
		}

		/// <summary>
		///		This function updates the user pref xml if user has opted for ignoring future warnings
		/// </summary>
		/// <param name="p_bIgnore">Whether toignore future warnings or not</param>
		/// <returns>Null</returns>
		public void IgnoreSSNChecking(bool p_bIgnore)
		{
			string sSQL=string.Empty;
			DbReader objReader=null;
			string sXml=string.Empty;
			XmlDocument objPrefXml=new XmlDocument();
			XmlElement objElem=null;
			DbConnection objConn=null;
            DbCommand objCmd = null;//Umesh
            DbParameter objParam = null;//Umesh

			sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + m_iUserId;
			objReader = DbFactory.GetDbReader(m_sConnectionString,sSQL);
            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objCmd = objConn.CreateCommand();
                objParam = objCmd.CreateParameter();
                objParam.Direction = ParameterDirection.Input;
                objParam.ParameterName = "XML";
                objParam.SourceColumn = "PREF_XML";
                objCmd.Parameters.Add(objParam);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }

			if (objReader != null)
			{
				if (objReader.Read())
				{
					if ((Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != null) && 
						(Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML")) != ""))
					{
						sXml = Conversion.ConvertObjToStr (objReader.GetValue("PREF_XML"));
						objReader.Close();
						objPrefXml.LoadXml (sXml);
						objElem = (XmlElement) objPrefXml.SelectSingleNode("//IgnoreSSNChecking");

						if (objElem == null)
						{
							objElem=objPrefXml.CreateElement("IgnoreSSNChecking");
							objPrefXml.SelectSingleNode("//setting").AppendChild(objElem);
						}

						if (p_bIgnore)
							objElem.InnerText="1";
						else
							objElem.InnerText="0";
                        objParam.Value = objPrefXml.OuterXml;
						sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID = " + m_iUserId;
					}
					else
					{
						sXml="<setting><IgnoreSSNChecking>";
						if (p_bIgnore)
							sXml=sXml+ "1"+"</IgnoreSSNChecking></setting>";
						else
							sXml=sXml+ "0"+"</IgnoreSSNChecking></setting>";
                        objParam.Value = sXml;
						sSQL = "UPDATE USER_PREF_XML SET PREF_XML=~XML~ WHERE USER_ID = " + m_iUserId;
					}
				}
                else
                {
                    sXml = "<setting><IgnoreSSNChecking>";
                    if (p_bIgnore)
                        sXml = sXml + "1" + "</IgnoreSSNChecking></setting>";
                    else
                        sXml = sXml + "0" + "</IgnoreSSNChecking></setting>";
                    objParam.Value = sXml;
                    sSQL = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + m_iUserId + ",~XML~)";
                }
			}
			else
			{
				sXml="<setting><IgnoreSSNChecking>";
				if (p_bIgnore)
					sXml=sXml+ "1"+"</IgnoreSSNChecking></setting>";
				else
					sXml=sXml+ "0"+"</IgnoreSSNChecking></setting>";
                objParam.Value = sXml;
				sSQL = "INSERT INTO USER_PREF_XML(USER_ID,PREF_XML) VALUES(" + m_iUserId + ",~XML~)";
			}

			try
			{
                //objConn = DbFactory.GetDbConnection(m_sConnectionString);
                //objConn.Open();
                //objConn.ExecuteNonQuery (sSQL);
                objCmd.CommandText = sSQL;
                objCmd.ExecuteNonQuery();
                objConn.Close();
                objConn.Dispose();
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("EntitySSNCheck.IgnoreSSNChecking.Error", m_iClientId), p_objEx);//rkaur27
			}
			finally
			{
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
				objElem=null;
				objPrefXml=null;
				if (objConn != null)
				{
					objConn.Close ();
					objConn.Dispose ();
				}
			}
		}

		#endregion
	}
}