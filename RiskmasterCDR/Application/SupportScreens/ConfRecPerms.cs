﻿using System;
using System.Xml;
using System.Collections;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.SupportScreens
{
    ///************************************************************** 
    ///* $File				: ConfRecPerms.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 03/24/2010
    ///* $Author			: Priya Mittal
    ///***************************************************************	
    /// <summary>	
    ///	This class lists the Users in User Permissions screen for a Confidential Record
    /// </summary>
    public class ConfRecPerms
    {
       	#region Constructor
		// Default Constructor
		public ConfRecPerms()
		{			
		}

		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
        /// <param name="p_sDsnId">DSN Id</param>
        /// <param name="p_sConnString">Connection String</param>
        /// <param name="p_iUserId">Login UserID</param>
        /// <param name="p_sSecurityConnString">Security Connection String</param>
        public ConfRecPerms(int p_sDsnId, string p_sConnString, int p_iUserId, string p_sSecurityConnString, int p_iClientId)
        {
            m_iDsnId = p_sDsnId;
            m_sConnectionString = p_sConnString;
            m_iUserId = p_iUserId;
            m_sSecurityConnString = p_sSecurityConnString;
            m_iClientId = p_iClientId;
        }
		#endregion

        #region Variable Declaration
        /// <summary>
        /// Private variable to store DSN Id
        /// </summary>
        private int m_iDsnId = 0;
        /// <summary>
        /// Private variable to store Connection String
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store Login User Id
        /// </summary>
        private int m_iUserId = 0;
        /// <summary>
        /// Private variable to store Security Connection String
        /// </summary>
        private string m_sSecurityConnString = "";
        private int m_iClientId = 0;
        #endregion

        #region Public Methods
        /// <summary>
        ///		This function creates list of Selected and Available Users in User Permissions screen 
        /// </summary>
        /// <param name="p_objXmlIn">XML Document containing Event Id</param>
        /// <returns>XML Document containing list of Selected and Available users</returns>
        public XmlDocument GetAvailablePermList(XmlDocument p_objXmlIn)
        {
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemChild = null;
            XmlElement objElemTemp = null;
            string sSQL = "";
            DbReader objReader = null;
            string sName = "";
            string sFName = "";
            string sLName = "";
            long lUID = 0;
            long lPrev = 0;
            long lDSNID = 0;
            string sSecDSN = string.Empty;
            String sDSN = string.Empty;
            ArrayList ArrSelectedUsers = null;
            long lEventId = 0;

            ArrSelectedUsers = new ArrayList();

            try
            {
                lDSNID = m_iDsnId;

                sSecDSN = m_sSecurityConnString;
                sDSN = m_sConnectionString;
                lEventId = Conversion.ConvertStrToLong(p_objXmlIn.SelectSingleNode("//EventID").InnerText);

                sSQL = " SELECT USER_ID FROM CONF_EVENT_PERM WHERE EVENT_ID=" + lEventId + " AND DELETED_FLAG =0";
                objReader = DbFactory.GetDbReader(sDSN, sSQL);
                while (objReader.Read())
                {
                    lUID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
                    ArrSelectedUsers.Add(lUID);
                }
                objReader.Close();

                if (ArrSelectedUsers.Count == 0)
                    ArrSelectedUsers.Add(m_iUserId);


                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("AvailableTemplatePerm");
                objDOM.AppendChild(objElemParent);
                objElemChild = objDOM.CreateElement("TemplatePermItems");
                objDOM.FirstChild.AppendChild(objElemChild);
                sSQL = "SELECT USER_DETAILS_TABLE.USER_ID,USER_TABLE.LAST_NAME,USER_TABLE.FIRST_NAME ";
                sSQL += " FROM USER_DETAILS_TABLE,USER_TABLE";
                sSQL += " WHERE USER_DETAILS_TABLE.USER_ID = USER_TABLE.USER_ID ";
                if (Conversion.ConvertLongToBool(lDSNID, m_iClientId))
                {
                    sSQL += "AND DSNID=" + lDSNID;
                    sSQL += " ORDER BY USER_TABLE.LAST_NAME";
                }
                objReader = DbFactory.GetDbReader(sSecDSN, sSQL);
                while (objReader.Read())
                {
                    lUID = Conversion.ConvertStrToLong(Conversion.ConvertObjToStr(objReader.GetValue("USER_ID")));
                    sLName = Conversion.ConvertObjToStr(objReader.GetValue("LAST_NAME"));
                    sFName = Conversion.ConvertObjToStr(objReader.GetValue("FIRST_NAME"));
                    if (sFName == "")
                    {
                        sName = sLName;
                    }
                    else
                    {
                        sName = sLName + ", " + sFName;
                    }
                    if (!sName.Trim().Equals("") && lUID != lPrev)
                    {
                        objElemTemp = objDOM.CreateElement("PermItem");
                        objElemTemp.SetAttribute("DisplayName", sName);
                        objElemTemp.SetAttribute("GroupId", "0");
                        objElemTemp.SetAttribute("IsGroup", "0");
                        objElemTemp.SetAttribute("UserId", lUID.ToString());
                    }
                    for (int iCount = 0; iCount < ArrSelectedUsers.Count; iCount++)
                    {
                        if (ArrSelectedUsers[iCount].ToString() == lUID.ToString())
                        {
                            objElemTemp.SetAttribute("Selected", "1");
                            break;
                        }
                    }
                    objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);
                }

                objReader.Close();

                objElemTemp = objDOM.CreateElement("CurrentUserID");
                objElemTemp.SetAttribute("userID", m_iUserId.ToString());
                objDOM.FirstChild.FirstChild.AppendChild(objElemTemp);

                p_objXmlIn = objDOM;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("ConfRecPerms.GetAvailablePermList.Error", m_iClientId), p_objEx);//rkaur27
            }
            finally
            {

            }
            return p_objXmlIn;
        }
        #endregion
    }
}
