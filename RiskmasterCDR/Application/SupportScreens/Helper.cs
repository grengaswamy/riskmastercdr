﻿
using System;
using System.Xml;
using System.Text;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Collections.Generic;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.SupportScreens
{
    ///************************************************************** 
    ///* $File				: Helper.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 01-14-2008
    ///* $Author			: Arnab Mukherjee
    ///***************************************************************	    
    /// <summary>
    /// This class will serve as a general class to fetch data from the database. This class is not bound
    /// to any specific module or class.
    /// </summary>
    public class Helper
    {        
		#region Constructor
		// Default Constructor
		public Helper()
		{			
		}

		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>
        public Helper(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)//sharishkumar Jira 827
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;//sharishkumar Jira 827
            this.Initialize();
		}        
		#endregion

		#region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;

        private int m_iClientId = 0;//sharishkumar Jira 827
		#endregion

		#region Public Methods
        /// Name		: GetWCPDFAttachToClaimFlag
        /// Author		: Arnab Mukherjee
        /// Date Created: 01/15/2008	
        ///************************************************************
        /// Amendment History
        ///************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///************************************************************
        /// <summary>
        /// MITS - 10753
        /// This function will match the State code and Selected_EID and fetch ATTACH_TO_CLAIM value from 
        /// JURIS_OPTIONS table. If the value = 0 => WC PDF forms to be atttached automatically else not.
        /// </summary>
        /// <param name="p_objXmlDoc">State code will be obtained from this input XML</param>
        /// <returns>It will return the status of the Flag stored in the DB</returns>
        public bool GetWCPDFAttachToClaimFlag(XmlDocument p_objXmlDoc)
        {
            DbReader objReader = null;
            string sSql = string.Empty;
            string sStateCode = string.Empty;
            string sOrgHierarchy = string.Empty;
            int iAttachToClaim = 0;
            bool bFlag = false;
            bool bflagstate= false;
            bool bflagdept = false;
            int iClaimID = 0;
            try
            {
                //Search for the selected State Code from XML passed from the calling method
                if (p_objXmlDoc.SelectSingleNode("//State") != null)
                    sStateCode = p_objXmlDoc.SelectSingleNode("//State").InnerText;

                //Search for the selected OrgHierarchy Code from XML passed from the calling method
                if (p_objXmlDoc.SelectSingleNode("//OrgHierarchy") != null)
                    sOrgHierarchy = p_objXmlDoc.SelectSingleNode("//OrgHierarchy").InnerText;

                //Added by Amitosh for MITS 27460  
              
                    if (p_objXmlDoc.SelectSingleNode("//" + "ClaimId") != null)
                        iClaimID = Conversion.ConvertObjToInt(p_objXmlDoc.SelectSingleNode("//" + "ClaimId").InnerText, m_iClientId);
              
                //End Amitosh
                //If State information hasnt come from screen.. this happens when an existing attached form is opened again
                if (sStateCode == "")
                {
                    return true;
                }
                
                //PJS MITS 10753 - 04-29-08 :Modified SQL Query in GetWCPDFAttachToClaimFlag() and given priority to the settings if there for any state.
                sSql = "SELECT S.STATE_ID, J.JURIS_ROW_ID, J.SELECTED_EID, J.ATTACH_TO_CLAIM "
                    + "FROM JURIS_OPTIONS J LEFT OUTER JOIN STATES S "
                    + "ON S.STATE_ROW_ID = J.JURIS_ROW_ID "
                    + "WHERE J.JURIS_ROW_ID = -1 OR UPPER(S.STATE_ID) = UPPER('" + sStateCode + "') ";
                if (string.IsNullOrEmpty(sOrgHierarchy))
                    sSql = sSql + "AND J.SELECTED_EID IN (-1, '" + GetClaimDeptt(iClaimID);
                else
                    sSql = sSql + "AND J.SELECTED_EID IN (-1, '" + sOrgHierarchy;

                 sSql = sSql+ "') ORDER BY J.JURIS_ROW_ID DESC";

                objReader = DbFactory.GetDbReader(m_sConnectionString, sSql);
                while (objReader.Read())
                {
                    string sStateId = Conversion.ConvertObjToStr(objReader.GetValue("STATE_ID"));
                    string sSelectedEid = Conversion.ConvertObjToStr(objReader.GetValue("SELECTED_EID"));
                    
                    
                    if (sStateId == sStateCode )
                    {

                        if (sSelectedEid != "-1" && sSelectedEid == sOrgHierarchy)
                        {
                            bFlag = false;
                            iAttachToClaim = Conversion.ConvertObjToInt(objReader.GetValue("ATTACH_TO_CLAIM"), m_iClientId);
                            if (iAttachToClaim == 0)
                                bFlag = true;
                            break;
                        }
                        if (sSelectedEid == "-1")
                        {
                            bFlag = false;
                            bflagstate = true;
                            iAttachToClaim = Conversion.ConvertObjToInt(objReader.GetValue("ATTACH_TO_CLAIM"), m_iClientId);
                            if (iAttachToClaim == 0)
                                bFlag = true;
                        }
                    }
                    
                    if (sStateId == "" && bflagstate == false )
                    {
                        if (sSelectedEid == "-1" && bflagdept == false )
                        {
                            bFlag = false;
                            iAttachToClaim = Conversion.ConvertObjToInt(objReader.GetValue("ATTACH_TO_CLAIM"), m_iClientId);
                            if (iAttachToClaim == 0)    // As 0 means to attach the claim
                                bFlag = true;
                        }
                        if (sSelectedEid != "-1" && sSelectedEid == sOrgHierarchy)
                        {
                            bFlag = false;
                            bflagdept = true;
                            iAttachToClaim = Conversion.ConvertObjToInt(objReader.GetValue("ATTACH_TO_CLAIM"), m_iClientId);
                            if (iAttachToClaim == 0)    // As 0 means to attach the claim
                                bFlag = true;

                        }
                    }                      
                }                
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("FROIOptions.GetFROIHistory.Error", m_iClientId), p_objEx);//sharishkumar Jira 827
            }
            finally
            {
                if (objReader != null)
                    objReader.Dispose();
            }
            return bFlag;
        }
		#endregion

		#region Private Methods

		/// <summary>
		/// Initialize objects
		/// </summary>
		private void Initialize()
		{            
            try
            {
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);//sharishkumar Jira 827
                m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("DupClaimList.Initialize.Error", m_iClientId), p_objEx);//sharishkumar Jira 827
            }
		}
        /// <summary>
        /// Added by Amitosh for MITS 27460  
        /// </summary>
        /// <param name="p_iClaimID"></param>
        /// <returns></returns>
        private string GetClaimDeptt(int p_iClaimID)
        {
            string sDept = string.Empty;
            try
            {
                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, "SELECT EVENT.DEPT_EID FROM EVENT,CLAIM WHERE EVENT.EVENT_ID=CLAIM.EVENT_ID  and CLAIM.CLAIM_ID =" + p_iClaimID))
                {
                    if(objReader.Read())
                    sDept =Conversion.ConvertObjToStr(objReader.GetValue(0));
                }
            }
            catch (Exception e)
            {
                sDept = string.Empty;
            }
            return sDept;
        }

		#endregion
	
    }
}
