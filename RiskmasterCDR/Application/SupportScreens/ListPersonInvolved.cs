﻿
using System;
using System.Xml;
using System.Globalization;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using System.Text;


namespace Riskmaster.Application.SupportScreens
{
	/// <summary>
	/// Summary description for PersonInvolvedList.
	/// </summary>
    public class ListPersonInvolved : IDisposable
	{

		#region "Constructor & Destructor"

		#region Constructor
		/// <summary>
			///	Constructor, initializes the variables to the default value
			/// </summary>
			/// <param name="p_sUserName">UserName</param>
			/// <param name="p_sPassword">Password</param>
			/// <param name="p_sDsnName">DsnName</param>
			/// <param name="p_iUserId">User Id</param>
			/// <param name="p_iUserGroupId">User Group Id</param>		
        public ListPersonInvolved(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iUserId, int p_iUserGroupId, int p_iClientId, string sLangid = "1033")//psharma206 //ttumula2 for RMA-7744(string sLangid = "1033")
        {
                m_iClientId = p_iClientId;	
				m_sUserName = p_sUserName;
				m_sPassword = p_sPassword;
				m_sDsnName = p_sDsnName;
				m_iUserId = p_iUserId ;
				m_iGroupId = p_iUserGroupId ;	
                sLangCode = sLangid; //ttumula2 on 11 Feb 2015 for RMA-6959
				this.Initialize();
			}		
        private string sLangCode = RMConfigurationManager.GetAppSetting("RMABaseLangCodeWithCulture").Split('|')[0].ToString();
		#endregion

		#region Destructor
        bool _isDisposed = false;
		/// <summary>
		/// Destructor
		/// </summary>
		~ListPersonInvolved()
		{
            Dispose();
		}
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;

                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }

                GC.SuppressFinalize(this);
            }
        }
		#endregion

		#endregion

		#region "Member Variables"

		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;

		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;

		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;

		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;

		/// <summary>
		/// Represents the connection string for the underlying Riskmaster Database.
		/// </summary>
		private string m_sConnectionString = "";
		
		/*/// <summary>
		///Represents the login name of the Logged in User
		/// </summary>
		private string m_sLoginName = "";*/

		/// <summary>
		/// Represents the User Id.
		/// </summary>
		private int m_iUserId = 0;

		/// <summary>
		/// The Group id 
		/// </summary>
		private int m_iGroupId = 0;

        private int m_iClientId = 0;//psharma206


		/// <summary>
		/// XML Document
		/// </summary>
		private XmlDocument m_objXmlDocument = null;
	
		#endregion

		#region Property declaration

		/// <summary>
		/// Gets & sets the user id
		/// </summary>
		public int UserId{get{return m_iUserId ;}set{m_iUserId = value;}} 

		/// <summary>
		/// Set the Group Id Property
		/// </summary>
		public int GroupId{get{return m_iGroupId;}set{m_iGroupId=value;}}

		#endregion

		#region "Public Function"

		#region "GetPIList(long p_lClaimId,long p_lEventId, out string p_sDocumentsXML)"
		/// Name		: GetPIList
		/// Author		: Mohit Yadav
		/// Date Created	: 21 December 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// fetches the data in XML format
		/// </summary>
		/// <param name="p_lClaimId">
		///		Claim ID to find pi list on (or 0 if EventID is being passed).
		///	</param>
		/// <param name="p_lEventId">
		///		Event ID to find pi list on (or 0 if Claim ID is non-zero).
		///	</param>
		/// <param name="p_sDocumentsXML">
		///		Out parameter. Returns information about persons involved in Xml format
		///	</param>
		/// <returns>0 - Success</returns>
		public int GetPIList(int p_iClaimId, int p_iEventId, out string p_sDocumentsXML)
		{
			Claim objClaim = null;
			Event objEvent = null;
			int iCount = 0;
			int iEventId = 0;
			int iEntityID=0;
			string sTableName="";
			int iEntityTableID=0;
			try
			{
				if (p_iClaimId != 0)  // claim id passed in - get event from it
				{
					objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim",false);
					objClaim.MoveTo(p_iClaimId);
					iEventId = Convert.ToInt32(objClaim.EventId.ToString());
				}
				else   // event id was passed in directly
					iEventId = p_iEventId;

				objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event",false);
				objEvent.MoveTo(iEventId);
				iCount = objEvent.PiList.Count;

				p_sDocumentsXML = objEvent.PiList.SerializeObject();
				//Mjain8 11/28/06 MITS 8448
				XmlNodeList objXmlNodeList = null;			
				XmlDocument Xmldoc=new XmlDocument();
				XmlElement objFormNode=null;
				Xmldoc.LoadXml(p_sDocumentsXML);
				objXmlNodeList = Xmldoc.SelectNodes("//PiEid[@tablename='OTHER_PEOPLE']");
 				foreach(XmlNode objNode in objXmlNodeList)
				{
                    iEntityID = Conversion.ConvertStrToInteger(((XmlElement)objNode).GetAttribute("codeid"));
                    iEntityTableID = GetEntityTableId(iEntityID, iEventId);     //avipinsrivas Start : Worked for Jira-340
                    sTableName = m_objDataModelFactory.Context.LocalCache.GetUserTableName(iEntityTableID);
                    objFormNode = (XmlElement)objNode.ParentNode.SelectSingleNode("PiTypeCode");
                    objFormNode.SetAttribute("tablename", sTableName);
                    objFormNode.InnerXml = "<![CDATA[O " + sTableName + "]]>";
				}
				p_sDocumentsXML=Xmldoc.InnerXml.ToString();
				//End MITS 8448
				return 1;
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch(Exception p_objException)
			{
				throw new RMAppException(Globalization.GetString("ListPersonInvolved.GetPIList.Error",m_iClientId),p_objException);//psharma206
			}
			finally
			{
			} 
		}
		#endregion

		#endregion

		#region "Private Functions"
		
        private int GetEntityTableId(int p_iEntityID, int p_iEventID)     //avipinsrivas Start : Worked for Jira-340
		{
			int iEntityTable_ID=0;
			string sSQL = string.Empty;
			DbReader objReader = null;
			try
			{
                //avipinsrivas Start : Worked for Jira-340
                if (m_objDataModelFactory.Context.InternalSettings.SysSettings.UseEntityRole)
                {
                    sSQL = " SELECT EXR.ENTITY_TABLE_ID  ";
                    sSQL = string.Concat(sSQL, " FROM PERSON_INVOLVED PI ");
                    sSQL = string.Concat(sSQL, " INNER JOIN ENTITY_X_ROLES EXR ON EXR.ER_ROW_ID = PI.PI_ER_ROW_ID ");
                    sSQL = string.Concat(sSQL, " WHERE PI.EVENT_ID = ", p_iEventID);
                    sSQL = string.Concat(sSQL, " AND PI.PI_EID = ", p_iEntityID);
                }
                else
                    sSQL = "SELECT ENTITY_TABLE_ID FROM ENTITY WHERE ENTITY_ID=" + p_iEntityID;
                //avipinsrivas End
                objReader = DbFactory.GetDbReader(m_sConnectionString, sSQL);
                if (objReader != null)
				{
                    if (objReader.Read())
						iEntityTable_ID = Conversion.ConvertStrToInteger(objReader["ENTITY_TABLE_ID"].ToString());
					objReader.Close();
					objReader = null; 
				}
			
			}
			catch (RMAppException p_objException)
			{
				throw p_objException;
			}
			finally
			{
				if(objReader!=null)
				{
					//Added by Shivendu
                    objReader.Close();
                    objReader.Dispose();
					objReader=null;					
				}
			}
			return( iEntityTable_ID );
		}		









		#region "Initialize"
		/// Name		: Initialize
		/// Author		: Mohit Yadav
		/// Date Created	: 21 December 2005
		/// ************************************************************
		/// Amendment History
		/// ************************************************************
		/// Date Amended   *   Amendment   *    Author
		///                *               *
		///                *               *
		/// ************************************************************
		/// <summary>
		/// Initialize the DataModal factory class object and Connection string;
		/// </summary>
		private void Initialize()
		{
			try
			{
				m_objDataModelFactory = new DataModelFactory(m_sDsnName,m_sUserName,m_sPassword,m_iClientId);//psharma206
				m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;
				m_objXmlDocument = new XmlDocument(); 
			}
			catch( DataModelException p_objEx )
			{
				throw p_objEx ;
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("ListPersonInvolved.Initialize.ErrorInit",m_iClientId) , p_objEx );	//psharma206			
			}			
		}	
		#endregion

		#endregion
        #region "GetPIList(long p_lClaimId,long p_lEventId, out string p_sDocumentsXML)"
        /// Name		: GetPIList
        /// Author		: Mohit Yadav
        /// Date Created	: 21 December 2005
        /// ************************************************************
        /// Amendment History
        /// ************************************************************
        /// Date Amended   *   Amendment   *    Author
        ///                *               *
        ///                *               *
        /// ************************************************************
        /// <summary>
        /// fetches the data in XML format
        /// </summary>
        /// <param name="p_lClaimId">
        ///		Claim ID to find pi list on (or 0 if EventID is being passed).
        ///	</param>
        /// <param name="p_lEventId">
        ///		Event ID to find pi list on (or 0 if Claim ID is non-zero).
        ///	</param>
        /// <param name="p_sDocumentsXML">
        ///		Out parameter. Returns information about persons involved in Xml format
        ///	</param>
        /// <returns>0 - Success</returns>
        public int GetPILevel(int p_iEventId, out string p_sDocumentsXML, string PageId = "")
        {
         
           
            int iEventId = 0;
          
            StringBuilder sbSQL;
            XmlDocument objDOM = null;
            XmlElement objElemParent = null;
            XmlElement objElemTemp = null;
            string sRiskLoc = string.Empty;
            string sSubLoc = string.Empty;
            string sDBType = string.Empty;
            sDBType = m_objDataModelFactory.Context.DbConn.DatabaseType.ToString();
            try
            {
                objDOM = new XmlDocument();
                objElemParent = objDOM.CreateElement("PersonInvolvedLevel");
                objDOM.AppendChild(objElemParent);
                iEventId = p_iEventId;

                sbSQL = new StringBuilder();
                if (sDBType == Constants.DB_SQLSRVR)
                sbSQL.Append(" SELECT POLICY_X_ENTITY.POLICY_UNIT_ROW_ID,POLICY_X_ENTITY.ENTITY_ID,(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN ('(' +  INTEGRAL_UNIT_DATA.UNIT_NUMBER  + ')')  ELSE ('(' + POINT_UNIT_DATA.STAT_UNIT_NUMBER + '/' + POINT_UNIT_DATA.UNIT_RISK_LOC  + '/' + POINT_UNIT_DATA.UNIT_RISK_SUB_LOC  + ')')  END) \"UNIT_NUMBER\", ");
                else if(sDBType== Constants.DB_ORACLE)
                sbSQL.Append(" SELECT POLICY_X_ENTITY.POLICY_UNIT_ROW_ID,POLICY_X_ENTITY.ENTITY_ID,(CASE WHEN POINT_UNIT_DATA.STAT_UNIT_NUMBER IS NULL THEN ('(' ||  INTEGRAL_UNIT_DATA.UNIT_NUMBER  || ')')  ELSE ('(' || POINT_UNIT_DATA.STAT_UNIT_NUMBER || '/' || POINT_UNIT_DATA.UNIT_RISK_LOC  || '/' || POINT_UNIT_DATA.UNIT_RISK_SUB_LOC  || ')')  END) \"UNIT_NUMBER\", ");

                // start ttumula2 on 11 Feb 2015 for RMA-6959
                //sbSQL.Append("(CASE WHEN POLICY_X_ENTITY.POLICY_UNIT_ROW_ID>0 THEN 'UNIT LEVEL'  ELSE 'POLICY LEVEL' END)  \"LEVEL\" FROM  ");

                sbSQL.Append("(CASE WHEN POLICY_X_ENTITY.POLICY_UNIT_ROW_ID>0 THEN");
                sbSQL.Append(" ");
                sbSQL.Append("'" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("lblUnitLevel", 0, PageId, m_iClientId), sLangCode) + "'");
                sbSQL.Append(" ELSE ");
                sbSQL.Append("'" + CommonFunctions.FilterBusinessMessage(Globalization.GetLocalResourceString("lblPolicyLevel", 0, PageId, m_iClientId), sLangCode) + "'");
                sbSQL.Append("  END)  \"LEVEL\"  FROM  ");
                //End ttumula2 on 11 Feb 2015 for RMA-6959
                sbSQL.Append("EVENT INNER JOIN CLAIM  ON EVENT.EVENT_ID=CLAIM.EVENT_ID INNER JOIN CLAIM_X_POLICY ON   CLAIM.CLAIM_ID=CLAIM_X_POLICY.CLAIM_ID  ");
                sbSQL.Append("INNER JOIN POLICY_X_ENTITY ON  CLAIM_X_POLICY.POLICY_ID=POLICY_X_ENTITY.POLICY_ID  LEFT OUTER JOIN POLICY_X_UNIT ON   ");
                sbSQL.Append("POLICY_X_ENTITY.POLICY_UNIT_ROW_ID=POLICY_X_UNIT.POLICY_UNIT_ROW_ID LEFT OUTER JOIN  ");
                sbSQL.Append("POINT_UNIT_DATA ON  POLICY_X_UNIT.UNIT_TYPE=POINT_UNIT_DATA.UNIT_TYPE AND POLICY_X_UNIT.UNIT_ID=POINT_UNIT_DATA.UNIT_ID ");
                sbSQL.Append("LEFT OUTER JOIN  INTEGRAL_UNIT_DATA ON  POLICY_X_UNIT.UNIT_TYPE=INTEGRAL_UNIT_DATA.UNIT_TYPE AND POLICY_X_UNIT.UNIT_ID=INTEGRAL_UNIT_DATA.UNIT_ID");
                sbSQL.Append(" WHERE EVENT.EVENT_ID =  " + iEventId);

                sbSQL.Append(" UNION ");
		//Change by kuldeep for mits:32251
		//sbSQL.Append("   SELECT '', POLICY_X_INSURED.INSURED_EID,'' ,'' ,'' ,'POLICY LEVEL'  \"LEVEL\"  FROM  EVENT INNER JOIN CLAIM  ON EVENT.EVENT_ID=CLAIM.EVENT_ID INNER ");
                sbSQL.Append("   SELECT 0, POLICY_X_INSURED.INSURED_EID,'' ,'POLICY LEVEL'  FROM  EVENT INNER JOIN CLAIM  ON EVENT.EVENT_ID=CLAIM.EVENT_ID INNER ");
                sbSQL.Append(" JOIN CLAIM_X_POLICY ON   CLAIM.CLAIM_ID=CLAIM_X_POLICY.CLAIM_ID  ");
                 sbSQL.Append(" INNER JOIN POLICY_X_INSURED ON  CLAIM_X_POLICY.POLICY_ID=POLICY_X_INSURED.POLICY_ID ");
                sbSQL.Append(" WHERE EVENT.EVENT_ID =  " + iEventId);



                using (DbReader objReader = DbFactory.GetDbReader(m_sConnectionString, sbSQL.ToString()))
                {
                    while (objReader.Read())
                    {
                        objElemTemp = objDOM.CreateElement("PILevel");
                        objElemTemp.SetAttribute("PiEid", Conversion.ConvertObjToStr(objReader.GetValue("ENTITY_ID")));
                        

                        if (string.Compare(objReader.GetValue("LEVEL").ToString(), "POLICY LEVEL") == 0)
                            objElemTemp.InnerXml = Conversion.ConvertObjToStr(objReader.GetValue("LEVEL")) ;
                        else 
                        {
                            objElemTemp.InnerXml = Conversion.ConvertObjToStr(objReader.GetValue("LEVEL")) + ' ' + Conversion.ConvertObjToStr(objReader.GetValue("UNIT_NUMBER")) ;
                        }
                        objDOM.FirstChild.AppendChild(objElemTemp);
                       // Xmldoc
                      //  iPsRowId = Conversion.ConvertObjToInt(objReader.GetValue(0), m_iClientId);
                    }
                }
                sbSQL.Remove(0, sbSQL.Length);

                p_sDocumentsXML = objDOM.InnerXml.ToString();
                //End MITS 8448
                return 1;
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ListPersonInvolved.GetPIList.Error", m_iClientId, sLangCode), p_objException);//psharma206
            }
            finally
            {
            }
        }
        #endregion

	
	}
}
