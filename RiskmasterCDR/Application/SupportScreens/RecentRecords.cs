﻿using System;
using System.Xml;
using System.Text;
using System.Data;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Collections.Generic;
using Riskmaster.ExceptionTypes;

namespace Riskmaster.Application.SupportScreens
{
    ///************************************************************** 
    ///* $File				: RecentRecords.cs 
    ///* $Revision			: 1.0.0.0 
    ///* $Creation Date		: 03-31-2009
    ///* $Author			: Nitin Sharma
    ///***************************************************************	    
    /// <summary>
    /// This class will fetch data from the database for Recently saved claims and events for R5.
    /// </summary>
    public class RecentRecords
    {
        #region Constructor
        // Default Constructor
        public RecentRecords(string connectionString, int userId,int p_iClientId)
        {
            DbConnection objConn;
            m_ConnectionString = connectionString;
            m_UserId = userId;
            //dvatsa
            m_iClientId = p_iClientId;
            //Added By Neha Mits 23633
            objConn = DbFactory.GetDbConnection(m_ConnectionString);
            objConn.Open();
            m_sDBType = objConn.DatabaseType.ToString().ToUpper();
            objConn.Close();
        }
        #endregion

        #region Variable Declaration
            string m_ConnectionString = string.Empty;
            int m_UserId = 0;
            /// <summary>
            /// Private Variable for Client Id
            /// </summary>
            private int m_iClientId = 0;
             // Neha Mits 23633
            internal struct RecentClaimConfig
            {
                private bool m_ClaimNumber;
                private bool m_Lob;
                private bool m_EventNumber;
                private bool m_DateOfEvent;
                private bool m_DateOfClaim;
                private bool m_Claimant;
                private bool m_ClaimType;
                private bool m_ClaimStatus;
                private string m_ClaimNumberHeader;
                private string m_LobHeader;
                private string m_EventNumberHeader;
                private string m_DateOfEventHeader;
                private string m_DateOfClaimHeader;
                private string m_ClaimantHeader;
                private string m_ClaimTypeHeader;
                private string m_ClaimStatusHeader;
                internal bool ClaimNumber { get { return m_ClaimNumber; } set { m_ClaimNumber = value; } }
                internal bool Lob { get { return m_Lob; } set { m_Lob = value; } }
                internal bool EventNumber { get { return m_EventNumber; } set { m_EventNumber = value; } }
                internal bool DateOfEvent { get { return m_DateOfEvent; } set { m_DateOfEvent = value; } }
                internal bool DateOfClaim { get { return m_DateOfClaim; } set { m_DateOfClaim = value; } }
                internal bool Claimant { get { return m_Claimant; } set { m_Claimant = value; } }
                internal bool ClaimType { get { return m_ClaimType; } set { m_ClaimType = value; } }
                internal bool ClaimStatus { get { return m_ClaimStatus; } set { m_ClaimStatus = value; } }
                internal string ClaimNumberHeader { get { return m_ClaimNumberHeader; } set { m_ClaimNumberHeader = value; } }
                internal string LobHeader { get { return m_LobHeader; } set { m_LobHeader = value; } }
                internal string EventNumberHeader { get { return m_EventNumberHeader; } set { m_EventNumberHeader = value; } }
                internal string DateOfEventHeader { get { return m_DateOfEventHeader; } set { m_DateOfEventHeader = value; } }
                internal string DateOfClaimHeader { get { return m_DateOfClaimHeader; } set { m_DateOfClaimHeader = value; } }
                internal string ClaimantHeader { get { return m_ClaimantHeader; } set { m_ClaimantHeader = value; } }
                internal string ClaimTypeHeader { get { return m_ClaimTypeHeader; } set { m_ClaimTypeHeader = value; } }
                internal string ClaimStatusHeader { get { return m_ClaimStatusHeader; } set { m_ClaimStatusHeader = value; } }

            }
            /// <summary>
            /// Object to store Diary Configurations Mits 23633 Neha
            /// </summary>
            private RecentClaimConfig m_RecentClaimConfig;
            /// <summary>
            /// Type of Connection Neha Mits 23633
            /// </summary>
            private static string m_sDBType = "";

        #endregion

        #region Public Methods
            public XmlDocument GetRecentSavedRecrods(XmlDocument p_objXmlDoc)
            {
                string recordType = string.Empty;
                XmlNodeList recentRecordsList = null;
                XmlDocument p_objXmlOut = null;

                try
                {
                    recordType = p_objXmlDoc.SelectSingleNode("//recordtype").InnerText;
                    recentRecordsList = GetSavedRecordsFromUserPrefXml(recordType);

                    //this null check has been added by Nitin for Mits 15255 on 13-Apr-2009
                    if (recentRecordsList != null)
                    {
                        if (recordType == "event")
                        {
                            p_objXmlOut = GetRecentSavedEvents(recentRecordsList);
                        }
                        else if (recordType == "claim")
                        {
                            p_objXmlOut = GetRecentSavedClaims(recentRecordsList);
                        }

                    }
                    return p_objXmlOut;
                }
                catch (RMAppException p_objEx)
                {
                    throw p_objEx;
                }
                catch (Exception p_objEx)
                {
                    //dvatsa
                    throw new RMAppException(Globalization.GetString("RecentRecords.GetRecentSavedRecords.Error",m_iClientId), p_objEx);
                }
                
        }

            /// <summary>
            /// Mits 23633 get the user specific header deatils for recent claim config screen
            /// </summary>
            /// <param name="p_iUserId"></param>
            /// <returns></returns>
            public XmlDocument GetRecentClaimConfig(int p_iUserId)
            {
                DbReader objReader = null;
                XmlDocument objOutDoc = null;
                XmlElement objRootNode = null;
                string sSQL = string.Empty;
                string sUserPrefsXML = string.Empty;
                XmlDocument objUserPrefXML = null;
                bool bStatus = false;
                try
                {
                    objOutDoc = new XmlDocument();
                   

                    sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + p_iUserId;
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                    if (objReader.Read())
                        sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                    objReader.Close();
                    sUserPrefsXML = sUserPrefsXML.Trim();
                    objUserPrefXML = new XmlDocument();

                    if (sUserPrefsXML != "")
                    {
                        objUserPrefXML.LoadXml(sUserPrefsXML);
                    }

                    XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimNumber/@selected");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.ClaimNumber = objNode.Value.ToString() == "1" ? true : false;
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Lob/@selected");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.Lob = objNode.Value.ToString() == "1" ? true : false;
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/EventNumber/@selected");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.EventNumber = objNode.Value.ToString() == "1" ? true : false;
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfEvent/@selected");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.DateOfEvent = objNode.Value.ToString() == "1" ? true : false;
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfClaim/@selected");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.DateOfClaim = objNode.Value.ToString() == "1" ? true : false;
                        bStatus = true;
                    }
                    //MITS 26304: smishra54
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Claimant/@selected");
                    //smishra54: End
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.Claimant = objNode.Value.ToString() == "1" ? true : false;
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimType/@selected");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.ClaimType = objNode.Value.ToString() == "1" ? true : false;
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimStatus/@selected");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.ClaimStatus = objNode.Value.ToString() == "1" ? true : false;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimNumberHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.ClaimNumberHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/LobHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.LobHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/EventNumberHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.EventNumberHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfEventHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.DateOfEventHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfClaimHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.DateOfClaimHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimantHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.ClaimantHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimTypeHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.ClaimTypeHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimStatusHeader/@value");
                    if (objNode != null && objNode.Value != null)
                    {
                        m_RecentClaimConfig.ClaimStatusHeader = objNode.Value.ToString();
                        bStatus = true;
                    }
                    if (bStatus)
                    {
                        objRootNode = objOutDoc.CreateElement("RecentClaimConfig");
                        objOutDoc.AppendChild(objRootNode);
                        this.CreateAndSetElement(objRootNode, "ClaimNumber", m_RecentClaimConfig.ClaimNumber.ToString());
                        this.CreateAndSetElement(objRootNode, "Lob", m_RecentClaimConfig.Lob.ToString());
                        this.CreateAndSetElement(objRootNode, "EventNumber", m_RecentClaimConfig.EventNumber.ToString());
                        this.CreateAndSetElement(objRootNode, "DateOfEvent", m_RecentClaimConfig.DateOfEvent.ToString());
                        this.CreateAndSetElement(objRootNode, "DateOfClaim", m_RecentClaimConfig.DateOfClaim.ToString());
                        this.CreateAndSetElement(objRootNode, "Claimant", m_RecentClaimConfig.Claimant.ToString());
                        this.CreateAndSetElement(objRootNode, "ClaimType", m_RecentClaimConfig.ClaimType.ToString());
                        this.CreateAndSetElement(objRootNode, "ClaimStatus", m_RecentClaimConfig.ClaimStatus.ToString());
                        this.CreateAndSetElement(objRootNode, "ClaimNumberHeader", m_RecentClaimConfig.ClaimNumberHeader.ToString());
                        this.CreateAndSetElement(objRootNode, "LobHeader", m_RecentClaimConfig.LobHeader.ToString());
                        this.CreateAndSetElement(objRootNode, "EventNumberHeader", m_RecentClaimConfig.EventNumberHeader.ToString());
                        this.CreateAndSetElement(objRootNode, "DateOfEventHeader", m_RecentClaimConfig.DateOfEventHeader.ToString());
                        this.CreateAndSetElement(objRootNode, "DateOfClaimHeader", m_RecentClaimConfig.DateOfClaimHeader.ToString());
                        this.CreateAndSetElement(objRootNode, "ClaimantHeader", m_RecentClaimConfig.ClaimantHeader.ToString());
                        this.CreateAndSetElement(objRootNode, "ClaimTypeHeader", m_RecentClaimConfig.ClaimTypeHeader.ToString());
                        this.CreateAndSetElement(objRootNode, "ClaimStatusHeader", m_RecentClaimConfig.ClaimStatusHeader.ToString());
                    }
                }
                catch (RMAppException p_objException)
                {
                    throw p_objException;
                }
                catch (Exception p_objException)
                {
                    //dvatsa
                    throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetUserPreference.Err",m_iClientId), p_objException);
                }
                finally
                {
                    objRootNode = null;
                    objUserPrefXML = null;
                    if (objReader != null)
                    {
                        objReader.Dispose();
                        objReader = null;
                    }
                }
                return objOutDoc;
            }

            /// <summary>
            /// Mits 23633,23649 saves the header values into databse for the recent calim config screen
            /// </summary>
            /// <param name="p_objInputDoc"></param>
            public void SetRecentClaimConfig(XmlDocument p_objInputDoc)
            {
                XmlDocument objUserPrefXML = null;
                DbReader objReader = null;
                string sUserPrefsXML = "";
                string sSQL = "";

                try
                {
                    sSQL = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID = " + m_UserId.ToString();
                    objReader = DbFactory.GetDbReader(m_ConnectionString, sSQL);

                    if (objReader.Read())
                        sUserPrefsXML = Common.Conversion.ConvertObjToStr(objReader.GetValue("PREF_XML"));
                    objReader.Close();
                    sUserPrefsXML = sUserPrefsXML.Trim();
                    objUserPrefXML = new XmlDocument();

                    if (sUserPrefsXML != "")
                    {
                        objUserPrefXML.LoadXml(sUserPrefsXML);
                    }
                    m_RecentClaimConfig.ClaimNumber = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/ClaimNumber"));
                    m_RecentClaimConfig.Lob = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/Lob"));
                    m_RecentClaimConfig.EventNumber = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/EventNumber"));
                    m_RecentClaimConfig.DateOfEvent = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/DateOfEvent"));
                    m_RecentClaimConfig.DateOfClaim = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/DateOfClaim"));
                    m_RecentClaimConfig.Claimant = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/Claimant"));
                    m_RecentClaimConfig.ClaimType = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/ClaimType"));
                    m_RecentClaimConfig.ClaimStatus = Common.Conversion.ConvertStrToBool(this.GetValue(p_objInputDoc, "RecentClaimConfig/ClaimStatus"));
                    m_RecentClaimConfig.ClaimNumberHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/ClaimNumberHeader").ToString();
                    m_RecentClaimConfig.LobHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/LobHeader").ToString();
                    m_RecentClaimConfig.EventNumberHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/EventNumberHeader").ToString();
                    m_RecentClaimConfig.DateOfEventHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/DateOfEventHeader").ToString();
                    m_RecentClaimConfig.DateOfClaimHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/DateOfClaimHeader").ToString();
                    m_RecentClaimConfig.ClaimantHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/ClaimantHeader").ToString();
                    m_RecentClaimConfig.ClaimTypeHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/ClaimTypeHeader").ToString();
                    m_RecentClaimConfig.ClaimStatusHeader = this.GetValue(p_objInputDoc, "RecentClaimConfig/ClaimStatusHeader").ToString();

                    CreateUserPreferXML(objUserPrefXML);
                    XmlNode objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimNumber/@selected");
                    objNode.Value = m_RecentClaimConfig.ClaimNumber ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Lob/@selected");
                    objNode.Value = m_RecentClaimConfig.Lob ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/EventNumber/@selected");
                    objNode.Value = m_RecentClaimConfig.EventNumber ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfEvent/@selected");
                    objNode.Value = m_RecentClaimConfig.DateOfEvent ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfClaim/@selected");
                    objNode.Value = m_RecentClaimConfig.DateOfClaim ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Claimant/@selected");
                    objNode.Value = m_RecentClaimConfig.Claimant ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimType/@selected");
                    objNode.Value = m_RecentClaimConfig.ClaimType ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimStatus/@selected");
                    objNode.Value = m_RecentClaimConfig.ClaimStatus ? "1" : "0";
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimNumberHeader/@value");
                    objNode.Value = m_RecentClaimConfig.ClaimNumberHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/LobHeader/@value");
                    objNode.Value = m_RecentClaimConfig.LobHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/EventNumberHeader/@value");
                    objNode.Value = m_RecentClaimConfig.EventNumberHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfEventHeader/@value");
                    objNode.Value = m_RecentClaimConfig.DateOfEventHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfClaimHeader/@value");
                    objNode.Value = m_RecentClaimConfig.DateOfClaimHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimantHeader/@value");
                    objNode.Value = m_RecentClaimConfig.ClaimantHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimTypeHeader/@value");
                    objNode.Value = m_RecentClaimConfig.ClaimTypeHeader;
                    objNode = objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimStatusHeader/@value");
                    objNode.Value = m_RecentClaimConfig.ClaimStatusHeader;

                    this.SaveUserPrefersXmlToDb(objUserPrefXML.OuterXml, Convert.ToInt32(m_UserId));
                }
                catch (RMAppException p_objException)
                {
                    throw p_objException;
                }
                catch (Exception p_objException)
                {
                    //dvatsa
                    throw new RMAppException(Globalization.GetString("ExecutiveSummary.SaveUserPreference.Err",m_iClientId), p_objException);
                }
                finally
                {
                    objUserPrefXML = null;
                    if (objReader != null)
                    {
                        objReader.Dispose();
                        objReader = null;
                    }

                }
            }
        #endregion

        #region Private Methods

        private XmlNodeList GetSavedRecordsFromUserPrefXml(string recordType)
        {
            string sSql = string.Empty;
            string sXml = string.Empty;
            XmlDocument objPrefXml = null;
            XmlNodeList recentRecordList = null;

            try
            {
                sSql = "SELECT PREF_XML FROM USER_PREF_XML WHERE USER_ID=" + m_UserId;
                using (DbReader objReader = DbFactory.GetDbReader(m_ConnectionString, sSql))
                {
                    if (objReader.Read())
                    {
                        sXml = objReader.GetValue("PREF_XML").ToString();
                    }
                }

                if (!string.IsNullOrEmpty(sXml))
                {
                    try
                    {
                        objPrefXml = new XmlDocument();
                        objPrefXml.LoadXml(sXml);
                    }
                    catch
                    {
                        throw new ApplicationException("UserPref xml is not in correct format");
                    }

                    recentRecordList = objPrefXml.SelectNodes("setting/RecentRecords/record[@recordtype='" + recordType + "']");
                }

                return recentRecordList;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("RecentRecords.GetRecentSavedRecords.Error",m_iClientId), p_objEx);
            }
            finally
            {
                objPrefXml = null;
            }
        }

        private XmlDocument GetRecentSavedEvents(XmlNodeList eventNodeList)
        {
            string recordId = string.Empty;
            string sSql = string.Empty;
            XmlElement objEventNode = null;
            XmlElement objRecentEventsNode = null;
            XmlDocument p_objXmlDoc = null;
            StringBuilder sbEventIDs = new StringBuilder();

            try
            {
                p_objXmlDoc = new XmlDocument();
                objRecentEventsNode = p_objXmlDoc.CreateElement("recentevents");

                //rsushilaggar 03/11/2011 MITS 24342
                if (eventNodeList.Count != 0)
                {
                    foreach (XmlNode recentEventNode in eventNodeList)
                    {
                        recordId = recentEventNode.Attributes["recordid"].Value;
                        if (sbEventIDs.Length > 0)
                            sbEventIDs.Append(",");
                        sbEventIDs.Append(recordId);
                    }

                    //sSql = "SELECT EVENT.EVENT_ID, EVENT.EVENT_NUMBER, EVENT.DATE_OF_EVENT,EVENT.DATE_REPORTED," +
                    //        " EV_STATUS.CODE_DESC   EVENT_STATUS,EV_TYPE.CODE_DESC EVENT_TYPE,EV_INDICATOR.CODE_DESC   EV_INDICATOR_CODE ," +
                    //        " DEPT.LAST_NAME   DEPARTMENT " +
                    //        " FROM EVENT, ENTITY   DEPT, CODES_TEXT   EV_STATUS, CODES_TEXT   EV_INDICATOR,CODES_TEXT  EV_TYPE " +
                    //        " WHERE EVENT.DEPT_EID = DEPT.ENTITY_ID AND EVENT.EVENT_ID <> 0 " +
                    //        " AND EVENT.EVENT_NUMBER IS NOT NULL  AND EVENT.EVENT_STATUS_CODE=EV_STATUS.CODE_ID " +
                    //        " AND EVENT.EVENT_TYPE_CODE=EV_TYPE.CODE_ID " +
                    //        " AND EVENT.EVENT_IND_CODE=EV_INDICATOR.CODE_ID AND EVENT.EVENT_ID in (" + sbEventIDs.ToString() + ")";

                    //Aman ML Change
                    sSql = "SELECT EVENT.EVENT_ID, EVENT.EVENT_NUMBER, EVENT.DATE_OF_EVENT,EVENT.DATE_REPORTED, " +
" EV_STATUS.CODE_DESC   EVENT_STATUS,EV_TYPE.CODE_DESC EVENT_TYPE,EV_INDICATOR.CODE_DESC   EV_INDICATOR_CODE, " +
" DEPT.LAST_NAME   DEPARTMENT  FROM EVENT, ENTITY   DEPT, CODES_TEXT   EV_STATUS, " +
" CODES_TEXT   EV_INDICATOR,CODES_TEXT  EV_TYPE,CODES EVNT_TYPE  , CODES EVNT_STATUS, CODES EVNT_INDICATOR" +
" WHERE EVENT.DEPT_EID = DEPT.ENTITY_ID AND " +
" EVENT.EVENT_ID <> 0  AND EVENT.EVENT_NUMBER IS NOT NULL  " +
" AND (EVENT.EVENT_STATUS_CODE=EVNT_STATUS.CODE_ID AND EVNT_STATUS.CODE_ID = EV_STATUS.CODE_ID AND EV_STATUS.LANGUAGE_CODE = '1033') " +
" AND (EVENT.EVENT_TYPE_CODE=EVNT_TYPE.CODE_ID AND EVNT_TYPE.CODE_ID = EV_TYPE.CODE_ID AND EV_TYPE.LANGUAGE_CODE = '1033')" +
" AND (EVENT.EVENT_IND_CODE=EVNT_INDICATOR.CODE_ID AND EVNT_INDICATOR.CODE_ID = EV_INDICATOR.CODE_ID AND EV_INDICATOR.LANGUAGE_CODE = '1033') " +
" AND  EVENT.EVENT_ID in (" + sbEventIDs.ToString() + ")";
    //Aman ML Change

                    using (DbReader objReader = DbFactory.GetDbReader(m_ConnectionString, sSql))
                    {
                        while (objReader.Read())
                        {
                            objEventNode = p_objXmlDoc.CreateElement("event");

                            objEventNode.SetAttribute("event_id", objReader.GetValue("EVENT_ID").ToString());
                            objEventNode.SetAttribute("event_number", objReader.GetValue("EVENT_NUMBER").ToString());
                            objEventNode.SetAttribute("date_of_event", Conversion.GetDBDateFormat(objReader.GetValue("DATE_OF_EVENT").ToString(), "d"));
                            objEventNode.SetAttribute("date_reported", Conversion.GetDBDateFormat(objReader.GetValue("DATE_REPORTED").ToString(), "d"));
                            objEventNode.SetAttribute("event_status", objReader.GetValue("EVENT_STATUS").ToString());
                            objEventNode.SetAttribute("event_type", objReader.GetValue("EVENT_TYPE").ToString());
                            objEventNode.SetAttribute("ev_indicator_code", objReader.GetValue("EV_INDICATOR_CODE").ToString());
                            objEventNode.SetAttribute("department", objReader.GetValue("DEPARTMENT").ToString());

                            objRecentEventsNode.AppendChild(objEventNode);
                        }
                    }

                    p_objXmlDoc.AppendChild(objRecentEventsNode);
                }
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("RecentRecords.GetRecentSavedRecords.Error",m_iClientId), p_objEx);
            }
            finally
            {
            }

            return p_objXmlDoc;
        }

        private XmlDocument GetRecentSavedClaims(XmlNodeList claimNodeList)
        {
            string recordId = string.Empty;
            string sSql = string.Empty;
            string sUserPrefsXML = string.Empty;//Neha
            bool bIsNoColumnSelected = true;//Neha
            XmlElement objClaimNode = null;
            XmlElement objRecentClaimsNode = null;
            XmlDocument p_objXmlDoc = null;
            XmlDocument objUserPrefXML = null; //Neha
            StringBuilder sbClaimIDs = new StringBuilder();
            string sClaimantSql = null;//Neha
            try
            {
                p_objXmlDoc = new XmlDocument();
                //Neha Mits 23633 getting header values from the database
                using (DbReader objRdr = DbFactory.GetDbReader(m_ConnectionString, "Select PREF_XML from USER_PREF_XML where USER_ID=" + m_UserId.ToString()))
                {
                    if (objRdr.Read())
                    {
                        sUserPrefsXML = Common.Conversion.ConvertObjToStr(objRdr.GetValue("PREF_XML"));
                    }
                    //objRdr.Close();
                }
                sUserPrefsXML = sUserPrefsXML.Trim();
                objUserPrefXML = new XmlDocument();
                if (sUserPrefsXML != "")
                {
                    objUserPrefXML.LoadXml(sUserPrefsXML);
                }

                // Create the xml if it doesn't exist in the database
                CreateUserPreferXML(objUserPrefXML);

                //Neha End
                objRecentClaimsNode = p_objXmlDoc.CreateElement("recentclaims");
                //Neha Start Mits 23633 getting header values from the database
                objRecentClaimsNode.SetAttribute("ClaimNumber",
                   objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimNumber/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("Lob",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Lob/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("EventNumber",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/EventNumber/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("DateOfEvent",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfEvent/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("DateofClaim",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfClaim/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("Claimant",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Claimant/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("ClaimType",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimType/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("ClaimStatus",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimStatus/@selected").Value.ToString());
                objRecentClaimsNode.SetAttribute("ClaimNumberHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimNumberHeader/@value").Value.ToString());
                objRecentClaimsNode.SetAttribute("LobHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/LobHeader/@value").Value.ToString());
                objRecentClaimsNode.SetAttribute("EventNumberHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/EventNumberHeader/@value").Value.ToString());
                objRecentClaimsNode.SetAttribute("DateOfEventHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfEventHeader/@value").Value.ToString());
                objRecentClaimsNode.SetAttribute("DateOfClaimHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfClaimHeader/@value").Value.ToString());
                objRecentClaimsNode.SetAttribute("ClaimantHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimantHeader/@value").Value.ToString());
                objRecentClaimsNode.SetAttribute("ClaimTypeHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimTypeHeader/@value").Value.ToString());
                objRecentClaimsNode.SetAttribute("ClaimStatusHeader",
                    objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimStatusHeader/@value").Value.ToString());

                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimNumber/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }
                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Lob/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }
                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/EventNumber/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }
                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfEvent/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }
                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/DateOfClaim/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }
                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/Claimant/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }
                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimType/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }
                if (Convert.ToInt32(objUserPrefXML.SelectSingleNode("//setting/RecentClaimConfig/ClaimStatus/@selected").Value) > 0)
                {
                    bIsNoColumnSelected = false;
                }

                objRecentClaimsNode.SetAttribute("NoHeaderSelected", bIsNoColumnSelected.ToString());
                //Neha end
                //rsushilaggar 03/11/2011 MITS 24342 
                if (claimNodeList.Count > 0)
                {
                    foreach (XmlNode recentClaimNode in claimNodeList)
                    {
                        recordId = recentClaimNode.Attributes["recordid"].Value;
                        if (sbClaimIDs.Length > 0)
                            sbClaimIDs.Append(",");
                        sbClaimIDs.Append(recordId);
                    }

                    //sSql = "SELECT CLAIM_ID, CLAIM_NUMBER,CLAIM.EVENT_NUMBER,CODES3.CODE_DESC LOB,DATE_OF_CLAIM,EVENT.DATE_OF_EVENT," +
                    //       " CODES1.CODE_DESC   CLAIM_TYPE," +
                    //        "CODES2.CODE_DESC   CLAIM_STATUS FROM CLAIM,CODES_TEXT   CODES1, CODES_TEXT   CODES2, EVENT,CODES_TEXT CODES3 " +
                    //        " WHERE CLAIM_TYPE_CODE=CODES1.CODE_ID AND CLAIM_STATUS_CODE=CODES2.CODE_ID" +
                    //        " AND CLAIM.LINE_OF_BUS_CODE = CODES3.CODE_ID AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM_ID in ( " + sbClaimIDs.ToString() + ")";
                    //Mits 23633 getting header values from the database - added claimant
                    //Deb:Optimized the sql query
                    if (m_sDBType == Constants.DB_SQLSRVR)
                        sClaimantSql = " CASE WHEN (SELECT TOP 1 COUNT(CLAIMANT.CLAIMANT_EID) FROM CLAIMANT  WHERE  CLAIMANT.PRIMARY_CLMNT_FLAG = -1 AND CLAIMANT.CLAIM_ID =CLAIM.CLAIM_ID )=0 " +
                            "THEN (SELECT TOP 1 ISNULL(ENTITY.LAST_NAME,'')+ ISNULL(', '+ENTITY.FIRST_NAME,'') CLAIMANT FROM CLAIMANT ,ENTITY  WHERE  CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND CLAIMANT.CLAIM_ID =CLAIM.CLAIM_ID ORDER BY CLAIMANT.CLAIMANT_ROW_ID ASC) " +
                             "ELSE (SELECT TOP 1 ISNULL(ENTITY.LAST_NAME,'')+ ISNULL(', '+ENTITY.FIRST_NAME,'') CLAIMANT FROM CLAIMANT ,ENTITY	WHERE  CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG = -1 AND CLAIMANT.CLAIM_ID =CLAIM.CLAIM_ID ) " +
                             " END  CLAIMANT, ";
                    else
                        //tmalhotra2 MITS 29974
                        //tmalhotra2 MITS 30081 & 29968 start
                        sClaimantSql = " CASE WHEN (SELECT COUNT(CLAIMANT.CLAIMANT_EID) FROM CLAIMANT  WHERE CLAIMANT.PRIMARY_CLMNT_FLAG = -1 AND CLAIMANT.CLAIM_ID =CLAIM.CLAIM_ID AND ROWNUM=1)=0 " +
                            //"THEN (SELECT NVL(ENTITY.LAST_NAME,'')||NVL(', '||ENTITY.FIRST_NAME,'') CLAIMANT FROM CLAIMANT ,ENTITY " +
                            "THEN (SELECT NVL(ENTITY.LAST_NAME,'')|| CASE WHEN (ENTITY.FIRST_NAME IS NOT NULL) THEN (', '||ENTITY.FIRST_NAME ) ELSE ('') END CLAIMANT FROM CLAIMANT ,ENTITY " +
                             "WHERE  CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND CLAIMANT.CLAIM_ID =CLAIM.CLAIM_ID AND ROWNUM=1 )" +
                              //"ELSE (SELECT  NVL(ENTITY.LAST_NAME,'')||NVL(', '||ENTITY.FIRST_NAME,'') CLAIMANT FROM CLAIMANT ,ENTITY " +
                              "ELSE (SELECT  NVL(ENTITY.LAST_NAME,'')|| CASE WHEN (ENTITY.FIRST_NAME IS NOT NULL) THEN (', '||ENTITY.FIRST_NAME ) ELSE ('') END CLAIMANT FROM CLAIMANT ,ENTITY " +
                              "WHERE  CLAIMANT.CLAIMANT_EID = ENTITY.ENTITY_ID AND CLAIMANT.PRIMARY_CLMNT_FLAG = -1 AND CLAIMANT.CLAIM_ID =CLAIM.CLAIM_ID AND ROWNUM=1 )  END  CLAIMANT, ";

                    //sSql = "SELECT CLAIM.CLAIM_ID, CLAIM_NUMBER,CLAIM.EVENT_NUMBER,CODES3.CODE_DESC LOB,DATE_OF_CLAIM,EVENT.DATE_OF_EVENT," +
                    //     sClaimantSql +
                    //       " CODES1.CODE_DESC   CLAIM_TYPE," +
                    //        "CODES2.CODE_DESC   CLAIM_STATUS FROM CLAIM,CODES_TEXT   CODES1, CODES_TEXT   CODES2, EVENT,CODES_TEXT CODES3 " +
                    //        " WHERE CLAIM_TYPE_CODE=CODES1.CODE_ID AND CLAIM_STATUS_CODE=CODES2.CODE_ID" +
                    //        " AND CLAIM.LINE_OF_BUS_CODE = CODES3.CODE_ID AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM_ID in ( " + sbClaimIDs.ToString() + ")";
                    //Deb:Optimized the sql query
                    //End 23633
                    
                    //Aman ML Change
                    sSql = "SELECT CLAIM.CLAIM_ID, CLAIM_NUMBER,CLAIM.EVENT_NUMBER,DATE_OF_CLAIM, CODES4.CODE_DESC LOB,EVENT.DATE_OF_EVENT, " +
                   sClaimantSql +
                   " CODES6.CODE_DESC   CLAIM_TYPE,CODES5.CODE_DESC   CLAIM_STATUS FROM CLAIM,CODES   CODES1, CODES   CODES2, EVENT,CODES CODES3 ,CODES_TEXT CODES4 ,CODES_TEXT CODES5 ,CODES_TEXT CODES6 " +
                   "WHERE (CLAIM_TYPE_CODE=CODES1.CODE_ID AND CODES1.CODE_ID = CODES6.CODE_ID AND CODES6.LANGUAGE_CODE = '1033') " +
                   "AND (CLAIM_STATUS_CODE=CODES2.CODE_ID AND CODES2.CODE_ID = CODES5.CODE_ID AND CODES5.LANGUAGE_CODE = '1033') " +
                   "AND (CLAIM.LINE_OF_BUS_CODE = CODES3.CODE_ID AND CODES3.CODE_ID = CODES4.CODE_ID AND CODES4.LANGUAGE_CODE = '1033') " +
                   "AND CLAIM.EVENT_ID = EVENT.EVENT_ID AND CLAIM_ID in ( " + sbClaimIDs.ToString() + ")ORDER BY CLAIM_ID";//RMA-12867 msampathkuma "added order by"
                    //Aman ML Change

                    using (DbReader objReader = DbFactory.GetDbReader(m_ConnectionString, sSql))
                    {
                        while (objReader.Read())
                        {
                            objClaimNode = p_objXmlDoc.CreateElement("claim");

                            objClaimNode.SetAttribute("claim_id", objReader.GetValue("CLAIM_ID").ToString());
                            objClaimNode.SetAttribute("claim_number", objReader.GetValue("CLAIM_NUMBER").ToString());
                            objClaimNode.SetAttribute("event_number", objReader.GetValue("EVENT_NUMBER").ToString());
                            objClaimNode.SetAttribute("claim_lob", objReader.GetValue("LOB").ToString());
                            objClaimNode.SetAttribute("date_of_claim", Conversion.GetDBDateFormat(objReader.GetValue("DATE_OF_CLAIM").ToString(), "d"));
                            objClaimNode.SetAttribute("date_of_event", Conversion.GetDBDateFormat(objReader.GetValue("DATE_OF_EVENT").ToString(), "d"));
                            objClaimNode.SetAttribute("claimant", objReader.GetValue("CLAIMANT").ToString());
                            objClaimNode.SetAttribute("claim_type", objReader.GetValue("CLAIM_TYPE").ToString());
                            objClaimNode.SetAttribute("claim_status", objReader.GetValue("CLAIM_STATUS").ToString());

                            objRecentClaimsNode.AppendChild(objClaimNode);
                        }
                    }
                    p_objXmlDoc.AppendChild(objRecentClaimsNode);
                }
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("RecentRecords.GetRecentSavedRecords.Error",m_iClientId), p_objEx);
            }
            finally
            {
            }

            return p_objXmlDoc;
        }

        //05/16/2011 Neha Start Mits 23633 getting header values from the database
        /// <summary>
        /// Create and Set the Text of the New Node.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Inner Text.</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText)
        {
            try
            {
                XmlElement objChildNode = null;
                this.CreateAndSetElement(p_objParentNode, p_sNodeName, p_sText, ref objChildNode);
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateAndSetElement.Err",m_iClientId), p_objEx);
            }
        }
        /// <summary>
        /// Create and Set the Text of the New Node.
        /// </summary>
        /// <param name="p_objParentNode">Parent node</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <param name="p_sText">Inner Text.</param>
        /// <param name="p_objChildNode">Child Node Refrence</param>
        private void CreateAndSetElement(XmlElement p_objParentNode, string p_sNodeName, string p_sText, ref XmlElement p_objChildNode)
        {
            try
            {
                CreateElement(p_objParentNode, p_sNodeName, ref p_objChildNode);
                p_objChildNode.InnerText = p_sText;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateAndSetElement.Err",m_iClientId), p_objEx);
            }
        }

        ///// <summary>
        ///// Create and Set the Text of the New Node.
        ///// </summary>
        ///// <param name="p_objParentNode">Parent node</param>
        ///// <param name="p_sNodeName">Node Name</param>
        ///// <param name="p_sText">Inner Text.</param>
        ///// <param name="p_objChildNode">Child Node Refrence</param>

        ///// <summary>
        ///// Create new Element in the Xml Document.
        ///// </summary>
        ///// <param name="p_objParentNode">Parent node</param>
        ///// <param name="p_sNodeName">Node Name</param>
        ///// <param name="p_objChildNode">Refrence to the New Node Created.</param>
        private void CreateElement(XmlElement p_objParentNode, string p_sNodeName, ref XmlElement p_objChildNode)
        {
            try
            {
                p_objChildNode = p_objParentNode.OwnerDocument.CreateElement(p_sNodeName);
                p_objParentNode.AppendChild(p_objChildNode);
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.CreateElement.Err",m_iClientId), p_objEx);
            }
        }

        /// <summary>
        /// Get Value of the Node.
        /// </summary>
        /// <param name="p_objDocument">Input document.</param>
        /// <param name="p_sNodeName">Node Name</param>
        /// <returns>Node Inner Text.</returns>
        private string GetValue(XmlDocument p_objDocument, string p_sNodeName)
        {
            XmlElement objNode = null;
            string sValue = "";

            try
            {
                objNode = (XmlElement)p_objDocument.SelectSingleNode("//" + p_sNodeName);

                if (objNode != null)
                    sValue = objNode.InnerText;
            }
            catch (Exception p_objEx)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("ExecutiveSummary.GetValue.Err",m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }
            return (sValue);
        }

        
        /// <summary>
        /// Create Default User Prefernce Xml.
        /// </summary>
        /// <param name="p_objUserPrefXML">User Preference Xml Doc</param>
        private void CreateUserPreferXML(XmlDocument p_objUserPrefXML)
        {
            XmlElement objTempNode = null;
            XmlElement objParentNode = null;
            try
            {
                // Create setting node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                if (objTempNode == null)
                {
                    objTempNode = p_objUserPrefXML.CreateElement("setting");
                    p_objUserPrefXML.AppendChild(objTempNode);
                }

                // Create RecentClaimConfig node if Not present.
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("/setting");
                    objTempNode = p_objUserPrefXML.CreateElement("RecentClaimConfig");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/ClaimNumber");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimNumber");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/Lob");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Lob");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/EventNumber");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("EventNumber");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/DateOfEvent");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateOfEvent");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/DateOfClaim");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateOfClaim");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/Claimant");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("Claimant");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/ClaimType");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimType");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/ClaimStatus");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimStatus");
                    //shownotes is true by default
                    objTempNode.SetAttribute("selected", "1");
                    objParentNode.AppendChild(objTempNode);
                }
                              
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/ClaimNumberHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimNumberHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Claim Number");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/LobHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("LobHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Line of Business");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/EventNumberHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("EventNumberHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Event Number");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/DateOfEventHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateOfEventHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Date of Event");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/DateOfClaimHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("DateOfClaimHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Date of Claim");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/ClaimantHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimantHeader");
                    //shownotes is true by default
                    objTempNode.SetAttribute("value", "Claimant");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/ClaimTypeHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimTypeHeader");
                    objTempNode.SetAttribute("value", "Claim Type");
                    objParentNode.AppendChild(objTempNode);
                }
                objTempNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig/ClaimStatusHeader");
                if (objTempNode == null)
                {
                    objParentNode = (XmlElement)p_objUserPrefXML.SelectSingleNode("//RecentClaimConfig");
                    objTempNode = p_objUserPrefXML.CreateElement("ClaimStatusHeader");
                    objTempNode.SetAttribute("value", "Claim Status");
                    objParentNode.AppendChild(objTempNode);
                }
                
               
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("RecentRecords.CreateUserPreferXML.Err",m_iClientId), p_objException);
            }
            finally
            {
                objTempNode = null;
                objParentNode = null;
            }
        }
   
        /// <summary>
        /// saves the user Prference xml to db
        /// </summary>
        /// <param name="p_sUserPrefXml"></param>
        /// <param name="p_iUserId"></param>
        private void SaveUserPrefersXmlToDb(string p_sUserPrefXml, int p_iUserId)
        {
            DbConnection objConn = null;
            DbWriter objWriter = null;
            string sSQL = string.Empty;

            try
            {
                //Try to delete the old one.
                sSQL = "DELETE FROM USER_PREF_XML WHERE USER_ID = " + p_iUserId;
                objConn = DbFactory.GetDbConnection(m_ConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sSQL);
                objConn.Close();

                objWriter = DbFactory.GetDbWriter(m_ConnectionString);
                objWriter.Tables.Add("USER_PREF_XML");
                objWriter.Fields.Add("USER_ID", p_iUserId);
                objWriter.Fields.Add("PREF_XML", p_sUserPrefXml);
                objWriter.Execute();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                //dvatsa
                throw new RMAppException(Globalization.GetString("RecentRecords.SaveUserPrefersXmlToDb.Err",m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                    objConn = null;
                }
                if (objWriter != null)
                {
                    objWriter = null;
                }
            }
        }
        //Neha End

        #endregion

    }
}
