using System;
using System.Xml;
using System.Globalization;
using System.Collections;
using Riskmaster.Common;
using Riskmaster.Db;
using System.Data;
using Riskmaster.ExceptionTypes;
namespace Riskmaster.Application.SupportScreens
{
	/// <summary>
	/// Summary description for HipaaLog.
	/// </summary>
	public class HipaaLog
	{
		private string m_sConnectionString="";
        private int m_iClientId = 0;
        public HipaaLog(string p_sConnectionString, int p_iClientId)
		{
			m_sConnectionString=p_sConnectionString;
            m_iClientId = p_iClientId;
		}
		private int GetMaxId()
		{
			string sSQL="";
			DbReader objRead=null;
			sSQL = "SELECT MAX(ACTLOG_ROW_ID) FROM ACTIVITY_LOG";
			try
			{
				objRead=DbFactory.GetDbReader(m_sConnectionString,sSQL);
				if (objRead.Read())
				{
                    return (Conversion.ConvertObjToInt(objRead.GetValue(0), m_iClientId) + 1);
				}
				else
				{
					objRead.Dispose();
					return 1;
				}
			}
			finally
			{   
                //Added by Shivendu
                if (objRead != null)
                {
                    objRead.Close();
                    objRead.Dispose();
                }
			}
		}
		public void LogHippaInfo(int p_iPatientId,string p_sEventId,string p_sClaimId,string p_sOperation
			,string p_sAccessType,string p_sUser)
		{
			DbConnection objConn=null;
			string sSQL="";
			int iCodeId=0;
			LocalCache objCache=null;
	//  int iActID=GetMaxId();
			objCache=new LocalCache(m_sConnectionString,m_iClientId);
			iCodeId=objCache.GetCodeId(p_sAccessType,"ACCESS_TYPE_CODE");
			objCache.Dispose();
			p_sOperation=p_sOperation.Replace("'","''");
      //Ijha: Mobile Apps: Issue in Downloading Claim due to hippa log table entry
            //sSQL = "INSERT INTO ACTIVITY_LOG(ACTLOG_ROW_ID,PATIENT_EID,"+
            //    "EVENT_ID,CLAIM_ID,OPERATION,ACCESS_TYPE_CODE,USER_LOGIN,DTTM_LOG) VALUES(" + 
            //    "'"+iActID.ToString()+"','"+p_iPatientId+"','"+p_sEventId+"','"+p_sClaimId+"','"+p_sOperation+"','"
            //    +iCodeId+"','"+p_sUser+"','"+Conversion.GetDateTime(DateTime.Now.ToString())+"')";
			try
			{
				objConn = DbFactory.GetDbConnection(m_sConnectionString);
				objConn.Open();

                if (DbFactory.IsOracleDatabase(m_sConnectionString))
                {
                    sSQL = "INSERT INTO ACTIVITY_LOG(ACTLOG_ROW_ID,PATIENT_EID," +
                   "EVENT_ID,CLAIM_ID,OPERATION,ACCESS_TYPE_CODE,USER_LOGIN,DTTM_LOG) VALUES(ACTIVITY_LOG_SEQUENCE.NEXTVAL,"+
                   "'" + p_iPatientId + "','" + p_sEventId + "','" + p_sClaimId + "','" + p_sOperation + "','"
                   + iCodeId + "','" + p_sUser + "','" + Conversion.GetDateTime(DateTime.Now.ToString()) + "')";
                }
                else
                {
                    sSQL = "INSERT INTO ACTIVITY_LOG(PATIENT_EID," +
                   "EVENT_ID,CLAIM_ID,OPERATION,ACCESS_TYPE_CODE,USER_LOGIN,DTTM_LOG) VALUES(" +
                   "'" + p_iPatientId + "','" + p_sEventId + "','" + p_sClaimId + "','" + p_sOperation + "','"
                   + iCodeId + "','" + p_sUser + "','" + Conversion.GetDateTime(DateTime.Now.ToString()) + "')";
                }               
				objConn.ExecuteNonQuery (sSQL);
			}
			catch(RMAppException p_objEx)
			{
				throw p_objEx ;
			}
			catch(Exception p_objEx)
			{
				throw new RMAppException(Globalization.GetString("HipaaLog.LogHippaInfo.Error", m_iClientId), p_objEx);//rkaur27
			}
			finally
			{
				if (objConn != null)
				{
					objConn.Close ();
					objConn.Dispose ();
				}
                if (objCache != null)
                objCache.Dispose();
                objCache=null;
			}
		}

	}
}
