﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Riskmaster.Db;
using Riskmaster.Security;
using System.Xml;
using Riskmaster.Common;
using Riskmaster.DataModel;
using Riskmaster.ExceptionTypes;
using System.Collections.Specialized;

namespace Riskmaster.Application.ImagRightWrapper
{
    public class DocumentMap
    {
        private UserLogin m_oUserLogin;
        private string m_sConnectionString;
        private string m_sUserId;
        private string m_sPsswd;
        private string m_sDsn;
        int m_iClientId;

        private Dictionary<string, string> dictFields = new Dictionary<string, string>()
        {
            {"IrDocMapId","IR_DOC_MAP_ID"},
            {"RmaDocClass","RMA_DOC_CLASS"},
            {"RmaDocCat","RMA_DOC_CAT"},
            {"RmaDocType","RMA_DOC_TYPE"},
            {"IrFolder","IR_FOLDER"},
            {"IrDocType","IR_DOC_TYPE"},
            {"IrDesc","IR_DESC"},
            {"IrDescChk","DOCTYPE_AS_DESC"},
            {"IrDescChkDocName","DOCNAME_AS_DESC"}
        };

        public DocumentMap(UserLogin p_oUserLoging, int p_iClientId)
        {
            m_oUserLogin = p_oUserLoging;
            m_sConnectionString = p_oUserLoging.objRiskmasterDatabase.ConnectionString;
            m_sDsn = p_oUserLoging.objUser.Dsn;
            m_sPsswd = p_oUserLoging.Password;
            m_sUserId = p_oUserLoging.LoginName;
            m_iClientId = p_iClientId;
        }
        public XmlDocument Get()
        {
            string sSql = string.Empty;
            XmlDocument xDoc = null;
            XmlNode xMaps = null;
            XmlNode xMap = null;
            int iCodeId = 0;
            int iLangCode = 0;
            LocalCache oCache = null;
            bool bSuccess = false;
            try
            {
                sSql = "SELECT * FROM IR_DOC_MAP";
                oCache = new LocalCache(m_sConnectionString, m_iClientId);
                iLangCode = Conversion.CastToType<int>(oCache.LanguageCode, out bSuccess);

                xDoc = new XmlDocument();
                xMaps = xDoc.CreateNode(XmlNodeType.Element, "Mappings",string.Empty);
                using (DbReader oReader = DbFactory.ExecuteReader(m_sConnectionString,sSql))
                {
                    if (oReader!=null)
                    {
                        while (oReader.Read())
                        {
                            xMap = xDoc.CreateNode(XmlNodeType.Element, "Map", string.Empty);
                            if (oReader["IR_DOC_MAP_ID"] != null && oReader["IR_DOC_MAP_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("IrDocMapId", Convert.ToString(oReader["IR_DOC_MAP_ID"]), xMap, xDoc);
                            }
                            if (oReader["RMA_DOC_CLASS"] != null && oReader["RMA_DOC_CLASS"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["RMA_DOC_CLASS"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("RmaDocClass", oCache.GetCodeDesc(iCodeId, iLangCode), xMap, xDoc);
                                }
                            }
                            if (oReader["RMA_DOC_CAT"] != null && oReader["RMA_DOC_CAT"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["RMA_DOC_CAT"].ToString(), out bSuccess);
                                if (iCodeId>0)
                                {
                                    CreateAndAppendNode("RmaDocCat", oCache.GetCodeDesc(iCodeId, iLangCode), xMap, xDoc);
                                }
                            }
                            if (oReader["RMA_DOC_TYPE"] != null && oReader["RMA_DOC_TYPE"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["RMA_DOC_TYPE"].ToString(), out bSuccess);
                                if (iCodeId>0)
                                {
                                    CreateAndAppendNode("RmaDocType", oCache.GetCodeDesc(iCodeId, iLangCode), xMap, xDoc);
                                }
                                
                            }
                            if (oReader["IR_FOLDER"] != null && oReader["IR_FOLDER"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["IR_FOLDER"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("IrFolder", oCache.GetCodeDesc(iCodeId, iLangCode), xMap, xDoc);
                                }
                            }
                            if (oReader["IR_DOC_TYPE"] != null && oReader["IR_DOC_TYPE"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["IR_DOC_TYPE"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("IrDocType", oCache.GetCodeDesc(iCodeId, iLangCode), xMap, xDoc);
                                }
                            }
                            
                            if (oReader["DOCTYPE_AS_DESC"] != null && oReader["DOCTYPE_AS_DESC"] != DBNull.Value && oReader["DOCNAME_AS_DESC"] != null && oReader["DOCNAME_AS_DESC"] != DBNull.Value)
                            {
                                if (string.Compare(Convert.ToString(oReader["DOCTYPE_AS_DESC"]), "-1")==0)
                                {
                                    CreateAndAppendNode("IrDesc", "[Document Type]", xMap, xDoc);
                                }
                                else if (string.Compare(Convert.ToString(oReader["DOCNAME_AS_DESC"]), "-1") == 0)
                                {
                                    CreateAndAppendNode("IrDesc", "[Document Name]", xMap, xDoc);
                                }
                                else
                                {
                                    if (oReader["IR_DESC"] != null && oReader["IR_DESC"] != DBNull.Value)
                                    {
                                        CreateAndAppendNode("IrDesc", Convert.ToString(oReader["IR_DESC"]), xMap, xDoc);
                                    }
                                }
                            }
                            else
                            {
                                if (oReader["IR_DESC"] != null && oReader["IR_DESC"] != DBNull.Value)
                                {
                                    CreateAndAppendNode("IrDesc", Convert.ToString(oReader["IR_DESC"]), xMap, xDoc);
                                }
                            }

                            xMaps.AppendChild(xMap);
                        }
                        xDoc.AppendChild(xMaps);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw;
            }
            return xDoc;
        }

        public XmlDocument Get(string p_sIrDocMapId)
        {
            bool bSuccess = false;
            int iDocMapId = Conversion.CastToType<int>(p_sIrDocMapId, out bSuccess);

            if (iDocMapId <=0)
            {
                throw new RMAppException(Globalization.GetString("DocumentMap.InvalidIRDocMapId", m_iClientId));
            }
            return Get(iDocMapId);
        }
        public XmlDocument Get(int p_iIrDocMapId)
        {
            string sSql = string.Empty;
            XmlDocument xDoc = null;
            XmlNode xMaps = null;
            XmlNode xMap = null;
            int iCodeId = 0;
            int iLangCode = 0;
            LocalCache oCache = null;
            bool bSuccess = false;
            try
            {
                sSql = "SELECT * FROM IR_DOC_MAP WHERE IR_DOC_MAP_ID = " + p_iIrDocMapId;
                oCache = new LocalCache(m_sConnectionString, m_iClientId);
                iLangCode = Conversion.CastToType<int>(oCache.LanguageCode, out bSuccess);

                xDoc = new XmlDocument();
                xMaps = xDoc.CreateNode(XmlNodeType.Element, "Mappings", string.Empty);
                using (DbReader oReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    if (oReader != null)
                    {
                        while (oReader.Read())
                        {
                            xMap = xDoc.CreateNode(XmlNodeType.Element, "Map", string.Empty);
                            if (oReader["IR_DOC_MAP_ID"] != null && oReader["IR_DOC_MAP_ID"] != DBNull.Value)
                            {
                                CreateAndAppendNode("IrDocMapId", Convert.ToString(oReader["IR_DOC_MAP_ID"]), xMap, xDoc);
                            }
                            if (oReader["RMA_DOC_CLASS"] != null && oReader["RMA_DOC_CLASS"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["RMA_DOC_CLASS"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("RmaDocClass", oCache.GetCodeDesc(iCodeId, iLangCode), xMap, xDoc);
                                }
                            }
                            if (oReader["RMA_DOC_CAT"] != null && oReader["RMA_DOC_CAT"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["RMA_DOC_CAT"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("RmaDocCat", oCache.GetCodeDesc(iCodeId, iLangCode), "codeid", iCodeId.ToString(), xMap, xDoc);
                                }
                            }
                            if (oReader["RMA_DOC_TYPE"] != null && oReader["RMA_DOC_TYPE"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["RMA_DOC_TYPE"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("RmaDocType", oCache.GetCodeDesc(iCodeId, iLangCode), "codeid", iCodeId.ToString(), xMap, xDoc);
                                }

                            }
                            if (oReader["IR_FOLDER"] != null && oReader["IR_FOLDER"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["IR_FOLDER"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("IrFolder", oCache.GetCodeDesc(iCodeId, iLangCode), "codeid", iCodeId.ToString(), xMap, xDoc);
                                }
                            }
                            if (oReader["IR_DOC_TYPE"] != null && oReader["IR_DOC_TYPE"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["IR_DOC_TYPE"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    CreateAndAppendNode("IrDocType", oCache.GetCodeDesc(iCodeId, iLangCode), "codeid", iCodeId.ToString(), xMap, xDoc);
                                }
                            }
                            if (oReader["IR_DESC"] != null && oReader["IR_DESC"] != DBNull.Value)
                            {
                                CreateAndAppendNode("IrDesc", Convert.ToString(oReader["IR_DESC"]), xMap, xDoc);
                            }
                            if (oReader["DOCTYPE_AS_DESC"] != null && oReader["DOCTYPE_AS_DESC"] != DBNull.Value)
                            {
                                if (string.Compare(Convert.ToString(oReader["DOCTYPE_AS_DESC"]), "-1")==0)
                                {
                                    CreateAndAppendNode("IrDescChk", "True", xMap, xDoc);
                                }
                                else
                                {
                                    CreateAndAppendNode("IrDescChk", "False", xMap, xDoc);
                                }
                                
                            }
                            if (oReader["DOCNAME_AS_DESC"] != null && oReader["DOCNAME_AS_DESC"] != DBNull.Value)
                            {
                                if (string.Compare(Convert.ToString(oReader["DOCNAME_AS_DESC"]), "-1") == 0)
                                {
                                    CreateAndAppendNode("IrDescChkDocName", "True", xMap, xDoc);
                                }
                                else
                                {
                                    CreateAndAppendNode("IrDescChkDocName", "False", xMap, xDoc);
                                }

                            }

                            xMaps.AppendChild(xMap);
                        }
                        xDoc.AppendChild(xMaps);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw;
            }
            return xDoc;
        }


        public bool Get(int p_iIrDocMapId, string p_sRMADocType, string p_sDocName, ref string p_sIrFolder, ref string p_sIrDocType, ref string p_sDescription)
        {
            string sSql = string.Empty;
            int iCodeId = 0;
            int iLangCode = 0;
            LocalCache oCache = null;
            bool bSuccess = false;
            bool bReturnValue = false;
            int irmADocType = 0;
            try
            {
                sSql = "SELECT * FROM IR_DOC_MAP WHERE IR_DOC_MAP_ID = " + p_iIrDocMapId;
                oCache = new LocalCache(m_sConnectionString, m_iClientId);
                iLangCode = Conversion.CastToType<int>(oCache.LanguageCode, out bSuccess);

                using (DbReader oReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    if (oReader != null)
                    {
                        if (oReader.Read())
                        {
                            bReturnValue = true;
                            if (oReader["IR_FOLDER"] != null && oReader["IR_FOLDER"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["IR_FOLDER"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    p_sIrFolder = oCache.GetCodeDesc(iCodeId, iLangCode);
                                }
                            }
                            if (oReader["IR_DOC_TYPE"] != null && oReader["IR_DOC_TYPE"] != DBNull.Value)
                            {
                                iCodeId = Conversion.CastToType<int>(oReader["IR_DOC_TYPE"].ToString(), out bSuccess);
                                if (iCodeId > 0)
                                {
                                    p_sIrDocType = oCache.GetCodeDesc(iCodeId, iLangCode);
                                }
                            }
                            if (oReader["DOCTYPE_AS_DESC"] != null && oReader["DOCTYPE_AS_DESC"] != DBNull.Value && oReader["DOCNAME_AS_DESC"] != null && oReader["DOCNAME_AS_DESC"] != DBNull.Value)
                            {
                                if (string.Compare(Convert.ToString(oReader["DOCTYPE_AS_DESC"]), "-1") == 0)
                                {
                                    irmADocType = Conversion.CastToType<int>(p_sRMADocType, out bSuccess);
                                    p_sDescription = oCache.GetCodeDesc(irmADocType);
                                }
                                else if (string.Compare(Convert.ToString(oReader["DOCNAME_AS_DESC"]), "-1") == 0)
                                {
                                    p_sDescription = p_sDocName;
                                }
                                else
                                {
                                    if (oReader["IR_DESC"] != null && oReader["IR_DESC"] != DBNull.Value)
                                    {
                                        p_sDescription = Convert.ToString(oReader["IR_DESC"]);
                                    }
                                }
                            }
                            

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw;
            }
            return bReturnValue;
        }

        public bool Get(string p_sDocClass, string p_sDoctype, string p_sDocCategory, string p_sFileExtension, string p_sDocName, ref string p_sIrFolder, ref string p_sIrDocType, ref string p_sDescription)
        {
            int iIrDocMapId = 0;
            bool bSuccess = false;

            //Document type is required field.
            //1. Try with Doc category, type and class
            //2. Try with Doc Class, type
            //3. Try with Doc Category, type
            //4. Try with Doc Type
            iIrDocMapId = DbFactory.ExecuteAsType<int>(m_sConnectionString,
                string.Format("SELECT IR_DOC_MAP_ID FROM IR_DOC_MAP WHERE RMA_DOC_TYPE={0} AND RMA_DOC_CLASS={1} AND RMA_DOC_CAT={2}", p_sDoctype, p_sDocClass, p_sDocCategory));
            
            if (iIrDocMapId == 0)
            {
                iIrDocMapId = DbFactory.ExecuteAsType<int>(m_sConnectionString,
                    string.Format("SELECT IR_DOC_MAP_ID FROM IR_DOC_MAP WHERE RMA_DOC_TYPE={0} AND RMA_DOC_CLASS={1} AND RMA_DOC_CAT={2}", p_sDoctype, p_sDocClass, 0));
            }
            if (iIrDocMapId == 0)
            {
                iIrDocMapId = DbFactory.ExecuteAsType<int>(m_sConnectionString,
                    string.Format("SELECT IR_DOC_MAP_ID FROM IR_DOC_MAP WHERE RMA_DOC_TYPE={0} AND RMA_DOC_CLASS={1} AND RMA_DOC_CAT={2}", p_sDoctype, 0, p_sDocCategory));
            }
            if (iIrDocMapId == 0)
            {
                iIrDocMapId = DbFactory.ExecuteAsType<int>(m_sConnectionString,
                    string.Format("SELECT IR_DOC_MAP_ID FROM IR_DOC_MAP WHERE RMA_DOC_TYPE={0} AND RMA_DOC_CLASS={1} AND RMA_DOC_CAT={2}", p_sDoctype, 0, 0));
            }
            if (iIrDocMapId>0)
            {
                bSuccess = Get(iIrDocMapId, p_sDoctype, p_sDocName, ref p_sIrFolder, ref p_sIrDocType, ref p_sDescription);
            }
            else
            {
                throw new RMAppException(Globalization.GetString("DocumentMap.ImportFail", m_iClientId));
            }

            return bSuccess;
        }

        public void Save(XmlDocument p_objXMLIn)
        {
            DbWriter oWriter = null;
            XmlNode xQueryBuider = null;
            DataModelFactory oFactory = null;
            int iId = 0;
            XmlNode xId = null;
            bool bSuccess = false;

            string p_sErrorMsg = string.Empty;
            try
            {
                if (!Validate(p_objXMLIn, ref p_sErrorMsg))
                {
                    throw new RMAppException(p_sErrorMsg);
                }

                oWriter = DbFactory.GetDbWriter(m_sConnectionString);
                oWriter.Tables.Add("IR_DOC_MAP");

                foreach (var item in dictFields)
                {
                    if (string.Compare(item.Key,"IrDocMapId",true)==0)
                    {
                        continue;
                    }
                    xQueryBuider = p_objXMLIn.SelectSingleNode("//" + item.Key);
                    if (xQueryBuider != null)
                    {
                        if (xQueryBuider.Attributes["codeid"] != null)
                        {
                            if (!string.IsNullOrEmpty(xQueryBuider.Attributes["codeid"].Value))
                            {
                                oWriter.Fields.Add(item.Value, xQueryBuider.Attributes["codeid"].Value);
                            }
                        }
                        else if (xQueryBuider.Attributes["type"] != null && string.Compare(xQueryBuider.Attributes["type"].Value, "date", false) == 0)
                        {
                            oWriter.Fields.Add(item.Value, Conversion.GetDate(xQueryBuider.InnerText));
                        }
                        else if (string.Compare(item.Key, "IrDescChk", true) == 0)
                        {
                            oWriter.Fields.Add(item.Value, (string.Compare(xQueryBuider.InnerText, "True", true) == 0 ? "-1" : "0"));
                        }
                        else if (string.Compare(item.Key, "IrDescChkDocName", true) == 0)
                        {
                            oWriter.Fields.Add(item.Value, (string.Compare(xQueryBuider.InnerText, "True", true) == 0 ? "-1" : "0"));
                        }
                        else
                        {
                            oWriter.Fields.Add(item.Value, xQueryBuider.InnerText);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(oWriter.Sql))
                {
                    xId = p_objXMLIn.SelectSingleNode("//IrDocMapId");
                    if (xId!= null)
                    {
                        iId = Conversion.CastToType<int>(xId.InnerText, out bSuccess);
                    }
                    if (iId>0)
                    {
                        oWriter.Where.Add("IR_DOC_MAP_ID='" + iId + "'");
                    }
                    else
                    {
                        oFactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                        iId = oFactory.Context.GetNextUID("IR_DOC_MAP");
                        oWriter.Fields.Add("IR_DOC_MAP_ID", iId);
                    }
                    oWriter.Execute();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw;
            }
            finally
            {
                if (oFactory != null)
                {
                    oFactory.Dispose();
                }
            }
            
        }

        /// <summary>
        /// deletes current row returns true if docmapid has a value
        /// changed by vchouhan6
        /// </summary>
        /// <param name="p_sId"></param>
        /// <returns>bool</returns>
        public bool Delete(string p_sId)
        {
            bool bSuccess = false;
            int iDocMapId = 0;
            try
            {
                iDocMapId = Conversion.CastToType<int>(p_sId, out bSuccess);
                if (iDocMapId > 0)
                {
                    DbFactory.ExecuteNonQuery(m_sConnectionString, string.Format("DELETE FROM IR_DOC_MAP WHERE IR_DOC_MAP_ID={0}", iDocMapId));
                    bSuccess = true;
                }
            }
            catch (Exception ex)
            {
                bSuccess = false;
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw;
            }

            return bSuccess;
        }

        //Old delete function returning void
        //public void Delete(string p_sId)
        //{
        //    bool bSuccess = false;
        //    int iDocMapId = 0;
        //    try
        //    {
        //        iDocMapId = Conversion.CastToType<int>(p_sId, out bSuccess);
        //        if (iDocMapId > 0)
        //        {
        //            DbFactory.ExecuteNonQuery(m_sConnectionString, string.Format("DELETE FROM IR_DOC_MAP WHERE IR_DOC_MAP_ID={0}", iDocMapId));
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT);
        //        throw;
        //    }
        //}
        
        private bool Validate(XmlDocument p_objXmlIn, ref string p_sErrorMsg)
        {
            XmlNode xrmaDocClass = null;
            XmlNode xrmaDocCat = null;
            XmlNode xrmaDocType = null;
            XmlNode xIrMapId = null;
            int irmADocClass = 0;
            int irmADocType = 0;
            int irmADocCat = 0;
            bool bValid = false;
            bool bConversionSuccess = false;
            int iIrMapIdFromTable = 0;
            int iIrMapIdFromXml = 0;
            try
            {
                xrmaDocCat = p_objXmlIn.SelectSingleNode("//RmaDocClass");
                xrmaDocClass = p_objXmlIn.SelectSingleNode("//RmaDocCat");
                xrmaDocType = p_objXmlIn.SelectSingleNode("//RmaDocType");
                xIrMapId = p_objXmlIn.SelectSingleNode("//IrDocMapId");
                if (xIrMapId!=null)
                {
                    iIrMapIdFromXml = Conversion.CastToType<int>(p_objXmlIn.SelectSingleNode("//IrDocMapId").InnerText, out bConversionSuccess);
                }

                if (xrmaDocClass != null && xrmaDocCat != null && xrmaDocType != null
                    && xrmaDocClass.Attributes["codeid"] != null && xrmaDocCat.Attributes["codeid"] != null && xrmaDocType.Attributes["codeid"] != null)
                {
                    irmADocClass = Conversion.CastToType<int>(xrmaDocClass.Attributes["codeid"].Value, out bConversionSuccess);
                    irmADocCat = Conversion.CastToType<int>(xrmaDocCat.Attributes["codeid"].Value, out bConversionSuccess);
                    irmADocType = Conversion.CastToType<int>(xrmaDocType.Attributes["codeid"].Value, out bConversionSuccess);

                    iIrMapIdFromTable = DbFactory.ExecuteAsType<int>(m_sConnectionString, string.Format("SELECT IR_DOC_MAP_ID FROM IR_DOC_MAP WHERE RMA_DOC_CAT={0} AND RMA_DOC_TYPE={1} AND RMA_DOC_CLASS={2}", irmADocCat, irmADocType, irmADocClass));
                    if (iIrMapIdFromTable > 0 && iIrMapIdFromTable != iIrMapIdFromXml)
                    {
                        p_sErrorMsg = Globalization.GetString("DocumentMap.RecordAlreadyExist", m_iClientId);
                        bValid = false;
                    }
                    else
                    {
                        bValid = true;
                    }
                }
                else
                {
                    p_sErrorMsg = Globalization.GetString("DocumentMap.MalformedXMLFound", m_iClientId);
                    bValid = false;
                    Log.Write("MalFormed Xml In DocumentMap.Validate: " + p_objXmlIn.OuterXml, Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                }
            }
            catch (Exception ex)
            {
                bValid = false;
                p_sErrorMsg = Globalization.GetString("DocumentMap.MalformedXMLFound", m_iClientId); 
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
            }
            return bValid;
        }

        public void GetUserData(int p_iClaimId, Claim p_objClaim, FUPFileMappings.UserDataType oUserDataType, ref string p_sUserdata1, ref string p_sUserdata2, ref string p_sUserdata3, ref string p_sUserdata4, 
            ref string p_sUserdata5, ref string p_sErrorMsg)
        {
            string sSql = string.Empty;
            string sId = string.Empty;
            string sTableName = string.Empty;
            string sFieldName = string.Empty;
            string sValue = string.Empty;
            bool bComplete = false;
            string sUserDataName = string.Empty;
            string sUseNameInFUPUserData = string.Empty;

            DataModelFactory ofactory = null;
            Claim oClaim = null;
            try
            {
                if (p_objClaim==null)
                {
                    ofactory = new DataModelFactory(m_oUserLogin, m_iClientId);
                    oClaim = ofactory.GetDataModelObject("Claim", false) as Claim;
                    oClaim.MoveTo(p_iClaimId);
                }
                else
                {
                    oClaim = p_objClaim;
                }

                sSql = " SELECT * FROM IR_USER_DATA_MAPPING WHERE USER_DATA_TYPE = " + ((int)oUserDataType) + " ORDER BY IR_USER_DATA_MAPPING_ID ASC ";
                using (DbReader oDbReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
                {
                    while (oDbReader.Read())
                    {
                        if (oDbReader["USER_DATA"] != null && oDbReader["USER_DATA"] != DBNull.Value)
                        {
                            sId = Convert.ToString(oDbReader["USER_DATA"]);
                        }
                        if (oDbReader["IR_NAME"] != null && oDbReader["IR_NAME"] != DBNull.Value)
                        {
                            sUserDataName = Convert.ToString(oDbReader["IR_NAME"]);
                        }
                        if (oDbReader["TABLE_NAME"] != null && oDbReader["TABLE_NAME"] != DBNull.Value)
                        {
                            sTableName = Convert.ToString(oDbReader["TABLE_NAME"]);
                        }
                        if (oDbReader["FIELD_NAME"] != null && oDbReader["FIELD_NAME"] != DBNull.Value)
                        {
                            sFieldName = Convert.ToString(oDbReader["FIELD_NAME"]);
                        }

                        switch (sTableName.ToUpper())
                        {
                            case "CLAIM":
                                sValue = GetClaimValue(oClaim, sFieldName);
                                break;
                            case "EVENT":
                                sValue = GetEventValue(oClaim, sFieldName);
                                break;
                            case "CURRENT ADJUSTER":
                                sValue = GetAdjusterValue(oClaim, sFieldName);
                                break;
                            case "PRIMARY CLAIMANT":
                                sValue = GetClaimantValue(oClaim, sFieldName);
                                break;
                            case "POLICY (PRIMARY/FIRST)":
                                sValue = GetPolicyValue(oClaim, sFieldName);
                                break;
                            case "RESERVE":
                                sValue = GetReserveValue(oClaim, sFieldName);
                                break;
                            case "CLAIM SUPPLEMENTAL":
                                sValue = GetClaimSuppValue(oClaim, sFieldName);
                                break;
                            case "EVENT SUPPLEMENTAL":
                                sValue = GetEventSuppValue(oClaim, sFieldName);
                                break;
                            default:
                                sValue = string.Empty;
                                break;
                        }

                        //If UserDataType is FUP then add Name to user data
                        sUseNameInFUPUserData = GetImageRightConfigValue("UseNameInFUPUserData");
                        if (oUserDataType == FUPFileMappings.UserDataType.FUP && string.Compare(sUseNameInFUPUserData, "true", true) == 0)
                        {
                            sValue = string.Concat(sUserDataName, "=", sValue);
                        }

                        switch (sId.ToUpper())
                        {
                            case "USERDATA1":
                                if (string.IsNullOrEmpty(p_sUserdata1))
                                {
                                    p_sUserdata1 = sValue;
                                }
                                else
                                {
                                    p_sUserdata1 = string.Concat(p_sUserdata1, "~|~", sValue);
                                }
                                break;
                            case "USERDATA2":
                                if (string.IsNullOrEmpty(p_sUserdata2))
                                {
                                    p_sUserdata2 = sValue;
                                }
                                else
                                {
                                    p_sUserdata2 = string.Concat(p_sUserdata2, "~|~", sValue);
                                }
                                break;
                            case "USERDATA3":
                                if (string.IsNullOrEmpty(p_sUserdata3))
                                {
                                    p_sUserdata3 = sValue;
                                }
                                else
                                {
                                    p_sUserdata3 = string.Concat(p_sUserdata3, "~|~", sValue);
                                }
                                break;
                            case "USERDATA4":
                                if (string.IsNullOrEmpty(p_sUserdata4))
                                {
                                    p_sUserdata4 = sValue;
                                }
                                else
                                {
                                    p_sUserdata4 = string.Concat(p_sUserdata4, "~|~", sValue);
                                }
                                break;
                            case "USERDATA5":
                                if (string.IsNullOrEmpty(p_sUserdata5))
                                {
                                    p_sUserdata5 = sValue;
                                }
                                else
                                {
                                    p_sUserdata5 = string.Concat(p_sUserdata5, "~|~", sValue);
                                }
                                break;
                            default:
                                bComplete = true;
                                break;
                        }
                        if (bComplete)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                p_sErrorMsg = Globalization.GetString("DocumentMap.GetUserDataException", m_iClientId); 
                Log.Write(ex.ToString(), Log.LOG_CATEGORY_DEFAULT,m_iClientId);
                throw;
            }
            finally
            {
                if (oClaim!=null)
                {
                    oClaim.Dispose();
                }
                if (ofactory != null)
                {
                    ofactory.Dispose();
                }
            }
        }

        private void CreateAndAppendNode(string p_sName, string p_sValue, XmlNode p_xBaseNode, XmlDocument p_xDoc)
        {
            XmlNode xNode = p_xDoc.CreateNode(XmlNodeType.Element, p_sName, string.Empty);
            xNode.InnerText = p_sValue;
            p_xBaseNode.AppendChild(xNode);
        }
        private void CreateAndAppendNode(string p_sName, string p_sValue, string p_sAttrName, string p_sAttrValue, XmlNode p_xBaseNode, XmlDocument p_xDoc)
        {
            XmlNode xNode = p_xDoc.CreateNode(XmlNodeType.Element, p_sName, string.Empty);
            xNode.InnerText = p_sValue;
            ((XmlElement)xNode).SetAttribute(p_sAttrName, p_sAttrValue);
            p_xBaseNode.AppendChild(xNode);
        }

        private string GetClaimValue(Claim p_oClaim, string p_sFieldName)
        {
            string sReturnVal = string.Empty;
            switch (p_sFieldName.ToUpper())
            {
                case "[CLAIM NUMBER]":
                    sReturnVal = p_oClaim.ClaimNumber;
                    break;
                case "[DATE OF CLAIM]":
                    sReturnVal = Conversion.GetDBDateFormat(p_oClaim.DateOfClaim,"MM/dd/yyyy");
                    break;
                default:
                    break;
            }
            
            return sReturnVal;
        }

        private string GetEventValue(Claim p_oClaim, string p_sFieldName)
        {
            string sReturnVal = string.Empty;
            switch (p_sFieldName.ToUpper())
            {
                case "[EVENT NUMBER]":
                    sReturnVal = p_oClaim.EventNumber;
                    break;
                case "[DATE OF EVENT]":
                    p_oClaim.LoadParent();
                    sReturnVal = Conversion.GetDBDateFormat(((Event)p_oClaim.Parent).DateOfEvent, "MM/dd/yyyy");
                    break;
                case "[DEPARTMENT]":
                    LocalCache oLocalCache = new LocalCache(m_sConnectionString, m_iClientId);
                    string sAbbreviation = string.Empty;
                    p_oClaim.LoadParent();
                    oLocalCache.GetOrgInfo(((Event)p_oClaim.Parent).DeptEid, ref sAbbreviation, ref sReturnVal);

                    oLocalCache.Dispose();
                    break;
                default:
                    break;
            }
            return sReturnVal;
        }

        private string GetAdjusterValue(Claim p_oClaim, string p_sFieldName)
        {
            string sSql = string.Empty;
            string sReturnVal = string.Empty;

            ClaimAdjuster oAdjuster = p_oClaim.CurrentAdjuster;
            if (oAdjuster!=null)
            {
                switch (p_sFieldName.ToUpper())
                {
                    case "[LAST NAME]":
                        sReturnVal = oAdjuster.AdjusterEntity.LastName;
                        break;
                    case "[LASTNAME], [FIRSTNAME]":
                        sReturnVal = (oAdjuster.AdjusterEntity.LastName + ", " + oAdjuster.AdjusterEntity.FirstName).Trim();
                        break;
                    case "[EXAMINERCODE] [NAME]":
                        //tanwar2 - reference number moved to entity from 14.1 CHP1
                        //sReturnVal = (oAdjuster.ReferenceNumber + " " + (oAdjuster.AdjusterEntity.FirstName + " " + oAdjuster.AdjusterEntity.LastName).Trim()).Trim();
                        sReturnVal = (oAdjuster.AdjusterEntity.ReferenceNumber + " " + (oAdjuster.AdjusterEntity.FirstName + " " + oAdjuster.AdjusterEntity.LastName).Trim()).Trim();
                        break;
                    default:
                        break;
                }

                oAdjuster.Dispose();
            }
            
            return sReturnVal;
        }

        private string GetClaimantValue(Claim p_oClaim, string p_sFieldName)
        {
            string sSql = string.Empty;
            string sReturnVal = string.Empty;
            int iLangCode = 0;
            bool bSuccess = false;
            Claimant oClaimant = null;

            oClaimant = p_oClaim.PrimaryClaimant;
            
            if (oClaimant == null)
            {
                int iClmntRowId = Int32.MaxValue;
                foreach (Claimant oClmnt in p_oClaim.ClaimantList)
                {
                    if (oClmnt.ClaimantRowId < iClmntRowId)
                    {
                        iClmntRowId = oClmnt.ClaimantRowId;
                    }
                }

                if (iClmntRowId<Int32.MaxValue)
                {
                    oClaimant = p_oClaim.ClaimantList[iClmntRowId];
                }
            }
            if (oClaimant != null)
            {
                LocalCache oCache = new LocalCache(m_sConnectionString, m_iClientId);
                iLangCode = Conversion.CastToType<int>(oCache.LanguageCode, out bSuccess);

                switch (p_sFieldName.ToUpper())
                {
                    case "[LAST NAME]":
                        sReturnVal = oClaimant.ClaimantEntity.LastName;
                        break;
                    case "[LASTNAME], [FIRSTNAME]":
                        sReturnVal = (oClaimant.ClaimantEntity.LastName + ", " + oClaimant.ClaimantEntity.FirstName).Trim();
                        break;
                    case "[CLAIMANT TYPE]":
                        sReturnVal = oCache.GetCodeDesc(oClaimant.ClaimantTypeCode, iLangCode);
                        break;
                    case "[FIRSTNAME] [LASTNAME]":
                        sReturnVal = (oClaimant.ClaimantEntity.FirstName + " " + oClaimant.ClaimantEntity.LastName).Trim();
                        break;
                    default:
                        break;
                }

                oCache.Dispose();
                oClaimant.Dispose();
            }

            return sReturnVal;
        }

        private string GetPolicyValue(Claim p_oClaim, string p_sFieldName)
        {
            string sReturnVal = string.Empty;

            Policy oPolicy = p_oClaim.PrimaryPolicy;

            if (oPolicy != null)
            {
                switch (p_sFieldName.ToUpper())
                {
                    case "[POLICY NAME]":
                        sReturnVal = oPolicy.PolicyName;
                        break;
                    case "[POLICY NUMBER]":
                        sReturnVal = oPolicy.PolicyNumber;
                        break;
                    default:
                        break;
                }

                oPolicy.Dispose();
            }
            
            return sReturnVal;
        }

        private string GetReserveValue(Claim p_oClaim, string p_sFieldName)
        {
            string sReturnVal = string.Empty;
            double dExpAmount = 0d;
            double dExpBalance = 0d;
            double dIndAmount = 0d;
            double dIndBalance = 0d;
            bool bMultiCvgClaim = false;
            bool bAdd = false;
            int iClaimantEid = 0;

            int iLossTypeReserveCode = p_oClaim.Context.LocalCache.GetCodeId("I", "MASTER_RESERVE");
            int iExpTypeReserveCode = p_oClaim.Context.LocalCache.GetCodeId("E", "MASTER_RESERVE");
            int iRelatedReserveTypeCode = 0;

            if (p_oClaim.Context.InternalSettings.SysSettings.MultiCovgPerClm == -1)
            {
                bMultiCvgClaim = true;
                iClaimantEid = p_oClaim.PrimaryClaimant.ClaimantEid;
            }
            foreach (ReserveCurrent oReserveCurrent in p_oClaim.ReserveCurrentList)
            {
                bAdd = false;
                iRelatedReserveTypeCode = p_oClaim.Context.LocalCache.GetRelatedCodeId(oReserveCurrent.ReserveTypeCode);

                if (bMultiCvgClaim)
                {
                    if (oReserveCurrent.ClaimantEid == iClaimantEid)
                    {
                        bAdd = true;
                    }
                }
                else
                {
                    bAdd = true;
                }

                if (bAdd)
                {
                    if ( iRelatedReserveTypeCode == iExpTypeReserveCode )
                    {
                        dExpAmount += oReserveCurrent.ReserveAmount;
                        dExpBalance += oReserveCurrent.BalanceAmount;
                    }
                    else if (iRelatedReserveTypeCode == iLossTypeReserveCode)
                    {
                        dIndAmount += oReserveCurrent.ReserveAmount;
                        dIndBalance += oReserveCurrent.BalanceAmount;
                    }
                }
            }

            switch (p_sFieldName.ToUpper())
            {
                case "LOSS [AMOUNT] EXP [AMOUNT]":
                    sReturnVal = string.Format("LOSS {0:c} EXP {1:c}", dIndAmount, dExpAmount);
                    break;
                case "LOSS [BALANCE] EXP [BALANCE]":
                    sReturnVal = string.Format("LOSS {0:c} EXP {1:c}", dIndBalance, dExpBalance);
                    break;
                case "[BALANCE]":
                    sReturnVal = string.Format("{0:c}", (dIndBalance + dExpBalance));
                    break;
                case "AMOUNT":
                    sReturnVal = string.Format("{0:c}", (dIndAmount + dExpAmount));
                    break;
                default:
                    break;
            }


            return sReturnVal;
        }

        private string GetClaimSuppValue(Claim p_oClaim, string p_sFieldName)
        {
            string sReturnVal = string.Empty;
            LocalCache oCache = null;
            string sSql = string.Format("SELECT CODE_FILE_ID, FIELD_TYPE FROM SUPP_DICTIONARY WHERE SYS_FIELD_NAME LIKE '{0}' AND SUPP_TABLE_NAME LIKE 'CLAIM_SUPP'", p_sFieldName);
            int iTableId = 0;
            int iCodeId = 0;
            int iLangCode = 0;
            bool bSuccess = false;
            SupplementalFieldTypes oFieldType = new SupplementalFieldTypes();

            using (DbReader oDbReader = DbFactory.ExecuteReader(m_sConnectionString,sSql))
            {
                if (oDbReader!=null && oDbReader.Read())
                {
                    if (oDbReader["CODE_FILE_ID"] != null && oDbReader["CODE_FILE_ID"] != DBNull.Value)
                    {
                        iTableId = Conversion.CastToType<int>(oDbReader["CODE_FILE_ID"].ToString(), out bSuccess);
                    }
                    if (oDbReader["FIELD_TYPE"] != null && oDbReader["FIELD_TYPE"] != DBNull.Value)
                    {
                        oFieldType = (SupplementalFieldTypes) Conversion.CastToType<int>(oDbReader["FIELD_TYPE"].ToString(), out bSuccess);
                    }
                }
            }
            if (p_oClaim.Supplementals[p_sFieldName] != null)
            {
                object oValue = p_oClaim.Supplementals[p_sFieldName].Value;
                if (iTableId > 0)
                {
                    oCache = new LocalCache(m_sConnectionString, m_iClientId);
                    iCodeId = Conversion.ConvertObjToInt(oValue, m_iClientId);
                    iLangCode = Conversion.CastToType<int>(oCache.LanguageCode, out bSuccess);
                    sReturnVal = oCache.GetCodeDesc(iCodeId, iLangCode);
                }
                else
                {
                    switch (oFieldType)
                    {
                        case SupplementalFieldTypes.SuppTypeCurrency:
                            sReturnVal = string.Format("{0:c}", Conversion.ConvertObjToDouble(oValue, m_iClientId));
                            break;
                        case SupplementalFieldTypes.SuppTypeDate:
                            sReturnVal = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(oValue), "MM/dd/yyyy");
                            break;
                        default:
                            sReturnVal = Conversion.ConvertObjToStr(oValue).Replace("\r","").Replace("\n","");
                            break;
                    }
                    
                }
            }

            if (oCache!=null)
            {
                oCache.Dispose();
            }
            return sReturnVal;
        }

        private string GetEventSuppValue(Claim p_oClaim, string p_sFieldName)
        {
            string sReturnVal = string.Empty;
            LocalCache oCache = null;
            string sSql = string.Format("SELECT CODE_FILE_ID, FIELD_TYPE FROM SUPP_DICTIONARY WHERE SYS_FIELD_NAME LIKE '{0}' AND SUPP_TABLE_NAME LIKE 'EVENT_SUPP'", p_sFieldName);
            int iTableId = 0;
            int iCodeId = 0;
            int iLangCode = 0;
            bool bSuccess = false;

            p_oClaim.LoadParent();

            SupplementalFieldTypes oFieldType = new SupplementalFieldTypes();
            using (DbReader oDbReader = DbFactory.ExecuteReader(m_sConnectionString, sSql))
            {
                if (oDbReader != null && oDbReader.Read())
                {
                    if (oDbReader["CODE_FILE_ID"] != null && oDbReader["CODE_FILE_ID"] != DBNull.Value)
                    {
                        iTableId = Conversion.CastToType<int>(oDbReader["CODE_FILE_ID"].ToString(), out bSuccess);
                    }
                    if (oDbReader["FIELD_TYPE"] != null && oDbReader["FIELD_TYPE"] != DBNull.Value)
                    {
                        oFieldType = (SupplementalFieldTypes)Conversion.CastToType<int>(oDbReader["FIELD_TYPE"].ToString(), out bSuccess);
                    }
                }
            }
            if (((Event)p_oClaim.Parent).Supplementals[p_sFieldName] != null)
            {
                object oValue = ((Event)p_oClaim.Parent).Supplementals[p_sFieldName].Value;
                if (iTableId > 0)
                {
                    oCache = new LocalCache(m_sConnectionString, m_iClientId);
                    iCodeId = Conversion.ConvertObjToInt(oValue, m_iClientId);
                    iLangCode = Conversion.CastToType<int>(oCache.LanguageCode, out bSuccess);
                    sReturnVal = oCache.GetCodeDesc(iCodeId, iLangCode);
                }
                else
                {
                    switch (oFieldType)
                    {
                        case SupplementalFieldTypes.SuppTypeCurrency:
                            sReturnVal = string.Format("{0:c}", Conversion.ConvertObjToDouble(oValue, m_iClientId));
                            break;
                        case SupplementalFieldTypes.SuppTypeDate:
                            sReturnVal = Conversion.GetDBDateFormat(Conversion.ConvertObjToStr(oValue), "MM/dd/yyyy");
                            break;
                        default:
                            sReturnVal = Conversion.ConvertObjToStr(oValue).Replace("\r", "").Replace("\n", "");
                            break;
                    }
                }
            }

            if (oCache != null)
            {
                oCache.Dispose();
            }
            return sReturnVal;
        }

        public ClaimantTypeToUse GetClaimantTypeToUse()
        {
            ClaimantTypeToUse oClmntTypeToUse = ClaimantTypeToUse.UsePrimary;

            string sClaimantTypeToUse = GetImageRightConfigValue("ClaimantTypeToUse");
            switch (sClaimantTypeToUse.ToUpper())
            {
                case "PRIMARY":
                    oClmntTypeToUse = ClaimantTypeToUse.UsePrimary;
                    break;
                case "FIRST":
                    oClmntTypeToUse = ClaimantTypeToUse.UseFirst;
                    break;
                case "PRIMARY_OR_FIRST":
                    oClmntTypeToUse = ClaimantTypeToUse.UseFirst;
                    break;
                case "ALL":
                    oClmntTypeToUse = ClaimantTypeToUse.UseAll;
                    break;
                default:
                    break;
            }
            return oClmntTypeToUse;
        }

        private string GetImageRightConfigValue(string p_sKey)
        {
            string sValue = string.Empty;
            NameValueCollection nvCol = RMConfigurationManager.GetNameValueSectionSettings("ImageRight", m_sConnectionString, m_iClientId);
            if (nvCol != null)
            {
                sValue = nvCol[p_sKey];
            }

            return sValue;
        }
    }

    public enum ClaimantTypeToUse
    {
        UsePrimary,
        UseFirst,
        UsePrimaryOrFirst,
        UseAll
    }
}
