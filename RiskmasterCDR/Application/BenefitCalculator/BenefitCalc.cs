﻿using System;
using System.Runtime.InteropServices;
using System.Reflection;

namespace Riskmaster.Application.BenefitCalculator
{
    /// <summary>
    /// Encapsulates the functionality currently provided by 
    /// the legacy COM object RMWCCalc
    /// </summary>
    public class BenefitCalc
    {
        //Private properties
        private RMWCCalc.CWCDisBenCalc m_rmCOMRateCalc;

        private string m_strLoginName = String.Empty;
        private string m_strPassword  = String.Empty;
        private string m_strDSN       = String.Empty;
        private int m_iClientId = 0;
        /// <summary>
        /// Default Class constructor
        /// </summary>
        private BenefitCalc(int p_iClientId)
        {
            this.m_iClientId = p_iClientId;
            //this.DBName     = "RMX_JURIS_RULES";
            //this.DBUserName = "csc";
            //this.DBPassword = "csc";
            //this.m_ClaimList = new ClaimClass();
        }//default constructor

        /// <summary>
        /// OVERLOADED: Class constructor hides/masks the default constructor with no parameters
        /// </summary>
        /// <param name="strLoginName"></param>
        /// <param name="strPassword"></param>
        /// <param name="strDSN"></param>
        /// <param name="intClaimID"></param>
        public BenefitCalc(string strLoginName, string strPassword, string strDSN, int intClaimID, int p_iClientId)
		{
            short sDWFlag          = 0;
            int intJurisTaxCode    = 0;
            int intJurisExemptions = 0;

            //Initialize the private member properties
            m_strLoginName = strLoginName;
            m_strPassword  = strPassword;
            m_strDSN       = strDSN;
            m_iClientId = p_iClientId;
            //Create the Legacy COM object
            m_rmCOMRateCalc = new RMWCCalc.CWCDisBenCalc();

            //Initialize the Legacy COM object
            m_rmCOMRateCalc.InitDisBenCalc(ref strLoginName, ref strPassword, ref strDSN,
                                           ref sDWFlag,      ref intClaimID,  ref intJurisTaxCode,
                                           ref intJurisExemptions, m_iClientId);
        }//class constructor

        #region Unused code
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="strDBName"></param>
        ///// <param name="strUserName"></param>
        ///// <param name="strPwd"></param>
        ///// <returns>rmUser object</returns>
        //private UserLogin AuthenticateRMUser(string p_sDBName, string p_sUser, string p_sPass)
        //{
        //    UserLogin User;
        //    rmLog = new Login();

        //    //Authenticate the user and create the UserLogin object
        //    User = rmLog.AuthenticateUser(p_sDBName, p_sUser, p_sPass);

        //    //Clean up
        //    rmLog = null;

        //    return User;
        //}//method AuthenticateRMUser()

        //private UserLogin m_rmUser = new UserLogin();
        //public UserLogin RMUserLogin
        //{
        //    get { return m_rmUser; }
        //    set { m_rmUser = value; }
        //}//property for RMUserLogin

        //public ClaimClass GetListOfClaims()
        //{
        //    string sSQL = "";

        //    rmUser = AuthenticateRMUser(DBName, DBUserName, DBPassword);
        //    if (rmUser == null)
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        this.DBConnString = rmUser.objRiskmasterDatabase.ConnectionString;
        //        this.DBName = rmUser.objRiskmasterDatabase.DataSourceName;
        //        this.DBUserName = rmUser.LoginName;
        //        this.DBPassword = rmUser.Password;

        //        Riskmaster.Db.DbConnection rmConn = Riskmaster.Db.DbFactory.GetDbConnection(DBConnString);
        //        rmConn.Open();

        //        //load the Claim Number combo box with claims
        //        //that are after 12/31/2000 and have an Average Wage
        //        //sSQL = "SELECT CLAIM.CLAIM_NUMBER, CLAIM.CLAIM_ID FROM CLAIM, EVENT, CLAIM_AWW " +
        //        //       " WHERE CLAIM.EVENT_ID = EVENT.EVENT_ID " +
        //        //       "   AND CLAIM.CLAIM_ID = CLAIM_AWW.CLAIM_ID " +
        //        //       "   AND EVENT.DATE_OF_EVENT > '19991231' " +
        //        //       "   AND CLAIM_AWW.AWW > 0";

        //        sSQL = "SELECT C.CLAIM_NUMBER,C.LINE_OF_BUS_CODE,C.CLAIM_ID,S.STATE_ID + ' (' + S.STATE_NAME + ') ' AS STATE" +
        //               "  FROM CLAIM C, STATES S" +
        //               " WHERE S.STATE_ROW_ID = C.FILING_STATE_ID" +
        //               "   AND C.CLAIM_ID NOT IN (SELECT CLAIM_ID FROM CLAIM_AWW) AND C.LINE_OF_BUS_CODE = 243";

        //        Riskmaster.Db.DbReader objReader = null;
        //        objReader = rmConn.ExecuteReader(sSQL);
        //        while (objReader.Read())
        //        {
        //            m_ClaimList.Claims.Add(objReader.GetInt("CLAIM_ID"));
        //            m_ClaimList.Display.Add(objReader.GetString("CLAIM_NUMBER") + " -- " + objReader.GetString("STATE"));
        //        }
        //        rmConn.Close();
        //    }
        //    return m_ClaimList;
        //}//method GetListOfClaims()

        //public ClaimClass GetClaim(int intClaimID)
        //{
        //    ClaimClass objClaimList = new ClaimClass();
        //    objClaimList.Claims.Add(intClaimID);
        //    objClaimList.Display.Add("Claim ID: " + intClaimID.ToString());

        //    return objClaimList;
        //}

        //public ClaimClass GetClaim(int intClaimID, string strClaimNumber, string strState)
        //{
        //    ClaimClass objClaimList = new ClaimClass();
        //    objClaimList.Claims.Add(intClaimID);
        //    objClaimList.Display.Add(strClaimNumber + " -- " + strState);

        //    return objClaimList;
        //} 
        #endregion

        /// <summary>
        /// Returns the Benefit Calculator data
        /// </summary>
        /// <returns></returns>
        public BenefitCalcData GetBasicRate()
        {
            int intFilingStateID  = 0;
            bool blnIsInitialized = false;
            string sWarning       = String.Empty;
            string sBenefitAbb    = String.Empty;

            BenefitCalcData objBenefitCalc = new BenefitCalcData();

            try
            {
                blnIsInitialized = m_rmCOMRateCalc.Initialized;
                
                if (blnIsInitialized)
                {
                    intFilingStateID = m_rmCOMRateCalc.XClaim.FilingStateID;

                    objBenefitCalc.YesCodeID                      = m_rmCOMRateCalc.BenefitStaticData.YesCodeID;
                    objBenefitCalc.NoCodeID                       = m_rmCOMRateCalc.BenefitStaticData.NoCodeID;
                    objBenefitCalc.WaitingPeriodDays              = m_rmCOMRateCalc.BenefitStaticData.WaitingPeriodDays;
                    objBenefitCalc.WaitingPeriodSatisfiedCode     = m_rmCOMRateCalc.BenefitStaticData.WaitingPeriodSatisfiedCode;
                    objBenefitCalc.EventDateDBFormat              = m_rmCOMRateCalc.BenefitStaticData.EventDateDBFormat;
                    objBenefitCalc.OriginalDisabilityDateDBFormat = m_rmCOMRateCalc.BenefitStaticData.OriginalDisabilityDateDBFormat;
                    objBenefitCalc.RetroactivePeriodDays          = m_rmCOMRateCalc.BenefitStaticData.RetroactivePeriodDays;
                    objBenefitCalc.RetroactivePerSatisfiedCode    = m_rmCOMRateCalc.BenefitStaticData.RetroactivePerSatisfiedCode;
                    objBenefitCalc.CurrentDisabilityDateDBFormat  = m_rmCOMRateCalc.BenefitStaticData.CurrentDisabilityDateDBFormat;
                    objBenefitCalc.AverageWage                    = m_rmCOMRateCalc.BenefitStaticData.AverageWage;
                    objBenefitCalc.CurrentMMIDisability           = m_rmCOMRateCalc.BenefitStaticData.CurrentMMIDisability;
                    objBenefitCalc.MostRecentRetToWkDateDBFormat  = m_rmCOMRateCalc.BenefitStaticData.MostRecentRetToWkDateDBFormat;

                    objBenefitCalc.CurrentMMIDateDBFormat         = m_rmCOMRateCalc.BenefitStaticData.CurrentMMIDateDBFormat;

                    //Call a function to remove null values from all properties populated in the object
                    //Set the values to their appropriate initial constructor values based on their .Net type
                    CheckNullValues(ref objBenefitCalc);

                    foreach (int intAbbr in Enum.GetValues(typeof(Abbreviation)))
                    {
                        Abbreviation eAbbr = (Abbreviation)intAbbr;

                        switch (eAbbr)
                        {
                            case Abbreviation.TT://TT
                                sBenefitAbb = m_rmCOMRateCalc.GetTempTotalAbbreviation(ref sWarning, ref intFilingStateID);
                                objBenefitCalc.Warning = GetWarning(sBenefitAbb, sWarning);
                                if (String.IsNullOrEmpty(objBenefitCalc.Warning))
                                {
                                    objBenefitCalc.TTRate = m_rmCOMRateCalc.GetBasicRate(ref sBenefitAbb);
                                   // commented  for mits 25176 //
                                  //  objBenefitCalc.Warning = objBenefitCalc.Warning + m_rmCOMRateCalc.Warning;
                                }

                                
                               objBenefitCalc.TTAbbr = sBenefitAbb;
                               
                                
                                break;

                           
                             case Abbreviation.PP://PP
                                 sBenefitAbb = m_rmCOMRateCalc.GetPermPartAbbreviation(ref sWarning, ref intFilingStateID);
                                
                                 objBenefitCalc.Warning = GetWarning(sBenefitAbb, sWarning);
                                
                                 if (String.IsNullOrEmpty(objBenefitCalc.Warning))
                                 {
                                     objBenefitCalc.PPRate = m_rmCOMRateCalc.GetBasicRate(ref sBenefitAbb);
                                    
                                    // commented  for mits 25176 //
                                    //   objBenefitCalc.Warning = objBenefitCalc.Warning + m_rmCOMRateCalc.Warning;
                                 }
                                 objBenefitCalc.PPAbbr = sBenefitAbb;
                              
                                 break;
                             case Abbreviation.PT://PT
                                 sBenefitAbb = m_rmCOMRateCalc.GetPermTotalAbbreviation(ref sWarning, ref intFilingStateID);
                                 objBenefitCalc.Warning = GetWarning(sBenefitAbb, sWarning);
                                 if (String.IsNullOrEmpty(objBenefitCalc.Warning))
                                 {
                                     objBenefitCalc.PTRate = m_rmCOMRateCalc.GetBasicRate(ref sBenefitAbb);
 									// commented  for mits 25176 //
                                    //  objBenefitCalc.Warning = objBenefitCalc.Warning + m_rmCOMRateCalc.Warning;

                                 }
                                 objBenefitCalc.PTAbbr = sBenefitAbb;
                                 break;  
                         

                        }//switch 
                    }//for
                }//if
                else
                {
                    objBenefitCalc.Warning = m_rmCOMRateCalc.Warning;
                }//else
            }//try
            catch (COMException ex)
            {
                throw ex;
            }//catch
            catch (Exception ex)
            {
                throw ex;
            }//catch

            return objBenefitCalc;
        }

        private static void CheckNullValues(ref BenefitCalcData objBenefitCalc)
        {
            Type objType = Type.GetType("Riskmaster.Application.BenefitCalculator.BenefitCalcData", false, true);

            foreach (PropertyInfo pi in objBenefitCalc.GetType().GetProperties())
            {
                object objValue = null;
                //objValue = objBenefitCalc.GetType().GetField(pi.Name).GetValue(objBenefitCalc);
                objValue = objType.GetProperty(pi.Name).GetValue(objBenefitCalc, null);

                

                //Check to see if the property of the object is null
                if (objValue == null)
                {
                    
                    //Iterate through the property type names
                    switch (pi.PropertyType.FullName)
                    {
                        case "System.String":
                            objBenefitCalc.GetType().GetProperty(pi.Name).SetValue(objBenefitCalc, string.Empty, null);
                            break;
                        case "System.Int32":
                            objBenefitCalc.GetType().GetProperty(pi.Name).SetValue(objBenefitCalc, 0, null);
                            break;
                        case "System.Double":
                            objBenefitCalc.GetType().GetProperty(pi.Name).SetValue(objBenefitCalc, 0.0, null);
                            break;
                        //TODO: Verify that there does not need to be a default case
                    }
                }
            }//foreach
        }//method CheckNullValues()

        /// <summary>
        /// Reads back the Override Rate from the database
        /// </summary>
        /// <returns>Benefit Calculator Data object</returns>
        public void GetOverrideRate(ref BenefitCalcData objBenefitCalcData)
        {
            bool blnIsInitialized = false;
            
            try
            {
                blnIsInitialized = m_rmCOMRateCalc.Initialized;
                
                if (blnIsInitialized)
                {
                    string strPPAbbr, strPTAbbr, strTTAbbr = String.Empty;

                    strPPAbbr = objBenefitCalcData.PPAbbr;
                    strPTAbbr = objBenefitCalcData.PTAbbr;
                    strTTAbbr = objBenefitCalcData.TTAbbr;

                    objBenefitCalcData.PPOverrideRate = m_rmCOMRateCalc.OverRideRateRead(ref strPPAbbr);
                    objBenefitCalcData.PTOverrideRate = m_rmCOMRateCalc.OverRideRateRead(ref strPTAbbr);
                    objBenefitCalcData.TTOverrideRate = m_rmCOMRateCalc.OverRideRateRead(ref strTTAbbr);
                }//if
            }//try
            catch (COMException ex)
            {
                throw ex;
            }//catch
            catch (Exception ex)
            {
                throw ex;
            }//catch
        }//method: GetOverrideRate()

        /// <summary>
        /// Saves the data and persists it to the database
        /// </summary>
        /// <param name="objBenefitCalcData">contains the Benefit Calculator Data object</param>
        public void Save(ref BenefitCalcData objBenefitCalcData)
        {
            bool blnIsInitialized = false;

            try
            {
                blnIsInitialized = m_rmCOMRateCalc.Initialized;
                
                if (blnIsInitialized)
                {
                    int BLANK_VALUE                                      = 0;
                    double dblPPRate, dblPTRate, dblTTRate               = 0.0;
                    string strPPAbbr, strPTAbbr, strTTAbbr, BLANK_STRING = String.Empty;

                    strPPAbbr = objBenefitCalcData.PPAbbr;
                    strPTAbbr = objBenefitCalcData.PTAbbr;
                    strTTAbbr = objBenefitCalcData.TTAbbr;
                    dblPPRate = objBenefitCalcData.PPOverrideRate;
                    dblPTRate = objBenefitCalcData.PTOverrideRate;
                    dblTTRate = objBenefitCalcData.TTOverrideRate;

                    //Call it 3 times based on each abbreviation
                    m_rmCOMRateCalc.OverRideRateWrite(ref strPPAbbr, ref dblPPRate, ref BLANK_STRING, ref BLANK_VALUE);
                    m_rmCOMRateCalc.OverRideRateWrite(ref strPTAbbr, ref dblPTRate, ref BLANK_STRING, ref BLANK_VALUE);
                    m_rmCOMRateCalc.OverRideRateWrite(ref strTTAbbr, ref dblTTRate, ref BLANK_STRING, ref BLANK_VALUE);
                }//if
                
            }//try
            catch (COMException ex)
            {
                throw ex;
            }//catch
            catch (Exception ex)
            {
                throw ex;
            }//catch

        }//method: Save()
        #region Unused code
        //public bool GetRate(int iAbbr, int iClaimID, 
        //                    int iJurisID, string sDateOfEvent, double dAverageWage)
        //{
        //    int mlClaimID           = iClaimID;
        //    int lJurisTaxCode       = 0;
        //    int lJurisExemptions    = 0;
        //    int lJurisRowID         = 0;
        //    short iDWFlag           = 0;
        //    string sUserDSN         = DBName;
        //    string sUserLoginName   = DBUserName;
        //    string sUserPassword    = DBPassword;
        //    string sWarning         = String.Empty;
        //    string sBenefitAbb      = String.Empty;
        //    bool blnIsInitialized   = false;

        //    RMWCCalc.CWCDisBenCalc rmCOMRateCalc = new RMWCCalc.CWCDisBenCalc();

        //    try
        //    {
        //        rmCOMRateCalc.InitDisBenCalc(ref sUserLoginName, ref sUserPassword, ref sUserDSN,
        //                                     ref iDWFlag, ref mlClaimID, ref lJurisTaxCode,
        //                                     ref lJurisExemptions);

        //        blnIsInitialized = rmCOMRateCalc.Initialized;

        //        if (blnIsInitialized)
        //        {
        //            JurisRowID      = iJurisID;
        //            Jurisdiction    = iJurisID.ToString();
        //            DateOfEvent     = sDateOfEvent;
        //            AverageWage     = dAverageWage.ToString();
        //            lJurisRowID     = JurisRowID;

        //            Abbreviation eAbbr = (Abbreviation)iAbbr;

        //            switch (eAbbr)
        //            {
        //                case Abbreviation.TT://TT
        //                    sBenefitAbb = rmCOMRateCalc.GetTempTotalAbbreviation(ref sWarning, ref lJurisRowID);
        //                    this.BenefitAbbr = sBenefitAbb;
        //                    GetWarning(sBenefitAbb, sWarning);
        //                    this.TTD = rmCOMRateCalc.GetBasicRate(ref sBenefitAbb);
        //                    ERRmessage = "TTD for Claim ID#: " + mlClaimID + " has a value of: " + TTD + Environment.NewLine;
        //                    break;
        //                case Abbreviation.PP://PP
        //                    sBenefitAbb = rmCOMRateCalc.GetPermPartAbbreviation(ref sWarning, ref lJurisRowID);
        //                    this.BenefitAbbr = sBenefitAbb;
        //                    GetWarning(sBenefitAbb, sWarning);
        //                    this.PPD = rmCOMRateCalc.GetBasicRate(ref sBenefitAbb);
        //                    ERRmessage = "PPD for Claim ID#: " + mlClaimID + " has a value of: " + PPD + Environment.NewLine;
        //                    break;
        //                case Abbreviation.PT://PT
        //                    sBenefitAbb = rmCOMRateCalc.GetPermTotalAbbreviation(ref sWarning, ref lJurisRowID);
        //                    this.BenefitAbbr = sBenefitAbb;
        //                    GetWarning(sBenefitAbb, sWarning);
        //                    this.PTD = rmCOMRateCalc.GetBasicRate(ref sBenefitAbb);
        //                    ERRmessage = "PTD for Claim ID#: " + mlClaimID + " has a value of: " + PTD + Environment.NewLine;
        //                    break;
        //            }//switch
        //        }//if


        //    }//try
        //    catch (COMException ex)
        //    {
        //        bool rethrow = ExceptionPolicy.HandleException(ex, "CatchAllExceptions");

        //        if (rethrow)
        //        {
        //            throw;
        //        }//if

        //    }//catch
        //    catch (Exception ex)
        //    {
        //        bool rethrow = ExceptionPolicy.HandleException(ex, "CatchAllExceptions");

        //        if (rethrow)
        //        {
        //            throw;
        //        }//if

        //    }//catch
        //    finally
        //    {
        //        rmCOMRateCalc = null;
        //    }//finally

        //    return blnIsInitialized;
        //}//method GetRate() overloaded - 5 parameters 
        #endregion

        /// <summary>
        /// Gets the warning for the Calculator class
        /// </summary>
        /// <param name="strAbbrev"></param>
        /// <param name="sWarning"></param>
        /// <returns></returns>
        private string GetWarning(string strAbbrev, string sWarning)
        {
            string strErrorMessage = String.Empty;

            if (! String.IsNullOrEmpty(sWarning.Trim()) && String.IsNullOrEmpty(strAbbrev))
            {
                strErrorMessage += sWarning + Environment.NewLine;
            }//if
            return strErrorMessage;
        }//method: GetWarning()
    }//class: BenefitCalc

    /// <summary>
    /// Enumeration to provide short code mappings to each
    /// of the individual integer values for PP, PT and TT
    /// </summary>
    public enum Abbreviation 
    { 
        /// <summary>
        /// TT: 0
        /// </summary>
        TT, 
        /// <summary>
        /// PP: 1
        /// </summary>
   
        PP, 
   
        /// <summary>
        /// PT: 2
        /// </summary>

        PT 
    }
}
