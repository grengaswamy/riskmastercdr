﻿using System;

namespace Riskmaster.Application.BenefitCalculator
{
    /// <summary>
    /// This class stores information needed to display in the UI
    /// for the Benefit Calculator
    /// </summary>
    public class BenefitCalcData : IBenefitCalc
    {
        #region Private Members
        private int m_iNoCodeID                     = 0;
        private int m_iYesCodeID                    = 0;
        private int m_iWaitingPeriodDays            = 0;
        private int m_iRetroactivePeriodDays        = 0;
        private int m_iWaitingPeriodSatisfiedCode   = 0;
        private int m_iWorkLossWageLossExistsCode   = 0;
        private int m_iRetroactivePerSatisfiedCode  = 0;

        private double m_dTTRate                    = 0.0;
        private double m_dPPRate                    = 0.0;
        private double m_dPTRate                    = 0.0;
        private double m_dAverageWage               = 0.0;
        private double m_dCurrentMMIDisability      = 0.0;
        private double m_dPPOverrideRate            = 0.0;
        private double m_dPTOverrideRate = 0.0;
        private double m_dTTOverrideRate = 0.0;

        private string m_sTTAbbr                            = String.Empty;
        private string m_sPPAbbr                            = String.Empty;
        private string m_sPTAbbr                            = String.Empty;
        private string m_sWarning                           = String.Empty;
        private string m_sEventDateDBFormat                 = String.Empty;
        private string m_sCurrentMMIDateDBFormat            = String.Empty;
        private string m_sMostRecentRetToWkDateDBFormat     = String.Empty;
        private string m_sCurrentDisabilityDateDBFormat     = String.Empty;
        private string m_sOriginalDisabilityDateDBFormat    = String.Empty;
        #endregion

        #region IBenefitCalc Members
        /// <summary>
        /// 
        /// </summary>
        public string TTAbbr
        {
            get { return m_sTTAbbr; }
            set { m_sTTAbbr = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string PPAbbr
        {
            get { return m_sPPAbbr; }
            set { m_sPPAbbr = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string PTAbbr
        {
            get { return m_sPTAbbr; }
            set { m_sPTAbbr = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double TTRate
        {
            get { return m_dTTRate; }
            set { m_dTTRate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double PPRate
        {
            get { return m_dPPRate; }
            set { m_dPPRate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double PTRate
        {
            get { return m_dPTRate; }
            set { m_dPTRate = value; }
        } 

        /// <summary>
        /// 
        /// </summary>
        public double AverageWage
        {
            get { return m_dAverageWage; }
            set { m_dAverageWage = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int WaitingPeriodDays
        {
            get { return m_iWaitingPeriodDays; }
            set { m_iWaitingPeriodDays = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int WaitingPeriodSatisfiedCode
        {
            get { return m_iWaitingPeriodSatisfiedCode; }
            set { m_iWaitingPeriodSatisfiedCode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentDisabilityDateDBFormat
        {
            get { return m_sCurrentDisabilityDateDBFormat; }
            set { m_sCurrentDisabilityDateDBFormat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentMMIDateDBFormat
        {
            get { return m_sCurrentMMIDateDBFormat; }
            set { m_sCurrentMMIDateDBFormat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public double CurrentMMIDisability
        {
            get { return m_dCurrentMMIDisability; }
            set { m_dCurrentMMIDisability = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string EventDateDBFormat
        {
            get { return m_sEventDateDBFormat; }
            set { m_sEventDateDBFormat = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string OriginalDisabilityDateDBFormat
        {
            get { return m_sOriginalDisabilityDateDBFormat; }
            set { m_sOriginalDisabilityDateDBFormat = value; }
        }

        /// <summary>
        /// Gets and sets the YesCodeID
        /// </summary>
        /// <remarks>This value is populated from the Code table</remarks>
        /// <example>Possible values are: Yes, No, and Unknown</example>
        /// <value>returns an integer based CODE_ID from the Code table</value>
        public int YesCodeID
        {
            get { return m_iYesCodeID; }
            set { m_iYesCodeID = value; }
        }

        /// <summary>
        /// Gets and sets the NoCodeID
        /// </summary>
        /// <remarks>This value is populated from the Code table</remarks>
        /// <example>Possible Values are: Yes, No, and Unknown</example>
        /// <value>Returns an integer based CODE_ID from the Code table</value>
        public int NoCodeID
        {
            get { return m_iNoCodeID; }
            set { m_iNoCodeID = value; }
        }

        /// <summary>
        /// Gets and sets the MostRecentRetToWkDateDBFormat
        /// </summary>
        public string MostRecentRetToWkDateDBFormat
        {
            get { return m_sMostRecentRetToWkDateDBFormat; }
            set { m_sMostRecentRetToWkDateDBFormat = value; }
        }

        /// <summary>
        /// Gets and sets the RetroactivePeriodDays
        /// </summary>
        public int RetroactivePeriodDays
        {
            get { return m_iRetroactivePeriodDays; }
            set { m_iRetroactivePeriodDays = value; }
        }

        /// <summary>
        /// Gets and sets the Warning
        /// </summary>
        public string Warning
        {
            get { return m_sWarning; }
            set { m_sWarning = value; }
        }

        /// <summary>
        /// Gets and sets the RetroactivePerSatisfiedCode
        /// </summary>
        public int RetroactivePerSatisfiedCode
        {
            get { return m_iRetroactivePerSatisfiedCode; }
            set { m_iRetroactivePerSatisfiedCode = value; }
        }

        /// <summary>
        /// Gets and sets the WorkLossWageLossExistsCode
        /// </summary>
        public int WorkLossWageLossExistsCode
        {
            get { return m_iWorkLossWageLossExistsCode; }
            set { m_iWorkLossWageLossExistsCode = value; }
        }

        /// <summary>
        /// Gets and sets the Override Rate for the PP Abbreviation
        /// </summary>
        public double PPOverrideRate
        {
            get { return m_dPPOverrideRate; }
            set { m_dPPOverrideRate = value; }
        }

        /// <summary>
        /// Gets and sets the Override Rate for the PT Abbreviation
        /// </summary>
        public double PTOverrideRate
        {
            get { return m_dPTOverrideRate; }
            set { m_dPTOverrideRate = value; }
        }

        /// <summary>
        /// Gets and sets the Override Rate for the TT Abbreviation
        /// </summary>
        public double TTOverrideRate
        {
            get { return m_dTTOverrideRate; }
            set { m_dTTOverrideRate = value; }
        }//property: TTOverrideRate

        #endregion
    }
}
