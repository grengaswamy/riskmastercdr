﻿using System;

namespace Riskmaster.Application.BenefitCalculator
{
    interface IBenefitCalc
    {
        /// <summary>
        /// gets and sets the average wage
        /// </summary>
        double AverageWage { get; set; }
        /// <summary>
        /// gets and sets the retroactive period
        /// </summary>
        int WaitingPeriodDays { get; set; }
        /// <summary>
        /// gets and sets the satisfied checkbox for the retroactive period
        /// </summary>
        int WaitingPeriodSatisfiedCode { get; set; }
        /// <summary>
        /// gets and sets the current diability date
        /// </summary>
        string CurrentDisabilityDateDBFormat { get; set; }
        /// <summary>
        /// gets and sets the current MMI date
        /// </summary>
        string CurrentMMIDateDBFormat { get; set; }
        /// <summary>
        /// gets and sets the current disability percentage
        /// </summary>
        double CurrentMMIDisability { get; set; }
        /// <summary>
        /// gets and sets the most recent RTW (from work loss)
        /// </summary>
        string EventDateDBFormat { get; set; }
        /// <summary>
        /// gets and sets the PP (permanent partial) rate
        /// </summary>
        string OriginalDisabilityDateDBFormat { get; set; }
        /// <summary>
        /// gets and sets the TT (temporary total) rate
        /// </summary>
        int YesCodeID { get; set; }
        /// <summary>
        /// gets and sets the PP abbreviation
        /// </summary>
        string MostRecentRetToWkDateDBFormat { get; set; }
        /// <summary>
        /// gets and sets the PT abbreviation
        /// </summary>
        int RetroactivePeriodDays { get; set; }
        /// <summary>
        /// gets and sets the TT abbreviation
        /// </summary>
        string Warning { get; set; }
        /// <summary>
        /// gets and sets the Override PP rate
        /// </summary>
        int NoCodeID { get; set; }
        /// <summary>
        /// gets and sets the Override PT rate
        /// </summary>
        int RetroactivePerSatisfiedCode { get; set; }
        /// <summary>
        /// gets and sets the Override TT rate
        /// </summary>
        int WorkLossWageLossExistsCode { get; set; }

        /// <summary>
        /// gets and sets the PP Override rate
        /// </summary>
        double PPOverrideRate
        {
            get;
            set;
        }

        /// <summary>
        /// gets and sets the PT Override Rate
        /// </summary>
        double PTOverrideRate
        {
            get;
            set;
        }

        /// <summary>
        /// gets and sets the TT Override Rate
        /// </summary>
        double TTOverrideRate
        {
            get;
            set;
        }
    }
}
