﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Riskmaster.ExceptionTypes;
using Riskmaster.Common;
using Riskmaster.DataModel;
using System.Collections.Specialized;
using System.Web.Security;
using Riskmaster.Security.Authentication;
using Riskmaster.Security.Encryption;

namespace Riskmaster.Application.VSSInterface
{
    /// <summary>
    /// This class will read the VSS config.
    /// </summary>
    public class SingleSignon
    {
       #region Variable Declaration
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
        /// Private Variable to store VSS Login URL
        /// </summary>
        private string m_sVSS_URL = "";
        private int m_iClientId = 0;
		#endregion

        #region public properties
      

      
        #endregion

		#region Constructor
		/// <summary>
        ///Author  :   Anshul Verma
        ///Dated   :   01/09/2013
		///	Constructor, initializes the variables to the default value
		/// </summary>
        /// <param name="p_sUserName">loginName</param>
        /// <param name="p_sPassword">passwd</param>
        public SingleSignon(string p_sUserName, string p_sPassword, string p_sSecurityConnString, int p_iClientId)
		{
            RMConfigurationManager.GetVSSExportSettings();
            string strSecurityConnString = string.Empty;
            m_iClientId = p_iClientId;
            try
            {
                if (VSSExportSection.VSSLoginURL != "")
                {
                    this.m_sUserName = p_sUserName; 
                    this.m_sPassword = RMCryptography.EncryptString(p_sPassword);
                    this.m_sVSS_URL = VSSExportSection.VSSLoginURL;
                }
            }
            catch (Exception ex)
            {
                throw new RMAppException(Globalization.GetString("SingleSignon.SingleSignonXML.Error", m_iClientId), ex);
            }
		}
		#endregion
        

		#region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_objXML"></param>
        /// <returns></returns>
        public XmlDocument SingleSignonXML(XmlDocument p_objXML)
        {
            XmlElement objNode = null;
            
            try
            {
                // VSS Login Url
                objNode = (XmlElement)p_objXML.SelectSingleNode("//VSSUrl");
                objNode.InnerText = m_sVSS_URL;

                // RMX User Name
                objNode = (XmlElement)p_objXML.SelectSingleNode("//loginName");
                objNode.InnerText = m_sUserName;
                
                // RMX Password
                objNode = (XmlElement)p_objXML.SelectSingleNode("//passwd");
                objNode.InnerText = m_sPassword;        
             }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("SingleSignon.SingleSignonXML.Error", m_iClientId), p_objEx);
            }
            finally
            {
                objNode = null;
            }

            return p_objXML;
        }

        #endregion
    }
}
