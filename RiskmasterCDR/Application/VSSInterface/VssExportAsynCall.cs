﻿using System;
using System.IO;
using System.Xml;
using Riskmaster.Settings;
using Riskmaster.DataModel;
using Riskmaster.Db;
using Riskmaster.Common;
using Riskmaster.ExceptionTypes;
using System.Text;

namespace Riskmaster.Application.VSSInterface
{
    ///************************************************************** 
    ///* $File		: VssExportWrapper.cs 
    ///* $Revision	: 1.0.0.0 
    ///* $Date		: 21-Dec-2012
    ///* $Author	: Anshul Verma
    ///* $Comment	: 
    ///* $Source	: 
    ///**************************************************************
    ///<summary>
    ///Author  :   Anshul Verma
    ///Dated   :   21-Dec-2012
    ///Purpose :   This is a Wrapper Class for .NET VSS Export Class.
    /// </summary>
    public class VssExportAsynCall
    {
        #region Variable Declarations

        /// <summary>
        /// Private variable to store user name
        /// </summary>
        private string m_sUserName = "";
        /// <summary>
        /// Private variable to store password
        /// </summary>
        private string m_sPassword = "";
        /// <summary>
        /// Private variable to store DSN Name
        /// </summary>
        private string m_sDsnName = "";
        /// <summary>
        /// Private variable to store connection string
        /// </summary>
        private string m_sConnectionString = "";
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private bool m_resultsFromAsyncCall;

        private int m_iClaimId=0;
        private int m_iClaimantId=0;
        private int m_iReserveId=0;
        private string m_sExportType=string.Empty;
        private string m_sEmailId = string.Empty;
        private string m_sUserDsn = string.Empty;
        private int m_iTransId=0;
        private int m_iAdjusterEid = 0;
        private string m_sUserId = string.Empty;
        private string m_sUserPassword = string.Empty;
        private DataModelFactory m_objDataModelFactory = null;
        public delegate bool VssExportDummy();

        private int m_iClientId = 0;

        #endregion

        #region Constructor		
		
		/// <summary>
		///	Constructor, initializes the variables to the default value
		/// </summary>
		/// <param name="p_sUserName">UserName</param>
		/// <param name="p_sPassword">Password</param>
		/// <param name="p_sDsnName">DsnName</param>

        public VssExportAsynCall()
        {
            this.Initialize();
        }
        public VssExportAsynCall(string p_sDsnName, string p_sUserName, string p_sPassword, int p_iClientId)
		{
			m_sUserName = p_sUserName;
			m_sPassword = p_sPassword;
			m_sDsnName = p_sDsnName;
            m_iClientId = p_iClientId;
			this.Initialize();
		}

        #endregion
        /// <summary>
        /// Initialize objects
        /// </summary>
        private void Initialize()
        {
            try
            {
                using (DataModelFactory dataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId))
                {
                    m_sConnectionString = dataModelFactory.Context.DbConn.ConnectionString;
                }
                m_objDataModelFactory = new DataModelFactory(m_sDsnName, m_sUserName, m_sPassword, m_iClientId);
            }
            catch (DataModelException p_objEx)
            {
                throw p_objEx;
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("VssExportAsynCall.Initialize.ErrorInit", m_iClientId), p_objEx);
            }
        }

        private bool _isDisposed = false;
        /// <summary>
        /// Un Initialize the data model factory object. 
        /// </summary>
        public void Dispose()
        {
            //tkr nothing to do, but left method in case is being called somewhere
            if (!_isDisposed)
            {
                _isDisposed = true;
            }
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  Sends Asynchronous call to Vss export Web Service
        /// </summary>
        /// <param name="iClaimId">Claim id </param>
        /// <param name="iClaimantId">Claimant id</param>
        /// <param name="iReserveId">Reserve id</param>
        /// <param name="sExportXml">Export xml</param>
        /// <param name="sCurrentTime">current date and time</param>
        /// <param name="sExportType">Export type</param>

        public void AsynchVssReserveExport(int iClaimId, int iClaimantId, int iReserveId,int iTransId,int iAdjusterEid, string sUserName,string sPassword,string sEmailId,string sDsn,string sExportType)
        {
            //assigning the arguments of AsynchVssReserveExport Function to global variables as
            m_iClaimId=iClaimId;
            m_iClaimantId=iClaimantId;
            m_iReserveId=iReserveId;
            m_sExportType=sExportType;
            m_iTransId = iTransId;
            m_iAdjusterEid = iAdjusterEid;
            m_sUserId = sUserName;
            m_sUserPassword = sPassword;
            m_sEmailId = sEmailId;
            m_sUserDsn = sDsn;
            
            // they cannot be passed as arguments to the CallBack Function
            try
            {
                IAsyncResult result = null;
                VssExportDummy vssExportCaller = null;
                switch (sExportType)
                {
                    case "Reserve":
                        vssExportCaller = new VssExportDummy(this.ReserveExport);
                        result = vssExportCaller.BeginInvoke(new AsyncCallback(CallbackVssExport), vssExportCaller);
                        break;
                    case "Adjuster":
                        vssExportCaller = new VssExportDummy(this.AdjusterExport);
                        result = vssExportCaller.BeginInvoke(new AsyncCallback(CallbackVssExport), vssExportCaller);
                        break;
                    case "PaymentVoid":
                        vssExportCaller = new VssExportDummy(this.PaymentVoidExport);
                        result = vssExportCaller.BeginInvoke(new AsyncCallback(CallbackVssExport), vssExportCaller);
                        break;
                    case "CheckPrinted":
                        vssExportCaller = new VssExportDummy(this.CheckPrintExport);
                        result = vssExportCaller.BeginInvoke(new AsyncCallback(CallbackVssExport), vssExportCaller);
                        break;
                    case "PassUpdate":
                        vssExportCaller = new VssExportDummy(this.PassUpdateExport);
                        result = vssExportCaller.BeginInvoke(new AsyncCallback(CallbackVssExport), vssExportCaller);
                        break;
                    default:
						break;
                }
            }
            catch (Exception p_objException)
            {
               
            }
            finally
            {
                
            }
        }

        /// <summary>
        ///Call Back Function for the Asynchronous Call sent to VSS Export Web Service 
        /// </summary>
        /// <param name="asyncResult"></param>
        public void CallbackVssExport(IAsyncResult asyncResult)
        {
            try
            {
                VssExportDummy dlgt = (VssExportDummy)asyncResult.AsyncState;
                bool ret = dlgt.EndInvoke(asyncResult);
                m_resultsFromAsyncCall = ret;
            }
            catch (Exception p_objException)
            {
                // throw p_objException;
            }
        }

        public bool ReserveExport()
        {
            VSSExportProcess objExportReserve = null;
            try
            {
                objExportReserve = new VSSExportProcess(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                return  objExportReserve.ReserveExport(m_iClaimId, m_iClaimantId, m_iReserveId);
            }
            catch
            {
            }
            return true;
        }

        public bool AdjusterExport()
        {
            VSSExportProcess objExportReserve = null;
            try
            {
                objExportReserve = new VSSExportProcess(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                return objExportReserve.AdjusterExport(m_iClaimId, m_iAdjusterEid, m_iClientId);//rkaur27
            }
            catch
            {
            }
            return true;
        }

        public bool PaymentVoidExport()
        {
            VSSExportProcess objExportReserve = null;
            try
            {
                objExportReserve = new VSSExportProcess(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                return objExportReserve.PaymentVoidExport(m_iTransId);
            }
            catch
            {
            }
            return true;
        }

        public bool CheckPrintExport()
        {
            VSSExportProcess objExportReserve = null;
            try
            {
                objExportReserve = new VSSExportProcess(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                return objExportReserve.CheckStatusExport(m_iTransId);
            }
            catch
            {
            }
            return true;
        }

        public bool PassUpdateExport()
        {
            VSSExportProcess objExportReserve = null;
            try
            {
                objExportReserve = new VSSExportProcess(m_sUserName, m_sPassword, m_sDsnName, m_iClientId);
                return objExportReserve.PassUpdateExport(m_sUserId, m_sUserPassword, m_sEmailId, m_sUserDsn);
            }
            catch
            {
            }
            return true;
        }
    }
}
