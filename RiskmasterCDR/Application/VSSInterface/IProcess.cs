using System;
using System.Xml;
namespace Riskmaster.Application.VSSInterface
{
    /**************************************************************
     * $File		: IProcess.cs
     * $Revision	: 1.0.0.0
     * $Date		: 12/10/2012
     * $Author		: Parag Sarin / Anshul Verma
     * $Comment		: This file contains interfaces for Claim & Reserve process
     * $Source		:  	
    **********************************************************/

    /// <summary>
	/// Export Process Interface
	/// </summary>
	public interface IExportProcess: IProcess
	{
        bool ReserveExport(int p_iClaimId, int p_iClaimantId, int p_iReserveId);
        bool CheckStatusExport(int p_iTransid);
        bool PaymentVoidExport(int p_iTransid);
       // bool AdjusterExport(int p_iClaimId, int p_iAdjusterEid);
        bool PassUpdateExport(string p_sUserId,string p_sPassword,string p_sEmailId,string p_sDsn);
	}

	/// <summary>
	/// Export Process Factory Interface
	/// </summary>
	public interface IExportProcessFactory: IProcess
	{
		IExportProcess CreateExportProcess();
	}


	/// <summary>
	/// Import Process Interface
	/// </summary>
	public interface IImportProcess: IProcess
	{
		XmlDocument Execute(XmlDocument p_objXmlIn);
        
	}

	/// <summary>
	/// Import Process Factory Interface
	/// </summary>
	public interface IImportProcessFactory
	{
		IImportProcess CreateImportProcess();
	}

    public interface IProcess
    {
        string RiskmasterConnectionString { get; set; }
    }//interface

}
