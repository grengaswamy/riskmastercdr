﻿
using System;
using System.Xml ;
using System.Text ;
using System.Collections ;
using Riskmaster.Db ;
using Riskmaster.Common ;
using Riskmaster.Settings ;
using Riskmaster.DataModel ;
using Riskmaster.ExceptionTypes ;
using Riskmaster.Security;

namespace Riskmaster.Application.AltWaitPeriods
{
	/// <summary>
	/// Summary description for ClassTd.
	/// </summary>
	public class DisClassTdManager : IDisposable
	{
		#region Variable Declaration 
		/// <summary>
		/// Private variable to store user name
		/// </summary>
		private string m_sUserName = "" ;
		/// <summary>
		/// Private variable to store password
		/// </summary>
		private string m_sPassword = "" ;
		/// <summary>
		/// Private variable to store DSN Name
		/// </summary>
		private string m_sDsnName = "" ;
		/// <summary>
		/// Private variable to store UserId
		/// </summary>
		private int m_iUserId = 0 ;
		/// <summary>
		/// Private variable to store User GroupId
		/// </summary>
		private int m_iUserGroupId = 0 ;
		/// <summary>
		/// Private variable to store connection string
		/// </summary>
		private string m_sConnectionString = "" ;
		/// <summary>
		/// Private variable to store the instance of Datamodel factory object
		/// </summary>
		private DataModelFactory m_objDataModelFactory = null ;
        private int m_iClientId = 0;
		/// <summary>
		/// Private variable to store the instance of XmlDocument object
		/// </summary>
		private XmlDocument m_objDocument = null ;				
		#endregion 

		#region Constructors
        public DisClassTdManager(UserLogin objUser, int p_iClientId)
		{
            m_iClientId = p_iClientId;
//			m_sUserName = p_sUserName;
//			m_sPassword = p_sPassword;
//			m_sDsnName = p_sDsnName;
//			m_iUserId = p_iUserId ;
//			m_iUserGroupId = p_iUserGroupId ;
            m_objDataModelFactory = new DataModel.DataModelFactory(objUser, m_iClientId);	
//			m_sConnectionString = m_objDataModelFactory.Context.DbConn.ConnectionString;			
									
		}
		
		#endregion 

        private bool _isDisposed = false;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                if (m_objDataModelFactory != null)
                {
                    m_objDataModelFactory.Dispose();
                    m_objDataModelFactory = null;
                }
                GC.SuppressFinalize(this);
            }
        }

		#region Public Functions 

		#region Getting Data for Benefits Table
		/// Name		: OnLoad
		/// Author		: Rahul Sharma
		/// Date Created: 08/29/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		/// 
		///*************************************************************************
		/// <summary>
		/// Retrieves the Benefits Table Data for particular Disability Class Id
		/// </summary>
		/// <param name="p_iClassId">Disability Class id</param>
		/// <returns>Xml Document of the Data</returns>
		
		public XmlDocument OnLoad( int p_iClassId )
		{
			XmlElement objRootNode=null;
			XmlElement objRowNode=null;
			string sEarnings=string.Empty;
            DisabilityClass objDisClass = null;
			//Functions objXmlFunc=null;
			try
			{
				//DisClassTd objClassTd = m_objDataModelFactory.GetDataModelObject( "DisClassTd" , false );
				objDisClass=(DisabilityClass) m_objDataModelFactory.GetDataModelObject("DisabilityClass",false);
				objDisClass.MoveTo(p_iClassId);
				//objXmlFunc=new Functions();
				Functions.StartDocument(ref m_objDocument,ref objRootNode , "DisClassTdData",m_iClientId);
				foreach (DisClassTd objClassTd in objDisClass.TableList)
				{
					sEarnings="$"+objClassTd.WagesFrom.ToString()+" - $" +objClassTd.WagesTo.ToString();
					Functions.CreateElement( objRootNode , "DisClassTd" , ref objRowNode ,m_iClientId);
					Functions.CreateAndSetElement(objRowNode,"TD_ROW_ID",objClassTd.TdRowId.ToString(),m_iClientId);
					Functions.CreateAndSetElement(objRowNode,"EARNINGS",sEarnings,m_iClientId);
					Functions.CreateAndSetElement(objRowNode,"CLASS_ID",objClassTd.ClassId.ToString(),m_iClientId);
					Functions.CreateAndSetElement(objRowNode,"WAGES_FROM",objClassTd.WagesFrom.ToString(),m_iClientId);
					Functions.CreateAndSetElement(objRowNode,"WAGES_TO",objClassTd.WagesTo.ToString(),m_iClientId);
					Functions.CreateAndSetElement (objRowNode,"WEEKLY_BENEFIT","$"+objClassTd.WeeklyBenefit.ToString(),m_iClientId);
                    Functions.CreateAndSetElement(objRowNode, "SUPPLEMENT", "$" + objClassTd.SuppAmt.ToString(),m_iClientId);
                }
				
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
				throw new RMAppException(Globalization.GetString("TODO",m_iClientId) , p_objEx );				
			}
			finally
			{
				objRootNode=null;
                if (objDisClass != null)
                {
                    objDisClass.Dispose();
                }
			}
			return( m_objDocument );
		}
		#endregion

		#region Get Benefit Table Data for specific Row Id
		/// Name		: GetBenefitData
		/// Author		: Rahul Sharma
		/// Date Created: 08/29/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		/// 
		///*************************************************************************
		/// <summary>
		/// Retrieves the Benefits Table Data for particular Disability Table Row Id
		/// </summary>
		/// <param name="p_iClassId">Table Row id</param>
		/// <returns>Xml Document of the Data</returns>
		
		public XmlDocument GetBenefitData( int p_iRowId )
		{
			XmlElement objRootNode=null;
			XmlElement objRowNode=null;
            DisClassTd objClassTd = null;
			//Functions objXmlFunc=null;
			try
			{
				objClassTd = (DisClassTd) m_objDataModelFactory.GetDataModelObject( "DisClassTd" , false );
				//DisabilityClass objDisClass=(DisabilityClass) m_objDataModelFactory.GetDataModelObject("DisabilityClass",false);
				objClassTd.MoveTo(p_iRowId);
				//objXmlFunc=new Functions();
				Functions.StartDocument(ref m_objDocument,ref objRootNode , "DisClassTdData",m_iClientId);
				Functions.CreateElement( objRootNode , "DisClassTd" , ref objRowNode,m_iClientId);
				Functions.CreateAndSetElement(objRowNode,"TD_ROW_ID",objClassTd.TdRowId.ToString(),m_iClientId);
				Functions.CreateAndSetElement(objRowNode,"CLASS_ID",objClassTd.ClassId.ToString(),m_iClientId);
				Functions.CreateAndSetElement(objRowNode,"WAGES_FROM",objClassTd.WagesFrom.ToString(),m_iClientId);
				Functions.CreateAndSetElement(objRowNode,"WAGES_TO",objClassTd.WagesTo.ToString(),m_iClientId);
				Functions.CreateAndSetElement (objRowNode,"WEEKLY_BENEFIT",objClassTd.WeeklyBenefit.ToString(),m_iClientId);
				Functions.CreateAndSetElement (objRowNode,"SUPPLEMENT",objClassTd.SuppAmt.ToString(),m_iClientId);
			}
			catch( RMAppException p_objEx )
			{
				throw p_objEx ;
			}
			catch( Exception p_objEx )
			{
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);				
			}
			finally
			{
                if (objClassTd != null)
                {
                    objClassTd.Dispose();
                }
				objRootNode=null;
			}
			return( m_objDocument );
		} 
		#endregion

		#region Add Benefit Table Data
		/// Name		: SaveBenefitTableData
		/// Author		: Rahul Sharma
		/// Date Created: 08/29/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		/// 
		///*************************************************************************
		/// <summary>
		/// Saves the Benefits Table Data for particular Disability Table Row Id and Class Id
		/// </summary>
		/// <param name="p_iClassId">Class Row id</param>
		/// <param name="p_iRowId">Table Row id</param>
		/// <returns>Success</returns>
		public string SaveBenefitTableData(string p_sInputXml,int p_iRowId, int p_iClassId)
		{
			XmlDocument objInputXml=null;
            DisabilityClass objDisClass = null;
            DisClassTd objClassTd = null;
            try
            {
                objDisClass = (DisabilityClass)m_objDataModelFactory.GetDataModelObject("DisabilityClass", false);
                objDisClass.MoveTo(p_iClassId);
                objInputXml = new XmlDocument();
                objInputXml.LoadXml(p_sInputXml);
                if (p_iRowId > 0)
                {
                    objClassTd = (DisClassTd)m_objDataModelFactory.GetDataModelObject("DisClassTd", false); ;
                    objClassTd.MoveTo(p_iRowId);
                    //					((DisClassTd)objDisClass.TableList[p_iRowId]).WagesFrom=Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WAGES_FROM").InnerXml);
                    //					((DisClassTd)objDisClass.TableList[p_iRowId]).WagesTo=Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WAGES_TO").InnerXml);
                    //					((DisClassTd)objDisClass.TableList[p_iRowId]).WeeklyBenefit=Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WEEKLY_BENEFIT").InnerXml);
                    //					objDisClass.Save();
                    //pmittal5 Mits 16116 05/22/09 - Fields type changed to "currency" from "numeric"
                    //objClassTd.WagesFrom = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WAGES_FROM").InnerXml);
                    //objClassTd.WagesTo = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WAGES_TO").InnerXml);
                    //objClassTd.WeeklyBenefit = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WEEKLY_BENEFIT").InnerXml);
                    //objClassTd.SuppAmt = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//SUPPLEMENT").InnerXml);
                    objClassTd.WagesFrom = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//WAGES_FROM").InnerText);
                    objClassTd.WagesTo = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//WAGES_TO").InnerText);
                    objClassTd.WeeklyBenefit = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//WEEKLY_BENEFIT").InnerText);
                    objClassTd.SuppAmt = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//SUPPLEMENT").InnerText);
                    objClassTd.Save();

                }
                else
                {
                    objClassTd = objDisClass.TableList.AddNew();

                    objClassTd.ClassId = Conversion.ConvertObjToInt(objInputXml.SelectSingleNode("//CLASS_ID").InnerXml, m_iClientId);
                    //pmittal5 Mits 16116 05/22/09 - Fields type changed to "currency" from "numeric"
                    //objClassTd.WagesFrom = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WAGES_FROM").InnerXml);
                    //objClassTd.WagesTo = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WAGES_TO").InnerXml);
                    //objClassTd.WeeklyBenefit = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//WEEKLY_BENEFIT").InnerXml);
                    //objClassTd.SuppAmt = Conversion.ConvertObjToDouble(objInputXml.SelectSingleNode("//SUPPLEMENT").InnerXml);
                    objClassTd.WagesFrom = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//WAGES_FROM").InnerText);
                    objClassTd.WagesTo = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//WAGES_TO").InnerText);
                    objClassTd.WeeklyBenefit = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//WEEKLY_BENEFIT").InnerText);
                    objClassTd.SuppAmt = Conversion.ConvertStrToDouble(objInputXml.SelectSingleNode("//SUPPLEMENT").InnerText);
                    //Code for assigning supplement value.
                    objDisClass.TableList.Add(objClassTd);
                    objDisClass.Save();
                }
                return "Success";
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
            }
            finally
            {
                if (objDisClass != null)
                {
                    objDisClass.Dispose();
                }
            }
		}
		#endregion

		#region Delete Benefit Data Row
		/// Name		: DeleteRowData
		/// Author		: Rahul Sharma
		/// Date Created: 08/29/2005		
		///*************************************************************************
		/// Amendment History
		///*************************************************************************
		/// Date Amended   *   Amendment   *								Author
		/// 
		///*************************************************************************
		/// <summary>
		/// Delete Table Data for particular Disability Table Row Id
		/// </summary>
		/// <param name="p_iRowId">Table Row id</param>
		/// <returns>Success</returns>
		public string DeleteRowData(int p_iRowId)
		{
            DisClassTd objClassTd = null;
            try
            {
                objClassTd = (DisClassTd)m_objDataModelFactory.GetDataModelObject("DisClassTd", false); ;
                objClassTd.MoveTo(p_iRowId);
                objClassTd.Delete();
                return "Success";
            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("TODO", m_iClientId), p_objEx);
            }
            finally
            {
                if (objClassTd != null)
                {
                    objClassTd.Dispose();
                }
            }
		}
		#endregion

		#endregion 
	

	}
}
