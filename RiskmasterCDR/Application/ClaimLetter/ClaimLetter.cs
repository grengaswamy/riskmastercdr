﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security;
using System.Diagnostics;
using System.Threading;
using Riskmaster.DataModel;
using Riskmaster.Application.EmailDocuments;
using System.Xml;


namespace Riskmaster.Application.ClaimLetter
{
	/// <summary>
	///Author  :   Mridul Bansal
	///Dated   :   11th June 2009
	///Purpose :   This class Fetches, Emails claim Letters.
	/// </summary>
	public class ClaimLetter : IDisposable
	{

		#region Variables Declaration
        private bool blnSuccess = false;
        /// <summary>
        /// Private variable to store the instance of Datamodel factory object
        /// </summary>
        private DataModelFactory m_objDataModelFactory = null;
		
        /// <summary>
		/// Password for Claim Letter
		/// </summary>
        private string m_sPassword = "";

        /// <summary>
        /// Suffix for Email attachment file
        /// </summary>
        private string m_sAttachFileSuffix = "";

		/// <summary>
		/// Connection string for database
		/// </summary>
		private string m_sConnectionString="";
		
        /// <summary>
		/// Record id to which to attach a doc
		/// </summary>
		private int m_RecordId=0; 
		
        /// <summary>
		/// Secure database connect string
		/// </summary>
        private string m_sSecureConnectString = "";
        /// <summary>
        /// 
        /// </summary>
        public string SecureConnString
        {
            //get
            //{
            //    return m_sSecureConnectString;
            //}
            set
            {
                m_sSecureConnectString = value;
            }
        }


		private UserLogin m_userLogin = null;

        private int m_iClientId = 0;
		
		#endregion

		#region Properties Declaration

        /// <summary>
        /// Suffix For Attachment file
        /// </summary>
        public string FileSuffix
        {
            set
            {
                m_sAttachFileSuffix = value;
            }
        }

        /// <summary>
        /// Connection string to Database
        /// </summary>
        public string DSN
        {
            set
            {
                m_sConnectionString = value;                
            }
        }

        /// <summary>
        /// Password for opening Claim Letter
        /// </summary>
        public string Password
        {
            //get
            //{
            //    return m_bIsPreFab;
            //}
            set
            {
                m_sPassword = value;
            }
        }

		/// <summary>
		/// Recordid for which Claim Letter will be generated
		/// </summary>
		public int RecordId
		{
			get
			{
				return m_RecordId;
			}
			set
			{
				m_RecordId=value;
			}
		}
		
        /// <summary>
		/// Connection string to Security database
		/// </summary>
		public string  SecurityDSN
		{
			set
			{
				m_sSecureConnectString=value;
			}
		}
		#endregion

		#region Claim Letter Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_userLogin"></param>
        public ClaimLetter(UserLogin p_userLogin, int p_iClientId)
		{
            m_objDataModelFactory = new DataModelFactory(p_userLogin.objRiskmasterDatabase.DataSourceName, p_userLogin.LoginName, p_userLogin.Password,m_iClientId);//sonali
			m_userLogin = p_userLogin;
            m_iClientId = p_iClientId;
		}
		#endregion

		#region ClaimLetter Destructor
		/// <summary>
		/// Destructor
		/// </summary>
		~ClaimLetter()
		{
            Dispose();
		}
		#endregion

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            m_sConnectionString = null;
            m_sSecureConnectString = null;
            m_RecordId = 0;
        }

        /// <summary>
        /// Sends Claim Letters as email attachments
        /// </summary>
        /// <param name="p_sTitle"></param>
        /// <param name="p_sLetterContent"></param>
        /// <param name="p_iCtr"></param>
        /// <returns></returns>
        public bool MailClaimLetters(string p_sTitle, string p_sLetterContent, int p_iCtr)
        {
            #region Variables
            StringBuilder sbSql = null;
            StringBuilder sbEmailIdsTo = null;
            StringBuilder sbMessage = null;
            StringBuilder sbSubject = null;
            DbReader objReader = null;
            Claim objClaim = null;
            Event objEvent = null;
            Entity objEntity = null;
            EmailDocuments.EmailDocuments objEmailDocuments = null;
            int iEntityId = 0;
            int iLevelId = 0;
            string sLevel = "";
            string sClmtName = "";
            string sEmailIdsTo = "";
            string sEmailFrom = "";
            bool bReturnValue = false;
            bool blnSuccess = false;
            #endregion Variables

            try
            {
                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(RecordId);                

                objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                objEvent.MoveTo(objClaim.EventId);

                switch (p_iCtr)
                {
                    case 1:
                        sLevel = "DEPARTMENT_EID";
                        iLevelId = 1012;
                        break;
                    case 2:
                        sLevel = "FACILITY_EID";
                        iLevelId = 1011;
                        break;
                    case 3:
                        sLevel = "LOCATION_EID";
                        iLevelId = 1010;
                        break;
                    case 4:
                        sLevel = "DIVISION_EID";
                        iLevelId = 1009;
                        break;
                    case 5:
                        sLevel = "REGION_EID";
                        iLevelId = 1008;
                        break;
                    case 6:
                        sLevel = "OPERATION_EID";
                        iLevelId = 1007;
                        break;
                    case 7:
                        sLevel = "COMPANY_EID";
                        iLevelId = 1006;
                        break;
                    case 8:
                        sLevel = "CLIENT_EID";
                        iLevelId = 1005;
                        break;
                    default:
                        break;
                }

                //Get Entity ID whose contacts will receive mail
                if (sLevel != "DEPARTMENT_EID")
                {
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT " + sLevel + " FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString()))
                    {
                        if (objReader.Read())
                            iEntityId = objReader.GetInt(sLevel);
                    }
                }
                else
                {
                    iEntityId = objEvent.DeptEid;
                }

                //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                sbSql = new StringBuilder();
                sbSql.Append("SELECT EMAIL_ADDRESS FROM ENT_X_CONTACTINFO EXC,CONTACT_LOB  WHERE EXC.EMAIL_ACK = -1 AND  EMAIL_ADDRESS IS NOT NULL AND EMAIL_ADDRESS <> ' ' AND EXC.ENTITY_ID = " + iEntityId + " AND EXC.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode);
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString()))
                {
                    sbEmailIdsTo = new StringBuilder();
                    while (objReader.Read())
                    {
                        sbEmailIdsTo.Append(objReader.GetString("EMAIL_ADDRESS") + ",");
                    }
                }
                if (sbEmailIdsTo.ToString() != "")
                    sEmailIdsTo = sbEmailIdsTo.ToString().Substring(0, sbEmailIdsTo.ToString().Length - 1);
                
                if (sEmailIdsTo != "")
                {
                    //Get Primary Claimant Name
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + objClaim.ClaimId + " ORDER BY PRIMARY_CLMNT_FLAG ASC");
                    iEntityId = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sbSql.ToString()), m_iClientId);
                    if (iEntityId != 0)
                    {
                        objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                        objEntity.MoveTo(iEntityId);
                        if (objEntity.LastName != "")
                            sClmtName = (objEntity.LastName + ", " + objEntity.FirstName).Trim();
                        else
                            sClmtName = objEntity.FirstName.Trim();
                    }

                    //Retrieve Secured File
                    byte[] bytes = bytes = Convert.FromBase64String(p_sLetterContent);
                    using (FileStream fs = new FileStream(Path.GetTempPath() + objClaim.ClaimNumber + m_sAttachFileSuffix + p_iCtr + ".doc", FileMode.Create, FileAccess.Write))
                    {
                        using (BinaryWriter bw = new BinaryWriter(fs))
                        {
                            fs.Write(bytes, 0, bytes.Length);
                        }
                    }

                    //Mail it
                    objEmailDocuments = new EmailDocuments.EmailDocuments(m_userLogin.objRiskmasterDatabase.DataSourceName, m_userLogin.LoginName, m_userLogin.Password, m_userLogin.UserId, m_userLogin.GroupId, m_iClientId);

                    //With Attachment
                    sbSubject = new StringBuilder();
                    sbMessage = new StringBuilder();
                    //                Injured party : Pavelski, Joe
                    //Date of Event : 03/25/2009


                    //Attached you will find document related to the above captioned claim.

                    //The attached file has been encrypted and password protected for your data
                    //security.

                    //Password has been sent in a separate email.
                    sbSubject.Append(objClaim.ClaimNumber);
                    sbMessage.Append("Injured party : " + sClmtName + "\n");
                    sbMessage.Append("Date of Event : " + Conversion.ToDate(objEvent.DateOfEvent).ToString("MM/dd/yyyy") + "\n\n\n");
                    sbMessage.Append("Attached you will find document related to the above captioned claim." + "\n\n");
                    sbMessage.Append("The attached file has been encrypted and password protected for your data security." + "\n\n");
                    sbMessage.Append("Password has been sent in a separate email.\n\n");

                    //Fetch FROM Email address                    
                    using (objReader = Riskmaster.Db.DbFactory.GetDbReader(m_sSecureConnectString, "SELECT ADMIN_EMAIL_ADDR FROM SETTINGS"))
                    {
                        if (objReader.Read())
                            sEmailFrom = objReader.GetString("ADMIN_EMAIL_ADDR");
                    }
                    bReturnValue = objEmailDocuments.SendEmail(sEmailIdsTo, sbSubject.ToString(), sbMessage.ToString(), Path.GetTempPath() + objClaim.ClaimNumber + m_sAttachFileSuffix + p_iCtr + ".doc", sEmailFrom);

                    //Password Mail
                    if (bReturnValue)
                    {
                        sbSubject = new StringBuilder();
                        sbMessage = new StringBuilder();
                        sbSubject.Append(objClaim.ClaimNumber + ", Password");
                        sbMessage.Append("Password of document for Claim " + objClaim.ClaimNumber + " is " + m_sPassword + ".");
                        bReturnValue = objEmailDocuments.SendEmail(sEmailIdsTo, sbSubject.ToString(), sbMessage.ToString(),"", sEmailFrom);
                    }
                    File.Delete(Path.GetTempPath() + objClaim.ClaimNumber + m_sAttachFileSuffix + p_iCtr + ".doc");
                }
                blnSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimLetter.MailClaimLetter.Error",m_iClientId), p_objException);
            }
            finally
            {
                if (sbSql != null)
                    sbSql = null;
                if (sbMessage != null)
                    sbMessage = null;
                if (sbSubject != null)
                    sbSubject = null;
                if (objReader != null)
                    objReader = null;
                if (objClaim != null)
                    objClaim.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
                if (objEmailDocuments != null)
                    objEmailDocuments = null;
            }
            return blnSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_iClaimId"></param>
        /// <param name="p_iLtrFlag"></param>
        public void MarkClaim(int p_iClaimId, int p_iLtrFlag)
        {
            StringBuilder sbSql = null;
            int iRowId = 0;
            DbConnection objConn = null;

            try
            {
                iRowId = Utilities.GetNextUID(m_sConnectionString, "CLM_LTR_LOG", m_iClientId);
                sbSql = new StringBuilder();
                sbSql.Append("INSERT INTO CLM_LTR_LOG (ROW_ID, CLAIM_ID, LTR_FLAG, DTTM_RCD_LAST_UPD, UPDATED_BY_USER) VALUES (");
                sbSql.Append(Utilities.GetNextUID(m_sConnectionString, "CLM_LTR_LOG", m_iClientId) + ", " + p_iClaimId + ", " + p_iLtrFlag + ", '" + Conversion.ToDbDateTime(System.DateTime.Now) + "', '" + m_userLogin.LoginName + "')");
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                objConn.ExecuteNonQuery(sbSql.ToString());
                objConn.Close();
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimLetter.MarkClaim.Error", m_iClientId), p_objException);
            }
            finally
            {
                sbSql = null;
                if (objConn != null)
                    //objConn.Close();
                    objConn = null;
            }
        }


        public int GetTemplateID(string sLtrType)
        {            
            int iTempId = 0;
            DbConnection objConn = null;

            try
            {
                objConn = DbFactory.GetDbConnection(m_sConnectionString);
                objConn.Open();
                if (sLtrType == "ACK")
                    iTempId = Riskmaster.Common.Conversion.ConvertObjToInt(objConn.ExecuteScalar("SELECT ACKLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR"), m_iClientId);                    
                else                
                {
                    iTempId = Riskmaster.Common.Conversion.ConvertObjToInt(objConn.ExecuteScalar("SELECT CLLTR_TMPL_ID FROM SYS_PARMS_CLM_LTR"), m_iClientId);                    
                }                
                objConn.Close();
                return iTempId;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimLetter.GetTemplateID.Error", m_iClientId), p_objException);
            }
            finally
            {                
                if (objConn != null)             
                    objConn = null;
            }
        }


        public void PrepareSplitXML(string sDataContents, int iTemplateID, int iNumRecords, int iClaimID, XmlNode objNode, ref XmlDocument p_objXmlOut)
        {            
            string sDataSource = "";            
            string[] sData;
            XmlElement e = null;
            DbConnection objConn = null;
            Claim objClaim = null;
            Event objEvent = null;
            StringBuilder sbSql = null;            
            StringBuilder sOrgEid = null;
            string[] sOrgList;
            string sSQL = "";
            DbReader rdr = null;
            int iAckCount = 0;
            bool bOrgFieldExist = false;
            int iSplitData = 0;
            int[] iLevel = {0,0,0,0,0,0,0,0};
            int i = 0;
            
            

            try
            {

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(iClaimID);

                objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                objEvent.MoveTo(objClaim.EventId);

                sDataSource = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(sDataContents));
                sData = sDataSource.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                sSQL = "SELECT FORM_ID FROM MERGE_FORM_DEF WHERE FORM_ID = " + iTemplateID ;
                sSQL = sSQL + "AND FIELD_ID IN (SELECT FIELD_ID FROM MERGE_DICTIONARY WHERE FIELD_TABLE = 'GENERIC_ORG')";
                using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                {                    
                    if (rdr.Read())
                    {
                        bOrgFieldExist = true;                        
                    }
                    
                }

                sbSql = new StringBuilder();
                sOrgEid = new StringBuilder();
                sbSql.Append("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                using (rdr = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString()))
                {
                    if (rdr.Read())
                        sOrgEid.Append(rdr.GetInt("DEPARTMENT_EID") + "," + rdr.GetInt("FACILITY_EID") + "," + rdr.GetInt("LOCATION_EID") + "," + rdr.GetInt("DIVISION_EID") + "," + rdr.GetInt("REGION_EID") + "," + rdr.GetInt("OPERATION_EID") + "," + rdr.GetInt("COMPANY_EID") + "," + rdr.GetInt("CLIENT_EID"));
                }
                sOrgList = sOrgEid.ToString().Split(new char[] { ',' });

                foreach (string sOrgLevel in sOrgList)
                {
                    //sSQL = "SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO WHERE ENTITY_ID = " + sOrgLevel + " AND EMAIL_ACK = -1";
                    //sSQL = sSQL + "  AND LINE_OF_BUS_CODE LIKE '%" + objClaim.LineOfBusCode + "%'";
                    
                    //Geeta : Mits 18718 for Acord Comma Seperated List Issue

                    sSQL = "SELECT EMAIL_ACK, LAST_NAME FROM ENT_X_CONTACTINFO,ENTITY,CONTACT_LOB WHERE ENTITY.ENTITY_ID = ENT_X_CONTACTINFO.ENTITY_ID AND ENT_X_CONTACTINFO.ENTITY_ID = " + sOrgLevel + " AND EMAIL_ACK = -1";
                    sSQL = sSQL + "  AND ENT_X_CONTACTINFO.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode;
                    using (rdr = objClaim.Context.DbConnLookup.ExecuteReader(sSQL))
                    {
                        if (rdr.Read())
                        {
                            iLevel[iAckCount] = iSplitData;
                            iAckCount = iAckCount + 1;
                            e = p_objXmlOut.CreateElement("Title" + i);
                            e.InnerText = rdr.GetString("LAST_NAME");
                            objNode.AppendChild(e);
                        }
                        else
                        {
                            e = p_objXmlOut.CreateElement("Title" + i);
                            e.InnerText = "";
                            objNode.AppendChild(e);
                        }
                        iSplitData = iSplitData + 1;
                        i = i + 1;
                    }
                }
                
                if (bOrgFieldExist)
                {
                    iSplitData = 0;                                                     
                    for (int iRow = 0; iRow < sData.Length - 1; iRow = iRow + (iNumRecords / iAckCount))
                    {
                        e = p_objXmlOut.CreateElement("SplitData" + iLevel[iSplitData]);
                        e.InnerText = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(sData[iRow] + Environment.NewLine + sData[sData.Length - 1]));
                        objNode.AppendChild(e);
                        iSplitData = iSplitData + 1;
                    }                    
                }
                else
                {
                    e = p_objXmlOut.CreateElement("SplitData0");
                    e.InnerText = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(sData[0] + Environment.NewLine + sData[sData.Length - 1]));
                    objNode.AppendChild(e);
                    iAckCount = 1; //Since Org Hierarchy Field does not exist, so data file will be just one. 
                }
                                
                //ADD whether Org Hierarchy Field exist or Not to the Output xml. 
                e = p_objXmlOut.CreateElement("OrgEntryExist");
                e.InnerText = bOrgFieldExist.ToString();
                objNode.AppendChild(e);

                e = p_objXmlOut.CreateElement("SplitDataNum");
                e.InnerText = iAckCount.ToString(); ;
                objNode.AppendChild(e);
                
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimLetter.PrepareSplitXML.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (objConn != null)
                    objConn = null;
            }
        }
      /// <summary>
      /// Get password from the lower level of Org Hierarchy
      /// </summary>
      /// <param name="iClaimID"></param>
      /// <returns></returns>
        public string GetPassword(int iClaimID)
        {
            string sPassword = "";           
            Claim objClaim = null;
            Event objEvent = null;
            StringBuilder sbSql = null;            
            StringBuilder sOrgEid = null;
            string[] sOrgList = null;
            DbReader objReader = null;
            try
            {                

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(iClaimID);
                                
                objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                objEvent.MoveTo(objClaim.EventId);

                //Fetch All the Org Levels
                sbSql = new StringBuilder();
                sOrgEid = new StringBuilder();
                sbSql.Append("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString()))
                {
                    if (objReader.Read())
                        sOrgEid.Append(objReader.GetInt("DEPARTMENT_EID") + "," + objReader.GetInt("FACILITY_EID") + "," + objReader.GetInt("LOCATION_EID") + "," + objReader.GetInt("DIVISION_EID") + "," + objReader.GetInt("REGION_EID") + "," + objReader.GetInt("OPERATION_EID") + "," + objReader.GetInt("COMPANY_EID") + "," + objReader.GetInt("CLIENT_EID"));
                }
                sOrgList = sOrgEid.ToString().Split(new char[] { ',' });

                //Fetch Password for Both FROI AND ACORD
                foreach (string sOrgLevel in sOrgList)
                {
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT FROI_ACORD_PASSWORD FROM ENTITY WHERE ENTITY_ID = " + sOrgLevel);
                    sPassword = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar(sbSql.ToString()));
                    if (sPassword != "")
                        break;
                }
                return sPassword;                
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimLetter.GetPassword.Error", m_iClientId), p_objException);
            }            
        }


        #region Function to Generate Claim Letter
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_sLetterFileContent"></param>
        /// <param name="p_sLetterType"></param>
        /// <param name="p_iClaimId"></param>
        /// <param name="p_saLvlData"></param>
        /// <returns></returns>
        public bool GenerateClaimLetter(out string p_sLetterFileContent, string p_sLetterType, int p_iClaimId, out string[] p_saLvlData)
        {
            #region Variables
            string sPath = "";
            string sLtrPath = "";
            string sPolicyName = "";
            string sPassword = "";
            string sAdjName = "";
            string sAdjPhone1 = "";
            string sAdjAddr1 = "";
            string sAdjAddr2 = "";
            string sAdjAddr3 = "";
            string sAdjAddr4 = "";
            string sAdjCity = "";
            string sAdjZipCode = "";
            string sLvlLastName = "";
            string sLvlFirstName = "";
            string sLvlPhone1 = "";
            string sLvlAddr1 = "";
            string sLvlAddr2 = "";
            string sLvlAddr3 = "";
            string sLvlAddr4 = "";
            string sLvlCity = "";
            string sLvlZipCode = "";
            string sLvlFax = "";
            string sSql = "";
            string[] sOrgList;
            string sAdjState = "";
            int iLvlState = 0;
            string sLvlState = "";
            string sStateCode = "";
            string sStateDesc = "";
            string sName = "";
            string sLocName = "";
            string sDtOfEvent = "";
            string sClientName = "";
            string sDateClosed = "";
            string sType = "";
            int iAckNeeded = 0;
            string sGenerateLtr = "NO";
            string sClaimNo = "";
            string sClmtName = "";
            int iEntityId = 0;
            int iCtr = 0;

            StringBuilder sbSql = null;
            StringBuilder sData = null;
            StringBuilder sOrgEid = null;
            DbReader objReader = null;
            Entity objEntity = null;
            Event objEvent = null;
            Claim objClaim = null;
            
            
            string[] saLvlData = {"$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$", 
                                     "$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$",
                                     "$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$",
                                     "$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$",
                                     "$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$",
                                     "$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$",
                                     "$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$",
                                     "$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$$^$NO$^$"};
            bool blnSuccess = false;
            #endregion Variables

            p_saLvlData = saLvlData;
            p_sLetterFileContent = "";
            try
            {
                #region Fetch Letter Template
                //sPath = objConfig.GetValue("RMSettings/StandardPaths/UserDataPath"); ;
                sPath = RMConfigurator.UserDataPath;
                //ACK or CLOSED Letter
                if (p_sLetterType == "ACK")
                {
                    //ACK Claim Letter
                    sLtrPath = sPath + "\\MailMerge\\template\\AckLetterTemplate.doc";
                    sType = "ACK";
                }
                else
                {
                    //Closed Claim Letter
                    sLtrPath = sPath + "\\MailMerge\\template\\ClosingLetterTemplate.doc";
                    sType = "CL";
                }

                //Create Letter Template File
                if (File.Exists(sLtrPath))
                {
                    using (FileStream fs = new FileStream(sLtrPath, FileMode.Open, FileAccess.Read))
                    {
                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            p_sLetterFileContent = Convert.ToBase64String(br.ReadBytes((int)fs.Length));
                        }
                    }
                }
                else
                    return false;
                #endregion Fetch Letter Template

                objClaim = (Claim)m_objDataModelFactory.GetDataModelObject("Claim", false);
                objClaim.MoveTo(p_iClaimId);
                sClaimNo = objClaim.ClaimNumber;
                
                //Get Primary Claimant Name
                sbSql = new StringBuilder();
                sbSql.Append("SELECT CLAIMANT_EID FROM CLAIMANT WHERE CLAIM_ID = " + objClaim.ClaimId + " ORDER BY PRIMARY_CLMNT_FLAG ASC");
                iEntityId = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sbSql.ToString()), m_iClientId);
                if (iEntityId != 0)
                {
                    objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                    objEntity.MoveTo(iEntityId);
                    if (objEntity.LastName != "")
                        sClmtName = (objEntity.LastName + ", " + objEntity.FirstName).Trim();
                    else
                        sClmtName = objEntity.FirstName.Trim();
                }

                objEvent = (Event)m_objDataModelFactory.GetDataModelObject("Event", false);
                objEvent.MoveTo(objClaim.EventId);

                //Fetch All the Org Levels
                sbSql = new StringBuilder();
                sOrgEid = new StringBuilder();
                sbSql.Append("SELECT * FROM ORG_HIERARCHY WHERE DEPARTMENT_EID = " + objEvent.DeptEid);
                using (objReader = DbFactory.GetDbReader(m_sConnectionString, sbSql.ToString()))
                {
                    if (objReader.Read())
                        sOrgEid.Append(objReader.GetInt("DEPARTMENT_EID") + "," + objReader.GetInt("FACILITY_EID") + "," + objReader.GetInt("LOCATION_EID") + "," + objReader.GetInt("DIVISION_EID") + "," + objReader.GetInt("REGION_EID") + "," + objReader.GetInt("OPERATION_EID") + "," + objReader.GetInt("COMPANY_EID") + "," + objReader.GetInt("CLIENT_EID"));
                }
                sOrgList = sOrgEid.ToString().Split(new char[] { ',' });

                //Fetch Password for Both FROI AND ACORD
                foreach (string sOrgLevel in sOrgList)
                {
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT FROI_ACORD_PASSWORD FROM ENTITY WHERE ENTITY_ID = " + sOrgLevel);
                    sPassword = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar(sbSql.ToString()));
                    if (sPassword != "")
                        break;
                }
                if (sPassword == "")
                    blnSuccess = false;                

                //Start Fetching Data
                sPolicyName = objClaim.PrimaryPolicy.PolicyName;

                //Get Current Adjuster Details
                objEntity = (Entity)m_objDataModelFactory.GetDataModelObject("Entity", false);
                objEntity.MoveTo(objClaim.CurrentAdjuster.AdjusterEid);
                sAdjName = (objEntity.FirstName + " " + objEntity.LastName).Trim();
                sAdjPhone1 = objEntity.Phone1;
                sAdjAddr1 = objEntity.Addr1;
                sAdjAddr2 = objEntity.Addr2;
                sAdjAddr3 = objEntity.Addr3;
                sAdjAddr4 = objEntity.Addr4;
                sAdjCity = objEntity.City;
                sAdjZipCode = objEntity.ZipCode;
                iLvlState = objEntity.StateId;
                if (iLvlState != 0)
                {
                    objClaim.Context.LocalCache.GetStateInfo(iLvlState, ref sStateCode, ref sStateDesc);
                    sAdjState = sStateCode + "-" + sStateDesc;
                    iLvlState = 0;
                }
                else
                    sAdjState = "";
                objEntity = null;
                
                //Get Client Name
                sClientName = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar("SELECT CLIENT_NAME FROM SYS_PARMS"));
                sDtOfEvent = Conversion.ToDate(objEvent.DateOfEvent).ToString("MM/dd/yyyy");
                sDateClosed = Conversion.ToDate(objClaim.DttmClosed).ToString("MM/dd/yyyy");

                //Get Location Name
                iEntityId = objEvent.DeptEid;
                sLocName = objClaim.Context.LocalCache.GetEntityLastFirstName(iEntityId);
                do
                {
                    sbSql = new StringBuilder();
                    sbSql.Append("SELECT FAX_NUMBER FROM ENT_X_CONTACTINFO WHERE ENTITY_ID = " + iEntityId + " AND EMAIL_ACK = -1");
                    sLvlFax = Conversion.ConvertObjToStr(objClaim.Context.DbConn.ExecuteScalar(sbSql.ToString()));

                    sbSql = new StringBuilder();
                    //Geeta : Mits 18718 for Acord Comma Seperated List Issue
                    sbSql.Append("SELECT EMAIL_ACK FROM ENT_X_CONTACTINFO EXC,CONTACT_LOB WHERE EXC.EMAIL_ACK = -1 AND EXC.ENTITY_ID = " + iEntityId + " AND EXC.CONTACT_ID = CONTACT_LOB.CONTACT_ID AND CONTACT_LOB.LINE_OF_BUS_CODE = " + objClaim.LineOfBusCode ) ;
                    iAckNeeded = Conversion.ConvertObjToInt(objClaim.Context.DbConn.ExecuteScalar(sbSql.ToString()), m_iClientId);
                    sGenerateLtr = "NO";
                    if (iAckNeeded != 0)
                        sGenerateLtr = "YES";

                    sSql = "SELECT PARENT_EID, ENTITY_ID, ENTITY_TABLE_ID, FIRST_NAME, LAST_NAME, ADDR1, ADDR2, ADDR3, ADDR4, PHONE1, CITY, STATE_ID, ZIP_CODE FROM ENTITY WHERE ENTITY.ENTITY_ID = " + iEntityId.ToString();
                    using (objReader = DbFactory.GetDbReader(m_sConnectionString, sSql))
                    {
                        if (objReader.Read())
                        {
                            iEntityId = objReader.GetInt("PARENT_EID");
                            sLvlLastName = objReader.GetString("LAST_NAME");
                            sLvlFirstName = objReader.GetString("FIRST_NAME");
                            sName = (sLvlLastName + " " + sLvlFirstName).Trim();
                            sLvlPhone1 = objReader.GetString("PHONE1");
                            sLvlCity = objReader.GetString("CITY");
                            sLvlAddr1 = objReader.GetString("ADDR1");
                            sLvlAddr2 = objReader.GetString("ADDR2");
                            sLvlAddr3 = objReader.GetString("ADDR3");
                            sLvlAddr4 = objReader.GetString("ADDR4");
                            sLvlZipCode = objReader.GetString("ZIP_CODE");
                            iLvlState = objReader.GetInt("STATE_ID");
                            if (iLvlState != 0)
                            {
                                objClaim.Context.LocalCache.GetStateInfo(iLvlState, ref sStateCode, ref sStateDesc);
                                sLvlState = sStateCode + "-" + sStateDesc;
                            }
                            else
                                sLvlState = "";
                        }
                    }

                    //Make String of Data
                    sData = new StringBuilder();
                    sData.Append(sClientName.ToUpper() + "$^$" + sName + "$^$" + sLvlPhone1 + "$^$" + sLvlAddr1 + "$^$" + sLvlAddr2 + "$^$" + sLvlAddr3 + "$^$" + sLvlAddr4 + "$^$" + sLvlCity + "$^$" + sLvlState + "$^$" + sLvlZipCode + "$^$");
                    sData.Append(sPolicyName + "$^$" + sLocName + "$^$" + sClaimNo + "$^$" + sClmtName + "$^$" + sDtOfEvent + "$^$");
                    sData.Append(sClientName + "$^$" + sAdjName + "$^$" + sAdjPhone1 + "$^$" + sAdjAddr1 + "$^$" + sAdjAddr2 + "$^$" + sAdjAddr3 + "$^$" + sAdjAddr4 + "$^$" + sAdjCity + "$^$" + sAdjState + "$^$" + sAdjZipCode + "$^$" + sType + "$^$" + sDateClosed + "$^$" + sLvlFax + "$^$" + sGenerateLtr + "$^$" + sPassword);
                    p_saLvlData[iCtr] = sData.ToString();
                    sData = null;
                    sLvlLastName = "";
                    sLvlFirstName = "";
                    sName = "";
                    sLvlPhone1 = "";
                    sLvlCity = "";
                    sLvlAddr1 = "";
                    sLvlAddr2 = "";
                    sLvlAddr3 = "";
                    sLvlAddr4 = "";
                    sLvlZipCode = "";
                    iLvlState = 0;
                    sLvlFax = "";
                    sStateCode = "";
                    sStateDesc = "";
                    iCtr += 1;
                }while(iEntityId != 0);
                blnSuccess = true;
            }
            catch (RMAppException p_objException)
            {
                throw p_objException;
            }
            catch (Exception p_objException)
            {
                throw new RMAppException(Globalization.GetString("ClaimLetter.GenerateClaimLetter.Error", m_iClientId), p_objException);
            }
            finally
            {
                if (sbSql != null)
                    sbSql = null;
                if (sData != null)
                    sData = null;
                if (objReader != null)
                    objReader = null;
                if (sOrgEid != null)
                    sOrgEid = null;
                if (objReader != null)
                    objReader = null;
                if (objEntity != null)
                    objEntity.Dispose();
                if (objEvent != null)
                    objEvent.Dispose();
                if (objClaim != null)
                    objClaim.Dispose();                
            }
            return blnSuccess;
        }
        #endregion
    }
}
