/**********************************************************************************************
 *   Date     |  MITS/JIRA      | Programmer | Description                                    *
 **********************************************************************************************
 * 12/09/2014 | RMA-4042         | ajohari2   | Underwriters - EFT Payments Part 2
 * 05/14/2015 | RMA-4606         | nshah28   |  Import third party Currency Exchange Rates from flat file
 **********************************************************************************************/
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Xml;
using System.Linq;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Timers;
using Riskmaster.BusinessAdaptor.Common;
using Riskmaster.Common;
using Riskmaster.Db;
using Riskmaster.ExceptionTypes;
using Riskmaster.Security.Encryption;
using Riskmaster.BusinessAdaptor;
using Riskmaster.Security;
using Riskmaster.DataModel;
using Riskmaster.Settings;
using System.Collections.Generic;
using System.Configuration;

namespace Riskmaster.Service.TaskManagement
{
    //************************************************************** 
    //* $File				: TaskManager.cs 
    //* $Revision			: 1.0.0.0 
    //* $Creation Date		: 12-07-2007
    //* $Author			    : Aashish Bhateja
    //***************************************************************	

    /// <summary>
    /// This class contains the core elements  of  the TaskManagement windows service.
    /// </summary>
    public partial class TaskManager : ServiceBase, IDisposable
    {
        /// <summary>
        /// Holds the connection string.
        /// </summary>
        //Add & change by kuladeep for Cloud--Start
        //public string m_sConnectionTenantSecurity = RMConfigurationManager.GetConfigConnectionString("rmATenantSecurity");
        private string m_sConnectionString = string.Empty;
        private string m_sConnectionSecurity = string.Empty;
        // private string m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource",0);//For Base functionality.
        // private string m_sConnectionSecurity = RMConfigurationManager.GetConfigConnectionString("RMXSecurity", 0);//For Base functionality.        
        //private static string m_sConnectionRiskmaster = "";
        //public static Hashtable m_hstUserLoginName = new Hashtable();
        //public static Hashtable m_hstDSNName = new Hashtable();
        //public static Hashtable m_hstJobName = new Hashtable(); 
        private string m_sConnectionRiskmaster = "";
        public Hashtable m_hstUserLoginName = new Hashtable();
        public Hashtable m_hstDSNName = new Hashtable();
        public Hashtable m_hstJobName = new Hashtable();
        public static Dictionary<int, CommonFunctions.MultiClientConnection> dictMultiTenant;
        //Add & change by kuladeep for Cloud--End
        BusinessAdaptorErrors systemErrors ;         // Collection for code to use to store system errors that need to be thrown back.

        /// <summary>
        /// Holds the name value collection from TMService settings in Riskmaster.config
        /// </summary>
        NameValueCollection objTMSettings = null;

        private static Dictionary<int, string> objDicTMState = null; //tmalhotra3: RMA 4613
        public bool m_bContinue = true;

        // Keep track of the current job.
        private const string AUTHKEY = "6378b87457a5ecac8674e9bac12e7cd9";

        // Set the default wait time as 20 mins, so in case the Task Manager service is not able to connect to
        // the TM database it will wait for 20 mins and then again attempt to connect to the database in 20 mins
        // In case someone updates the connection information during this wait time service will pick up the 
        // updated connection information and start running the tasks.This will make sure that users do not
        // have to start the service manually.
        private const int m_iDefaultWaitTime = 1200000;

        #region For debugging purpose : Windows Application

        //#if DEBUG
        //        /// <summary>
        //        /// The main entry point for the application.
        //        /// </summary>
        //        [STAThread]
        //        static void Main(string[] args)
        //        {
        //            TaskManager objManager = new TaskManager();

        //            try
        //            {


        //                //string sTemp = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

        //                //sTemp = sTemp + "\\connectionStrings.config";



        //                //XDocument objXDoc = XDocument.Load(sTemp);

        //                //XElement objXElement = objXDoc.XPathSelectElement("/connectionStrings/add[@name='TaskManagerDataSource']");





        //                //string sConnectionString1 = objXElement.Attribute("connectionString").Value;

        //                //objXDoc = XDocument.Load(sTemp);

        //                //objXElement = objXDoc.XPathSelectElement("/connectionStrings/add[@name='TaskManagerDataSource']");

        //                //sConnectionString1 = objXElement.Attribute("connectionString").Value;




        //                // Poll the TM_JOBS table
        //                //objManager.CheckForKilling();
        //                //objManager.LookForJob();

        //                //// Poll the TM_SCHEDULE table
        //                //objManager.CheckSchedule();

        //                if (Conversion.ConvertObjToBool(objManager.objTMSettings["ArchiveJobs"]))
        //                    objManager.CheckArchive();
        //            }
        //            catch (RMAppException p_objEx)
        //            {
        //                EventLog.WriteEntry("MainMethod : RMAppException", p_objEx.InnerException.Message);
        //                //systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
        //                //logErrors("MainMethod", null, false, systemErrors);
        //            }
        //            catch (Exception p_objEx)
        //            {
        //                EventLog.WriteEntry("MainMethod : Exception", p_objEx.InnerException.Message);
        //                //this.systemErrors.Add(p_objEx, Globalization.GetString("TaskManagementAdaptor.WorkerMethod.Error"), BusinessAdaptorErrorType.Error);
        //                //logErrors("MainMethod", null, false, systemErrors);
        //            }

        //            finally
        //            { }


        //        }

        //#endif

        #endregion


        /// <summary>
        /// This method will poll the TM_SCHEDULE table every 20 seconds to see if there are any scheduled tasks that 
        /// need to be initiated as JOBS and copied to the TM_JOBS table.
        /// </summary>
        private void CheckSchedule(int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            // akaushik5 Commneted for MITS 38134 Starts
            //DbConnection objConntemp = null; //sanoopsharma added to avoid conflicts with datareader
            // akaushik5 Commneted for MITS 38134 Ends
            int iTaskTypeId = 0;
            int iScheduleId = 0;
            int iScheduleTypeId = 0; // This will be used to get the new next run date.
            int iIntervalTypeId = 0; // Defines the interval type i.e. minutes, days, weeks, months etc.
            int iInterval = 0; // The "x" in:  Run the task every x minutes; Run the task every x weeks; etc.
            string sTriggerDirectory = string.Empty;
            string sTimeToRun = string.Empty;
            int iDayOfMonth = 0;
            string sNextRunDttm = string.Empty;
            string sNewNextRunDttm = string.Empty;
            string sTempDTTM = string.Empty;
            string sConfig = string.Empty;
            string sUserName = string.Empty;
            string sJobName = string.Empty;
            string sDataSourceName = string.Empty;
            const int ST_ONE_TIME = 1;
            const int ST_PERIODICALLY = 2;
            const int ST_WEEKLY = 3;
            const int ST_MONTHLY = 4;
            const int ST_YEARLY = 5;
            const int ST_DIRECTORY_TRIGGERED = 6;
            const int ST_DAILY = 7;

            DateTime dtNext = DateTime.MinValue;
            string sConnectionString = string.Empty;//Add by kuladeep for Cloud
            
            // akaushik5 Added for MITS 38134 Starts
            string tmScheduleSql = string.Empty;
            // akaushik5 Added for MITS 38134 Ends
            
            try
            {

                //Add & change by kuladeep for Cloud--Start
                if (iClientId != 0)//Check for Cloud functionality and change connection string as per env.
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //Add & change by kuladeep for Cloud--End

                systemErrors = new BusinessAdaptorErrors(iClientId);

                // akaushik5 Changed for MITS 38134 Starts
                //sSQL = "SELECT * FROM TM_SCHEDULE WHERE NEXT_RUN_DTTM <= " + Conversion.GetDateTime(DateTime.Now.ToString());
                tmScheduleSql = string.Format("SELECT * FROM TM_SCHEDULE WHERE NEXT_RUN_DTTM <= {0}", Conversion.GetDateTime(DateTime.Now.ToString()));
                // akaushik5 Changed for MITS 38134 Ends

                objConn = DbFactory.GetDbConnection(sConnectionString);
                // akaushik5 Commneted for MITS 38134 Starts
                //objConntemp = DbFactory.GetDbConnection(sConnectionString); //sanoopsharma
                // akaushik5 Commneted for MITS 38134 Starts
                objConn.Open();

                //akaushik5 Changed for MITS 38134 Starts
                //using (objReader = objConn.ExecuteReader(sSQL))
                using (objReader = objConn.ExecuteReader(tmScheduleSql))
                //akaushik5 Changed for MITS 38134 Ends
                {
                    while (objReader.Read())
                    {
                        iScheduleId = objReader.GetInt32("SCHEDULE_ID");
                        iTaskTypeId = objReader.GetInt32("TASK_TYPE_ID");

                        //rsolanki2 : exec Summary Enhacements
                        string sExeName = string.Empty;
                        bool bIsSingleTon = checkForSingleTon(iTaskTypeId, out sExeName, iClientId);
                        if (bIsSingleTon)
                        {
                            //Process[] processlist = Process.GetProcesses();
                            var list = Process.GetProcesses().Where(p => p.ProcessName.ToLower().StartsWith(sExeName.ToLower()));
                            if (list.Count() > 0)
                            {
                                continue;
                            }
                        }
                        iScheduleTypeId = objReader.GetInt32("SCHEDULE_TYPE_ID");
                        iIntervalTypeId = objReader.GetInt32("INTERVAL_TYPE_ID");
                        iInterval = objReader.GetInt32("INTERVAL");
                        sTriggerDirectory = objReader.GetString("TRIGGER_DIRECTORY");
                        sTimeToRun = objReader.GetString("TIME_TO_RUN");
                        iDayOfMonth = objReader.GetInt32("DAY_OF_MON_TO_RUN");
                        sNextRunDttm = objReader.GetString("NEXT_RUN_DTTM");
                        sConfig = objReader.GetString("CONFIG");
                        //Get the user creating the task
                        if (!objReader.IsDBNull("TM_USER"))
                        {
                            sUserName = objReader.GetString("TM_USER");
                        }
                        //Get the DSN task created in 
                        if (!objReader.IsDBNull("TM_DSN"))
                        {
                            sDataSourceName = objReader.GetString("TM_DSN");
                        }

                        sJobName = objReader.GetString("TASK_NAME");
                        sNextRunDttm = Conversion.ToDate(sNextRunDttm).ToShortDateString() + " " + Conversion.ToDate(sNextRunDttm).ToLongTimeString();

                        // Calculate the "New Next Run DateTime" based on the schedule type.
                        switch (iScheduleTypeId)
                        {
                            case ST_ONE_TIME:
                                {
                                    sNewNextRunDttm = string.Empty;
                                    break;
                                }
                            case ST_PERIODICALLY: // Every:X Minutes X Days X Weeks X Months
                                {
                                    sNewNextRunDttm = GetNextNewRunDTTM(iIntervalTypeId, iInterval, sNextRunDttm);
                                    if (Conversion.ConvertStrToLong(sNewNextRunDttm) < Conversion.ConvertStrToLong(DateTime.Now.ToString("yyyyMMddHHmmss")))
                                    {
                                        sTempDTTM = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

                                        sNewNextRunDttm = GetNextNewRunDTTM(iIntervalTypeId, iInterval, sTempDTTM);
                                    }

                                    break;
                                }
                            case ST_WEEKLY: // Ex: On Mondays, Weds, Fridays
                                {
                                    dtNext = Convert.ToDateTime(sNextRunDttm).AddDays(1);
                                    for (int i = 0; i < 7; i++)
                                    {
                                        if ((dtNext.DayOfWeek == DayOfWeek.Monday && objReader.GetInt16("MON_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Tuesday && objReader.GetInt16("TUE_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Wednesday && objReader.GetInt16("WED_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Thursday && objReader.GetInt16("THU_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Friday && objReader.GetInt16("FRI_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Saturday && objReader.GetInt16("SAT_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Sunday && objReader.GetInt16("SUN_RUN") == -1))
                                        {
                                            sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                            break;
                                        }
                                        dtNext = dtNext.AddDays(1);
                                    }
                                    if (Conversion.ConvertStrToLong(sNewNextRunDttm) < Conversion.ConvertStrToLong(DateTime.Now.ToString("yyyyMMddHHmmss")))
                                    {
                                        dtNext = DateTime.Now.AddDays(1);
                                        for (int i = 0; i < 7; i++)
                                        {
                                            if ((dtNext.DayOfWeek == DayOfWeek.Monday && objReader.GetInt16("MON_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Tuesday && objReader.GetInt16("TUE_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Wednesday && objReader.GetInt16("WED_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Thursday && objReader.GetInt16("THU_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Friday && objReader.GetInt16("FRI_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Saturday && objReader.GetInt16("SAT_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Sunday && objReader.GetInt16("SUN_RUN") == -1))
                                            {
                                                sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                                break;
                                            }
                                            dtNext = dtNext.AddDays(1);
                                        }
                                    }
                                    break;
                                }
                            case ST_MONTHLY: // Ex: 1st of every Month.
                                {
                                    dtNext = Convert.ToDateTime(sNextRunDttm).AddMonths(1);
                                    sNewNextRunDttm = Conversion.ToDbDateTime(Convert.ToDateTime(sNextRunDttm).AddMonths(1));
                                    if (Conversion.ConvertStrToLong(sNewNextRunDttm) < Conversion.ConvertStrToLong(DateTime.Now.ToString("yyyyMMddHHmmss")))
                                    {
                                        //jramkumar for MITS 34696
                                        dtNext = new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, dtNext.Day, dtNext.Hour, dtNext.Minute, dtNext.Second);
                                        sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                    }
                                    break;
                                }
                            case ST_YEARLY: // Ex: Jan, July only.
                                {
                                    dtNext = Convert.ToDateTime(sNextRunDttm).AddMonths(1);
                                    //jramkumar for MITS 34696
                                    for (int i = 0; i < 12; i++)
                                    {
                                        if ((dtNext.Month == (int)Month.January && objReader.GetInt16("JAN_RUN") == -1) || (dtNext.Month == (int)Month.February && objReader.GetInt16("FEB_RUN") == -1) || (dtNext.Month == (int)Month.March && objReader.GetInt16("MAR_RUN") == -1) || (dtNext.Month == (int)Month.April && objReader.GetInt16("APR_RUN") == -1) || (dtNext.Month == (int)Month.May && objReader.GetInt16("MAY_RUN") == -1) || (dtNext.Month == (int)Month.June && objReader.GetInt16("JUN_RUN") == -1))
                                        {
                                            sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                            break;
                                        }
                                        else if ((dtNext.Month == (int)Month.July && objReader.GetInt16("JUL_RUN") == -1) || (dtNext.Month == (int)Month.August && objReader.GetInt16("AUG_RUN") == -1) || (dtNext.Month == (int)Month.September && objReader.GetInt16("SEP_RUN") == -1) || (dtNext.Month == (int)Month.October && objReader.GetInt16("OCT_RUN") == -1) || (dtNext.Month == (int)Month.November && objReader.GetInt16("NOV_RUN") == -1) || (dtNext.Month == (int)Month.December && objReader.GetInt16("DEC_RUN") == -1))
                                        {
                                            sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                            break;
                                        }
                                        dtNext = dtNext.AddMonths(1);
                                    }
                                    if (Conversion.ConvertStrToLong(sNewNextRunDttm) < Conversion.ConvertStrToLong(DateTime.Now.ToString("yyyyMMddHHmmss")))
                                    {
                                        //jramkumar for MITS 34696
                                        dtNext = new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, dtNext.Day, dtNext.Hour, dtNext.Minute, dtNext.Second);
                                        for (int i = 0; i < 12; i++)
                                        {
                                            if ((dtNext.Month == (int)Month.January && objReader.GetInt16("JAN_RUN") == -1) || (dtNext.Month == (int)Month.February && objReader.GetInt16("FEB_RUN") == -1) || (dtNext.Month == (int)Month.March && objReader.GetInt16("MAR_RUN") == -1) || (dtNext.Month == (int)Month.April && objReader.GetInt16("APR_RUN") == -1) || (dtNext.Month == (int)Month.May && objReader.GetInt16("MAY_RUN") == -1) || (dtNext.Month == (int)Month.June && objReader.GetInt16("JUN_RUN") == -1))
                                            {
                                                sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                                break;
                                            }
                                            else if ((dtNext.Month == (int)Month.July && objReader.GetInt16("JUL_RUN") == -1) || (dtNext.Month == (int)Month.August && objReader.GetInt16("AUG_RUN") == -1) || (dtNext.Month == (int)Month.September && objReader.GetInt16("SEP_RUN") == -1) || (dtNext.Month == (int)Month.October && objReader.GetInt16("OCT_RUN") == -1) || (dtNext.Month == (int)Month.November && objReader.GetInt16("NOV_RUN") == -1) || (dtNext.Month == (int)Month.December && objReader.GetInt16("DEC_RUN") == -1))
                                            {
                                                sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                                break;
                                            }
                                            dtNext = dtNext.AddMonths(1);
                                        }
                                    }
                                    break;
                                }
                            case ST_DAILY:
                                {
                                    dtNext = Convert.ToDateTime(sNextRunDttm).AddDays(1);
                                    for (int i = 0; i < 7; i++)
                                    {
                                        if ((dtNext.DayOfWeek == DayOfWeek.Monday && objReader.GetInt16("MON_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Tuesday && objReader.GetInt16("TUE_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Wednesday && objReader.GetInt16("WED_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Thursday && objReader.GetInt16("THU_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Friday && objReader.GetInt16("FRI_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Saturday && objReader.GetInt16("SAT_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Sunday && objReader.GetInt16("SUN_RUN") == -1))
                                        {
                                            sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                            break;
                                        }
                                        dtNext = dtNext.AddDays(1);
                                    }
                                    if (Conversion.ConvertStrToLong(sNewNextRunDttm) < Conversion.ConvertStrToLong(DateTime.Now.ToString("yyyyMMddHHmmss")))
                                    {
                                        dtNext = DateTime.Now.AddDays(1);
                                        for (int i = 0; i < 7; i++)
                                        {
                                            if ((dtNext.DayOfWeek == DayOfWeek.Monday && objReader.GetInt16("MON_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Tuesday && objReader.GetInt16("TUE_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Wednesday && objReader.GetInt16("WED_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Thursday && objReader.GetInt16("THU_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Friday && objReader.GetInt16("FRI_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Saturday && objReader.GetInt16("SAT_RUN") == -1) || (dtNext.DayOfWeek == DayOfWeek.Sunday && objReader.GetInt16("SUN_RUN") == -1))
                                            {
                                                sNewNextRunDttm = Conversion.ToDbDateTime(dtNext);
                                                break;
                                            }
                                            dtNext = dtNext.AddDays(1);
                                        }
                                    }
                                    break;
                                }
                            case ST_DIRECTORY_TRIGGERED:// Ex: "New File Added"
                                break;
                            default:
                                break;
                        }

                        if (!sNewNextRunDttm.Equals(string.Empty))
                        {
                            sSQL = "UPDATE TM_SCHEDULE SET NEXT_RUN_DTTM='" + sNewNextRunDttm + "' WHERE SCHEDULE_ID=" + iScheduleId + " AND NEXT_RUN_DTTM='" + Conversion.ToDbDateTime(Convert.ToDateTime(sNextRunDttm)) + "'";

                            // akaushik5 Changed for MITS 38134 Starts
                            //if (objConn != null)
                            //{
                            //    if (objConn.State == ConnectionState.Open)
                            //        objConn.Close();
                            //}

                            //objConn.Open();
                            //if (objConn.ExecuteNonQuery(sSQL) == 1)
                            //{
                            //    if (objConn != null)
                            //    {
                            //        if (objConn.State == ConnectionState.Open)
                            //            objConn.Close();
                            //    }
                            using (DbConnection objConnection = DbFactory.GetDbConnection(sConnectionString))
                            {
                                objConnection.Open();
                                if (objConnection.ExecuteNonQuery(sSQL) == 1)
                                {
                                    // akaushik5 Changed for MITS 38134 Ends 
                                    sNextRunDttm = Conversion.ToDbDateTime(Convert.ToDateTime(sNextRunDttm));
                                    //int iJobID = GetNextUID("TM_JOBS");//Add & change by kuladeep for Cloud
                                    int iJobID = GetNextUID("TM_JOBS", iClientId);

                                // Check if it is a Data Integrator job.
                                if (IsDetailedStatusDisabled(iTaskTypeId, iClientId))//Add & change by kuladeep for Cloud
                                {
                                    sConfig = UpdateConfig(sConfig, iJobID);
                                }
                                //else if (IsPrintCheckBatch(iTaskTypeId))
                                //Added condition for checking Auto Mail Merge Starts Anu Tennyson
                                //else if (IsPrintCheckBatch(iTaskTypeId, iClientId) || IsAutoMailMerge(iTaskTypeId, iClientId))//Add & change by kuladeep for Cloud
                               //JIRA RMA-4606 nshah28- adding condition for checking CurrencyExchangeInterface starts
                                else if (IsPrintCheckBatch(iTaskTypeId, iClientId) || IsAutoMailMerge(iTaskTypeId, iClientId) || IsCurrencyExchange(iTaskTypeId,iClientId))//Add & change by kuladeep for Cloud
                                {
                                    sConfig = UpdateConfig(sConfig, iJobID);
                                }

                                    // akaushik5 Changed for MITS 38134 Starts
                                    //objConntemp.Open();
                                    //int count = Conversion.ConvertObjToInt(objConntemp.ExecuteScalar("select count(JOB_ID) from TM_JOBS where TM_DSN = '" +
                                    //    sDataSourceName + "' and JOB_NAME = 'Policy System Update'"),iClientId);
                                    //if (objConntemp != null)
                                    //{
                                    //    if (objConntemp.State == ConnectionState.Open)
                                    //        objConntemp.Close();
                                    //}
                                    int count = Conversion.ConvertObjToInt(objConnection.ExecuteScalar(string.Format("SELECT COUNT(JOB_ID) FROM TM_JOBS WHERE TM_DSN = '{0}' AND JOB_NAME = 'Policy System Update'", sDataSourceName)), iClientId);
                                    // akaushik5 Changed for MITS 38134 Ends
                                    if (count > 0)
                                        break;
                                    sSQL = "INSERT INTO TM_JOBS (JOB_ID,TASK_TYPE_ID,JOB_STATE_ID,START_DTTM,END_DTTM,TARGET_HOST,CONFIG,MESSAGE,JOB_NAME,OPTIONSET_ID,TM_USER,TM_DSN)";
                                    sSQL = sSQL + " SELECT " + iJobID + ",TASK_TYPE_ID,1,'" + sNextRunDttm + "',NULL,'" + System.Environment.MachineName + "','" + sConfig + "',NULL,TASK_NAME,OPTIONSET_ID,TM_USER,TM_DSN FROM TM_SCHEDULE WHERE SCHEDULE_ID=" + iScheduleId;

                                    m_hstUserLoginName.Add(iClientId + "_" + iJobID, sUserName);//Add & change by kuladeep for Cloud
                                    m_hstDSNName.Add(iClientId + "_" + iJobID, sDataSourceName);
                                    m_hstJobName.Add(iClientId + "_" + iJobID, sJobName);
                                    // akasuhik5 Changed for MITS 38134 Starts
                                    //objConn.Open();
                                    //objConn.ExecuteNonQuery(sSQL);
                                    objConnection.ExecuteNonQuery(sSQL);
                                }

                                if (!object.ReferenceEquals(objConnection, null) && objConnection.State.Equals(ConnectionState.Open))
                                {
                                    objConnection.Close();
                                }
                                // akasuhik5 Changed for MITS 38134 Ends
                            }
                        }
                        else
                        {
                            // akaushik5 Commented for MITS 38134 Starts
                            //if (objConn != null)
                            //{
                            //    if (objConn.State == ConnectionState.Open)
                            //        objConn.Close();
                            //}
                            // akaushik5 Commented for MITS 38134 Ends
                            sNextRunDttm = Conversion.ToDbDateTime(Convert.ToDateTime(sNextRunDttm));
                            //int iJobID = GetNextUID("TM_JOBS");//Add & change by kuladeep for Cloud
                            int iJobID = GetNextUID("TM_JOBS", iClientId);
                            //Ashish Ahuja MITS 34537 Start
                            if (sJobName == "ProgressNotes")
                            {
                                sConfig = sConfig.Replace("'", "''");
                                int sindex = sConfig.IndexOf("<arg>-nf") + 8;
                                int mindex = sConfig.IndexOf("</arg>", sindex);
                                sConfig.Substring(sindex + 1);
                                sConfig = sConfig.Substring(0, sindex) + "<![CDATA[\"" + sConfig.Substring(sindex, mindex - sindex) + "\"]]>" + sConfig.Substring(mindex);
                            }
                            //Ashish Ahuja MITS 34537 End
                            // Check if it is a Data Integrator job.
                            if (IsDetailedStatusDisabled(iTaskTypeId, iClientId))//Add & change by kuladeep for Cloud
                            {
                                sConfig = UpdateConfig(sConfig, iJobID);//Add & change by kuladeep for Cloud
                            }
                            //Added condition for checking Auto Mail Merge Starts Anu Tennyson
                           // else if (IsPrintCheckBatch(iTaskTypeId, iClientId) || IsAutoMailMerge(iTaskTypeId, iClientId))//Add & change by kuladeep for Cloud
                            //JIRA RMA-4606 nshah28- adding condition for checking CurrencyExchangeInterface starts
                            else if (IsPrintCheckBatch(iTaskTypeId, iClientId) || IsAutoMailMerge(iTaskTypeId, iClientId) || IsCurrencyExchange(iTaskTypeId, iClientId))//Add & change by kuladeep for Cloud
                            {
                                sConfig = UpdateConfig(sConfig, iJobID);//Add & change by kuladeep for Cloud
                            }
                            //Anu Tennyson : Ends
                            //sanoopsharma - checking if an already existing task is running for Policy System Update. 
                            // akaushik5 Changed for MITS 38134 Starts
                            //objConntemp.Open();

                            //int count = Conversion.ConvertObjToInt(objConntemp.ExecuteScalar("select count(JOB_ID) from TM_JOBS where TM_DSN = '" +
                            //    sDataSourceName + "' and JOB_NAME = 'Policy System Update'"),iClientId);

                            //if (objConntemp != null)
                            //{
                            //    if (objConntemp.State == ConnectionState.Open)
                            //        objConntemp.Close();
                            //}
                            //if (count > 0)
                            //    break;
                            if (TaskManager.GetPolicySystemCount(iClientId, sDataSourceName, sConnectionString) > 0)
                            {
                                break;
                            }
                            // akaushik5 Changed for MITS 38134 Starts
                            
                            //sanoopsharma END
                            sSQL = "INSERT INTO TM_JOBS (JOB_ID,TASK_TYPE_ID,JOB_STATE_ID,START_DTTM,END_DTTM,TARGET_HOST,CONFIG,MESSAGE,JOB_NAME,OPTIONSET_ID,TM_USER,TM_DSN)";
                            sSQL = sSQL + " SELECT " + iJobID + ",TASK_TYPE_ID,1,'" + sNextRunDttm + "',NULL,'" + System.Environment.MachineName + "','" + sConfig + "',NULL,TASK_NAME,OPTIONSET_ID,TM_USER,TM_DSN FROM TM_SCHEDULE WHERE SCHEDULE_ID=" + iScheduleId;

                            m_hstUserLoginName.Add(iClientId + "_" + iJobID, sUserName);//Add & change by kuladeep for Cloud
                            m_hstDSNName.Add(iClientId + "_" + iJobID, sDataSourceName);
                            m_hstJobName.Add(iClientId + "_" + iJobID, sJobName);
                            // akaushik5 Changed for MITS 38134 Starts
                            //objConn.Open();
                            //if (objConn.ExecuteNonQuery(sSQL) == 1)
                            //{
                            //    if (objConn != null)
                            //    {
                            //        if (objConn.State == ConnectionState.Open)
                            //            objConn.Close();
                            //    }
                            //    objConn.Open();
                            //    sSQL = "DELETE FROM TM_SCHEDULE WHERE SCHEDULE_ID=" + iScheduleId;
                            //    objConn.ExecuteNonQuery(sSQL);
                            //}
                            using (DbConnection objConnection = DbFactory.GetDbConnection(sConnectionString))
                            {
                                objConnection.Open();
                                if (objConnection.ExecuteNonQuery(sSQL) == 1)
                                {
                                    sSQL = "DELETE FROM TM_SCHEDULE WHERE SCHEDULE_ID=" + iScheduleId;
                                    objConnection.ExecuteNonQuery(sSQL);
                                }

                                if (!object.ReferenceEquals(objConnection, null) && objConnection.State.Equals(ConnectionState.Open))
                                {
                                    objConnection.Close();
                                }
                            }
                            // akaushik5 Changed for MITS 38134 Ends
                        }
                    }
                }
            }


            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("checkschedule : RMAppException", p_objEx.InnerException.Message + m_sConnectionString);
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                logErrors("CheckSchedule", null, false, systemErrors, iClientId);
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("checkschedule : Exception", p_objEx.InnerException.Message + m_sConnectionString);
                this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.CheckSchedule.Error", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("CheckSchedule", null, false, systemErrors, iClientId);
            }

            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                // akasuhik5 Commented for MITS 38134 Starts
                //if (objConntemp != null)
                //{
                //    objConntemp.Dispose();
                //}
                // akasuhik5 Commented for MITS 38134 Ends
            }
        }

        /// <summary>
        /// Gets the policy system count.
        /// </summary>
        /// <param name="iClientId">The i client identifier.</param>
        /// <param name="sDataSourceName">Name of the s data source.</param>
        /// <param name="sConnectionString">The s connection string.</param>
        /// <returns></returns>
        private static int GetPolicySystemCount(int iClientId, string sDataSourceName, string sConnectionString)
        {
            int count = default(int);
            using (DbConnection objConnection = DbFactory.GetDbConnection(sConnectionString))
            {
                objConnection.Open();
                count = Conversion.ConvertObjToInt(objConnection.ExecuteScalar(string.Format("SELECT COUNT(JOB_ID) FROM TM_JOBS WHERE TM_DSN = '{0}' AND JOB_NAME = 'Policy System Update'", sDataSourceName)), iClientId);

                if (!object.ReferenceEquals(objConnection, null) && objConnection.State.Equals(ConnectionState.Open))
                {
                    objConnection.Close();
                }
            }
            return count;
        }

        //rsolanki2 : checking if it is a singleton task, in which case only one task will run at a time.
        private bool checkForSingleTon(int iTaskTypeId, out string sExeName, int iClientId = 0)
        {
            try
            {
                string sSQL = string.Empty;
                sExeName = string.Empty;
                DbConnection objConn = null;
                //Add & change by kuladeep for Cloud-Start
                string sConnectionString = string.Empty;
                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //Add & change by kuladeep for Cloud-End

                systemErrors = new BusinessAdaptorErrors(iClientId);

                sSQL = "SELECT CONFIG FROM TM_TASK_TYPE WHERE TASK_TYPE_ID =  " + iTaskTypeId.ToString();
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();

                XDocument xd = XDocument.Parse((objConn.ExecuteScalar(sSQL).ToString()));
                XElement xe = xd.XPathSelectElement("//Task");//.Attribute("IsSingleTon").Value;
                if (xe != null
                    && xe.Value != null
                    && xe.Attribute("IsSingleTon") != null)
                {
                    string sAttr = xe.Attribute("IsSingleTon").Value;
                    bool bSuccess = false;
                    if (Conversion.CastToType<bool>(sAttr, out bSuccess))
                    {
                        sExeName = xe.Attribute("Name").Value;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //this.systemErrors.Add(ex, Globalization.GetString("Riskmaster.Service.TaskManagement.checkForSingleTon.Error"), BusinessAdaptorErrorType.Error);
                this.systemErrors.Add(ex, "Riskmaster.Service.TaskManagement.checkForSingleTon.Error", BusinessAdaptorErrorType.Error);
                logErrors("checkForSingleTon", null, false, systemErrors, iClientId);                
                throw;
            }
        }

        /// <summary>
        /// Calculates next scheduled periodic DTTM from current DTTM.
        /// </summary>
        /// <param name="p_iIntervalTypeId">Interval Type</param>
        /// <param name="p_iInterval">Interval</param>
        /// <param name="p_sNextRunDttm">Current DTTM for which next periodical DTTM has to be calculated</param>
        /// <returns>Next Run Date and Time</returns>
        private string GetNextNewRunDTTM(int p_iIntervalTypeId, int p_iInterval, string p_sNextRunDttm)
        {
            string sNewNextRunDttm = string.Empty;
            const int PRD_MINS = 1;
            const int PRD_DAYS = 2;
            const int PRD_WEEKS = 3;
            const int PRD_MONTHS = 4;

            try
            {
                switch (p_iIntervalTypeId)
                {
                    case PRD_MINS:
                        sNewNextRunDttm = Conversion.ToDbDateTime(Convert.ToDateTime(p_sNextRunDttm).AddMinutes(p_iInterval));
                        break;
                    case PRD_DAYS:
                        sNewNextRunDttm = Conversion.ToDbDateTime(Convert.ToDateTime(p_sNextRunDttm).AddDays(p_iInterval));
                        break;
                    case PRD_WEEKS:
                        sNewNextRunDttm = Conversion.ToDbDateTime(Convert.ToDateTime(p_sNextRunDttm).AddDays(p_iInterval * 7));
                        break;
                    case PRD_MONTHS:
                        sNewNextRunDttm = Conversion.ToDbDateTime(Convert.ToDateTime(p_sNextRunDttm).AddMonths(p_iInterval));
                        break;
                    default:
                        break;
                }

                return sNewNextRunDttm;
            }
            catch { return ""; }


        }

        /// <summary>
        /// This method will check db to see if the Archive needs to be deleted.
        /// </summary>
        private void CheckArchive(int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            ArrayList objArrToDel = null;
            string sConnectionString = string.Empty;//Add & change by kuladeep for Cloud

            try
            {
                //Add & change by kuladeep for Cloud-Start
                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //Add & change by kuladeep for Cloud-End
                systemErrors = new BusinessAdaptorErrors(iClientId);

                objArrToDel = new ArrayList();
                sSQL = "SELECT JOB_ID, JOB_HIST_ID, START_DTTM, END_DTTM FROM TM_JOBS_HIST ORDER BY JOB_ID ASC";

                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();

                using (objReader = objConn.ExecuteReader(sSQL))
                {
                    while (objReader.Read())
                    {
                        TimeSpan objTimeStamp = DateTime.Today - Conversion.ToDate(objReader.GetString("END_DTTM"));
                        //PJS 04-24-09 - MITS 15692 - Corrected logic ex: 1.11 days gives 1 days
                        if (objTimeStamp.Days >= Conversion.ConvertObjToInt(objTMSettings["ArchiveDays"],iClientId))
                        {
                            objArrToDel.Add(objReader.GetValue("JOB_ID"));
                        }
                    }
                }
                objConn.Close();

                if (objArrToDel.Count > 0)
                {
                    // ABhateja 12.12.2009 : Mits 19084
                    // Used "BETWEEN" instead of "IN" as "IN" fails in Oracle for an expression containing more than 1000 values.

                    sSQL = "DELETE FROM TM_JOBS_HIST WHERE JOB_ID BETWEEN " + objArrToDel[0].ToString() + " AND " + objArrToDel[objArrToDel.Count - 1].ToString();

                    objConn.Open();
                    objConn.ExecuteNonQuery(sSQL);
                    objConn.Close();

                    sSQL = "DELETE FROM TM_JOB_LOG WHERE JOB_ID BETWEEN " + objArrToDel[0].ToString() + " AND " + objArrToDel[objArrToDel.Count - 1].ToString();

                    objConn.Open();
                    objConn.ExecuteNonQuery(sSQL);
                    objConn.Close();
                }
            }
            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("CheckArchive : RMAppException", p_objEx.InnerException.Message);
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                logErrors("CheckArchive", null, false, systemErrors, iClientId);
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("CheckArchive : Exception", p_objEx.InnerException.Message);
                this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.CheckArchive.Error", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("CheckArchive", null, false, systemErrors, iClientId);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }

            }
        }

        /// <summary>
        /// This method will look for the job to be run or killed in the TM_JOBS table.
        /// </summary>
        private void LookForJob(int iClientId = 0)
        {
            string sSQL = string.Empty;
            string sConfig = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            bool bRepeat = false;
            int iJobId = 0;
            int iTaskTypeId = 0;
            //Add & change by kuladeep for Cloud
            string sConnectionString = string.Empty;
            Job m_objJob = null;
            try
            {
                //Add & change by kuladeep for Cloud-Start
                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //Add & change by kuladeep for Cloud-End

                systemErrors = new BusinessAdaptorErrors(iClientId);

                do
                {
                    sSQL = "SELECT * FROM TM_JOBS WHERE JOB_STATE_ID=1";
                    sSQL = sSQL + " AND (START_DTTM IS NULL OR START_DTTM <= " + Conversion.GetDateTime(DateTime.Now.ToString()) + ")";

                    objConn = DbFactory.GetDbConnection(sConnectionString);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (objReader.Read())
                        {
                            iJobId = objReader.GetInt32("JOB_ID");
                            iTaskTypeId = objReader.GetInt32("TASK_TYPE_ID");
                            sConfig = objReader.GetString("CONFIG");

                            if (objConn != null)
                            {
                                if (objConn.State == ConnectionState.Open)
                                    objConn.Close();
                            }

                            objConn.Open();

                            int iCount = Conversion.ConvertObjToInt(objConn.ExecuteNonQuery(String.Format("UPDATE TM_JOBS SET JOB_STATE_ID=2, TARGET_HOST='" + System.Environment.MachineName + "', START_DTTM = '" + Conversion.GetDateTime(DateTime.Now.ToString()) + "' WHERE JOB_ID={0} AND JOB_STATE_ID=1", iJobId)), iClientId);

                            if (iCount >= 1)
                            {
                                m_objJob = new Job();
                                m_objJob.Config = sConfig;
                                m_objJob.JobId = iJobId;
                                m_objJob.TaskTypeId = iTaskTypeId;
                                m_objJob.ClientId = iClientId;
                                bRepeat = false;
                            }
                            else
                            {
                                bRepeat = true;
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                } while (bRepeat);

                if (m_objJob != null)
                {
                    string sXml = m_objJob.Config;

                    if (!sXml.Equals(string.Empty))
                    {
                        XmlDocument xmlJob = new XmlDocument();
                        xmlJob.LoadXml(sXml);

                        //Add & change by kuladeep for Cloud-Start
                        RunJob(xmlJob, m_objJob, iClientId);
                        //RunJob(xmlJob);
                        //Add & change by kuladeep for Cloud-End
                    }

                }
            }
            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("LookForJob : RMAppException", p_objEx.InnerException.Message);
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                logErrors("LookForJob", null, false, systemErrors, iClientId);
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("LookForJob : Exception", p_objEx.InnerException.Message);
                this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.LookForJob.Error", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("LookForJob", null, false, systemErrors, iClientId);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }

            }
        }

        /// <summary>
        /// This method will look for the job to be killed in the TM_JOBS table.
        /// </summary>
        private void CheckForKilling(int iClientId = 0)
        {
            string sSQL = string.Empty;
            string sConfig = string.Empty;
            DbReader objReader = null;
            DbConnection objConn = null;
            bool bRepeat = false;
            int iJobId = 0;
            string sConnectionString = string.Empty;//Add & change by kuladeep for Cloud

            try
            {
                //Add & change by kuladeep for Cloud-Start
                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //Add & change by kuladeep for Cloud-End
                systemErrors = new BusinessAdaptorErrors(iClientId);

                do
                {
                    sSQL = "SELECT * FROM TM_JOBS WHERE JOB_STATE_ID=5";
                    sSQL = sSQL + " AND (START_DTTM IS NULL OR START_DTTM <= " + Conversion.GetDateTime(DateTime.Now.ToString()) + ")";

                    objConn = DbFactory.GetDbConnection(sConnectionString);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (objReader.Read())
                        {
                            iJobId = objReader.GetInt32("JOB_ID");

                            if (objConn != null)
                            {
                                if (objConn.State == ConnectionState.Open)
                                    objConn.Close();
                            }

                            objConn.Open();

                            int iCount = Conversion.ConvertObjToInt(objConn.ExecuteNonQuery(String.Format("UPDATE TM_JOBS SET JOB_STATE_ID=6, TARGET_HOST='" + System.Environment.MachineName + "' WHERE JOB_ID={0} AND JOB_STATE_ID=5", iJobId)),iClientId);

                            if (iCount >= 1)
                            {
                                Process proc = Process.GetProcessById((int)ProcessManager.m_StaticColProcessByJobId[iClientId + "_" + iJobId]);//Change by kuladeep for cloud.
                                proc.Kill();
                                ProcessManager.m_StaticColProcessByJobId.Remove(iClientId + "_" + iJobId);//Change by kuladeep for cloud.
                                bRepeat = false;
                            }
                            else
                            {
                                bRepeat = true;
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                } while (bRepeat);

            }
            catch (RMAppException p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                logErrors("CheckForKilling", null, false, systemErrors, iClientId);
            }
            catch (Exception p_objEx)
            {
                this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.CheckForKilling.Error", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("CheckForKilling", null, false, systemErrors, iClientId);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }

            }
        }

        /// <summary>
        /// This method removes the record from TM_JOBS and creates the record in TM_JOBS_HIST.
        /// </summary>
        public void ArchiveJob(int p_iJobId, int p_iJobStateId, int iClientId = 0)
        {
            string sSQL = string.Empty;
            string sConfig = string.Empty;
            DbConnection objConn = null;
            string sConnectionString = string.Empty;//Add & change by kuladeep for Cloud

            try
            {
                //Add & change by kuladeep for Cloud-Start
                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //send mail before table insertion incase job crashes.
                if (p_iJobStateId == 4 || p_iJobStateId == 7 || p_iJobStateId == 8)  //tmalhotra3: Email notification Enh :RMA 4613
                {
                    SendEmailNotification(p_iJobId, p_iJobStateId, iClientId);
                }
                if (p_iJobStateId == 4)
                {
                    GenerateDiary(p_iJobId, true, iClientId);//Add & change by kuladeep for Cloud
                }

                int iJobHistId = GetNextUID("TM_JOBS_HIST", iClientId); //Add & change by kuladeep for Cloud

                sSQL = "INSERT INTO TM_JOBS_HIST (JOB_HIST_ID,JOB_ID,TASK_TYPE_ID,JOB_STATE_ID,START_DTTM,END_DTTM,CONFIG,TARGET_HOST,MESSAGE,JOB_NAME,OPTIONSET_ID)";
                sSQL = sSQL + " SELECT " + iJobHistId + ",JOB_ID,TASK_TYPE_ID," + p_iJobStateId + ",START_DTTM,'" + Conversion.GetDateTime(DateTime.Now.ToString()) + "',CONFIG,TARGET_HOST,MESSAGE,JOB_NAME,OPTIONSET_ID FROM TM_JOBS WHERE JOB_ID=" + p_iJobId;

                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();

                if (objConn.ExecuteNonQuery(sSQL) == 1)
                {
                    if (objConn != null)
                    {
                        if (objConn.State == ConnectionState.Open)
                            objConn.Close();
                    }
                    sSQL = "DELETE FROM TM_JOBS WHERE JOB_ID=" + p_iJobId;

                    objConn.Open();
                    objConn.ExecuteNonQuery(sSQL);
                }

            }
            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("Archive Job : RMAppException", p_objEx.Message);
                //this.systemErrors.Add(p_objEx, Globalization.GetString("TaskManagementAdaptor.WorkerMethod.Error"), BusinessAdaptorErrorType.Error);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("Archive Job : Exception", p_objEx.Message);
                //this.systemErrors.Add(p_objEx, Globalization.GetString("TaskManagementAdaptor.WorkerMethod.Error"), BusinessAdaptorErrorType.Error);
                throw new RMAppException(Globalization.GetString("Riskmaster.Service.TaskManagement.ArchiveJob.Error", iClientId), p_objEx);
            }

            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }
        }

        /// <summary>
        /// Logs the status of the job to TM_JOB_LOG table.
        /// </summary>
        /// <param name="p_iJobId">Job Id.</param>
        /// <param name="p_sData">Output data retrieved from the running job.</param>
        public void LogStatus(int p_iJobId, string p_sData, int iClientId = 0)
        {
            DbConnection objConn = null;
            DbReader objReader = null;
            string sSQL = string.Empty;
            string sMessage = string.Empty;
            int iJobLogId = 0;
            XmlDocument objTempDoc = null;
            XmlElement objTempElement = null;
            XmlElement objTempChild = null;
            XmlAttribute objTempAttr = null;

            const int ST_COMPLETED = 3;
            const int ST_COMPLETED_WITH_ERROR = 4;
            const int ST_KILLED = 6;
            const int ST_FAILED_TO_LAUNCH = 7;
            const string TASKTYPE_FULL = "full";
            const string TASKTYPE_ONCOMPLETION = "oncompletion";
            //Shruti for 12557
            StringBuilder sbSql = null;
            DbCommand objCmd = null;
            DbParameter objParam = null;
            //Shruti for 12557 ends
            string sConnectionString = string.Empty;//Add & change by kuladeep for Cloud
            try
            {
                //Add & change by kuladeep for Cloud-Start
                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //Add & change by kuladeep for Cloud-End            

                objConn = DbFactory.GetDbConnection(sConnectionString);

                sSQL = "SELECT JOB_LOG_ID, MESSAGE FROM TM_JOB_LOG WHERE JOB_ID = " + p_iJobId;
                objConn.Open();
                objReader = objConn.ExecuteReader(sSQL);
                if (objReader.Read())
                {
                    sMessage = objReader.GetString("MESSAGE");
                    iJobLogId = Conversion.ConvertObjToInt(objReader.GetValue("JOB_LOG_ID"),iClientId);
                }
                objReader.Close();
                objConn.Close();

                if (sMessage != null && sMessage.Trim() != string.Empty && p_sData.Replace("\0", "").Trim() != string.Empty)
                {
                    objTempDoc = new XmlDocument();
                    objTempDoc.LoadXml(sMessage);
                    objTempElement = (XmlElement)objTempDoc.SelectSingleNode("MessageList");
                    if (Conversion.ConvertObjToStr(objTMSettings["TaskType"]).ToLower() == TASKTYPE_FULL)
                    {
                        objTempChild = objTempDoc.CreateElement("Message");
                        objTempAttr = objTempDoc.CreateAttribute("date");
                        objTempAttr.Value = Conversion.ToDbDate(DateTime.Now);
                        objTempChild.Attributes.Append(objTempAttr);
                        objTempAttr = objTempDoc.CreateAttribute("time");
                        objTempAttr.Value = Conversion.ToDbTime(DateTime.Now);
                        objTempChild.Attributes.Append(objTempAttr);
                        objTempChild.InnerText = RemoveAmparsand(p_sData.Replace("\0", "").Trim());
                        objTempElement.AppendChild(objTempChild);

                        if (objConn != null)
                        {
                            if (objConn.State == ConnectionState.Open)
                                objConn.Close();
                        }
                        objConn.Open();
                        //Shruti for 12557
                        sbSql = new StringBuilder();
                        objCmd = objConn.CreateCommand();
                        objParam = objCmd.CreateParameter();
                        objParam.Direction = ParameterDirection.Input;
                        objParam.Value = objTempDoc.OuterXml;
                        objParam.ParameterName = "XML";
                        objParam.SourceColumn = "MESSAGE";
                        objCmd.Parameters.Add(objParam);
                        sbSql.Append("UPDATE TM_JOB_LOG SET MESSAGE=~XML~ WHERE JOB_LOG_ID=" + iJobLogId.ToString());
                        objCmd.CommandText = sbSql.ToString();
                        objCmd.ExecuteNonQuery();
                        //sSQL = "UPDATE TM_JOB_LOG "
                        //+ "SET MESSAGE = '" + objTempDoc.OuterXml + "'"
                        //+ "WHERE JOB_LOG_ID = " + iJobLogId.ToString();
                        //objConn.ExecuteNonQuery(sSQL);
                        //Shruti for 12557 ends
                    }
                    else if (Conversion.ConvertObjToStr(objTMSettings["TaskType"]).ToLower() == TASKTYPE_ONCOMPLETION)
                    {
                        sSQL = "SELECT JOB_STATE_ID FROM TM_JOBS WHERE JOB_ID = " + p_iJobId;
                        objConn.Open();
                        objReader = objConn.ExecuteReader(sSQL);
                        if (objReader.Read())
                        {
                            if (Conversion.ConvertObjToInt(objReader.GetValue("JOB_STATE_ID"), iClientId) == ST_COMPLETED ||
                                Conversion.ConvertObjToInt(objReader.GetValue("JOB_STATE_ID"), iClientId) == ST_COMPLETED_WITH_ERROR ||
                                Conversion.ConvertObjToInt(objReader.GetValue("JOB_STATE_ID"), iClientId) == ST_KILLED ||
                                Conversion.ConvertObjToInt(objReader.GetValue("JOB_STATE_ID"), iClientId) == ST_FAILED_TO_LAUNCH)
                            {
                                objTempChild = objTempDoc.CreateElement("Message");
                                objTempAttr = objTempDoc.CreateAttribute("date");
                                objTempAttr.Value = Conversion.ToDbDate(DateTime.Now);
                                objTempChild.Attributes.Append(objTempAttr);
                                objTempAttr = objTempDoc.CreateAttribute("time");
                                objTempAttr.Value = Conversion.ToDbTime(DateTime.Now);
                                objTempChild.Attributes.Append(objTempAttr);
                                objTempChild.InnerText = RemoveAmparsand(p_sData.Replace("\0", "").Trim());
                                objTempElement.AppendChild(objTempChild);

                                if (objConn != null)
                                {
                                    if (objConn.State == ConnectionState.Open)
                                        objConn.Close();
                                }
                                objConn.Open();
                                //Shruti for 12557
                                sbSql = new StringBuilder();
                                objCmd = objConn.CreateCommand();
                                objParam = objCmd.CreateParameter();
                                objParam.Direction = ParameterDirection.Input;
                                objParam.Value = objTempDoc.OuterXml;
                                objParam.ParameterName = "XML";
                                objParam.SourceColumn = "MESSAGE";
                                objCmd.Parameters.Add(objParam);
                                sbSql.Append("UPDATE TM_JOB_LOG SET MESSAGE=~XML~ WHERE JOB_LOG_ID=" + iJobLogId.ToString());
                                objCmd.CommandText = sbSql.ToString();
                                objCmd.ExecuteNonQuery();

                                //sSQL = "UPDATE TM_JOB_LOG "
                                //+ "SET MESSAGE = '" + objTempDoc.OuterXml + "'"
                                //+ "WHERE JOB_LOG_ID = " + iJobLogId.ToString();
                                //objConn.ExecuteNonQuery(sSQL);
                                //Shruti for 12557 ends
                            }
                        }
                        else
                        {
                            objTempChild = objTempDoc.CreateElement("Message");
                            objTempAttr = objTempDoc.CreateAttribute("date");
                            objTempAttr.Value = Conversion.ToDbDate(DateTime.Now);
                            objTempChild.Attributes.Append(objTempAttr);
                            objTempAttr = objTempDoc.CreateAttribute("time");
                            objTempAttr.Value = Conversion.ToDbTime(DateTime.Now);
                            objTempChild.Attributes.Append(objTempAttr);
                            objTempChild.InnerText = RemoveAmparsand(p_sData.Replace("\0", "").Trim());
                            objTempElement.AppendChild(objTempChild);

                            if (objConn != null)
                            {
                                if (objConn.State == ConnectionState.Open)
                                    objConn.Close();
                            }
                            objConn.Open();
                            //Shruti for 12557
                            //sSQL = "UPDATE TM_JOB_LOG "
                            //+ "SET MESSAGE = '" + objTempDoc.OuterXml + "'"
                            //+ "WHERE JOB_LOG_ID = " + iJobLogId.ToString();
                            //objConn.ExecuteNonQuery(sSQL);
                            sbSql = new StringBuilder();
                            objCmd = objConn.CreateCommand();
                            objParam = objCmd.CreateParameter();
                            objParam.Direction = ParameterDirection.Input;
                            objParam.Value = objTempDoc.OuterXml;
                            objParam.ParameterName = "XML";
                            objParam.SourceColumn = "MESSAGE";
                            objCmd.Parameters.Add(objParam);
                            sbSql.Append("UPDATE TM_JOB_LOG SET MESSAGE=~XML~ WHERE JOB_LOG_ID=" + iJobLogId.ToString());
                            objCmd.CommandText = sbSql.ToString();
                            objCmd.ExecuteNonQuery();
                            //Shruti for 12557
                        }
                    }
                }
                else if (p_sData.Replace("\0", "").Trim() != string.Empty)
                {
                    int iJobID = p_iJobId;
                    //Prepare the log message xml

                    objTempDoc = new XmlDocument();

                    objTempElement = objTempDoc.CreateElement("MessageList");
                    objTempDoc.AppendChild(objTempElement);

                    objTempChild = objTempDoc.CreateElement("Message");
                    objTempAttr = objTempDoc.CreateAttribute("date");
                    objTempAttr.Value = Conversion.ToDbDate(DateTime.Now);
                    objTempChild.Attributes.Append(objTempAttr);
                    objTempAttr = objTempDoc.CreateAttribute("time");
                    objTempAttr.Value = Conversion.ToDbTime(DateTime.Now);
                    objTempChild.Attributes.Append(objTempAttr);
                    objTempChild.InnerText = RemoveAmparsand(p_sData.Replace("\0", "").Trim());
                    objTempElement.AppendChild(objTempChild);

                    //Shruti for 12557
                    //sSQL = "INSERT INTO TM_JOB_LOG "
                    //    + "(JOB_LOG_ID,JOB_ID,LOG_TIME,MESSAGE) "
                    //    + "Values (" + iID + ", " + iJobID + ", '"
                    //    + Conversion.GetDateTime(DateTime.Now.ToString()) + "', '" + objTempDoc.OuterXml + "')";
                    if (objConn != null)
                    {
                        if (objConn.State == ConnectionState.Open)
                            objConn.Close();
                    }
                    objConn.Open();

                    int iID = GetNextUID("TM_JOB_LOG", iClientId);//Add & change by kuladeep for Cloud

                    sbSql = new StringBuilder();
                    objCmd = objConn.CreateCommand();
                    objParam = objCmd.CreateParameter();
                    objParam.Direction = ParameterDirection.Input;
                    objParam.Value = objTempDoc.OuterXml;
                    objParam.ParameterName = "XML";
                    objParam.SourceColumn = "MESSAGE";
                    objCmd.Parameters.Add(objParam);
                    sbSql.Append("INSERT INTO TM_JOB_LOG (JOB_LOG_ID,JOB_ID,LOG_TIME,MESSAGE) ");
                    sbSql.Append("Values (" + iID + ", " + iJobID + ", '");
                    sbSql.Append(Conversion.GetDateTime(DateTime.Now.ToString()) + "', ~XML~)");
                    objCmd.CommandText = sbSql.ToString();
                    objCmd.ExecuteNonQuery();
                    //objConn.ExecuteNonQuery(sSQL);
                    //Shruti for 12557 ends
                }
            }
            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("TaskManager.LogStatus", p_objEx.Message, EventLogEntryType.Error);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("TaskManager.LogStatus", p_objEx.Message);
                throw new RMAppException(Globalization.GetString("Riskmaster.Service.TaskManagement.LogStatus.Error", iClientId), p_objEx);
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
                if (objReader != null)
                {
                    objReader.Dispose();
                }
            }
        }

        /// <summary>
        /// This function will read the Configuration settings and run the Job accordingly.
        /// </summary>
        /// <param name="xmlJob"></param>
        private void RunJob(XmlDocument p_objXmlJob, Job m_objJob, int iClientId = 0)
        {
            XmlElement xmlTaskPath = null;    // Will point to the path of the task file
            string sTaskPath = string.Empty;
            string sArgs = string.Empty;

            try
            {
                // Input Xml Format.
                //<Task Name="WPA" cmdline="yes">
                //     <Path>C:\Riskmaster\RMX\R4\New Processing Service\Code\Service\ConsoleApplication1\bin\Debug\ConsoleApplication1.exe</Path>
                //     <Args>
                //          <arg>
                //          </arg>
                //          <arg>
                //          </arg>
                //     </Args>
                //</Task>

                systemErrors = new BusinessAdaptorErrors(iClientId);
                xmlTaskPath = (XmlElement)p_objXmlJob.SelectSingleNode("/Task/Path");
                if (xmlTaskPath == null)
                {
                    // Throw back system error if part of envelope not there.
                    systemErrors.Add("Riskmaster.Service.TaskManagement.MalformedEnvelope",
                        Globalization.GetString("Riskmaster.Service.TaskManagement.MalformedEnvelope", iClientId),
                        BusinessAdaptorErrorType.SystemError);

                    logErrors("RunJob", p_objXmlJob, false, systemErrors, iClientId);

                    return; // formatOutputXML(null, false, systemErrors);
                }


                sTaskPath = Conversion.ConvertObjToStr(objTMSettings["TaskPath"]) + xmlTaskPath.InnerText;
                //Shruti for 11882
                if (!File.Exists(sTaskPath))
                {
                    sTaskPath = xmlTaskPath.InnerText;
                }
                //Shruti for 11882

                sArgs = GetArgs(p_objXmlJob, iClientId);

                ProcessManager objProcMgr = new ProcessManager();

                bool bIsDetailedStatusDisabled = IsDetailedStatusDisabled(m_objJob.TaskTypeId, iClientId);

                //objProcMgr.Add(m_objJob.JobId, sTaskPath, sArgs, bIsDetailedStatusDisabled,iClientId);
                objProcMgr.Add(m_objJob.ClientJobId, sTaskPath, sArgs, bIsDetailedStatusDisabled, iClientId);

                //Add & change by kuladeep for Cloud-Start
                //ThreadParams objTP = new ThreadParams(objProcMgr.Get(m_objJob.JobId), m_objJob.JobId, bIsDetailedStatusDisabled);
                ThreadParams objTP = new ThreadParams(objProcMgr.Get(m_objJob.ClientJobId), m_objJob.JobId, bIsDetailedStatusDisabled, iClientId);
                //Add & change by kuladeep for Cloud-End

                ThreadGenerator objThrdGen = new ThreadGenerator();
                //objThrdGen.Add("Thread" + m_objJob.JobId);
                objThrdGen.Add("Thread" + m_objJob.ClientJobId, iClientId);

                objThrdGen.Get(0).Start(objTP);

                // For testing only. -START-
                // This is the temporary exit condition for now in the "Debug" mode.
                // It will be removed once the code is deployed as a service.
                //if (m_objJob.JobId == 150)
                //    m_bContinue = false;
                // For testing only. -END-

            }
            catch (RMAppException p_objEx)
            {
                //EventLog.WriteEntry("RunJob : RMAppException", p_objEx.InnerException.Message + m_sConnectionString);
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                //EventLog.WriteEntry("RunJob : Exception", p_objEx.InnerException.Message + m_sConnectionString);
                throw new RMAppException(Globalization.GetString("Riskmaster.Service.TaskManagement.RunJob.Error", iClientId), p_objEx);
            }

            finally
            { }
        }

        /// <summary>
        /// Gets the next UID from the TM_IDs table.
        /// </summary>
        /// <param name="p_sTableName"></param>
        /// <returns></returns>
        private int GetNextUID(string p_sTableName, int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            DbReader objReader = null;
            int iOrigUID = 0;
            int iNextUID = 0;
            int iCollisionRetryCount = 0;
            int iErrRetryCount = 0;
            int iRows = 0;

            const int COLLISION_RETRY_COUNT = 1000;
            const int ERROR_RETRY_COUNT = 5;
            string sConnectionString = string.Empty;//Add & change by kuladeep for Cloud
            try
            {
                //Add & change by kuladeep for Cloud-Start
                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
                else
                {
                    sConnectionString = m_sConnectionString;
                }
                //Add & change by kuladeep for Cloud-End

                do
                {
                    sSQL = "SELECT NEXT_ID FROM TM_IDS WHERE TABLE_NAME = '" + p_sTableName + "'";

                    objConn = DbFactory.GetDbConnection(sConnectionString);
                    objConn.Open();

                    using (objReader = objConn.ExecuteReader(sSQL))
                    {
                        if (!objReader.Read())
                        {
                            objReader.Close();
                            objReader = null;
                            throw new RMAppException(Globalization.GetString("Riskmaster.Service.TaskManagement.TaskManagement.GetNextUID.NoSuchTable", iClientId));
                        }

                        iNextUID = objReader.GetInt("NEXT_ID");
                        objReader.Close();
                        objReader = null;

                        // Compute next id
                        iOrigUID = iNextUID;
                        if (iOrigUID != 0)
                            iNextUID++;
                        else
                            iNextUID = 2;

                        // try to reserve id (searched update)
                        sSQL = "UPDATE TM_IDS SET NEXT_ID = " + iNextUID + " WHERE TABLE_NAME = '" + p_sTableName + "'";

                        // only add searched clause if no chance of a null originally
                        // in row (only if no records ever saved against table)   
                        if (iOrigUID != 0)
                            sSQL += " AND NEXT_ID = " + iOrigUID;

                        // Try update
                        try
                        {
                            iRows = objConn.ExecuteNonQuery(sSQL);
                        }
                        catch (Exception e)
                        {
                            iErrRetryCount++;
                            if (iErrRetryCount >= 5)
                            {
                                objConn.Close();
                                throw new RMAppException(Globalization.GetString("Riskmaster.Service.TaskManagement.TaskManagement.GetNextUID.ErrorModifyingDB", iClientId), e);
                            }
                        }
                        // if success, return
                        if (iRows == 1)
                        {
                            objConn.Close();
                            return iNextUID - 1;
                        }
                        else // collided with another user - try again (up to 1000 times)
                            iCollisionRetryCount++;

                    }

                } while ((iErrRetryCount < ERROR_RETRY_COUNT) && (iCollisionRetryCount < COLLISION_RETRY_COUNT));

                if (iCollisionRetryCount >= COLLISION_RETRY_COUNT)
                {
                    objConn.Close();
                    throw new RMAppException
                        (Globalization.GetString("Riskmaster.Service.TaskManagement.TaskManagement.GetNextUID.CollisionTimeout", iClientId));
                }
                objConn.Close();
                //shouldn't get here under normal conditions.
                return 0;

            }
            catch (RMAppException p_objEx)
            {
                throw p_objEx;
            }
            catch (Exception p_objEx)
            {
                throw new RMAppException(Globalization.GetString("Riskmaster.Service.TaskManagement.GetNextUID.Error", iClientId), p_objEx);
            }

            finally
            {
                if (objReader != null)
                {
                    objReader.Close();
                    objReader.Dispose();
                }
                if (objConn != null)
                {
                    if (objConn.State == System.Data.ConnectionState.Open)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Logs errors.Taken from the CWS code.
        /// </summary>
        /// <param name="sAdaptorMethod"></param>
        /// <param name="xmlRequest"></param>
        /// <param name="bCallResult"></param>
        /// <param name="errors"></param>
        public static void logErrors(string sAdaptorMethod, XmlDocument xmlRequest, bool bCallResult, BusinessAdaptorErrors errors, int iClientId)
        {

            // First, log the error to the error log

            // Add overall result status
            string sCall = sAdaptorMethod;
            if (bCallResult)
                sCall += " (Success)";
            else
                sCall += " (Error)";

            // Log the overall call failure and the input envelope (only if call failed)
            if (bCallResult == false)
            {
                LogItem oLogItem = new LogItem();
                oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                oLogItem.Category = "TaskManagerServiceLog";
                oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                oLogItem.Message = sAdaptorMethod + " call failed.";
                //if (xmlRequest != null)
                //    oLogItem.RMParamList.Add("XML Input Envelope", xmlRequest.OuterXml);
                Log.Write(oLogItem, iClientId);//rkaur27
            }

            // Iterate through individual errors and log them. If exceptions are present, those will be walked (via InnerException) and all exceptions in the list will be logged.
            if (errors != null)
            {
                foreach (BusinessAdaptorError err in errors)
                {
                    // ... build error type
                    string sErrorType = null;
                    switch (err.ErrorType)
                    {
                        case BusinessAdaptorErrorType.SystemError:
                            sErrorType = "SystemError";
                            break;
                        case BusinessAdaptorErrorType.Error:
                            sErrorType = "Error";
                            break;
                        case BusinessAdaptorErrorType.Warning:
                            sErrorType = "Warning";
                            break;
                        case BusinessAdaptorErrorType.Message:
                            sErrorType = "Message";
                            break;
                        case BusinessAdaptorErrorType.PopupMessage:
                            sErrorType = "PopupMessage";
                            break;
                        default:
                            sErrorType = "SystemError";
                            break;
                    };

                    // ... add individual error messages/warnings
                    if (err.oException == null)
                    {  // non-Exception case - use what is in ErrorCode and ErrorDescription directly.
                        // Init logfile item for this error
                        LogItem oLogItem = new LogItem();
                        oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                        oLogItem.Category = "TaskManagerServiceLog";
                        oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                        oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
                        oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);
                        oLogItem.RMParamList.Add("Call Error Type", sErrorType);

                        // ... add error code/number
                        oLogItem.RMParamList.Add("Error Code", err.ErrorCode);

                        // ... add error description
                        oLogItem.Message = err.ErrorDescription;

                        // Flush entry to log file
                        Log.Write(oLogItem, iClientId);
                    }  // non-Exception case
                    else
                    {  // Exception case
                        Exception exc = err.oException;

                        while (exc != null)
                        {
                            // Init logfile item for this error
                            LogItem oLogItem = new LogItem();
                            oLogItem.EventId = 0;   // Don't have a meaningful value for this yet
                            oLogItem.Category = "TaskManagerServiceLog";
                            oLogItem.RMParamList.Add("Adaptor Call/Result", sCall);
                            oLogItem.RMParamList.Add("Call Error Type", sErrorType);
                            oLogItem.RMParamList.Add("Caller Identity", errors.ActiveLoginName);
                            oLogItem.RMParamList.Add("Target DSN", errors.ActiveDataSourceName);


                            // ... determine error code - assembly + exception type
                            oLogItem.RMParamList.Add("Error Code", err.oException.Source + "." + err.oException.GetType().Name);

                            // ... add error description
                            if (err.ErrorDescription != "")
                                oLogItem.Message = err.ErrorDescription;
                            else
                                oLogItem.Message = err.oException.Message;

                            // ... dump exception info to log
                            oLogItem.RMParamList.Add("Exception Source", exc.Source);
                            oLogItem.RMParamList.Add("Exception Type", exc.GetType().Name);
                            oLogItem.RMParamList.Add("Exception Message", exc.Message);
                            oLogItem.RMParamList.Add("Exception StackTrace", exc.StackTrace);

                            // ... flush entry to log file
                            Log.Write(oLogItem, iClientId);

                            // ... get next exception in chain (if there is one)
                            exc = exc.InnerException;
                        }
                    }  // Exception case
                }
            }

        }

        public enum Month
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }

        #region Windows Service Related Code (Entry Point, Start\Stop)

        //#if ! DEBUG

        static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;

            //More than one user Service may run within the same process. To add
            //another service to this process, change the following line to
            //create a second service object. For example,

            //  ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};

            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new TaskManager() };

            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        //#endif


        public TaskManager(int ClientId = 0)
        {
            InitializeComponent();
         
            objTMSettings = RMConfigurationManager.GetSectionSettings("TaskManagerServiceSettings", string.Empty, 0);//sonali
           
            if (ClientId == 0 && (ConfigurationManager.AppSettings["CloudDeployed"] != null) && (ConfigurationManager.AppSettings["CloudDeployed"]).ToLower() == "false") //-It is Cloud functionality.                
            {
                 m_sConnectionString = RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", ClientId);
                 m_sConnectionSecurity = RMConfigurationManager.GetConfigConnectionString("RMXSecurity", ClientId);
            }
        }

        protected override void OnStart(string[] args)
        {
            int m_iClientId = 0;
            try
            {
                //Add & change by kuladeep for Cloud
                if ((ConfigurationManager.AppSettings["CloudDeployed"] != null) && (ConfigurationManager.AppSettings["CloudDeployed"]).ToLower() == "true") //-It is Cloud functionality.                
                {
                    dictMultiTenant = CommonFunctions.MultiTenantConnection(0);
                    if (dictMultiTenant != null)
                    {
                        foreach (KeyValuePair<int, CommonFunctions.MultiClientConnection> pair in dictMultiTenant)
                        {
                            int iClientId = pair.Key;
                            m_iClientId = iClientId;
                            object threadargs = new object[1] { iClientId };
                            Thread worker = new Thread(new ParameterizedThreadStart(WorkerMethod));
                            worker.Start(threadargs);
                        }
                    }
                }
                else//-It is base functionality.
                {
                    Thread worker = new Thread(new ThreadStart(WorkerMethod));
                    worker.Start();
                }
            }
            catch (Exception e)
            {
                //EventLog.WriteEntry("OnStart", e.InnerException.Message);
                //// Throw back error if business adaptor isn't trapping its own exceptions.
                systemErrors = new BusinessAdaptorErrors(m_iClientId);

                systemErrors.Add(e, BusinessAdaptorErrorType.SystemError);

                logErrors("OnStart", null, false, systemErrors, m_iClientId);
            }

        }

        /// <summary>
        /// MITS 19666 Remove timer because it's not reliable in Windows Service.
        /// this WorkerMethod function call only for base,so we are passing 0 as clientId 
        /// </summary>
        public void WorkerMethod()
        {
            int iClientId = 0;
            //Exception objException = null;
            bool bIsConnectionValid = false;

            //Check for the valid connection 
            bIsConnectionValid = CreateConnection();

            //Geeta R7 : Task Manager Diary 
            //Look for jobs in the running status when service starts up              
            if (bIsConnectionValid)
            {
                LookForRunningJobs();
            }

            do
            {
                try
                {
                    //Check for the valid connection 
                    bIsConnectionValid = CreateConnection();                    
                    if (bIsConnectionValid)
                    {
                        // Poll the TM_JOBS table
                        System.Threading.Thread.Sleep(Conversion.ConvertObjToInt(objTMSettings["JobTablePollTime"], iClientId));
                        CheckForKilling();
                        LookForJob();

                        // Poll the TM_SCHEDULE table
                        System.Threading.Thread.Sleep(Conversion.ConvertObjToInt(objTMSettings["ScheduleTablePollTime"], iClientId));
                        CheckSchedule();

                        if (Conversion.ConvertObjToBool(objTMSettings["ArchiveJobs"], iClientId))
                            CheckArchive();
                    }
                    else 
                    {
                        return;
                    }
                }
                catch (RMAppException p_objEx)
                {
                    systemErrors = new BusinessAdaptorErrors(iClientId);
                    systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                    logErrors("WorkerMethod", null, false, systemErrors, iClientId);
                }
                catch (Exception p_objEx)
                {
                    systemErrors = new BusinessAdaptorErrors(iClientId);
                    this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.WorkerMethod.Error", iClientId), BusinessAdaptorErrorType.Error);
                    logErrors("WorkerMethod", null, false, systemErrors, iClientId);
                }
            }while (m_bContinue);
        }
        public void WorkerMethod(object threadargs)
        {

            Array argArray = new object[1];
            argArray = (Array)threadargs;
            int iClientId = (int)argArray.GetValue(0);

            systemErrors = new BusinessAdaptorErrors(iClientId);

            //Exception objException = null;
            bool bIsConnectionValid = false;
            
            //Check for the valid connection 
            bIsConnectionValid = CreateConnection(iClientId);

            //Look for jobs in the running status when service starts up              
            if (bIsConnectionValid)
            {
                LookForRunningJobs(iClientId);
            }

            do
            {
                try
                {
                    //Check for the valid connection 
                    bIsConnectionValid = CreateConnection(iClientId);
                    if (bIsConnectionValid)
                    {
                        // Poll the TM_JOBS table
                        System.Threading.Thread.Sleep(Conversion.ConvertObjToInt(objTMSettings["JobTablePollTime"], iClientId));
                        CheckForKilling(iClientId);
                        LookForJob(iClientId);

                        // Poll the TM_SCHEDULE table
                        System.Threading.Thread.Sleep(Conversion.ConvertObjToInt(objTMSettings["ScheduleTablePollTime"], iClientId));
                        CheckSchedule(iClientId);

                        if (Conversion.ConvertObjToBool(objTMSettings["ArchiveJobs"],iClientId))
                            CheckArchive(iClientId);
                    }
                    else
                    {
                        return;
                    }
                }
                catch (RMAppException p_objEx)
                {
                    systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                    logErrors("WorkerMethod", null, false, systemErrors, iClientId);
                }
                catch (Exception p_objEx)
                {
                    this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.WorkerMethod.Error", iClientId), BusinessAdaptorErrorType.Error);
                    logErrors("WorkerMethod", null, false, systemErrors, iClientId);
                }
            } while (m_bContinue);

        }

        /// <summary>
        /// Get connectionstring value in case user may correct value
        /// </summary>
        private void GetConnectionString(int iClientId = 0)
        {
            if (iClientId != 0)//Check for Cloud functionality
            {
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                m_sConnectionString = StructConnString.TMConnectionString;

            }
            else
            {
                // Get the updated connection string.In case someone updates the connection string during this
                // elapsed time.
                string sCurrentDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                sCurrentDirectory = sCurrentDirectory + "\\connectionStrings.config";

                XDocument objXDoc = XDocument.Load(sCurrentDirectory);

                XElement objXElement = objXDoc.XPathSelectElement("/connectionStrings/add[@name='TaskManagerDataSource']");

                // akaushik5 Changed for MITS 38134 Starts
                // if connection string is encrypted then XElement will be null.
                // Don't try to reassign the value of connection string in that case.
                //string sConnectionString = objXElement.Attribute("connectionString").Value;
                this.m_sConnectionString = !object.ReferenceEquals(objXElement, null) && !object.ReferenceEquals(objXElement.Attribute("connectionString"), null)
                    ? objXElement.Attribute("connectionString").Value
                    : RMConfigurationManager.GetConfigConnectionString("TaskManagerDataSource", iClientId);
                    // akaushik5 Changed for MITS 38134 Ends
            }
        }

        protected override void OnStop()
        {
            m_bContinue = false;
            this.Dispose();
        }
        #endregion

        /// <summary>
        /// Gets the arguments entered while scheduling the task from Task config.
        /// </summary>
        /// <param name="p_objXML">Task config</param>
        /// <returns>A string containing space separated arguments</returns>
        private string GetArgs(XmlDocument p_objXML, int p_iClientId)
        {
            XmlNodeList objArgs = null;
            StringBuilder sbArgs = new StringBuilder();
            string objPath = string.Empty;
            bool bIsOsha = false;
            UserLogin objLogin = null;
            int iCounter = 0;
            string sDSNName = string.Empty;
            string sUserName = string.Empty;
            if (p_objXML == null)
                return sbArgs.ToString();

            if (p_objXML.SelectSingleNode("//Path") != null)
            {
                objPath = p_objXML.SelectSingleNode("//Path").InnerText;
            }
            if (objPath.Contains("Osha"))
            {
                bIsOsha = true;
            }

            objArgs = p_objXML.SelectNodes("//arg");
            if (objArgs != null)
            {
                foreach (XmlNode objArg in objArgs)
                {
                    if (iCounter == 0)
                        sDSNName = objArg.InnerText;
                    if (iCounter == 1)
                        sUserName = objArg.InnerText;
                    if ((objArg.Attributes["type"] != null) && ((objArg.Attributes["type"].Value == "password") || (objArg.Attributes["type"].Value == "connectionstring")))
                    {
                        objArg.InnerText = RMCryptography.DecryptString(objArg.InnerText);
                    }
                    if (p_objXML.SelectSingleNode("//Task") != null)
                    {
                        if (p_objXML.SelectSingleNode("//Task").Attributes["Name"] != null)
                        {
                            if (p_objXML.SelectSingleNode("//Task").Attributes["Name"].Value == "PrintCheckBatch" && objArg.Attributes["type"] != null && objArg.Attributes["type"].Value == "user")
                            {
                                objLogin = new UserLogin(sUserName, sDSNName, p_iClientId);
                                sbArgs.Append(BuildArgs(objArg.InnerXml, objLogin, p_iClientId));
                            }
                            else if (bIsOsha)
                            {
                                sbArgs.Append(objArg.InnerText + "|^|");
                            }
                            else
                            {
                                sbArgs.Append(objArg.InnerText + " ");
                            }
                        }
                    }
                    iCounter++;
                }
            }

            return sbArgs.ToString().Trim();
        }

        private string BuildArgs(string sInput, UserLogin p_objUserLogin, int p_iClientId)
        {
            StringBuilder sbArg = new StringBuilder();
            XmlDocument objDoc = new XmlDocument();
            objDoc.LoadXml(sInput);
            bool bFirstNode = true;
            foreach (XmlNode objNode in objDoc.SelectNodes("//PrintSettings"))
            {
                if (bFirstNode)
                    sbArg.Append("-a ");
                else
                    sbArg.Append(" -a ");
                sbArg.Append(objNode.SelectSingleNode("AccountId").InnerText);
                sbArg.Append("-");
                sbArg.Append(objNode.SelectSingleNode("StockId").InnerText);
                sbArg.Append("-");
                sbArg.Append(objNode.SelectSingleNode("OrgId").InnerText);
                sbArg.Append("-");
                sbArg.Append(objNode.SelectSingleNode("Order").InnerText.Replace(' ', '_'));
                sbArg.Append("-");
                sbArg.Append(objNode.SelectSingleNode("CombinedPayment").InnerText);
                sbArg.Append("-");
                sbArg.Append(objNode.SelectSingleNode("AutoPayment").InnerText);
                sbArg.Append("-");
                sbArg.Append(objNode.SelectSingleNode("RptType").InnerText);
				// npadhy - JIRA 6418 Starts. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
                //JIRA:4042 START: ajohari2
                //sbArg.Append("-");
                //sbArg.Append(objNode.SelectSingleNode("EFTPayment").InnerText);
                //sbArg.Append(" ");
                //JIRA:4042 End: 
                sbArg.Append("-");
                
                using (LocalCache objCache = new LocalCache(p_objUserLogin.objRiskmasterDatabase.ConnectionString, p_iClientId))
                {
                    // New Jobs Without any EFT Payment and with Distribution Type - Distribution type will come from config
                    if (objNode.SelectSingleNode("DistributionTypeId") != null)
                    {
                        sbArg.Append(objNode.SelectSingleNode("DistributionTypeId").InnerText);
                    }
                    // Old Jobs with EFT Payment in it. If the EFT Payment is true the we default it to EFT payments, 
                    else if (objNode.SelectSingleNode("DistributionTypeId") == null && (objNode.SelectSingleNode("EFTPayment") != null && objNode.SelectSingleNode("EFTPayment").InnerText.ToLower() == "true"))
                    {
                        sbArg.Append(objCache.GetCodeId("EFT", "DISTRIBUTION_TYPE"));
                    }
                    // Old Job prior to EFT Payment enhancement. Default to RML, and if EFT payment is false then also default to RML
                    else if (objNode.SelectSingleNode("DistributionTypeId") == null && (objNode.SelectSingleNode("EFTPayment") == null || (objNode.SelectSingleNode("EFTPayment") != null && objNode.SelectSingleNode("EFTPayment").InnerText.ToLower() == "false")))
                    {
                        sbArg.Append(objCache.GetCodeId("RML", "DISTRIBUTION_TYPE"));
                    }
                }
                bFirstNode = false;
				// npadhy - JIRA 6418 Ends. We do not want to specifically track EFT payment. If the Distribution type is EFT then it is an EFT Payment. So introducing Distribution Type and Commenting out EFTPayment
            }
            return sbArg.ToString();
        }
        /// <summary>
        /// Replace & , > from string
        /// </summary>
        /// <param name="p_sValue">string</param>
        /// <param name="p_iLength">length</param>
        /// <returns>string</returns>
        internal static string RemoveAmparsand(string p_sValue)
        {
            p_sValue = p_sValue.Replace("&", "&amp;");
            p_sValue = p_sValue.Replace(">", "&gt;");
            p_sValue = p_sValue.Replace("<", "&lt;");
            p_sValue = p_sValue.Replace("-", "&quot;");
            p_sValue = p_sValue.Replace("'", "&apos;");
            return p_sValue;
        }

        internal bool IsDetailedStatusDisabled(int p_iTaskTypeId, int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            string sConnectionString = string.Empty;
            if (iClientId != 0)//Check for Cloud functionality
            {
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                sConnectionString = StructConnString.TMConnectionString;
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }
            //Add & change by kuladeep for Cloud-End
            //sSQL = "SELECT DISABLE_DETAILED_STATUS FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + p_iTaskTypeId + " AND SYSTEM_MODULE_NAME <> 'PrintCheckBatch' AND SYSTEM_MODULE_NAME <> 'PolicySystemUpdate' ";
            //JIRA RMA-4606 nshah28(commenting above query)
            sSQL = "SELECT DISABLE_DETAILED_STATUS FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + p_iTaskTypeId + " AND SYSTEM_MODULE_NAME <> 'PrintCheckBatch' AND SYSTEM_MODULE_NAME <> 'PolicySystemUpdate' AND SYSTEM_MODULE_NAME <> 'CurrencyExchangeInterface' ";
            try
            {
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                return Conversion.ConvertObjToBool(objConn.ExecuteScalar(sSQL), iClientId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConn.Close();
                objConn.Dispose();
            }
        }

        //Anu Tennyson : Added for Auto Mail Merge Starts
        internal bool IsAutoMailMerge(int p_iTaskTypeId, int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            string sConnectionString = string.Empty;
            if (iClientId != 0)//Check for Cloud functionality
            {
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                sConnectionString = StructConnString.TMConnectionString;
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }
            //Add & change by kuladeep for Cloud-End

            sSQL = "SELECT 1 FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + p_iTaskTypeId + " AND SYSTEM_MODULE_NAME = 'AutoMailMerge'";
            try
            {
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                return Conversion.ConvertObjToBool(objConn.ExecuteScalar(sSQL),iClientId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConn.Close();
                objConn.Dispose();
            }
        }
        //Ends

        internal bool IsPrintCheckBatch(int p_iTaskTypeId, int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            //Add & change by kuladeep for Cloud-Start
            string sConnectionString = string.Empty;
            if (iClientId != 0)//Check for Cloud functionality
            {
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                sConnectionString = StructConnString.TMConnectionString;
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }
            //Add & change by kuladeep for Cloud-End

            sSQL = "SELECT 1 FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + p_iTaskTypeId + " AND SYSTEM_MODULE_NAME IN('PrintCheckBatch','PolicySystemUpdate')";
            try
            {
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                return Conversion.ConvertObjToBool(objConn.ExecuteScalar(sSQL), iClientId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConn.Close();
                objConn.Dispose();
            }
        }

        //JIRA RMA-4606 nshah28 start
        internal bool IsCurrencyExchange(int p_iTaskTypeId, int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            string sConnectionString = string.Empty;
            if (iClientId != 0)//Check for Cloud functionality
            {
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                sConnectionString = StructConnString.TMConnectionString;
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }
            //Add & change by kuladeep for Cloud-End

            sSQL = "SELECT 1 FROM TM_TASK_TYPE WHERE TASK_TYPE_ID = " + p_iTaskTypeId + " AND SYSTEM_MODULE_NAME = 'CurrencyExchangeInterface'";
            try
            {
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                return Conversion.ConvertObjToBool(objConn.ExecuteScalar(sSQL), iClientId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objConn.Close();
                objConn.Dispose();
            }
        }
        //JIRA RMA-4606 nshah28 end

        private string UpdateConfig(string p_sConfig, int p_iJobId)
        {
            XElement objXElement = XElement.Parse(p_sConfig);

            var objArgs = from args in objXElement.Descendants("arg")

                          select args;

            foreach (XElement arg in objArgs)
            {
                if (arg.Attribute("type") != null)
                {
                    if (arg.Attribute("type").Value == "di")
                    {
                        arg.Value = "-j " + p_iJobId.ToString();
                        break;
                    }
                    //Anu Tennyson : Added "AutoMerge" to support Auto Mail Merge Functionality Starts
                    if (arg.Attribute("type").Value == "print" || arg.Attribute("type").Value == "AutoMerge")
                    //Anu Tennyson Ends
                    {
                        arg.Value = p_iJobId.ToString();
                        break;
                    }
                    //JIRA RMA-4606 nshah28 start
                    if (arg.Attribute("type").Value == "currencyexchange")
                    {
                        arg.Value =  "-jb" + p_iJobId.ToString();
                        break;
                    }
                    //JIRA RMA-4606 nshah28 end
                }
            }

            return objXElement.ToString();
        }

        internal int GetTaskTypeId(int p_iJobId, int iClientId = 0)
        {
            string sSQL = string.Empty;
            DbConnection objConn = null;
            //Add & change by kuladeep for Cloud-Start
            string sConnectionString = string.Empty;
            if (iClientId != 0)//Check for Cloud functionality
            {
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                sConnectionString = StructConnString.TMConnectionString;
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }

            sSQL = "SELECT TASK_TYPE_ID FROM TM_JOBS WHERE JOB_ID = " + p_iJobId;

            objConn = DbFactory.GetDbConnection(sConnectionString);
            objConn.Open();

            return Conversion.ConvertObjToInt(objConn.ExecuteScalar(sSQL),iClientId);
        }

        /// <summary>
        /// Determines whether the connection string is valid or not.
        /// </summary>
        /// <param name="p_sConnectStr">The connection string to be validated.</param>
        /// <returns>A boolean representing whether the connection string is valid or not.</returns>
        private bool IsConnectionStringValid(string p_sConnectStr, ref Exception p_objException)
        {
            bool bIsConnectionStringValid = false;
            DbConnection objConn = DbFactory.GetDbConnection(p_sConnectStr);
            try
            {
                objConn.Open();
                bIsConnectionStringValid = true;
            }
            catch (Exception ex)
            {
                p_objException = ex;
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Dispose();
                }
            }

            return bIsConnectionStringValid;
        }


        //internal void ErrorOnLaunch(int p_iJobId)
        //{
        //    DbConnection objConn = null;
        //    try
        //    {
        //        objConn = DbFactory.GetDbConnection(m_sConnectionString);
        //        objConn.Open();
        //        objConn.ExecuteNonQuery(String.Format("UPDATE TM_JOBS SET JOB_STATE_ID=7, TARGET_HOST='" + System.Environment.MachineName + "', START_DTTM = '" + Conversion.GetDateTime(DateTime.Now.ToString()) + "' WHERE JOB_ID={0}", p_iJobId));

        //    }
        //    catch (Exception p_objEx)
        //    {
        //        this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.ErrorOnLaunch.Error"), BusinessAdaptorErrorType.Error);
        //        logErrors("ErrorOnLaunch", null, false, systemErrors);
        //    }
        //    finally
        //    {
        //        if (objConn != null)
        //        {
        //            objConn.Dispose();
        //        }
        //    }
        //}
        /// <summary>
        /// Generates Diary for jobs completed with errors and in case of any faliure occurs while executing a task  
        /// </summary>
        /// <param name="p_iJobId"></param>
        /// <param name="bArchiveJob"></param>
        private void GenerateDiary(int p_iJobId, bool bArchiveJob, int iClientId = 0)
        {
            string sJobUserLoginName = string.Empty;
            string sJobDataSourceName = string.Empty;
            string sJobName = string.Empty;
            string sTempJobUserLoginName = string.Empty;
            string sTempJobDataSourceName = string.Empty;
            string sTempJobName = string.Empty;
            string sUsrMgrLoginName = string.Empty;
            string sEntryNotes = string.Empty;
            string sSQL = string.Empty;
            DbConnection objConn = null;           
            DbReader objReader = null;
            bool bTaskMgrDiaryFlag = false;
            SysSettings objSettings = null;
            WpaDiaryEntry objWPADiary = null;
            DataModelFactory objDMF = null;
            UserLogin objLogin = null;
            string sConnectionStringSec = string.Empty;//Add & change by kuladeep for Cloud
            string sConnectionString = string.Empty;

            try
            {
                //Add & change by kuladeep for Cloud-Start
                systemErrors = new BusinessAdaptorErrors(iClientId);

                if (iClientId != 0)//Check for Cloud functionality
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionStringSec = StructConnString.SecConnectionString;
                }
                else
                {
                    sConnectionStringSec = m_sConnectionSecurity;
                }
                //Add & change by kuladeep for Cloud-End
                //To assign the hash table values again in case of new object creation via archive jobs
                // tmalhotra3:changes done to make Task manager diary run in case of error in job
               if (iClientId != 0)//Check for Cloud functionality
               { 
                if (dictMultiTenant.ContainsKey(iClientId))
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
               }
               else
               {
                sConnectionString = m_sConnectionString;
               }

                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();
                objReader = objConn.ExecuteReader("SELECT JOB_NAME,TM_USER,TM_DSN FROM TM_JOBS WHERE JOB_ID = " + p_iJobId);
                if (objReader.Read()) 
                {
                        sTempJobUserLoginName = Convert.ToString(objReader.GetValue("TM_USER"));
                        sTempJobDataSourceName =  Convert.ToString(objReader.GetString("TM_DSN"));
                        sTempJobName =  Convert.ToString(objReader.GetString("JOB_NAME"));
                }

                if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(m_hstUserLoginName[Convert.ToString(iClientId) + "_" + Convert.ToString(p_iJobId)])))
                {
                    m_hstUserLoginName.Add(iClientId + "_" + p_iJobId, sTempJobUserLoginName);
                }
                if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(m_hstDSNName[Convert.ToString(iClientId) + "_" + Convert.ToString(p_iJobId)])))
                {
                    m_hstDSNName.Add(iClientId + "_" + p_iJobId, sTempJobDataSourceName);
                }
                if (string.IsNullOrEmpty(Conversion.ConvertObjToStr(m_hstJobName[Convert.ToString(iClientId) + "_" + Convert.ToString(p_iJobId)])))
                {
                    m_hstJobName.Add(iClientId + "_" + p_iJobId, sTempJobName);
                }
                objConn.Close();
                // tmalhotra3:changes done to make Task manager diary run in case of error in job changes end

                ////Add & change by kuladeep for Cloud
                sJobUserLoginName = Conversion.ConvertObjToStr(m_hstUserLoginName[Convert.ToString(iClientId) + "_" + Convert.ToString(p_iJobId)]);
                sJobDataSourceName = Conversion.ConvertObjToStr(m_hstDSNName[Convert.ToString(iClientId) + "_" + Convert.ToString(p_iJobId)]);
                sJobName = Conversion.ConvertObjToStr(m_hstJobName[Convert.ToString(iClientId) + "_" + Convert.ToString(p_iJobId)]);

                if (bArchiveJob)
                {
                    sEntryNotes = "Error occured while executing the task. (Click on the attached record to view details) ";
                }
                else
                {
                    sEntryNotes = "Some hardware failure occured while executing the task. Task has been rescheduled.";
                }

                if (sJobUserLoginName != "" && sJobDataSourceName != "")
                {
                    //Initialize User Login Object
                    SecurityDatabaseManager.CreateSecurityDatabase(sConnectionStringSec);
                    objLogin = new UserLogin(sJobUserLoginName, sJobDataSourceName,iClientId);
                    sUsrMgrLoginName = objLogin.objUser.GetManagerLoginName(objLogin.objUser.ManagerId, objLogin.DatabaseId);

                    objSettings = new SysSettings(objLogin.objRiskmasterDatabase.ConnectionString,iClientId);
                    bTaskMgrDiaryFlag = objSettings.TaskMgrDiary;
                    //Check for the Task Manager Diary Utility Setting Flag
                    //using (objConn = DbFactory.GetDbConnection(objLogin.objRiskmasterDatabase.ConnectionString))
                    //{
                    //    objConn.Open();

                    //    sSQL = "SELECT TASK_MGR_DIARY FROM SYS_PARMS";

                    //    using (objReader = objConn.ExecuteReader(sSQL))
                    //    {
                    //        if (objReader.Read())
                    //        {
                    //            iTaskMgrDiaryFlag = objReader.GetInt32(0);
                    //        }
                    //    }
                    //}
                    //objConn.Close();
                    //objReader.Close();

                    if (bTaskMgrDiaryFlag)
                    {
                        //Create Diary Object

                        objDMF = new DataModelFactory(objLogin,iClientId);
                        objWPADiary = (WpaDiaryEntry)objDMF.GetDataModelObject("WpaDiaryEntry", false);

                        //Send Diary to the User
                        objWPADiary.EntryId = 0;
                        objWPADiary.EntryName = sJobName;
                        objWPADiary.EntryNotes = sEntryNotes;
                        objWPADiary.CreateDate = Conversion.ToDbDateTime(DateTime.Now);
                        objWPADiary.CompleteTime = Conversion.ToDbTime(DateTime.Now);
                        objWPADiary.CompleteDate = Conversion.ToDbDate(DateTime.Now);
                        objWPADiary.AttachRecordid = p_iJobId;
                        objWPADiary.AttachTable = "TM_JOB_LOG";
                        objWPADiary.AssignedUser = sJobUserLoginName;
                        objWPADiary.AssigningUser = sJobUserLoginName;
                        objWPADiary.StatusOpen = true;
                        objWPADiary.Priority = 1;
                        objWPADiary.Save();

                        if (sUsrMgrLoginName != "")
                        {
                            //Send Diary to the Manager
                            objWPADiary.EntryId = 0;
                            objWPADiary.AssignedUser = sUsrMgrLoginName;
                            objWPADiary.Save();
                        }
                        else
                        {
                            // Throw back system error if manager is not defined
                            systemErrors.Add("Riskmaster.Service.TaskManagement.GenerateDiary.JobUserMgrUndefined",
                            Globalization.GetString("Riskmaster.Service.TaskManagement.GenerateDiary.JobUserMgrUndefined", iClientId),
                            BusinessAdaptorErrorType.SystemError);

                            logErrors("GenerateDiary", null, false, systemErrors, iClientId);
                        }
                    }
                }
                else
                {
                    
                    // Throw back system error if User is not defined
                    systemErrors.Add("Riskmaster.Service.TaskManagement.GenerateDiary.JobUserUndefined",
                    Globalization.GetString("Riskmaster.Service.TaskManagement.GenerateDiary.JobUserUndefined", iClientId),
                    BusinessAdaptorErrorType.SystemError);

                    logErrors("GenerateDiary", null, false, systemErrors, iClientId);
                }
            }

            catch (RMAppException p_objEx)
            {
                systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
                logErrors("GenerateDiary", null, false, systemErrors, iClientId);
            }
            catch (Exception p_objEx)
            {
                this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.GenerateDiary.Error", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("GenerateDiary", null, false, systemErrors, iClientId);
            }

            finally
            {
                //if (objReader != null)
                //{
                //    objReader.Dispose();
                //}
                //if (objConn != null)
                //{
                //    objConn.Close();
                //    objConn.Dispose();
                //}
                objSettings = null;
                objDMF = null;
                objWPADiary = null;
            }
        }

        private void LookForRunningJobs(int iClientId = 0)
        {
            int iCount = 0;
            string sSQL = string.Empty;
            string sUserLoginName = string.Empty;
            string sDataSourceName = string.Empty;
            string sJobName = string.Empty;
            int iJobId = 0;

            DbConnection objConn = null;
            DbReader objReader = null;
            //Add & change by kuladeep for Cloud-Start
            string sConnectionString = string.Empty;
            if (iClientId != 0)//Check for Cloud functionality
            {
                if (dictMultiTenant.ContainsKey(iClientId))
                {

                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }
            //Add & change by kuladeep for Cloud-End        
            systemErrors = new BusinessAdaptorErrors(iClientId);

            try
            {
                objConn = DbFactory.GetDbConnection(sConnectionString);
                objConn.Open();

                sSQL = "SELECT JOB_ID,JOB_NAME,TM_USER,TM_DSN FROM TM_JOBS WHERE JOB_STATE_ID = 2 AND START_DTTM IS NOT NULL";

                objReader = objConn.ExecuteReader(sSQL);
                while (objReader.Read())
                {
                    if (!objReader.IsDBNull("TM_USER"))
                    {
                        sUserLoginName = objReader.GetString("TM_USER");
                    }
                    if (!objReader.IsDBNull("TM_DSN"))
                    {
                        sDataSourceName = objReader.GetString("TM_DSN");
                    }

                    iJobId = objReader.GetInt32("JOB_ID");
                    sJobName = objReader.GetString("JOB_NAME");

                    m_hstJobName.Add(iClientId + "_" + iJobId, sJobName);//TODO-
                    m_hstUserLoginName.Add(iClientId + "_" + iJobId, sUserLoginName);
                    m_hstDSNName.Add(iClientId + "_" + iJobId, sDataSourceName);

                    GenerateDiary(iJobId, false, iClientId);
                }

                objConn.Close();
                objConn.Open();

                sSQL = "UPDATE TM_JOBS SET JOB_STATE_ID=1,START_DTTM = NULL WHERE JOB_STATE_ID = 2";
              iCount = Conversion.ConvertObjToInt(objConn.ExecuteNonQuery(sSQL),iClientId);                                         

           }
           catch (RMAppException p_objEx)
           {            
               systemErrors.Add(p_objEx, BusinessAdaptorErrorType.Error);
               logErrors("LookForRunningJobs", null, false, systemErrors, iClientId);
           }
           catch (Exception p_objEx)
           {
               this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.LookForRunningJobs.Error", iClientId), BusinessAdaptorErrorType.Error);
               logErrors("LookForRunningJobs", null, false, systemErrors, iClientId);
           }
           finally
           {             
               if (objReader != null)
               {
                   objReader.Close();
                   objReader.Dispose();
               }
               if (objConn != null)
               {
                   objConn.Close();
                   objConn.Dispose();
               }

            }

        }

        /// <summary>
        /// This method tries to connect to Taskmanager database
        /// </summary>
        private bool CreateConnection(int iClientId = 0)
        {
            Exception objException = null;
            bool bIsConnectionValid = false;
            //Add & change by kuladeep for Cloud-Start
            string sConnectionString = string.Empty;
            if (iClientId != 0)//Check for Cloud functionality
            {
                CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                sConnectionString = StructConnString.TMConnectionString;
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }
            //Add & change by kuladeep for Cloud-End
            systemErrors = new BusinessAdaptorErrors(iClientId);
            try
            {
                do
                {
                    bIsConnectionValid = IsConnectionStringValid(sConnectionString, ref objException);
                    if (!bIsConnectionValid)
                    {
                        this.systemErrors.Clear();
                        this.systemErrors.Add(objException, Globalization.GetString("Riskmaster.Service.TaskManagement.OnStart.Error", iClientId), BusinessAdaptorErrorType.Error);
                        logErrors("OnStart : InsideDoWhile", null, false, systemErrors, iClientId);

                        //sleep and get connectionstring value in case it's corrected, then try to connect again
                        int iSleepTime = 0;
                        while (iSleepTime <= m_iDefaultWaitTime)
                        {
                            if (!m_bContinue)
                                return bIsConnectionValid;

                            Thread.Sleep(10000);
                            iSleepTime += 10000;
                        }

                        GetConnectionString(iClientId);
                        // akaushik5 Added for MITS 38134 Starts
                        sConnectionString = this.m_sConnectionString;
                        // akaushik5 Added for MITS 38134 Ends
                    }
                }
                while (!bIsConnectionValid);                
            }
            catch (Exception p_objEx)
            {
                this.systemErrors.Add(p_objEx, Globalization.GetString("Riskmaster.Service.TaskManagement.CreateConnection.Error", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("CreateConnection", null, false, systemErrors, iClientId);
            }

            return bIsConnectionValid;
        }

        // 4/30/2015 tmalhotra3: Task Manager Email Notification enhancement(RMA 4613)
        private void SendEmailNotification(int sJobId, int p_jobStateId, int iClientId = 0)
        {
            Mailer objMail = null;
            string sFromName = string.Empty;
            string sJobName = string.Empty;
            string sDSNName = string.Empty;
            string sEmailIDFrom = string.Empty;
            string sEmailIdTo = string.Empty;
            string sConnectionStringSec = string.Empty;
            bool bTaskMgrEmailFlag = false;       
            SysSettings objSysSettings =null;
            string sSQL = string.Empty;
            UserLogin objuserLogin =null;
            string sConnectionString = string.Empty;
            string sJobState = string.Empty;
            string sStartDate = string.Empty;

            systemErrors = new BusinessAdaptorErrors(iClientId);
            if (iClientId != 0)//Check for Cloud functionality
            {
                if (dictMultiTenant.ContainsKey(iClientId))
                {
                    CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                    sConnectionString = StructConnString.TMConnectionString;
                }
            }
            else
            {
                sConnectionString = m_sConnectionString;
            }     

            try
            {
                if (objDicTMState == null)
                {
                    objDicTMState = new Dictionary<int, string>();
                    using (DbReader objReader = DbFactory.ExecuteReader(sConnectionString, "SELECT JOB_STATE_ID,JOB_STATE FROM TM_JOB_STATE"))
                    {
                        while (objReader.Read())
                        {
                            objDicTMState.Add(Convert.ToInt16(objReader.GetValue("JOB_STATE_ID")), Convert.ToString(objReader.GetValue("JOB_STATE")));
                        }
                    }
                }
                using (DbReader objReader = DbFactory.ExecuteReader(sConnectionString, "SELECT JOB_NAME,TM_USER,TM_DSN,START_DTTM FROM TM_JOBS WHERE JOB_ID =" + sJobId))
                {
                    if (objReader.Read())
                    {
                        sFromName = Convert.ToString(objReader.GetValue("TM_USER"));
                        sJobName = Convert.ToString(objReader.GetValue("JOB_NAME"));
                        sDSNName = Convert.ToString(objReader.GetValue("TM_DSN"));
                        if (!string.IsNullOrEmpty(Convert.ToString(objReader["START_DTTM"])))
                        {
                            sStartDate = Convert.ToString(Conversion.ToDate(Convert.ToString(objReader["START_DTTM"])));
                        }
                    }
                }
                objuserLogin = new UserLogin(sFromName, sDSNName, iClientId);
                objSysSettings = new SysSettings(objuserLogin.objRiskmasterDatabase.ConnectionString, iClientId);
                bTaskMgrEmailFlag = objSysSettings.TaskMgrEmail;         
                sJobState = objDicTMState[p_jobStateId];

                if (bTaskMgrEmailFlag)
                {
                    if (iClientId != 0)//Check for Cloud functionality
                    {
                        CommonFunctions.MultiClientConnection StructConnString = dictMultiTenant[iClientId];
                        sConnectionStringSec = StructConnString.SecConnectionString;
                    }
                    else
                    {
                        sConnectionStringSec = m_sConnectionSecurity;
                    }
                    sSQL = "SELECT DISTINCT EMAIL_ADDR FROM USER_TABLE, USER_DETAILS_TABLE WHERE "
                        + " USER_TABLE.USER_ID = USER_DETAILS_TABLE.USER_ID AND USER_DETAILS_TABLE.LOGIN_NAME = '"
                        + sFromName + "'";
                    sEmailIdTo = Conversion.ConvertObjToStr(DbFactory.ExecuteScalar(sConnectionStringSec, sSQL));

                    using (DbReader objReader = Riskmaster.Db.DbFactory.ExecuteReader(sConnectionStringSec, "SELECT ADMIN_EMAIL_ADDR FROM SETTINGS"))
                    {
                        if (objReader.Read())
                            sEmailIDFrom = Convert.ToString(objReader.GetValue("ADMIN_EMAIL_ADDR"));
                    }
                    if (sEmailIDFrom == "" || sEmailIdTo == "")
                        throw new InvalidValueException(Globalization.GetString("Riskmaster.Service.TaskManagement.SendEmailNotification.BlankEmailID", iClientId));

                    if (!string.IsNullOrEmpty(sEmailIDFrom))
                    {
                        objMail = new Mailer(iClientId);
                        objMail.IsBodyHtml = true;
                        objMail.To = sEmailIdTo;
                        objMail.From = sEmailIDFrom;
                        objMail.Subject = "Error occurred in " + sJobName;
                        objMail.Body = "Error occurred while running the job. Please find the details below: <br/> Error State: " + sJobState + "<br/> Job Name: " + sJobName
                        + "<br/> Job Id: " + sJobId + "<br/> Job Start Date and Time: " + sStartDate + "<br/> Datasource Name: " + sDSNName + " <br/> Job Scheduled By: " + sFromName;
                        objMail.SendMail();
                    }
                }
            }
            catch (InvalidValueException ive)
            {
                this.systemErrors.Add(ive, Globalization.GetString("Riskmaster.Service.TaskManagement.SendEmailNotification.BlankEmailID", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("Riskmaster.Service.TaskManagement.SendEmailNotification.BlankEmailID", null, false, systemErrors, iClientId);
            }
            catch (Exception exc)
            {
                this.systemErrors.Add(exc, Globalization.GetString("Riskmaster.Service.TaskManagement.SendEmailNotification.Error", iClientId), BusinessAdaptorErrorType.Error);
                logErrors("SendEmailNotification", null, false, systemErrors, iClientId);

            }
            finally
            {
                if (objuserLogin != null)
                {
                    objuserLogin = null;
                }
                if (objSysSettings != null)
                {
                    objSysSettings = null;
                }
                if (objMail != null)
                {
                    objMail = null;
                }
            }
        }
    }
}
