using System;
using System.Collections.Generic;
using System.Text;

namespace Riskmaster.Service.TaskManagement
{
    //************************************************************** 
    //* $File				: Job.cs 
    //* $Revision			: 1.0.0.0 
    //* $Creation Date		: 12-07-2007
    //* $Author			    : Aashish Bhateja
    //***************************************************************	

    /// <summary>
    /// This class represents a Job.
    /// </summary>
    class Job
    {
        #region Variables Declaration

        /// <summary>
        /// Job Id.
        /// </summary>
        private int m_iJobId = 0;
        /// <summary>
        /// Task Type Id.
        /// </summary>
        private int m_iTaskTypeId = 0;
        /// <summary>
        /// Job State Id.
        /// </summary>
        private int m_iJobStateId = 0;
        /// <summary>
        /// Start Date time for the Job.
        /// </summary>
        private string m_sStartDttm = "";
        /// <summary>
        /// End Date time for the Job.
        /// </summary>
        private string m_sEndDttm = "";
        /// <summary>
        /// Target Host where the Job is supposed to run.
        /// </summary>
        private string m_sTargetHost = "";
        /// <summary>
        /// Config Xml containing the settings to run the Job.
        /// </summary>
        private string m_sConfig = "";
        /// <summary>
        /// Any message that the service wants to log for the Job.
        /// </summary>
        private string m_sMessage = "";

        private int m_iClientId = 0;

        #endregion

        #region Properties Declaration

        /// <summary>
        /// Job Id.
        /// </summary>
        public int JobId
        {
            get
            {
                return m_iJobId;
            }
            set
            {
                m_iJobId = value;
            }
        }


        public int ClientId
        {
            get
            {
                return m_iClientId;
            }
            set
            {
                m_iClientId = value;
            }
        }
       
        /// <summary>
        /// Task Type Id.
        /// </summary>
        public int TaskTypeId
        {
            get
            {
                return m_iTaskTypeId;
            }
            set
            {
                m_iTaskTypeId = value;
            }
        }

        /// <summary>
        /// Job State Id.
        /// </summary>
        public int JobStateId
        {
            get
            {
                return m_iJobStateId;
            }
            set
            {
                m_iJobStateId = value;
            }
        }

        /// <summary>
        /// Start Date time for the Job.
        /// </summary>
        public string StartDttm
        {
            get
            {
                return m_sStartDttm;
            }
            set
            {
                m_sStartDttm = value;
            }
        }

        /// <summary>
        /// End Date time for the Job.
        /// </summary>
        public string EndDttm
        {
            get
            {
                return m_sEndDttm;
            }
            set
            {
                m_sEndDttm = value;
            }
        }

        /// <summary>
        /// Target Host where the Job is supposed to run.
        /// </summary>
        public string TargetHost
        {
            get
            {
                return m_sTargetHost;
            }
            set
            {
                m_sTargetHost = value;
            }
        }

        /// <summary>
        /// Config Xml containing the settings to run the Job.
        /// </summary>
        public string Config
        {
            get
            {
                return m_sConfig;
            }
            set
            {
                m_sConfig = value;
            }
        }

        /// <summary>
        /// Any message that the service wants to log for the Job.
        /// </summary>
        public string Message
        {
            get
            {
                return m_sMessage;
            }
            set
            {
                m_sMessage = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ClientJobId
        {
            get
            {
                return String.Format("{0}_{1}", m_iClientId, m_iJobId);
            }
        }

        #endregion


    }
}
