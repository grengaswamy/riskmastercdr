#!/bin/bash
npm install -g yo;
npm install --global generator-angular;
mkdir riskmasterAngular && cd riskmasterAngular;
mv ../bower.json .;
mv ../package.json .;
mv ../Gruntfile.js .;
mkdir app;
mv ../app/* ./app;
npm install;
bower install;
grunt;

