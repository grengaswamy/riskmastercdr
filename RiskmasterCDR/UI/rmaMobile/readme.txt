For Running app :
1. Host "App" folder in IIS and you r ready to go!


For Setting up Development Environment
1. Download & Install node (http://nodejs.org/download/)

2. Download & install git (http://git-scm.com/downloads)

3. Check node & git are installed with these commands.(run them in command prompt)
   "node --version && git --version"

4.run "setup.sh" 
  This will setup whole environment for development.

5.For making Android app 
  run "cordova.sh"
