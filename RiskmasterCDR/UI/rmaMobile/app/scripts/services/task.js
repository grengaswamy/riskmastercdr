'use strict';
app.factory('Task',['$http','$q','Auth','Event','Claim', function($http,$q,Auth,Event,Claim){
    var raw=[];
    var baseUrl= 'RMService/MobileAdjusterService.svc';
    var status='';
    var loading='true';
    var diaries=[];
    var error='false';
    var errorMessage='';
    var d=$q.defer();


    var format=function(raw){

        //Sorting claims & events
        angular.forEach(raw, function(diary){
            var temp={ "task subject":"", "entry notes":"","work activity":"","complete date":"","complete time":"", "attachments":"","linked":"","type":""};
            if(diary["attachprompt"].indexOf("EVENT")>-1){
                temp["task subject"]=diary["tasksubject"];
                temp["entry notes"]=diary["EntryNotes"];
                temp["work activity"]=diary["work_activity"];
                temp["complete date"]=diary["complete_date"];
                temp["complete time"]=diary["CompleteTime"];
                temp["attachments"]=diary["attachprompt"];
                temp["type"]="E";
                var find=temp["attachments"].replace("EVENT ","");
                var found=Event.find(find);
                if(found){
                    temp["linked"]=found["Event Number"];
                }
                diaries.push(temp);
            }
            else if(diary["attachprompt"].indexOf("CLAIM")>-1){
                temp["task subject"]=diary["tasksubject"];
                temp["entry notes"]=diary["EntryNotes"];
                temp["complete date"]=diary["complete_date"];
                temp["complete time"]=diary["CompleteTime"];
                temp["attachments"]=diary["attachprompt"];
                temp["type"]="C";
                var find=temp["attachments"].replace("CLAIM ","");
                var found=Claim.find(find);
                if(found){
                    temp["linked"]=found["Claim Number"];
                }
                diaries.push(temp);

            }
            else {
                temp["task subject"]=diary["tasksubject"];
                temp["entry notes"]=diary["EntryNotes"];
                temp["complete date"]=diary["complete_date"];
                temp["complete time"]=diary["CompleteTime"];
                temp["attachments"]=diary["attachprompt"];
                diaries.push(temp);
            }

        });
        console.log("formatted tasks");
        console.log(diaries);
    }
    var Task ={
        requestTasks: function(){
            return  $http({
                method : 'GET',
                url: Auth.getServer()+ baseUrl+'/TasksList',
                headers : {'session': Auth.getSession(), 'user': Auth.getUsername()}
//                cache : true
              });
        },
        loadTasks : function(){
            console.log("loading tasks");
            diaries=[];
            Task.requestTasks()
                 .success(function(data){
                    if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                        console.log("error fetching tasks list");
                        console.log(data);
                        status=data.ResultMessage.MsgStatus.MsgStatusCd;
                        error='true';
                        errorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                        loading='false';
                    }
                    else{
                        error='false';
                        console.log("tasks success");
                        status=data.ResultMessage.MsgStatus.MsgStatusCd;
                        angular.forEach(data.ResultMessage.Document.diaries.diary, function(diary){
                            raw.push(diary);
                        });
                        console.log("tasks");
                        console.log(raw);
                        format(raw);
                        d.resolve(diaries.length);
                        loading='false';
                    }

                 })
                 .error(function(error){
                     console.log("error fetching tasksList");
                     console.log(error);
                 });
        },
        getTasks:function(){
            return diaries;
        },
        find: function(id){
            console.log("inside find: "+id);
            var temp=[];
            for(var i=0;i<diaries.length;i++){
                if(diaries[i]["linked"]==id){
                    temp.push(diaries[i]);
                }
            }
            return temp;
        },
        getLoading : function(){
            return loading;
        },
        getStatus: function (){
            return status;
        },
        getError : function(){
            return error;
        },
        getErrorMessage : function(){
            return errorMessage;
        },
        getTotalTasks : function (){
            return d.promise;
        },
        addTask:function(t){
            diaries.push(t);
        }
    };
    return Task;
}]);
