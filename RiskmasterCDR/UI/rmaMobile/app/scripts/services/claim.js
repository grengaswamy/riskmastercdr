﻿'use strict';
app.factory('Claim',['$http','$q','Auth',
    function($http,$q,Auth){
    var claims=[];
    var raw='';
    var baseUrl= 'RMService/MobileAdjusterService.svc';
    var claimAttributes=[];
    var columns=[];
    var loading='true';
    var status='';
    var error='false';
    var errorMessage='';
    var d=$q.defer();

        var Claim ={
        requestClaims : function(){
            return  $http({
                method : 'GET',
                url: Auth.getServer()+ baseUrl+'/ClaimsList',
                headers : {'session': Auth.getSession()}

            });
        },
        requestClaimDetails: function(id){
             return $http({
                method : 'GET',
                url : Auth.getServer()+ baseUrl+'/Claims/'+id,
                headers : { 'session': Auth.getSession()}
            });

        },
        requestClaimPaymentDetails: function(c){
             return $http({
                method : 'GET',
                url : Auth.getServer()+ baseUrl+'/PaymentDetails/'+c["Claim Number"],
                headers : { 'session': Auth.getSession(), 'pid' : c["pid"]}
            });

        },
        requestPersonsInvolved: function(id){
                return $http({
                    method : 'GET',
                    url : Auth.getServer()+ baseUrl+'/PersonsInvolvedList/'+id,
                    headers : { 'session': Auth.getSession()}
                });

        },
        loadClaims : function(){
            console.log("Loading claims");
            claims=[];
            Claim.requestClaims()
                    .success(function(data){
                        if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                            console.log("error fetching claims list");
                            console.log(data);
                            status=data.ResultMessage.MsgStatus.MsgStatusCd;
                            error='true';
                            errorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                            loading='false';
                        }
                        else{
                            error='false';
                            console.log("Claims success");
                            console.log(data);
                            raw=data;
                            status=data.ResultMessage.MsgStatus.MsgStatusCd;
                            //getting column heads
                            angular.forEach(data.ResultMessage.Document.results.columns.column, function(column){
                                claimAttributes.push(column.text);
                                columns.push(column);
                            });

                            //formatting data
                            angular.forEach(data.ResultMessage.Document.results.data.row, function(row){
                                var temp={};
                                angular.forEach(row.field, function(v, j){
                                    temp[claimAttributes[j]]=v;

                                });
                                temp['pid']=row.pid;
                                claims.push(temp);
                            });
                            loading="false";
                            d.resolve(claims.length);
                            console.log("claims formatted");
                            console.log(claims);
                        }
                   })
                   .error(function(data,config,status,headers){
                       console.log("error reaching server");
                       console.log(data);
                       console.log(config);
                       console.log(status);
                       error='true';
                       status='error';
                       errorMessage="server not reachable";
                    });

        },

        setClaims: function(data){
            claims=data;
        },
        getClaims:function(){
            return claims;
        },
        find: function(id){
            for(var i=0;i<claims.length;i++){
                var claim=claims[i];
                if(claim["Claim Number"]==id){
                    return claim;
                    }
            }
            return null;
        },
        findEvent: function(id){
            var result=[];
            console.log("looking for claim with event id :"+id);
            for(var i=0;i<claims.length;i++){
                var claim=claims[i];
                if(claim["Event Number"]==id){
                    result.push(claim);
                }
            }
            console.log("attached claims");
            console.log(result);
            return result;
        },
        getLoading : function(){
            return loading;
        },
        getStatus: function(){
            return status;
        },
        getError : function(){
            return error;
        },
        getErrorMessage : function(){
            return errorMessage;
        },
        getPid : function(id){
            for(var i=0;i<claims.length;i++){
                var claim=claims[i];
                if(claim["Claim Number"]==id){
                      return claim.pid;
                    }
            }
            return null;
            
        },
        getTotalClaims : function(){
            return d.promise;
        }
    };
    return Claim;
}]);
