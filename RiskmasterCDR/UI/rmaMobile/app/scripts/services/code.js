'use strict';
app.factory('Code',['$http','Auth',function($http,Auth){

    var codes=[];
    var claimStatus={};
    var unitTypeCode={};
    var states={};
    var noteTypeCode=[];
    var wpaActivities=[];
    var personsInvolvedType=[];
    var entity=[];
    var claimType=[];
    var otherPeople=[];

    var baseUrl= 'RMService/MobileAdjusterService.svc';
    var Code = {
        requestCodes : function(){
            $http({
                method : 'GET',
                url: Auth.getServer()+ baseUrl+'/Codes',
                headers : {'session': Auth.getSession()},
                cache : true
            })
            .success(function(data){
                codes=data;
                angular.forEach(data.CodeTables.CodeTable,function(code){
                    switch(code.name)
                    {
                        case 'CLAIM_STATUS' : claimStatus= code; break;
                        case 'UNIT_TYPE_CODE' : unitTypeCode=code; break;
                        case 'STATES' : states=code; break;
                        case 'NOTE_TYPE_CODE' : noteTypeCode=code; break;
                        case 'WPA_ACTIVITIES' : wpaActivities=code; break;
                        case 'PERSON_INV_TYPE' : personsInvolvedType=code ; break; 
                        case 'ENTITY' : entity=code ; break;
                        case 'CLAIM_TYPE' : claimType=code; break;
                        case 'OTHER_PEOPLE' : otherPeople=code; break;
                    }
                });
                console.log("code objects");
                console.log(claimStatus);
                console.log(unitTypeCode);
                console.log(states);
                console.log(noteTypeCode);
                console.log(wpaActivities);
                console.log(personsInvolvedType);
                console.log(entity);
                console.log(claimType);
                console.log(otherPeople);
            })
            .error(function(error){
                console.log("error requesting codes");
                console.log(error);
            });
        },
        
        getClaimStatus:function() { return claimStatus;},
        getUnitTypeCode : function() { return unitTypeCode;},
        getStates : function() { return states;},
        getNoteTypeCode : function () { return noteTypeCode;},
        getWpaActivities : function() { return wpaActivities;},
        getPersonsInvolvedType : function () { return personsInvolvedType;},
        getEntity : function () { return entity;},
        getClaimType : function () { return claimType;},
        getOtherPeople: function () { return otherPeople;}
    }
    return Code;
}]);