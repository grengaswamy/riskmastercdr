﻿'use strict';
app.factory('Note',['$http','Auth',function($http,Auth){

    var notes=[];
    var note={};
    var baseUrl= 'RMService/MobileAdjusterService.svc';
    
    var Note = {
        requestNotes : function(){
            $http({
                method : 'GET',
                url: Auth.getServer()+ baseUrl+'/Notes',
                headers : {'session': Auth.getSession()}

            })
            .success(function(data){
                
            })
            .error(function(error){
                console.log("error requesting codes");
                console.log(error);
            });
        },
        postNote : function(note){
           return $http({
                method : 'POST',
                url : Auth.getServer()+baseUrl +'/Notes',
                data : note,
                headers : {'session' : Auth.getSession(), 'Content-Type' : 'json'}
             });
            
        }
        
    }
    return Note;
}]);