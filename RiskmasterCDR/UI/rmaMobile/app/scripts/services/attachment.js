'use strict';
app.factory('Attachment',['$http','Auth', function($http,Auth){
    var baseUrl= 'RMService/MobileAdjusterService.svc';
    var Attachment = {
        requestClaimAttachment: function (attachment) {
            return  $http({
                method: 'POST',
                data : attachment,
                url: Auth.getServer() + baseUrl + '/ClaimAttachment',
                headers: {'session': Auth.getSession(),'Content-Type': 'json'}

            });
        },
        requestEventAttachment: function (attachment) {
            return  $http({
                method: 'POST',
                data : attachment,
                url: Auth.getServer() + baseUrl + '/EventAttachment',
                headers: {'session': Auth.getSession(), 'Content-Type': 'json'}

            });
        }
    };
    return Attachment;
}]);

