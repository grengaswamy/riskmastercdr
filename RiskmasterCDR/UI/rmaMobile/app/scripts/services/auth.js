'use strict';
app.factory('Auth',['$rootScope','$http','$location',
    function($rootScope,$http,$location){
        var isAuthenticated='false';
        var sessionId='';
        var server='http://'+$location.host()+'/';

        var baseUrl='RMService/MobileAuthenticationService.svc';
        var user = { username: '', password: '', strMessage: '', dsn : ''};
        var userDSNs='';

        var Auth ={
            setServer: function(userServer){
                server= userServer || server;
            },
            getServer:function(){
                return server;
            },
            signedIn : function (){
                return isAuthenticated;
            },
            login : function(user){
                return   $http({
                    method: 'POST',
                    url : server+ baseUrl+'/AuthenticateMobile',
                    data : user,
                    headers: {'Content-Type': 'raw' }
                });
            },
            logout : function(session){
                return $http({
                    method : 'POST',
                    url : server+baseUrl+'/LogOut',
                    data: session,
                    headers: {'Content-Type': 'raw'}
                });
            },
            requestDSNs : function (user){
                return $http({
                    method : 'POST',
                    url : server+baseUrl+'/GetUserDSNsMobile',
                    data : user,
                    headers : {'Content-Type':'json'}
                });
            },
            setDSNs : function(dsnList){
                userDSNs=dsnList;
            },
            getUserDSNs : function(){
                return userDSNs;
            },
            getUserSessionID : function(user){
                return $http({
                    method : 'POST',
                    url : server+baseUrl+'/GetUserSessionIDMobile',
                    data : user,
                    headers : { 'Content-Type':'json'}
                });
            },
            getSession:function(){
                return sessionId;
            },
            setIsAuthenticated: function(value){
                isAuthenticated=value;
            },
            setSessionID : function (value) {
                sessionId=value;
            },
            sessionReady: function(){
                if(sessionId.length>0) return true;
                else return false;
            },
            setUser : function(u){
                user.username= u.username;
            },
            getUser : function(){
                return user;
            },
            getUsername: function(){
                return user.username;
            }
        };

        $rootScope.signedIn = function (){
            return Auth.signedIn();
        }
        $rootScope.sessionReady = function(){
            return Auth.sessionReady();
        }

        return Auth;
    }]);

