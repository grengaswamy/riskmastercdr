/**
 * Created by nsharma202 on 9/1/2014.
 */
'use strict';
app.factory('Auth',['$rootScope','$http','localStorageService',
    function($rootScope,$http,localStorageService){
        localStorageService.clearAll();
        localStorageService.set('isAuthenticated',false);
        localStorageService.set('sessionId','');
        localStorageService.set('server','http://localhost/');
        localStorageService.set('baseUrl','RMService/MobileAuthenticationService.svc');
        localStorageService.set('userDSNs','');
        localStorageService.set('user',JSON.stringify({"username":"", "password":"", "strMessage":"", "dsn" :""}));

//        var isAuthenticated=localStorageService.get('isAuthenticated');
//        var sessionId=localStorageService.get('sessionId');
//        var server=localStorageService.get('server');
//        var baseUrl=localStorageService.get('baseUrl');
//        var user = JSON.parse(localStorageService.get('user'));
//        var userDSNs=localStorageService.get('userDSNs');


        var Auth ={
            setServer: function(userServer){
                localStorageService.set('server', userServer || localStorageService.get('server'));
            },
            getServer:function(){
                return localStorageService.get('server');
            },
            signedIn : function (){
                return localStorageService.get('isAuthenticated');
            },
            login : function(user){
                return   $http({
                    method: 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/AuthenticateMobile',
                    data : user,
                    headers: {'Content-Type': 'raw' }
                });
            },
            logout : function(session){
                return $http({
                    method : 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/LogOut',
                    data: session,
                    headers: {'Content-Type': 'raw'}
                });
            },
            requestDSNs : function (user){
                return $http({
                    method : 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/GetUserDSNsMobile',
                    data : user,
                    headers : {'Content-Type':'json'}
                });
            },
            setDSNs : function(dsnList){
                localStorageService.set('userDSNs',JSON.stringify(dsnList));
            },
            getUserDSNs : function(){
                return JSON.parse(localStorageService.get('userDSNs'));
            },
            getUserSessionID : function(user){
                return $http({
                    method : 'POST',
                    url : localStorageService.get('server')+ localStorageService.get('baseUrl')+'/GetUserSessionIDMobile',
                    data : user,
                    headers : { 'Content-Type':'json'}
                });
            },
            getSession:function(){
                return localStorageService.get('sessionId');
            },
            setIsAuthenticated: function(value){
                localStorageService.set('isAuthenticated',value);
            },
            setSessionID : function (value) {
                localStorageService.set('sessionId',value);
            },
            sessionReady: function(){
                if(localStorageService.get('sessionId').length>0) return true;
                else return false;
            },
            setUser : function(u){
                localStorageService.set('user',JSON.stringify(u));
            },
            getUser : function(){
                var user=localStorageService.get('user');
                return user;
            },
            getUsername: function(){
                return localStorageService.get('user');
            }
        };

        $rootScope.signedIn = function (){
            return Auth.signedIn();
        }
        $rootScope.sessionReady = function(){
            return Auth.sessionReady();
        }

        return Auth;
    }]);


