﻿'use strict';
app.factory('Diary',['$http','Auth',
    function($http,Auth){

    var diaries=[];
    var diary={};
    var baseUrl= 'RMService/MobileAdjusterService.svc';
    
    var Diary = {
        requestDiary : function(){
            $http({
                method : 'GET',
                url: Auth.getServer()+ baseUrl+'/Diary',
                headers : {'session': Auth.getSession()}

            })
            .success(function(data){
                
            })
            .error(function(error){
                console.log("error requesting codes");
                console.log(error);
            });
        },
        postDiary : function(diary){
           return $http({
                method : 'POST',
                url : Auth.getServer()+baseUrl +'/CreateTask',
                data : diary,
                headers : {'session' : Auth.getSession(), 'user': Auth.getUsername() ,'Content-Type' : 'json'}
             });
            
        }
        
    }
    return Diary;
}]);