'use strict';
app.factory('Event',['$rootScope','$q','$http','Auth',function($rootScope,$q,$http,Auth){
    var events=[];
    var raw='';
    var baseUrl= 'RMService/MobileAdjusterService.svc';
    var eventAttributes=[];
    var columns=[];
    var loading='true';
    var status='';
    var error='false';
    var errorMessage='';
    var d=$q.defer();
    var Event ={
        requestEvents : function(){
            return  $http({
                method : 'GET',
                url: Auth.getServer()+ baseUrl+'/EventsList',
                headers : {'session': Auth.getSession()}

            });
        },
        loadEvents : function(){
            console.log("Loading events");
            events=[];
            Event.requestEvents()
                    .success(function(data){

                        if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                            console.log("error fetching events");
                            console.log(data);
                            status=data.ResultMessage.MsgStatus.MsgStatusCd;
                            error='true';
                            errorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                            loading='false';
                        }
                        else{

                            error='false';
                            console.log("event success");
                            raw=data;
                            status=data.ResultMessage.MsgStatus.MsgStatusCd;
                            //getting column heads
                            angular.forEach(data.ResultMessage.Document.results.columns.column, function(column){
                                eventAttributes.push(column.text);
                                columns.push(column);
                            });

                            //formatting data
                            angular.forEach(data.ResultMessage.Document.results.data.row, function(row){
                                var temp={};
                                angular.forEach(row.field, function(v, j){
                                    temp[eventAttributes[j]]=v;
                                });
                                events.push(temp);
                            });
                            loading='false';
                            d.resolve(events.length);
                            console.log("FORMATTED Events");
                            console.log(events);
                        }
                   })
                   .error(function(error){
                       console.log("error !: Service Not reachable");
                       console.log(error);
                    error=true;
                    errorMessage="server not reachable";
                    loading=false;
                   });

        },
        setEvents: function(data){
            events=data;
        },
        getEvents:function(){
            return events;
        },
        find: function(id){
            for(var i=0;i<events.length;i++){
                var event=events[i];
                if(event["Event Number"]==id){
                    return event;
                    }
            }
            return null;
        },
        getLoading : function(){
            return loading;
        },
        getStatus: function(){
            return status;
        },
        getError : function(){
            return error;
        },
        getErrorMessage : function(){
            return errorMessage;
        },
        getTotalEvents : function(){
            return d.promise;
        }
    };
    return Event;
}]);
