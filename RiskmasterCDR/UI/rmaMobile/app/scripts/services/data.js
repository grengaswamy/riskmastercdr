﻿app.factory('Data',['Event','Task','Claim','Code',function(Event,Task,Claim,Code){

    var welcomeDiv='true';
    var loginDiv='false';
    var homeDiv='false';
    var progressBarStatus=0;

    var Data= {
        setData: function(){},

        load : function(){
            progressBarStatus=0;

            Event.loadEvents();
            Claim.loadClaims();
            Task.loadTasks();
            Code.requestCodes();
        },
        setWelcomeStatus: function(w){
            welcomeDiv=w;
        },
        getWelcomeStatus: function(){
            return welcomeDiv;
        },
        setLoginStatus: function(l){
            loginDiv=l;
        },
        getLoginStatus:function(){
            return loginDiv;
        },
        setHomeStatus:function(h){
            homeDiv=h;
        },
        getHomeStatus: function(){
            return homeDiv;
        },
        setProgressBar:function(){
            progressBarStatus=0;
        },
        setProgressBarStatus: function(v){
            progressBarStatus+=v;
        },
        getProgressBarStatus: function(){
            return progressBarStatus;
        }

    }
    return Data;
    }]);