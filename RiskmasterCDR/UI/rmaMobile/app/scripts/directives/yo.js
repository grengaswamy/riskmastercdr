app.directive('yoo',[function() {
    return {
        restrict: 'EA',
        require: '^webcam',
        scope: true,
        template: '<a class="btn btn-primary" ng-click="takeSnapshot()">Take snapshot</a>',
        link: function(scope, ele, attrs, cameraCtrl) {
            scope.takeSnapshot = function() {
                cameraCtrl.takeSnapshot()
                    .then(function(image) {
                        // data image here
                        console.log("image");
                        console.log(image);
                        scope.attachPic(image);
                    });
            }
        }
    }
}])