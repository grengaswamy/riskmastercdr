app.directive('mappy', [function() {
    return {
        restrict: 'EA',
        scope: true,
        template: '<div id="map-canvas" style="height: 480px;"/>',
        link: function(scope, ele, attrs) {
            var map;
            var geocoder;
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var mapOptions = {
                zoom: 18,
                center: latlng
            }
            geocoder = new google.maps.Geocoder();
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            geocoder.geocode( { 'address': scope.address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }
    }
}])
