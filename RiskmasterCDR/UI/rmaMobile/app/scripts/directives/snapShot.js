app.directive('cameraControlSnapshot', [function() {
    return {
        restrict: 'EA',
        require: '^camera',
        scope: true,
        template: '<a class="btn btn-primary text-center" ng-click="takeSnapshot()">Take snapshot</a>',
        link: function(scope, ele, attrs, cameraCtrl) {
            scope.takeSnapshot = function() {
                cameraCtrl.takeSnapshot()
                    .then(function(image) {
                        scope.attachPic(image);
                    });
            }
        }
    }
}])