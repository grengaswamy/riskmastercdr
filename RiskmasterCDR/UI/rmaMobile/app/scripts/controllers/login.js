'use strict';
app.controller('LoginCtrl',['$scope','$location','Auth','$rootScope','Data',
    function($scope,$location,Auth,$rootScope,Data){

    $scope.user = { username: '', password: '', strMessage: '', dsn : ''};
    $scope.userDSNs=Auth.getUserDSNs();
    $scope.DSNs=[];
    $scope.selectedDSN='';
    $scope.loginMsg='';
    $scope.server='';
    $scope.error='false';
    $scope.dsnError='false';
    $scope.message='';
    $scope.loginLoading='false';
    $scope.dsnLoading='false';

    $scope.login= function(){
        $scope.loginLoading='true';
        Auth.setServer($scope.server);
        console.log("server : "+Auth.getServer());
        Auth.login($scope.user)
            .success(function(login){
                $rootScope.wow='true';
                console.log("Auth:" +login);
                Auth.setIsAuthenticated(login);
                Auth.setUser($scope.user);
                console.log("Auth Service up ? : "+$rootScope.signedIn());
                if(login=='true'){
                    Auth.requestDSNs($scope.user)
                        .success(function(data){
                            $scope.error='false';
                            $scope.loginLoading='false';
                            console.log("getting DSNs");
                            console.log(data);
                            $scope.userDSNs=data;
                            Auth.setDSNs(data);
                        })
                        .error(function(error){
                            $scope.error='true';
                            $scope.message='Error getting DSN';
                            console.log("DSN error");
                            console.log(error);
                        });
                }
                else
                {
                    $scope.error='true';
                    $scope.message='Incorrect ID or Password. Please try again';
                    $scope.loginLoading='false';
                    console.log($scope.message);
                }
            })
            .error(function(error){
                $scope.error='true';
                $scope.loginLoading='false';
                $scope.message='Remote Server Not Reachable';
                Auth.setServer('');
                console.log("auth error");
                console.log(error);
            });
    }

    $scope.goo=function(dsn){
        $scope.dsnLoading='true';
        $scope.selectedDSN=dsn;

        console.log("setting up home");
        console.log("selected DSN  : "+$scope.selectedDSN.Key);
        $scope.user=Auth.getUser();
        $scope.user.dsn=$scope.selectedDSN.Key;
        Auth.getUserSessionID($scope.user)
            .success(function(data){
                Auth.setSessionID(data);
                console.log("sessionDone going to /home");
                console.log(Auth.getSession());
                $scope.dsnLoading='false';
                Data.load();
                $location.path('/home');
            })
            .error(function(error){
                $scope.dsnError='true';
                $scope.message="error getting session";
                console.log("error getting session");
                console.log(error);
                $location.path('/login');
            });
    }



}])
