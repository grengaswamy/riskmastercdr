﻿'use strict';
app.controller('TasksCtrl',['$scope','Auth','Task',
    function($scope,Auth,Task) {
        $scope.status = Task.getStatus();
        $scope.loading = Task.getLoading();
        $scope.errorMessage=Task.getErrorMessage();
        $scope.error=Task.getError();
        $scope.formattedTasks=Task.getTasks();

        Task.getTotalTasks().then(function(t){
            $scope.totalTasks=t;
            $scope.loading=Task.getLoading();
        })
    }
]);
