'use strict';
app.controller('NavCtrl',['$scope','Auth','$location','$rootScope','Data',
function($scope,Auth,$location,$rootScope,Data){

    $scope.reload= function(){
       Data.load();
    }

    $scope.logout= function(){
        Auth.logout(Auth.getSession())
            .success(function(){
                Auth.setIsAuthenticated("false");
                Auth.setSessionID('');
                console.log("bye bye");

                $rootScope.wow='false';
                $location.path('/');
            })
            .error(function(){
                console.log("aaj tu logout krke dikha :D");
            })
    }
}]);
