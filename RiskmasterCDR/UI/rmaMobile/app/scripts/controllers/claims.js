﻿'use strict';
app.controller('ClaimsCtrl',['$scope','Auth','Claim',
    function($scope,Auth,Claim) {

        $scope.formattedClaims=Claim.getClaims();
        $scope.status=Claim.getStatus();
        $scope.loading = Claim.getLoading();
        $scope.errorMessage=Claim.getErrorMessage();
        $scope.error=Claim.getError();

        Claim.getTotalClaims().then(function(c){
            $scope.totalClaims=c;
            $scope.loading=Claim.getLoading();
        });


    }
]);

