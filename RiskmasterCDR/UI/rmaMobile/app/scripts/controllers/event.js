﻿/**
 * Created by nsharma202 on 8/28/2014.
 */
'use strict';
app.controller('EventCtrl',['$scope','$routeParams','$q','$timeout','Auth','Claim','Event','Task','Code','Diary','Attachment',
    function($scope,$routeParams,$q,$timeout,Auth,Claim,Event,Task,Code,Diary,Attachment){

        $scope.event=Event.find($routeParams.eventId);
        $scope.eventNo=$routeParams.eventId;
        console.log("event is");
        console.log($scope.event);
        $scope.claims=Claim.findEvent($scope.eventNo);
        $scope.tasks='';
        $scope.tasks=Task.find($scope.eventNo);
        //TASK
        $scope.taskError='false';
        $scope.taskErrorMessage='';
        $scope.taskAdded='false';
        $scope.taskEntered='';
        $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
        $scope.taskTypes=Code.getWpaActivities();
        $scope.submitTask=function(){
            $scope.task.recordId=$scope.eventNo;
            $scope.task.table='EVENT';
            $scope.task.type=$scope.taskEntered.codeid;
            $scope.task.activity=$scope.taskEntered.codeid+" | "+$scope.taskEntered.shortcode+" "+$scope.taskEntered.desc;
            console.log($scope.task);
            Diary.postDiary($scope.task)
                .success(function(data,status, headers, config){
                    console.log(data.ResultMessage.MsgStatus.MsgStatusCd);
                    if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                        $scope.taskError='true';
                        $scope.taskAdded='false';
                        $scope.taskErrorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                        console.log("error posting task");
                        console.log(data);
                        console.log(status);
                    }
                    else {
                        console.log("task added");
                        $scope.taskAdded='true';
                        $scope.taskError=false;
                        console.log(data);
                        var temp={ "task subject":"", "entry notes":"","work activity":"","complete date":"","complete time":"", "attachments":"","linked":"","type":""};
                        temp["task subject"]=$scope.task.name;
                        temp["entry notes"]=$scope.task.description;
                        temp["complete date"]=$scope.task.dueDate;
                        temp["complete time"]=$scope.task.dueTime;
                        temp["work activity"]=$scope.task.activity;
                        temp["type"]="E";
                        temp["attachments"]="Event "+$scope.eventNo;
                        temp["linked"]=$scope.eventNo;
                        //$scope.$apply($scope.tasks.push.apply(temp));
                        //console.log(temp);
//                        console.log("task");
//                        console.log($scope.task);
                        $scope.tasks.push(temp);
                        console.log("attached tasks");
                        console.log($scope.tasks);
                        Task.addTask(temp);
                    }
                })
                .error(function(data,status, headers,config){
                    console.log("error creating task");
                    $scope.message="error creating task";
                    $scope.taskError=true;
                    $scope.taskAdded=false;
                    $scope.taskErrorMessage="error creating task";
                    console.log(data);
                    console.log(status);
                })

        }

        //task ends


//        //Setting geoCodes for googleMaps
//        $scope.geoCodeMessage='';
//        var tempAddress=$scope.event["Loc.: Address 1"]+" "+$scope.event["Loc.: Address 2"]+" "+$scope.event["Loc.: City"]+" "+$scope.event["Loc.: State"]+" "+$scope.event["Loc.: Country"];
//        console.log(tempAddress);
//        var address= tempAddress;
//        var map;
//        var geocoder;
//        var latlng = new google.maps.LatLng(-34.397, 150.644);
//        var mapOptions = {
//            zoom: 18,
//            center: latlng
//        };
//        geocoder = new google.maps.Geocoder();
//        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
//        geocoder.geocode( { 'address': address}, function(results, status) {
//            if (status == google.maps.GeocoderStatus.OK) {
//                map.setCenter(results[0].geometry.location);
//                var marker = new google.maps.Marker({
//                    map: map,
//                    position: results[0].geometry.location
//                });
//            } else {
//                console.log("Geocode was not successful for the following reason: " + status);
//                $scope.geoCodeMessage="Sorry address could not be mapped :(";
//            }
//        });
//        //geocode ends

        //camera starts
//        $scope.cam='true';
//        $scope.picture='false';
//        $scope.savedPic='';
//        $scope.attachPic = function(pic){
//            console.log("back in main controller");
//            console.log(pic);
//            $scope.savedPic=pic;
//            $scope.cam='false';
//            $scope.picture='true';
//
//        }
//        $scope.reTake= function(){
//            console.log('retake');
//            $scope.cam='true';
//            $scope.picture='false';
//
//        }
        //camera ends

//        //Attachments
//        $scope.attachment={ fileName:'', title:'', subject:'',notes:'',content:'', number:''};
//        $scope.attach=function(){
//            $scope.attachment.content=$scope.savedPic.replace("data:image/png;base64,","");
//            $scope.attachment.fileName='abc.jpeg';
//            $scope.attachment.title='tempie title';
//            $scope.attachment.number=$routeParams.eventId;
//            Attachment.requestEventAttachment($scope.attachment)
//                .success(function(data){
//                    console.log(data);
//                    if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
//                        $scope.message=data.ResultMessage.MsgStatus.ExtendedStatus;
//                    }
//                    else{
//                        console.log("uploaded");
//                        console.log(data);
//                    }
//
//                })
//                .error(function(data, status, headers, config) {
//                    console.log("data");
//                    console.log(data);
//                    console.log("status");
//                    console.log(status);
//                    console.log("headers");
//                    console.log(headers);
//                    console.log("config");
//                    console.log(config);
//
//                })
//
//        }




    }]);