﻿'use strict';
app.controller('EventsCtrl',['$scope','Auth','Event',
    function($scope,Auth,Event){

        $scope.formattedEvents=Event.getEvents();
        $scope.loading=Event.getLoading();
        $scope.status=Event.getStatus();
        $scope.errorMessage=Event.getErrorMessage();
        $scope.error=Event.getError();

        Event.getTotalEvents().then(function(l){
            $scope.totalEvents=l;
            $scope.loading=Event.getLoading();
        } );
    }
]);