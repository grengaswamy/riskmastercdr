'use strict';
app.controller('SettingsCtrl',['$scope','Auth',function($scope,Auth){

    $scope.w='true';
    $scope.changeW='false';
    $scope.deletetW='false';
    $scope.email='';
    $scope.password='';
    $scope.newPassword='';
    $scope.deleteAccount=function(){
      Auth.deleteAccount($scope.email,$scope.password).then(function(){
          Auth.logOut();

      }, function(err){
          console.log(err.toString());
          $scope.error=err.toString();
      });

    };
    $scope.change = function(){
        $scope.changeW=!($scope.changeW);
    }
    $scope.delete = function(){
        $scope.deleteW=!($scope.deleteW);
    }
    $scope.changePassword  = function(){
     Auth.changePassword($scope.email,$scope.password,$scope.newPassword).then(function(){
         Auth.logOut();

     }, function(err){
         console.log(err.toString());
         $scope.error=err.toString();
     });
    }
}]);