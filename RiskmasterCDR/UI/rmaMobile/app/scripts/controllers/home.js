/**
 * Created by nsharma202 on 8/12/2014.
 */
'use strict';
app.controller('HomeCtrl',['$scope','$q','$location','$rootScope','Data','Auth','Event','Claim','Task','Code',
    function($scope,$q,$location,$rootScope,Data,Auth,Event,Claim,Task){
    $scope.welcomeDiv=Data.getWelcomeStatus();
    $scope.loginDiv=Data.getLoginStatus();
    $scope.homeDiv=Data.getHomeStatus();

    $scope.getStarted=function(){
        $scope.welcomeDiv=false;
        $scope.loginDiv=true;
        Data.setWelcomeStatus(false);
        Data.setLoginStatus(true);
    }


//   Login
        $scope.user = { username: '', password: '', strMessage: '', dsn : ''};
        $scope.userDSNs=Auth.getUserDSNs(); //iska kch krna hai
        $scope.DSNs=[];
        $scope.selectedDSN='';
        $scope.loginMsg='';
        $scope.server='';
        $scope.error='false';
        $scope.dsnError='false';
        $scope.message='';
        $scope.loginLoading='false';
        $scope.dsnLoading='false';

        $scope.login= function(){
            $scope.loginLoading='true';
            Auth.setServer($scope.server);
            console.log("server : "+Auth.getServer());
            Auth.login($scope.user)
                .success(function(login){
                    console.log("Auth:" +login);
                    Auth.setIsAuthenticated(login);
                    Auth.setUser($scope.user);
                    console.log("Auth Service up ? : "+$rootScope.signedIn());
                    if(login=='true'){
                        Auth.requestDSNs($scope.user)
                            .success(function(data){
                                $scope.error='false';
                                $scope.loginLoading='false';
                                console.log("getting DSNs");
                                console.log(data);
                                $scope.userDSNs=data;
                                Auth.setDSNs(data);
                            })
                            .error(function(error){
                                $scope.error='true';
                                $scope.message='Error getting DSN';
                                console.log("DSN error");
                                console.log(error);
                            });
                    }
                    else
                    {
                        $scope.error='true';
                        $scope.message='Incorrect ID or Password. Please try again';
                        $scope.loginLoading='false';
                        console.log($scope.message);
                    }
                })
                .error(function(error){
                    $scope.error='true';
                    $scope.loginLoading='false';
                    $scope.message='Remote Server Not Reachable';
                    Auth.setServer('');
                    console.log("auth error");
                    console.log(error);
                });
        }

        $scope.goo=function(dsn){
            $scope.dsnLoading='true';
            $scope.selectedDSN=dsn;

            console.log("setting up home");
            console.log("selected DSN  : "+$scope.selectedDSN.Key);
            $scope.user=Auth.getUser();
            console.log($scope.user);
            $scope.user.dsn=$scope.selectedDSN.Key;
            Auth.getUserSessionID($scope.user)
                .success(function(data){
                    Auth.setSessionID(data);
                    console.log("sessionDone going to /home");
                    console.log(Auth.getSession());
                    $scope.dsnLoading='false';
                    $scope.progressBarStatus=0; //Need to remove this with some new logic ***************************************************
                    Data.load();
                    $scope.home();
                    $scope.loginDiv=false;
                    Data.setLoginStatus(false);
                    Data.setHomeStatus(true);
                    $scope.homeDiv=true;
                })
                .error(function(error){
                    $scope.dsnError='true';
                    $scope.message="error getting session";
                    console.log("error getting session");
                    console.log(error);
                    $scope.loginDiv=true;
                    $scope.homeDiv=false;
                    Data.setLoginStatus(true);
                    Data.setHomeStatus(false);
                });
        }

//   Home
        $scope.progressBarStatus=Data.getProgressBarStatus();
        $scope.lastUpdated = new Date();
        Event.getTotalEvents().then(function(l){
          $scope.totalEvents=l;
        });
        Claim.getTotalClaims().then(function(c){
            $scope.totalClaims=c;
        });
        Task.getTotalTasks().then(function(t){
            $scope.totalTasks=t;
        });
        $scope.home=function(){
            console.log("home called");
            $scope.user=Auth.getUser();
            $scope.eventsReady=Event.getLoading();
            $scope.tasksReady=Task.getLoading();
            $scope.claimsReady=Claim.getLoading();
            Data.setProgressBar;

            Event.getTotalEvents().then(function(l){
                $scope.totalEvents=l;
                $scope.eventsReady=Event.getLoading();
                $scope.progressBarStatus+=30;
                Data.setProgressBarStatus(30);
            } );

            Claim.getTotalClaims().then(function(c){
                $scope.totalClaims=c;
                $scope.claimsReady=Claim.getLoading();
                $scope.progressBarStatus+=30;
                Data.setProgressBarStatus(30);
            });

            Task.getTotalTasks().then(function(t){
                $scope.totalTasks=t;
                $scope.tasksReady=Task.getLoading();
                $scope.progressBarStatus+=40;
                Data.setProgressBarStatus(40);
            });
        }
        $scope.reload= function(){
            //$scope.progressBarStatus=0;
            Data.load();
        }

        $scope.logout= function(){
            Auth.logout(Auth.getSession())
                .success(function(){
                    Auth.setIsAuthenticated("false");
                    Auth.setSessionID('');
                    console.log("bye bye");
                    $scope.welcomeDiv=true;
                    $scope.loginDiv=false;
                    $scope.homeDiv=false;
                    Data.setProgressBar;
                    Data.setWelcomeStatus(true);
                    Data.setLoginStatus(false);
                    Data.setHomeStatus(false);
                })
                .error(function(){
                    console.log("error logging out");
                    console.log("aaj tu logout krke dikha :D");
                })
        }

}]);


