﻿'use strict';
app.controller('ClaimCtrl',['$scope','$routeParams','$q','Auth','Claim','Event','Task','Code','Note','Diary','Attachment',
    function($scope,$routeParams,$q,Auth,Claim,Event,Task,Code,Note,Diary,Attachment){
    
    $scope.claim=Claim.find($routeParams.claimId);
    $scope.claimNo=$routeParams.claimId;
    $scope.pid=Claim.getPid($scope.claimNo);
    console.log($scope.claim);
    $scope.error='false';
    $scope.errorMessage='';
    $scope.status='';
    $scope.message='';
    $scope.loading='true';
    $scope.adjusterInformation='';
    $scope.claimantsInformation='';
    $scope.claimInfo='';
    $scope.tasks='';
    $scope.eventDetail='';
    $scope.paymentHistory='';
    $scope.personsInvolved=[];
    $scope.policyManagementInformation='';
    $scope.personsInvolvedTypeSelected='';
    $scope.notes=[];
    $scope.employeeInformation='';

    //Request Claim details
    Claim.requestClaimDetails($scope.claimNo)
            .success(function(data){
                $scope.status=data.ResultMessage.MsgStatus.MsgStatusCd;
                if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                    $scope.error=true;
                    $scope.errorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                    console.log("error fetching claim details");
                    console.log(data);
                    $scope.loading='false';
                }
                else{
                    $scope.claim=data;
                    console.log("claim");
                    console.log(data);
                    $scope.adjusterInformation=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.AdjusterInformation;
                    $scope.claimantsInformation=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.ClaimantsInformation;
                    console.log($scope.claimantsInformation);
                    $scope.claimInfo=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.ClaimInfo;
                    $scope.eventDetail=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.EventDetail;
                    $scope.paymentHistory=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.PaymentHistory;
                    $scope.policyManagementInformation=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.PolicyManagementInformation;
                    if(data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.EmployeeInformation){
                        $scope.employeeInformation=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.EmployeeInformation;
                    }
                    var temp=data.ResultMessage.Document.ExecutiveSummaryReport.File.ClaimSummary.PersonsInvolved;
                    console.log(temp);

                    //Sorting PersonsInvolved
                    angular.forEach(temp,function(person){
                        for(var key in person){
                            if(Array.isArray(person[key])){
                                angular.forEach(person[key],function(p){
                                    $scope.personsInvolved.push(p);
                                })
                            }
                            else{
                                    $scope.personsInvolved.push(person[key]);
                            }
                            break;
                        }
                    })
                    console.log($scope.personsInvolved);

                    $scope.tasks=Task.find($scope.claimNo);
                    console.log($scope.tasks);
                    if($scope.claimInfo.EnhancedNotes){
                        if(Array.isArray($scope.claimInfo.EnhancedNotes.Note)){
                            angular.forEach($scope.claimInfo.EnhancedNotes.Note,function(note){
                                $scope.notes.push(note);
                            });
                        }
                        else{
                            $scope.notes.push($scope.claimInfo.EnhancedNotes.Note);
                        }
                    }


                    $scope.loading='false';

                }

            })
            .error(function(error){
                $scope.error=true;
                $scope.errorMessage=error;
                $scope.serverError="Server not reachable";
                $scope.loading='false';
                $scope.status=error.status;
                console.log("error fetching claim details");
                console.log(error);
            });

    //Request PaymentDetails
        $scope.paymentDetailStatus='';
        $scope.paymentDetailError='false';
        $scope.paymentDetailMessage='';
        $scope.paymentDetails='';
        Claim.requestClaimPaymentDetails($scope.claim)
              .success(function(data){
                console.log("payment details request response");
                console.log(data);
                if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                   $scope.paymentDetailError=true;
                   $scope.paymentDetailMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                   console.log("error fetching payment details for claim :"+$scope.claimNo);
                   $scope.loading='false';
                }
                else{
                    $scope.paymentDetailError=false;
                    $scope.paymentDetails=data.ResultMessage.Document.PaymentHistory;
                    console.log("payment details resquest");
                    console.log(data);
                }
            })
              .error(function(data){
                $scope.paymentDetailError=true;
                $scope.paymentDetailMessage='error reaching service';
                $scope.serverError="Server not reachable";
                console.log("error reaching service");
                console.log(data);
            });

    //Payment Filter
        $scope.paymentFilterType='';
        $scope.paymentFilter=function(p){
            console.log("filter "+p);
            $scope.paymentFilterType=p;
        }
    //PersonsInvolved Filter
        $scope.personsInvolvedTypes=Code.getPersonsInvolvedType();
        $scope.personFilter=function(p){
//            $scope.persons=[];
//            angular.forEach($scope.personsInvolved, function(persons){
//                angular.forEach(persons, function(person){
//
//                    if(person.EntityType){
//                        console.log(person);
//                        console.log(person.EntityType);
//                        if(person.EntityType.indexOf(p.desc.toString())>-1){
//                            console.log("inside");
//                            $scope.persons.push(
//                                {
//                                    'name' : person.Name,
//                                    'type' : person.EntityType,
//                                    'address' : person.AddressInformation.Address1.Addr1 + person.AddressInformation.Address1.Addr2,
//                                    'homePhone' : person.HomePhone,
//                                    'officePhone' : person.OfficePhone
//                                });
//                        }
//                    }
//                    else{
//                        console.log(person);
//                        console.log(person.Type);
//                        if(person.Type){
//                            if(person.Type.indexOf(p.desc.toString())>-1) {
//                                $scope.persons.push(
//                                    {
//                                        'name': person.Name,
//                                        'type': person.Type,
//                                        'address': person.AddressInformation.Address1.Addr1 + person.AddressInformation.Address1.Addr2,
//                                        'homePhone': '',
//                                        'officePhone': ''
//                                    });
//                            }
//                        }
//                    }
//
//
//                });
//            });
//            $scope.personsInvolved=$scope.persons;
//            console.log("filtered");
//            console.log($scope.persons);
        }

    //Add Notes
    $scope.noteError=false;
    $scope.noteErrorMessage='';
    $scope.noteAdded='false';
    $scope.note= {claimNumber:'', activityDate:'', noteText:'', noteTypeCode:''};
    $scope.noteTypes= Code.getNoteTypeCode();
    $scope.submitNote =function(){
        $scope.note.claimNumber=$routeParams.claimId;
        $scope.note.noteTypeCode=$scope.note.noteTypeCode.codeid;
        var defer = $q.defer();
        console.log("going to post note");
        Note.postNote($scope.note)
        .success(function(data, status, headers, config){
                if(data.ProgressNote){
                    console.log("note added!");

                    var temp={ActivityDate:'',ClaimNumber:'',DateCreated:'',EnteredBy:'',NoteId:'',NoteText:'',NoteType:'',Subject:'',TimeCreated:'',UserType:''};
                    temp.ClaimNumber=data.ProgressNote["ClaimNumber"];
                    temp.NoteId=data.ProgressNote["ClaimProgressNoteId"];
                    temp.DateCreated=data.ProgressNote["DateCreated"];
                    temp.ActivityDate=data.ProgressNote["DateEntered"];
                    temp.TimeCreated=data.ProgressNote["TimeCreated"];
                    temp.NoteText=$scope.note.noteText;
                    temp.NoteType=$scope.note.noteTypeCode;
                    $scope.noteAdded=true;
                    $scope.noteError=false;
                    $scope.notes.push(temp);
                    $scope.noteAdded='true';
                    console.log(data);
                    defer.resolve();
                }
                else {
                    $scope.noteError=true;
                    $scope.noteAdded=false;
                    $scope.noteErrorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                    console.log("error posting note");
                    console.log(data);
                    console.log(status);
                }
        })
        .error(function(data, status, headers, config) {
                $scope.noteError=true;
                $scope.noteAdded=false;
                $scope.noteErrorMessage="error posting note...server not reachable";
                console.log(data);
                console.log(status);
                console.log(config);
        });
        defer.promise.then(function(){
            console.log("its resovled now ");
            //creating a note to push live..with attributes same as coming from RMA
        });
        
        
        
    }

    //Add Task
    $scope.taskError='false';
    $scope.taskErrorMessage='';
    $scope.taskAdded='false';
    $scope.taskEntered='';
    $scope.task={ name:'', type: '', dueDate :'', dueTime : '', description : '', recordId:'', table:'', activity:''};
    $scope.taskTypes=Code.getWpaActivities();
    $scope.submitTask=function(){
        $scope.task.recordId=$routeParams.claimId;
        $scope.task.table='CLAIM';
        console.log($scope.taskEntered);
        $scope.task.type=$scope.taskEntered.codeid;
        $scope.task.activity=$scope.taskEntered.codeid+" | "+$scope.taskEntered.shortcode+" "+$scope.taskEntered.desc;
        console.log($scope.task);
        Diary.postDiary($scope.task)
        .success(function(data,status, headers, config){
                if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                    $scope.taskError=true;
                    $scope.taskAdded=false;
                    $scope.taskErrorMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
                    console.log("error posting task");
                    console.log(data);
                    console.log(status);
                }
                else {
                    console.log("task added");
                    $scope.taskAdded=true;
                    $scope.taskError=false;
                    console.log(data);
                    var temp={ "task subject":"", "entry notes":"","work activity":"","complete date":"","complete time":"", "attachments":"","linked":"","type":""};
                    temp["task subject"]=$scope.task.name;
                    temp["entry notes"]=$scope.task.description;
                    temp["complete date"]=$scope.task.dueDate;
                    temp["complete time"]=$scope.task.dueTime;
                    temp["work activity"]=$scope.task.activity;
                    temp["type"]="C";
                    temp["attachments"]=$routeParams.claimId;
                    $scope.tasks.push(temp);
                    console.log($scope.tasks)
                    Task.addTask(temp);
                }
            })
        .error(function(data,status, headers,config){
            console.log("error creating task");
            $scope.message="error creating task";
            $scope.taskError=true;
            $scope.taskAdded=false;
            $scope.taskErrorMessage="error creating task";
            console.log(data);
            })

    }

    //camera starts
    $scope.cam='true';
    $scope.picture='false';
    $scope.savedPic='';
    $scope.attachPic = function(pic){
          console.log("back in main controller");
          console.log(pic);
          $scope.savedPic=pic;
          $scope.cam='false';
          $scope.picture='true';
        }
    $scope.reTake= function(){
           console.log('retake');
           $scope.cam='true';
           $scope.picture='false';
        }
    //camera ends

    //Attachments
    $scope.attachment={ fileName:'', title:'', subject:'',notes:'',content:'', number:''};
    $scope.attach=function(){
            $scope.attachment.content=$scope.savedPic.replace("data:image/png;base64,","");
            $scope.attachment.fileName='abc.jpeg';
            $scope.attachment.title='tempie title';

            $scope.attachment.number=$routeParams.claimId;
            Attachment.requestClaimAttachment($scope.attachment)
                .success(function(data){
                    console.log(data);
                    if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
                        $scope.message=data.ResultMessage.MsgStatus.ExtendedStatus;
                    }
                    else{
                        console.log("uploaded");
                        console.log(data);
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log("data");
                    console.log(data);
                    console.log("status");
                    console.log(status);
                    console.log("headers");
                    console.log(headers);
                    console.log("config");
                    console.log(config);
                })
        }


        //    //Request PersonsInvolved
//        $scope.personsInvolvedStatus='';
//        $scope.personsInvolvedError='false';
//        $scope.personsInvolvedMessage='';
//        $scope.persons=[];
//        Claim.requestPersonsInvolved($scope.claimNo)
//            .success(function(data,status,config){
//                console.log("persons involved request response");
//                console.log(data);
//
//                if(data.ResultMessage.MsgStatus.MsgStatusCd=='Error'){
//                   $scope.personsInvolvedError=true;
//                   $scope.personsInvolvedMessage=data.ResultMessage.MsgStatus.ExtendedStatus;
//                   console.log("error fetching persons involved details");
//                   console.log(data);
//                   $scope.loading='false';
//                }
//                else{
//                    $scope.personsInvolvedError=false;
//                    var columns=[];
//                    //getting column heads
//                    if(data.ResultMessage.Document.results.columns.column){
//                        angular.forEach(data.ResultMessage.Document.results.columns.column, function(column){
//                            columns.push(column.text);
//                        })
//                    }
//
//                    //formatting data
//                    if(data.ResultMessage.Document.results.data.row){
//                        angular.forEach(data.ResultMessage.Document.results.data.row, function(row){
//                            var temp={};
//                            angular.forEach(row.field, function(v, j){
//                                temp[columns[j]]=v;
//                            });
//                            $scope.persons.push(temp);
//                        });
//                    }
//                    console.log("persons involved ");
//                    console.log($scope.persons);
//                }
//            })
//            .error(function(data){
//                $scope.personsInvolvedError=true;
//                $scope.personsInvolvedMessage='error reaching service';
//                console.log(data);
//            });
    }]);