'use strict';

/**
 * @ngdoc overview
 * @name yoRiskyApp
 * @description
 * # yoRiskyApp
 *
 * Main module of the application.
 */
var app=angular.module('yoRiskyApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'LocalStorageModule'
  ])
    .config(['localStorageServiceProvider', function(localStorageServiceProvider){
        localStorageServiceProvider.setPrefix('csc.riskmaster.mobile');

    }])
    .config(function ($routeProvider,$httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        //$httpProvider.defaults.cache = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $routeProvider
            .when('/',{
                templateUrl: '../views/home.html',
                controller:'HomeCtrl'
            })
            .when('/home',{
                templateUrl: '../views/home.html',
                controller:'HomeCtrl'
            })
            .when('/login', {
                templateUrl : 'views/login.html',
                controller: 'LoginCtrl'
            })
            .when('/events',{
                templateUrl : 'views/events.html',
                controller:'EventsCtrl'
            })
            .when('/events/:eventId',{
                templateUrl : 'views/event.html',
                controller:'EventCtrl'
            })
            .when('/claims',{
                templateUrl: 'views/claims.html',
                controller:'ClaimsCtrl'
            })
            .when('/claims/:claimId',{
                templateUrl: 'views/claim.html',
                controller:'ClaimCtrl'
            })
            .when('/tasks',{
                templateUrl : 'views/tasks.html',
                controller:'TasksCtrl'
            })
            .when('/request', {
                templateUrl : 'views/request.html',
                controller: 'RequestCtrl'
            })
            .when('/feedback', {
                templateUrl : 'views/feedback.html',
                controller: 'FeedbackCtrl'
            })
            .when('/settings', {
                templateUrl : 'views/settings.html',
                controller: 'SettingsCtrl'
            })
            .when('/about',{
                templateUrl: 'views/about.html'
            })
            .when('/404',{
                templateUrl: '404.html'
            })
            .when('/temp',{
                templateUrl: 'views/temp.html',
                controller:'tempCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
