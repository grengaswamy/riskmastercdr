<%@ page session="false" %>
<%@ page import="java.io.IOException"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.net.URL"%>
<%@ page import="java.net.URLConnection"%>
<%@ page import="java.net.URLEncoder"%>
<html>
<head>
<title>CSC Authentication Gateway</title>
</head>

<body>
<%
	//BSB 03.08.2007
	// Create a "pass-thru" page that can accept credentials and 
	// silently set up an SSO session before redirecting the client to
	// the final application landing page.
	// Currently you must land on home.html in order to correctly 
	// set up an RMX session.
	
	// Note: All SSO and Application URLS to accomplish this 
	// are constructed during the request - hence there is no configured 
	// full URL's to mess up reverse proxy or other host-name\ip address
	// funny business.

	//Values arriving from external world...
	String username = (String) request.getParameter("loginname");
	if(username==null) username="";
	//System.out.println("Username="+username);
	String password = (String) request.getParameter("password");
	if(password==null) password="";
	
	
	//BSB Construct additional details for use after SSO that should
	// be sheperded all the way to the oxf/status page.
	String rmx_details="";
	String apptarget = (String) request.getParameter("apptarget");
	if(apptarget==null) 
		apptarget="";
	else
		rmx_details = rmx_details + "&apptarget=" + URLEncoder.encode(apptarget);

	String datasource = (String) request.getParameter("datasource");
	if(datasource==null)
		datasource="";
	else
		rmx_details = rmx_details + "&datasource=" + datasource;

	String view = (String) request.getParameter("view");
	if(view==null)
		view="";
	else
		rmx_details = rmx_details + "&view=" + view;

	//Calculated URL Paths...	
	String base = request.getScheme() + "://" + request.getServerName() + ":" + Integer.toString(request.getServerPort());
 	//String target_url =  base + "/oxf/home?pg=riskmaster/RMXPortal/main";
	String target_url =  base + request.getContextPath() + "/redirect.jsp";
	//System.out.println(target_url); 
	String SSO_url =  base + "/SSO/loginService";
	String SSO_forward_url = base + "/SSO/goService3.jsp"; 
	
	//Do the SSO Login Request...
	String token_and_ticket = SSOloginRequest(request, response, SSO_url, URLEncoder.encode(target_url), username, password );
	
	String strIncorrectLoginURL=request.getScheme() + "://" + request.getServerName() + ":" + Integer.toString(request.getServerPort())+request.getContextPath() +"/login.jsp";
	//System.out.println (strIncorrectLoginURL);

	//Forward so that cookie can be set and SSO will do the final redirect...
	if(token_and_ticket != null && !token_and_ticket.startsWith("loginerror"))
	{
		//BSB Tack on Parameters to be passed into RMX.     
		// This can only happen AFTER setting up access to the service 
		// We don't want the querystring params set up as part of the service name in there)'
		// That will cause the following type of nasty error from SSO:
		// javax.servlet.ServletException: CAS authentication error: INVALID_SERVICE ticket 'ST-42-hQ8fLzAjV0zrFyCTAx6o' does not match supplied service
		target_url =  target_url + "?rmx_details=" + URLEncoder.encode(rmx_details);
		
		SSO_forward_url = SSO_forward_url + token_and_ticket.substring(0,token_and_ticket.lastIndexOf("&casurl=")) + "&casurl=" + URLEncoder.encode(target_url) ;
		//SSO_forward_url = SSO_forward_url + "&username=" + username;
	%>
		<form id='RMXFrmLogin' name='' method='post' action='<%=SSO_forward_url %> ' >
			<input id='username' name='username' value='<%=username%>' type='hidden'/>			
			
		</form>;
		<script language='JavaScript'>									
			RMXFrmLogin.submit();
		</script>
	<%
		
		//response.sendRedirect(SSO_forward_url); 
	}
	else
	{
		response.sendRedirect(strIncorrectLoginURL); 
	}
%>

	
   
	</body>
</html>
<%!

String TGC_ID = "CASTGC";
String CAS_Ticket = "casticket=";
String CAS_TGC = "castgc=";
String Service_Token = "token";
String Service_Ticket = "ticket";
String PRIVACY_ID = "CASPRIVACY";

String SSOloginRequest(HttpServletRequest request, HttpServletResponse response, String SSOurl, String casurl, String username, String password )
  throws IOException
  {
  	String token = null;
  	String ticket = null;
  	String token_and_ticket = null;
  	
	int i_port = request.getServerPort();
  	String s_port = Integer.toString(i_port);
  	
 
  	String urlparms = "&casurl=" + casurl;
 
  	String url = SSOurl + "?username=" + username + "&password=" + URLEncoder.encode(password) + urlparms;
    BufferedReader r = null;
    	try 
    	{
    		URL u = new URL(url);
      		URLConnection uc = u.openConnection();
      		uc.setRequestProperty("Connection", "close");
      
      		r = new BufferedReader(new InputStreamReader(uc.getInputStream()));
      
      		String line;
      
      		StringBuffer buf = new StringBuffer();
      
      		while ((line = r.readLine()) != null)
        		buf.append(line + "\n");
      
      		String returndata = buf.toString();
      		//uc.
      		if(returndata != null && !returndata.startsWith("loginerror"))
      		{
      			if(returndata.startsWith(CAS_Ticket))
      			{
      				int start_token = returndata.indexOf(CAS_Ticket);
      				int end_token = returndata.indexOf(",");
      				if(!(start_token == -1) && !(end_token == -1))      					
      					token = returndata.substring(start_token + CAS_Ticket.length(),end_token);
      				
        			int start_tgc = returndata.indexOf(CAS_TGC);
          			int end_tgc = returndata.indexOf(",", start_tgc);
     				if(!(start_tgc == -1) && !(end_tgc == -1))             			
     					ticket = returndata.substring(start_tgc + CAS_TGC.length(), end_tgc);
      			}
  
      			if(!(token == null) && !(ticket == null))
      				token_and_ticket = "?" + Service_Token + "=" + token + "&" + Service_Ticket + "=" + ticket + urlparms;
      			
      			return token_and_ticket;
      		}
      		else
      		if(returndata != null && returndata.startsWith("loginerror"))
      			token_and_ticket = returndata;
      		
      		return token_and_ticket;
    	} 
    	catch (Exception ex)
		{
	        // ignore
		}
    	finally
		{
	      try
	      {
	        if (r != null)
	          r.close();
	      }
	      catch (IOException ex)
	      {
	        // ignore
	      }
    	}
    	return token_and_ticket;
    }

%>