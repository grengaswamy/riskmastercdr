﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Riskmaster
{
    public partial class GenericErrorPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the last error from the server
            Exception ex = Server.GetLastError();

            // Create a safe message
            string safeMsg = "A problem has occurred in the web site. ";

            // Show Inner Exception fields for local access
            Exception baseException = ex.GetBaseException();
            if (ex.InnerException != null && baseException != null)
            {
                innerTrace.Text = baseException.StackTrace;
                InnerErrorPanel.Visible = Request.IsLocal;
                innerMessage.Text = baseException.Message;
            }
            // Show Trace for local access
            if (Request.IsLocal)
                exTrace.Visible = true;

            // Fill the page fields
            exMessage.Text = ex.Message;
            exTrace.Text = ex.StackTrace;

            //Display the error message details
            //Response.Write(String.Format("<h1>Error message: {0}</h1>", ex.Message));
            //// Show error details
            //Response.Write("<p>Error details:</p>");
            //Response.Write(ex.ToString());


            //Log the root cause of the error here
            ErrorHelper.logErrors(baseException);

            Server.ClearError();
        }
    }
}
