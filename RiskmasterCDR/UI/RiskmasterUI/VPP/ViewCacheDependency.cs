﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Caching;
using System.Timers;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
//using Riskmaster.UI.VPPService;
using Riskmaster.Models;

namespace Riskmaster.VPP
{
    /// <summary>
    /// Summary description for PageCacheDependencyFactory
    /// </summary>
    public class PageCacheDependencyFactory
    {
        public System.Timers.Timer m_Timer = new System.Timers.Timer();
        //private string m_LastUpdated = string.Empty;

        //timer polling interval in seconds
        public int m_PollingInterval = 10;

        private static volatile PageCacheDependencyFactory m_instance = null;
        private static object syncFactory = new object();

        private Dictionary<string, PageCacheDependency> m_CacheDependencyDictionary = new Dictionary<string, PageCacheDependency>();
        private Dictionary<string, string> m_FileLastUpdatedDictionary = new Dictionary<string, string>();
        public Dictionary<string, string> m_LastUpdatedDictionary = new Dictionary<string, string>();
        private object syncObject = null;// new object();

        private PageCacheDependencyFactory()
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["VPPPollingInterval"]))
            {
                m_PollingInterval = int.Parse(ConfigurationManager.AppSettings["VPPPollingInterval"]);
                if (m_PollingInterval < 10)
                    m_PollingInterval = 10;
            }

            //m_Timer.Interval = m_PollingInterval * 1000;
            //m_Timer.Elapsed += new ElapsedEventHandler(CheckDependencyHandler);
            //m_Timer.Start();

            //m_LastUpdated = GetInitialLastUpdated();
        }
        private object GetObject(int iClientId)
        {
            object obj = Riskmaster.Cache.CacheCommonFunctions.RetreiveValueFromCache<object>("SyncObject", iClientId);
            if (obj == null)
            {
                obj = new object();
                Riskmaster.Cache.CacheCommonFunctions.AddValue2Cache<object>("SyncObject", iClientId,obj);
            }
            return obj;
        }

        /// <summary>
        /// Get initial last updated time stamp value
        /// </summary>
        /// <returns></returns>
        public string GetInitialLastUpdated(int iClientId)
        {
            string sMaxLastUpdated = string.Empty;
            RMServiceType oRMServiceType = new RMServiceType();
            VPPTimeStampResponse oServiceResponse = new VPPTimeStampResponse();
            //VPPServiceClient oServiceClient = new VPPServiceClient();
            try
            {
                oRMServiceType.ClientId = iClientId;
                oServiceResponse = AppHelper.GetResponse<VPPTimeStampResponse>("RMService/VPP/inittimestamp", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oRMServiceType);
                //oServiceResponse = oServiceClient.GetInitialTimeStamp(oServiceRequest);
            }
            finally
            {
                oRMServiceType = null; 
                //if (oServiceClient != null)
                //    oServiceClient.Close();
            }

            sMaxLastUpdated = oServiceResponse.TimeStamp;

            return sMaxLastUpdated;
        }

        /// <summary>
        /// Get the singleton object
        /// </summary>
        public static PageCacheDependencyFactory Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (syncFactory)
                    {
                        if (m_instance == null)
                            m_instance = new PageCacheDependencyFactory();
                    }
                }

                return m_instance;
            }
        }

        public PageCacheDependency GetPageCacheDependency(string sPageName,int iClientId)
        { 
            string sCacheKey = sPageName.ToLower();
            lock (GetObject(iClientId))
            {
                if (m_CacheDependencyDictionary.ContainsKey(sCacheKey))
                {
                    m_CacheDependencyDictionary[sCacheKey] = null;
                    m_CacheDependencyDictionary.Remove(sCacheKey);
                }

                PageCacheDependency oCacheDependency = new PageCacheDependency(sCacheKey);
                m_CacheDependencyDictionary.Add(sCacheKey, oCacheDependency);
                return m_CacheDependencyDictionary[sCacheKey];
            }
        }

        /// <summary>
        /// Get page's last updated time stamp from the cached dictionary. if not found,
        /// try to retrieve from the database
        /// </summary>
        /// <param name="sPageName"></param>
        /// <returns></returns>
        public string GetPageLastUpdated(string sPageName,int iClientId)
        {
            string sLastUpdate = string.Empty;
            lock (GetObject(iClientId))
            {
                string sCacheKey = sPageName.ToLower();
                if (m_FileLastUpdatedDictionary.ContainsKey(sCacheKey))
                {
                    sLastUpdate = m_FileLastUpdatedDictionary[sCacheKey];
                }
                else
                {
                    sLastUpdate = GetFileLastUpdatedFromDB(sCacheKey);
                    m_FileLastUpdatedDictionary.Add(sCacheKey, sLastUpdate);
                }

                return sLastUpdate;
            }
        }


        /// <summary>
        /// Get page's last updated timestamp from the database
        /// </summary>
        /// <param name="sPageName"></param>
        /// <returns></returns>
        private string GetFileLastUpdatedFromDB(string sFullPageName)
        {
            string sLastUpdated = string.Empty;

            try
            {
                string sDsnID = string.Empty;
                string sViewID = string.Empty;
                string sPageName = string.Empty;
                string sClientId = string.Empty;
				//rsolanki2: Updating regex to account for Custom views which can have negative viewID
                Regex rx = new Regex(@"(?<datasourceid>\d+)_(?<viewid>-?\d+)_(?<clientid>-?\d+)_(?<pagename>[\w_-]+\.aspx)");
                if (rx.IsMatch(sFullPageName))
                {
                    sDsnID = rx.Match(sFullPageName).Result("${datasourceid}");
                    sViewID = rx.Match(sFullPageName).Result("${viewid}");
                    sClientId = rx.Match(sFullPageName).Result("${clientid}");
                    sPageName = rx.Match(sFullPageName).Result("${pagename}");
                }

                //It may need throw exception if any of there value is blank
                if (!(string.IsNullOrEmpty(sDsnID) || string.IsNullOrEmpty(sViewID) || string.IsNullOrEmpty(sPageName) || string.IsNullOrEmpty(sClientId)))
                {
                    VPPFileRequest oVPPFileRequest = new VPPFileRequest();
                    oVPPFileRequest.DataSourceId = int.Parse(sDsnID);
                    oVPPFileRequest.ViewId = int.Parse(sViewID);
                    oVPPFileRequest.PageName = sPageName;
                    oVPPFileRequest.ClientId = int.Parse(sClientId);
                    VPPTimeStampResponse oServiceResponse = null;
                    //VPPServiceClient oServiceClient = new VPPServiceClient();
                    try
                    {
                        //oServiceResponse = oServiceClient.GetFileTimeStamp(oServiceRequest);
                        oServiceResponse = AppHelper.GetResponse<VPPTimeStampResponse>("RMService/VPP/filetimestamp", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oVPPFileRequest);
                    }
                    finally
                    {
                        oVPPFileRequest = null;
                        //if (oServiceClient != null)
                        //    oServiceClient.Close();
                    }

                    if (oServiceResponse != null)
                    {
                        sLastUpdated = oServiceResponse.TimeStamp;
                    }
                }
            }
            catch (Exception e)
            {
                string sError = e.Message;
            }

            return sLastUpdated;
        }

        /// <summary>
        /// The function will be called by the timer periodically
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void CheckDependencyHandler(object sender, ElapsedEventArgs e,string sClientIds)
        {
            string[] sCIds = sClientIds.Split(',');
            foreach (string sId in sCIds)
            {
                string sLastUpdated = string.Empty;

                //Get pages which have been updated during last polling cycle
                VPPModifiedPagesRequest oVPPModifiedPagesRequest = new VPPModifiedPagesRequest();
                VPPModifiedPagesResponse oServiceResponse = null;
                //VPPServiceClient oServiceClient = new VPPServiceClient();
                oVPPModifiedPagesRequest.LastUpdated = m_LastUpdatedDictionary["VPPLastUpdated_"+sId];
                try
                {
                    oVPPModifiedPagesRequest.ClientId = int.Parse(sId);
                    oServiceResponse = AppHelper.GetResponse<VPPModifiedPagesResponse>(Common.CommonFunctions.RetrieveBaseURL() + "RMService/VPP/modifiedpages", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oVPPModifiedPagesRequest);
                    //oServiceResponse = oServiceClient.GetModifiedPageList(oServiceRequest);
                }
                finally
                {
                    //if (oServiceClient != null)
                    //    oServiceClient.Close();
                }

                List<string> oModifiedPageList = oServiceResponse.ModifiedPageList.ToList();
                if (oModifiedPageList == null)
                    return;

                //reset each cachedependency object for the modifed pages
                foreach (string sPageName in oModifiedPageList)
                {
                    var KVPs = from kvp in m_CacheDependencyDictionary
                               where kvp.Key.EndsWith(sPageName)
                               select kvp;
                    foreach (KeyValuePair<string, PageCacheDependency> kvp in KVPs)
                    {
                        kvp.Value.ResetCache();
                    }
                }

                if (!string.IsNullOrEmpty(oServiceResponse.MaxLastUpdated))
                {
                    m_LastUpdatedDictionary["VPPLastUpdated_" + sId] = oServiceResponse.MaxLastUpdated;
                }
            }
        }

        /*protected override void DependencyDispose()
        {
            m_Timer.Stop();
        }
         */
    }


    /// <summary>
    /// PageDependency object to control which page is expired (out of date)
    /// </summary>
    public class PageCacheDependency : CacheDependency
    {
        private string m_CacheKey = string.Empty;

        public PageCacheDependency(string sCacheKey)
        {
            m_CacheKey = sCacheKey;
        }

        public void ResetCache()
        {
            NotifyDependencyChanged(this, EventArgs.Empty);
        }

        protected override void DependencyDispose()
        {
        }
    }
}