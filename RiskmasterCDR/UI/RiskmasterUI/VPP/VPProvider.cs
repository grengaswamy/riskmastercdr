﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Hosting;
using System.Collections;
using System.Web.Caching;
using System.Security.Permissions;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
//using Riskmaster.UI.VPPService;
using System.IO;
using Riskmaster.Models;
using System.Timers;

namespace Riskmaster.VPP
{
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Medium)]
    [AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.High)]
    public class VPProvider : VirtualPathProvider
    {
        Hashtable m_VirtualFiles = null;
        private bool m_bCachePage = false;

        public VPProvider()
            : base()
        {
            //
            // TODO: Add constructor logic here
            //
            if (ConfigurationManager.AppSettings["CachePage"] == "1")
                m_bCachePage = true;
        }


        /// <summary>
        /// Get the aspx file name for the requested url
        /// </summary>
        /// <param name="virtualPath">The url of the requested page</param>
        /// <returns></returns>
        private string GetPageName(string virtualPath)
        {
            Regex rx = new Regex(@"/(?<pagename>[\w_-]+\.aspx)");
            string sPageName = rx.Match(virtualPath).Result("${pagename}");
            return sPageName;
        }

        /// <summary>
        /// Check if requested page should be retrieved from the database.
        /// If the page url in the format of number_number_page.aspx, it should be retrieved from DB
        /// </summary>
        /// <param name="virtualPath">The url of the requested page</param>
        /// <returns></returns>
        private bool IsPathVirtual(string virtualPath)
        {
            //limit the page name to [a-zA-Z_0-9-]
			//rsolanki2: Updating regex to incorporate updates for Custom views
            Regex rx = new Regex(@"/\d+_-?\d+_[\w_-]+\.aspx");
            return rx.IsMatch(virtualPath);
        }

        /// <summary>
        /// Check if requested page exists or not
        /// </summary>
        /// <param name="virtualPath">The url of the requested page</param>
        /// <returns></returns>
        public override bool FileExists(string virtualPath)
        {
            if (IsPathVirtual(virtualPath))
            {
                return true;
            }
            else
                return Previous.FileExists(virtualPath);
        }

        /// <summary>
        /// Let ASP.NET default provider determine if the virtual directory exists or not
        /// </summary>
        /// <param name="virtualDir">The virtual folder in the requested url</param>
        /// <returns></returns>
        public override bool DirectoryExists(string virtualDir)
        {
            // Right now, we are not storing the directory information in  
            // our database. We assume that all of the virtual content is 
            // served from a directory that is created inside the ASP.NET 
            // Web site. Therefore, let ASP.NET default provider determine
            // if the virtual directory exist or not.
            return Previous.DirectoryExists(virtualDir);
        }


        /// <summary>
        /// This method is used by the compilation system to obtain a VirtualFile instance to 
        /// work with a given virtual file path.
        /// </summary>
        /// <param name="virtualPath">The requested url</param>
        /// <returns></returns>
        public override VirtualFile GetFile(string virtualPath)
        {
            if (IsPathVirtual(virtualPath))
                return new VPPVirtualFile(virtualPath, this);
            else
                return Previous.GetFile(virtualPath);
        }


        /// <summary>
        /// This method is used by the compilation system to obtain a VirtualDirectory 
        /// instance to work with a given virtual directory.
        /// </summary>
        /// <param name="virtualDir">The requested url</param>
        /// <returns></returns>
        public override VirtualDirectory GetDirectory(string virtualDir)
        {
            if (IsPathVirtual(virtualDir))
                return new VPPVirtualDirectory(virtualDir, this);
            else
                return Previous.GetDirectory(virtualDir);
        }

        /// <summary>
        /// retrieve aspx page from the database
        /// </summary>
        /// <param name="virtualPath">request url</param>
        /// <returns></returns>
        public string GetFileContents(string virtualPath)
        {
            //Get the DSN ID, View ID and page name value from the page url
            string sDsnID = string.Empty;
            string sViewID = string.Empty;
            string sPageName = string.Empty;
            string sPageContent = string.Empty;
            string sClientId = string.Empty;
            Regex rx = new Regex(@"/(?<datasourceid>\d+)_(?<viewid>-?\d+)_(?<clientid>-?\d+)_(?<pagename>[\w_-]+\.aspx)");
            if (rx.IsMatch(virtualPath))
            {
                sDsnID = rx.Match(virtualPath).Result("${datasourceid}");
                sViewID = rx.Match(virtualPath).Result("${viewid}");
                sClientId = rx.Match(virtualPath).Result("${clientid}");
                sPageName = rx.Match(virtualPath).Result("${pagename}");
            }

            //It may need throw exception if any of there value is blank
            if (!(string.IsNullOrEmpty(sDsnID) || string.IsNullOrEmpty(sViewID) || string.IsNullOrEmpty(sPageName) || string.IsNullOrEmpty(sClientId)))
            {
                VPPFileRequest oVPPFileRequest = new VPPFileRequest();
                oVPPFileRequest.DataSourceId = int.Parse(sDsnID);
                oVPPFileRequest.ViewId = int.Parse(sViewID);
                oVPPFileRequest.PageName = sPageName;
                oVPPFileRequest.ClientId = int.Parse(sClientId);
                VPPFileResponse oServiceResponse = null;
                //VPPServiceClient oServiceClient = new VPPServiceClient();
                try
                {
                    //oServiceResponse = oServiceClient.GetFileContent(oServiceRequest);
                    oServiceResponse = AppHelper.GetResponse<VPPFileResponse>("RMService/VPP/filecontent", AppHelper.HttpVerb.POST, AppHelper.APPLICATION_JSON, oVPPFileRequest);
                }
                finally
                {
                    oVPPFileRequest = null;
                    //if (oServiceClient != null)
                    //    oServiceClient.Close();
                }

                if (oServiceResponse != null)
                {
                    sPageContent = oServiceResponse.FileContent;
                }
            }

            //If the page does not exist in the database, we should return a error page
            if (string.IsNullOrEmpty(sPageContent))
            {
                string sUrl = string.Empty;
                int index = virtualPath.LastIndexOf("/");
                if (index > 0)
                {
                    sUrl = virtualPath.Substring(0, index);
                }
                sUrl += "//PageNotFound.aspx";

                string sPhyicalFilePath = HttpContext.Current.Request.MapPath(sUrl);
                if (File.Exists(sPhyicalFilePath))
                {
                    sPageContent = File.ReadAllText(sPhyicalFilePath);
                }
            }

            return sPageContent;
        }

        public Hashtable GetVirtualData
        {
            get { return this.m_VirtualFiles; }
            set { this.m_VirtualFiles = value; }
        }


        /// <summary>
        /// Creates a cache dependency based on the specified virtual paths
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <param name="virtualPathDependencies"></param>
        /// <param name="utcStart"></param>
        /// <returns></returns>
        public override CacheDependency GetCacheDependency(
                    string virtualPath,
                    System.Collections.IEnumerable virtualPathDependencies,
                    DateTime utcStart)
        {
            if (IsPathVirtual(virtualPath))
            {
                System.Collections.Specialized.StringCollection fullPathDependencies = null;

                // Get the full path to all dependencies.
                foreach (string virtualDependency in virtualPathDependencies)
                {
                    if (fullPathDependencies == null)
                        fullPathDependencies = new System.Collections.Specialized.StringCollection();

                    string sPhysicalPath = HostingEnvironment.MapPath(virtualDependency);
                    fullPathDependencies.Add(sPhysicalPath);
                }
                if (fullPathDependencies == null)
                    return null;

                // Copy the list of full-path dependencies into an array.
                string[] fullPathDependenciesArray = new string[fullPathDependencies.Count];
                fullPathDependencies.CopyTo(fullPathDependenciesArray, 0);
                // Copy the virtual path into an array.
                string[] virtualPathArray = new string[1];
                virtualPathArray[0] = HostingEnvironment.MapPath(virtualPath);

                if (m_bCachePage)
                {
                    string sPageName = GetPageName(virtualPath);
                    return PageCacheDependencyFactory.Instance.GetPageCacheDependency(sPageName,AppHelper.ClientId);
                }
                else
                    return new CacheDependency(virtualPathArray, fullPathDependenciesArray, utcStart);
            }
            else
                return Previous.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
        }

        public delegate void AsyncVPPCache(string sClientIds);
        /// <summary>
        /// Returns a hash of the specified virtual paths. For powerview page, the value is 
        /// the concatenation of file name and time stamp of the file
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <param name="virtualPathDependencies"></param>
        /// <returns></returns>
        public override string GetFileHash(string virtualPath, IEnumerable virtualPathDependencies)
        {
            string result = string.Empty;
            int iClientId = AppHelper.ClientId;
            if (IsPathVirtual(virtualPath))
            {
                string sFileName = GetPageName(virtualPath);

                string sKeyValue = Riskmaster.Cache.CacheCommonFunctions.RetreiveValueFromCache<string>("VPPClients", 0);
                if (string.IsNullOrEmpty(sKeyValue))
                {
                    Riskmaster.Cache.CacheCommonFunctions.AddValue2Cache<string>("VPPClients", 0, iClientId.ToString());
                    PageCacheDependencyFactory.Instance.m_LastUpdatedDictionary.Add("VPPLastUpdated_"+iClientId, PageCacheDependencyFactory.Instance.GetInitialLastUpdated(iClientId));
                    InvokeVPPCache(iClientId.ToString());
                }
                else if (!sKeyValue.Contains(iClientId.ToString()))
                {
                    sKeyValue += "," + iClientId;
                    Riskmaster.Cache.CacheCommonFunctions.UpdateValue2Cache<string>("VPPClients", 0, sKeyValue);
                    PageCacheDependencyFactory.Instance.m_LastUpdatedDictionary.Add("VPPLastUpdated_"+iClientId, PageCacheDependencyFactory.Instance.GetInitialLastUpdated(iClientId));
                    InvokeVPPCache(sKeyValue);
                }

                result = sFileName + AppHelper.ClientId + PageCacheDependencyFactory.Instance.GetPageLastUpdated(sFileName,iClientId);
            }
            else
            {
                result = Previous.GetFileHash(virtualPath, virtualPathDependencies);
            }

            return result;
        }
        /// <summary>
        /// Invoke VPP Cache
        /// </summary>
        /// <param name="sClientIds"></param>
        private void InvokeVPPCache(string sClientIds)
        {
            AsyncVPPCache dDelegate = new AsyncVPPCache(BeginVPPCheck);
            dDelegate.BeginInvoke(sClientIds, new AsyncCallback(EndVPPCheck), null);
        }
        /// <summary>
        /// Begin Check
        /// </summary>
        /// <param name="sClientId"></param>
        private void BeginVPPCheck(string sClientId)
        {
            if (PageCacheDependencyFactory.Instance.m_Timer.Enabled == true)
            {
                PageCacheDependencyFactory.Instance.m_Timer.Stop();
                PageCacheDependencyFactory.Instance.m_Timer.Close();
                PageCacheDependencyFactory.Instance.m_Timer.Dispose();
            }
            PageCacheDependencyFactory.Instance.m_Timer = new System.Timers.Timer();
            PageCacheDependencyFactory.Instance.m_Timer.Interval = (PageCacheDependencyFactory.Instance.m_PollingInterval / sClientId.Split(',').Count<string>()) * 1000;
            //m_Timer.Elapsed += new ElapsedEventHandler(CheckDependencyHandler);
            PageCacheDependencyFactory.Instance.m_Timer.Elapsed += new ElapsedEventHandler((sender, e) => PageCacheDependencyFactory.Instance.CheckDependencyHandler(sender, e, sClientId));
            PageCacheDependencyFactory.Instance.m_Timer.Start();
        }
        /// <summary>
        /// On end check
        /// </summary>
        /// <param name="ar"></param>
        private void EndVPPCheck(IAsyncResult ar)
        {
        }
    }
}
