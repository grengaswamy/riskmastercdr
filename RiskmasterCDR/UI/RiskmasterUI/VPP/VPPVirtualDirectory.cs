﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Security.Permissions;
using System.Web.Hosting;
namespace Riskmaster.VPP
{
    /// <summary>
    /// Summary description for VPPVirtualDirectory
    /// </summary>
    public class VPPVirtualDirectory : VirtualDirectory
    {
        VPProvider m_vpp = null;

        public VPPVirtualDirectory(string virtualDir, VPProvider provider)
            : base(virtualDir)
        {
            //
            // TODO: Add constructor logic here
            //
            m_vpp = provider;
        }

        private ArrayList m_children = new ArrayList();
        public override IEnumerable Children
        {
            get { return m_children; }
        }

        private ArrayList m_directories = new ArrayList();
        public override IEnumerable Directories
        {
            get { return m_directories; }
        }

        private ArrayList m_files = new ArrayList();
        public override IEnumerable Files
        {
            get { return m_files; }
        }
    }
}