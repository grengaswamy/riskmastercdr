﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Hosting;
using System.IO;
using System.Text;
namespace Riskmaster.VPP
{
    /// <summary>
    /// Summary description for VPPVirtualFile
    /// </summary>
    public class VPPVirtualFile : VirtualFile
    {
        private VPProvider m_vpp = null;
        private string m_VirtualPath = string.Empty;

        public VPPVirtualFile(string virtualPath, VPProvider provider)
            : base(virtualPath)
        {
            //
            // TODO: Add constructor logic here
            //
            this.m_vpp = provider;
            this.m_VirtualPath = virtualPath;
        }


        /// <summary>
        /// Retrieve the file content from the virtual source
        /// </summary>
        /// <returns></returns>
        public override System.IO.Stream Open()
        {
            string m_fileContents = m_vpp.GetFileContents(m_VirtualPath);
            Stream stream = new MemoryStream();
            if (!string.IsNullOrEmpty(m_fileContents))
            {
                //Put the page content on the stream
                StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);//Deb Encoding for some languages is required
                writer.Write(m_fileContents);
                writer.Flush();
                stream.Seek(0, SeekOrigin.Begin);
            }

            return stream;
        }
    }
}