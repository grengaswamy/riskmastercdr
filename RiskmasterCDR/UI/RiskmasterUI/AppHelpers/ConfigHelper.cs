﻿using System;
using System.Web.Configuration;
using System.Collections;

namespace Riskmaster.AppHelpers
{

    /// <summary>
    /// Handles all support for Configuration Section Management
    /// </summary>
    public class ConfigHelper
    {
        /// <summary>
        /// Gets the RISKMASTER X Version information configured in appSettings.config
        /// </summary>
        /// <returns>string containing the current RISKMASTER X Version release</returns>
        /// <remarks>This value is used for configuring Page Titles and other areas for display
        /// in the User Interface</remarks>
        public static string GetRMXVersion()
        {
            return WebConfigurationManager.AppSettings["RMXVersion"];
        }//method: GetRMXVersion()

        /// <summary>
        /// Gets the LogOut Page Url used in the LogOut action
        /// in custom authentication schemes
        /// </summary>
        /// <returns>string containing the configured Url for the LogOut page</returns>
        public static string GetLogoutPageUrl()
        {
            Hashtable configCustomAuthSection = WebConfigurationManager.GetWebApplicationSection("customAuthentication") as Hashtable;

            return configCustomAuthSection["logOutUrl"] as string;
        }//method: GetLogoutPageUrl();

        /// <summary>
        /// Gets the Login Page Url used for redirection during Forms Authentication
        /// </summary>
        /// <returns>string containing the configured Login Url for Forms Authentication</returns>
        public static string GetFormsAuthLoginUrl()
        {
            AuthenticationSection authSection = WebConfigurationManager.GetWebApplicationSection("system.web/authentication") as AuthenticationSection;

            return authSection.Forms.LoginUrl;
        }//method: GetFormsAuthLoginUrl

        /// <summary>
        /// Gets the Default redirect page for the RMX Home Page
        /// </summary>
        /// <returns>string containing the configured default Redirect page</returns>
        public static string GetDefaultPageUrl()
        {
            string strLoginUrl = string.Empty;
            strLoginUrl = GetFormsAuthLoginUrl();
            const string RELATIVE_PATHING = "~/";

            //Remove the relative pathing for the Url
            if (strLoginUrl.StartsWith(RELATIVE_PATHING))
            {
                strLoginUrl.Remove(0, RELATIVE_PATHING.Length);
            }//if

            return strLoginUrl;
        }//method: GetDefaultPageUrl()

        /// <summary>
        /// Gets the list of Riskmaster Business Adaptors used for Extensibility
        /// </summary>
        /// <exception cref="NullReferenceException" />
        /// <returns>RMAdaptorsCollection containing all 
        /// Riskmaster Business Adaptors required for dependency 
        /// injection by the Web Service</returns>
        //rsolanki2 : extensibility updates
        public static RMAdaptersCollection GetRMExtensibilityAdaptors()
        {
            RMExtensibilitySection rmExtensibilityAdaptorsSection = WebConfigurationManager.GetSection("Extensibility") as RMExtensibilitySection;

            if (rmExtensibilityAdaptorsSection == null)
            {
                throw new NullReferenceException();
            } // if

            return rmExtensibilityAdaptorsSection.RMExtensibilityAdapters;
        } // method: GetRMExtensibilityAdaptors

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static RMExtensibilityCollection GetRMExtensibilityForms()
        {
            RMExtensibilitySection rmExtensibilityFormsSection = WebConfigurationManager.GetSection("Extensibility") as RMExtensibilitySection;

            if (rmExtensibilityFormsSection == null)
            {
                throw new NullReferenceException();
            } // if

            return rmExtensibilityFormsSection.RMFormMapping;
        } // method: GetRMExtensibilityForms
    }


}