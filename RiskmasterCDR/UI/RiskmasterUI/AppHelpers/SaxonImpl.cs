﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using Riskmaster.Common;
namespace Riskmaster.AppHelpers
{
    public class SaxonImpl
    {
        private XmlDocument context = null;
        public SaxonImpl(XmlDocument doc)
        {
            context = doc;
        }
        public string concat(string s1, string s2)
        {
            return string.Concat(s1, s2);
        }
        public XmlNode evaluate(string expression)
        {
            // npadhy MITS 15514 Added code to handle the controls which do not have the Ref attribute associated with them.
            // By default it will give a blank node.
            XmlNode node = context.SelectSingleNode("//BlankNode");
            if (!string.IsNullOrEmpty(expression))
            {
                node = context.SelectSingleNode(expression);

                // npadhy MITS 15374 Added code to handle the ref for which the Context does not correspong to a node.
                if(node == null)
                    node = context.SelectSingleNode("//BlankNode");
            }
            return node;
        }
        /// <summary>
        /// Amandeep Kaur Added this function to set multi code lookup data without error code
        /// Date 24/12/2012
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public XmlNode evaluateCodes(string expression)
        {           
            XmlNode node = context.SelectSingleNode("//BlankNode");
            if (!string.IsNullOrEmpty(expression))
            {
                node = context.SelectSingleNode(expression);                         
                if (node == null)
                    node = context.SelectSingleNode("//BlankNode");
                node.InnerText = ErrorHelper.UpdateErrorMessage(node.InnerText);
            }
            return node;
        }
        /// <summary>
        /// Amandeep Kaur Added this function to show entity list lookup data without error code
        /// Date 24/12/2012
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public string getValue(string expression)
        {
            string nodeText = string.Empty;
            XmlNode node = context.SelectSingleNode("//BlankNode");
            if (!string.IsNullOrEmpty(expression))
            {
                node = context.SelectSingleNode(expression);
                if (node == null)
                    node = context.SelectSingleNode("//BlankNode");
                node.InnerText = ErrorHelper.UpdateErrorMessage(node.InnerText);
                nodeText = node.InnerText;
            }
            return nodeText;
        }
        /// <summary>
        /// rsushilaggar Added this function to show multi code lookup data in CSV format
        /// MITS 25351 Date 07/06/2011
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public string toCsv(string expression)
        {
            expression += "/Item";
            string sConcat = string.Empty;
            XmlNodeList nodes;
            if (!string.IsNullOrEmpty(expression))
            {
                nodes = context.SelectNodes(expression);
                if (nodes != null && nodes.Count > 0)
                {
                    foreach (XmlNode item in nodes)
                    {
                        sConcat += item.InnerText + ",";
                    }

                    sConcat = sConcat.Substring(0, sConcat.Length - 1);
                }
                
            }
            return sConcat;
        }
        public string formatdate(string year, string month, string day)
        {
            return string.Format("{0}/{1}/{2}", month, day, year);
        }

        public string formattime(string p_sDataValue)
        {
            XmlNode node = context.SelectSingleNode(p_sDataValue);
            if (node == null)
            {
                return string.Empty;
            }
            p_sDataValue = node.InnerText;
            p_sDataValue = AppHelper.GetUITime(p_sDataValue);
            return p_sDataValue;
        }
        public string formatdatetime(string p_sDataValue)
        {
            string sReturnString = Conversion.GetDBDTTMFormat(p_sDataValue, "d", "t");
            return sReturnString;
        }
        public string formatcurrency(string p_sDataValue)
        {  
            //rupal:start, r8 multicurrency
            string sCulture=System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            double dAmt =  Convert.ToDouble(p_sDataValue);
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(sCulture);
            return dAmt.ToString("c");
            //rupal:end
        }
        ~SaxonImpl()
        {
            context = null;
        }


    } 
}
