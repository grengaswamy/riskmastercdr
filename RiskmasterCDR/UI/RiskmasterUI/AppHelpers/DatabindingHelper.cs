﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using Riskmaster.UI.Shared.Controls;
using MultiCurrencyCustomControl;

namespace Riskmaster.AppHelpers
{
    public class DatabindingHelper
    {
        private const char m_GAValueDelimiter = '|';
        private const char m_GAFieldDelimiter = ';';
        private static string m_sCurrencyValue = string.Empty;//Deb Multi Currency
        private static string m_sCurrentControlType = "";
        //MITS 22793 : Password visible on viewing source
        private const string TEMP_DSPL_PWD = "*#$%$@$*!!*#$*";

        private static bool m_bFirst = false; //MITS 20077 hlv 9/17/12

        public static string CurrentControlType
        {
            get
            {
                return m_sCurrentControlType;
            }
            set
            {
                m_sCurrentControlType = value;
            }
        }
        //Deb Multi Currency
        public static string CurrencyValue
        {
            get
            {
                return m_sCurrencyValue;
            }
            set
            {
                m_sCurrencyValue = value;
            }
        }

        //MITS 20077 hlv 9/17/12 begin
        public static bool IsFirst
        {
            get
            {
                return m_bFirst;
            }
            set
            {
                m_bFirst = value;
            }
        }
        //MITS 20077 hlv 9/17/12 end

        //Deb Multi Currency
        /// <summary>
        /// Set control values to CWS request message
        /// </summary>
        /// <param name="oElement"></param>
        /// <param name="sRMXRef"></param>
        /// <param name="sValue"></param>
        public static void SetValue2Message(XElement oElement, string sRMXRef, string sValue)
        {
            SetValue2Message(oElement, sRMXRef, sValue, false);
        }


        /// <summary>
        /// Set control values to CWS request message
        /// </summary>
        /// <param name="oElement"></param>
        /// <param name="sRMXRef"></param>
        /// <param name="sValue"></param>
        /// <param name="bOverride">flag to indicate if the existing value should be overriden</param>
        public static void SetValue2Message(XElement oElement, string sRMXRef, string sValue, bool bOverride)
        {
            //Added by bhsarma33 - Pan testing
           // sValue = AppHelper.HTMLCustomEncode(sValue);
            //End additions by bhsarma33 - Pan testing
            string sAttributeName = String.Empty;
            //Raman Bhatia: Handle special scenario of creation of element of following type:
            //.Document/form/group/displaycolumn/control[@name ='Zip']
            //CreateMessageElement would normally fail in such a scenario
            if ((sRMXRef.EndsWith("]") && (sRMXRef.IndexOf('[') > 0) && (sRMXRef.IndexOf(']') > 0)))
            {
                string sAttributeString = sRMXRef.Substring(sRMXRef.IndexOf('['));
                sRMXRef = sRMXRef.Remove(sRMXRef.IndexOf('['));
                int iAttributeBeginPos = sAttributeString.IndexOf("'");
                int iAttributeEndPos = sAttributeString.LastIndexOf("'");
                sAttributeName = sAttributeString.Substring(iAttributeBeginPos + 1, (iAttributeEndPos - iAttributeBeginPos - 1)).Trim();
            }

            //Raman Bhatia: Handling special scenario as mentioned above
            //Check existance or create element
            XObject oTargetObject = null;
            if (sAttributeName != String.Empty)
                oTargetObject = CreateMessageElement(oElement, sRMXRef, sAttributeName);
            else
                oTargetObject = CreateMessageElement(oElement, sRMXRef);

            if (oTargetObject is XAttribute)
                ((XAttribute)oTargetObject).Value = sValue;
            else if (oTargetObject is XElement)
                //mjain8 added for the cases where there are more control referencing the same rmxref path. it will not allow to overwrite the value.Subject to change if it break any case. 
                //MGaba2:Commenting this condition.As this scenario should be handled specifically in the code behind of that page
                //if (string.IsNullOrEmpty(((XElement)oTargetObject).Value) || ((XElement)oTargetObject).Value == "0" || bOverride)
                {
                    ((XElement)oTargetObject).Value = sValue;
                    if (sAttributeName != String.Empty)
                    {
                        ((XElement)oTargetObject).SetAttributeValue("name", sAttributeName);
                        ((XElement)oTargetObject).SetAttributeValue("type", m_sCurrentControlType);
                        m_sCurrentControlType = "";

                    }
                }
            //Deb Multi Currency
            if (DatabindingHelper.CurrentControlType == "currency")
            {
                ((XElement)oTargetObject).SetAttributeValue("CurrencyValue",CurrencyValue);
            }
            //Deb Multi Currency
        }


        /// <summary>
        /// Dynamically create CWS request message element
        /// </summary>
        /// <param name="oElement"></param>
        /// <param name="sRMXRef"></param>
        /// <returns></returns>
        private static XObject CreateMessageElement(XElement oElement, string sRMXRef)
        {
            //Check if it's an attribute or not
            int index = sRMXRef.IndexOf("/@");
            string sElementPath = string.Empty;
            string sAttributeName = string.Empty;
            if (index > 0)
            {
                sElementPath = sRMXRef.Substring(0, index);
                sAttributeName = sRMXRef.Substring(index + 2);
            }
            else
                sElementPath = sRMXRef;

            //create the element if it does not exist
            XElement oParentElement = null;
            string sElementName = string.Empty;
            XObject oTargetObject = oElement.XPathSelectElement(sElementPath);
            if (oTargetObject == null)
            {
                index = sElementPath.LastIndexOf("/");
                if (index > 0)
                {
                    string sParentPath = sElementPath.Substring(0, index);
                    oParentElement = (XElement)CreateMessageElement(oElement, sParentPath);
                    sElementName = sElementPath.Substring(index + 1);
                    if (sElementName == "..")//Mona
                    {
                        return oParentElement.Parent;
                    }
                }
                else
                {
                    oParentElement = oElement;
                    sElementName = sElementPath;
                }

                //Raman Bhatia: Handling scenario where element has a conditional [@name = '..'] in the end
                if ((sElementName.EndsWith("]") && (sElementName.IndexOf('[') > 0) && (sElementName.IndexOf(']') > 0)))
                {
                    string sAttributeString = sElementName.Substring(sElementName.IndexOf('['));
                    sElementName = sElementName.Remove(sElementName.IndexOf('['));
                    int iAttributeBeginPos = 0;
                    int iAttributeEndPos = 0;
                    if (sAttributeString.Contains("'"))
                    {
                        iAttributeBeginPos = sAttributeString.IndexOf("'");
                        iAttributeEndPos = sAttributeString.LastIndexOf("'");
                    }
                    else if (sAttributeString.Contains("\""))
                    {
                        iAttributeBeginPos = sAttributeString.IndexOf("\"");
                        iAttributeEndPos = sAttributeString.LastIndexOf("\"");
                    }
                    string sTempAttributeName = sAttributeString.Substring(iAttributeBeginPos + 1, (iAttributeEndPos - iAttributeBeginPos - 1)).Trim();

                    oTargetObject = new XElement(sElementName);
                    //Mona:considering the case when there is a space between . and =
                    if (sAttributeString.IndexOf("[.") < 0)//Mona
                    {
                        ((XElement)oTargetObject).SetAttributeValue("name", sTempAttributeName);
                        ((XElement)oTargetObject).SetAttributeValue("type", m_sCurrentControlType);                        
                    }
                    else
                    {
                        ((XElement)oTargetObject).SetValue(sTempAttributeName);
                        
                        //Mona: Condition is to prevent making of an extra parent node in the xml 
                        //as for the first time node is already created.
                        if ((oParentElement.Parent.Nodes().Count() != 1) || (oParentElement.HasElements))
                        {
                            XElement oNewParentObject = new XElement(oParentElement.Name);
                            oParentElement.Parent.Add(oNewParentObject);
                            oParentElement = oNewParentObject;
                        }
                    }
                    m_sCurrentControlType = "";
                }
                else
                {
                    //Raman 07/08/2009: If Supplemental control is the first control on page then our logic would try to create "*" node in Instance
                    //as binding of supplementals is like /Instance/*/Supplementals
                    //in normal scenario when supplemental control comes then already controls exist in XML with xpath as /Instance/* hence it does not try to create this node
                    if (sElementName == "*")
                    {
                        XElement oSysFormNameEle = oElement.XPathSelectElement("./Document/ParamList/Param[@name='SysFormVariables']/FormVariables/SysFormName");
                        if (oSysFormNameEle != null)
                        {
                            sElementName = oSysFormNameEle.Value;
                            sElementName = sElementName.Substring(0, 1).ToUpper() + sElementName.Substring(1);
                        }
                    }
                    oTargetObject = new XElement(sElementName);
                }

                //MITS 20077 hlv 9/18/12 begin
                //oParentElement.Add(oTargetObject);
                if (m_bFirst)
                    oParentElement.AddFirst(oTargetObject);
                else //MITS 20077 hlv 9/18/12 end
                    oParentElement.Add(oTargetObject);
            }

            //if it's a attribute, add it to the element if it does not exist
            XAttribute oAttribute = null;
            if (!string.IsNullOrEmpty(sAttributeName))
            {
                oAttribute = ((XElement)oTargetObject).Attribute(sAttributeName);
                if (oAttribute == null)
                {
                    oAttribute = new XAttribute(sAttributeName, string.Empty);
                    ((XElement)oTargetObject).Add(oAttribute);
                }
                oTargetObject = oAttribute;
            }

            return oTargetObject;
        }

        /// <summary>
        /// Dynamically create CWS request message element
        /// </summary>
        /// <param name="oElement"></param>
        /// <param name="sRMXRef"></param>
        /// <returns></returns>
        private static XObject CreateMessageElement(XElement oElement, string sRMXRef , string sAttribute)
        {
            //Check if it's an attribute or not
            int index = sRMXRef.IndexOf("/@");
            string sElementPath = string.Empty;
            string sAttributeName = string.Empty;
            if (index > 0)
            {
                sElementPath = sRMXRef.Substring(0, index);
                sAttributeName = sRMXRef.Substring(index + 2);
            }
            else
                sElementPath = sRMXRef;

            //create the element if it does not exist
            XElement oParentElement = null;
            string sElementName = string.Empty;
            XObject oTargetObject = oElement.XPathSelectElement(sElementPath);
            if (oTargetObject == null)
            {
                index = sElementPath.LastIndexOf("/");
                if (index > 0)
                {
                    string sParentPath = sElementPath.Substring(0, index);
                    oParentElement = (XElement)CreateMessageElement(oElement, sParentPath);
                    sElementName = sElementPath.Substring(index + 1);
                }
                else
                {
                    oParentElement = oElement;
                    sElementName = sElementPath;
                }

                oTargetObject = new XElement(sElementName);
                oParentElement.Add(oTargetObject);
            }

            //if it's a attribute, add it to the element if it does not exist
            XAttribute oAttribute = null;
            if (!string.IsNullOrEmpty(sAttributeName))
            {
                oAttribute = ((XElement)oTargetObject).Attribute(sAttributeName);
                if (oAttribute == null)
                {
                    oAttribute = new XAttribute(sAttributeName, string.Empty);
                    ((XElement)oTargetObject).Add(oAttribute);
                }
                oTargetObject = oAttribute;
            }

            //Check whether element is same in special scenario of /displaycolumn/control[@name='...']
            oAttribute = ((XElement)oTargetObject).Attribute("name");
            if (oAttribute != null)
            {
                if (oAttribute.Value != sAttribute)
                {
                    index = sElementPath.LastIndexOf("/");
                    if (index > 0)
                    {
                        string sParentPath = sElementPath.Substring(0, index);
                        oParentElement = (XElement)CreateMessageElement(oElement, sParentPath);
                        sElementName = sElementPath.Substring(index + 1);
                    }
                    else
                    {
                        oParentElement = oElement;
                        sElementName = sElementPath;
                    }

                    oTargetObject = new XElement(sElementName);
                    oParentElement.Add(oTargetObject);
                }
            }
            return oTargetObject;
        }

        /// <summary>
        /// Get value from server side controls
        /// </summary>
        /// <param name="oControl"></param>
        /// <returns></returns>
        public static string GetControlValue(WebControl oControl, Hashtable oRadioValues)
        {
            string sValue = string.Empty;

            Type controlType = oControl.GetType();
            string sType = controlType.ToString();
            int index = sType.LastIndexOf(".");
            sType = sType.Substring(index + 1);
            switch (sType)
            {
                case "TextBox":
                    bool bCtrlStatus = ((TextBox)oControl).ReadOnly;
                    if (bCtrlStatus)
                    {
                        string sRMXType = oControl.Attributes["RMXType"];                               
                        if (sRMXType == null || sRMXType != "code")
                        {
                            //rsolanki2: mits 24385: the Asp.net controls which were hidden were getting overwritten with null values in the db.
                            TextBox objTextBox = oControl as TextBox;

                            //MGaba2:MITS 24371:Start
                            //MITS 22793 : Password visible on viewing source
                            //Temp password was updating in database in case password policy is ON
                            if (oControl.Attributes["type"] != null
                                && oControl.Attributes["type"].ToString() == "password")
                            {                                
                                if (oControl.Attributes["tempuserpwd"] != null)
                                {
                                    if (String.Compare(TEMP_DSPL_PWD, HttpContext.Current.Request.Form[oControl.ID]) != 0)
                                    {
                                        sValue = HttpContext.Current.Request.Form[oControl.ID];
                                    }
                                    else
                                    {
                                        sValue = oControl.Attributes["tempuserpwd"].ToString();
                                    }
                                }  
                                else
                                {
                                    sValue = HttpContext.Current.Request.Form[oControl.ID];
                                }
                            }
                            //rsolanki2: mits 24385: the Asp.net controls which were hidden were getting overwritten with null values in the db.
                            else if (!objTextBox.Visible)
                            {
                                sValue = objTextBox.Text;
                            }
                            else
                            {//MGaba2:MITS 24371:End
                                if (sRMXType != "date")//jira 6415
                                    sValue = HttpContext.Current.Request.Form[oControl.ID];
                                else
                                    sValue = objTextBox.Text;
                            }
                        }
                        else
                        {
                            string sControlId = oControl.ClientID;
                            sControlId = sControlId.Replace('_', '$');
                            sValue = HttpContext.Current.Request.Form[sControlId];
                        }
                        //Mjain8 There are some cases where read only controls are removed for ex. Pi other>entitytableidtextname in onupdateform so it returns null and raise an exception
                        if (sValue == null)
                            sValue = "";
                    }
                    else
                    {
                        if (oControl.Attributes["type"] != null
                            && oControl.Attributes["type"].ToString() == "password")
                        {
                            //MITS 22793 : Password visible on viewing source
                            if (oControl.Attributes["tempuserpwd"] != null)
                            {
                                if (String.Compare(TEMP_DSPL_PWD, ((TextBox)oControl).Text) != 0)
                                {
                                    sValue = ((TextBox)oControl).Text;
                                }
                                else
                                {
                                    sValue = oControl.Attributes["tempuserpwd"].ToString();
                                }


                            }
                            else if (oControl.Attributes["tempdsnpwd"] != null)
                            {
                                if (String.Compare(TEMP_DSPL_PWD, ((TextBox)oControl).Text) != 0)
                                {
                                    sValue = ((TextBox)oControl).Text;
                                }
                                else
                                {
                                    sValue = oControl.Attributes["tempdsnpwd"].ToString();
                                }


                            }
                                
                            else
                            {
                                sValue = ((TextBox)oControl).Text;
                            }

                        }
                        else
                        {
                            sValue = ((TextBox)oControl).Text;
                        }
                        
                    }
                        
                    break;
                case "CheckBox":
                    if ((oControl.Attributes["FormType"] != null && oControl.Attributes["FormType"].ToString() == "Customize") ||
                    (oControl.ID != null && oControl.ID.StartsWith("supp_") == true) || ////MITS # 29793 - 20120926 - bsharma33 - BR Id # 5.1.22 
                    (oControl.Attributes["RMXRef"] != null && oControl.Attributes["RMXRef"].Contains("ADMTable"))) //MITS 37598 - Exception when creating new AT record which uses check boxes fields
                    {
                        sValue = "0";
                        if (((CheckBox)oControl).Checked)
                        {
                            sValue = "-1";
                        }
                    }
                    else
                    {
                        sValue = "False";
                        if (((CheckBox)oControl).Checked)
                        {
                            sValue = "True";
                        }
                    }
                    break;
                case "RadioButton":
                    string sGroupName = ((RadioButton)oControl).GroupName;
                    string sControlValue = oControl.Attributes["value"].ToString();
                    if (((RadioButton)oControl).Checked == false && oControl.Attributes["group"] != null)
                    {
                        sValue = sControlValue;
                    }
                    //if (oRadioValues.Contains(sGroupName))
                    if (oRadioValues.Contains(sGroupName) && oControl.Attributes["group"]==null)
                    {
                        sValue = oRadioValues[sGroupName].ToString();
                    }
                    else if (((RadioButton)oControl).Checked)
                    {
                        sValue = sControlValue;
                        oRadioValues.Add(sGroupName, sControlValue);
                    }
                    break;
                // npadhy Added Code for saving the Combobox
                case "DropDownList":
                    sValue = ((DropDownList)oControl).SelectedValue;
                    break;
                case "Label":
                    sValue = ((Label)oControl).Text;
                    break;
                case "RadioButtonList":
                    sValue = ((RadioButtonList)oControl).SelectedValue;
                    break;
                case "ComboBox"://MGaba2:R7:Added case for Ajax Combo used on Purge History PopUp
                    sValue = ((AjaxControlToolkit.ComboBox)oControl).SelectedValue;
                    break;
                //Deb: Commented below code as they create problem while creating request xml  
                //case "ListBox":
                //    sValue = ((ListBox)oControl).SelectedValue;
                //    break;
                case "CurrencyTextbox":
                    sValue = ((CurrencyTextbox)oControl).AmountInString;
                    CurrencyValue = ((CurrencyTextbox)oControl).Text;
                    break;
                case "ListBox":
                    var selected = ((ListBox)oControl).GetSelectedIndices().ToList();
                    var selectedValues = (from c in selected
                                          select ((ListBox)oControl).Items[c].Value).ToList();
                    foreach (string sValue1 in selectedValues)
                    {
                        sValue = sValue + sValue1+ "|^|";
                    }
                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                case "HyperLink":
                    sValue = ((HyperLink)oControl).NavigateUrl;
                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            }

            //Format the value to be displayed
            string sFormatAs = oControl.Attributes["FormatAs"];
            switch (sFormatAs)
            {
                case "date":
                    sValue = AppHelper.GetDateInenUS(sValue);
                    break;
                case "time":
                    //sValue = GetRMTime(sValue);
                    break;
            }

            return sValue;
        }

        /// <summary>
        /// Set value for server side control
        /// </summary>
        /// <param name="oControl"></param>
        /// <param name="sValue"></param>
        public static void SetValue2Control(WebControl oControl, string sValue)
        {
            //sValue = HttpContext.Current.Server.UrlDecode(sValue);
            string sFormatAs = oControl.Attributes["FormatAs"];
            switch (sFormatAs)
            {
                case "date":
					 /* rkulavil : RMA-4726/MITS 37305 - start */
                    //sValue = AppHelper.GetUIDate(sValue);
                    sValue = AppHelper.GetDate(sValue);
					 /* rkulavil : RMA-4726/MITS 37305 - end */
                    break;
                case "time":
					 /* rkulavil : RMA-4726/MITS 37305 - start */
                    //sValue = AppHelper.GetUITime(sValue);
                    sValue = AppHelper.GetTime(sValue);
					 /* rkulavil : RMA-4726/MITS 37305 - end */
                    break;
                case "datetime":
                    sValue = AppHelper.GetUIDatetTime(sValue);
                    break;

            }

            Type controlType = oControl.GetType();
            string sType = controlType.ToString();
            int index = sType.LastIndexOf(".");
            sType = sType.Substring(index + 1);
            switch (sType)
            {
                case "TextBox":
                    ((TextBox)oControl).Text = ErrorHelper.UpdateErrorMessage(sValue);   //Aman ML Change 
                    if (oControl.Attributes["RMXType"] != null
                        && oControl.Attributes["RMXType"].ToString() == "currency")
                    {
                        double dValue = 0.0;
                        if (sValue != string.Empty && Double.TryParse(sValue, out dValue))
                            ((TextBox)oControl).Text = String.Format("{0:C}", dValue);
                    }
                    if (oControl.Attributes["type"] != null
                        && oControl.Attributes["type"].ToString() == "password")
                    {
                        //MITS 22793 : Password visible on viewing source
                        if ((oControl.Attributes["tempuserpwd"] != null || oControl.Attributes["tempdsnpwd"] != null) && sValue != "")
                        {
                            ((TextBox)oControl).Attributes.Add("value", TEMP_DSPL_PWD);
                            //((TextBox)oControl).Attributes.Add("value", sValue);
                        }
                    }
                    //Start by Shivendu for MITS 19296
                    if (oControl.Attributes["RMXType"] != null
                        && (string.Compare(oControl.Attributes["RMXType"].ToString(), "ssn") == 0
                        || string.Compare(oControl.Attributes["RMXType"].ToString(), "taxid") == 0))
                    {
                        if (!string.IsNullOrEmpty(sValue))
                        {
                            bool bValidFormat = false;
                            sValue = AppHelper.GetUISSNTaxID(sValue, oControl.Attributes["RMXType"].ToString(), out bValidFormat);
                            if (bValidFormat)
                            {
                                
                                ((TextBox)oControl).Attributes.Remove("style");
                                ((TextBox)oControl).Attributes.Add("style", "background-color:White;");
                            }
                            else
                            {
                                ((TextBox)oControl).Attributes.Remove("style");
                                ((TextBox)oControl).Attributes.Add("style", "background-color:#FF9999;");

                                
                            }
                            ((TextBox)oControl).Text = sValue;
                        }
                        
                    }
                    //End by Shivendu for MITS 19296
                    break;
                case "CheckBox":
                    if (sValue.ToLower() == "true" ||sValue.ToLower() == "-1" || sValue.ToLower() == "1"
                        //sgoel6 MITS 16899
                        || sValue.ToLower() == "yes")
                        ((CheckBox)oControl).Checked = true;
                    else
                        ((CheckBox)oControl).Checked = false;
                    break;
                case "RadioButton":
                    if (oControl.Attributes["value"] != null)
                    {
                        string sAttVal = oControl.Attributes["value"].ToString();
                        if (sValue.ToLower() == sAttVal.ToLower())
                            ((RadioButton)oControl).Checked = true;
                        else
                            ((RadioButton)oControl).Checked = false;
                    }
                    else
                    {
                        ((RadioButton)oControl).Checked = false;
                    }
                    break;
                case "ListBox":
                    ListBox oLBControl = (ListBox)oControl;
                    if (!string.IsNullOrEmpty(sValue))
                    {
                        XElement oLB = XElement.Parse(sValue);
                        if (oLB.HasElements)
                        {
                            while (oLBControl.Items.Count > 0)
                                oLBControl.Items.RemoveAt(0);
                        }

                        foreach (XElement oEle in oLB.Elements())
                        {
                            string sText = ErrorHelper.UpdateErrorMessage(oEle.Value);
                            string sCode = oEle.Attribute("value").Value;
                            //MGaba2:MITS 23158:Adding tooltip to multicode
                            ListItem objLI = new ListItem(sText, sCode);
                            objLI.Attributes["title"] = sText;
                            oLBControl.Items.Add(objLI);
                        }
                    }
                    else
                    {
                        while (oLBControl.Items.Count > 0)
                            oLBControl.Items.RemoveAt(0);
                    }
                    break;
                case "Button":
                    Button oButton = (Button)oControl;
                    string sButonText = oButton.Text;
                    //Raman Bhatia: We need to append count of children in form buttons
                    if (sButonText.Contains("(0)"))
                    {
                        sButonText = sButonText.Replace("(0)", ('(' + sValue + ')'));
                    }
                    else if (sButonText.Contains('(') && sButonText.Contains(')'))
                    {
                        sButonText = sButonText.Remove(sButonText.IndexOf('(')) + '(' + sValue + ')';
                    }
                    else
                    {
                        sButonText = sButonText + '(' + sValue + ')';
                    }

                    oButton.Text = sButonText;
                    break;
                case "DropDownList"://Parijat: Jurisdictionals- Inorder to make SetValue2Control a generic functionality
                    DropDownList oLBControl1 = (DropDownList)oControl;
                    oLBControl1.SelectedIndex = oLBControl1.Items.IndexOf(oLBControl1.Items.FindByValue(sValue));
                    break;
                case "Label":
                    Label lblControl1 = (Label)oControl;

                    if (oControl.Attributes["DataRunTime"] != null && oControl.Attributes["DataRunTime"].ToString() == "true")
                    {
                        if (oControl.Attributes["StaticValue"] != null && oControl.Attributes["StaticValue"].ToString() != "")
                        {
                            lblControl1.Text = oControl.Attributes["StaticValue"].ToString() + "  " + sValue;
                        }
                        else
                        {
                            lblControl1.Text = sValue;
                        }
                    }
                    else
                        lblControl1.Text += sValue;
                    break;
                case "ComboBox"://MGaba2:R7: Added case for Ajax Combo used on Purge History PopUp
                    AjaxControlToolkit.ComboBox cbControl = (AjaxControlToolkit.ComboBox)oControl;
                    cbControl.SelectedIndex = cbControl.Items.IndexOf(cbControl.Items.FindByValue(sValue));
                    break;
                case "CurrencyTextbox":
                    ((CurrencyTextbox)oControl).AmountInString = sValue;
                    break;
                //Indu: Added the below lines of code for Mits 28937(Moving bottom down button to Toolbar)
                case "ImageButton":
                    ImageButton oIButton = (ImageButton)oControl;
                    string sIButonText = string.Empty;
                    sIButonText = oIButton.ToolTip;
                    //sIButonText = sIButonText + '(' + sValue + ')';


                    if (sIButonText.Contains("(0)"))
                    {
                        sIButonText = sIButonText.Replace("(0)", ('(' + sValue + ')'));
                    }
                    else if (sIButonText.Contains('(') && sIButonText.Contains(')'))
                    {
                        sIButonText = sIButonText.Remove(sIButonText.IndexOf('(')) + '(' + sValue + ')';
                    }
                    else
                    {
                        sIButonText = sIButonText + '(' + sValue + ')';
                    }
                    oIButton.ToolTip = sIButonText;

                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                case "HyperLink":
                    ((HyperLink)oControl).NavigateUrl = sValue;
                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            }
        }

        /// <summary>
        /// Set value for server side control. 
        /// Overrides the SetValue2Control which assigns the value to the control
        /// </summary>
        /// <param name="oControl"> The Control which has to be populated with the value.</param>
        /// <param name="sSelectedValue"> The selected item for the combobox</param>
        /// <param name="sValueCollection"> The collection for the combox to be populated</param>
        public static void SetValue2Control(WebControl oControl, string sSelectedValue, string sValueCollection)
        {
            Type controlType = oControl.GetType();
            string sType = controlType.ToString();
            int index = sType.LastIndexOf(".");
            sType = sType.Substring(index + 1);
            string[] sArr={};
            string[] sDataOperator = {","};
            ListItem oItem;
            switch (sType)
            {
                case "DropDownList":
                    if (!string.IsNullOrEmpty(sValueCollection) && sValueCollection != "RETAINRMXVALUE")
                    {
                        XElement oLB = XElement.Parse(sValueCollection);
                        DropDownList oLBControl = (DropDownList)oControl;
                        if (oLB.HasElements)
                        {
                            while (oLBControl.Items.Count > 0)
                                oLBControl.Items.RemoveAt(0);
                        }
                        foreach (XElement oEle in oLB.Elements())
                        {
                            string sText = ErrorHelper.UpdateErrorMessage(oEle.Value);
                            string sCode = "";
                            if(oEle.Attribute("value")!=null)
                                sCode = oEle.Attribute("value").Value;
                            else
                                sCode = "";
                            if (string.IsNullOrEmpty(sCode))
                                sCode = "0";
                            oLBControl.Items.Add(new ListItem(sText, sCode));
                        }

                        if (string.IsNullOrEmpty(sSelectedValue) || sSelectedValue == "RETAINRMXVALUE")
                            sSelectedValue = "0";

                        oItem = oLBControl.Items.FindByValue(sSelectedValue);
                        if (oItem != null)
                            oItem.Selected = true;
                    }
                    break;
                case "ListBox":
                    if (!string.IsNullOrEmpty(sValueCollection) && sValueCollection != "RETAINRMXVALUE")
                    {
                        XElement oLB = XElement.Parse(sValueCollection);
                        ListBox oLBControl = (ListBox)oControl;
                        if (oLB.HasElements)
                        {
                            while (oLBControl.Items.Count > 0)
                                oLBControl.Items.RemoveAt(0);
                        }
                        foreach (XElement oEle in oLB.Elements())
                        {
                            string sText = ErrorHelper.UpdateErrorMessage(oEle.Value);
                            string sCode = "";
                            if (oEle.Attribute("value") != null)
                                sCode = oEle.Attribute("value").Value;
                            else
                                sCode = "";
                            if (string.IsNullOrEmpty(sCode))
                                sCode = "0";
                            oLBControl.Items.Add(new ListItem(sText, sCode));
                        }

                        if (string.IsNullOrEmpty(sSelectedValue) || sSelectedValue == "RETAINRMXVALUE")
                            sSelectedValue = "0";
                            sArr = sSelectedValue.Split(sDataOperator, StringSplitOptions.None);
                            for (int i = 0; i < sArr.Length; i++)
                            {
                                oItem = oLBControl.Items.FindByValue(sArr[i]);
                                if (oItem != null)
                                    oItem.Selected = true;
                            }

                            oItem = oLBControl.Items.FindByValue(sSelectedValue);
                        if (oItem != null)
                            oItem.Selected = true;
                    }
                    break;
            }
        }

        /// <summary>
        /// Adds a Linq element to the existing element
        /// Used for Grid, while creating the Grid XML for saving.
        /// </summary>
        /// <param name="oParentElement"> The existing parent Element to which the Linq element is added</param>
        /// <param name="sRMXRef"> The path where the child element is added to parent</param>
        /// <param name="oChildElement"> The Child element which is added to parent</param>
        public static void SetObject2Message(XElement oParentElement, string sRMXRef, XElement oChildElement)
        {
            //Check existance or create element
            XObject oTargetObject = CreateMessageElement(oParentElement, sRMXRef);
            if (oChildElement != null)
            {
                // oTargetObject.Document.Add(oChildElement);
                ((XElement)oTargetObject).Add(oChildElement);
            }
        }

        public static void UpdateCustomizedControls(XElement oMessageElement, string sRMXType, Control oCtrl)
        {
            XElement objEle = null;

            try
            {
                switch (sRMXType)
                {
                    case "htmltext":
                        //asharma326 JIRA 6422 for supp HTML text
                        if (((TextBox)oCtrl).Attributes["fieldtype"] != null
                            && (!string.IsNullOrEmpty(((TextBox)oCtrl).Attributes["fieldtype"]))
                            && ((((TextBox)oCtrl).Attributes["fieldtype"]).Equals("configurablesupphtmlfield")))
                        {
                            objEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/MemoCustomizations/HtmlText");
                            if (objEle != null)
                            {
                                ((TextBox)oCtrl).Rows = Convert.ToInt32(objEle.Attribute("rows").Value);
                                ((TextBox)oCtrl).Columns = Convert.ToInt32(objEle.Attribute("cols").Value);

                            }
                        }
                        break;
                    case "memo":
                        objEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/MemoCustomizations/Memo");
                        if (objEle != null)
                        {
                            ((TextBox)oCtrl).Rows = Convert.ToInt32(objEle.Attribute("rows").Value);
                            ((TextBox)oCtrl).Columns = Convert.ToInt32(objEle.Attribute("cols").Value);

                        }
                        break;

                    case "textml":
                        objEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/MemoCustomizations/TextML");
                        if (objEle != null)
                        {
                            ((TextBox)oCtrl).Rows = Convert.ToInt32(objEle.Attribute("rows").Value);
                            ((TextBox)oCtrl).Columns = Convert.ToInt32(objEle.Attribute("cols").Value);

                        }
                        break;
                    case "readonlymemo":
                        objEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/MemoCustomizations/ReadOnlyMemo");
                        if (objEle != null)
                        {
                            ((TextBox)oCtrl).Rows = Convert.ToInt32(objEle.Attribute("rows").Value);
                            ((TextBox)oCtrl).Columns = Convert.ToInt32(objEle.Attribute("cols").Value);

                        }
                        break;

                    case "freecode":
                        objEle = oMessageElement.XPathSelectElement("./ParamList/Param[@name='SysFormVariables']/FormVariables/SysExData/MemoCustomizations/FreeCode");
                        if (objEle != null)
                        {
                            ((TextBox)oCtrl).Rows = Convert.ToInt32(objEle.Attribute("rows").Value);
                            ((TextBox)oCtrl).Columns = Convert.ToInt32(objEle.Attribute("cols").Value);

                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Dynamically hide the nodes from the Page
        /// </summary>
        /// <param name="oMessageElement"></param>
        public static void HideNodes(XElement oSysHideNodes, Page oPage )
        {
            string[] sHideNodes = new String[50];
            Control ctrlTemp = null;
            bool bFound = false;
            try
            {
                if (oSysHideNodes != null)
                {
                    sHideNodes = oSysHideNodes.Value.Split('|');
                }
                foreach (string sCurrentNode in sHideNodes)
                {
                    bFound = false;

                    if (string.IsNullOrEmpty(sCurrentNode))
                    {
                        continue;
                    }

                    ctrlTemp = oPage.FindControl("div_" + sCurrentNode);
                    if (ctrlTemp != null)
                    {
                        ctrlTemp.Visible = false;
                        bFound = true;
                    }
                    else //This may be a Tab which needs to removed
                    {
                        ctrlTemp = oPage.FindControl("FORMTAB" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = false;
                        }
                        ctrlTemp = oPage.FindControl("TABS" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = false;
                        }
                        ctrlTemp = oPage.FindControl("TBSP" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = false;
                        }
                        //Start Ramkumar MITS 33606
                        ctrlTemp = oPage.FindControl("TBD" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = false;
                        }
                        //End Ramkumar MITS 33606
                    }

                    //If not a div and tab, it could be a stand alone control
                    if (bFound == false)
                    {
                        ctrlTemp = oPage.FindControl(sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = false;
                        }
                        ctrlTemp = oPage.FindControl(sCurrentNode+"_Bottom");
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Dynamically show the nodes from the Page
        /// </summary>
        /// <param name="oMessageElement"></param>
        public static void ShowNodes(XElement oSysVisibleNodes, Page oPage)
        {
            string[] sVisibleNodes = new String[50];
            Control ctrlTemp = null;
            bool bFound = false;

            try
            {
                if (oSysVisibleNodes != null)
                {
                    sVisibleNodes = oSysVisibleNodes.Value.Split('|');
                }
                foreach (string sCurrentNode in sVisibleNodes)
                {
                    bFound = false;

                    if (string.IsNullOrEmpty(sCurrentNode))
                    {
                        continue;
                    }

                    ctrlTemp = oPage.FindControl("div_" + sCurrentNode);
                    if (ctrlTemp != null)
                    {
                        bFound = true;
                        ctrlTemp.Visible = true;
                    }
                    else //This may be a Tab which needs to removed
                    {
                        ctrlTemp = oPage.FindControl("FORMTAB" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = true;
                        }
                        ctrlTemp = oPage.FindControl("TABS" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = true;
                        }
                        ctrlTemp = oPage.FindControl("TBSP" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = true;
                        }
                        //Start Ramkumar MITS 33606
                        ctrlTemp = oPage.FindControl("TBD" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            bFound = true;
                            ctrlTemp.Visible = true;
                        }
                        //End Ramkumar MITS 33606
                    }

                    //If not a div and tab, it could be a stand alone control
                    if (bFound == false)
                    {
                        ctrlTemp = oPage.FindControl(sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Dynamically display all the display nodes in the Page
        /// These nodes are usually controls which have been hidden by killednodes and are now needed to be displayed back
        /// </summary>
        /// <param name="oMessageElement"></param>
        public static void UpdateDisplayNodes(XElement oSysDisplayNodes, Page oPage)
        {
            string[] sDisplayNodes = new String[50];
            Control ctrlTemp = null;
            try
            {
                if (oSysDisplayNodes != null)
                {
                    sDisplayNodes = oSysDisplayNodes.Value.Split('|');
                }
                foreach (string sCurrentNode in sDisplayNodes)
                {
                    ctrlTemp = oPage.FindControl("div_" + sCurrentNode);
                    if (ctrlTemp != null)
                    {
                        ctrlTemp.Visible = true;
                    }
                    else //This may be a Tab which needs to displayed
                    {
                        ctrlTemp = oPage.FindControl("FORMTAB" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = true;
                        }
                        ctrlTemp = oPage.FindControl("TABS" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = true;
                        }
                        ctrlTemp = oPage.FindControl("TBSP" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = true;
                        }
                        //Start Ramkumar MITS 33606
                        ctrlTemp = oPage.FindControl("TBD" + sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = true;
                        }
                        //End Ramkumar MITS 33606   
                        //Bharani - MITS : - Start
                        //(For Bottom Down Buttons to Toolbar Buttons)
                        ctrlTemp = oPage.FindControl(sCurrentNode);
                        if (ctrlTemp != null)
                        {
                            ctrlTemp.Visible = true;
                        }
                        //Bharani - MITS : - End
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Disable the specified control in the page
        /// </summary>
        /// <param name="sControlId"></param>
        /// <param name="oPage"></param>
        public static void DisableControls(string sControlId, Page oPage)
        {
            Control oControl = null;
            Control divControl = null;

            oControl = oPage.FindControl(sControlId);
            divControl = oPage.FindControl("div_" + sControlId);

            if (divControl != null)
            {
                foreach (Control ctl in divControl.Controls)
                {
                    if (ctl is LiteralControl)//deb : MITS 20003
                    {
                        if (((LiteralControl)ctl).Text.Trim().IndexOf("button") != -1)
                            ctl.Visible = false;                        
                    }    
                    else
                        DisableControl(ctl);
                }
            }
            else if (oControl != null)
            {
                DisableControl(oControl);
            }
        }

        /// <summary>
        /// disable one control
        /// </summary>
        /// <param name="oControl"></param>
        public static void DisableControl(Control oControl)
        {
            switch (oControl.GetType().ToString())
            {
                case "ASP.ui_shared_controls_codelookup_ascx":
                    ((CodeLookUp)oControl).Enabled = false;
                    break;
                case "ASP.ui_shared_controls_multicodelookup_ascx":
                    ((MultiCodeLookUp)oControl).Enabled = false;
                    break;
                case "ASP.ui_shared_controls_usercontroldatagrid_ascx":
                    ((UserControlDataGrid)oControl).Readonly = true;
                    break;
                case "System.Web.UI.WebControls.TextBox":
                    ((TextBox)oControl).ReadOnly = true;
                    ((TextBox)oControl).Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                    break;
                case "System.Web.UI.WebControls.ImageButton":
                    ((ImageButton)oControl).Enabled = false;
                    break;
                case "System.Web.UI.WebControls.Button":
                    ((Button)oControl).Enabled = false;
                    break;
                case "System.Web.UI.WebControls.CheckBox":
                    ((CheckBox)oControl).Enabled = false;
                    break;
                case "System.Web.UI.WebControls.DropDownList":
                    ((DropDownList)oControl).Enabled = false;
                    break;
                case "System.Web.UI.WebControls.RadioButton":
                    ((RadioButton)oControl).Enabled = false;
                    break;
                case "ASP.ui_shared_controls_systemusers_ascx"://deb : MITS 20003
                    ((SystemUsers)oControl).Enabled = false;
                    break;
                case "MultiCurrencyCustomControl.CurrencyTextbox":
                    ((CurrencyTextbox)oControl).Enabled = false; //rupal
                    //((CurrencyTextbox)oControl).ReadOnly = true;
                    break;
            }
        }


        /// <summary>
        /// enable one control
        /// </summary>
        /// <param name="oControl"></param>
        public static void EnableControl(Control oControl)
        {
            switch (oControl.GetType().ToString())
            {
                case "ASP.ui_shared_controls_codelookup_ascx":
                    if (((CodeLookUp)oControl).Attributes["PowerViewReadOnly"] == null
                        || (((CodeLookUp)oControl).Attributes["PowerViewReadOnly"] != null
                            && ((CodeLookUp)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((CodeLookUp)oControl).Enabled = true;
                    }
                    break;
                case "ASP.ui_shared_controls_multicodelookup_ascx":
                    if (((MultiCodeLookUp)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((MultiCodeLookUp)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((MultiCodeLookUp)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((MultiCodeLookUp)oControl).Enabled = true;
                        ((MultiCodeLookUp)oControl).Attributes.Remove("BackgroundColor");//deb : MITS 20003
                    }
                    break;
                case "ASP.ui_shared_controls_usercontroldatagrid_ascx":
                    if (((UserControlDataGrid)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((UserControlDataGrid)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((UserControlDataGrid)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((UserControlDataGrid)oControl).Readonly = false;
                    }
                    break;
                case "System.Web.UI.WebControls.TextBox":
                    if (((TextBox)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((TextBox)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((TextBox)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        if (((WebControl)oControl).Attributes["rmxtype"] != "memo" && ((WebControl)oControl).Attributes["rmxtype"] != "policylookup")//deb
                        {
                            ((TextBox)oControl).ReadOnly = false;
                            //((TextBox)oControl).Style.Add(HtmlTextWriterStyle.BackgroundColor, "White");
                            ((TextBox)oControl).Style.Add(HtmlTextWriterStyle.BackgroundColor, "");//deb : MITS 20003
                        }
                    }
                    break;
                case "System.Web.UI.WebControls.ImageButton":
                    if (((ImageButton)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((ImageButton)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((ImageButton)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((ImageButton)oControl).Enabled = true;
                    }
                    break;
                case "System.Web.UI.WebControls.Button":
                    if (((Button)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((Button)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((Button)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((Button)oControl).Enabled = true;
                    }
                    break;
                case "System.Web.UI.WebControls.CheckBox":
                    if (((CheckBox)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((CheckBox)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((CheckBox)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((CheckBox)oControl).Enabled = true;
                    }
                    break;
                case "System.Web.UI.WebControls.DropDownList":
                    if (((DropDownList)oControl).Attributes["PowerViewReadOnly"] == null
                        || (((DropDownList)oControl).Attributes["PowerViewReadOnly"] != null
                            && ((DropDownList)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))   //asharma326 adding powerview attribute condition for checking in case control is disabled from powerview.
                        ((DropDownList)oControl).Enabled = true;
                    break;
                case "System.Web.UI.WebControls.RadioButton":
                    if (((RadioButton)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((RadioButton)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((RadioButton)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((RadioButton)oControl).Enabled = true;
                    }
                    break;
                case "ASP.ui_shared_controls_systemusers_ascx"://deb : MITS 20003
                    if (((SystemUsers)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((SystemUsers)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((SystemUsers)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((SystemUsers)oControl).Enabled = true;
                    }
                    break;
                case "MultiCurrencyCustomControl.CurrencyTextbox":
                    if (((CurrencyTextbox)oControl).Attributes["PowerViewReadOnly"] == null
                       || (((CurrencyTextbox)oControl).Attributes["PowerViewReadOnly"] != null
                           && ((CurrencyTextbox)oControl).Attributes["PowerViewReadOnly"].ToLower() == "false"))
                    {
                        ((CurrencyTextbox)oControl).Enabled = true;
                        //((CurrencyTextbox)oControl).ReadOnly = false; //rupal:multicurrency
                    }
                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
                case "System.Web.UI.WebControls.HyperLink":
                    ((HyperLink)oControl).Enabled = true;
                    break;
                //WWIG GAP20A - agupta298 - MITS 36804 - JIRA - 4691
            }
        }

        /// <summary>
        /// Enable the specified control in the page
        /// </summary>
        /// <param name="sControlId"></param>
        /// <param name="oPage"></param>
        public static void EnableControls(string sControlId, Page oPage)
        {
            Control oControl = null;
            Control divControl = null;

            oControl = oPage.FindControl(sControlId);
            divControl = oPage.FindControl("div_" + sControlId);

            if (divControl != null)
            {
                foreach (Control ctl in divControl.Controls)
                {
                    EnableControl(ctl);
                }
            }
            else if (oControl != null)
            {
                EnableControl(oControl);
            }
        }

        /// <summary>
        /// Dynamically update controls marked as readonly nodes 
        /// </summary>
        /// <param name="oMessageElement"></param>
        public static void UpdateReadOnlyControls(XElement oSysReadOnlyNodes, Page oPage)
        {
            string[] sReadOnlyNodes = new String[50];

            try
            {
                if (oSysReadOnlyNodes != null)
                {
                    sReadOnlyNodes = oSysReadOnlyNodes.Value.Split('|');
                }
                // Changed the Implementation of this Function as UpdateControlAttributes is also disabling the controls.
                // So removed the logic of disabling controls from here and putting in function
                foreach (string sCurrentNode in sReadOnlyNodes)
                {
                    if(!string.IsNullOrEmpty(sCurrentNode))
                        DisableControls(sCurrentNode, oPage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Dynamically update controls marked as readwrite nodes 
        /// </summary>
        /// <param name="oMessageElement"></param>
        public static void UpdateReadWriteControls(XElement oSysReadWriteNodes, Page oPage)
        {
            string[] sReadWriteNodes = new String[50];

            try
            {
                if (oSysReadWriteNodes != null)
                {
                    sReadWriteNodes = oSysReadWriteNodes.Value.Split('|');
                }
                // Changed the Implementation of this Function as UpdateControlAttributes is also enabling the controls.
                // So removed the logic of enabling controls from here and putting in function
                foreach (string sCurrentNode in sReadWriteNodes)
                {
                    if( !string.IsNullOrEmpty(sCurrentNode))
                        EnableControls(sCurrentNode, oPage);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Dynamically update all the modified controls in the Page
        /// </summary>
        /// <param name="oMessageElement"></param>
        public static void UpdateModifiedControls(IEnumerable<XElement> oModifiedControlsList, Page oPage)
        {
            string[] sModifiedControls = new String[50];
            string sControlType = "";
            string sControlID = "";
            string sOldAttribute = "";
            string sNewAttribute = "";
            string m_SuppleGridControlIds = "";
            Control ctrlTemp = null;
            try
            {
                if (oModifiedControlsList != null)
                {
                    foreach (XElement oEle in oModifiedControlsList)
                    {
                        sControlType = oEle.Attribute("ControlType").Value;
                        sControlID = oEle.Attribute("id").Value;

                        switch (sControlType)
                        {
                            case "FormButton":
                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("param");
                                    if (oAttr != null)
                                    {
                                        sOldAttribute = ((Button)ctrlTemp).OnClientClick;
                                        int iStartPos = sOldAttribute.IndexOf("'");
                                        int iEndPos = sOldAttribute.IndexOf("',", iStartPos);
                                        if (iEndPos > iStartPos)
                                        {
                                            string sTemp = sOldAttribute.Substring(iStartPos + 1, (iEndPos - iStartPos - 1));
                                            sNewAttribute = sOldAttribute.Replace(sTemp, oAttr.Value);
                                            ((Button)ctrlTemp).OnClientClick = sNewAttribute;

                                        }
                                        ///Added for changing the PostBackUrl as well.
                                        sNewAttribute = oAttr.Value;
                                        iStartPos = sNewAttribute.IndexOf("=", sNewAttribute.IndexOf("SysFormName"));
                                        iEndPos = sNewAttribute.IndexOf("&", iStartPos);
                                        if (iEndPos > iStartPos)
                                        {
                                            string sNewFormName = sNewAttribute.Substring(iStartPos + 1, (iEndPos - iStartPos - 1));
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Button")) 
                                            {
                                                ((Button)ctrlTemp).PostBackUrl = sNewFormName + ".aspx?" + sNewAttribute;
                                            }
                                            
                                        }
                                    }
                                }

                                break;

                            case "PolicyLookup":

                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("ref");
                                    if (oAttr != null)
                                    {
                                        ((WebControl)ctrlTemp).Attributes["RMXRef"] = oAttr.Value;
                                    }
                                }

                                ctrlTemp = oPage.FindControl(sControlID + "_open");
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("param");
                                    if (oAttr != null)
                                    {
                                        //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Button"))
                                        {
                                            sOldAttribute = ((Button)ctrlTemp).OnClientClick;
                                        }
                                        int iStartPos = sOldAttribute.IndexOf("'");
                                        int iEndPos = sOldAttribute.IndexOf("',", iStartPos);
                                        if (iEndPos > iStartPos)
                                        {
                                            string sTemp = sOldAttribute.Substring(iStartPos + 1, (iEndPos - iStartPos - 1));
                                            sNewAttribute = sOldAttribute.Replace(sTemp, oAttr.Value);
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Button"))
                                            {
                                                ((Button)ctrlTemp).OnClientClick = sNewAttribute;
                                            }

                                        }

                                    }
                                }

                                ctrlTemp = oPage.FindControl(sControlID + "_cid");
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("idref");
                                    if (oAttr != null)
                                    {
                                        ((WebControl)ctrlTemp).Attributes["RMXRef"] = oAttr.Value;
                                    }
                                }

                                break;

                            case "Labels":

                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("Text");
                                    if (oAttr != null)
                                    {
                                        //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                        {
                                            ((Label)ctrlTemp).Text = oAttr.Value;
                                        }
                                    }
                                    oAttr = oEle.Attribute("Width");
                                    if (oAttr != null)
                                    {
                                        //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                        {
                                            ((Label)ctrlTemp).Attributes["class"] = "";
                                            ((Label)ctrlTemp).Attributes["Style"] = "float: left;text-align: left;left: 1%;padding-right: 2%;font-size: 10pt;font-family: Tahoma, Arial, Helvetica, sans-serif;color: #333333";
                                            ((Label)ctrlTemp).Attributes["Width"] = oAttr.Value;
                                        }
                                    }
                                    oAttr = oEle.Attribute("Class");
                                    if (oAttr != null)
                                    {
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                        {
                                            ((Label)ctrlTemp).Attributes["class"] = oAttr.Value;
                                        }
                                    }
                                }
                                break;
                            case "ComboBox":
                                ctrlTemp = oPage.FindControl("lbl_" + sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("Text");
                                    if (oAttr != null)
                                    {
                                        //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Label"))
                                        {
                                            ((Label)ctrlTemp).Text = oAttr.Value;
                                        }
                                    }
                                }
                                break;
                            case "PiEntityLookup":
                                ctrlTemp = oPage.FindControl("div_" + sControlID);
                                {
                                    XAttribute oAttr = oEle.Attribute("withlink");
                                    if (oAttr != null)
                                    {
                                        Control ctrTmp = ctrlTemp.FindControl("lnk_" + sControlID);
                                        if (ctrTmp != null)
                                        {
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.LinkButton"))
                                            {
                                                ((LinkButton)ctrTmp).Visible = false;
                                            }
                                        }

                                    }
                                }
                                break;
                            case "ToggleControls":
                                //mjain8: added for controls which get visible. For ex. piotherform Type(combobox) replaced by a readonly textbox
                                ctrlTemp = oPage.FindControl("div_" + sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("Text");
                                    if (oAttr != null)
                                    {
                                        if (oAttr.Value == "true")
                                        {
                                            ctrlTemp.Visible = true;
                                        }
                                        else
                                        {
                                            ctrlTemp.Visible = false;
                                        }

                                    }
                                }
                                break;
                            case "TextBox":
                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("Text");
                                    if (oAttr != null)
                                    {
                                        //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                        {
                                            ((TextBox)ctrlTemp).Text = oAttr.Value;
                                        }
                                    }

                                    //---- Added by Ashutosh Kashyap on 15-May-09 for MITS : 15797
                                    oAttr = oEle.Attribute("ReadOnly");
                                    if (oAttr != null)
                                    {
                                        //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                        {
                                            ((TextBox)ctrlTemp).ReadOnly = Convert.ToBoolean(oAttr.Value);
                                        }
                                    }

                                    oAttr = oEle.Attribute("CssClass");
                                    if (oAttr != null)
                                    {
                                        if (string.Compare(oAttr.Value.ToString().ToLower(), "disabled") == 0)
                                        {
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                            {
                                                ((TextBox)ctrlTemp).Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                                            }
                                        }
                                        else
                                        {
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                            {
                                                ((TextBox)ctrlTemp).CssClass = oAttr.Value;
                                            }
                                        }
                                    }
                                    //-------------------------------------------------------------
                                    
                                }
                                break;
                            case "RadioButton":
                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("Text");
                                    if (oAttr != null)
                                    {
                                        //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                        if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.RadioButton")) 
                                        {
                                            ((RadioButton)ctrlTemp).Text = oAttr.Value;
                                        }
                                    }
                                }

                                break;
                            case "Hidden":
                                //MITS 14164 changes done as control if already on the page need not be added again
                                XAttribute oNewAttr = oEle.Attribute("NewControl");
                                if (oNewAttr != null)
                                {
                                    TextBox ctrlNew = new TextBox();
                                    TextBox ctrlExisting = new TextBox();
                                    
                                    //smishra25 07/07/2009: Commented the if condition which was added below
                                    //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                    //if (oPage.Form.FindControl(sControlID) != null)
                                    //{
                                    //    if (oPage.Form.FindControl(sControlID).GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                    //    {
                                            ctrlExisting = (TextBox)oPage.Form.FindControl(sControlID);
                                    //    }
                                    //}

                                            if (ctrlExisting == null)
                                            {
                                                ctrlNew.Attributes["style"] = "display:none";
                                                //ctrlNew.Attributes["runat"] = "server";
                                                ctrlNew.ID = sControlID;
                                                if (oEle.Attribute("Text") != null)
                                                {
                                                    //Start by Shivendu for MITS 17520
                                                    if (oEle.Attribute("id") != null && oEle.Attribute("id").Value.IndexOf("_zapatecgridxml") > 0 )
                                                    {
                                                        oEle.Attribute("Text").Value = oEle.Attribute("Text").Value.Replace("amp;", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                                                        m_SuppleGridControlIds = m_SuppleGridControlIds + sControlID + "|";
                                                    }
                                                   
                                                    //End by Shivendu for MITS 17520
                                                    ctrlNew.Text = oEle.Attribute("Text").Value;
                                                }
                                                if (oEle.Attribute("ref") != null)
                                                {
                                                    ctrlNew.Attributes["RMXRef"] = oEle.Attribute("ref").Value;
                                                }
                                                ctrlNew.Attributes["RMXType"] = "hidden";
                                                oPage.Form.Controls.Add(ctrlNew);
                                            }
                                            else
                                            {
                                                ctrlExisting.Attributes["style"] = "display:none";
                                                if (oEle.Attribute("Text") != null)
                                                {
                                                    //Start by Shivendu for MITS 17520
                                                    if (oEle.Attribute("id") != null && oEle.Attribute("id").Value.IndexOf("_zapatecgridxml") > 0 && oPage.Form.Action.ToLower().Contains("split.aspx"))
                                                    {
                                                        oEle.Attribute("Text").Value = oEle.Attribute("Text").Value.Replace("amp;", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                                                        m_SuppleGridControlIds = m_SuppleGridControlIds + sControlID + "|";
                                                    }
                                                   
                                                    //End by Shivendu for MITS 17520 oEle.Attribute("Text").Value = oEle.Attribute("Text").Value.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "'");
                                                    ctrlExisting.Text = oEle.Attribute("Text").Value;
                                                }
                                                if (oEle.Attribute("ref") != null)
                                                {
                                                    ctrlExisting.Attributes["RMXRef"] = oEle.Attribute("ref").Value;
                                                }
                                                ctrlExisting.Attributes["RMXType"] = "hidden";

                                            }

                                }

                                break;

                            case "Date":
                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("readonly");
                                    if (oAttr != null)
                                    {
                                        if (oAttr.Value == "true")
                                        {
                                            //((TextBox)ctrlTemp).Enabled = false;
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox"))
                                            {
                                                ((TextBox)ctrlTemp).ReadOnly = true;
                                                ((TextBox)ctrlTemp).Style.Add(HtmlTextWriterStyle.BackgroundColor, "#F2F2F2");
                                            }
                                            ctrlTemp = oPage.FindControl(sControlID + "btn");
                                            if (ctrlTemp != null)
                                            {
                                                //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                                if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Button")) 
                                                {
                                                    ((Button)ctrlTemp).Enabled = false;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //((TextBox)ctrlTemp).Enabled = true;
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.TextBox")) 
                                            {
                                                ((TextBox)ctrlTemp).ReadOnly = false;
                                            }
                                            ctrlTemp = oPage.FindControl(sControlID + "btn");
                                            if (ctrlTemp != null)
                                            {
                                                //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                                if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.Button")) 
                                                {
                                                    ((Button)ctrlTemp).Enabled = true;
                                                }
                                            }
                                        }
                                    }
                                }
                                break;

                            case "Checkbox":
                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("readonly");
                                    if (oAttr != null)
                                    {
                                        if (oAttr.Value == "true")
                                        {
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))  
                                            {
                                                ((CheckBox)ctrlTemp).Enabled = false;
                                            }
                                        }
                                        else
                                        {
                                            //Raman 07/02/2009 : Adding a null check in all parsing of controls as control types may change in readonly powerviews
                                            if (ctrlTemp.GetType().ToString().Equals("System.Web.UI.WebControls.CheckBox"))      
                                            {
                                                ((CheckBox)ctrlTemp).Enabled = true;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "PolicyNumberLookup":
                                ctrlTemp = oPage.FindControl(sControlID);
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("ref");
                                    if (oAttr != null)
                                    {
                                        ((WebControl)ctrlTemp).Attributes["RMXRef"] = oAttr.Value;
                                    }
                                    oAttr = oEle.Attribute("type");
                                    if (oAttr != null)
                                    {
                                        ((WebControl)ctrlTemp).Attributes["RMXType"] = oAttr.Value;
                                    }

                                }
                                ctrlTemp = oPage.FindControl(sControlID + "_cid");
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("idref");
                                    if (oAttr != null)
                                    {
                                        ((WebControl)ctrlTemp).Attributes["RMXRef"] = oAttr.Value;
                                    }
                                }
                                ctrlTemp = oPage.FindControl(sControlID + "btn");
                                //PJS MITS 16563 - corrected onclientclick to onclick below
                                if (ctrlTemp != null)
                                {
                                    XAttribute oAttr = oEle.Attribute("onclientclickparameters");
                                    if (oAttr != null)
                                    {
                                        ((WebControl)ctrlTemp).Attributes["onclick"] = oAttr.Value;
                                    }
                                }
                                break;
                        }
                    }
                }
                //Start by Shivendu for MIST 17520
                TextBox ctrlSuppleGridIds = (TextBox)oPage.FindControl("txtSuppleGridIds");
                if (ctrlSuppleGridIds != null)
                {
                    ctrlSuppleGridIds.Text = m_SuppleGridControlIds;
                }

                //End by Shivendu for MIST 17520
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Update control's attribute in the page
        /// </summary>
        /// <param name="oControlAppendAttributeList"></param>
        /// <param name="oPage"></param>
        public static void UpdateControlAttributes(XElement oControlAppendAttributeList, Page oPage)
        {
            Control ctrlTemp = null;
            Control ctrlAssociated = null;
            //XElement objAttListNode = null;

            try
            {
                if (oControlAppendAttributeList == null)
                    return;

                foreach (XElement objNode in oControlAppendAttributeList.Nodes())
                {
                    ctrlTemp = (Control)oPage.FindControl(objNode.Name.LocalName);

                    // The Tabs have LinkTabs appended infront of the name
                    if (ctrlTemp == null)
                    {
                        ctrlTemp = (Control)oPage.FindControl("LINKTABS" + objNode.Name.LocalName);
                    }

                    if (ctrlTemp == null)
                        continue;
                    
                    //Here may need a loop for each child nodes, not only for the first child
                    //pmittal5 Mits 16471 - When Claim is opened for editing, "Event Number" field has 2 child attribute nodes
                    //objAttListNode = (XElement)objNode.FirstNode;
                    foreach (XElement objAttListNode in objNode.Nodes())
                    {
                        if (objAttListNode == null)
                            continue;

                        if (ctrlTemp is WebControl)
                        {
                            switch (ctrlTemp.GetType().ToString())
                            {
                                case "System.Web.UI.WebControls.Button":

                                    if (string.Compare(objAttListNode.Name.LocalName, "disabled", true) == 0)
                                    {
                                        //((Button)ctrlTemp).Enabled = (objAttListNode.Attribute("value").Value == "true" ? false : true);
                                        bool bDisabled = ((objAttListNode.Attribute("value").Value == "true" ? true : false));
                                        if (((Button)ctrlTemp).Attributes["disabled"] == null)
                                        {
                                            if (bDisabled)
                                            {
                                                ((Button)ctrlTemp).Attributes.Add("disabled", "disabled");
                                            }
                                            else
                                            {
                                                ((Button)ctrlTemp).Attributes.Remove("disabled");
                                            }
                                        }
                                        else
                                        {
                                            if (bDisabled)
                                            {
                                                ((Button)ctrlTemp).Attributes["disabled"] = "disabled";
                                            }
                                            else
                                            {
                                                ((Button)ctrlTemp).Attributes.Remove("disabled");
                                            }
                                        }

                                        //((Button)ctrlTemp).Style.Add("disabled", sBool.ToLower());

                                    }
                                    else if (string.Compare(objAttListNode.Name.LocalName, "title", true) == 0)
                                    {
                                        ((Button)ctrlTemp).Text = objAttListNode.Attribute("value").Value;

                                    }
                                    //amrit: added condition for handling mits:14704
                                    if (string.Compare(objAttListNode.Name.LocalName, "visible", true) == 0)
                                    {
                                        //((Button)ctrlTemp).Enabled = (objAttListNode.Attribute("value").Value == "true" ? false : true);
                                        bool bVisible = ((objAttListNode.Attribute("value").Value == "true" ? true : false));
                                        //if (((Button)ctrlTemp).Attributes["disabled"] == null)
                                        //{
                                        if (bVisible)
                                        {
                                            // ((Button)ctrlTemp).Attributes.Add("visible", "visible");
                                            ((Button)ctrlTemp).Visible = true;

                                        }
                                        else
                                        {
                                            //((Button)ctrlTemp).Attributes.Remove("visible");
                                            ((Button)ctrlTemp).Visible = false;

                                        }
                                    }
                                    //mits:14704 ends
                                    //if (string.Compare(objNode.Name.LocalName, "physicianslist1btn", true) == 0)
                                    //{
                                    if (string.Compare(objAttListNode.Name.LocalName, "onclientclick", true) == 0)
                                    {
                                        (((Button)ctrlTemp)).OnClientClick = objAttListNode.Attribute("value").Value;
                                        //((Button)ctrlTemp).Attributes.Remove("onclientclick");
                                        //((Button)ctrlTemp).Attributes.Add("onclick", "return lookupData('physicianslist1','PHYSICIANS',4,'physicianslist1',3)");
                                        //"return lookupData('physicianslist1','PHYSICIANS,MEDICAL_STAFF',4,'physicianslist1',3)"
                                    }
                                    break;
                                //Added for Mits 28937
                                case "System.Web.UI.WebControls.ImageButton":

                                    if (string.Compare(objAttListNode.Name.LocalName, "disabled", true) == 0)
                                    {
                                        bool bDisabled = ((objAttListNode.Attribute("value").Value == "true" ? true : false));
                                        if (((ImageButton)ctrlTemp).Attributes["disabled"] == null)
                                        {
                                            if (bDisabled)
                                            {
                                                ((ImageButton)ctrlTemp).Attributes.Add("disabled", "disabled");
                                            }
                                            else
                                            {
                                                ((ImageButton)ctrlTemp).Attributes.Remove("disabled");
                                            }
                                        }
                                        else
                                        {
                                            if (bDisabled)
                                            {
                                                ((ImageButton)ctrlTemp).Attributes["disabled"] = "disabled";
                                            }
                                            else
                                            {
                                                ((ImageButton)ctrlTemp).Attributes.Remove("disabled");
                                            }
                                        }
                                    }
                                    else if (string.Compare(objAttListNode.Name.LocalName, "title", true) == 0)
                                    {
                                        ((ImageButton)ctrlTemp).ToolTip = objAttListNode.Attribute("value").Value;

                                    }
                                    if (string.Compare(objAttListNode.Name.LocalName, "visible", true) == 0)
                                    {
                                        bool bVisible = ((objAttListNode.Attribute("value").Value == "true" ? true : false));
                                        if (bVisible)
                                        {
                                            ((ImageButton)ctrlTemp).Visible = true;

                                        }
                                        else
                                        {
                                            ((ImageButton)ctrlTemp).Visible = false;

                                        }
                                    }
                                    if (string.Compare(objAttListNode.Name.LocalName, "onclientclick", true) == 0)
                                    {
                                        (((ImageButton)ctrlTemp)).OnClientClick = objAttListNode.Attribute("value").Value;
                                    }
                                    break;
                                case "System.Web.UI.WebControls.TextBox":
                                    if (objAttListNode != null)
                                    {
                                        switch (objAttListNode.Name.LocalName.ToLower())
                                        {
                                            case "readonlytext":    //pmittal5 Mits 16471 - For 'Event Number' field on Claims screen, "readOnlyText" attribute is set to make it disabled.
                                            case "disabled":
                                                if (string.Compare(objAttListNode.Attribute("value").Value, "true", true) == 0)
                                                {
                                                    DisableControls(objNode.Name.LocalName, oPage);
                                                }
                                                else
                                                {
                                                    EnableControls(objNode.Name.LocalName, oPage);
                                                }
                                                break;

                                            case "title":
                                                ctrlAssociated = (Control)oPage.FindControl("lbl_" + objNode.Name.LocalName);
                                                if (ctrlAssociated != null)
                                                {
                                                    ((Label)ctrlAssociated).Text = objAttListNode.Attribute("value").Value;
                                                }
                                                break;

                                            case "linktobutton":
                                                ctrlAssociated = (Control)oPage.FindControl(objNode.Name + "_open");
                                                if (ctrlAssociated != null)
                                                {
                                                    ((Button)ctrlAssociated).Visible = true;
                                                }
                                                break;
                                            case "ref":
                                                //Mona:Value of RMXRef is coming from business layer on Policy Enhance screen
                                                ((TextBox)ctrlTemp).Attributes["RMXRef"] = objAttListNode.Attribute("value").Value;
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        if (string.Compare(objNode.Name.LocalName, "ev_eventnumber", true) == 0)
                                        {
                                            ctrlAssociated = (Control)oPage.FindControl(objNode.Name.LocalName + "_open");
                                            if (ctrlAssociated != null)
                                            {
                                                ((Button)ctrlAssociated).Visible = false;
                                            }
                                        }
                                    }
                                    break;
                                case "System.Web.UI.WebControls.CheckBox":
                                    if (string.Compare(objAttListNode.Name.LocalName, "ref", true) == 0)
                                    {//Mona:Value of RMXRef is coming from business layer on Policy Enhance screen
                                        ((CheckBox)ctrlTemp).Attributes["RMXRef"] = objAttListNode.Attribute("value").Value;
                                    }
                                    break;
                                // abhateja 01.08.2009
                                // Need to get ref at run-time based on whether account or sub account is being used.
                                case "System.Web.UI.WebControls.DropDownList":
                                    //if (string.Compare(objAttListNode.Name.LocalName, "ref", true)==0)
                                    //{
                                    //    ((DropDownList)ctrlTemp).Attributes["RMXRef"] = objAttListNode.Attribute("value").Value;
                                    //}
                                    // npadhy Need to associate some more attributes with DropDownList. So removing the If condition
                                    // and putting a switch case instead
                                    if (objAttListNode != null)
                                    {
                                        switch (objAttListNode.Name.LocalName.ToLower())
                                        {
                                            case "title":
                                                ctrlAssociated = (Control)oPage.FindControl("lbl_" + objNode.Name.LocalName);
                                                if (ctrlAssociated != null)
                                                {
                                                    ((Label)ctrlAssociated).Text = objAttListNode.Attribute("value").Value;
                                                }
                                                break;
                                            case "ref":
                                                ctrlAssociated = (Control)oPage.FindControl(objNode.Name.LocalName);
                                                if (ctrlAssociated != null)
                                                {
                                                    ((DropDownList)ctrlTemp).Attributes["RMXRef"] = objAttListNode.Attribute("value").Value;
                                                }
                                                break;
                                            case "onchange":
                                                ctrlAssociated = (Control)oPage.FindControl(objNode.Name.LocalName);
                                                if (ctrlAssociated != null)
                                                {
                                                    ((DropDownList)ctrlTemp).Attributes["onchange"] = objAttListNode.Attribute("value").Value;
                                                }
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                        else if (ctrlTemp is HtmlControl)
                        {
                            switch (ctrlTemp.GetType().ToString())
                            {
                                case "System.Web.UI.HtmlControls.HtmlAnchor":
                                    if (string.Compare(objAttListNode.Name.LocalName, "title", true) == 0)
                                    {
                                        ((HtmlAnchor)ctrlTemp).Title = objAttListNode.Attribute("value").Value;
                                        ((HtmlAnchor)ctrlTemp).InnerText = objAttListNode.Attribute("value").Value;
                                        ((HtmlAnchor)ctrlTemp).InnerHtml = objAttListNode.Attribute("value").Value;

                                    }
                                    break;
                            }
                        }
                        else if (ctrlTemp is CodeLookUp)
                        {
                            switch (objAttListNode.Name.LocalName.ToLower())
                            {
                                case "disabled":
                                    if (string.Compare(objAttListNode.Attribute("value").Value, "true", true) == 0)
                                    {
                                        DisableControls(objNode.Name.LocalName, oPage);
                                    }
                                    else
                                    {
                                        EnableControls(objNode.Name.LocalName, oPage);
                                    }
                                    break;

                                case "title":
                                    ctrlAssociated = (Control)oPage.FindControl("lbl_" + objNode.Name);
                                    if (ctrlAssociated != null)
                                    {
                                        ((Label)ctrlAssociated).Text = objAttListNode.Attribute("value").Value;
                                    }
                                    break;
                                case "ref":
                                    ((CodeLookUp)ctrlTemp).Attributes["RMXRef"] = objAttListNode.Attribute("value").Value;
                                    foreach (Control ctrlWithInUserControl in ctrlTemp.Controls)
                                    {
                                        if (ctrlWithInUserControl.ClientID == (objNode.Name + "_codelookup"))
                                        {
                                            ((TextBox)ctrlWithInUserControl).Attributes["RMXRef"] = objAttListNode.Attribute("value").Value;
                                        }
                                        if (ctrlWithInUserControl.ClientID == (objNode.Name + "_codelookup_cid"))
                                        {
                                            ((TextBox)ctrlWithInUserControl).Attributes["RMXRef"] = objAttListNode.Attribute("value").Value + "/@codeid";
                                        }
                                    }
                                    break;
                            }
                        }
                        else if (ctrlTemp is UserControlDataGrid)
                        {
                            if (string.Compare(objAttListNode.Name.LocalName, "hidebuttons", true) == 0)
                            {
                                ((UserControlDataGrid)ctrlTemp).HideButtons = objAttListNode.Attribute("value").Value;
                            }
                        }
                    }  
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }



        /// <summary>
        /// Display the content of the selected tab.
        /// </summary>
        /// <param name="oPage"></param>
        public static void UpdateTabFocus(Page oPage)
        {
            try
            {
                //Geeta : Added a null check for hTabName.As code was throwing exception for list classes
                if ((TextBox)oPage.FindControl("hTabName") != null)
                {
                    string sSelectedTab = ((TextBox)oPage.FindControl("hTabName")).Text;

                    if (sSelectedTab != "")
                    {
                        HtmlContainerControl oCtrls = (HtmlContainerControl)oPage.FindControl("TabsDivGroup");

                        if (oCtrls != null)
                        {
                            //Deselect All
                            for (int i = 0; i < oCtrls.Controls.Count; i++)
                            {
                                if (oCtrls.Controls[i] != null && oCtrls.Controls[i].ID != null)
                                {
                                    if (oCtrls.Controls[i].ID.Substring(0, 4) == "TABS")
                                    {
                                        string sControlId = oCtrls.Controls[i].ID.Substring(4);

                                        HtmlContainerControl oDivScreenControl = (HtmlContainerControl)oPage.FindControl("FORMTAB" + sControlId);
                                        if (oDivScreenControl != null)
                                        {
                                            oDivScreenControl.Style.Add(HtmlTextWriterStyle.Display, "none");
                                        }

                                        HtmlContainerControl oDivTabControl = (HtmlContainerControl)oPage.FindControl("TABS" + sControlId);

                                        if (oDivTabControl != null)
                                        {
                                            oDivTabControl.Attributes["class"] = "NotSelected";
                                        }

                                        HtmlAnchor oDivLink = (HtmlAnchor)oPage.FindControl("LINKTABS" + sControlId);
                                        if (oDivLink != null)
                                        {
                                            oDivLink.Attributes["class"] = "NotSelected1";
                                        }
                                    }
                                }
                            }

                            // Select the Required One
                            HtmlContainerControl oDivScreenControlSelected = (HtmlContainerControl)oPage.FindControl("FORMTAB" + sSelectedTab);
                            if (oDivScreenControlSelected != null)
                            {
                                oDivScreenControlSelected.Style.Add(HtmlTextWriterStyle.Display, "");
                            }

                            HtmlContainerControl oDivTabControlSelected = (HtmlContainerControl)oPage.FindControl("TABS" + sSelectedTab);
                            if (oDivTabControlSelected != null)
                            {
                                oDivTabControlSelected.Attributes["class"] = "Selected";
                            }

                            HtmlAnchor oDivLinkSelected = (HtmlAnchor)oPage.FindControl("LINKTABS" + sSelectedTab);
                            if (oDivLinkSelected != null)
                            {
                                oDivLinkSelected.Attributes["class"] = "Selected";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// Find the control which initiate the postback
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static Control GetPostBackControl(Page page)
        {
            Control postbackControlInstance = null;

            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if (ctrlname != null && ctrlname != string.Empty)
            {
                postbackControlInstance = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.Button)
                    {
                        postbackControlInstance = c;
                        break;
                    }
                }
            }

            // handle the ImageButton postbacks
            if (postbackControlInstance == null)
            {
                for (int i = 0; i < page.Request.Form.Count; i++)
                {
                    if ((page.Request.Form.Keys[i].EndsWith(".x")) || (page.Request.Form.Keys[i].EndsWith(".y")))
                    {
                        postbackControlInstance = page.FindControl(page.Request.Form.Keys[i].Substring(0, page.Request.Form.Keys[i].Length - 2));
                        return postbackControlInstance;
                    }
                }
            }

            return postbackControlInstance;
        }


        /// <summary>
        /// Find a control recursively.
        /// </summary>
        /// <param name="root"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ClientID == id)
            {
                return root;
            }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);
                if (t != null)
                {
                    return t;
                }
            }

            return null;
        }

        /// <summary>
        /// Perform supp field group association
        /// </summary>
        /// <param name="oMessageElement"></param>
        public static void PerformGroupAssociation(Page oPage)
        {
            Control oControl = oPage.FindControl("GroupAssocFieldList");
            if (oControl == null)
                return;

            TextBox oGroupAssocFieldList = (TextBox)oControl;
            string sGroupAssocFieldList = oGroupAssocFieldList.Text;
            if (string.IsNullOrEmpty(sGroupAssocFieldList))
                return;

            string[] arGroupAssocFieldList = sGroupAssocFieldList.Split('|');
            foreach (string sField in arGroupAssocFieldList)
            {
                TextBox oGroupAssocField = (TextBox)oPage.FindControl(sField + "_GroupAssoc");
                if (oGroupAssocField == null)
                    return;

                string sGroupAssocFields = oGroupAssocField.Text;
                if (string.IsNullOrEmpty(sGroupAssocFields))
                    return;

                //MITS 19433 It could be controlled by multiple fields
                string[] arGroupAssocFields = sGroupAssocFields.Split(m_GAFieldDelimiter);
                string sClientControlNames = string.Empty;
                bool bVisible = false;
                foreach (string sGroupAssocField in arGroupAssocFields)
                {
                    string[] arGroupAssocField = sGroupAssocField.Split(':');
                    string sServerControlName = arGroupAssocField[0];
                    string sCriteriaValue = arGroupAssocField[1];

                    Control oGAControl = oPage.FindControl(sServerControlName);
                    if (oGAControl == null)
                        continue;

                    string sControlValue = string.Empty;
                    string sClientControlName = string.Empty;
                    string sControlType = oGAControl.GetType().ToString();
                    switch (sControlType)
                    {
                        case "System.Web.UI.WebControls.TextBox":
                            sClientControlName = oGAControl.ClientID;
                            sControlValue = ((TextBox)oGAControl).Text;
                            break;
                        case "ASP.ui_shared_controls_codelookup_ascx":
                            sClientControlName = ((CodeLookUp)oGAControl).GroupAssocClientId;
                            sControlValue = ((CodeLookUp)oGAControl).CodeId;
                            break;
                        case "ASP.ui_shared_controls_multicodelookup_ascx":
                            sClientControlName = ((MultiCodeLookUp)oGAControl).GroupAssocClientId;
                            sControlValue = ((MultiCodeLookUp)oGAControl).ListCodes;
                            break;
                        case "System.Web.UI.WebControls.DropDownList":
                            sClientControlName = ((DropDownList)oGAControl).ClientID;
                            sControlValue = ((DropDownList)oGAControl).SelectedValue;
                            break;
                    }
                    if (!string.IsNullOrEmpty(sClientControlNames))
                        sClientControlNames += m_GAFieldDelimiter;
                    sClientControlNames += sClientControlName;

                    //For multiple code list, we have to check each value
                    sControlValue = sControlValue.Trim();
                    if (!string.IsNullOrEmpty(sControlValue))
                    {
                        string[] arControlValues = sControlValue.Split(' ');
                        foreach (string sValue in arControlValues)
                        {
                            if (sCriteriaValue.IndexOf(m_GAValueDelimiter + sControlValue + m_GAValueDelimiter) >= 0)
                            {
                                bVisible = true;
                                break;
                            }
                        }
                    }
                }
                oGroupAssocField.Attributes.Add("clientcontrolname", sClientControlNames);

                Control oDivControl = oPage.FindControl("div_" + sField);
                if (oDivControl == null)
                    continue;

                HtmlControl oHtmlDivControl = (HtmlControl)oDivControl;

                if (bVisible)
                    oHtmlDivControl.Style.Remove("display");
                else
                    oHtmlDivControl.Style.Add("display", "none");
            }
        }

        public static void UpdateSysRequired(XElement oSysRequiredFields, Page oPage)
        {
            string[] sNodes = new String[50];

            try
            {
                if (oSysRequiredFields != null)
                {
                    sNodes = oSysRequiredFields.Value.Split('|');
                }
                // Changed the Implementation of this Function as UpdateControlAttributes is also disabling the controls.
                // So removed the logic of disabling controls from here and putting in function
                foreach (string sCurrentNode in sNodes)
                {
                    TextBox oTextBox =(TextBox)oPage.FindControl("SysRequired");
                    if (oTextBox != null)
                    {
                        //Mits36244
                        if (oTextBox.Text.Contains("\n"))
                        {
                            oTextBox.Text = oTextBox.Text.ToString().Replace("\n", string.Empty).Replace(" ", "");
                        }
                        oTextBox.Text = oTextBox.Text.Replace("|" + sCurrentNode + "|", "|");
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
     

}
